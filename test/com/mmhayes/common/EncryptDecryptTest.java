package com.mmhayes.common;

import com.mmhayes.common.utils.MMHCommonItemsLibrary;
import com.mmhayes.common.utils.StringFunctions;
import com.sun.jna.Native;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


public class EncryptDecryptTest {

    private String plainTextPassword = "QCSuper";
    private String encodedPassword;

    @BeforeMethod(alwaysRun = true)
    public void setup() {
        MMHCommonItemsLibrary.setJnaLibraryPathTest();
    }

    @Test
    public void A_encrypt() {

        MMHCommonItemsLibrary.MMHCommonItems mmhPass = Native.load(MMHCommonItemsLibrary.getDLL(), MMHCommonItemsLibrary.MMHCommonItems.class);
        encodedPassword = mmhPass.Encrypt(plainTextPassword);
        System.out.println("Encrypted Password: "+encodedPassword);
        assertThat(encodedPassword, anything(encodedPassword));

    }

    @Test
    public void B_decrypt() {

        MMHCommonItemsLibrary.MMHCommonItems mmhPass = Native.load(MMHCommonItemsLibrary.getDLL(), MMHCommonItemsLibrary.MMHCommonItems.class);
        String decryptedPassword = mmhPass.Decrypt(encodedPassword);
        System.out.println("Decrypted Password: " + decryptedPassword);
        assertThat(decryptedPassword, equalTo(plainTextPassword));

    }

    @Test
    public void D_decodePasswordSCrypt(){

        String passwordToDecode = "123038821449099008797668";
        String decodedPassword = StringFunctions.decodePassword(passwordToDecode);
        System.out.println("Decoded Password: " + decodedPassword);
        assertThat(decodedPassword, equalTo("QCSuper"));

    }

    @Test
    public void E_decodePasswordNew(){

        String decodedPassword = StringFunctions.decodePassword(encodedPassword);
        System.out.println("Decoded Password: " + decodedPassword);
        assertThat(decodedPassword, equalTo("QCSuper"));

    }

   /* @Test
    public void Z_beep() {

        MMHCommonItemsLibrary.Kernel32 lib = Native.load("kernel32", MMHCommonItemsLibrary.Kernel32.class);
        lib.Beep(440, 500);
        assertThat("true", equalTo("true"));

    }*/

}
