package com.mmhayes.common;

import java.util.List;

import static java.util.Arrays.asList;

/**
 * Created by admin on 5/12/2017.
 */
public class AddressSplitter {
    private AddressInputQueue addressInputQueue;
    public List<String> split() {
        return asList(addressInputQueue.next().split(","));
    }
}
