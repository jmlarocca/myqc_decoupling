package com.mmhayes.common;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by admin on 5/12/2017.
 */
public class AddressSplitterTest {
    @InjectMocks private AddressSplitter subject = new AddressSplitter();
    @Mock private AddressInputQueue addressInputQueue;
    @BeforeMethod(alwaysRun = true)
    public void injectDoubles() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void splitsAddressByComma() {
        when(addressInputQueue.next()).thenReturn("jim@w.com,kent@b.com");
        List<String> result = subject.split();
        assertThat(result, hasItems("jim@w.com", "kent@b.com"));
    }
}
