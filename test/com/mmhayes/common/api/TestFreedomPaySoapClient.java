package com.mmhayes.common.api;

import com.mmhayes.common.processors.freedompay.freeway.*;
import com.mmhayes.common.utils.MMHProperties;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/* Account Detail Model
Last Updated (automatically updated by SVN)
$Author: jrmitaly $: Author of last commit
$Date: 2016-06-28 12:45:31 -0400 (Tue, 28 Jun 2016) $: Date of last commit
$Rev: 16814 $: Revision of last commit
Notes: Test for sending FreedomPay Soap Request
*/

public class TestFreedomPaySoapClient {

     private String requestID = "";

     public TestFreedomPaySoapClient(){

     }

    @BeforeClass
    public void setUp() {
        // code that will be invoked when this test is instantiated
        MMHProperties.APP_SETTINGS_FILE = "app.properties";

        // App Specific
        MMHProperties.SQL_STRINGS_FILE = "QUICKCHARGE_SQLStringsSQLSERVER.properties";
        MMHProperties.projectSQLFile = "QUICKCHARGE_SQLStrings";
        String propertiesPath = "C:\\IdeaProjects\\Kryptonite_9.0.2\\src\\com\\mmhayes\\quickcharge\\properties";
        String sqlPropertiesPath = "C:\\IdeaProjects\\Kryptonite_9.0.2\\src\\com\\mmhayes\\quickcharge\\properties";

        // LOAD
        //SC2P: do not allow properties from files
        //MMHProperties.loadApplicationPropertiesFile(propertiesPath,"","");
        MMHProperties.loadSQLPropertyFiles(sqlPropertiesPath);
    }

    @Test
    public void charge(){

        String storeID= "1462136017";
        String terminalID= "2467015011";
        String CARD_TYPE = "token";
        String CARD_TOKEN = "8150130049736420";
        String DECISION_ACCEPT = "ACCEPT";

        // Build Card
        Card card = new Card();
        card.setCardType(CARD_TYPE);
        card.setAccountNumber(CARD_TOKEN);

        PurchaseTotals purchaseTotals = new PurchaseTotals();
        purchaseTotals.setTaxTotal("0");
        purchaseTotals.setChargeAmount("100");

        CCAuthService ccAuthService = new CCAuthService();
        ccAuthService.setRun("true");

        CCCaptureService ccCaptureService = new CCCaptureService();
        ccCaptureService.setRun("true");

        // Build Request
        RequestMessage requestMessage = new RequestMessage();
        requestMessage.setStoreId(storeID);
        requestMessage.setTerminalId(terminalID);
        requestMessage.setCard(card);
        requestMessage.setPurchaseTotals(purchaseTotals);
        requestMessage.setCcAuthService(ccAuthService);
        requestMessage.setCcCaptureService(ccCaptureService);
        //requestMessage.setComments("com.mmhayes.common.api.TestFreedomPaySoapClient.charge()");

        // Method tests sending a soap Request
        FreewayService freewayService = new FreewayService();
        ReplyMessage reply = freewayService.getFreewayServiceSoap().submit(requestMessage);
        requestID = reply.getRequestID();

        Assert.assertEquals(reply.getDecision(),DECISION_ACCEPT);
        Assert.assertNotEquals(reply.getRequestID(), "");
    }

    @Test
    public void refund(){

        String storeID= "1462136017";
        String terminalID= "2467015011";
        String CARD_TYPE = "token";
        String CARD_TOKEN = "8150130049736420";
        String DECISION_ACCEPT = "ACCEPT";

        // Build Card
        Card card = new Card();
        card.setCardType(CARD_TYPE);
        card.setAccountNumber(CARD_TOKEN);

        PurchaseTotals purchaseTotals = new PurchaseTotals();
        purchaseTotals.setTaxTotal("0");
        purchaseTotals.setChargeAmount("100");

        CCCreditService ccCreditService = new CCCreditService();
        ccCreditService.setRun("true");

        // Build Request
        RequestMessage requestMessage = new RequestMessage();
        requestMessage.setStoreId(storeID);
        requestMessage.setTerminalId(terminalID);
        requestMessage.setCard(card);
        requestMessage.setPurchaseTotals(purchaseTotals);
        requestMessage.setCcCreditService(ccCreditService);
        //requestMessage.setComments("com.mmhayes.common.api.TestFreedomPaySoapClient.refund()");

        // Add orderRequestID from Above
        requestMessage.setOrderRequestID(requestID);

        // Method tests sending a soap Request
        FreewayService freewayService = new FreewayService();
        ReplyMessage reply = freewayService.getFreewayServiceSoap().submit(requestMessage);

        Assert.assertEquals(reply.getDecision(),DECISION_ACCEPT);
    }
}
