package com.mmhayes.common.api;

import com.mmhayes.common.utils.MMHEmail;
import com.mmhayes.common.utils.MMHProperties;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/* Account Detail Model
Last Updated (automatically updated by SVN)
$Author: jrmitaly $: Author of last commit
$Date: 2016-06-28 12:45:31 -0400 (Tue, 28 Jun 2016) $: Date of last commit
$Rev: 16814 $: Revision of last commit
Notes: Test for sending an email using gmail account
*/

public class SendEmailTest {

     public SendEmailTest(){

     }

    @BeforeClass
    public void setUp() {
        // code that will be invoked when this test is instantiated
        MMHProperties.APP_SETTINGS_FILE = "app.properties";
        MMHProperties.SQL_STRINGS_FILE = "MYQUICKCHARGE_SQLStringsSQLSERVER.properties";
        MMHProperties.projectSQLFile = "MYQUICKCHARGE_SQLStrings";
        //SC2P: do not allow properties from files
        //MMHProperties.loadApplicationPropertiesFile("C:\\IdeaProjects\\MyQC_1.3\\src\\com\\mmhayes\\myqc\\properties","","");
        MMHProperties.loadSQLPropertyFiles("C:\\IdeaProjects\\MyQC_1.3\\src\\com\\mmhayes\\myqc\\properties");
    }

    @Test
    public void sendTestEmail(){
        // Method tests sending an email via with property configurations

        // initialize return value
        boolean result;

        // Required method param - String toAddress
        String toAddress = "mmhayesdev@gmail.com";

        // Required method param - String subject
        String subject = "Send Email Test";

        // Required method param - String htmlBody
        String htmlBody = "<html>"+
                "<head>J</head>"+
                "<body><div style='margin:2em;border:1 solid blue'>TEST EMAIL BODY</div></body>"+
                "</html>";

        // EXECUTE TEST METHOD
        result = MMHEmail.sendEmail(toAddress, subject, htmlBody);

        // CHECK Outcome
        Assert.assertEquals(result, true);
    }
}
