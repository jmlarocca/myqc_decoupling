package com.mmhayes.common.api;

import com.mmhayes.common.utils.Logger;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.testng.IObjectFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.ObjectFactory;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.LinkedList;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Demo of TestNG plus PowerMock to test a method which calls static methods in other classes
 */
@SuppressStaticInitializationFor("com.mmhayes.common.utils.Logger")
//@PrepareForTest(Logger.class)
    // normally needed, but redundant with SuppressStaticInitializerFor
public class TNGPdemo1 {
    /** Let TestNG work with a mocked static class.
     *  This is not needed in JUnit.
     */
    @ObjectFactory
    public IObjectFactory getObjectFactory() {
        return new org.powermock.modules.testng.PowerMockObjectFactory();
    }

    @BeforeTest
    public void setUp() {
        mockStatic(Logger.class);
    }

    @Test
    public void testFinalizePaginationPropertiesWithNoProperties() throws Exception{
        //Exception ex = new Exception("foo");
        //when (Logger.class, Logger.class.getMethod("logException", Exception.class)).withArguments(ex).thenReturn(null);
        //when (Math.ceil(1.1)).thenReturn(2.0);
        new ModelPaginator(new HashMap<String,LinkedList>()).finalizePaginationProperties();
        //verifyStatic();
        //Logger.logException(ex);
    }

    @Test
    public void testFinalizePaginationPropertiesWithBadProperties() throws Exception {
        HashMap<String,LinkedList> properties = new HashMap<>();
        LinkedList bar = new LinkedList();
        bar.add("abc"); // string that can't be parsed as an integer
        properties.put("MODELS", bar);
        new ModelPaginator(properties).finalizePaginationProperties();
    }
}
