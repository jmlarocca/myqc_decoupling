package com.mmhayes.common;

import com.mmhayes.common.receiptGen.ReceiptData.KPJobHeader;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by cjczyzewski on 10/25/2018.
 */
public class PhoneNumberTest {

    @Test
    public void testNull() {
        String result = KPJobHeader.sanitizePhoneNumber(null, true);
        Assert.assertEquals(result, "");
    }

    @Test
    public void testWithHyphens() {
        String result = KPJobHeader.sanitizePhoneNumber("555-432-8765", false);
        Assert.assertEquals(result, "5554328765");
    }

    @Test
    public void testWithHyphensAddUSCode() {
        String result = KPJobHeader.sanitizePhoneNumber("555-432-8765", true);
        Assert.assertEquals(result, "15554328765");
    }

    @Test
    public void testComplexPhone() {
        String result = KPJobHeader.sanitizePhoneNumber("(518) 123-4567", false);
        Assert.assertEquals(result, "5181234567");
    }

    @Test
    public void testComplexPhoneAddUSCode() {
        String result = KPJobHeader.sanitizePhoneNumber("(518) 123-4567", true);
        Assert.assertEquals(result, "15181234567");
    }

    @Test
    public void testCountryCodeAlreadyExists() {
        String result = KPJobHeader.sanitizePhoneNumber("1-519-112-5565", true);
        Assert.assertEquals(result, "15191125565");
    }

    @Test
    public void testAlreadyCorrect() {
        String result = KPJobHeader.sanitizePhoneNumber("5553213321", false);
        Assert.assertEquals(result, "5553213321");
    }

    @Test
    public void testAlreadyCorrectAddUSCode() {
        String result = KPJobHeader.sanitizePhoneNumber("5553213321", true);
        Assert.assertEquals(result, "15553213321");
    }

    @Test
    public void testShortLength() {
        String result = KPJobHeader.sanitizePhoneNumber("555-1234", false);
        Assert.assertEquals(result, "5551234");
    }

    @Test
    public void testTryToAddUSCodeButShortLength() {
        String result = KPJobHeader.sanitizePhoneNumber("555-1234", true);
        Assert.assertEquals(result, "5551234");
    }

}
