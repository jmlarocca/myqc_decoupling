/* Quickcharge Self-Service Application -> Application Setup
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-09-24 15:39:27 -0400 (Fri, 24 Sep 2021) $: Date of last commit
 $Rev: 6720 $: Revision of last commit
 Notes: Defines the qcssapp object for use across the entire application
 */

var qcssapp = { //provides global backbone scope for this application
        DebugMode: "", //default debug mode setting - "" === off
        Collections: {}, //all backbone collections
        Events: _.clone(Backbone.Events), //all backbone events
        Functions: {}, //all global functions
        Models: {}, //all backbone models
        Views: {}, //all backbone views
        Router: "",
        Location: "", //base URL for all ajax calls
        UserCode: '', //application code for the current session
        Session: "", //global session ID
        AppVersion: "6.4.0", //version of this app (the "front-end version")
        StoreSelected: "", //ID of the store the user has selected to order from
        OrderMenuHistory: [], //array to keep the history of navigating through online ordering
        CurrentOrder: { //object to keep track of the user's current order
            "products": [],
            "combos": '',
            "orderType": "",
            "orderLocation": "",
            "orderTime": "",
            "orderComments": "",
            "storeModel": "",
            "selectedRewards": [],
            "automaticRewards": [],
            "useMealPlanDiscount": true,
            "expressReorder": false,
            "expressReorderModel": "",
            "suggestedKeypadIDs" : [],
            "storePaymentModel":null,
            "token": "",
            "grabAndGo": false,
            "voucherCode": ""
        },
        orderObject: {},
        futureOrderDate: "",
        userBadge: "",
        ProductInProgress: { //object to keep track of changes to a particular product
            "id": "",
            "name": "",
            "quantity": "",
            "cartProductID": "",
            "autoMod": false,
            "modifiers": [],
            "prepOption": {},
            "substituteProduct": {} ,
            "prepOptionSetID": "",
            "productInStore": true,
            "modifierMenus": [],
            "nutritionInfo": [],
            "favorite": true
        },
        TimePicker: "",        //time picker object for future orders time selection
        ServerVersion: '',     //version of the back end server
        importAllInactive: '',
        onPhoneApp: null,  //keep track of whether the current insance is running on the phone app
        ssoLogin: false,       // User login setting
        globalAuthType: '',    // Global AuthenticationTypeID
        usingBranding : false,     // used for specific elements to determine if branding should be applied
        onGround : true,
        CreateAccountSettings: {   // Account Creation settings
            "settingsModel" :"",
            "accountGroups" :"",
            "usernameParam" :"",
            "inviteEmployeeId" :"",
            "guestLicenseAvailable": false,
            "employeeLicenseAvailable": false
        },
        loggedOutFingerprint: false,  // used for tracking if the user logged out or hit cancel with fingerprint authentication
        verifyFingerprintFromLogin : false,   // keeps track if user is coming from Login page after logout or cancellation
        enableFingerprint : false,  //global accessor to tell if the global setting 'Enable Fingerprint' is turned on
        inactivatedFavorites: [],   // holds recently inactivated favorite IDs for notification on My Quick Picks page
        preventUnloadLogout: false, //prevents the unload event from logging the user out when they navigate away from MyQC (added for FreedomPay)
        AccountSettings: {
            "spendingProfile":"",
            "accountGroup": "",
            "accountType": "",
            "accountStatus": false,
            "employeeLowBalanceThreshold":"",
            "needsSave": false
        },
        personModel: null, // holds information related to a person account
        accountModel: null, // holds information for the currently logged in account (or selected person account)
        Aliases: {
            accountAlias: "Account",
            personAccountAlias: "Person Account"
        },
        showTour: true, // determines whether the tour will show on the about page
        appConfig: "myqc",
        preventStandardStartup: false,
        ComboInProgress: [],
        CurrentCombo: {
            id: "",
            name: "",
            paDiscountId: "",
            comboDetails: [],
            comboDetailKeypadIds: [],
            nextDetailIndex: 0,
            editingFromCart: false, //this switches to true if a combo is clicked to be edited from the cart (used to differentiate new combos from existing ones)
            swapIndex: null,
            updatedComboTransLineId: null,
            editObj: null
        },
        defaultBrightness: null, //holds the default brightness to reset to after leaving the QR code view
        network: {
            online: false, // whether the app is communicating locally or to the server
            onlineStatus: false // whether the internet is available, controlled by JS events for online and offline
        },
        healthyIndicator: {  // tokenized filenames for healthy indicator icons
            wellness: '',
            wellnessLabel: 'Healthy',
            vegetarian: '',
            vegetarianLabel: 'Vegetarian',
            vegan: '',
            veganLabel: 'Vegan',
            glutenFree: '',
            glutenFreeLabel: 'Gluten Free'
        },
        nutrition: {
            nutritionCategory1: null,
            nutritionCategory2: null,
            nutritionCategory3: null,
            nutritionCategory4: null,
            nutritionCategory5: null
        },
        help: {
            allowHelpfulHints: false,
            loginFAQSetID: null
        },
        qcBaseURL: '',
        currency: {
            showCurrency: true,
            currencyBeforePrice: true,
            decimalInPrice: true,
            currencyType: "$"
        },
        keypadPositionID: 1,
        locationFilterSettings:{},
        primaryOrgLevels:[],
        requests: []
    }

    ;(function (qcssapp, $) {
    'use strict';

    jQuery.support.cors = true;

    //******* BROWSER SUPPORT *********/

    // Detects whether Internet Explorer is being used
    qcssapp.Functions.internetExplorer = function() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE");
        return (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./));
    };

    // If on IE, tell the user the browser is not supported
    if (qcssapp.Functions.internetExplorer()) {
        var message = "Due to security concerns, Internet Explorer is no longer supported by My Quickcharge.";
        message += " Please click the link below to open the application in Microsoft Edge.";
        $.alert({
            title: "",
            content: message,
            backgroundDismissAnimation: "",
            buttons: {
                open: {
                    text: "Open in Edge",
                    action: function() {
                        window.location = "microsoft-edge:" + window.location;
                        return false;
                    }
                }
            },
            onOpenBefore: function() {
                $(".jconfirm .container").css("max-width", "600px");
                $(".jconfirm-bg").css({"background-color": "#eeeeee", "opacity": 1});
            }
        });
        // Prevent the page from continuing to load
        throw new Error("Browser not supported");
    }

    //******* CALLS *********/

    //generic function to make calls to the api at a given endpoint
    qcssapp.Functions.callAPI = function(endPoint, verb, dataParameters, successParameters, successFn, errorFn, completeFn, errorParameters, time, preventShowMask, preventHideMask, fullScreenMask, completeParameters, preventTimoutPopup, transparency) {
        if (qcssapp.DebugMode) {
            console.log("Call to callAPI - " + "verb: " + verb + " - endPoint: " + endPoint );

            if ( dataParameters ) {
                console.log("dataParameters: ");
                console.log(dataParameters);
            }
        }

        if ( !preventShowMask ) {
            qcssapp.Functions.showMask(fullScreenMask, false, false, transparency);
        }

        if (endPoint.length) {
            //default verb to get
            if ( !verb ) {
                verb = 'GET';
            }

            if ( !time ) {
                time = 20000;
            }

            var requestId = qcssapp.Functions.createUniqueRequestId();

            qcssapp.requests.push(requestId);

            $.ajax({
                url: endPoint,
                type: verb,
                contentType: 'application/json',
                dataType:'json',
                timeout: time,
                data: dataParameters,
                requestId: requestId,
                headers: { 'X-MMHayes-RequestId' : requestId },
                success: function (data) {
                    if (qcssapp.DebugMode) {
                        console.log("Success in callAPI for endpoint: " + endPoint);
                        console.log(data);
                    }

                    // don't run success function should the request be untracked
                    if(!qcssapp.Functions.checkActiveRequests(this)) {
                        successFn = '';
                    }

                    //if no results or status is error or details has error in it or there is an unset id, display error
                    if ( data && ((data.hasOwnProperty('status') && data.status == "error") || (data.hasOwnProperty('details') && data.details.toLowerCase().indexOf("error") !== -1) || (data.hasOwnProperty('id') && !qcssapp.Functions.isNumeric(data.id))) ) {
                        qcssapp.Functions.throwFatalError();
                    } else {
                        if(qcssapp.offlineView != null && !qcssapp.offlineView.hidden) {
                            qcssapp.offlineView.fadeOut();
                        }

                        if ( successFn ) {
                            successFn(data, successParameters);
                        }
                    }

                    return data;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (qcssapp.DebugMode) {
                        console.log("Error occurred in callAPI for endpoint: " + endPoint);
                        console.log(jqXHR); //jQuery XML HTTP Request object - contains lots of good information
                        console.log(textStatus); //Possible values (besides null) are "timeout", "error", "abort", and "parsererror"
                        console.log(errorThrown); //text of HTTP status such as "Not Found" or "Internal Server Error"
                    }

                    //TODO: extract this to a common function
                    if ( navigator && navigator.splashscreen ) {
                        navigator.splashscreen.hide();
                    }

                    // on error, just remove the requestId to prevent the success function from running should the response be returned later
                    // allow the error functions to run still
                    qcssapp.Functions.checkActiveRequests(this);

                    var errorResponseJSON = "";
                    var mmhErrorCode = "";
                    var mmhErrorDetails = "";
                    var httpStatus = "";
                    var errorMsg = "<br /><br /> Click the button below to log out now.";

                    if (jqXHR) {
                        if (isValidJSON(jqXHR.responseText)) {
                            errorResponseJSON = JSON.parse(jqXHR.responseText);
                        }
                        if (jqXHR.status){
                            httpStatus = jqXHR.status;
                        }
                    } else {
                        qcssapp.Functions.throwFatalError('','login');
                        return false;
                    }

                    //if the errorResponseJSON is valid then we can handle MMHErrors
                    if (errorResponseJSON && errorResponseJSON.errorCode && errorResponseJSON.details) {

                        //set errorCode and details to local vars
                        mmhErrorCode = errorResponseJSON.errorCode;
                        mmhErrorDetails = errorResponseJSON.details;

                        //check the httpStatus in the header compared to in the JSON - let the JSON win
                        if (errorResponseJSON.httpStatus != httpStatus) {
                            console.log("WARNING: HTTP status does not match HTTP Status in errorResponseJSON!");
                            httpStatus = errorResponseJSON.httpStatus;
                        }

                        //Handle specific mmhErrorCodes here - probably not all error codes should throwFatalErrors -jrmitaly 6/29/2016

                        //session expiration error - only execute if server version is 1.2 or higher
                        if ( mmhErrorCode == "E2001" ) {
                            qcssapp.Functions.handleSessionExpiration(mmhErrorDetails+errorMsg);
                            return;
                        }

                        //funding error - issue creating adding a new credit card in Account Funding
                        if(mmhErrorCode == "E7700") {
                            var fundingErrorMsg = mmhErrorDetails.split('-')[1];
                            if(fundingErrorMsg == " deactivated") {  //if the attempts are maxed out for manual funding
                                $('#accountFunding_paymentMethod_saved-method').html('');
                                $('#accountFunding').find('.template-gen').remove();
                                new qcssapp.Views.AccountFundingView();
                                qcssapp.Router.navigate('account-funding', {trigger:true}); //navigate back to account funding
                            } else {  //if attempts are still available, display error message
                                qcssapp.Functions.displayError(fundingErrorMsg, 'Warning');
                                if(errorParameters != undefined && errorFn && typeof errorFn == 'function') {  //if creating a payment method
                                    errorFn(errorParameters); //reload iframe
                                }
                            }
                            return;
                        }else if(mmhErrorCode == "E7001") {
                            if(errorParameters != undefined && errorFn && typeof errorFn == 'function') {
                                errorFn(errorParameters); //reload iframe
                            }
                            qcssapp.Functions.displayPopup("Sorry, you've attempted to spend an amount that exceeds your spending limit within this store.", 'Submission over spending limit' , 'Okay');
                            return;
                        } else if (mmhErrorCode == "E7701") {
                            var fundingCardMsg = mmhErrorDetails.split('-')[1];
                            qcssapp.Functions.displayError(fundingCardMsg, 'Warning');
                            if(errorParameters != undefined && errorFn && typeof errorFn == 'function') {  //if creating a payment method
                                errorFn(errorParameters); //reload iframe
                            }
                            return;
                        } else if (mmhErrorCode == "E7302") { //too many free product rewards
                            if(errorParameters != undefined && errorFn && typeof errorFn == 'function') {  //if creating a payment method
                                errorFn(errorParameters, mmhErrorCode);
                            }
                            return;
                        }  else if (mmhErrorCode == "E7303") { //invalid reward submitted
                            if(errorParameters != undefined && errorFn && typeof errorFn == 'function') {  //if creating a payment method
                                errorFn(errorParameters, mmhErrorCode, mmhErrorDetails);
                            }
                            return;
                        } else if (mmhErrorCode == "E7800") { //purchase restriction exception
                            if(errorParameters != undefined && errorFn && typeof errorFn == 'function') {  //product in order is ineligible for purchase
                                errorFn(errorParameters, mmhErrorCode, mmhErrorDetails);
                            }
                            return;
                        } else if (mmhErrorCode == "E0000" && httpStatus == "405") { //purchase restriction exception
                            mmhErrorDetails = "E0000 - Method is Not Allowed. Status Code 405. Please login again.";
                            qcssapp.Functions.throwFatalError(mmhErrorDetails+errorMsg, 'login');
                            return false;
                        } else if (mmhErrorCode == "E5000" && errorParameters != undefined && errorParameters.callErrorFunction) { //missing data exception
                            if (errorFn && typeof errorFn == "function") {
                                errorFn(errorParameters, mmhErrorCode, mmhErrorDetails);
                            }
                            return;
                        } else {
                            //for unhandled mmhErrorCodes - return details prepended to default errorMsg
                            qcssapp.Functions.throwFatalError(mmhErrorDetails+errorMsg, 'error');
                            return false;
                        }

                    } else if (httpStatus) { //if the data is not valid JSON, then we should check the HTTP Status code
                        if (httpStatus == "403") {
                            mmhErrorDetails = "E2000 - Invalid Authorization. Please login again.";
                            qcssapp.Functions.throwFatalError(mmhErrorDetails+errorMsg, 'login');
                            return false;
                        } if (httpStatus == "500") {
                            mmhErrorDetails = "E0000 - An error occurred. Please check logs and try again.";
                            qcssapp.Functions.throwFatalError(mmhErrorDetails+errorMsg, 'error');
                            return false;
                        }
                    }

                    var showOfflineModal = qcssapp.Functions.canShowOfflineModal();

                    if ( textStatus == "timeout" ) {
                        //calling to cloud settings or checking a code or cloud login, just error function
                        if ( this.url.indexOf('www.mmhcloud.com/') != -1 || preventTimoutPopup ) {
                            if ( errorFn && typeof errorFn == 'function' ) {
                                errorFn(errorParameters);
                            }
                        } else if (showOfflineModal && (navigator.connection && navigator.connection.type && navigator.connection.type === Connection.NONE) ) {
                            qcssapp.Functions.loadNetworkOfflineModal(0);

                        } else {
                            qcssapp.Functions.handleNetworkError('timeout');
                        }
                    } else {
                        var networkState = "";
                        if ( !(errorParameters && errorParameters.hasOwnProperty('preventOfflineError') && errorParameters.preventOfflineError == true) ) {
                            //TODO: on mobile check
                            if( typeof window.cordova !== "undefined" && !showOfflineModal ) {
                                qcssapp.Functions.handleNetworkError('offline');
                            } else if ( typeof window.cordova == "undefined" ) {
                                networkState = navigator.onLine;
                                if ( !networkState && !showOfflineModal ) {
                                    qcssapp.Functions.handleNetworkError('offline');
                                }
                            }
                        }

                        if ( errorFn && typeof errorFn == 'function' ) {
                            errorFn(errorParameters);
                        } else if (!showOfflineModal) {
                            qcssapp.Functions.displayError('An error occurred, please check your internet connection and try again later');
                        }

                        if (showOfflineModal && (navigator.connection && navigator.connection.type && navigator.connection.type === Connection.NONE) ) {
                            qcssapp.Functions.loadNetworkOfflineModal(0);

                        // If online on the phone but can't find resource, show server error
                        } else if ( !(navigator.connection && navigator.connection.type && navigator.connection.type === Connection.NONE) && httpStatus == '404') {
                            qcssapp.Functions.handleNetworkError('timeout');
                        }
                    }

                    //always hide mask on error
                    qcssapp.Functions.hideMask();

                    return false;
                },
                complete: function (data) {
                    if ( completeFn ) {
                        completeFn(data, completeParameters);
                    }

                    if ( !preventHideMask ) {
                        qcssapp.Functions.hideMask();
                    }
                }
            });
        } else {
            if ( qcssapp.DebugMode ) {
                console.log("Bad endpoint on callAPI");
            }

            return false;
        }
    };

    //******* PAGE/VIEW RELATED FUNCTIONS *******/

    //remove all template generated content from the active panel
    qcssapp.Functions.deleteGeneratedContent = function() {
        //reset event handlers for infinite scroll
        $('#show-purchases').off();
        $('#show-deductions').off();
        $('#show-funding').off();
        $('#show-balances').off();
        $('#rewards-programs-list').off();
        $('#accountSettings').off();

        //remove generated content
        var $panels = $('.panel').not('#main');
        $panels.find('.template-gen').remove();
        $panels.find('.template-gen-container').children().not('.persist').remove();

        $('#globalBalanceContainer').show();
    };

    //main function for transitioning from page to page, takes element ID as in "#main" as target attribute
    qcssapp.Functions.pageTransition = function(target, forward, slideUp) {
        var $toPanel = $(target);

        var $toView = $toPanel.closest('.view');
        var $activePanels = $('.panel.active').not($toPanel);
        var $activeViews = $('.view.active').not($toView);
        var $activeSlidePanels = $('.panel.active-slide').not($toPanel);
        var $activeSlideViews = $('.view.active-slide').not($toView);

        if(slideUp) {
            if ( !$toView.hasClass('active-slide') ) {
                $toView.addClass('active-slide');
            }

            $toPanel.addClass('active-slide');
        } else {
            if ( !$toView.hasClass('active') ) {
                $toView.addClass('active');
            }

            $toPanel.addClass('active');

            $activePanels.removeClass('active');
            $activeViews.removeClass('active');
            $activeSlidePanels.removeClass('active-slide');
            $activeSlideViews.removeClass('active-slide');
        }
    };

    //standard route function to transition between pages
    qcssapp.Functions.executeStandardRoute = function(panel, view, slideLeft, slideUp) {
        //this prevents navigating before the app is loaded, as when someone loads the page with a route in the URL
        if ( qcssapp.onPhoneApp == null ) {
            qcssapp.Router.navigate('loading');
            return;
        }

        qcssapp.Functions.resetErrorMsgs();

        //set page titles
        var $panel = $(panel);

        $('.scroll-indicator').hide();
        if ( qcssapp.$activePanel ) {
            if ( qcssapp.$activePanel.find('.scrollElement').length ) {
                qcssapp.$activePanel.find('.scrollElement').off('scroll');
            } else {
                qcssapp.$activePanel.off('scroll');
            }
        }
        qcssapp.$activePanel = $panel;
        setTimeout(function() {
            qcssapp.Functions.setupScrollIndicator($panel.find('.scrollElement').length ? $panel.find('.scrollElement') : qcssapp.$activePanel);
        }, 500);

        if ( typeof $panel.attr('data-title') !== typeof undefined ) {
            $panel.parent().siblings('header').find('h1').text($panel.attr('data-title'));
        }

        qcssapp.Functions.pageTransition(panel, slideLeft != 'slide-left', slideUp);
    };

    //determines whether the scroll indicator should be showing for the given panel
    qcssapp.Functions.checkScrollIndicator = function($panel) {
        var windowPaneHeight = window.innerHeight - 60;
        var panelContentHeight = 0;
        var $scroll = $('.scroll-indicator');

        //won't show if there are no children - will be no height
        if ( !$panel || !$panel.children() || $panel.children().length == 0 ) {
            $scroll.hide();
            return;
        }

        //sum the heights of all direct children of the panel
        var totalDirectChildHeight = 0;
        var totalGrandChildHeight = 0;
        $.each($panel.children(), function() {
            var $child = $(this);
            var childHeight = $child.outerHeight(true);

            if ( ($child.css('display') != "block" && $child.css('display') != "flex") || childHeight == 0 ) {
                return true;
            }

            if ( $child.hasClass('scrollElement') ) {
                $.each($child.children(), function() {
                    var $grandchild = $(this);
                    var grandchildHeight = $grandchild.outerHeight(true);

                    if ( ($grandchild.css('display') != "block" && $grandchild.css('display') != "flex") || grandchildHeight == 0 ) {
                        return true;
                    }

                    totalGrandChildHeight = totalGrandChildHeight + grandchildHeight;
                });
            } else {
                totalDirectChildHeight = totalDirectChildHeight + childHeight;
            }
        });

        panelContentHeight = totalDirectChildHeight + totalGrandChildHeight;

        //if the page is not bigger than the window, just hide the scroll indicator
        if ( panelContentHeight < windowPaneHeight ) {
            $scroll.hide();
            return;
        }

        //check the amount scrolled already
        var $scrollEl = $panel;
        if ( $panel.find('.scrollElement').length ) {
            $scrollEl = $panel.find('.scrollElement');
        }
        var scrollOffsetTop = $scrollEl.children(':visible').first().offset().top;
        var scrolledHeight = scrollOffsetTop > 0 ? scrollOffsetTop : Math.abs(scrollOffsetTop) + 60;

        //if the user hasn't scrolled or if they are not yet near the bottom of the page, show the scroll indicator
        var panelContentHeightAdjuster = $panel.selector == "#product-detail-page .scrollElement" ? $('.product-detail_button-container').height() : -10; // on product detail pages, adjust the panel height needed to hide the scroll indicator
        if ( (scrolledHeight < 85 && panelContentHeight > windowPaneHeight + 100) || ( totalGrandChildHeight ? totalDirectChildHeight : 0 ) + scrolledHeight + windowPaneHeight < panelContentHeight + panelContentHeightAdjuster ) {
            $scroll.show();

            if($panel.attr('id') == 'show-order') {
                $scroll.hide();
            }

        } else {
            $scroll.hide();
        }
    };

    //called from views to set up the event for scrolling on the element that scrolls (panel, list, etc)
    qcssapp.Functions.setupScrollIndicator = function($scrollEl) {
        var $panel = $scrollEl.hasClass('panel') ? $scrollEl : ($scrollEl.closest('.panel') && !$scrollEl.closest('.panel').length) ? $scrollEl : $scrollEl.closest('.panel');

        qcssapp.Functions.checkScrollIndicator($panel);

        $scrollEl.on('scroll', function() {
            _.debounce(qcssapp.Functions.checkScrollIndicator($panel));
        });
    };

    //function that checks if the user is on the iPhone X or above
    qcssapp.Functions.onIphoneX = function() {
        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

        var aspect = window.screen.width / window.screen.height;

        if(window.orientation == 90) {
            aspect = window.screen.height / window.screen.width;
        }

        return iOS && aspect.toFixed(3) == "0.462";
    };

    //function that updates the styles for mobile devices and iPhone X
    qcssapp.Functions.adjustToScreenSize = function() {
        var $body = $('body');

        if (qcssapp.onPhoneApp) {
            $body.addClass('mobile');
            if (qcssapp.Functions.onIphoneX()) {
                $body.addClass('iphoneX');
            } else {
                $body.removeClass('iphoneX');
            }
        } else {
            $body.removeClass('mobile');
            $body.removeClass('iphoneX');
        }
    };


    qcssapp.Functions.showLocationSelector = function(successFn) {
        // load the org levels
        if (qcssapp.DebugMode) {
            console.log("Getting org levels...");
        }

        qcssapp.Functions.getStoreLocations(successFn);

    };


    //******* MASK *********/

    //shows the blocking mask with given message
    qcssapp.Functions.showMask = function(toggle, transparent, preventHideMaskOnTimeout, transparency) {
        if ( typeof toggle == 'undefined' ) {
            toggle = false;
        }

        var $blockMask = $('.block-mask');
        $blockMask.removeClass('transparent');
        $blockMask.css('opacity', '');

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE");

        // If on IE, have to remove and re-add loader to initiate the :before and :after animation
        if((msie > 0  || navigator.userAgent.match(/Trident.*rv\:11\./)) && $blockMask.css('display') == 'none') {
            $blockMask.find('#preloader').children().remove();
            $blockMask.find('#preloader').append('<div id="loader"></div>');
        }

        $blockMask.toggleClass('full-screen', toggle).show();
        if(transparent) {
            $blockMask.addClass('transparent').show();
        }
        if(transparency) {
            $blockMask.removeClass('transparent');
            $blockMask.css('opacity', transparency);
            $blockMask.show();
        }

        clearTimeout(qcssapp.maskTimeout);

        if(!preventHideMaskOnTimeout){
            // If mask is showing for more than 20 seconds close it
            qcssapp.maskTimeout = setTimeout(function(){
                if($blockMask.css('display') == 'block') {
                    qcssapp.Functions.hideMask();
                }
            },20000);

        }
    };

    //hides the blocking mask
    qcssapp.Functions.hideMask = function() {
        var $mask = $('.block-mask');
        setTimeout(function() {
            $mask.fadeOut(400);
        }, 250);
    };

    //shows the blocking mask in modal
    qcssapp.Functions.showModalMask = function() {
        $('.modal-block-mask').show();
    };

    //hides the blocking mask in modal
    qcssapp.Functions.hideModalMask = function() {
        var $mask = $('.modal-block-mask');
        setTimeout(function() {
            $mask.fadeOut(400);
        }, 250);
    };

    //********* HTML ENCODING / DECODING FUNCTIONS ***************//

    // encodes html
    qcssapp.Functions.htmlEncode = function(value) {
        return $('<div/>').text(value).html();
    };

    // decodes html
    qcssapp.Functions.htmlDecode = function(value) {
        if (value) {
            if (typeof value !== "undefined") {
                if(typeof value === "boolean"){return value;}
                value = value.toString();
                if (typeof value === "string"){
                    value = value.replace(/_lt_/g, "<");
                    value = value.replace(/_gt_/g, ">");
                }
            }
        }

        //see stack article: https://stackoverflow.com/questions/1147359/how-to-decode-html-entities-using-jquery/23596964#23596964
        var textArea = document.createElement('textarea');
        textArea.innerHTML = value;
        return textArea.value;
    };

    // cleans up text input
    qcssapp.Functions.cleanInput = function(value) {
        if ( value == null || value.length == 0 ) {
            return '';
        }

        if ( value.indexOf('\u2019') != -1 ) {
            value = value.split('\u2019').join('\'');
        }

        return value.trim();
    };


    //******* ONLINE / OFFLINE FUNCTIONALITY  *********/

    qcssapp.Functions.handleNetworkOnline = function() {
        qcssapp.network.onlineStatus = true;
    };

    qcssapp.Functions.handleNetworkOffline = function() {
        qcssapp.network.online = false;
        qcssapp.network.onlineStatus = false;

        qcssapp.Functions.loadNetworkOfflineModal(1);
    };

    //check if the offline modal should be shown if callAPI fails due to network connection
    qcssapp.Functions.canShowOfflineModal = function() {
        if( !qcssapp.onPhoneApp ) {
            return false;
        }

        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        var userSession = mdl.get('sessionID');
        var badge = mdl.get('badge');

        //if session and badge are saved in localStorage
        return userSession != '' && badge != '';
    };

    qcssapp.Functions.loadNetworkOfflineModal = function(offlineEvent) {
        if( !qcssapp.onPhoneApp ) {
            return true;
        }

        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        var userSession = mdl.get('sessionID');
        var badge = mdl.get('badge');

        //if session and badge are saved in localStorage
        if ( userSession != '' && badge != '' ) {
            var params = {
                badge: badge,
                fromLogin: false
            };

            if(typeof qcssapp.offlineView == 'undefined') {
                qcssapp.offlineView = new qcssapp.Views.NetworkOfflineView({hidden: true});
            }

            qcssapp.offlineView.offlineEvent = offlineEvent;

            //if not an offline event (couldn't connect to server) and the view is already showing, show can't connect error
            if(!offlineEvent && !qcssapp.offlineView.hidden) {
                qcssapp.offlineView.displayError('Connection failed');
            } else {
                if (!offlineEvent && !$('.view.active').length) {
                    params.fromLogin = true;
                }

                qcssapp.offlineView.loadView(params);
            }

            return false;
        }
    };

    //******* EVENT HANDLERS *********/

    $(window).on('offline', function() {
        console.log('offline event');
        qcssapp.Functions.handleNetworkOffline();
    });

    $(window).on('online', function() {
        console.log('online event');
        qcssapp.Functions.handleNetworkOnline();
    });

    //log user out if they close the browser and don't have keepMeLoggedIn checked
    $(window).on('beforeunload', function(e) {
        //get out session model from localStorage
        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();

        //if session is saved in localStorage - user is logged in, may need to log them out
        if ( mdl.attributes && mdl.attributes.hasOwnProperty('sessionID') && mdl.get('sessionID') != '' ) {
            var userKeepLogged = mdl.get('keepLogged');

            //only log user out if keepLogged is not flagged
            if ( userKeepLogged !== true && !qcssapp.preventUnloadLogout) {
                qcssapp.Functions.logOut();
            }
        }
    });

    //global error event handler - added by jrmitaly on 6/29/2016
    window.onerror = function(msg, url, line, col, error) {

        //maybe one day we could display these errors or send a request to the server to log the error? -jrmitaly - 6/29/2016
        console.log("FATAL ERROR OCCURRED!");

        if (msg && url && line) { //should exist in all supported browsers
            console.log("Message: "+msg);
            console.log("URL: "+ url);
            console.log("Line: "+line);
        } else {
            console.log("Could not find basic error information!");
        }

        if (col) { //HTML 5 - might not exist in all browsers
            console.log("Column: "+col);
        }

        if (error) {
            console.log("Error: "+error);
        }


        var endPoint = qcssapp.Location + "/api/myqc/error/log";

        var errorObj = {
            msg: msg,
            url: url,
            line: line,
            col: col,
            error: error
        }

        qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(errorObj),'',
            function(data) {
                console.log('Successfully logged error on back end.');
            },
            function(errorParameters) {
                displayFrontEndError(errorParameters);
            },
            '',
            errorObj,
            '',
            true,
            true
        );

        //throw generic fatal error
        qcssapp.Functions.throwFatalError();

        // returning true supresses "pop up" errors (like in OLD IE)
        return true;
    };

    qcssapp.Functions.logError = function(msg, url, line, col, error) {
        //maybe one day we could display these errors or send a request to the server to log the error? -jrmitaly - 6/29/2016
        console.log("FATAL ERROR OCCURRED!");

        if (msg && url && line) { //should exist in all supported browsers
            console.log("Message: "+msg);
            console.log("URL: "+ url);
            console.log("Line: "+line);
        } else {
            console.log("Could not find basic error information!");
        }

        if (col) { //HTML 5 - might not exist in all browsers
            console.log("Column: "+col);
        }

        if (error) {
            console.log("Error: "+error);
        }

        var endPoint = qcssapp.Location + "/api/myqc/error/log";

        var errorObj = {
            msg: msg,
            url: url,
            line: line,
            col: col,
            error: error
        };

        qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(errorObj),'',
            function(data) {
                console.log('Successfully logged error on back end.');
            },
            '', '', errorObj, '', true, true
        );
    };

    // Create a unique request Id for front end logging on kiosk
    qcssapp.Functions.createUniqueRequestId = function() {
        return Math.floor(100000 + Math.random() * 900000) + Date.now().toString() + Math.random().toString(30).substring(8,15).toUpperCase();
    };

    qcssapp.Functions.checkActiveRequests = function(that) {
        // ensure no failed, timed-out, etc. responses are allowed to continue with their callbacks
        var requestId = that.headers['X-MMHayes-RequestId'];
        if(requestId && qcssapp.requests.indexOf(requestId) !== -1) {
            if(qcssapp.DebugMode) {
                console.log('Found matching requestId for response. RequestId "' + requestId + '" will now be removed from requests array');
            }
            var index = qcssapp.requests.indexOf(requestId);
            qcssapp.requests.splice(index, 1);
            return true;
        } else {
            if(qcssapp.DebugMode) {
                console.log('Could not find match for "' + requestId + '" in requests array.');
            }
            return false;
        }
    };

    //********* EVENT HANDLER FUNCTIONS ***************//

    //for when front end errors cannot be logged on the back end due to connectivity or bad url
    function displayFrontEndError(errorObj) {
        $.alert('Cannot relay error to server for logging. Error details: <br/>' +
            '<ul>' +
            '<li>Message: ' + qcssapp.Functions.htmlEncode(errorObj.msg) + '</li>' +
            '<li>URL: ' + qcssapp.Functions.htmlEncode(errorObj.url) + '</li>' +
            '<li>Line: ' + qcssapp.Functions.htmlEncode(errorObj.line) + '</li>' +
            '<li>Col: ' + qcssapp.Functions.htmlEncode(errorObj.col) + '</li>' +
            '<li>Error: ' + qcssapp.Functions.htmlEncode(errorObj.error) + '</li>' +
            '</ul>');
    }

    //check whether the string passed is valid JSON or not - added by jrmitaly 6/29/2016
    function isValidJSON(jsonStr) {
        try {
            JSON.parse(jsonStr);
        } catch (ex) {
            return false;
        }
        return true;
    }

    // Compares two models to see if they're the same
    Backbone.Model.prototype.equalTo = function(other) {
        return _.isEqual(this.attributes, other.attributes);
    };

    //********* CONTROLLER RELATED FUNCTIONS ***************//

    //Controller Model - used to keep track of view properties in the Controller
    qcssapp.Models.Controller = Backbone.Model.extend({
        defaults: {
            id: '',

            //which route should be called when a view with this model is activated - not required for views that will not be activated
            route: null,

            options: {},
            constructView: null,

            destroys: '',
            destructible: false,
            destroyOn: []
        }
        //TODO: add in a validate method that checks qcssapp.Views[id] exists
        //TODO: add in a validate method that checks if route set then route in router.routes exists
    });

    qcssapp.Collections.Controller = Backbone.Collection.extend({
        model: qcssapp.Models.Controller
    });

    qcssapp.Controller = {
        history: [],
        activeView: [],
        collection: null,
        constructedViews: [],

        initialize: function() {
            this.collection = new qcssapp.Collections.Controller();
        },

        constructView: function(controllerModel) {
            var modelID = controllerModel.get('id');
            var modelExists = this.collection.findWhere({id: modelID});
            !modelExists ? this.collection.add(controllerModel) : this.collection.set(controllerModel, {remove:false});

            var newView = new qcssapp.Views[controllerModel.get('id')](controllerModel.get('options'));

            var controllerViewOptions = {
                controllerID:modelID,
                destructible:controllerModel.get('destructible'),
                destroyOn:controllerModel.get('destroyOn')
            };
            _(newView).extend(controllerViewOptions);

            this.constructedViews.push(newView);
            return newView;
        },

        activateView: function(controllerModel, back) {
            var modelID = controllerModel.get('id');

            !back ? this.history.push(modelID) : null;

            var viewIsConstructed = _(_(this.constructedViews).pluck('controllerID')).contains(modelID);
            if (!viewIsConstructed) {
                this.constructView(controllerModel);
            }

            this.activeView = modelID;

            var modelRoute = controllerModel.get('route');
            qcssapp.Router.navigate(modelRoute);
            qcssapp.Functions.executeStandardRoute('#'+modelRoute, '', back ? 'slide-left' : 'slide');

            this.destroyViews(modelID);
        },

        destroyViews: function(modelID) {
            var viewsToDestroy = _(this.constructedViews).filter(function(view) {
                return view.destructible && ( _(view.destroyOn).contains(modelID) || (_(view.destroyOn).contains('any') && view.controllerID != modelID));
            });
            var modelsToRemove = [];
            _(viewsToDestroy).each(function(view) { view.remove(); modelsToRemove.push(view.controllerID); });
            this.constructedViews = _(this.constructedViews).difference(viewsToDestroy);
            this.collection.remove(modelsToRemove);
            return viewsToDestroy.length;
        },

        back: function() {
            if ( this.history.length == 0 ) {
                return;
            }

            this.history.pop();
            var prevID = _.last(this.history);
            var prevModel = this.collection.get(prevID);

            this.activateView(prevModel, true);
        },

        resetHistory: function() {
            this.history = [];
        }
    };

    qcssapp.Controller.initialize();

})(qcssapp, jQuery);