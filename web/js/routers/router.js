;(function () {
	'use strict';

	var QCRouter = Backbone.Router.extend({
		routes: {
			'main': 'mainRoute',
            'show-balances': 'balanceRoute',
            'show-purchases': 'purchaseRoute',
            'show-deductions': 'deductionRoute',
            'show-funding': 'fundingRoute',
            'show-order': 'orderRoute',
            'show-spendinglimits': 'spendinglimitsRoute',
            'show-accountSettings': 'accountSettingsRoute',
            'show-freeze': 'freezeRoute',
            'show-receive': 'receiveRoute',
            'show-rewards': 'rewardsProgramsRoute',
            'rewards': 'rewardsRoute',
            'reward-history': 'historyRoute',
            'forgot-password-page': 'forgotPWRoute',
            'tour-page-0': 'tourRoute',
            'privacy-policy': 'privacyPolicyRoute',
            'tos-review': 'tosReviewRoute',
            'login-page': 'loginRoute',
            'logout-page': 'logoutRoute',
            'add-application': 'addCodeRoute',
            'create-account-page':'createAccountRoute',
            'confirm': 'confirmRoute',
            'message': 'msgRoute',
            'error': 'errorRoute',
            'keypad/:id': 'keypadRoute',
            'product/:id': 'productRoute',
            'modifier/:id': 'modifierRoute',
            'cart': 'cartRoute',
            'cart-page': 'cartPageRoute',
            'review': 'reviewRoute',
            'enterCode': 'enterCodeRoute',
            'tos-page': 'tosRoute',
            'tos-success': 'tosSuccessRoute',
            'force': 'forceRoute',
            'account-funding': 'accountFundingRoute',
            'reauth': 'reAuthRoute',
            'receipt': 'receiptRoute',
            'payment': 'paymentRoute',
            'payment-agreement': 'paymentAgreementRoute',
            'redeem': 'redeemRoute',
            'favorites': 'favoritesRoute',
            'person-password': 'personPasswordRoute',
            'manage-account': 'manageAccountRoute',
            'add-account': 'addAccountRoute',
            'favorite-order': 'favoriteOrdersRoute',
            'show-date': 'futureDateRoute',
            'suggestive-selling': 'suggestiveSellingRoute',
            'combo': 'comboRoute',
            'faq': 'faqRoute',
            'order-placed': 'orderPlacedRoute',
            'product-detail/:id': 'productDetailRoute',
            'filtering-options': 'filteringOptionsRoute'
		},
        initialize: function() {
            this.$generalBackIcon = $('.general-back-icon');
            this.$homeLink = $('.home-link');
        },
        showHomeIcon: function() {
            this.$generalBackIcon.hide();
            this.$homeLink.show();
        },
        showBackIcon: function() {
            this.$homeLink.hide();
            this.$generalBackIcon.show();
        },
        mainRoute: function() {
            qcssapp.Functions.executeStandardRoute('#main', '#mainview', 'slide-left');
        },
        balanceRoute: function() {
            qcssapp.Functions.executeStandardRoute('#show-balances', '#pageview');
        },
        purchaseRoute: function() {
            qcssapp.Functions.executeStandardRoute('#show-purchases', '#purchasesview');
        },
        freezeRoute: function() {
            qcssapp.Functions.executeStandardRoute('#show-freeze', '#pageview');
        },
        deductionRoute: function() {
            qcssapp.Functions.executeStandardRoute('#show-deductions', '#pageview');
        },
        fundingRoute: function() {
            qcssapp.Functions.executeStandardRoute('#show-funding', '#accountfundingview');
        },
        orderRoute: function() {
            qcssapp.Functions.executeStandardRoute('#show-order', '#orderview');

            qcssapp.Functions.resetOrdering(false);

            $('.keypad-page').remove();
        },
        spendinglimitsRoute: function() {
            qcssapp.Functions.executeStandardRoute('#show-spendinglimits', '#pageview');
        },
        accountSettingsRoute: function() {
            qcssapp.Functions.executeStandardRoute('#accountSettings', '#pageview');
        },
        forgotPWRoute: function() {
            qcssapp.Functions.executeStandardRoute('#forgot-password-page', '#loginview');
        },
        forceRoute: function() {
            qcssapp.Functions.executeStandardRoute('#force-password-page', '#loginview');
        },
        personPasswordRoute: function() {
            qcssapp.Functions.executeStandardRoute('#person-password-page', '#loginview');
        },
        tourRoute: function() {
            qcssapp.Functions.executeStandardRoute('#tour-page-0', '#tourview');

            qcssapp.Functions.hideMask();
        },
        privacyPolicyRoute: function() {
            qcssapp.Functions.executeStandardRoute('#privacy-policy-page', '#privacypolicyview');
        },
        tosReviewRoute: function() {
            qcssapp.Functions.executeStandardRoute('#tos-review-page', '#tosreviewview');
        },
        loginRoute: function() {
            qcssapp.Functions.executeStandardRoute('#login-page', '#loginview', 'slide-left');
            qcssapp.Functions.checkLoginNameParameter();
        },
        logoutRoute: function() {
            qcssapp.Functions.executeStandardRoute('#logout-page', '#logoutview', 'slide-left');
        },
        enterCodeRoute: function() {
            qcssapp.Functions.executeStandardRoute('#enterCode-page', '#loginview', 'slide-left');
        },
        addCodeRoute: function() {
            $('#addCodeCurrentName').html($('#loginCodeSelect').find('option:selected').attr('data-alias'));
            qcssapp.Functions.executeStandardRoute('#add-application', '#loginview');
        },
        createAccountRoute: function() {
            qcssapp.Functions.executeStandardRoute('#create-account-page', '#loginview');
        },
        confirmRoute: function() {
            qcssapp.Functions.executeStandardRoute('#general-confirm-view .panel', '#general-confirm-view');
        },
        msgRoute: function() {
            qcssapp.Functions.executeStandardRoute('#general-msg-view .panel', '#general-msg-view');

            qcssapp.Functions.hideMask();

            $('#general-msg-view .panel').addClass('active');
        },
        keypadRoute: function(id) {
            var keypadPageID = '#keypad-'+id;
            qcssapp.Functions.executeStandardRoute(keypadPageID, '#keypadview');

            qcssapp.Functions.formatFullKeypadLines();

            $('.keypad-page').add('.modifier-page').not(keypadPageID).remove();
        },
        modifierRoute: function(id) {
            var modifierPageID = '#modifier-'+id;
            qcssapp.Functions.executeStandardRoute(modifierPageID, '#modifierview');

            $('.modifier-page').not(modifierPageID).add('.keypad-page').remove();
        },
        productRoute: function(id) {
            var productID = '#product-page';
            qcssapp.Functions.executeStandardRoute(productID, '#productview');

            $('.keypad-page').add('.modifier-page').remove();
        },
        cartRoute: function() {
            if ( !qcssapp.ServerVersion || qcssapp.Views.CartView == 'undefined') {
                return;
            }

            qcssapp.Functions.showMask();

            qcssapp.Functions.addHistoryRecord(0, 'cart');

            qcssapp.Functions.purgeModifierHistory();

            qcssapp.Functions.clearCurrentModifiers();

            qcssapp.Functions.resetCombos();

            qcssapp.Functions.resetDynamicViewEvents();

            new qcssapp.Views.CartView();

            qcssapp.Functions.executeStandardRoute('#show-cart', '#cartview');

            qcssapp.CurrentOrder.selectedRewards = [];
            qcssapp.CurrentOrder.automaticRewards = [];
            qcssapp.CurrentOrder.useMealPlanDiscount = true;
            qcssapp.CurrentOrder.expressReorder = false;

            $('.keypad-page').add('.modifier-page').remove();
        },
        cartPageRoute: function() {
            if ( !qcssapp.ServerVersion || qcssapp.Views.CartView == 'undefined') {
                return;
            }

            qcssapp.Functions.addHistoryRecord(0, 'cart');

            qcssapp.Functions.purgeModifierHistory();

            qcssapp.Functions.clearCurrentModifiers();

            qcssapp.Functions.resetCombos();

            qcssapp.Functions.resetDynamicViewEvents();

            new qcssapp.Views.CartView();

            qcssapp.Functions.executeStandardRoute('#show-cart', '#cartview');

            qcssapp.CurrentOrder.selectedRewards = [];
            qcssapp.CurrentOrder.automaticRewards = [];
            qcssapp.CurrentOrder.useMealPlanDiscount = true;
            qcssapp.CurrentOrder.expressReorder = false;

            $('.keypad-page').add('.modifier-page').remove();
        },
        receiveRoute: function() {
            qcssapp.CurrentOrder.expressReorder = false;
            qcssapp.Functions.executeStandardRoute('#show-receive', '#receiveview');

            if ( $('#show-receive').hasClass('showTimeDiffError') && qcssapp.futureOrderDate == "" ) {
                var label = "computer";

                //if on phone
                if( typeof window.cordova !== "undefined" ) {
                    label = "device";
                }

                var currentTime = moment(qcssapp.CurrentOrder.storeModel.attributes.currentStoreTime, 'HH:mm:ss.SSS').format('hh:mm A');

                qcssapp.Functions.displayError('Your '+label+' time is significantly varied from the store time! Store time is currently '+ currentTime,'Warning','#show-receive');
            }
        },
        reviewRoute: function() {
            qcssapp.Functions.executeStandardRoute('#review-page', '#reviewview');
        },
        errorRoute: function() {
            qcssapp.Functions.executeStandardRoute('#general-error-view .panel', '#general-error-view');
        },
        tosSuccessRoute: function() {
            qcssapp.Functions.executeStandardRoute('#tos-success', '#tosaccept-view');
        },
        tosRoute: function() {
            qcssapp.Functions.executeStandardRoute('#tos-page', '#tosview');

            qcssapp.Functions.hideMask();
        },
        accountFundingRoute: function() {
            qcssapp.Functions.executeStandardRoute('#accountFunding', '#pageview');
        },
        reAuthRoute: function() {
            qcssapp.Functions.executeStandardRoute('#reauth', '#reauthView');
            qcssapp.Functions.hideMask();
        },
        receiptRoute: function() {
            qcssapp.Functions.executeStandardRoute('#receipt', '#receipt-view');
        },
        rewardsProgramsRoute: function() {
            qcssapp.Functions.executeStandardRoute('#show-rewards', '#rewardsview');
        },
        rewardsRoute: function() {
            qcssapp.Functions.executeStandardRoute('#program-rewards', '#rewardsview');
            this.showBackIcon();
        },
        historyRoute: function() {
            qcssapp.Functions.executeStandardRoute('#program-rewards-history', '#rewardsview');
            this.showBackIcon();
        },
        paymentRoute: function() {
            qcssapp.Functions.executeStandardRoute('#payment', '#paymentview');
        },
        paymentAgreementRoute: function() {
            qcssapp.Functions.executeStandardRoute('#payment-agreement', '#paymentview');
        },
        redeemRoute: function() {
            qcssapp.Functions.executeStandardRoute('#redeem-rewards', '#redeemview');
        },
        favoritesRoute: function() {
            qcssapp.Functions.executeStandardRoute('#my-quick-picks-page', '#favoriteview');

            $('.keypad-page').add('.modifier-page').not('#my-quick-picks-page').remove();
        },
        favoriteOrdersRoute: function() {
            qcssapp.Functions.executeStandardRoute('#favorite-order', '#favoriteorderview');
        },
        manageAccountRoute: function() {
            qcssapp.Functions.executeStandardRoute('#manage-account', '#manageview');
        },
        addAccountRoute: function() {
            qcssapp.Functions.executeStandardRoute('#add-account', '#addaccountview');
        },
        futureDateRoute: function() {
            qcssapp.Functions.executeStandardRoute('#show-date', '#futuredateview');
        },
        suggestiveSellingRoute: function() {
            qcssapp.Functions.executeStandardRoute('#suggestive-selling-page', '#suggestiveview');
        },
        comboRoute: function() {
            qcssapp.Functions.executeStandardRoute('#combo-page', '#comboview');
        },
        faqRoute: function() {
            qcssapp.Functions.executeStandardRoute('#faq-page', '#faqview');
        },
        orderPlacedRoute: function() {
            qcssapp.Functions.executeStandardRoute('#order-placed-page', '#orderplacedview');
        },
        productDetailRoute: function() {
            qcssapp.Functions.executeStandardRoute('#product-detail-page', '#product-detail-view', '', true);
        },
        filteringOptionsRoute: function() {
		    qcssapp.Functions.executeStandardRoute('#filtering-options-page', '#filteringoptionsview', '', true);
        }
    });

    qcssapp.Router = new QCRouter();
	Backbone.history.start({ pushState: false });
})();
