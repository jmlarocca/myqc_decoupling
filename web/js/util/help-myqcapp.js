;(function (qcssapp, $) {
    'use strict';

    //******* HELP / HINTS AND TRICKS / FAQ FUNCTIONS *******/

    qcssapp.Functions.setHelpfulHintsAndFAQS = function(data) {
        qcssapp.help.allowHelpfulHints = false;
        var $helpIcon = $('.help-icon');
        $helpIcon.addClass('hidden');

        if(!qcssapp.Functions.checkServerVersion(4,0,41)) {
            return;
        }

        if ( data.hasOwnProperty('allowHelpfulHints') && data['allowHelpfulHints'] ) {
            qcssapp.help.allowHelpfulHints = true;
            $helpIcon.removeClass('hidden');
            qcssapp.Functions.setSVGColors();
        }

        var $faqElements = $('#faqLink, #loginSeparator');
        $faqElements.hide();

        if (data.hasOwnProperty('loginFAQSetID') && data['loginFAQSetID'] != null) {
            qcssapp.help.loginFAQSetID = data['loginFAQSetID'];
            $faqElements.show();
        }
    };

    qcssapp.Functions.helpViewActive = function() {
        return $('.help-view').length;
    };

    qcssapp.Functions.setHelpInfoTop = function($helpHand, $helpInfo, extraPadding) {
        if(!$helpInfo.length || !$helpHand.length) {
            return;
        }

        extraPadding = !extraPadding ? 0 : extraPadding;

        var handOffsetTop = $helpHand.offset().top;
        var helpHandHeight = $helpHand[0].clientHeight;

        $helpInfo.css('top', handOffsetTop + helpHandHeight + extraPadding);
    };

    qcssapp.Functions.setHelpInfoTopReverse = function($helpHand, $helpInfo, extraPadding) {
        if(!$helpInfo.length || !$helpHand.length) {
            return;
        }

        extraPadding = !extraPadding ? 0 : extraPadding;

        var handOffsetTop = $helpHand[0].offsetTop;
        var helpInfoHeight = $helpInfo[0].clientHeight;

        $helpInfo.css('top', handOffsetTop - helpInfoHeight - extraPadding);
    };

    qcssapp.Functions.setHandTop = function($element, $helpHand, extraPadding) {
        if(!$element.length || !$helpHand.length) {
            return;
        }

        var elementOffsetTop = $element.offset().top;
        var elementOffsetHeight = $element[0].clientHeight;

        $helpHand.css('top', elementOffsetTop + elementOffsetHeight - extraPadding);
    };

    qcssapp.Functions.setHandTopReverse = function($element, $helpHand, extraPadding) {
        if(!$element.length || !$helpHand.length) {
            return;
        }

        var elementOffsetTop = $element.offset().top;
        var handHeight = $helpHand[0].clientHeight;

        $helpHand.css('top', elementOffsetTop - handHeight + extraPadding);
    };


    qcssapp.Functions.setHandTopSimple = function($element, $helpHand, extraPadding) {
        if(!$element.length || !$helpHand.length) {
            return;
        }

        var elementOffsetTop = $element.offset().top;

        $helpHand.css('top', elementOffsetTop + extraPadding);
    };

    qcssapp.Functions.setHandLeft = function($element, $helpHand) {
        if(!$element.length || !$helpHand.length) {
            return;
        }

        var elementOffsetLeft = $element[0].offsetLeft;

        if(typeof elementOffsetLeft === 'undefined') {
            elementOffsetLeft = Number($element.css('left').replace('px', ''));
        }

        $helpHand.css('left', elementOffsetLeft);
    };

    //******* HELP ICON EVENT HANDLER *******/

    $(document).on('click', '.help-icon', function(e) {
        var $target = $(e.currentTarget);
        var view = $target.attr('data-view');
        var appendElement = typeof $target.attr('data-append') === 'undefined' ? view : $target.attr('data-append');

        var options = {
            appendElement: appendElement,
            view: view,
            hidden: true
        };

        new qcssapp.Views.HelpView(options);
    });


})(qcssapp, jQuery);