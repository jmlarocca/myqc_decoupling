;(function (qcssapp, $) {
    'use strict';

    //******* EXPRESS REORDER RELATED FUNCTIONS *******/

    qcssapp.Functions.expressReorder = function() {
        var endPoint = qcssapp.Location + '/api/ordering/express/reorder';

        var orderObject = qcssapp.Functions.buildExpressReorderObject();

        qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(orderObject), '',
            function(data) {
                if ( data.errorDetails && data.errorDetails.length > 0 && !data.advanceScreen) {
                    if(data.errorDetails === 'Missing or invalid Payment Method') {
                        // if the user no longer has a card on file, go to the cart and ask them to go add a card or use QC Balance
                        qcssapp.Functions.navigateToCart();
                        qcssapp.Functions.displayError('Please Add a Credit Card or use Quickcharge Balance on Checkout', 'Warning');
                        return;
                    }
                    qcssapp.Functions.displayError(data.errorDetails, 'Warning');
                    return;
                }

                var paymentMethod = 'Quickcharge';
                var showPaymentMethod = false;
                if (qcssapp.Functions.isCreditCardAllowed()) {
                    showPaymentMethod = true;

                    var paymentMethodCollection = qcssapp.Functions.getStorePaymentSetting("paymentMethodCollection", []);

                    if( data.useCreditCardAsTender && paymentMethodCollection.length > 0) {
                        var accountPaymentMethod = new qcssapp.Models.AccountPaymentMethod(paymentMethodCollection[0], {parse:true});
                        paymentMethod = accountPaymentMethod.get('paymentMethodType').toUpperCase() + ' XXXX ' + accountPaymentMethod.get('paymentMethodLastFour');
                    } else {
                        data.useCreditCardAsTender = false;
                    }
                }

                //get back final cart object w/ taxes, fees, discounts, etc. Send to final review page
                if ( data.products.length > 0 ) {
                    //send to final review page
                    data.phone = qcssapp.Functions.htmlDecode(data.phone);
                    data.time = qcssapp.Functions.htmlDecode(data.time);
                    data.readyTime = qcssapp.Functions.htmlDecode(data.readyTime);
                    data.submittedTime = qcssapp.Functions.htmlDecode(data.submittedTime);
                    data.comments = qcssapp.Functions.htmlDecode(data.comments);
                    data.diningOptionPAPluName = qcssapp.Functions.htmlDecode(data.diningOptionPAPluName);
                    data.paymentMethod = paymentMethod;
                    data.showPaymentMethod = showPaymentMethod;
                    data.futureOrderDate = qcssapp.futureOrderDate;

                    if(!qcssapp.Functions.returnToCartIfIsExpressOrderAndTerminalInfoChanged()) {
                        new qcssapp.Views.OrderReview(data);
                        qcssapp.Functions.addHistoryRecord(0, 'review');
                        qcssapp.Router.navigate('review', {trigger: true});

                        if ( data.errorDetails && data.errorDetails.length > 0 ) {
                            $('#submitOrder').attr('disabled','disabled').addClass('unavailable');
                            qcssapp.Functions.displayError(data.errorDetails, 'Warning');
                        } else if ( data.time == "ASAP" ) {
                            var readyTime = moment(data.readyTime, "hh:mm A");
                            if ( readyTime.diff(receiveView.currentStoreTime, 'minutes') > 120 ) {
                                qcssapp.Functions.displayError("This order will take longer than two hours to be ready. Please take note of the estimated time below", 'Warning');
                            }
                        }
                    }

                } else {
                    if ( data.errorDetails && data.errorDetails.length > 0 ) {
                        qcssapp.Functions.displayError(data.errorDetails);
                    } else {
                        qcssapp.Functions.displayError("An error occurred while processing your order.");
                    }
                }

            },
            function() {
                if (qcssapp.DebugMode) {
                    console.log("Error trying to express reorder.");
                }
            }, '', '', '', true, true
        );

    };

    qcssapp.Functions.buildExpressReorderObject = function() {
        if(qcssapp.CurrentOrder.orderType == "notChosen" || qcssapp.CurrentOrder.orderType == "") {
            qcssapp.CurrentOrder.orderType = "pickup";
        }

        var reorderTransactionID = "";
        var phone = "";
        var useCreditCardAsTender = false;
        var useDiningOptions = "";
        var type = qcssapp.CurrentOrder.orderType;
        var comments = qcssapp.Functions.htmlDecode(qcssapp.CurrentOrder.orderComments);
        var location = qcssapp.Functions.htmlDecode(qcssapp.CurrentOrder.orderLocation);

        if(qcssapp.CurrentOrder.expressReorderModel != "") {
            reorderTransactionID = qcssapp.CurrentOrder.expressReorderModel.id;
            phone = qcssapp.CurrentOrder.expressReorderModel.phone;
            useCreditCardAsTender =  qcssapp.CurrentOrder.expressReorderModel.usingCreditCardAsTender;
            useDiningOptions = qcssapp.CurrentOrder.expressReorderModel.usingDiningOptions;
            comments = qcssapp.Functions.htmlDecode(qcssapp.CurrentOrder.expressReorderModel.comments);
            location = qcssapp.Functions.htmlDecode(qcssapp.CurrentOrder.expressReorderModel.location);
            if (qcssapp.CurrentOrder.expressReorderModel.orderType != "") {
                type = qcssapp.CurrentOrder.expressReorderModel.orderType;
                qcssapp.CurrentOrder.orderType = type;
            }
        }

        if (qcssapp.Functions.isCreditCardAllowed() &&
            !qcssapp.Functions.getStorePaymentSetting("allowMyQCFunding", true) &&
            !qcssapp.Functions.getStorePaymentSetting("hasQuickChargeBalance", false))
        {
            useCreditCardAsTender = true;
        }

        qcssapp.CurrentOrder.orderTime = "";
        qcssapp.CurrentOrder.orderLocation = "";

        var orderObject = {
            type: type,
            location: location,
            time: qcssapp.CurrentOrder.orderTime,
            phone: phone,
            comments: comments,
            personName: qcssapp.accountModel.get('name'),
            submittedTime: moment().format("YYYY-MM-DD HH:mm:ss.SSS"),
            deliveryMinimum: qcssapp.CurrentOrder.storeModel.get('deliveryMinimum'),
            reAuthTypeID: qcssapp.CurrentOrder.storeModel.get('reAuthTypeID'),
            ignoreInventoryCounts: qcssapp.CurrentOrder.storeModel.get('ignoreInventoryCounts'),
            products: qcssapp.Functions.buildProductArray(),
            reorderTransactionID: reorderTransactionID,
            useCreditCardAsTender: useCreditCardAsTender,
            useDiningOptions: useDiningOptions
        };

        //for rewards - only available in 1.5 and up
        if ( qcssapp.Functions.checkServerVersion(1,5) ) {
            //set up rewards for OrderModel
            orderObject.rewards = qcssapp.Functions.buildRewardsArray([], true);
        }

        //for discounts
        if ( qcssapp.Functions.checkServerVersion(2,1) ) {
            orderObject.useMealPlanDiscount = qcssapp.CurrentOrder.useMealPlanDiscount;
        }

        //for charging the user now or at time of pickup - ordering for future days
        if(qcssapp.Functions.checkServerVersion(4,0)) {
            if(qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('chargeAtPurchaseTime')) {
                orderObject.chargeAtPurchaseTime = qcssapp.CurrentOrder.storeModel.get('chargeAtPurchaseTime');
            }

            if(qcssapp.futureOrderDate) {
                orderObject.futureOrderDate = qcssapp.futureOrderDate;
            }

            if(qcssapp.personModel != null) {
                orderObject.personID = qcssapp.personModel.get('personID');
            }

            if(qcssapp.CurrentOrder.combos.length > 0) {
                orderObject.combos = qcssapp.Functions.buildCombosArray();
            }
        }

        if(qcssapp.CurrentOrder.grabAndGo) {
            orderObject.grabAndGo = qcssapp.CurrentOrder.grabAndGo;
        }

        return orderObject;
    };

    qcssapp.Functions.checkForRewardsAndDiscounts = function() {

        qcssapp.Functions.showMask(false, false, true);

        //create temporary order object to pass cart products to resource
        var orderObject = {
            "products": qcssapp.Functions.buildProductArray()
        };

        if ( qcssapp.Functions.checkServerVersion(2,1) ) {
            orderObject.useMealPlanDiscount = qcssapp.CurrentOrder.useMealPlanDiscount;
        }

        qcssapp.Functions.orderInquiry(orderObject, qcssapp.Functions.confirmUseDiscount);

    };

    qcssapp.Functions.confirmUseDiscount = function(data) {
        //if there's no data or no discount or the user already specified to not use meal plan discount, proceed as normal
        if ( !data || data.length == 0 || data.discounts == null || data.discounts.length == 0 || !data.useMealPlanDiscount || !qcssapp.Functions.checkServerVersion(2,1,8) ) {
            qcssapp.Functions.handleInquiryResponse( data );
            return;
        }

        var hasSubtotalCouponDiscount = false;
        var mealPlan = false;
        var discountName = '';

        //if there is a discount, check for a subtotal coupon discount
        for ( var i = 0; i < data.discounts.length; i++ ) {
            var discount = data.discounts[i].discount;

            if ( discount.subTotalDiscount && discount.type.toLowerCase() === 'coupon' ) {
                hasSubtotalCouponDiscount = true;
                mealPlan = discount.isMealPlan;
                discountName = discount.name;
                break;
            }
        }

        //if open transaction, skip subtotal discounts popup
        if(qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('chargeAtPurchaseTime') && !qcssapp.CurrentOrder.storeModel.get('chargeAtPurchaseTime')) {
            qcssapp.CurrentOrder.useMealPlanDiscount = false;
            qcssapp.Functions.checkForRewardsAndDiscounts();
            return;
        }

        //if no subtotal coupon discount is chosen, proceed as normal
        if ( !hasSubtotalCouponDiscount ) {
            qcssapp.Functions.handleInquiryResponse( data );
            return;
        }

        //ask if they want to use their subtotal coupon (meal plan) discount
        var title = mealPlan ? 'Use Meal Plan?' : 'Use Discount?';
        var content = '<p>Do you want to use <strong>'+ discountName +'</strong> for this purchase?</p>';
        $.confirm({
            title: title,
            content: content,
            buttons: {
                confirm: {
                    text: 'Yes',
                    btnClass: 'btn-default useMealPlan-yes',
                    action: function() {
                        //if they want to, then proceed as normal
                        qcssapp.Functions.handleInquiryResponse( data );
                    }
                },
                cancel: {
                    text: 'No',
                    btnClass: 'btn-default useMealPlan-no',
                    action: function() {
                        //inquire again with the flag off to get the proper numbers for rewards
                        qcssapp.CurrentOrder.useMealPlanDiscount = false;
                        qcssapp.Functions.checkForRewardsAndDiscounts();
                    }
                }
            }
        });

    };

    qcssapp.Functions.handleInquiryResponse = function(data) {

        //build combos from transaction model products for express reorders
        if(data != null && data.length != 0 && qcssapp.CurrentOrder.expressReorder) {
            qcssapp.Functions.adjustCartForCombos(data);
        }

        //check for transaction model and loyalty points object
        if ( data == null || data.length == 0 || data.loyaltyAccount == null || data.loyaltyAccount.loyaltyPoints == null || data.loyaltyAccount.loyaltyPoints.length == 0 ) {
            qcssapp.Functions.buildReceivePage();
            return;
        }

        //check for if discount makes the transaction free
        if ( data.discounts != null && data.discounts.length > 0 ) {
            qcssapp.CurrentOrder.discounts = data.discounts;
            var discountAmount = Math.abs(data.discounts[0].amount);

            var total = 0;
            $.each(data.products, function() {
                total = total + this.extendedAmount;

                //add modifiers to the total
                if ( this.modifiers && this.modifiers.length > 0 ) {
                    total = total + _.reduce(_.pluck(this.modifiers, 'extendedAmount'), function(a, b) { return a + b }, 0);
                }
            });

            if ( discountAmount.toFixed(2) >= total ) {
                qcssapp.Functions.buildReceivePage();
                return;
            }
        }

        //if open transaction, skip redeem rewards page
        if(qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('chargeAtPurchaseTime') && !qcssapp.CurrentOrder.storeModel.get('chargeAtPurchaseTime')) {
            qcssapp.Functions.buildReceivePage();
            return;
        }

        var loyaltyPoints = data.loyaltyAccount.loyaltyPoints;

        //check for manual rewards
        var manualRewards = [];
        var hasManualRewards = false;
        for ( var m = 0; m < loyaltyPoints.length; m++ ) {
            var loyaltyPointsLine = loyaltyPoints[m];

            if ( !loyaltyPointsLine ) {
                continue;
            }

            if ( loyaltyPointsLine.rewardsAvailableForTransaction != null && loyaltyPointsLine.rewardsAvailableForTransaction.length > 0 ) {
                hasManualRewards = true;
                for ( var i = 0; i <  loyaltyPointsLine.rewardsAvailableForTransaction.length; i++ ) {
                    var rewardLineModel = loyaltyPointsLine.rewardsAvailableForTransaction[i];
                    if ( rewardLineModel == null ) {
                        return true;
                    }

                    var rewardData = {
                        "id": rewardLineModel.reward.id,
                        "pointsToRedeem": rewardLineModel.reward.pointsToRedeem,
                        "name": rewardLineModel.reward.name,
                        "programID": loyaltyPointsLine.loyaltyProgram.id,
                        "maxPerTransaction": rewardLineModel.reward.maxPerTransaction,
                        "redeemable": true,
                        "rewardTypeID": rewardLineModel.reward.rewardTypeId,
                        "value": rewardLineModel.reward.value,
                        "canUseWithOtherRewards": rewardLineModel.reward.canUseWithOtherRewards,
                        "autoPayout": false
                    };

                    manualRewards.push(rewardData);
                }
            }
        }

        var automaticRewards = [];
        var hasAutomaticRewards = false;
        if ( data.products != null && data.products.length > 0 ) {
            for ( var j = 0; j < data.products.length; j++ ) {
                //productLineItemModel
                var product = data.products[j];
                if ( product == null ) {
                    continue;
                }

                //no rewards automatically applied on this product - continue
                if ( !product.hasOwnProperty('rewards') || product.rewards.length == 0 ) {
                    continue;
                }

                //populate automatic rewards
                hasAutomaticRewards = true;
                for ( var k = 0; k < product.rewards.length > 0; k++ ) {
                    var productRewardLine = product.rewards[k];
                    if ( productRewardLine == null || !productRewardLine.hasOwnProperty('reward') ) {
                        continue;
                    }

                    var rewardModel = productRewardLine.reward;

                    var thisReward = {
                        "id": rewardModel.id,
                        "pointsToRedeem": rewardModel.pointsToRedeem,
                        "name": rewardModel.name,
                        "maxPerTransaction": rewardModel.maxPerTransaction,
                        "programID": product.loyaltyPointDetails[k].loyaltyProgramId,
                        "quantity": 1, //always 1 - the ItemRewardModel doesn't have a quantity parameter
                        "rewardTypeID": rewardModel.rewardTypeId,
                        "value": rewardModel.value,
                        "canUseWithOtherRewards": rewardModel.canUseWithOtherRewards,
                        "autoPayout": true
                    };

                    //add this reward to the automatic rewards collection
                    automaticRewards.push(thisReward);
                }
            }
        }

        if ( !hasAutomaticRewards && !hasManualRewards ) {
            qcssapp.Functions.buildReceivePage();
            return;
        }

        //goto redeem rewards view
        qcssapp.Functions.addHistoryRecord(0, 'redeem');

        new qcssapp.Views.RedeemRewards({manualRewards: manualRewards, automaticRewards: automaticRewards, loyaltyPoints: loyaltyPoints});

        qcssapp.Router.navigate('redeem', {trigger: true});

        qcssapp.Functions.hideMask();
    };

    $('#express-reorders').on('beforeChange', function(event,slick,currentSlide,nextSlide) {
        closeExpandedExpressReorder(currentSlide,nextSlide);
    });

    function closeExpandedExpressReorder(currentSlide,nextSlide) {
        var $expressOrder = $('#express-reorders').find('.slick-active').find('.express-order-line');
        if($expressOrder.hasClass('expanded')) {
            var currentHeight = $expressOrder.height();
            var itemsHeight = $expressOrder.find('.express-order-items').height();

            $expressOrder.removeClass('expanded');
            $expressOrder.find('.expand-container').removeClass('expanded');
            $expressOrder.find('.express-order-items').hide();

            currentHeight = currentHeight - itemsHeight;
            $expressOrder.height(currentHeight)
        }

        var $expressDots = $('#express-dots');
        $expressDots.find('.dot').eq(currentSlide).removeClass('dot-active');
        $expressDots.find('.dot').eq(nextSlide).addClass('dot-active');

    }

})(qcssapp, jQuery);