;(function (qcssapp, $) {
    'use strict';

    //******* PRODUCT RELATED FUNCTIONS *******/

    //builds a JSON array of the current products for use on OrderModel
    qcssapp.Functions.buildProductArray = function() {
        //build product array
        var productArr = [];

        $.each(qcssapp.CurrentOrder.products, function() {
            var productObj = {
                "id": this.get('productID'),
                "quantity": this.get('quantity'),
                "name": this.get('name'),
                "price": this.get('originalPrice'),
                "modifiers": qcssapp.Functions.buildModifierArray(this.get('modifiers'))
            };

            if ( qcssapp.Functions.checkServerVersion(2,1,9) ) {
                productObj.keypadID = this.get('keypadID');
            }

            if ( qcssapp.Functions.checkServerVersion(4,0) ) {
                productObj.prepOption = this.get('prepOption');
                productObj.scaleUsed = this.get('scaleUsed');
                productObj.comboTransLineItemId = this.get('comboTransLineItemId');
                productObj.comboDetailId = this.get('comboDetailId');
                productObj.basePrice = this.get('basePrice');
                productObj.comboPrice = this.get('comboPrice');
                productObj.upcharge = this.get('upcharge');
            }

            productArr.push(productObj);
        });

        return productArr;
    };

    //******* PRODUCT NUTRITION RELATED FUNCTIONS *******/

    //checks if the nutrition category is overflowing the cell, if so it replaces the full name with the receipt label
    qcssapp.Functions.checkCategoryForOverflow = function($element) {
        var categoryName = $element.text();
        $('body').append("<span id='categoryClone'>"+categoryName+"</span>");

        var $categoryClone = $('#categoryClone');
        if($categoryClone.width() > $element.width()) {
            $element.text($element.attr('data-category-name'));
        }
        $categoryClone.remove();
    };

    //creates an object with the necessary product information for determining the nutrition information
    qcssapp.Functions.getProductObject = function(data) {
        var product = {};
        product.PAPLUID = data.productID || data.cartProductID;
        product.QUANTITY = data.quantity;
        product.PRODUCTNAME = data.name;

        if(data.modifiers != null && data.modifiers.length > 0) {
            product.MODIFIERINFO = data.modifiers;
        }
        return product;
    };

    //******* PURCHASE RESTRICTION PROFILE RELATED FUNCTIONS *******/

    //check if the selected product has a purchase restriction, only do this if the user has QC Balance spending method available
    qcssapp.Functions.checkPurchaseRestriction = function(productModel) {

        var paymentMethodCollection = qcssapp.Functions.getStorePaymentSetting("paymentMethodCollection", []);

        //if there are no purchase restriction details or Use CC as a Tender is configured and there is a payment method, don't check for restrictions on product selection
        if(!qcssapp.Functions.checkServerVersion(4,0) || qcssapp.CurrentOrder.storeModel.get('purchaseRestrictionProfileDetails').length == 0 || (qcssapp.Functions.isCreditCardAllowed() && paymentMethodCollection.length > 0) ) {
            return true;
        }

        var validatedItemRestrictionType = qcssapp.Functions.validateItemPurchaseRestriction(productModel);

        if(validatedItemRestrictionType != 0) {

            var container;
            if ($(".product-code-input-view").length > 0) {
                // If entering a product code, overlay the restriction message on the input view
                container = ".product-code-input-view";
            } else {
                // Otherwise, overlay on the view that is active
                container = "#" + $(".view.active").attr("id");
            }

            var options = {
                onCheckOut: false,
                purchaseRestrictionProducts: productModel,
                container: container,
                hidden: true
            };

            this.purchaseRestrictionView = new qcssapp.Views.PurchaseRestrictionView(options);
            this.purchaseRestrictionView.fadeIn();
            return false;
        }

        return true;
    };

    //check if any products in the cart have a purchase restriction
    qcssapp.Functions.checkPurchaseRestrictionOnNext = function() {
        if( qcssapp.CurrentOrder.storeModel.get('purchaseRestrictionProfileDetails').length == 0 ) {
            return true;
        }

        var ineligibleProducts = [];

        //loop over each product in the cart and check if it has a purchase restriction
        $.each(qcssapp.CurrentOrder.products, function() {

            var validatedItemRestrictionType = qcssapp.Functions.validateItemPurchaseRestriction(this.attributes);

            //if purchase restriction found, add to array
            if(validatedItemRestrictionType != 0) {
                ineligibleProducts.push(this);
            }
        });

        //if there are any purchase restriction products, show alert
        if(ineligibleProducts.length > 0) {

            var ineligibleProductsTotalPrice = 0;
            var ineligibleProductsTotalTax = 0;
            var rewardsTotalAmount = 0;
            var discountsTotalAmount = 0;
            var voucherAmount = 0;

            if(ineligibleProducts) {
                $.each(ineligibleProducts, function(index, product) {
                    ineligibleProductsTotalPrice += Number(product.get('price'));

                    //Defect 4108: A modifier price should only count towards a restricted
                    // product's total price if they share a tax ID.
                    if (product.attributes.modifiers) {
                        if (!product.attributes.taxIDs) {
                            product.attributes.taxIDs = "";
                        }
                        var curProdTaxIDs = product.attributes.taxIDs.split(",");
                        $.each(product.attributes.modifiers, function(modIndex, curMod) {
                            if (!curMod.taxIDs) {
                                curMod.taxIDs = "";
                            }
                            var curModTaxIDs = curMod.taxIDs.split(",");
                            if(!curModTaxIDs.some(function(val) { return curProdTaxIDs.indexOf(val) >= 0; })) {
                                //if they do not share a Tax ID, then subtract the modifier price (including prep options) from the current total
                                var modifierPrice = Number(curMod.price);
                                modifierPrice += curMod.prepOptions.map(prep => Number(prep.price)).reduce((total, cur) => total + cur, 0);
                                ineligibleProductsTotalPrice -= modifierPrice;
                            }
                        });
                    }

                    // Defect 4073: A zero-dollar product should still be restricted if its tax is nonzero
                    if (product.get("taxTotal")) {
                        ineligibleProductsTotalTax += Number(product.get("taxTotal"));
                    }
                });
            }

            if(qcssapp.CurrentOrder.rewards) {
                $.each(qcssapp.CurrentOrder.rewards, function(index, reward) {
                    rewardsTotalAmount += Number(reward.get('value'));
                });
            }

            if(qcssapp.CurrentOrder.discounts) {
                $.each(qcssapp.CurrentOrder.discounts, function(index, discount){
                    discountsTotalAmount -= Number(discount.amount);
                });
            }

            if (qcssapp.CurrentOrder.voucherTotal) {
                voucherAmount = qcssapp.CurrentOrder.voucherTotal;
            }

            // Do not apply purchase restrictions if:
            //   1. The tax on restricted products is covered by a voucher
            //   2. The price of restricted products is covered by the remaining voucher amount plus discounts and rewards
            if (qcssapp.Functions.comparePrices(voucherAmount, ineligibleProductsTotalTax) >= 0 &&
                qcssapp.Functions.comparePrices(discountsTotalAmount + rewardsTotalAmount + voucherAmount - ineligibleProductsTotalTax, ineligibleProductsTotalPrice) >= 0)
            {
                return true;
            }

            var options = {
                onCheckOut: true,
                purchaseRestrictionProducts: ineligibleProducts,
                container: '#' + $('.view.active').attr('id'),
                hidden: true
            };

            this.purchaseRestrictionView = new qcssapp.Views.PurchaseRestrictionView(options);
            this.purchaseRestrictionView.fadeIn();
            return false;
        }

        return true;
    };

    //validate if the productModel falls under any of the purchase restrictions
    qcssapp.Functions.validateItemPurchaseRestriction = function(productModel) {
        var foundRestrictionType = 0;

        //check each product restriction against the productModel details
        $.each(qcssapp.CurrentOrder.storeModel.get('purchaseRestrictionProfileDetails'), function() {

            //extra validation in case productModel doesn't have productID, subDeptID, departmentID - avoid front-end error
            var productID = typeof productModel.productID === 'undefined' ? productModel.id : productModel.productID;
            var subDeptID = typeof productModel.subDeptID === 'undefined' ? 0 : productModel.subDeptID;
            var departmentID = typeof productModel.departmentID === 'undefined' ? 0 : productModel.departmentID;

            if(productModel.price<=0){
                return foundRestrictionType;
            } else if(this['papluID'] != null && productID == this['papluID']) { //product restriction
                foundRestrictionType = 1;
                return false;

            } else if(this['pasubDeptID'] != null && subDeptID == this['pasubDeptID']) { //sub dept restriction
                foundRestrictionType = 2;
                return false;

            } else if(this['padepartmentID'] != null && departmentID == this['padepartmentID']) { //department restriction
                foundRestrictionType = 3;
                return false;
            }
        });

        return foundRestrictionType; //returning the type of restriction in case we have to use it later
    };

    //build prep option and upcharge name
    qcssapp.Functions.buildProductName = function(productModel) {
        var name = "";

        //if this product is part of a combo
        if(productModel.get('isCombo')) {

            //if product has an upcharge set the price as the upcharge
            var upchargeAmount = Number(productModel.get('originalPrice')) - Number(productModel.get('basePrice'));

            if(upchargeAmount < 0) {
                upchargeAmount = upchargeAmount * -1;
                name = productModel.get('name') + ' (' + qcssapp.Functions.formatPrice(upchargeAmount, '-') + ')';

            } else if(upchargeAmount > 0) {
                name = productModel.get('name') + ' (' + qcssapp.Functions.formatPrice(upchargeAmount, '+') + ')';
            }
        }

        if ( !$.isEmptyObject(productModel.get('prepOption')) ) {
            var prepOption = typeof productModel.get('prepOption').attributes !== 'undefined' ? productModel.get('prepOption').attributes : productModel.get('prepOption');

            var canShowPrepOption = !(prepOption.defaultOption && !prepOption.displayDefault) || (prepOption.defaultOption && Number(prepOption.price) > 0);

            if(canShowPrepOption) {

                var prepOptionName = qcssapp.Functions.htmlDecode(prepOption.name);

                if(name == "") {
                    name = productModel.get('name') + ' - <span class="prep-option-name">' + prepOptionName;
                } else {
                    name += ' - <span class="prep-option-name">' + prepOptionName;
                }

                if ( Number(prepOption.price) > 0 ) {
                    var prepPrice = Number(prepOption.price).toFixed(2);
                    name += ' (' + qcssapp.Functions.formatPrice(prepPrice, '+') + ')</span>';
                } else {
                    name += '</span>';
                }
            }
        }

        return name;
    };

    //******* PRODUCT SCAN RELATED FUNCTIONS *******//

    //sets the scan popup flag and saves it to local storage
    qcssapp.Functions.setFirstScanPopup = function() {
        var mdl = new qcssapp.Models.FirstBarcodeScanModel({
            id: 1,
            scanPopupShown: 'true'
        }, {parse:true});
        mdl.save();
    };

    //check the scan popup flag
    qcssapp.Functions.checkFirstScanPopup = function() {
        var mdl = new qcssapp.Models.FirstBarcodeScanModel();
        mdl.fetch();

        if ( mdl.attributes ) {
            return (mdl.get("scanPopupShown") == "true");
        }

        return false;
    };

    qcssapp.Functions.handleProductScan = function(code) {
        var pageLocation = window.location.hash;

        var $productCodeInputView = $('.product-code-input-view');
        var $appendElement = $productCodeInputView.length ? $productCodeInputView : '';
        var preventShowMask = $productCodeInputView.length ? true : false;

        // If on modifier or product page, show alert that you must finish adding current product first
        if ( pageLocation.indexOf('#modifier') != -1 || pageLocation.indexOf('#product') != -1 ) {
            qcssapp.Functions.displayPopup('Please finish adding this item to the Cart before trying to scan another item.', 'Unable to Scan Item', 'OK', '', '', '', '', '', $appendElement);
            return;

            // If on receive, rewards or review page, show alert that you can't scan product at this point
        } else if ( pageLocation.indexOf('#show-receive') != -1 || pageLocation.indexOf('#redeem') != -1 || pageLocation.indexOf('#review') != -1 ) {
            qcssapp.Functions.displayPopup('You are unable to scan an item at this point in the transaction. Please return to your Cart if you would like to add this item to your order.', 'Unable to Scan Item', 'OK', '', '', '', '', '', $appendElement);
            return;

            // If on store page show alert that you must choose store first
        } else if ( pageLocation.indexOf('#show-order') != -1 ) {
            qcssapp.Functions.displayPopup('Please select a Store before scanning an item', 'Unable to Scan Item', 'OK', '', '', '', '', '', $appendElement);
            return;
        }

        if ( !code || !code.toString() ) {
            return;
        }

        //search for product by plu code
        var endPoint = qcssapp.Location + '/api/ordering/product/code/' + code.toString();

        var productScanSuccess = function( response ) {
            if ( response && response.id == 0 && response.name != '' || response.priceOpen ) {
                qcssapp.Functions.displayPopup('The item code <b>'+code+'</b> is unavailable due to configuration.', 'Invalid Code', 'OK', '', '', '', '', '', $appendElement);

                return;
            }

            if ( !response || !response.id || response.id == 0 ) {
                qcssapp.Functions.displayPopup('Unable to find a valid item for the code <b>'+code+'</b>.', 'Invalid Code', 'OK', '', '', '', '', '', $appendElement);
                return;
            }


            // if adding item to a combo, verify the product scanned is a valid item in the combo.
            if( qcssapp.CurrentCombo.id != ""){
                var productId = response.id;

                var productIsInCombo = qcssapp.Functions.verifyProductInCombo(productId);

                if(!productIsInCombo){
                    qcssapp.Functions.displayPopup('The product selected is not available for the current combo. Please select another product.', 'Invalid Combo Item', 'OK', '', '', '', '', '', $appendElement);
                    return;
                }
            }

            var productName = response.name;

            qcssapp.ProductInProgress.id = response.id;
            qcssapp.ProductInProgress.productInStore = response.productInStore;
            //Defect 3900 : prepOption sets weren't being loaded properly through this process, passing the entire response so that we can grab the prep option set id in the product-detail functions
            qcssapp.Functions.loadProductFromScan(response);
        };

        qcssapp.Functions.callAPI(endPoint, 'GET', '', '', productScanSuccess,
            function() {
                qcssapp.Functions.displayPopup('An error occurred when attempting to look up product with code: ' + code +' , please try again or contact a manager if this issue continues.', 'Error', 'OK', '', '', '', '', '', $appendElement);
            }, '', '', '', preventShowMask
        );
    };

    qcssapp.Functions.showScanPlugin = function() {
        if( !qcssapp.onPhoneApp ) {
            return;
        }

        qcssapp.phoneScanActive = true;
        qcssapp.phoneScanBack = false;

        cordova.plugins.barcodeScanner.scan(
            function (result) {
                qcssapp.phoneScanActive = navigator.userAgent.match(/Android/i) ? true : qcssapp.phoneScanActive;

                $('.camera-scan-mask').hide();
                if(result.text && result.text.trim() != "") {
                    qcssapp.Functions.handleProductScan(result.text.toString());
                } else {
                    new qcssapp.Views.ProductCodeInputView({hidden: true});

                    // Flag if the user hit the back button on the Android phone
                    qcssapp.phoneScanBack = navigator.userAgent.match(/Android/i);
                }
            },
            function (error) {
                qcssapp.phoneScanActive = navigator.userAgent.match(/Android/i) ? true : qcssapp.phoneScanActive;

                $('.camera-scan-mask').hide();
                qcssapp.Functions.displayPopup(error, 'Unable to Scan', 'OK');
            },
            {
                preferFrontCamera : false, // iOS and Android
                showFlipCameraButton : false, // iOS and Android
                showTorchButton : true, // iOS and Android
                torchOn: false, // Android, launch with the torch switched on (if available)
                saveHistory: true, // Android, save scan history (default false)
                prompt : "Place a barcode inside the scan area", // Android
                resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
                orientation : "", // Android only (portrait|landscape), default unset so it rotates with the device
                disableAnimations : true, // iOS
                disableSuccessBeep: false // iOS and Android
            }
        );

        $('.camera-scan-mask').show();
    };

    $(document).on('click', '.scan-barcode-btn', function() {
        if(!qcssapp.Functions.checkFirstScanPopup()) {
            new qcssapp.Views.BarcodeScanHelpView({hidden: true});

            qcssapp.Functions.setFirstScanPopup();
            return;
        }

        qcssapp.Functions.showScanPlugin();
    });

    //******* PRODUCT BACKWARDS COMPATIBILITY RELATED FUNCTIONS *******/

    qcssapp.Functions.getFavoriteDetails = function(productView) {
        if( !qcssapp.Functions.checkServerVersion(1,6) ) {
            return;
        }

        var endPoint = qcssapp.Location + '/api/ordering/favorite/details';

        if( qcssapp.Functions.checkServerVersion(3,0) ) {
            endPoint = qcssapp.Location + '/api/favorite/details';
        }

        var productFavoriteModel = qcssapp.Functions.setProductFavoriteModel(productView.model);

        qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(productFavoriteModel), '',
            function(response) {

                if(response.terminalID && qcssapp.Functions.isNumeric(response.terminalID) && !qcssapp.Functions.getStoreSetting('terminalID', null)) {
                    qcssapp.CurrentOrder.storeModel.set('terminalID', response.terminalID);
                }

                if( qcssapp.Functions.checkServerVersion(4,0) ) {
                    //product is a favorite, set orderingFavoriteID
                    if(response) {
                        //if the favorite is active show heart-icon
                        if(response.active) {
                            productView.$favorite.addClass('favorite').attr('src', 'images/icon-favorites.svg');
                        } else {
                            productView.$favorite.removeClass('favorite').attr('src', 'images/icon-favorites-unchecked.svg');
                        }
                    }

                } else {
                    //product is not a favorite
                    if(response.length == 0) {
                        return;
                    }

                    //product is a favorite, set orderingFavoriteID
                    if(response[0]['ID']) {
                        //if the product is a ACTIVE favorite show heart-icon and set model property
                        if(response[0]['ACTIVE']) {
                            productView.$favorite.addClass('favorite').attr('src', 'images/icon-favorites.svg');
                            productView.productFavoriteModel.set('active', true);
                        }
                    }
                }

                productView.addProductFavoriteModel(response);

            }.bind(productView),
            function() {
                if (qcssapp.DebugMode) {
                    console.log("Error settings this product as favorite.");
                }

            }, '', '', '', true, true
        );
        //$('#my-quick-picks-page').off();
    };

    qcssapp.Functions.checkFavoriteDetails = function(productView) {

        var productFavoriteModel = productView.isProductAndModsFavorite();

        if(productFavoriteModel == null) {
            qcssapp.Functions.getFavoriteDetails(productView);
        }
    };

    qcssapp.Functions.setProductFavoriteModel = function(model) {
        var productFavoriteModel = new qcssapp.Models.ProductFavorite();

        productFavoriteModel.set('id', model.get('productID'));
        productFavoriteModel.set('productID', model.get('productID'));
        productFavoriteModel.set('modifiers', qcssapp.Functions.buildModifierArray(model.get('modifiers')));
        productFavoriteModel.set('prepOption', model.get('prepOption'));
        productFavoriteModel.set('keypadID', model.get('keypadID'));
        productFavoriteModel.set('orderingFavoriteID', model.get('orderingFavoriteID'));

        var terminalID = qcssapp.CurrentOrder.storeModel && typeof qcssapp.CurrentOrder.storeModel.get('terminalID') !== 'undefined' ? qcssapp.CurrentOrder.storeModel.get('terminalID') : null;
        productFavoriteModel.set('terminalID', terminalID);

        if(model.get('prepOption') && !productFavoriteModel.get('prepOptionSetID') && model.get('prepOption').attributes) {
            productFavoriteModel.set('prepOptionSetID', model.get('prepOption').get('prepOptionSetID'));
        } else if(model.get('prepOption') && !productFavoriteModel.get('prepOptionSetID')) {
            productFavoriteModel.set('prepOptionSetID', model.get('prepOption').prepOptionSetID);
        }

        return productFavoriteModel;
    };

    qcssapp.Functions.loadModifierDetails = function(id, successFunction, successParam, productResponse) {
        var endPoint = qcssapp.Location + "/api/ordering/modmenu/"+id;

        qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
            function(response) {
                //check for valid response
                if (Number(response.id) >= 0 && response.items && response.items.length > 0) {

                    var callbackFn = function() {
                        //check if there is another modifier keypad, load that
                        if( response.nextSetDetailID ) {
                            qcssapp.Functions.loadModifierDetails(response.nextSetDetailID, successFunction, successParam, productResponse);

                        } else if(typeof successFunction === 'function') {
                            productResponse.modifierMenus = qcssapp.ProductInProgress.modifierMenus;
                            successFunction(productResponse, successParam);
                        }
                    };

                    if(!qcssapp.Functions.checkServerVersion(1,6)) {
                        qcssapp.ProductInProgress.modifierMenus.push(response);

                        callbackFn();
                    } else {
                        qcssapp.Functions.loadProductModifierNutrition(productResponse, response, callbackFn);
                    }

                } else {
                    qcssapp.Functions.displayPopup('There was an issue loading the modifier details. If this continues, please contact the store manager and/or select a different product.');
                }
            }, '', '', '', '', true, true
        );
    };

    // Get the Product and Modifier Nutrition info for backwards compatibility
    qcssapp.Functions.loadProductModifierNutrition = function(productResponse, modifierResponse, callbackFn) {
        var endPoint = qcssapp.Location + '/api/ordering/product/nutrition';

        var dataParameters = qcssapp.Functions.formatProductAndModsForNutrition(productResponse, modifierResponse);

        var params = {
            productResponse: productResponse,
            modifierResponse: modifierResponse,
            callbackFn: callbackFn
        };

        qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(dataParameters), params,
            function(nutritionResponse) {
                if ( nutritionResponse ) {

                    qcssapp.Functions.checkNutritionCategories(nutritionResponse);

                    try {
                        qcssapp.Functions.formatProductNutrition(params.productResponse, nutritionResponse);
                        qcssapp.Functions.formatModifierNutrition(params.modifierResponse, nutritionResponse);
                    } catch (e) {
                        qcssapp.Functions.logError(e.message, 'qcssapp.Functions.loadProductModifierNutrition', 478, 0, e.name);
                    }

                    qcssapp.ProductInProgress.modifierMenus.push(params.modifierResponse);

                    callbackFn();
                }
            },
            '', '', '', '', true, true
        );
    };

    // Get the Product and Nutrition info for backwards compatibility
    qcssapp.Functions.loadProductNutrition = function(productResponse, successFunction, successParam) {
        var endPoint = qcssapp.Location + '/api/ordering/product/nutrition';

        var modifiers = { items: [] };

        var dataParameters = qcssapp.Functions.formatProductAndModsForNutrition(productResponse, modifiers);

        var params = {
            productResponse: productResponse,
            successFunction: successFunction,
            successParam: successParam
        };

        qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(dataParameters), params,
            function(nutritionResponse) {
                if ( nutritionResponse ) {

                    qcssapp.Functions.checkNutritionCategories(nutritionResponse);

                    try {
                        qcssapp.Functions.formatProductNutrition(params.productResponse, nutritionResponse);
                    } catch (e) {
                        qcssapp.Functions.logError(e.message, 'qcssapp.Functions.loadProductNutrition', 513, 0, e.name);
                    }

                    if(typeof params.successFunction === 'function') {
                        params.successFunction(params.productResponse, params.successParam);
                    }
                }
            },
            '', '', '', '', true, true
        );
    };

    // Creates an object with the necessary product information for determining the nutrition information
    qcssapp.Functions.formatProductAndModsForNutrition = function(product, modResponse) {
        var product = {
            PAPLUID: product.id,
            QUANTITY: product.quantity,
            PRODUCTNAME: product.name
        };

        if(modResponse.items != null && modResponse.items.length > 0) {
            product.MODIFIERINFO = modResponse.items;
        }

        return product;
    };

    // Update the global nutrition information if it is empty
    qcssapp.Functions.checkNutritionCategories = function(response) {
        if(qcssapp.nutrition.nutritionCategory1 == null && response['nutritionCategories'] && response['nutritionCategories'][0] && !$.isEmptyObject(response['nutritionCategories'][0])) {
            qcssapp.nutrition.nutritionCategory1 = response['nutritionCategories'][0];
        }

        if(qcssapp.nutrition.nutritionCategory2 == null && response['nutritionCategories'] && response['nutritionCategories'][1] && !$.isEmptyObject(response['nutritionCategories'][1])) {
            qcssapp.nutrition.nutritionCategory2 = response['nutritionCategories'][1];
        }

        if(qcssapp.nutrition.nutritionCategory3 == null && response['nutritionCategories'] && response['nutritionCategories'][2] && !$.isEmptyObject(response['nutritionCategories'][2])) {
            qcssapp.nutrition.nutritionCategory3 = response['nutritionCategories'][2];
        }

        if(qcssapp.nutrition.nutritionCategory4 == null && response['nutritionCategories'] && response['nutritionCategories'][3] && !$.isEmptyObject(response['nutritionCategories'][3])) {
            qcssapp.nutrition.nutritionCategory4 = response['nutritionCategories'][3];
        }

        if(qcssapp.nutrition.nutritionCategory5 == null && response['nutritionCategories'] && response['nutritionCategories'][4] && !$.isEmptyObject(response['nutritionCategories'][4])) {
            qcssapp.nutrition.nutritionCategory5 = response['nutritionCategories'][4];
        }
    };

    // Update the product nutrition information if it is empty
    qcssapp.Functions.formatProductNutrition = function(productResponse, nutritionResponse) {
        if(typeof productResponse.nutritionInfo1 !== 'undefined') {
            return;
        }

        var productNutrition = nutritionResponse['nutritionItemList'] && nutritionResponse['nutritionItemList'][0] && !$.isEmptyObject(nutritionResponse['nutritionItemList'][0]) ? nutritionResponse['nutritionItemList'][0] : null;

        if(productNutrition == null) {
            return;
        }

        var productNutritionID = typeof productNutrition['pluID'] !== 'undefined' ? productNutrition['pluID'] : null;

        if(productNutritionID == null || productNutritionID != productResponse.id) {
            return;
        }

        productResponse.nutritionInfo1 = qcssapp.Functions.isNumeric(productNutrition['nutritionInfo1']) ? productNutrition['nutritionInfo1'] : '';
        productResponse.nutritionInfo2 = qcssapp.Functions.isNumeric(productNutrition['nutritionInfo2']) ? productNutrition['nutritionInfo2'] : '';
        productResponse.nutritionInfo3 = qcssapp.Functions.isNumeric(productNutrition['nutritionInfo3']) ? productNutrition['nutritionInfo3'] : '';
        productResponse.nutritionInfo4 = qcssapp.Functions.isNumeric(productNutrition['nutritionInfo4']) ? productNutrition['nutritionInfo4'] : '';
        productResponse.nutritionInfo5 = qcssapp.Functions.isNumeric(productNutrition['nutritionInfo5']) ? productNutrition['nutritionInfo5'] : '';

    };

    // Update the modifier nutrition information if it is empty
    qcssapp.Functions.formatModifierNutrition = function(modifierResponse, nutritionResponse) {
        if(!nutritionResponse['nutritionItemList']) {
            return;
        }

        var nutritionItems = nutritionResponse['nutritionItemList'];
        var modifiers = modifierResponse.items;

        for(var i=1; i<nutritionItems.length; i++) {

            for(var j=0; j<modifiers.length; j++) {
                if(nutritionItems[i].pluID && nutritionItems[i].pluID == modifiers[j].id) {
                    modifiers[j].nutritionInfo1 = qcssapp.Functions.isNumeric(nutritionItems[i]['nutritionInfo1']) ? nutritionItems[i]['nutritionInfo1'] : '';
                    modifiers[j].nutritionInfo2 = qcssapp.Functions.isNumeric(nutritionItems[i]['nutritionInfo2']) ? nutritionItems[i]['nutritionInfo2'] : '';
                    modifiers[j].nutritionInfo3 = qcssapp.Functions.isNumeric(nutritionItems[i]['nutritionInfo3']) ? nutritionItems[i]['nutritionInfo3'] : '';
                    modifiers[j].nutritionInfo4 = qcssapp.Functions.isNumeric(nutritionItems[i]['nutritionInfo4']) ? nutritionItems[i]['nutritionInfo4'] : '';
                    modifiers[j].nutritionInfo5 = qcssapp.Functions.isNumeric(nutritionItems[i]['nutritionInfo5']) ? nutritionItems[i]['nutritionInfo5'] : '';
                }

            }
        }
    };

    // Compare prices, rounding to 2 decimal places
    // Returns -1, 0, or 1 as price1 is less than, equal to, or greater than price2
    qcssapp.Functions.comparePrices = function(price1, price2) {
        price1 = parseFloat(price1.toFixed(2));
        price2 = parseFloat(price2.toFixed(2));
        if (price1 < price2) {
            return -1;
        } else if (price1 === price2) {
            return 0;
        } else {
            return 1;
        }
    };

})(qcssapp, jQuery);