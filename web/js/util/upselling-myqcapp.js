;(function (qcssapp, $) {
    'use strict';

    //******* PRODUCT SUGGESTIVE KEYPAD RELATED FUNCTIONS *******/

    //determine which complementary suggestive keypad should show based on product or sub department configuration and what's already been shown
    qcssapp.Functions.getSuggestiveKeypad = function(prodComplementaryKeypad, subDeptComplementaryKeypad) {
        //if the product's complementary keypad hasn't been used then return it
        if( prodComplementaryKeypad != null && $.inArray(prodComplementaryKeypad, qcssapp.CurrentOrder.suggestedKeypadIDs) == -1 ) {
            return prodComplementaryKeypad;

        //otherwise if the sub department's complementary keypad hasn't been used then return it
        } else if( subDeptComplementaryKeypad != null && $.inArray(subDeptComplementaryKeypad, qcssapp.CurrentOrder.suggestedKeypadIDs) == -1 ) {
            return subDeptComplementaryKeypad;
        }

        return null;
    };

    //get the details for the suggestive keypad that will show after 'Add to Cart'
    qcssapp.Functions.loadProductSuggestiveKeypad = function( keypadID, subDeptComplementaryKeypad ) {
        var endPoint = qcssapp.Location + '/api/ordering/suggestive/selling/' + keypadID;

        qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
            function(response) {

                if( response.length == 0) {
                    console.log('There was an error loading the product suggestions.');

                    var suggestiveKeypadID = qcssapp.Functions.getSuggestiveKeypad(null, subDeptComplementaryKeypad);
                    if( suggestiveKeypadID != null ) {
                        qcssapp.Functions.loadProductSuggestiveKeypad( suggestiveKeypadID, null );
                        return;
                    }

                    qcssapp.Functions.addHistoryRecord(0, 'cart');
                    qcssapp.Router.navigate('cart', {trigger: true});

                    return;
                }

                //check suggestive keypad items against items in the cart
                if( !qcssapp.Functions.validateSuggestiveItems( response[0].items ) ) {
                    qcssapp.Functions.addHistoryRecord(0, 'cart');
                    qcssapp.Router.navigate('cart', {trigger: true});
                    return;
                }

                //for the SuggestiveSelling view to know it's from 'Add to Cart' route
                response.fromLocation = "product";

                //flag this keypad as 'suggested' so we know not to show it again
                qcssapp.CurrentOrder.suggestedKeypadIDs.push(keypadID);

                qcssapp.Functions.addHistoryRecord(0, 'suggestive');

                //create new Suggestive Selling view
                qcssapp.SuggestionView = new qcssapp.Views.SuggestiveSelling(response);

                //navigate to new suggestive selling view
                qcssapp.Router.navigate('suggestive-selling', {trigger: true});

            }, '', '', '', '', false, false
        );
    };

    //check the upsell profile on the terminal for valid suggestive selling keypads on 'Check Out'
    qcssapp.Functions.checkUpsellProfileForKeypads = function() {
        var validSuggestions = [];
        var suggestions = 0;

        var upsellProfileKeypads = qcssapp.CurrentOrder.storeModel.get('upsellProfileKeypads');
        var maxNumUpsellKeypads = qcssapp.CurrentOrder.storeModel.get('maxNumUpsellKeypads');

        if(upsellProfileKeypads.length == 0 || maxNumUpsellKeypads == null) {
            return validSuggestions;
        }

        //get the id's of the keypads that have already been suggested
        var invalidSuggestiveKeypadIDs = qcssapp.CurrentOrder.suggestedKeypadIDs.slice();

        //get the id's of the similarKeypadIDs on all the sub department's of the products in the cart
        $.each(qcssapp.CurrentOrder.products, function() {
            if(this.get('subDeptSimilarKeypadID') != null && $.inArray(this.get('subDeptSimilarKeypadID'), invalidSuggestiveKeypadIDs) == -1) {
                invalidSuggestiveKeypadIDs.push(this.get('subDeptSimilarKeypadID'));
            }
        });

        //only add valid suggestive keypads to the list if they're not in the invalid list and we haven't reached the maxNumUpsellKeypads
        $.each(upsellProfileKeypads, function() {
            if( $.inArray(this.id, invalidSuggestiveKeypadIDs) == -1 && suggestions != maxNumUpsellKeypads && qcssapp.Functions.validateSuggestiveItems(this.items) ) {
                validSuggestions.push(this);
                suggestions++;

            // Add remaining upsell keypads to inactive list so they're no showing again
            } else if( $.inArray(this.id, invalidSuggestiveKeypadIDs) == -1 ) {
                qcssapp.CurrentOrder.suggestedKeypadIDs.push(this.id);
            }
        });

        return validSuggestions;
    };

    //check if there is at least one item on the suggestive selling keypad that is not in the cart
    qcssapp.Functions.validateSuggestiveItems = function( keypadItems ) {
        var itemOnKeypadNotInCart = false;

        //check if the item on the keypad is in the cart
        $.each(keypadItems, function() {
            var productID = this.productID;
            var foundProduct = false;

            $.each(qcssapp.CurrentOrder.products, function() {
                if(productID == this.get('productID')) {
                    foundProduct = true;
                    return false;
                }
            });

            //if at least one product is not in the cart
            if(!foundProduct) {
                itemOnKeypadNotInCart = true;
                return false;
            }
        });

        //if true then at least one item on the keypad is not in the cart so show the suggestive keypad
        return itemOnKeypadNotInCart;
    };

})(qcssapp, jQuery);