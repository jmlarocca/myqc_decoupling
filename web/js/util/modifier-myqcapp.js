;(function (qcssapp, $) {
    'use strict';

    //******* MODIFIER RELATED FUNCTIONS *******/

    //reset the current changes object
    qcssapp.Functions.clearCurrentModifiers = function() {
        qcssapp.ProductInProgress = {
            "id": "",
            "name": "",
            "quantity":"",
            "cartProductID": "",
            "autoMod": false,
            "modifiers": [],
            "modIndex": 0,
            "prepOption": {},
            "substituteProduct": {} ,
            "prepOptionSetID": "",
            "productInStore": true,
            "modifierMenus": [],
            "nutritionInfo": [],
            "favorite": true
        };
    };

    //takes an array of modifier objects and returns a comma separated list of the names and prices (if not zero)
    qcssapp.Functions.convertModifierArrayToCsv = function(mods) {
        var list = "";

        $.each(mods, function() {
            if ( Number(this.price) > 0 ) {
                list = list + this.name + " (" + qcssapp.Functions.formatPrice(this.price, '+') + ")" + ", ";
            } else if ( Number(this.price) < 0 ) {
                list = list + this.name + " (" + qcssapp.Functions.formatPrice(this.price) + ")" + ", ";
            } else {
                list = list + this.name + ", ";
            }
        });

        list = list.substr(0, list.length - 2);

        return qcssapp.Functions.htmlDecode( list );
    };

    //takes an array of modifier objects and returns a comma separated list of the names and prices (if not zero)
    qcssapp.Functions.convertModifierAndPrepOptionArrayToCsv = function(mods, preventTag) {
        if( !qcssapp.Functions.checkServerVersion(4,0) ) {
            return qcssapp.Functions.convertModifierArrayToCsv(mods);
        }

        var list = "";

        $.each(mods, function() {

            var modifierLine = '';

            if ( Number(this.price) > 0 ) {
                modifierLine = this.name + " (" + qcssapp.Functions.formatPrice(this.price, '+', preventTag) + ")";
            } else if ( Number(this.price) < 0 ) {
                modifierLine = this.name + " (" + qcssapp.Functions.formatPrice(this.price, undefined, preventTag) + ")";
            } else {
                modifierLine = this.name;
            }

            //if the prep option exists show prep option, don't display prep if it is the default option and displayDefaultMyQC is OFF and price is not greater than 0
            if(this.prepOption != null) {
                var defaultOption = typeof this.prepOption.defaultOption === 'undefined' ? this.prepOption.get('defaultOption') : this.prepOption.defaultOption;
                var displayDefault = typeof this.prepOption.displayDefault === 'undefined' ? this.prepOption.get('displayDefault') : this.prepOption.displayDefault;
                var prepPrice = typeof this.prepOption.price === 'undefined' ? this.prepOption.get('price') : this.prepOption.price;
                var prepName = typeof this.prepOption.name === 'undefined' ? this.prepOption.get('name') : this.prepOption.name;

                var canShowPrepName = !(defaultOption == true && !displayDefault) || (defaultOption == true && Number(prepPrice) > 0);

                if(canShowPrepName) {
                    var prepOptionName = qcssapp.Functions.htmlDecode(prepName);
                    if ( Number(prepPrice) > 0 ) {
                        modifierLine += ' - ' + prepOptionName + " (" + qcssapp.Functions.formatPrice(prepPrice, '+') + ")";
                    } else {
                        modifierLine += ' - ' + prepOptionName;
                    }
                }
            }

            list += modifierLine + ", ";
        });

        list = list.substr(0, list.length - 2);

        return list;
    };

})(qcssapp, jQuery);