;(function (qcssapp, $) {
    'use strict';

    //******* FAVORITE ITEMS AND VIEW RELATED FUNCTIONS *******/

    //creates a favorite record for the product, returns orderingFavoriteID if successful
    qcssapp.Functions.createFavorite = function(favoriteMod, callbackFn) {
        var  endpoint = qcssapp.Location + '/api/ordering/favorite/update';

        if( qcssapp.Functions.checkServerVersion(2,1) ) {
            endpoint = qcssapp.Location + '/api/ordering/favorite/active';

            if( qcssapp.Functions.checkServerVersion(3,0) ) {
                endpoint = qcssapp.Location + '/api/favorite/product/active';
            }
        }

        var favoriteModel = qcssapp.Functions.setProductFavoriteModel(favoriteMod);
        favoriteModel.set('active', favoriteMod.get('active'));

        qcssapp.Functions.callAPI(endpoint, 'POST', JSON.stringify(favoriteModel), '',
            function(response) {
                if(response == null) {
                    qcssapp.Functions.displayError('There was an issue creating this product as a favorite');
                    return;
                }

                //update the favoriteModel with the new favorite record's orderingFavoriteID
                favoriteModel.set('orderingFavoriteID', response);

                if ( callbackFn && typeof callbackFn == 'function' ) {
                    callbackFn(favoriteModel);
                }
            },
            function() {
                if (qcssapp.DebugMode) {
                    console.log("Error creating this product as a favorite.");
                }
            },
            '', '', '', true, true
        );
    };

    //remove the product as a favorite, passes in orderingFavoriteID and returns 1 if successful
    qcssapp.Functions.removeFavorite = function(favorite, callbackFn, callbackParams) {
        if(favorite == null || favorite == "") {
            qcssapp.Functions.displayError("There was an issue removing this product as a favorite");
            return;
        }

        var endpoint = qcssapp.Location + '/api/ordering/favorite/update';
        var dataParameters = JSON.stringify(favorite);

        if( qcssapp.Functions.checkServerVersion(2,1) ) {
            endpoint = qcssapp.Location + '/api/ordering/favorite/inactive/' + favorite;
            dataParameters = "";

            if( qcssapp.Functions.checkServerVersion(3,0) ) {
                endpoint = qcssapp.Location + '/api/favorite/product/inactive/' + favorite;
            }
        }

        qcssapp.Functions.callAPI(endpoint, 'POST', dataParameters, '',
            function(response) {
                if ( callbackFn && typeof callbackFn == 'function' ) {

                    //if on earlier back end version then the ordering favorite ID is returned, update model with ID
                    if( !qcssapp.Functions.checkServerVersion(2,1) && response > 1 && callbackParams.productView.productFavoriteModel.get('orderingFavoriteID') == 0) {
                        callbackParams.productView.productFavoriteModel.set('orderingFavoriteID', response);
                    }

                    callbackFn(callbackParams);
                }
            },
            function() {
                if (qcssapp.DebugMode) {
                    console.log("Error removing this product as a favorite.");
                }
            },
            '', '', '', true, true
        );
    };

    qcssapp.Functions.displayUnavailablePopup = function() {
        if( !((typeof qcssapp.ProductInProgress.unavailableModifiers != 'undefined' && qcssapp.ProductInProgress.unavailableModifiers != "") ||
            (typeof qcssapp.ProductInProgress.unavailableModPrepOptions != 'undefined' && qcssapp.ProductInProgress.unavailableModPrepOptions != "")) ) {
             return;
        }

        var customizationWord = 'customization';
        var optionWord = 'option';
        var verb = 'is';
        var modifierStr = "";
        if(typeof qcssapp.ProductInProgress.unavailableModifiers != 'undefined' && qcssapp.ProductInProgress.unavailableModifiers != "") {
            modifierStr = qcssapp.ProductInProgress.unavailableModifiers;

            if(qcssapp.ProductInProgress.unavailableModifiers.indexOf(',') != -1) {
                var modifierNames = qcssapp.ProductInProgress.unavailableModifiers.split(',');
                modifierStr = qcssapp.Functions.buildModifierString(modifierNames, '');
                customizationWord = 'customizations';
                verb = 'are';
            }

            delete qcssapp.ProductInProgress.unavailableModifiers;
        }

        var prepStr = "";
        if(typeof qcssapp.ProductInProgress.unavailableModPrepOptions != 'undefined' && qcssapp.ProductInProgress.unavailableModPrepOptions != "") {
            prepStr = qcssapp.ProductInProgress.unavailableModPrepOptions;

            if(qcssapp.ProductInProgress.unavailableModPrepOptions.indexOf(',') != -1) {
                var prepNames = qcssapp.ProductInProgress.unavailableModPrepOptions.split(',');
                prepStr = qcssapp.Functions.buildModifierString(prepNames, '');
                optionWord = 'options';
                verb = 'are';
            }

            delete qcssapp.ProductInProgress.unavailableModPrepOptions;
        }

        var message = '';
        if(modifierStr != "" && prepStr != "") {
            message = '<div class="unavailable-mods-popup">The set '+customizationWord+' </br><b>' + modifierStr + '</b></br> and prep ' + optionWord + ' </br><b> '+ prepStr +'</b></br> are currently unavailable for this product, please select manually.</div>';

        } else if (modifierStr != "" && prepStr == "") {
            message = '<div class="unavailable-mods-popup">The set '+customizationWord+' </br><b>' + modifierStr + '</b></br> ' + verb + ' currently unavailable for this product, please select manually.</div>';

        } else if (modifierStr == "" && prepStr != "") {
            message = '<div class="unavailable-mods-popup">The prep ' + optionWord + ' </br><b> '+ prepStr +'</b></br> ' + verb + ' currently unavailable for this product, please select manually.</div>';
        }

        $.confirm({
            title: '',
            content: message,
            buttons: {
                create: {
                    text: 'Close',
                    btnClass: 'btn-default unavailable-mods-btn prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                    action: function() {}
                }
            }
        });
    };

    qcssapp.Functions.buildModifierString = function(modifierName, productName) {
        if(productName != "") {
            $.each(modifierName, function(index) {
                productName += this;
                if(index+1 == modifierName.length - 1) {
                    productName += ' and ';
                } else if(index != modifierName.length - 1){
                    productName += ', ';
                }
            });
        } else {
            $.each(modifierName, function(index) {
                productName += '<b>'+ this + '</b>';
                if(index+1 == modifierName.length - 1) {
                    productName += ' and ';
                } else if(index != modifierName.length - 1){
                    productName += ', ';
                }
            });
        }
        return productName
    };

    //gets the current number of products in the cart, adds up all the quantities
    qcssapp.Functions.updateNumberOfProductsInCart = function() {
        var numProducts = 0;

        if ( qcssapp.CurrentOrder.products.length > 0 ) {
            $.each( qcssapp.CurrentOrder.products, function() {
                if(this.get('scaleUsed')) {
                    numProducts += 1;
                } else {
                    numProducts += Number(this.get('quantity'));
                }
            });
        }

        var $cartProductNum = $('.cart-product-num');
        if(numProducts % 1 != 0) {
            numProducts = numProducts.toFixed(2);
        }
        $cartProductNum.text(numProducts);
        return numProducts;
    };

    //******* FAVORITE VIEW EVENT HANDLER *******/

    //for updating an existing product and modifiers, replaces plus icon with check and sets the selected line
    $(document).on('click', '.mod-line', function(e) {
        e.stopPropagation();
        var $target = $(e.currentTarget);

        if(!qcssapp.usingBranding) {
            if(!$target.hasClass('selected-mod')) {

                $target.parent().find('.selected-mod').find('img').remove();
                $target.parent().find('.selected-mod').removeClass('selected-mod');
                $target.addClass('selected-mod').append('<img id="line-svg" class="svg loaded update-mod update-modifier" src="images/icon-check.svg">');
            } else {
                $target.removeClass('selected-mod');
                $target.find('img').remove();
            }
        } else {

            if(!$target.hasClass('selected-mod')) {
                $target.parent().find('.selected-mod').find('svg').remove();
                $target.parent().find('.selected-mod').removeClass('selected-mod');

                $target.addClass('selected-mod');
                $target.append('<img id="line-svg" class="svg loaded update-mod update-modifier" src="images/icon-check.svg" style="width:0">');
            } else {
                $target.removeClass('selected-mod');
                $target.find('svg').remove();
            }
            qcssapp.Functions.setSVGColors();

        }

        if($target.parent().find('.selected-mod').length > 0) {
            $('.existing-favorite-mods').parent().parent().css({'margin-bottom':'5px'});
            $('.update-exist-btn').css({'display':'inline-block'});
        } else {
            $('.update-exist-btn').css({'display':'none'});
        }
    });


    $(document).on('click', '#undo-favorite-remove', function(e) {
        var $element = $(this);
        var id = $element.attr('data-id');
        var $favoriteList = $('#favorite-list');
        var $lineContent = $favoriteList.hasClass('active-list') ? $favoriteList.find('.line-content[data-id="'+id+'"]') : $('#order-list').find('.line-content[data-id="'+id+'"]');

        if($lineContent.length == 0) {
            $lineContent = $favoriteList.hasClass('active-list') ? $('#favorites-unavailable').find('.line-content[data-id="'+id+'"]') : $('#order-unavailable-list').find('.line-content[data-id="'+id+'"]');
        }

        //hitting undo after a slide
        if( !$lineContent.parent().hasClass('favorite-line') ) {
            var $favoriteLine = $lineContent.parent().parent().parent().parent().parent();

            if($favoriteLine.length) {
                $favoriteLine.slick('slickGoTo', 1);
                $favoriteLine.removeClass('hide-slide');
                $favoriteLine.slick('unslick');
                $favoriteLine.find('.undo-remove-favorite').trigger('click');
            }
        } else {  //hitting undo after removing an unavailable favorite
            $lineContent.parent().removeClass('hide-slide');
            $lineContent.parent().find('.undo-remove-favorite').trigger('click');
        }

        $element.parent().find('.close-jq-toast-single').trigger('click');
    });


})(qcssapp, jQuery);