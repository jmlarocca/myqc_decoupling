;(function (qcssapp, $) {
    'use strict';

    //******* COMBO RELATED FUNCTIONS *******/

    qcssapp.Functions.checkProductCombos = function(completeCallback, completeCallbackParams) {
        if ( !qcssapp.Functions.checkServerVersion(3,0,0) ) {
            if ( typeof completeCallback == 'function' ) {
                completeCallback(completeCallbackParams);
            }
            return;
        }

        //create temporary order object to pass cart products to resource
        var orderObject = {
            "products": qcssapp.Functions.buildProductArray(),
            "combos": qcssapp.Functions.buildCombosArray()
        };

        //check if any combos are present on the returned transaction model
        qcssapp.Functions.orderInquiry(orderObject, qcssapp.Functions.adjustCartForCombos, '', '', completeCallback, completeCallbackParams);
    };

    qcssapp.Functions.adjustCartForCombos = function(response) {
        //no valid response, just return
        if ( response == null || response.length == 0 || response.loyaltyAccount == null ) {
            return;
        }

        if( typeof response.combos == 'undefined' ) {
            qcssapp.CurrentOrder.combos = '';
        }

        if ( response.combos && response.combos.length > 0 ) {
            //set combos collection
            qcssapp.CurrentOrder.combos = new qcssapp.Collections.Combo();
            qcssapp.CurrentOrder.combos.set(response.combos, {parse:response.combos});

            if ( response.products ) {
                qcssapp.Functions.setCurrentOrderProducts(response.products, qcssapp.CurrentOrder);
            }
        }

        qcssapp.Functions.checkComboProducts();
    };

    qcssapp.Functions.checkComboProducts = function() {

        //if this product was in a combo but is no longer in a combo, remove combo field values, there could be an error doing express reorder or order from the receipt and a product is no longer valid
        $.each(qcssapp.CurrentOrder.products, function() {
            if(this.get('comboTransLineItemId') != null && $.isEmptyObject(this.get('comboModel'))) {
                this.set('comboTransLineItemId', null);
                this.set('comboDetailId', null);
                this.set('comboPrice', null);
            }
        });
    };

    //format the returned products for qcssapp.CurrentOrder.products
    qcssapp.Functions.setCurrentOrderProducts = function(products, orderObject) {

        var productsInCart = [];
        $.each(products, function(index) {
            var productModel = new qcssapp.Models.Product(this);
            productModel.set('id', index);
            productModel.set('name', this.product.name);
            productModel.set('description', this.product.description);
            productModel.set('price', this.amount.toFixed(2));
            productModel.set('originalPrice', this.amount.toFixed(2));
            productModel.set('productID', this.product.id);

            //fix product name and set modifiers
            var that = this;
            $.each(orderObject.products, function() {
                qcssapp.Functions.setProductModel(productModel, that, this);
            });

            productsInCart.push(productModel);
        });

        var comboTransIndex = -1;

        //set the comboModel on the product model so you have the combo name and combo amount for the cart lines
        orderObject.combos.each( function( combo ) {

            var comboDetailsLength = combo.get('details').length;
            var foundDetails = 0;

            //find the combo detail for this product, set the comboModel on productModel
            combo.get('details').each(function(mod) {

                //loop over each product
                $.each(productsInCart, function() {

                    //if the product's comboDetailID matches the comboDetail ID and the product does not already have a comboModel set and the combo has not found all matching products to it's details
                    //if(this.get('comboDetailId') == mod.get('id') && $.isEmptyObject(this.get('comboModel')) ) {
                    if(this.get('comboDetailId') == mod.get('id') && $.isEmptyObject(this.get('comboModel')) && this.get('comboTransLineItemId') == comboTransIndex && foundDetails != comboDetailsLength) {

                        //set the total of all the upcharges in combo for cart line and review lines
                        if(this.get('upcharge') != "" && Number(this.get('upcharge')) > 0) {
                            if(typeof combo.get('upcharge') == 'undefined') {
                                combo.set('upcharge', Number(this.get('upcharge')));
                            } else {
                                var upcharge = combo.get('upcharge');
                                combo.set('upcharge', Number(upcharge) + Number(this.get('upcharge')));
                            }
                        }

                        this.set('comboModel', combo);
                        foundDetails ++;
                        return false;
                    }
                });
            }, this);

            comboTransIndex--;
        }, this);

        orderObject.products = productsInCart;

        if(qcssapp.cartBarView != null) {
            qcssapp.cartBarView.addProducts();
        }
    };

    // Set fields on combo product from CurrentOrder product
    qcssapp.Functions.setProductModel = function(productModel, comboProductModel, oldProductModel) {
        if(comboProductModel.product.id == oldProductModel.get('productID') && qcssapp.Functions.modifiersAreValid(comboProductModel, oldProductModel) && qcssapp.Functions.prepOptionsAreValid(comboProductModel, oldProductModel)) {
            productModel.set('name', oldProductModel.get('name'));
            productModel.get('product').name = oldProductModel.get('name');
            productModel.set('image', oldProductModel.get('image'));
            productModel.set('departmentID', oldProductModel.get('departmentID'));
            productModel.set('subDeptID', oldProductModel.get('subDeptID'));
            productModel.set('scaleUsed', oldProductModel.get('scaleUsed'));
            productModel.set('keypadID', oldProductModel.get('keypadID'));
            productModel.set('modSetDetailID', oldProductModel.get('modSetDetailID'));
            productModel.set('originalPrice', oldProductModel.get('originalPrice'));
            productModel.set('nutritionInfo', oldProductModel.get('nutritionInfo'));
            productModel.set('healthy', oldProductModel.get('healthy'));
            productModel.set('vegetarian', oldProductModel.get('vegetarian'));
            productModel.set('vegan', oldProductModel.get('vegan'));
            productModel.set('glutenFree', oldProductModel.get('glutenFree'));
            productModel.set('complementaryKeypadID', oldProductModel.get('complementaryKeypadID'));
            productModel.set('subDeptComplementaryKeypadID', oldProductModel.get('subDeptComplementaryKeypadID'));
            productModel.set('subDeptSimilarKeypadID', oldProductModel.get('subDeptSimilarKeypadID'));
            productModel.set('hasPrinterMappings', oldProductModel.get('hasPrinterMappings'));
            productModel.set('productInStore', oldProductModel.get('productInStore'));
            productModel.set('modifierMenus', oldProductModel.get('modifierMenus'));
            productModel.set('taxIDs', oldProductModel.get('taxIDs'));

            if(oldProductModel.attributes.hasOwnProperty('diningOptionProduct')) {
                productModel.set('diningOptionProduct', oldProductModel.get('diningOptionProduct'));
            }

            if(comboProductModel.hasOwnProperty('modifiers') && oldProductModel.get('modifiers').length > 0) {
                productModel.set('modifiers', oldProductModel.get('modifiers'));
                productModel.set('modifiersList', oldProductModel.get('modifiersList'));

                var modifiersTotal = 0;
                $.each(oldProductModel.get('modifiers'), function() {
                    modifiersTotal += Number(this.price);
                    if(this.prepOption != null && Number(this.prepOption.price) > 0) {
                        modifiersTotal += Number(this.prepOption.price);
                    }
                });
                productModel.set('price', Number(Number(productModel.get('price')) + modifiersTotal).toFixed(2));
            }

            if(typeof oldProductModel.get('prepOption') !== 'undefined' && !$.isEmptyObject(oldProductModel.get('prepOption'))) {
                productModel.set('prepOption', oldProductModel.get('prepOption'));

                if(typeof oldProductModel.get('prepOption') !== 'undefined' && typeof oldProductModel.get('prepOption').attributes !== 'undefined') {
                    productModel.set('prepOptionSetID', oldProductModel.get('prepOption').get('prepOptionSetID'));
                } else {
                    productModel.set('prepOptionSetID', oldProductModel.get('prepOption').prepOptionSetID);
                }
            }
        }
    };

    //compare the two product's modifiers to see if they are the same
    qcssapp.Functions.modifiersAreValid = function(product, currentProduct) {
        if(!product.hasOwnProperty('modifiers') && (currentProduct.get('modifiers') == null || currentProduct.get('modifiers').length == 0)) {
            return true;
        }

        if(product.hasOwnProperty('modifiers') && product.modifiers.length != currentProduct.get('modifiers').length) {
            return false;
        }

        var foundAllModifiers = true;
        $.each(product.modifiers, function() {
            var modID = this.id;
            if(typeof this.product != 'undefined') {
                modID = this.product.id;
            }

            var foundMod = false;
            $.each(currentProduct.get('modifiers'), function() {

                if(this.id == modID) {
                    foundMod = true;
                    return false;
                }
            });

            if(!foundMod) {
                foundAllModifiers = false;
                return false;
            }
        });

        return  foundAllModifiers;

    };

    //compare the two product's prep options to see if they are the same
    qcssapp.Functions.prepOptionsAreValid = function(product, currentProduct) {
        if(product['prepOption'] == null && $.isEmptyObject(currentProduct.get('prepOption'))) {
            return true;
        }

        if( !$.isEmptyObject(currentProduct.get('prepOption')) && product['prepOption'].id == currentProduct.get('prepOption').id && product['prepOption'].price == currentProduct.get('prepOption').price) {
            return true;
        }

        if( !$.isEmptyObject(currentProduct.get('prepOption')) && typeof currentProduct.get('prepOption').get('id') !== 'undefined' && product['prepOption'].id == currentProduct.get('prepOption').get('id') && typeof currentProduct.get('prepOption').get('price') !== 'undefined' && product['prepOption'].price == currentProduct.get('prepOption').get('price')) {
            return true;
        }

        return  false;

    };

    qcssapp.Functions.buildCombosArray = function() {
        //build product array
        var comboArr = [];

        if(qcssapp.CurrentOrder.combos == "") {
            return comboArr;
        }

        qcssapp.CurrentOrder.combos.each(function( mod ) {
            if(mod.get('id') === "") {
                return;
            }

            var comboModel = mod.get('combo');
            var comboObj = {
                "id": comboModel.id,
                "quantity": mod.get('quantity'),
                "combo": comboModel,
                "itemId": comboModel.id,
                "amount": mod.get('amount')
            };

            comboArr.push(comboObj);
        }, this);

        return comboArr;
    };

    //substitute the product in the combo for another product on the same keypad of the combo product
    qcssapp.Functions.substituteProduct = function(productID) {
        //check if the substitute product is set
        if($.isEmptyObject(qcssapp.ProductInProgress.substituteProduct)) {
            qcssapp.Functions.displayError('There was an issue while trying to substitute ' + qcssapp.ProductInProgress.name + '. Please return to the cart and try again.');
            return;
        }

        var oldProduct = qcssapp.ProductInProgress.substituteProduct;

        //if selected the same product with same modifiers, navigate to the cart
        if(oldProduct.get('productID') == qcssapp.ProductInProgress.id && qcssapp.Functions.modifiersAreValid(qcssapp.ProductInProgress, oldProduct)) {
            qcssapp.Router.navigate('cart', {trigger: true});

        } else {
            //used for substituting the combo at the exact index in the cart
            qcssapp.ProductInProgress.cartProductID = oldProduct.id;

            qcssapp.Functions.loadProductComboKeypad(productID);
        }
    };

    //maintain the order of the cart by replacing the old product at it's exact index in the cart
    qcssapp.Functions.substituteComboInCart = function(productModel) {
        if(qcssapp.Functions.checkNegativePricingOnSubstitute(productModel)) {
            qcssapp.Functions.displayPopup('Cannot add product to the combo. Product does not have the required pricing.', 'Invalid Price', 'OK');
            return;
        }

        if(qcssapp.Functions.checkWeightedProduct(productModel)) {
            qcssapp.Functions.displayPopup('Cannot add weighted products to a combo.', 'Invalid Product', 'OK');
            return;
        }

        // update details for swapped product
        productModel.set('id', qcssapp.ProductInProgress.substituteProduct.get('id'));
        productModel.set('quantity', qcssapp.ProductInProgress.substituteProduct.get('quantity'));
        productModel.set('comboTransLineItemId', qcssapp.ProductInProgress.substituteProduct.get('comboTransLineItemId'));

        //remove the old product from the cart
        if ( productModel.get('id') >= 0 ) {
            qcssapp.CurrentOrder.products.splice(productModel.get('id'), 1, productModel);

            qcssapp.OrderMenuHistory.pop();
        }

        //reset ProductInProgress object
        qcssapp.Functions.clearCurrentModifiers();

        //navigate to the cart after checking combos
        var addCallback = function() {
            qcssapp.Functions.navigateToCart();
        };

        //check that combos are still accurate
        if ( qcssapp.Functions.updateNumberOfProductsInCart() > 1 ) {
            qcssapp.Functions.checkProductCombos(addCallback);
        }
    };

    //call for keypad details, render new keypad view
    qcssapp.Functions.loadCombo = function(id, index, editObj, calledFromCart) {
        var endPoint = qcssapp.Location + '/api/ordering/combo/' + id;

        qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
            function(response) {
                if(response) {

                    if(response.hasOwnProperty('comboDetails')) {
                        var comboName = qcssapp.CurrentCombo.name;
                        qcssapp.CurrentCombo = response;
                        qcssapp.CurrentCombo.name = comboName;
                        qcssapp.CurrentCombo.nextDetailIndex = index;
                        if(calledFromCart!=undefined){
                            qcssapp.CurrentCombo.editingFromCart = calledFromCart; //added for case where we need to edit from the cart vs when we are not in it (update vs add to cart)
                        }
                        else{qcssapp.CurrentCombo.editingFromCart = false; }

                        if(editObj) {
                            qcssapp.CurrentCombo.editObj = editObj;
                        }

                        qcssapp.Functions.loadComboKeypad();
                    }
                }
            }
        );
    };

    qcssapp.Functions.rebuildComboObject = function(id, editObj, successFunction, calledFromCart){
        var endPoint = qcssapp.Location + '/api/ordering/combo/' + id;

        qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
            function(response) {
                if(response) {

                    if(response.hasOwnProperty('comboDetails')) {
                        var comboName = response.name;
                        qcssapp.CurrentCombo = response;
                        qcssapp.CurrentCombo.name = comboName;
                        if(calledFromCart!=undefined){
                            qcssapp.CurrentCombo.editingFromCart = calledFromCart; //added for case where we need to edit from the cart vs when we are not in it (update vs add to cart)
                        }
                        else{qcssapp.CurrentCombo.editingFromCart = false; }

                        if(editObj) {
                            qcssapp.CurrentCombo.editObj = editObj;
                        }
                    }

                    if(typeof successFunction == "function"){
                        successFunction();
                    }

                }
            }
        );
    };

    //show the next combo keypad in the list
    qcssapp.Functions.loadComboKeypad = function() {
        var index = qcssapp.CurrentCombo.nextDetailIndex;

        //check if at the end of the combo details (also make sure that the index is defined before moving forward)
        if(index != qcssapp.CurrentCombo.comboDetails.length && index!= undefined) {
            var keypad = qcssapp.CurrentCombo.comboDetails[index].keypadId;

            qcssapp.Functions.loadKeypad(keypad, undefined, undefined, undefined, undefined, true);

            qcssapp.CurrentCombo.nextDetailIndex++;
            return;
        }

        qcssapp.Functions.loadComboView();
    };

    qcssapp.Functions.loadComboView = function() {
        qcssapp.Functions.showMask();

        //Defect 3870: Need to pass a value to control the comboview element
        qcssapp.Functions.resetDynamicViewEvents("loadComboView");

        qcssapp.Functions.addHistoryRecord(qcssapp.CurrentCombo.id, 'combo');

        //create new combo view
        new qcssapp.Views.ComboView();

        //go to new combo view
        qcssapp.Router.navigate('combo', {trigger: true});
    };

    //reset the global combo objects
    qcssapp.Functions.resetCombos = function() {
        qcssapp.ComboInProgress = [];

        qcssapp.CurrentCombo = {
            id: "",
            name: "",
            paDiscountId: "",
            comboDetails: [],
            comboDetailKeypadIds: [],
            nextDetailIndex: 0,
            swapIndex: null,
            updatedComboTransLineId: null,
            editObj: false
        };
    };

    qcssapp.Functions.checkNegativePricing = function(productModel) {
        var indexToBeAdded = qcssapp.ComboInProgress.length;

        if(qcssapp.CurrentCombo.swapIndex != null) {
            indexToBeAdded = qcssapp.CurrentCombo.swapIndex;
        }

        if(qcssapp.CurrentCombo.comboDetails.length && qcssapp.CurrentCombo.comboDetails[indexToBeAdded]) {
            var comboDetail = qcssapp.CurrentCombo.comboDetails[indexToBeAdded];
            var basePrice = Number(comboDetail.basePrice);
            var comboPrice = Number(comboDetail.comboPrice);
            var productPrice = Number(productModel.get('price'));

            if(((productPrice - basePrice) + comboPrice) < 0) {
                return true;
            }
        }

        return false;
    };

    qcssapp.Functions.checkNegativePricingOnSubstitute = function(productModel) {
        var substituteProduct = qcssapp.ProductInProgress.substituteProduct;

        if(substituteProduct) {
            var basePrice = Number(substituteProduct.get('basePrice'));
            var comboPrice = Number(substituteProduct.get('comboPrice'));
            var productPrice = Number(productModel.get('price'));

            if(((productPrice - basePrice) + comboPrice) < 0) {
                return true;
            }
        }

        return false;
    };

    qcssapp.Functions.checkWeightedProduct = function(productModel) {
        return productModel.get('scaleUsed');
    };

    // Format combo upcharge on keypad line
    qcssapp.Functions.formatComboUpcharge = function(model) {
        var formattedUpcharge = "";

        var comboKeypadIndex = qcssapp.CurrentCombo.swapIndex == null ? qcssapp.ComboInProgress.length : qcssapp.CurrentCombo.swapIndex;
        var comboDetail = qcssapp.CurrentCombo.comboDetails[comboKeypadIndex];

        if(typeof comboDetail !== 'undefined') {
            var basePrice = Number(comboDetail.basePrice);
            var upcharge = Number(Number(model.get('price')) - basePrice);

            if(upcharge == 0) {
                formattedUpcharge = "";
            } else if (upcharge < 0) {
                upcharge = Number(upcharge * -1);
                formattedUpcharge = qcssapp.Functions.formatPrice(upcharge, '-');
            } else {
                formattedUpcharge = qcssapp.Functions.formatPrice(upcharge, '+');
            }
        }

        return formattedUpcharge
    };

    // Verify product is valid in a combo
    qcssapp.Functions.verifyProductInCombo = function(productId){
        var productInCombo = false;

        if(qcssapp.CurrentCombo && qcssapp.CurrentCombo.comboDetails && qcssapp.CurrentCombo.comboDetails.length){
            for(var i = 0; i < qcssapp.CurrentCombo.comboDetails.length; i++){
                var comboDetail = qcssapp.CurrentCombo.comboDetails[i];

                if(comboDetail && comboDetail.keypadDetailPAPluIds && comboDetail.keypadDetailPAPluIds.length){
                    for(var x = 0; x < comboDetail.keypadDetailPAPluIds.length; x++){
                        var keypadProductId = comboDetail.keypadDetailPAPluIds[x];

                        if(Number(keypadProductId) == Number(productId)){
                            return true;
                        }
                    }
                }
            }

            return productInCombo;
        }

        return productInCombo;
    };

    // Build the ComboInProgress object for the given combo index
    qcssapp.Functions.buildComboInProgress = function(comboIndex) {
        qcssapp.ComboInProgress = [];
        $.each(qcssapp.CurrentOrder.products, function() {
            if (!$.isEmptyObject(this.get("comboModel")) && this.get("comboModel").get("id") == comboIndex) {
                qcssapp.ComboInProgress.push(this);
            }
        });
    };

    // Remove the combo with the given index from the cart
    qcssapp.Functions.removeComboFromCart = function(comboIndex) {
        qcssapp.CurrentOrder.combos.models.splice(comboIndex, 1);
        qcssapp.CurrentOrder.combos.length--;

        qcssapp.CurrentOrder.combos.each(function(combo, index) {
            combo.id = index;
            combo.set("id", index);
        });

        var removedComboTransLineItemId = qcssapp.ComboInProgress[0].get("comboTransLineItemId");

        // Remove each product in the combo from the cart
        $.each(qcssapp.ComboInProgress, function() {
            var index = qcssapp.Functions.getProductCartIndexByID(this.id);
            if (index >= 0) {
                qcssapp.CurrentOrder.products.splice(index, 1);
            }
            qcssapp.Functions.updateNumberOfProductsInCart();
        });

        $.each(qcssapp.CurrentOrder.products, function(index) {
            this.id = index;
            this.set("id", index);
            if (this.get("comboTransLineItemId") < removedComboTransLineItemId) {
                this.set("comboTransLineItemId", this.get("comboTransLineItemId") + 1);
            }
        });
    };

    // If the given product is part of a combo, remove the combo from the CurrentOrder
    qcssapp.Functions.removeComboByProduct = function(productModel) {
        if (typeof productModel.get("comboModel") !== "undefined" && !$.isEmptyObject(productModel.get("comboModel"))) {
            var comboID = productModel.get("comboModel").id;
            var comboModel = new qcssapp.Models.Combo;
            qcssapp.CurrentOrder.combos.models.splice(comboID, 1, comboModel);

            // Remove combo details on products that were in that combo
            $.each(qcssapp.CurrentOrder.products, function() {
                if (typeof this.get("comboModel") !== "undefined" && !$.isEmptyObject(this.get("comboModel")) && this.get("comboModel").id == comboID) {
                    this.set("comboModel", "");
                    this.set("comboTransLineItemId", "");
                    this.set("comboDetailId", "");
                    this.set("upcharge", "");
                }
            });
        }
    };

})(qcssapp, jQuery);