;(function (qcssapp, $) {
    'use strict';

    //******* STORE / SCHEDULE RELATED FUNCTIONS *******/

    qcssapp.Functions.setStoreSettings = function() {
        qcssapp.Functions.buildCurrentOrderToken();

        qcssapp.Functions.checkStoreItemScanning();
    };

    qcssapp.Functions.getStoreSetting = function(settingName, defaultValue) {
        if(qcssapp.CurrentOrder && qcssapp.CurrentOrder.storeModel && typeof qcssapp.CurrentOrder.storeModel.get(settingName) !== 'undefined' ) {
            return qcssapp.CurrentOrder.storeModel.get(settingName);
        }

        return defaultValue;
    };

    qcssapp.Functions.getStorePaymentSetting = function(settingName, defaultValue) {
        if(qcssapp.CurrentOrder && qcssapp.CurrentOrder.storePaymentModel && typeof qcssapp.CurrentOrder.storePaymentModel.get(settingName) !== 'undefined' ) {
            return qcssapp.CurrentOrder.storePaymentModel.get(settingName);
        }

        return defaultValue;
    };

    //returns true if user can order pickup from the store right now or at a given time based on given store model
    qcssapp.Functions.checkStoreOpenForPickUp = function(mod, now) {
        if ( !now ) {
            now = moment();
        }

        var available = false;
        if ( mod.attributes.pickUpOrderingOpenTime.length > 0 && mod.attributes.pickUpOrderingCloseTime.length > 0 && moment(mod.attributes.pickUpOrderingOpenTime, "hh:mm A", true).isValid() && moment(mod.attributes.pickUpOrderingCloseTime, "hh:mm A", true).isValid() && mod.attributes.pickUpOpenTimes.length > 0 ) {
            //create date objects based on open and closed times
            var pickupOpenTime = moment(mod.attributes.pickUpOrderingOpenTime, "hh:mm A");
            var pickupCloseTime = moment(mod.attributes.pickUpOrderingCloseTime, "hh:mm A");

            //if between open and closed times, set available flag
            if ( pickupOpenTime.diff(now) < 0 && now.diff(pickupCloseTime) < 0 ) {
                available = true;
            }
        }
        return available;
    };

    //returns true if user can order delivery from the store right now or at a given time based on given store model
    qcssapp.Functions.checkStoreOpenForDelivery = function(mod, now) {
        if ( !now ) {
            now = moment();
        }

        var available = false;
        if ( mod.attributes.deliveryOrderingOpenTime.length > 0 && mod.attributes.deliveryOrderingCloseTime.length > 0 && moment(mod.attributes.deliveryOrderingOpenTime, "hh:mm A", true).isValid() && moment(mod.attributes.deliveryOrderingCloseTime, "hh:mm A", true).isValid() && mod.attributes.deliveryOpenTimes.length > 0 ) {
            //create date objects based on open and closed times
            var deliveryOpenTime = moment(mod.attributes.deliveryOrderingOpenTime, "hh:mm A");
            var deliveryCloseTime = moment(mod.attributes.deliveryOrderingCloseTime, "hh:mm A");

            //if between open and closed times, set available flag
            if ( deliveryOpenTime.diff(now) < 0 && now.diff(deliveryCloseTime) < 0 ) {
                available = true;
            }
        }
        return available;
    };

    // Returns true if the use of a credit card is allowed for both the terminal and the account group of the user
    qcssapp.Functions.isCreditCardAllowed = function() {
        var terminalAllowed = qcssapp.Functions.getStoreSetting("usesCreditCardAsTender", false);
        var accountAllowed = qcssapp.Functions.getStorePaymentSetting("allowPayAsYouGo", true);
        return terminalAllowed && accountAllowed;
    };

    //returns a TimePicker object based on the store id and order type
    qcssapp.Functions.createTimePicker = function(storeID, orderType, successFunction, successParameters) {
        //default to pickup if an orderType hasn't been chosen yet
        if(orderType == "") {
            orderType = "notChosen"
        }

        var verb = 'GET';
        var dateParameters = '';
        var endPoint = qcssapp.Location + '/api/ordering/buildTimePicker/' + storeID + '/' + orderType;

        if(qcssapp.futureOrderDate) {
            verb = 'POST';
            endPoint = qcssapp.Location + '/api/ordering/buildTimePicker';
            dateParameters = JSON.stringify({storeID:storeID, orderType: orderType, futureOrderDate:qcssapp.futureOrderDate});
        }

        qcssapp.Functions.callAPI(endPoint, verb, dateParameters, '',
            function(data) {
                var timeObj = data.timePicker;

                //going from cart-view to receive-view
                if(successParameters == '') {
                    if(timeObj.openTimes.length == 0) {
                        if(window.location.hash == "#suggestive-selling") {
                            qcssapp.Functions.addHistoryRecord(0, 'cart');
                            qcssapp.Router.navigate('cart', {trigger: true});
                        }

                        qcssapp.Functions.displayError("We're sorry this store is not currently accepting orders");
                        $('#orderTime').val("");
                    } else {
                        $("#orderTime").prop('disabled', false);
                        successFunction(timeObj);
                    }
                } else {  //changing the orderType to delivery or pickup in the receive view
                    successFunction(timeObj, successParameters);
                }
            },
            function(errorParameters) {
                errorParameters.view.fetchError()
            },
            '',
            '',
            '',
            true,
            true
        );
    };

    //call for keypad details, render new keypad view
    qcssapp.Functions.loadKeypad = function(id, back, callback, callbackSuccessParameters, substituteProduct, removeOld) {
        var endPoint = qcssapp.Location + '/api/ordering/menu/' + id;
        var successParameters = {id: id, back: back, callback: callback,
            callbackSuccessParameters: callbackSuccessParameters, removeOld: removeOld};
        var verb = "GET";

        if(qcssapp.Functions.checkServerVersion(3,0)) {
            verb = "POST";
        }

        //if substituting a product in a combo, check that all products are available in the combo detail's keypad
        if(typeof substituteProduct !== 'undefined' && !$.isEmptyObject(substituteProduct)) {
            successParameters.substituteProduct = substituteProduct;
        }

        qcssapp.Functions.callAPI(endPoint, verb, '', successParameters,
            function(response, successParameters) {
                if ( response.errorDetails && response.errorDetails.length > 0 ) {
                    qcssapp.Functions.displayError(response.errorDetails, 'Warning');
                    return;
                }

                //check for valid response
                if ( Number(response.id) > 0 ) {

                    //only add history if not going back
                    if ( !successParameters.back ) {
                        var keypadLabel = qcssapp.CurrentCombo.id != "" ? 'combo-keypad' : 'keypad';

                        qcssapp.Functions.addHistoryRecord(successParameters.id, keypadLabel);
                    }

                    //remove old event listeners
                    qcssapp.Functions.resetDynamicViewEvents();

                    //remove modifiers from history if navigating to a regular keypad
                    if ( qcssapp.OrderMenuHistory.length > 0 ) {
                        qcssapp.Functions.purgeModifierHistory();
                    }

                    //reset the current changed product parameters object
                    qcssapp.Functions.clearCurrentModifiers();

                    if(successParameters.substituteProduct) {
                        response.substituteProduct = successParameters.substituteProduct;
                    }

                    //create new KP view
                    var oldKeypadItems = $('.keypad-list').children();
                    var keypadView = new qcssapp.Views.KeypadView(response);

                    // If only one item on combo keypad, automatically select it
                    if (qcssapp.CurrentCombo.id != "" && response.items.length == 1) {
                        qcssapp.OrderMenuHistory.pop();
                        var lastKeypadIndex = keypadView.$el.find('.keypad-list').length-1;
                        keypadView.$el.find('.keypad-list').eq(lastKeypadIndex).find('.keypad-line').eq(0).trigger('click');
                        return;
                    }

                    if (successParameters.removeOld) {
                        oldKeypadItems.remove();
                    }

                    //url path
                    var navigatePath = 'keypad/' + response.id;

                    //go to new KP view
                    qcssapp.Router.navigate(navigatePath, {trigger: true});

                    keypadView.adjustKeypad();

                    // Show error is no products on keypad
                    if (qcssapp.CurrentCombo.id != "" && response.items.length == 0) {
                        qcssapp.Functions.displayError('No items were found on this menu! Check Combo Configuration.');
                    } else if (response.items.length == 0) {
                        qcssapp.Functions.displayError('The items on the menu cannot be shown due to configuration. Use the back button to go back.');
                    }

                    if ( successParameters.callback && typeof successParameters.callback == 'function' ) {
                        successParameters.callback(successParameters.callbackSuccessParameters);
                    }
                } else {
                    qcssapp.Functions.displayError('There was an error loading the menu for the store, invalid data. Please try again');
                }
            }
        );
    };

    qcssapp.Functions.formatFullKeypadLines = function() {
        $.each($('.keypad-page').find('.keypad-name-full'), function() {
            if( Number($(this).outerWidth() / $('.keypad-page').outerWidth()) > 0.68 ) {
                $(this).offsetParent().find('.keypad-icons-full-mobile').css('max-width', '28%');
            }
        });
    };

    //sets the name of the day for ordering for future days
    qcssapp.Functions.updateOrderingDayName = function() {
        var $orderView = $('#orderview');
        var $orderHeader = $orderView.find('h1');
        var $homeLink = $('#orderview .home-link');

        $orderHeader.html('Ordering for Today');

        if($orderView.width() < 415) {
            $orderHeader.addClass('store-header-shrink');
            $homeLink.addClass('home-icon-shrink');
        }

        if(qcssapp.futureOrderDate == "") {
            $orderHeader.removeClass('store-header-shrink');
            $homeLink.removeClass('home-icon-shrink');
            return;
        }

        var futureOrderDate = new Date(qcssapp.futureOrderDate);
        var dateToday = new Date();
        dateToday.setHours(0);
        dateToday.setMinutes(0);
        dateToday.setSeconds(0);

        var dayDiff = Math.round((futureOrderDate - dateToday)/(1000*60*60*24));
        if(dayDiff > 6) {
            var date = 'Ordering for '+qcssapp.Functions.formatFutureDate();
            $orderHeader.html(date);
        } else {
            var day = 'Ordering for '+qcssapp.Functions.formatFutureDay();
            $orderHeader.html(day);
        }

    };

    qcssapp.Functions.formatFutureDate = function() {
        var dateFormatted = qcssapp.futureOrderDate;
        var date = new Date(dateFormatted);
        return (date.getMonth() + 1) + '/' + date.getDate();
    };

    qcssapp.Functions.formatFutureDay = function() {
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var futureOrderDate = new Date(qcssapp.futureOrderDate);
        return days[futureOrderDate.getDay()];
    };

    //set the number in the calendar icon based on today's date or the future order date
    qcssapp.Functions.updateCalendarIconNum = function() {
        var $calendarNum = $('.calendar-num');

        var date = new Date();
        var day = date.getUTCDate();

        if(qcssapp.futureOrderDate != "") {
            date = new Date(qcssapp.futureOrderDate);
            day =  date.getUTCDate();
        }

        $calendarNum.text(day);
    };

    qcssapp.Functions.checkStoreItemScanning = function() {
        var $itemScanIcon = $('.scan-barcode-btn');
        $itemScanIcon.addClass('hidden');

        if(!qcssapp.Functions.checkServerVersion(4,0,58) || !qcssapp.onPhoneApp) {
            return;
        }

        if(qcssapp.CurrentOrder && qcssapp.CurrentOrder.storeModel && qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('allowItemScan') && qcssapp.CurrentOrder.storeModel.get('allowItemScan')) {
            $itemScanIcon.removeClass('hidden');
        }
    };

    // Sets the header for the Select Store page
    qcssapp.Functions.setSelectStoreHeader = function(data) {
        if(!qcssapp.Functions.checkServerVersion(4,0,50)) {
            return;
        }

        if ( data.hasOwnProperty('selectStoreHeader') && data['selectStoreHeader'] != null ) {
            $('#show-order').attr('data-text',data['selectStoreHeader']);
        }
    };

    // Refreshes the Cart page when a product is scanned and added to the Cart
    qcssapp.Functions.resetCart = function() {
        qcssapp.Functions.showMask();

        qcssapp.Functions.purgeModifierHistory();

        qcssapp.Functions.clearCurrentModifiers();

        qcssapp.Functions.resetCombos();

        qcssapp.Functions.resetDynamicViewEvents();

        new qcssapp.Views.CartView();

        qcssapp.Functions.executeStandardRoute('#show-cart', '#cartview');

        qcssapp.CurrentOrder.selectedRewards = [];
        qcssapp.CurrentOrder.automaticRewards = [];
        qcssapp.CurrentOrder.useMealPlanDiscount = true;
        qcssapp.CurrentOrder.expressReorder = false;

        $('.keypad-page').add('.modifier-page').remove();
    };

    // Determines if the Order Details page should be skipped based on configuration settings on the MyQC Terminal
    qcssapp.Functions.skipOrderDetails = function() {
        if(!(qcssapp.CurrentOrder.storeModel && qcssapp.CurrentOrder.storeModel.attributes)) {
            return false;
        }

        var orderType = qcssapp.CurrentOrder.orderType;
        if(orderType == "notChosen" || orderType == "") {
            orderType = "pickup";
        }

        var forceASAP = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('forceASAP') ? qcssapp.CurrentOrder.storeModel.get('forceASAP') : false;
        var showPhoneNumberInput = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('showPhoneNumberInput') ? qcssapp.CurrentOrder.storeModel.get('showPhoneNumberInput') : true;
        var showCommentInput = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('showCommentInput') ? qcssapp.CurrentOrder.storeModel.get('showCommentInput') : true;
        var showDeliveryInput = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('showDeliveryInput') ? qcssapp.CurrentOrder.storeModel.get('showDeliveryInput') : true;

        if(qcssapp.CurrentOrder.grabAndGo) {
            forceASAP = true;
        }

        if(orderType == 'pickup' && forceASAP && !showPhoneNumberInput && !showCommentInput && !qcssapp.Functions.isCreditCardAllowed()) {
            return true;

        } else if(orderType == 'delivery' && forceASAP && !showPhoneNumberInput && !showCommentInput && !qcssapp.Functions.isCreditCardAllowed()) {
            return true;
        }

        return false;
    };

    qcssapp.Functions.getStoreLocations = function(successCallback, errorCallback){
        return qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/orgLevels','GET','',{},
            function(result, successParameters) {
                if (qcssapp.DebugMode) {
                    console.log("Retrieved location filter settings :");
                    console.log(result);
                }


                //result set should contain many details, only concerned about name, number and type
                if ( !result ) {
                    qcssapp.Functions.throwFatalError("An error occurred while loading account details, please log in again",'login');
                    return;
                }


                if(typeof successCallback == "function"){
                    successCallback(result);
                }

            },
            function() {
                if (qcssapp.DebugMode) {
                    console.log("Error loading account details");
                }

                if(typeof errorCallback == "function"){
                    errorCallback();
                }

                qcssapp.Functions.throwFatalError("An error occurred while loading account details, please log in again",'login');
            },
            '',
            '',
            '',
            true,
            false,
            false
        );
    };

    //********* IMAGE FUNCTIONS ***************//

    qcssapp.Functions.determineTemplate = function(imagePositionID, image) {
        var templateID = '';
        switch ( imagePositionID ) {
            case ("2"):
                templateID = 'keypad-line-template-right';
                break;
            case ("3"):
                templateID = 'keypad-line-template-bottom';
                break;
            case ("4"):
                templateID = 'keypad-line-template-left';
                break;
            case ("1"):
                templateID = 'keypad-line-template-center';
                break;
            case ("5"):
                templateID = 'keypad-line-template-center';
                break;
            case ("6"):
                templateID = 'keypad-line-template-full';
                break;
        }

        if(image == "" || imagePositionID == "" || imagePositionID == '0' || imagePositionID == '7') { //if IconPosition is null, Top, 0, or Hidden
            templateID = 'keypad-line-template';
        }
        return templateID
    };


})(qcssapp, jQuery);