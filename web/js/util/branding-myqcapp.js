;(function (qcssapp, $) {
    'use strict';

    //******* BRANDING RELATED FUNCTIONS *******/

    //determines if the app is using branding styles or not
    qcssapp.Functions.determineBranding = function(data) {
        if( !data || !data.brandingDetails || !(data.brandingDetails.MYQCPROGRAMNAME || data.brandingDetails.myQCProgramName) || !qcssapp.Functions.checkServerVersion(1,4)) {
            if(qcssapp.usingBranding) {
                qcssapp.usingBranding = false;
                qcssapp.Functions.removeBrandingIfApplied();
            }
            return;
        }
        qcssapp.usingBranding = true;

        qcssapp.Functions.getMyQCFeatures();

        qcssapp.Functions.setBrandingGlobals(data.brandingDetails);
    };

    //sets the branding details global object
    qcssapp.Functions.setBrandingGlobals = function(brandingDetails) {
        var primaryColor = '#'+ ( brandingDetails.MYQCPRIMARYCOLOR || brandingDetails.myQCPrimaryColor );
        var primaryFontColor = '#'+ ( brandingDetails.MYQCFONTCOLORONPRIMARY || brandingDetails.myQCFontColorOnPrimary );
        var secondaryColor = '#'+ ( brandingDetails.MYQCSECONDARYCOLOR || brandingDetails.myQCSecondaryColor );
        var secondaryFontColor = '#'+ ( brandingDetails.MYQCFONTCOLORONSECONDARY || brandingDetails.myQCFontColorOnSecondary );
        var tertiaryColor = '#'+ ( brandingDetails.MYQCTERTIARYCOLOR || brandingDetails.myQCTertiaryColor );
        var tertiaryFontColor = '#'+ ( brandingDetails.MYQCFONTCOLORONTERTIARY || brandingDetails.myQCFontColorOnTertiary );
        var accentColorOne = '#'+ ( brandingDetails.MYQCACCENTCOLORONE || brandingDetails.myQCAccentColorOne );
        var accentColorTwo = '#'+ ( brandingDetails.MYQCACCENTCOLORTWO || brandingDetails.myQCAccentColorTwo );
        var tokenizedLogo = ( brandingDetails.MYQCTOKENIZEDPROGRAMLOGO || brandingDetails.myQCTokenizedProgramLogo );

        //if the program name is blank set the Login header text as 'Login' and the tour text as 'This app'
        var loginText = brandingDetails.MYQCPROGRAMNAME || brandingDetails.myQCProgramName;

        //if there is no logo image set the src path as null
        var programLogoPath = qcssapp.qcBaseURL + '/webimages/'+ tokenizedLogo;
        if ( qcssapp.onPhoneApp ) {   // Cordova API detected
            var hostName = qcssapp.Functions.get_hostname(qcssapp.Location); //can't have /myqc in the url
            programLogoPath = hostName + '/webimages/'+ tokenizedLogo;
        }
        if( tokenizedLogo == '') {
            programLogoPath = '';
            $('.logo-container, .login-logo-container, .tour-logo-container').attr('hidden', true);
            $('#user-information').addClass('user-information-iphoneX');
            $('.tour-title').text('Welcome');
            $("#login-header h1").text(loginText);
            $("#login-page").attr('data-title', loginText);
        } else {
            $('#login-page').css('padding-top','10%');
            $('.logo-container, .login-logo-container, .tour-logo-container').attr('hidden', false);
            $('.tour-title').text('Welcome To');
            $("#login-header h1").text('Login');
            $("#login-page").attr('data-title', 'Login');
            $('#user-information').removeClass('user-information-iphoneX');
        }

        $("#logout-header h1").text(loginText);
        $('.logo-container img').attr('src', programLogoPath);
        $('.login-logo-container img').attr('src', programLogoPath);
        $('.tour-logo-container img').attr('src', programLogoPath);
        $('.tour-svg').css('background', primaryColor);
        $('.tour-healthy-icon-container').css('background', primaryColor);

        //FOR TESTING
//        $('.logo-container img').attr('src', 'https://qc90devmsdn.mmhayes.com/webimages/c21a6296fcc142ba9959174f6f9b3348.png');
//        $('.login-logo-container img').attr('src', 'https://qc90devmsdn.mmhayes.com/webimages/c21a6296fcc142ba9959174f6f9b3348.png');
//        $('.tour-logo-container img').attr('src', 'https://qc90devmsdn.mmhayes.com/webimages/c21a6296fcc142ba9959174f6f9b3348.png');

        //prominent button gradients
        var linearGradientX = qcssapp.Functions.colorLuminance(primaryColor,0.03);
        var linearGradientY = qcssapp.Functions.colorLuminance(primaryColor,-0.13);

        //unfreeze button gradient
        var linearGradientXSec = qcssapp.Functions.colorLuminance(secondaryColor,0.03);
        var linearGradientYSec = qcssapp.Functions.colorLuminance(secondaryColor,-0.13);

        //loader styles
        var outerCircle = qcssapp.Functions.colorLuminance(primaryColor,-0.3);
        var innerCircle = qcssapp.Functions.colorLuminance(primaryColor,0.3);

        //set the colors for the main classes, user info subtitle on nav menu, header colors, checkbox colors, and modifier buttons
        var style = $('<style>' +
            '.primary-background-color {background-color: '+ primaryColor +' !important}' +
            ' .font-color-primary {color: '+ primaryFontColor +' !important}' +
            ' .secondary-background-color {background-color: '+ secondaryColor +' !important}' +
            ' .font-color-secondary {color: '+ secondaryFontColor +' !important}' +
            ' .tertiary-background-color {background-color: '+ tertiaryColor +' !important}' +
            ' .font-color-tertiary {color: '+ tertiaryFontColor +' !important}' +
            ' .accent-color-one {color: '+ accentColorOne +' !important}' +
            ' .accent-color-two {color: '+ accentColorTwo +' !important}' +
            ' .primary-button-font {color: '+ primaryColor +' !important}' +
            ' .primary-gradient-color {background-image: linear-gradient('+linearGradientX+ ',' + linearGradientY + ') !important}' +
            ' .primary-border-color {border-color: '+qcssapp.Functions.colorLuminance(primaryColor,-0.28)+' !important}' +
            ' .secondary-gradient-color {background-image: linear-gradient('+linearGradientXSec+ ',' + linearGradientYSec + ') !important}' +
            ' .secondary-border-color {border-color: '+qcssapp.Functions.colorLuminance(secondaryColor,-0.28)+' !important}' +
            ' .font-color-primary-gradient {color: '+qcssapp.Functions.colorLuminance(primaryFontColor,-0.09)+' !important}' +
            ' .user-information-title {color: '+ primaryFontColor +' !important}' +
            ' .orderMethodBtn.selected, .filterBtn.selected, .receiveBtn.selected {background-color: '+ secondaryColor +' !important}' +
            ' #myqcapp header h1 {color: '+ primaryFontColor +' !important}' +
            ' #myqcapp header {background: '+ primaryColor +' !important}' +
            ' input[type="checkbox"]:checked + label::before {background-color: '+ primaryColor +' !important; color: '+ primaryFontColor +' !important;}' +
            ' header .home-link {color: '+ primaryFontColor +' !important}' +
            ' .mod-circle {border: 2px solid '+tertiaryColor+'}' +
            ' #plus-icon, #minus-icon {border: 1px solid'+primaryFontColor+'}' +
            ' #myQuickPicks {border-bottom: 2px solid '+accentColorTwo+'; border-top: 2px solid '+accentColorTwo+'}' +
            '.cart-product-num, .plus-cart {color: '+primaryColor+';}' +
            ' .modifier-line-icon {background: none; padding-top: 18px;}' +
            ' .line-modifier.selected .modifier-line-icon {background: none; padding-top: 18px;}' +
            ' #express-reorders {border: 2px solid '+primaryColor+';}'+
            ' .datepicker--cell.-current-, .datepicker--cell.-current-.-focus- {color: '+primaryColor+'} .datepicker--day-name {color: '+secondaryColor+'}' +
            ' .datepicker--cell.-selected-, .datepicker--cell.-selected-.-current- {color: '+primaryFontColor+';background-color: '+primaryColor+'}' +
            ' .secondary-font-border-color {border-color: '+secondaryFontColor+';}'+
            ' .primary-font-background-color {color: '+primaryColor+' !important}'+
            ' .primary-border-background-color {border-color: '+primaryColor+' !important}'+
            ' .tertiary-font-background-color {color: '+tertiaryColor+' !important}'+
            ' .tertiary-border-color {border-color: '+tertiaryColor+' !important}'+
            ' .combo-swap:hover {background-color: '+accentColorTwo+' !important; color:#fff !important;}'+
            ' .accent-color-two-background {background-color: '+accentColorTwo+' !important;}'+
            ' .accent-color-one-background {background-color: '+accentColorOne+' !important;}'+
            ' .accent-color-one-border {border-color: '+accentColorOne+' !important;}'+
            ' .prep-selected {background-color: '+tertiaryColor+' !important; color: '+tertiaryFontColor+' !important;}'+
            ' .prep-selected-product {background-color: '+primaryColor+' !important; color: '+primaryFontColor+' !important;}'+
            ' #loader {border-top-color: '+outerCircle+' !important; border-left-color: '+outerCircle+' !important;} #loader:before {border-top-color: '+primaryColor+' !important; border-left-color: '+primaryColor+' !important;} #loader:after{border-top-color: '+innerCircle+' !important; border-left-color: '+innerCircle+' !important;}' +
            ' #express-arrow-left {border-right: 16px solid '+primaryColor+' !important;} #express-arrow-right {border-left: 16px solid '+primaryColor+' !important;}' +
            ' .help-image {filter: drop-shadow(0px 0px 0px '+primaryColor+') !important;}' +
            ' .selectric-donation_charity-select .selectric { border-bottom: 2px solid '+accentColorTwo+' !important;}' +
            ' .selectric-donation_charity-select .button { border-top: 3px solid '+accentColorTwo+' !important; border-right: 3px solid '+accentColorTwo+' !important;}' +
            ' .modifier_prep-option-preview-arrow {border-top: 5px solid '+accentColorOne+' !important;}' +
            '@media only screen and (min-device-width: 415px) {.modifier-line-icon {padding:15px 0 0 5px} #express-arrow-left {border-right: 30px solid '+primaryColor+' !important;} #express-arrow-right {border-left: 30px solid '+primaryColor+' !important;} .plus-cart {margin-top:12px;} }' +
            '@media only screen and (min-device-width: 615px) {.modifier-line-icon {padding:18px 0 0 20px}}' +
            '</style>').appendTo($('head'));

        qcssapp.Functions.updateElementsForBranding();

        qcssapp.Functions.setSVGColors();

    };

    qcssapp.Functions.get_hostname = function(url) {
        if(qcssapp.qcBaseURL != '') {
            return qcssapp.qcBaseURL;
        }

        var m = url.match(/^http[s]?:\/\/[^/]+/);
        return m ? m[0] : null;
    };

    //certain elements need to be changed for branding
    qcssapp.Functions.updateElementsForBranding = function() {
        //override the SSO link colors and change padding of logo container on nav menu
        $('#logoutLink, #logoutMyQCLink').css('color','inherit');
        $('.logo-container').css('padding','2% 3% 0');
        $('.login-logo-container img').css({width:'auto', 'max-width':'80%', 'max-height':'110px'});
        $('.logo-container img').css({width:'auto', 'max-width':'80%', 'max-height':'135px'});

        //hide the original tour, show the branding tour, update styles for new pages
        $('.original-tour, .order-back, .order-cart, .freeze-og').attr('hidden', true);
        $('.branding-tour, .order-back-svg, .order-cart-svg, .freeze-svg').attr('hidden', false);
        $('.tour-navigation-item').attr('hidden', false);
        $('.tour-navigation-list').css({width:'80%', display:'flex', 'justify-content':'space-between'});
        $('.tour-navigation-prev, .tour-navigation-next').css('width', '10%');
        $('.tour-logo-bottom').css('display', 'inline');
        $('#tour-footer').addClass('tour-foot-iphoneX');
        $('#og-overview-tour').attr('src', 'images/tour-overview.svg');
        $('.tour-logo-container').css('margin-top', '10px');

        //show the 'powered by quickcharge' footer, hide the mmhayes footer
        $('.main-footer, .powered-by-logo').css({display: 'flex', 'justify-content': 'center'});
        $('.powered-by-logo, .main-footer').attr('hidden', false);
        $('.mmhayes-logo').attr('hidden', true);

        //if seeing the tour first, update 'Get Started' button styles and tour footer styles
        if($('#getStartedBtn').is(":visible") && qcssapp.usingBranding) {
            $('.tour-footer').css('height', '170px');
            $('#tour-logo-get-started-container').css({position: 'absolute', bottom: '55%', left:'15%', right:'15%'});
        }

        //reset svg colors if branding is already applied
        qcssapp.Functions.resetSVGs();
    };

    qcssapp.Functions.resetSVGs = function() {

        //reset svg colors if branding is already applied
        $('.freeze-svg').remove();
        $('.freeze-btn-icon-container').append('<img class="freeze-btn-icon svg freeze-svg font-color-primary" src="images/button-lock.svg" alt=""/>');

        if($('.order-back-icon svg').length) {
            $('.order-back-icon svg').remove();
            $('.order-back-icon').append('<img class="svg order-back-svg font-color-primary" src="images/icon-back.svg">');
        }

        if($('.general-back-icon svg').length) {
            $('.general-back-icon svg').remove();
            $('.general-back-icon').append('<img class="svg order-back-svg font-color-primary" src="images/icon-back.svg">');
        }

        if($('.order-cart-icon svg').length) {
            $('.order-cart-icon svg').remove();
            $('.order-cart-icon').append('<img class="svg order-cart-svg font-color-primary" src="images/icon-cart.svg">');
        }

        if($('.help-icon svg').length) {
            $('.help-icon svg').remove();
            $('.help-icon').append('<img class="help-image svg help-icon-svg font-color-primary" src="images/icon-help-button.svg">');
        }

        if($('#express-reorder-btn svg').length) {
            $('#express-reorder-btn svg').remove();
        }
    };

    //if going from a code with branding to a code without branding, have to remove branding styles because they were appended to the head
    qcssapp.Functions.removeBrandingIfApplied = function() {

        var style = $('<style>' +
            '.primary-background-color {background-color: #ef4135 !important}' +
            ' .font-color-primary {color: white !important}' +
            ' .secondary-background-color {background-color: #133d8d !important}' +
            ' .font-color-secondary {color: white !important}' +
            ' .tertiary-background-color {background-color: #2eb941 !important}' +
            ' .font-color-tertiary {color: white !important}' +
            ' .accent-color-one {color: #ef4135 !important}' +
            ' .accent-color-two {color: #133d8d !important}' +
            ' .primary-button-font {color:  #ef4135 !important}' +
            ' .primary-gradient-color {background-image: linear-gradient(#f44336, #c62828) !important}' +
            ' .primary-border-color {border-color: #b71c1c !important}' +
            ' .secondary-gradient-color {background-image: linear-gradient(#133d8d, #133d8d) !important}' +
            ' .secondary-border-color {border-color: #133d8d !important}' +
            ' .font-color-primary-gradient {color:  #fbcfcc !important}' +
            ' .user-information-title {color: white !important}' +
            ' #myqcapp header h1 {color: white !important}' +
            ' #myqcapp header {background: #ef4135 !important}' +
            ' input[type="checkbox"]:checked + label::before {background-color: #ef4135 !important;color:#FFF}' +
            ' header .home-link {color: white !important}' +
            ' .orderMethodBtn.selected, .filterBtn.selected, .receiveBtn.selected {background-color: #133d8d !important}' +
            ' #myQuickPicks {border-bottom: 2px solid #133d8d; border-top: 2px solid #133d8d}' +
            '.cart-product-num {color: #ef4135;}' +
            ' .mod-circle {border: 2px solid #2eb941}' +
            ' .modifier-line-icon {background: url("images/icon-add.svg") center center no-repeat; padding-top: 0px;background-size:60%}' +
            ' .line-modifier.selected .modifier-line-icon {background: url("images/icon-check.svg") center center no-repeat; padding-top: 0px;background-size:60%}' +
            ' #express-reorders {border: 2px solid #ef4135;}'+
            ' .datepicker--cell.-current-, .datepicker--cell.-current-.-focus- {color: #ef4135;} .datepicker--day-name {color: #133d8d;}' +
            ' .datepicker--cell.-selected-, .datepicker--cell.-selected-.-current- {color: #fff; background-color: #ef4135;}' +
            ' .primary-font-background-color {color: #ef4135 !important}'+
            ' .primary-border-background-color {border-color: #ef4135 !important}'+
            ' .prep-selected {background-color: #2eb941; !important; color: #fff}'+
            ' .prep-selected-product {background-color: #ef4135; !important; color: #fff}'+
            ' .tertiary-font-background-color {color: #2eb941 !important}'+
            ' .tertiary-border-color {border-color: #2eb941 !important}'+
            ' .combo-swap:hover {background-color: #133d8d !important; color:#fff;}'+
            ' .accent-color-two-background {background-color: #133d8d;}'+
            ' .accent-color-one-background {background-color: #ef4135;}'+
            ' .accent-color-one-border {border-color: #ef4135;}'+
            ' #loader {border-top-color: #bd0024; border-left-color: #bd0024;} #loader:before {border-top-color: #e0022c; border-left-color: #e0022c;} #loader:after{border-top-color: #ff0030; border-left-color: #ff0030;}' +
            ' #express-arrow-left {border-right: 16px solid #d02419 !important;} #express-arrow-right {border-left: 16px solid #d02419 !important;}' +
            ' .help-image {filter: drop-shadow(0px 0px 0px #fff);}' +
            ' .modifier_prep-option-preview-arrow {border-top: 5px solid #ef4135;}' +
            '@media only screen and (min-device-width: 415px) {.modifier-line-icon {background-size:40%;padding:0 0 0 20px} #express-arrow-left {border-right: 30px solid #d02419;} #express-arrow-right {border-left: 30px solid #d02419;}}' +
            '@media only screen and (min-device-width: 415px) {.modifier-line.selected .modifier-line-icon {background-size:40%;}}' +
            '@media only screen and (min-device-width: 615px) {.modifier-line-icon {padding:0 0 0 20px}}' +
            '</style>').appendTo($('head'));

        //change back header text, logo image on nav menu, get rid of logo on login page
        $("#login-header h1").text('My Quickcharge');
        $("#logout-header h1").text('My Quickcharge');
        $("#login-page").attr('data-title', 'My Quickcharge');
        $('.logo-container img').attr('src', 'images/logo-myqc.svg');
        $('.login-logo-container').attr('hidden', true);
        $('.login-logo-container img').css({width:'60%', 'max-width':'350px', 'max-height':''});

        //change back the logo container, hide powered by quickcharge footer
        $('.logo-container').attr('hidden', false);
        $('.logo-container').css('padding','3% 5% 0');
        $('.logo-container img').css({width:'100%','max-width':'350px'});
        $('#logoutLink, #logoutMyQCLink').css('color','#ef4135');
        $('.powered-by-logo, .main-footer').attr('hidden', true);
        $('.main-footer, .powered-by-logo').css({display: '', 'justify-content': ''});
        $('#user-information').removeClass('user-information-iphoneX');

        //change back tour text, main tour logo, hide branding tour, show original tour and mmhayes-logo footer, update styles
        $(".tour-text-0").text('Quickcharge allows you to make cashless purchases within your organization.');
        $(".subtext-note").text('Use the arrows at the bottom of the page to explore the features of Quickcharge.');
        $('.tour-logo-container img').attr('src', 'images/logo-myqc.svg');
        $('.tour-svg').attr('background', 'none');
        $('.original-tour, .order-back, .order-cart, .freeze-og').attr('hidden', false);
        $('.branding-tour, .order-back-svg, .order-cart-svg, .freeze-svg').attr('hidden', true);
        $('.mmhayes-logo').attr('hidden', false);
        $('.tour-navigation-list').css({width:'50%', display:'', 'justify-content':''});
        $('.tour-navigation-prev, .tour-navigation-next').css('width', '25%');
        $('#og-overview-tour').attr('src', 'images/tour-red-overview.svg');
        $('.tour-logo-bottom').css('display', 'none');
        $('.tour-footer').css('height', '120px');
        $('#tour-logo-get-started-container').css({position: '', bottom: '', left:'', right:''});
        $('.tour-logo-container').css('margin-top', '20px');
        $('#tour-page-2').addClass('tour-panel'); //add original panels back to list
        $('#tour-page-3').addClass('tour-panel');
        $('.tour-title').text('Welcome To');
        $('.freeze-btn-icon-container svg').remove();

        for(var i=4;i< $('.tour-panel').length; i++) {  //hide extra tour navigation dots that are used for branding
            $('.tour-navigation-item').eq(i).attr('hidden', true);
        }
        $('.tour-navigation-item').eq(3).attr('hidden',false);

        qcssapp.Functions.resetSVGs();
    };

    //changes the fill of the svg colors
    qcssapp.Functions.setSVGColors = function() {
        if(qcssapp.usingBranding) {
            $('img.svg').each(function(){
                var $img = $(this);
                var imgID = $img.attr('id');
                var imgClass = $img.attr('class');
                var imgURL = $img.attr('src');
                var imgColor = $img.attr('data-color');

                $.get(imgURL, function(data) {
                    var color = "";
                    var $svg = $(data).find('svg');

                    // Add replaced image's ID to the new SVG
                    if(typeof imgID !== 'undefined') {
                        $svg = $svg.attr('id', imgID);
                    }
                    // Add replaced image's classes to the new SVG
                    if(typeof imgClass !== 'undefined') {
                        $svg = $svg.attr('class', imgClass+' replaced-svg');
                    }

                    //get the color from the class name or depending on the url
                    if($img.hasClass('font-color-primary')) {
                        color = $('.font-color-primary').css('color');
                    } else if ($img.hasClass('primary-background-color') ||  imgURL == 'images/icon-add.svg' || $img.hasClass('primary-color') || $img.hasClass('primary-font-background-color')) {
                        color = $('.primary-background-color').css('background-color');
                    }  else if ($img.hasClass('') || imgURL == 'images/icon-check.svg') {
                        color = $('.tertiary-background-color').css('background-color') ;
                    } else if ($img.hasClass('font-color-secondary')) {
                        color = $('.font-color-secondary').css('color');
                    }

                    if ($img.hasClass('update-mod') && imgURL == 'images/icon-check.svg') {
                        $svg.css({'padding-bottom':'62px', 'display':'block', 'width':'18px'});
                    }

                    if ($img.hasClass('calendar-svg') || $img.hasClass('help-icon-svg') ) {
                        $svg.attr('fill', color);
                    }

                    var $path = $(data).find('path');
                    var $circle = $(data).find('circle');
                    var $g = $(data).find('g');

                    if(typeof $path.attr('fill') !== 'undefined') {
                        $path.attr('fill', color);
                    }

                    if(typeof $circle.attr('fill') !== 'undefined') {
                        $circle.attr('fill', color);
                    }

                    if($img.hasClass('scan-barcode-img')) {
                        $g.attr('fill', color);
                    }

                    if($img.hasClass('add-to-cart-svg')) {
                        $svg.css('filter', 'drop-shadow(3px 3px 2px rgba(0,0,0,0.2))');
                    }

                    if($img.hasClass('express-cart-svg')) {
                        $svg.css('filter', 'drop-shadow(3px 3px 2px rgba(0,0,0,0.2))');
                    }

                    // Replace image with new SVG
                    $img.replaceWith($svg);

                }, 'xml');
            });
        }

    };

    //given a hex value and luminosity percent, this function calculates the new hex value for prominent button styles
    qcssapp.Functions.colorLuminance = function(hex, lum) {
        // validate hex string
        hex = (hex).replace(/[^0-9a-f]/gi, '');
        if (hex.length < 6) {
            hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
        }
        lum = lum || 0;
        var hexDupe = hex;
        // convert to decimal and change luminosity
        var rgb = "#", i;
        for (i = 0; i < 3; i++) {
            var c;
            if(i==0) {
                c = parseInt(hexDupe.substring(0,2), 16);
            } else if(i==1) {
                c = parseInt(hexDupe.substring(2,4), 16);
            } else if(i==2) {
                c = parseInt(hexDupe.substring(4,6), 16);
            }
            c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
            rgb += ("00"+c).substring(c.length);
        }

        return rgb;
    };

    //*********** STYLE RELATED FUNCTIONS ***********/

    // Set the global tokenized filenames for the healthy indicator icons
    qcssapp.Functions.setHealthyIndicatorIcons = function(data) {
        qcssapp.healthyIndicator.wellness = 'images/icon-healthy.svg';
        qcssapp.healthyIndicator.vegetarian = 'images/icon-vegetarian.svg';
        qcssapp.healthyIndicator.vegan = 'images/icon-vegan.svg';
        qcssapp.healthyIndicator.glutenFree = 'images/icon-glutenfree.svg';


        if( !qcssapp.Functions.checkServerVersion(4,0) ) {
            return;
        }

        var hostName = qcssapp.qcBaseURL;
        if ( qcssapp.onPhoneApp ) {   // Cordova API detected
            hostName = qcssapp.Functions.get_hostname(qcssapp.Location); //can't have /myqc in the url
        }

        qcssapp.healthyIndicator.wellness = data.hasOwnProperty('wellnessTokenizedFilename') && data['wellnessTokenizedFilename'] != '' ? hostName + '/webimages/' + data['wellnessTokenizedFilename'] : 'images/icon-healthy.svg';
        qcssapp.healthyIndicator.vegetarian = data.hasOwnProperty('vegetarianTokenizedFilename') && data['vegetarianTokenizedFilename'] != '' ? hostName + '/webimages/' + data['vegetarianTokenizedFilename'] : 'images/icon-vegetarian.svg';
        qcssapp.healthyIndicator.vegan = data.hasOwnProperty('veganTokenizedFilename') && data['veganTokenizedFilename'] != '' ? hostName + '/webimages/' + data['veganTokenizedFilename'] : 'images/icon-vegan.svg';
        qcssapp.healthyIndicator.glutenFree = data.hasOwnProperty('glutenFreeTokenizedFilename') && data['glutenFreeTokenizedFilename'] != '' ? hostName + '/webimages/' + data['glutenFreeTokenizedFilename'] : 'images/icon-glutenfree.svg';

        if( !qcssapp.Functions.checkServerVersion(4,1,0) ) {
            return;
        }

        qcssapp.healthyIndicator.wellnessLabel = data.hasOwnProperty('wellnessLabel') && data['wellnessLabel'] != '' ? qcssapp.Functions.htmlDecode(data['wellnessLabel']) : 'Healthy';
        qcssapp.healthyIndicator.vegetarianLabel = data.hasOwnProperty('vegetarianLabel') && data['vegetarianLabel'] != '' ? qcssapp.Functions.htmlDecode(data['vegetarianLabel']) : 'Vegetarian';
        qcssapp.healthyIndicator.veganLabel = data.hasOwnProperty('veganLabel') && data['veganLabel'] != '' ? qcssapp.Functions.htmlDecode(data['veganLabel']) : 'Vegan';
        qcssapp.healthyIndicator.glutenFreeLabel = data.hasOwnProperty('glutenFreeLabel') && data['glutenFreeLabel'] != '' ? qcssapp.Functions.htmlDecode(data['glutenFreeLabel']) : 'Gluten Free';

        //TODO: grace update this for version number
        if( !qcssapp.Functions.checkServerVersion(4,1,1) ) {
            return;
        }

        qcssapp.nutrition.nutritionCategory1 = data.hasOwnProperty('nutritionCategory1') && data['nutritionCategory1'] != null ? data.nutritionCategory1 : null;
        qcssapp.nutrition.nutritionCategory2 = data.hasOwnProperty('nutritionCategory2') && data['nutritionCategory2'] != null ? data.nutritionCategory2 : null;
        qcssapp.nutrition.nutritionCategory3 = data.hasOwnProperty('nutritionCategory3') && data['nutritionCategory3'] != null ? data.nutritionCategory3 : null;
        qcssapp.nutrition.nutritionCategory4 = data.hasOwnProperty('nutritionCategory4') && data['nutritionCategory4'] != null ? data.nutritionCategory4 : null;
        qcssapp.nutrition.nutritionCategory5 = data.hasOwnProperty('nutritionCategory5') && data['nutritionCategory5'] != null ? data.nutritionCategory5 : null;
    };


})(qcssapp, jQuery);