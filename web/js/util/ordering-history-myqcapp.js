;(function (qcssapp, $) {
    'use strict';

    //******* ORDERING HISTORY RELATED FUNCTIONS *******/

    //add object to order history record
    qcssapp.Functions.addHistoryRecord = function(id, type, options) {
        //only a keypad should be the first record in history, after picking the store
        if ( qcssapp.OrderMenuHistory.length == 0 && !qcssapp.Functions.isNumeric(qcssapp.StoreSelected) ) {
            if ( qcssapp.DebugMode ) {
                console.log('attempted to add history record id/type: '+ id + '/' + type + ', but failed because the store has not been set, and the order history was empty. First history record must always be a home keypad');
            }
            return false;
        }

        //also record options
        if ( options ) {
            //for products, two different screens possible, edit or add. Edit uses cartProductID, Add uses PAPLUID
            qcssapp.OrderMenuHistory.push({id: id, type: type, edit: options.edit});
        } else {
            qcssapp.OrderMenuHistory.push({id: id, type: type, edit: false});
        }

        if ( qcssapp.DebugMode ) {
            console.log('Added history record id/type: '+ id + '/' + type);
        }
    };

    //pop the current page off of the history record, and return the new last object in the history record
    qcssapp.Functions.getHistoryRecord = function() {
        if ( qcssapp.DebugMode ) {
            console.log('Order menu history is: ');
            console.log(qcssapp.OrderMenuHistory);
            console.log('Getting history record at position: ' + (qcssapp.OrderMenuHistory.length-1));
            console.log(qcssapp.OrderMenuHistory[qcssapp.OrderMenuHistory.length-1]);
        }

        if ( qcssapp.OrderMenuHistory.length == 0 ) {
            return false;
        }

        var storeKeypad = qcssapp.OrderMenuHistory[0];
        var currentPage = qcssapp.OrderMenuHistory[qcssapp.OrderMenuHistory.length-1];

        if ( !qcssapp.CurrentOrder.expressReorder && (qcssapp.OrderMenuHistory.length == 1 || (storeKeypad.hasOwnProperty('id') && currentPage.hasOwnProperty('id') && currentPage.hasOwnProperty('type') && currentPage.type == 'keypad' && storeKeypad.id == currentPage.id)) ) {
            if ( qcssapp.CurrentOrder.products.length > 0 ) {
                //confirm want to clear order
                var message = "Going back to the store list will reset your cart. Are you sure you want to empty your cart?";

                qcssapp.Functions.displayPopup(message, 'Warning', 'Close', '', '', 'Empty Cart', qcssapp.Functions.resetOrdering, true);

            } else {
                qcssapp.Functions.resetOrdering(true);
            }

            //if on the combo view, go back to the original keypad before the combo
        } else if ( qcssapp.OrderMenuHistory.length > 1 && currentPage.type == 'combo' ) {
            qcssapp.OrderMenuHistory.pop();

            var found = null;
            var length = qcssapp.OrderMenuHistory.length-1;
            for (var i=length; i>=0; i--) {
                var type = qcssapp.OrderMenuHistory[i].type;
                if(type != 'keypad' && type != 'cart') {
                    //pop the current page off
                    qcssapp.OrderMenuHistory.pop();
                } else {
                    found = i;
                    break;
                }
            }

            if(found != null) {
                return qcssapp.OrderMenuHistory[found];
            }

        } else if ( qcssapp.OrderMenuHistory.length > 1 ) {
            var index = qcssapp.OrderMenuHistory.length-2;

            //pop the current page off
            qcssapp.OrderMenuHistory.pop();

            //if on the cart page and the last page was a product with modifiers, pop off product page to go back to most recent keypad
            if(qcssapp.OrderMenuHistory[index].type == 'product' && currentPage.type == 'cart') {
                $.each(qcssapp.CurrentOrder.products, function() {
                    if(this.get('productID') == qcssapp.OrderMenuHistory[index].id && this.get('modSetDetailID') > 0) {
                        //pop the product page off
                        index = qcssapp.OrderMenuHistory.length-2;
                        qcssapp.OrderMenuHistory.pop();

                        return false;
                    }
                });
            }

            //return the new last record in the history (new current page)
            return qcssapp.OrderMenuHistory[index];
        }
    };

    //clear the order history record
    qcssapp.Functions.clearHistory = function() {
        qcssapp.OrderMenuHistory = [];
    };

    //removes all history records for modifier pages and deals with duplicate history entries as a result
    qcssapp.Functions.purgeModifierHistory = function() {
        if ( qcssapp.OrderMenuHistory.length > 0 ) {
            for ( var i = qcssapp.OrderMenuHistory.length - 1; i > 0; i-- ) {
                if ( qcssapp.OrderMenuHistory[i].type == "modifier" ) {
                    qcssapp.OrderMenuHistory.splice(i,1);
                    continue;
                }

                if ( (Number(qcssapp.OrderMenuHistory[i].id) == Number(qcssapp.OrderMenuHistory[i-1]) && qcssapp.OrderMenuHistory[i].type == qcssapp.OrderMenuHistory[i-1].type) || ( qcssapp.OrderMenuHistory[i+1] && Number(qcssapp.OrderMenuHistory[i].id) == Number(qcssapp.OrderMenuHistory[i+1].id) && qcssapp.OrderMenuHistory[i].type == qcssapp.OrderMenuHistory[i+1].type ) ) {
                    qcssapp.OrderMenuHistory.splice(i,1);
                }
            }
        }
    };

    //resets every ordering property and brings user back to the store selection
    qcssapp.Functions.resetOrdering = function(goToView, filterDefault) {

        //reset history
        qcssapp.Functions.clearHistory();

        //clear current order
        qcssapp.Functions.emptyCart();

        //reset the number of products in the cart
        qcssapp.Functions.updateNumberOfProductsInCart();

        //reset order object
        qcssapp.orderObject = {};

        //clear store selected & ajax header
        qcssapp.Functions.setSession(qcssapp.Session, qcssapp.Location, '', qcssapp.UserCode);

        if(filterDefault) {
            //reset ordering filter selector
            $('.orderMethodPickup').trigger('click');
        }

        if ( goToView ) {
            $('#show-order').off().find('.template-gen').remove();
            new qcssapp.Views.OrderStoreView();
            qcssapp.Router.navigate('show-order', {trigger: true});
        }
    };

    //reset cart
    qcssapp.Functions.emptyCart = function() {
        qcssapp.CurrentOrder = {
            "products": [],
            "combos": '',
            "orderType": "",
            "orderLocation": "",
            "orderTime": "",
            "orderComments": "",
            "storeModel": "",
            "rewards": [],
            "selectedRewards": [],
            "automaticRewards": [],
            "useMealPlanDiscount": true,
            "expressReorder": false,
            "expressReorderModel": "",
            "suggestedKeypadIDs" : [],
            "storePaymentModel":null,
            "token": "",
            "grabAndGo": false,
            "voucherCode": "",
            "voucherTotal": 0
        };
    };

    qcssapp.Functions.navigateToCart = function() {
        qcssapp.Functions.addHistoryRecord(0, 'cart');

        qcssapp.Router.navigate('cart', {trigger: true});
    };

    //reset ordering page view events for regenerating pages
    qcssapp.Functions.resetDynamicViewEvents = function(curSrc) {
        $('#keypadview').off();
        $('#suggestive-keypadview').off();
        $('#productview').off();
        $('#cartList').off();
        $('#modifierview').off();
        $('#transaction-list').off();
        $('#accountFunding').off();
        $('#favoriteview').off();

        //Defect 3870: Turning off the comboview element was disabling events when navigating
        //             back from a swap function (either browser or in-app back function)
        if(curSrc && curSrc != "loadComboView") {
            $('#comboview').off();
        }
    };

    //******* ORDERING EVENT HANDLER *******/

    //handle click of back icon in online ordering
    $(document).on('click', '.order-back-icon', function() {
        var curObj = qcssapp.OrderMenuHistory[qcssapp.OrderMenuHistory.length-1];
        var prevObj = qcssapp.Functions.getHistoryRecord();
        console.log("Previous Object: ", prevObj);

        //if the last view was the suggestive view, get the next to last view
        if (prevObj && prevObj.hasOwnProperty('type') && prevObj.type == "suggestive") {
            prevObj = qcssapp.Functions.getHistoryRecord();
        }

        if ( qcssapp.DebugMode ) {
            console.log('Order back clicked');
            console.log(prevObj);
        }
        $('.scroll-indicator').css("bottom", "");

        //if the first product is a dining option product, remove it so it does not appear in the cart
        if(window.location.hash == '#review' && qcssapp.CurrentOrder.products[0].get('diningOptionProduct')) {
            qcssapp.CurrentOrder.products = qcssapp.CurrentOrder.products.slice(1);
            qcssapp.Functions.updateNumberOfProductsInCart();
        }

        //go to cart if in express reorder
        if(qcssapp.CurrentOrder.expressReorder && !qcssapp.Functions.skipOrderDetails()) {
            qcssapp.CurrentOrder.expressReorder = false;
            qcssapp.Functions.buildReceivePage();
            return;
        } else if (qcssapp.CurrentOrder.expressReorder) {
            qcssapp.CurrentOrder.expressReorder = false;
            qcssapp.Router.navigate('cart', {trigger: true});
            return;
        }

        if ( prevObj && Number(prevObj.id) >= 0 && prevObj.type) {
            var type = prevObj.type;
            var id = prevObj.id;

            qcssapp.Functions.resetDynamicViewEvents();

            if (type == "keypad" || type == "store" ) {
                //if exiting combo view completely, reset combo objects
                if(qcssapp.CurrentCombo.id != "") {
                    qcssapp.Functions.resetCombos();
                }
                qcssapp.Functions.loadKeypad(id, true, undefined, undefined, qcssapp.ProductInProgress.substituteProduct);
            } else if (type == "cart") {
                qcssapp.Router.navigate('cart', {trigger: true});
            } else if (type == "redeem") {
                qcssapp.Router.navigate('redeem', {trigger: true});
            } else if (type == "receive") {
                qcssapp.Router.navigate('show-receive', {trigger: true});
            } else if(type == "favorites") {
                $('#my-quick-picks-page').off();
                var data = {fromOrderBack: true};
                new qcssapp.Views.MyQuickPicksView(data);
                qcssapp.Router.navigate('favorites', {trigger: true});
            } else if (type == "favorite-order") {
                qcssapp.Router.navigate('favorite-order', {trigger: true});
            } else if (type == "store-list") {
                qcssapp.Router.navigate('show-order', {trigger: true});
            } else if (type == "combo-keypad") {
                if(qcssapp.CurrentCombo.id != "" && $.inArray(id, qcssapp.CurrentCombo.comboDetailKeypadIds) != -1) {
                    qcssapp.CurrentCombo.nextDetailIndex = $.inArray(id, qcssapp.CurrentCombo.comboDetailKeypadIds) + 1;
                    qcssapp.ComboInProgress.pop();
                }
                qcssapp.Functions.loadKeypad(id, true);
            } else if (type == "combo") {

                var success = function(){
                    // Rebuild the Combo in Progress
                    $.each(qcssapp.CurrentOrder.products, function() {
                        if( !$.isEmptyObject(this.get('comboModel'))) {
                            var isComboActive = this.get('comboModel').get('comboActive');
                            if(isComboActive!=undefined && isComboActive!=null){
                                qcssapp.ComboInProgress.push(this); //not pushing it in if it's not part of the combo (Defect 3939 Addendum, so items not in current combo in progress don't get added into that list)
                            }
                        }
                    });

                    var comboQuantity = $('#combo-quantity').val();
                    qcssapp.Functions.loadComboView();
                    $('#combo-quantity').val(comboQuantity).trigger('change');
                };

                //Defect 3895: Case - User just added combo to order/cart, then hits the back button (this was causing an error).
                //Will now act as if the user clicked the combo that they just added as a line item, allowing them to edit it. Further clicks of back will function normally
                if(curObj.type=="cart" && (qcssapp.ComboInProgress.length == 0 ||qcssapp.ComboInProgress.length ==undefined)){
                    if(qcssapp.CurrentOrder.combos.models){
                        var comboIndex = qcssapp.CurrentOrder.combos.models.length - 1;
                        var comboModel = qcssapp.CurrentOrder.combos.models[comboIndex];

                        qcssapp.ComboInProgress = [];
                        qcssapp.CurrentCombo.name = comboModel.get('name');

                        $.each(qcssapp.CurrentOrder.products, function() {
                            if( !$.isEmptyObject(this.get('comboModel')) && this.get('comboModel').get('id') == comboIndex ) {
                                qcssapp.ComboInProgress.push(this);
                            }
                        });

                        qcssapp.Functions.loadCombo(comboModel.get('comboId'), qcssapp.ComboInProgress.length, comboModel, true);
                    }
                    return;
                }

                //Defect 3939: Change made to handle case where combo has not been added to CurrentOrder (the cart) yet
                //If there is a current combo being formed and the user chooses to swap, then hit the back button, navigates the user back to combo details
                var comboModel;
                if(qcssapp.CurrentOrder.combos != ""){
                    comboModel = qcssapp.CurrentOrder.combos.where({comboId:id})[0];
                }
                else{
                    qcssapp.Router.navigate('combo', {trigger: true}); //if nothing has been put into the order yet, just navigate back to details
                    return;
                }
                // Rebuild Current Combo
                qcssapp.Functions.rebuildComboObject(id, comboModel, success , qcssapp.CurrentCombo.editingFromCart);

            }
        }
    });

    //click cart icon activates cart route, brings user to cart
    $(document).on('click', '.order-cart-icon', function() {
        handleCartClick();
        qcssapp.Router.navigate('cart', {trigger: true});
    });

    function handleCartClick() {
        var currentPage = qcssapp.OrderMenuHistory[qcssapp.OrderMenuHistory.length-1];

        if(currentPage['type'] == "review") {
            //pop off review
            qcssapp.OrderMenuHistory.pop();

            if( !qcssapp.CurrentOrder.expressReorder ) {
                //pop off receive
                qcssapp.OrderMenuHistory.pop();

                //pop off cart
                if (qcssapp.OrderMenuHistory[qcssapp.OrderMenuHistory.length-1].type !== "keypad") {
                    qcssapp.OrderMenuHistory.pop();
                }

            } else if (qcssapp.OrderMenuHistory[qcssapp.OrderMenuHistory.length-1].type == "redeem") {
                //pop off redeem reward
                qcssapp.OrderMenuHistory.pop();
            }
        }
    }


})(qcssapp, jQuery);