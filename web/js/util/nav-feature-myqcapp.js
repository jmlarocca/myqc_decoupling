;(function (qcssapp, $) {
    'use strict';

    //******* NAVIGATION MENU FEATURE RELATED FUNCTIONS *******/

    //builds the main application view, then navigates the user to the main menu
    qcssapp.Functions.initializeApplication = function() {
        if (qcssapp.DebugMode) {
            console.log("Starting Main Application...");
        }

        //TODO: move this to myqcapp.js mobile/desktop checking
        var userAgent = navigator.userAgent;
        var ipad = userAgent.match(/(iPad).*OS\s([\d_]+)/) ? true : false;
        var iphone = !ipad && userAgent.match(/(iPhone\sOS)\s([\d_]+)/) ? true : false;
        var ios = ipad || iphone;
        if ( ios ) {
            $('body').addClass('ios');
        }

        //in case this isn't set due to app resuming
        qcssapp.onPhoneApp = (typeof window.cordova !== "undefined");

        // if fingerprint is enabled but user canceled verification OR logged out and then closed app
        // 'keep me logged in' is technically ON... don't go to nav menu, stay on login page to verify fingerprint
        if(!(qcssapp.loggedOutFingerprint && qcssapp.Functions.fingerprintEnabled())) {

            qcssapp.Functions.showMask(true);

            //load the menu and rest of the application based on the signed in user
            new qcssapp.Views.NavView();

            //page transition to the main home navigation page
            qcssapp.Router.navigate('main', {trigger:true});
        }
    };

    //handles navigating back to the main menu view
    qcssapp.Functions.homeNavigationCallback = function() {
        //TODO: this function shouldnt be necessary - just not removing views properly
        qcssapp.Functions.resetDynamicViewEvents();
        qcssapp.futureOrderDate = ""; //reset future order date if on store list page

        //TODO: temporary controller calls since not all of the application uses the controller yet
        qcssapp.Controller.destroyViews('NavView');
        qcssapp.Controller.resetHistory();
        qcssapp.Controller.activeView = 'NavView';

        qcssapp.Functions.showMask(true);
        qcssapp.Functions.checkAccountSettings();
    };

    //********* QUICK PAY FUNCTIONS **********//

    //generate badge number for QR code
    qcssapp.Functions.generateQRCode = function() {
        qcssapp.Router.navigate('quickpay');
        qcssapp.Functions.executeStandardRoute('#show-quickPay');

        var endPoint = qcssapp.Location + '/api/myqc/qrcode';

        var dataParameters = {
            'badgePrefix': qcssapp.accountModel.get('badgeNumPrefix'),
            'badgeSuffix': qcssapp.accountModel.get('badgeNumSuffix'),
            'qrCodeLength': qcssapp.accountModel.get('qrCodeLength')
        };

        qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(dataParameters), '',
            function(result) {
                var $quickPayMsg = $('.quickPay-code-msg');
                if(result == 0) {
                    $quickPayMsg.hide();
                    qcssapp.Functions.displayError('There was an issue generating the QR Code');
                    return;
                }

                $quickPayMsg.show();
                qcssapp.Functions.setBrightness( 1 );

                result = result.toString();

                var $qrCode = kjua({text:result});
                $('#quickPay-code-container').html($qrCode);
            },
            '', '', '', ''
        );
    };

    //inactivate all badges with qr code badge type
    qcssapp.Functions.inactivateQRCodeBadges = function(callback, callParams) {
        qcssapp.Functions.resetBrightness();

        var endPoint = qcssapp.Location + '/api/myqc/inactivate/qrcode';

        qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
            function(result) {
                if(result < 1) {
                    console.log('There was an issue inactivating the badge numbers');
                }

                if(callback && typeof callback == 'function') {
                    callback(callParams);
                }
            },
            '', '', '', ''
        );
    };

    qcssapp.Functions.setBrightness = function( val ) {
        if ( !qcssapp.onPhoneApp || !cordova.plugins || !cordova.plugins.brightness ) {
            return;
        }

        var brightnessSuccess = function( val ) {
            qcssapp.defaultBrightness = val;
        };

        cordova.plugins.brightness.getBrightness( brightnessSuccess, function() {} );
        cordova.plugins.brightness.setBrightness( val );
    };

    qcssapp.Functions.resetBrightness = function() {
        if ( !qcssapp.onPhoneApp || !cordova.plugins || !cordova.plugins.brightness ) {
            return;
        }

        var val = qcssapp.defaultBrightness;

        //in case the default brightness was never set
        if ( !val ) {
            //iOS doesn't let you reset to system default with -1, so let's just assume a default of 0.75
            if ( $('body').hasClass('ios') ) {
                val = 0.75;
            } else {
                val = -1;
            }
        }

        cordova.plugins.brightness.setBrightness( val );
    };

    //********* FREEZE FUNCTIONS **********//

    //handles front end changes for when a user freezes or unfreezes their account
    qcssapp.Functions.toggleFreeze = function($userInfo, accountStatus) {
        if ( $('#main').attr("data-accountfreeze") == "no" ) {
            return false;
        }

        $('#freeze-btn').toggleClass('frozen');

        var $freezeContents = $(".freeze-content");
        var $navFreeze = $('#nav-freeze');
        var freezeIcon = $navFreeze.find('img');
        var $freezeBtnIcon = $('.freeze-btn-icon-container');
        var $freezeBtnIconImg = $freezeBtnIcon.find('img');

        if ( accountStatus == "F" ) {
            $userInfo.attr('data-accountStatus', 'A');

            $navFreeze.text("Freeze Account").prepend(freezeIcon);

            $freezeContents.each(function() {
                var $this = $(this);

                $this.text($this.attr('data-defaultText') );
            });

            if(qcssapp.usingBranding) {
                $freezeBtnIcon.children('svg').remove();
                $freezeBtnIcon.append('<img class="freeze-btn-icon svg freeze-svg font-color-primary" src="images/button-lock.svg" alt=""/>');
            } else {
                $freezeBtnIconImg.attr('src', $freezeBtnIcon.attr('data-defaultimg'));
            }

            $("#show-freeze").attr('data-title','Freeze Account');
        } else {
            $userInfo.attr('data-accountStatus', 'F');

            $navFreeze.text("Unfreeze Account").prepend(freezeIcon);

            $freezeContents.each(function() {
                var $this = $(this);

                $this.text($this.attr('data-frozenText') );
            });

            if(qcssapp.usingBranding) {
                $freezeBtnIcon.children('svg').remove();
                $freezeBtnIcon.append('<img class="freeze-btn-icon svg freeze-svg font-color-secondary" src="images/button-unlock.svg" alt=""/>');
            } else {
                $freezeBtnIconImg.attr('src', $freezeBtnIcon.attr('data-frozenImg'));
            }

            $("#show-freeze").attr('data-title','Unfreeze Account');
        }
        qcssapp.Functions.setSVGColors();
    };

    //********* EVENT HANDLERS **********//

    //go to #main event
    $(document).on('click', '.home-link', function(e) {
        qcssapp.Functions.checkCodeForUpdates('','',qcssapp.Functions.homeNavigationCallback);
    });

    //handles view back icon
    $(document).on('click', '.general-back-icon', function() {
        if ( Backbone.history.getFragment() == 'program-rewards' ||
            Backbone.history.getFragment() == 'program-rewards-history' ||
            (Backbone.history.getFragment() == 'receipt' && _(qcssapp.Controller.history).contains('ReceiptView')) ) {
            qcssapp.Controller.back();
        } else if ( Backbone.history.getFragment() == 'show-accountSettings' ) {
            var successFunction = function() {
                qcssapp.Functions.logOut();
            };
            qcssapp.Functions.cancelChanges(successFunction, '', true);
        } else {
            onBackKeyDown();
        }
    });

    //handle device back button
    document.addEventListener("backbutton", function(e) {
        e.preventDefault();

        //TODO: right now only for rewards
        if ( Backbone.history.getFragment() == 'program-rewards' ||
            Backbone.history.getFragment() == 'program-rewards-history' ||
            (Backbone.history.getFragment() == 'receipt' && _(qcssapp.Controller.history).contains('ReceiptView')) ) {
            qcssapp.Controller.back();
        } else {
            onBackKeyDown();
        }
    }, true);

    //click freeze/unfreeze button event
    $(document).on('click', '#freeze-btn', function() {
        qcssapp.Functions.showMask();
        freezeHandler();
    });

    //********* EVENT HANDLER FUNCTIONS **********//

    //handles the content shown on the process of freezing - confirming and messaging the user about their account status
    function freezeHandler() {
        if ( $('#main').attr("data-accountfreeze") == "no" ) {
            return false;
        }

        var freezeTitle = "Freeze Account";
        var freezeMain = "Really freeze account?";
        var freezeContent = "Yes, I want to temporarily freeze my account";

        var unfreezeTitle = "Unfreeze Account";
        var unfreezeMain = "Really unfreeze account?";
        var unfreezeContent = "Yes, I am ready to unfreeze my account";

        var $userInfo = $('.user-information-title');
        var accountStatus = $userInfo.attr('data-accountStatus');

        var freezeSuccessMain = "Your account is now frozen.";
        var freezeSuccessContent = "As long as your account is frozen, your badge cannot be used to make any purchases. If you find your badge, select &quot;Unfreeze Account&quot; from the main menu. If not, contact HR to request a replacement badge.";

        var unfreezeSuccessMain = "Your account is no longer frozen.";
        var unfreezeSuccessContent = "Your badge is now able to make purchases as usual.";

        var useTitle = freezeTitle;
        var useMain = freezeMain;
        var useContent = freezeContent;
        var useSuccessMain = freezeSuccessMain;
        var useSuccessContent = freezeSuccessContent;
        var endPoint = qcssapp.Location + '/api/account/freeze';

        if ( accountStatus == "F") {
            useTitle = unfreezeTitle;
            useMain = unfreezeMain;
            useContent = unfreezeContent;
            useSuccessMain = unfreezeSuccessMain;
            useSuccessContent = unfreezeSuccessContent;
            endPoint = qcssapp.Location + '/api/account/unfreeze';
        }

        //new general view
        new qcssapp.Views.GeneralConfirm({title: useTitle, main: useMain, content: useContent});

        var confirmContinueBtn = new qcssapp.Views.ButtonView({
            text: "Continue",
            buttonType: "customCB",
            appendToID: "#general-confirm-btn-container",
            id: "confirmContinueBtn",
            class: "align-right button-flat accent-color-one",
            callback: function() {

                if ($('#general-confirm-check').is(':checked')) {

                    var successParameters = {useTitle:useTitle, useSuccessMain:useSuccessMain, useSuccessContent:useSuccessContent, $userInfo:$userInfo, accountStatus:accountStatus};

                    qcssapp.Functions.callAPI( endPoint, 'POST', '', successParameters,
                        function(data, successParameters) {
                            if (data.status == "success") {
                                var msgView = new qcssapp.Views.GeneralMsg({
                                    title: successParameters.useTitle,
                                    main: successParameters.useSuccessMain,
                                    content: successParameters.useSuccessContent
                                });

                                //change freeze page text
                                //change nav link text
                                qcssapp.Functions.toggleFreeze(successParameters.$userInfo, successParameters.accountStatus);

                                //btn view to continue
                                //Cancel add code Button
                                new qcssapp.Views.ButtonView({
                                    text: "Continue",
                                    buttonType: "customCB",
                                    appendToID: "#general-msg-btn-container",
                                    pageLinkID: "#main",
                                    id: "messageContinueBtn",
                                    class: "button-flat accent-color-one",
                                    callback: function () {
                                        //click callback back to nav
                                        qcssapp.Router.navigate('main', {trigger: true});
                                    }
                                });

                                qcssapp.Router.navigate('message', {trigger: true});
                            } else {
                                //error message
                                qcssapp.Functions.displayError('Error attempting to change account status.');
                            }
                        },
                        '',
                        function() {
                            qcssapp.Functions.hideMask();
                        }
                    );
                } else {
                    qcssapp.Functions.displayError('You must check the box to continue.');
                }
            }
        });

        var confirmCancelBtn = new qcssapp.Views.ButtonView({
            text: "Cancel",
            buttonType: "customCB",
            appendToID: "#general-confirm-btn-container",
            id: "confirmCancelBtn",
            class: "align-right button-flat accent-color-one",
            callback: function() {
                qcssapp.Router.navigate('main', {trigger: true});
            }
        });

        qcssapp.Router.navigate('confirm', {trigger:true});

        qcssapp.Functions.hideMask();
    }

    //handles all 'back' events including android phone back button
    function onBackKeyDown() {
        var $activeView = $('.view.active');
        var $activePanel = $('.panel.active');
        var activeViewID = $activeView.attr('id');
        var activePanelID = $activePanel.attr('id');
        var phoneScanBack = qcssapp.phoneScanBack ? true : false;

        // set global variable back to false
        qcssapp.phoneScanBack = false;

        if ( activeViewID == "pageview" || activeViewID == "orderview" || ( activeViewID == "tourview" && $("#tour-header").is(":visible") ) ||
             activePanelID == "show-rewards" || activeViewID == "purchasesview" || activeViewID == "accountfundingview" ) {
            qcssapp.Functions.checkCodeForUpdates('','',qcssapp.Functions.homeNavigationCallback);
        } else if ( activeViewID == "mainview" || activeViewID == "loginview" || ( activeViewID == "tourview" && !$('#tour-header').is(':visible') ) ) {
            navigator.app.exitApp();
        } else if ( activeViewID == "keypadview" ) {
            var storeKeypad = qcssapp.OrderMenuHistory[0];
            var keypadID = activePanelID.split('-')[1];

            //if keypad is the home keypad
            if ( qcssapp.OrderMenuHistory.length == 1 || storeKeypad.id == keypadID ) {

                // If the user hit the back button on the Android to get out of the phone scan plugin, don't go back to store page
                if(phoneScanBack) {
                    return;
                }

                if ( qcssapp.CurrentOrder.products.length > 0 ) {
                    var message = 'Going back to the store list will reset your cart. Are you sure you want to empty your cart?';

                    qcssapp.Functions.displayPopup(message, 'Warning', 'Close', '', '', 'Empty Cart', qcssapp.Functions.resetOrdering, true);

                } else {
                    $activeView.find(".order-back-icon").trigger("click");
                }
            } else {
                $activeView.find(".order-back-icon").trigger("click");
            }
        } else if ( activeViewID == "cartview" || activeViewID == "productview" || activeViewID == "receiveview" || activeViewID == "reviewview" ||
                    activeViewID == "favoriteview" || activeViewID == "suggestiveview" || activeViewID == "redeemview" ) {
            if ($(".block-mask").is(":hidden")) {
                $activeView.find(".order-back-icon").trigger("click");
            }
        } else if ( activeViewID == "receipt-view") {
            var fromLocation = $('#receipt-btn-container').attr('data-fromLocation');

            if ( fromLocation == 'rewards') {
                qcssapp.Router.navigate('reward-history', {trigger: true});
            } else if ( fromLocation == "purchases") {
                qcssapp.Router.navigate('show-purchases', {trigger: true});
            } else {
                qcssapp.Router.navigate('main', {trigger: true});
            }
        } else if ( activeViewID == "rewardsview" ) {
            qcssapp.Router.navigate('show-rewards', {trigger:true});
        } else if ( activeViewID == "paymentview") {
            //unload payment frame always
            $('#payment_frame').attr('src','');

            // if funding on purchase
            if(qcssapp.Functions.hasOrderInProgress()){
                qcssapp.Functions.buildReceivePage();
                return
            }

            if($('#nav-accountFunding').length) {
                qcssapp.Router.navigate('account-funding', {trigger:true});
            } else {
                qcssapp.Functions.checkCodeForUpdates('','',qcssapp.Functions.homeNavigationCallback);
            }
        }  else if ( activeViewID == "addaccountview") {
            var options = {'autoSelect': false, 'fromBack': true};
            new qcssapp.Views.ManageAccountView(options);
            qcssapp.Router.navigate('manage-account', {trigger:true});
        } else if ( activeViewID == "futuredateview" ) {
            qcssapp.Router.navigate('show-order', {trigger:true});
            qcssapp.Functions.updateOrderingDayName();
        } else if ( activeViewID == "faqview" ) {
            qcssapp.Functions.showMask();
            if($activeView.hasClass('funding-faq')) {
                qcssapp.Router.navigate('account-funding', {trigger:true});
            } else {
                qcssapp.Router.navigate('login-page', {trigger: true});
            }
            qcssapp.Functions.hideMask();
        } else if ( activeViewID == "privacypolicyview" || activeViewID == "tosreviewview" ) {
            qcssapp.Router.navigate("tour-page-0", {trigger:true});
        } else {
            return false;
        }
    }

})(qcssapp, jQuery);