;(function (qcssapp, $) {
    'use strict';

    //******* FAVORITE ORDERING RELATED FUNCTIONS *******/

    //returns the items in the favorite order and calls orderInquiry to display any combos
    qcssapp.Functions.loadFavoriteOrder = function(id, successParams, callback) {
        var endPoint = qcssapp.Location + '/api/favorite/order/' + id;

        qcssapp.Functions.callAPI(endPoint, 'POST', '', successParams,
            function(response, successParams) {

                //if swiped on favorite order
                var favoriteOrderCallback = callback;

                //if clicked on express order cart on favorite order view
                if(!callback) {
                    //we need to set the favorite order items as the CurrentOrder.products so save what the current products in the cart are
                    response['currentProductsInCart'] = qcssapp.CurrentOrder.products;
                    qcssapp.CurrentOrder.products = [];

                    favoriteOrderCallback = function(result, callbackParams) {
                        //if there is only one product in the order
                        if(typeof callbackParams == 'undefined') {
                            callbackParams = result;
                        }

                        //update the current order products with the diningOptionProduct flag
                        $.each(callbackParams.items, function() {
                            var item = this;
                            $.each(qcssapp.CurrentOrder.products, function() {
                                if(item.id == this.get('productID')) {
                                    if(item.diningOptionProduct) {
                                        this.set('diningOptionProduct', true);
                                    }
                                    if($.isEmptyObject(this.get('prepOption')) && !$.isEmptyObject(item.prepOption)) {
                                        this.set('prepOption', new qcssapp.Models.PrepOption( item.prepOption ));
                                    }
                                }
                            });
                        });

                        //set the favorite order items as the products returned back from the orderInquiry
                        callbackParams.items = qcssapp.CurrentOrder.products;

                        //set the CurrentOrder.products back to the actual products in the cart
                        qcssapp.CurrentOrder.products =  callbackParams['currentProductsInCart'];

                        //reset the number in the cart icon
                        qcssapp.Functions.updateNumberOfProductsInCart();

                        //not all views use the controller, need to destroy favorite order view
                        qcssapp.Controller.destroyViews('FavoriteOrderView');
                        qcssapp.Controller.constructedViews = [];

                        var options = {
                            data: callbackParams,
                            params:successParams
                        };

                        var favoriteOrderControllerModel = new qcssapp.Models.Controller({
                            id: 'FavoriteOrderView',
                            route: 'favorite-order',
                            options: options,
                            destructible: true,
                            destroyOn: ['NavView', 'MyQuickPicksView']
                        });

                        qcssapp.Controller.activateView(favoriteOrderControllerModel);
                        qcssapp.Functions.hideMask();
                    };
                }

                var productsToAddToCart = [];
                if(response.items.length > 0) {
                    $.each(response.items, function() {
                        this.productID = this.id;
                        var productModel = new qcssapp.Models.Product(this);
                        productModel.set('productID', productModel.get('id'));

                        if(this.prepOption != undefined && !$.isEmptyObject(this.prepOption)) {
                            productModel.set('prepOption', new qcssapp.Models.PrepOption( this.prepOption ));
                        }

                        if(this.modifiers && this.modifiers.length){
                            productModel.set('modifiers', this.modifiers);
                        }
                        else{
                            productModel.set('modifiers', []);
                        }


                        productsToAddToCart.push(productModel);
                    });
                }

                //reset the CurrentOrder.products object with the items on the favorite order
                for (var i = 0; i < productsToAddToCart.length; i++) {
                    var product = productsToAddToCart[i];

                    qcssapp.Functions.addProductToCart(product);
                }

                if( qcssapp.CurrentOrder.products.length > 1 ) {
                    qcssapp.Functions.checkProductCombos(favoriteOrderCallback, response);
                    return;
                }

                favoriteOrderCallback(response);

            },
            function() {
                if (qcssapp.DebugMode) {
                    console.log("Error trying to load products in favorite order.");
                }
            },
            '', '', '', false, true
        );

    };

    qcssapp.Functions.isFavoriteOrder = function(transactionId, callback, preventShowMask) {
        preventShowMask = typeof preventShowMask === 'undefined' ? false : preventShowMask;

        callback = callback || function() {};
        var endPoint = qcssapp.Location + '/api/favorite/order/is-favorite';
        if(!transactionId || transactionId == '') {
            if(qcssapp.orderObject.products.length < 1) {
                return;
            } else {
                endPoint += '/cart';
                qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify({products: qcssapp.orderObject.products}), '',
                    function(data) {
                        callback.call(this, data);
                    }, function() {

                    }, '', '', '', preventShowMask);

            }

        } else {
            endPoint += '/' +transactionId;

            qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
                function (data) {
                    callback.call(this, data);
                }, function () {
                    if (qcssapp.DebugMode) {
                        console.log("Error trying to determine if order is favorite order");
                    }
                }, '', '', '', true);
        }
    }

    qcssapp.Functions.createFavoriteOrder = function(id, callback, callbackParams) {
        callback = callback || function(){};

        $.confirm({
            title: 'Name your order',
            content: _.template("<input type='text' id='favoriteName' autofocus/>"),
            draggable: true,
            buttons: {
                okay: {
                    text: 'Ok',
                    btnClass: 'btn-default popup-close-btn favorite-order-create-btn',
                    action: function () {
                        var input = $('input#favoriteName');
                        if (!input.val().trim()) {
                            $.alert({
                                title: '',
                                content: "<span style='text-align: center;display: flex;'>Please give your favorite order a name.</span>",
                                type: 'red',
                                buttons: {
                                    okay: {
                                        text: 'Ok',
                                        btnClass: 'primary-background-color font-color-primary primary-gradient-color primary-border-color name-order-popup-button',
                                        action: function () {

                                        }
                                    }
                                }
                            });
                            return false;
                        // D-4417: special characters and emojis would result in a lost title of the order
                        // regex matches whitespace, 0-9, A-Z, a-z
                        } else if(!input.val().match(/^[\u{20}\u{30}-\u{39}\u{41}-\u{5A}\u{61}-\u{7A}]+$/u)) {
                            $.alert({
                                title: '',
                                content: "<span style='text-align: center;display: flex;'>Please do not use special characters or emojis in your order's name.</span>",
                                type: 'red',
                                buttons: {
                                    okay: {
                                        text: 'Ok',
                                        btnClass: 'primary-background-color font-color-primary primary-gradient-color primary-border-color name-order-popup-button',
                                        action: function () {

                                        }
                                    }
                                }
                            });
                            return false;
                        } else {
                            var favoriteName = input.val();
                            var url, body;
                            if(!id) {
                                var orderObject = qcssapp.orderObject;
                                if(qcssapp.Functions.checkServerVersion(4,0)) {
                                    orderObject = qcssapp.CurrentOrder;
                                }

                                // If no transaction Id
                                if($.isEmptyObject(orderObject) || orderObject.products.length < 1) {
                                    return;
                                } else {
                                    url = qcssapp.Location + "/api/favorite/order/activate/cart";
                                    body = {
                                        name: favoriteName,
                                        products: orderObject.products
                                    };

                                    if(typeof callbackParams !== 'undefined' && typeof callbackParams.expressOrderID !== 'undefined') {
                                        body.expressOrderID = callbackParams.expressOrderID;
                                        callbackParams.orderName = favoriteName;
                                    }
                                }
                            } else {
                                // If transaction Id available
                                url = qcssapp.Location + "/api/favorite/order/activate";
                                body = {
                                    name: favoriteName,
                                    transactionId: id
                                };
                            }
                            qcssapp.Functions.callAPI(url, "POST", JSON.stringify(body), {},
                                function (data) {
                                    if (qcssapp.DebugMode) {
                                        console.log("Favorite Order Saved");
                                    }
                                    // Change the button icon rto show as favorite
                                    callback.call(this, data, callbackParams);
                                }, function (error) {
                                    if (qcssapp.DebugMode) {
                                        console.log("Error trying to create favorite order");
                                    }
                                }, '', '', '', false);
                        }
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-default popup-close-btn favorite-order-create-btn name-order-popup-button',
                    action: function () {
                        this.close();
                        return false;
                    }
                }
            }
        })
    };

    qcssapp.Functions.toggleFavoriteOrder = function(id, callback, preventShowMask, callbackParams) {
        var endPoint = qcssapp.Location + '/api/favorite/order/' + id + '/toggle';
        callback = callback || function() {};

        qcssapp.Functions.callAPI(endPoint, 'PUT', {}, '',
            function(data) {
                callback.call(this, data, callbackParams);
            },
            function() {
                if (qcssapp.DebugMode) {
                    console.log("Error trying to change favorite order status.");
                }
            },
            '', '', '', preventShowMask
        );
    };


})(qcssapp, jQuery);