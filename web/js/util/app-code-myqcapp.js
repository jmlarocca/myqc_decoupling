;(function (qcssapp, $) {
    'use strict';

    //******* APP CODE RELATED FUNCTIONS *******/

    //checks the current session code for updates
    qcssapp.Functions.checkCodeForUpdates = function(code, sessionID, callback) {
        if ( !code.length && !sessionID.length ) {
            var mdl = new qcssapp.Models.SessionModel();
            mdl.fetch();
            code = mdl.get('code');
            sessionID = mdl.get('sessionID');
        }

        //if not a valid code, call the given callback and return, special cases for codes 'home server' and 'default'
        if ( !code || code == "" || code == 'home server' || code == 'default' || code == 'SSO' ) {
            if ( callback && typeof callback == "function" ) {
                callback();
            }

            //if special code, set the location as the current location on the browser
            if ( code == 'home server' || code == 'default' ) {
                var location = window.location.origin + window.location.pathname;
                qcssapp.Functions.setSession(sessionID, location.substr(0, location.length-1), '', code);
            }
            return;
        }

        var endPoint = 'https://www.mmhcloud.com/gateway/api/auth/code/'+code;
        //endPoint = 'gatewayCodes.json';

        var successParameters = {callback:callback, code:code, sessionID:sessionID, preventOfflineError:true};

        qcssapp.Functions.callAPI( endPoint, 'GET', '', successParameters,
            function(data, successParameters) {
                if ( !data.LOCATION ) {
                    if (qcssapp.DebugMode) {
                        console.log("Checking for code update failed to return a location for code: "+successParameters.code);
                    }
                    return false;
                }

                //all attributes of the code
                var attrs = {
                    CODE: data.CODE.toLowerCase(),
                    ALIAS: data.ALIAS,
                    LOCATION: data.LOCATION.trim(),
                    NAME: data.NAME,
                    INSTANCEID: data.INSTANCEID,
                    INSTANCEUSERID: data.INSTANCEUSERID,
                    ALLOWINVALIDSSLCERTS: data.ALLOWINVALIDSSLCERTS
                };

                if ( attrs.LOCATION != qcssapp.Location ) {
                    if (qcssapp.DebugMode) {
                        console.log("Code has changed, updating session and location");
                    }

                    var instanceCodeSelectView = new qcssapp.Views.instanceCodeSelectView();
                    var mdl = instanceCodeSelectView.collection.get(successParameters.code);
                    mdl.save(attrs);

                    qcssapp.Functions.setSession(successParameters.sessionID, attrs.LOCATION, '', attrs.CODE)

                }

                if ( successParameters.callback && typeof successParameters.callback == 'function' ) {
                    successParameters.callback();
                }
            },
            function(params) {
                if (qcssapp.DebugMode) {
                    console.log("Error checking code validity");
                }

                var msg = 'Error occurred attempting to connect to cloud to check code for updates.';
                var endPoint = qcssapp.Location + "/api/myqc/error/log";

                var errorObj = {
                    msg: msg,
                    url: '',
                    line: '',
                    col: '',
                    error: ''
                };

                qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(errorObj),'',
                    function() {
                        console.log('Successfully logged error on back end.');
                    },
                    '',
                    '',
                    '',
                    '',
                    true,
                    true
                );

                if ( params.callback && typeof params.callback == 'function' ) {
                    params.callback();
                }

                qcssapp.Functions.hideMask();
            },
            '',
            successParameters
        );
    };

    //******* APP CODE EVENT HANDLER FUNCTIONS *******/

    //enter keypress event submits add code input
    $(document).on('keyup', '#instanceCodeInput', function(e) {
        if (e.keyCode == 13) {
            $("#saveInstanceCode").trigger("click");
        }
    });

    //enter keypress event submits field on enter page (mobile)
    $(document).on('keyup', '#enterInstanceCode', function(e) {
        if (e.keyCode == 13) {
            $("#saveEnterInstanceCode").trigger("click");
        }
    });

    //handles changing the code selector on login page
    $(document).on('change', '#loginCodeSelect', function() {
        var $keepLogged = $('#keepLogged');
        $keepLogged.prop('checked', false);
        var $enableFingerprint = $('#enableFingerprint');
        $enableFingerprint.prop('checked', false);
        $('#loginName, #loginPassword').show();
        qcssapp.loggedOutFingerprint = false;

        //turn off checkFingerprintOnDevice in local storage
        qcssapp.Functions.turnOffFingerprint(1);

        var thisLocation = $(this).val();

        if ( thisLocation == null ) {
            return;
        }

        var successParameter = {location:thisLocation};

        qcssapp.Functions.checkLoginSettings(thisLocation + '/api/auth/login/settings', successParameter);
    });

})(qcssapp, jQuery);