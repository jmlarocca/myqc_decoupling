;(function (qcssapp, $) {
    'use strict';

    //******* FINGERPRINT RELATED FUNCTIONS *******/

    //checks if fingerprint authentication is available for the device and user
    qcssapp.Functions.enableFingerprint = function(data) {
        qcssapp.enableFingerprint = (data.enableFingerprint === "true" || data.enableFingerprint === true);

        //if valid server version AND on the phone AND fingerprint is turned on globally AND there are fingerprints setup... show enable fingerprint checkbox
        if(qcssapp.Functions.checkServerVersion(1,6) && typeof window.cordova !== "undefined" && qcssapp.enableFingerprint) {
            qcssapp.Functions.isFingerprintAvailableOnDevice(false);
        } else {
            qcssapp.Functions.turnOffFingerprint(0);
        }
    };

    //check if the device has fingerprint authentication or touch id setup and if there are valid fingerprints
    qcssapp.Functions.isFingerprintAvailableOnDevice = function(fromResetLogout) {
        var $enableFingerprint = $('label[for=enableFingerprint]');
        $enableFingerprint.hide();

        if ( navigator.userAgent.match(/Android/i) ) { // Android fingerprint verification
            FingerprintAuth.isAvailable(
                function(result){ // Success callback
                    if(result.isAvailable && result.hasEnrolledFingerprints) {
                        $enableFingerprint.show();
                        var $enableFingerInput = $('#enableFingerprint');
                        if(fromResetLogout) {
                            qcssapp.loggedOutFingerprint = true;
                            $enableFingerInput.prop('checked',true);
                            $('#loginName, #loginPassword, label[for=keepLogged]').hide();
                        }
                    } else if(!result.hasEnrolledFingerprints){
                        $enableFingerprint.show();
                        qcssapp.Functions.resetFingerprintInSession();
                        $('#enableFingerprint').attr('no-enrolled-fingerprints', true);
                    }
                }, function(){  // Error callback
                    if ( qcssapp.DebugMode ) {
                        console.log("Fingerprint Authentication is not available on this device.");
                    }
                }
            );

        } else if ( navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) ) {  // iOS TouchID verification
            window.plugins.touchid.isAvailable(
                function() {
                    $enableFingerprint.show();
                    if(fromResetLogout) {
                        qcssapp.loggedOutFingerprint = true;
                        $('#enableFingerprint').prop('checked',true);
                        $('#loginName, #loginPassword, label[for=keepLogged]').hide();
                    }
                }, function(err) {
                    if(err.code == -8 && fromResetLogout) {
                        $('label[for=keepLogged]').show();
                        qcssapp.Functions.turnOffFingerprint(true);
                        qcssapp.Functions.fingerprintLockoutPopup();

                    } else if(err.code == -8 && qcssapp.verifyFingerprintFromLogin) {
                        qcssapp.Functions.fingerprintReset();
                        qcssapp.Functions.fingerprintLockoutPopup();
                    }
                    if ( qcssapp.DebugMode ) {
                        console.log("Touch ID is not available on this device.");
                    }
                }
            );

            qcssapp.Functions.allowFaceID();
        }
    };

    qcssapp.Functions.allowFaceID = function() {
        var $fingerPrintLabel = $('#loginFormContainer label[for="enableFingerprint"]')
        var $fingerPrintLabelSettings = $('#enable-fingerprint-label');

        if ( qcssapp.Functions.onIphoneX() ) {
            $fingerPrintLabel.text('Enable Face ID');
        } else {
            $fingerPrintLabel.text('Enable Fingerprint / Facial Recognition');
        }
    };

    //opens the fingerprint authentication dialog box so the user can verify their fingerprint
    qcssapp.Functions.verifyFingerprintOnDevice = function() {
        if(!qcssapp.Functions.checkServerVersion(1,6)) {
            return;
        }

        if( navigator.userAgent.match(/Android/i) ) {  //Android fingerprint verification
            // doesn't matter what is passed in just checks if the clientID and clientSecret exist

            var encryptConfig = {
                clientId: 'My Quickcharge',
                clientSecret: 'quickcharge_password',
                disableBackup: true,
                dialogTitle:'Scan your fingerprint'
            };

            // opens the fingerprint dialog box
            FingerprintAuth.encrypt(encryptConfig,
                function() {  // Success callback
                    //if the user previously logged out or hit cancel with enable fingerprint ON, send them to nav menu from login page
                    if(qcssapp.loggedOutFingerprint) {

                        //if the user is verifying fingerprint from login page, don't make them re-verify on nav menu
                        if(qcssapp.verifyFingerprintFromLogin) {
                            qcssapp.loggedOutFingerprint = false;
                        }

                        if(qcssapp.personModel && !qcssapp.accountModel) {
                            qcssapp.Functions.navigateToManageAccount(false);
                        } else if(!qcssapp.ssoLogin) {
                            qcssapp.Functions.initializeApplication();
                        }
                    }

                    if(qcssapp.ssoLogin) { //if coming from SSO, turn checkFingerprintOnDevice ON in the session model
                        qcssapp.Functions.extendSSODSKeyForFingerprint();
                    }

                    return true;
                }, function(err){   // Error callback
                    console.log("Error in FingerprintAuth.encrypt(): "+err);
                    if(err == "INIT_CIPHER_FAILED") {
                        qcssapp.Functions.fingerprintChangePopup();
                    }

                    //if cancelled then turn off checkbox
                    if(err == "FINGERPRINT_CANCELLED" && window.location.hash.indexOf("login-page") == -1) {
                        qcssapp.Functions.fingerprintReset();
                    }

                    if(err == "FINGERPRINT_ERROR" && window.location.hash.indexOf("login-page") == -1) {
                        qcssapp.Functions.fingerprintReset();

                    }

                }
            );

        } else if ( navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) ) {  //iOS TouchID verification
            // check if there is a change to the fingerprints saved on the device first
            window.plugins.touchid.didFingerprintDatabaseChange(
                function(changed) {
                    if(changed) {
                        qcssapp.Functions.fingerprintChangePopup();
                    } else {
                        // opens the touch id dialog box
                        window.plugins.touchid.verifyFingerprint(
                            'Scan your fingerprint please',
                            function() {  // Success callback
                                //if the user previously logged out or hit cancel with enable fingerprint ON, send them to nav menu from login page
                                if(qcssapp.loggedOutFingerprint) {

                                    //if the user is verifying fingerprint from login page, don't make them re-verify on nav menu
                                    if(qcssapp.verifyFingerprintFromLogin) {
                                        qcssapp.loggedOutFingerprint = false;
                                    }

                                    if(qcssapp.personModel && !qcssapp.accountModel) {
                                        qcssapp.Functions.navigateToManageAccount(false);
                                    } else if(!qcssapp.ssoLogin) {
                                        qcssapp.Functions.initializeApplication();
                                    }
                                }

                                if(qcssapp.ssoLogin) { //if coming from SSO, turn checkFingerprintOnDevice ON in the session model
                                    qcssapp.Functions.extendSSODSKeyForFingerprint();
                                }

                                return true;
                            }, function(err) {  // Error callback
                                if ( qcssapp.DebugMode ) {
                                    console.log("Touch ID failed to verify fingerprint.");
                                }
                                if(window.location.hash.indexOf("login-page") == -1) {  //if not on login page, go to login page
                                    qcssapp.Functions.fingerprintReset();

                                } else if (window.location.hash.indexOf("login-page") != -1) {  //if on login page, destory session and show login fields
                                    qcssapp.Functions.isFingerprintAvailableOnDevice(false);
                                }
                            }
                        );
                    }
                }
            );
        }
    };

    // logs out, shows the 'Keep me Logged in' checkbox and turns 'Enable Fingerprint' off
    qcssapp.Functions.fingerprintReset = function() {
        if(!qcssapp.ssoLogin) {
            $('label[for=keepLogged]').show();
            $('#enableFingerprint').prop('checked',false);
            qcssapp.Functions.resetOnLogout();
        } else {
            var logoutURL = qcssapp.Location+'/sso/sls?dskey='+qcssapp.Session;
            qcssapp.Functions.resetOnLogout(true);
            new qcssapp.Views.LogoutView();
            qcssapp.Router.navigate('logout-page', {trigger:true});
            cordova.InAppBrowser.open(logoutURL, '_system');
        }
    };

    // pop displayed when the fingerprints saved on the device has changed, forces the user to login again and 'register' their fingerprint
    qcssapp.Functions.fingerprintChangePopup = function() {
        $.confirm({
            title: '',
            content: '<div id="reauth-fingerprint">There has been a change to the fingerprints saved on this device.</br></br>Please re-authenticate your login name and password before allowing Touch ID to scan your fingerprint again.</div>',
            buttons: {
                cancel: {
                    text: 'Logout',
                    btnClass: 'btn-default fingerprint-logout',
                    action: function() {
                        qcssapp.Functions.fingerprintReset();
                    }
                }
            }
        });
    };

    qcssapp.Functions.fingerprintLockoutPopup = function() {
        $.confirm({
            title: '',
            content: "<div id='reauth-fingerprint'>Access to TouchID has been locked. </br></br>Unlock TouchID by providing your Apple ID passcode on the device's unlock screen or in the device's Touch ID Settings.</div>",
            buttons: {
                cancel: {
                    text: 'Close',
                    btnClass: 'btn-default fingerprint-logout',
                    action: function() {}
                }
            }
        });
    };

    //completely turn off fingerprint authentication (ex: if switching code or it's not configured)
    qcssapp.Functions.turnOffFingerprint = function(onCodeChange) {
        qcssapp.Functions.resetFingerprintInSession();
        $('label[for=enableFingerprint]').hide();
        $('#enableFingerprint').prop('checked',false);
        $('#keepLogged').prop('checked',false);
        if(onCodeChange) {
            var mdl = new qcssapp.Models.SessionModel();
            mdl.fetch();
            mdl.set('sessionID',false);
            mdl.save();
        }
    };

    //turns off checkFingerprintOnDevice in local storage
    qcssapp.Functions.resetFingerprintInSession = function() {
        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        mdl.set('checkFingerprintOnDevice',false);
        mdl.save();
    };

    //turns off checkFingerprintOnDevice in local storage
    qcssapp.Functions.enableCheckFingerprintOnDevice = function() {
        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        if(!mdl.get('checkFingerprintOnDevice')) {
            mdl.set('checkFingerprintOnDevice',true);
            mdl.save();
        }
    };

    //checks Session Model is fingerprint is enabled on device
    qcssapp.Functions.fingerprintEnabled = function() {
        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        return mdl.get('checkFingerprintOnDevice');
    };

    //checks if the sessionID (DSKey) is valid in local storage
    qcssapp.Functions.getSessionID = function() {
        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        return mdl.get('sessionID');
    };

    //checks if the keep my logged in is turned on in local storage
    qcssapp.Functions.getKeepMeLogged = function() {
        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        return mdl.get('keepLogged');
    };

    //******* FINGERPRINT WITH SSO RELATED FUNCTIONS *******/

    //check if fingerprint authentication is available for the device
    qcssapp.Functions.isSSOFingerprintAvailable = function() {

        if(qcssapp.Functions.checkServerVersion(1,6) && typeof window.cordova !== "undefined" && qcssapp.enableFingerprint) {

            if ( navigator.userAgent.match(/Android/i) ) { // Android fingerprint verification
                FingerprintAuth.isAvailable(
                    function(result){ // Success callback
                        if(result.isAvailable && result.hasEnrolledFingerprints) {

                            $('#fingerprintAuthCheckboxContainer').show();

                            $('#enable-fingerprint').prop('checked', false);

                            var isFingerprintEnabled = qcssapp.Functions.fingerprintEnabled();
                            if(isFingerprintEnabled) {
                                $('#enable-fingerprint').prop('checked', true);
                            }

                        } else if(!result.hasEnrolledFingerprints){
                            $.confirm({
                                title: '',
                                content: '<div id="no-enrolled-fingerprint" >Fingerprint Authentication is available but there are no enrolled fingerprints on this device.</div>',
                                buttons: {
                                    cancel: {
                                        text: 'Close',
                                        btnClass: 'btn-default fingerprint-close',
                                        action: function() {
                                            $('#enableFingerprint').prop('checked',false);
                                        }
                                    }
                                }
                            });
                        }
                    }, function(){  // Error callback
                        if ( qcssapp.DebugMode ) {
                            console.log("Fingerprint Authentication is not available on this device.");
                        }
                    }
                );

            } else if ( navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) ) {  // iOS TouchID verification
                window.plugins.touchid.isAvailable(
                    function() {

                        $('#fingerprintAuthCheckboxContainer').show();

                        $('#enable-fingerprint').prop('checked', false);

                        var isFingerprintEnabled = qcssapp.Functions.fingerprintEnabled();
                        if(isFingerprintEnabled) {
                            $('#enable-fingerprint').prop('checked', true);
                        }

                    }, function(err) {
                        if(err.code == -8) {
                            $('label[for=keepLogged]').show();
                            qcssapp.Functions.turnOffFingerprint(true);
                            qcssapp.Functions.fingerprintLockoutPopup();

                        } else if(err.code == -8 && qcssapp.verifyFingerprintFromLogin) {
                            qcssapp.Functions.fingerprintReset();
                            qcssapp.Functions.fingerprintLockoutPopup();
                        }
                        if ( qcssapp.DebugMode ) {
                            console.log("Touch ID is not available on this device.");
                        }
                    }
                );
            }

        } else {
            qcssapp.Functions.turnOffFingerprint(0);
        }
    };

    //extend their DSKey session as if Keep Me Logged In was ON if they want to use fingerprint authentication
    qcssapp.Functions.extendSSODSKeyForFingerprint = function() {
        var endPoint = qcssapp.Location+ '/api/account/sso/extend/dsKey';

        qcssapp.Functions.callAPI(endPoint,'GET', '', '',
            function(result) {
                //if update is successful verify the fingerprint
                if(result == 1) {
                    qcssapp.Functions.enableCheckFingerprintOnDevice();

                } else {
                    qcssapp.Functions.displayError('Could not extend session for fingerprint authentication. Please try again.');
                }
            },
            function(params) {
                if ( qcssapp.DebugMode ) {
                    console.log('ERROR could not update DSKey');
                }

                qcssapp.Functions.displayError('Could not extend session for fingerprint authentication.');
            },
            '', '', '', true, true
        );

    };

    //******* FINGERPRINT RESUME RELATED FUNCTIONS *******/

    //check if fingerprint auth is turned on for the device, if it is verify the fingerprint
    qcssapp.Functions.onResume = function() {
        //if ( qcssapp.onPhoneApp && ( navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) )) {   // on iPhone or iPad
        if ( qcssapp.onPhoneApp && !qcssapp.phoneScanActive) {
            //if not on the login or logout page for sso and fingerprint is turned on, verify it
            if(qcssapp.Functions.fingerprintEnabled() && window.location.hash.indexOf("login-page") == -1 && window.location.hash.indexOf("logout-page") == -1) {
                qcssapp.Functions.verifyFingerprintOnDevice();
            } else if (qcssapp.enableFingerprint && qcssapp.Functions.checkServerVersion(1,6)) {
                qcssapp.Functions.isFingerprintAvailableOnDevice(false);
            }
        }

        qcssapp.phoneScanActive = false;
    };

})(qcssapp, jQuery);