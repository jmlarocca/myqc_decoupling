;(function (qcssapp, $) {
    'use strict';

    //******* TOUR RELATED FUNCTIONS *******/

    qcssapp.Functions.getMyQCFeatures = function()  {
        var endPoint = qcssapp.Location + '/api/myqc/check/tourFeatures';
        qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
            function(result) {
                qcssapp.Functions.buildTour(result);
            },
            '',
            '',
            '',
            '',
            false,
            false,
            true
        );
    };

    //determine which tour panels should be displayed or not
    qcssapp.Functions.buildTour = function(pages) {
        $('#tour-page-2').removeClass('tour-panel'); //remove the original panels from the list
        $('#tour-page-3').removeClass('tour-panel');


        var $tourPanel = $('.tour-panel');
        for(var panelIndex=2; panelIndex<$tourPanel.length; panelIndex++) {
            var found = false;
            for(var pageIndex=0; pageIndex<pages.length; pageIndex++) {
                if(pages[pageIndex].id.indexOf('Tour') != -1) {
                    var pageName = (pages[pageIndex].id).substr(0,(pages[pageIndex].id).indexOf('Tour'));
                    if(pageName == $tourPanel.eq(panelIndex).attr('data-tourname') || pageName == $tourPanel.eq(panelIndex).attr('data-featurename')) {
                        if(pages[pageIndex].available == "yes") {
                            found = true;
                            break;
                        }
                    }
                    if($tourPanel.eq(panelIndex).attr('data-tourname') == "funding" && pageName == "accountfunding" && pages[pageIndex].available == "yes") {
                        found = true;
                        break;
                    }
                    if($tourPanel.eq(panelIndex).attr('data-tourname') == "deductionhistory" && pageName == "deductions" && pages[pageIndex].available == "yes") {
                        found = true;
                        break;
                    }
                    if($tourPanel.eq(panelIndex).attr('data-tourname') == "spendingprofile" && pageName == "spendingprofiles" && pages[pageIndex].available == "yes") {
                        found = true;
                        break;
                    }
                    if($tourPanel.eq(panelIndex).attr('data-tourname') == "healthyIndicatorTour" && pageName == "healthyIndicator" && pages[pageIndex].available == "yes") {
                        found = true;
                        if(qcssapp.Functions.checkServerVersion(3, 0, 20)) {
                            // Checks for the healthy Icons but uses onlineordering because it is only available when online ordering is
                            var background = $('[data-tourname="quickpay"]').children('.tour-featured-container').children('img').css('background-color');
                            var $imageContainer = $('.tour-healthy-icon-container');
                            var icons = pages[pageIndex].version.split(', ');
                            found = icons.length > 0;
                            var healthyText = "Look for special indicators for ";
                            if (found) {
                                $.each(icons, function (index, icon) {
                                    // If there is only one icon make them like the other panels
                                    if(icons.length === 1) {
                                        $('<img/>').attr('src', 'images/tour-' + icon.toLowerCase() + '.svg')
                                            .addClass('tour-svg')
                                            .appendTo($tourPanel.eq(panelIndex).find('.tour-featured-container'))
                                            .css('background', background);
                                        $imageContainer.remove();
                                    } else {
                                        if($imageContainer.length === 0) {
                                            // Since we have to remove the container when switching app codes
                                            // we need to add it back to the page so we have something to append the icons
                                            // to.
                                            $('<div class="tour-healthy-icon-container"></div>')
                                                .appendTo($tourPanel.eq(panelIndex).find('.tour-featured-container'));
                                            $imageContainer = $('.tour-healthy-icon-container');
                                            $imageContainer.css('background', background);
                                        }
                                        if($imageContainer.children().length && icons.length == $imageContainer.children().length) {
                                            return;
                                        }

                                        $('<img/>').attr('src', 'images/tour-' + icon.toLowerCase() + '.svg')
                                            .addClass('tour-healthy-icon')
                                            .appendTo($imageContainer)
                                            .css({'max-width': '50%', 'width': '50%'});

                                    }
                                    if(index === icons.length - 1 && icons.length > 1) {
                                        healthyText += " and "
                                    }
                                    if(icon.toLowerCase() == 'healthy') {
                                        healthyText +=  qcssapp.healthyIndicator.wellnessLabel + " <img src=\""+qcssapp.healthyIndicator.wellness+"\">";

                                        if(qcssapp.healthyIndicator.wellness != 'images/icon-healthy.svg') {
                                            $('.tour-healthy-icon-container').find('img[src="images/tour-healthy.svg"]').addClass('custom-icon').attr('src', qcssapp.healthyIndicator.wellness);
                                        }
                                    }
                                    if(icon.toLowerCase() == 'vegan') {
                                        healthyText +=  qcssapp.healthyIndicator.veganLabel + " <img src=\""+qcssapp.healthyIndicator.vegan+"\">";

                                        if(qcssapp.healthyIndicator.vegan != 'images/icon-vegan.svg') {
                                            $('.tour-healthy-icon-container').find('img[src="images/tour-vegan.svg"]').addClass('custom-icon').attr('src', qcssapp.healthyIndicator.vegan);
                                        }
                                    }
                                    if(icon.toLowerCase() == 'vegetarian') {
                                        healthyText +=  qcssapp.healthyIndicator.vegetarianLabel + " <img src=\""+qcssapp.healthyIndicator.vegetarian+"\">";

                                        if(qcssapp.healthyIndicator.vegetarian != 'images/icon-vegetarian.svg') {
                                            $('.tour-healthy-icon-container').find('img[src="images/tour-vegetarian.svg"]').addClass('custom-icon').attr('src', qcssapp.healthyIndicator.vegetarian);
                                        }
                                    }
                                    if(icon.toLowerCase() == 'glutenfree') {
                                        healthyText += qcssapp.healthyIndicator.glutenFreeLabel + " <img src=\""+qcssapp.healthyIndicator.glutenFree+"\">";

                                        if(qcssapp.healthyIndicator.glutenFree != 'images/icon-glutenfree.svg') {
                                            $('.tour-healthy-icon-container').find('img[src="images/tour-glutenfree.svg"]').addClass('custom-icon').attr('src', qcssapp.healthyIndicator.glutenFree);
                                        }
                                    }
                                    if(index < icons.length - 1 ) {
                                        healthyText += ", "
                                    }
                                    if(index === icons.length - 1) {
                                        healthyText += " items."
                                    }
                                });
                                // If there is only 2 icons use column layout
                                if(icons.length == 2) {
                                    $imageContainer.css({'flex-flow': 'column wrap'});
                                }
                            }
                            $('#healthy-text').html(healthyText);
                        }
                        break;
                    }
                }
            }
            if(!found) {
                $tourPanel.eq(panelIndex).addClass('disabled');
                $('.tour-navigation-item').eq(panelIndex).addClass('hidden');
            }
        }

        if($('#getStartedBtn').is(":visible") && qcssapp.usingBranding) {
            $('.tour-footer').css('height', '170px');
            $('#tour-logo-get-started-container').css({position: 'absolute', bottom: '55%', left:'15%', right:'15%'});
            $('.tour-panel').css('height', '');
        } else if(!$('#getStartedBtn').is(':visible')) {
            // the header and 'get started' button change the heights needed to properly display the tour panel content
            $('.tour-panel').css('height', '93%');
        }

    };

    qcssapp.Functions.showAboutPageElements = function() {
        $(".app-version, .app-version-label, #aboutqc-links").show();
        $(".aboutqc-link-arrow").attr("src", "images/icon-expand-arrow.svg");
        var $privacyPolicyLink = $("#privacy-policy-link");
        $privacyPolicyLink.off();
        $privacyPolicyLink.on("click", function() {
            qcssapp.Router.navigate("privacy-policy", {trigger: true});
        });
    };

    // If the user has accepted TOS, show a link to display it
    qcssapp.Functions.showAcceptedTOS = function() {
        var endPoint = qcssapp.Location + "/api/account/tos/getAccepted";
        qcssapp.Functions.callAPI(endPoint, "GET", "", "",
            function(result) {
                if (result && result.HTMLCONTENT && result.ACCEPTEDON) {
                    // Fill in the content of the TOS display
                    $("#tos-review-page").html(qcssapp.Functions.htmlDecode(result.HTMLCONTENT).replace(/_nbsp/g, "&nbsp;"));
                    $("#tos-review-date").text("Accepted " + moment(result.ACCEPTEDON).format("M/DD/YYYY h:mm A"));
                    if (result.TITLE) {
                        $("#tos-review-header").text(result.TITLE);
                    }

                    // Show the link
                    var $TOSLink = $("#tos-link");
                    $TOSLink.off();
                    $TOSLink.on("click", function() {
                        qcssapp.Router.navigate("tos-review", {trigger: true});
                    });
                    $TOSLink.show();
                }
            }
        );
    };

    //START Tour Navigation
    $(document).on('click', '.tour-navigation-next, .arrow-right', function(e) {
        var $thisPanel = $('.tour-panel.active');
        if(!($('.arrow-right').hasClass('hidden'))) {
            slideTour($thisPanel, true);
        }
        e.stopPropagation();
    });

    $('.tour-panel').swipe({
        swipeLeft: function(event, direction, distance, duration, fingerCount) {
            $('.tour-navigation-next').trigger('click');
        },
        swipeRight: function(event, direction, distance, duration, fingerCount) {
            $('.tour-navigation-prev').trigger('click');
        }
    });

    $(document).on('click', '.tour-navigation-prev, .arrow-left', function(e) {
        var $thisPanel = $('.tour-panel.active');
        if(!($('.arrow-left').hasClass('hidden'))) {
            slideTour($thisPanel, false);
        }

        e.stopPropagation();
    });

    //for the about/tour page - handles page transitions
    function slideTour($panel, forward) {
        var index = Number($panel.attr('data-tourindex'));
        var nextIndex = 0;
        $('.tour-navigation-item').eq(index).removeClass('current');  //remove dot color from current panel

        if(qcssapp.usingBranding) {
            var panelLength = $('.tour-panel').length;
            nextIndex = index;
            var found = false;
            var $tourPage = '';

            if ( forward ) {
                while(!found) {   //loop over each panel until you find one without the class 'disabled'
                    nextIndex ++;
                    $tourPage = $('#tour-page-' + nextIndex);
                    if(nextIndex == 2 || nextIndex == 3) {   //tour pages 2 and 3 conflict with the original tour pages id
                        $tourPage = $('#tour-page-' + nextIndex+'-1')
                    }
                    if(!($tourPage.hasClass('disabled'))) {
                        found = true;
                    }
                }
            } else {
                while(!found) {
                    nextIndex --;
                    $tourPage = $('#tour-page-' + nextIndex);
                    if(nextIndex == 2 || nextIndex == 3) {  //tour pages 2 and 3 conflict with the original tour pages id
                        $tourPage = $('#tour-page-' + nextIndex+'-1')
                    }
                    if(!($tourPage.hasClass('disabled'))) {
                        found = true;
                    }
                }
            }

            found = false;

            //check if there are any more panels after this one that are disabled, if not disable right arrow
            if(nextIndex < panelLength && forward) {
                for(var i=nextIndex+1;i<panelLength;i++) {
                    var tourPg =  $('#tour-page-' + i);
                    if(nextIndex == 2 || nextIndex == 3) {  //tour pages 2 and 3 conflict with the original tour pages id
                        tourPg =  $('#tour-page-' + i + '-1');
                    }
                    if(!(tourPg.hasClass('disabled'))) {
                        found = true;
                        break;
                    }
                }
                if(!found) {
                    $('.arrow-right').addClass('hidden');

                }
            } else {
                $('.arrow-right').removeClass('hidden');
            }

            if ( nextIndex > -1 && nextIndex < panelLength) {
                if(nextIndex != 0) { //if not on the first panel, show left arrow
                    $('.arrow-left').removeClass('hidden');
                } else {  //hide left arrow
                    $('.arrow-left').addClass('hidden');
                }
                var nextSelector = "#tour-page-" + nextIndex;
                if(nextIndex == 2 || nextIndex == 3) {
                    nextSelector = "#tour-page-" + nextIndex + '-1';
                }
                $('.tour-navigation-item').eq(nextIndex).addClass('current');  //change color or dots as the tour moves
                qcssapp.Functions.executeStandardRoute(nextSelector, '#tourview', forward ? 'slide' : 'slide-left');
            }
        } else {
            if ( forward ) {
                nextIndex = index + 1;
            } else {
                nextIndex = index - 1;
            }

            if ( nextIndex > -1 && nextIndex < 4 ) {
                if(nextIndex != 0) { //if not on the first panel, show left arrow
                    $('.arrow-left').removeClass('hidden');
                } else {  //hide left arrow
                    $('.arrow-left').addClass('hidden');
                }
                if(nextIndex != 3) { //if not on the first panel, show left arrow
                    $('.arrow-right').removeClass('hidden');
                } else {  //hide left arrow
                    $('.arrow-right').addClass('hidden');
                }
                var nextSelector1 = "#tour-page-" + nextIndex;
                $('.tour-navigation-item').eq(nextIndex).addClass('current');
                qcssapp.Functions.executeStandardRoute(nextSelector1, '#tourview', forward ? 'slide' : 'slide-left');
            }
        }
    }

})(qcssapp, jQuery);