;(function (qcssapp, $) {
    'use strict';

    //******* ORDERING RELATED FUNCTIONS *******/

    //call to POS API TransactionModel Inquire endpoint for reward/discount/tax considerations
    qcssapp.Functions.orderInquiry = function(orderObject, successFunction, errorFunction, errorParams, completeFunction, completeParams, preventShowMask = true, preventHideMask = true, fullScreenMask = false, transparency = .5) {
        var endPoint = qcssapp.Location + '/api/ordering/order/inquire';

        qcssapp.Functions.callAPI(
            endPoint,
            'POST',
            JSON.stringify(orderObject),
            '',
            successFunction,
            errorFunction,
            completeFunction,
            errorParams,
            30000,
            preventShowMask,
            preventHideMask,
            fullScreenMask,
            completeParams,
            false,
            transparency
        );
    };

    qcssapp.Functions.addProductToCart = function(productModel) {
        var cartProductID = 0;

        var numCartProducts = qcssapp.CurrentOrder.products.length;

        if ( numCartProducts > 0 ) {
            var lastProduct = qcssapp.CurrentOrder.products[numCartProducts-1];

            if ( lastProduct && Number(lastProduct.get('id')) >= 0 ) {
                cartProductID = lastProduct.get('id') + 1;
            }
        }

        productModel.set('id', cartProductID);

        if (qcssapp.DebugMode) {
            console.log('Number of cart products: ' + numCartProducts);
            console.log('Adding product with cartID: ' + cartProductID);
            console.log(productModel.attributes);
        }

        qcssapp.CurrentOrder.products.push(productModel);

        qcssapp.Functions.isGrabAndGoOrder();

        qcssapp.Functions.updateNumberOfProductsInCart();
    };

//    //adds product object to cart product array
//    qcssapp.Functions.addProductToCart = function(productModel, addCallback, addCallbackParams, skipOrderInquire, skipUpdateCartNum) {
//        //need a custom card ID because backbone does not allow same IDs in a collection
//        //also products with different modifiers would still have the same product ID
//        //preventing the ability to know which to update or delete easily if necessary
//        var cartProductID = 0;
//
//        var numCartProducts = qcssapp.CurrentOrder.products.length;
//
//        if ( numCartProducts > 0 ) {
//            var lastProduct = qcssapp.CurrentOrder.products[numCartProducts-1];
//
//            if ( lastProduct && Number(lastProduct.get('id')) >= 0 ) {
//                cartProductID = lastProduct.get('id') + 1;
//            }
//        }
//
//        productModel.set('id', cartProductID);
//
//        if (qcssapp.DebugMode) {
//            console.log('Number of cart products: ' + numCartProducts);
//            console.log('Adding product with cartID: ' + cartProductID);
//            console.log(productModel.attributes);
//        }
//
//        qcssapp.CurrentOrder.products.push(productModel);
//
//        //don't update when loading items into favorite order view
//        if(!skipUpdateCartNum) {
//            qcssapp.Functions.updateNumberOfProductsInCart();
//        }
//
//        qcssapp.Functions.isGrabAndGoOrder();
//
//        var numProducts = qcssapp.CurrentOrder.products.length;
//
//        //if from reorder only call order/inquire when all products have been added to cart because of async calls
//        if ( numProducts > 1 && !skipOrderInquire ) {
//            qcssapp.Functions.checkProductCombos(addCallback, addCallbackParams);
//
//        } else {
//            if ( typeof addCallback == 'function' ) {
//                addCallback(addCallbackParams);
//            }
//        }
//    };

    qcssapp.Functions.removeProductFromCart = function(cartProductID, onCartView) {
        // Get the index and remove the product from the CurrentOrder
        var index = qcssapp.Functions.getProductCartIndexByID(cartProductID);
        if (index >= 0) {
            var productModel = qcssapp.CurrentOrder.products[index];
            qcssapp.CurrentOrder.products.splice(index, 1);
            if (!onCartView) {
                qcssapp.OrderMenuHistory.pop();
            }

            // If this product was part of a combo, remove the combo from the CurrentOrder
            qcssapp.Functions.removeComboByProduct(productModel);
        }

        // Update the number of products in the cart
        qcssapp.Functions.updateNumberOfProductsInCart();

        // Check for combos and then refresh or navigate to the cart page
        if (onCartView) {
            qcssapp.Functions.checkProductCombos(qcssapp.Functions.resetCart);
        } else {
            qcssapp.Functions.checkProductCombos(qcssapp.Functions.navigateToCart);
        }

        // Check if the order is a Grab and Go Order
        qcssapp.Functions.isGrabAndGoOrder();
    };

    //get the cart product's index in the array by its cart product id
    qcssapp.Functions.getProductCartIndexByID = function(id) {
        var index = -1;

        $.each(qcssapp.CurrentOrder.products, function() {
            index++;
            if ( this.get('id') == id ) {
                return false;
            }
        });

        return index;
    };

    //submits the order object for transaction processing
    qcssapp.Functions.submitOrder = function() {
        var endPoint = qcssapp.Location + '/api/ordering/order/submit';

        qcssapp.Functions.checkASAPTime();

        qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(qcssapp.orderObject), '',
            function(data) {
                if( !qcssapp.Functions.isNumeric(data.orderNumber) ) {
                    //error message
                    if ( data && data.errorDetails && data.errorDetails.length > 0 ) {
                        qcssapp.Functions.displayError(data.errorDetails);
                        return;
                    }

                    qcssapp.Functions.displayError('Store is not currently accepting orders');
                    return;
                }

                var options = {'transactionModel': data};
                new qcssapp.Views.OrderPlacedView(options);
                qcssapp.Router.navigate('order-placed', {trigger: true});

            },
            function(errorParameters, errorCode, errorDetails) {
                if(errorCode == "E7800") {

                    var productRestrictions = errorDetails.substring('E7800 - Purchase Restriction on '.length, errorDetails.length-1);
                    var errorMsg =  "You are restricted from purchasing item <b>" + productRestrictions + "</b>. Please remove this item from your order before continuing";

                    //format the product names for the error message
                    if(productRestrictions.indexOf(",") != -1) {
                        var array =  productRestrictions.split(",");
                        var newArray = [];
                        $.each(array, function() {
                            newArray.push("<b>" + this + "</b>")
                        });
                        productRestrictions = newArray.join(", ");
                        productRestrictions = productRestrictions.substring(0, productRestrictions.lastIndexOf(",")) + ' and' + productRestrictions.substring(productRestrictions.lastIndexOf(",") + 1, productRestrictions.length);
                        errorMsg = "You are restricted from purchasing items </b>" + productRestrictions + "</b>. Please remove these items from your order before continuing";
                    }

                    qcssapp.Functions.displayError(errorMsg);
                }
            },
            function() {
                qcssapp.Functions.hideMask();
            },
            '',
            30000
        );
    };

    //for creating and navigating to the Receive view
    qcssapp.Functions.buildReceivePage = function(callback) {

        if (qcssapp.Functions.checkServerVersion(3,0) && qcssapp.CurrentOrder.expressReorder ) {
            qcssapp.Functions.expressReorder();

        } else if ( qcssapp.Functions.checkServerVersion(1,4) ) {
            var successFunction = function(data) {
                qcssapp.TimePickerModel = data;
                new qcssapp.Views.ReceiveView({
                    timePickerModel: data
                });

                var $receiveView = $('#receiveview');
                var $showReceive = $('#show-receive');

                $receiveView.find('h1').text('');
                $showReceive.attr('data-title', '');

                qcssapp.Functions.addHistoryRecord(0, 'receive');

                qcssapp.Router.navigate('#show-receive', {trigger: true});

                if(typeof callback !== 'undefined') {
                    callback();
                }

                if (qcssapp.Functions.checkServerVersion(4,1) && qcssapp.Functions.skipOrderDetails()) {
                    qcssapp.OrderMenuHistory.pop();
                    return;
                }

                $receiveView.find('h1').text('Order Details');
                $showReceive.attr('data-title', 'Order Details');

                if (!qcssapp.CurrentOrder.voucherCode) {
                    qcssapp.Functions.hideMask();
                }
            };

            qcssapp.Functions.createTimePicker(qcssapp.StoreSelected, qcssapp.CurrentOrder.orderType, successFunction, '');

        } else {
            new qcssapp.Views.ReceiveView({
                timePickerModel: ""
            });

            qcssapp.Functions.addHistoryRecord(0, 'receive');

            qcssapp.Router.navigate('#show-receive', {trigger: true});

            qcssapp.Functions.hideMask();
        }
    };

    //******* MISCELLANEOUS ORDERING FUNCTIONS *******/

    //returns the users Quickcharge balance and if there is a payment method setup before they enter the store
    qcssapp.Functions.getAccountBalanceDetails = function(successParameters, fromReorder) {
        var endPoint = qcssapp.Location + '/api/account/balance/details';

        //gets all details related to the account's balance and payment method
        qcssapp.Functions.callAPI(endPoint, 'GET', '', successParameters,
            function(response) {

                qcssapp.CurrentOrder.storePaymentModel = new qcssapp.Models.StorePaymentModel({
                    hasQuickChargeBalance: response['userBalance'] > 0,
                    paymentMethodCollection: response['accountPaymentMethod'].collection,
                    accountFundingName: response['accountFundingName'] ? response['accountFundingName'] : "",
                    allowMyQCFunding: true,
                    allowPayAsYouGo: true,
                    userBalance: response['userBalance']
                });

                if(response['accountFundingName'] && response['accountFundingName'] == "Payment Method") {
                    qcssapp.CurrentOrder.storePaymentModel.set('allowMyQCFunding', false);
                }

                if(response['allowMyQCFunding']) {
                    qcssapp.CurrentOrder.storePaymentModel.set('allowMyQCFunding', response['allowMyQCFunding']);
                }

                if (response.hasOwnProperty("allowPayAsYouGo")) {
                    qcssapp.CurrentOrder.storePaymentModel.set("allowPayAsYouGo", response["allowPayAsYouGo"]);
                }

                if (fromReorder) {
                    qcssapp.Functions.addHistoryRecord(successParameters.homeKeypadID, 'keypad');

                    if ( qcssapp.CurrentOrder.expressReorder && successParameters.message == "" ) {
                        qcssapp.Functions.checkForRewardsAndDiscounts();
                        return;
                    } else if (qcssapp.CurrentOrder.expressReorder) {
                        qcssapp.Functions.checkComboProducts();
                    }

                    qcssapp.Router.navigate('cart', {trigger: true});

                    if ( successParameters.message && successParameters.message.length > 0 ) {
                        qcssapp.Functions.displayError(successParameters.message, 'Warning');
                    }

                } else {

                    qcssapp.CurrentOrder.storeModel = successParameters.storeModel;
                    qcssapp.Functions.setStoreSettings();

                    var callback = function() {
                        var homeKeypadID = successParameters.storeModel.get('homeKeypadID');

                        if(qcssapp.CurrentOrder.expressReorder) {
                            qcssapp.Functions.addHistoryRecord(homeKeypadID, 'keypad');

                            qcssapp.Functions.checkForRewardsAndDiscounts();
                            return;
                        }

                        qcssapp.Functions.loadKeypad(homeKeypadID);
                    };

                    if( !qcssapp.Functions.isCreditCardAllowed() && !qcssapp.Functions.getStorePaymentSetting('allowMyQCFunding', true) &&  response['userBalance'] < 10 ) {
                        qcssapp.Functions.hideMask();
                        qcssapp.Functions.displayPopup('Your available balance is ' + qcssapp.Functions.formatPriceInApp(response['userBalance']) + '. You will not be able to complete a transaction more than this amount', 'Warning', 'OK', callback);
                        return;
                    }

                    callback();
                }

            },
            function() {
                //display error message to user
                qcssapp.Functions.displayError('There was an error loading the account balance, please try again later.');
                qcssapp.Functions.hideMask();
            },
            '',
            '',
            '',
            false,
            true
        );
    };

    //builds a JSON array of the current rewards in the correct format for the back end POS API
    qcssapp.Functions.buildRewardsArray = function(rewardsSelected, includeAutoRewards) {
        //set up rewards for OrderModel
        var rewardArr = [];
        var combinedRewards = [];

        if ( rewardsSelected == null || rewardsSelected.length == 0 ) {
            $.each(qcssapp.CurrentOrder.selectedRewards, function(i) {
                combinedRewards.push(qcssapp.CurrentOrder.selectedRewards[i]);
            });
        } else {
            $.each(rewardsSelected, function(i) {
                combinedRewards.push(rewardsSelected[i]);
            });
        }

        if ( includeAutoRewards ) {
            $.each(qcssapp.CurrentOrder.automaticRewards, function(i) {
                combinedRewards.push(qcssapp.CurrentOrder.automaticRewards[i]);
            });
        }

        $.each(combinedRewards, function() {
            if ( !this || !this.get ) {
                console.log("in rewards selected ERROR:" + combinedRewards);
                return true;
            }

            var quantity = this.get('quantity');

            //LoyaltyRewardLineItem has a quantity property but it is NOT supported as of 2/1/18
            var reward = {
                id: this.get('id'),
                name: this.get('name'),
                rewardTypeId: this.get('rewardTypeID'),
                maxPerTransaction: this.get('maxPerTransaction'),
                pointsToRedeem: this.get('pointsToRedeem'),
                value: this.get('value'),
                autoPayout: this.get('autoPayout')
            };

            for ( var i = 0; i < quantity; i++ ) {
                var rewardLine = {
                    reward: reward,
                    amount: -1 * Number(reward.value)
                };

                rewardArr.push(rewardLine);
            }
        });

        return rewardArr;
    };

    //loads the receipt data from an html string into the receipt view
    qcssapp.Functions.viewReceipt = function(transactionID, showReorder, showFavorites, fromLocation, rewardsProgramID, txnView) {
        var preventHideMask = !!(qcssapp.Functions.checkServerVersion(3,0));

        qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/transactions/receipt/' + transactionID, 'GET',
            '',
            '',
            function(response) {
                if ( response ) {
                    $('.general-back-icon').show();
                    var receiptOptions = {
                        transactionID : transactionID,
                        showReorder : showReorder,
                        showFavorites : showFavorites,
                        fromLocation : fromLocation,
                        content: response.content,
                        rewardsProgramID : rewardsProgramID,
                        txnView: txnView
                    };

                    if ( fromLocation == 'rewards' || fromLocation == 'purchases') {
                        var rewardsControllerModel = new qcssapp.Models.Controller({
                            id: 'ReceiptView',
                            route: 'receipt',
                            options: receiptOptions,
                            destructible: true,
                            destroyOn: ['any']
                        });
                        qcssapp.Controller.activateView(rewardsControllerModel);
                    } else {
                        new qcssapp.Views.ReceiptView(receiptOptions);

                        qcssapp.Router.navigate('receipt', {trigger: true});
                    }
                    //scroll back to top of view
                    $('#receipt').scrollTop(0);
                } else {
                    qcssapp.Functions.displayError("Could not retrieve receipt for the chosen transaction.");
                }
            }, '', '', '', '', false, preventHideMask
        );
    };

    qcssapp.Functions.openTransactionAlert = function(msg) {
        var content = '<div class="open-txn-alert">'+msg+'<br><br> Place the order?</div>';
        $.confirm({
            title: '',
            content: content,
            buttons: {
                cancel: {
                    text: 'No',
                    btnClass: 'btn-default open-txn-alert-btn prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                    action: function() {
                        qcssapp.Functions.hideMask();
                    }
                },
                confirm: {
                    text: 'Yes',
                    btnClass: 'btn-default open-txn-alert-btn prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                    action: function() {
                        qcssapp.Functions.submitOrder();
                    }
                }
            }
        });
    };

    qcssapp.Functions.buildCurrentOrderToken = function() {
        if(!qcssapp.Functions.checkServerVersion(3,0,62)) {
            return;
        }

        var endPoint = qcssapp.Location + '/api/myqc/order/token';

        qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
            function(result) {
                if(!result.dskey) {
                    console.log('Error creating a valid token');
                    return;
                }

                qcssapp.CurrentOrder.token = result.dskey.toString();
            },
            '', '', '', '', false, true
        );
    };

    qcssapp.Functions.isGrabAndGoOrder = function() {
        qcssapp.CurrentOrder.grabAndGo = false;

        if(!qcssapp.Functions.checkServerVersion(4,0,58)) {
            return;
        }

        var printerMappedProduct = false;

        $.each(qcssapp.CurrentOrder.products, function(index, product) {
            if(product.get('hasPrinterMappings')) {
                printerMappedProduct = true;
                return false;
            } else if (product.get('modifiers') && product.get('modifiers').length > 0) {

                $.each(product.get('modifiers') , function(index, mod) {
                    if(mod.hasPrinterMappings) {
                        printerMappedProduct = true;
                        return false;
                    }
                });

                if(printerMappedProduct) {
                    return false;
                }
            }
        });

        if(!printerMappedProduct) {
            qcssapp.CurrentOrder.grabAndGo = true;
        }
    };

    qcssapp.Functions.hasOrderInProgress = function(){
        return qcssapp.CurrentOrder.products.length > 0;
    };

    qcssapp.Functions.formatASAP = function(timeASAP) {
        var setASAP = moment(timeASAP).format("hh:mm A");
        var hour = setASAP.substr(0, setASAP.indexOf(':'));
        var min = setASAP.substr(setASAP.indexOf(':'), setASAP.length);

        if ( hour.charAt(0)=="0" ) {
            setASAP = hour.charAt(1) + min;
        }

        qcssapp.asapTime = setASAP;
        return setASAP;
    };

    qcssapp.Functions.setASAPTime = function(timeObj, prepTime, $pageElement) {
        var timeASAP = moment();
        if ( qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('clientTimezoneOffset') && !(qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('minNumDaysForFutureOrders') && qcssapp.CurrentOrder.storeModel.get('minNumDaysForFutureOrders') > 0) ) {
            var clientTimezoneOffset = qcssapp.CurrentOrder.storeModel.get('clientTimezoneOffset');
            if ( (Number(clientTimezoneOffset) > 600 || (Number(clientTimezoneOffset)*-1) > 600) && $pageElement != '' ) {
                $pageElement.addClass('showTimeDiffError');
            }
            timeASAP = timeASAP.add(clientTimezoneOffset,'s');
        }

        var openTimes = timeObj.openTimes;
        var closeTimes = timeObj.closeTimes;

        //order time has to be within an interval of store open/close times
        for ( var i = 0; i < openTimes.length; i++ ) {
            var readyTime = moment();
            var open = moment(openTimes[i], "hh:mm A");
            var close = moment(closeTimes[i], "hh:mm A");
            if (closeTimes[i] === "12:00 AM") {
                close = close.add(1, "day");
            }

            //if the store isn't open yet then the prep time has to be applied to the open time
            if(timeASAP.isBefore(open)) {
                var openTime = moment(open);
                readyTime = openTime.add(prepTime, 'minutes');
            } else {
                readyTime = timeASAP.add(prepTime, 'minutes');
            }

            var orderReady = moment(readyTime);

            close = close.add(prepTime, 'minutes');

            //if the prep time is 0 then the open will be the same as the orderReady, shouldn't be configured this way but still need to handle scenario
            if(prepTime == 0 && open.isSame(orderReady) && (close.isAfter(orderReady) || close.isSame(orderReady, "minute"))) {
                return readyTime;
            }

            if ( open.isBefore(orderReady) && (close.isAfter(orderReady) || close.isSame(orderReady, "minute"))) {
                return readyTime;
            }
        }
    };

    qcssapp.Functions.checkASAPTime = function() {

        if( !qcssapp.CurrentOrder.expressReorder && qcssapp.orderObject.time.indexOf('ASAP') != -1 && typeof qcssapp.TimePickerModel !== 'undefined') {
            var prepTime = qcssapp.CurrentOrder.orderType == "delivery" ? Number(qcssapp.CurrentOrder.storeModel.get('pickUpPrepTime')) + Number(qcssapp.CurrentOrder.storeModel.get('deliveryPrepTime')) : Number(qcssapp.CurrentOrder.storeModel.get('pickUpPrepTime'));
            var timeASAP = qcssapp.Functions.setASAPTime(qcssapp.TimePickerModel, prepTime, '');
            timeASAP = qcssapp.Functions.formatASAP(timeASAP);
            qcssapp.orderObject.time = 'ASAP (' +timeASAP + ')';

        } else if (qcssapp.CurrentOrder.expressReorder) {
            qcssapp.orderObject.time = "";
        }
    };


    //******************* RESUME ORDERING RELATED FUNCTION *************//

    qcssapp.Functions.resumeOrdering = function(paymentMethod, errorMessage) {
        var order = qcssapp.Functions.getOrderInSession();
        var orderHistory = qcssapp.Functions.getOrderHistoryInSession();
        qcssapp.Functions.clearOrderInSession();

        if(!order.storeModel) {
            qcssapp.Functions.initializeApplication();
            return;
        }

        qcssapp.OrderMenuHistory = orderHistory;

        qcssapp.Functions.updateStoreModel(order);

        qcssapp.Functions.updateProductModels(order);

        qcssapp.Functions.updateComboModels(order);

        qcssapp.Functions.updateRewardModels(order);

        // If there was an error saving credit card, show error on Order Details page
        if(errorMessage) {
            var callback = function() {
                qcssapp.Functions.displayPopup(errorMessage, 'Warning', 'OK');
            };

            qcssapp.Functions.buildReceivePage(callback);
            return;
        }

        // If a payment method was successfully saved, update in StorePaymentModel
        if(paymentMethod != null) {
            var paymentMethodCollection = [];
            paymentMethodCollection.push(paymentMethod);
            qcssapp.CurrentOrder.storePaymentModel.set('paymentMethodCollection', paymentMethodCollection);
        }

        // Navigate to Order Details page
        qcssapp.Functions.buildReceivePage();

    };

    // Update the storeModel and storePaymentModel on the CurrentOrder
    qcssapp.Functions.updateStoreModel = function(order) {
        var storeModel = new qcssapp.Models.OrderStore(order.storeModel, {parse: true});
        var storePaymentModel = new qcssapp.Models.StorePaymentModel(order.storePaymentModel, {parse: true});

        if(storePaymentModel.get('paymentProcessor') != null) {
            var paymentProcessor = new qcssapp.Models.PaymentProcessor(storePaymentModel.get('paymentProcessor'));
            storePaymentModel.set('paymentProcessor', paymentProcessor);
        }

        qcssapp.CurrentOrder = order;
        qcssapp.CurrentOrder.storeModel = storeModel;
        qcssapp.CurrentOrder.storePaymentModel = storePaymentModel;

        // Update the store for the session
        qcssapp.Functions.setStoreSettings();
        qcssapp.StoreSelected = storeModel.get('id');
        qcssapp.Functions.setSession(qcssapp.Session, qcssapp.Location, qcssapp.StoreSelected, qcssapp.UserCode);
    };

    // Update the products on the CurrentOrder
    qcssapp.Functions.updateProductModels = function(order) {
        var productArray = [];
        $.each(order.products, function(index, product) {
            var productModel = new qcssapp.Models.Product(product, {parse: true});
            productArray.push(productModel);
        });

        qcssapp.CurrentOrder.products = productArray;
    };

    // Update the combos on the CurrentOrder
    qcssapp.Functions.updateComboModels = function(order) {
        if(order.combos) {
            var comboCollection = new qcssapp.Collections.Combo();
            comboCollection.set(order.combos, {parse:order.combos});
            qcssapp.CurrentOrder.combos = comboCollection;
        }
    };

    // Update the selectedRewards on the CurrentOrder
    qcssapp.Functions.updateRewardModels = function(order) {
        if(order.selectedRewards) {
            var rewardArray = [];
            $.each(order.selectedRewards, function(index, reward) {
                var rewardModel = new qcssapp.Models.Reward(reward, {parse: true});
                rewardArray.push(rewardModel);
            });

            qcssapp.CurrentOrder.selectedRewards = rewardArray;
        }
    };

    // moved from review-view.js
    //Defect 4062: specific functionality to account for the case where an express order was made while credit card tender was available on a terminal, then at some point after
    //it is disabled. Express order will still save credit card tender, leading to purchase restrictions being ignored.
    //D-4295: amend above bug fix so that the user will be brought to the cart only if the express order had a credit card and the terminal no longer uses a credit card
    qcssapp.Functions.returnToCartIfIsExpressOrderAndTerminalInfoChanged = function() {
        if (qcssapp.CurrentOrder.expressReorder) {
            if (qcssapp.CurrentOrder.expressReorderModel.storeModel && qcssapp.CurrentOrder.expressReorderModel.storeModel.attributes) {
                if (!qcssapp.CurrentOrder.expressReorderModel.storeModel.get('useOnlineOrderingFunding') && (qcssapp.CurrentOrder.expressReorderModel.storeModel.get('useFundingTerminal'))) {
                    if (qcssapp.CurrentOrder.expressReorderModel.usingCreditCardAsTender && !qcssapp.Functions.isCreditCardAllowed()) {
                        qcssapp.Functions.navigateToCart();
                        return true;
                    }
                }
            }
        }
        return false;
    }

})(qcssapp, jQuery);