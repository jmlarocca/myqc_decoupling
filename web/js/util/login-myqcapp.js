;(function (qcssapp, $) {
    'use strict';

    //******* LOGIN RELATED FUNCTIONS *******/

    //checks the login settings (such as showKeepLogged) for the given endpoint
    qcssapp.Functions.checkLoginSettings = function(endPoint, successParameters, successFunction) {
        //TODO: Adjust callAPI parameters
        qcssapp.Functions.callAPI(endPoint, 'GET', '', successParameters,
            function(data, successParameters) {
                var $keepLogged = $('label[for=keepLogged]');

                if ( data.showKeepLogged === "true" || data.showKeepLogged === true ) {
                    $keepLogged.show();
                } else {
                    $keepLogged.hide();
                }

                if ( data.hasOwnProperty('APIVersion') || data.hasOwnProperty('apiversion')) {
                    qcssapp.ServerVersion = data.APIVersion || data.apiversion;
                    $('.app-version').text(qcssapp.AppVersion + ' / ' + qcssapp.ServerVersion);
                }

                if ( data.hasOwnProperty('importAllInactive') ) {
                    qcssapp.importAllInactive = data.importAllInactive;
                }

                if ( data.hasOwnProperty('globalAuthType') ) {
                    qcssapp.globalAuthType = data.globalAuthType;
                }

                if ( data.hasOwnProperty('onGround') ) {
                    qcssapp.onGround = typeof data.onGround == 'boolean' ? data.onGround : (data.onGround === "true");
                }

                if ( data.hasOwnProperty('showTour') ) {
                    qcssapp.showTour = typeof data.showTour == 'boolean' ? data.showTour : (data.showTour === "true");
                }

                var $logoutQCAuthMsg = $('#logoutQCAuthMsg');
                if ( data.showQCAuthLoginLinkMYQC === "true" || data.showQCAuthLoginLinkMYQC === true ) {
                    $logoutQCAuthMsg.show();
                } else {
                    $logoutQCAuthMsg.hide();
                }

                if ( typeof successFunction == 'function' ) {
                    successFunction(successParameters);
                }

                if ( data.hasOwnProperty('qcbaseURL') && data['qcbaseURL'].trim() != '' ) {
                    qcssapp.qcBaseURL = data['qcbaseURL'];
                } else {
                    qcssapp.qcBaseURL = '';
                }

                qcssapp.Location = successParameters.location;

                if (successParameters.registerToken) {
                    // Register FCM token
                    qcssapp.Functions.registerToken();
                }

                qcssapp.Functions.determineBranding(data);

                qcssapp.Functions.setHealthyIndicatorIcons(data);

                qcssapp.Functions.accountCreation(data);

                qcssapp.Functions.enableFingerprint(data);

                qcssapp.Functions.setAliases(data);

                qcssapp.Functions.setHelpfulHintsAndFAQS(data);

                qcssapp.Functions.setSelectStoreHeader(data);

                qcssapp.Functions.setCurrencySettings(data);
            },
            '',
            '',
            '',
            '',
            true,
            true,
            true
        )
    };

    //retrieves and sets the session parameters when resuming the app from being redirected to the native browser
    qcssapp.Functions.resumeSession = function() {
        //check for session in session (phone) or localstorage (browser)
        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        var userSession = mdl.get('sessionID');
        //get the location from the saved localStorage session
        var userLocation = mdl.get('location');
        var userCode = mdl.get('code');

        if (qcssapp.onPhoneApp == null ) {
            qcssapp.onPhoneApp = (typeof window.cordova !== "undefined");
        }

        if ( !qcssapp.Session ) {
            if (userSession == '') {
                if (!qcssapp.onPhoneApp && history && history.replaceState) {
                    history.replaceState("?", "title", "/myqc/");
                }
                qcssapp.Functions.resetOnLogout();
                qcssapp.Functions.displayError("Your session has expired, please log in and try again.", 'Session Expired', '#login-page');
                return;
            }

            //reset the resumeApp flag
            mdl.set('resumeApp', false);
            mdl.save();

            //set the session and location
            qcssapp.Functions.setSession(userSession, userLocation, '', userCode);

            // Register FCM token
            qcssapp.Functions.registerToken();

            if (userCode == "SSO") {
                qcssapp.ssoLogin = true;
            }
        } else {
            //reset the resumeApp flag
            mdl.set('resumeApp', false);
            mdl.save();
        }
    };

    qcssapp.Functions.resumeFunding = function(status, transactionID) {

        //determine next step by checking the status
        var loginCheckSuccessFn;

        //replace the get parameters to hide them from the user
        if (!qcssapp.onPhoneApp && history && history.replaceState) {
            history.replaceState("?", "title", "/myqc/");
        }

        if ( status == "E" || status == "D" ) {
            loginCheckSuccessFn = function () {
                var errorMessage = 'An error occurred while attempting to save payment method, please try again.';

                if(qcssapp.Functions.getOrderInSession() != '') {
                    qcssapp.Functions.resumeOrdering(null, errorMessage);
                    return;
                }

                qcssapp.Functions.displayError(errorMessage);
            }
        } else if ( status == "R" ) {
            loginCheckSuccessFn = function () {
                var errorMessage = 'Could not save payment method, card was rejected. Please try a different card.';

                if(qcssapp.Functions.getOrderInSession() != '') {
                    qcssapp.Functions.resumeOrdering(null, errorMessage);
                    return;
                }

                qcssapp.Functions.displayError(errorMessage);
            }
        } else if ( status == "C" ) {
            loginCheckSuccessFn = function () {
                if(qcssapp.Functions.getOrderInSession() != '') {
                    qcssapp.Functions.resumeOrdering(null);
                    return;
                }

                qcssapp.Functions.displayError('Adding Payment Method was cancelled, request may have timed out. Please try again.');
            }
        } else if ( status == "A" ) {
            loginCheckSuccessFn = function () {
                //make a call to funding resource create/default with the transactionID filled in
                var endPoint = qcssapp.Location + '/api/funding/account/payment/default/create';

                var paymentObject = {
                    "paymentProcessorID": 6, //FreedomPay ID
                    "processorPaymentOneTimeToken": transactionID
                };

                qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(paymentObject), '',
                    function (response) {
                        //check for valid response
                        if (!response || !response.id || !qcssapp.Functions.isNumeric(response.id)) {
                            qcssapp.Functions.displayError('An error occurred while attempting to save payment method, please try again.', 'Error');
                            return;
                        }

                        if(qcssapp.Functions.getOrderInSession() != '') {
                            qcssapp.Functions.resumeOrdering(response);
                            return;
                        }

                        setTimeout(function() {
                            //reset account funding page
                            var $accountFunding = $('#accountFunding');
                            $accountFunding.off();
                            $accountFunding.find('.template-gen').remove();

                            //build account funding view
                            new qcssapp.Views.AccountFundingView();

                            //bring user to account funding
                            qcssapp.Router.navigate('account-funding', {trigger: true});
                        }, 300);
                    },
                    function () {
                        qcssapp.Functions.displayError('An error occurred while attempting to save payment method, please try again.', 'Error');
                    },
                    function () {
                        //replace the get parameters to hide them from the user
                        if (!qcssapp.onPhoneApp && history && history.replaceState) {
                            history.replaceState("?", "title", "/myqc/");
                        }
                    }
                );
            };
        }

        var loginCheckErrorFn = function() {
            if (!qcssapp.onPhoneApp && history && history.replaceState) {
                history.replaceState("?", "title", "/myqc/");
            }
            qcssapp.Functions.throwFatalError("No user session found, please log in again", "login");
        };

        qcssapp.preventUnloadLogout = false;
        qcssapp.Functions.loginCheck(loginCheckSuccessFn, loginCheckErrorFn);
    };

    qcssapp.Functions.loginCheckHandler = function(data) {
        if ( data.result == "yes" ) {
            if ( qcssapp.UserCode == "SSO" ) {
                qcssapp.ssoLogin = true;
            }

            //check if the result has a .personID key, if so set the global personModel
            if ( data.personID ) {
                if ( !qcssapp.personModel ) {
                    qcssapp.personModel = new qcssapp.Models.Person();
                }
                qcssapp.personModel.set('personID', data.personID);

                //account model will return null if the account is inactive
                if ( !data.accountModel ) {
                    //go to select account to manage view (no back button)
                    if(qcssapp.Functions.fingerprintEnabled() && !qcssapp.verifyFingerprintFromLogin) {
                        qcssapp.Functions.verifyFingerprintOnDevice();
                        qcssapp.verifyFingerprintFromLogin = true;
                    }

                    qcssapp.Functions.navigateToManageAccount(false);
                    return;  //need this or it goes to NavView and calls DetailView which throws error if no account model
                }
            }

            //also if for a person, check through and set the .accountModel
            if ( data.accountModel ) {
                var accountModel = new qcssapp.Models.AccountInfoModel();
                accountModel.set('name', data.accountModel.name);
                accountModel.set('accountNumber', data.accountModel.number);
                accountModel.set('badgeNumber', data.accountModel.badge);
                accountModel.set('accountStatus', data.accountModel.status);
                accountModel.set('accountType', data.accountModel.accountTypeId);
                accountModel.set('phone', data.accountModel.phone);
                accountModel.set('orgLevelPrimaryId', data.accountModel.orgLevelPrimaryId);
                accountModel.set('employeeLowBalanceThreshold', data.accountModel.employeeLowBalanceThreshold);

                if ( data.accountModel.hasOwnProperty('badgeNumPrefix') ) {
                    accountModel.set('badgeNumPrefix', data.accountModel.badgeNumPrefix);
                }

                if ( data.accountModel.hasOwnProperty('badgeNumSuffix') ) {
                    accountModel.set('badgeNumSuffix', data.accountModel.badgeNumSuffix);
                }

                if ( data.accountModel.hasOwnProperty('mobilePhone') ) {
                    accountModel.set('mobilePhone', data.accountModel.mobilePhone);
                }

                if ( data.accountModel.hasOwnProperty('email') ) {
                    accountModel.set('email', data.accountModel.email);
                }

                if ( data.accountModel.hasOwnProperty('qrCodeLength') ) {
                    accountModel.set('qrCodeLength', data.accountModel.qrCodeLength);
                }

                qcssapp.accountModel = accountModel;
            }

            if ( !$('#nav-about').length ) {
                //start app using saved session to auth
                qcssapp.Functions.initializeApplication();
                qcssapp.Functions.checkPersonAccountEmail();
            } else {
                qcssapp.Router.navigate('main', {trigger: true});
            }

            setTimeout(function() {
                //TODO: extract this to a common function
                if ( navigator && navigator.splashscreen ) {
                    navigator.splashscreen.hide();
                }
            },400);
        } else {
            //check for SSO code, and if found redirect to the logout view
            if ( qcssapp.UserCode == "SSO" ) {
                new qcssapp.Views.LogoutView();
                qcssapp.Router.navigate('logout-page', {trigger:true});

                setTimeout(function() {
                    //TODO: extract this to a common function
                    if ( navigator && navigator.splashscreen ) {
                        navigator.splashscreen.hide();
                    }
                },400);

                return;
            }

            //otherwise just reset any generated content and stay on the login view
            qcssapp.Functions.resetOnLogout();
        }
    };

    //determines the validity of the dskey
    qcssapp.Functions.loginCheck = function(successFunction, errorFunction) {
        qcssapp.Functions.callAPI(qcssapp.Location + '/api/auth/login/check', 'GET', '', '',
            function(data) {
                qcssapp.Functions.loginCheckHandler(data);

                if ( successFunction && typeof successFunction == 'function' ) {
                    successFunction(data);
                }
            },
            errorFunction
        );
    };

    //sets the first login flag and saves it to local storage
    qcssapp.Functions.setFirstLogin = function() {
        var mdl = new qcssapp.Models.FirstLoginModel({
            id: 1,
            logged: 'true'
        }, {parse:true});
        mdl.save();
    };

    //handles sso login
    qcssapp.Functions.handleSSOLogin = function(ssoValidation, location) {
        if ( !ssoValidation ) {
            alert('no validation object found');
            return;
        }

        ssoValidation = ssoValidation.replace(/%7B/g, '{').replace(/%7D/g, '}').replace(/%22/g, '"').replace(/%5B/g, '[').replace(/%5D/g, ']');
        var ssoValidationObject = JSON.parse(ssoValidation);
        if ( !ssoValidationObject.hasOwnProperty('IsValid') || !ssoValidationObject.hasOwnProperty('DSKey') ) {
            qcssapp.Functions.resetOnLogout(true);
            new qcssapp.Views.LogoutView();
            qcssapp.Router.navigate('logout-page', {trigger:true});
            qcssapp.Functions.displayError('Invalid login attempt');
            return;
        }

        var dskey = ssoValidationObject.DSKey;
        var isValid = ssoValidationObject.IsValid;

        if ( !isValid ) {
            var errorDetails = "An error occurred while attempting to log in";
            if ( ssoValidationObject.hasOwnProperty("errorDetails") && ssoValidationObject.errorDetails.length > 0 ) {
                errorDetails = ssoValidationObject.errorDetails;
            }

            qcssapp.Functions.displayError(errorDetails);
            return;
        }

        var tosID = ssoValidationObject.TOS && ssoValidationObject.TOS.length ? ssoValidationObject.TOS[0].id.toString() : '';
        var userID = -1*ssoValidationObject.instanceUserID;

        var dataParms = JSON.stringify({
            tosID: tosID,
            userID: userID
        });

        var successParms = {
            showTOS: Number(tosID) > 0
        };

        //create session
        qcssapp.Functions.createSession(dskey, location, 'SSO', false, false, qcssapp.Functions.fingerprintEnabled());
        qcssapp.Functions.setFirstLogin();
        qcssapp.ssoLogin = true;

        // Register FCM token
        qcssapp.Functions.registerToken();

        //validate dskey and look up tos content if necessary
        //TODO: Adjust callAPI parameters
        qcssapp.Functions.callAPI(qcssapp.Location + '/api/auth/login/verifysso', 'POST', dataParms, successParms,
            function(data, successParms) {
                if ( data.result == "yes" ) {
                    //create special SSO code if not on mobile app
                    var instanceCodeCollection = new qcssapp.Collections.instanceCodes();
                    if ( typeof window.cordova == 'undefined' ) {
                        var codeModel = new qcssapp.Models.instanceCode();
                        codeModel.set('CODE', 'SSO');
                        codeModel.set('LOCATION', location);
                        codeModel.set('USED', 1);
                        instanceCodeCollection.create(codeModel);
                    } else {
                        //set used flag for code on app
                        instanceCodeCollection.fetch();
                        var ssoCodeModel = instanceCodeCollection.get("SSO");
                        ssoCodeModel.set("USED", 1);
                        ssoCodeModel.save();
                    }

                    setTimeout(function() {
                        //TODO: extract this to a common function
                        if ( navigator && navigator.splashscreen ) {
                            navigator.splashscreen.hide();
                        }
                    },400);

                    qcssapp.loggedOutFingerprint = false;

                    if ( !successParms.showTOS ) {
                        qcssapp.Functions.initializeApplication();
                        return;
                    }

                    qcssapp.Functions.showTOS(data);
                } else {
                    qcssapp.Functions.resetOnLogout();
                    qcssapp.ssoLogin = false;
                    qcssapp.Functions.displayError(data.errorDetails);
                }
            }
        );
    };

    //****** LOGOUT FUNCTIONS *****/

    //attempts to log user out on back end, always destroys front end session
    qcssapp.Functions.logOut = function() {
        var endPoint = qcssapp.Location + '/api/auth/logout';
        //TODO: Adjust callAPI parameters
        qcssapp.Functions.callAPI( endPoint, 'POST', '', '',
            function(result) {
                if (qcssapp.DebugMode) {
                    console.log("Successfully requested logout.");
                }
                if (result.status == 'success') {
                    if (qcssapp.DebugMode) {
                        console.log("Successfully logged user out on back-end.");
                    }
                } else {
                    console.log("WARNING: Failed to log out user on back-end. Result object...");
                }
            },
            function() {
                console.log("WARNING: An error occurred when attempting to access the /logout end point.");
            },
            function() {
                //TODO: on mobile check
                if ( typeof window.cordova == "undefined" ) {
                    qcssapp.Functions.destroySession();
                    if(!qcssapp.ssoLogin){
                        qcssapp.Router.navigate('login-page', {trigger: true});
                        location.reload(true);
                    }
                } else {
                    qcssapp.Functions.resetOnLogout();
                }
            },
            '',
            '',
            '',
            '',
            true
        );
    };

    //handler for logout resetting
    qcssapp.Functions.resetOnLogout = function(preventLoginNavigate) {
        var $loginName = $('#loginName');
        var $loginPass = $('#loginPassword');

        //TODO: extract this to a common function
        //hide splash screen
        if ( navigator && navigator.splashscreen ) {
            navigator.splashscreen.hide();
        }

        qcssapp.Functions.resetOrdering(false);

        if (qcssapp.Functions.fingerprintEnabled() && !qcssapp.ssoLogin) {

            //if hit cancel in verify fingerprint OR logged out, set flag to identify this case and don't kill session
            if (!qcssapp.loggedOutFingerprint && ((navigator.userAgent.match(/Android/i) && window.location.hash != '') || !navigator.userAgent.match(/Android/i))) {
                qcssapp.Functions.isFingerprintAvailableOnDevice(true);

            } else {
                qcssapp.loggedOutFingerprint = false;
                qcssapp.verifyFingerprintFromLogin = false;

                $('#loginName, #loginPassword, label[for=keepLogged]').show();

                //if the DSKey expired, want to keep enabledFingerprint on
                if((navigator.userAgent.match(/Android/i) && window.location.hash == '') || (!navigator.userAgent.match(/Android/i) && window.location.hash.indexOf("login-page") == -1)) {
                    $('#enableFingerprint').prop('checked',true);
                    $('label[for=keepLogged]').hide();

                    //if turning off enable fingerprint after logout out or cancelling verification
                } else {
                    $('#enableFingerprint').prop('checked',false);
                }
            }
        } else {
            //reset session
            qcssapp.Functions.destroySession();
            qcssapp.personModel = null;
            qcssapp.accountModel = null;

        }

        //reset login form
        $loginPass.val('');
        $loginName.val('');
        $('#keepLogged').prop('checked',false);

        //TODO: unnecessary off call due to not cleaning up views properly
        //reset event handlers for infinite scroll
        $('#show-purchases').off();

        //TODO: none of this block should be necessary if views are removed properly
        //remove generated content
        $('.template-gen').remove();
        $('.template-gen-container').children().not('.persist').remove();
        $('.error-msg-container').html('').hide();
        $('#globalBalanceContainer').show();
        $('#spendingProfileTitle').show();
        $('#spendingProfileSelect').show();
        $('#spendingProfileButtonContainer').show();

        //reset QR code
        $('#quickPay-code-container').html('');

        //reset account funding payment method
        $('#accountFunding_paymentMethod_saved-method').html('');

        //move to login page
        if ( preventLoginNavigate ) {
            return;
        }

        qcssapp.Router.navigate('login-page', {trigger:true});

        $loginName.trigger('focus');
        qcssapp.Functions.checkLoginNameParameter();

        qcssapp.Functions.hideMask();
    };

    //****** ACCOUNT LOGIN FUNCTIONS *****/

    //checks if there are available licenses
    qcssapp.Functions.checkAccountLicenseAvailability = function(successParameters) {
        if ( !qcssapp.ServerVersion || qcssapp.importAllInactive != "true" ) {
            qcssapp.Functions.showTOS(successParameters);
            return;
        }

        var endPoint = qcssapp.Location+ '/api/auth/licenses';

        //TODO: Adjust callAPI parameters
        qcssapp.Functions.callAPI(endPoint,'POST', '', successParameters,
            function(result, successParameters) {
                if(result) {
                    qcssapp.Functions.showTOS(successParameters);
                } else {
                    var errorMsg ='Cannot display Terms of Service agreement due to insufficient licenses. Please contact your supervisor for more information.';
                    qcssapp.Functions.throwFatalError(errorMsg, 'login');
                }
            },
            function(params) {
                if ( qcssapp.DebugMode ) {
                    console.log('ERROR response for license check');
                }

                qcssapp.Functions.displayError('Error on license check.');
            },
            '',
            '',
            '',
            true,
            true
        );

    };

    //creates TOS view and navigates to it, if able, or errors if not
    qcssapp.Functions.showTOS = function(params) {
        //if TOS accept needed
        if ( Object.keys(params.tos[0]).length > 0 ) {
            // DECODE HTML
            if(typeof params.tos[0].content !== "undefined"){
                params.tos[0].content = qcssapp.Functions.htmlDecode(params.tos[0].content).replace(/_nbsp/g, "&nbsp;");
            }
            new qcssapp.Views.tosView({
                tos: params.tos
            });

            qcssapp.Router.navigate('#tos-page', {trigger:true});
        } else {
            qcssapp.Functions.displayError('Error loading terms of service.');
        }
    };

    //****** PERSON ACCOUNT LOGIN FUNCTIONS *****/

    //check if there are employees mapped to this personid
    qcssapp.Functions.navigateToManageAccount = function(autoSelect) {

        var options = {'autoSelect': autoSelect};  //if there is only one mapping, should it be automatically selected or not

        var manageAccountControllerModel = new qcssapp.Models.Controller({
            id: 'ManageAccountView',
            route: 'manage-account',
            options: options,
            destructible: true,
            destroyOn: ['NavView']
        });
        qcssapp.Controller.activateView(manageAccountControllerModel);
    };

    //****** LOGIN EVENT HANDLERS *****/

    // Add event handler for logoutQCAuthMsg
    $(document).on('click', '#logoutQCAuthMsg', function() {
        //go to login page
        qcssapp.Router.navigate('login-page', {trigger:true});
    });


})(qcssapp, jQuery);