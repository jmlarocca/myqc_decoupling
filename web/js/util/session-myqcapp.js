;(function (qcssapp, $) {
    'use strict';

    //******* SESSION RELATED FUNCTIONS *******/

    //creates and saves a new session model. Only one session model can exist (updates overwrite).
    qcssapp.Functions.createSession = function( sessionID, location, code, forcePassword, keepLogged, enableFingerprint ) {
        if ( typeof keepLogged == typeof undefined || keepLogged == '' ) {
            keepLogged = false;
        }

        //TODO: should not be creating a new model every time
        //set SESSION + LOCATION and save to localStorage
        var mdl = new qcssapp.Models.SessionModel({
            sessionID:sessionID,
            code:code,
            location:location,
            forcePassword:forcePassword,
            id: 1,
            keepLogged: keepLogged,
            checkFingerprintOnDevice: enableFingerprint,
            badge: '',
            order: ''
        }, {parse:true});
        mdl.save();

        qcssapp.Functions.setSession( sessionID, location, '', code );
    };

    //sets the global session value and turns on custom auth header
    qcssapp.Functions.setSession = function( sessionID, location, storeID, code ) {
        qcssapp.Session = sessionID;
        qcssapp.Location = location;
        qcssapp.UserCode = code;
        qcssapp.StoreSelected = storeID;

        $.ajaxSetup({
            headers: {
                'X-MMHayes-AppConfig': qcssapp.appConfig,
                'X-MMHayes-Auth': qcssapp.Session,
                'X-MMHayes-AppVersion': qcssapp.AppVersion,
                'X-MMHayes-StoreID': qcssapp.StoreSelected
            }
        });
    };

    //empties the saved session and location, resets the global session value and custom auth header value
    qcssapp.Functions.destroySession = function() {
        qcssapp.Functions.createSession('', '', '', false, false, false);
    };

    //checks the user's session in the case of a E2001 error response for a timed out session
    qcssapp.Functions.handleSessionExpiration = function(errorThrown) {
        //only if server version is 1.2 or above!
        if ( !qcssapp.Functions.checkServerVersion(1,2) ) {
            qcssapp.Functions.throwFatalError(errorThrown, 'login');
            return false;
        }

        var endPoint = qcssapp.Location + '/api/auth/extend/check';

        //TODO: Adjust callAPI parameters
        qcssapp.Functions.callAPI(endPoint, 'GET', '','',
            function(response) {
                var content = 'Your session has timed out.';

                if ( response === true ) {
                    content += ' Extend your session or log out.';
                } else {
                    content += ' Please login again.'
                }

                $.confirm({
                    title:'Session Expired',
                    content:content,
                    buttons: {
                        logout: {
                            text:'Logout',
                            btnClass: 'extend-logout-button',
                            action: function() {
                                if(qcssapp.Functions.fingerprintEnabled()) {
                                    qcssapp.loggedOutFingerprint = true;
                                }
                                qcssapp.Functions.logOut();
                            }
                        },
                        extend: {
                            text: 'Extend',
                            btnClass: 'extend-button',
                            action: function() {
                                qcssapp.Functions.extendSession();
                            }
                        }
                    },
                    onContentReady: function() {
                        var $extendBtn = $('.extend-button');
                        if ( !response ) {
                            $extendBtn.hide();
                        } else {
                            $extendBtn.show();
                        }
                    }
                });
            },
            function() {
                qcssapp.Functions.throwFatalError('Session has timed out. Please login again.', 'login');
            }
        );
    };

    //attempts to extend the user's session
    qcssapp.Functions.extendSession = function() {
        var endPoint = qcssapp.Location + '/api/auth/extend';

        //TODO: Adjust callAPI parameters
        qcssapp.Functions.callAPI(endPoint, 'POST', '', '',
            function(response) {
                if ( response ) {
                    if ( response.status == 'success' ) {
                        var $activeView = $('.view.active');
                        //var $activePanel = $('.panel.active');
                        var activeViewID = $activeView.attr('id');
                        //var activePanelID = $activePanel.attr('id');

                        if ( activeViewID == "pageview" || activeViewID == "orderview" || ( activeViewID == "tourview" && $('#tour-header').is(':visible') ) ) {
                            qcssapp.Functions.checkCodeForUpdates('','',qcssapp.Functions.homeNavigationCallback);
                        } else if ( activeViewID == "mainview" ) {
                            //main menu will only show if you navigate to another page first
                            qcssapp.Router.navigate('tour-page-0', {trigger: true});
                            qcssapp.Router.navigate('main', {trigger: true});
                        }
                    } else {
                        //failed? tell the user? Hide the extend button and change text?
                        $.confirm({
                            buttons: {
                                title: 'Failed',
                                content: 'Could not extend session, timer expired. Please login again.',
                                logout: {
                                    text:'Logout',
                                    btnClass: 'extend-logout-button',
                                    action: function() {
                                        qcssapp.Functions.logOut();
                                    }
                                }
                            }
                        });
                    }
                } else {
                    //some error state - log user out
                    $.confirm({
                        buttons: {
                            title: 'Failed',
                            content: 'Could not extend session, an error occurred. Please login again.',
                            logout: {
                                text:'Logout',
                                btnClass: 'extend-logout-button',
                                action: function() {
                                    qcssapp.Functions.logOut();
                                }
                            }
                        }
                    });
                }
            }
        );
    };

    //set badge if there is a dskey
    qcssapp.Functions.setBadgeInSession = function() {
        var badgeText = qcssapp.accountModel.get('badgeNumber');

        //strip the badge prefix
        if ( qcssapp.accountModel.get('badgeNumPrefix') ) {
            var prefix = qcssapp.accountModel.get('badgeNumPrefix');
            if ( prefix == badgeText.substr(0, prefix.length) ) {
                badgeText = badgeText.substr(prefix.length, 999);
            }
        }

        //stripe the badge suffix
        if ( qcssapp.accountModel.get('badgeNumSuffix') ) {
            var suffix = qcssapp.accountModel.get('badgeNumSuffix');
            if ( suffix == badgeText.substr(badgeText.length - suffix.length, 999) ) {
                badgeText = badgeText.substr(0, badgeText.length - suffix.length);
            }
        }

        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        mdl.set('badge',badgeText);
        mdl.save();
    };

    qcssapp.Functions.setResumeAppAndOrderInSession = function() {
        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        mdl.set('resumeApp', true);
        mdl.set('order', qcssapp.CurrentOrder);
        mdl.set('orderHistory', qcssapp.OrderMenuHistory);
        mdl.save();
    };

    qcssapp.Functions.getOrderInSession = function() {
        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        return mdl.get('order');
    };

    qcssapp.Functions.getOrderHistoryInSession = function() {
        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        return mdl.get('orderHistory');
    };

    qcssapp.Functions.clearOrderInSession = function() {
        var mdl = new qcssapp.Models.SessionModel();
        mdl.fetch();
        mdl.set('order','');
        mdl.set('orderHistory','');
        mdl.save();
    };

    // Registers the FCM token for the current session with the back-end
    qcssapp.Functions.registerToken = function() {
        if (!FirebasePlugin || !qcssapp.Session || !qcssapp.Functions.checkServerVersion(6, 1)) {
            return;
        }

        // Show prompt to request permission if not already done
        // (iOS only, Android has permission by default)
        FirebasePlugin.grantPermission(function(granted) {
            if (qcssapp.DebugMode) {
                console.log("Permission result: " + granted);
            }
        });

        clearTimeout(qcssapp.pendingTokenRegistration);

        // Get the FCM token
        FirebasePlugin.getToken(function(token) {
            if (token) {
                var endPoint = qcssapp.Location + "/api/account/registerDevice";

                // Send the token and the device type (iOS/Android)
                var registrationObject = {
                    "token": token,
                    "deviceTypeID": qcssapp.Functions.getDeviceTypeID()
                };

                qcssapp.Functions.callAPI(endPoint, "POST", JSON.stringify(registrationObject), "",
                    function(response) {
                        if (!response && qcssapp.DebugMode) {
                            console.log("Error saving FCM token to the database");
                        }
                    }, "", "", "", "", true
                );
            } else {
                if (qcssapp.DebugMode) {
                    console.log("FCM token has not been allocated yet");
                }
                // Wait 5 seconds for the token to be allocated and then try again
                qcssapp.pendingTokenRegistration = setTimeout(qcssapp.Functions.registerToken, 5000);
            }
        }, function(error) {
            if (qcssapp.DebugMode) {
                console.log("Error getting FCM token: " + error);
            }
        });
    };

})(qcssapp, jQuery);