;(function (qcssapp, $) {
    'use strict';

    //******* ERROR RELATED FUNCTIONS *******/

    //displays warning errors on the front end to the user, on the active panel or given panel
    qcssapp.Functions.displayError = function(error, title, panelID) {
        if ( !title || title == "" ) {
            title = "Error";
        }

        var errorMsgView = new qcssapp.Views.ErrorMsg({
            title:title,
            content: qcssapp.Functions.htmlDecode( error )
        });

        var errorMsgViewRendered = errorMsgView.render();
        var $activePanel = $('.panel.active');

        if ( panelID && panelID.length > 0 ) {
            $activePanel = $(panelID);
        }

        $activePanel.scrollTop(0).find('.error-msg-container').html(errorMsgViewRendered.el).slideDown();
    };

    //hides all warning errors on all views/panels
    qcssapp.Functions.resetErrorMsgs = function() {
        $('.error-msg-container').html('').slideUp();
    };

    //determines type of fatal error (unexpected, bad auth, missing data) and handles moving user back and displaying warning error or fatally erroring
    qcssapp.Functions.throwFatalError = function(errorMsg, goToPage) {
        var $activeView = $('.view.active');
        var $activePanel = $('.panel.active');
        var activeViewID = $activeView.attr('id');
        var activePanelID = $activePanel.attr('id');
        var $tourHeader = $('#tour-header');
        var homeKeypadID = "";

        //if cant determine active view/panel, just generic throwFatalError
        if ( goToPage != 'main' && ($activePanel.length != 1 || $activeView.length != 1 || goToPage == 'error') ) {
            qcssapp.Functions.displayFatalError(errorMsg);
            qcssapp.Functions.hideMask();
            return;
        }

        var successParameters = {
            errorMsg: errorMsg,
            goToPage: goToPage
        };

        qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/check/validUser','GET','',successParameters,
            function(result, successParamters) {
                var showError = false;
                //get home keypad
                if (qcssapp.CurrentOrder && qcssapp.CurrentOrder.storeModel && qcssapp.CurrentOrder.storeModel.get('homeKeypadID') ) {
                    homeKeypadID = qcssapp.CurrentOrder.storeModel.get('homeKeypadID');
                }

                //go back to main nav view
                if (successParameters.goToPage == 'main' || activeViewID == 'pageview' || activeViewID == 'orderview' || ( activeViewID == 'tourview' && $tourHeader.is(':visible') ) || activeViewID.indexOf('general-msg') >= 0 || activeViewID.indexOf('general-confirm') >= 0) {
                    showError = true;
                    qcssapp.Functions.checkCodeForUpdates('', '', qcssapp.Functions.homeNavigationCallback);
                    $('#main').addClass('active');
                    if ( successParameters.errorMsg ) {
                        qcssapp.Functions.displayError(successParameters.errorMsg, 'main');
                    } else {
                        qcssapp.Functions.displayError('An error occurred, please try again.', 'main');
                    }
                } else if (successParameters.goToPage == 'login' || activeViewID == 'mainview' || activeViewID == 'loginview' || ( activeViewID == 'tourview' && !$tourHeader.is(':visible') )) {
                    showError = true;

                    //go back to login view
                    qcssapp.Functions.resetOnLogout();
                } else if (activeViewID == 'keypadview') {
                    if (activePanelID.indexOf('keypad') >= 0) {
                        var currentKeypadID = activePanelID.split('-')[1];
                    }

                    if (homeKeypadID != currentKeypadID && qcssapp.Functions.isNumeric(homeKeypadID)) {
                        qcssapp.Functions.loadKeypad(homeKeypadID, false, function() {
                                if ( successParameters.errorMsg ) {
                                    qcssapp.Functions.displayError(successParameters.errorMsg);
                                } else {
                                    qcssapp.Functions.displayError('An error occurred, please try again.');
                                }
                            },
                            successParameters.errorMsg);
                    } else {
                        showError = true;
                        qcssapp.Functions.resetOrdering(true);
                    }
                } else if ( activeViewID == 'productview' || activeViewID == 'cartview' || activeViewID == 'modifierview' ) {
                    if ( qcssapp.Functions.isNumeric(homeKeypadID) ) {
                        qcssapp.Functions.loadKeypad(homeKeypadID, false, function() {
                                if (successParameters.errorMsg) {
                                    qcssapp.Functions.displayError(successParameters.errorMsg);
                                } else {
                                    qcssapp.Functions.displayError('An error occurred, please try again.');
                                }
                            },
                            successParameters.errorMsg);
                    } else {
                        showError = true;
                        qcssapp.Functions.resetOrdering(true);
                    }
                } else if ( activeViewID == 'receiveview' || activeViewID == 'reviewview' || activeViewID == 'reauthView' ) {
                    showError = true;
                    qcssapp.Router.navigate('cart', {trigger: true});
                } else if ( activeViewID == 'paymentview' ) {
                    showError = true;
                    qcssapp.Router.navigate('account-funding', {trigger: true});
                } else {
                    qcssapp.Functions.displayFatalError(successParameters.errorMsg);
                    qcssapp.Functions.hideMask();
                    return;
                }

                if ( showError ) {
                    if ( successParameters.errorMsg ) {
                        qcssapp.Functions.displayError(successParameters.errorMsg);
                    } else {
                        qcssapp.Functions.displayError('An error occurred, please try again.');
                    }
                }
            }
        );
    };

    //display function for fatal error view
    qcssapp.Functions.displayFatalError = function(errorMsg) {
        if(typeof errorMsg != "undefined" && errorMsg.length > 0) {
            qcssapp.Functions.throwFatalError("An error occurred while accessing your account. Log in again.", "login");
        } else if ( !$('#general-error-view').hasClass('active') ) {
            new qcssapp.Views.GeneralError({
                content: errorMsg
            });

            qcssapp.Router.navigate('error', {trigger:true});

            $('.general-error-page').addClass('active');
        }
    };

    //handles the case when calls timeout or when there is no internet connection
    qcssapp.Functions.handleNetworkError = function(errorType) {
        $('.network-error_modal').remove();

        new qcssapp.Views.NetworkErrorView(errorType);
    };

    //displays popup messages on the front end to the user, on the active view
    qcssapp.Functions.displayPopup = function(message, title, buttonName1, buttonCallback1, buttonCallbackParam1, buttonName2, buttonCallback2, buttonCallbackParam2, $appendElement) {
        if ( !title || title == "" ) {
            title = "Warning";
        }

        $('.general-popup').remove();

        var popupMsgView = new qcssapp.Views.GeneralPopup({
            title:title,
            message: qcssapp.Functions.htmlDecode( message ),
            buttonName1: buttonName1,
            buttonCallback1: buttonCallback1,
            buttonCallbackParam1: buttonCallbackParam1,
            buttonName2: buttonName2,
            buttonCallback2: buttonCallback2,
            buttonCallbackParam2: buttonCallbackParam2
        });

        if(typeof $appendElement !== 'undefined' && $appendElement.length) {
            $appendElement.append(popupMsgView.render().el);

        } else {
            var $activeView = $('.view.active');
            $activeView.append(popupMsgView.render().el);
        }
    };


})(qcssapp, jQuery);