;(function (qcssapp, $) {
    'use strict';

    //******* UTILITY FUNCTIONS *******/

    //global function to grab a get parameter by name
    qcssapp.Functions.getParameterByName = function(name, url) {
        if ( url == null ) {
            url = window.location.search;
        }

        var match = RegExp('[?&]' + name + '=([^&]*)').exec(url);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    };

    //specify DebugMode for this application
    qcssapp.DebugMode = qcssapp.Functions.getParameterByName("debug");

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings = function() {
        _.templateSettings = { //change backbone's micro template to use "{{= }}" instead of "<%= %>" which conflicts with JSP
            evaluate: /\{\{(.+?)\}\}/g,
            interpolate: /\{\{=(.+?)\}\}/g,
            escape: /\{\{-(.+?)\}\}/g
        };
    };

    //checks that the server version is at least equal to the given version number
    qcssapp.Functions.checkServerVersion = function(majorVersionNumber, minorVersionNumber, buildVersionNumber) {
        if ( !qcssapp.ServerVersion || qcssapp.ServerVersion.toString().indexOf('.') == -1 ) {
            return false;
        }

        var serverVersionSplit = qcssapp.ServerVersion.toString().split('.');

        if ( buildVersionNumber == null ) {
            buildVersionNumber = 0;
        }

        // return statement below wasn't working when server version was "2.0.0", because the minor version was greater than the ".0"
        //TODO: clean this up later, too many if statements, too long. But works at least for now
        if(qcssapp.Functions.isNumeric(serverVersionSplit[1]) && qcssapp.Functions.isNumeric(serverVersionSplit[2])) {
            if(Number(serverVersionSplit[0]) > majorVersionNumber) { // if the server version is greater than the major version, you're good
                return true;
            } else if (Number(serverVersionSplit[0]) < majorVersionNumber) {  // if the server version is less than the major version, not good
                return false;
            } else if (Number(serverVersionSplit[0]) == majorVersionNumber ) {  // if the server version is equal to the major version, check minor version
                if(Number(serverVersionSplit[1]) > minorVersionNumber) { // if the server version is greater than the minor version, you're good
                    return true;
                } else if (Number(serverVersionSplit[1]) < minorVersionNumber) {  // if the server version is less than the minor version, not good
                    return false;
                } else if (Number(serverVersionSplit[1]) == minorVersionNumber) {  // if the server version is equal to the minor version, check build version
                    if(Number(serverVersionSplit[2]) > buildVersionNumber) { // if the server version is greater than the build version, you're good
                        return true;
                    } else if (Number(serverVersionSplit[2]) < buildVersionNumber) {  // if the server version is less than the build version, not good
                        return false;
                    } else if (Number(serverVersionSplit[2]) == buildVersionNumber) {  // if the server version is equal to the build version, exact same builds
                        return true;
                    }
                }
            }
        }
        //return !(!$.isNumeric(serverVersionSplit[1]) || !$.isNumeric(serverVersionSplit[2]) || Number(serverVersionSplit[0]) < majorVersionNumber || Number(serverVersionSplit[1]) < minorVersionNumber || Number(serverVersionSplit[2]) < buildVersionNumber)
    };

    //returns the cache model to the console
    qcssapp.Functions.checkCache = function() {
        //make a call to funding resource create/default with the transactionID filled in
        var endPoint = qcssapp.Location + '/api/myqc/check/cache';

        qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
            function(result) {
                console.log(result);
            },
            '',
            '',
            '',
            '',
            true,
            true
        );
    };

    // Returns the device type ID: 1 = iOS, 2 = Android, 0 = other
    qcssapp.Functions.getDeviceTypeID = function() {
        if (!window.cordova) {
            return 0;
        } else if (window.cordova.platformId === "ios") {
            return 1;
        } else if (window.cordova.platformId === "android") {
            return 2;
        } else {
            return 0;
        }
    };

    // Replacement for jQuery.isNumeric, depreciated in jQuery 3
    qcssapp.Functions.isNumeric = function(obj) {
        return !Array.isArray(obj) && (obj - parseFloat(obj) + 1) >= 0;
    };

})(qcssapp, jQuery);