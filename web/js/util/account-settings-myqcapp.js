;(function (qcssapp, $) {
    'use strict';

    //******* ACCOUNT SETTINGS RELATED FUNCTIONS *******/

    qcssapp.Functions.checkAccountSettings = function() {
        if(!qcssapp.Functions.checkServerVersion(2,0) || window.location.hash != "#show-accountSettings") {
            qcssapp.Functions.checkAccountStatus(function() {
                qcssapp.Router.navigate('main', {trigger: true});
                $('#main').scrollTop(0);
                qcssapp.Functions.hideMask();
            });
        } else {
            var successFunction = function() {
                qcssapp.Functions.checkAccountStatus(function() {
                    qcssapp.Router.navigate('main', {trigger: true});
                    $('#main').scrollTop(0);
                    qcssapp.Functions.hideMask();
                });
            }
            qcssapp.Functions.cancelChanges(successFunction, '');
        }
    };

    //verifies the user's account status as active, or else logs them out
    qcssapp.Functions.checkAccountStatus = function(callback, preventHideMask, preventShowMask) {
        var preventHide = preventHideMask === true;
        var preventShow = preventShowMask === true;

        var successParameters = {
            callback: callback
        };

        //TODO: Adjust callAPI parameters
        qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/accountdetails','GET','',successParameters,
            function(result) {
                if (qcssapp.DebugMode) {
                    console.log("Retrieved user details:");
                    console.log(result);
                }

                //result set should contain many details
                if ( !result ) {
                    qcssapp.Functions.throwFatalError('', 'login');
                    return false;
                }

                var employeeStatus = "";
                var accountActive = "";
                var employeeNumber = "";

                //check through result set for the details and set them
                $.each(result, function() {
                    if ( this.id == "account-status" ) {
                        employeeStatus = this.value.toUpperCase();
                    } else if ( this.id == "account-active" ) {
                        accountActive = this.value;
                    } else if ( this.id == "employee-number" ) {
                        employeeNumber = this.value;
                    }
                });

                //if using person accounts and the selected student is set in the global accountModel obj
                var studentAcct = false;
                if(qcssapp.personModel && qcssapp.accountModel && qcssapp.accountModel.get("accountNumber") == employeeNumber) {
                    studentAcct = true;
                }

                //check status
                if ( (accountActive != "true" && !studentAcct ) || !(employeeStatus == "A" || employeeStatus == "F" || (studentAcct && employeeStatus == "W")) ) {
                    if ( employeeStatus == "W" ) {
                        //build general message view for placed orders
                        new qcssapp.Views.GeneralMsg({
                            title: "Re-Authenticate",
                            main: "New terms of service available",
                            content: 'There is a new terms of service that must be accepted in order to continue using the application. Please log in again below.'
                        });

                        //btn view to continue
                        new qcssapp.Views.ButtonView({
                            text: "Log In Again",
                            buttonType: "customCB",
                            appendToID: "#general-msg-btn-container",
                            pageLinkID: "#main",
                            id: "messageContinueBtn",
                            class: "button-flat accent-color-one",
                            callback: function () {
                                qcssapp.Functions.logOut();
                            }
                        });

                        //navigate to general msg view
                        qcssapp.Router.navigate('message', {trigger: true});
                        return;
                    }

                    if(qcssapp.personModel) {
                        var errorMsg = "The selected "+qcssapp.Aliases.accountAlias.toLowerCase()+"'s account has been inactivated.";
                        qcssapp.Functions.throwFatalError(errorMsg, 'login');
                    } else {
                        qcssapp.Functions.throwFatalError('', 'login');
                    }
                    return false;
                } else {
                    //set data attr
                    var $userInfo = $('.user-information-title');
                    var currentStatus = $userInfo.attr('data-accountStatus');

                    if ( currentStatus != employeeStatus) {
                        var $freezePageHeader = $('#page-header').find('h1');

                        qcssapp.Functions.toggleFreeze($userInfo, currentStatus);

                        if ( $('#show-freeze').hasClass('active') ) {
                            if ( employeeStatus == "A" ) {
                                $freezePageHeader.text('Freeze Account');
                            } else {
                                $freezePageHeader.text('Unfreeze Account');
                            }
                        }
                    }

                    if(qcssapp.personModel && employeeNumber == "") {
                        qcssapp.accountModel = null;
                        var options = {'autoSelect': false};
                        new qcssapp.Views.ManageAccountView(options);
                        qcssapp.Router.navigate('manage-account', {trigger:true});
                    } else if (window.location.hash.indexOf('quickpay') == 1 && qcssapp.Functions.checkServerVersion(2,1)) {
                        qcssapp.Functions.inactivateQRCodeBadges(successParameters.callback, employeeStatus);
                    } else if (successParameters.callback && typeof successParameters.callback == 'function') {
                        successParameters.callback(employeeStatus);
                    }
                    return true;
                }

            },
            '',
            '',
            '',
            '',
            preventShow,
            preventHide,
            true
        );
    };

    //reverts the account back to the original account settings if they were changed
    qcssapp.Functions.cancelChanges = function(successFunction, successParameters, forceChange) {
        if(!qcssapp.AccountSettings.needsSave) {  //if the user hasn't made any changes
            if(forceChange) {   //log them out if they are going back from the force change account settings page
                qcssapp.Functions.resetFingerprintInSession();
                qcssapp.Functions.logOut();
                return;

            } else {  //or send them to the nav menu if they are already logged in
                qcssapp.Functions.checkAccountStatus(function() {
                    //remove generated content
                    $('#item-list').empty();
                    qcssapp.Functions.showMask(true);

                    //set this so the fingerprint auth pop doesn't show when navigating back from account settings page
                    qcssapp.verifyFingerprintFromLogin = true;

                    //load the menu and rest of the application based on the signed in user
                    new qcssapp.Views.NavView();
                    qcssapp.Router.navigate('main', {trigger: true});
                    qcssapp.Functions.hideMask();
                });
                return;
            }
        }

        var $accountGroup = $('#accountGroupSetting');
        var $accountType = $('#accountTypeSetting');

        var dataParameters = {};

        //look for fields that have been changed, add to data parameters object
        if(qcssapp.AccountSettings.accountStatus) {
            dataParameters.statusChanged = true;
        } else if (!qcssapp.AccountSettings.accountStatus && qcssapp.AccountSettings.spendingProfile == "") {
            $accountGroup.val(qcssapp.AccountSettings.accountGroup);
            $accountType.val(qcssapp.AccountSettings.accountType);
            return;
        }

        if(qcssapp.AccountSettings.accountGroup != $accountGroup.val()) {
            dataParameters.accountGroupID = qcssapp.AccountSettings.accountGroup;
        }
        if(qcssapp.AccountSettings.spendingProfile != $('#spendingProfileSetting').val()) {
            dataParameters.spendingProfileID = qcssapp.AccountSettings.spendingProfile;
        }
        if(qcssapp.AccountSettings.accountType != $accountType.val()) {
            dataParameters.accountTypeID = qcssapp.AccountSettings.accountType;
        }

        qcssapp.Functions.callAPI(qcssapp.Location + '/api/account/settings/cancel', 'POST', JSON.stringify(dataParameters), '',
            function(response) {
                //execute success function
                if(successFunction != null) {
                    successFunction(successParameters);
                }
            },
            function(errorParameters) {
                errorParameters.accountSettingsView.fetchError();
            },
            '', '', '', true, true
        );
    };

    //checks if the MyQCForceChange bit is on for the account's account group
    qcssapp.Functions.checkForceChange = function(continueFunction, response) {
        if (!qcssapp.Functions.checkServerVersion(2,0)) {
            continueFunction(response);
            $('#page-header').find('.general-back-icon').remove();
            $('.home-link').show();
            return;
        }

        qcssapp.Functions.callAPI(qcssapp.Location + '/api/account/force/change', 'GET', '', '',
            function (result) {
                if(result) {
                    $('#accountSettings').off();
                    new qcssapp.Views.AccountSettingsView(true);

                    qcssapp.Router.navigate('show-accountSettings', {trigger:true});
                } else {
                    continueFunction(response);
                }
            },
            function (params) {
                if (qcssapp.DebugMode) {
                    console.log('ERROR response for checking if Force Change is turned on for Account Group');
                }
                qcssapp.Functions.displayError('Error in Force Change on Account Group check.');
            },
            '', '', '', false, true
        );
    };

    //set the global alias names
    qcssapp.Functions.setAliases = function(data) {
        if (qcssapp.Functions.checkServerVersion(3,0)) {
            if(data.accountAlias) {
                qcssapp.Aliases.accountAlias = data.accountAlias;
            }

            if(data.personAccountAlias) {
                qcssapp.Aliases.personAccountAlias = data.personAccountAlias;
            }
        }
    };

    qcssapp.Functions.saveOrgLevel = function(dataParameters){
        qcssapp.Functions.callAPI(qcssapp.Location + '/api/account/settings/save', 'POST', JSON.stringify(dataParameters), '',
            function(response) {
                if(response) {
                    if(qcssapp.DebugMode){
                        console.log("Saved User Org Level: ", response);
                    }

                } else {
                    qcssapp.Functions.displayError('There was an issue saving your Account Settings, please try again.');

                }
            },
            function() {
                qcssapp.Functions.displayError('There was an issue saving your Account Settings, please try again.');
            },
            '', '', '', false, true
        );
    };

    //******* PERSON ACCOUNT SETTING FUNCTIONS *******/

    //check if the account being managed has an email set, if not show notification
    qcssapp.Functions.checkPersonAccountEmail = function() {

        if(qcssapp.personModel && qcssapp.accountModel && qcssapp.accountModel.get('email') == "") {
            var name = qcssapp.accountModel.get('name');

            $.confirm({
                title: '',
                content: '<div class="content-text"><b>' + name + '</b> does not have an email and will not be able to login to access their account. </br></br>You can set an email for  </br><b>' + name + '</b> on the Settings page.</div>',
                buttons: {
                    cancel: {
                        text: 'Close',
                        btnClass: 'btn-default close-btn prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {}
                    }
                }
            });
        }
    };

    //******* ACCOUNT SETTINGS EVENT HANDLERS *******/

    //when hovering over modal lines except the selected modal line
    $(document).on('mouseover', '.modal-line', function () {
        $('.modal-line').css({'color':'black ', 'background':'white'});
        $(this).css({'color':'white', 'background':'rgba(11, 163, 230, 0.8)'});
    });

})(qcssapp, jQuery);