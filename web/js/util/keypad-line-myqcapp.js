;(function (qcssapp, $) {
    'use strict';

    //******* KEYPAD LINE RELATED FUNCTIONS *******/

    qcssapp.Functions.setCurrencySettings = function(data) {
        if(!qcssapp.Functions.checkServerVersion(4,0,57)) {
            return;
        }

        if ( data.hasOwnProperty('showCurrency') && data['showCurrency'] != null ) {
            qcssapp.currency.showCurrency = data['showCurrency'];
        }

        if ( data.hasOwnProperty('currencyType') && data['currencyType'] != null ) {
            qcssapp.currency.currencyType = data['currencyType'];
        }

        if ( data.hasOwnProperty('currencyBeforePrice') && data['currencyBeforePrice'] != null ) {
            qcssapp.currency.currencyBeforePrice = data['currencyBeforePrice'];
        }

        if ( data.hasOwnProperty('decimalInPrice') && data['decimalInPrice'] != null ) {
            qcssapp.currency.decimalInPrice = data['decimalInPrice'];
        }

        if ( data.hasOwnProperty('keypadPositionID') && data['keypadPositionID'] != null ) {
            qcssapp.keypadPositionID = data['keypadPositionID'];
        }
    };

    // Format the price and currency type
    qcssapp.Functions.formatPrice = function(price, plusNegSign, removeTag) {
        plusNegSign = typeof plusNegSign === 'undefined' ? '': plusNegSign;
        removeTag = typeof removeTag === 'undefined' ? false : removeTag;

        price = Number(price);
        var formattedPrice = price.toLocaleString(undefined, {'minimumFractionDigits':2, 'maximumFractionDigits': 2});

        // Replace commas with decimals... and decimals with commas
        if(!qcssapp.currency.decimalInPrice) {
            formattedPrice = formattedPrice.replace(".", "*");
            formattedPrice = formattedPrice.replace(",", ".");
            formattedPrice = formattedPrice.replace("*", ",");
        }

        // Format currency type after price
        if(qcssapp.currency.showCurrency && !qcssapp.currency.currencyBeforePrice) {
            formattedPrice = plusNegSign + formattedPrice +
                (removeTag ? '' : '<span class="line_currency">')
                 + qcssapp.currency.currencyType.trim() +
                (removeTag ? '' : '</span>');

            // Add special formatting for dollar signs before the price
        } else if (qcssapp.currency.showCurrency && qcssapp.currency.currencyBeforePrice && qcssapp.currency.currencyType.trim() == '$*') {
            formattedPrice =
                (removeTag ? '' : '<span class="line_currency-dollar">')
                    + plusNegSign +
                    (removeTag ? '$</span>' : '$')
                    + formattedPrice;

            // Format currency type before price
        } else if (qcssapp.currency.showCurrency) {
            formattedPrice =
                (removeTag ? '' : '<span class="line_currency">') +
                    plusNegSign + qcssapp.currency.currencyType.trim() +
                    (removeTag ? '' : '</span>') + formattedPrice;
        }

        if(!qcssapp.currency.showCurrency && plusNegSign.length) {
            formattedPrice = plusNegSign + formattedPrice;
        }

        return formattedPrice;
    };

    qcssapp.Functions.getIconGrayscalePercentage = function() {
        var percent = "";

        if(typeof qcssapp.CurrentOrder.storeModel.get('iconGrayscalePercentage') !== 'undefined' && qcssapp.CurrentOrder.storeModel.get('iconGrayscalePercentage') != null && qcssapp.CurrentOrder.storeModel.get('iconGrayscalePercentage') != '' ) {
            percent = qcssapp.Functions.htmlDecode(qcssapp.CurrentOrder.storeModel.get('iconGrayscalePercentage'));

            if(percent.indexOf("%") != -1) {
                percent = percent.replace('%', '');
            }
        }

        return percent;
    };

    // Format the price and currency type in App besides Online Ordering
    qcssapp.Functions.formatPriceInApp = function(price, plusNegSign) {
        if(price === "") {
            price = 0;
        }

        plusNegSign = typeof plusNegSign === 'undefined' ? '': plusNegSign;

        price = Number(price);
        var formattedPrice = price.toLocaleString(undefined, {'minimumFractionDigits':2, 'maximumFractionDigits': 2});

        // Replace commas with decimals... and decimals with commas
        if(!qcssapp.currency.decimalInPrice) {
            formattedPrice = formattedPrice.replace(".", "*");
            formattedPrice = formattedPrice.replace(",", ".");
            formattedPrice = formattedPrice.replace("*", ",");
        }

        // Format currency type after price
        if(!qcssapp.currency.currencyBeforePrice) {
            formattedPrice = plusNegSign + formattedPrice +  '<span class="line_currency">' + qcssapp.currency.currencyType.trim() + '</span>';

           // Format currency type before price
        } else  {
            formattedPrice ='<span class="line_currency">' + plusNegSign + qcssapp.currency.currencyType.trim() + '</span>' + formattedPrice;
        }

        return formattedPrice;
    };

    //changes the fill of the svg colors
    qcssapp.Functions.setMenuIconSVG = function($el, color) {
        $el.each(function(){
            var $img = $(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');
            var imgColor = $img.attr('data-color');

            $.get(imgURL, function(data) {
                var $svg = $(data).find('svg');

                // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                var $path = $(data).find('path');

                if(typeof $path.attr('fill') !== 'undefined') {
                    $path.attr('fill', color);
                }

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');
        });
    };

    //********* IMAGE FUNCTIONS ***************//

    qcssapp.Functions.determineTemplate = function(mod) {
        var imagePositionID = mod.get('iconPosition');
        var image = mod.get('image');
        var priceBelowName = mod.get('priceBelowName') && mod.get('menuID') == null;

        var templateID = '';
        switch ( imagePositionID ) {
            case ("2"):
                templateID = priceBelowName ?  'keypad-line-template_right-below' : 'keypad-line-template_right';
                break;
            case ("3"):
                templateID = 'keypad-line-template_bottom';
                break;
            case ("4"):
                templateID = priceBelowName ?  'keypad-line-template_left-below' : 'keypad-line-template_left';
                break;
            case ("1"):
                templateID = 'keypad-line-template_center';
                break;
            case ("5"):
                templateID = 'keypad-line-template_center';
                break;
            case ("6"):
                templateID = 'keypad-line-template_full';
                break;
        }

        if(image == "" || imagePositionID == "" || imagePositionID == '0' || imagePositionID == '7') { //if IconPosition is null, Top, 0, or Hidden
            templateID = priceBelowName ?  'keypad-line-template_original-below' : 'keypad-line-template_original';
        }
        return templateID
    };


})(qcssapp, jQuery);