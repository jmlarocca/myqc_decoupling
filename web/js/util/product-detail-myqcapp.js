;(function (qcssapp, $) {
    'use strict';

    //******* PRODUCT DETAIL RELATED FUNCTIONS *******/

    // Open the product page from the menus
    qcssapp.Functions.goToProductDetail = function(productId, keypadModel) {

        // Show a transparent loading mask to prevent the user from clicking the button twice
        qcssapp.Functions.showMask(false, true);

        var successFn = function(response) {

            // Check if the product has any purchase restrictions
            if( !qcssapp.Functions.checkPurchaseRestriction(response) ) {
                return;
            }

            // Create new product model
            var productModel = new qcssapp.Models.ProductDetail(response, {parse: response});

            if(productModel.get('image') == ''){
                productModel.set('image', keypadModel.get('image'));
            }

            // Url path
            var navigatePathStr = 'product-detail/' + productModel.get('id');
            var previousPathStr = window.location.hash.replace('#', '');

            if(previousPathStr.indexOf('product-detail') != -1) {
                return;
            }

            new qcssapp.Views.ProductDetailView(productModel, previousPathStr);
            qcssapp.Router.navigate(navigatePathStr, {trigger: true});

        };

        qcssapp.Functions.loadProductDetail(productId, true, successFn);
    };

    // Call to get product details and modifier menus
    qcssapp.Functions.loadProductDetail = function(id, preventMask, successFunction, successParam, modifierList) {

        var endPoint = qcssapp.Location + '/api/ordering/product/' + id;
        var verb = 'GET';
        var sendData = [];

        //for modifier product, need to send the modifiers to get the new correct price
        if ( modifierList && modifierList.length > 0 ) {
            $.each( modifierList, function() {
                var modifier = {id: this.id};

                if ( qcssapp.Functions.checkServerVersion(1,3,19) ) {
                    modifier.modIndex = this.modIndex;
                }

                //include the keypadID the modifier came from
                if ( qcssapp.Functions.checkServerVersion(2,1,8) ) {
                    modifier.keypadID = this.keypadID;
                }

                //include prep option if the modifier has one set
                if ( qcssapp.Functions.checkServerVersion(4,0) && this.prepOption != null ) {
                    modifier.prepOptionSetID = this.prepOption.prepOptionSetID;
                    modifier.prepOption = this.prepOption;
                }

                //include prep option if the modifier has one set
                if ( qcssapp.Functions.checkServerVersion(4,0,58)) {
                    modifier.hasPrinterMappings = this.hasPrinterMappings;
                }

                sendData[sendData.length] = modifier;
            });

            if (sendData.length) {
                sendData = JSON.stringify(sendData);
            }

            verb = 'POST';
        }

        qcssapp.Functions.callAPI(endPoint, verb, sendData, '',
            function(response) {
                if ( Number(response.id) <= 0 ) {
                    qcssapp.Functions.displayPopup('There was an error loading the selected product. If this continues please contact the store manager or select a different product', 'Error', 'OK');
                    return;
                }

                response.quantity = 1;
                response.price = Number(response.price).toFixed(2);

                if( !qcssapp.Functions.checkServerVersion(4,1,10) && response.modSetDetailID) {
                    qcssapp.ProductInProgress.modifierMenus = [];
                    qcssapp.Functions.loadModifierDetails(response.modSetDetailID, successFunction, successParam, response);

                } else if (!qcssapp.Functions.checkServerVersion(4,1,10) ) {
                    qcssapp.Functions.loadProductNutrition(response, successFunction, successParam)

                } else if(typeof successFunction === 'function') {
                    successFunction(response, successParam);
                }

            }, '', '', '', '', preventMask, false
        );
    };

    // ************ PRODUCT CART RELATED DETAILS ******************* //

    // Show product detail view when clicking on product line in Cart
    qcssapp.Functions.loadProductFromCart = function(productModel) {
        qcssapp.Functions.showMask(false, true);

        new qcssapp.Views.ProductDetailView(productModel, 'cart-page', true);

        qcssapp.Router.navigate('product-detail/' + productModel.get('id'), {trigger: true});
    };

    // Show product detail view when clicking on product line in Cart from Express Reorders
    qcssapp.Functions.reloadProductFromCart = function(productModel) {
        qcssapp.Functions.showMask(false, true);

        var successParam = {
            productModel: productModel
        };

        var successFn = function(response, params) {

            //check if the product has any purchase restrictions
            if( !qcssapp.Functions.checkPurchaseRestriction(response) ) {
                return;
            }

            // Update the new product model with the selected prep option and modifiers
            var oldProductModel = params.productModel;
            response.prepOptionSetID = oldProductModel.get('prepOptionSetID');
            response.prepOption = oldProductModel.get('prepOption');
            response.modifiers = oldProductModel.get('modifiers');
            response.id = oldProductModel.get('productID');

            //create new product model
            var productModel = new qcssapp.Models.ProductDetail(response, {parse: response});

            new qcssapp.Views.ProductDetailView(productModel, 'cart-page', true);
            qcssapp.Router.navigate('product-detail/' + productModel.get('id'), {trigger: true});
        }.bind(this);

        // Call loadProductDetail to get the modifier menus
        qcssapp.Functions.loadProductDetail(productModel.get('productID'), true, successFn, successParam);
    };

    // ************ PRODUCT SCANNING RELATED DETAILS ******************* //

    // For loading products after scanning
    qcssapp.Functions.loadProductFromScan = function(productId) {
        //Defect 3900 : prepOption sets weren't being loaded properly through this process, this block is here to grab them and set to the model during the successFN if they exist.
        var prepOptionSet;
        if(productId.prepOptionSetID){
            prepOptionSet = productId.prepOptionSetID
        }
        productId = productId.id;

        qcssapp.Functions.showMask(false, true);

        var successFn = function(response) {

            // Check if the product has any purchase restrictions
            if (!qcssapp.Functions.checkPurchaseRestriction(response)) {
                return;
            }

            //create new product model
            var productModel = new qcssapp.Models.ProductDetail(response, {parse: response});

            //setting the prep option set so it loads upon pulling the detail page after the scan
            if(prepOptionSet){
                productModel.set('prepOptionSetID' , prepOptionSet);
            }
            //product scanning related fields
            var productScanAddToCart = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('productScanAddToCart') ?  qcssapp.CurrentOrder.storeModel.get('productScanAddToCart') : false;
            response.productInStore = qcssapp.ProductInProgress.productInStore;

            // If we're not adding the product to the Cart on Scan, go to product view
            if(!productScanAddToCart) {

                //url path
                var navigatePathStr = 'product-detail/' + productModel.get('id');
                var previousPathStr = window.location.hash.replace('#', '');

                if(previousPathStr.indexOf('product-detail') != -1) {
                    return;
                }

                new qcssapp.Views.ProductDetailView(productModel, previousPathStr);
                qcssapp.Router.navigate(navigatePathStr, {trigger: true});
                return;
            }


            // add product to Cart
            qcssapp.Functions.addProductToCart(productModel);

            // if on cart, refresh product list
            if ( window.location.hash == "#cart" ) {
                qcssapp.Functions.resetCart();

            } else {  //go to cart page
                qcssapp.Functions.navigateToCart();
            }

            $.toast({
                text: productModel.get('name') + ' added to the Cart.',
                loader:false,
                transition: 'slide',
                position: 'bottom-center-stretch',
                hideAfter: 5000,
                bgColor: '#4D4D4D'
            });
        };

        qcssapp.Functions.loadProductDetail(productId, true, successFn);
    };

    //Loads default modifiers for the product with the given ID
    qcssapp.Functions.loadDefaultModifiers = function (productModel) {

        var successFn = function (productModel, response) {

            productModel.set("modifiers", response);

            qcssapp.Functions.loadDefaultPrepOptions(productModel);

            productModel.set("modifiersList", qcssapp.Functions.convertModifierAndPrepOptionArrayToCsv(response, true));

        }.bind(this, productModel);

        var endpoint = qcssapp.Location + "/api/ordering/product/" + productModel.get("id") + "/default/modifiers";
        qcssapp.Functions.callAPI(endpoint, "GET", "", "", successFn);
    };

    //Loads the default prep option for the product as well as the prep option for the product's default modifiers
    qcssapp.Functions.loadDefaultPrepOptions = function (productModel) {

        var successFn = function (productModel, response) {

            // try to find the loaded prep option for the parent product
            if (Number(productModel.get("prepOptionSetID")) > 0) {
                for (var i = 0; i < response.length; i++) {
                    if (response[i]["prepOptionSetID"] == productModel.get("prepOptionSetID")) {
                        var parentPrepOptModel = new qcssapp.Models.PrepOption(response[i]);
                        productModel.set("prepOption", parentPrepOptModel);
                        break;
                    }
                }
            }

            // try to find the loaded prep option for the modifiers
            for (var i = 0; i < productModel.get("modifiers").length; i++) {
                if (Number(productModel.get("modifiers")[i]["prepOptionSetID"]) > 0) {
                    for (var j = 0; j < response.length; j++) {
                        if (response[j]["prepOptionSetID"] == productModel.get("modifiers")[i]["prepOptionSetID"]) {
                            var modifierPrepOptModel = new qcssapp.Models.PrepOption(response[j]);
                            productModel.get("modifiers")[i]["prepOption"] = modifierPrepOptModel;
                            break;
                        }
                    }
                }
            }

            // add product to Cart
            qcssapp.Functions.addProductToCart(productModel);

            // if on cart, refresh product list
            if ( window.location.hash == "#cart" ) {
                qcssapp.Functions.resetCart();

            } else {  //go to cart page
                qcssapp.Functions.navigateToCart();
            }

            $.toast({
                text: productModel.get('name') + ' added to the Cart.',
                loader:false,
                transition: 'slide',
                position: 'bottom-center-stretch',
                hideAfter: 5000,
                bgColor: '#4D4D4D'
            });
        }.bind(this, productModel);

        if(!productModel.get("keypadID")) {
            productModel.set("keypadID", 0);
        }

        var endpoint = qcssapp.Location + "/api/ordering/product/" + productModel.get("productID") + "/keypad/" + productModel.get("keypadID") + "/default/prepOptions";
        qcssapp.Functions.callAPI(endpoint, "GET", "", "", successFn);
    };

    // ************ PRODUCT UPSELL RELATED DETAILS ******************* //

    // Add product to Cart from Suggestive Selling Keypad
    qcssapp.Functions.loadProductUpsell = function(productId) {

        var successFn = function(response) {

            //create new product model
            var productModel = new qcssapp.Models.ProductDetail(response, {parse: response});
            if (productModel.get('price')) {
                productModel.set('price', Number(productModel.get('price')));
            }

            $.when(qcssapp.Functions.addProductToCart(productModel)).done(function() {
                qcssapp.CurrentOrder.suggestionsAdded = true;
                $.toast({
                    text: productModel.get('name') + ' added to the Cart.',
                    loader:false,
                    transition: 'slide',
                    position: 'bottom-center-stretch',
                    hideAfter: 5000,
                    bgColor: '#4D4D4D'
                });

                var suggestionCompleteFn = function() {
                    // When the suggestion has finished being added, decrement the counter
                    if (qcssapp.CurrentOrder.suggestionsInProgress) {
                        qcssapp.CurrentOrder.suggestionsInProgress--;
                    }
                };

                if (qcssapp.CurrentOrder.products.length > 1) {
                    qcssapp.Functions.checkProductCombos(suggestionCompleteFn);
                } else {
                    suggestionCompleteFn();
                }
            });
        }.bind(this);

        qcssapp.Functions.loadProductDetail(productId, true, successFn);
    };

    // ************ PRODUCT COMBO RELATED DETAILS ******************* //

    // Show product detail view when clicking on combo product line in Combo View
    qcssapp.Functions.loadProductFromCombo= function(productModel) {
        qcssapp.Functions.showMask(false, true);

        new qcssapp.Views.ProductDetailView(productModel, 'combo', true);

        qcssapp.Router.navigate('product-detail/' + productModel.get('id'), {trigger: true});
    };

    // Add product to cart and navigate to cart
    qcssapp.Functions.loadProductComboKeypad = function(productId) {
        var successFn = function(response) {

            var productModel = new qcssapp.Models.ProductDetail(response, {parse: response});

            qcssapp.Functions.substituteComboInCart(productModel);
        };

        qcssapp.Functions.loadProductDetail(productId, true, successFn);
    };

    // For swapping a product in the combo view, update ComboInProgress with swapped product
    qcssapp.Functions.loadProductCombo = function(productId, keypadModel) {
        var successFn = function(response) {

            //create new product model
            var productModel = new qcssapp.Models.ProductDetail(response, {parse: response});

            if(qcssapp.Functions.checkNegativePricing(productModel)) {
                qcssapp.Functions.displayPopup('Cannot add product to the combo. Product does not have the required pricing.', 'Invalid Product', 'OK');
                return;
            }

            if(qcssapp.Functions.checkWeightedProduct(productModel)) {
                qcssapp.Functions.displayPopup('Cannot add weighted products to a combo.', 'Invalid Product', 'OK');
                return;
            }

            if(productModel.get('image') == ''){
                productModel.set('image', keypadModel.get('image')); //If no image is loaded from the product info, and the keypad has an image for the product, populate with that image.
            }

            if(qcssapp.CurrentCombo.swapIndex != null) {
                var replacementProduct = qcssapp.ComboInProgress[qcssapp.CurrentCombo.swapIndex];
                productModel.set('quantity', replacementProduct.get('quantity'));
                productModel.set('comboTransLineItemId', replacementProduct.get('comboTransLineItemId'));
                qcssapp.CurrentCombo.updatedComboTransLineId = replacementProduct.get('comboTransLineItemId');

                qcssapp.ComboInProgress.splice(qcssapp.CurrentCombo.swapIndex, 1, productModel);
                qcssapp.CurrentCombo.swapIndex = null;

            } else {
                qcssapp.ComboInProgress.push(productModel);
            }

            qcssapp.Functions.loadComboKeypad();
        };

        qcssapp.Functions.loadProductDetail(productId, true, successFn);

    };

    // ************ PRODUCT FAVORITE RELATED DETAILS ******************* //

    // For swiping favorite products, add product to cart without navigating to cart
    qcssapp.Functions.goToProductDetailFromFavorite = function(productFavoriteModel) {
        qcssapp.Functions.showMask(false, true);

        var successFn = function(response) {
            //check if the product has any purchase restrictions
            if( !qcssapp.Functions.checkPurchaseRestriction(response) ) {
                return;
            }

            //create new product model
            var productModel = new qcssapp.Models.ProductDetail(response, {parse: response});

            productModel.set('modifiers', productFavoriteModel.get('modifiers'));
            productModel.set('prepOption', productFavoriteModel.get('prepOption'));
            productModel.set('favoriteID', productFavoriteModel.get('id'));

            if(productFavoriteModel.get('unavailableModifiers') && productFavoriteModel.get('unavailableModifiers').length) {
                qcssapp.Functions.removeUnavailableMods(productModel, productFavoriteModel);
            }

            //url path
            var navigatePathStr = 'product-detail/' + productModel.get('id');
            var previousPathStr = window.location.hash.replace('#', '');

            if(previousPathStr.indexOf('product-detail') != -1) {
                return;
            }

            new qcssapp.Views.ProductDetailView(productModel, previousPathStr);
            qcssapp.Router.navigate(navigatePathStr, {trigger: true});

            if(productFavoriteModel.get('unavailableModifiers') != "" || productFavoriteModel.get('unavailableModPrepOptions') != "" || productFavoriteModel.get('unavailablePrepOption') != false) {
                qcssapp.Functions.displayPopup("This favorite has modifiers or prep options that are no longer available. Please make any necessary adjustments before adding to the order.", 'Warning', 'OK', '', '', '', '', '', $('#product-detail-page'));
            }
        };

        var productId = productFavoriteModel.get('productID').toString();

        qcssapp.Functions.loadProductDetail(productId, true, successFn);

    };

    // Add product to cart and navigate to cart
    qcssapp.Functions.loadProductFavorite = function(productModel) {
        var successFn = function(response) {

            var productModel = new qcssapp.Models.ProductDetail(response, {parse: response});
            if(qcssapp.ProductInProgress.prepOption){
                productModel.set("prepOption", qcssapp.ProductInProgress.prepOption);
            }

            qcssapp.Functions.addProductToCart(productModel);
        };

        var productId = productModel.get('productID');
        var modifierList = productModel.get('modifiers');

        qcssapp.Functions.loadProductDetail(productId, true, successFn, '', modifierList);
    };

    // Remove unavailable modifiers from product list
    qcssapp.Functions.removeUnavailableMods = function(productModel, productFavoriteModel) {
        try {
            var unavailableMods = productFavoriteModel.get('unavailableModifiers').split(',');
            $.each(unavailableMods, function(index, modID) {
                productModel.set('modifiers', $.grep(productModel.get('modifiers'), function(mod) {
                    return mod.id != Number(modID);
                }));
            });
        } catch (e) {
            qcssapp.Functions.logError(e.message, 'qcssapp.Functions.removeUnavailableMods', 513, 0, e.name);
        }
    };

    // ************ PRODUCT NUTRITION RELATED DETAILS ******************* //

    // Build the nutrition models including the nutrition categories
    qcssapp.Functions.buildNutritionModels = function(model) {
        var nutritionArray = [];

        if(model.get('nutritionInfo1') !== '' && model.get('nutritionInfo1') != null && qcssapp.nutrition.nutritionCategory1 != null) {
            var nutritionInfoModel1 = new qcssapp.Models.ProductNutritionModel({
                value:  model.get('nutritionInfo1'),
                name: qcssapp.nutrition.nutritionCategory1['name'],
                shortName: qcssapp.nutrition.nutritionCategory1['shortName'],
                measurementLbl: qcssapp.nutrition.nutritionCategory1['measurementLbl'],
                nutritionInfo: model.get('nutritionInfo1') + ' ' + qcssapp.nutrition.nutritionCategory1['measurementLbl']
            });

            nutritionArray.push(nutritionInfoModel1);
        } else {
            nutritionArray.push(null);
        }

        if(model.get('nutritionInfo2') !== '' && model.get('nutritionInfo2') != null && qcssapp.nutrition.nutritionCategory2 != null) {
            var nutritionInfoModel2 = new qcssapp.Models.ProductNutritionModel({
                value:  model.get('nutritionInfo2'),
                name: qcssapp.nutrition.nutritionCategory2['name'],
                shortName: qcssapp.nutrition.nutritionCategory2['shortName'],
                measurementLbl: qcssapp.nutrition.nutritionCategory2['measurementLbl'],
                nutritionInfo: model.get('nutritionInfo2') + ' ' + qcssapp.nutrition.nutritionCategory2['measurementLbl']
            });

            nutritionArray.push(nutritionInfoModel2);
        } else {
            nutritionArray.push(null);
        }

        if(model.get('nutritionInfo3') !== '' && model.get('nutritionInfo3') != null && qcssapp.nutrition.nutritionCategory3 != null) {
            var nutritionInfoModel3 = new qcssapp.Models.ProductNutritionModel({
                value:  model.get('nutritionInfo3'),
                name: qcssapp.nutrition.nutritionCategory3['name'],
                shortName: qcssapp.nutrition.nutritionCategory3['shortName'],
                measurementLbl: qcssapp.nutrition.nutritionCategory3['measurementLbl'],
                nutritionInfo: model.get('nutritionInfo3') + ' ' + qcssapp.nutrition.nutritionCategory3['measurementLbl']
            });

            nutritionArray.push(nutritionInfoModel3);
        } else {
            nutritionArray.push(null);
        }

        if(model.get('nutritionInfo4') !== '' && model.get('nutritionInfo4') != null && qcssapp.nutrition.nutritionCategory4 != null) {
            var nutritionInfoModel4 = new qcssapp.Models.ProductNutritionModel({
                value:  model.get('nutritionInfo4'),
                name: qcssapp.nutrition.nutritionCategory4['name'],
                shortName: qcssapp.nutrition.nutritionCategory4['shortName'],
                measurementLbl: qcssapp.nutrition.nutritionCategory4['measurementLbl'],
                nutritionInfo: model.get('nutritionInfo4') + ' ' + qcssapp.nutrition.nutritionCategory4['measurementLbl']
            });

            nutritionArray.push(nutritionInfoModel4);
        } else {
            nutritionArray.push(null);
        }

        if(model.get('nutritionInfo5') !== '' && model.get('nutritionInfo5') != null && qcssapp.nutrition.nutritionCategory5 != null) {
            var nutritionInfoModel5 = new qcssapp.Models.ProductNutritionModel({
                value:  model.get('nutritionInfo5'),
                name: qcssapp.nutrition.nutritionCategory5['name'],
                shortName: qcssapp.nutrition.nutritionCategory5['shortName'],
                measurementLbl: qcssapp.nutrition.nutritionCategory5['measurementLbl'],
                nutritionInfo: model.get('nutritionInfo5') + ' ' + qcssapp.nutrition.nutritionCategory5['measurementLbl']
            });

            nutritionArray.push(nutritionInfoModel5);
        } else {
            nutritionArray.push(null);
        }

        return nutritionArray;

    };

    // Use for duplicating nutrition array for product detail view
    qcssapp.Functions.duplicateArray = function(array) {
        var newArray = [];

        $.each(array, function() {
            var model = this;

            if(model != null) {
                var nutritionInfoModel = new qcssapp.Models.ProductNutritionModel({
                    value:  model.get('value'),
                    name: model.get('name'),
                    shortName: model.get('shortName'),
                    measurementLbl: model.get('measurementLbl'),
                    nutritionInfo: model.get('nutritionInfo')
                });
            } else {
                nutritionInfoModel = null
            }

            newArray.push(nutritionInfoModel);
        });

        return newArray;
    };

    // ************ PRODUCT MODIFIER RELATED DETAILS ******************* //

    // Build the modifier array so it doesn't include properties that aren't on the Modifier Model
    qcssapp.Functions.buildModifierArray = function(modifiers) {
        var modifierArray = [];

        if(modifiers == null) {
            return modifierArray;
        }

        var productModifiers = JSON.parse(JSON.stringify(modifiers));

        $.each(productModifiers, function(index, mod) {
            delete mod.sortOrder;
            delete mod.index;
            delete mod.nutritionArray;
            delete mod.nutritionInfo1;
            delete mod.nutritionInfo2;
            delete mod.nutritionInfo3;
            delete mod.nutritionInfo4;
            delete mod.nutritionInfo5;
            delete mod.description;
            delete mod.hasPrinterMappings;

            modifierArray.push(mod);
        });

        return modifierArray;
    };

    // Update the select modifiers with the the predefined flag so they are shown as selected
    qcssapp.Functions.updateSelectedModifiers = function( productModel, modifierMenu ) {
        if(modifierMenu.get('modifiers').models.length == 0) {
            return modifierMenu;
        }

        var productModifiers = JSON.parse(JSON.stringify(productModel.get('modifiers')));

        // Reset the fields that mark modifier as selected
        $.each(modifierMenu.get('modifiers').models, function(index, menuModifier) {
            menuModifier.set('prepOption', null);
            menuModifier.selectedPrep = null;
            menuModifier.predefined = null;
        });

        // Loop over the modifiers on the product model
        $.each(productModifiers, function(index, modifier) {
            var keypadId = typeof modifier.keypadID !== 'undefined' ? modifier.keypadID : null;
            var prepOption = typeof modifier.prepOption !== 'undefined' ? modifier.prepOption : null;

            if( keypadId == null || Number(keypadId) != Number(modifierMenu.get('id')) ) {
                return;
            }

            // If the modifier is on a modifier menu, mark is as predefined so it shows as selected
            $.each(modifierMenu.get('modifiers').models, function(index, menuModifier) {
                if( typeof modifier.id === 'undefined' || Number(modifier.id) != Number(menuModifier.get('id')) ) {
                    return true;
                }

                menuModifier.predefined = true;

                // If the product modifier has a prep option, set it on the menu modifier's prep option
                if(prepOption != null && menuModifier.get('prepOptions').length) {
                    prepOption = new qcssapp.Models.PrepOption(prepOption);
                    menuModifier.set('prepOption', prepOption);
                }

                return false;
            });

        });

        return modifierMenu;
    };


    // ************ PRODUCT PREP OPTION RELATED DETAILS ******************* //

    // Create the prep option model
    qcssapp.Functions.createPrepOptionModel = function(prepOption) {

        return new qcssapp.Models.PrepOption({
            id: prepOption.id,
            name: prepOption.name,
            prepOptionSetID: prepOption.prepOptionSetID,
            sortOrder: prepOption.sortOrder,
            price: prepOption.price,
            defaultOption: prepOption.defaultOption,
            displayDefault: prepOption.displayDefault,
            buttonText: prepOption.buttonText
        });

    };

})(qcssapp, jQuery);