;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.StorePaymentModel - Base Model for storing payment details for store
     */
    qcssapp.Models.StorePaymentModel = Backbone.Model.extend({
        defaults: {
            id: '',
            hasQuickChargeBalance: true,
            paymentMethodCollection: [],
            accountFundingName: "Account Funding",
            allowMyQCFunding: true,
            paymentProcessor: null,
            userBalance: 0
        }
    });
})(qcssapp);