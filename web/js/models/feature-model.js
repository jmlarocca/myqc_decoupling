;(function (qcssapp) {
	'use strict'; //activates strict mode for this scope

	/* qcssapp.Models.Feature - list item for display in main navigation
	*/	
    qcssapp.Models.Feature = Backbone.Model.extend({
        defaults: {
            id: '',
            version: 'N/A',
            available: 'no'
        }
    });
})(qcssapp);