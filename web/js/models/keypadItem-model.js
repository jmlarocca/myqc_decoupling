;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.KeypadItems - Base Model for Store objects
     */
    qcssapp.Models.KeypadItems = Backbone.Model.extend({
        defaults: {
            id: '',
            name: "No Data Available",
            type: "",
            price: "",
            info: "" ,
            image: "",
            iconPosition: null,
            shrinkImageToFit: false,
            showNameOverlay: true,
            showPriceOverlay: true,
            items: [],
            imageLoaded: false,
            healthy: false,
            vegetarian: false,
            vegan: false,
            glutenFree: false,
            nutritionInfo: [],
            prepOptionSetID: "",
            prepOptions: [],
            defaultModifier: false,
            autoShowModSet: true,
            description: '',
            comboID: '',
            scaleUsed: false,
            imageShapeID: '',
            priceBelowName: false,
            fontIsBold: true,
            hasPrinterMappings: true
        },

        initialize: function() {
            this.set('imageLoaded', false);
        },

        toggleImageLoaded: function() {
            this.set('imageLoaded', true);
        }
    });
})(qcssapp);