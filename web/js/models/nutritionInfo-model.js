;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Balances - Base Model for Balance objects
     */
    qcssapp.Models.NutritionInfoModel = Backbone.Model.extend({
        defaults: {
            nutritionInfo: ''
        }
    });
})(qcssapp);