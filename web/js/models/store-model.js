;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Store - Base Model for Store objects
     */
    qcssapp.Models.Store = Backbone.Model.extend({
        defaults: {
            name: 'No Data Available',
            balance: "",
            id: ''
        }
    });
})(qcssapp);