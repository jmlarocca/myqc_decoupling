;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Term - Base Model for spending profile option objects
     */
    qcssapp.Models.Term = Backbone.Model.extend({
        defaults: {
            id: 0,
            title: "",
            content: ""
        }
    });
})(qcssapp);