;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Modifier - Base Model for modifier objects
     */
    qcssapp.Models.Modifier = Backbone.Model.extend({
        defaults: {
            id: 0,
            index: 0,
            keypadID: null,
            prepOptionSetID: null,
            name: '',
            description: '',
            sortOrder: '',
            price: '',
            nutritionInfo1: '',
            nutritionInfo2: '',
            nutritionInfo3: '',
            nutritionInfo4: '',
            nutritionInfo5: '',
            hasNutrition: false,
            hasPrinterMappings: true,
            vegetarian: false,
            vegan: false,
            glutenFree: false,
            healthy: false,
            defaultModifier: false,
            prepOption: null,
            taxIDs: null            ////Defect 4108: TaxIDs need to be included for purchase restrictions
        }
    });
})(qcssapp);