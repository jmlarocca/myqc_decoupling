;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.ProductDetail - Base Model for storing products
     */
    qcssapp.Models.ProductDetail = Backbone.Model.extend({
        defaults: {
            id: '',
            productID: '',
            index: '',
            name: '',
            description: '',
            price: '',
            originalPrice: '',
            priceOpen: false,
            quantity: 1,
            fromCart: false,
            keypadID: null,

            modSetDetailID: '',
            modifiers: [],
            modifierMenus: [],
            modifiersList: '',
            autoShowModSet: true,

            hasNutrition: false,
            nutritionInfo1: '',
            nutritionInfo2: '',
            nutritionInfo3: '',
            nutritionInfo4: '',
            nutritionInfo5: '',
            healthy: false,
            vegetarian: false,
            vegan: false,
            glutenFree: false,

            upcharge: '',
            comboTransLineItemId: '',
            comboDetailId: '',
            comboModel: {},
            comboPrice: '',

            favorite: false,
            popular: false,
            diningOptionProduct: false,

            scaleUsed: false,
            fixedTareWeight:'',
            tareName:'',
            tareInfo:'',
            grossWeight:'',
            grossWeightInfo:'',

            prepOptionSetID: '',
            prepOption: {},

            complementaryKeypadID: null,
            subDeptComplementaryKeypadID: null,
            subDeptSimilarKeypadID: null,
            subDeptID: null,
            departmentID: null,

            hasPrinterMappings: true,
            productInStore: true,

            lastUpdateDTM: '',
            lastUpdateCheck: '',

            disclaimers:[]
        },
        parse: function( response ) {
            response.productID = response.id;

            if(response.modifiers == null) {
                response.modifiers = [];
            }

            var $activeView = $('.view.active');
            if($activeView.attr('id') == 'keypadview') {
                var $keypadPage = $activeView.find('.keypad-page');
                var id = $keypadPage.attr('data-id');
                response.keypadID = qcssapp.Functions.isNumeric(id) ? Number(id) : null;
            }

            if(qcssapp.ProductInProgress.prepOptionSetID) {
                response.prepOptionSetID = qcssapp.ProductInProgress.prepOptionSetID;
            }

            if(response.prepOption && response.prepOption.prepOptionSetID && !response.prepOptionSetID) {
                response.prepOptionSetID = response.prepOption.prepOptionSetID;
            }

            if(response.prepOption && !response.prepOption.attributes) {
                response.prepOption = new qcssapp.Models.PrepOption(response.prepOption);
            }

            return response;
        }
    });
})(qcssapp);