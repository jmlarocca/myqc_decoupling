;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Balances - Base Model for Balance objects
     */
    qcssapp.Models.AccountGroup = Backbone.Model.extend({
        defaults: {
            id :0,
            accountGroupName :"",
            accountGroupID :0,
            accountTypeID:0,
            accountTypeName:"",
            availableForAcctCreation: false,
            spendingProfiles : []
        }
    });
})(qcssapp);