;(function (qcssapp) {
	'use strict'; //activates strict mode for this scope

	/* qcssapp.Models.Item - list item for display in main navigation
	*/	
    qcssapp.Models.Item = Backbone.Model.extend({
        defaults: {
            title: '',
            id: ''
        }
    });
})(qcssapp);