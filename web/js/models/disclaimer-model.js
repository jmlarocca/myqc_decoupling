;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Disclaimer - Base Model for disclaimer
     */
    qcssapp.Models.Disclaimer = Backbone.Model.extend({
        defaults: {
            id: '',
            name: '',
            text: '',
            title: '',
            type: ''
        }
    });
})(qcssapp);