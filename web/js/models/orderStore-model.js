;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.OrderStore - store listing for online ordering
     */
    qcssapp.Models.OrderStore = Backbone.Model.extend({
        defaults: {
            "id": "",
            "header1": "",
            "header2": "",
            "header3": "",
            "header4": "",
            "header5": "",
            "venueCategory": "",
            "pickUpOrderingOpenTime": "",
            "pickUpOrderingCloseTime": "",
            "pickupPrepTime": "",
            "deliveryOrderingOpenTime": "",
            "deliveryOrderingCloseTime": "",
            "deliveryPrepTime": "",
            "deliveryMinimum": "",
            "reAuthTypeID": 1,
            "currentStoreTime": "",
            "clientTimezoneOffset": 0,
            "requirePhoneNum": true,
            "usesSMSNotifications": false,
            "usesEatInTakeOut": false,
            "enableFavorites": false,
            "usesCreditCardAsTender": false,
            "hasQuickChargeBalance": false,
            "accountFundingPageName": '',
            "paymentMethod": [],
            "pickupWaitTime": "",
            "deliveryWaitTime": "",
            "tokenizedFileName": "",
            "iconPosition": 1,
            "maxNumDaysForFutureOrders": 0,
            'terminalID': ''
        },
        parse: function(response) {
            response.header1 = qcssapp.Functions.htmlDecode(response.header1);
            response.header2 = qcssapp.Functions.htmlDecode(response.header2);
            response.header3 = qcssapp.Functions.htmlDecode(response.header3);
            response.header4 = qcssapp.Functions.htmlDecode(response.header4);
            response.header5 = qcssapp.Functions.htmlDecode(response.header5);
            response.pickUpOrderingOpenTime = qcssapp.Functions.htmlDecode(response.pickUpOrderingOpenTime);
            response.pickUpOrderingCloseTime = qcssapp.Functions.htmlDecode(response.pickUpOrderingCloseTime);
            response.pickupCloseTime = qcssapp.Functions.htmlDecode(response.pickupCloseTime);
            response.deliveryOrderingOpenTime = qcssapp.Functions.htmlDecode(response.deliveryOrderingOpenTime);
            response.deliveryOrderingCloseTime = qcssapp.Functions.htmlDecode(response.deliveryOrderingCloseTime);
            response.deliveryCloseTime = qcssapp.Functions.htmlDecode(response.deliveryCloseTime);
            response.currentStoreTime = qcssapp.Functions.htmlDecode(response.currentStoreTime);

            return response;
        }
    });
})(qcssapp);


