;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Modal - Base Model for detail objects
     */
    qcssapp.Models.LocationFilterSettings = Backbone.Model.extend({
        defaults: {
            myQCAllowOnlineOrderLocation: 0,
            myQCAllowOnlineOrderLocationSkip: 0,
            myQCPromptBeforeOnlineOrder:0
        }
    });
})(qcssapp);