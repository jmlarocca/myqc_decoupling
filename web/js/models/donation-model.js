;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Donation - Base Model for donation
     */
    qcssapp.Models.Donation = Backbone.Model.extend({
        defaults: {
            id: '',
            name: '',
            description: '',
            terminalId: '',
            donationTypeId: '',
            loyaltyProgram: {}
        }
    });
})(qcssapp);