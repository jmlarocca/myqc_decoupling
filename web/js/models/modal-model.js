;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Modal - Base Model for detail objects
     */
    qcssapp.Models.Modal = Backbone.Model.extend({
        defaults: {
            title: '',
            lineData: ''
        }
    });
})(qcssapp);