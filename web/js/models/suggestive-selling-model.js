;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.SuggestiveSelling - Base Model for Suggestive Selling Keypads
     */
    qcssapp.Models.SuggestiveSelling = Backbone.Model.extend({
        defaults: {
            id: ''
        }
    });
})(qcssapp);