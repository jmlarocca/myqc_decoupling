;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.ProductNutritionModel - Base Model for Store objects
     */
    qcssapp.Models.ProductNutritionModel = Backbone.Model.extend({
        defaults: {
            value: '',
            name: "",
            shortName: "",
            measurementLbl: "",
            nutritionInfo: ''
        }
    });
})(qcssapp);