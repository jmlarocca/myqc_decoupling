;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Balances - Base Model for Balance objects
     */
    qcssapp.Models.NutritionValues = Backbone.Model.extend({
        defaults: {
            id: '',
            pluID: 0,
            quantity: '',
            productName: '',
            nutritionInfo1: '',
            nutritionInfo2: '',
            nutritionInfo3: '',
            nutritionInfo4: '',
            nutritionInfo5: ''
        }
    });
})(qcssapp);