;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.SurchargeLine - Base Model for Surcharge objects
     */
    qcssapp.Models.SurchargeLine = Backbone.Model.extend({
        defaults: {
            id: '',
            name: '',
            amount: ''
        }
    });
})(qcssapp);