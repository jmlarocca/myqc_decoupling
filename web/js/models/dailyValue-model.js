;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Balances - Base Model for Balance objects
     */
    qcssapp.Models.DailyValue = Backbone.Model.extend({
        defaults: {
            id: '',
            dvLabel: '',
            dvTotal1: '',
            dvTotal2: '',
            dvTotal3: '',
            dvTotal4: '',
            dvTotal5: ''
        }
    });
})(qcssapp);