;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.PurchaseRestriction - purchase restriction for items based on product, department or sub department
     */
    qcssapp.Models.PurchaseRestriction = Backbone.Model.extend({
        defaults: {
            id: '',
            papluID: '',
            pasubdeptID: '',
            padepartmentID: ''
        }
    });
})(qcssapp);


