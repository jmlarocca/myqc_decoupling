;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.ErrorMsg - Base Model for detail objects
     */
    qcssapp.Models.ErrorMsg = Backbone.Model.extend({
        defaults: {
            title: '',
            content: ''
        }
    });
})(qcssapp);