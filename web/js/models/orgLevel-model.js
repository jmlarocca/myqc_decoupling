;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.OrgLevel - holds information related to an org level
     */
    qcssapp.Models.OrgLevel = Backbone.Model.extend({
        defaults: {
            name: '',
            orgLevelPrimaryId: null,
            pickupAvailable: null,
            deliveryAvailable: null
        }
    });
})(qcssapp);


