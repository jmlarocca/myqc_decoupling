;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Product - Base Model for Store objects
     */
    qcssapp.Models.Product = Backbone.Model.extend({
        defaults: {
            id: '',
            productID: '',
            name: '',
            description: '',
            image: '',
            price: '',
            originalPrice: '',
            priceOpen: false,
            quantity: 1,
            modSetDetailID: '',
            modifiers: [],
            modifiersList: '',
            fromCart: false,
            hasNutrition: false,
            upcharge: '',
            comboTransLineItemId: '',
            comboDetailId: '',
            comboModel: {},
            keypadID: null,
            comboPrice: '',
            healthy: false,
            vegetarian: false,
            vegan: false,
            glutenFree: false,
            favorite: false,
            popular: false,
            diningOptionProduct: false,
            scaleUsed: false,
            fixedTareWeight:'',
            tareName:'',
            tareInfo:'',
            grossWeight:'',
            grossWeightInfo:'',
            autoShowModSet: true,
            prepOptionSetID: '',
            prepOption: {},
            complementaryKeypadID: null,
            subDeptComplementaryKeypadID: null,
            subDeptSimilarKeypadID: null,
            subDeptID: null,
            departmentID: null,
            hasPrinterMappings: true,
            productInStore: true
        },
        parse: function (response){
            var $checkView = $('.view.active');
            if($checkView.attr('id') !== 'mainview') {
                response.productID = response.id;
            }

            if(response.prepOption && !response.prepOption.attributes) {
                response.prepOption = new qcssapp.Models.PrepOption(response.prepOption);
            }

            if (response.comboModel && !response.comboModel.attributes) {
                response.comboModel = new qcssapp.Models.Combo(response.comboModel);
            }

            return response;
        }
    });
})(qcssapp);