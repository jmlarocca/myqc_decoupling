;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.ComboDetail - Base Model for ComboDetail objects
     */
    qcssapp.Models.ComboDetail = Backbone.Model.extend({
        defaults: {
            id: '',
            comboId: '',
            basePricePAPluId: '',
            basePrice: '',
            comboPrice: '',
            keypadId: '',
            keypadDetailPAPluIds: []
        }
    });
})(qcssapp);