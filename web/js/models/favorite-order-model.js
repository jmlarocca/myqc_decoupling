;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.FavoriteOrder - Base Model for Favorite Orders
     */
    qcssapp.Models.FavoriteOrder = Backbone.Model.extend({
        defaults: {
            id: '',
            name: '',
            available: '',
            active: false,
            items: []
        }
    });
})(qcssapp);