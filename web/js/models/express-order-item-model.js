;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.ExpressOrderItem - Base Model for Express Order Items
     */
    qcssapp.Models.ExpressOrderItem = Backbone.Model.extend({
        defaults: {
            id: '',
            lineID: '',
            productID: '',
            keypadID: '',
            name: '',
            price: '',
            quantity: '',
            originalPrice: '',
            modifiers: [],
            modifiersList: '',
            scaleUsed: false,
            modSetDetailID: '',
            autoShowModSet: false,
            comboTransLineItemId: '',
            comboDetailId: '',
            basePrice: 0,
            comboPrice: 0,
            upcharge: '',
            comboName: '',
            image: '',
            fixedTareWeight:'',
            tareName:'',
            tareInfo:'',
            grossWeight:'',
            grossWeightInfo:'',
            hasPrinterMappings: true
        }
    });
})(qcssapp);