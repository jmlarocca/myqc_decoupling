;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.AccountPerson - Base Model for Account Person objects
     */
    qcssapp.Models.AccountPerson = Backbone.Model.extend({
        defaults: {
            id :0,
            name: "",
            active :false,
            personRelationshipID: "",
            AccountModel: ""
        }
    });
})(qcssapp);