;(function (qcssapp) {
	'use strict'; //activates strict mode for this scope

	/* qcssapp.Models.Transaction - Base Model for Transaction objects
	*/	
    qcssapp.Models.Transaction = Backbone.Model.extend({
        defaults: {
            store: 'Default',
            date: '',
            amt: 0.00,
            id: '',
            paTransactionID: '',
            paorderTypeID: '',
            terminalInProfile: false,
            showReceiptsOnMyQC: true,
            showReorder: true,
            enableFavorites: true,
            transTypeID: '',
            refundID: '',
            duplicateTxn: false
        }
    });
})(qcssapp);