;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.PaymentProcessor - Base Model for FundingAmount objects
     */
    qcssapp.Models.PaymentProcessor = Backbone.Model.extend({
        defaults: {
            "id": null,
            "publicAPIKey": null,
            "paymentPageFileName": null
        }
    });
})(qcssapp);