;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Deductions - Base Model for Deductions objects
     */
    qcssapp.Models.Deductions = Backbone.Model.extend({
        defaults: {
            code: 'Default',
            date: '',
            amt: '',
            id: ''
        }
    });
})(qcssapp);