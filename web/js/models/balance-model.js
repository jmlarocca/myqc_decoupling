;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Balances - Base Model for Balance objects
     */
    qcssapp.Models.Balance = Backbone.Model.extend({
        defaults: {
            name: 'Default',
            limit: 0.00,
            balance: 0.00,
            avail: 0.00,
            balanceValue: 0.00,
            availValue: 0.00,
            id: '',
            stores: {}
        }
    });
})(qcssapp);