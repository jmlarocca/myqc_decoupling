;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.FundingAmount - Base Model for FundingAmount objects
     */
    qcssapp.Models.FundingAmount = Backbone.Model.extend({
        defaults: {
            "id": null,
            "amountTypeID": null,
            "amount": null,
            "defaulted": false,
            "selected": false
        }
    });
})(qcssapp);