;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.RewardHistory - Base Model for a reward history record
     */
    qcssapp.Models.RewardHistory = Backbone.Model.extend({
        defaults: {
            id: 0, //this should be the PATransactionID
            paorderTypeID: 1, //this should be the PATransactionID
            storeName: "",
            revCenterName: "",
            timeStamp: "",
            points: 0,
            redeemedReward: false,
            showReceiptsOnMyQC: false,
            showReorder: false,
            giftIcon: "",
            enableFavorites: true
        },
        parse: function(response) {
            response.patransactionID = response.id;  //set the patransactionID as the id for the receipt view

            return response;
        }
    });
})(qcssapp);