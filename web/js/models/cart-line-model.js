;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.CartLine - Base Model for Store objects
     */
    qcssapp.Models.CartLine = Backbone.Model.extend({
        defaults: {
            id: '',
            cartProductID: '',
            name: "No Data Available",
            price: "",
            quantity: "",
            total: "",
            healthy: false,
            vegetarian: false,
            vegan: false,
            glutenFree: false,
            tareInfo:"",
            grossWeightInfo:""
        }
    });
})(qcssapp);