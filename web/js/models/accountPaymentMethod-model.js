;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.AccountPaymentMethod - Base Model for FundingAmount objects
     */
    qcssapp.Models.AccountPaymentMethod = Backbone.Model.extend({
        defaults: {
            id : null,
            paymentMethodTypeID : null,
            paymentMethodType : null,
            publicAPIKey : null,
            processorPaymentOneTimeToken : null,
            paymentMethodFullName : null,
            paymentMethodEmailAddress : null,
            paymentMethodLastFour : null,
            paymentMethodExpirationMonth : null,
            paymentMethodExpirationYear : null,
            paymentMethodZipCode : null,
            modelIsDefault: false,
            personID: null
        },
        parse: function(response) {
            var paymentMethodType;

            switch ( response.paymentMethodTypeID ) {
                case (1):
                    paymentMethodType = 'visa';
                    break;
                case (2):
                    paymentMethodType = 'amex';
                    break;
                case (3):
                    paymentMethodType = 'mastercard';
                    break;
                case (4):
                    paymentMethodType = 'discover';
                    break;
                case (5):
                    paymentMethodType = 'jcb';
                    break;
                case (6):
                    paymentMethodType = 'dinersclub';
                    break;
                case (7):
                    paymentMethodType = '';
                    break;
                }

            response.paymentMethodType = paymentMethodType;

            return response;
        }
    });
})(qcssapp);