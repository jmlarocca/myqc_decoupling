;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Reward - Base Model for a reward
     */
    qcssapp.Models.Reward = Backbone.Model.extend({
        defaults: {
            id: 0,
            name: "",
            value: 0,
            typeName: "",
            rewardTypeID: 0,
            pointsToRedeem: null,
            redeemable: false,
            programID: null,
            maxPerTransaction: null,
            quantity: 0,
            canUseWithOtherRewards: false,
            autoPayout: false
        },

        incrementQuantity: function() {
            if ( this.get('redeemable') == false || this.get('quantity') == this.get('maxPerTransaction') ) {
                return;
            }

            this.set('quantity', Number(this.get('quantity')) + 1);
        },

        decrementQuantity: function() {
            if ( this.get('quantity') == 0 ) {
                return;
            }

            this.set('quantity', Number(this.get('quantity')) - 1);
        }
    });
})(qcssapp);