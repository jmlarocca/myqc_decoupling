;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.ProductFavorite - Base Model for Favorited Products
     */
    qcssapp.Models.ProductFavorite = Backbone.Model.extend({
        defaults: {
            id: '',
            productID: '',
            keypadID: '',
            modifiers: [],
            prepOption: {},
            active: false,
            orderingFavoriteID: ''
        }
    });
})(qcssapp);