;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.FirstLoginModel - list item for display in main navigation
     */
    qcssapp.Models.FirstLoginModel = Backbone.Model.extend({
        localStorage:new Backbone.LocalStorage('mmh-user-logged999'),
        defaults: {
            id: 1,
            logged: 'false'
        },
        parse: function(data) {
            if ( data && data[0] ) {
                data = data[0];
            }

            return data
        }
    });
})(qcssapp);