;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.instanceCode - Base Model for spending profile option objects
     */
    qcssapp.Models.instanceCode = Backbone.Model.extend({
        idAttribute: 'CODE',
        defaults: {
            LOCATION: qcssapp.Location,
            CODE: '',
            NAME: '',
            ALIAS: '',
            INSTANCEID: 0,
            INSTANCEUSERID: 0,
            USED: 0,
            ALLOWINVALIDSSLCERTS: false
        }
    });
})(qcssapp);