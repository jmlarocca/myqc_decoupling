;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.ModifierMenu - Base Model for modifier menu objects
     */
    qcssapp.Models.ModifierMenu = Backbone.Model.extend({
        defaults: {
            id: 0,
            name: '',
            description: '',
            min: null,
            max: null,
            modSetDetailID: null,
            modifiers: [],
            keypadID: null
        }
    });
})(qcssapp);