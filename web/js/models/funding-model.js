;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Funding - Base Model for Funding objects
     */
    qcssapp.Models.Funding = Backbone.Model.extend({
        defaults: {
            comment: 'Default',
            date: '',
            amt: '',
            id: '',
            fundedBy: '',
            fundingFee: ''
        }
    });
})(qcssapp);