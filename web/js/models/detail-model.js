;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Details - Base Model for detail objects
     */
    qcssapp.Models.Details = Backbone.Model.extend({
        defaults: {
            name: 'Default',
            number: 'Default',
            type: '0',
            status: null
        }
    });
})(qcssapp);