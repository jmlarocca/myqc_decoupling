;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Person - holds information related to a person account
     */
    qcssapp.Models.Person = Backbone.Model.extend({
        defaults: {
            personID: null,
            lowBalanceThreshold: 20,
            accountPersonCollection: null
        }
    });
})(qcssapp);


