/**
 * Created by admin on 1/22/2016.
 */
;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.CartLine - Base Model for Store objects
     */
    qcssapp.Models.ReviewLine = Backbone.Model.extend({
        defaults: {
            id: '',
            name: "No Data Available",
            price: "",
            quantity: "",
            total: "",
            tareInfo:"",
            grossWeightInfo:""
        }
    });
})(qcssapp);