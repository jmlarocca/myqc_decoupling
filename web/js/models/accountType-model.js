;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.AccountType - Base Model for account type option objects
     */
    qcssapp.Models.AccountType = Backbone.Model.extend({
        defaults: {
            name: 'Default',
            id: '',
            accountGroupID: ''
        }
    });
})(qcssapp);