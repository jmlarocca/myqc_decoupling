;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.AccountAutoFundingInfo - Base Model for FundingAmount objects
     */
    qcssapp.Models.AccountAutoFundingInfo = Backbone.Model.extend({
        defaults: {
            "reloadAmount": null,
            "reloadThreshold": null,
            "reloadPaymentMethodID": null,
            "reloadSetUpByPersonID": null,
            "reloadSetUpByName": null
        },
        parse: function(response) {
            response.reloadThreshold = Math.abs(response.reloadThreshold);

            return response;
        }
    });
})(qcssapp);