;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Balances - Base Model for Balance objects
     */
    qcssapp.Models.AccountInfoModel = Backbone.Model.extend({
        defaults: {
            username:"",
            email:"",
            password:"",
            accountGroup:"", //holds accountGroupID
            spendingProfile:"", //holds spendingProfileID
            accountType:"", //holds accountTypeID
            name:"",
            accountStatus:"",
            accountNumber:"",
            badgeNumber:"",
            instanceAlias:"",
            forceChangeAccountGroup: false,
            phone:'',
            mobilePhone: '',
            lowBalanceThreshold:'',
            employeeLowBalanceThreshold:'',
            badgeNumPrefix: '',
            badgeNumSuffix: '',
            id: '',
            qrCodeLength: '',
            orgLevelPrimaryData:{
                orgLevelPrimaryId:'0',
                name:'All Locations'
            }
        }
    });
})(qcssapp);