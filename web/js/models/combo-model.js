;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Combo - Base Model for Combo objects
     */
    qcssapp.Models.Combo = Backbone.Model.extend({
        defaults: {
            id: '',
            comboId: '',
            name: '',
            description: '',
            price: '',
            quantity: '',
            details: []
        }
    });
})(qcssapp);