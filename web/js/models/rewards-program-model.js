;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.RewardsProgram - Base Model for Reward Program objects
     */
    qcssapp.Models.RewardsProgram = Backbone.Model.extend({
        defaults: {
            id: 0,
            name: "",
            points: 0,
            newPoints: 0,
            description: "No details available.",
            expanded: false,
            showDetails: false,
            donations: []
        },

        initialize: function() {
            this.on('change:expanded', this.hideDetails);
        },

        expand: function() {
            this.set('expanded', true);
        },

        collapse: function() {
            this.set('expanded', false);
        },

        toggleExpanded: function() {
            this.set('expanded', !this.get('expanded'));
        },

        toggleShowDetails: function() {
            this.set('showDetails', !this.get('showDetails'));
        },

        hideDetails: function() {
            this.set('showDetails', false);
        }
    });
})(qcssapp);