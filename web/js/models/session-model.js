;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.SessionModel - list item for display in main navigation
     */
    qcssapp.Models.SessionModel = Backbone.Model.extend({
        localStorage:new Backbone.LocalStorage('mmh-user-session111'),
        defaults: {
            id: 1,
            sessionID: '',
            location: '',
            forcePassword: false,
            code: '',
            keepLogged: false,
            checkFingerprintOnDevice: false,
            resumeApp: false,
            badge: '',
            order: '',
            orderHistory: ''
        },
        parse: function(data) {
            if ( data && data[0] ) {
                data = data[0];
            }

            return data
        }
    });
})(qcssapp);