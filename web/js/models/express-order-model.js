;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.ExpressOrder - Base Model for Express Orders
     */
    qcssapp.Models.ExpressOrder = Backbone.Model.extend({
        defaults: {
            id: '',
            phone: '',
            tenderTypeID: '',
            usingDiningOptions: false,
            usingCreditCardAsTender: false,
            orderType: "",
            terminalID: '',
            storeID: '',
            storeName: '',
            total: 0,
            favoriteOrderID: '',
            favoriteOrderName: '',
            favoriteOrderActive: false,
            itemList: [],
            enableFavorites: true,
            comments: '',
            location: ''
        }
    });
})(qcssapp);