;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.spendingProfile - Base Model for spending profile option objects
     */
    qcssapp.Models.spendingProfile = Backbone.Model.extend({
        defaults: {
            value: 'Default',
            id: '',
            active: false,
            price: ''
        }
    });
})(qcssapp);