;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.GeneralMsg - Base Model for general msg objects
     */
    qcssapp.Models.GeneralMsg = Backbone.Model.extend({
        defaults: {
            title: 'Default',
            main: 'Default',
            content: 'Default'
        }
    });
})(qcssapp);