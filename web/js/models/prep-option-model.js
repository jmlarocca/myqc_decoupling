;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.PrepOption - Base Model for prep option objects
     */
    qcssapp.Models.PrepOption = Backbone.Model.extend({
        defaults: {
            id: 0,
            name: '',
            prepOptionSetID: '',
            sortOrder: '',
            price: '',
            defaultOption: false,
            displayDefault: false,
            buttonText: ''
        }
    });
})(qcssapp);