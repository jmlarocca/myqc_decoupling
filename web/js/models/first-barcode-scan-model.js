;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.FirstBarcodeScanModel - keeps track of whether the barcode popup has shown or not
     */
    qcssapp.Models.FirstBarcodeScanModel = Backbone.Model.extend({
        localStorage:new Backbone.LocalStorage('mmh-user-logged888'),
        defaults: {
            id: 1,
            scanPopupShown: 'false'
        },
        parse: function(data) {
            if ( data && data[0] ) {
                data = data[0];
            }

            return data
        }
    });
})(qcssapp);