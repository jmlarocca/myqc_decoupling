;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.Favorite - Base Model for Favorited Products
     */
    qcssapp.Models.Favorite = Backbone.Model.extend({
        defaults: {
            id: '',
            orderingFavoriteID: '',
            name: '',
            price: '',
            available: '',
            productID: '',
            active: false,
            modifiers: [],
            modifierNames: "",
            favorite: false,
            popular: false,
            recent: false,
            iconHTML: "",
            image:"",
            primary: "",
            imageLoaded: false,
            unavailableModifiers: "",
            unavailablePrepOption:false,
            popularModifierID: null,
            popularModifierDetailID: null,
            modifierSetID: null,
            modifierSetDetailID: null,
            keypadID: null,
            productCount: null,
            healthy: false,
            vegetarian: false,
            vegan: false,
            glutenFree: false,
            unavailableModPrepOptions: "",
            defaultPrepOption: null,
            prepOption: null,
            prepOptionSetID: null,
            autoShowModSet: true,
            nutritionInfo: []
        },

        initialize: function() {
            this.set('imageLoaded', false);
        },

        toggleImageLoaded: function() {
            this.set('imageLoaded', true);
        }
    });
})(qcssapp);