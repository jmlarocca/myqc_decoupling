;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.FundingBalance - Base Model for Balance objects
     */
    qcssapp.Models.FundingBalance = Backbone.Model.extend({
        defaults: {
            balance: null,
            lastSynced: ''
        },
        parse: function(response) {
            response.lastSynced = moment(response.lastSynced, 'MM/DD/YYYY hh:mm:ss A').format('MM/DD/YY h:mm A');

            var accountType = Number($(".user-information-title").attr('data-accountType'));
            if ( accountType && (accountType == 2 || accountType == 4 )) {
                response.balance = -1*response.balance;
            }

            response.balance = Number(response.balance).toFixed(2);

            return response;
        }
    });
})(qcssapp);