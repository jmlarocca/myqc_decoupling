;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.OrgLevel - holds information related to an org level
     */
    qcssapp.Models.UserLocationModel = Backbone.Model.extend({
        localStorage:new Backbone.LocalStorage('mmh-user-selectedLocation'),
        defaults: {
            id:1,
            hasSelectedLocation: false
        }
    });
})(qcssapp);
