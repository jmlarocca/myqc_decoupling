;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Models.FAQ - Base Model for FAQ objects
     */
    qcssapp.Models.FAQ= Backbone.Model.extend({
        defaults: {
            id: '',
            question: '',
            answer: '',
            linkToAccountFunding: false
        }
    });
})(qcssapp);