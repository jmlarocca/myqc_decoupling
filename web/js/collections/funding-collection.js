;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Funding - Contains a collection of Funding data
     - Loads data from REST API using GET
     */
    qcssapp.Collections.Funding = Backbone.Collection.extend({
        model: qcssapp.Models.Funding, //base model for this collection
        parse: function(response) {
            $.each(response, function() {
                this.amt = Number(this.amt).toFixed(2);
                this.fundingFee = Number(this.fundingFee).toFixed(2);
                this.date = moment(qcssapp.Functions.htmlDecode(this.date), 'MM/DD/YYYY hh:mm:ss A').format('MM/DD/YY h:mm A');
            });

            return response;
        }
    });
})(qcssapp);