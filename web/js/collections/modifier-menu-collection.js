;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.ModifierMenus - Contains a collection of modifier menus */

    qcssapp.Collections.ModifierMenus = Backbone.Collection.extend({
        model: qcssapp.Models.ModifierMenu, //base model for this collection
        parse: function(modMenu) {
            var index = 0;
            $.each(modMenu, function() {

                var modifierCollection = new qcssapp.Collections.Modifiers();

                if(typeof this.items !== 'undefined') {
                    modifierCollection.set(this.items, {parse:this.items});

                    modifierCollection.each(function( mod ) {
                        if( !mod.get('keypadID') ){
                            mod.set('keypadID', this.id);
                        }
                    }, this);

                    this.modifiers = modifierCollection;
                    delete this.items;

                } else if(typeof this.modifiers !== 'undefined' && typeof this.modifiers.models === 'undefined') {
                    modifierCollection.set(this.modifiers, {parse:this.modifiers});

                    this.modifiers = modifierCollection;
                }
            });

            return modMenu;
        }
    });
})(qcssapp);