(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.SuggestiveSellingCollection - Contains a collection of suggestive selling keypads*/
    qcssapp.Collections.SuggestiveSellingCollection = Backbone.Collection.extend({
        model: qcssapp.Models.SuggestiveSelling //base model for this collection
    });
})(qcssapp);