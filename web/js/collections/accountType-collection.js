;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.AccountType - Contains a collection of account type options
     - Loads data from REST API using GET
     */
    qcssapp.Collections.AccountType = Backbone.Collection.extend({
        model: qcssapp.Models.AccountType //base model for this collection
    });
})(qcssapp);