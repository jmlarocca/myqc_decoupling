;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.PrepOption - Contains a collection of prep options */

    qcssapp.Collections.PrepOptions = Backbone.Collection.extend({
        model: qcssapp.Models.PrepOption, //base model for this collection
        parse: function(prepOptions) {
            var newPrepArray = [];

            // Remove duplicate prep option ids
            $.each(prepOptions, function() {
                var prep = this;

                if(newPrepArray.length == 0) {
                    newPrepArray.push(prep);
                } else {
                    var duplicateFound = false;
                    $.each(newPrepArray, function() {
                        if(this.id == prep.id) {
                            duplicateFound = true;
                        }
                    });

                    if(!duplicateFound) {
                        newPrepArray.push(prep);
                    }
                }
            });

            return newPrepArray;
        }
    });
})(qcssapp);