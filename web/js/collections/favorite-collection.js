(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.FavoriteCollection - Contains a collection of favorite models for My Quick Picks page */
    qcssapp.Collections.FavoriteCollection = Backbone.Collection.extend({
        model: qcssapp.Models.Favorite, //base model for this collection
        parse: function(favorite) {
            $.each(favorite, function() {

                if(this.prepOption != null) {
                    var prepOptionModel = new qcssapp.Models.PrepOption({
                        id: this.prepOption.id,
                        name: this.prepOption.name,
                        prepOptionSetID: this.prepOption.prepOptionSetID,
                        sortOrder: this.prepOption.sortOrder,
                        price: this.prepOption.price,
                        defaultOption: this.prepOption.defaultOption,
                        displayDefault: this.prepOption.displayDefault,
                        buttonText: this.prepOption.buttonText
                    });

                    this.prepOption = prepOptionModel;
                }
            });

            return favorite;
        }
    });
})(qcssapp);