;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Deductions - Contains a collection of deductions
     - Loads data from REST API using GET
     */
    qcssapp.Collections.Deductions = Backbone.Collection.extend({
        model: qcssapp.Models.Deductions //base model for this collection
    });
})(qcssapp);