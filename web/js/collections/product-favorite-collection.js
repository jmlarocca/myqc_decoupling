(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.ProductFavorites - Contains a collection of favorite models for product detail view */
    qcssapp.Collections.ProductFavorites = Backbone.Collection.extend({
        model: qcssapp.Models.ProductFavorite //base model for this collection
    });
})(qcssapp);