;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.instanceCodes - Contains a collection of spending profile options
     - Loads data from REST API using GET
     */
    qcssapp.Collections.instanceCodes = Backbone.Collection.extend({
        model: qcssapp.Models.instanceCode, //base model for this collection

        localStorage : new Backbone.LocalStorage("instanceCodes111"),

        parse: function(response) {
            return response;
        }
    });
})(qcssapp);