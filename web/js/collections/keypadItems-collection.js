;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Balance - Contains a collection of keypad item models
     - Loads data from REST API using GET
     */
    qcssapp.Collections.KeypadItems = Backbone.Collection.extend({
        model: qcssapp.Models.KeypadItems //base model for this collection
    });
})(qcssapp);