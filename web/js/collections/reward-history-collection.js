(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.RewardHistory - Contains a collection of rewards history for a loyalty program */
    qcssapp.Collections.RewardHistory = Backbone.Collection.extend({
        model: qcssapp.Models.RewardHistory //base model for this collection
    });
})(qcssapp);