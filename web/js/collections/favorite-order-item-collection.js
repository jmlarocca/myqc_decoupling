(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.FavoriteOrderItemCollection - Contains a collection of favorite order models for My Quick Picks page */
    qcssapp.Collections.FavoriteOrderItemCollection = Backbone.Collection.extend({
        model: qcssapp.Models.Product //base model for this collection
    });
})(qcssapp);