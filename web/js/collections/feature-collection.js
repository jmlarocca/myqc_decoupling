;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Features - Contains a collection of book models
        - Loads data from REST API using GET
    */
    qcssapp.Collections.Features = Backbone.Collection.extend({
            model: qcssapp.Models.Feature //base model for this collection
    });
})(qcssapp);