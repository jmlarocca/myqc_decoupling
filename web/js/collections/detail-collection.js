;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Details - Contains a collection of details
     - Loads data from REST API using GET
     */
    qcssapp.Collections.Details = Backbone.Collection.extend({
        model: qcssapp.Models.Details, //base model for this collection
        url: function() {
            return qcssapp.Location + '/api/myqc/accountdetails'; //REST API endpoint for this collection
        }
    });
})(qcssapp);