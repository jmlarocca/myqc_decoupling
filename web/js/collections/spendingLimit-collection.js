(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Balance - Contains a collection of balance groups
     - Loads data from REST API using GET
     */
    qcssapp.Collections.Limits = Backbone.Collection.extend({
        model: qcssapp.Models.Balance //base model for this collection
    });
})(qcssapp);