(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Rewards - Contains a collection of rewards for a loyalty program */
    qcssapp.Collections.Rewards = Backbone.Collection.extend({
        model: qcssapp.Models.Reward, //base model for this collection
        parse: function(response, totalPoints) {
            $.each(response, function() {
                this.redeemable = Number(this.pointsToRedeem) <= Number(totalPoints);
            });
            return response;
        }
    });
})(qcssapp);