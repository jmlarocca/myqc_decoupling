(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.FAQs - Contains a collection of FAQs
     - Loads data from REST API using GET
     */
    qcssapp.Collections.FAQs = Backbone.Collection.extend({
        model: qcssapp.Models.FAQ //base model for this collection
    });
})(qcssapp);