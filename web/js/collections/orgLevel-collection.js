;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.OrgLevels - Contains a collection of org level models
     - Loads data from REST API using GET
     */
    qcssapp.Collections.OrgLevels = Backbone.Collection.extend({
        model: qcssapp.Models.OrgLevel //base model for this collection
    });
})(qcssapp);