(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Balance - Contains a collection of balance groups
     - Loads data from REST API using GET
     */
    qcssapp.Collections.Balances = Backbone.Collection.extend({
        model: qcssapp.Models.Balance, //base model for this collection
        parse: function(response) {

            $.each(response, function() {
                if ( this.avail == null || !qcssapp.Functions.isNumeric(this.avail)) { //if avail is null, then this balance group is "UNLIMITED"
                    this.avail = "";
                } else {
                    if ( Number(this.avail) < 0) {
                        this.avail = "0.00";
                    } else {
                        this.avail = this.avail.toFixed(2);
                    }
                }

                //format decimals
                this.balance = this.balance.toFixed(2);
                this.limit = this.limit.toFixed(2);

                //if stores are wrapped in "collection" object, remove it
                if ( this.stores.collection ) {
                    this.stores = this.stores.collection;
                }

                //format decimals in each store record
                $.each(this.stores, function() {
                    this.balance = this.balance.toFixed(2);
                });
            });
            return response;
        }
    });
})(qcssapp);