;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Items - Contains a collection of book models
        - Loads data from REST API using GET
    */
    qcssapp.Collections.Items = Backbone.Collection.extend({
            model: qcssapp.Models.Item //base model for this collection
    });
})(qcssapp);