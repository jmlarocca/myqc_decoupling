(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    qcssapp.Collections.NutritionCollection = Backbone.Collection.extend({
        model: qcssapp.Models.NutritionInfoModel //base model for this collection
    });
})(qcssapp);