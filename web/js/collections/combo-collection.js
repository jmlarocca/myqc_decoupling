(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Combo - Contains a collection of combos
     - Loads data from REST API using GET
     */
    qcssapp.Collections.Combo = Backbone.Collection.extend({
        model: qcssapp.Models.Combo, //base model for this collection
        parse: function(response) {
            var index = 0;
            $.each(response, function() {

                var comboDetailsCollection = new qcssapp.Collections.ComboDetail();
                comboDetailsCollection.set(this.combo.details);

                this.combo.details = comboDetailsCollection;
                this.details = comboDetailsCollection;

                this.name = this.combo.name;
                this.price = this.amount;
                this.id = index;
                this.comboId = this.combo.id;

                index++;
            });

            return response;
        }
    });
})(qcssapp);