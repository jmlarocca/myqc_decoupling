(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.RewardsProgram - Contains a collection of user loyalty programs*/
    qcssapp.Collections.RewardsProgram = Backbone.Collection.extend({
        model: qcssapp.Models.RewardsProgram, //base model for this collection
        parse: function(response) {
            $.each(response, function() {
                this.id = this.loyaltyProgram.id;
                this.name = this.loyaltyProgram.name;
                this.newPoints = this.points;
                this.points += this.originalPoints;

                if ( this.loyaltyProgram.description != null && this.loyaltyProgram.description.length > 0 ) {
                    this.description = this.loyaltyProgram.description;
                }

                delete this.loyaltyProgram;
                delete this.loyaltyRewardTransLineId;
                delete this.refNumber;
                delete this.originalPoints;
            });
            return response;
        },

        initialize: function() {
            this.on('change:expanded', this.collapseOthers);
        },

        collapseOthers: function(program) {
            if ( !program.get('expanded') ) {
                return;
            }

            var programsToCollapse = this.filter(function(thisProgram) { return ( thisProgram.get('expanded') && program.get('id') != thisProgram.get('id')) });

            if (programsToCollapse.length == 0) {
                return;
            }

            _.each(programsToCollapse, function(thisProgram) { thisProgram.collapse(); });
        }
    });
})(qcssapp);