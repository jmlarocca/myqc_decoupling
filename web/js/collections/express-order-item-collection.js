(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.ExpressOrderItemCollection - Contains a collection of express order item models for reorder */
    qcssapp.Collections.ExpressOrderItemCollection = Backbone.Collection.extend({
        model: qcssapp.Models.ExpressOrderItem //base model for this collection
    });
})(qcssapp);