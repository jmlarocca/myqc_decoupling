/**
 * Created by mmhayes on 9/21/2020.
 */
;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Disclaimers - Contains a collection of donations
     - Loads data from REST API using GET
     */
    qcssapp.Collections.Disclaimers = Backbone.Collection.extend({
        model: qcssapp.Models.Disclaimer, //base model for this collection
        parse: function(response) {
            $.each(response, function() {
                var parsedText = this.text.replace(/\\n|\\r/g, "<br/>");
                this.text = parsedText;
            });

            return response;
        }
    });
})(qcssapp);