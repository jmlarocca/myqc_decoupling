(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.ExpressOrderCollection - Contains a collection of express order models for reorder */
    qcssapp.Collections.ExpressOrderCollection = Backbone.Collection.extend({
        model: qcssapp.Models.ExpressOrder //base model for this collection
    });
})(qcssapp);