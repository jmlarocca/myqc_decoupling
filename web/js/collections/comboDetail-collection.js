(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.ComboDetail - Contains a collection of combo details
     - Loads data from REST API using GET
     */
    qcssapp.Collections.ComboDetail = Backbone.Collection.extend({
        model: qcssapp.Models.ComboDetail //base model for this collection
    });
})(qcssapp);