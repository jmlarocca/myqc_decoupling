;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.AccountPersonCollection - Contains a collection of account person options
     - Loads data from REST API using GET
     */
    qcssapp.Collections.AccountPersonCollection = Backbone.Collection.extend({
        model: qcssapp.Models.AccountPerson //base model for this collection
    });
})(qcssapp);