;(function (qcssapp, $) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Purchases - Contains a collection of Purchases
        - Loads data from REST API using GET
    */
    qcssapp.Collections.Purchases = Backbone.Collection.extend({
            model: qcssapp.Models.Transaction //base model for this collection
    });
})(qcssapp, jQuery);