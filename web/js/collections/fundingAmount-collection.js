(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.FundingAmount - Contains a collection of funding amounts
     - Loads data from REST API using GET
     */
    qcssapp.Collections.FundingAmount = Backbone.Collection.extend({
        model: qcssapp.Models.FundingAmount //base model for this collection
    });
})(qcssapp);