;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Cart - Contains a collection of cart products
     - Loads data from REST API using GET
     */
    qcssapp.Collections.Cart = Backbone.Collection.extend({
        model: qcssapp.Models.CartLine //base model for this collection
    });
})(qcssapp);