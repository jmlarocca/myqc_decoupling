;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Donations - Contains a collection of donations
     - Loads data from REST API using GET
     */
    qcssapp.Collections.Donations = Backbone.Collection.extend({
        model: qcssapp.Models.Donation, //base model for this collection
        parse: function(response) {
            $.each(response, function() {
                this.name = qcssapp.Functions.htmlDecode(this.name);
                this.description = qcssapp.Functions.htmlDecode(this.description);
            });
            return response;
        }
    });
})(qcssapp);