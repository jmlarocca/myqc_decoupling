;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Terms - Contains a collection of term models
     - Loads data from REST API using GET
     */
    qcssapp.Collections.Terms = Backbone.Collection.extend({
        model: qcssapp.Models.Term //base model for this collection
    });
})(qcssapp);