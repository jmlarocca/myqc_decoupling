;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.spendingProfile - Contains a collection of spending profile options
     - Loads data from REST API using GET
     */
    qcssapp.Collections.spendingProfile = Backbone.Collection.extend({
        model: qcssapp.Models.spendingProfile //base model for this collection
    });
})(qcssapp);