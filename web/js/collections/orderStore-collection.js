;(function (qcssapp, $) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.OrderStoreCollection - Contains a collection of Stores for ordering
     - Loads data from REST API using GET
     */
    qcssapp.Collections.OrderStoreCollection = Backbone.Collection.extend({
        model: qcssapp.Models.OrderStore, //base model for this collection
        url: function() {
            return qcssapp.Location + '/api/ordering/stores';
        },
        parse: function(response) {
            $.each(response, function() {
                this.header1 = qcssapp.Functions.htmlDecode(this.header1);
                this.header2 = qcssapp.Functions.htmlDecode(this.header2);
                this.header3 = qcssapp.Functions.htmlDecode(this.header3);
                this.header4 = qcssapp.Functions.htmlDecode(this.header4);
                this.header5 = qcssapp.Functions.htmlDecode(this.header5);
                this.pickUpOrderingOpenTime = qcssapp.Functions.htmlDecode(this.pickUpOrderingOpenTime);
                this.pickUpOrderingCloseTime = qcssapp.Functions.htmlDecode(this.pickUpOrderingCloseTime);
                this.pickupCloseTime = qcssapp.Functions.htmlDecode(this.pickupCloseTime);
                this.deliveryOrderingOpenTime = qcssapp.Functions.htmlDecode(this.deliveryOrderingOpenTime);
                this.deliveryOrderingCloseTime = qcssapp.Functions.htmlDecode(this.deliveryOrderingCloseTime);
                this.deliveryCloseTime = qcssapp.Functions.htmlDecode(this.deliveryCloseTime);
                this.currentStoreTime = qcssapp.Functions.htmlDecode(this.currentStoreTime);
            });

            return response;
        }
    });
})(qcssapp, jQuery);