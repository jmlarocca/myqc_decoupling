;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.AccountGroup - Contains a collection of account group options
     - Loads data from REST API using GET
     */
    qcssapp.Collections.AccountGroup = Backbone.Collection.extend({
        model: qcssapp.Models.spendingProfile //base model for this collection
    });
})(qcssapp);