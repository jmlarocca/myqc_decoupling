;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.PurchaseRestrictionCollection - Contains a collection of purchase restrictions
     - Loads data from REST API using GET
     */
    qcssapp.Collections.PurchaseRestrictionCollection = Backbone.Collection.extend({
        model: qcssapp.Models.Product
    });
})(qcssapp);

