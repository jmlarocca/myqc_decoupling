;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Modifiers - Contains a collection of modifiers */

    qcssapp.Collections.Modifiers = Backbone.Collection.extend({
        model: qcssapp.Models.Modifier, //base model for this collection
        parse: function(mod) {
            var index = 0;
            $.each(mod, function() {

                this.index = index;

                index++;
            });

            return mod;
        }
    });
})(qcssapp);