(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Balance - Contains a collection of balance groups
     - Loads data from REST API using GET
     */
    qcssapp.Collections.DailyValueCollection = Backbone.Collection.extend({
        model: qcssapp.Models.DailyValue //base model for this collection
    });
})(qcssapp);