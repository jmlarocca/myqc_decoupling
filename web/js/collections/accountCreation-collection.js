;(function (qcssapp) {
    'use strict'; //activates strict mode for this scope

    /* qcssapp.Collections.Cart - Contains a collection of cart products
     - Loads data from REST API using GET
     */
    qcssapp.Collections.AccountGroups = Backbone.Collection.extend({
        model: qcssapp.Models.AccountGroup,
        parse: function(response) {

            $.each(response, function() {
                if (this.hasOwnProperty('ACCOUNTGROUPID')) { //if there is an ACCOUNTGROUPID, replace the name with the lowercase key name
                    this.accountGroupID = this.ACCOUNTGROUPID;
                    delete this.ACCOUNTGROUPID;
                }
                if (this.hasOwnProperty('ACCOUNTGROUPNAME')) { //if there is an ACCOUNTGROUPNAME, replace the name with the lowercase key name
                    this.accountGroupName = this.ACCOUNTGROUPNAME;
                    delete this.ACCOUNTGROUPNAME;
                }
                if (this.hasOwnProperty('ACCOUNTTYPEID')) { //if there is an ACCOUNTTYPEID, replace the name with the lowercase key name
                    this.accountTypeID = this.ACCOUNTTYPEID;
                    delete this.ACCOUNTTYPEID;
                }
                if (this.hasOwnProperty('ACCOUNTTYPENAME')) { //if there is an ACCOUNTTYPENAME, replace the name with the lowercase key name
                    this.accountTypeName = this.ACCOUNTTYPENAME;
                    delete this.ACCOUNTTYPENAME;
                }
                if (this.hasOwnProperty('AVAILABLEFORACCTCREATION')) { //if there is an AVAILABLEFORACCTCREATION, replace the name with the lowercase key name
                    this.availableForAcctCreation = this.AVAILABLEFORACCTCREATION;
                    delete this.AVAILABLEFORACCTCREATION;
                }
            });
            return response;
        }
    });
})(qcssapp);

//{"accountGroups": [{
//    id :0,
//    accountGroupName :"Prepaid",
//    accountGroupID :1,
//    accountTypeID:2,
//    accountTypeName:"PrepaidType",
//    accountGroupActive:true
//    spendingProfiles : [{
//        spendingProfileID: 1,
//        spendingProfileName:"Default"
//    },
//        {
//            spendingProfileID: 2,
//            spendingProfileName:"Cashless"
//        }
//    ]
//},
//    {
//        id:1,
//        accountGroupName :"Payroll Deduct",
//        accountGroupID :2,
//        accountTypeID:5,
//        accountTypeName:"PayrollType",
//        spendingProfiles : [{
//            spendingProfileID: 4,
//            spendingProfileName:"Cash Emps"
//        },
//            {
//                spendingProfileID: 3,
//                spendingProfileName:"cafeteria"
//            }
//        ]}
//],
//    autoGenerateLength:16
//};