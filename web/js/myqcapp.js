var qcssapp = qcssapp || {};
var FirebasePlugin;
var firebase;

//Initializes the entire application, starting with the login view
function onAppReady(qcssapp) {
    'use strict';

    qcssapp.onPhoneApp = (typeof window.cordova !== "undefined");
    qcssapp.onMobileBrowser = mobileAndTabletCheck();

    FirebasePlugin = window.FirebasePlugin;
    if (FirebasePlugin) {
        // On a mobile device, allocate Firebase token
        FirebasePlugin.getToken();
    } else if (!qcssapp.onPhoneApp && firebase) {
        // In the browser, initialize Firebase web platform
        var firebaseConfig = {
            apiKey: "AIzaSyAjmqs8igvxcLfiuVV_8rlFa1Ep9ulZNio",
            authDomain: "my-quickcharge.firebaseapp.com",
            projectId: "my-quickcharge",
            storageBucket: "my-quickcharge.appspot.com",
            messagingSenderId: "912778523119",
            appId: "1:912778523119:web:f6be235975bb557585c61a",
            measurementId: "G-6JJRHZZKQ4"
        };
        firebase.initializeApp(firebaseConfig);
        firebase.analytics();
    }

    //show status bar
    if ( qcssapp.onPhoneApp ) {   // Cordova API detected
        if ( typeof StatusBar != "undefined" ) {
            // TODO: CHANGE THIS ONCE WE ARE READY TO TRY STATUS BAR AGAIN
            StatusBar.overlaysWebView(false);
            StatusBar.hide();
        }

        //instantiate FastClick js to remove mobile browser tap to click delay
        FastClick.attach(document.body);

        qcssapp.Functions.adjustToScreenSize();
    }

    if (qcssapp.DebugMode) {
        console.log("Initializing application");
        console.log(qcssapp);
    }

    if ( qcssapp.ssoLogin ) {
        return;
    }

    new qcssapp.Views.LoginView();
}

document.addEventListener("app.Ready", function() {
    onAppReady(qcssapp);
}) ;

document.addEventListener("resume", qcssapp.Functions.onResume, false);

window.mobileAndTabletCheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

//handles when the app is opened via url schema
window.handleOpenURL = function(url) {
    var ssoValidation = RegExp('[?&]ssoValidation=([^&]*)').exec(url);
    var resumeApp = qcssapp.Functions.getParameterByName('resumeApp', url);
    var transactionID = qcssapp.Functions.getParameterByName('transid', url);
    var status = qcssapp.Functions.getParameterByName('status', url);
    if ( status && status.indexOf('#') != -1 ) {
        status = status.split('#')[0];
    }

    if ( !ssoValidation && !resumeApp) {
        return;
    }

    if ( ssoValidation ) {
        var instanceCodeCollection = new qcssapp.Collections.instanceCodes();
        instanceCodeCollection.fetch();
        var ssoCode = instanceCodeCollection.get("SSO");
        var location = ssoCode.get('LOCATION');

        if ( ssoCode && location && location.length ) {
            qcssapp.ssoLogin = true;
        }

        var ssoSuccessParameters = {
            ssoValidation: ssoValidation[1].toString(),
            location: location
        };

        var ssoSuccessFunction = function(ssoSuccessParameters) {
            qcssapp.Functions.resetOnLogout(true);
            qcssapp.Functions.handleSSOLogin(ssoSuccessParameters.ssoValidation, ssoSuccessParameters.location);
        };

        qcssapp.Functions.checkLoginSettings(location+'/api/auth/login/settings', ssoSuccessParameters, ssoSuccessFunction);
    } else {
        qcssapp.preventStandardStartup = true;

        qcssapp.Functions.resumeSession();

        qcssapp.Functions.resumeFunding(status, transactionID);
    }
};
