;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ModifierMenuView - View for displaying modifier menus
     */
    qcssapp.Views.ModifierMenuView = Backbone.View.extend({
        internalName: "Modifier Menu View",

        el: '.modifier-menu', //references existing HTML element for order review - can be referenced as this.$el throughout the view

        // Cache the template function for a single item.
        template: _.template( $('#modifier-menu-template').html() ),

        events: {
            'click .modifier-menu_header-container': 'expandMenu',
            'click .modifier_details': 'clickModifier'
        },

        initialize: function() {

            this.max = this.model.get('max');
            this.min = this.model.get('min');
            this.radioMenu = !!(this.min == 1 && this.max == 1);
            this.predefined = typeof this.model.predefined !== 'undefined' ? this.model.predefined : false;
            this.requiredMenu = !(this.min == null || this.min == 0);

            this.selectedModifiers = new qcssapp.Collections.Modifiers();
            this.selectedModifiers.reset();

            return this;
        },

        //events: //events not needed for this view
        render: function() {

            this.setElement(this.template(this.model.toJSON()));

            this.$modifierList = this.$el.find('.modifier-menu_list');
            this.$menuDescription = this.$el.find('.modifier-menu_description');
            this.$selectedModText = this.$el.find('.modifier-menu_selected-modifiers');
            this.$headerContainer = this.$el.find('.modifier-menu_header-container');
            this.$menuRequirement = this.$el.find('.modifier-menu_requirement');

            this.setMenuModifiers();
            this.updateSelectedModifierText();
            this.checkMenuRequirements();
            this.openMenu();

            this.listenTo(qcssapp.selectedModCollection, 'change:prepOption', this.updateSelectedModifierText);

            return this;
        },

        setMenuModifiers: function() {
            this.model.get('modifiers').each(function( mod ) {
                this.renderModView( mod );
            }, this);

        },

        renderModView: function( mod ) {
            var modifierView = new qcssapp.Views.ModifierMenuLineView({
                model:mod
            });

            this.$modifierList.append(modifierView.render().el);

            var modifierPredefined = typeof mod.predefined !== 'undefined' ? mod.predefined : false;

            if(this.predefined) {
                if( modifierPredefined && !this.radioMenu ) {
                    this.addCheckboxModifier(modifierView.$el, mod);
                } else if( modifierPredefined ) {
                    this.addRadioModifier(modifierView.$el, mod);
                }
            } else {
                if( mod.get('defaultModifier') && !this.radioMenu ) {
                    this.addCheckboxModifier(modifierView.$el, mod);
                } else if( mod.get('defaultModifier') ) {
                    this.addRadioModifier(modifierView.$el, mod);
                }
            }

            if(this.radioMenu) {
                this.$modifierList.find('.modifier_select').addClass('radio-mod');
            }
        },

        expandMenu: function() {
            if( !this.$el.hasClass('expand-menu') ) {
                this.openMenu();
                return;
            }

            var headerTop = this.$headerContainer.position().top;
            var $productPage =  $('.product-detail_container');
            var pageScrollTop =  $productPage.scrollTop();

            if(pageScrollTop > headerTop)  {
                $productPage.animate({scrollTop: headerTop});
            }

            this.closeMenu()
        },

        closeMenu: function() {
            this.$el.removeClass('expand-menu');
            this.$modifierList.slideUp(300);
            this.$menuDescription.slideUp(300);

            if(this.$selectedModText.text() != '') {
                this.$selectedModText.slideDown(300);
            }
            setTimeout(function() {
                qcssapp.Functions.checkScrollIndicator($('#product-detail-page').find('.scrollElement'))
            }, 300);

        },

        openMenu: function() {
            this.$el.addClass('expand-menu');
            this.$modifierList.slideDown(300);
            this.$menuDescription.slideDown(300);
            this.$selectedModText.slideUp(300);
            var _this = this;
            setTimeout(function(_this) {
                qcssapp.Functions.checkScrollIndicator(_this.$el.closest('.scrollElement'))
            }, 300, _this);

        },

        clickModifier: function(e) {
            var $selectedMod = $(e.currentTarget).parent();
            var modIndex = Number($selectedMod.attr('data-index'));
            var modModel = this.model.get('modifiers').models[modIndex];

            // removing modifier
            if(!this.radioMenu && $selectedMod.hasClass('selected-modifier')) {
                this.removeModifier($selectedMod, modModel);

                //Defect 3832 / 4117: Notify user when attempting to add more modifiers than allowed
                //Do not use this check for a radio menu or with Null / 0 Max Mods
                //use (Length + 1) since we haven't added the modifier yet, and we're trying to do so
            } else if (!this.radioMenu && this.max > 0 && this.selectedModifiers.length + 1 > this.max) {
                $.alert("You can only make "+this.max+" selection(s).");

            // adding modifier - validate modifier count against max modifiers allowed before adding
            } else if (!this.radioMenu && this.validateModifiers()) {
                this.addCheckboxModifier($selectedMod, modModel);

            } else if (!this.radioMenu && this.min == 0 && this.max == 1) {
                this.removeAndAddCheckboxModifier($selectedMod, modModel);

            } else if (this.radioMenu) {
                this.removeAndAddRadioModifier($selectedMod, modModel);
            }
        },

        validateModifiers: function() {
            return !(this.max != null && this.max > 0 && this.selectedModifiers.length == this.max);
        },

        removeAndAddRadioModifier: function($mod, modModel) {
            if($mod.hasClass('selected-modifier')) {
                return;
            }

            var $previousSelectedMod = this.$el.find('.selected-modifier');
            var previousSelectedModIndex = $previousSelectedMod.attr('data-index');
            var previousSelectedModModel = this.model.get('modifiers').models[previousSelectedModIndex];

            if(typeof previousSelectedModModel !== 'undefined') {
                this.removeModifier($previousSelectedMod, previousSelectedModModel);
            }

            this.addRadioModifier($mod, modModel);
            this.closeMenu();
        },

        removeAndAddCheckboxModifier: function($mod, modModel) {
            if($mod.hasClass('selected-modifier')) {
                return;
            }

            var $previousSelectedMod = this.$el.find('.selected-modifier');
            var previousSelectedModIndex = $previousSelectedMod.attr('data-index');
            var previousSelectedModModel = this.model.get('modifiers').models[previousSelectedModIndex];

            this.removeModifier($previousSelectedMod, previousSelectedModModel);
            this.addCheckboxModifier($mod, modModel);
        },

        addRadioModifier: function($mod, modModel) {
            $mod.addClass('selected-modifier');

            if(qcssapp.usingBranding) {
                $mod.find('.modifier_select').addClass('accent-color-one-border');
                $mod.find('.modifier_select-circle').addClass('accent-color-one-background');
                $mod.find('.modifier_name-container').addClass('accent-color-one');
            }

            this.addModifier($mod, modModel);
        },

        addCheckboxModifier: function($mod, modModel) {
            $mod.addClass('selected-modifier');

            if(qcssapp.usingBranding) {
                $mod.find('.modifier_select').addClass('accent-color-one-background accent-color-one-border');
                $mod.find('.modifier_name-container').addClass('accent-color-one');
            }

            this.addModifier($mod, modModel);
        },

        addModifier: function($mod, modModel) {
            this.showPrepOptionPreview($mod);

            // D-4404: remove invalid property to avoid a 400 error if the server is not up to spec
            if(!qcssapp.Functions.checkServerVersion(6,0,7)) {
                delete modModel.attributes.taxIDs;
            }

            this.addModifierPrepOption($mod, modModel);

            this.selectedModifiers.add(modModel);

            this.updateSelectedModifierText();
            this.checkMenuRequirements();

            qcssapp.selectedModCollection.add(modModel);
        },

        removeModifier: function($mod, modModel) {
            $mod.removeClass('selected-modifier');

            if(qcssapp.usingBranding) {
                $mod.find('.modifier_select').removeClass('accent-color-one-background accent-color-one-border');
                $mod.find('.modifier_select-circle').removeClass('accent-color-one-background');
                $mod.find('.modifier_name-container').removeClass('accent-color-one');
            }

            modModel.set('prepOption', null);

            this.hidePrepOptionPreview($mod);

            this.selectedModifiers.remove(modModel);

            this.updateSelectedModifierText();
            this.checkMenuRequirements();

            qcssapp.selectedModCollection.remove(modModel);
        },

        updateSelectedModifierText: function() {
            var selectedModifierText = '';

            this.selectedModifiers.each(function( mod ) {
                var modName = mod.get('name');
                var modPrice = mod.get('price');

                var modText = modName;

                if(Number(modPrice) > 0) {
                    modText += ' (' + qcssapp.Functions.formatPrice(modPrice, '+') + ')';
                }

                if(mod.get('prepOption') != null) {
                    var prepName = mod.get('prepOption').get('name');
                    var prepPrice = mod.get('prepOption').get('price');

                    if( !(mod.get('prepOption').get('defaultOption') && !mod.get('prepOption').get('displayDefault') && Number(prepPrice) <= 0) ) {
                        modText += ' - ' + prepName;

                        if(Number(prepPrice) > 0) {
                            modText += ' (' + qcssapp.Functions.formatPrice(prepPrice, '+') + ')';
                        }
                    }
                }

                selectedModifierText += modText + ', ';

            }, this);

            if(selectedModifierText.length > 0) {
                selectedModifierText = selectedModifierText.substring(0, selectedModifierText.length - 2);
            }

            this.$selectedModText.html(selectedModifierText);
        },

        checkMenuRequirements: function() {
            if(!this.requiredMenu) {
                this.$el.addClass('menu-valid');

            } else if(this.selectedModifiers.length >= this.min) {
                this.$el.addClass('menu-valid');

            } else {
                this.$el.removeClass('menu-valid');
            }

            if(this.$el.hasClass('menu-valid') && this.requiredMenu) {
                this.$menuRequirement.addClass('required accent-color-one').text('✓ Required')
            } else if (this.requiredMenu){
                this.$menuRequirement.addClass('required accent-color-one').text('Required')
            }
        },

        showPrepOptionPreview: function($mod) {
            if($mod.find('.modifier_prep-option-list').children().length > 0) {
                var prepOptionContainer = $mod.find('.modifier_prep-option-container');
                if (document.contains(prepOptionContainer[0])) {
                    prepOptionContainer.slideDown();
                } else {
                    prepOptionContainer.css('display', 'block');
                }
            }
        },

        hidePrepOptionPreview: function($mod) {
            $mod.find('.modifier_prep-option-container').slideUp();
        },

        addModifierPrepOption: function($mod, modifierModel) {
            if(!modifierModel.get('prepOptions').length) {
                return;
            }

            if(modifierModel.get('prepOption') == null) {
                var $selectedPrep = $mod.find('.selected-prep');
                if($selectedPrep){
                    var prepID = $selectedPrep.attr('data-id');

                    $.each(modifierModel.get('prepOptions'), function(index, prep) {
                        if(Number(prep.id) == Number(prepID)) {
                            modifierModel.set('prepOption', new qcssapp.Models.PrepOption(prep));
                            return false;
                        }
                    });
                }
            }
        }
    });
})(qcssapp, jQuery);
