;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.BarcodeScanHelpView - View for modal to display barcode scanning explanation
     */
    qcssapp.Views.BarcodeScanHelpView = Backbone.View.extend({
        internalName: "Barcode Scan Help View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'barcode-scan-help-view hidden',

        template: _.template( $('#barcode-scan-help-template').html() ),

        //handle "enter" events on form fields
        events: {
            "click .barcode-scan-help_cancel-button": "handleCancel",
            "click .barcode-scan-help_continue-button": "handleContinue"
        },

        initialize: function (options) {
            this.options = options || {};

            this.hidden = this.options.hidden;

            this.callback = this.options.callback;
            this.callbackParams = this.options.callbackParams;

            _.bindAll( this, 'handleContinue', 'handleCancel');

            this.render();
        },

        render: function() {
            this.$el.html( this.template( {} ) );

            $('body').append( this.$el );

            this.fadeIn();

            return this;
        },

        handleCancel: function() {
            this.hide();
            this.$el.remove();
        },

        handleContinue: function() {
            this.hide();
            this.$el.remove();

            qcssapp.Functions.showScanPlugin();
        },

        fadeIn: function(dur) {
            dur = !dur ? 150 : dur;

            this.hidden = false;
            //this.$el.fadeIn(dur, function() {
                this.$el.removeClass('hidden');
           // }.bind(this));
        },

        hide: function() {
            this.hidden = true;
            this.$el.fadeOut(150, function() {
                this.$el.hide();
                this.$el.remove();
            }.bind(this));
        },

        displayError: function( errorMsg ) {
            this.$errorMsg.html( errorMsg );
            this.$errorMsg.slideDown();
        }
    });
})(qcssapp, jQuery);
