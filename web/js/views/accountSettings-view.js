;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.spendingProfileView - View for displaying spending profile Collections
     */
    qcssapp.Views.AccountSettingsView = Backbone.View.extend({
        internalName: "Account Settings View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#accountSettings', //references existing HTML element for Transaction Collections - can be referenced as this.$el throughout the view

        events: {
            'change #accountGroupSetting': 'reloadSelectors',  //rename this function
            'change #low-balance-check, #emp-low-balance-check': 'lowBalanceChange',
            'change #enable-fingerprint': 'fingerprintChange',
            'change #accountEmailSetting': 'emailInputChange',
            'change #lowBalanceSetting, #empLowBalanceSetting': 'lowBalanceInputChange',
            "keyup #accountEmailSetting": "enterEventHandler"
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in account settings view");
            }

            if(qcssapp.personModel) {
                qcssapp.$activePanel.parent().siblings('header').find('h1').text('Settings');
            }

            this.forceChange = (typeof options !== 'undefined');
            this.accountGroupForced = "";

            //selectors
            this.$accountGroupSelect = $('#accountGroupSetting');
            this.$spendingProfileSelect = $('#spendingProfileSetting');
            this.$accountTypeSelect = $('#accountTypeSetting');
            this.$accountEmailInput = $('#accountEmailSetting');
            this.$fingerprintCheckbox = $('#enable-fingerprint');

            this.$userSettingsContainer = $('#userSettingsContainer');
            this.$userLowBalanceCheckbox = $('#low-balance-check');
            this.$userLowBalanceInput = $('#lowBalanceSetting');

            this.$employeeLowBalanceCheckbox = $('#emp-low-balance-check');
            this.$employeeLowBalanceInput = $('#empLowBalanceSetting');


            this.$userLowBalanceInput.prop('disabled', true);

            this.hasAccountGroupChanged = false;
            this.hasSpendingProfileChanged = false;

            this.originalEmail = "";
            this.originalLowBalance = "";
            this.originalEmpLowBalance = "";
            this.hasEmailChanged = false;
            this.hasLowBalanceChanged = false;
            this.hasEmpLowBalanceChanged = false;
            this.needsInviteSent = false;
            this.employeeID = "";

            this.onCharge = false;
            this.chargeAmount = null;
            this.fundingFee = null;

            this.showSpendingProfileDetails = false;

            this.tos = [];

            this.loadCollection(true, false);
        },

        //populates the account group, spending profile and account type selectors. Sets the limits
        loadCollection: function(isInitial, afterSave) {
            var parameters = {
                accountSettingsView:this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/account/settings', 'GET', '', parameters,
                function(response) {
                    parameters.accountSettingsView.fetchSuccess(response);

                    if(isInitial) {
                        parameters.accountSettingsView.displayButtons();
                    }

                    if(afterSave) {
                        if(qcssapp.personModel) {

                            //if an email was added and an invite was sent, show that in the success message
                            if(parameters.accountSettingsView.needsInviteSent) {
                                $('#accountSettingsMsgContainer').show().text('Settings successfully updated! An invite has been sent to the email.');
                                parameters.accountSettingsView.needsInviteSent = false;
                                qcssapp.accountModel.set('email', parameters.accountSettingsView.$accountEmailInput.val());

                            } else {
                                $('#accountSettingsMsgContainer').show().text('Settings successfully updated!');
                            }
                        } else {
                            $('#accountSettingsMsgContainer').show().text('Account Settings successfully updated!');
                        }
                    }
                },
                function(errorParameters) {
                    errorParameters.accountSettingsView.fetchError();
                },
                '', '', '', false, true
            );
        },

        //handles error for fetching of the collections
        fetchError: function() {
            if (qcssapp.DebugMode) {
                console.log("Error loading data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            //display error message to user
            qcssapp.Functions.displayError('There was an error loading the account settings details, please try again later.');
            qcssapp.Functions.hideMask();
        },

        //handles successful fetching of the collections
        fetchSuccess: function(data) {
            if (qcssapp.DebugMode) {
                console.log("Successfully loaded data for "+this.internalName);
                console.log(this);
                if(this.collection){
                    console.log(this.collection.toJSON());
                }
            }

            this.resetPage();

            if (qcssapp.currency.showCurrency && qcssapp.currency.currencyType.trim()) {
                var lowBalanceLabel = "Low Balance Threshold (" + qcssapp.currency.currencyType.trim() + ") :";
                this.$employeeLowBalanceInput.parent().find("label").text(lowBalanceLabel);
                this.$userLowBalanceInput.parent().find("label").text(lowBalanceLabel);
            }

            //render the selectors
            this.render(data);

            //get limits
            if(this.showSpendingProfileDetails) {
                this.displayGlobalLimit();
                this.displayLimits();
            } else {
                $('#accountSettingSubtitle, .globalSpendingLimitContainer, #spendingLimitsSettingContainer').hide();
                qcssapp.Functions.hideMask();
            }

            //set the account's settings to their corresponding global variable
            qcssapp.AccountSettings.accountGroup = this.$accountGroupSelect.val();
            qcssapp.AccountSettings.spendingProfile = this.$spendingProfileSelect.val();
            qcssapp.AccountSettings.accountType = this.$accountTypeSelect.val();
            qcssapp.AccountSettings.employeeLowBalanceThreshold = this.$employeeLowBalanceInput.val();
        },

        //calls renderLine on each item in the spending profile collection
        render: function(data) {
            this.$accountGroupSelect.prop('disabled', false).addClass('selector-down-arrow');

            //check for the account group information
            if ( data.accountGroupOptions != null && data.accountGroupOptions.length > 0 ) {
                this.accountGroupCollection.set(data.accountGroupOptions);
                this.renderSelector(this.accountGroupCollection, this.$accountGroupSelect);

                //if force change is on and the account group is changed then remove that account group from the list
                if(this.forceChange && this.hasAccountGroupChanged && this.accountGroupForced != "") {
                    this.$accountGroupSelect.find('option[value='+this.accountGroupForced+']').remove();
                } else if (this.forceChange) {
                    this.accountGroupForced = this.$accountGroupSelect.val();
                }
            } else {
                qcssapp.Functions.displayError('Error loading Account Group details.');
            }

            //check for the spending profile information
            if ( data.spendingProfileOptions != null ) {
                this.spendingProfileCollection.set(data.spendingProfileOptions);
            }

            //create special spending profile selector-modal
            this.createSpendingProfileSelectModal();

            //if there are no spending profile options, disable selector and show error
            if ( data.spendingProfileOptions == null || data.spendingProfileOptions.length == 0) {
                this.$spendingProfileSelect.prop('disabled', true);
                this.$spendingProfileSelect.val(" ").text('').removeClass('selector-down-arrow');
                qcssapp.Functions.displayError('Error loading Spending Profile details.');
            }

            //check for the account type information
            if ( data.accountTypeOptions != null && data.accountTypeOptions.length > 0 ) {
                this.accountTypeCollection.set(data.accountTypeOptions);
                this.renderSelector(this.accountTypeCollection, this.$accountTypeSelect);
                this.$accountTypeSelect.prop('disabled', true);
            } else {
                qcssapp.Functions.displayError('Error loading Account Type details.');
            }

            //if there is only one option in the selector, disable it since it can't be changed to anything
            if(this.$accountGroupSelect.find('option').length == 1) {
                this.$accountGroupSelect.prop('disabled', true).removeClass('selector-down-arrow');

                //if force change is on throw error because the account group must be changed but there are no options to change to
                if(this.forceChange && this.$accountGroupSelect.val() == this.accountGroupForced) {
                    $('#changeAccountGroupMsg').remove();
                    qcssapp.Functions.displayError('This Account Group must be changed but no other Account Group options were found.  Please contact your IT department or manager');
                }
            }

            //if there is only one option in the selector, disable it since it can't be changed to anything
            if( (this.spendingProfileCollection.length == 1 && this.spendingProfileCollection.models[0].get("price") == "") || (this.forceChange && !this.hasAccountGroupChanged)) {
                this.$spendingProfileSelect.prop('disabled', true).trigger('click');
            } else if ((this.spendingProfileCollection.length == 1 && this.spendingProfileCollection.models[0].get("price") != "")) {
                this.$spendingProfileSelect.addClass('selector-down-arrow');
            }

            if(data.hasOwnProperty('accountModel')) {
                this.loadAccountSettings(data);
            }

            //if the user is a prepaid account and 'Show Spending Profile Details for Declining Balances' is OFF then hide spending profile limits
            this.showSpendingProfileDetails = data.showSpendingProfileDetails != null ? data.showSpendingProfileDetails : false;
        },

        //-------------- CREATE PAGE ELEMENT FUNCTIONS ---------------//

        //creates Save and Cancel buttons
        displayButtons: function() {
            var that = this;
            var buttonText = 'Save';
            var $changeAccountGroupMsg = $('#changeAccountGroupMsg');

            //if force change is on disable the spending profile selector until it is changed, hide home-link button, show back button and display message at top
            if(this.forceChange) {
                this.$spendingProfileSelect.prop('disabled', true).removeClass('selector-down-arrow');
                $changeAccountGroupMsg.show();
                if(qcssapp.Functions.checkServerVersion(2,1) && this.$accountTypeSelect.val() == "4") {
                    $changeAccountGroupMsg.text("Cannot have a Gift Card account type. Please change your Account Group in order to continue");
                }
                $('#page-header').append('<a class="general-back-icon"><img class="order-back" src="images/icon-back.svg"><img hidden="" class="svg order-back-svg font-color-primary" src="images/icon-back.svg"></a>');
                $('.home-link').hide();
                buttonText = 'Continue';
            } else {
                $changeAccountGroupMsg.hide();
                $('#page-header').find('.general-back-icon').remove();
                $('.home-link').show();
            }

            new qcssapp.Views.ButtonView({
                text: buttonText,
                buttonType: "customCB",
                appendToID: "#accountSettingButtonContainer",
                id: "saveAccountSettingsBtn",
                parentView:this,
                class: "template-gen prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                callback: function() {
                    if(that.$accountGroupSelect.val() == null || that.$accountGroupSelect.val() == " ") {
                        qcssapp.Functions.displayError('Please select an Account Group');
                        return;
                    } else if (that.$spendingProfileSelect.val() == null || that.$spendingProfileSelect.val() == " ") {
                        qcssapp.Functions.displayError('Please select a Spending Profile');
                        return;
                    }

                    if(that.tos.length > 0) {
                        that.confirmAccountDetailsChangeForTOS();
                    } else {
                        that.saveAccountSettings();
                    }
                }
            });

            new qcssapp.Views.ButtonView({
                text: "Cancel",
                buttonType: "customCB",
                appendToID: "#accountSettingButtonContainer",
                id: "cancelAccountSettingsBtn",
                parentView:this,
                class: "template-gen prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                callback: function() {
                    var successParameters = {accountSettingsView: that};

                    var successFunction = function(successParameters) {
                        successParameters.accountSettingsView.loadCollection(false);
                        qcssapp.AccountSettings.needsSave = false;
                        $('#accountSettingButtonContainer, #userSettingButtonContainer, #accountSettingsMsgContainer').hide();
                    };

                    qcssapp.Functions.cancelChanges(successFunction, successParameters);
                }
            });

            $('#accountSettingButtonContainer').hide();

        },

        //creates spending profile selector-modal
        createSpendingProfileSelectModal: function() {
            $('.qcss-select').remove(); //remove previous select selects

            //get the user's currently selected spending profile to set as the initial value
            var spendingProfileValue = '';
            this.spendingProfileCollection.each(function( mod ) {
                if(mod.get('active')) {
                    spendingProfileValue = mod.get('id');
                }
                mod.set('value', qcssapp.Functions.htmlDecode(mod.get('value')));
            }, this );

            var onOpen = function() {
                //if one of the spending profile lines doesn't have a price then set the price flex to 0 so that name fills the whole line
                $.each($('.spending-profile-line'), function() {
                    var price = $(this).find('.spending-profile-price').text();
                    if(price == "") {
                        $(this).find('.spending-profile-price').css('flex', '0');
                    }
                });
            };

            //create spending profile select view and modal view
            new qcssapp.Views.SelectView({
                buttonType: "customCB",
                appendToID: "#spendingProfileSettingContainer",
                id: "spendingProfileSetting",  //selector id
                data: this.spendingProfileCollection, //pass in the data that will populate the modal-selector
                textProperty: 'value',  //the model property that will be the text of the selector
                valueProperty:'id', //the model property that will be the value of the selector
                initialValue: spendingProfileValue,  //set the selector's initial value if it has one
                onOpen: onOpen,  //optional function called when modal is open
                class: "", //any additional classes besides qcss-select
                modal: {
                    title: 'Select a new Spending Profile',  //title displayed in the modal
                    id:'spendingProfileModal', //modal id
                    columnHeaderTemplateID: 'spending-profile-header-template', //the template for the column headers if there is one
                    columnHeaderDependentField: 'price', //don't show the column header if there are no values for this field in any model
                    parentView:this,  //the account settings view
                    templateID: 'spending-profile-modal-template', //the template used for each modal-line
                    callback: function(modalView) { //optional callback function, otherwise modal closes and populates selector on selection

                        //check if the selected spending profile is different from the user's original spending profile
                        if(modalView.selectedModel.get('id') != this.parentView.$spendingProfileSelect.val()) {

                            if(modalView.selectedModel.get('id') != qcssapp.AccountSettings.spendingProfile) {
                                //if the selected spending profile has a price show confirmation message
                                if(modalView.selectedModel.get('price') != "" && modalView.selectedModel.get('price') != "$0.00") {
                                    this.parentView.$spendingProfileSelect.val(modalView.selectedModel.get('id')); //change the modal value
                                    this.parentView.getMealPlanDetails(modalView);

                                    //otherwise select spending profile and show save and cancel buttons
                                } else {
                                    qcssapp.Functions.showMask();
                                    modalView.selectModalValue();
                                    this.parentView.spendingProfileChange();
                                }

                            } else {
                                modalView.selectModalValue();
                                this.parentView.spendingProfileChange();
                            }
                        } else {
                            modalView.closeModal();
                        }
                    }
                }
            });

            this.$spendingProfileSelect = $('#spendingProfileSetting');
        },

        //-------------- SAVE FUNCTION ---------------//

        //if the account group or account type has been changed, save the change (if the spending profile is changed it is automatically changed in QC_Employees so it can display the limits)
        saveAccountSettings: function() {
            // Ensure valid low balance notification input
            if ((this.$userLowBalanceCheckbox.is(":checked") && !this.$userLowBalanceInput.val()) ||
                (this.$employeeLowBalanceCheckbox.is(":checked") && !this.$employeeLowBalanceInput.val())) {
                qcssapp.Functions.displayError("Please provide a number for the Low Balance Threshold.");
                return;
            }

            this.onCharge = false;
            var parameters = { accountSettingsView:this };
            var dataParameters = {};

            if(qcssapp.AccountSettings.accountGroup != this.$accountGroupSelect.val()) {
                dataParameters.accountGroupID = this.$accountGroupSelect.val();
            }

            if(qcssapp.AccountSettings.accountType != this.$accountTypeSelect.val()) {
                dataParameters.accountTypeID = this.$accountTypeSelect.val();
            }

            if(this.tos.length == 0) {
                dataParameters.statusChanged = true;
            }

            if(this.hasEmailChanged) {
                dataParameters.email = this.$accountEmailInput.val();
                if(dataParameters.email != '') {
                    this.needsInviteSent = true;
                }
            }

            if(this.hasLowBalanceChanged) {
                dataParameters.personID = qcssapp.personModel.get('personID');
                dataParameters.lowBalanceThreshold = this.$userLowBalanceInput.val();
            }

            if(this.hasEmpLowBalanceChanged) {
                dataParameters.employeeLowBalanceThreshold = this.$employeeLowBalanceInput.val();
            }

            if($.isEmptyObject(dataParameters)) {
                if(this.tos.length > 0) {
                    var params = {
                        tos: parameters.accountSettingsView.tos
                    };
                    qcssapp.Functions.showTOS(params);
                } else {
                    this.resetAccountSettingsAfterSave();
                }
                return;
            }

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/account/settings/save', 'POST', JSON.stringify(dataParameters), parameters,
                function(response) {
                    if(response) {
                        qcssapp.Functions.hideMask();
                        if(parameters.accountSettingsView.tos.length > 0) {
                            var params = {
                                tos: parameters.accountSettingsView.tos
                            };
                            qcssapp.Functions.showTOS(params);
                        } else {
                            parameters.accountSettingsView.resetAccountSettingsAfterSave();
                        }
                    } else {
                        qcssapp.Functions.displayError('There was an issue saving your Account Settings. Your original account settings have been reset.');
                        var successParameters = {accountSettingsView: parameters.accountSettingsView};

                        var successFunction = function(successParameters) {
                            successParameters.accountSettingsView.loadCollection(false);
                            qcssapp.AccountSettings.needsSave = false;
                            $('#accountSettingButtonContainer, #userSettingButtonContainer').hide();
                        };

                        qcssapp.Functions.cancelChanges(successFunction, successParameters);
                    }
                },
                function() {
                    qcssapp.Functions.displayError('There was an issue saving your Account Settings. Your original account settings have been reset.');
                },
                '', '', '', false, true
            );
        },

        //-------------- CHANGE FUNCTIONS ---------------//

        //if the spending profile is changed, change limits and check is TOS is required
        spendingProfileChange: function(onCharge) {
            $('#accountSettingsMsgContainer, #changeAccountGroupMsg').hide();
            this.changeLimitsAndCheckTOS(this.$spendingProfileSelect, onCharge);
        },

        //event handler if the low balance checkbox is changed
        lowBalanceChange: function(e) {
            var $lowBalanceCheckbox = $(e.currentTarget);
            var lowBalanceCheckboxId = $lowBalanceCheckbox.attr('id');
            var $lowBalanceInput = lowBalanceCheckboxId === 'low-balance-check' ? this.$userLowBalanceInput : this.$employeeLowBalanceInput;

            //whenever the low balance checkbox is changed, set the value to empty
            if(!$lowBalanceCheckbox.is(':checked')) {
                $lowBalanceInput.val('');
                $lowBalanceInput.prop('disabled', true);
                $lowBalanceInput.parent().hide();
            } else {
                $lowBalanceInput.parent().show();
                $lowBalanceInput.val('');
                $lowBalanceInput.prop('disabled', false);
            }

            if(lowBalanceCheckboxId === 'low-balance-check'){
                qcssapp.AccountSettings.needsSave = this.originalLowBalance != this.$userLowBalanceInput.val();
                this.hasLowBalanceChanged = true;
            }
            else{
                qcssapp.AccountSettings.needsSave = this.originalEmpLowBalance != this.$employeeLowBalanceInput.val();
                this.hasEmpLowBalanceChanged = true;
            }

            $('#accountSettingButtonContainer').show();
        },

        //event handler if the email input field is changed
        emailInputChange: function() {
            this.hasEmailChanged = this.originalEmail != this.$accountEmailInput.val();
            qcssapp.AccountSettings.needsSave = this.originalEmail != this.$accountEmailInput.val();
            $('#accountSettingButtonContainer').show();
        },

        //event handler if the fingerprint checkbox is changed
        fingerprintChange: function() {
            if(this.$fingerprintCheckbox.is(':checked') && !qcssapp.Functions.fingerprintEnabled()) {
                qcssapp.Functions.verifyFingerprintOnDevice();

            } else {
               qcssapp.Functions.turnOffFingerprint();
            }
        },

        //event handler if the low balance input field is changed
        lowBalanceInputChange: function(event) {

            var $lowBalanceInput = $(event.target);
            var lowBalanceId = $lowBalanceInput.attr('id');

            //validate the low balance field
            if(!qcssapp.Functions.isNumeric($lowBalanceInput.val())) {
                $lowBalanceInput.val('');
                qcssapp.Functions.displayError('Please provide a number for the Low Balance Threshold.');
            }

            //low balance threshold minimum validation
            if(parseInt($lowBalanceInput.val()) < 0) {
                $lowBalanceInput.val('');
                qcssapp.Functions.displayError('The Low Balance Threshold cannot be negative.');
            }

            //display is as a money value
            if($lowBalanceInput.val() != "" && qcssapp.Functions.isNumeric($lowBalanceInput.val())) {
                var lowBalance = parseFloat($lowBalanceInput.val()).toFixed(2);
                $lowBalanceInput.val(lowBalance);
            }

            if(lowBalanceId === 'lowBalanceSetting'){
                qcssapp.AccountSettings.needsSave = this.originalLowBalance != $lowBalanceInput.val();
            } else if(lowBalanceId === 'empLowBalanceSetting'){
                qcssapp.AccountSettings.needsSave = this.originalEmpLowBalance != $lowBalanceInput.val();
                this.hasEmpLowBalanceChanged = this.originalEmpLowBalance != $lowBalanceInput.val();
            }

            if(qcssapp.AccountSettings.needsSave) $('#accountSettingButtonContainer').show();

        },

        //-------------- SELECTOR FUNCTIONS ---------------//

        //renders the account group and account type selectors
        renderSelector: function(collection, $selector) {
            collection.each(function( mod ) {
                this.renderSelectorLine( mod , $selector);
            }, this );
        },

        //populate the selector
        renderSelectorLine: function(mod , $selector) {
            var selectorView = new qcssapp.Views.AccountSettingsOption({
                model:mod
            });

            $selector.append(selectorView.render().el);

            if(mod.attributes.active) {
                $selector.val(mod.attributes.id);
            }
        },

        //if the account group is changed, reload the selector with the account groups it can change to and the spending profiles mapped to it
        reloadSelectors: function(event, accountGroupVal) {
            if(this.$accountGroupSelect.val() == "") {
                return;
            }

            var parameters = {
                accountSettingsView:this
            };

            var dataParameters = {
                accountGroupID: this.$accountGroupSelect.val(),
                originalSpendingProfile: qcssapp.AccountSettings.spendingProfile
            };

            if(accountGroupVal) {
                dataParameters.accountGroupID = accountGroupVal;
            }

            if( qcssapp.Functions.checkServerVersion(3,0) ) {
                qcssapp.Functions.callAPI(qcssapp.Location + '/api/account/settings/changed', 'POST', JSON.stringify(dataParameters), parameters,
                    function(response) {
                        parameters.accountSettingsView.updateSelectorOptions(response,event);
                    },
                    '', '', '', '', false, true
                );

            } else {
                qcssapp.Functions.callAPI(qcssapp.Location + '/api/account/settings/changed/'+ this.$accountGroupSelect.val(), 'GET', '', parameters,
                    function(response) {
                        parameters.accountSettingsView.updateSelectorOptions(response,event);
                    },
                    function(errorParameters) {
                        errorParameters.accountSettingsView.fetchError();
                    },
                    '', '', '', false, true
                );
            }
        },

        //determine what the spending profile options should be based on the original value selected
        updateSelectorOptions: function(data, event) {
            $('#accountSettingButtonContainer').show();
            $('#changeAccountGroupMsg').hide();

            if(event) {
                $('#accountSettingsMsgContainer').hide();
            }

            qcssapp.AccountSettings.needsSave = true;
            this.hasAccountGroupChanged = true;

            var selectedSpendingProfile = this.$spendingProfileSelect.val();
            var selectedSpendingProfileText = this.$spendingProfileSelect.text();
            this.$spendingProfileSelect.empty();
            this.$accountGroupSelect.empty();
            this.$accountTypeSelect.empty();

            this.render(data);

            //reload spending profile selector with the options it can change to
            if ( this.spendingProfileCollection.length > 0 ) {

                var valueInNewList = false;
                this.spendingProfileCollection.each(function(mod){ //check if the original spending profile value is in the new list
                    if(mod.get('id') == selectedSpendingProfile) {
                        valueInNewList = true;
                    }
                });

                //if the original spending profile value is in the list, set it to that value
                if(valueInNewList) {
                    this.$spendingProfileSelect.val(selectedSpendingProfile).text(selectedSpendingProfileText);
                    if(typeof event !== 'undefined') {
                        this.changeLimitsAndCheckTOS(event);
                    }

                //if the original spending profile is not in the list but there is only one value, set it to this
                } else if (this.spendingProfileCollection.length == 1 && this.spendingProfileCollection.models[0].get("price") == "") {
                    this.$spendingProfileSelect.val(this.spendingProfileCollection.models[0].get("id"));
                    this.$spendingProfileSelect.text(this.spendingProfileCollection.models[0].get("value"));
                    this.hasSpendingProfileChanged = true;
                    if(typeof event !== 'undefined') {
                        this.changeLimitsAndCheckTOS(event);
                    }

                //otherwise don't set the value
                } else {
                    this.$spendingProfileSelect.val(" ");
                    $('#accountSettingSubtitle, .globalSpendingLimitContainer, #spendingLimitsSettingContainer').hide();
                    qcssapp.Functions.hideMask();
                }

            }  else { //if there are no spending profiles for the account group, reset it to the original account group and display error
                //$('#accountSettingsMsgContainer').show().text(this.$accountGroupSelect.find('option[value='+this.$accountGroupSelect.val()+']').text() + ' is not mapped to any Spending Profiles!');
                qcssapp.Functions.displayError(this.$accountGroupSelect.find('option[value='+this.$accountGroupSelect.val()+']').text() + ' is not mapped to any Spending Profiles!');
                this.reloadSelectors(undefined, qcssapp.AccountSettings.accountGroup);
            }
        },

        //-------------- TOS FUNCTIONS ---------------//

        //checks if the TOS is required for the account group and spending profile change
        changeLimitsAndCheckTOS: function(e, onCharge) {
            var accountGroupID = $('#accountGroupSetting').val();
            var spendingProfileID = $('#spendingProfileSetting').val();

            var id = $(e).attr('id');
            if(typeof e.currentTarget !== 'undefined') {
                id = $(e.currentTarget).attr('id');
            }

            this.checkForAccountGroupChange(id);

            qcssapp.AccountSettings.needsSave = true;

            var dataParameters = {
                'accountGroupID':accountGroupID,
                'spendingProfileID':spendingProfileID,
                'spendingProfileChanged':this.hasSpendingProfileChanged,
                'accountGroupChanged': this.hasAccountGroupChanged,
                'originalSpendingProfile': qcssapp.AccountSettings.spendingProfile //pass in the original spending profile to see if user's SP has funding terminal in it
            };

            if(this.tos.length > 0) {
                dataParameters.needsTOS = true;
            }

            var parameters = {
                accountSettingsView:this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/account/check/tos', 'POST', JSON.stringify(dataParameters), parameters,
                function(response) {
                    var $btnContainer = $('#accountSettingButtonContainer');
                    var $cancelBtn =  $('#cancelAccountSettingsBtn');

                    //if a TOS is returned, user needs to accept new TOS for this spending profile change
                    if(response != null) {
                        response = parameters.accountSettingsView.keysToLowerCase(response);
                        parameters.accountSettingsView.tos = response;
                        qcssapp.AccountSettings.accountStatus = true; //account status needs to be changed
                        $btnContainer.show();

                    } else { //no TOS required
                        parameters.accountSettingsView.tos = [];
                        qcssapp.AccountSettings.accountStatus = false;
                        $btnContainer.show();
                    }

                    //if the account group and spending profile are the same as the original values, no change
                    if(qcssapp.AccountSettings.accountGroup == parameters.accountSettingsView.$accountGroupSelect.val() && qcssapp.AccountSettings.spendingProfile == parameters.accountSettingsView.$spendingProfileSelect.val()) {
                        parameters.accountSettingsView.hasAccountGroupChanged = false;
                        parameters.accountSettingsView.hasSpendingProfileChanged = false;
                        parameters.accountSettingsView.tos = [];
                        $btnContainer.hide();
                    }

                    $cancelBtn.show();
                    if(onCharge) {
                        $btnContainer.hide();
                        $cancelBtn.hide();
                        $('#saveAccountSettingsBtn').trigger('click');
                    }

                    //update spending limits container
                    parameters.accountSettingsView.resetLimits();

                },
                function(errorParameters) {
                    errorParameters.accountSettingsView.fetchError();
                },
                '', '', '', true, true
            );
        },

        //check if the account group has been changed when checking if TOS is required
        checkForAccountGroupChange: function(id) {
            if(id == 'accountGroupSetting') {
                this.hasAccountGroupChanged= true;
                return;
            }

            this.hasSpendingProfileChanged = true;

            if(this.$accountGroupSelect.val() != qcssapp.AccountSettings.accountGroup) {
                this.hasAccountGroupChanged= true;
            } else if (this.hasAccountGroupChanged && this.$accountGroupSelect.val() == qcssapp.AccountSettings.accountGroup) {
                this.hasAccountGroupChanged= false;
            }
        },

        //displays a popup that lets the user know the changes they made require another TOS to be accepted
        confirmAccountDetailsChangeForTOS: function() {
            qcssapp.Functions.hideMask();
            var that = this;
            var buttonText = "Proceed";
            var message ="<div class='account-settings-msg-content'>The selections you have made require you to select a Terms of Service. <br/><br/>Click proceed to review the new agreement.</div>";

            if(this.onCharge) {
                buttonText = "Ok";
                message ="<div class='account-settings-msg-content'>The selections you have made require you to select a Terms of Service. <br/>";
            }

            $.confirm({
                title: '',
                content: message,
                onOpenBefore: function() {
                    var $cancelBtn = $('.cancel-btn');
                    $cancelBtn.show();
                    if(that.onCharge) {
                        $cancelBtn.hide();
                    }
                },
                buttons: {
                    cancel: {
                        text: 'Cancel',
                        btnClass: 'btn-default cancel-btn account-settings-btns prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {
                        }
                    },
                    accept: {
                        text: buttonText,
                        btnClass: 'btn-default account-settings-btns prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {
                            //send them to the TOS page
                            $('#item-list').empty();
                            $('#tosview').find('.pages').empty();

                            that.saveAccountSettings();
                        }
                    }
                }
            });
        },

        //-------------- USER AND ACCOUNT SETTING FUNCTIONS ---------------//

        //if using a person account then get the low balance threshold and populate the field
        loadUserSettings: function() {
            this.$userSettingsContainer.show();

            var personID = qcssapp.personModel.get('personID');  //get person ID
            var successParameters = {
                accountSettingsView:this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/account/user/settings/'+ parseInt(personID), 'GET', '', successParameters,
                function(lowBalanceThreshold, successParameters) {
                    if(lowBalanceThreshold == null) {
                        successParameters.accountSettingsView.$userLowBalanceCheckbox.prop('checked', false);
                        successParameters.accountSettingsView.$userLowBalanceInput.val("");
                        successParameters.accountSettingsView.$userLowBalanceInput.parent().hide();
                    } else if (Number(lowBalanceThreshold) >= 0) {
                        successParameters.accountSettingsView.$userLowBalanceCheckbox.prop('checked', true);
                        successParameters.accountSettingsView.$userLowBalanceInput.parent().show();
                        successParameters.accountSettingsView.$userLowBalanceInput.val(lowBalanceThreshold.toFixed(2));
                    }

                    successParameters.accountSettingsView.$userLowBalanceInput.prop('disabled', true);

                    successParameters.accountSettingsView.originalLowBalance = successParameters.accountSettingsView.$userLowBalanceInput.val()

                },
                function() {
                    successParameters.accountSettingsView.$userLowBalanceCheckbox.prop('checked', false);
                    successParameters.accountSettingsView.$userLowBalanceInput.val("");
                    successParameters.accountSettingsView.$userLowBalanceInput.parent().hide();

                    successParameters.accountSettingsView.$userLowBalanceInput.prop('disabled', true);
                    successParameters.accountSettingsView.originalLowBalance = successParameters.accountSettingsView.$userLowBalanceInput.val()
                },
                '', '', '', false, true
            );
        },

        //load the account model settings, add an email input if there is one
        loadAccountSettings: function(data) {
            $('.settingsHeader').show();
            $('#userSettingsHeader').text(qcssapp.Aliases.personAccountAlias + ' Settings');
            $('#accountSettingsHeader').text(qcssapp.Aliases.accountAlias + ' Settings');
            this.$accountEmailInput.prop('disabled', false);
            this.$accountEmailInput.parent().show();

            if(typeof data.accountModel.email !== 'undefined' && data.accountModel.email != "") {  //if there is an email address, set it and disable input
                var emailAddress = qcssapp.Functions.htmlDecode(data.accountModel.email);
                this.$accountEmailInput.val(emailAddress);
                this.$accountEmailInput.prop('disabled', true);

            } else if(!qcssapp.personModel) {  //if email is blank hide field for employee accounts, show for person accounts
                this.$accountEmailInput.val('');
                this.$accountEmailInput.parent().hide();
            } else {
                this.$accountEmailInput.val('');
            }

            //get account's employeeID
            if(typeof data.accountModel.id !== 'undefined' && data.accountModel.id != "") {
                this.employeeID = data.accountModel.id;
            }

            //set account's original email
            this.originalEmail = this.$accountEmailInput.val();
            this.$userSettingsContainer.hide();

            if(qcssapp.personModel) {
                this.loadUserSettings();
            } else {
                $('.settingsHeader').hide();
            }

            if (qcssapp.Functions.checkServerVersion(6, 1) &&
                (data.accountModel.accountTypeId == "2" || data.accountModel.accountTypeId == "4")) {

                if (data.accountModel.employeeLowBalanceThreshold != undefined) {
                    var employeeLowBalanceThreshold = Number(data.accountModel.employeeLowBalanceThreshold);
                    this.originalEmpLowBalance = employeeLowBalanceThreshold.toFixed(2);

                    this.$employeeLowBalanceCheckbox.prop('checked', true);
                    this.$employeeLowBalanceInput.parent().show();
                    this.$employeeLowBalanceInput.val(employeeLowBalanceThreshold.toFixed(2));

                    if(this.$employeeLowBalanceInput.prop('disabled')){
                        this.$employeeLowBalanceInput.prop('disabled', false);
                    }
                }
                else{
                    this.$employeeLowBalanceCheckbox.prop('checked', false);
                    this.$employeeLowBalanceInput.parent().hide();
                }
            }
            else{
                this.$employeeLowBalanceCheckbox.prop('checked', false);
                this.$employeeLowBalanceCheckbox.parent().hide();
                this.$employeeLowBalanceInput.parent().hide();
            }

            //determine fingerprint settings
            this.setFingerprintAuthentication()
        },

        //determine if fingerprint authentication is enabled from Session Model, set checkbox accordingly
        setFingerprintAuthentication: function() {
            $('#fingerprintAuthCheckboxContainer').hide();
            if(!qcssapp.ssoLogin) {
                return;
            }
            qcssapp.Functions.isSSOFingerprintAvailable();
        },

        //-------------- FUNDING FUNCTIONS ---------------//

        getMealPlanDetails: function(modalView) {
            var selectedSpendingProfile = this.$spendingProfileSelect.val();

            var endPoint = qcssapp.Location + '/api/account/calculate/mealPlan/' + selectedSpendingProfile;
            var successParameters = {
                accountSettingsView: this,
                modalView: modalView
            };

            //gets all details related to funding
            qcssapp.Functions.callAPI(endPoint, 'GET', '', successParameters,
                function(response, successParameters) {
                    successParameters.mealPlanModel = response;
                    if(response.accountPaymentMethodModel != null ) {
                        successParameters.accountSettingsView.showConfirmationModal(response, successParameters);
                    } else {
                        successParameters.accountSettingsView.showPaymentMethodModal(response, successParameters);
                    }
                },
                '', '', '', '', true, true
            );
        },

        showPaymentMethodModal: function(response, successParameters) {
            var msg = '<div class="account-settings-msg-content">You must set up a payment method under Account Funding first!</br></br>Do you want to do this now?</div>';
            if(response['accountFundingName']) {
                msg = '<div class="account-settings-msg-content">You must set up a payment method on the '+response['accountFundingName']+' page first!</br></br>Do you want to do this now?</div>';
            }

            var view = this;
            var parameters = successParameters;

            $.confirm({
                title: '',
                content: msg,
                buttons: {
                    cancel: {
                        text: "No",
                        btnClass: 'btn-default account-settings-btns primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {
                            //change spending profile selector back to original value
                            if( view.$spendingProfileSelect.text() == "" && view.$spendingProfileSelect.val() != " " && view.$spendingProfileSelect.val() != null && view.$spendingProfileSelect.val() != "" ) {
                                view.$spendingProfileSelect.val(" ");
                            } else {
                                view.$spendingProfileSelect.val(qcssapp.AccountSettings.spendingProfile);
                            }
                        }
                    },
                    Confirm: {
                        text: "Yes",
                        btnClass: 'btn-default account-settings-btns prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {
                            qcssapp.AccountSettings.needsSave = true;

                            var successParameters = {
                                accountSettingsView:view,
                                modalView: parameters.modalView
                            };

                            var successFunction = function() {
                                //change spending profile selector back to original value and close modal
                                successParameters.accountSettingsView.$spendingProfileSelect.val(qcssapp.AccountSettings.spendingProfile);
                                successParameters.modalView.closeModal();

                                //navigate to Account Funding view
                                new qcssapp.Views.AccountFundingView();
                                qcssapp.Router.navigate('account-funding', {trigger: true});
                            };

                            qcssapp.Functions.cancelChanges(successFunction, successParameters);
                        }
                    }
                }
            });
        },

        showConfirmationModal: function(response, parameters) {
            if( response.total != null) {
                this.chargeAmount = response.total;
            }

            var view = this;
            var msg = '<div class="account-settings-msg-content">Do you want to purchase this Meal Plan? By pressing "Confirm" below, you agree that <b>$'+Number(response.total).toFixed(2)+'</b> will be charged to your payment method on file.</div>';

            if(response.fundingFee != null || response.taxTotal != null) {
                this.renderMessage(parameters);
                return;
            }

            $.confirm({
                title: '',
                content: msg,
                buttons: {
                    cancel: {
                        text: "Cancel",
                        btnClass: 'btn-default account-settings-btns primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {
                            //change spending profile selector back to original value
                            if( view.$spendingProfileSelect.text() == "" && view.$spendingProfileSelect.val() != " " && view.$spendingProfileSelect.val() != null && view.$spendingProfileSelect.val() != "" ) {
                                view.$spendingProfileSelect.val(" ");
                            } else {
                                view.$spendingProfileSelect.val(qcssapp.AccountSettings.spendingProfile);
                            }
                        }
                    },
                    Confirm: {
                        text: "Confirm",
                        btnClass: 'btn-default account-settings-btns prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {
                            //if payment method setup, charge account for meal plan
                            parameters.modalView.closeModal();
                            view.chargeAccount(parameters);
                        }
                    }
                }
            });
        },


        renderMessage: function(parameters) {
            var fundingTitle = 'Price';
            var total = 0;
            //show the name of the Spending Profile next to the amount being charged
            if(parameters.modalView.selectedModel.get('value').length) {
                fundingTitle = parameters.modalView.selectedModel.get('value');
            }

            var mealPlanModel = parameters['mealPlanModel'];

            var options = {
                fundingTitle: fundingTitle,
                fundingAmount: Number(mealPlanModel.price).toFixed(2)
            };

            if( mealPlanModel['taxTotal'] != null  && mealPlanModel.fundingFee != null  ) {
                options.templateID = 'funding-fee-tax-template';
                options.taxRateAmount = Number(mealPlanModel['taxTotal']).toFixed(2);
                options.fundingTotalCharge = Number(mealPlanModel.subtotal).toFixed(2);
                options.fundingFee = Number(mealPlanModel.fundingFee).toFixed(2);
                options.fundingTotalTaxCharge = Number(mealPlanModel.total).toFixed(2);
                options.fundingFeeDisclaimer = mealPlanModel.accountPaymentMethodModel.fundingFeeDisclaimer;
                options.fundingFeeLabel = qcssapp.Functions.htmlDecode(mealPlanModel['surchargeName']);

            } else if (mealPlanModel['taxTotal'] != null) {
                options.fundingFee = Number(mealPlanModel['taxTotal']).toFixed(2);
                options.fundingTotalCharge = Number(mealPlanModel.total).toFixed(2);
                options.fundingFeeDisclaimer = '';
                options.fundingFeeLabel = 'Tax';

            } else if (mealPlanModel.fundingFee != null) {
                options.fundingFee = Number(mealPlanModel.fundingFee).toFixed(2);
                options.fundingTotalCharge = Number(mealPlanModel.total).toFixed(2);
                options.fundingFeeDisclaimer = mealPlanModel.accountPaymentMethodModel.fundingFeeDisclaimer;
                options.fundingFeeLabel = qcssapp.Functions.htmlDecode(mealPlanModel['surchargeName']);
            }

            total = Number(mealPlanModel.total).toFixed(2);
            this.chargeAmount = total;

            var fundingFeeView = new qcssapp.Views.FundingFee(options);

            fundingFeeView.$el.find('.funding-fee_disclaimer').addClass('funding-align');
            fundingFeeView.$el.find('.funding-fee_details').css({'font-size': '18px'});

            var fundingFeeViewHTML = fundingFeeView.$el.html();

            var endingText = this.determineAgreementText(total);

            $.confirm({
                title: '',
                content: fundingFeeViewHTML + endingText,
                onOpenBefore: function() {
                    if($('.jconfirm-scrollpane').width() > 600) {
                        $('.funding-align').offsetParent().parent().parent().parent().parent().css('max-width', '550px');
                    }
                },
                onOpen: function() {
                    if($('.jconfirm-scrollpane').width() > 600) {
                        $('.funding-align').offsetParent().parent().parent().parent().parent().css('max-width', '550px');
                    }
                },
                buttons: {
                    cancel: {
                        text: 'Cancel',
                        btnClass: 'btn-default account-settings-btns primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {
                            //change spending profile selector back to original value
                            parameters.accountSettingsView.$spendingProfileSelect.val(qcssapp.AccountSettings.spendingProfile);
                        }
                    },
                    confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-default account-settings-btns prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {
                            //if payment method setup, charge account for meal plan
                            parameters.modalView.closeModal();
                            parameters.accountSettingsView.chargeAccount(parameters);
                        }
                    }
                }
            });
        },

        //charge the user's account for the meal plan tied to the spending profile
        chargeAccount: function(parameters) {
            qcssapp.Functions.showMask();
            var endPoint = qcssapp.Location + '/api/funding/charge/' + this.chargeAmount;

            qcssapp.Functions.callAPI(endPoint, 'POST', '', parameters,
                function(fundingModel, parameters) {

                    //if charge is successful, create PATransaction
                    if(fundingModel['processedAmount']) {
                        parameters.accountSettingsView.createMealPlanTransaction(parameters, fundingModel);
                    }
                },
                function() {
                    $('#spendingProfileSetting').val(qcssapp.AccountSettings.spendingProfile);
                    qcssapp.Functions.hideMask();
                }, '', '',  30000, true, true
            );
        },

        //creates a PATransaction, sets the tender to the Credit Card Tender that is in the same Revenue Center of the Funding Terminal
        createMealPlanTransaction: function(parameters, fundingModel) {
            var endPoint = qcssapp.Location + '/api/account/mealPlan/transaction';

            var dataParameters = JSON.stringify(parameters.mealPlanModel);

            qcssapp.Functions.callAPI(endPoint, 'POST', dataParameters, parameters,
                function(response, parameters) {
                    qcssapp.Functions.showMask();
                    //if a PATransactionID is returned, change selector value
                    if(response != null && parseInt(response) > 0) {
                        parameters.modalView.selectModalValue();
                        parameters.accountSettingsView.onCharge = true;
                        parameters.accountSettingsView.spendingProfileChange(true);

                    } else {
                        // Reset selector value to original selection
                        $('#spendingProfileSetting').val(qcssapp.AccountSettings.spendingProfile);

                        if(qcssapp.Functions.checkServerVersion(4,1,4)) {
                            parameters.accountSettingsView.refundAccount(fundingModel);

                        } else {
                            qcssapp.Functions.displayError('There was an error creating the transaction for the meal plan purchase. We were unable to refund the charge, please contact support.');
                            qcssapp.Functions.hideMask();
                        }
                    }
                },
                '', '', '',  '', true, true
            );
        },

        //refund the user's account for the meal plan if the transaction fails
        refundAccount: function(fundingModel) {
            var endPoint = qcssapp.Location + '/api/funding/refund';

            qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(fundingModel), '',
                function() {
                    qcssapp.Functions.displayError('There was an error creating the transaction for the meal plan purchase.');

                },
                function() {
                    qcssapp.Functions.displayError('There was an error creating the transaction for the meal plan purchase. We were unable to refund the charge, please contact support.');

                }, '', '',  30000
            );
        },

        determineAgreementText: function(fundingTotalTaxCharge) {
            var introText = '<div class="funding-fee-ending-msg">By pressing "Confirm" below, I agree that ';
            var middleText = ' will be charged to my payment method on file</div>';

            var totalCharge = Number(this.chargeAmount).toFixed(2);

            if ( this.fundingFee != null ) {
                totalCharge = (Number(this.chargeAmount) + Number(this.fundingFee)).toFixed(2);
            }

            if(fundingTotalTaxCharge != null) {
                totalCharge = fundingTotalTaxCharge;
            }

            totalCharge = qcssapp.Functions.formatPriceInApp(totalCharge);

            return introText + '<b>' + totalCharge + '</b>' + middleText;
        },

        //-------------- LIMIT FUNCTIONS ---------------//

        //resets the spending profile limits container
        resetLimits: function() {
            $('#spendingLimitsSettingContainer').empty();
            this.limitCollection = new qcssapp.Collections.Limits();

            if(this.showSpendingProfileDetails) {
                this.displayGlobalLimit();
                this.displayLimits();
            } else {
                $('#accountSettingSubtitle, .globalSpendingLimitContainer, #spendingLimitsSettingContainer').hide();
                qcssapp.Functions.hideMask();
            }
        },

        //GET request for global balance information, adding the global balance view to the page
        displayGlobalLimit: function() {
            $('#accountSettingSubtitle, .globalSpendingLimitContainer, #spendingLimitsSettingContainer').show();

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/limits/global', 'GET', '','',
                function(data) {
                    if ( data ) {
                        new qcssapp.Views.GlobalLimitView(data);
                    }
                },
                function(params) {
                    if (qcssapp.DebugMode) {
                        console.log("Error loading global balance data");
                    }
                },
                '', '', '', false, true
            );
        },

        //displays the limits based on the spending profile (existing functionality)
        displayLimits: function() {
            var successParameters = {
                spendingProfileView:this
            };

            var errorParameters = {
                spendingProfileView:this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/limits', 'GET', '',successParameters,
                function(response, successParameters) {
                    $.each(response, function() {
                        if ( this.avail == null || Number(this.avail) == 1000000 || Number(this.limit) == 1000000 ) {
                            this.avail = "";
                            this.limit = "";
                        } else {
                            this.limit = "$" + this.limit.toFixed(2);
                        }
                    });

                    successParameters.spendingProfileView.limitCollection.set(response);
                    successParameters.spendingProfileView.renderLimits();
                },
                function(errorParameters) {
                    errorParameters.spendingProfileView.fetchError();
                },
                '', errorParameters, '', true, true
            );
        },

        renderLimits: function() {
            this.limitCollection.each(function( mod ) {
                this.renderLimit( mod );
            }, this );

            qcssapp.Functions.hideMask();
        },

        renderLimit: function( mod ) {
            var spendingLimitView = new qcssapp.Views.SpendingLimitView({
                model: mod
            });
            $('#spendingLimitsSettingContainer').append( spendingLimitView.render().el );
        },

        //-------------- HELPER FUNCTIONS ---------------//

        //simulate click of save button when enter is pressed on the email
        enterEventHandler: function(e) {
            var $saveAccountSettingsBtn = $('#saveAccountSettingsBtn');
            if (e.keyCode == 13 && $saveAccountSettingsBtn.length ) {
                $saveAccountSettingsBtn.trigger('click');
            }
        },

        //resets the page when canceling changes with the 'Cancel' button
        resetPage: function() {
            //create new collection
            this.spendingProfileCollection = new qcssapp.Collections.spendingProfile();
            this.accountGroupCollection = new qcssapp.Collections.AccountGroup();
            this.accountTypeCollection = new qcssapp.Collections.AccountType();

            this.limitCollection = new qcssapp.Collections.Limits();

            this.$accountGroupSelect.empty();
            this.$spendingProfileSelect.empty();
            this.$accountTypeSelect.empty();

            this.hasAccountGroupChanged = false;
            this.hasSpendingProfileChanged = false;
            this.hasEmpLowBalanceChanged = false;
            this.tos = [];
            this.chargeAmount = null;
            this.fundingFee = null;

            $('#accountSettingsMsgContainer').hide();

            if(this.forceChange) {
                $('#changeAccountGroupMsg').show();
                if(qcssapp.Functions.checkServerVersion(2,1) && this.$accountTypeSelect.val() == "4") {
                    $('#changeAccountGroupMsg').text("Cannot have a Gift Card account type. Please change your Account Group in order to continue");
                }
            }
        },

        //resets the global variables and page variables, shows success message and hides buttons on 'Save'
        resetAccountSettingsAfterSave: function() {
            qcssapp.AccountSettings.needsSave = false;
            qcssapp.AccountSettings.accountGroup = this.$accountGroupSelect.val();
            qcssapp.AccountSettings.spendingProfile = this.$spendingProfileSelect.val();
            qcssapp.AccountSettings.accountGroup = this.$accountTypeSelect.val();
            qcssapp.AccountSettings.employeeLowBalanceThreshold = this.$employeeLowBalanceInput.val();

            if(this.needsInviteSent && qcssapp.personModel) {
                qcssapp.CreateAccountSettings.inviteEmployeeId = this.employeeID;
                qcssapp.Functions.sendAnotherInvite();
            }

            if(this.forceChange) {
                $('#item-list').empty();
                this.needsInviteSent = false;
                qcssapp.Functions.initializeApplication();
            } else {
                qcssapp.Functions.showMask();
                $('#userSettingButtonContainer, #accountSettingButtonContainer').hide();
                this.hasAccountGroupChanged = false;
                this.hasSpendingProfileChanged = false;
                this.hasEmailChanged = false;
                this.hasLowBalanceChanged = false;
                this.tos = [];
                this.chargeAmount = null;
                this.fundingFee = null;

                this.loadCollection(false, true);
            }
        },

        //change the object keys of the tos object to lowercase for the showTOS() function
        keysToLowerCase: function(response) {
            var newObject = {};
            var keys = Object.keys(response[0]);
            for(var i=0;i<keys.length;i++) {
                var key = keys[i];
                newObject[key.toLowerCase()] = response[0][key];
            }
            response[0] = newObject;
            return response;
        }
    });

    //event handler for clicking on a spending profile options
    $(document).on('change', '#spendingProfileSelect', function() {
        $('#spendingProfileMsgContainer').hide();
        $('#cancelSpendingProfileBtn').show();

        var dataParameters = {'id':parseInt($(this).val())};
        var options = {'verb':'POST', 'dataParameters':dataParameters};

        new qcssapp.Views.spendingProfileSelectView(options);
    });


})(qcssapp, jQuery);   