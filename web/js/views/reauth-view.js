;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ReAuth - View for displaying Re-authorization for order confirmation
     */
    qcssapp.Views.ReAuth = Backbone.View.extend({
        internalName: "ReAuth View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#reauth', //references existing HTML element for Reauth view - can be referenced as this.$el throughout the view

        //template: //template not needed for this view

        events: {
            "keyup #reauth-password": "enterEventHandler",
            "focus #reauth-password": "scrollContainer",
            "focusout #reauth-password": "removeScrollContainer"
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in order store view");
            }

            this.$reauthInput = $('#reauth-password');

            //reset page
            qcssapp.Functions.resetErrorMsgs();
            this.$reauthInput.val('').trigger('change');
            $('#reauth-option-check').prop('checked', false);

            //get options (OrderModel is entire options)
            this.options = options || {};

            this.orderSubmitted = false;

            if ( this.options.reAuthTypeID == 1 ) {
                $('.reauth-option-container').show();
            }

            this.$buttonContainer = $("#reauth-btn-container");

            if ( !this.$buttonContainer.hasClass('initialized') ) {
                this.displayButtons();
            }
        },

        enterEventHandler: function(e) {
            if (e.keyCode == 13) {
                $("#reAuthSubmit").trigger("click");
            }
        },

        displayButtons: function() {
            new qcssapp.Views.ButtonView({
                text: "CANCEL ORDER",
                buttonType: "customCB",
                appendToID: "#reauth-btn-container",
                id: "reauthCancelOrderButton",
                class: "button-flat accent-color-one",
                callback: function() {
                    //new qcssapp.Views.OrderStoreView();
                    qcssapp.Functions.resetOrdering(true);
                    //$('#show-order').find('.template-gen').remove();
                }
            });

            new qcssapp.Views.ButtonView({
                text: "Submit",
                buttonType: "customCB",
                appendToID: "#reauth-btn-container",
                id: "reAuthSubmit",
                parentView: this,
                class: "button-flat accent-color-one",
                callback: function () {
                    if ( this.orderSubmitted ) {
                        if ( qcssapp.DebugMode ) {
                            console.log('The order has already been submitted, preventing submitting again')
                        }

                        return;
                    }

                    var pass = this.parentView.$reauthInput.val();
                    var endPoint = qcssapp.Location + '/api/auth/reauth';
                    var toggleUserReAuth = $('#reauth-option-check').is(':checked');

                    qcssapp.Functions.callAPI(endPoint,'POST',pass,{'view':this.parentView},
                        function(data, successParams) {
                            var isValid = data["isValid"];
                            var callError = data["errorDetails"];

                            if ( callError ) {
                                qcssapp.Functions.displayError(callError);
                            } else if ( isValid ) {
                                successParams.view.$reauthInput.trigger("blur");
                                //submit that order (call)

                                if ( !qcssapp.orderObject.chargeAtPurchaseTime && typeof qcssapp.CurrentOrder.storeModel.get('openTxnPopupMessage') !== 'undefined' && qcssapp.CurrentOrder.storeModel.get('openTxnPopupMessage') != null && qcssapp.CurrentOrder.storeModel.get('openTxnPopupMessage') != "" ){
                                    qcssapp.Functions.openTransactionAlert(qcssapp.CurrentOrder.storeModel.get('openTxnPopupMessage'));

                                } else {
                                    qcssapp.Functions.submitOrder();
                                }

                                successParams.view.orderSubmitted = true;

                                if ( toggleUserReAuth ) {
                                    qcssapp.Functions.callAPI(qcssapp.Location + '/api/auth/reauth/off','GET',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        true
                                    );
                                }
                            } else {
                                qcssapp.Functions.displayError('Could not authenticate. Please make sure that you have entered your password correctly.');
                                qcssapp.Functions.hideMask();
                            }
                        },
                        '',
                        '',
                        '',
                        '',
                        '',
                        true
                    );
                }
            });

            this.$buttonContainer.addClass('initialized');
        },



        scrollContainer: function() {
            if( qcssapp.onPhoneApp && navigator.userAgent.match(/Android/i) && this.$el.find('#reauthErrorContainer').is(':visible')) {
                this.$el.addClass('slide-up');
            }
        },

        removeScrollContainer: function() {
            if( qcssapp.onPhoneApp && navigator.userAgent.match(/Android/i) && this.$el.find('#reauthErrorContainer').is(':visible')) {
                this.$el.removeClass('slide-up');
            }
        }
    });
})(qcssapp, jQuery);