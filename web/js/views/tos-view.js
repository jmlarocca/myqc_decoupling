;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.tosView - View for displaying terms Collections
     */
    qcssapp.Views.tosView = Backbone.View.extend({
        internalName: "TOS View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#tosview .pages', //references existing HTML element for instance code Collections - can be referenced as this.$el throughout the view

        //events: events not needed for this view

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in tos view");
            }

            //create new collection
            this.collection = new qcssapp.Collections.Terms();

            //initialize event listener of "add" to call "renderLine" on the collection
            this.listenTo( this.collection, 'add', this.renderPage );

            var terms = [];

            $.each(options.tos, function() {
                var term = new qcssapp.Models.Term(this);
                terms.push(term);
            });

            this.collection.add(terms[0]);

            return this;
        },

        //creates a new view which is a new panel/page in the application for a single TOS
        renderPage: function( mod ) {
            var termView = new qcssapp.Views.TermView({
                model: mod
            });
            this.$el.append( termView.render().el );

            var $tosAcceptContainer = termView.$el.find('.tosAcceptContainer');

            //TODO: move this inside the accept success callback
            //Continue Button
            new qcssapp.Views.ButtonView({
                text: "Continue",
                buttonType: "customCB",
                appendToID: "#tos-success-button-container",
                id: "tosContinueBtn",
                class: "button-flat accent-color-one",
                callback: function() {
                    qcssapp.Functions.initializeApplication();
                }
            });

            //TODO: this and next buttons should go on termview
            //Add Code Button
            new qcssapp.Views.ButtonView({
                text: "Cancel",
                buttonType: "customCB",
                appendToID: $tosAcceptContainer,
                id: "tosCancelBtn",
                class: "button-flat accent-color-one",
                callback: function() {
                    qcssapp.Functions.logOut();
                }
            });

            //TODO: extract callback to view function, use parentView to call it
            new qcssapp.Views.ButtonView({
                id: 'tos-accept',
                text: 'Submit',
                buttonType: 'customCB',
                class: 'button-flat accent-color-one',
                appendToID: $tosAcceptContainer,
                endPointURL: qcssapp.Location + '/api/account/tos/accept/' + termView.$el.attr('data-tosID'),
                parentView: this,
                callback: function() {
                    qcssapp.Functions.resetErrorMsgs();

                    if ( !$('#tos-accept-check').is(':checked') ) {
                        qcssapp.Functions.displayError('You must accept the conditions before continuing!');
                        return;
                    }

                    var successParameters = {tosView: this.parentView};

                    //TODO: Adjust callAPI parameters
                    qcssapp.Functions.callAPI(this.endPointURL, 'POST', '', successParameters,
                        function(response) {
                            if (qcssapp.DebugMode) {
                                console.log("Success response accepting terms");
                            }

                            //TODO: fix this block, check failure first, remove else
                            if ( response.status == "success" ) {
                                if (qcssapp.DebugMode) {
                                    console.log("Terms approved, starting application");
                                }

                                if ( qcssapp.Functions.checkServerVersion(1,5) ) {
                                    // SEND TOS Acceptance Email
                                    var tosID = termView.$el.attr('data-tosID');
                                    var tos_params = {
                                        tosID:tosID
                                    };
                                    successParameters.tosView.sendTOSEmail(tos_params);
                                }

                                //TODO: TOS success panel should move inside of tosview
                                qcssapp.Router.navigate('tos-success', {trigger:true});
                                //TODO: clean up termview and tosview here
                            } else {
                                if (qcssapp.DebugMode) {
                                    console.log("Error in terms approval");
                                    console.log(response);
                                }

                                qcssapp.Functions.displayError('There was an error accepting the terms. Please try again.');
                            }
                        },
                        function() {
                            if (qcssapp.DebugMode) {
                                console.log("Error response accepting terms");
                            }

                            qcssapp.Functions.hideMask();
                        }
                    );
                }
            });

            //TODO: yikes - clean up
            var $activePanel = $('#tos-page');
            var pageHeight = $(window).height();
            var logoHeight = 71;
            var errorHeight = 20;
            var titleHeight = 34;
            var acceptHeight = 57;
            var headerHeight = 44;
            var footerHeight = 49;

            var termsHeight = pageHeight - headerHeight - footerHeight - logoHeight - errorHeight - titleHeight - acceptHeight - 70;

            $activePanel.find('.termsContainer').height(termsHeight);
        },

        //sends email on TOS accept success
        sendTOSEmail: function(params) {
            // Call API End Point on Server
            qcssapp.Functions.callAPI(qcssapp.Location + "/api/myqc/tos/send/"+params.tosID, "POST", params, '',
                function(){
                    console.log("Send Accepted TOS Email succeeded.");
                },
                function(){
                    console.log("Error calling Send Accepted TOS Email.");
                });
        }

    });
})(qcssapp, jQuery);