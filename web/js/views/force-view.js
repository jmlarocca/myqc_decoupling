;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ForceView - View for displaying force password page
     */
    qcssapp.Views.ForceView = Backbone.View.extend({
        internalName: "Force Password View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#force-password-page', //references existing HTML element

        events: {'keypress input':'submitForm'},

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in force password view");
            }

            _.bindAll(this, 'render', 'attemptPasswordChange', 'submitForm', 'getPasswordStrengthRequirement', 'testStrength');

            this.showTos = false;

            this.options = options || '';

            if ( this.options.tos && this.options.tos[0] && Object.keys(this.options.tos[0]).length > 0 ) {
                this.showTos = true;
            }

            this.$forcePassword = $('#forcePassword');
            this.$forcePasswordConfirm = $('#forcePasswordConfirm');
            this.$forcePswdRequirements = $('#force-password-reqs');

            this.looseMsg = 'Please provide a password with at least <b>six</b> characters <br/>' +
                'Also, include at least <b>two</b> of the following in your password: <br/>' +
                '<ul>' +
                '<li>Uppercase Letters (ex. MMHAYES)</li>' +
                '<li>Lowercase Letters (ex. mmhayes)</li>' +
                '<li>Special Characters (ex. !@#$%_)</li>' +
                '<li>Numeric Characters (ex. 123456)</li>' +
                '</ul>';

            this.strictMsg = 'Please provide a password with at least <b>eight</b> characters <br/>' +
                'Also, include at least <b>three</b> of the following in your password: <br/>' +
                '<ul>' +
                '<li>Uppercase Letters (ex. MMHAYES)</li>' +
                '<li>Lowercase Letters (ex. mmhayes)</li>' +
                '<li>Special Characters (ex. !@#$%_)</li>' +
                '<li>Numeric Characters (ex. 123456)</li>' +
                '</ul>';

            this.passStrength = "strict";
            this.$forcePswdRequirements.html(this.strictMsg);

            this.render();

            this.getPasswordStrengthRequirement();
        },

        //calls renderLine on each item in the spending profile collection
        render: function() {
            if ( $('#forcePasswordSubmit').length ) {
                return;
            }

            new qcssapp.Views.ButtonView({
                text: "Submit",
                appendToID: "#force-password-form-container",
                id: "forcePasswordSubmit",
                buttonType: 'customCB',
                class: "template-gen button-flat accent-color-one",
                parentView:this,
                callback: function() {
                    this.parentView.attemptPasswordChange();
                }
            });

            new qcssapp.Views.ButtonView({
                text: "Cancel",
                appendToID: "#force-password-form-container",
                id: "forcePasswordCancel",
                buttonType: 'customCB',
                class: "template-gen button-flat accent-color-one",
                callback: function() {
                    qcssapp.Functions.logOut();
                }
            });
        },

        attemptPasswordChange: function() {
            qcssapp.Functions.showMask();

            qcssapp.Functions.resetErrorMsgs();

            var passStrength = this.passStrength;

            var password = this.$forcePassword.val();
            var passwordConfirm = this.$forcePasswordConfirm.val();

            var loginName = this.options.loginName;

            var endPoint = qcssapp.Location + '/api/auth/update';
            var dataParameters =  {
                resetPass:password,
                confirmResetPass:passwordConfirm,
                loginName:loginName
            };

            //set the instanceUserTypeID to determine if it is a person account or qc account
            if(qcssapp.Functions.checkServerVersion(3,0) && typeof this.options.data[0].instanceUserTypeID !== 'undefined') {
                dataParameters.instanceUserTypeID = this.options.data[0].instanceUserTypeID
            }

            var successParameters = {
                showTos:this.showTos,
                tos: this.options.tos
            };

            //validation
            if ( password !== passwordConfirm ) {
                qcssapp.Functions.displayError('Passwords do not match.');
                qcssapp.Functions.hideMask();
                return;
            }

            if ( !( (passStrength == "loose" && this.testStrength(password, false)) || (passStrength == "strict" && this.testStrength(password, true)) ) ) {
                qcssapp.Functions.displayError('Password does not meet requirements.');
                qcssapp.Functions.hideMask();
                return;
            }

            //TODO: Adjust callAPI parameters
            qcssapp.Functions.callAPI(endPoint,'POST',JSON.stringify(dataParameters),successParameters,
                function(data, successParameters) {
                    var resetResult = data["details"];
                    var isValid = data["isValid"];
                    var resetErrors = data["errorDetails"];

                    if (resetErrors) { //if any errors encountered..
                        qcssapp.Functions.displayError("An error has occurred! Please try again.");
                    } else if (resetResult && isValid) { //report success message from server
                        $.confirm({
                            title: 'Success!',
                            content: 'Your password has been successfully updated.',
                            icon: 'fa fa-check',
                            buttons: {
                                accept: {
                                    text:'Continue',
                                    btnClass: 'qcss-button button-flat accent-color-one',
                                    action: function() {
                                        if ( !qcssapp.personModel ) {
                                            var continueFunction = function(successParameters) {
                                                if ( successParameters.showTos ) {
                                                    var successParam = {
                                                        tos: successParameters.tos
                                                    };
                                                    //check insufficient amount of licenses
                                                    qcssapp.Functions.checkAccountLicenseAvailability(successParam);
                                                } else {
                                                    qcssapp.Functions.initializeApplication();
                                                }
                                            };
                                            qcssapp.Functions.checkForceChange(continueFunction, successParameters);
                                        } else {
                                            //go to select account to manage view (no back button)
                                            qcssapp.Functions.navigateToManageAccount(true);
                                        }
                                    }
                                }
                            }
                        });
                    } else if (resetResult) { //report message from server
                        qcssapp.Functions.displayError(resetResult);
                        qcssapp.Functions.hideMask();
                    }
                },
                '',
                function() {
                    $("#forcePassword").val("");
                    $("#forcePasswordConfirm").val("");
                    qcssapp.Functions.hideMask();
                }
            );
        },

        submitForm: function(e) {
            if (e.which == 13) {
                this.attemptPasswordChange();
            }
        },

        //GET request for password strength requirement
        getPasswordStrengthRequirement: function() {

            //TODO: this shouldn't be necessary as login/settings is already called on the login page
            qcssapp.Functions.callAPI(qcssapp.Location + '/api/auth/login/settings', 'GET', '','',
                function(data) {

                    if (data["passStrength"] === "loose") {
                        this.passStrength = "loose";
                        this.$forcePswdRequirements.html(this.looseMsg);
                    }

                }.bind(this),
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error getting password strength requirement");
                    }
                }
            );
        },

        testStrength: function( password, strict ) {
            var uppercase = /[A-Z]/;
            var uppercaseBool = uppercase.test(password) ? 1 : 0;
            var lowercase = /[a-z]/;
            var lowercaseBool = lowercase.test(password) ? 1 : 0;
            var numeric = /[0-9]/;
            var numericBool = numeric.test(password) ? 1 : 0;
            var specialCharacter = /[!@#$%^&*()_+=-?<>|]/;
            var specialCharacterBool = specialCharacter.test(password) ? 1 : 0;
            var length = password.length;
            var verification = specialCharacterBool + uppercaseBool + lowercaseBool + numericBool;
            if(strict) {
                return verification >= 3 && length >= 8;
            } else {
                return verification >= 2 && length >= 6;
            }
        }
    });
})(qcssapp, jQuery);   