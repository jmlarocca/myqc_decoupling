;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.HelpSlideView - View for displaying help slides
     */
    qcssapp.Views.HelpSlideView = Backbone.View.extend({
        //is a div
        tagName:  'div',
        className: '',

        // Cache the template function for a single item.
        template: _.template( $('#help-slide-template').html() ),

        initialize: function(options) {

            this.options = options;

            if(!this.options.reverse) {
                this.options.reverse = '';
            }

            return this;
        },

        render: function() {

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.options ) );

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE");

            // If on IE, have to remove and re-add loader to initiate the :before and :after animation
            if((msie > 0  || navigator.userAgent.match(/Trident.*rv\:11\./))) {
                this.$el.find('.help-hand-circle').addClass('help-circle-hide');
            }



            return this;
        }
    });
})(qcssapp, jQuery);
