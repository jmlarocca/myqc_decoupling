;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ManageAccountView - View for displaying accounts to manage
     */
    qcssapp.Views.ManageAccountView = Backbone.View.extend({
        internalName: "Select Account to Manage View", //NOT A STANDARD BACKBONE PROPERTY

        id: 'manage-page', //references existing HTML element

        events: {
            "click .account-person-line": 'selectAccountLine'
        },

        template: _.template( $('#manage-account-template').html() ),

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in manage account view");
            }

            this.$container = $('#manage-account');

            this.autoSelect = options.autoSelect;

            this.fromBack = typeof options.fromBack !== 'undefined';

            this.collection = new qcssapp.Collections.AccountPersonCollection();

            this.$container.attr('data-title', 'Select ' + qcssapp.Aliases.accountAlias);

            this.loadCollection();

            qcssapp.Functions.hideMask();
        },

        loadCollection: function() {
            var personID = qcssapp.personModel.get('personID');
            var endPoint = qcssapp.Location + '/api/account/person/employees/' + personID;

            var successParameters = {
                'manageView': this
            };

            qcssapp.Functions.callAPI(endPoint, 'POST', '', '',
                function(data) {
                    successParameters.manageView.collection.set(data);
                    successParameters.manageView.fetchSuccess();
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Unable to determine employee accounts");
                    }
                    successParameters.manageView.fetchError();
                    qcssapp.Functions.hideMask();
                }
            );
        },

        //handles successful fetching of the collections
        fetchSuccess: function() {
            if (qcssapp.DebugMode) {
                console.log("Successfully loaded data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            var $manageHeader = $('#manage-header');
            $manageHeader.find('.home-link').hide();

            this.$container.html(this.$el.html( this.template() ) );

            this.$employeeList = this.$el.find('#employee-list');
            this.$employeeList.show();

            this.displayButtons();

            //if there is only one employee mapped to this person ID
            if(this.collection.length == 1) {

                //if a student has not been chosen and auto select is ON, automatically select it and update DSKey
                if(!qcssapp.accountModel && this.autoSelect) {
                    var model = this.collection.models[0];
                    var employeeID = model.get('employeeID');
                    var accountModel = model.get('accountModel');

                    this.setAccountModel(employeeID, accountModel);

                //if a has been chosen show selected account but don't need to update DSKey and global accountModel again
                } else {
                    this.render();
                    if(this.autoSelect) {
                        $('.account-person-line').eq(0).addClass('selected-account');
                    }
                }

            //if there is more than one employee mapped to this person ID, show employees
            } else if (this.collection.length > 1) {
                this.render();

            //if there are no employees mapped to this person send them to add account page
            } else {
                $('#manageAccountMsg').text('Please add an account to manage.');
                this.$employeeList.hide();
                qcssapp.accountModel = null;

                if(!this.fromBack) {  //if coming back from Add Account View, don't want to go to it again
                    new qcssapp.Views.AddAccountView();
                    qcssapp.Router.navigate('add-account', {trigger:true});
                }
            }

            //if an employee has been selected, show back icon
            if(qcssapp.accountModel) {
                $manageHeader.find('.home-link').show();
            }
        },

        //handles error for fetching of the collections
        fetchError: function() {
            if (qcssapp.DebugMode) {
                console.log("Error loading data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            //display error message to user
            qcssapp.Functions.displayError('There was an error loading the employee associations for this account, please try again later.');
            qcssapp.Functions.hideMask();
        },

        //calls renderLine on student account
        render: function() {
            this.collection.each(function( mod ) {
                this.renderLine( mod );
            }, this );

            //check if a student in the list has been selected
            this.isAccountSelect();

            this.displayButtons();
        },

        //appends the student account to the list
        renderLine: function( mod ) {
            var personAccountLine = new qcssapp.Views.AccountPersonLineView({
                model:mod
            });

            this.$employeeList.append(personAccountLine.render().el);
        },

        //creates the Add Student button in the view
        displayButtons: function() {
            if ( $('#addAccountBtn').length ) {
                return;
            }

            new qcssapp.Views.ButtonView({
                text: 'Add ' + qcssapp.Aliases.accountAlias,
                buttonType: "customCB",
                appendToID: "#manage-account-btn-container",
                id: "addAccountBtn",
                class: "align-center add-account-button order-button prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                callback: function() {
                    qcssapp.Functions.showMask();

                    new qcssapp.Views.AddAccountView();
                    qcssapp.Router.navigate('add-account', {trigger:true});

                    qcssapp.Functions.hideMask();
                }
            });

        },

        //if the name of the employee in the global account obj is in the list of student's, set as selected
        isAccountSelect: function() {
            var $accountLines = $('.account-person-line');
            var $selectedAccount = '';
            var selectedIndex = '';

            if(qcssapp.accountModel) {
                var name = qcssapp.accountModel.get('name');
                $.each($accountLines, function(index) {
                    var lineName = $(this).find('.account-person-name').text();
                    if(lineName == name) {
                        $(this).addClass('selected-account');
                        $selectedAccount = $(this);
                        selectedIndex = index;
                    }
                });
            }

            if($accountLines.length > 7 && selectedIndex != 0 && selectedIndex != '') {
                $accountLines.splice(selectedIndex, 1);
                $selectedAccount.prependTo(this.$employeeList);
            }
        },

        //event handler when selecting a student account from the list
        selectAccountLine: function(e) {
           // e.stopPropagation();
            var $this = $(e.currentTarget);

            if($this.hasClass('selected-account')) {
                qcssapp.Functions.showMask();
                qcssapp.Router.navigate('main', {trigger: true});
                qcssapp.Functions.checkPersonAccountEmail();
                qcssapp.Functions.hideMask();
                return;
            }

            var id = $this.attr('data-id');
            var $accountLines = $('.account-person-line');

            //apply/remove selected account styles
            $.each($accountLines, function() {
               $(this).removeClass('selected-account');
            });

            $this.addClass('selected-account');

            //set the global accountModel
            this.setAccountModel(id);
        },

        //set the global account model object
        setAccountModel: function(id, accountModelHM) {
            var newAccountModel = {};
            var employeeID = "";

            //if coming into page and only one account available to manage
            if(accountModelHM) {
                employeeID = id;
                newAccountModel = accountModelHM;

            //otherwise selecting account so find the selected account's account model
            } else {
                this.collection.each(function(mod){
                    if(mod.get('id') == id) {
                        newAccountModel = mod.get('accountModel');
                        employeeID = mod.get('employeeID');
                    }
                });
            }

            var accountModel = new qcssapp.Models.AccountInfoModel();

            accountModel.set('name', newAccountModel.name);
            accountModel.set('accountNumber', newAccountModel.number);
            accountModel.set('badgeNumber', newAccountModel.badge);
            accountModel.set('accountStatus', newAccountModel.status);
            accountModel.set('accountType', newAccountModel.accountTypeId);
            accountModel.set('phone', newAccountModel.phone);

            if ( newAccountModel.hasOwnProperty('badgeNumPrefix') ) {
                accountModel.set('badgeNumPrefix', newAccountModel.badgeNumPrefix);
            }

            if ( newAccountModel.hasOwnProperty('badgeNumSuffix') ) {
                accountModel.set('badgeNumSuffix', newAccountModel.badgeNumSuffix);
            }

            if ( newAccountModel.hasOwnProperty('mobilePhone') ) {
                accountModel.set('mobilePhone', newAccountModel.mobilePhone);
            }

            if ( newAccountModel.hasOwnProperty('email') ) {
                accountModel.set('email', newAccountModel.email);
            }

            if ( newAccountModel.hasOwnProperty('qcCodeLength') ) {
                accountModel.set('qcCodeLength', newAccountModel.qcCodeLength);
            }
            qcssapp.accountModel = accountModel;

            //update the DSKey
            this.updateDSKey(employeeID);
        },

        //add the employeeID to the DSKey record
        updateDSKey: function(employeeID) {
            var endPoint = qcssapp.Location+ '/api/account/person/update/dsKey/'+employeeID;

            if(employeeID == "") {
                qcssapp.Functions.displayError('The selected account does not have a valid Employee ID.');
                return;
            }

            qcssapp.Functions.callAPI(endPoint,'GET', '', '',
                function(result) {
                    if(result == 1) {

                        //check that the selected account is active
                        qcssapp.Functions.checkAccountStatus(function() {
                            //need to reload page to show the newly selected student ID and prevent menu list from duplicating
                            $('#item-list').empty();
                            qcssapp.Functions.initializeApplication();
                            qcssapp.Functions.checkPersonAccountEmail();
                            qcssapp.Functions.hideMask();
                        });

                    } else {
                        qcssapp.Functions.displayError('Could not update DSKey for selected Employee. Please try again.');
                    }
                },
                function(params) {
                    if ( qcssapp.DebugMode ) {
                        console.log('ERROR could not update DSKey');
                    }

                    qcssapp.Functions.displayError('Could not update DSKey for selected Employee.');
                },
                '', '', '', true, true
            );

        }


    });
})(qcssapp, jQuery);   