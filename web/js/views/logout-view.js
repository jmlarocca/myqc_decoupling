;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.LogoutView - View for displaying the logout/sso splash screen*/

    qcssapp.Views.LogoutView = Backbone.View.extend({
        internalName: "Logout View", //NOT A STANDARD BACKBONE PROPERTY

        //TODO: This should be the whole panel
        el: '#logoutFormContainer',

        events: {
            'click #logoutLink': 'logoutLinkHandler',
            'click #logoutQCAuthMsg': 'qcAuthLinkHandler'
        },

        initialize: function () {
            //start of application
            if (qcssapp.DebugMode) {
                console.log("Initialize logout view");
            }
        },

        logoutLinkHandler: function(e) {
            // set an item in local storage telling the browser or app this is the origin of the sso request
            localStorage.setItem("SSO-Origin", "1");

            //on phone open the logout link in the system browser
            if ( (typeof window.cordova !== "undefined") ) {
                e.preventDefault();

                var codeCollection = new qcssapp.Collections.instanceCodes();
                codeCollection.fetch();
                var ssoCodeModel = codeCollection.findWhere({CODE: 'SSO'});

                cordova.InAppBrowser.open(ssoCodeModel.get('LOCATION') + '/sso', '_system');
            }

            return true;
        },

        qcAuthLinkHandler: function() {
            $('#addInstance').show();
        }
    });
})(qcssapp, jQuery);
