(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.instanceCodeListItemView - View for displaying spending profile options individually
     */
    qcssapp.Views.instanceCodeListItemView = Backbone.View.extend({
        internalName: "Instance Code List Item View", //NOT A STANDARD BACKBONE PROPERTY

        // Cache the template function for a single item.
        template: _.template( $('#instance-code-list-item-template').html() ),

        render: function() {
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.setElement( this.template( this.model.attributes ) );

            return this;
        }
    });
})(qcssapp, jQuery);
