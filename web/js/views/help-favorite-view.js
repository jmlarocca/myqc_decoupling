;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.HelpFavoriteView - View for displaying help info on the my quick picks page
     */
    qcssapp.Views.HelpFavoriteView = Backbone.View.extend({
        internalName: "Help Favorite View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'help-favorite-cart-view',

        template: _.template( $('#help-favorite-template').html() ),

        events: {
            'click .help-next' : 'nextHelpSlide',
            'click .help-close' : 'closeHelp'
        },

        listID: {
            POPULAR: '#popular-list',
            ITEMS: '#favorite-list',
            ORDERS: '#order-list'
        },

        favoriteHelp: [
             {
                id: 1,
                title: 'Favorite Items',
                text: 'Swipe right to add the favorite item to the Cart',
                buttonText: 'Next',
                showArrow: '',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 2,
                title: 'Favorite Items',
                text: 'Swipe left to remove the item as a favorite',
                buttonText: 'Next',
                showArrow: '',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 3,
                title: 'Favorite Items',
                text: 'Click the product line to go to the product page',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 4,
                title: 'Favorite Orders',
                text: 'Swipe right to express reorder the products in the order',
                buttonText: 'Next',
                showArrow: '',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 5,
                title: 'Favorite Orders',
                text: 'Swipe left to remove the order as a favorite',
                buttonText: 'Next',
                showArrow: '',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 6,
                title: 'Favorite Orders',
                text: 'Click the favorite order line to view all items in the order',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 7,
                title: 'Popular Items',
                text: 'Swipe right to add a popular item to the cart or add customizations',
                buttonText: 'Next',
                showArrow: '',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 8,
                title: 'Popular Items',
                text: 'Click the product line to go to the product page',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }
        ],

        initialize: function (options) {
            this.options = options || {};

            this.parentView = this.options.parentView;

            this.$view = $('#favoriteview');
            this.$activeListID = '#' + this.$view.find('.active-list').attr('id');

            this.slideEnd = 8;
            this.slideStart = 1;

            if (this.$activeListID == this.listID.ITEMS) {
                this.slideStart = 1;
                this.slideEnd = 8;
                this.$view.find('.favoriteBtn').trigger('click');

            } else if (this.$activeListID == this.listID.POPULAR) {
                this.slideStart = 7;
                this.slideEnd = 6;
                this.$view.find('.popularBtn').trigger('click');

            } else if (this.$activeListID == this.listID.ORDERS) {
                this.slideStart = 4;
                this.slideEnd = 3;
                this.$view.find('.ordersBtn').trigger('click');
            }
        },

        render: function() {
            this.$el.html( this.template( {} ) );

            this.formatList();

            this.$myQuickPicksContainer = this.$view.find('.myQuickPicksFilterContainer').clone();
            this.$el.append(this.$myQuickPicksContainer);

            this.renderSlides();

            return this;
        },

        formatList: function() {

            if (this.$activeListID == this.listID.ITEMS) {

                var $favoriteItemLine = $(this.$activeListID) != undefined && $(this.$activeListID).find('.favorite-line') != undefined && $(this.$activeListID).find('.favorite-line').first() != undefined ? $(this.$activeListID).find('.favorite-line').first() : undefined;
                var favoriteOffsetTop = $favoriteItemLine != undefined && $favoriteItemLine.offset() != undefined ? $favoriteItemLine.offset().top : 0;

                if(favoriteOffsetTop > 0) {
                    this.$favoriteItemLine = $favoriteItemLine.clone();
                    this.$favoriteItemLine.css('top', favoriteOffsetTop);

                    this.$el.append(this.$favoriteItemLine);
                }

            } else if (this.$activeListID == this.listID.ORDERS) {

                var $favoriteOrderLine = $(this.$activeListID) != undefined && $(this.$activeListID).find('.favorite-line') != undefined && $(this.$activeListID).find('.favorite-line').first() != undefined ? $(this.$activeListID).find('.favorite-line').first() : undefined;
                var orderOffsetTop = $favoriteOrderLine != undefined && $favoriteOrderLine.offset() != undefined ? $favoriteOrderLine.offset().top : 0;

                if(orderOffsetTop > 0) {
                    this.$favoriteOrderLine = $favoriteOrderLine.clone();
                    this.$favoriteOrderLine.css('top', orderOffsetTop);

                    this.$el.append(this.$favoriteOrderLine);
                }

            } else if (this.$activeListID == this.listID.POPULAR) {

                var $popularLine = $(this.$activeListID) != undefined && $(this.$activeListID).find('.favorite-line') != undefined && $(this.$activeListID).find('.favorite-line').first() != undefined ? $(this.$activeListID).find('.favorite-line').first() : undefined;
                var popularOffsetTop = $popularLine != undefined && $popularLine.offset() != undefined ? $popularLine.offset().top : 0;

                this.$popularLine = null;

                if(popularOffsetTop > 0) {
                    this.$popularLine = $popularLine.clone();
                    this.$popularLine.css('top', popularOffsetTop);

                    this.$el.append(this.$popularLine);
                }
            }
        },

        renderSlides: function() {
            $.each(this.favoriteHelp, function(index, slideInfo) {

                var helpSlideView = new qcssapp.Views.HelpSlideView(slideInfo);

                this.$el.append( helpSlideView.render().$el.find('.help-slide') );

            }.bind(this));
        },

        nextHelpSlide: function() {
            //if at the end of the slide length
            if( this.slideStart == this.slideEnd ) {
                this.removeAllHiddenLists();
                this.parentView.fadeOut();
                return;
            }

            var $currentSlide = this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]');
            $currentSlide.removeClass('active-slide').addClass('inactive-slide');

            this.slideStart++;

            if(this.slideStart == 9) {
                this.slideStart = 1;
            }

            if(this.slideStart == 4) {
                this.$myQuickPicksContainer.find('.myQuickPicksBtn').removeClass('selected secondary-background-color font-color-secondary');
                this.$myQuickPicksContainer.find('.ordersBtn').addClass('selected');

                this.$el.find('.favorite-line').remove();
                this.$view.find('#order-list').removeClass('list-hide');
                this.$view.find('#orders-unavailable-separator').removeClass('list-hide');
                this.$view.find('#order-unavailable-list').removeClass('list-hide');
                this.$view.find('.ordersBtn').trigger('click');

                this.$el.find('.help-slide[data-slide-id="4"]').removeClass('inactive-slide');
                this.$el.find('.help-slide[data-slide-id="5"]').removeClass('inactive-slide');
                this.$el.find('.help-slide[data-slide-id="6"]').removeClass('inactive-slide');

                this.$activeListID = '#order-list';

                this.formatList();
                this.setSlideStyles();

            } else if(this.slideStart == 7) {
                this.$myQuickPicksContainer.find('.myQuickPicksBtn').removeClass('selected secondary-background-color font-color-secondary');
                this.$myQuickPicksContainer.find('.popularBtn').addClass('selected');

                this.$el.find('.favorite-line').remove();
                this.$view.find('#popular-list').removeClass('list-hide');
                this.$view.find('.popularBtn').trigger('click');

                this.$el.find('.help-slide[data-slide-id="7"]').removeClass('inactive-slide');
                this.$el.find('.help-slide[data-slide-id="8"]').removeClass('inactive-slide');

                this.$activeListID = '#popular-list';

                this.formatList();
                this.setSlideStyles();

            } else if(this.slideStart == 1) {
                this.$myQuickPicksContainer.find('.myQuickPicksBtn').removeClass('selected secondary-background-color font-color-secondary');
                this.$myQuickPicksContainer.find('.favoriteBtn').addClass('selected');

                this.$el.find('.favorite-line').remove();
                this.$view.find('#favorite-list').removeClass('list-hide');
                this.$view.find('#favorites-unavailable-separator').removeClass('list-hide');
                this.$view.find('#favorites-unavailable').removeClass('list-hide');
                this.$view.find('.favoriteBtn').trigger('click');

                this.$el.find('.help-slide[data-slide-id="1"]').removeClass('inactive-slide');
                this.$el.find('.help-slide[data-slide-id="2"]').removeClass('inactive-slide');
                this.$el.find('.help-slide[data-slide-id="3"]').removeClass('inactive-slide');

                this.$activeListID = '#favorite-list';

                this.formatList();
                this.setSlideStyles();

            } else {
                var $nextSlide = this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]');
                $nextSlide.addClass('active-slide');

                //if the next slide is the last, change button text to Done
                if( this.slideStart == this.slideEnd ) {
                    $nextSlide.find('.help-next').text('Done');
                }
            }
        },

        setSlideStyles: function() {

            if(this.$activeListID == this.listID.ITEMS) {

                // Slide 1
                var $helpInfoSlide1 = $('.help-slide[data-slide-id="1"] .help-info');
                var $helpHandSlide1 = $('.help-slide[data-slide-id="1"] .help-hand');
                var $helpArrowSlide1 = $('.help-slide[data-slide-id="1"] .help-arrow');

                if(this.$favoriteItemLine == null) {
                    $helpHandSlide1.hide();
                    $helpArrowSlide1.hide();
                    $helpInfoSlide1.addClass('no-favorite-cart-help');
                    $helpInfoSlide1.find('.help-text').text('Once you favorite an item it will show up here');

                } else {
                    this.setHelpArrowTop($helpArrowSlide1, this.$favoriteItemLine);
                    qcssapp.Functions.setHandTop($helpArrowSlide1, $helpHandSlide1, 25);
                    qcssapp.Functions.setHelpInfoTop($helpHandSlide1, $helpInfoSlide1, 25);
                }

                // Slide 2
                var $helpInfoSlide2 = $('.help-slide[data-slide-id="2"] .help-info');
                var $helpHandSlide2 = $('.help-slide[data-slide-id="2"] .help-hand');
                var $helpArrowSlide2 = $('.help-slide[data-slide-id="2"] .help-arrow');

                if(this.$favoriteItemLine == null) {
                    $helpHandSlide2.hide();
                    $helpArrowSlide2.hide();
                    $helpInfoSlide2.addClass('no-favorite-cart-help');
                    $helpInfoSlide2.find('.help-text').text('You can swipe right to add your favorite item to the Cart');


                } else {
                    this.setHelpArrowTop($helpArrowSlide2, this.$favoriteItemLine);
                    qcssapp.Functions.setHandTop($helpArrowSlide1, $helpHandSlide2, 25);
                    qcssapp.Functions.setHelpInfoTop($helpHandSlide2, $helpInfoSlide2, 25);
                }

                // Slide 3
                var $helpInfoSlide3 = $('.help-slide[data-slide-id="3"] .help-info');
                var $helpHandSlide3 = $('.help-slide[data-slide-id="3"] .help-hand');

                if(this.$favoriteItemLine == null) {
                    $helpHandSlide3.hide();
                    $helpInfoSlide3.addClass('no-favorite-cart-help');
                    $helpInfoSlide3.find('.help-text').text('Or you can swipe left to remove your item as a favorite');

                } else {
                    qcssapp.Functions.setHandTop(this.$favoriteItemLine, $helpHandSlide3, 40);
                    qcssapp.Functions.setHelpInfoTop($helpHandSlide3, $helpInfoSlide3, 5);
                }

            } else if(this.$activeListID == this.listID.ORDERS) {

                // Slide 4
                var $helpInfoSlide4 = $('.help-slide[data-slide-id="4"] .help-info');
                var $helpHandSlide4 = $('.help-slide[data-slide-id="4"] .help-hand');
                var $helpArrowSlide4 = $('.help-slide[data-slide-id="4"] .help-arrow');

                if(this.$favoriteOrderLine == null) {
                    $helpHandSlide4.hide();
                    $helpArrowSlide4.hide();
                    $helpInfoSlide4.addClass('no-favorite-cart-help');
                    $helpInfoSlide4.find('.help-text').text('Once you favorite an order it will show up here by name');

                } else {
                    this.setHelpArrowTop($helpArrowSlide4, this.$favoriteOrderLine);
                    qcssapp.Functions.setHandTop($helpArrowSlide4, $helpHandSlide4, 25);
                    qcssapp.Functions.setHelpInfoTop($helpHandSlide4, $helpInfoSlide4, 25);
                }

                // Slide 5
                var $helpInfoSlide5 = $('.help-slide[data-slide-id="5"] .help-info');
                var $helpHandSlide5 = $('.help-slide[data-slide-id="5"] .help-hand');
                var $helpArrowSlide5 = $('.help-slide[data-slide-id="5"] .help-arrow');

                if(this.$favoriteOrderLine == null) {
                    $helpHandSlide5.hide();
                    $helpArrowSlide5.hide();
                    $helpInfoSlide5.addClass('no-favorite-cart-help');
                    $helpInfoSlide5.find('.help-text').text('You can swipe right on your favorite order to express reorder all items in the order');

                } else {
                    this.setHelpArrowTop($helpArrowSlide5, this.$favoriteOrderLine);
                    qcssapp.Functions.setHandTop($helpArrowSlide5, $helpHandSlide5, 25);
                    qcssapp.Functions.setHelpInfoTop($helpHandSlide5, $helpInfoSlide5, 25);
                }

                // Slide 6
                var $helpInfoSlide6 = $('.help-slide[data-slide-id="6"] .help-info');
                var $helpHandSlide6 = $('.help-slide[data-slide-id="6"] .help-hand');

                if(this.$favoriteOrderLine == null) {
                    $helpHandSlide6.hide();
                    $helpInfoSlide6.addClass('no-favorite-cart-help');
                    $helpInfoSlide6.find('.help-text').text('Or you can swipe left to remove the order as a favorite order');

                } else {
                    qcssapp.Functions.setHandTop(this.$favoriteOrderLine, $helpHandSlide6, 40);
                    qcssapp.Functions.setHelpInfoTop($helpHandSlide6, $helpInfoSlide6, 5);
                }

            } else if(this.$activeListID == this.listID.POPULAR) {

                // Slide 7
                var $helpInfoSlide7 = $('.help-slide[data-slide-id="7"] .help-info');
                var $helpHandSlide7 = $('.help-slide[data-slide-id="7"] .help-hand');
                var $helpArrowSlide7 = $('.help-slide[data-slide-id="7"] .help-arrow');

                if(this.$popularLine == null) {
                    $helpHandSlide7.hide();
                    $helpArrowSlide7.hide();
                    $helpInfoSlide7.addClass('no-favorite-cart-help');
                    $helpInfoSlide7.find('.help-text').text('Popular items will show up here, they are the most frequently ordered items');

                } else {
                    this.setHelpArrowTop($helpArrowSlide7, this.$popularLine);
                    qcssapp.Functions.setHandTop($helpArrowSlide7, $helpHandSlide7, 25);
                    qcssapp.Functions.setHelpInfoTop($helpHandSlide7, $helpInfoSlide7, 25);
                }

                // Slide 8
                var $helpInfoSlide8 = $('.help-slide[data-slide-id="8"] .help-info');
                var $helpHandSlide8 = $('.help-slide[data-slide-id="8"] .help-hand');

                if(this.$popularLine == null) {
                    $helpHandSlide8.hide();
                    $helpInfoSlide8.addClass('no-favorite-cart-help');
                    $helpInfoSlide8.find('.help-text').text('You can swipe right on a popular item to add it to the cart or click on it to view customizations');

                } else {
                    qcssapp.Functions.setHandTop(this.$popularLine, $helpHandSlide8, 40);
                    qcssapp.Functions.setHelpInfoTop($helpHandSlide8, $helpInfoSlide8, 5);
                }
            }

            this.hideInactiveSlides();
        },

        setHelpArrowTop: function($helpArrow, $helpLine) {
            var favoriteLineOffsetTop = $helpLine.length ? $helpLine.css('top') : 0;
            favoriteLineOffsetTop = Number(favoriteLineOffsetTop.replace('px', ''));
            var favoriteLineHeight = $helpLine.length ? $helpLine[0].clientHeight : 0;

            var helpArrow1Height = $helpArrow.find('.help-arrow-point').css('height');
            helpArrow1Height = Number(helpArrow1Height.replace('px', ''));

            $helpArrow.css('top', favoriteLineOffsetTop + Number(Number(favoriteLineHeight - helpArrow1Height) / 2));
        },

        hideInactiveSlides: function() {
            if(this.slideStart != 0) {
                this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]').addClass('active-slide');
                this.$el.find('.help-slide[data-slide-id!="'+this.slideStart+'"]').addClass('inactive-slide');
            }
        },

        removeAllHiddenLists: function() {
            this.$view.find('#order-list').removeClass('from-order-back');
            this.$view.find('#order-list').removeClass('list-hide');
            this.$view.find('#orders-unavailable-separator').removeClass('list-hide');
            this.$view.find('#order-unavailable-list').removeClass('list-hide');
            this.$view.find('#favorite-list').removeClass('list-hide');
            this.$view.find('#favorites-unavailable-separator').removeClass('list-hide');
            this.$view.find('#favorites-unavailable').removeClass('list-hide');
            this.$view.find('#popular-list').removeClass('list-hide');
        },

        closeHelp: function() {
            this.removeAllHiddenLists();
            this.parentView.fadeOut();
        }
    });
})(qcssapp, jQuery);
