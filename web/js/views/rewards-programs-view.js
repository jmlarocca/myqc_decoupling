;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.RewardsProgramsView - View for displaying Rewards Program Collection
     */
    qcssapp.Views.RewardsProgramsView = Backbone.View.extend({
        internalName: "Rewards Programs View",

        id: 'rewards-programs-list',

        initialize: function () {
            if (qcssapp.DebugMode) {
                console.log("initialize in rewards programs view");
            }

            _.bindAll(this, 'render');

            this.$container = $('#show-rewards');
            this.collection = new qcssapp.Collections.RewardsProgram();
            this.donationCollection = new qcssapp.Collections.Donations();

            this.loadCollection();
        },

        loadCollection: function() {
            var parameters = {
                rewardsProgramView:this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/loyalty/programs', 'GET', '', parameters,
                function(response, successParameters) {
                    successParameters.rewardsProgramView.fetchSuccess(response);
                },
                function(errorParameters) {
                    errorParameters.rewardsProgramView.fetchError();
                },
                '',
                parameters,
                '',
                false
            );
        },

        fetchSuccess: function(data) {
            var $donationPage = $('.donation-page');
            if($donationPage.length) { // if after fatal error, donation page might still be there
                $donationPage.remove();
            }

            this.rewardsMenuText = $('#show-rewards').attr('data-title').substring(0,$('#show-rewards').attr('data-title').indexOf('Programs'));
            if ( data.length == 0 ) {
                this.$el.append('<li class="align-center italic">No'+this.rewardsMenuText+'Programs available.</li>');

                this.$container.append(this.$el);
                //no point in rendering nothing - end here
                qcssapp.Functions.hideMask();
                return;
            }

            this.collection.set(data, {parse: true});

            if(qcssapp.Functions.checkServerVersion(4,0,50)) {
                this.loadDonationCollection();
                return;
            }

            this.render();
        },

        render: function() {
            this.collection.each(function( mod ) {
                this.renderLine( mod );
            }, this );

            //expand the first rewards program information
            this.collection.at(0).expand();

            if(this.$container.find('#rewards-programs-list').length) {
                this.$container.find('#rewards-programs-list').remove();
            }

            this.$container.append(this.$el);

            if(qcssapp.usingBranding) {
                qcssapp.Functions.setSVGColors();
            }
        },

        renderLine: function( mod ) {
            var controllerModel = new qcssapp.Models.Controller({
                id: 'RewardsProgramView',
                options: {model: mod},
                destructible: true,
                destroyOn: ['NavView']
            });

            var RewardsProgramView = qcssapp.Controller.constructView(controllerModel);

            this.$el.append( RewardsProgramView.render().el );
        },

        fetchError: function() {
            var $donationPage = $('.donation-page');
            if($donationPage.length) { // if after fatal error, donation page might still be there
                $donationPage.remove();
            }

            qcssapp.Functions.displayError('There was an error loading your rewards programs, please try again later.');
            qcssapp.Functions.hideMask();
        },

        // Get the loyalty donations in user's loyalty programs
        loadDonationCollection: function() {

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/loyalty/donations', 'GET', '', '',
                function(response) {
                    this.addDonationsToProgram(response);

                }.bind(this),
                function() {
                    this.fetchDonationErrorError();
                }
            );
        },

        // Update the loyalty programs with any donation
        addDonationsToProgram: function(data) {
            this.collection.each(function( loyaltyProgram ) {
                loyaltyProgram.set('donations', []);
            }, this );

            this.donationCollection.set(data, {parse: true});

            // Loop over the loyalty programs
            this.collection.each(function( loyaltyProgram ) {

                // Loop over the loyalty donations and add the donation to the program that matches the ID
                this.donationCollection.each(function( donation ) {

                    if(!$.isEmptyObject(donation.get('loyaltyProgram')) && donation.get('loyaltyProgram').id && donation.get('loyaltyProgram').id == loyaltyProgram.get('id')) {
                        loyaltyProgram.get('donations').push(donation);
                    }
                }, this );
            }, this );

            this.render();
        },

        fetchDonationErrorError: function() {
            qcssapp.Functions.displayError('There was an error loading your loyalty donations, please try again later.');
            qcssapp.Functions.hideMask();
        }
    });
})(qcssapp, jQuery);