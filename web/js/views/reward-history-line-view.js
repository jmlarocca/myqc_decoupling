;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.RewardHistoryLineView - View for displaying individual rewards on the reward view
     */
    qcssapp.Views.RewardHistoryLineView = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'reward-history-container',

        events: {
            'click': 'viewReceipt'
        },

        // Cache the template function for a single item.
        template: _.template( $('#reward-history-line-template').html() ),

        initialize: function() {
            return this;
        },

        render: function() {
            //format points based on if positive
            if ( Number(this.model.get('points')) > 0 ) {
                this.model.set('points','+' + this.model.get('points'));
            }

            this.model.set('giftIcon' ,'');

            if ( this.model.get('redeemedReward') ) {
                this.model.set('giftIcon','receipt-icon-gift');
            }

            if ( !this.model.get('showReceiptsOnMyQC') ) {
                this.model.set('giftIcon','receipt-icon-none');
            }

            if ( this.model.get('paorderTypeID') != null && Number(this.model.get('paorderTypeID') > 1 ) ){
                this.$el.addClass('show-reorder');
            }

            this.$el.html( this.template( this.model.toJSON() ) );

            return this;
        },

        viewReceipt: function() {
            if ( !this.model.get('showReceiptsOnMyQC') ) {
                return;
            }

            //in case any errors are apparent
            qcssapp.Functions.resetErrorMsgs();
            var transactionID = this.model.get('id');
            var showReorder = this.model.has('paorderTypeID') && (this.model.get('paorderTypeID') == 2 || this.model.get('paorderTypeID') == 3);
            var showFavorites = this.model.get('enableFavorites');

            qcssapp.Functions.viewReceipt(transactionID, showReorder, showFavorites, 'rewards', this.model.get("programID"), this.model.txnView);
        }
    });
})(qcssapp, jQuery);
