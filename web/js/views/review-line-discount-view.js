;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.reviewLineDiscountView - View for displaying Cart Collections
     */
    qcssapp.Views.reviewLineDiscountView = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'review-line review-discount-line',

        // Cache the template function for a single item.
        template: _.template( $('#review-discount-line-template').html() ),

        initialize: function() {
            return this;
        },

        render: function() {
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            return this;
        }
    });
})(qcssapp, jQuery);
