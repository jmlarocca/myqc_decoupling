;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FavoriteLineView - View for displaying individual rewards on the reward view
     */
    qcssapp.Views.FavoriteLineView = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'favorite-line',

        events: {
            'click .line-content': 'goToProductPage',
            'click .remove-favorite': 'removeFavorite',
            'click .undo-remove-favorite': 'undoRemoveFavorite',
            'click .add-cart': 'addToCart',
            'click .add-to-cart': 'slickAddButton',
            'click .remove-from-cart': 'slickRemoveButton'
        },

        // Cache the template function for a single item.
        template: _.template( $('#favorite-line-template').html() ),

        initialize: function() {
            this.slickButtonFraction = 4;
            return this;
        },

        render: function() {
            if(this.model.get('id') != null) {
                this.model.set('orderingFavoriteID', this.model.get('id'));
            }

            this.model.set('favoriteName', this.model.get('name'));

            if(this.model.get('prepOption') != null) {
                var canShowPrepOption = !(this.model.get('prepOption').get('defaultOption') && !this.model.get('prepOption').get('displayDefault')) || (this.model.get('prepOption').get('defaultOption') && Number(this.model.get('prepOption').get('price'))> 0)

                if(canShowPrepOption) {
                    var prepOptionName = qcssapp.Functions.htmlDecode(this.model.get('prepOption').get('name'));

                    this.model.set('favoriteName', this.model.get('name') + ' - <span class="prep-option-name">' + prepOptionName + '</span>');

                    if( Number(this.model.get('prepOption').get('price')) > 0 ) {
                        var prepPrice = Number(this.model.get('prepOption').get('price')).toFixed(2);
                        this.model.set('favoriteName', this.model.get('name') + ' - <span class="prep-option-name">' + prepOptionName + ' (' + qcssapp.Functions.formatPrice(prepPrice, '+') + ')</span>');
                    }
                }
            }

            if ( this.model.get('productID') != null) {

                //determine price or product count for favorite ordres
                if ( this.model.get('primary') != "order" && this.model.get('price') != null && this.model.get('price') != "" ) {
                    this.model.set('price', qcssapp.Functions.formatPrice(this.model.get('price')));

                } else if (this.model.get('primary') == "order") {  //replace price with product count for favorite orders
                    this.model.attributes.price = this.model.get('productCount') == 1 ? this.model.get('productCount') + ' Item' : this.model.get('productCount') + ' Items';

                } else {
                    this.model.set('price', qcssapp.Functions.formatPrice('0.00'));
                }

                if ( this.model.get('modifierNames') != "" || ( this.model.get('modifiers') && this.model.get('modifiers').length > 0 ) ) {
                    this.model.set('modifierNames', qcssapp.Functions.convertModifierAndPrepOptionArrayToCsv(this.model.get('modifiers')));
                } else {
                    this.model.set('modifierNames', '');
                    this.$el.find('.favorite-line_modifiers').hide();
                }

                $.each(this.model.get('modifiers'), function() {
                    if(this.prepOption != null) {
                        this.prepOption = qcssapp.Functions.createPrepOptionModel(this.prepOption);
                    }
                    if( this.defaultPrepOption != null ) {
                        this.prepOption = qcssapp.Functions.createPrepOptionModel(this.defaultPrepOption);
                    }
                });
            }

            this.$el.html( this.template( this.model.toJSON() ) );

            this.$el.attr('data-id', this.model.get('id'));
            this.$el.find('.line-content').attr('data-id', this.model.get('id'));

            this.setImage();

            this.setIcons();

            if(this.model.get("popularModifierID") != null || (this.model.get('primary') == 'popular' && this.model.get('prepOptionSetID') != null) ) {
                this.$el.addClass('popular-modifiers');
            }

            if( (this.model.get('primary') == 'favorite' || this.model.get('primary') == 'order') && !this.model.get('available') ) {
                this.$el.find('.action').hide();
            }

            if (this.model.get('primary') == "order") {
                this.$el.find('.line-content').addClass('order-line');
            }

            if (this.model.get('modifiers').length == 0) {
                this.$el.find('.modifier-names').hide();
            }

            if(qcssapp.Functions.checkServerVersion(3,0) && this.model.get('primary') != "order") {
                var nutritionInfoView = new qcssapp.Views.NutritionInfoView({collection: this.model.get('nutritionInfo')});

                if(nutritionInfoView.collection.length != 0) {
                    this.$el.find('.favorite-line_nutrition-container').addClass('show-nutrition').append($(nutritionInfoView.render().el).find('.line_nutrition-info'));
                }

                if(nutritionInfoView.collection.length == 1 && this.$el.find('.favorite-line_icon-container').children().length) {
                    this.$el.find('.favorite-line_info-container').addClass('align-row');
                }
            }

            return this;
        },

        setImage: function() {
            if (this.model.get('image') == "") {
                this.$el.find('.favorite-line_image-container').hide();
                return
            }

            this.$el.find('.line-content').addClass('hasImage');

            var imagePath = qcssapp.qcBaseURL + '/webimages/'+ this.model.get('image');

            qcssapp.onPhoneApp = (typeof window.cordova !== "undefined");
            if ( qcssapp.onPhoneApp ) {   // Cordova API detected
                var hostName = qcssapp.Functions.get_hostname(qcssapp.Location);
                imagePath = hostName + '/webimages/'+ this.model.attributes.image;
            }

            var backgroundImageSize = 'cover';

            this.$el.find('.favorite-line_image').attr('data-bg', imagePath);
            this.$el.find('.favorite-line_image').addClass('lazyload');
            this.$el.find('.favorite-line_image').css('background-size', backgroundImageSize);

            this.$el.find('.favorite-line_image').on('lazyloaded', function(e) {
                var $image = $(e.target);

                if(qcssapp.DebugMode) {
                    var name = $image.parent().parent().find('.favorite-line_name').text();
                    console.log('lazy loaded: ' + name);
                }

                // handle broken images
                if($image.hasClass('broken-image')) {
                    $image.parent().hide();
                }

                setTimeout(function() {
                    this.model.toggleImageLoaded();
                }.bind(this), 100);

            }.bind(this));

            //hide the image for fade-in effect later
            this.$el.find('.favorite-line_image').addClass('hideImages');

            this.formatImageShape();
        },

        setIcons: function(){
            if(!qcssapp.Functions.checkServerVersion(3,0)) {
                return;
            }

            if(this.model.get('productID') == null) {
                return;
            }

            //add the icons in order based on what the primary list is
            if(this.model.get('primary') == "favorite") {
                this.appendIcon('favorite', '<img class="quickpick-icon heart-line-icon" src="images/icon-favorites.svg"/>');
                this.appendIcon('popular', '<img class="quickpick-icon" src="images/icon-popular.svg"/>');
                this.appendIcon('recent', '<img class="quickpick-icon" src="images/icon-recent.svg"/>');

            } else if(this.model.get('primary') == "popular") {
                this.appendIcon('popular', '<img class="quickpick-icon" src="images/icon-popular.svg"/>');
                this.appendIcon('favorite', '<img class="quickpick-icon heart-line-icon" src="images/icon-favorites.svg"/>');
                this.appendIcon('recent', '<img class="quickpick-icon" src="images/icon-recent.svg"/>');

            } else if(this.model.get('primary') == "recent") {
                this.appendIcon('recent', '<img class="quickpick-icon" src="images/icon-recent.svg"/>');
                this.appendIcon('popular', '<img class="quickpick-icon" src="images/icon-popular.svg"/>');
                this.appendIcon('favorite', '<img class="quickpick-icon heart-line-icon" src="images/icon-favorites.svg"/>');
            }

            this.appendIcon('healthy', "<img src=\""+qcssapp.healthyIndicator.wellness+"\">", true);
            this.appendIcon('vegetarian', "<img src=\""+qcssapp.healthyIndicator.vegetarian+"\">", true);
            this.appendIcon('vegan', "<img src=\""+qcssapp.healthyIndicator.vegan+"\">", true);
            this.appendIcon('glutenFree', "<img src=\""+qcssapp.healthyIndicator.glutenFree+"\">", true);

            if(this.$el.find('.favorite-line_icon-container').children().length > 0) {
                this.$el.find('.favorite-line_icon-container').addClass('show-icons');
            }

            var grayscalePercent = qcssapp.Functions.getIconGrayscalePercentage();
            if(grayscalePercent != '') {
                this.$el.find('.favorite-line_icon-container img').css('filter', 'grayscale('+grayscalePercent+'%)');
            }
        },

        appendIcon: function(tab, img, isHealthyIcon) {
            if(this.model.get(tab) && isHealthyIcon) {
                this.$el.find('.favorite-line_icon-container').append(img);

            } else if(this.model.get(tab)) {
                this.$el.find('.favorite-line_name').append(img);
            }
        },

        //for when you click on a product line, go to product page
        goToProductPage: function() {
            //if favorite order and available, on click go to favorite order view
            if(this.model.get('primary') == "order" && this.model.get('available')) {
                this.goToFavoriteOrderPage();

            //if favorite order and unavailable, on click show remove favorite order modal
            } else if (this.model.get('primary') == "order" && !this.model.get('available')) {
                this.removeProductAsFavoritePopup();

            } else {  //else favorite product or popular

                qcssapp.ProductInProgress.prepOptionSetID = this.model.get('prepOptionSetID');
                qcssapp.ProductInProgress.favorite = this.model.get('favorite');

                //if on popular page, go right to product page - don't need to worry about modifiers
                if(this.model.get('primary') == "popular") {
                    qcssapp.Functions.goToProductDetailFromFavorite(this.model);

                    //if on recent or favorite page, need to show popup on product page if there are unavailable modifiers
                } else if(this.model.get('primary') == "recent" || this.model.get('primary') == "favorite") {

                    //clicking on an unavailable favorite triggers remove unavailable favorite popup
                    if( !this.model.get('available') ) {
                        this.removeProductAsFavoritePopup();
                        return;
                    }

                    qcssapp.Functions.goToProductDetailFromFavorite(this.model);
                }
            }
        },

        goToFavoriteOrderPage: function() {
            var favoriteOrderID = this.model.get('id');
            if(favoriteOrderID == null) {
                qcssapp.Functions.displayError('There was an issue loading the favorite order details');
                return;
            }

            qcssapp.Functions.addHistoryRecord(0, 'favorite-order');

            var params = {
                orderList: this.model.get('orderList'),
                indexInList: this.model.get('favoriteOrderIndex')
            };

            qcssapp.Functions.loadFavoriteOrder(favoriteOrderID, params);
        },

        removeFavorite: function() {
            this.model.set('active', false);

            //remove favorite order
            if(this.model.get('primary') == 'order') {
                qcssapp.Functions.toggleFavoriteOrder(this.model.get('id'), '', true);

                //remove favorite product
            } else if ( !qcssapp.Functions.checkServerVersion(2,1) ) {
                qcssapp.Functions.removeFavorite(this.model);

                //remove favorite product
            } else {
                qcssapp.Functions.removeFavorite(this.model.get('orderingFavoriteID'));
            }
        },

        undoRemoveFavorite: function(e) {
            e.stopPropagation();
            this.model.set('active', true);

            //add favorite order
            if(this.model.get('primary') == 'order') {
                qcssapp.Functions.toggleFavoriteOrder(this.model.get('id'), '', true);

            } else {
                var mod =  this.model;
                if(qcssapp.Functions.checkServerVersion(4,0)) {
                    mod = new qcssapp.Models.ProductFavorite();
                    mod.set('id', this.model.get('productID'));
                    mod.set('productID', this.model.get('productID'));
                    mod.set('orderingFavoriteID', this.model.get('id'));
                    mod.set('modifiers', this.model.get('modifiers'));
                    mod.set('prepOption', this.model.get('prepOption'));
                    mod.set('active', true);
                }

                qcssapp.Functions.createFavorite(mod);
            }

            if(this.model.get('available')) {
                this.setSlicker();
            }
        },

        //remove an unavailable product as a favorite
        removeProductAsFavoritePopup: function() {
            var name = this.model.get('name');
            var id = this.model.get('id');
            var message = '<div class="unavailable-mods-popup">Would you like to remove <b>'+name+'</b> as a favorite?</div>';
            var that = this;
            var $favoriteLine = this.$el;

            $.confirm({
                title: '',
                content: message,
                buttons: {
                    cancel: {
                        text: 'Cancel',
                        btnClass: 'btn-default existing-mods-btn prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {}
                    },
                    remove: {
                        text: 'Remove',
                        btnClass: 'btn-default existing-mods-btn prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {
                            setTimeout(function() {

                                $favoriteLine.addClass('hide-slide');

                                $.toast({
                                    text: name + ' was removed from Favorites. <br><a id="undo-favorite-remove" data-id="'+id+'">Undo</a>',
                                    loader:false,
                                    transition: 'slide',
                                    position: 'bottom-center-stretch',
                                    hideAfter: 10000,
                                    bgColor: '#4D4D4D'
                                });


                            }, 500);
                            that.removeFavorite();
                        }
                    }
                }
            });
        },

        addToCart: function() {
            if(this.model.get('primary') == 'order') {
                qcssapp.CurrentOrder.products = [];

                var callback = function() {
                    qcssapp.CurrentOrder.expressReorder = true;
                    qcssapp.Functions.checkForRewardsAndDiscounts();
                };

                var params = {
                    orderList: this.model.get('orderList'),
                    indexInList: this.model.get('favoriteOrderIndex')
                };

                qcssapp.Functions.loadFavoriteOrder(this.model.get('id'), params, callback);
                return;
            }

            qcssapp.ProductInProgress.prepOptionSetID = this.model.get('prepOptionSetID');
            qcssapp.ProductInProgress.prepOption = this.model.get('prepOption');
            qcssapp.ProductInProgress.favorite = this.model.get('favorite');

            if(this.model.get('unavailableModifiers') != "" || this.model.get('unavailableModPrepOptions') != "") {
                qcssapp.Functions.goToProductDetailFromFavorite(this.model);

            } else {
                qcssapp.Functions.loadProductFavorite(this.model);
            }
        },

        setSlicker: function() {

            if(this.model.get('primary') == 'recent' || this.model.get('primary') == 'popular') {
                this.$el.find('.remove-from-cart').remove();
            } else if(!this.model.get('available')) {
                return;
            }

            this.$el.slick({
                infinite: false,
                initialSlide: 1,
                touchThreshold: this.slickButtonFraction * 2,
                variableWidth: true,
                outerEdgeLimit: true
            });

            this.$el.on('beforeChange', function(event,slick,currentSlide,nextSlide) {
                if(nextSlide == 2) {
                    if($(this).parent().attr('id') != "favorite-list" && $(this).parent().attr('id') != "order-list") {
                        event.preventDefault();
                    }
                }

                if ($(this).find('.line-content').hasClass('order-line')) {
                    $(this).find('.add-to-cart').text('Express Reorder');
                }
            });

            // Hide the Slick arrows
            $('.slick-arrow').hide();

            // Set the widths of the Slick slides
            var lineWidth = this.$el.width();
            if (lineWidth > 0) {
                this.$el.find(".slick-slide:has(.line-content)").width(lineWidth);
                this.$el.find(".slick-slide:has(.action)").width(lineWidth / this.slickButtonFraction);
                this.$el.find(".action").width(lineWidth / this.slickButtonFraction);
                this.$el.slick("setPosition");
            }
        },

        // Called when the Slick add button is pressed
        slickAddButton: function() {
            var lineView = this;
            var $favoriteLine = this.$el;

            var message = 'Added!';
            if($favoriteLine.find('.add-to-cart').text() == message) {
                return;
            }

            $favoriteLine.find('.add-to-cart').text(message);

            if($favoriteLine.hasClass('popular-modifiers')) {
                lineView.goToProductPage()
            } else {
                var productModel = lineView.model;
                productModel.id = lineView.model.get('productID');
                if(qcssapp.Functions.checkPurchaseRestriction(lineView.model)) {
                    lineView.addToCart();
                }
            }

            setTimeout(function() {
                //if the product is in the popular list and can be customized, send to product page
                $favoriteLine.slick('slickGoTo', 1);
                $favoriteLine.find('.add-to-cart').text('Add to Cart');
            }, 1000);
        },

        // Called when the Slick remove button is pressed
        slickRemoveButton: function() {
            var lineView = this;
            var $favoriteLine = this.$el;
            var productName = $favoriteLine.find('.favorite-line_name').text();

            var $undoFavorite = $favoriteLine.find('.undo-remove-favorite');

            $favoriteLine.find('.remove-from-cart').text(productName + ' removed');

            $undoFavorite.appendTo($favoriteLine.find('.remove-from-cart'));

            $favoriteLine.addClass('hide-slide');

            setTimeout(function() {
                if($favoriteLine.find('.remove-from-cart').text() == 'Remove') {
                    return;
                }

                var id = $favoriteLine.find('.line-content').attr('data-id');

                $favoriteLine.find('.remove-from-cart').text('Remove');
                $undoFavorite.appendTo($favoriteLine.find('.remove-from-cart'));

                $.toast({
                    text: productName + ' was removed from Favorites. <br><a id="undo-favorite-remove" data-id="'+id+'">Undo</a>',
                    loader:false,
                    transition: 'slide',
                    position: 'bottom-center-stretch',
                    bgColor: '#4D4D4D',
                    hideAfter: 10000
                });

                lineView.removeFavorite();

            },300);
        },

        // Format the shape of the image on the menu button
        formatImageShape: function() {
            var imageShapeID = null;

            if(typeof this.model.get('imageShapeID') === 'undefined' || this.model.get('imageShapeID') == null || this.model.get('imageShapeID') == '' ) {
                imageShapeID = 1;
            } else {
                imageShapeID = this.model.get('imageShapeID');
            }

            if(imageShapeID == 2) { // Square
                this.$el.find('.favorite-line_image-container').addClass('line_image-square');

            } else if (imageShapeID == 3) { // Circle
                this.$el.find('.favorite-line_image-container').addClass('line_image-circle');
            }

            if(this.model.get('shrinkImageToFit') && imageShapeID == 3) {
                this.$el.find('.favorite-line_image-container').addClass('shrink-to-fit-circle');
            } else if(this.model.get('shrinkImageToFit')) {
                this.$el.find('.favorite-line_image-container').addClass('shrink-to-fit');
            }
        }

    });
})(qcssapp, jQuery);
