;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.SelectView - View for displaying select elements
     */
    qcssapp.Views.SelectView = Backbone.View.extend({
        internalName: "Select View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'qcss-select',

        //template: //template not needed for this view

        events: {
            'click': 'selectClick'
        },

        initialize: function (options) {
            //assign the selector options
            this.options = options;

            //id -> selector ID
            this.id = this.options.id;

            //class -> selector classes
            this.class = this.options.class;

            if (qcssapp.DebugMode) {
                console.log("Initialize in select view - id: " + this.id);
            }
            //appendToID -> where to append the selector
            this.appendToID = this.options.appendToID;

            //the data that populates the modal
            this.data = this.options.data;

            //the text property for the selector
            this.textProperty = this.options.textProperty;

            //the value property for the selector
            this.valueProperty = this.options.valueProperty;

            //the initial value of the selector when first loaded
            this.initialValue = this.options.initialValue;

            //callback after the button is clicked
            this.callback = this.options.callback;

            //onOpen function called after modal is opened
            this.onOpen = this.options.onOpen;

            //render the selector
            this.render();

            //sets the modal properties
            this.modal = this.options.modal;

            return this;
        },

        render: function() {

            this.$el.attr('id',this.id); //set selector id

            if(this.initialValue) { //if the selector has an initial value set it
                this.$el.val(this.initialValue);

                if(this.valueProperty && this.textProperty) {  //find the display text corresponding with the value
                    var text = '';
                    this.data.each( function( mod ) {
                        if(mod.get(this.valueProperty) == this.initialValue) {
                            text = mod.get(this.textProperty);
                        }
                    }, this );

                    this.$el.text(text);
                }
            }

            if(this.data.length > 1) { //if there are selector options show the select-arrow
                this.class += 'selector-down-arrow';
            }


            if(this.$el.prop('disabled') == true) { //if the selector is disabled, don't open modal and remove the select-down-arrow
                this.$el.removeClass('selector-down-arrow');
                return;
            }

            this.$el.appendTo(this.appendToID).addClass(this.class); //append the selector and set its classes
        },

        //general event click handling for all selectors
        selectClick: function(e) {
            e.preventDefault();
            
            if (qcssapp.DebugMode) {
                console.log("Select element clicked - id: " + this.id);
            }

            if(this.$el.prop('disabled') == true) { //if the selector is disabled, don't open modal and remove the select-down-arrow
                this.$el.removeClass('selector-down-arrow');
                return;
            }

            this.renderModal();

            if(this.onOpen) {
                this.onOpen();
            }

            return false;
        },

        //open the modal child view
        renderModal: function() {
            var that = this;

            new qcssapp.Views.Modal({
                title: this.modal.title,
                id: this.modal.id,
                parentView: this.modal.parentView,
                selectorID: this.id,
                selectorTextProperty: this.textProperty,
                selectorValueProperty: this.valueProperty,
                columnHeaderTemplateID: this.modal.columnHeaderTemplateID,
                columnHeaderDependentField: this.modal.columnHeaderDependentField,
                templateID: this.modal.templateID,
                templateData: this.data,
                callback: function(modalView) {
                    that.modal.callback(modalView);
                }
            });
        }

    });
})(qcssapp, jQuery);
