(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FundingView - View for displaying Funding Collections
     */
    qcssapp.Views.FundingView = Backbone.View.extend({
        internalName: "Funding View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#show-funding', //references existing HTML element for Funding Collections - can be referenced as this.$el throughout the view

        //template: //template not needed for this view

        events: {
            'scroll': 'checkScroll'
        },

        initialize: function () {
            if (qcssapp.DebugMode) {
                console.log("initialize in funding view");
            }

            this.itemsPerPage = 20;
            this.currentPage = 1;
            this.sort = "date";
            this.sortOrder = "desc";
            this.endOfList = false;

            //bind "this" reference to functions in the view
            _.bindAll(this, "fetchSuccess", "fetchError", "checkScroll");

            //create new collection
            this.collection = new qcssapp.Collections.Funding();

            this.$fundingList = $('#funding-list');
            this.$fundingList.children().remove();

            //property to prevent further requests while loading, true because of the next fetch, turned false in fetch success
            this.loading = true;

            //populates the new collection
            this.loadCollection();
        },

        fetchError: function() { //handles error for fetching the funding collection
            if (qcssapp.DebugMode) {
                console.log("Error loading data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            //display error message to user
            qcssapp.Functions.displayError('There was an error loading your funding records, please try again later.');
            qcssapp.Functions.hideMask();
        },

        fetchSuccess: function(data) { //handles successful fetching the funding collection
            if (qcssapp.DebugMode) {
                console.log("Successfully loaded data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            if ( this.currentPage == 1 && data.length == 0 ) {
                this.$fundingList.append('<li class="align-center italic">No Funding Records Found.</li>');

                //no point in rendering nothing
                qcssapp.Functions.hideMask();
                return false;
            }

            if ( this.collection.length < this.itemsPerPage ) {
                this.endOfList = true;
            }

            //display view
            this.render();

            //reset loading once complete
            this.loading = false;

            //for infinite scroll message
            qcssapp.Functions.hideMask();
        },

        //calls renderLine on each funding
        render: function() {
            this.collection.each(function( mod ) {
                this.renderLine( mod );
            }, this );

            if ( this.endOfList ) {
                this.$fundingList.append('<li class="align-center primary-color endlist accent-color-one italic">End of Funding List</li>');
            }
        },

        //builds new funding transaction view and appends to this list view
        renderLine: function( mod ) {
            var fundingLineView = new qcssapp.Views.FundingLineView({
                model: mod
            });
            this.$fundingList.append( fundingLineView.render().el );
        },

        //determines whether to load more based on scroll position and loading status
        checkScroll: function() {
            if ( this.endOfList || this.loading ) {
                return;
            }

            var triggerPoint = 100; //100px from bottom of page
            var pageScroll = this.$el.scrollTop();
            var pageViewHeight = this.$el.height();
            var listHeight = this.$fundingList.height();

            if ( pageScroll + triggerPoint + pageViewHeight > listHeight ) {
                this.currentPage += 1;

                this.loading = true;
                qcssapp.Functions.showMask();

                //load data
                this.loadCollection();
            }
        },

        loadCollection: function() {
            var dataParameters = $.param({ models: this.itemsPerPage, page: this.currentPage, sort: this.sort, order: this.sortOrder });

            var successParameters = {
                fundingView:this
            };

            var errorParameters = {
                fundingView:this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/funds', 'GET', dataParameters,successParameters,
                function(response, successParameters) {
                    successParameters.fundingView.collection.set(response, {parse: true});
                    successParameters.fundingView.fetchSuccess(response);
                },
                function(errorParameters) {
                    errorParameters.fundingView.fetchError();
                },
                '',
                errorParameters
            );
        }
    });
})(qcssapp, jQuery);   