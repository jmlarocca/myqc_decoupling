;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.SpendingLimitView - View for displaying the global balance view
     */
    qcssapp.Views.NutritionInfoLineView = Backbone.View.extend({
        internalName: "Nutrition Info Line View",

        // Cache the template function for a single item.
        template: _.template( $('#nutrition-info-template').html() ),

        //events: //events not needed for this view
        render: function() {

            if(this.model.get('nutritionInfo').length) {
                if(this.model.get('nutritionInfo').indexOf(" ") != -1 && this.model.get('nutritionInfo').indexOf("e+") != -1) {
                    var formattedNutrition = this.model.get('nutritionInfo').substring(0, this.model.get('nutritionInfo').indexOf(" "));
                    var newNutrition = this.model.get('nutritionInfo').replace(formattedNutrition, Number(formattedNutrition));
                    this.model.set('nutritionInfo', newNutrition);
                }
            }

            this.model.set('nutritionAmt', this.model.get('nutritionInfo'));
            this.model.set('nutritionLabel', '');

            if(this.model.get('nutritionInfo').indexOf(" ") != -1) {
                var nutritionAmount = this.model.get('nutritionInfo').substring(0, this.model.get('nutritionInfo').indexOf(" "));
                var nutritionLabel = this.model.get('nutritionInfo').substring(this.model.get('nutritionInfo').indexOf(" "));

                this.model.set('nutritionAmt', Number(nutritionAmount));
                this.model.set('nutritionLabel', nutritionLabel);
            }

            this.setElement(this.template(this.model.toJSON()));
            return this;
        }
    });
})(qcssapp, jQuery);
