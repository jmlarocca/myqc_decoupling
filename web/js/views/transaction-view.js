;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
	qcssapp.Functions.SetGlobalViewSettings();

 /* qcssapp.Views.TransactionView - View for displaying Purchases Collections
	*/
    qcssapp.Views.TransactionView = Backbone.View.extend({
        internalName: "Transaction View", //NOT A STANDARD BACKBONE PROPERTY  

        tagName: 'ul',
        className: 'info-list scrollElement template-gen-container list',
        id: 'transaction-list', //references existing HTML element for Rewards Collection- can be referenced as this.$el throughout the view

        //template: //template not needed for this view

        events: {
            "click .trans-click": 'viewReceipt'
        },

        initialize: function () {
            if (qcssapp.DebugMode) {
                console.log("initialize in transaction view");
            }

            this.$container = $('#show-purchases');

            this.initialized = false;

            //TODO make a model with these defaults
            this.itemsPerPage = 20;
            this.currentPage = 1;
            this.sort = "date";
            this.sortOrder = "desc";
            this.endOfList = false;
            this.txnList = [];

            //bind "this" reference to functions in the view
            _.bindAll(this, "fetchSuccess", "fetchError", "checkScroll");

            //event didn't bubble properly for using the events, need to explicitly set the function trigger
            this.$container.on('scroll',this.checkScroll);

            //create new collection
            this.collection = new qcssapp.Collections.Purchases();

            //property to prevent further requests while loading, true because of the next fetch, turned false in fetch success
            this.loading = true;

            //populates the new collection
            this.loadCollection();
        },

        fetchError: function() { //handles error for fetching the transaction collection
			if (qcssapp.DebugMode) {
				console.log("Error loading data for "+this.internalName);
				console.log(this);
				console.log(this.collection.toJSON());
			}

            //display error message to user
            qcssapp.Functions.displayError('There was an error loading your purchases, please try again later.');
            qcssapp.Functions.hideMask();
		},

		fetchSuccess: function(data) { //handles successful fetching the transaction collection
			if (qcssapp.DebugMode) {
				console.log("Successfully loaded data for "+this.internalName);
				console.log(this);
				console.log(this.collection.toJSON());
			}

            if(qcssapp.help.allowHelpfulHints) {
                this.$container.find('.help-icon').removeClass('hidden');
            }

            if ( this.currentPage == 1 && data.length == 0 ) {
                this.$container.append(this.$el);

                $('#transaction-list').append('<li class="align-center italic">No Purchases Found.</li>');

                if(qcssapp.help.allowHelpfulHints) {
                    this.$container.find('.help-icon').addClass('hidden');
                }

                //no point in rendering nothing
                qcssapp.Functions.hideMask();
                return false;
            }

            if ( this.collection.length < this.itemsPerPage ) {
                this.endOfList = true;
            }

			//display view
			this.render();

            //reset loading once complete
            this.loading = false;

            qcssapp.Functions.hideMask();
        },

        //calls renderLine on each transaction
        render: function() {
            if ( this.currentPage == 1 ) {
                this.$container.append(this.$el);
            }

            this.collection.each(function( txn ) {
                if(txn.get('duplicateTxn')) {
                    return;
                }

                this.renderLine( txn );
            }, this );

            if ( this.endOfList  && !$('.endlist').length) {
                $('#transaction-list').append('<li class="template-gen primary-color endlist accent-color-one align-center italic">End of Purchases List</li>');
            }

        },

        //builds new individual transaction view and appends to this list view
        renderLine: function( txn ) {
            var txnView = new qcssapp.Views.TxnView({
                model: txn
            });

            if( typeof txn.get('date') !== 'undefined' ) {
                var date = qcssapp.Functions.htmlDecode(txn.get('date'));
                var transactionMoment = moment(date, "MM/DD/YYYY HH:mm A");
                var today = moment();
                var isFuture = (transactionMoment.diff(today)) > 0;

                if(today.date() == transactionMoment.date() && today.month() == transactionMoment.month() && today.year() == transactionMoment.year()) {
                    isFuture = false;
                }

                var $futureHeader = $('.date-header.future');
                var $todayHeader = $('.date-header.today');

                if(isFuture && !$futureHeader.length) {
                    this.$el.prepend('<div class="date-header future">Future Orders</div>');

                } else if(!isFuture && $futureHeader.length && !$todayHeader.length) {
                    this.$el.append("<div class='date-header today'>Today and Previous Day's Orders</div>");

                }
            }

            this.$el.append( txnView.render().el );
        },

        //determines whether to load more based on scroll position and loading status
        checkScroll: function() {
            var triggerPoint = 100; //100px from bottom of page
            var $page = $('#show-purchases');
            var pageScroll = $page.scrollTop();
            var pageViewHeight = $page.height();
            var listHeight = this.$el.height();

            if ( !this.loading && !this.endOfList && (pageScroll + triggerPoint + pageViewHeight) > (listHeight) ) {
                this.loading = true;

                this.currentPage += 1;

                this.loadCollection();
            }

            if(!$page.length) {
                return;
            }

            if ( ($page.scrollTop() + $page.innerHeight() ) >= $page[0].scrollHeight ) {
                $('.scroll-indicator').hide();
            } else {
                $('.scroll-indicator').show();
            }
        },

        loadCollection: function(receiptCallback, receiptView) {
            var dataParameters = $.param({ models: this.itemsPerPage, page: this.currentPage, sort: this.sort, order: this.sortOrder });
            dataParameters = decodeURIComponent(dataParameters);

            var successParameters = {
                txnView:this
            };

            var errorParameters = {
                txnView:this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/transactions', 'GET', dataParameters,successParameters,
                function(response, successParameters) {
                    $.each(response, function() {
                        this.amt = this.amt.toFixed(2);
                        this.date = qcssapp.Functions.htmlDecode(this.date);
                        var datetime = this.date.substring(0, this.date.length - 6);
                        var ampm = this.date.substring(this.date.length - 2, this.date.length);
                        var newDate = datetime +"&nbsp;"+ ampm;
                        this.date = newDate;

                        if ( this.patransactionID == null ) {
                            this.patransactionID = 0;
                        }
                    });

                    successParameters.txnView.collection.set(response);
                    successParameters.txnView.fetchSuccess(response);

                    var currentTxnList = successParameters.txnView.txnList;
                    var newTxnList =  successParameters.txnView.collection.toJSON();

                    successParameters.txnView.txnList = $.merge(currentTxnList, newTxnList);

                    if(receiptCallback) {
                        receiptCallback(receiptView, successParameters.txnView);
                    }

                    if(qcssapp.Functions.helpViewActive()) {
                        var viewHeight = this.$container.find('#transaction-list')[0].clientHeight + 60;
                        $('#purchasesview').find('.help-view').height(viewHeight);
                    }
                }.bind(this),
                function(errorParameters) {
                    errorParameters.txnView.fetchError();
                },
                '',
                errorParameters
            );
        },

        viewReceipt: function(e) {
            //in case any errors are apparent
            qcssapp.Functions.resetErrorMsgs();
            e.stopPropagation();
            var $target = $(e.currentTarget);
            var transactionID = 0;
            var hasReceipt = false;
            var showReorder = false;
            var showFavorites = true;

            //dont know exactly where they will click/tap, get the ID out wherever they do
            if($target.hasClass('transaction-header')) {
                $target = $target.parent().find('.transaction-body')
            } else if($target.hasClass('list-store'))  {
                $target = $target.parent().parent();
            } else {
                $target = $target.parent();
            }

            if( $target.hasClass('transaction-body') ) {
                transactionID = $target.attr('data-transactionID');
                hasReceipt = !$target.parent().hasClass('no-receipt');
                showReorder = !$target.parent().hasClass('no-reorder');
                showFavorites = !$target.parent().hasClass('no-favorites'); //Defect 3915: Added .parent() to correctly validate the favorites check
            }

            if ( !hasReceipt ) {
                return false;
            }

            if ( $('#main').attr('data-onlineordering') == "no" ) {
                showReorder = false;
            }

            qcssapp.Functions.viewReceipt(transactionID, showReorder, showFavorites, 'purchases', undefined, this);
        }
    });
})(qcssapp, jQuery);   