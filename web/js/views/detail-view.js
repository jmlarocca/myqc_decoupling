;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.DetailView - View for displaying Detail Collections
     */
    qcssapp.Views.DetailView = Backbone.View.extend({
        internalName: "Detail View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#user-information', //references existing HTML element for Detail Collections - can be referenced as this.$el throughout the view

        template: _.template( $('#detail-template').html() ),

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in " + this.internalName);
            }

            //create new model with given options
            this.model = new qcssapp.Models.Details({
                name: qcssapp.accountModel.get('name'),
                number: qcssapp.accountModel.get('accountNumber'),
                type: qcssapp.accountModel.get('accountType'),
                status: qcssapp.accountModel.get('accountStatus'),
                label: qcssapp.personModel == null ? 'Account Number' : qcssapp.Aliases.accountAlias + ' ID'
            });

            //display view
            this.render();
        },

        render: function() {
            this.$el.html( this.template( this.model.toJSON() ) );
            return this;
        }
    });
})(qcssapp, jQuery);   