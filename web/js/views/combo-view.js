;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ComboView - View for displaying combo details
     */
    qcssapp.Views.ComboView = Backbone.View.extend({
        internalName: "Combo View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#comboview', //references existing HTML element

        template: _.template( $('#combo-template').html() ),

        events: {
            'click .combo-quantity-increment': 'addQuantity',
            'click .combo-quantity-decrement': 'removeQuantity',
            'change #combo-quantity': 'validateQuantity'
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in combo view");
            }

            _.bindAll(this, 'render', 'renderLine', 'addQuantity', 'removeQuantity', 'validateQuantity', 'calculateCombo', 'displayButtons', 'addToOrder', 'updateOrder', 'inquireCombo', 'updatePrices');

            this.options = options || '';

            this.render();
        },

        //calls renderLine on each item in the combo
        render: function() {

            this.calculateCombo();

            // Format the combo price
            qcssapp.CurrentCombo.comboPriceText = qcssapp.Functions.formatPrice(qcssapp.CurrentCombo.comboPrice);

            this.$el.find('.pages').html( this.template( qcssapp.CurrentCombo ) );

            // Remove the formatted combo price
            delete qcssapp.CurrentCombo.comboPriceText;

            this.$quantity = this.$el.find('#combo-quantity');
            this.$comboItems = this.$el.find('#combo-items-list');
            this.$comboItems.children().remove();

            var quantity = qcssapp.ComboInProgress[0].get('quantity');
            this.$quantity.val( quantity ).trigger('change');

            this.displayButtons();

            var view = this;
            $.each(qcssapp.ComboInProgress, function ( ) {

                //calculate the total
                this.set('total', this.get('quantity') * this.get('price'));
                this.set('productName', this.get('name'));

                view.renderLine( this );
            });

            var subtotal = qcssapp.CurrentCombo.comboSubtotal;

            subtotal = Number(quantity) * Number(subtotal);

            //calculate and add subtotal,
            this.$comboItems.append('<li class="cart-line subtotal-line combo-subtotal">Subtotal:<span class="subtotal accent-color-one">'+ qcssapp.Functions.formatPrice(subtotal) +'</span></li>');

            qcssapp.Functions.hideMask();
        },

        renderLine: function( mod ) {
            mod.set('total', Number( mod.get('total').toFixed(2) ) );

            var comboLineView = new qcssapp.Views.ComboItemLineView({
                model:mod
            });

            this.$comboItems.append(comboLineView.render().$el);
        },

        calculateCombo: function() {

            var combo = qcssapp.CurrentCombo;

            var products = qcssapp.ComboInProgress;
            var comboPrice = 0;
            var comboSubtotal = 0;

            $.each(combo.comboDetails, function() {
                var comboDetail = this;
                comboPrice += comboDetail.comboPrice;

                $.each(products, function() {
                    if(comboDetail.keypadDetailPAPluIds.length && $.inArray(this.get('productID').toString(), comboDetail.keypadDetailPAPluIds) != -1 && !this.attributes.hasOwnProperty('foundDetail')) {
                        comboSubtotal += ((this.get('price') - comboDetail.basePrice) + comboDetail.comboPrice);

                        if( this.get('basePrice') == null ) {
                            this.set('basePrice', comboDetail.basePrice);
                            this.set('comboPrice', comboDetail.comboPrice);

                            var upcharge = this.get('price') - this.get('basePrice');
                            this.set('upcharge', upcharge);
                        }

                        this.set('foundDetail', true);
                        return false;
                    }
                });
            });

            $.each(products, function() {
                delete this.attributes.foundDetail;
            });

            qcssapp.CurrentCombo.comboPrice = Number(comboPrice).toFixed(2);
            qcssapp.CurrentCombo.comboSubtotal = Number(comboSubtotal).toFixed(2);
            qcssapp.CurrentCombo.quantity = 1;
        },

        displayButtons: function() {
            if ( $('#comboAddToOrderBtn').length ) {
                return;
            }

            var buttonText = "Add to Order";
            if(qcssapp.CurrentCombo.editingFromCart===true) {
                buttonText = "Update";
            }

            new qcssapp.Views.ButtonView({
                text: buttonText,
                appendToID: "#combo-button-container",
                id: "comboAddToOrderBtn",
                buttonType: 'customCB',
                class: "align-center order-button prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color product-button",
                parentView:this,
                callback: function() {
                    qcssapp.Functions.showMask();

                    if(qcssapp.CurrentCombo.editingFromCart==true) {
                        this.parentView.updateOrder();

                    } else {
                        this.parentView.addToOrder();
                    }
                }
            });

            new qcssapp.Views.ButtonView({
                text: "Cancel",
                appendToID: "#combo-button-container",
                id: "comboCancelBtn",
                buttonType: 'customCB',
                class: "align-center order-button primary-button-font primary-border-color product-button",
                parentView: this,
                callback: function() {
                    qcssapp.OrderMenuHistory.pop();

                    var historyLength = qcssapp.OrderMenuHistory.length-1;

                    if(qcssapp.OrderMenuHistory[historyLength].type == 'cart') {
                        qcssapp.Functions.resetCombos();
                        qcssapp.Router.navigate('cart', {trigger: true});
                        return;
                    }

                    var found = null;
                    for (var i=historyLength; i>=0; i--) {
                        var id = qcssapp.OrderMenuHistory[i].id;
                        if(qcssapp.CurrentCombo.id != "" && $.inArray(id, qcssapp.CurrentCombo.comboDetailKeypadIds) != -1) {
                            //pop the current page off
                            qcssapp.OrderMenuHistory.pop();
                        } else {
                            found = i;
                        }
                    }

                    if(found != null) {
                        var obj = qcssapp.OrderMenuHistory[found];
                        qcssapp.Functions.loadKeypad(obj.id, true);
                    }

                    qcssapp.Functions.resetCombos();
                }
            });

            if(qcssapp.CurrentCombo.editingFromCart == false) {
                return;
            }

            new qcssapp.Views.ButtonView({
                text: "REMOVE FROM ORDER",
                appendToID: "#combo-button-container",
                id: "comboRemoveBtn",
                buttonType: 'customCB',
                class: "align-center order-button primary-button-font primary-border-color product-button",
                callback: function() {
                    var comboIndex = qcssapp.ComboInProgress[0].get("comboModel").get("id");
                    qcssapp.Functions.removeComboFromCart(comboIndex);
                    qcssapp.Functions.resetCombos();
                    qcssapp.Router.navigate('cart', {trigger: true});
                }.bind(this)
            });
        },

        addToOrder: function() {
            var successFn = function(response) {
                //update the combo name from the keypad button
                response.combos[0].combo.name = qcssapp.CurrentCombo.name;

                var combo = new qcssapp.Collections.Combo();
                combo.set(response.combos, {parse:response.combos});

                if(qcssapp.CurrentOrder.combos.length) {
                    var comboModel = new qcssapp.Models.Combo(response.combos[0]);
                    comboModel.id = qcssapp.CurrentOrder.combos.length;
                    comboModel.set('id', qcssapp.CurrentOrder.combos.length);
                    qcssapp.CurrentOrder.combos.add(comboModel);

                    var mostRecentComboTransLineItemId = 0 - qcssapp.CurrentOrder.combos.length;
                    $.each(response.products, function() {
                        this.comboTransLineItemId = mostRecentComboTransLineItemId;
                    });

                } else {
                    qcssapp.CurrentOrder.combos = combo;
                }

                var comboObjects = {
                    products: qcssapp.ComboInProgress,
                    combos: qcssapp.CurrentOrder.combos
                };

                //reformat the products in the combo after they are returned from the order/inquire
                qcssapp.Functions.setCurrentOrderProducts(response.products, comboObjects);

                qcssapp.ComboInProgress = comboObjects.products;

                $.each(comboObjects.products, function( index ) {
                    qcssapp.Functions.addProductToCart(this);
                });

                qcssapp.Functions.navigateToCart();
            };

            this.inquireCombo(successFn);
        },

        updateOrder: function() {
            var successFn = function(response) {
                //update the combo name from the keypad button
                response.combos[0].combo.name = qcssapp.CurrentCombo.name;

                var combo = new qcssapp.Collections.Combo();
                combo.set(response.combos, {parse:response.combos});

                var comboModel = new qcssapp.Models.Combo(response.combos[0]);
                comboModel.set('id', qcssapp.CurrentCombo.editObj.id);
                qcssapp.CurrentOrder.combos.models.splice(qcssapp.CurrentCombo.editObj.id, 1, comboModel);

                var comboObjects = {
                    products: qcssapp.ComboInProgress,
                    combos: combo
                };

                //reformat the products in the combo after they are returned from the order/inquire
                qcssapp.Functions.setCurrentOrderProducts(response.products, comboObjects);

                if(qcssapp.CurrentCombo.updatedComboTransLineId) {
                    $.each(comboObjects.products, function( index ) {
                        this.set('comboTransLineItemId', qcssapp.CurrentCombo.updatedComboTransLineId);
                    });
                }

                var collectionIndex = 0;

                $.each(qcssapp.CurrentOrder.products, function() {

                    if( !$.isEmptyObject(this.get('comboModel')) && this.get('comboModel').get('id') == qcssapp.CurrentCombo.editObj.id ) {
                        var index = this.id;
                        if ( index >= 0 ) {
                            if(!qcssapp.CurrentCombo.updatedComboTransLineId) {
                                comboObjects.products[collectionIndex].set('comboTransLineItemId', qcssapp.CurrentOrder.products[index].get('comboTransLineItemId'));
                            }

                            qcssapp.CurrentOrder.products.splice(index, 1, comboObjects.products[collectionIndex]);
                            qcssapp.CurrentOrder.products[index].id = index;
                            qcssapp.CurrentOrder.products[index].set('id', index);
                            qcssapp.CurrentOrder.products[index].get('comboModel').set('id', qcssapp.CurrentCombo.editObj.id);
                            collectionIndex++;
                        }
                    }
                });

                qcssapp.Functions.navigateToCart();
            };

            this.inquireCombo(successFn);
        },

        inquireCombo: function(successFn) {
            var productArr = [];

            var quantity = this.$quantity.val();

            $.each(qcssapp.ComboInProgress, function() {
                var productObj = {
                    "id": this.get('productID'),
                    "quantity": quantity,
                    "name": this.get('name'),
                    "price": this.get('originalPrice'),
                    "modifiers": qcssapp.Functions.buildModifierArray(this.get('modifiers')),
                    "keypadID": this.get('keypadID'),
                    "prepOption": this.get('prepOption'),
                    "comboTransLineItemId": qcssapp.CurrentCombo.id
                };

                productArr.push(productObj);
            });

            //create temporary order object to pass combo products to
            var orderObject = {
                "products": productArr,
                "combos": []
            };

            qcssapp.Functions.orderInquiry(orderObject, successFn, '', '', '', '');
        },

        addQuantity: function(e) {
            e.stopPropagation();

            this.$quantity.val( Number( this.$quantity.val() ) + 1).trigger("change");
        },

        removeQuantity: function(e) {
            e.stopPropagation();

            if ( Number( this.$quantity.val() ) == 1 ) {
                return false;
            } else {
                this.$quantity.val( Number( this.$quantity.val() ) - 1).trigger("change");
            }
        },

        validateQuantity: function(e) {
            e.stopPropagation();

            if ( qcssapp.Functions.isNumeric( this.$quantity.val() ) && Number(this.$quantity.val()) > 0 ) {
                this.updatePrices();
                return true;

            } else {
                $.alert('Invalid quantity, please enter a positive number');
                this.$quantity.val('1');
            }
        },

        updatePrices: function() {
            var quantity = this.$quantity.val();

            var subtotal = qcssapp.CurrentCombo.comboSubtotal;
            var subtotalQuantity = Number(quantity) * Number(subtotal);
            this.$el.find('.subtotal').html(qcssapp.Functions.formatPriceInApp(subtotalQuantity));
        }

    });
})(qcssapp, jQuery);   