;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ProductCodeInputView - View for modal to input product code
     */
    qcssapp.Views.ProductCodeInputView = Backbone.View.extend({
        internalName: "Product Code Input View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'product-code-input-view hidden',

        template: _.template( $('#product-code-input-template').html() ),

        //handle "enter" events on form fields
        events: {
            "click .product-code-input_close-button": "handleClose",
            "click .product-code-input_submit-button": "handleSubmit",
            "focus .product-code-input": "scrollContainer",
            "focusout .product-code-input": "removeScrollContainer"
        },

        initialize: function (options) {
            this.options = options || {};

            this.hidden = this.options.hidden;

            this.callback = this.options.callback;
            this.callbackParams = this.options.callbackParams;

            _.bindAll( this, 'handleSubmit', 'handleClose');

            this.render();
        },

        render: function() {
            this.$el.html( this.template( {} ) );

            $('body').append( this.$el );

            this.$productCodeInput = this.$el.find('.product-code-input');
            this.$productCodeContainer = this.$el.find('.product-code-input_container');
            this.$productCodeInputContainer = this.$el.find('.product-code-input_input-container');
            this.$productCodeInput = this.$el.find('.product-code-input');

            this.fadeIn();

            return this;
        },

        handleClose: function() {
            this.hide();
            this.$el.remove();
        },

        handleSubmit: function() {
            this.$productCodeInputContainer.removeClass('invalid');

            if(this.$productCodeInput.val() == '') {
                this.$productCodeInputContainer.addClass('invalid');
            } else {
                qcssapp.Functions.handleProductScan(this.$productCodeInput.val().toString());
            }
        },

        scrollContainer: function() {
            if( qcssapp.onPhoneApp && navigator.userAgent.match(/Android/i) ) {
                this.$productCodeContainer.addClass('slide-up');
            }
        },

        removeScrollContainer: function() {
            if( qcssapp.onPhoneApp && navigator.userAgent.match(/Android/i) ) {
                this.$productCodeContainer.removeClass('slide-up');
            }
        },

        fadeIn: function(dur) {
            dur = !dur ? 150 : dur;

            this.hidden = false;
            //this.$el.fadeIn(dur, function() {
            this.$el.removeClass('hidden');
            // }.bind(this));
        },

        hide: function() {
            this.hidden = true;
            this.$el.fadeOut(150, function() {
                this.$el.hide();
                this.$el.remove();
            }.bind(this));
        },

        displayError: function( errorMsg ) {
            this.$errorMsg.html( errorMsg );
            this.$errorMsg.slideDown();
        }
    });
})(qcssapp, jQuery);
