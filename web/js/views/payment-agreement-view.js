;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.PaymentAgreement - View for displaying payment agreement page
     */
    qcssapp.Views.PaymentAgreement = Backbone.View.extend({
        internalName: 'Payment Agreement View', //NOT A STANDARD BACKBONE PROPERTY

        el: '#payment-agreement', //references existing HTML element for - can be referenced as this.$el throughout the view

        //template: //template not needed for this view

        //events: //events not needed for this view

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log('initialize in payment agreement view');
            }

            this.options = options || {};

            //passed in options
            this.paymentType = this.options.paymentType;
            this.endPoint = this.options.endPoint;
            this.currentBalance = this.options.currentBalance;
            this.accountFundingView = this.options.accountFundingView;
            this.minutesUntilFunding = 0;

            //view properties
            this.dataParameters = '';
            this.agreementText = '';
            this.fundingFee = null;

            if ( this.paymentType == 'oneTime' ) {
                this.fundingAmount = Number(this.options.oneTimeAmount);
            } else {
                this.fundingAmount = Number(this.options.reloadAmount);
            }

            //view elements
            this.$agreementText = this.$el.find('#payment-agreement_text-container_content');
            this.$agreeCB = $('#payment-agreement_text-container_agree-cb');
            this.$fundingFeeContainer = $('#payment-agreement_text-container_fee-container');

            //reset checkbox
            this.$agreeCB.prop('checked', false);
            $('.checkbox-color').css('background-color', '#fcfcfc');
            $('.checkbox-text').css('color', 'transparent');

            //bind 'this' reference to functions in the view
            _.bindAll(this, 'render', 'determineAgreementText', 'displayButtons' , 'acceptAgreement', 'renderFeeBreakdown');

            //in the regression case just render
            if ( !qcssapp.Functions.checkServerVersion(2,0,26) ) {
                this.render();
                return;
            }

            //check for a fee, then render
            this.determineFundingFee();
        },

        //build agreement text based on payment type
        determineAgreementText: function() {
            var totalCharge = Number(this.fundingAmount).toFixed(2);

            if ( this.fundingFee != null ) {
                totalCharge = (Number(this.fundingAmount) + Number(this.fundingFee)).toFixed(2);
            }

            var totalChargeText = qcssapp.Functions.formatPriceInApp(totalCharge);
            var fundingAmountText = qcssapp.Functions.formatPriceInApp(this.fundingAmount);
            var reloadThresholdText = qcssapp.Functions.formatPriceInApp(this.options.reloadThreshold);

            if ( this.options.paymentType == 'oneTime' ) {
                this.agreementText = 'By pressing "I Agree" below, I agree that <b>' + totalChargeText + '</b> will be charged to my payment method on file and <b>'+fundingAmountText+'</b> will be loaded to my Quickcharge account';
            } else if ( this.options.paymentType == 'automatic' ) {
                this.agreementText = 'By pressing "I Agree" below, I agree that each time my Quickcharge account balance reaches <b>' + reloadThresholdText + '</b> or less, my payment method on file will be charged <b>' + totalChargeText + '</b> and my Quickcharge account will be funded by <b>'+fundingAmountText+'</b>';
            }
        },

        determineFundingFee: function() {
            var parameters = {
                agreementView: this
            };

            var endPoint = qcssapp.Location + '/api/funding/fee/'+ this.fundingAmount;

            qcssapp.Functions.callAPI(endPoint, 'GET', '', parameters,
                function(result, parameters) {
                    if ( !result ) {
                        return;
                    }

                    parameters.agreementView.fundingFee = result.fundingFee;
                    parameters.agreementView.fundingFeeDisclaimer = result.fundingFeeDisclaimer;
                    if(result.surchargeName == null || result.surchargeName == "") {
                        parameters.agreementView.fundingFeeLabel = result.fundingFeeLabel;
                    } else {
                        parameters.agreementView.fundingFeeLabel = result.surchargeName;
                    }
                    parameters.agreementView.render();
                }
            )
        },

        render: function() {
            this.renderFeeBreakdown();

            this.determineAgreementText();

            this.$agreementText.html(this.agreementText);

            $("#payment-agreement_button").empty();
            this.displayButtons();

            qcssapp.Functions.hideMask();
        },

        renderFeeBreakdown: function() {
            if ( this.fundingFee == null ) {
                this.$fundingFeeContainer.html('');
                return;
            }

            var options = {
                fundingTitle: 'Funding Amount',
                fundingFeeDisclaimer: this.fundingFeeDisclaimer,
                fundingFeeLabel: this.fundingFeeLabel,
                fundingAmount: Number(this.fundingAmount).toFixed(2),
                fundingTotalCharge: (Number(this.fundingAmount) + Number(this.fundingFee)).toFixed(2),
                fundingFee: Number(this.fundingFee).toFixed(2)
            };

            var fundingFeeView = new qcssapp.Views.FundingFee(options);

            var fundingFeeViewHTML = fundingFeeView.el;
            this.$fundingFeeContainer.html(fundingFeeViewHTML);
        },

        displayButtons: function() {
            //AGREE button declaration
            new qcssapp.Views.ButtonView({
                text: 'Continue',
                appendToID: '#payment-agreement_button',
                buttonType: 'customCB',
                class: 'button-flat template-gen accent-color-one',
                id: 'payment-agreement_button_agree',
                parentView: this,
                callback: function() {
                    if ( !this.parentView.$agreeCB.is(':checked') ) {
                        qcssapp.Functions.displayError('You must accept the agreement to continue!');
                        return;
                    }

                    qcssapp.Functions.showMask();

                    this.parentView.acceptAgreement();
                }
            });

            //DISAGREE button declaration
            new qcssapp.Views.ButtonView({
                text: 'Cancel',
                appendToID: '#payment-agreement_button',
                buttonType: 'customCB',
                class: 'button-flat template-gen accent-color-one',
                id: 'payment-agreement_button_disagree',
                parentView: this,
                callback: function() {
                    qcssapp.Router.navigate('account-funding', {trigger:true});
                }
            });
        },

        determineMinutesUntilFundingExecutor:function(callbackFn, callbackParams) {

            var endPoint = qcssapp.Location + '/api/funding/auto/minutes';

            qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
                function(result) {

                    if(result > 0) {
                        this.minutesUntilFunding = result;
                    }

                    callbackFn(callbackParams)
                }.bind(this)
            )
        },

        acceptAgreement: function() {
            var successParameters = {
                paymentType: this.paymentType,
                currentBalance: this.currentBalance,
                accountFundingView: this.accountFundingView
            };

            var callType = 'POST';
            var endPoint = this.endPoint;

            //build up successParameters based on type
            if ( this.paymentType == 'oneTime' ) {
                endPoint = endPoint + '/' + this.options.oneTimeAmount;
            } else {
                successParameters.successMessage = 'Automatic Reloads Enabled!';

                //updating, not enabling
                if ( endPoint.indexOf('enable') == -1 ) {
                    successParameters.successMessage = 'Automatic Reloads Updated!';
                }

                this.dataParameters = {
                    reloadAmount : Number(this.options.reloadAmount),
                    reloadThreshold : Number(this.options.reloadThreshold)
                };

                if(qcssapp.Functions.checkServerVersion(2,0)) {
                    var reloadPaymentMethodID = this.options.accountFundingView.accountPaymentMethodModel.get('id');
                    this.dataParameters['reloadPaymentMethodID'] = Number(reloadPaymentMethodID)
                }
            }

            qcssapp.Functions.callAPI(endPoint, callType, JSON.stringify(this.dataParameters), successParameters,
                function(response, successParameters) {

                    var callbackParams = {
                        response:response,
                        successParameters:successParameters
                    };

                    var callbackFn = function(params) {
                        var main = '';
                        var content = '';

                        if ( this.paymentType == 'oneTime') {
                            main = 'Successfully Added Funds!';
                            var amount = Math.abs(Number(params.response.fundedAmount));
                            amount = qcssapp.Functions.formatPriceInApp(amount);
                            content = '<b>' + amount + ' </b> has been added to your current Quickcharge balance.';

                            //update the current balance with the added funds
                            this.accountFundingView.displayCurrentBalance();
                        } else {
                            main = params.successParameters.successMessage;
                            var reloadAmountText = qcssapp.Functions.formatPriceInApp(params.response.reloadAmount);
                            var reloadThresholdText = qcssapp.Functions.formatPriceInApp(params.response.reloadThreshold);

                            //TODO: we can let the user know here if their automatic funding is going to trigger right away, if desired
                            content = 'Your Quickcharge account will be automatically funded by <b>' + reloadAmountText + ' </b> from your saved payment method whenever your account reaches <b>' + reloadThresholdText + '</b> or less until you disable automatic funding.';

                            if(this.minutesUntilFunding == 1) {
                                content += '<br/><br/>Please note this may take up to <b>' + this.minutesUntilFunding + ' </b>minute to complete. If you need funds immediately please use the "One-Time" Load feature on the Account Funding Page.';
                            } else if (this.minutesUntilFunding > 1) {
                                content += '<br/><br/>Please note this may take up to <b>' + this.minutesUntilFunding + ' </b>minutes to complete. If you need funds immediately please use the "One-Time Load" feature on the Account Funding Page.';
                            }

                            //update account funding view based on automatic reloads now being enabled
                            this.accountFundingView.handleAutomaticEnabled();
                        }

                        new qcssapp.Views.GeneralMsg({
                            title: 'Success',
                            main: main,
                            content: content
                        });

                        new qcssapp.Views.ButtonView({
                            text: "Continue",
                            buttonType: "customCB",
                            appendToID: "#general-msg-btn-container",
                            pageLinkID: "#main",
                            id: "messageContinueBtn",
                            class: "button-flat accent-color-one",
                            callback: function () {
                                qcssapp.Router.navigate('account-funding', {trigger:true});
                            }
                        });

                        qcssapp.Router.navigate('message', {trigger: true});

                    }.bind(this);

                    if(qcssapp.Functions.checkServerVersion(3,0,39)) {
                        this.determineMinutesUntilFundingExecutor(callbackFn, callbackParams);
                        return;
                    }

                    callbackFn(callbackParams);

                }.bind(this),
                '',
                function() {
                    qcssapp.Functions.hideMask();
                },
                '',
                30000
            );
        }
    });
})(qcssapp, jQuery);