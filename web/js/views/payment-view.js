;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.Payment - View for displaying payment page
     */
    qcssapp.Views.Payment = Backbone.View.extend({
        internalName: 'Payment View', //NOT A STANDARD BACKBONE PROPERTY

        el: '#payment', //references existing HTML element for - can be referenced as this.$el throughout the view

        //template: //template not needed for this view

        //events: //events not needed for this view

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log('initialize in payment view');
            }

            window.removeEventListener('message', this.handleFrameMessage);

            this.options = options || {};

            //bind 'this' reference to functions in the view
            _.bindAll(this, 'handleFrameMessage', 'loadFrame', 'unloadFrame');

            //view elements
            this.$frame = $('#payment_frame');
            //this.$frame.hide();

            //set event handler
            if(this.options.slideDownFlag) {
                this.$frame.hide();
                this.$frame.on('load', function() {
                    $(this).slideDown();
                    qcssapp.Functions.hideMask();
                });
            }  else { //if reloading the iframe after error occurs, don't slide down just show
                this.$frame.show();
            }
            //view properties
            this.paymentObject = {};
            this.paymentPageFileName = this.options.paymentPageFileName;
            this.goToAgreement = this.options.goToAgreement;

            this.sentMessage = false;
            window.addEventListener('message', this.handleFrameMessage, false);

            this.loadFrame();
        },

        loadFrame: function() {
            if ( this.paymentPageFileName == null || this.paymentPageFileName.length == 0 ) {
                qcssapp.Functions.throwFatalError('An error occurred attempting to load account funding, please try again.', 'main');
                return;
            }

            var cacheBust = new Date().getMilliseconds();

            //load iframe
            this.$frame
                .css('height', '700px')
                .attr('src', 'https://payments.mmhcloud.com/' + this.paymentPageFileName + '&time='+cacheBust);
                //.attr('src', 'http://192.168.244.128/myqc/payments_stripe_v3_test.html?APIKey=pk_test_xywyK8Pq4uMHy5k9YzfgcLKP&time='+cacheBust);
        },

        unloadFrame: function() {
            this.$frame.attr('src', '').slideUp();
        },

        handleFrameMessage: function(e) {
            if ( this.sentMessage ) {
                return;
            }

            this.sentMessage = true;
            window.removeEventListener('message', this.handleFrameMessage);

            if ( e.data && e.data.hasOwnProperty('paymentProcessorID') ) {
                this.paymentObject = e.data;

                if ( !e.data.hasOwnProperty('paymentMethodType') ) {
                    qcssapp.Functions.throwFatalError('An error occurred while attempting to save the Payment Method, please try again.', 'error');
                    return;
                }

                var paymentMethodTypeID = 0;

                switch ( e.data.paymentMethodType.toLowerCase() ) {
                    case ('visa'):
                        paymentMethodTypeID = 1;
                        break;
                    case ('american express'):
                        paymentMethodTypeID = 2;
                        break;
                    case ('mastercard'):
                        paymentMethodTypeID = 3;
                        break;
                    case ('discover'):
                        paymentMethodTypeID = 4;
                        break;
                    case ('jcb'):
                        paymentMethodTypeID = 5;
                        break;
                    case ('diners club'):
                        paymentMethodTypeID = 6;
                        break;
                }

                delete this.paymentObject.paymentMethodType;
                this.paymentObject.paymentMethodTypeID = paymentMethodTypeID;
                this.saveAccountPaymentMethodInfo();
                return;
            }

            //back to account funding
            this.unloadFrame();

            // Go to Order Details page if adding payment card on checkout
            if(qcssapp.CurrentOrder.storeModel) {
                qcssapp.Functions.buildReceivePage();
                return;
            }

            qcssapp.Router.navigate('account-funding', {trigger:true});
        },

        saveAccountPaymentMethodInfo: function() {
            var endPoint = qcssapp.Location + '/api/funding/account/payment/default/create';

            var successParameters = {
                paymentView:this,
                goToAgreement: this.goToAgreement,
                accountFundingView: this.options.accountFundingView
            };

            var errorParameters = {
                paymentView:this
            };

            qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(this.paymentObject), successParameters,
                function(response, successParameters) {
                    //check for valid response
                    if ( !response || !response.id || !qcssapp.Functions.isNumeric(response.id) ) {
                        qcssapp.Functions.displayError('An error occurred while attempting to save payment method, please try again.', 'Error');
                        return;
                    }

                    //unload and hide payment iframe
                    successParameters.paymentView.unloadFrame();
                    successParameters.paymentView.$frame.slideUp();
                    successParameters.paymentView.options.slideDownFlag = true;

                    // Go to Order Details page if adding payment card on checkout
                    if(qcssapp.CurrentOrder.storeModel) {
                        var paymentMethodCollection = [];
                        paymentMethodCollection.push(response);
                        qcssapp.CurrentOrder.storePaymentModel.set('paymentMethodCollection', paymentMethodCollection);

                        qcssapp.Functions.buildReceivePage();
                        return;
                    }

                    var paymentMethodModel = new qcssapp.Models.AccountPaymentMethod(response, {parse: true});

                    //builds and renders the payment method template with the given data
                    var paymentMethodView = new qcssapp.Views.PaymentMethod({
                        model: paymentMethodModel
                    });

                    //update on main payment page, in case of cancelling
                    var paymentMethodHTML = paymentMethodView.el;
                    successParameters.accountFundingView.$paymentMethodCurrentMethod.add(successParameters.accountFundingView.$agreementPaymentMethod).html(paymentMethodHTML);
                    successParameters.accountFundingView.$paymentMethod.removeClass('hidden').show();
                    successParameters.accountFundingView.hasPaymentMethod = true;
                    successParameters.accountFundingView.accountPaymentMethodModel = paymentMethodModel;
                    successParameters.accountFundingView.$el.find('.button').remove();
                    successParameters.accountFundingView.displayButtons();

                    qcssapp.Router.navigate('account-funding', {trigger:true});
                },
                function(errorParameters) {
                    //reload a new iframe when payment method is invalid
                    errorParameters.paymentView.options.slideDownFlag = false;
                    errorParameters.paymentView.initialize(errorParameters.paymentView.options);
                },
                "",
                errorParameters
            );
        }
    });
})(qcssapp, jQuery);