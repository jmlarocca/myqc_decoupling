;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.BalanceGroupLineView - View for displaying Balances Collections
     */
    qcssapp.Views.BalanceGroupLineView = Backbone.View.extend({
        //is a div
        tagName:  'div',
        className: 'tile tile-list balance-group-container',

        // Cache the template function for a single item.
        template: _.template( $('#balance-template').html() ),

        initialize: function() {
            //TODO: don't reference attributes directly
            this.stores = this.model.attributes.stores;

            //TODO: move storeCollection to balanceModel and pre-populate with stores
            this.storeCollection = new qcssapp.Collections.Stores();

            this.storeCollection.set(this.stores);

            return this;
        },

        render: function() {

            if(Number(this.model.get('balance')) < 0) {
                var balance = Number(this.model.get('balance')) * -1;
                this.model.set('balance', qcssapp.Functions.formatPriceInApp(balance, '-'));
            } else {
                this.model.set('balance', qcssapp.Functions.formatPriceInApp(this.model.get('balance')));
            }

            if(Number(this.model.get('avail')) < 0) {
                this.model.set('avail', qcssapp.Functions.formatPriceInApp(this.model.get('avail'), '-'));
            } else {
                this.model.set('avail', qcssapp.Functions.formatPriceInApp(this.model.get('avail')));
            }

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            return this;
        }
    });
})(qcssapp, jQuery);
