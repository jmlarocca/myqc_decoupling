;(function (qcssapp, $) {
    'use strict';

    /* qcssapp.Views.RedeemRewards - View for displaying Redeem Rewards functionality
     */
    qcssapp.Views.RedeemRewards = Backbone.View.extend({
        internalName: "Reward View", //NOT A STANDARD BACKBONE PROPERTY

        id: 'redeem-rewards',

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in reward view");
            }

            this.options = options || {};
            this.$automaticContainer = $('.redeem-rewards_automatic-container');
            this.$automaticList = this.$automaticContainer.find('.redeem-rewards_rewards-list');
            this.$automaticContainer.hide();
            this.$automaticList.html('');
            this.$manualContainer = $('.redeem-rewards_manual-container');
            this.$manualList = this.$manualContainer.find('.redeem-rewards_rewards-list');
            this.$manualList.html('');
            this.$manualContainer.hide();

            //collections and variables
            this.manualCollection = new qcssapp.Collections.Rewards();
            this.manualCollection.set(this.options.manualRewards);

            this.automaticCollection = new qcssapp.Collections.Rewards();

            //reset saved auto rewards
            qcssapp.CurrentOrder.automaticRewards = [];

            //if there are automatic rewards
            if ( this.options.automaticRewards && this.options.automaticRewards.length > 0 ) {
                this.automaticCollection.set(this.options.automaticRewards);
                for ( var reward in this.automaticCollection.models ) {
                    qcssapp.CurrentOrder.automaticRewards.push(this.automaticCollection.models[reward]);
                }

                //update manual container list title to reflect that there are automatically applied reward(s)
                this.$manualContainer.find('.redeem-rewards_page-title').text('Use Additional Rewards?');
            }

            this.loyaltyProgramCollection = new qcssapp.Collections.RewardsProgram();
            this.loyaltyProgramCollection.set(this.options.loyaltyPoints, {parse: true});
            this.counter = 0;

            //elements
            this.$counter = $('.redeem-rewards_counter');
            this.$buttonContainer = $('.redeem-rewards_button-container');
            this.$buttonContainer.html('');

            this.$counter.html(this.counter);

            this.continueConfirm = null;

            //reset selected rewards
            qcssapp.CurrentOrder.selectedRewards = [];

            //event handlers
            this.listenTo(this.manualCollection, 'change:quantity', this.handleRewardQuantityChange);

            _.bindAll(this, 'inquire', 'handleRewardQuantityChange', 'getSelectedRewards', 'updateCounter');

            this.render();
        },

        render: function() {
            //check for automatic rewards
            if ( this.automaticCollection.size() > 0 ) {
                //render all automatic rewards
                this.automaticCollection.each( function(mod) {
                    this.renderLine(mod, true);
                }, this );

                this.$automaticContainer.show();

                //since automatic rewards have quantity already, update the counter to reflect this
                this.updateCounter();
            }

            //check for manual rewards
            if ( this.manualCollection.size() > 0 ) {
                //render all manual rewards
                this.manualCollection.each( function(mod) {
                    this.renderLine(mod, false);
                }, this );

                this.$manualContainer.toggle(this.$manualList.find('li').length > 0);
            }

            //Continue button
            new qcssapp.Views.ButtonView({
                text: this.counter == 0 ? 'Skip' : 'Apply Rewards',
                buttonType: "customCB",
                appendToID: this.$buttonContainer,
                parentView: this,
                id: "redeemContinue",
                class: "align-center order-button prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                callback: function() {
                    var redeemView = this.parentView;
                    redeemView.continueToNextStep();
                }
            });

            //Back to cart button
            new qcssapp.Views.ButtonView({
                text: "BACK TO CART",
                buttonType: "customCB",
                appendToID: this.$buttonContainer,
                parentView: this,
                id: "cancelRedeem",
                class: "align-center order-button primary-button-font primary-border-color",
                callback: function() {
                    //pop off redeem
                    qcssapp.OrderMenuHistory.pop();

                    qcssapp.Router.navigate('cart', {trigger: true});
                }
            });
        },

        renderLine: function( mod, isAutomatic ) {
            //return if there are automatic rewards and this reward can't be used with other rewards
            if ( !isAutomatic && qcssapp.CurrentOrder.automaticRewards.length > 0 && !mod.get('canUseWithOtherRewards') ) {
                return;
            }

            //create new Redeem Reward Line View
            var redeemRewardView = new qcssapp.Views.RedeemRewardLine({
                model: mod,
                loyaltyProgramModel: this.loyaltyProgramCollection.get(mod.get('programID')),
                automatic: isAutomatic
            });

            //append to the appropriate list
            if ( isAutomatic ) {
                this.$automaticList.append(redeemRewardView.render().el);
            } else {
                //don't render anything if the manual reward is already maxed or
                if ( redeemRewardView.model.get('redeemable') == false ) {
                    return;
                }
                this.$manualList.append(redeemRewardView.render().el);
            }
        },

        handleRewardQuantityChange: function( rewardModel ) {
            //if this reward's quantity is 0, just update the counter and return
            if ( Number(rewardModel.get('quantity')) == 0 ) {
                this.updateCounter();
                return;
            }

            var redeemView = this;

            //if !canUseWithOtherRewards (can't use this reward with others), then must reduce quantity of all other transaction credit rewards to 0
            if ( !rewardModel.get('canUseWithOtherRewards') ) {
                //check for other selected rewards
                var otherSelectedRewards = this.manualCollection.filter(function(mod) { return mod != rewardModel && mod.get('quantity') > 0 });

                //if there are no other selected rewards, allow the quantity change, update the counter and return
                if ( !otherSelectedRewards || otherSelectedRewards.length == 0 ) {
                    this.inquire(rewardModel);
                    return;
                }

                //if there are other selected rewards, ask the user how they would like to proceed
                $.confirm({
                    title: '',
                    content: '<p style="text-align:center; font-size: 22px; line-height: 1.2em;">The reward <i>'+ rewardModel.get('name') +'</i> can\'t be used with any other rewards. <br /><br /> If you choose to continue attempting to select this reward, all other selected rewards will be removed.</p>',
                    buttons: {
                        confirm: {
                            text: 'Continue',
                            btnClass: 'qcss-button align-center order-button redeem-rewards-modal-remove-previous prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                            action: function () {
                                _.each(otherSelectedRewards, function (mod) {
                                    mod.set('quantity', 0)
                                });
                                redeemView.inquire(rewardModel);
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            btnClass: 'qcss-button align-center order-button redeem-rewards-modal-remove primary-button-font primary-border-color',
                            action: function () {
                                rewardModel.set('quantity', 0);
                            }
                        }
                    }
                });

                return;
            }

            //check the previously selected reward to see if it cannot be used with other rewards
            var previouslySelectedExclusiveReward = this.manualCollection.find(function(mod) { return mod.get('canUseWithOtherRewards') == false && mod.get('quantity') > 0 });
            if ( previouslySelectedExclusiveReward && previouslySelectedExclusiveReward.id ) {
                $.confirm({
                    title: '',
                    content: '<p style="text-align:center; font-size: 22px; line-height: 1.2em;">The previously selected reward <i>'+ previouslySelectedExclusiveReward.get('name') +'</i> can\'t be used with other rewards. <br /><br /> If you choose to continue attempting to select this reward, '+ previouslySelectedExclusiveReward.get('name') +'</i> reward will be removed.</p>',
                    buttons: {
                        confirm: {
                            text: 'Continue',
                            btnClass: 'qcss-button align-center order-button redeem-rewards-modal-remove-previous prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                            action: function () {
                                previouslySelectedExclusiveReward.set('quantity', 0);
                                redeemView.inquire(rewardModel);
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            btnClass: 'qcss-button align-center order-button redeem-rewards-modal-remove primary-button-font primary-border-color',
                            action: function () {
                                rewardModel.set('quantity', 0);
                            }
                        }
                    }
                });

                return;
            }

            this.inquire(rewardModel);
        },

        inquire: function(rewardModel, callback) {
            var redeemView = this;

            //create temporary order object to pass cart products to resource
            var orderObject = {
                "products": qcssapp.Functions.buildProductArray(),
                "rewards": qcssapp.Functions.buildRewardsArray(this.getSelectedRewards(), true)
            };

            if ( qcssapp.Functions.checkServerVersion(2,1) ) {
                orderObject.useMealPlanDiscount = qcssapp.CurrentOrder.useMealPlanDiscount;
            }

            if ( qcssapp.Functions.checkServerVersion(3,0,0) && qcssapp.CurrentOrder.combos.length > 0 ) {
                orderObject.combos = qcssapp.Functions.buildCombosArray()
            }

            var prevQty = Number(rewardModel._previousAttributes.quantity);

            qcssapp.Functions.orderInquiry(orderObject, function(data) {
                qcssapp.Functions.hideMask();
                var rewardValueAdjustmentConfirmationNecessary = false;
                var confirmationMessage = '';

                //check for over selection of transaction credit rewards
                var rewardMap = [];
                var rewardCount = 0;
                $.each(data.products, function(j) {
                    var productLine = data.products[j];
                    var rewardLines = productLine.rewards;

                    if ( rewardLines && rewardLines.length > 0 ) {
                        $.each(rewardLines, function(k) {
                            var rewardLine = rewardLines[k];
                            var reward = rewardLine.reward;
                            var rewardId = reward.id;
                            var rewardAmount = Math.abs(rewardLine.amount);

                            if ( !rewardMap[rewardId] ) {
                                reward.amount = rewardAmount;
                                reward.quantity = 1;
                                rewardMap[rewardId] = reward;
                            } else {
                                var existingMap = rewardMap[rewardId];
                                var existingAmount = existingMap.amount;
                                existingMap.amount = existingAmount + rewardAmount;
                                existingMap.quantity = existingMap.quantity + 1;
                            }

                            rewardCount++;
                        });
                    }
                });

                //one or more rewards could not be applied and were left out of the inquire response, need to deselect them
                var checkForUnappliedRewards = false;
                var sentRewardMap = [];
                if ( orderObject.rewards.length > rewardCount ) {
                    checkForUnappliedRewards = true;

                    //build sent reward map
                    $.each($.extend({}, orderObject.rewards), function() {
                        var reward = this.reward;

                        if ( reward.autoPayout ) {
                            return true;
                        }

                        var rewardId = reward.id;
                        var rewardAmount = Math.abs(this.amount);

                        if ( !sentRewardMap[rewardId] ) {
                            reward.amount = rewardAmount;
                            reward.quantity = 1;
                            sentRewardMap[rewardId] = reward;
                            reward.found = false;
                        } else {
                            var existingMap = sentRewardMap[rewardId];
                            var existingAmount = existingMap.amount;
                            existingMap.amount = existingAmount + rewardAmount;
                            existingMap.quantity = existingMap.quantity + 1;
                        }
                    });
                }

                //check reward values against reward amounts after the inquire assigns amounts
                var errorReward;
                for ( var rewardMapEntry in rewardMap ) {
                    if ( !rewardMapEntry || !rewardMap.hasOwnProperty(rewardMapEntry) || !rewardMap[rewardMapEntry] ) {
                        continue;
                    }

                    var reward = rewardMap[rewardMapEntry];

                    if ( checkForUnappliedRewards && sentRewardMap[rewardMapEntry] && sentRewardMap[rewardMapEntry].quantity == reward.quantity ) {
                        sentRewardMap[rewardMapEntry].found = true;
                    }

                    //if not transaction credit reward, just continue
                    if (reward.rewardTypeId != 2) {
                        continue;
                    }

                    var rewardName = reward.name;
                    var rewardValue = reward.value;
                    var rewardAmount = reward.amount;
                    //if the reward amount is less than the total value, need to warn the user
                    if ( Number(rewardAmount) == 0 ) {
                        rewardValueAdjustmentConfirmationNecessary = true;
                        confirmationMessage += 'Reward <b>' + rewardName + '</b> is not eligible for any amount for the current transaction, it will not be used.';
                    //use rewardAmount.toFixed to round, in a case such as a $1 credit spread over 3 products, the summed value will be 0.9999
                    } else if ( Number(rewardAmount.toFixed(2)) < Number(rewardValue) ) {
                        rewardValueAdjustmentConfirmationNecessary = true;
                        confirmationMessage += 'Reward <b>' + rewardName + '</b> cannot be fully applied. The available reward amount for the current transaction is '+ qcssapp.Functions.formatPriceInApp(rewardAmount) +'.';
                        errorReward = reward;
                    }
                }

                //if there are unapplied rewards, identify and remove their selection(s)
                if ( checkForUnappliedRewards ) {
                    var unappliedRewardIds = [];
                    for ( var sentRewardMapEntry in sentRewardMap ) {
                        if ( !sentRewardMapEntry || !sentRewardMap.hasOwnProperty(sentRewardMapEntry) || !sentRewardMap[sentRewardMapEntry] ) {
                            continue;
                        }

                        if ( sentRewardMap[sentRewardMapEntry].found == true ) {
                            continue;
                        }

                        unappliedRewardIds.push(sentRewardMap[sentRewardMapEntry].id);
                    }

                    if ( unappliedRewardIds.length ) {
                        for ( var unappliedRewardId in unappliedRewardIds ) {
                            if ( !unappliedRewardId || !unappliedRewardIds.hasOwnProperty(unappliedRewardId) || !unappliedRewardIds[unappliedRewardId] ) {
                                continue;
                            }

                            //set all unapplied reward's quantities to 0
                            var rewardToRemove = redeemView.manualCollection.findWhere({'id':unappliedRewardIds[unappliedRewardId]});

                            if ( rewardToRemove ) {
                                rewardToRemove.set('quantity', 0);
                            }
                        }

                        $.alert('Removed will not reduce the total amount due and will not be applied.');
                    }
                }

                //if the transaction credits will be able to fully apply (not considering discount) then just continue
                if ( !rewardValueAdjustmentConfirmationNecessary ) {
                    if ( callback ) {
                        callback();
                    }
                    redeemView.updateCounter();
                    return;
                }

                //ask user to confirm partial use of transacion credit reward
                redeemView.continueConfirm = $.confirm({
                    title: '',
                    content: '<div style="text-align: center;">' + confirmationMessage + '</div>',
                    buttons: {
                        confirm: {
                            text: 'Use Anyway',
                            btnClass: 'qcss-button align-center order-button redeem-rewards-modal-update prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                            action: function() {
                                if ( callback ) {
                                    callback();
                                }
                                redeemView.updateCounter();
                            }
                        },
                        cancel: {
                            text: 'Undo Last Action',
                            btnClass: 'qcss-button align-center order-button redeem-rewards-modal-remove primary-button-font primary-border-color',
                            action: function() {
                                rewardModel.set('quantity', prevQty);
                            }
                        }
                    }
                });
            },
            function(params, mmhErrorCode) {
                qcssapp.Functions.hideMask();
                if ( mmhErrorCode == "E7302" ) { //too many free product rewards
                    $.alert('You are attempting to apply too many free product rewards for this order. Your last action has been undone.');
                } else if ( mmhErrorCode == "E7303" ) { //invalid reward selection
                    $.alert('Invalid reward selection, certain products may not be eligible for this reward. Your last action has been undone.');
                }

                if ( rewardModel && params.hasOwnProperty('prevQty') ) {
                    rewardModel.set('quantity', params.prevQty);
                }
            },
            {
                rewardModel: rewardModel,
                redeemView: redeemView,
                prevQty: Number(rewardModel._previousAttributes.quantity)
            }, '', '', false, false, true);
        },

        getSelectedRewards: function() {
            //get the manually selected rewards
            var rewardsSelected = [];
            var manualRewards = this.manualCollection.filter(function(reward) {
                return Number(reward.get('quantity')) > 0;
            });

            for ( var reward in manualRewards ) {
                rewardsSelected.push(manualRewards[reward])
            }

            //create temporary order object to pass cart products to resource
            return rewardsSelected;
        },

        //counts and displays the number of Rewards currently chosen
        updateCounter: function() {
            //count the manual rewards
            var manualQuantities = this.manualCollection.pluck('quantity');
            var totalManualQuantity = Number(_.reduce(manualQuantities, function(i, j){ return i + j; }, 0));

            //count the automatic rewards
            var automaticQuantities = this.automaticCollection.pluck('quantity');
            var totalAutomaticQuantity = Number(_.reduce(automaticQuantities, function(i, j){ return i + j; }, 0));

            //update counter data and display
            this.counter = totalAutomaticQuantity + totalManualQuantity;

            // store the rewards as they are selected in qcssapp for later use
            qcssapp.CurrentOrder.rewards = [];
            for( var i = 0; i < manualQuantities.length; i++ ) {
                for( var j = 0; j < manualQuantities[i]; j++ ) {
                    qcssapp.CurrentOrder.rewards.push(this.manualCollection.models[i]);
                }
            }
            for( i = 0; i < automaticQuantities.length; i++ ) {
                for( j = 0; j < automaticQuantities[i]; j++ ) {
                    qcssapp.CurrentOrder.rewards.push(this.automaticCollection.models[i]);
                }
            }

            var $redeemContinue = $('#redeemContinue');
            if ( this.counter > 0 && $redeemContinue.text().toLowerCase() == 'skip' ) {
                $redeemContinue.text("Apply Rewards");
            } else if ( this.counter == 0 ) {
                $redeemContinue.text("Skip");
            }

            this.$counter.html(this.counter);
        },

        continueToNextStep: function() {
            var rewardsSelected = this.getSelectedRewards();

            qcssapp.CurrentOrder.selectedRewards = [];

            //set rewards on global accessor to access later
            $.each(rewardsSelected, function(i) {
                qcssapp.CurrentOrder.selectedRewards.push(rewardsSelected[i]);
            });

            //continue to receive page
            if ( this.continueConfirm ) {
                this.continueConfirm.close();
            }

            setTimeout(function() {
                qcssapp.Functions.showMask();
                qcssapp.Functions.buildReceivePage();
            }, 200)
        }
    });
})(qcssapp, jQuery);