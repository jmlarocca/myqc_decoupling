;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.AddAccountView - View for displaying accounts to manage
     */
    qcssapp.Views.AddAccountView = Backbone.View.extend({
        internalName: "Add Account to Manage View", //NOT A STANDARD BACKBONE PROPERTY

        id: 'add-page', //references existing HTML element

        template: _.template( $('#add-account-template').html() ),

        events: {
            'change #addAccountFirstName, #addAccountLastName, #addAccountMiddleInit, #addSchoolCode, #addAccountID': 'checkFields'
        },

        initialize: function () {
            if (qcssapp.DebugMode) {
                console.log("initialize in add account view");
            }

            this.$container = $('#add-account');

            this.$container.html(this.$el.html( this.template() ) );
            this.$container.attr('data-title', 'Add ' + qcssapp.Aliases.accountAlias);

            this.personID = qcssapp.personModel.get('personID');
            this.$relationshipSelector = $('#relationshipAccountSelect');

            this.inactiveMapping = false;
            this.newMapping = false;
            this.employeeID = "";
            this.foundEmployee = false;

            $('#addAccountFirstName').val("");
            $('#addAccountMiddleInit').val("");
            $('#addAccountLastName').val("");
            $('#addSchoolCode').val("");
            $('#addAccountID').val("");

            this.loadRelationships();
            this.displayButtons();
        },

        //appends each relationship status to the relationship selector
        renderLine: function( data ) {
            var that = this;

            $.each(data, function() {
                var accountRelationshipLine = new qcssapp.Views.AccountRelationshipLineView({
                    model:this
                });
                that.$relationshipSelector.append(accountRelationshipLine.render().el);
            });
        },

        //creates the Submit button in the view
        displayButtons: function() {
            if ( $('#submitAccountBtn').length ) {
                $('#submitAccountBtn').hide();
                return;
            }

            var that = this;
            new qcssapp.Views.ButtonView({
                text: "Search",
                buttonType: "customCB",
                appendToID: "#add-account-btn-container",
                id: "searchAccountBtn",
                class: "template-gen button-flat accent-color-one",
                callback: function() {
                    that.findMapping();
                }
            });

            new qcssapp.Views.ButtonView({
                text: "Submit",
                buttonType: "customCB",
                appendToID: "#add-account-btn-container",
                id: "submitAccountBtn",
                class: "align-center submit-account-button prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                callback: function() {
                    that.submitAccount();
                }
            });

            $('#submitAccountBtn').hide();
        },

        //load the relationship selector with relationship statuses
        loadRelationships: function() {
            if(this.$relationshipSelector.find('option').length) {
                return;
            }

            var endPoint = qcssapp.Location + '/api/account/person/relationships';

            var successParameters = { 'manageView': this };

            qcssapp.Functions.callAPI(endPoint, 'POST', '', successParameters,
                function(data) {
                    if(data) {
                        successParameters.manageView.renderLine(data);
                    }
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Unable to determine relationship types");
                    }
                    qcssapp.Functions.hideMask();
                }
            );

        },

        checkFields: function(e) {
            var $target = $(e.currentTarget);

            if ( $target.attr('id') == 'addSchoolCode') {
                var schoolCode = $target.val();

                if ( schoolCode.length != 3 || !(/^[0-9]+$/i.test(schoolCode)) ) {
                    $.alert('School Code must be 3 digits long and only contain numbers.');
                    $target.val('');
                    return;
                }

                if ( schoolCode == '186' ) {
                    $.alert('186 is not a valid school code. Please check the e-mail you received about My Quickcharge or with your school to determine the proper school code.');
                    $target.val('');
                    return;
                }
            }

            if(this.foundEmployee) {
                qcssapp.Functions.showMask();
                this.foundEmployee = false;
                $('#submitAccountBtn, #addAccountMsgContainer').hide();
                $('#searchAccountBtn').show();
                qcssapp.Functions.hideMask();
            }
        },

        //takes the First Name, Last Name and Student ID and checks if there is an employee that matches the info
        findMapping: function() {
            this.inactiveMapping = false;
            this.newMapping = false;

            if(this.validateFields()) {
                var endPoint = qcssapp.Location + '/api/account/person/mapping/find';

                var studentID = $('#addSchoolCode').val().trim() + "-" + $('#addAccountID').val().trim();

                var dataParameters = {
                    'firstName': qcssapp.Functions.cleanInput($('#addAccountFirstName').val()),
                    'lastName': qcssapp.Functions.cleanInput($('#addAccountLastName').val()),
                    'middleInitial': $('#addAccountMiddleInit').val().trim(),
                    'studentID': studentID,
                    'personID': this.personID
                };

                var successParameters = { 'addView': this };

                qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(dataParameters), successParameters,
                    function(data) {
                        if(data.result != "") {
                            if(data.result == "false") { //no employee found
                                $('#submitAccountBtn').hide();
                                $('#searchAccountBtn').show();
                                qcssapp.Functions.displayError('No account found for this Name, School Code and/or Student ID. Please try again.');

                            } else if (data.result == "gift-card") {
                                $('#submitAccountBtn').hide();
                                $('#searchAccountBtn').show();
                                qcssapp.Functions.displayError('Cannot add Gift Card accounts. Please try again.');

                            } else if (data.result == "true") {  //employee found but no mapping found
                                successParameters.addView.newMapping = true;
                                successParameters.addView.employeeID = data.employeeID;
                                successParameters.addView.foundEmployee = true;
                                successParameters.addView.submitAccount();

                            } else if (data.result == "inactiveMapping") {  //employee found and inactive mapping found
                                successParameters.addView.inactiveMapping = true;
                                successParameters.addView.employeeID = data.employeeID;
                                successParameters.addView.foundEmployee = true;
                                successParameters.addView.submitAccount();
                            } else if(data.result == "activeMapping") {  //employee found and active mapping found
                                qcssapp.Functions.displayError('You already manage this account, please enter a different Name, School Code and Student ID.');
                                $('#addAccountFirstName').val("");
                                $('#addAccountMiddleInit').val("");
                                $('#addAccountLastName').val("");
                                $('#addSchoolCode').val("");
                                $('#addAccountID').val("");
                                $('#submitAccountBtn').hide();
                                $('#searchAccountBtn').show();
                            }

                        } else {
                            qcssapp.Functions.displayError('No Accounts with this Name, School Code and/or Student ID found. Please try again.');
                            $('#submitAccountBtn').hide();
                            $('#searchAccountBtn').show();
                        }
                    },
                    function() {
                        if (qcssapp.DebugMode) {
                            console.log("Unable to find student account");
                        }
                        qcssapp.Functions.hideMask();
                    }
                );
            }
        },

        //creates a new student to person mapping or reactivates the found mapping
        submitAccount: function() {
            if(this.validateFields()) {
                var endPoint = qcssapp.Location + '/api/account/person/add';

                var $firstName = $('#addAccountFirstName');
                var $middleInit = $('#addAccountMiddleInit');
                var $lastName = $('#addAccountLastName');

                var name = $firstName.val() + ' ' + $lastName.val();
                if($middleInit.val() != "") {
                    name = $firstName.val() + ' ' + $middleInit.val() +' ' + $lastName.val();
                }

                var dataParameters = {
                    'employeeID': this.employeeID,
                    'personID': this.personID,
                    'name' : name,
                    'personRelationshipID' : this.$relationshipSelector.val(),
                    'inactiveMapping': this.inactiveMapping,
                    'newMapping': this.newMapping
                };

                var successParameters = { 'addView': this };

                qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(dataParameters), successParameters,
                    function(data) {
                        if(data == 1) {

                            //go to the Select Student page but don't select the new student automatically
                            var options = {'autoSelect': true};
                            new qcssapp.Views.ManageAccountView(options);
                            qcssapp.Router.navigate('manage-account', {trigger:true});

                        } else {
                            qcssapp.Functions.displayError('Could not add account. Please try again.');
                        }
                    },
                    function() {
                        if (qcssapp.DebugMode) {
                            console.log("Unable to find student account");
                        }
                        qcssapp.Functions.hideMask();
                    }
                );
            }
        },

        //checks if the First Name, Last Name and Student ID fields are properly filled out
        validateFields: function() {

            var $firstName = $('#addAccountFirstName');
            var $lastName = $('#addAccountLastName');
            var $schoolCode = $('#addSchoolCode');
            var $studentID = $('#addAccountID');
            var $relationship = $('#relationshipAccountSelect');

            if($firstName.val() == "") {
                qcssapp.Functions.displayError('Please provide a valid First Name.');
                return false;
            }

            if($lastName.val() == "") {
                qcssapp.Functions.displayError('Please provide a valid Last Name.');
                return false;
            }

            if($schoolCode.val() == "") {
                qcssapp.Functions.displayError('Please provide a valid School Code.');
                return false;
            }

            if($studentID.val() == "") {
                qcssapp.Functions.displayError('Please provide a valid Student ID.');
                return false;
            }

            if($relationship.val() == "0") {
                qcssapp.Functions.displayError('Please select a Relationship.');
                return false;
            }

            return true;
        }
    });
})(qcssapp, jQuery);   