;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FavoriteOrderLineView - View for displaying individual favorite order lines on the favorite order view
     */
    qcssapp.Views.FavoriteOrderLineView = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'favorite-order-line',

//        events: {
//            'click .line-content': 'goToProductPage'
//        },

        // Cache the template function for a single item.
        template: _.template( $('#favorite-order-line-template').html() ),

        initialize: function() {
            return this;
        },

        render: function() {

            //format total to two decimals (and leading zero if below 1)
            this.model.set('total', Number(Number(this.model.get('price')) * Number(this.model.get('quantity'))).toFixed(2));

            if(Number(this.model.get('total')) < 0) {
                var total = Number(this.model.get('total')) * -1;
                this.model.set('total', qcssapp.Functions.formatPriceInApp(total, '-'));
            } else {
                this.model.set('total', qcssapp.Functions.formatPriceInApp(this.model.get('total')));
            }

            //if quantity more than one then show equation
            if ( this.model.get('quantity') > 1 ) {
                this.model.set('equation',  this.model.get('quantity') + " <span class='quantity'>x</span> " + qcssapp.Functions.formatPriceInApp(this.model.get('price')));
            } else {
                this.model.set('equation', "&nbsp;");
            }

            //if product has modifiers, create modifiers list csv
            if ( this.model.get('modifiers') && this.model.get('modifiers').length > 0 ) {
                this.model.set('modifiersList', qcssapp.Functions.convertModifierArrayToCsv(this.model.get('modifiers')));
            } else {
                this.model.set('modifiersList', "");
            }

            this.$el.html( this.template( this.model.toJSON() ) );

            return this;
        }


    });
})(qcssapp, jQuery);
