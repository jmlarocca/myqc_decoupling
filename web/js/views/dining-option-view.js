(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.DiningOptionView - View for displaying spending profile options individually
     */
    qcssapp.Views.DiningOptionView = Backbone.View.extend({
        internalName: "Dining Option View", //NOT A STANDARD BACKBONE PROPERTY

        // Cache the template function for a single item.
        template: _.template( $('#dining-options-select-template').html() ),

        render: function() {
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.setElement( this.template( this.model ) );

            return this;
        }
    });
})(qcssapp, jQuery);
