;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ProductModNutritionLineView - View for displaying the nutrition line view on modifier lines
     */
    qcssapp.Views.ProductModNutritionLineView = Backbone.View.extend({
        internalName: "Product Modifier Nutrition Line View",

        // Cache the template function for a single item.
        template: _.template( $('#product-mod-detail-nutrition-template').html() ),

        //events: //events not needed for this view
        render: function() {

            this.setElement(this.template(this.model.toJSON()));

            return this;
        }
    });
})(qcssapp, jQuery);
