;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ManualAccountFundingView - View for manually funding accounts during online ordering
     */
    qcssapp.Views.ManualFundingView = Backbone.View.extend({
        internalName: "Manual Funding View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'manual-funding-container',

        template: _.template( $('#manual-funding-template').html() ),

        initialize: function (options) {
            this.options = options || {};

            this.$container = $(this.options.container);
            this.hidden = this.options.hidden;

            this.fundingFee = null;
            this.fundingFeeDisclaimer = null;
            this.fundingFeeLabel = null;
            this.fundingAmount = null;
            this.agreementText = '';

            this.userBalance = this.options.userBalance;
            this.accountFundingDetails = this.options.accountFundingDetailModel;
            this.orderTotal = this.options.total;
            this.customFundingAmount = this.options.customFundingAmount;

            this.oneTimeFundingAmountCollection = new qcssapp.Collections.FundingAmount();

            //_.bindAll( this,  'handleScanOption', 'handleLoginOption', 'handleCancelOption', 'handleAcctLogin', 'enterEventHandler', 'resetView', 'displayError' );

            this.render();
        },

        render: function() {

            var fundingMsg = "You do not have enough available funds to complete this transaction.<br>";
            fundingMsg += "You have <b>$" + Number(this.userBalance).toFixed(2) + "</b> available to spend at this location today and the transaction total is <b>$" + Number(this.orderTotal).toFixed(2) + "</b><br><br>";

            if(this.accountFundingDetails) {
                fundingMsg += "Please add funds to your account in order to complete the transaction.";
            } else if (qcssapp.Functions.isCreditCardAllowed()) {
                fundingMsg = "Your Quickcharge Balance of  <b>$" + Number(this.userBalance).toFixed(2) + "</b> is insufficient to allow you to complete this transaction for <b>$" + Number(this.orderTotal).toFixed(2) + "</b>.<br><br> Please add a payment card to either fund your account or to directly pay for your transaction.";
            } else {
                fundingMsg = "Your Quickcharge Balance of  <b>$" + Number(this.userBalance).toFixed(2) + "</b> is insufficient to allow you to complete this transaction for <b>$" + Number(this.orderTotal).toFixed(2) + "</b>.<br><br> Please add a payment card to fund your account.";
            }

            var templateData = {
                fundingMsg: fundingMsg
            };

            this.$el.html( this.template( templateData ) );

            if ( this.hidden ) {
                this.$el.addClass('hidden');
            }

            this.$errorMsg = this.$el.find('.manual-funding_error');
            this.$fundingMsg = this.$el.find('#manual-funding_msg');

            //add payment method container elements
            this.$addPaymentMethodTitle = this.$el.find('#manual-accountFunding_addPaymentMethod-title');
            this.$addPaymentMethod = this.$el.find('#manual-accountFunding_addPaymentMethod');

            //payment method container elements
            this.$paymentMethod = this.$el.find('#manual-accountFunding_paymentMethod');
            this.$paymentMethodCurrentMethod = this.$el.find('#manual-accountFunding_paymentMethod_saved-method');
            this.$agreementPaymentMethod = this.$el.find('#manual-payment-agreement_paymentMethod_saved-method');

            //one time container elements
            this.$oneTime = this.$el.find('#manual-accountFunding_fundOneTime');
            this.$oneTimeLabel = this.$el.find('#manual-accountFunding_fundOneTime_label');
            this.$oneTimeSelect = this.$el.find('#manual-accountFunding_fundOneTime_amount');
            this.$customFundingAmount = this.$el.find('#manual-accountFunding_fundOneTime_customAmount');

            //payment agreement container elements
            this.$agreementContainer = this.$el.find('#manual-payment-agreement_text-container');
            this.$agreementText = this.$el.find('#manual-payment-agreement_text-container_content');
            this.$agreeCB = this.$el.find('#manual-payment-agreement_text-container_agree-cb');
            this.$fundingFeeContainer = this.$el.find('#manual-payment-agreement_text-container_fee-container');

            //success container elements
            this.$successContainer = this.$el.find('#manual-payment-agreement_success-container');
            this.$successMsg = this.$el.find('#manual-general-msg-main');
            this.$successContent = this.$el.find('#manual-general-msg-content');

            //reset checkbox
            this.$agreeCB.prop('checked', false);
            $('.checkbox-color').css('background-color', '#fcfcfc');
            $('.checkbox-text').css('color', 'transparent');

            if ( this.accountFundingDetails && this.accountFundingDetails.hasOwnProperty('accountPaymentMethods') && this.accountFundingDetails.accountPaymentMethods.length > 0 ) {
                this.hasPaymentMethod = true;

                this.accountPaymentMethodModel = new qcssapp.Models.AccountPaymentMethod(this.accountFundingDetails.accountPaymentMethods[0], {parse:true});

                //builds and renders the payment method template with the given data
                var paymentMethodView = new qcssapp.Views.PaymentMethod({
                    model: this.accountPaymentMethodModel
                });

                this.$paymentMethodCurrentMethod.append(paymentMethodView.render().el);

                var paymentMethodHTML = paymentMethodView.el;
                this.$paymentMethodCurrentMethod.add(this.$agreementPaymentMethod).html(paymentMethodHTML);
            }

            //check amounts to determine whether to show One Time fields, Automatic fields, or the filter and One Time fields
            if ( this.accountFundingDetails && this.accountFundingDetails.hasOwnProperty('fundingAmounts') && this.accountFundingDetails.fundingAmounts.length > 0 ) {

                var groupedFundingAmounts = _.groupBy(this.accountFundingDetails.fundingAmounts, 'amountTypeID');
                var oneTimeFundingAmounts = groupedFundingAmounts[1];

                if ( oneTimeFundingAmounts && oneTimeFundingAmounts.length > 0 ) {
                    var view = this;

                    //populate the one-time select
                    $.each(oneTimeFundingAmounts, function() {
                        var fundingAmountModel = new qcssapp.Models.FundingAmount(this);
                        view.oneTimeFundingAmountCollection.add(fundingAmountModel);
                    });
                }

                this.oneTimeFundingAmountCollection.each(function( mod ) {
                    this.renderFundingAmount(mod, this.$oneTimeSelect);

                }, this );

                if ( oneTimeFundingAmounts && oneTimeFundingAmounts.length > 0 ) {
                    //there will be no selected -- only default for one time funding
                    var defaultOneTimeFundingAmountModel = _.findWhere(oneTimeFundingAmounts, {'defaulted': true});
                    if ( defaultOneTimeFundingAmountModel ) {
                        this.$oneTimeSelect.val(defaultOneTimeFundingAmountModel.amount);
                    }
                }
            }

            if(this.customFundingAmount != null) {
                var fundingModel = new qcssapp.Models.FundingAmount({
                    id: 1,
                    amountTypeID: 1,
                    amount: this.customFundingAmount,
                    defaulted:true,
                    selected:true
                });

                this.renderFundingAmount(fundingModel, this.$oneTimeSelect);

                var customFundingMsg = 'Do you want to fund your account the exact amount in order to complete the transaction?';
                this.$customFundingAmount.html(customFundingMsg);
                this.$customFundingAmount.show();
            }

            this.$container.append( this.$el );

            this.displayButtons();

            if( !this.accountFundingDetails ) {
                this.$paymentMethod.hide();
                this.$oneTime.hide();
                this.$addPaymentMethod.show();
                this.$addPaymentMethodTitle.show();
            }

            return this;
        },

        //handles the displaying of all buttons on the account funding view
        displayButtons: function() {

            // CANCEL button declaration
            new qcssapp.Views.ButtonView({
                text: 'Cancel',
                appendToID: '#manual-payment-method_add-button',
                buttonType: 'customCB',
                class: 'button-flat template-gen accent-color-one',
                id: 'manualCancelPaymentCardButton',
                parentView: this,
                callback: function() {
                    this.resetView();
                    this.fadeOut();

                }.bind(this)
            });


            // Add Card button declaration
            new qcssapp.Views.ButtonView({
                text: 'Add Card',
                appendToID: '#manual-payment-method_add-button',
                buttonType: 'customCB',
                class: 'button-flat template-gen accent-color-one',
                id: 'manualAddPaymentCardButton',
                parentView: this,
                callback: function() {
                    $('#paymentMethodSelect').val("3").trigger("change");

                    this.resetView();
                    this.fadeOut();

                }.bind(this)
            });

            // CANCEL button declaration
            new qcssapp.Views.ButtonView({
                text: 'Cancel',
                appendToID: '#manual-accountFunding_fundOneTime_button',
                buttonType: 'customCB',
                class: 'button-flat template-gen accent-color-one',
                id: 'manualOneTimeCancelButton',
                parentView: this,
                callback: function() {
                    this.resetView();
                    this.fadeOut();

                }.bind(this)
            });

            // Add Funds one time funding button
            new qcssapp.Views.ButtonView({
                text: "Add Funds",
                appendToID: "#manual-accountFunding_fundOneTime_button",
                buttonType: "customCB",
                class: "accent-color-one button-flat template-gen",
                id: "manualOneTimePaymentButton",
                parentView: this,
                callback: function() {
                    this.handleOneTimeButton();
                }.bind(this)
            });

            // CANCEL button declaration
            new qcssapp.Views.ButtonView({
                text: 'Cancel',
                appendToID: '#manual-payment-agreement_button',
                buttonType: 'customCB',
                class: 'button-flat template-gen accent-color-one',
                id: 'manual-payment-agreement_button_disagree',
                parentView: this,
                callback: function() {
                    this.resetView();
                    this.fadeOut();

                }.bind(this)
            });

            // AGREE button declaration
            new qcssapp.Views.ButtonView({
                text: 'Continue',
                appendToID: '#manual-payment-agreement_button',
                buttonType: 'customCB',
                class: 'button-flat template-gen accent-color-one',
                id: 'manual-payment-agreement_button_agree',
                callback: function() {
                    if ( !this.$agreeCB.is(':checked') ) {
                        qcssapp.Functions.displayError('You must accept the agreement to continue!');
                        return;
                    }

                    qcssapp.Functions.showModalMask();

                    this.acceptAgreement();

                }.bind(this)
            });
        },

        //Go to either the payments page or the agreement page depending on if there is a saved payment method
        handleOneTimeButton: function() {
            this.fundingAmount = this.$oneTimeSelect.val();

            if(this.fundingAmount == null || this.fundingAmount == "" || this.fundingAmount == " ") {
                $('.manual-funding_error').show().text('There was an issue funding the account. Please try again on the Account Funding page.');
                return;
            }

            this.$agreementContainer.removeClass('hidden');
            this.$oneTime.addClass('hidden');
            this.$fundingMsg.hide();

            this.determineFundingFee();
        },

        determineFundingFee: function() {
            var endPoint = qcssapp.Location + '/api/funding/fee/'+ this.fundingAmount;

            qcssapp.Functions.showModalMask();

            qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
                function(result) {
                    if ( !result ) {
                        qcssapp.Functions.hideModalMask();
                        return;
                    }

                    this.fundingFee = result.fundingFee;
                    this.fundingFeeDisclaimer = result.fundingFeeDisclaimer;
                    if(result.surchargeName == null || result.surchargeName == "") {
                        this.fundingFeeLabel = result.fundingFeeLabel;
                    } else {
                        this.fundingFeeLabel = result.surchargeName;
                    }
                    this.renderPaymentAgreement();

                }.bind(this),
                '', '', '', '', true
            )
        },

        renderPaymentAgreement: function() {
            this.renderFeeBreakdown();

            this.determineAgreementText();

            this.$agreementText.html(this.agreementText);

            if ( this.$el.find('button').length == 0 ) {
                this.displayButtons();
            }

            qcssapp.Functions.hideModalMask();
        },

        renderFeeBreakdown: function() {
            if ( this.fundingFee == null ) {
                this.$fundingFeeContainer.html('');
                return;
            }

            var options = {
                fundingTitle: 'Funding Amount',
                fundingFeeDisclaimer: this.fundingFeeDisclaimer,
                fundingFeeLabel: this.fundingFeeLabel,
                fundingAmount: Number(this.fundingAmount).toFixed(2),
                fundingTotalCharge: (Number(this.fundingAmount) + Number(this.fundingFee)).toFixed(2),
                fundingFee: Number(this.fundingFee).toFixed(2)
            };

            var fundingFeeView = new qcssapp.Views.FundingFee(options);

            var fundingFeeViewHTML = fundingFeeView.el;
            this.$fundingFeeContainer.html(fundingFeeViewHTML);
        },

        //build agreement text based on payment type
        determineAgreementText: function() {
            var totalCharge = Number(this.fundingAmount).toFixed(2);

            if ( this.fundingFee != null ) {
                totalCharge = (Number(this.fundingAmount) + Number(this.fundingFee)).toFixed(2);
            }

            this.agreementText = 'By pressing "I Agree" below, I agree that <b>$' + totalCharge + '</b> will be charged to my payment method on file and <b>$'+Number(this.fundingAmount).toFixed(2)+'</b> will be loaded to my Quickcharge account';
        },

        renderFundingAmount: function(mod, $selector) {
            var fundingAmountView = new qcssapp.Views.FundingAmount({
                model:mod
            });

            $selector.append(fundingAmountView.render().el);
        },

        acceptAgreement: function() {
            var oneTimeAmount = this.$oneTimeSelect.val();
            var endPoint =  qcssapp.Location + '/api/funding/fund/' + oneTimeAmount;

            qcssapp.Functions.callAPI(endPoint, 'POST', '', '',
                function(response) {
                    this.getAccountBalance(response['fundedAmount']);

                }.bind(this),
                '',
                function() {
                    qcssapp.Functions.hideModalMask();
                },
                '',
                30000,
                true
            );
        },

        getAccountBalance: function(fundedAmount) {
            var endPoint = qcssapp.Location + '/api/account/balance';

            //gets all details related to the account's balance and payment method
            qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
                function(response) {

                    var quickChargeBalance = response['userBalance'];

                    var main = 'Successfully Added Funds!';
                    var amount = Math.abs(Number(fundedAmount));
                    var content = '<b>$' + Number(amount).toFixed(2) + ' </b> has been added to your current Quickcharge balance. Your total balance is now <b>$' + Number(quickChargeBalance).toFixed(2) + ' </b>';

                    this.$successMsg.html(main);
                    this.$successContent.html(content);

                    new qcssapp.Views.ButtonView({
                        text: "Continue",
                        buttonType: "customCB",
                        appendToID: "#manual-general-msg-btn-container",
                        pageLinkID: "#main",
                        id: "manualMessageContinueBtn",
                        parentView:this,
                        class: "button-flat accent-color-one",
                        callback: function () {
                            this.parentView.resetView();
                            this.parentView.fadeOut();
                            $('#receiveNext').trigger('click');
                        }
                    });

                    this.$agreementContainer.addClass('hidden');
                    this.$paymentMethod.addClass('hidden');
                    this.$successContainer.removeClass('hidden');

                    qcssapp.Functions.hideModalMask();

                }.bind(this),
                function() {
                    //display error message to user
                    qcssapp.Functions.hideModalMask();
                }, '', '', '', true
            );
        },

        resetView: function() {
            this.resetFields();
        },

        resetFields: function() {
            this.fundingAmount = null;
            this.fundingFee = null;
            this.fundingFeeLabel = null;
            this.fundingFeeDisclaimer = null;
        },


        fadeIn: function(dur) {
            dur = !dur ? 150 : dur;

            this.hidden = false;
            this.$el.fadeIn(dur, function() {
                this.$el.removeClass('hidden');
            }.bind(this));
        },

        fadeOut: function(dur) {
            dur = !dur ? 150 : dur;

            this.hidden = false;
            this.$el.fadeOut(dur, function() {
                this.$el.addClass('hidden');
                this.remove();
            }.bind(this));
        },

        hide: function() {
            this.hidden = true;
            this.$el.hide();
        },

        displayError: function( errorMsg ) {
            this.$errorMsg.html( errorMsg );
            this.$errorMsg.slideDown();
        }


    });
})(qcssapp, jQuery);
