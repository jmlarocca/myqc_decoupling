;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.PaymentMethod - View for displaying payment method models
     */
    qcssapp.Views.PaymentMethod = Backbone.View.extend({
        tagName: 'div',
        className: 'payment-method',

        // Cache the template function for a single item.
        template: _.template( $('#payment-method-template').html() ),

        initialize: function(options) {
            //grab given options
            this.options = options || {};

            //get the model from the options
            this.model = this.options.model;

            //render right away
            this.render();
        },

        render: function() {
            //build payment method view with given options
            this.$el.html( this.template( this.model.attributes ));

            return this;
        }
    });
})(qcssapp, jQuery);
