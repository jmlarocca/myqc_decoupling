;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.RewardHeaderView - View for displaying Reward Header information
     */
    qcssapp.Views.RewardHeaderView = Backbone.View.extend({
        internalName: "Reward Header View", //NOT A STANDARD BACKBONE PROPERTY

        template: _.template( $('#program-rewards-header-template').html() ),

        className: 'rewards-header-view',

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in rewards header");
            }

            this.options = options || {};

            this.render();

            return this;
        },

        render: function() {
            this.$el.html( this.template( this.options ) );

            return this;
        }
    });
})(qcssapp, jQuery);