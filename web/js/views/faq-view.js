;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FAQ - View for displaying FAQs
     */
    qcssapp.Views.FAQ = Backbone.View.extend({
        internalName: "FAQ View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#faq-page', //references existing HTML element for order review - can be referenced as this.$el throughout the view

        template: _.template( $('#faq-template').html() ),

        events: {
           'input #faq-search-input': 'handleSearch',
           'click .faq-search-cancel': 'handleSearchCancel'
        },

        initialize: function ( options ) {

            this.options = options || {};
            this.loginFAQ = this.options.loginFAQ;
            this.faqID = this.options.faqID || '';

            if(this.loginFAQ) {
                this.collection = new qcssapp.Collections.FAQs();
            } else {
                this.collection = qcssapp.accountModel.get('FAQs');
            }

            this.$el.html( this.template( {} ) );

            this.$faqView = $('#faqview');
            this.$homeLink = this.$faqView.find('.home-link');
            this.$generalBackICon = this.$faqView.find('.general-back-icon');
            this.$faqHeader = this.$faqView.find('h1');

            this.$faqView.removeClass('funding-faq');

            this.$homeLink.show();
            this.$generalBackICon.hide();
            this.$faqHeader.text($('#nav-faq').text().trim());

            if( this.loginFAQ ) {
                this.$homeLink.hide();
                this.$generalBackICon.show();
                this.$faqHeader.text('FAQs');
            }

            if(this.faqID.length) {
                this.$homeLink.hide();
                this.$generalBackICon.show();
                this.$faqView.addClass('funding-faq');
            }

            this.loadCollection();
        },

        //fetch collection to be used in this view
        loadCollection: function() {
            if(!this.loginFAQ) {
                this.render();
                return;
            }

            var endPoint = qcssapp.Location + '/api/myqc/login/faq';

            qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
                function(response) {

                    this.collection.set(response);

                    this.render();

                }.bind(this)
            );
        },


        render: function() {
            this.$faqList = this.$el.find('.faq-list');

            if(this.collection.isEmpty()) {
                return;
            }

            this.collection.each(function( mod ) {
                this.renderLine( mod );
            }, this );

            if(!this.loginFAQ) {
                qcssapp.Functions.hideMask();
            }

            if(this.faqID.length) {
                this.$faqList.find('.faq-line_question-container[data-id="'+this.faqID+'"]').trigger('click');
            }
        },

        renderLine: function( mod ) {
            var faqLineView = new qcssapp.Views.FAQLine({
                model:mod
            });

            this.$faqList.append(faqLineView.render().el);
        },

        handleSearch: function() {
            var searchVal = this.$el.find('#faq-search-input').val().toLowerCase();
            var $faqLines = this.$faqList.find('.faq-line');

            if(searchVal == '') {
                this.$faqList.find('.faq-line').show();
                return;
            }

            $.each($faqLines, function(){
                var $line = $(this);
                var question = $line.find('.faq-line_question').text().toLocaleLowerCase();
                var answer = $line.find('.faq-line_answer').text().toLocaleLowerCase();

                if(question.indexOf(searchVal) != -1) {
                    $line.show();
                } else if(answer.indexOf(searchVal) != -1) {
                    $line.show();
                } else {
                    $line.hide();
                }

            });
        },

        handleSearchCancel: function() {
            this.$el.find('#faq-search-input').val('');
            this.$faqList.find('.faq-line').show();
        }
    });

})(qcssapp, jQuery);