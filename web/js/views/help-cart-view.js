;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.HelpCartView - View for displaying help info on the my cart page
     */
    qcssapp.Views.HelpCartView = Backbone.View.extend({
        internalName: "Help Cart View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'help-favorite-cart-view',

        template: _.template( $('#help-cart-template').html() ),

        events: {
            'click .help-next' : 'nextHelpSlide',
            'click .help-close' : 'closeHelp'
        },

        cartHelp: [
            {
                id: 1,
                title: 'Cart Items',
                text: 'Swipe right to add the item to the cart',
                buttonText: 'Next',
                showArrow: '',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 2,
                title: 'Cart Items',
                text: 'Swipe left to remove the item from the cart',
                buttonText: 'Next',
                showArrow: '',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 3,
                title: 'Cart Items',
                text: 'Click the product line to go to the product page',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }
        ],

        initialize: function (options) {
            this.options = options || {};

            this.parentView = this.options.parentView;

            this.$view = $('#cartview');
            this.$activeListID = '#cartList';

            this.slide = 2;
            this.slideEnd = 3;
        },

        render: function() {
            this.$el.html( this.template( {} ) );

            this.formatList();

            this.renderSlides();

            return this;
        },

        formatList: function() {

            var $cartItemLine = $(this.$activeListID) != undefined && $(this.$activeListID).find('.cart-line') != undefined && $(this.$activeListID).find('.cart-line').first() != undefined ? $(this.$activeListID).find('.cart-line').first() : undefined;
            var cartOffsetTop = $cartItemLine != undefined && $cartItemLine.offset() != undefined ? $cartItemLine.offset().top : 0;

            if(cartOffsetTop > 0) {
                this.$cartItemLine = $cartItemLine.clone();
                this.$cartItemLine.css('top', cartOffsetTop);

                this.$el.append(this.$cartItemLine);
            }

        },

        renderSlides: function() {
            $.each(this.cartHelp, function(index, slideInfo) {

                var helpSlideView = new qcssapp.Views.HelpSlideView(slideInfo);

                this.$el.append( helpSlideView.render().$el.find('.help-slide') );

            }.bind(this));
        },

        nextHelpSlide: function() {
            //if at the end of the slide length
            if( this.slide == this.slideEnd ) {
                this.parentView.fadeOut();
                return;
            }

            var $currentSlide = this.$el.find('.help-slide[data-slide-id="'+this.slide+'"]');
            $currentSlide.removeClass('active-slide').addClass('inactive-slide');

            this.slide++;

            var $nextSlide = this.$el.find('.help-slide[data-slide-id="'+this.slide+'"]');
            $nextSlide.addClass('active-slide');

            //if the next slide is the last, change button text to Done
            if( this.slide == this.slideEnd ) {
                $nextSlide.find('.help-next').text('Done');
            }
        },

        setSlideStyles: function() {

            // Slide 1
            var $helpInfoSlide1 = $('.help-slide[data-slide-id="1"] .help-info');
            var $helpHandSlide1 = $('.help-slide[data-slide-id="1"] .help-hand');
            var $helpArrowSlide1 = $('.help-slide[data-slide-id="1"] .help-arrow');

            if(this.$cartItemLine == null) {
                $helpHandSlide1.hide();
                $helpArrowSlide1.hide();
                $helpInfoSlide1.addClass('no-favorite-cart-help');
                $helpInfoSlide1.find('.help-text').text(this.cartHelp[0].text);

            } else {
                this.setHelpArrowTop($helpArrowSlide1, this.$cartItemLine);
                qcssapp.Functions.setHandTop($helpArrowSlide1, $helpHandSlide1, 25);
                qcssapp.Functions.setHelpInfoTop($helpHandSlide1, $helpInfoSlide1, 25);
            }

            // Slide 2
            var $helpInfoSlide2 = $('.help-slide[data-slide-id="2"] .help-info');
            var $helpHandSlide2 = $('.help-slide[data-slide-id="2"] .help-hand');
            var $helpArrowSlide2 = $('.help-slide[data-slide-id="2"] .help-arrow');

            if(this.$cartItemLine == null) {
                $helpHandSlide2.hide();
                $helpArrowSlide2.hide();
                $helpInfoSlide2.addClass('no-favorite-cart-help');
                $helpInfoSlide2.find('.help-text').text(this.cartHelp[1].text);


            } else {
                this.setHelpArrowTop($helpArrowSlide2, this.$cartItemLine);
                qcssapp.Functions.setHandTop($helpArrowSlide1, $helpHandSlide2, 25);
                qcssapp.Functions.setHelpInfoTop($helpHandSlide2, $helpInfoSlide2, 25);
            }

            // Slide 3
            var $helpInfoSlide3 = $('.help-slide[data-slide-id="3"] .help-info');
            var $helpHandSlide3 = $('.help-slide[data-slide-id="3"] .help-hand');

            if(this.$cartItemLine == null) {
                $helpHandSlide3.hide();
                $helpInfoSlide3.addClass('no-favorite-cart-help');
                $helpInfoSlide3.find('.help-text').text(this.cartHelp[2].text);

            } else {
                qcssapp.Functions.setHandTop(this.$cartItemLine, $helpHandSlide3, 40);
                qcssapp.Functions.setHelpInfoTop($helpHandSlide3, $helpInfoSlide3, 5);
            }

            this.hideInactiveSlides();
        },

        setHelpArrowTop: function($helpArrow, $helpLine) {
            var cartLineOffsetTop = $helpLine.length ? $helpLine.css('top') : 0;
            cartLineOffsetTop = Number(cartLineOffsetTop.replace('px', ''));
            var cartLineHeight = $helpLine.length ? $helpLine[0].clientHeight : 0;

            var helpArrow1Height = $helpArrow.find('.help-arrow-point').css('height');
            helpArrow1Height = Number(helpArrow1Height.replace('px', ''));

            $helpArrow.css('top', cartLineOffsetTop + Number(Number(cartLineHeight - helpArrow1Height) / 2));
        },

        hideInactiveSlides: function() {
            this.$el.find('.help-slide[data-slide-id="'+this.slide+'"]').addClass('active-slide');
            this.$el.find('.help-slide[data-slide-id!="'+this.slide+'"]').addClass('inactive-slide');
        },

        closeHelp: function() {
            this.parentView.fadeOut();
        }
    });
})(qcssapp, jQuery);
