;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    //View for displaying help info on the receipt page
    qcssapp.Views.HelpReceiptView = Backbone.View.extend({
        internalName: "Help Receipt View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'help-receipt-view',

        template: _.template( $('#help-receipt-template').html() ),

        events: {
            'click .help-next' : 'nextHelpSlide',
            'click .help-close' : 'closeHelp'
        },

        receiptHelp: [
            {
                id: 1,
                title: 'Favorite Order',
                text: 'Click here to favorite this transaction and give it a name',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 2,
                title: 'Add to Cart',
                text: 'Click here to add all products in the transaction to the Cart',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 3,
                title: 'Express Reorder',
                text: 'Click here to Express Reorder the products in this transaction',
                buttonText: 'Done',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }
        ],

        initialize: function (options) {
            this.options = options || {};
            this.parentView = this.options.parentView;
            this.$view = $('#receipt-view');
            this.slideEnd = this.receiptHelp.length;

            //Defect 3915: If there are no favorites, we need to start on slide 2
            this.options.hasFaves = $('.favorite-order-icon').is(':visible');
            if(this.options.hasFaves)   { this.slideStart = 1; }
            else                        { this.slideStart = 2; }
        },

        render: function() {
            this.$el.html( this.template( {} ) );

            this.renderSlides();

            return this;
        },

        renderSlides: function() {
            $.each(this.receiptHelp, function(index, slideInfo) {
                var helpSlideView = new qcssapp.Views.HelpSlideView(slideInfo);

                this.$el.append( helpSlideView.render().$el.find('.help-slide') );

            }.bind(this));

            var parentReceiptIcons = this.$view.find('#receipt-order-icons');
            this.$receiptIcons = parentReceiptIcons.clone();
            this.$receiptIcons.css({'width': parentReceiptIcons[0].offsetWidth, 'top': parentReceiptIcons.offset().top + $("#receipt").get(0).scrollTop, 'left': parentReceiptIcons[0].offsetLeft});
            this.$receiptIcons.attr('id', 'receipt-order-icons-dupe');

            this.$el.append(this.$receiptIcons);

            this.$favoriteIcon = this.$receiptIcons.find('.favorite-order-icon');
            this.$addToCartIcon = this.$receiptIcons.find('.add-to-cart-icon');
            this.$expressReorderIcon = this.$receiptIcons.find('.express-order-icon');

            this.showPageContent();
        },

        nextHelpSlide: function() {
            //if at the end of the slide length
            if( this.slideStart == this.slideEnd ) {
                this.parentView.fadeOut();
                return;
            }

            var $currentSlide = this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]');
            $currentSlide.removeClass('active-slide');

            this.slideStart++;

            var $nextSlide = this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]');
            $nextSlide.addClass('active-slide');

            this.showPageContent();
            this.hideInactiveSlides();
        },

        showPageContent: function() {
            switch(this.slideStart) {
                case 1:
                    this.$favoriteIcon.addClass('receipt-help-icon');
                    break;
                case 2:
                    this.$favoriteIcon.removeClass('receipt-help-icon');
                    this.$addToCartIcon.addClass('receipt-help-icon');
                    break;
                case 3:
                    this.$favoriteIcon.removeClass('receipt-help-icon');
                    this.$addToCartIcon.removeClass('receipt-help-icon');
                    this.$expressReorderIcon.addClass('receipt-help-icon');
                    break;
                default:
                    break;
            }
        },

        setSlideStyles: function() {
            // Slide 1
            var $helpHandSlide1 = $('.help-slide[data-slide-id="1"] .help-hand');
            var $helpInfoSlide1 = $('.help-slide[data-slide-id="1"] .help-info');
            qcssapp.Functions.setHandTopSimple(this.$favoriteIcon, $helpHandSlide1, 20);
            qcssapp.Functions.setHelpInfoTop($helpHandSlide1, $helpInfoSlide1);

            // Slide 2
            var $helpHandSlide2 = $('.help-slide[data-slide-id="2"] .help-hand');
            var $helpInfoSlide2 = $('.help-slide[data-slide-id="2"] .help-info');
            qcssapp.Functions.setHandTopSimple(this.$addToCartIcon, $helpHandSlide2, 30);
            qcssapp.Functions.setHelpInfoTop($helpHandSlide2, $helpInfoSlide2);

            // Slide 3
            var $helpHandSlide3 = $('.help-slide[data-slide-id="3"] .help-hand');
            var $helpInfoSlide3 = $('.help-slide[data-slide-id="3"] .help-info');
            qcssapp.Functions.setHandTopSimple(this.$expressReorderIcon, $helpHandSlide3, 30);
            qcssapp.Functions.setHelpInfoTop($helpHandSlide3, $helpInfoSlide3);

            //Defect 3915: Without favorites, positions for slide2 and slide3 need to be adjusted
            if(!this.options.hasFaves) {
                $helpHandSlide2.css('left', '25%'); $helpInfoSlide2.css('left', '2%');
                $helpHandSlide3.css('left', '64%'); $helpInfoSlide3.css('left', '28%');
            }

            this.hideInactiveSlides();
        },

        setIcon: function($parentIcon, $icon) {
            var parentIconTop = $parentIcon.offset().top;
            var parentIconLeft = $parentIcon[0].offsetLeft;

            if(typeof parentIconLeft === 'undefined') {
                parentIconLeft = $parentIcon.offset().left;
            }

            $icon.css({'top': parentIconTop, 'left': parentIconLeft});
        },

        hideInactiveSlides: function() {
            if(this.slideStart != 0) {
                this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]').addClass('active-slide');
                this.$el.find('.help-slide[data-slide-id!="'+this.slideStart+'"]').addClass('inactive-slide');
            }
        },

        closeHelp: function() {
            this.parentView.fadeOut();
        }
    });
})(qcssapp, jQuery);
