;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.SpendingLimitView - View for displaying the global balance view
     */
    qcssapp.Views.NutritionCartView = Backbone.View.extend({
        internalName: "Nutrition Cart View",

        // Cache the template function for a single item.
        template: _.template( $('#nutrition-cart-template').html() ),

        //events: //events not needed for this view

        render: function() {
            this.setElement( this.template( this.model ) );
            return this;
        }
    });
})(qcssapp, jQuery);
