;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FAQLine - View for displaying FAQ Lines
     */
    qcssapp.Views.FAQLine = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'faq-line',

        // Cache the template function for a single item.
        template: _.template( $('#faq-line-template').html() ),

        events: {
            'click .faq-line_question-container': 'handleExpand'
        },

        initialize: function() {
            return this;
        },

        render: function() {

            this.model.set('question', qcssapp.Functions.htmlDecode(this.model.get('question')));
            this.model.set('answer', qcssapp.Functions.htmlDecode(this.model.get('answer')));

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            return this;
        },

        handleExpand: function() {
            var $answerContainer = this.$el.find('.faq-line_answer-container');
            var $faqList = $('.faq-list');

            if($answerContainer.css('display') == 'block') {
                $answerContainer.slideUp();
                this.$el.removeClass('expand');

            } else {
                $faqList.find('.faq-line_answer-container').slideUp();
                $faqList.find('.faq-line').removeClass('expand');

                $answerContainer.slideDown();
                this.$el.addClass('expand');
            }
        }
    });
})(qcssapp, jQuery);
