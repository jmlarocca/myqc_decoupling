;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.CartView - View for displaying Cart view
     */
    qcssapp.Views.CartView = Backbone.View.extend({
        internalName: "Cart View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#cartList', //references existing HTML element for Cart - can be referenced as this.$el throughout the view

        //template: _.template( $('#').html() ), //not needed for this view

        events: {
            "click .cart-line": "editProduct",
            "click .cart-remove": "removeProduct"
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in cart view");
            }

            this.options = options || '';

            this.slickButtonFraction = 4;

            //prevent buttons from being displayed more than once
            if ( !$("#cartButtonContainer").hasClass('initialized') ) {
                this.displayButtons();
            }

//            _.bindAll(this);

            $('#show-cart').attr('data-title', 'My Order - '+qcssapp.Functions.htmlDecode(qcssapp.CurrentOrder.storeModel.get('header1')));

            this.$cartContainer = $('.cart-nutrition-container');

            this.render();
        },

        //creates cart product lines in cartview
        render: function() {
            //remove old cart lines
            this.$el.children().remove();

            var $keepShopping = $('#keepShoppingButton');
            var $checkOut = $('#checkOutButton');

            if ( qcssapp.CurrentOrder.products && qcssapp.CurrentOrder.products[0] && qcssapp.CurrentOrder.products[0].get('diningOptionProduct') ) {
                var shouldRenumberIDS = false;
                if(qcssapp.CurrentOrder.products[0].get('id')==0){shouldRenumberIDS=true;}
                qcssapp.CurrentOrder.products = qcssapp.CurrentOrder.products.slice(1);

                //Defect 3883: uncommented this and put in a proper conditional to handle returning to cart from express order page. Fixes issue where
                //products would be removed from cart in error.
                if(shouldRenumberIDS==true){
                    $.each(qcssapp.CurrentOrder.products, function() {
                        if((Number(this.get('id')) - 1) >= 0) {
                            this.set('id', (Number(this.get('id')) - 1));
                        }
                    });
                }
            }

            qcssapp.Functions.updateNumberOfProductsInCart();

            if ( qcssapp.CurrentOrder.products.length == 0 ) {
                this.$el.append('<li class="cart-line cart-empty">Your cart is empty!</li>');
                $checkOut.hide();
                $keepShopping.removeClass('primary-button-font');
                $keepShopping.addClass('primary-gradient-color font-color-primary prominent-button');
                $('.cart-nutrition-container').hide();
                qcssapp.Functions.hideMask();
                return;
            }

            //create cart collection from the currentOrder objects
            this.cartCollection = new qcssapp.Collections.Cart();

            var cartCollectionArray = [];

            $.each(qcssapp.CurrentOrder.products, function() {
                if(this.get("fixedTareWeight")!= undefined && this.get("fixedTareWeight") != "" &&
                    this.get("tareName") != undefined && this.get("tareName")!= ""){
                    this.set("grossWeightInfo","<div>Gross Weight: " +(Number(this.get("quantity")) + Number(this.get("fixedTareWeight")) ).toFixed(2)+" lbs</div>");
                    this.set("tareInfo","<div>"+ this.get("tareName") +": "+ this.get("fixedTareWeight") +" lbs</div>");
                }
                var cartLineObj = {
                    id: this.get('id'),
                    cartProductID: this.get('productID'),
                    name: this.get('name'),
                    price: this.get('price'),
                    originalPrice: this.get('originalPrice'),
                    basePrice: this.get('basePrice'),
                    quantity: this.get('quantity'),
                    total: Number(this.get('price')) * Number(this.get('quantity')),
                    modifiers: this.get('modifiers'),
                    modifiersList: this.get('modifiersList'),
                    comboModel: this.get('comboModel'),
                    comboTransLineItemId: this.get('comboTransLineItemId'),
                    upcharge: this.get('upcharge'),
                    healthy: this.get('healthy'),
                    vegetarian: this.get('vegetarian'),
                    vegan: this.get('vegan'),
                    glutenFree: this.get('glutenFree'),
                    scaleUsed: this.get('scaleUsed'),
                    tareName: this.get("tareName"),
                    fixedTareWeight:this.get("fixedTareWeight"),
                    tareInfo:this.get("tareInfo"),
                    grossWeightInfo:this.get("grossWeightInfo"),
                    grossWeight:this.get("grossWeight"),
                    prepOption: this.get('prepOption')
                };

                cartCollectionArray.push(cartLineObj);
            });

            this.cartCollection.reset(cartCollectionArray);

            var subtotal = 0;
            var comboInProgressID = "";

            this.cartCollection.each(function( mod ) {
                //calculate the total
                if(mod.get('scaleUsed')){
                    var modTotal = Number(mod.get('price') - mod.get('originalPrice'));
                    var total = Number(Math.round(((mod.get('quantity') * mod.get('originalPrice')) + modTotal)+"e"+2)+"e-"+2).toFixed(2);
                }
                else{
                    var total = Number(Math.round((mod.get('quantity') * mod.get('price'))+"e"+2)+"e-"+2).toFixed(2);
                }

                mod.set('total', total);

                //if the product does not contain a comboModel then add product line
                if( mod.get('comboTransLineItemId') == "" || mod.get('comboTransLineItemId') == null ) {
                    this.renderLine( mod , false );
                    subtotal += Number(mod.get('total'));
                    comboInProgressID = "";

                //if the combo still needs to list out details, add product line as combo product line
                } else if (comboInProgressID == mod.get('comboTransLineItemId')) {
                    this.renderLine( mod, true );
                    subtotal += Number(mod.get('total'));

                //otherwise adding a new combo so add the title combo line first then a line for the product
                } else {
                    this.renderComboLine( mod );
                    subtotal += Number(mod.get('comboTotal'));
                    this.renderLine( mod, true );
                    subtotal += Number(mod.get('total'));
                    comboInProgressID = mod.get('comboTransLineItemId');
                }

            }, this );

            //calculate and add subtotal,
            this.$el.append('<li class="cart-line subtotal-line">Subtotal:<span class="subtotal accent-color-one">'+ qcssapp.Functions.formatPrice(Number(subtotal)) +'</span></li>');

            $checkOut.show();
            $keepShopping.removeClass('primary-gradient-color font-color-primary prominent-button');
            $keepShopping.addClass('primary-button-font');

            this.createNutritionCartTable();
        },

        renderLine: function( mod, isCombo ) {
            mod.set('total', Number( mod.get('total') ).toFixed(2) );

            if(isCombo) {
               mod.set('isCombo', true);
            }

            var cartLineView = new qcssapp.Views.cartLineView({
                model:mod
            });

            this.$el.append(cartLineView.render().el);

            this.createSlickLine(cartLineView);
        },

        renderComboLine: function( mod ) {

            var comboLineView = new qcssapp.Views.comboLineView({
                model:mod
            });

            this.$el.append(comboLineView.render().el);

            this.createSlickLine(comboLineView);
        },

        displayButtons: function() {
            //create buttons
            new qcssapp.Views.ButtonView({
                text: "CHECK OUT",
                buttonType: "customCB",
                appendToID: "#cartButtonContainer",
                id: "checkOutButton",
                parentView: this,
                class: "align-center order-button prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                callback: function() {
                    this.parentView.checkOut();
                }
            });

            new qcssapp.Views.ButtonView({
                text: "KEEP SHOPPING",
                buttonType: "customCB",
                appendToID: "#cartButtonContainer",
                id: "keepShoppingButton",
                class: "align-center order-button primary-button-font primary-border-color",
                callback: function() {
                    //grab the first record in history record
                    var storeKeypad = qcssapp.OrderMenuHistory[0];

                    qcssapp.Functions.loadKeypad(storeKeypad.id);
                }
            });

            new qcssapp.Views.ButtonView({
                text: "CANCEL ORDER",
                buttonType: "customCB",
                appendToID: "#cartButtonContainer",
                id: "cartCancelOrderButton",
                class: "align-center order-button primary-button-font primary-border-color",
                callback: function() {
                    qcssapp.Functions.resetOrdering(true);
                }
            });

            $('#cartButtonContainer').addClass('initialized');
        },

        checkOut: function() {

            if(qcssapp.Functions.checkServerVersion(4,0)) {

                //Defect 4127 -> Related to Defect 3895
                //New Functionality: If the user clicks checkout, clears combo related menu history as it's no longer necessary (user can always edit combos by clicking them)
                //and it was prone to error if returned to via the back button
                for(var i=0; i<qcssapp.OrderMenuHistory.length;i++){
                    var cleanOrderMenuHistory = [];
                    if(qcssapp.OrderMenuHistory[i].type!="combo" || qcssapp.OrderMenuHistory[i].type!="combo-keypad"){
                        cleanOrderMenuHistory.push(qcssapp.OrderMenuHistory[i]);
                    }
                    qcssapp.OrderMenuHistory = cleanOrderMenuHistory;
                }
                //get the valid keypads to show from the upsell profile
                var suggestiveKeypads = qcssapp.Functions.checkUpsellProfileForKeypads();

                //if valid keypads, show suggestive selling view
                if(suggestiveKeypads.length > 0) {
                    qcssapp.Functions.showMask();

                    qcssapp.CurrentOrder.suggestedKeypadIDs.push(suggestiveKeypads[0].id);

                    qcssapp.Functions.addHistoryRecord(0, 'suggestive');

                    //create new Suggestive Selling view
                    qcssapp.SuggestionView = new qcssapp.Views.SuggestiveSelling(suggestiveKeypads);

                    //navigate to new suggestive selling view
                    qcssapp.Router.navigate('suggestive-selling', {trigger: true});

                    qcssapp.Functions.hideMask();
                    return;
                }
            }

            //prevent checking order for rewards if the server is not yet up to 1.5
            if ( !qcssapp.Functions.checkServerVersion(1,5) ) {
                qcssapp.Functions.buildReceivePage();
                return;
            }

            //abstracted rewards and discount functions for express reorders to use as well
            qcssapp.Functions.checkForRewardsAndDiscounts();
        },


        editProduct: function(e) {
            var $target = $(e.currentTarget);

            //don't navigate anywhere if clicking on line with the name of a combo
            if($target.attr('data-cartcomboid')) {
                this.buildComboView($target.attr('data-cartcomboid'));
                return;
            }

            var cartProductID = Number($target.attr('data-cartProductID'));

            // store this to properly update cart items, works in tandem with
            // product-detail-view.updateOrder
            qcssapp.SelectedProductID = cartProductID;

            if ( qcssapp.DebugMode ) {
                console.log(qcssapp.CurrentOrder);
                console.log("Editing product with cartProductID:" + cartProductID);
                console.log(qcssapp.CurrentOrder.products[cartProductID]);
            }

            var index = qcssapp.Functions.getProductCartIndexByID(cartProductID);

            if(qcssapp.CurrentOrder.products.length > 0) {
                var productModel = qcssapp.CurrentOrder.products[index];

                // If the product has modifiers, but it doesn't have modifier menus - probably came from express reorder where the menus aren't loaded
                if(productModel.get('modifiers') && productModel.get('modifiers').length > 0 && !(productModel.get('modifierMenus') && productModel.get('modifierMenus').length > 0)) {
                    qcssapp.Functions.reloadProductFromCart(productModel);
                    return;
                }

                qcssapp.Functions.loadProductFromCart(productModel);
            }
        },

        removeProduct: function(e) {
            var $target = $(e.currentTarget);
            if ($target.attr("data-cartProductID")) { // The line is a cart item, not a combo title
                var cartProductID = Number($target.attr("data-cartProductID"));

                qcssapp.Functions.showMask();
                qcssapp.Functions.removeProductFromCart(cartProductID, true);
            } else if ($target.attr("data-cartComboID")) { // The line is a combo title
                var cartComboID = Number($target.attr("data-cartComboID"));

                // Remove the combo and all its products from the CurrentOrder
                qcssapp.Functions.buildComboInProgress(cartComboID);
                qcssapp.Functions.removeComboFromCart(cartComboID);

                // Refresh the cart page
                qcssapp.Functions.resetCart();
            }
        },

        initializeNutritionTable: function() {
            this.nutritionCollection = new qcssapp.Collections.NutritionCollection();
            this.dailyValueCollection = new qcssapp.Collections.DailyValueCollection();

            $.each($('.cart-nutrition-value-row'),function() { $(this).remove(); });
            $.each($('.nutrition-cart-category'),function() { $(this).remove(); });
        },

        //returns an object with each product's info and modifiers
        getProductList: function() {
            var curName, curExists = false, productList = [];

            this.cartCollection.each(function( mod ) {
                var product = qcssapp.Functions.getProductObject(mod.attributes);
                curName = product["PRODUCTNAME"];
                product['MODIFIERINFO'] = mod.get('modifierList');

                //Defect 3861:  Duplicate Items' nutritional info, when added separately, are not combined into one result in the cart view
                //Solution:     Loop through the existing products and increment the quantity if it exists, and push the new item if it wasn't found
                //Note:         Check by ProductName because we probably should not combine items here if other options make them different
                $.each(productList, function(i, curProduct) {
                    if(curProduct["PRODUCTNAME"] == curName) { curExists = true; curProduct["QUANTITY"]++; return false; }
                    else { curExists = false; }
                });

                if(!curExists) { productList.push(product); }
            });

            return productList;
        },

        //creates a nutrition information table from the products in the cart
        createNutritionCartTable:function() {
            this.$cartContainer.show();

            if(!qcssapp.Functions.checkServerVersion(1,6)) {
                this.$cartContainer.hide();
                qcssapp.Functions.hideMask();
                return;
            }

            var successParameters = {
                cartView:this
            };

            this.initializeNutritionTable();
            var productList = this.getProductList();

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/ordering/cart/nutrition', 'POST', JSON.stringify(productList), successParameters,
                function(response) {
                    if ( response ) {
                        var index = 1;
                        $.each(response.allProductNutritionTotals, function () {
                            this.id = index;
                            index++;
                        });

                        successParameters.cartView.renderCartNutrition(response);
                    }
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error loading nutrition data for products in the cart.");
                    }
                }, '', '', '', true, false
            );
        },

        //adds the category row, then each product nutrition info row, then the total nutrition info row, finally the daily value rows
        renderCartNutrition: function(nutritionData) {

            //category row
            this.addNutritionCategoryRows(nutritionData.nutritionCategories);

            //product rows
            this.nutritionCollection.set(nutritionData.allProductNutritionTotals);
            this.addProductNutritionRows();

            //total product row
            this.addNutritionRow(nutritionData.totalNutritionItem);

            //daily value rows
            this.dailyValueCollection.set(nutritionData.calculatedDVTotals);
            this.addTotalDailyValueRows();

            //formatting
            //this.cartNutritionExpand();
            this.cleanUpCartNutritionTable();
            this.setSlickWidths();
        },

        //adds the nutrition category row
        addNutritionCategoryRows: function(nutritionCategories) {
            //add the 'Product' and 'Qty' cell
            $('.cart-nutrition-category-row').append( '<td class="nutrition-cart-category cart-quantity-cell">Qty</td>' ).append( '<td class="nutrition-cart-category cart-product-cell">Product</td>' );

            $.each(nutritionCategories, function(index) {  //add each cell
                if(this.name != " ") {
                    $('.cart-nutrition-category-row').append( '<td class="nutrition-cart-category" data-category-name="'+this.shortName+'">'+this.name +'</td>' );

                } else {
                    $('.cart-nutrition-category-row').append( '<td class="nutrition-cart-category hideRows" data-hidden-index="'+index+'"></td>' );
                }
            });

            this.checkCategoryNameOverflow();
        },

        //create a row for each product/modifier in the collection
        addProductNutritionRows: function() {
            this.nutritionCollection.each(function( mod ) {
                this.addNutritionRow(mod.attributes);
            }, this );
        },

        //add the nutritional info row
        addNutritionRow: function(mod) {
            var nutritionView = new qcssapp.Views.NutritionCartView({
                model: mod
            });
            $('.cart-nutrition-body').append( nutritionView.render().el );
        },

        //create a row for each daily value total
        addTotalDailyValueRows: function() {
            this.dailyValueCollection.each(function( mod ) {
                this.addDailyValueRow(mod.attributes);
            }, this );
        },

        //add the daily value row
        addDailyValueRow: function(mod) {
            var dailyValueView = new qcssapp.Views.DailyValueCartView({
                model: mod
            });
            $('.cart-nutrition-body').append( dailyValueView.render().el );
        },

        //if the full category name doesn't fit in the cell, use the short name
        checkCategoryNameOverflow: function() {
            $.each($('.nutrition-cart-category'), function() {
                $(this).addClass('primary-background-color primary-gradient-color font-color-primary');
                qcssapp.Functions.checkCategoryForOverflow($(this));
            });
        },

        //hides nutritional info column if the category is not set
        cleanUpCartNutritionTable: function() {
            $.each($('.nutrition-cart-category[data-hidden-index]'), function(){
                var index = $(this).attr('data-hidden-index');
                $.each($(".nutrition-value[data-nutrition-index='"+index+"']"), function() {
                    $(this).addClass('hideRows');
                });
            });

            //format the 'Totals' label and 'DV' labels
            var $cartProductCell = $('.cart-product-cell');
            $('.cart-quantity-cell').last().addClass('cart-dv-cell');
            $cartProductCell.last().addClass('cart-dv-cell');
            $.each($('.cart-product-cell:not(.cart-dv-cell,.font-color-primary)'), function() {
                $(this).css('font-size','13px');
            });

            var count = $('.nutrition-cart-category:not(.hideRows)').length;
            if(count == 4) { $cartProductCell.css({width:'33%'}); }
            if(count == 3) { $cartProductCell.css({width:'45%'}); }
            if(count == 2) { this.$cartContainer.hide(); }

            $('.starDescription, .poundDescription').show();
        },

        //clicking on combo line in cart, going to combo view
        buildComboView: function(comboIndex) {
            var comboModel = qcssapp.CurrentOrder.combos.models[comboIndex];

            qcssapp.CurrentCombo.name = comboModel.get('name');

            qcssapp.Functions.buildComboInProgress(comboIndex);

            qcssapp.Functions.loadCombo(comboModel.get('comboId'), qcssapp.ComboInProgress.length, comboModel, true);
        },

        // Create a Slick carousel from the cart or combo line
        createSlickLine: function(line) {
            // Create the remove button
            line.$el.wrap('<div></div>');
            var slickContainer = line.$el.parent();
            slickContainer.append('<div class="cart-remove">Remove</div>');

            // Add the product or combo ID as an attribute to the remove button
            var cartProductID = line.$el.attr("data-cartProductID");
            var cartComboID = line.$el.attr("data-cartComboID");
            if (cartProductID) {
                slickContainer.children().last().attr("data-cartProductID", cartProductID);
            }
            if (cartComboID) {
                slickContainer.children().last().attr("data-cartComboID", cartComboID);
            }

            // Create Slick
            slickContainer.slick({
                infinite: false,
                initialSlide: 0,
                touchThreshold: this.slickButtonFraction * 2,
                variableWidth: true,
                outerEdgeLimit: true
            });
            this.setSlickWidths();
        },

        // Set the widths of the Slick slides -- exact widths are only known after other formatting is completed
        setSlickWidths: function() {
            // Hide the Slick arrows
            $(".slick-arrow").hide();

            var cartLineWidth = $("#cartList").width();
            if (cartLineWidth > 0) {
                $(".slick-slide:has(.cart-line)").width(cartLineWidth);
                $(".slick-slide:has(.cart-remove)").width(cartLineWidth / this.slickButtonFraction);
                $(".cart-remove").width(cartLineWidth / this.slickButtonFraction);
            }
        }

    });
})(qcssapp, jQuery);