;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.NetworkErrorView - View for displaying newtork errors
     */
    qcssapp.Views.NetworkErrorView = Backbone.View.extend({
        internalName: "Network Error View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'network-error_modal',

        template: _.template( $('#network-error-template').html() ),

        initialize: function (options) {
            this.options = options || {};

            this.hidden = this.options.hidden;

            this.callback = this.options.callback;
            this.callbackParams = this.options.callbackParams;

            _.bindAll( this, 'displayButtons');

            this.render();
        },

        render: function() {
            var content = "Poor network connection detected. <br>Please check that your device is connected to WiFi or has a stable internet connection.";

            if(this.options == 'timeout') {
                content = "The requested server may be busy. <br /> Please try again in a moment.";
            }

            var templateData = {
                content: content
            };

            this.$el.html( this.template( templateData ) );

            if ( this.hidden ) {
                this.$el.addClass('hidden');
            }

            $('body').append( this.$el );

            this.displayButtons();

            return this;
        },

        displayButtons: function() {
            var buttonText = "Logout";

            var $activePanel = $('.panel.active');
            var activePanelID = $activePanel.attr('id');

            if ( $activePanel.length == 1 && (activePanelID == 'login-page' || activePanelID == 'add-application' || activePanelID == 'enterCode-page')) {
                buttonText = 'Close';
            }

            if (typeof activePanelID === 'undefined' ) {
                buttonText = 'Close';
            }

            if ( qcssapp.Location == "" ) {
                qcssapp.Location = $('#loginCodeSelect').val()
            }

            //Remove from order button
            new qcssapp.Views.ButtonView({
                text: buttonText,
                buttonType: "customCB",
                appendToID: "#network-error_button-container",
                id: "network-error-close-btn",
                parentView: this,
                class: "align-center ",
                callback: function() {
                    this.fadeOut();
                    $('#addInstance').show();

                    if ( $activePanel.length == 1 && (activePanelID == 'login-page' || activePanelID == 'add-application' || activePanelID == 'enterCode-page')) {
                        return;
                    }

                    qcssapp.Functions.resetOnLogout();

                    if ( qcssapp.Location == "" ) {
                        qcssapp.Location = $('#loginCodeSelect').val()
                    }
                }.bind(this)
            });

            if ( $activePanel.length == 1 && activePanelID == 'add-application' ) {
                return;
            }

            //Remove from order button
            new qcssapp.Views.ButtonView({
                text: "Retry",
                buttonType: "customCB",
                appendToID: "#network-error_button-container",
                id: "network-error-retry-btn",
                parentView: this,
                class: "align-center ",
                callback: function() {
                    this.fadeOut();

                    if ( qcssapp.Location == "" ) {
                        qcssapp.Location = $('#loginCodeSelect').val()
                    }
                    var endPoint = qcssapp.Location + '/api/auth/login/settings';
                    qcssapp.Functions.callAPI(endPoint, 'GET','','',
                        function() {
                            if(window.location.hash == "") {
                                qcssapp.Router.navigate('login-page', {trigger:true});
                            }
                        },
                        function() {
                            if ( qcssapp.DebugMode ) {
                                console.log('Attempting to retry network error...');
                            }
                        }
                    );
                }.bind(this)
            });

            this.checkLoadedImages()
        },

        checkLoadedImages: function() {

            var image = new Image();
            image.onload = function() {
                this.$el.find('.network-error_icon').show();
            }.bind(this);

            image.onerror = function() {
                this.$el.find('.network-error_icon').hide();
            }.bind(this);

            image.src = "images/icon-network-connection.svg";
        },

        fadeIn: function(dur) {
            dur = !dur ? 150 : dur;

            this.hidden = false;
            this.$el.fadeIn(dur, function() {
                this.$el.removeClass('hidden');
            }.bind(this));
        },

        fadeOut: function(dur) {
            dur = !dur ? 150 : dur;

            this.hidden = true;
            this.$el.fadeOut(dur, function() {
                this.$el.remove();
            }.bind(this));
        }
    });
})(qcssapp, jQuery);
