;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.Receipt - View for displaying Re-authorization for order confirmation
     */
    qcssapp.Views.ReceiptView = Backbone.View.extend({
        internalName: "Receipt View", //NOT A STANDARD BACKBONE PROPERTY

        id: 'receipt-page', //references existing HTML element for Reauth view - can be referenced as this.$el throughout the view

        template: _.template( $('#receipt-template').html() ),

        events: {
            "click #receipt-arrow-right": 'slideReceiptRight',
            "click #receipt-arrow-left": 'slideReceiptLeft',
            'click .favorite-order-icon' : 'favoriteOrder',
            'click .add-to-cart-icon' : 'orderAgain',
            'click .express-order-icon' : 'expressReorder'
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in "+ this.internalName);
            }

            this.$container = $('#receipt');

            //get options (entire content is receipt)
            this.options = options || {};

            if ( !this.options || !this.options.content ) {
                this.options.content = "No receipt found for this transaction.";
            }

            this.render();

            this.favoriteOrderId = "";
            this.nextTransactionID = "";
            this.previousTransactionID = "";
            this.nextTransactionShowReorder = true;
            this.previousTransactionShowReorder = true;
            this.nextTransactionShowFavorite = true;
            this.previousTransactionShowFavorite = true;
            this.txnList = [];

            this.getSurroundingTransactions();
        },

        render: function() {
            this.$container.html(this.$el.html( this.template( this.options ) ) );
            this.$helpIcon = $('#receipt-view').find('.help-icon');
            this.$helpIcon.addClass('hidden');

            var wellnessIcons = $('.receipt-wellness-icon');
            wellnessIcons.css('height', function() { return $(this).css('font-size'); });
            wellnessIcons.attr('src', qcssapp.healthyIndicator.wellness);

            $('#receipt-order-icons').hide();
            if ( this.options.showReorder == true && $('#orderAgainBtn').length == 0 ) {
                this.renderReorderButton();
            } else {
                qcssapp.Functions.hideMask();
            }

            return this;
        },

        renderIcons: function() {
            $('#receipt-order-icons').show();
            var originalSetting = qcssapp.usingBranding;

            //if not using branding still set svg color
            if(!qcssapp.usingBranding) {
                qcssapp.usingBranding = true;
            }

            qcssapp.Functions.setSVGColors();
            qcssapp.usingBranding = originalSetting;

            $('img.add-to-cart-icon').hide();
            $('img.express-cart-icon').hide();

            this.$el.find('.favorite-order-icon').css('filter', 'drop-shadow(3px 3px 2px rgba(0,0,0,0.2))');

            if(qcssapp.help.allowHelpfulHints ) {
                this.$helpIcon.removeClass('hidden');
            }
        },

        renderReorderButton : function() {
            if(qcssapp.personModel) {
                qcssapp.Functions.hideMask();
                return;
            }

            var receiptView = this;

            if(!qcssapp.Functions.checkServerVersion(3, 0)){
                $('#receipt-order-icons').hide();
                //Reorder button
                new qcssapp.Views.ButtonView({
                    text: "Order Again",
                    buttonType: "customCB",
                    appendToID: "#receipt-btn-container",
                    id: "orderAgainBtn",
                    class: "align-center order-button prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                    callback: function() {
                        receiptView.orderAgain()
                    }
                });
            } else {

                this.renderIcons();

                var $favoriteOrderIcon = $('.favorite-order-icon');
                $favoriteOrderIcon.show() ;
                if(!this.options.showFavorites) {
                    $favoriteOrderIcon.hide();
                    qcssapp.Functions.hideMask();
                    return;
                }

                var transactionID = $("#receipt-btn-container").attr('data-transactionID');

                qcssapp.Functions.isFavoriteOrder(transactionID, function (data) {
                    var favImg;
                    if(!data || data.length < 1) {
                        favImg = 'images/icon-favorites-unchecked.svg';
                    } else if(!data[0].ACTIVE) {
                        favImg = 'images/icon-favorites-unchecked.svg';
                        receiptView.favoriteOrderId = data[0].FAVORITEORDERID;
                    } else {
                        favImg = 'images/icon-favorites.svg';
                        receiptView.favoriteOrderId = data[0].FAVORITEORDERID;
                    }

                    $favoriteOrderIcon.attr('src', favImg);
                });
            }
        },

        expressReorder: function() {
            qcssapp.CurrentOrder.expressReorder = true;
            this.orderAgain();
        },

        orderAgain : function() {
            var transactionID = $("#receipt-btn-container").attr('data-transactionID');

            if ( transactionID && Number(transactionID) > 0 ) {
                var endPoint = qcssapp.Location + '/api/ordering/cart/' + transactionID;

                //use separate endpoint to call backend so we don't remove the dining option product for express reorders
                if(qcssapp.CurrentOrder.expressReorder) {
                    endPoint = qcssapp.Location + '/api/ordering/express/cart/' + transactionID;
                }

                var successParameters = {receiptView: this};

                qcssapp.Functions.callAPI(endPoint, 'GET', '', successParameters,
                    function(data, successParameters) {
                        if ( data.message && data.message.length > 0 ) {
                            data.message = qcssapp.Functions.htmlDecode(data.message)
                        }

                        if ( !data || data.products == null || data.storeModel == null || data.products.length == 0 || data.storeModel == '' ) {
                            if ( data.message && data.message.length > 0 ) {
                                qcssapp.Functions.displayError(data.message);
                            } else {
                                qcssapp.Functions.displayError('Could not retrieve details to re-order, please try again later');
                            }
                            return;
                        }

                        $('#show-order').off();
                        new qcssapp.Views.OrderStoreView();

                        var storeModel = new qcssapp.Models.OrderStore(data.storeModel, {parse: true});
                        qcssapp.CurrentOrder.storeModel = storeModel;
                        qcssapp.Functions.setStoreSettings();

                        qcssapp.StoreSelected = data.storeID;

                        qcssapp.Functions.setSession(qcssapp.Session, qcssapp.Location, qcssapp.StoreSelected, qcssapp.UserCode);

                        var productsToAddToCart = [];
                        var showErrors = false;
                        var errors = "<ul class='reorder-error-list'>";
                        var errorMessages = [];
                        $.each(data.products, function() {
                            if ( this.message.length == 0 ) {
                                this.productID = this.id;
                                var productModel = new qcssapp.Models.Product(this, {parse:true});
                                productsToAddToCart.push(productModel);
                            } else {
                                // Don't show the same error message more than once
                                if(!errorMessages.includes(this.message)){
                                    errors = errors + "<li>" + this.message + "</li>";
                                    errorMessages.push(this.message);
                                    showErrors = true;
                                }
                            }
                        });

                        errors = errors + "</ul>";
                        errors = qcssapp.Functions.htmlDecode(errors);

                        if ( showErrors && !data.message ) {
                            data.message = errors;
                        } else if ( showErrors ) {
                            data.message = data.message + errors;
                        }

                        if(showErrors) {
                            qcssapp.CurrentOrder.expressReorder = false;
                        }

                        var reorderCallback = function(data, reorderParams) {
                            //homeKeypadID and message params could be passed in as data or as reorderParams depending on number of products
                            if(typeof reorderParams == 'undefined') {
                                reorderParams = data;
                            }

                            //check if homeKeypadID is undefined, if so but storeModel is not then set homeKeypadID to storeModel's homeKeypadID
                            if(typeof reorderParams.homeKeypadID == 'undefined' && typeof reorderParams.storeModel !== 'undefined') {
                                reorderParams.homeKeypadID = reorderParams.storeModel.homeKeypadID;
                            }

                            //if the store is using credit cards as a tender, get the account's balance and payment method
                            if(qcssapp.Functions.checkServerVersion(3,0) && typeof storeModel.get('usesCreditCardAsTender') !== 'undefined' && storeModel.get('usesCreditCardAsTender')) {
                                var successParameters = {
                                    homeKeypadID: reorderParams.homeKeypadID,
                                    message: reorderParams.message
                                };

                                qcssapp.Functions.getAccountBalanceDetails(successParameters, true);

                            //if clicked express reorder and the products in the cart are all valid
                            } else if ( qcssapp.CurrentOrder.expressReorder && reorderParams.message == "" ){
                                qcssapp.Functions.addHistoryRecord(reorderParams.homeKeypadID, 'keypad');
                                qcssapp.Functions.checkForRewardsAndDiscounts();

                            } else { //otherwise continue as normal
                                qcssapp.Functions.addHistoryRecord(reorderParams.homeKeypadID, 'keypad');

                                qcssapp.Router.navigate('cart', {trigger: true});

                                if ( reorderParams.message && reorderParams.message.length > 0 ) {
                                    qcssapp.Functions.displayError(reorderParams.message);
                                }
                            }
                        };

                        if ( productsToAddToCart.length == 0 ) {
                            reorderCallback(data);
                        }

                        if(qcssapp.CurrentOrder.expressReorder) {
                            successParameters.receiptView.buildExpressReorderModel(data);
                            for (var p = 0; p < productsToAddToCart.length; p++) {
                                qcssapp.Functions.addProductToCart(productsToAddToCart[p]);
                            }
                            reorderCallback(data);

                        } else {

                            for (var i = 0; i < productsToAddToCart.length; i++) {
                                var product = productsToAddToCart[i];
                                qcssapp.Functions.addProductToCart(product);
                            }

                            if( qcssapp.CurrentOrder.products.length > 1 ) {
                                qcssapp.Functions.checkProductCombos(reorderCallback, data);
                                return;
                            }

                            reorderCallback(data);
                        }
                    },
                    '',
                    '',
                    '',
                    '',
                    false,
                    true
                );
            }
        },

        favoriteOrder: function() {
            var that = this;
            var transactionID = $("#receipt-btn-container").attr('data-transactionID');
            if(that.favoriteOrderId !== '') {
                qcssapp.Functions.toggleFavoriteOrder(that.favoriteOrderId, function(data) {
                    that.toggleBtnImage();
                }, false);
            } else {
                if (transactionID && Number(transactionID) > 0) {
                    // Create new Favorite
                    qcssapp.Functions.createFavoriteOrder(transactionID, function(data) {
                        that.favoriteOrderId = data.favoriteOrderId;
                        that.toggleBtnImage();
                    })
                }
            }
        },

        buildExpressReorderModel: function(data) {
            var transactionID = $("#receipt-btn-container").attr('data-transactionID');

            var expressOrderModel = new qcssapp.Models.ExpressOrder();
            expressOrderModel.set('id', transactionID);
            expressOrderModel.set('phone', data.phone);
            expressOrderModel.set('usingDiningOptions', data.useDiningOptions);
            expressOrderModel.set('tenderTypeID', data.tenderTypeID);
            expressOrderModel.set('orderType', data.orderType);

            if(data.tenderTypeID != null && Number(data.tenderTypeID) == 2) {
                expressOrderModel.set('usingCreditCardAsTender', true);
            }

            //if there was no phone set on the transaction, then a landline phone was used
            if(data.phone == '') {
                expressOrderModel.set('phone', qcssapp.accountModel.get('phone'));
            }

            //if there is a comment set it
            if(typeof data.comments !== 'undefined' && data.comments != '') {
                expressOrderModel.set('comments', qcssapp.Functions.htmlDecode(data.comments));
            }

            //if there is a delivery location set it
            if(typeof data.location !== 'undefined' && data.location != '') {
                expressOrderModel.set('location', qcssapp.Functions.htmlDecode(data.location));
            }

            qcssapp.CurrentOrder.expressReorderModel = expressOrderModel.attributes;
        },

        slideReceiptLeft: function() {
            if(this.previousTransactionID)  {
                qcssapp.Functions.viewReceipt(this.previousTransactionID, this.previousTransactionShowReorder, this.previousTransactionShowFavorite, "", this.options.rewardsProgramID, this.options.txnView);
            }
        },

        slideReceiptRight: function() {
            if(this.nextTransactionID) {
                qcssapp.Functions.viewReceipt(this.nextTransactionID, this.nextTransactionShowReorder, this.nextTransactionShowFavorite, "", this.options.rewardsProgramID, this.options.txnView);
            }
        },

        //get the previous and next transactions
        getSurroundingTransactions: function() {
            var $arrowLeft = $('#receipt-arrow-left');
            var $arrowRight = $('#receipt-arrow-right');

            $arrowLeft.show();
            $arrowRight.show();

            if ( !qcssapp.Functions.checkServerVersion(1,6) || typeof this.options.fromLocation == 'undefined') {
                $arrowLeft.hide();
                $arrowRight.hide();
                return;
            }

            var receiptView = this;

            var txnView = this.options.txnView;
            this.txnList = txnView.txnList;

            this.setPreviousTransaction(receiptView);
            this.setNextTransaction(receiptView);

            if(this.previousTransactionID == "") {
                $arrowLeft.hide();
            }

            if (this.nextTransactionID == "") {
                this.loadNextTransactionCollection()
            }
        },

        //get the previous transaction from the list
        setPreviousTransaction: function(receiptView) {
            var selectedTransactionID = receiptView.options.transactionID;
            var index = 0;

            var txnList = receiptView.txnList.slice();  //reverse the txnList when looking for previous
            txnList = txnList.reverse();

            //while there is a transaction at the index and the previousTransactionID is not set
            while(typeof txnList[index] !== 'undefined' && receiptView.previousTransactionID == "") {
                var txnModel = txnList[index];

                //if this paTransactionID is the same as the selected transaction id and a next transaction exists
                if(txnModel.patransactionID == selectedTransactionID && typeof txnList[index+1] !== 'undefined') {

                    //if the "next" transaction can show receipts in myqc, set the "next" (aka previous) transaction
                    if(txnList[index+1].showReceiptsOnMyQC) {
                        receiptView.previousTransactionID = txnList[index+1].patransactionID;
                        receiptView.previousTransactionShowReorder = txnList[index + 1].showReorder;
                        receiptView.previousTransactionShowFavorite = txnList[index + 1].enableFavorites;

                        //otherwise the next transaction becomes the selected transaction for the next loop
                    } else {
                        selectedTransactionID = txnList[index+1].patransactionID;
                    }
                }

                index++; //look at the next transaction
            }
        },

        //get the next transaction from the list
        setNextTransaction: function(receiptView) {
            var selectedTransactionID = receiptView.options.transactionID;
            var index = 0;

            //while there is a transaction at the index and the nextTransactionID is not set
            while(typeof receiptView.txnList[index] !== 'undefined' && receiptView.nextTransactionID == "") {
                var txnModel = receiptView.txnList[index];

                //if this paTransactionID is the same as the selected transaction id and a next transaction exists
                if(txnModel.patransactionID == selectedTransactionID && typeof receiptView.txnList[index+1] !== 'undefined') {

                    //if the next transaction can show receipts in myqc, set the next transaction
                    if(receiptView.txnList[index+1].showReceiptsOnMyQC) {
                        receiptView.nextTransactionID = receiptView.txnList[index+1].patransactionID;
                        receiptView.nextTransactionShowReorder = receiptView.txnList[index + 1].showReorder;
                        receiptView.nextTransactionShowFavorite = receiptView.txnList[index + 1].enableFavorites;

                        //otherwise the next transaction becomes the selected transaction for the next loop
                    } else {
                        selectedTransactionID = receiptView.txnList[index+1].patransactionID;
                    }
                }

                index++;
            }
        },

        //get the next 20 transactions
        loadNextTransactionCollection: function() {
            var receiptView = this;
            var txnView = this.options.txnView;

            var receiptSuccessFn = function(receiptView, txnView) {
                if(txnView.collection.models.length > 0) {
                    receiptView.options.txnView = txnView;

                    receiptView.setNextTransaction(receiptView);

                } else {
                    $('#receipt-arrow-right').hide();
                }
            };

            txnView.loading = true;

            txnView.currentPage += 1;
            txnView.loadCollection(receiptSuccessFn, receiptView);
        },

        toggleBtnImage: function() {
            var img = $('.favorite-order-icon');
            var src = img.attr('src') === 'images/icon-favorites-unchecked.svg' ? 'images/icon-favorites.svg' : 'images/icon-favorites-unchecked.svg';
            img.attr('src', src);
        }

    });

    $('#receipt').swipe({
        swipeLeft: function(event, direction, distance, duration, fingerCount) {
            $('#receipt-arrow-right').trigger('click');
        },
        swipeRight: function(event, direction, distance, duration, fingerCount) {
            $('#receipt-arrow-left').trigger('click');
        }
    });

})(qcssapp, jQuery);