;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ExpressOrderItemLineView - View for displaying products and modifiers in the express order
     */
    qcssapp.Views.ExpressOrderItemLineView = Backbone.View.extend({
        //is a list tag.
        tagName:  'li',
        className: 'template-gen express-order-item-line',

        // Cache the template function for a single item.
        template: _.template( $('#express-reorder-item-line-template').html() ),

        render: function() {

            if(this.model.get('modifiers') != '' && this.model.get('modifiers').length > 0) {
                this.model.set('modifiersList', qcssapp.Functions.convertModifierAndPrepOptionArrayToCsv(this.model.get('modifiers')));
            }

            if(this.model.get('price') != '') {
                this.model.set('linePrice', qcssapp.Functions.formatPrice(this.model.get('price')));
            } else {
                this.model.set('linePrice', qcssapp.Functions.formatPrice('0.00'));
            }

            if(this.model.get('isCombo')) {
                var upcharge = Number(this.model.get('price')) - Number(this.model.get('basePrice'));
                if(Number(upcharge) != 0) {
                    this.model.set('upcharge', upcharge.toFixed(2));
                }
            }

            this.model.set('itemName', this.model.get('name'));

            var name = "";

            //if this product is part of a combo
            if(this.model.get('isCombo')) {

                //if product has an upcharge set the price as the upcharge
                var upchargeAmount = Number(this.model.get('originalPrice')) - Number(this.model.get('basePrice'));

                if(upchargeAmount < 0) {
                    upchargeAmount = upchargeAmount * -1;
                    name = this.model.get('name') + ' (' + qcssapp.Functions.formatPrice(upchargeAmount, '-') + ')';
                } else if(upchargeAmount > 0) {
                    name = this.model.get('name') + ' (' +  qcssapp.Functions.formatPrice(upchargeAmount, '+') + ')';
                }
            }

            if(typeof this.model.get('prepOption') !== 'undefined' && this.model.get('prepOption') != null) {
                this.model.set('prepOptionSetID', this.model.get('prepOption').prepOptionSetID);

                if(!(this.model.get('prepOption').defaultOption && !this.model.get('prepOption').displayDefault) || (this.model.get('prepOption').defaultOption && Number(this.model.get('prepOption').price) > 0)) {
                    var prepOptionName = this.model.get('prepOption').name;

                    if(name == "") {
                        name = this.model.get('name') + ' - <span class="prep-option-name">' + prepOptionName;
                    } else {
                        name += ' - <span class="prep-option-name">' + prepOptionName;
                    }

                    if ( Number(this.model.get('prepOption').price) > 0 ) {
                        var prepPrice = Number(this.model.get('prepOption').price).toFixed(2);
                        name += ' (' + qcssapp.Functions.formatPrice(prepPrice, '+') + ')</span>';
                    } else {
                        name += '</span>';
                    }
                }
            }

            if(name != "") {
                this.model.set('itemName', name);
            }

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            if(this.model.get('quantity') != '' && Number(this.model.get('quantity')) != 1) {
                var price = this.$el.find('.express-item-line-price').text();
                if(this.model.get('scaleUsed')) {
                    price = Number(this.model.get('quantity')).toFixed(2) + ' lbs at ' + price;
                } else {
                    price = this.model.get('quantity') + " <span class='quantity'>x</span> " + price;
                }
                this.$el.find('.express-item-line-price').html(price);
            }

            if(this.model.get('isCombo')) {
                this.$el.addClass('express-combo-line');
                this.$el.find('.express-item-line-price').hide().text('');
            }

            return this;
        }
    });
})(qcssapp, jQuery);
