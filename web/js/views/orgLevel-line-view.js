;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.DetailLineView - View for displaying Account Detail Collections (Collection Item Models)
     */
    qcssapp.Views.OrgLevelLineView = Backbone.View.extend({
        //is an option tag.
        tagName:  'option',
        className:'locationOption',

        // Cache the template function for a single item.
        template: _.template( $('#orgLevel-template').html() ),

        render: function() {
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            // this.$el.html( this.template( this.model.attributes ) );
            this.$el.text(this.model.get('name'));
            this.$el.attr('value', this.model.get('orgLevelPrimaryId'));
            this.$el.attr('id', 'orgLevel-option' + this.model.get('orgLevelPrimaryId'));

            return this;
        }
    });
})(qcssapp, jQuery);
