;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FavoriteView - View for displaying My Quick Picks page
     */
    qcssapp.Views.MyQuickPicksView = Backbone.View.extend({
        internalName: "My Quick Picks View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'ul',
        className: 'info-list scrollElement',
        id: 'my-quick-picks-list',

        el: '#my-quick-picks-page',

        events: {
            'click .popularBtn': function() {this.filterList('popular');},
            'click .favoriteBtn': function() {this.filterList('favorite');},
            'click .recentBtn': function() {this.filterList('recent');},
            'click .ordersBtn': function() {this.filterList('order');}
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in favorites view");
            }

            this.$container = $('#myQuickPicks-list-container');

            this.options = options || {};

            this.itemsPerPageFav = 20; this.itemsPerPageRec = 20; this.itemsPerPagePop = 20; this.itemsPerPageUnav = 20; this.itemsPerPageOrd = 20; this.itemsPerPageOrdUnav = 20;
            this.currentPageFav = 1; this.currentPageRec = 1; this.currentPagePop = 1; this.currentPageUnav = 1; this.currentPageOrd = 1; this.currentPageOrdUnav = 1;
            this.endOfListFav = false; this.endOfListRec = false; this.endOfListPop = false; this.endOfListUnav = false; this.endOfListOrd = false; this.endOfListOrdUnav = false;

            this.sort = "date";
            this.sortOrder = "desc";

            //bind "this" reference to functions in the view
            _.bindAll(this, "fetchSuccess", "fetchError", "checkScroll");

            //event didn't bubble properly for using the events, need to explicitly set the function trigger
            this.$container.on('scroll',this.checkScroll);

            this.favoriteCollection = new qcssapp.Collections.FavoriteCollection();
            this.recentCollection = new qcssapp.Collections.FavoriteCollection();
            this.popularCollection = new qcssapp.Collections.FavoriteCollection();
            this.orderCollection = new qcssapp.Collections.FavoriteCollection();
            this.orderUnavailableCollection = new qcssapp.Collections.FavoriteCollection();

            this.$favoriteList = $('#favorite-list');
            this.$recentList = $('#recent-list');
            this.$popularList = $('#popular-list');
            this.$favoritesUnavailable = $('#favorites-unavailable');
            this.$orderList = $('#order-list');
            this.$orderUnavailbleList = $('#order-unavailable-list');

            this.$view = $('#favoriteview');
            this.$helpIcon = this.$view.find('.help-icon');
            this.$favoriteHeader = this.$view.find('#favorite-header');
            this.$helpIcon.addClass('hidden');

            this.$el.find('.active-list').removeClass('active-list');
            this.$favoriteList.addClass('active-list');

            this.$favoritesSeperator = $('#favorites-unavailable-separator');
            this.$ordersSeperator = $('#orders-unavailable-separator');

            this.favoriteUnavailableCollection = new qcssapp.Collections.FavoriteCollection();

            this.listenTo(this.favoriteCollection, 'change:imageLoaded', this.loadImage);
            this.listenTo(this.favoriteUnavailableCollection, 'change:imageLoaded', this.loadImage);
            this.listenTo(this.recentCollection, 'change:imageLoaded', this.loadImage);
            this.listenTo(this.popularCollection, 'change:imageLoaded', this.loadImage);
            this.listenTo(this.orderCollection, 'change:imageLoaded', this.loadImage);
            this.listenTo(this.orderUnavailableCollection, 'change:imageLoaded', this.loadImage);

            this.resetFavoriteElements();

            this.formatFavoriteList();

            //property to prevent further requests while loading, true because of the next fetch, turned false in fetch success
            this.loading = true;
            this.unavailableOrders = false;
            this.appendEndDiv = false;
            this.appendEndOrdDiv = false;

            this.popularListLoaded = false;
            this.recentListLoaded = false;
            this.orderListLoaded = false;

            this.allProductList = null;
            this.getAllProductsInThisStore();

            this.notifyInactivatedFavorites();

            this.checkForHelpfulHints();

            this.productLines = 0;
            this.nextOneFilled = 0;
            this.waitingCount = 0;

            this.helpViewActive = false;
        },

        //get all the products in the active keypads of the current store, use this variable when loading the collections
        getAllProductsInThisStore: function() {
            var parameters = { MyQuickPicksView:this };
            var endPoint = qcssapp.Location + '/api/ordering/all/products';

            if( qcssapp.Functions.checkServerVersion(3,0) ) {
                endPoint = qcssapp.Location + '/api/favorite/all/products';
            }

            qcssapp.Functions.callAPI(endPoint, 'POST', '', parameters,
                function(response) {
                    if(response) {
                        parameters.MyQuickPicksView.allProductList = response.join(',');
                    }

                    if (!parameters.MyQuickPicksView.checkHistory()) {
                        //populates the new collections
                        parameters.MyQuickPicksView.loadFavoriteCollection(false);
                        parameters.MyQuickPicksView.loadUnavailableCollection(false);
                    }
                },
                function(errorParameters) {
                    console.log('There was an issue getting a list of all the products in the store.');
                },
                '', '', '', false, true
            );

        },

        // favorite collection
        loadFavoriteCollection: function(preventMask) {
            var dataParameters = $.param({ models: this.itemsPerPageFav, page: this.currentPageFav, sort: this.sort, order: this.sortOrder, productList: this.allProductList});
            dataParameters = decodeURIComponent(dataParameters);

            var parameters = { MyQuickPicksView:this };
            var endPoint = qcssapp.Location + '/api/ordering/list/favorite';

            if( qcssapp.Functions.checkServerVersion(3,0) ) {
                endPoint = qcssapp.Location + '/api/favorite/items/list';
            }

            qcssapp.Functions.callAPI(endPoint, 'GET', dataParameters, parameters,
                function(response, successParameters) {
                    if(response) {
                        //send in the collection element and return data to be rendered
                        successParameters.MyQuickPicksView.fetchSuccess(parameters.MyQuickPicksView.favoriteCollection, response, parameters.MyQuickPicksView.$favoriteList);

                        successParameters.MyQuickPicksView.setPriority();
                    }
                },
                function(errorParameters) {
                    errorParameters.MyQuickPicksView.fetchError();
                },
                '', parameters, '', preventMask, false
            );
        },

        // unavailable favorites collection
        loadUnavailableCollection: function(preventMask) {
            var dataParameters = $.param({ models: this.itemsPerPageUnav, page: this.currentPageUnav, sort: this.sort, order: this.sortOrder, productList: this.allProductList});
            dataParameters = decodeURIComponent(dataParameters);

            var parameters = { MyQuickPicksView:this };
            var endPoint = qcssapp.Location + '/api/ordering/list/unavailable';

            if( qcssapp.Functions.checkServerVersion(3,0) ) {
                endPoint = qcssapp.Location + '/api/favorite/items/unavailable/list';
            }

            qcssapp.Functions.callAPI(endPoint, 'GET', dataParameters, parameters,
                function(response, successParameters) {
                    if(response) {

                        //send in the collection element and return data to be rendered
                        if(response.length > 0) {
                            successParameters.MyQuickPicksView.fetchSuccess(parameters.MyQuickPicksView.favoriteUnavailableCollection, response, parameters.MyQuickPicksView.$favoritesUnavailable);
                            successParameters.MyQuickPicksView.$favoritesUnavailable.show();
                            successParameters.MyQuickPicksView.$favoritesSeperator.show();
                        } else {
                            successParameters.MyQuickPicksView.$favoritesUnavailable.hide();
                            successParameters.MyQuickPicksView.$favoritesSeperator.hide();
                        }

                        successParameters.MyQuickPicksView.setPriority();
                        successParameters.MyQuickPicksView.checkHistory();
                    }
                },
                function(errorParameters) {
                    errorParameters.MyQuickPicksView.fetchError();
                },
                '', parameters, '', preventMask, false
            );
        },

        //popular collection
        loadPopularCollection: function(preventMask) {
            var dataParameters = $.param({ models: this.itemsPerPagePop, page: this.currentPagePop, sort: this.sort, order: this.sortOrder, productList: this.allProductList});
            dataParameters = decodeURIComponent(dataParameters);

            var parameters = { MyQuickPicksView:this };
            var endPoint = qcssapp.Location + '/api/ordering/list/popular';

            if( qcssapp.Functions.checkServerVersion(3,0) ) {
                endPoint = qcssapp.Location + '/api/favorite/popular/list';
            }

            qcssapp.Functions.callAPI(endPoint, 'GET', dataParameters, parameters,
                function(response, successParameters) {
                    if(response) {
                        //send in the collection element and return data to be rendered
                        successParameters.MyQuickPicksView.fetchSuccess(parameters.MyQuickPicksView.popularCollection, response, parameters.MyQuickPicksView.$popularList);
                        // allow the page to reload every time the popular button is clicked to update the favorited items
                        // successParameters.MyQuickPicksView.popularListLoaded = true;

                        successParameters.MyQuickPicksView.setPriority();

                        successParameters.MyQuickPicksView.checkSliderRows(successParameters.MyQuickPicksView.$popularList, false);
                    }
                },
                function(errorParameters) {
                    errorParameters.MyQuickPicksView.fetchError();
                },
                '', parameters, '', preventMask, false
            );
        },

        //recent collection
        loadRecentCollection: function(preventMask) {
            var dataParameters = $.param({ models: this.itemsPerPageRec, page: this.currentPageRec, sort: this.sort, order: this.sortOrder, productList: this.allProductList});
            dataParameters = decodeURIComponent(dataParameters);

            var parameters = { MyQuickPicksView:this };
            var endPoint = qcssapp.Location + '/api/ordering/list/recent';

            if( qcssapp.Functions.checkServerVersion(3,0) ) {
                endPoint = qcssapp.Location + '/api/favorite/recent/list';
            }

            qcssapp.Functions.callAPI(endPoint, 'GET', dataParameters, parameters,
                function(response, successParameters) {
                    if(response) {
                        //send in the collection element and return data to be rendered
                        successParameters.MyQuickPicksView.fetchSuccess(parameters.MyQuickPicksView.recentCollection, response, parameters.MyQuickPicksView.$recentList);
                        successParameters.MyQuickPicksView.recentListLoaded = true;

                        successParameters.MyQuickPicksView.setPriority();

                        successParameters.MyQuickPicksView.checkSliderRows(successParameters.MyQuickPicksView.$recentList, false);
                    }
                },
                function(errorParameters) {
                    errorParameters.MyQuickPicksView.fetchError();
                },
                '', parameters, '', preventMask, false
            );
        },

        //orders collection
        loadOrderCollection: function(preventMask) {
            var dataParameters = $.param({ models: this.itemsPerPageOrd, page: this.currentPageOrd, sort: this.sort, order: this.sortOrder, productList: this.allProductList});
            dataParameters = decodeURIComponent(dataParameters);

            var parameters = { MyQuickPicksView:this };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/favorite/orders/list', 'GET', dataParameters, parameters,
                function(response, successParameters) {
                    if(response) {
                        //send in the collection element and return data to be rendered
                        successParameters.MyQuickPicksView.fetchSuccess(parameters.MyQuickPicksView.orderCollection, response, parameters.MyQuickPicksView.$orderList);
                        successParameters.MyQuickPicksView.orderListLoaded = true;

                        successParameters.MyQuickPicksView.setPriority();

                        var preventMask = $('.help-view').length ? true : false;
                        successParameters.MyQuickPicksView.loadOrderUnavailableCollection(preventMask);

                        successParameters.MyQuickPicksView.checkSliderRows(successParameters.MyQuickPicksView.$orderList, false);
                    }
                },
                function(errorParameters) {
                    errorParameters.MyQuickPicksView.fetchError();
                },
                '', parameters, '', preventMask, false
            );
        },

        // unavailable orders collection
        loadOrderUnavailableCollection: function(preventMask) {
            var dataParameters = $.param({ models: this.itemsPerPageOrdUnav, page: this.currentPageOrdUnav, sort: this.sort, order: this.sortOrder, productList: this.allProductList});
            dataParameters = decodeURIComponent(dataParameters);

            var parameters = { MyQuickPicksView:this };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/favorite/orders/unavailable/list', 'GET', dataParameters, parameters,
                function(response, successParameters) {
                    if(response) {

                        //send in the collection element and return data to be rendered
                        if(response.length > 0) {
                            successParameters.MyQuickPicksView.fetchSuccess(parameters.MyQuickPicksView.orderUnavailableCollection, response, parameters.MyQuickPicksView.$orderUnavailbleList);
                            successParameters.MyQuickPicksView.unavailableOrders = true;
                            successParameters.MyQuickPicksView.$orderUnavailbleList.show();
                            successParameters.MyQuickPicksView.$ordersSeperator.show();
                        } else {
                            successParameters.MyQuickPicksView.$ordersSeperator.hide();
                        }

                        successParameters.MyQuickPicksView.setPriority();

                        successParameters.MyQuickPicksView.checkSliderRows(successParameters.MyQuickPicksView.$orderUnavailbleList, false);
                    }
                },
                function(errorParameters) {
                    errorParameters.MyQuickPicksView.fetchError();
                },
                '', parameters, '', preventMask, false
            );
        },

        //set the priority of which image should be loaded first
        setPriority: function() {
            var count = 0;
            this.nextOneFilled = 0;
            $.each(this.$container.find('.hideImages'), function() {
                var $lineContent = $(this).parent().parent();
                var $image =  $(this);
                if( $lineContent.hasClass('hasImage') && !$image.hasClass('finished')) {
                    $lineContent.attr('priority', count);
                    count++;
                }
            });
        },

        fetchSuccess: function(collection, data, $listElement) { //handles successful fetching the reward history collection
            collection.set(data, {parse: data});

            //render this view now that we've fetched the reward history collection
            this.render(collection, $listElement);

            //reset loading once complete
            this.loading = false;
        },

        //calls renderLine on product
        render: function(collection, $listElement) {
            var id = $listElement.attr('id');

            if(!this.checkIfCollectionIsEmpty(id, collection, $listElement)) {
                return;
            }

            $listElement.addClass('populated');

            var controllerModel = new qcssapp.Models.Controller({
                id: 'FavoriteLineView',
                destructible: true,
                destroyOn: ['NavView', 'KeypadView', 'CartView']
            });

            var index = 0;
            this.lineViews = [];
            collection.each(function( mod ) {
                if($listElement.attr('id') == 'order-list') {
                    var orderList =  this.orderCollection.toJSON();
                    mod.set('orderList', orderList);
                    mod.set('favoriteOrderIndex', index);
                    index++;
                }

                this.renderLine( mod, controllerModel, $listElement );
            }, this );

            this.checkEndOfList(id, collection, $listElement);

            $.each(this.lineViews, function() {
                this.setSlicker();
            });
        },

        //builds a new reward history line view, then creates and appends
        renderLine: function( mod, controllerModel, $listElement ) {
            controllerModel.set('options', {model:mod});

            var FavoriteLineView = qcssapp.Controller.constructView(controllerModel);
            $listElement.append( FavoriteLineView.render().el );

            this.lineViews.push(FavoriteLineView);
        },

        //displays error
        fetchError: function() { //handles error for fetching the reward history collection
            qcssapp.Functions.displayError('There was an error loading favorite products, please try again later.');
        },

        //displays the correct list based on what filter is chosen
        filterList: function(filter) {
            if($('.help-view').length) {

                if(filter == 'favorite' && !this.favoriteClicked) {
                    this.favoriteClicked = true;
                    return;
                }

                if(filter == 'order' && this.$orderList.hasClass('from-order-back'))  {
                    $('#favorite-list').addClass('list-hide');
                    $('#favorites-unavailable-separator').addClass('list-hide');
                    $('#favorites-unavailable').addClass('list-hide');
                    this.checkSliderRows(this.$favoriteList, false);
                }

                if(!this.orderListLoaded || !this.popularListLoaded) {
                    if(!this.orderListLoaded) {
                        $('#order-list').addClass('list-hide');
                        $('#orders-unavailable-separator').addClass('list-hide');
                        $('#order-unavailable-list').addClass('list-hide');
                        this.loadOrderCollection(true);
                    }

                    if(!this.popularListLoaded) {
                        $('#popular-list').addClass('list-hide');
                        this.loadPopularCollection(true);
                    }

                    return;
                }
                this.helpViewActive = true;
            }

            //Defect 4124: When loading a Quick Picks menu, if a section was filtered, it would not load images.
            //             This was because the "hideImages" CSS class was applied in the line view, but never removed.
            //             Here, we reset the imageLoaded status in the collection model, for each item in the list.
            //             When it is set to "true" later in the process, it will fire the loadImage method from the change listener.

            var targetModelArray;            //find the correct model collection to reset
            if      ( filter == 'popular' )  { targetModelArray = this.popularCollection.models; }
            else if ( filter == 'favorite' ) { targetModelArray = this.favoriteCollection.models; }
            else if ( filter == 'recent' )   { targetModelArray = this.recentCollection.models; }
            else if ( filter == 'order' )    { targetModelArray = this.orderCollection.models; }
            else                             { targetModelArray = []; }

            //reset the imageLoaded property for each item within the collection we found
            $.each(targetModelArray, function(index, curModel) {
                curModel.set('imageLoaded', false);
            })

            this.$container.find('.favorite-line-thumbnail-image').removeClass('fadeInImages');
            var $target = $('.favoriteBtn');
            this.$container.scrollTop(0);
            this.$el.find('.active-list').removeClass('active-list');

            this.$recentList.hide();
            this.$favoriteList.hide();
            this.$favoritesUnavailable.hide();
            this.$orderUnavailbleList.hide();
            this.$favoritesSeperator.hide();
            this.$ordersSeperator.hide();
            this.$popularList.hide();
            this.$orderList.hide();

            var preventMask = qcssapp.$activePanel[0].id === "product-detail-page";

            if ( filter == 'favorite' ) {
                this.loadFavoriteCollection(preventMask);
                this.loadUnavailableCollection(preventMask);
                this.resetFavoriteElements();
                // ensure the lists load again next time button is clicked
                this.popularListLoaded = false;
                this.orderListLoaded = false;

                this.$favoriteList.show().addClass('active-list');

                this.checkSliderRows(this.$favoriteList, !preventMask);

            } else if ( filter == 'popular' ) {
                if(!this.popularListLoaded) {
                    this.loadPopularCollection(preventMask);
                    this.resetFavoriteElements();
                    this.orderListLoaded = false;
                    $target = $('.popularBtn');
                    this.$popularList.show().addClass('active-list');
                } else {
                    $target = $('.popularBtn');
                    this.checkSliderRows(this.$popularList, true);
                }
                this.popularListLoaded = false

            } else if ( filter == 'recent' ) {
                if(!this.recentListLoaded) {
                    this.loadRecentCollection(false);

                    $target = $('.recentBtn');
                    this.$recentList.show().addClass('active-list');
                } else {
                    $target = $('.recentBtn');
                    this.checkSliderRows(this.$recentList, true);
                }

            } else if ( filter == 'order' ) {
                if(!this.orderListLoaded) {
                    this.loadOrderCollection(false);
                    this.resetFavoriteElements();
                    this.popularListLoaded = false;

                    $target = $('.ordersBtn');
                    this.$orderList.show().addClass('active-list');
                }
                if(this.unavailableOrders) {
                    this.$orderUnavailbleList.show();
                    this.$ordersSeperator.show();
                } else {
                    this.$orderUnavailbleList.hide();
                    this.$ordersSeperator.hide();
                }

                $target = $('.ordersBtn');
                this.checkSliderRows(this.$orderList, true);

            }

            this.$el.find('.active-list').scrollTop();
            $target.parent().find('.selected').removeClass('selected secondary-background-color font-color-secondary');
            $target.addClass('selected secondary-background-color font-color-secondary');
        },

        //displays notification for products that were recently removed from as favorites
        notifyInactivatedFavorites: function() {
            if(qcssapp.inactivatedFavorites.length > 0) {
                var message =  'Sorry, ' + qcssapp.inactivatedFavorites.length + ' product is no longer available and was removed from Favorites.';
                if(qcssapp.inactivatedFavorites.length > 1) {
                    message =  'Sorry, ' + qcssapp.inactivatedFavorites.length + ' products are no longer available and were removed from Favorites.';
                }

                setTimeout(function() {
                    $.toast({
                        text: message,
                        loader:false,
                        transition: 'slide',
                        position: 'bottom-center-stretch',
                        allowToastClose: false,
                        bgColor: '#4D4D4D',
                        hideAfter: 4000
                    });

                }, 1500);

                qcssapp.inactivatedFavorites = [];
            }
        },

        //determines whether to load more based on scroll position and loading status
        checkScroll: function(e) {
            var triggerPoint = 100; //100px from bottom of page
            var pageScroll = this.$container.scrollTop();
            var listHeight = this.$el.find('.active-list').height();
            var pageViewHeight = $('#my-quick-picks-page').height();
            var id = this.$el.find('.active-list').attr('id');
            var heightTotal = pageScroll + pageViewHeight + triggerPoint;

            this.checkListScroll(id, heightTotal, listHeight);
        },

        //determines if the active list needs to scroll
        checkListScroll: function(id, heightTotal, listHeight) {
            if ( id == "favorite-list" && !this.loading && !this.endOfListFav && heightTotal > (listHeight) ) {
                this.loading = true;
                this.currentPageFav += 1;

                //prevents collection from being called more than once, scroll bubbles up
                if(this.$favoriteList.attr('data-counter') != this.currentPageFav) {
                    this.$favoriteList.attr('data-counter', this.currentPageFav);
                    this.loadFavoriteCollection(true);
                }
            } else if ( id == "favorite-list" && !this.loading && this.endOfListFav && !this.endOfListUnav && heightTotal > (listHeight) ) {
                this.loading = true;
                this.currentPageUnav += 1;

                if(this.$favoritesUnavailable.attr('data-counter') != this.currentPageUnav) {
                    this.$favoritesUnavailable.attr('data-counter', this.currentPageUnav);
                    this.loadUnavailableCollection(true);
                }
            } else if ( id == "recent-list" && !this.loading && !this.endOfListRec && heightTotal > (listHeight) && this.recentListLoaded) {
                this.loading = true;
                this.currentPageRec += 1;

                if(this.$recentList.attr('data-counter') != this.currentPageRec) {
                    this.$recentList.attr('data-counter', this.currentPageRec);
                    this.loadRecentCollection(true);
                }
            } else if ( id == "popular-list" && !this.loading && !this.endOfListPop && heightTotal > (listHeight) && this.popularListLoaded) {
                this.loading = true;
                this.currentPagePop += 1;

                if(this.$popularList.attr('data-counter') != this.currentPagePop) {
                    this.$popularList.attr('data-counter', this.currentPagePop);
                    this.loadPopularCollection(true);
                }
            } else if ( id == "order-list" && !this.loading && !this.endOfListOrd && heightTotal > (listHeight) ) {
                this.loading = true;
                this.currentPageOrd += 1;

                //prevents collection from being called more than once, scroll bubbles up
                if(this.$orderList.attr('data-counter') != this.currentPageOrd) {
                    this.$orderList.attr('data-counter', this.currentPageOrd);
                    this.loadOrderCollection(true);
                }
            } else if ( id == "order-list" && !this.loading && this.endOfListOrd && !this.endOfListOrdUnav && heightTotal > (listHeight) ) {
                this.loading = true;
                this.currentPageOrdUnav += 1;

                if(this.$orderUnavailbleList.attr('data-counter') != this.currentPageOrdUnav) {
                    this.$orderUnavailbleList.attr('data-counter', this.currentPageOrdUnav);
                    this.loadOrderUnavailableCollection(true);
                }
            }
        },

        //check if list element has loaded all products it can
        checkEndOfList: function(id, collection, $listElement) {
            if ( id == "favorite-list" && collection.length < this.itemsPerPageFav ) {
                this.endOfListFav = true;
                if(!this.appendEndDiv) {
                    $listElement.append('<div class="favorites-list-end accent-color-one">End of Favorite Products List</div>');
                }
            } else if (id == "recent-list" && collection.length < this.itemsPerPageRec ) {
                this.endOfListRec = true;
                $listElement.append('<div class="favorites-list-end accent-color-one">End of Recent Products List</div>');

            } else if (id == "popular-list" && collection.length < this.itemsPerPagePop ) {
                this.endOfListPop = true;
                $listElement.append('<div class="favorites-list-end accent-color-one">End of Popular Products List</div>');

            } else if ( id == "order-list" && collection.length < this.itemsPerPageOrd ) {
                this.endOfListOrd = true;
                if(!this.appendEndOrdDiv) {
                    $listElement.append('<div class="favorites-list-end accent-color-one">End of Favorite Orders List</div>');
                }
            } else if ( id == "favorites-unavailable" && collection.length < this.itemsPerPageUnav ) {
                this.endOfListUnav = true;
                this.$favoriteList.find('.favorites-list-end').remove();
                $listElement.append('<div class="favorites-list-end accent-color-one">End of Favorite Products List</div>');
                this.appendEndDiv = true;

            } else if ( id == "order-unavailable-list" && collection.length < this.itemsPerPageOrdUnav ) {
                this.endOfListOrdUnav = true;
                this.$orderList.find('.favorites-list-end').remove();
                $listElement.append('<div class="favorites-list-end accent-color-one">End of Favorite Orders List</div>');
                this.appendEndOrdDiv = true;
            }

            if ( id == "favorites-unavailable" ) {
                if(this.appendEndDiv && !collection.isEmpty()) {
                    this.$favoriteList.find('.favorites-list-end').remove();
                }
            }

            if ( id == "orders-unavailable-list" ) {
                if(this.appendEndOrdDiv && !collection.isEmpty()) {
                    this.$orderList.find('.favorites-list-end').remove();
                }
            }
        },

        // check if the collection is empty, set variable and display message
        checkIfCollectionIsEmpty: function(id, collection, $listElement) {
            var isValid = true;
            if(id == 'favorite-list' && this.currentPageFav == 1 && collection.isEmpty()) {
                if(this.favoriteUnavailableCollection.isEmpty()) {
                    if(!$listElement.find('.no-products').length) {
                        $listElement.append('<ul class="no-products"><li id="no-favorites" class="favorites-list-end accent-color-one">No Products Found</li></ul>');
                    }
                    $listElement.removeClass('populated');
                    this.endOfListFav = true;
                    isValid = false;
                }
            }

            if(id == 'recent-list' && this.currentPageRec == 1 && collection.isEmpty()) {
                if(!$listElement.find('.no-products').length) {
                    $listElement.append('<ul class="no-products"><li id="no-favorites" class="favorites-list-end accent-color-one">No Products Found</li></ul>');
                }
                $listElement.removeClass('populated');
                this.endOfListRec = true;
                isValid = false;
            }

            if(id == 'popular-list' && this.currentPagePop == 1 && collection.isEmpty()) {
                if(!$listElement.find('.no-products').length) {
                    $listElement.append('<ul class="no-products"><li id="no-favorites" class="favorites-list-end accent-color-one">No Products Found</li></ul>');
                }
                $listElement.removeClass('populated');
                this.endOfListPop = true;
                isValid = false;
            }

            if(id == 'order-list' && this.currentPageOrd == 1 && collection.isEmpty()) {
                if(this.orderUnavailableCollection.isEmpty()) {
                    if(!$listElement.find('.no-products').length) {
                        $listElement.append('<ul class="no-products"><li id="no-favorites" class="favorites-list-end accent-color-one">No Orders Found</li></ul>');
                    }
                    $listElement.removeClass('populated');
                    this.endOfListOrd = true;
                    isValid = false;
                }
            }

            if( (id == 'favorites-unavailable' && this.currentPageUnav == 1) ) {
                if(this.favoriteCollection.isEmpty() && !collection.isEmpty()) {
                    this.$favoriteList.find('.no-products').remove();
                }
            }

            if( (id == 'orders-unavailable-list' && this.currentPageOrdUnav == 1) ) {
                if(this.orderCollection.isEmpty() && !collection.isEmpty()) {
                    this.$orderList.find('.no-products').remove();
                }
            }
            return isValid;
        },

        checkForHelpfulHints: function() {
            this.$helpIcon.addClass('hidden');
            this.$favoriteHeader.removeClass('help-favorite');

            if(qcssapp.help.allowHelpfulHints) {
                this.$helpIcon.removeClass('hidden');
                this.$favoriteHeader.addClass('help-favorite');
            }
        },

        checkSliderRows: function($listElement, showMask) {
            if(this.helpViewActive) {
                $listElement.show().addClass('active-list');
                showMask = false;
            }

            if(showMask) {
                qcssapp.Functions.showMask();

                $listElement.show().addClass('active-list');

                $.each($listElement.find('.slick-track'), function() {
                    if($(this).width() == 0) {
                        $(this).parent().parent().slick('slickGoTo',1);
                    }
                });

                qcssapp.Functions.hideMask();

            } else {

                $.each($listElement.find('.slick-track'), function() {
                    if($(this).width() == 0) {
                        $(this).parent().parent().slick('slickGoTo',1);
                    }
                });
            }
        },

        //resets the lists and hides non-favorite elements when navigating to and from this page
        resetFavoriteElements: function() {
            this.$favoriteList.children().remove();
            this.$favoritesUnavailable.children().remove();
            this.$orderUnavailbleList.children().remove();
            this.$recentList.children().remove();
            this.$popularList.children().remove();
            this.$orderList.children().remove();

            this.$favoriteList.show().attr('data-counter', "0");
            this.$favoritesUnavailable.show().attr('data-counter', "0");
            this.$favoritesSeperator.hide();
            this.$recentList.hide().attr('data-counter', "0");
            this.$popularList.hide().attr('data-counter', "0");
            this.$orderList.hide().attr('data-counter', "0");
            this.$orderUnavailbleList.hide().attr('data-counter', "0");
            this.$ordersSeperator.hide();

            var $orderElements = $('.ordersBtn, #orders-list');
            var $recentElements = $('.recentBtn, #recent-list');
            var $favoriteElement = $($('.favoriteBtn')[0]);
            var $itemElement = $($('.favoriteBtn')[1]);
            $orderElements.addClass('favorite-hidden');
            $itemElement.addClass('favorite-hidden');
            $recentElements.removeClass('favorite-hidden');
            $favoriteElement.removeClass('favorite-hidden');

            if( qcssapp.Functions.checkServerVersion(3,0) ) {
                $orderElements.removeClass('favorite-hidden');
                $itemElement.removeClass('favorite-hidden');
                $recentElements.addClass('favorite-hidden');
                $favoriteElement.addClass('favorite-hidden');
            }

            var $defaultBtn = $('.favoriteBtn');
            $defaultBtn.parent().find('.selected').removeClass('selected secondary-background-color font-color-secondary');
            $defaultBtn.addClass('selected secondary-background-color font-color-secondary');

            qcssapp.OrderMenuHistory[1].id = 1;

            if(typeof this.options.fromOrderBack == 'undefined') {
                $.toast({
                    text: 'Swipe right to add to cart. Swipe left to remove favorite ',
                    loader:false,
                    transition: 'slide',
                    position: 'bottom-center-stretch',
                    allowToastClose: false,
                    bgColor: '#4D4D4D',
                    hideAfter: 5000
                });
            } else {
                this.$orderList.addClass('from-order-back');
            }
        },

        //this is called once the image has been completely loaded
        loadImage: function(lineModel) {
            this.productLines++;
            var $productLine = this.$container.find(".hasImage[data-id='"+lineModel.attributes.id+"']:not(.finished)");
            var priority = $productLine.attr('priority');

            //if the image that was just loaded is the next one that needs to be filled, show it
            if(priority == this.nextOneFilled) {
                var $keypadLineImage = $productLine.parent().find('.favorite-line_image');

                $keypadLineImage.removeClass('hideImages');
                $keypadLineImage.addClass('fadeInImages');
                $productLine.addClass('finished');

                this.nextOneFilled++;

                //check if the next one that needs to be filled is a 'waiting' image
                var $waitingProductLines = this.$container.find(".line-content.waiting");
                if($waitingProductLines.length>0) {
                    this.checkWaitingImages(false);
                }

            } else {
                $productLine.addClass('waiting');
                this.checkWaitingImages(false);
            }

            //check if we've gone through all the images and there are still some 'waiting'
            var $waitingProductLinesEnd = this.$container.find(".line-content.waiting");
            if(this.productLines == this.$container.find(".line-content.hasImage").length &&  $waitingProductLinesEnd.length>0) {
                this.waitingCount = 0;
                this.checkWaitingImages(true);
                this.waitingLength = 0;
            }
        },

        //checks the 'waiting' images if their priority is the next one that needs to be loaded
        checkWaitingImages: function(atEnd) {
            var $waitingProductLine = this.$container.find(".line-content.waiting[priority='"+this.nextOneFilled+"']:not(.finished)");

            if($waitingProductLine.length == 1) {
                var $waitingProductLineImage = $waitingProductLine.parent().find('.favorite-line_image');

                if($waitingProductLine[0].id.indexOf('full') != -1) {
                    $waitingProductLineImage = $waitingProductLine.parent();
                }
                $waitingProductLineImage.removeClass('hideImages');
                $waitingProductLine.removeClass('waiting').addClass('finished');
                $waitingProductLineImage.addClass('fadeInImages');
                this.nextOneFilled++;
            }

            //if we've gone through all the images, look at the remainder in waiting and load them in order
            if(atEnd) {
                var $waitingProductLines = this.$container.find(".line-content.waiting");
                if($waitingProductLines.length > 0) {
                    if(this.waitingCount != $waitingProductLines.length) {
                        this.waitingCount = $waitingProductLines.length;
                        this.checkWaitingImages(true);
                    }
                }
            }
        },

        checkHistory: function() {
            if(!qcssapp.Functions.checkServerVersion(3,0)) {
                return false;
            }

            var lastPage = qcssapp.Controller.history[qcssapp.Controller.history.length - 1];
            if(lastPage == "FavoriteOrderView") {
                $('.ordersBtn').trigger('click');
                qcssapp.Controller.destroyViews('FavoriteOrderView');
                qcssapp.Controller.constructedViews = [];
                qcssapp.Controller.resetHistory();
                qcssapp.Controller.activeView = "";
                return true;
            }
            return false;
        },

        formatFavoriteList: function() {
            if(qcssapp.keypadPositionID == 1) {
                this.$el.removeClass('tile-favorites');
                return;
            }

            if(qcssapp.keypadPositionID == 2) {
                this.$el.addClass('tile-favorites');
            }
        }

    });
})(qcssapp, jQuery);