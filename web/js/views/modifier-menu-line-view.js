;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ModifierMenuLineView - View for displaying the modifier menu lines
     */
    qcssapp.Views.ModifierMenuLineView = Backbone.View.extend({
        internalName: "Modifier Menu Line View",

        el: '.modifier_container', //references existing HTML element for order review - can be referenced as this.$el throughout the view

        // Cache the template function for a single item.
        template: _.template( $('#modifier-menu-line-template').html() ),

        events: {
            'click .prep-option_container': 'clickPrepOption',
            'click .modifier_prep-option-preview-container-inner': 'expandPrepOptionList'
        },

        initialize: function() {
            this.model.set('modifierPrice', this.model.get('price'));
            this.model.set('showPrice', '');

            var price = this.model.get('price');

            if(Number(price) <= 0) {
                this.model.set('modifierPrice', '');
                this.model.set('showPrice', 'hidden');
            } else {
                price = qcssapp.Functions.formatPrice(Number(price), '+');
                this.model.set('modifierPrice', price);
            }

            this.setElement(this.template(this.model.toJSON()));

            this.$el.attr('data-id', this.model.get('id'));
            this.$el.attr('data-index', this.model.get('index'));

            this.$name = this.$el.find('.modifier_name');
            this.$infoContainer = this.$el.find('.modifier_info-container');
            this.$nutritionList = this.$el.find('.modifier_nutrition');
            this.$iconList = this.$el.find('.modifier_icons');
            this.$nutritionContainer = this.$el.find('.modifier_nutrition-container');
            this.$prepOptionList = this.$el.find('.modifier_prep-option-list');
            this.$prepOptionPreviewContainer = this.$el.find('.modifier_prep-option-preview-container');
            this.$prepOptionPreview = this.$el.find('.modifier_prep-option-preview');

            this.nutritionArray = qcssapp.Functions.buildNutritionModels(this.model);

            delete this.model.attributes['modifierPrice'];
            delete this.model.attributes['showPrice'];
        },

        //events: //events not needed for this view
        render: function() {
            this.setHealthyIcons();

            this.setNutrition();

            this.setPrepOptions();

            return this;
        },

        setNutrition: function() {
            this.model.set('nutritionArray', this.nutritionArray);

            var nutritionCount = 0;
            $.each(this.nutritionArray, function(index, nutritionModel) {
                if(nutritionModel != null) {
                    nutritionCount++;
                }
            });

            if(nutritionCount == 0) {
                this.$nutritionList.hide();
                return;
            }

            for( var x = 0; x < this.nutritionArray.length; x++) {
                var nutritionModel = this.nutritionArray[x];

                if(nutritionModel != null) {
                    var nutritionLineView = new qcssapp.Views.ProductModNutritionLineView({
                        model:nutritionModel
                    });

                    this.$nutritionList.append(nutritionLineView.render().el);
                }
            }

            this.$nutritionList.addClass('show-nutrition');
        },

        setHealthyIcons: function() {
            if(!qcssapp.Functions.checkServerVersion(3,0)) {
                return;
            }

            var healthyFlag = false;

            if(this.model.get('healthy')){
                healthyFlag = true;
                this.$iconList.append("<img src=\""+qcssapp.healthyIndicator.wellness+"\">");
            }
            if(this.model.get('vegetarian')){
                healthyFlag = true;
                this.$iconList.append("<img src=\""+qcssapp.healthyIndicator.vegetarian+"\">");
            }
            if(this.model.get('vegan')){
                healthyFlag = true;
                this.$iconList.append("<img src=\""+qcssapp.healthyIndicator.vegan+"\">");
            }
            if(this.model.get('glutenFree')){
                healthyFlag = true;
                this.$iconList.append("<img src=\""+qcssapp.healthyIndicator.glutenFree+"\">");
            }

            if(healthyFlag) {
                this.$iconList.addClass('show-icons');
            }

            if(healthyFlag && this.nutritionArray.length == 1) {
                this.$nutritionContainer.addClass('align-rows');
            }
        },

        setPrepOptions: function() {
            if(this.model.get('prepOptions').length == 0) {
                return;
            }

            this.prepOptionCollection = new qcssapp.Collections.PrepOptions();

            this.prepOptionCollection.set(this.model.get('prepOptions'));

            this.prepOptionCollection.each(function( prep ) {
                this.renderPrepOptionLine( prep );
            }, this );

            this.setDefaultPrepOption();
        },

        renderPrepOptionLine: function(prep) {
            var prepOptionLineView = new qcssapp.Views.ProductPrepOptionView({
                model:prep
            });

            this.$prepOptionList.append(prepOptionLineView.render().el);
        },

        setDefaultPrepOption: function() {

            if(this.model.get('prepOption') == null) {

                var defaultPrepModel = null;
                this.prepOptionCollection.each(function( prep ) {
                    if(prep.get('defaultOption')) {
                        defaultPrepModel = prep;
                        return false;
                    }

                    return true;
                }, this );

                if(defaultPrepModel != null) {
                    this.$prepOptionList.find('.default-prep').trigger('click');
                }

                if(this.model.get('defaultModifier')) {
                    this.model.set('prepOption', defaultPrepModel);
                }

            } else {
                var selectedPrepId = this.model.get('prepOption').get('id');
                this.$prepOptionList.find('.prep-option_container[data-id="'+selectedPrepId+'"]').trigger('click');
            }
        },

        clickPrepOption: function(e) {
            var $selectedPrep = $(e.currentTarget);

            var prepId = $selectedPrep.attr('data-id');
            var prepModel = null;

            this.prepOptionCollection.each(function( prep ) {

                // Find the model with the selected prep id
                if(Number(prepId) == Number(prep.get('id'))) {
                    prepModel = prep;
                    return false;
                }

                return true;
            }, this );

            this.selectPrepOption($selectedPrep, prepModel);

            if (document.contains(this.$prepOptionPreviewContainer[0])) {
                this.$prepOptionPreviewContainer.slideDown();
            } else {
                this.$prepOptionPreviewContainer.css('display', 'block');
            }
            this.$prepOptionList.removeClass('show-prep-list').slideUp();
        },

        selectPrepOption: function($prep, prepOptionModel) {
            if($prep.hasClass('selected-prep')) {
                return;
            }

            var prepName = prepOptionModel.get('name');
            var prepPrice = prepOptionModel.get('price');
            if(Number(prepPrice) > 0) {
                this.$prepOptionPreview.html(prepName + ' (' + qcssapp.Functions.formatPrice(prepPrice, '+') + ')');
            } else {
                this.$prepOptionPreview.html(prepName);
            }

            this.$prepOptionList.find('.prep-option_select').removeClass('accent-color-one-border');
            this.$prepOptionList.find('.prep-option_select-circle').removeClass('accent-color-one-background');
            this.$prepOptionList.find('.selected-prep').removeClass('selected-prep');

            $prep.addClass('selected-prep');

            if(qcssapp.usingBranding) {
                $prep.find('.prep-option_select').addClass('accent-color-one-border');
                $prep.find('.prep-option_select-circle').addClass('accent-color-one-background');
            }

            this.model.set('prepOption', prepOptionModel);
        },

        expandPrepOptionList: function() {
            if(this.$prepOptionList.hasClass('show-prep-list')) {
                this.$prepOptionList.removeClass('show-prep-list');
                this.$prepOptionList.slideUp();
                this.$prepOptionPreviewContainer.slideDown();
            } else {
                this.$prepOptionList.slideDown(function() {
                    this.$prepOptionList.addClass('show-prep-list');
                }.bind(this));
                this.$prepOptionPreviewContainer.slideUp();
            }
        }
    });
})(qcssapp, jQuery);
