(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.PrepOptionMenuView - View for displaying prep option menu
     */
    qcssapp.Views.PrepOptionMenuView = Backbone.View.extend({
        internalName: "Prep Option Menu View",

        el: '.prep-option-menu', //references existing HTML element for order review - can be referenced as this.$el throughout the view

        // Cache the template function for a single item.
        template: _.template( $('#product-prep-option-menu-template').html() ),

        events: {
            'click .prep-option-menu_header-container': 'expandMenu',
            'click .prep-option_container': 'clickPrepOption'
        },

        initialize: function(options) {

            this.prepOptionCollection = options.prepOptionCollection;

            // Default menu name to the {Product Name} Prep Options
            this.prepMenuName = this.model.get('name') + ' Prep Options';

            if(this.prepOptionCollection.length) {
                var prepOption = this.prepOptionCollection.models[0];
                this.prepMenuName = typeof prepOption.get('prepSetName') !== 'undefined' ? prepOption.get('prepSetName') : this.prepMenuName;
            }

            return this;
        },

        // Render the prep option menu view
        render: function() {

            var options = {
                id: this.model.get('prepOptionSetID'),
                name: this.prepMenuName
            };

            this.setElement(this.template(options));

            this.$prepOptionList = this.$el.find('.prep-option-menu_list');
            this.$headerContainer = this.$el.find('.prep-option-menu_header-container');
            this.$selectedPrepText = this.$el.find('.prep-option-menu_selected-prep');

            this.setPrepOptions();
            this.openMenu();

            return this;
        },

        // Loop over each prep option and create a view for each line
        setPrepOptions: function() {
            this.prepOptionCollection.each(function( prep ) {
                this.renderPrepOptionView( prep );
            }, this);
        },

        // Create each prep option line
        renderPrepOptionView: function( prep ) {
            var prepOptionView = new qcssapp.Views.ProductPrepOptionView({
                model:prep
            });

            this.$prepOptionList.append(prepOptionView.render().el);

            // If prep option is already selected, select that prep
            if(this.model.get('prepOption') != null) {
                var selectedPrepOptionID = this.model.get('prepOption').get('id');

                if(prep.get('id') == selectedPrepOptionID) {
                    this.selectPrepOption(prepOptionView.$el, prep);
                }

            // Otherwise select the default prep option
            } else  if( prep.get('defaultOption') ) {
                this.selectPrepOption(prepOptionView.$el, prep);
            }
        },

        // Finds the clicked prep option in the collection
        clickPrepOption: function(e) {
            var $selectedPrep = $(e.currentTarget);

            var prepId = $selectedPrep.attr('data-id');
            var prepModel = null;

            this.prepOptionCollection.each(function( prep ) {

                // Find the model with the selected prep id
                if(Number(prepId) == Number(prep.get('id'))) {
                    prepModel = prep;
                    return false;
                }

                return true;
            }, this );

            // Select the prep option
            this.selectPrepOption($selectedPrep, prepModel);

            this.closeMenu();
        },

        // Selects the prep option, updates colors
        selectPrepOption: function($prep, prepOptionModel) {
            if($prep.hasClass('selected-prep')) {
                return;
            }

            this.$prepOptionList.find('.prep-option_select').removeClass('accent-color-one-border');
            this.$prepOptionList.find('.prep-option_select-circle').removeClass('accent-color-one-background');
            this.$prepOptionList.find('.prep-option_detail-container').removeClass('accent-color-one');
            this.$prepOptionList.find('.selected-prep').removeClass('selected-prep');

            $prep.addClass('selected-prep');

            if(qcssapp.usingBranding) {
                $prep.find('.prep-option_select').addClass('accent-color-one-border');
                $prep.find('.prep-option_select-circle').addClass('accent-color-one-background');
                $prep.find('.prep-option_detail-container').addClass('accent-color-one');
            }

            // Triggers call to updateView() function on product-detail-view
            this.model.set('prepOption', prepOptionModel);

            // Update selected prep option text when menu is collapsed
            this.updateSelectedPrepText();
        },

        // Handles click event for expanding and collapsing the menu
        expandMenu: function() {
            // If the menu is collapsed, open it
            if( !this.$el.hasClass('expand-menu') ) {
                this.openMenu();
                return;
            }

            // Otherwise we need to collapse menu
            var headerTop = this.$headerContainer.position().top;
            var $productPage =  $('.product-detail_container');
            var pageScrollTop =  $productPage.scrollTop();

            // Scroll to where the menu top is to avoid clunky force scroll
            if(pageScrollTop > headerTop)  {
                $productPage.animate({scrollTop: headerTop});
            }

            // Collapse the menu
            this.closeMenu();
        },

        // Collapse the menu
        closeMenu: function() {
            this.$el.removeClass('expand-menu');
            this.$prepOptionList.slideUp(300);

            // Show the selected prep option text
            if(this.$selectedPrepText.text() != '') {
                this.$selectedPrepText.slideDown(300);
            }
            setTimeout(function() {
                qcssapp.Functions.checkScrollIndicator($('#product-detail-page').find('.scrollElement'))
            }, 300);
        },

        // Open the menu
        openMenu: function() {
            this.$el.addClass('expand-menu');
            this.$prepOptionList.slideDown(300);
            this.$selectedPrepText.slideUp(300);
            var _this = this;
            setTimeout(function(_this) {
                qcssapp.Functions.checkScrollIndicator(_this.$el.closest('.scrollElement'))
            }, 300, _this);
        },

        // Updates the selected prep option text that shows when the menu is closed
        updateSelectedPrepText: function() {
            var selectedPrepText = '';

            if(this.model.get('prepOption') != null) {
                var prepName = this.model.get('prepOption').get('name');
                var prepPrice = this.model.get('prepOption').get('price');

                // Always going to show selected prep name, even if it is a default
                selectedPrepText = prepName;

                // Only show price if it is greater than 0
                if(Number(prepPrice) > 0) {
                    selectedPrepText += ' (' + qcssapp.Functions.formatPrice(prepPrice, '+') + ')';
                }
            }

            this.$selectedPrepText.html(selectedPrepText);
        }
    });
})(qcssapp, jQuery);
