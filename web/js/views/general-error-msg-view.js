;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ErrorMsg - View for displaying Detail Collections
     */
    qcssapp.Views.ErrorMsg = Backbone.View.extend({
        internalName: "Error Msg View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div', //references existing HTML element for Detail Collections - can be referenced as this.$el throughout the view

        template: _.template( $('#general-error-msg-template').html() ),

        //events: //events not needed for this view

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in " + this.internalName);
            }

            //set options
            this.options = options;
            this.title = this.options.title;
            this.content = this.options.content;

            //create new model with given options
            this.model = new qcssapp.Models.ErrorMsg({
                title: this.title,
                content: this.content
            });

            return this;
        },

        render: function() {
            this.$el.html( this.template( this.model.attributes )).css('color','#ef4135');

            qcssapp.Functions.hideMask();
            return this;
        }
    });
})(qcssapp, jQuery);   