;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.StoreView - View for displaying Store Collections
     */
    qcssapp.Views.StoreView = Backbone.View.extend({
        //is a list tag.
        tagName:  'div',
        className: 'stores-list-item',

        // Cache the template function for a single item.
        template: _.template( $('#store-template').html() ),

        render: function() {
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            return this;
        }
    });
})(qcssapp, jQuery);
