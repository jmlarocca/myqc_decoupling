;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.SuggestiveSelling - View for Suggestive Selling Keypads
     */
    qcssapp.Views.SuggestiveSelling = Backbone.View.extend({
        internalName: "Suggestive Selling View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#suggestive-selling-page', //references existing HTML element for Suggestive Selling Keypads

        initialize: function(options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in suggestive selling view");
            }

            _.bindAll(this, 'formatSuggestiveKeypads');

            this.options = options || '';

            this.fromLocation = this.options.fromLocation || '';
            delete this.options.fromLocation;

            this.$suggestiveDots = this.$el.find('#suggestive-dots');
            this.$suggestionContainer = this.$el.find('#suggestion-container');
            this.$suggestionHeader = this.$el.find('#suggestive-header-msg');
            this.suggestionKeypadIndex = 0;

            this.$suggestiveDots.hide();
            this.$suggestiveDots.find('.dot').not(':first').remove();

            this.suggestionCollection = new qcssapp.Collections.SuggestiveSellingCollection();
            this.suggestionCollection.set(this.options);

            qcssapp.CurrentOrder.suggestionsAdded = false;

            this.render();

            //bind "this" reference to functions in the view
            _.bindAll(this, "checkScroll");

            //event didn't bubble properly for using the events, need to explicitly set the function trigger
            this.$el.on('scroll',this.checkScroll);
        },

        render: function() {
            if(qcssapp.CurrentOrder.storeModel && qcssapp.CurrentOrder.storeModel.get('upsellPageMessage')) {
                this.$suggestionHeader.text(qcssapp.Functions.htmlDecode(qcssapp.CurrentOrder.storeModel.get('upsellPageMessage')));
            }

            if(this.$suggestionContainer.find('#suggestive-keypadview').length) {
                this.$suggestionContainer.find('#suggestive-keypadview').find('.keypad-list').remove();
            }

            this.suggestionCollection.each(function( mod, index ) {
                this.renderLine(mod, index);

            }, this );

            this.formatSuggestiveKeypads();
        },

        renderLine: function( mod, index ) {
            var keypadView =  new qcssapp.Views.SuggestiveKeypadView(mod.attributes);

            this.$suggestionContainer.append(keypadView.el);

            var zIndex = this.suggestionCollection.length - index;
            keypadView.$keypadList.css('z-index', zIndex);

            if(this.suggestionCollection.length > 1 && this.$suggestiveDots.find('.dot').length != this.suggestionCollection.length) {
                this.$suggestiveDots.show();
                var $dot = this.$suggestiveDots.find('.dot').eq(0).clone();
                this.$suggestiveDots.append($dot);
            }
        },

        noThanks: function() {
            $('.scroll-indicator').removeClass('hidden');
            if(this.suggestionCollection.length > 1 && this.suggestionKeypadIndex != (this.suggestionCollection.length-1) ) {
                this.goToNextKeypad();
                return;
            }

            //after 'Add to Cart' on product view
            if(this.fromLocation == 'product' || qcssapp.CurrentOrder.suggestionsAdded) {
                qcssapp.Functions.showMask();

                setTimeout(function(){
                    this.checkPendingRequests();
                }.bind(this),1000);

            //after 'Checkout' on cart view
            } else {
                var upsellProfileKeypads = qcssapp.CurrentOrder.storeModel.get('upsellProfileKeypads');

                //get the id's of the keypads that have already been suggested
                var invalidSuggestiveKeypadIDs = qcssapp.CurrentOrder.suggestedKeypadIDs;

                // Add the remaining upsell profile keypads to the of invalid keypads so we don't show anymore
                $.each(upsellProfileKeypads, function() {
                    if( $.inArray(this.id, invalidSuggestiveKeypadIDs) == -1 ) {
                        invalidSuggestiveKeypadIDs.push(this.id);
                    }
                });

                qcssapp.Functions.checkForRewardsAndDiscounts();
            }

            this.$el.hide();
            this.$el.find('#suggestive-keypadview').hide();
        },

        goToNextKeypad: function() {
            var $keypadList = this.$suggestionContainer.find('.keypad-list');

            this.$suggestiveDots.find('.dot').eq( this.suggestionKeypadIndex ).removeClass('dot-active');
            $keypadList.eq(this.suggestionKeypadIndex).addClass('slide-keypad-out');

            //slide to the next keypad
            this.suggestionKeypadIndex++;

            //update the height of the container
            setTimeout(function() {

                var $keypad = $keypadList.eq(this.suggestionKeypadIndex);
                var keypadHeight = $keypad.outerHeight();
                if(this.$suggestionContainer.height() < keypadHeight) {
                    this.$suggestionContainer.height(keypadHeight);
                }

            }.bind(this), 200);

            $keypadList.removeClass('active-keypad');
            $keypadList.eq(this.suggestionKeypadIndex).addClass('active-keypad');

            //add next keypad to suggestedKeypadsID list
            qcssapp.CurrentOrder.suggestedKeypadIDs.push(this.suggestionCollection.models[this.suggestionKeypadIndex].get('id'));

            //update dots
            this.$suggestiveDots.find('.dot').eq( this.suggestionKeypadIndex ).addClass('dot-active');
        },

        formatSuggestiveKeypads: function() {
            var $suggestiveKeypadView = this.$suggestionContainer.find('#suggestive-keypadview');
            var $keypadList = $suggestiveKeypadView.find('.keypad-list');

            $suggestiveKeypadView.show();
            $keypadList.not(':first').addClass('hide-list');

            $keypadList.removeClass('active-keypad');
            $keypadList.eq(0).addClass('active-keypad');

            this.$suggestiveDots.find('.dot').removeClass('dot-active');
            this.$suggestiveDots.find('.dot').eq(0).addClass('dot-active');

            this.$suggestionContainer.css('height', 'auto');
        },

        checkPendingRequests: function() {
            if (qcssapp.CurrentOrder.suggestionsInProgress > 0) {
                setTimeout(function() {
                    this.checkPendingRequests();
                }.bind(this), 1000);

            } else {
                qcssapp.Functions.addHistoryRecord(0, 'cart');
                qcssapp.Router.navigate('cart', {trigger: true});
            }
        },


        checkScroll: function() {
            if ( (this.$el.scrollTop() + this.$el.innerHeight() ) >= this.$el[0].scrollHeight ) {
                $('.scroll-indicator').addClass('hidden');
            } else {
                $('.scroll-indicator').removeClass('hidden');
            }
        }
    });

    $(document).on('click', '#suggestive_button-next', function() {
        qcssapp.SuggestionView.noThanks();
    });

})(qcssapp, jQuery);
