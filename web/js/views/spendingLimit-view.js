;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.SpendingLimitView - View for displaying the global balance view
     */
    qcssapp.Views.SpendingLimitView = Backbone.View.extend({
        internalName: "Spending Limit View",

        // Cache the template function for a single item.
        template: _.template( $('#spending-limits-template').html() ),

        //events: //events not needed for this view

        render: function() {

            this.model.set('limitText', this.model.get('limit'));

            if(this.model.get('limit').indexOf('$') != -1) {
                var limit = this.model.get('limit').replace('$', '');
                this.model.set('limitText', qcssapp.Functions.formatPriceInApp(limit));
            }

            this.setElement( this.template( this.model.attributes ) );
            return this;
        }
    });
})(qcssapp, jQuery);
