;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.cartLineView - View for displaying Cart Collections
     */
    qcssapp.Views.cartLineView = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'cart-line',

        // Cache the template function for a single item.
        template: _.template( $('#cart-line-template').html() ),

        initialize: function() {
            return this;
        },

        render: function() {
            //format total to two decimals (and leading zero if below 1)
            this.model.set('total', Number(this.model.attributes.total).toFixed(2));
            this.model.set('info', qcssapp.Functions.formatPrice(this.model.get('total')));

            this.model.set('equation', '');
            this.model.set('showQuantity', 'hidden');
            this.model.set('showTareInfo', 'hidden');
            this.model.set('showModifiers', 'hidden');

            //if quantity more than one then show equation
            if ( this.model.attributes.quantity > 1 && !this.model.get('scaleUsed') ) {
                this.model.set('equation', this.model.get('quantity') + " <span class='quantity'>x</span> " + qcssapp.Functions.formatPrice(this.model.get('price')));
                this.model.set('showQuantity', '');
            } else if ( this.model.get('scaleUsed') && Number(this.model.get('quantity')) != 1 ) {
                this.model.set('equation', Number(this.model.get('quantity')).toFixed(2) + ' lbs at ' + qcssapp.Functions.formatPrice(this.model.get('originalPrice')));
                this.model.set('showQuantity', '');
            }

            if(this.model.get('tareInfo') != '' && this.model.get('grossWeightInfo') != '') {
                this.model.set('showTareInfo', '');
            }

            //if product has modifiers, create modifiers list csv
            if ( this.model.get('modifiers') && this.model.get('modifiers').length > 0 ) {
                this.model.set('modifierList', this.model.get('modifiers'));
                this.model.set('modifiers', qcssapp.Functions.convertModifierAndPrepOptionArrayToCsv(this.model.attributes.modifiers));
                this.model.set('showModifiers', '');
            }

            if(this.model.get('isCombo')) {
                this.$el.addClass('combo-line');

                //don't show the product's price or equation unless there is an upcharge
                this.model.set('total', '');
                this.model.set('info', '');
                this.model.set('equation', '');
            }

            var name = qcssapp.Functions.buildProductName(this.model);
            if(name != "") {
                this.model.set('name', name);
            }

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            //need to remove the '$' from the line if it is a combo line and has no upcharge
            if(this.model.get('isCombo') ) {
                this.$el.find('.cart-line-total').text('');
            }

            this.$el.attr('data-productID', this.model.attributes.productID).attr('data-cartProductID', this.model.attributes.id);

            this.setHealthyIcons();

            return this;
        },

        setHealthyIcons: function(){
            if(!qcssapp.Functions.checkServerVersion(3,0))
                return;

            var healthyFlag = false;

            if(this.model.get('healthy')){
                healthyFlag = true;
                var healthyPath = "<img src=\""+qcssapp.healthyIndicator.wellness+"\">";
                this.$el.find('.cart-line_icon-container').append(healthyPath);
            }
            if(this.model.get('vegetarian')){
                healthyFlag = true;
                var vegetarianPath = "<img src=\""+qcssapp.healthyIndicator.vegetarian+"\">";
                this.$el.find('.cart-line_icon-container').append(vegetarianPath);
            }
            if(this.model.get('vegan')){
                healthyFlag = true;
                var veganPath = "<img src=\""+qcssapp.healthyIndicator.vegan+"\">";
                this.$el.find('.cart-line_icon-container').append(veganPath);
            }
            if(this.model.get('glutenFree')){
                healthyFlag = true;
                var glutenFree = "<img src=\""+qcssapp.healthyIndicator.glutenFree+"\">";
                this.$el.find('.cart-line_icon-container').append(glutenFree);
            }
            if(healthyFlag){
                this.$el.find('.cart-line_icon-container').addClass('show-icons');

                var grayscalePercent = qcssapp.Functions.getIconGrayscalePercentage();
                if(grayscalePercent != '') {
                    this.$el.find('.cart-line_icon-container img').css('filter', 'grayscale('+grayscalePercent+'%)');
                }
            }
        }
    });
})(qcssapp, jQuery);
