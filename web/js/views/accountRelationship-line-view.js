(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.AccountRelationshipLineView - View for displaying individual account persons
     */
    qcssapp.Views.AccountRelationshipLineView = Backbone.View.extend({
        internalName: "Account Relationship Line View", //NOT A STANDARD BACKBONE PROPERTY

        // Cache the template function for a single item.
        template: _.template( $('#account-relationship-line-template').html() ),

        render: function() {
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.setElement( this.template( this.model ) );

            return this;
        }
    });
})(qcssapp, jQuery);
