;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.StoreLineView - View for displaying stores Collection for ordering
     */
    qcssapp.Views.StoreLineView = Backbone.View.extend({
        //is a list tag.
        tagName:  'li',
        className: 'template-gen orderStoreLine',

        // Cache the template function for a single item.
        template: _.template( $('#orderStore-template').html() ),

        render: function() {

            //don't show the wait times if placing an order in the future
            if( qcssapp.futureOrderDate != "" ) {
                this.model.set('pickupWaitTime', '');
                this.model.set('deliveryWaitTime', '');
            }

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            this.$el.attr('tid', this.model.get('terminalID'));

            if(this.model.get('pickupWaitTime') == '') {
                this.$el.find('.wait-time-pickup').addClass('hidden');
            }

            if(this.model.get('deliveryWaitTime') == '') {
                this.$el.find('.wait-time-delivery').addClass('hidden');
            }

            this.renderStoreImage();

            return this;
        },

        renderStoreImage: function() {
            if(!qcssapp.Functions.checkServerVersion(3, 0))
                return;
            if(this.model.get("tokenizedFileName")) {
                var imagePath = qcssapp.qcBaseURL + "/webimages/" + this.model.get("tokenizedFileName");
                qcssapp.onPhoneApp = (typeof window.cordova !== 'undefined');
                if(qcssapp.onPhoneApp) {
                    var hostName = qcssapp.Functions.get_hostname(qcssapp.Location);
                    imagePath = hostName + "/webimages/" + this.model.get("tokenizedFileName");
                }
                var that = this;
                $('<img/>').attr('src', imagePath).on('load', function() {
                    $(this).remove(); // prevent memory leaks as @benweet suggested

                    if(that.model.get('imagePosition') === 2) {
                        that.$el.addClass('store-image-full');
                        that.$el.css('background', 'url('+ imagePath +') no-repeat center center / cover');

                    } else {
                        that.$el.addClass('store-image-left');
                        that.$el.find('.store-icon img').attr({'src': imagePath, 'alt': 'Store Image'});
                    }
                });
            }

            if(this.model.get('imagePosition') === 2) {
                this.$el.attr('image-full', true);
            }
        }
    });
})(qcssapp, jQuery);
