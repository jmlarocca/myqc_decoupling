;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ExpressOrderLineView - View for displaying express order Collection for reordering
     */
    qcssapp.Views.ExpressOrderLineView = Backbone.View.extend({
        //is a list tag.
        tagName:  'div',
        className: 'template-gen express-order-line',

        events: {
            'click .express-btn': 'loadExpressReorder',
            'click .favorite-order-icon': 'handleFavOrderIcon',
            'click .expandEvent': 'expandItems'
        },

        // Cache the template function for a single item.
        template: _.template( $('#express-reorder-line-template').html() ),

        render: function() {

            this.usingDiningOptions = this.model.get('usingDiningOptions') && this.model.get('itemList').length > 1;

            var itemCount = 0;
            $.each(this.model.get('itemList'), function() { //get the number of items in the transaction based on quantity
                if(this.scaleUsed) {
                    itemCount += 1;
                } else {
                    itemCount += Number(this.quantity);
                }
            });

            if(this.usingDiningOptions) {  //subtract one item from the count if one of the products is a dining option product
                itemCount -= 1;
            }

            this.model.set('itemCount', itemCount + ' Items');
            if(itemCount == 1) {
                this.model.set('itemCount', itemCount + ' Item');
            }

            if(this.model.get('favoriteOrderActive') && this.model.get('favoriteOrderName') != '') {
                this.model.set('orderName', this.model.get('favoriteOrderName'));
            } else {
                var itemName = "";
                var item = this.model.get('itemList')[0];
                if(this.usingDiningOptions) {
                    item = this.model.get('itemList')[1];
                }

                itemName = item.name;

                if(typeof item.comboName !== 'undefined' && item.comboName != "") {
                    itemName = qcssapp.Functions.htmlDecode(item.comboName);
                }

                this.model.set('orderName', itemName);
            }

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            if(this.model.get('enableFavorites')) {
                this.$el.find('.favorite-order-icon').attr('src', 'images/icon-favorites-unchecked.svg');
                if(this.model.get('favoriteOrderActive')) {
                    this.$el.find('.favorite-order-icon').attr('src', 'images/icon-favorites.svg');
                }
            } else {
                this.$el.find('.favorite-order-icon').hide();
            }

            this.$itemList = this.$el.find('.express-order-items');

            $.each(this.model.get('itemList'), function(index){
                this.id = index;
            });

            this.itemCollection = new qcssapp.Collections.ExpressOrderItemCollection();
            this.itemCollection.set(this.model.get('itemList'));

            this.model.set('itemList', this.itemCollection);

            var activeComboTransLineItemId = null;

            this.itemCollection.each(function( mod ) {
                if(this.usingDiningOptions && mod.get('id') == 0) {
                    mod.set('diningOptionProduct', true);
                    return;
                }

                var isComboLine = (mod.get('comboTransLineItemId') != null && mod.get('comboTransLineItemId') != '');

                if(isComboLine && (activeComboTransLineItemId == null || activeComboTransLineItemId != mod.get('comboTransLineItemId')) ) {
                    activeComboTransLineItemId = mod.get('comboTransLineItemId');
                    this.renderComboLine( mod );
                }

                this.renderItemLine( mod, isComboLine );

            }, this );

            //if there is no phone on transaction then use landline phone
            if(this.model.get('phone') == '') {
                this.model.set('phone', qcssapp.accountModel.get('phone'));
            }

            return this;
        },

        renderItemLine: function( mod, isInCombo ) {

            if(isInCombo) {
                mod.set('isCombo', true);
            }

            var expressOrderItemView = new qcssapp.Views.ExpressOrderItemLineView({
                model: mod
            });

            this.$itemList.append( expressOrderItemView.render().el );
        },

        renderComboLine: function( mod ) {

            var comboBaseTotal = 0;
            var transLineItemID = mod.get('comboTransLineItemId');

            this.itemCollection.each(function( mod ) {
               if(mod.get('comboTransLineItemId') == transLineItemID) {
                   comboBaseTotal += Number(mod.get('price') - mod.get('basePrice')) + Number(mod.get('comboPrice'));
               }

            }, this );

            mod.set('comboBaseTotal', comboBaseTotal);

            var expressOrderComboView = new qcssapp.Views.ExpressOrderComboLineView({
                model: mod
            });

            this.$itemList.append( expressOrderComboView.render().el );
        },

        loadExpressReorder: function() {
            if(qcssapp.Functions.helpViewActive()) {
                return;
            }

            qcssapp.CurrentOrder.products = [];

            qcssapp.CurrentOrder.expressReorderModel = this.model.attributes;
            qcssapp.CurrentOrder.expressReorder = true;
            qcssapp.CurrentOrder.orderType = this.model.get('orderType');

            //add products to CurrentOrder object
            this.model.get('itemList').each(function ( mod ) {
                var productID = mod.attributes.productID;

                //create new product model
                var productModel = new qcssapp.Models.ProductDetail( mod.attributes, {parse: mod.attributes} );
                productModel.set('productID', productID);

                //add to CurrentOrder object
                qcssapp.Functions.addProductToCart(productModel);

            }, this );

            //if there is a dining option product, remove it from the cart's icon count
            if(this.usingDiningOptions) {
                var $cartNum = $('.cart-product-num');
                var productsInCart = Number($cartNum.eq(0).text()) - 1;
                $cartNum.text(productsInCart);
            }

            var storeModel = this.model.get('storeModel');
            var storeID = this.model.get('storeID');

            //set store for the transaction in the session model
            if ( storeModel != null && Number( storeModel.get('id') ) > 0 ) {

                //set the selected store
                qcssapp.Functions.setSession(qcssapp.Session, qcssapp.Location, storeID, qcssapp.UserCode);

                //if the store is using credit cards as a tender, get the account's balance and payment method
                if(qcssapp.Functions.checkServerVersion(3,0) && typeof storeModel.get('usesCreditCardAsTender') !== 'undefined' && storeModel.get('usesCreditCardAsTender')) {
                    var successParameters = {
                        storeModel: storeModel,
                        storeID: storeID
                    };

                    qcssapp.Functions.getAccountBalanceDetails(successParameters);

                } else {  //otherwise continue to keypad
                    qcssapp.Functions.addHistoryRecord(storeModel.get('homeKeypadID'), 'keypad');

                    qcssapp.CurrentOrder.storeModel = storeModel;
                    qcssapp.Functions.setStoreSettings();

                    qcssapp.Functions.checkForRewardsAndDiscounts();
                }

            } else {
                qcssapp.Functions.displayError("Error - there was an error retrieving the information for this store.");
            }
        },

        handleFavOrderIcon: function() {
            if(qcssapp.Functions.helpViewActive()) {
                return;
            }

            var favoriteID = this.model.get('favoriteOrderID');
            var id = this.model.get('id').toString();

            var callback = function(data, param) {
                if(typeof data['favoriteOrderId'] !== 'undefined') {
                    param.expressView.model.set('favoriteOrderID', data['favoriteOrderId']);
                }

                var $orderLine = $('#express-reorders').find('.slick-active');
                var $favoriteOrderIcon = $orderLine.find('.express-heart-icon').find('.favorite-order-icon');
                var src = $favoriteOrderIcon.attr('src') === 'images/icon-favorites-unchecked.svg' ? 'images/icon-favorites.svg' : 'images/icon-favorites-unchecked.svg';
                $favoriteOrderIcon.attr('src', src);

                //if removing a favorite order
                var orderName = "";
                if($favoriteOrderIcon.attr('src') == 'images/icon-favorites-unchecked.svg') {
                    orderName = param.expressView.model.get('itemList').get(0).get('name');

                    if(param.expressView.usingDiningOptions) {
                        orderName = param.expressView.model.get('itemList').get(1).get('name');
                    }

                    param.expressView.model.set('favoriteOrderActive', false);

                } else { //adding a favorite order
                    orderName = $('#favoriteName').val(); //name from modal

                    if(orderName == "" || typeof orderName == 'undefined') {
                        if(typeof param.orderName !== 'undefined' && param.orderName != "") {
                            orderName = param.orderName;
                        } else {
                            orderName = param.expressView.model.get('favoriteOrderName');
                            if(orderName == "") {  //get first product's name
                                orderName =  param.expressView.model.get('itemList').get(0).get('name');

                                if(param.expressView.usingDiningOptions) {
                                    orderName = param.expressView.model.get('itemList').get(1).get('name');
                                }
                            }
                        }
                    } else {
                        param.expressView.model.set('favoriteOrderName', orderName);
                    }

                    param.expressView.model.set('favoriteOrderActive', true);
                }

                param.expressView.model.set('orderName', orderName);
                $orderLine.find('.express-order-name').text(qcssapp.Functions.htmlDecode(orderName));

                qcssapp.CurrentOrder.products = [];
            };

            var callbackParams = {expressView:this, expressOrderID: this.model.get('id')};

            if(favoriteID != null) {
                qcssapp.Functions.toggleFavoriteOrder(favoriteID, callback, false, callbackParams);

            } else {

                var storeModel = this.model.get('storeModel');
                var storeID = this.model.get('storeID');

                //set store for the transaction in the session model
                if ( storeModel != null && Number( storeModel.get('id') ) > 0 ) {

                    //set the selected store
                    qcssapp.Functions.setSession(qcssapp.Session, qcssapp.Location, storeID, qcssapp.UserCode);
                }

                qcssapp.CurrentOrder.products = [];

                //add products to CurrentOrder object
                this.model.get('itemList').each(function ( mod ) {

                    //create new product model
                    var productModel = new qcssapp.Models.ProductDetail( mod.attributes, {parse: mod.attributes} );
                    productModel.set('id', productModel.get('productID'));

                    qcssapp.CurrentOrder.products.push(productModel);

                }, this );

                // Create new Favorite
                qcssapp.Functions.createFavoriteOrder(id, callback, callbackParams);
            }
        },

        expandItems: function() {
            var $expressItems =  this.$el.find('.express-order-items');
            var itemsHeight = 0;
            var currentHeight = this.$el['0'].clientHeight;
            var $expand = this.$el.find('.expand-container');

            if(!this.$el.attr('data-height')) {
                this.$el.attr('data-height', this.$el.height());
            }

            if(this.$el.hasClass('expanded')) {
                this.$el.removeClass('expanded');
                $expand.removeClass('expanded');
                itemsHeight = $expressItems['0'].clientHeight;
                $expressItems.hide();
                currentHeight = currentHeight - itemsHeight;

                if(this.$el.attr('data-height')) {
                    currentHeight = this.$el.attr('data-height');
                }

                this.$el.height(currentHeight)
            } else {
                this.$el.addClass('expanded');
                $expand.addClass('expanded');
                $expressItems.show();
                itemsHeight = $expressItems.height();

                if(this.$el.find('.express-order-store-name').height() > 25) {
                    itemsHeight = $expressItems['0'].clientHeight;
                }

                currentHeight += itemsHeight;
                this.$el.height(currentHeight)
            }

        }
    });
})(qcssapp, jQuery);
