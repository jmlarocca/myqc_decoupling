;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.GeneralMsg - View for displaying Detail Collections
     */
    qcssapp.Views.GeneralMsg = Backbone.View.extend({
        internalName: "General Msg View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#general-msg-view', //references existing HTML element for Detail Collections - can be referenced as this.$el throughout the view

        template: _.template( $('#general-msg-template').html() ),

        //events: //events not needed for this view

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in " + this.internalName);
            }

            //set options
            this.options = options;
            this.title = this.options.title;
            this.main = this.options.main;
            this.content = this.options.content;

            //create new model with given options
            this.model = new qcssapp.Models.GeneralMsg({
                title: this.title,
                main: this.main,
                content: this.content
            });

            //display view
            this.render();
        },

        render: function() {
            this.$el.html( this.template( this.model.attributes ) );
            qcssapp.Functions.hideMask();
            return this;
        }
    });
})(qcssapp, jQuery);   