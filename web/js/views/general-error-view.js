;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.GeneralMsg - View for displaying Detail Collections
     */
    qcssapp.Views.GeneralError = Backbone.View.extend({
        internalName: "General Error View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#general-error-view', //references existing HTML element for Detail Collections - can be referenced as this.$el throughout the view

        template: _.template( $('#general-error-template').html() ),

        //events: //events not needed for this view

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in " + this.internalName);
            }

            //set options
            this.options = options;
            this.content = this.options.content;

            //create new model with given options
            this.model = new qcssapp.Models.GeneralMsg({
                content: this.content
            });

            //display view
            this.render();
        },

        render: function() {
            this.$el.html( this.template( this.model.attributes ) );

            new qcssapp.Views.ButtonView({
                text: "Back",
                buttonType: "customCB",
                appendToID: "#general-error-btn-container",
                id: "generalErrorLogout",
                callback: function() {
                    if(qcssapp.Functions.fingerprintEnabled()) {
                        qcssapp.loggedOutFingerprint = true;
                    }
                    Backbone.history.history.back();
                }
            });

            qcssapp.Functions.hideMask();
            return this;
        }
    });
})(qcssapp, jQuery);   