;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.OrderStoreView - View for displaying Stores for ordering collection
     */
    qcssapp.Views.OrderStoreView = Backbone.View.extend({
        internalName: "Order Store View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#show-order', //references existing HTML element for Store Collections - can be referenced as this.$el throughout the view

        //template: //template not needed for this view

        events: {
            'click .orderMethodDelivery': function() {this.filter('delivery');},
            'click .orderMethodPickup': function() {this.filter('pickup');},
            'click .orderStoreLine': function(e) {this.goToStore(e);},
            'click #express-reorder-btn' : 'expressReorder'
        },

        initialize: function () {
            if (qcssapp.DebugMode) {
                console.log("initialize in order store view");
            }

            //bind "this" reference to functions in the view
            _.bindAll(this, "fetchSuccess", "fetchError", "filter");

            this.$availableList = this.$el.find('#stores-available');
            this.$unavailableList = this.$el.find('#stores-unavailable');
            this.$storeSeparator = $('#stores-unavailable-separator');
            this.$expressReorders = $('#express-reorders');
            this.$expressElements = $('#express-reorders, #express-reorders-btn-container, #express-arrow-left, #express-arrow-right, #express-dots');
            this.$calendarIcon = $('.calendar-icon');
            this.$orderHelpIcon = this.$el.find('.help-icon');

            //create new collection
            this.collection = new qcssapp.Collections.OrderStoreCollection();

            this.expressOrderCollection = new qcssapp.Collections.ExpressOrderCollection();

            //populates the new collection
            this.loadCollection();
        },

        loadCollection: function() {
            var preventHideMask = false;

            //if coming from the Receipt View from OrderAgain, don't hide mask yet
            if(this.fromReceipt || qcssapp.Functions.checkServerVersion(3,0)) {
                preventHideMask = true;
                qcssapp.Controller.resetHistory();
            }

            var successParameters = {
                view : this
            };

            var errorParameters = {
                view: this
            };

            var endpoint = qcssapp.Location + '/api/ordering/stores';

            var date = "";

            if(qcssapp.futureOrderDate) {
                date = qcssapp.futureOrderDate.replace("/", "-");
                date = date.replace("/", "-");
                endpoint = qcssapp.Location + '/api/ordering/stores/' + date;
            }

            var locationId = qcssapp.accountModel.get('orgLevelPrimaryData').orgLevelPrimaryId;

            if( locationId != "0" &&
                locationId != "" &&
                qcssapp.locationFilterSettings.myQCAllowOnlineOrderLocation &&
                qcssapp.Functions.checkServerVersion(6,1,1)
            ){
                endpoint = qcssapp.Location + '/api/ordering/stores/location/' + locationId + "/" + (date != "" ? date : "null");
            }

            qcssapp.Functions.callAPI(endpoint, 'GET', '', successParameters,
                function(data, successParameters) {
                    successParameters.view.collection.set(data, {parse: true});

                    successParameters.view.fetchSuccess(data);
                },
                function(errorParameters) {
                    errorParameters.view.fetchError()
                },
                '',
                errorParameters,
                30000,
                false,
                preventHideMask,
                true
            )
        },

        fetchError: function() { //handles error for fetching the store collection
            if (qcssapp.DebugMode) {
                console.log("Error loading data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            qcssapp.Functions.hideMask();
        },

        fetchSuccess: function(data) { //handles successful fetching the store collection
            if (qcssapp.DebugMode) {
                console.log("Successfully loaded data for " + this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            this.$expressElements.hide();
            this.$calendarIcon.hide();
            this.$unavailableList.empty();

            if ( data.length == 0 ) {
                this.filter('pickup');
                this.$unavailableList.append('<li class="align-center italic">No Stores Found.</li>');
                this.$storeSeparator.hide();
                qcssapp.Functions.hideMask();

                if(qcssapp.help.allowHelpfulHints) {
                    this.$orderHelpIcon.addClass('hidden');
                }
                this.checkShouldShowLocationFilter(); // show location filter

                return false; //no point in rendering nothing
            }

            //display view
            this.render();

            this.formatStorePosition();

            var defaultFilter = $('.pickupOpen').length === 0 ? 'delivery' : 'pickup';
            this.filter(qcssapp.locationFilterSettings.orderMethod || defaultFilter);

            var $storeList = $('#store-list');
            $storeList.css('overflow-y', 'hidden');
            $storeList.height('');

            var $orderView = $('#orderview');
            var $showOrder = $('#show-order');
            var $orderHeader = $orderView.find('h1');
            var $homeLink = $('#orderview .home-link');

            var storeText = typeof $showOrder.attr('data-text') !== 'undefined' ?  $showOrder.attr('data-text') : 'Select Store';

            $orderHeader.text(storeText);
            $orderHeader.removeClass('store-header-shrink');
            $homeLink.removeClass('home-icon-shrink');

            //load express reorders
            if(qcssapp.Functions.checkServerVersion(3,0) && !qcssapp.CurrentOrder.expressReorder && !this.fromReceipt) {
                this.loadExpressOrderTransactions();
            }

            //determine which stores are available for future orders
            if(qcssapp.Functions.checkServerVersion(4,0)) {
                this.checkForFutureOrderStores();
            }

            if(qcssapp.Functions.checkServerVersion(6,1,1)){
                this.checkShouldShowLocationFilter();
            }
        },

        //calls renderLine on each store
        render: function() {
            this.collection.each(function( mod ) {
                this.renderLine( mod );
            }, this );

            this.fromReceipt = !!(qcssapp.Controller.activeView == 'ReceiptView');
            qcssapp.Controller.activeView = "";

            if ( this.$unavailableList.find('li').length == 0 ) {
                this.$storeSeparator.hide();
            }

            //show or hide method filter container based on if both pickup and delivery are available
            var $filterContainer = $('.orderMethodFilterContainer');
            if ( $('.pickupOpen').length > 0 && $('.deliveryOpen').length > 0 ) {
                // D-4280: without this, the filter would always be hidden after it was hidden the first time
                $filterContainer.slideDown();
            } else {
                $filterContainer.hide();
            }
        },

        //builds new individual store view and appends to this list view
        renderLine: function( mod ) {
            var now = moment();

            if ( mod.get('currentStoreTime') && moment(mod.get('currentStoreTime'), 'HH:mm:ss.SSS').isValid()) {
                mod.set('clientTimezoneOffset', moment(mod.get('currentStoreTime'), 'HH:mm:ss.SSS').diff(now, 'seconds'));
                now = now.add(mod.get('clientTimezoneOffset'),'s');
                if(qcssapp.CurrentOrder.storeModel != "") {
                    qcssapp.CurrentOrder.storeModel.set('clientTimezoneOffset', mod.get('clientTimezoneOffset'));
                }
            }

            var storeView = new qcssapp.Views.StoreLineView({
                model: mod
            });

            var pickup = qcssapp.Functions.checkStoreOpenForPickUp(mod, now);

            if ( pickup ) {
                storeView.$el.addClass('pickupOpen');

                var pickupCloseTime = moment(mod.get('pickUpOrderingCloseTime'), "hh:mm A");

                var minUntilStoreClose = qcssapp.Functions.checkServerVersion(3,0) ? mod.get('numMinutesUntilStoreClose') : 60;
                //add closing time warning to these stores
                if(minUntilStoreClose !== 0) {

                    if ( pickupCloseTime.subtract(minUntilStoreClose, 'm').diff(now) < 0 ) {
                        storeView.$el.addClass('closingSoon');
                    }
                }
            }

            var delivery = qcssapp.Functions.checkStoreOpenForDelivery(mod, now);

            if ( delivery ) {
                storeView.$el.addClass('deliveryOpen');
            }

            //to determine where to append the storeLine view
            if ( pickup || delivery ) {
                this.$availableList.append( storeView.render().el );
                storeView.$el.addClass('open');
                mod.set('available', true);

                if( mod.get('imagePosition') == 2 && mod.get('tokenizedFileName') != '' ) {
                    var $lastAvailable = this.$availableList.find('.orderStoreLine:nth-last-of-type(2)');
                    if($lastAvailable.attr('image-full') && qcssapp.keypadPositionID == 1) {
                        storeView.$el.addClass('store-image-full-border');
                    }
                }

            } else {
                this.$unavailableList.append( storeView.render().el );
                storeView.$el.addClass('closed');
                mod.set('available', false);

                if( mod.get('imagePosition') == 2 && mod.get('tokenizedFileName') != '' ) {
                    var $lastUnavailable = this.$unavailableList.find('.orderStoreLine:nth-last-of-type(2)');
                    if($lastUnavailable.attr('image-full') && qcssapp.keypadPositionID == 1) {
                        storeView.$el.addClass('store-image-full-border');
                    }
                }

            }
        },

        loadExpressOrderTransactions: function() {
            var storeIDArr = [];
            this.$availableList.find('li').each(function( mod ) {
                var storeID = $(this).find('.store-info-container').attr('data-storeID');
                storeIDArr.push(storeID);
            }, this );

            var storeIDs = storeIDArr.join(',');

            //if no storeIDs then return
            if(storeIDs == "") {
                this.$expressElements.hide();
                qcssapp.Functions.hideMask();

                if(qcssapp.help.allowHelpfulHints) {
                    this.$orderHelpIcon.addClass('hidden');
                }
                return;
            }

            var endPoint = qcssapp.Location + '/api/ordering/express/orders/'+ storeIDs;
            var successParameters = {
                view : this
            };

            qcssapp.Functions.callAPI(endPoint, 'GET', '', successParameters,
                function(data, successParameters) {
                    successParameters.view.fetchOrdersSuccess(data);
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Failed to load data for express reorders");
                    }
                },
                '', '', 30000, false, false, true, '', true
            )
        },

        fetchOrdersSuccess: function(data) { //handles successful fetching the store collection
            if (qcssapp.DebugMode) {
                console.log("Successfully loaded data for express reorders");
            }

            if(qcssapp.help.allowHelpfulHints && (this.futureOrderAvailable || data.length > 0) ) {
                this.$orderHelpIcon.removeClass('hidden');
            } else {
                this.$orderHelpIcon.addClass('hidden');
            }

            //no point in rendering nothing
            if ( data.length == 0 ) {
                return false;
            }

            this.expressOrderCollection.set(data);

            this.$expressElements.addClass('fadeOut').show();

            //display express orders
            this.renderExpressOrders();

            this.$expressElements.addClass('fadeIn');
        },

        renderExpressOrders: function() {
            var lineID = 0;
            var $expressReorders = $('#express-reorders');

            //remove slick properties from list
            if( $expressReorders.find('.slick-list').length) {
                $expressReorders.empty();
                $expressReorders.removeClass('slick-initialized slick-slider');
                $('#express-arrow-right').insertAfter('#express-reorders');
                $('#express-arrow-left').insertAfter('#express-reorders');
                $('.slick-arrow').remove();
            }

            $('#express-dots .dot').not(':first').remove();

            this.expressOrderCollection.each(function( mod ) {
                mod.set('lineID', lineID);
                this.renderExpressOrderLine( mod );
                lineID ++;
            }, this );

            $expressReorders.slick({
                centerPadding:'50px'
            });

            this.formatExpressReorder();
        },

        renderExpressOrderLine: function( mod ) {
            var storeModel = this.collection.get(mod.get('storeID'));
            mod.set('storeModel', storeModel);

            var expressOrderView = new qcssapp.Views.ExpressOrderLineView({
                model: mod
            });

            this.$expressReorders.append( expressOrderView.render().el );

            if(mod.get('lineID') > 0) {
                var $expressDots = $('#express-dots');
                var $dot = $expressDots.find('.dot').eq(0).clone();
                $expressDots.append($dot);
            }
        },

        //trigger button click for express order line model
        expressReorder: function() {
            var $order = this.$expressReorders.find('.slick-active');
            $order.find('.express-btn').trigger('click');
        },

        formatExpressReorder: function() {
            var $slickPrev = $('.slick-prev');
            var $slickNext = $('.slick-next');
            $slickPrev.text('').insertAfter('#express-arrow-left');
            $slickPrev.append($('#express-arrow-left'));
            $slickNext.text('').insertAfter('#express-arrow-right');
            $slickNext.append($('#express-arrow-right'));

            var $page = $('#show-order');
            var $storeList = $('#store-list');
            var $expressReorders = $('#express-reorders');
            var $orderTypeFilter = $('.orderMethodFilterContainer');
            var $expressDots = $('#express-dots');

            //determine the height available for the store list

            setTimeout(function(){
                var expressOrderOffset = $expressReorders.offset().top;
                var storeListOffset = $storeList.offset().top;

                if($orderTypeFilter.height() < 20) {
                    storeListOffset  = storeListOffset = $orderTypeFilter["0"].scrollHeight + 95;
                }

                var listHeight = (expressOrderOffset - 5) - storeListOffset;

                //don't show scroll bar if the stores don't overlap the express order div
                if($('#stores-available').height() + $('#stores-unavailable').height() > listHeight ) {
                    $storeList.css('overflow-y', 'scroll');
                    $storeList.height(listHeight);
                } else {
                    $storeList.css('overflow-y', 'hidden');
                    $storeList.height('');
                }

                var blockWidth = $expressReorders.outerWidth();
                $('#express-reorders-btn-container').width(blockWidth);

            }, 200);

            if(!qcssapp.usingBranding) {
                $('#express-reorder-btn svg').hide();
            }

            //hide the arrows if there is only one transaction
            if( this.expressOrderCollection.length == 1 ) {
                $('#express-arrow-left, #express-arrow-right').hide();
            }

            if(qcssapp.onPhoneApp) {
                $('.express-order-name').css('padding-bottom','2px');
                $('.express-order-items').css('padding','0 0');
                $('#express-arrow-right').css('margin-left', '30px;')
            }

            $expressDots.find('.dot').removeClass('dot-active');
            $expressDots.find('.dot').eq(0).addClass('dot-active');
        },

        filter: function(filter) {
            this.$availableList.find('li.none-found').remove();

            var $target = this.$el.find('.orderMethodPickup');
            var $closingSoonMsgs = $(".closingSoon .closingSoonMsg");

            if ( filter == 'delivery' ) {
                $target = this.$el.find('.orderMethodDelivery');

                $closingSoonMsgs.hide();

                //move stores that don't deliver to unavailable
                this.$availableList.find('li').not('.deliveryOpen').appendTo(this.$unavailableList);

                //move stores that deliver to available
                this.$unavailableList.find('.deliveryOpen').appendTo(this.$availableList);

                //set the order type
                qcssapp.CurrentOrder.orderType="delivery";
            } else if ( filter == 'pickup' ) {
                $closingSoonMsgs.show();

                //move stores that don't do pickup to unavailable
                this.$availableList.find('li').not('.pickupOpen').appendTo(this.$unavailableList);
                //move stores that pickup to available
                this.$unavailableList.find('.pickupOpen').appendTo(this.$availableList);

                //set the order type
                qcssapp.CurrentOrder.orderType="pickup";
            }

            this.$el.find('.selected').removeClass('selected secondary-background-color font-color-secondary');
            $target.addClass('selected secondary-background-color font-color-secondary');

            if ( qcssapp.usingBranding ) {
                $target.siblings().css('backgroundColor','white');
                $target.siblings().css('color','black');
            }

            if ( this.$availableList.find('li').length == 0 ) {
                this.$availableList.append('<li class="align-center italic none-found">No Stores Currently Available.</li>');
            } else {
                var lastStore = this.$availableList.find('li').last();
                lastStore.css({'border-bottom': '1px solid #ccc'});
            }

            if ( this.$unavailableList.find('li').length == 0 ) {
                this.$storeSeparator.hide();
            } else {
                this.$storeSeparator.show();
            }
        },

        goToStore: function(e) {
            qcssapp.Functions.resetErrorMsgs();

            var $target = $(e.currentTarget);

            if ( $target.parent().attr('id') == "stores-unavailable" || $target.hasClass('none-found') ) {
                return false;
            }

            this.setOrderType($target);

            var $targetInfo = $target.find('.store-info-container');
            var storeID = $targetInfo.attr('data-storeID');

            var storeModel = this.collection.get(storeID);

            if ( storeModel != null && Number( storeModel.get('id') ) > 0 ) {

                // Set the selected store
                qcssapp.Functions.setSession(qcssapp.Session, qcssapp.Location, storeID, qcssapp.UserCode);

                // Get the account's balance and payment method
                if(qcssapp.Functions.checkServerVersion(3,0)) {
                    var successParameters = {
                        view: this
                    };
                    var errorParameters = {
                        view: this
                    };

                    qcssapp.Functions.callAPI(qcssapp.Location + '/api/ordering/store/' + storeID, 'GET', '', successParameters,
                        function(data, successParameters) {
                            successParameters.view.collection.remove(storeModel);
                            successParameters.view.collection.add(data);
                            storeModel = successParameters.view.collection.get(storeID);
                            successParameters = {
                                storeModel: storeModel,
                                storeID: storeID
                            };

                            qcssapp.Functions.getAccountBalanceDetails(successParameters);
                        },
                        function(errorParameters) {
                            errorParameters.view.fetchError()
                        },
                        '',
                        errorParameters,
                        30000,
                        false,
                        true,
                        true
                    )
                } else {  //otherwise continue to keypad
                    var successParameters = {
                        view: this
                    };
                    var errorParameters = {
                        view: this
                    };

                    qcssapp.Functions.callAPI(qcssapp.Location + '/api/ordering/store/' + storeID, 'GET', '', successParameters,
                        function(data, successParameters) {
                            successParameters.view.collection.remove(storeModel);
                            successParameters.view.collection.add(data);
                            storeModel = successParameters.view.collection.get(storeID);
                            qcssapp.CurrentOrder.storeModel = storeModel;
                            qcssapp.Functions.setStoreSettings();

                            var homeKeypadID = storeModel.get('homeKeypadID');
                            qcssapp.Functions.loadKeypad(homeKeypadID);
                        },
                        function(errorParameters) {
                            errorParameters.view.fetchError()
                        },
                        '',
                        errorParameters,
                        30000,
                        false,
                        true,
                        true
                    )
                }

            } else {
                qcssapp.Functions.displayError("Error - there was an error retrieving the information for this store.");
            }
        },

        setOrderType: function($target) {
            if(qcssapp.CurrentOrder.orderType != "") {
                return;
            }

            if ( $target.hasClass('pickupOpen') && $target.hasClass('deliveryOpen') && $('.orderMethodPickup').hasClass('selected') ) {
                qcssapp.CurrentOrder.orderType = "pickup";
            } else if ( $target.hasClass('pickupOpen') && $target.hasClass('deliveryOpen') && $('.orderMethodDelivery').hasClass('selected') ) {
                qcssapp.CurrentOrder.orderType = "delivery";
            } else if ( $target.hasClass('pickupOpen') && !$target.hasClass('deliveryOpen') ) {
                qcssapp.CurrentOrder.orderType = "pickup";
            } else if ( $target.hasClass('deliveryOpen') && !$target.hasClass('pickupOpen') ) {
                qcssapp.CurrentOrder.orderType = "delivery";
            }

        },

        checkForFutureOrderStores: function() {
            var $orderView = $('#orderview');
            var $orderHeaderContainer = $orderView.find('#order-header');
            var $orderHeader = $orderView.find('h1');
            var $homeLink = $('#orderview .home-link');
            var $showOrder = $('#show-order');

            var storeText = typeof $showOrder.attr('data-text') !== 'undefined' ?  $showOrder.attr('data-text') : 'Select Store';

            $orderHeader.text(storeText);
            $orderHeaderContainer.removeClass('help-store');

            var futureOrderAvailable = false;
            this.collection.each(function( mod ) {
                if(Number(mod.get('maxNumDaysForFutureOrders')) > 0) {
                    futureOrderAvailable = true;
                    return false;
                }
            }, this );

            this.futureOrderAvailable = futureOrderAvailable;

            if(futureOrderAvailable) {
                $orderHeader.html('Ordering for Today');
                $orderHeader.removeClass('store-header-shrink');
                $homeLink.removeClass('home-icon-shrink');
                this.$calendarIcon.show();

                if(qcssapp.usingBranding) {
                    $('.calendar').hide();
                }

                if(navigator.userAgent.match(/Android/i)) {
                    $('.calendar-num').css({'font-size':'17px', 'top':'51px'});
                }

                if(qcssapp.help.allowHelpfulHints) {
                    $orderHeaderContainer.addClass('help-store');
                }
            }

            if(qcssapp.futureOrderDate) {
                qcssapp.Functions.updateOrderingDayName();
            }

            qcssapp.Functions.updateCalendarIconNum();
        },

        formatStorePosition: function() {
            if(qcssapp.keypadPositionID == 1) {
                this.$el.removeClass('tile-stores');
                return;
            }

            if(qcssapp.keypadPositionID == 2) {
                this.$el.addClass('tile-stores');
            }
        },

        checkShouldShowLocationFilter: function() {

            if(qcssapp.locationFilterSettings.myQCAllowOnlineOrderLocation){
                // show the order type in the location selector modal instead of with the stores
                $('.orderMethodFilterContainer').hide();

                $('.location-select-container').css({'display': 'flex', 'height': 'fit-content'});

                var that = this;

                // after a valid selection
                var onSelectLocation = function(){
                    qcssapp.Functions.resetOrdering(false, false);

                    $.each($('#store-list').find('.orderStoreLine'), function(){
                        $(this).remove();
                    });

                    that.loadCollection();

                    that.setLocationSelector();
                };

                this.setLocationSelector();

                $('.location-select').off();

                $('.location-select').on('click', function(){
                    qcssapp.Functions.showMask(false, true);
                    new qcssapp.Views.FilteringOptionsView({
                        applyCallback: onSelectLocation
                    });
                    qcssapp.Router.navigate('filtering-options', {trigger: true});
                });

                // force them to pick a location by opening the modal immediately, defaults to the first location
                // will happen if they have not picked a location and cannot skip OR
                // they have not picked a location and myQCPromptBeforeOnlineOrder is true
                if( // if nothing was yet selected and the user is not allowed to skip
                    ( !qcssapp.locationFilterSettings.myQCAllowOnlineOrderLocationSkip
                        && ( $('.location-select').attr('data-OrgLevelId') == "0" || !qcssapp.accountModel.get('orgLevelPrimaryData').orgLevelPrimaryId )
                    )
                    || // if the app is to prompt the user and nothing has been selected, and they were not prompted this session
                    (qcssapp.locationFilterSettings.myQCPromptBeforeOnlineOrder
                        && ( $('.location-select').attr('data-OrgLevelId') == "0" || !qcssapp.accountModel.get('orgLevelPrimaryData').orgLevelPrimaryId )
                        && !qcssapp.locationFilterSettings.userWasPromptedForLocation )
                ) {
                    $('.location-select').trigger('click');
                }

            }

        },

        setLocationSelector: function(){
            var $locationSelector = $('.location-select');
            var orgLevelData = qcssapp.accountModel.get('orgLevelPrimaryData');

            var locationSelectorHTML = `Ordering `;
            if (qcssapp.locationFilterSettings.orderMethod == 'delivery') {
                locationSelectorHTML += `<strong class="location-select-text-large accent-color-one">Delivery</strong> at `;
            } else {
                locationSelectorHTML += `<strong class="location-select-text-large accent-color-one">Pickup</strong> at `;
            }

            locationSelectorHTML += `<strong class="location-select-text-large accent-color-one">${orgLevelData.name}</strong>`;

            $locationSelector.html(`<p class="location-select-text">${locationSelectorHTML}</p>`);
            $locationSelector.attr('data-OrgLevelId', orgLevelData.orgLevelPrimaryId);
        }
    });

    $(document).on('click', '.calendar-icon', function() {
        if(qcssapp.Functions.helpViewActive()) {
            return;
        }

        qcssapp.Functions.showMask();
        new qcssapp.Views.FutureDateView();
        qcssapp.Router.navigate('show-date', {trigger: true});
    });

})(qcssapp, jQuery);