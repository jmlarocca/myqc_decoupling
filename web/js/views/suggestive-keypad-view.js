;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.KeypadView - View for displaying Keypads
     */
    qcssapp.Views.SuggestiveKeypadView = Backbone.View.extend({
        internalName: "Suggestive Keypad View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#suggestive-keypadview', //references existing HTML element for Keypads - can be referenced as this.$el throughout the view

        template: _.template( $('#suggestive-keypad-template').html() ),

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in keypad view");
            }

            qcssapp.Functions.resetErrorMsgs();

            this.options = options || '';

            this.keypadItemCollection = new qcssapp.Collections.KeypadItems();

            this.keypadItemCollection.set(this.options.items);

            this.listenTo(this.keypadItemCollection, 'change:imageLoaded', this.loadImage);

            this.render();

            this.nextOneFilled = 0;
            this.keypadLines = 0;

            var count = 0;
            $.each(this.$keypadList.find('.suggestive-keypad-line'), function() {
                if($(this).hasClass('hasImage')) {
                    $(this).attr('priority', count);
                    count++;
                }
                // hide the image container if the image is hidden
                else {
                    $(this).find('.line_image-container').addClass('hidden');
                }
            });
        },

        //creates keypad page in keypadview, calls renderLine on each keypad item
        render: function() {
            this.$el.find('.suggestion-lists').append( this.template( this.options ) );
            this.$keypadList = this.$el.find('#suggestive-list-'+this.options.id);

            if ( this.options.items.length > 0 ) {
                var keypadID = this.options.id;
                this.keypadItemCollection.each(function (mod) {
                    mod.set('keypadID', keypadID);
                    this.renderLine(mod);

                }, this);
            } else {
                this.$el.find('.error-msg-container').html('No products were found on this menu! Use the back button to go back.').slideDown();
            }
            return this;
        },

        renderLine: function( mod ) {
            //don't show keypads in suggestive keypad
            if(mod.get('menuID') != null) {
                return;
            }

            var keypadItemView = new qcssapp.Views.SuggestiveKeypadLineView({
                model:mod
            });

            this.$keypadList.append(keypadItemView.render().el);
        },

        loadImage: function(keypadLineModel) {
            this.keypadLines++;
            var $keypadLine = this.$keypadList.find(".suggestive-keypad-line[data-id='"+keypadLineModel.attributes.id+"']");
            var priority = $keypadLine.attr('priority');

            //if the image that was just loaded is the next one that needs to be filled, show it
            if(priority == this.nextOneFilled) {

                var $keypadLineImage = $keypadLine.parent().find('.line_image');
                if($keypadLine[0].id.indexOf('full') != -1) {
                    $keypadLineImage = $keypadLine.parent();
                }

                $keypadLineImage.removeClass('hideImages');
                $keypadLineImage.addClass('fadeInImages');
                this.nextOneFilled++;

                //check if the next one that needs to be filled is a 'waiting' image
                var $waitingKeypadLines = this.$keypadList.find(".suggestive-keypad-line.waiting");
                if($waitingKeypadLines.length>0) {
                    this.checkWaitingImages(false);
                }

                //otherwise add class 'waiting' to the image so it can be checked later
            } else {
                $keypadLine.addClass('waiting');
                this.checkWaitingImages(false);
            }

            //check if we've gone through all the images and there are still some 'waiting'
            var $waitingKeypadLinesEnd = this.$keypadList.find(".suggestive-keypad-line.waiting");
            if(this.keypadLines == this.$keypadList.find(".suggestive-keypad-line.hasImage").length &&  $waitingKeypadLinesEnd.length>0) {
                this.checkWaitingImages(true)

            }
        },

        //checks the 'waiting' images if their priority is the next one that needs to be loaded
        checkWaitingImages: function(atEnd) {
            var $waitingKeypadLine = this.$keypadList.find(".suggestive-keypad-line.waiting[priority='"+this.nextOneFilled+"']");

            if($waitingKeypadLine.length == 1) {
                var $waitingKeypadLineImage = $waitingKeypadLine.parent().find('.line_image');

                if($waitingKeypadLine[0].id.indexOf('full') != -1) {
                    $waitingKeypadLineImage = $waitingKeypadLine.parent();
                }
                $waitingKeypadLineImage.removeClass('hideImages');
                $waitingKeypadLine.removeClass('waiting');
                $waitingKeypadLineImage.addClass('fadeInImages');
                this.nextOneFilled++;
            }

            //if we've gone through all the images, look at the remainder in waiting and load them in order
            if(atEnd) {
                var $waitingKeypadLines = this.$keypadList.find(".suggestive-keypad-line.waiting");
                if($waitingKeypadLines.length > 0) {
                    this.checkWaitingImages(true);
                }
            }
        }

    });
})(qcssapp, jQuery);