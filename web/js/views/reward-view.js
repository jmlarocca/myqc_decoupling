;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.RewardView - View for displaying Rewards Collection
     */
    qcssapp.Views.RewardView = Backbone.View.extend({
        internalName: "Reward View", //NOT A STANDARD BACKBONE PROPERTY

        id: 'program-rewards-list',

        template: _.template( $('#program-rewards-list-template').html() ),

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in reward view");
            }

            this.$container = $('#program-rewards');

            this.options = options || {};

            this.programModel = this.options.programModel;
            this.collection = new qcssapp.Collections.Rewards();

            //elements
            this.$headerContainer = this.$container.find('.program-rewards-header');

            this.createProgramHeader();

            this.loadCollection();
        },

        createProgramHeader: function() {
            var controllerModel = new qcssapp.Models.Controller({
                id: 'RewardHeaderView',
                options: this.programModel.toJSON(),
                destructible: true,
                destroyOn: ['NavView', 'RewardsProgramsView']
            });
            var headerView = qcssapp.Controller.constructView(controllerModel);
            this.$headerContainer.append(headerView.$el);
        },

        //fetch collection to be used in this view
        loadCollection: function() {
            var parameters = {
                rewardView:this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/loyalty/rewards/'+this.programModel.get('id'), 'GET', '', parameters,
                function(response, successParameters) {
                    successParameters.rewardView.fetchSuccess(response);
                },
                function(errorParameters) {
                    errorParameters.rewardView.fetchError();
                },
                '',
                parameters
            );
        },

        fetchSuccess: function(data) {
            //can't just call .set(data , {parse}) -- need to pass parameter
            this.collection.set(this.collection.parse(data, this.programModel.get('points')));

            this.render();
        },

        render: function() {
            this.$el.html( this.template() );
            this.$container.append(this.$el);

            this.rewardsMenuText = $('#show-rewards').attr('data-title').substring(0,$('#show-rewards').attr('data-title').indexOf('Programs'));
            $('.rewards-program-header-text').text(this.rewardsMenuText+'Program');

            this.$availableList = $('#program-rewards-available');
            this.$unavailableList = $('#program-rewards-unavailable');
            this.$unavailableLabel = $('#program-rewards-unavailable-label');

            if ( this.collection.isEmpty() ) {
                this.$availableList.append('<li class="align-center">No'+this.rewardsMenuText+'Found.</li>');
                this.$unavailableLabel.add(this.$unavailableList).hide();
            } else {
                var controllerModel = new qcssapp.Models.Controller({
                    id: 'RewardLineView',
                    destructible: true,
                    destroyOn: ['NavView', 'RewardsProgramsView']
                });

                this.collection.each(function( mod ) {
                    this.renderLine( mod, controllerModel );
                }, this );

                //show no rewards available message for the case where all rewards are not available
                if ( this.$availableList.find('li').length == 0 ) {
                    this.$availableList.append('<li class="align-center italic">No'+this.rewardsMenuText+'Currently Redeemable.</li>');
                }

                //hide the unavailable list if none are unavailable
                if ( this.$unavailableList.find('li').length == 0 ) {
                    this.$unavailableLabel.add(this.$unavailableList).hide();
                }
            }
        },

        renderLine: function( mod, controllerModel ) {
            controllerModel.set('options', {model:mod});

            var RewardLineView = qcssapp.Controller.constructView(controllerModel);

            if ( mod.get('redeemable') ) {
                this.$availableList.append( RewardLineView.render().el );
                return;
            }

            this.$unavailableList.append( RewardLineView.render().el );
        },

        fetchError: function() {
            qcssapp.Functions.displayError('There was an error loading rewards, please try again later.');
        }
    });
})(qcssapp, jQuery);