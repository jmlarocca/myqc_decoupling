;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.PersonPasswordView - View for displaying access code for person accounts
     */
    qcssapp.Views.PersonPasswordView = Backbone.View.extend({
        internalName: "Person Account Password View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#person-password-page', //references existing HTML element

        events: {'keypress input':'submitForm'},

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in person password view");
            }

            if ( qcssapp.Location == "" ) {
                qcssapp.Location = $('#loginCodeSelect').val()
            }

            _.bindAll(this, 'render', 'submitPersonPassword', 'submitForm');

            this.options = options || '';

            this.render();

            qcssapp.Functions.hideMask();
        },

        //calls renderLine on each item in the spending profile collection
        render: function() {
            if ( $('#personPasswordSubmit').length ) {
                return;
            }

            new qcssapp.Views.ButtonView({
                text: "Submit",
                appendToID: "#person-password-form-container",
                id: "personPasswordSubmit",
                buttonType: 'customCB',
                class: "template-gen button-flat accent-color-one",
                parentView:this,
                callback: function() {
                    this.parentView.submitPersonPassword();
                }
            });

            new qcssapp.Views.ButtonView({
                text: "Cancel",
                appendToID: "#person-password-form-container",
                id: "personPasswordCancel",
                buttonType: 'customCB',
                class: "template-gen button-flat accent-color-one",
                callback: function() {
                    qcssapp.Functions.showMask();

                    $('#personPassword').val("");
                    qcssapp.Router.navigate('login-page', {trigger: true});
                    $('#person-password-page').off();

                    qcssapp.Functions.hideMask();
                }
            });
        },

        submitPersonPassword: function() {
            qcssapp.Functions.showMask();
            qcssapp.Functions.resetErrorMsgs();

            var password = $('#personPassword').val();

            var endPoint = qcssapp.Location + '/api/account/person/password';
            var dataParameters =  {
                personPassword:password
            };

            var successParameters = {
                view:this
            };

            //validation
            if ( password == "" ) {
                qcssapp.Functions.displayError('Please enter an access code');
                qcssapp.Functions.hideMask();
                return;
            }


            //TODO: Adjust callAPI parameters
            qcssapp.Functions.callAPI(endPoint,'POST',JSON.stringify(dataParameters),successParameters,
                function(data, successParameters) {
                    if(data) {
                        qcssapp.Functions.hideMask();

                        new qcssapp.Views.CreateAccountView(1);
                        qcssapp.Router.navigate('create-account-page', {trigger:true});

                    } else {
                        qcssapp.Functions.displayError("Incorrect access code. Please obtain the account creation access code from your organization. Note the access code is case sensitive.");
                        qcssapp.Functions.hideMask();
                    }
                },
                '',
                function() {
                    $("#personPassword").val("");
                    qcssapp.Functions.hideMask();
                }
            );
        },

        submitForm: function(e) {
            if (e.which == 13) {
                this.submitPersonPassword();
            }
        }
    });
})(qcssapp, jQuery);   