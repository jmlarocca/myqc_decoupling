;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.LogoutView - View for displaying the logout/sso splash screen*/

    qcssapp.Views.CreateAccountView = Backbone.View.extend({
        internalName: "Create Account View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#create-account-page',

        events: {
            "change #createAccountEmail, #createAccountUsername" : "checkForDuplicateName",
            "change #createAccountAcctNumber, #createAccountBadgeNumber" : "checkForDuplicateNum",
            "change #accountGroupSelect" : "accountGroupChange",
            "change #createAccountLowBalanceThreshold" : "lowBalanceChange",
            "click .icon-info-link" : "openPasswordInfoBox"
        },

        initialize: function (personAccount) {
            //start of application
            if (qcssapp.DebugMode) {
                console.log("Initialize logout view");
            }

            if ( qcssapp.Location == "" ) {
                qcssapp.Location = $('#loginCodeSelect').val()
            }

            this.personAccountCreation = !!(personAccount == 1);

            //this.getPasswordStrengthRequirement();
            if(!$('#createAccount-input-container').hasClass('initialized')) {
                this.displayButtons();
            } else {
                this.removeButtons();
            }

            this.settingsModel = qcssapp.CreateAccountSettings.settingsModel.attributes;
            this.accountGroupsModel = qcssapp.CreateAccountSettings.accountGroups.models;

            var accountInfoMdl = new qcssapp.Models.AccountInfoModel();
            this.accountInfoModel = accountInfoMdl.attributes;

            this.looseMsg = '<p id="password-popup-text">Your password must be 6 or more characters in length and include at least 2 of the following: <br/></p>' +
                '<ul>' +
                '<li class="password-popup-list">Uppercase Letters (ex. MMHAYES)</li>' +
                '<li class="password-popup-list">Lowercase Letters (ex. mmhayes)</li>' +
                '<li class="password-popup-list">Special Characters (ex. !@#$%_)</li>' +
                '<li class="password-popup-list">Numeric Characters (ex. 123456)</li>' +
                '</ul>';

            this.strictMsg = '<p id="password-popup-text">Your password must be 8 or more characters in length and include at least 3 of the following: <br/></p>' +
                '<ul>' +
                '<li class="password-popup-list">Uppercase Letters (ex. MMHAYES)</li>' +
                '<li class="password-popup-list">Lowercase Letters (ex. mmhayes)</li>' +
                '<li class="password-popup-list">Special Characters (ex. !@#$%_)</li>' +
                '<li class="password-popup-list">Numeric Characters (ex. 123456)</li>' +
                '</ul>';

            this.passStrength = "strict";
            this.passMessage = this.strictMsg;

            this.getPasswordStrengthRequirement();
            this.render();
        },

        render: function() {
            // no global settings model was set
            if(this.settingsModel === null || this.settingsModel === undefined) {
                if(qcssapp.CreateAccountSettings.accountGroups && qcssapp.CreateAccountSettings.accountGroups.length) {
                    this.showHideFields();
                    var models = qcssapp.CreateAccountSettings.accountGroups.models;
                    var $accessCodeInput = $('#createAccountAccessCode');
                    var $accountGroupSelect = $('#accountGroupSelect');

                    $accessCodeInput.val('');
                    $accountGroupSelect.empty();
                    $accountGroupSelect.parent().hide();

                    $accountGroupSelect.change( () => {
                        if($accountGroupSelect.val()) {
                            for (var model of models) {
                                if (model.get("accountGroupName") == $accountGroupSelect.val()) {
                                    this.settingsModel = model.attributes;
                                    break;
                                }
                            }
                        } else {
                            return;
                        }

                        if($('#spendingProfileAccountSelect').length) {
                            $('#spendingProfileAccountSelect').empty();
                        }

                        //if no spending profiles throw error to user that the account group is not configured properly
                        if (this.settingsModel && this.settingsModel.spendingProfiles && this.settingsModel.spendingProfiles.length == 0){
                            qcssapp.Functions.displayError('No Spending Profiles are mapped to this Account Group.');
                            $('#spendingProfileSelectContainer').hide();
                            $('#accountTypeSpan').text('');

                            //if you have one spending profile hide the selector and set the first option to the name of the spending profile
                        } else if(this.settingsModel && this.settingsModel.spendingProfiles && this.settingsModel.spendingProfiles.length == 1) {
                            $('#spendingProfileAccountSelect').attr('disabled', true);
                            var spendingProfileNameOne = this.settingsModel.spendingProfiles[0].spendingProfileName;
                            $('#spendingProfileAccountSelect').append($("<option />").val(spendingProfileNameOne).text(spendingProfileNameOne));
                            $('#accountTypeSpan').text('Account Type: '+this.settingsModel.accountTypeName);
                            if(this.settingsModel.accountTypeID == "") {   //if no account groups configured display error
                                qcssapp.Functions.displayError('This Account Group does not have an Account Type configured.');
                                $('#accountTypeSpan').text('');
                            }

                        } else if(this.settingsModel) { //otherwise show the selector
                            var $spendingProfileSelector = $('#spendingProfileAccountSelect');
                            $spendingProfileSelector.attr('disabled', false);
                            $spendingProfileSelector.append($("<option />").val('').text(''));
                            $('#accountTypeSpan').text('Account Type: '+this.settingsModel.accountTypeName);
                            if(this.settingsModel.accountTypeID == "") {   //if no account groups configured display error
                                qcssapp.Functions.displayError('This Account Group does not have an Account Type configured.');
                                $('#accountTypeSpan').text('');
                            }

                            //populate the spending profile selector
                            $.each(this.settingsModel.spendingProfiles, function() {
                                var spendingProfileName = this.spendingProfileName;
                                $spendingProfileSelector.append($("<option />").val(spendingProfileName).text(spendingProfileName));
                            });
                        } else {
                            return;
                        }

                        this.showHideFields();
                    });

                    var listAccessCodes = [];
                    for(var model of models) {
                        if(listAccessCodes.indexOf(model.get("accessCodeName").toLowerCase() === -1)) {
                            listAccessCodes.push(model.get("accessCodeName").toLowerCase());
                        }
                    }
                    if(qcssapp.CreateAccountSettings.personAccount) {
                        listAccessCodes.push(qcssapp.CreateAccountSettings.personAccount.ACCESSCODENAME)
                    }

                    // check for any account groups whenever the user stops typing with this debounced function
                    var debounceInput = _.debounce(function () {
                        this.settingsModel = ""; // reset settings model when no settings should be available
                        this.personAccountCreation = false;
                        this.accessCodeID = -1;
                        qcssapp.Functions.resetErrorMsgs();

                        // if they're creating a person account, there are defined options
                        if(qcssapp.CreateAccountSettings && qcssapp.CreateAccountSettings.personAccount &&
                            qcssapp.CreateAccountSettings.personAccount.ACCESSCODENAME &&
                            $accessCodeInput.val().toLowerCase() == qcssapp.CreateAccountSettings.personAccount.ACCESSCODENAME.toLowerCase()) {
                            this.settingsModel = {
                                personAccountCreation: true,
                                accountEmailForUsername: true
                            };
                            this.personAccountCreation = true;
                            this.accessCodeID = qcssapp.CreateAccountSettings.personAccount.ACCESSCODEID;
                            this.showHideFields();
                            return;
                        }

                        $accountGroupSelect.empty();
                        $accountGroupSelect.append(`<option value=""></option>`);

                        // access codes should be of just one type
                        var accountTypeID = -1;

                        for (var model of models) {
                            if (model.get("accessCodeName").toLowerCase() === $accessCodeInput.val().toLowerCase()) {
                                accountTypeID = model.get('accountTypeID');
                                $accountGroupSelect.append(`<option value="${model.get("accountGroupName")}">${model.get("accountGroupName")}</option>`);
                            }
                        }

                        if (!qcssapp.CreateAccountSettings.employeeLicenseAvailable && (accountTypeID === 1 || accountTypeID === 2)) {
                            this.errorMsg = `Account Creation not permitted for ${accountTypeID === 1 ? "Payroll Deduct" : "Prepaid"} accounts at this time. Please try again later.`;
                            if (qcssapp.DebugMode) {
                                console.log("No employee account licenses available.");
                            }
                        } else if (!qcssapp.CreateAccountSettings.guestLicenseAvailable && accountTypeID === 5) {
                            this.errorMsg = `Account Creation not permitted for Guest accounts at this time. Please try again later.`;
                            if (qcssapp.DebugMode) {
                                console.log("No guest account licenses available.");
                            }
                        }

                        // if there is an error message, just show the message immediately
                        if(this.errorMsg) {
                            qcssapp.Functions.displayError(this.errorMsg);
                            delete this.errorMsg;
                            $accountGroupSelect.empty(); // be sure this is empty or it may show some fields if only one acct group is available
                        } else if($accountGroupSelect.children().length === 1) { // only the blank option
                            $accountGroupSelect.parent().hide();
                        } else {
                            if($accountGroupSelect.children().length === 2) { // only blank option + one acct group
                                var changeValue = $($accountGroupSelect.children()[1]).val();
                                $accountGroupSelect.val(changeValue).change();
                                $accountGroupSelect.attr('disabled', true);
                            }
                            $accountGroupSelect.parent().show();
                        }

                        this.showHideFields();
                    }, 350).bind(this);

                    if(qcssapp.onPhoneApp) {
                        $accessCodeInput.val(qcssapp.CreateAccountSettings.instanceCode.toUpperCase());

                        // if person accounts are disabled, do not allow them to change access code
                        var employeeAcctCreationModelIndex = 1; // if we have a person account, we gotta check the second model later for the account type
                        if (models.length === 1) {
                            $accessCodeInput.prop('disabled', true);
                        }
                        if(!qcssapp.CreateAccountSettings.personAccount) {
                            employeeAcctCreationModelIndex = 0; // no person account, check the first model
                        } else if(qcssapp.CreateAccountSettings.personAccount && $accessCodeInput.val().toLowerCase() === qcssapp.CreateAccountSettings.personAccount.ACCESSCODENAME.toLowerCase()) {
                            this.personAccountCreation = true;
                        }

                        if (models[employeeAcctCreationModelIndex] && models[employeeAcctCreationModelIndex].get("accountTypeID")) {
                            // access codes should be of just one type
                            var accountTypeID = models[employeeAcctCreationModelIndex].get("accountTypeID");

                            if (!qcssapp.CreateAccountSettings.employeeLicenseAvailable && (accountTypeID === 1 || accountTypeID === 2)) {
                                this.errorMsg = `Account Creation not permitted for ${accountTypeID === 1 ? "Payroll Deduct" : "Prepaid"} accounts at this time. Please try again later.`;
                                if (qcssapp.DebugMode) {
                                    console.log("No employee account licenses available.");
                                }
                            } else if (!qcssapp.CreateAccountSettings.guestLicenseAvailable && accountTypeID === 5) {
                                this.errorMsg = `Account Creation not permitted for Guest accounts at this time. Please try again later.`;
                                if (qcssapp.DebugMode) {
                                    console.log("No guest account licenses available.");
                                }
                            }
                        }


                        $accountGroupSelect.append(`<option value=""></option>`);

                        // by now, all account groups with other access codes have been filtered out
                        // check that there are licenses available, notify them otherwise
                        if (!this.errorMsg) {
                            for (var model of models) {
                                if(model.get("accountGroupName")) {
                                    $accountGroupSelect.append(`<option value="${model.get("accountGroupName")}">${model.get("accountGroupName")}</option>`);
                                }
                            }
                        }
                        // wait 350 ms to populate the accountGroup dropdown after typing
                        $accessCodeInput.keyup(() => debounceInput());

                        if(qcssapp.CreateAccountSettings.personAccount && qcssapp.CreateAccountSettings.personAccount.ACCESSCODENAME && $accessCodeInput.val().toLowerCase() === qcssapp.CreateAccountSettings.personAccount.ACCESSCODENAME.toLowerCase()) {
                            this.settingsModel = { personAccountCreation: true };
                            this.showHideFields();
                        } else if ($accountGroupSelect.children().length === 1) {
                            if(!this.errorMsg) {
                                this.errorMsg = "Account Creation not permitted with this Access Code. Please try again later.";
                            }
                            this.showHideFields();
                        } else {
                            $accountGroupSelect.parent().show();
                            $accountGroupSelect.change();
                        }

                    // in on browser and there's only one access code, well, we know what access code they'll be using
                    // ignore for person accounts
                    } else if(listAccessCodes.length === 1 && listAccessCodes[0] !== qcssapp.CreateAccountSettings.personAccount.ACCESSCODENAME) {
                        // set value & hide field
                        $accessCodeInput.val(listAccessCodes[0]);
                        $accessCodeInput.parent().hide();

                        // populate the account group selector
                        for (var model of models) {
                            $accountGroupSelect.append(`<option value="${model.get("accountGroupName")}">${model.get("accountGroupName")}</option>`);
                        }

                        if($accountGroupSelect.children().length === 0) {
                            qcssapp.Functions.displayError("Account Creation not permitted with this Access Code");
                        } else {
                            $accountGroupSelect.parent().show();
                            $accountGroupSelect.change();
                        }

                    } else {
                        // wait 350 ms to populate the accountGroup dropdown after typing
                        $accessCodeInput.keyup(() => debounceInput());
                    }
                } else {
                    qcssapp.Functions.displayError("No Account Groups are currently configured for Account Creation", "Warning");
                }

            } else {
                this.showHideFields();
            }

            if(!qcssapp.Functions.checkServerVersion(6, 2, 0)) {
                $('#accessCodeCreateAcctContainer').hide();
            }

        },

        showHideFields: function() {
            if(this.errorMsg) {
                var errorMsg = this.errorMsg
                setTimeout(() => { qcssapp.Functions.displayError(errorMsg) }, 500);
              delete this.errorMsg;
            } else if(this.settingsModel) {

                if(this.settingsModel.personAccountCreation) {
                    $(".first-line-add, #firstNameCreateAcctContainer, #middleInitNameCreateAcctContainer, #lastNameCreateAcctContainer," +
                        " #lowBalanceThresholdCreateContainer, #passwordCreateContainer, #emailCreateContainer").show();
                    return;
                }

                // show all fields by default, hide depending on settings
                $('.first-line-add, #firstNameCreateAcctContainer, #middleInitNameCreateAcctContainer, #lastNameCreateAcctContainer, #usernameCreateContainer, #emailCreateContainer, #passwordCreateContainer, ' +
                    '#lowBalanceThresholdCreateContainer, #accountNumberContainer, #badgeNumberContainer, #spendingProfileSelectContainer, #accountTypeSpanContainer').show();

                //if in the cloud and using email then hide username fields
                if (this.settingsModel.accountEmailForUsername && !qcssapp.onGround) {
                    $("#usernameCreateContainer, #passwordCreateContainer").hide();
                } else {  //otherwise hide email fields
                    if (qcssapp.onGround && this.settingsModel.promptForEmail) {
                        $("#emailCreateContainer").show();
                    } else if ((!qcssapp.onGround && this.settingsModel.promptForEmail) || (qcssapp.onGround && !this.settingsModel.promptForEmail) || (!this.settingsModel.accountEmailForUsername && !qcssapp.onGround)) {
                        $("#emailCreateContainer").hide();
                    }
                }

                if(!this.settingsModel.promptForName) { //if false don't show name field
                    $('#firstNameCreateAcctContainer, #middleInitNameCreateAcctContainer, #lastNameCreateAcctContainer').hide();
                }

                if(!this.settingsModel.promptForEmpNumber) { //if false don't shown account number field
                    $('#accountNumberContainer').hide();
                }

                if(!this.settingsModel.promptForBadgeNumber) { //if false don't show badge number field
                    $('#badgeNumberContainer').hide();
                }

                // guests have no balance to check against
                if(this.settingsModel.accountTypeID === 5) {
                    $('#lowBalanceThresholdCreateContainer').hide();
                }

                if(!this.personAccountCreation) {
                    this.setSelectors();
                } else {
                    if(!qcssapp.Functions.checkServerVersion(6,2,0)) {
                        $('#accountGroupSelectContainer').hide();
                    }
                    //only show email, password field and name if promptForName is on
                    $(' #accountTypeSpanContainer, #spendingProfileSelectContainer, #usernameCreateContainer, #accountNumberContainer, #badgeNumberContainer').hide();
                    $('#emailCreateContainer, #passwordCreateContainer, #firstNameCreateAcctContainer, #middleInitNameCreateAcctContainer, #lastNameCreateAcctContainer').show();
                }

            } else {
                $('.first-line-add, #lastNameCreateAcctContainer, #usernameCreateContainer, #emailCreateContainer, #passwordCreateContainer, ' +
                    '#lowBalanceThresholdCreateContainer, #accountNumberContainer, #badgeNumberContainer, #spendingProfileSelectContainer, #accountTypeSpanContainer').hide();
            }
        },

        //create Cancel and Create Account buttons
        displayButtons: function() {
            new qcssapp.Views.ButtonView({
                text: "Cancel",
                appendToID: "#createAccount-input-container",
                id: "createAccountCancel",
                buttonType: 'customCB',
                class: "template-gen button-flat accent-color-one",
                parentView: this,
                callback: function() {
                    //clear the input fields and navigate back to the login page
                    this.parentView.resetCreateAccount();
                    qcssapp.Router.navigate('login-page', {trigger: true});
                    $('#create-account-page').off();
                }
            });
            new qcssapp.Views.ButtonView({
                text: "Create Account",
                appendToID: "#createAccount-input-container",
                id: "createAccountBtn",
                buttonType: 'customCB',
                class: "template-gen button-flat accent-color-one",
                parentView:this,
                callback: function() {
                    // not using global settings to control account creation
                    if(this.parentView.settingsModel === null || this.parentView.settingsModel === undefined) {
                        var validAccessCode = false;
                        var $accessCodeInput = $('#createAccountAccessCode');
                        if($accessCodeInput.val()) {
                            for(var model of qcssapp.CreateAccountSettings.accountGroups.models) {
                                if (model.get("accessCodeName").toLowerCase() === $accessCodeInput.val().toLowerCase()) {
                                    validAccessCode = true;
                                    break;
                                }
                            }
                        }

                        if(!validAccessCode) {
                            qcssapp.Functions.displayError("Please enter a valid access code", "Error");
                            return;
                        }

                    }

                    if(!this.parentView.personAccountCreation) {
                        //validate the input fields
                        this.parentView.checkFields();
                    } else {

                        this.parentView.checkPersonFields();
                    }
                }
            });

            $('#createAccount-input-container').addClass('initialized');
        },

        checkFields: function() {
            qcssapp.Functions.showMask();

            qcssapp.Functions.resetErrorMsgs();

            var accountGroup = $("#accountGroupSelect").val();

            if(qcssapp.Functions.checkServerVersion(6,2,1) // check server version
                && $("#accessCodeCreateAcctContainer").css('display') !== 'none') // check that the access code container is visible
            {
                if($("#accountGroupSelectContainer").css('display') === 'none') { // if the access codes are not valid, this should be hidden
                    qcssapp.Functions.displayError("Please enter a valid access code.");
                    return;
                }
                if(!accountGroup) {
                    qcssapp.Functions.displayError("Please select an account group.");
                    return;
                }
            }

            //set variables
            var firstName = $('#createAccountFirstName').val();
            var middleInitial = $('#createAccountMiddleInit').val();
            var lastName = $('#createAccountLastName').val();
            var spendingProfile = $('#spendingProfileAccountSelect').val();
            var acctNumber = $('#createAccountAcctNumber').val();
            var badgeNumber = $('#createAccountBadgeNumber').val();
            var accountGroupID = "", spendingProfileID = "", accountTypeID = "", email="", emailConfirm = "";
            var checkForEmailPrompt = false;

            var accountName = $('#createAccountUsername').val();
            var accountNameConfirm = $('#createAccountConfirmUsername').val();
            var nameText = 'Username';
            if(this.settingsModel.accountEmailForUsername) {
                accountName = $('#createAccountEmail').val();
                accountNameConfirm = $('#createAccountConfirmEmail').val();
                nameText = 'Email'
            }

            if(this.settingsModel.promptForEmail && qcssapp.onGround) {
                email = $('#createAccountEmail').val();
                emailConfirm = $('#createAccountConfirmEmail').val();
                checkForEmailPrompt = true;
            }

            //name validation
            if(this.settingsModel.promptForName) { //if prompt for name is ON
                if(firstName == "") {
                    qcssapp.Functions.displayError('Please provide a valid First Name.');
                    qcssapp.Functions.hideMask();
                    return;
                }

                if(lastName == "") {
                    qcssapp.Functions.displayError('Please provide a valid Last Name.');
                    qcssapp.Functions.hideMask();
                    return;
                }

                if ( qcssapp.Functions.checkServerVersion(3,0,39) ) {
                    this.accountInfoModel.firstName = firstName;
                    this.accountInfoModel.middleInitial = middleInitial;
                    this.accountInfoModel.lastName = lastName;
                } else {
                    var name = "";
                    if(firstName != "") {
                        name += firstName;
                    }
                    if (middleInitial != "") {
                        name += " " + middleInitial;
                    }
                    if (lastName != "") {
                        name += " " + lastName;
                    }
                    this.accountInfoModel.name = name;
                }

            } else { //if prompt for name is OFF set username or email as name parameter
                this.accountInfoModel.name = accountName;
            }

            //username/email validation
            if(accountName == "") {
                qcssapp.Functions.displayError('Please provide a valid '+ nameText + '.');
                qcssapp.Functions.hideMask();
                return;
            }
            if(accountName.toLowerCase() != accountNameConfirm.toLowerCase()) {
                qcssapp.Functions.displayError(nameText +'s do not match.');
                qcssapp.Functions.hideMask();
                return;
            }

            //if prompt for email is on check for an inputted email
            if(email == "" && checkForEmailPrompt) {
                qcssapp.Functions.displayError('Please provide a valid Email.');
                qcssapp.Functions.hideMask();
                return;
            }

            //if prompt for email is on check emails match
            if(email.toLowerCase() != emailConfirm.toLowerCase() && checkForEmailPrompt) {
                qcssapp.Functions.displayError('Emails do not match.');
                qcssapp.Functions.hideMask();
                return;
            }

            //password validation
            if(!this.checkPasswordRequirements()) {
              return;
            }

            //inputted account number validation
            if(this.settingsModel.promptForEmpNumber && (acctNumber == "" || (!(qcssapp.Functions.isNumeric(acctNumber)) || !(acctNumber > -1)))) {
                qcssapp.Functions.displayError('Please provide a valid Account Number.');
                qcssapp.Functions.hideMask();
                return;
            }

            //inputted badge number validation
            if(this.settingsModel.promptForBadgeNumber && (badgeNumber == "" || (!(qcssapp.Functions.isNumeric(badgeNumber)) || !(badgeNumber > -1)))) {
                qcssapp.Functions.displayError('Please provide a valid Badge Number.');
                qcssapp.Functions.hideMask();
                return;
            }

            //if the account group has not been set yet (selector is not hidden)
            if(this.accountInfoModel.accountGroup == "") {

                //check for valid account group
                if(accountGroup == "") {
                    qcssapp.Functions.displayError('Please select a valid Account Group.');
                    qcssapp.Functions.hideMask();
                    return;
                }

                //get the accountGroupID and accountTypeID from the chose account group name
                $.each(this.accountGroupsModel, function() {
                    if(accountGroup == this.get('accountGroupName')) {
                        accountGroupID = this.get('accountGroupID');
                        accountTypeID = this.get('accountTypeID');
                    }
                });

                //set the accountGroup in the accountInfoModel
                this.accountInfoModel.accountGroup = accountGroupID;
            }

            //if the spending profile has not been set yet (selector is not hidden)
            if(this.accountInfoModel.spendingProfile == "") {

                //check for valid spending profile selection
                if(spendingProfile == "" && spendingProfile.children().length === 0) {
                    this.accountInfoModel.accountGroup = "";
                    qcssapp.Functions.displayError('Please select a valid Spending Profile.');
                    qcssapp.Functions.hideMask();
                    return;
                }

                //if account group doesn't have spending profile mapped
                if(spendingProfile == null) {
                    this.accountInfoModel.accountGroup = "";
                    qcssapp.Functions.displayError('No Spending Profiles are mapped to the Account Group.');
                    qcssapp.Functions.hideMask();
                    return;
                }

                //get the spendingProfileID from the chosen spending profile name
                $.each(this.accountGroupsModel, function() {
                    $.each(this.attributes.spendingProfiles, function() {
                        if(spendingProfile == this.spendingProfileName) {
                            spendingProfileID = this.spendingProfileID;
                            return false;
                        }
                    });
                });

                //set the spendingProfile in the accountInfoModel
                this.accountInfoModel.spendingProfile = spendingProfileID;
            }

            //set username or email
            if(this.settingsModel.accountEmailForUsername) {
                this.accountInfoModel.email = accountName;
            } else {
                this.accountInfoModel.username = accountName;
                if(checkForEmailPrompt) {
                    this.accountInfoModel.email = email;
                }
            }

            //set account type
            if(this.accountInfoModel.accountType == "") {
                this.accountInfoModel.accountType = accountTypeID;
            }

            this.handleAccountAndBadgeNumbers(acctNumber, badgeNumber);
        },

        //check the fields are filled out for person accounts
        checkPersonFields: function() {
            qcssapp.Functions.showMask();

            qcssapp.Functions.resetErrorMsgs();

            //set variables
            var firstName = $('#createAccountFirstName').val();
            var middleInitial = $('#createAccountMiddleInit').val();
            var lastName = $('#createAccountLastName').val();
            var email = $('#createAccountEmail').val();
            var emailConfirm = $('#createAccountConfirmEmail').val();
            var lowBalanceThreshold = $('#createAccountLowBalanceThreshold').val();

            if(firstName == "") {
                qcssapp.Functions.displayError('Please provide a valid First Name.');
                qcssapp.Functions.hideMask();
                return;
            }

            if(lastName == "") {
                qcssapp.Functions.displayError('Please provide a valid Last Name.');
                qcssapp.Functions.hideMask();
                return;
            }

            //email validation
            if(email == "") {
                qcssapp.Functions.displayError('Please provide a valid Email.');
                qcssapp.Functions.hideMask();
                return;
            }

            //confirm email
            if(email.toLowerCase() != emailConfirm.toLowerCase()) {
                qcssapp.Functions.displayError('Emails do not match.');
                qcssapp.Functions.hideMask();
                return;
            }

            //password validation
            if(!this.checkPasswordRequirements()) {
                return;
            }

            //low balance threshold number validation
            if(!qcssapp.Functions.isNumeric(lowBalanceThreshold) && lowBalanceThreshold != '') {
                qcssapp.Functions.displayError('Please provide a number for the Low Balance Threshold.');
                qcssapp.Functions.hideMask();
                return;
            }

            //low balance threshold minimum validation
            if(parseInt(lowBalanceThreshold) < 0 && lowBalanceThreshold != '') {
                qcssapp.Functions.displayError('The Low Balance Threshold cannot be negative.');
                qcssapp.Functions.hideMask();
                return;
            }

            if ( qcssapp.Functions.checkServerVersion(3,0,39) ) {
                this.accountInfoModel.firstName = firstName;
                this.accountInfoModel.middleInitial = middleInitial;
                this.accountInfoModel.lastName = lastName;
            } else {
                var name = "";
                if(firstName != "") {
                    name += firstName;
                }
                if (middleInitial != "") {
                    name += " " + middleInitial;
                }
                if (lastName != "") {
                    name += " " + lastName;
                }
                this.accountInfoModel.name = name;
            }

            this.accountInfoModel.email = email;
            this.accountInfoModel.lowBalanceThreshold = lowBalanceThreshold;
            this.accountInfoModel.accessCodeID = this.accessCodeID;
            if (qcssapp.CreateAccountSettings.personAccount && qcssapp.CreateAccountSettings.personAccount.ACCESSCODEID) {
                this.accountInfoModel.accessCodeID = qcssapp.CreateAccountSettings.personAccount.ACCESSCODEID;
            }

            this.createPersonAccount(this.accountInfoModel);

        },

        //determines if the account or badge number needs to be auto generated and if the TOS needs to be checked here or after auto-generation
        handleAccountAndBadgeNumbers: function(acctNumber, badgeNumber) {
            //account number AND badge number are inputted
            if(this.settingsModel.promptForEmpNumber && this.settingsModel.promptForBadgeNumber) {
                this.accountInfoModel.accountNumber = acctNumber;
                this.accountInfoModel.badgeNumber = badgeNumber;

                //check if TOS is required and if there are available licenses
                var dataParameters = JSON.stringify({accountGroupID: parseInt(this.accountInfoModel.accountGroup), spendingProfileID: parseInt(this.accountInfoModel.spendingProfile), accountTypeID: parseInt(this.accountInfoModel.accountType)});
                var successParams = {accountInfoModel: this.accountInfoModel, accountView: this};
                this.checkTOSAndLicenses(dataParameters, successParams);

            //account number needs to be auto generated and badge number is inputted
            } else if(!this.settingsModel.promptForEmpNumber && this.settingsModel.promptForBadgeNumber){
                this.accountInfoModel.badgeNumber = badgeNumber;
                this.autoGenerateNumber(true, false);

            //account number is inputted and badge number needs to be auto generated
            } else if(this.settingsModel.promptForEmpNumber && !this.settingsModel.promptForBadgeNumber){
                this.accountInfoModel.accountNumber = acctNumber;
                this.autoGenerateNumber(false, false);

            //account number AND badge number need to be auto generated, set badge number to account number
            } else {
                this.autoGenerateNumber(true, true);
            }
        },

        //auto generate a random account or badge number, check for duplicates in database
        autoGenerateNumber: function(isAcctNum, autoGenerateBoth) {
            var length = this.settingsModel.autoGeneratedLength;

            var number = "", accountNumber = "", badgeNumber = "";
            for(var i=0;i<length;i++) {
                number += Math.floor(Math.random() * 10)
            }

            if(isAcctNum) {
                accountNumber = number;
            } else {
                badgeNumber = number;
            }

            var dataParameters = {accountNumber: accountNumber, badgeNumber: badgeNumber, isAutoGenerated: true, autoGenerateBoth: autoGenerateBoth};
            var successParameters = {accountInfoModel:  this.accountInfoModel, settingsModel: this.settingsModel, isAutoGenerated: true, accountView: this};

            this.checkDBForDuplicateNumber(dataParameters, successParameters);
        },

        //check if a TOS is required and if there are available licenses
        checkTOSAndLicenses: function (dataParameters, successParameters) {
            var endPoint = qcssapp.Location + '/api/account/tos/status';

            qcssapp.Functions.callAPI(endPoint, 'POST', dataParameters, successParameters,
                function(data, successParameters) {
                    if(data.validLicenses) {

                        //if there are licenses available set the accountStatus
                        successParameters.accountInfoModel.accountStatus = data.accountStatus;

                        //create the account
                        successParameters.accountView.createAccount(successParameters.accountInfoModel);

                    } else {
                        var errorMsg ='Cannot create account due to insufficient licenses. Please contact your supervisor for more information.';
                        qcssapp.Functions.throwFatalError(errorMsg, "login");
                        qcssapp.Functions.hideMask();
                    }
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error checking for TOS and available licenses in account creation");
                    }
                    qcssapp.Functions.hideMask();
                }, '', '', '', false, true
            );
        },

        //creates an employee account in myqc
        createAccount: function(accountInfo) {
            var endPoint = qcssapp.Location + '/api/account/createAccount';
            var successParameters = {
                accountInfo: accountInfo,
                accountView: this
            };
            qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(accountInfo), successParameters,
                function(data, successParameters) {
                    if(data.status == "success") {

                        successParameters.accountView.resetCreateAccount();  //clear create account page fields

                        //if email account was created
                        if(data.hasOwnProperty('email')) {
                            var msg = '<div class="acctCreationSuccessMsgContainer">An invitation will be sent to you at <p class="displayUsername" style="margin-bottom:3px"><strong>'+data.email+'</strong></br></p><p class="displayUsername"> to verify your account</p></br><p class="account-created-subtext">Return to</p></div>';
                            msg += '<div class="loginHereLink align-right italic accent-color-one">Login</div>';
                            qcssapp.CreateAccountSettings.usernameParam = data.email;  //set for url parameter

                            //if username account was created
                        }  else {
                            var msg = '<div class="acctCreationSuccessMsgContainer">Your username is <p class="displayUsername"><strong>'+data.username+'</strong></p></br><p class="account-created-subtext">Use your credentials to</p></div>';
                            msg += '<div class="loginHereLink align-right italic accent-color-one">Login here</div>';

                            qcssapp.CreateAccountSettings.usernameParam =  data.username; //set username for url parameter
                        }

                        //build general message view for placed orders
                        var msgView = new qcssapp.Views.GeneralMsg({
                            title: "Account Created",
                            main: "Your account has been created!",
                            content: msg
                        });

                        //navigate to general msg view
                        qcssapp.Router.navigate('message', {trigger: true});

                    } else {
                        if(data.errorMessage && data.errorMessage.length) {
                            qcssapp.Functions.displayError(data.errorMessage);
                        } else {
                            qcssapp.Functions.displayError('There was a problem creating the account.');
                        }
                    }

                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error response creating account");
                    }
                    qcssapp.Functions.hideMask();
                }
            );
        },

        //creates a person account in myqc
        createPersonAccount: function(accountInfo) {
            var endPoint = qcssapp.Location + '/api/account/person/create';

            var successParameters = {
                accountInfo: accountInfo,
                accountView: this
            };
            qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(accountInfo), successParameters,
                function(data, successParameters) {
                    if(data.status == "success") {

                        successParameters.accountView.resetCreateAccount();  //clear create account page fields

                        var msg = '<div class="acctCreationSuccessMsgContainer">Your login name is <p class="displayUsername"><strong>'+data.email+'</strong></p></br><p class="account-created-subtext">Use your credentials to</p></div>';
                        msg += '<div class="loginHereLink align-right italic accent-color-one">Login here</div>';
                        $('createAccountLowBalanceThreshold').val('');

                        qcssapp.CreateAccountSettings.usernameParam =  data.email; //set username for url parameter

                        //build general message view for placed orders
                        var msgView = new qcssapp.Views.GeneralMsg({
                            title: "Account Created",
                            main: "Your account has been created!",
                            content: msg
                        });

                        //navigate to general msg view
                        qcssapp.Router.navigate('message', {trigger: true});

                    } else {
                        qcssapp.Functions.displayError('There was a problem creating the account.');
                    }

                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error response creating account");
                    }
                    qcssapp.Functions.hideMask();
                }
            );
        },

        //check for duplicate account number or badge number in database
        checkForDuplicateNum: function(e) {
            qcssapp.Functions.resetErrorMsgs();
            var $target = $(e.currentTarget);
            var accountNumber = "", badgeNumber = "";
            var contentMessage = "";

            if(!!($target.attr('id') == 'createAccountAcctNumber')) {
                accountNumber = $target.val();
            } else {
                badgeNumber = $target.val()
            }

            if($target.val() != "") {

                //account number validation
                if (accountNumber != "" && (!(qcssapp.Functions.isNumeric(accountNumber)) || !(accountNumber > -1) || accountNumber.indexOf('.') != -1)) {
                    contentMessage = '<p class="duplicate-popup-text" id="duplicate-text-line1"><b>Invalid Input</b><br/><br/>Please provide a valid account number containing only positive integers.</p>';
                    this.confirmMessage('', contentMessage);
                    $('#createAccountAcctNumber').val('');
                    return;
                }

                //badge number validation
                if (badgeNumber != "" &&(!(qcssapp.Functions.isNumeric(badgeNumber)) || !(badgeNumber > -1) || badgeNumber.indexOf('.') != -1)) {
                    contentMessage = '<p class="duplicate-popup-text" id="duplicate-text-line1"><b>Invalid Input</b><br/><br/>Please provide a valid badge number containing only positive integers.</p>';
                    this.confirmMessage('', contentMessage);
                    $('#createAccountBadgeNumber').val('');
                    return;
                }

                var dataParameters = {accountNumber: accountNumber, badgeNumber: badgeNumber, isAutoGenerated: false};
                var successParameters = {accountInfoModel:  this.accountInfoModel, settingsModel: this.settingsModel, isAutoGenerated: false, accountView:this};

                this.checkDBForDuplicateNumber(dataParameters, successParameters);
            }
        },

        //check for duplicate username or email
        checkForDuplicateName: function(e) {
            var $target = $(e.currentTarget);
            var username ="",email="";

            if($target.val() != "") {
                if(!!($target.attr('id') == 'createAccountEmail')) {
                    email = $target.val();
                } else {
                    username = $target.val()
                }
                var contentMessage = "";

                //email validation
                if(email != "") {
                    if (!(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($target.val()))) {
                        contentMessage = '<p class="duplicate-popup-text" id="duplicate-text-line1"><b>Invalid Input</b><br/><br/>Please provide a valid e-mail address in the proper format.</p>';
                        this.confirmMessage('', contentMessage);
                        $('#createAccountEmail').val('');
                        return;
                    }
                }

                //username in the cloud cannot contain numbers (because of issue in findAvailableLoginName() in Gateway Project)
                if(username != "" && !qcssapp.onGround) {
                    if (/\d/.test($target.val())) {
                        contentMessage = '<p class="duplicate-popup-text" id="duplicate-text-line1"><b>Invalid Input</b><br/><br/>Please provide a valid username without numbers.</p>';
                        this.confirmMessage('', contentMessage);
                        $('#createAccountUsername').val('');
                        return;
                    }

                    if ((/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($target.val())) || $target.val().indexOf('@') != -1) {
                        contentMessage = '<p class="duplicate-popup-text" id="duplicate-text-line1"><b>Invalid Input</b><br/><br/>Please provide a valid username, not an email-address.</p>';
                        this.confirmMessage('', contentMessage);
                        $('#createAccountUsername').val('');
                        return;
                    }
                }

                var dataParameters = {username: username, email: email};
                this.checkDBForDuplicateNames(dataParameters);
            }
        },

        //check for duplicate usernames or emails
        checkDBForDuplicateNames: function(dataParameters) {
            var endPoint = qcssapp.Location + '/api/account/duplicate/name';
            var successParameters = {
                accountView: this
            };

            qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(dataParameters), successParameters,
                function(data, successParameters) {

                    //duplicate username on ground or in cloud... or duplicate email on ground
                    if(data.hasOwnProperty('isDuplicate') && data.isDuplicate) {
                        var contentMessage = "";

                        //duplicate username on the ground
                        if(data.hasOwnProperty('username') && qcssapp.onGround) {
                            contentMessage = '<p class="duplicate-popup-text" id="duplicate-text-line1">This username is already in use. <br/></p><p class="duplicate-popup-text duplicate-text">You can try this suggested username </p><p class="duplicate-popup-text duplicate-text" ><b>'+ data.username+'</b></p>';
                            successParameters.accountView.confirmMessage('', contentMessage);
                            $('#createAccountUsername').val('');

                            //duplicate username in the cloud
                        } else if(data.hasOwnProperty('username') && !qcssapp.onGround){
                            var username = data.username.split('@')[0];
                            contentMessage = '<p class="duplicate-popup-text" id="duplicate-text-line1">The username </p><p class="duplicate-popup-text" style="margin-top:5px;margin-bottom:5px;"><b>'+ username +'</b></p><p class="duplicate-popup-text"> is already associated with an account.</p><p class="duplicate-popup-text duplicate-text">Please provide a different username.</p>';
                            successParameters.accountView.confirmMessage('', contentMessage);
                            $('#createAccountUsername').val('');

                            //duplicate email on ground
                        } else if(data.hasOwnProperty('email') && qcssapp.onGround) {
                            contentMessage = '<p class="duplicate-popup-text" id="duplicate-text-line1">The e-mail address <b>'+ data.email +'</b> is already associated with an account.</p><p class="duplicate-popup-text duplicate-text">Please provide a different e-mail address.</p>';
                            successParameters.accountView.confirmMessage('', contentMessage);
                            $('#createAccountEmail').val('');
                        }
                    }

                    //duplicate email in the cloud
                    if(data.hasOwnProperty('isDuplicateCloud') && data.hasOwnProperty('email') && !qcssapp.onGround) {

                        //if duplicate email in QC_Employees and MMH_GatewayUser tell them to pick new email, inviteStatus doesn't matter OR if duplicate email in cloud only
                        if((data.isDuplicateInstance &&  data.isDuplicateCloud) || (!data.isDuplicateInstance && data.isDuplicateCloud)) {
                            contentMessage = '<p class="duplicate-popup-text" id="duplicate-text-line1">The e-mail address <b>'+ data.email +'</b> is already associated with an account.</p><p class="duplicate-popup-text duplicate-text">Please provide a different e-mail address.</p>';
                            successParameters.accountView.confirmMessage('', contentMessage);
                            $('#createAccountEmail').val('');

                            //if duplicate username in just QC_Employees and inviteStatus is anything but 'Invite Accepted' ask if they want another invite sent
                        } else if (data.isDuplicateInstance && data.inviteStatus != 4 && !data.isDuplicateCloud) {
                            contentMessage = '<p class="duplicate-popup-text" id="duplicate-text-line1">The e-mail address <b>'+ data.email +'</b> is already associated with an account.</p><p class="duplicate-popup-text duplicate-text" >Would you like another invite sent to this email?</p>';
                            qcssapp.CreateAccountSettings.inviteEmployeeId = data.employeeID;
                            $('#createAccountEmail').val('');

                            $.confirm({   //send another invite pop-up
                                title: '',
                                content: contentMessage,
                                buttons: {
                                    sendAnotherInvite: {
                                        text: 'Send Another Invite',
                                        btnClass: 'btn-default send-invite-btn',
                                        action: function() {
                                            qcssapp.Functions.sendAnotherInvite();
                                        }
                                    },
                                    cancel: {
                                        text: 'Cancel',
                                        btnClass: 'btn-default send-invite-close-btn',
                                        action: function() {

                                        }
                                    }
                                },
                                onContentReady: function() {
                                    this.$content.css('word-break', 'break-word');
                                }
                            });
                        }
                    }
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error checking for duplicate account name in account creation");
                    }
                    qcssapp.Functions.hideMask();
                }
            );
        },

        //check for duplicate account numbers or badge numbers
        checkDBForDuplicateNumber: function(dataParameters, successParameters) {
            var endPoint = qcssapp.Location + '/api/account/duplicate/number';

            qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(dataParameters), successParameters,
                function(data, successParameters) {
                    if(data.isDuplicate && !data.isAutoGenerated) {
                        var contentMessage = "";

                        //if duplicate badge number set pop-up message
                        if(data.hasOwnProperty('newAcctNum') && data.newAcctNum != "") {
                            contentMessage = '<p class="duplicate-popup-text">The account number <p/><p class="duplicate-popup-text" style="margin:10px 0 10px 0"><b>'+data.newAcctNum + '</b><br/></p><p class="duplicate-popup-text"> is already in use. </p><p class="duplicate-popup-text" style="margin:20px 10px 0 10px">Please try another account number.</p>';
                            $('#createAccountAcctNumber').val('');

                            //if duplicate account number set pop-up message
                        } else if (data.hasOwnProperty('newBadgeNum') && data.newBadgeNum != ""){
                            contentMessage = '<p class="duplicate-popup-text">The badge number <p/><p class="duplicate-popup-text" style="margin:10px 0 10px 0"><b>'+data.newBadgeNum +'</b><br/></p><p class="duplicate-popup-text"> is already in use. </p><p class="duplicate-popup-text" style="margin:20px 10px 0 10px">Please try another badge number.</p>';
                            $('#createAccountBadgeNumber').val('');
                        }

                        //pop-up message that says the account number or badge number is already in use
                        $.confirm({
                            title: '',
                            content: contentMessage,
                            buttons: {
                                cancel: {
                                    text: 'Close',
                                    btnClass: 'btn-default popup-close-btn duplicate-close-btn',
                                    action: function() {
                                    }
                                }
                            }
                        });

                        qcssapp.Functions.hideMask();

                        //if duplicate not found
                    } else {
                        if(typeof successParameters.accountInfoModel !== 'undefined' && typeof successParameters.settingsModel !== 'undefined') {

                            //if employee(acct) and badge number are both auto generated set them both to the same number
                            if(!successParameters.settingsModel.promptForEmpNumber && !successParameters.settingsModel.promptForBadgeNumber) {
                                successParameters.accountInfoModel.accountNumber = data.newAcctNum;
                                successParameters.accountInfoModel.badgeNumber = data.newAcctNum;
                            } else {
                                //set account number for accountInfoModel
                                if(data.hasOwnProperty('newAcctNum')) {
                                    successParameters.accountInfoModel.accountNumber = data.newAcctNum;
                                } else if(data.hasOwnProperty('newBadgeNum')){  //set badge number for accountInfoModel
                                    successParameters.accountInfoModel.badgeNumber = data.newBadgeNum;
                                }
                            }

                            //if creating the account after the number has been auto generated (otherwise we would be checking for duplicate number on input)
                            if(successParameters.isAutoGenerated) {

                                //check if TOS is required and if there are available licenses
                                var dataParameters = JSON.stringify({accountGroupID: parseInt(successParameters.accountInfoModel.accountGroup), spendingProfileID: parseInt(successParameters.accountInfoModel.spendingProfile), accountTypeID: parseInt(successParameters.accountInfoModel.accountType)});
                                var successParams = {accountInfoModel: successParameters.accountInfoModel, accountView: successParameters.accountView};
                                successParameters.accountView.checkTOSAndLicenses(dataParameters, successParams);

                            } else {
                                qcssapp.Functions.hideMask();
                            }
                        }
                    }
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error checking for duplicate account number in account creation");
                    }
                    qcssapp.Functions.hideMask();
                }, '', '', '', false, true
            );
        },

        //check for a valid password
        checkPasswordRequirements: function() {
            //don't check for password if email account creation
            if(this.settingsModel.accountEmailForUsername && !qcssapp.onGround && !this.personAccountCreation) {
                return true;
            }

            qcssapp.Functions.resetErrorMsgs();

            var passStrength = this.passStrength;
            var password = $('#createAccountPassword').val();
            var passwordConfirm = $('#createAccountConfirmPassword').val();

            //validation
            if ( !( (passStrength == "loose" && this.testStrength(password, false)) || (passStrength == "strict" && this.testStrength(password, true)) ) ) {
                this.openPasswordInfoBox();
                $('#createAccountPassword').val('');
                $('#createAccountConfirmPassword').val('');
                qcssapp.Functions.displayError('Password does not meet requirements.');
                qcssapp.Functions.hideMask();
                return false;
            }

            //check if passwords match
            if ( password !== passwordConfirm ) {
                qcssapp.Functions.displayError('Passwords do not match.');
                qcssapp.Functions.hideMask();
                return false;
            }

            this.accountInfoModel.password = password;
            return true;
        },

        //GET request for password strength requirement
        getPasswordStrengthRequirement: function() {

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/auth/login/settings', 'GET', '','',
                function(data) {

                    if (data["passStrength"] === "loose") {
                        this.passStrength = "loose";
                        this.passMessage = this.looseMsg;
                    }

                }.bind(this),
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error getting password strength requirement");
                    }

                    this.passStrength = "strict";
                    this.passMessage = this.strictMsg;
                }.bind(this)
            );
        },

        //tests the password strength based on if it loose or strict
        testStrength: function(password, isStrict) {
            var uppercase = /[A-Z]/;
            var uppercaseBool = uppercase.test(password) ? 1 : 0;
            var lowercase = /[a-z]/;
            var lowercaseBool = lowercase.test(password) ? 1 : 0;
            var numeric = /[0-9]/;
            var numericBool = numeric.test(password) ? 1 : 0;
            var specialCharacter = /[!@#$%^&*()_+=-?<>|]/;
            var specialCharacterBool = specialCharacter.test(password) ? 1 : 0;
            var length = password.length;
            var verification = specialCharacterBool + uppercaseBool + lowercaseBool + numericBool;
            if(isStrict) {
                return verification >= 3 && length >= 8;
            } else {
                return verification >= 2 && length >= 6;
            }
        },

        lowBalanceChange: function(e) {
            var $target = $(e.currentTarget);
            if($target.val() != "" && qcssapp.Functions.isNumeric($target.val())) {
                var lowBalance = parseFloat($target.val()).toFixed(2);
                $target.val(lowBalance);
            }
        },

        //handles the 'on change' event for the Account Group Selector
        accountGroupChange: function(e) {
            // handled elsewhere in later versions
            if (qcssapp.Functions.checkServerVersion(6,2,0)) {
                return;
            }
            qcssapp.Functions.resetErrorMsgs();

            var $target = $(e.currentTarget);

            //if the selected option is an account group name
            if($target.val() != "") {
                $('#spendingProfileAccountSelect').empty();

                //loop over each account group until you find the account group that matches the selected account group name
                $.each(this.accountGroupsModel, function() {
                    if($target.val() == this.attributes.accountGroupName) {
                        if($('#spendingProfileAccountSelect').length) {
                            $('#spendingProfileAccountSelect').empty();
                        }

                        //if no spending profiles throw error to user that the account group is not configured properly
                        if (this.attributes.spendingProfiles.length == 0){
                            qcssapp.Functions.displayError('No Spending Profiles are mapped to this Account Group.');
                            $('#spendingProfileSelectContainer').hide();
                            $('#accountTypeSpan').text('');

                        //if you have one spending profile hide the selector and set the first option to the name of the spending profile
                        } else if(this.attributes.spendingProfiles.length == 1) {
                            $('#spendingProfileSelectContainer').hide();
                            var spendingProfileNameOne = this.attributes.spendingProfiles[0].spendingProfileName;
                            $('#spendingProfileAccountSelect').append($("<option />").val(spendingProfileNameOne).text(spendingProfileNameOne));
                            $('#accountTypeSpan').text('Account Type: '+this.attributes.accountTypeName);
                            if(this.attributes.accountTypeID == "") {   //if no account groups configured display error
                                qcssapp.Functions.displayError('This Account Group does not have an Account Type configured.');
                                $('#accountTypeSpan').text('');
                            }

                        } else { //otherwise show the selector
                            $('#spendingProfileSelectContainer').show();
                            $('#spendingProfileAccountSelect').append($("<option />").val('').text(''));
                            $('#accountTypeSpan').text('Account Type: '+this.attributes.accountTypeName);
                            if(this.attributes.accountTypeID == "") {   //if no account groups configured display error
                                qcssapp.Functions.displayError('This Account Group does not have an Account Type configured.');
                                $('#accountTypeSpan').text('');
                            }

                            //populate the spending profile selector
                            $.each(this.attributes.spendingProfiles, function() {
                                var $selector = $('#spendingProfileAccountSelect');
                                var spendingProfileName = this.spendingProfileName;
                                $selector.append($("<option />").val(spendingProfileName).text(spendingProfileName));
                            });
                        }
                    }
                });
            } else {   //if the selected option is empty hide the spending profile selector and don't show the Account Type
                $('#spendingProfileSelectContainer').hide();
                $('#accountTypeSpan').text('');
            }
        },

        //set the Account Group Selector and determine whether to hide or show the Spending Profile selector
        setSelectors: function() {
            // handled on change of account groups in versions where A.G.s control the acct creation settings
            if(qcssapp.Functions.checkServerVersion(6,2,1)) {
                return;
            }

            $('#lowBalanceThresholdCreateContainer').hide();

            $('#accountGroupSelect').empty();
            $('#spendingProfileAccountSelect').empty();

            $('#accountGroupSelect').append($("<option />").val('').text(''));
            $('#spendingProfileSelectContainer').hide();

            //if only one account group hide selector and set accountGroupID and accountTypeID
            if(this.accountGroupsModel.length == 1) {
                $('#accountGroupSelectContainer').hide();
                this.accountInfoModel.accountGroup = this.accountGroupsModel[0].get('accountGroupID');
                this.accountInfoModel.accountType = this.accountGroupsModel[0].get('accountTypeID');

                //if only one spending profile then hide selector
                if(this.accountGroupsModel[0].attributes.spendingProfiles.length == 1) {
                    this.accountInfoModel.spendingProfile = this.accountGroupsModel[0].attributes.spendingProfiles[0].spendingProfileID;
                } else {
                    $('#spendingProfileSelectContainer').show();
                    $('#spendingProfileAccountSelect').append($("<option />").val('').text(''));
                    $.each(this.accountGroupsModel[0].attributes.spendingProfiles, function() {  //set the selector options
                        var spendingProfile = this.spendingProfileName;
                        $('#spendingProfileAccountSelect').append($("<option />").val(spendingProfile).text(spendingProfile));
                    });
                }
            } else { //if there is more than one account group, show the selector
                $('#accountGroupSelectContainer').show();
                $.each(this.accountGroupsModel, function() {  //set the selector options
                    var acctGroupName = this.attributes.accountGroupName;
                    $('#accountGroupSelect').append($("<option />").val(acctGroupName).text(acctGroupName));
                });
            }
        },

        //opens the password info pop-up when the info icon is clicked
        openPasswordInfoBox: function() {
            $.confirm({
                title: '<h2 id="password-popup-title"><b><u>Password Criteria</u></b></h2>',
                content: this.passMessage,
                buttons: {
                    cancel: {
                        text: 'Close',
                        btnClass: 'btn-default popup-close-btn',
                        action: function() {

                        }
                    }
                }
            });
        },

        //when navigating back to the create account page for the second time, didn't want to call displayButtons again
        //because then it would add the buttons twice. But the old 'this' object would be passed to the checkFields functions
        //when createAccountBtn was pressed. Tried using .off() and unbind() but didn't work, this solution works although I don't like it
        removeButtons: function() {
            var $createAcctContainer = $('#createAccount-input-container');
            $createAcctContainer.find('#createAccountBtn').remove();
            $createAcctContainer.find('#createAccountCancel').remove();
            this.displayButtons();
        },

        //clears the fields in the 'Create Account' page
        resetCreateAccount: function() {
            var $clearFieldsArr = [$('#createAccountAccessCode'), $('#createAccountFirstName'), $('#createAccountMiddleInit'), $('#createAccountLastName'), $('#createAccountUsername'), $('#createAccountConfirmUsername'), $('#createAccountPassword'), $('#createAccountConfirmPassword'), $('#createAccountEmail'), $('#createAccountConfirmEmail'), $('#createAccountAcctNumber'), $('#createAccountBadgeNumber')]
            $.each($clearFieldsArr, function() {
                this.val('');
            });
            $('#accountTypeSpan').text('');

            $('#confirm-email, #confirm-username, #confirm-password').attr('hidden', true);
        },

        //template for pop-up with just a Close button
        confirmMessage: function(title, content) {
            $.confirm({
                title: title,
                content: content,
                buttons: {
                    cancel: {
                        text: 'Close',
                        btnClass: 'btn-default popup-close-btn',
                        action: function() {

                        }
                    }
                },
                onContentReady: function() {
                    this.$content.css('word-break', 'break-word');
                }
            });
        }
    });
})(qcssapp, jQuery);