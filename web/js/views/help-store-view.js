;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.HelpStoreView - View for displaying help info on the store page
     */
    qcssapp.Views.HelpStoreView = Backbone.View.extend({
        internalName: "Help Store View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'help-store-view',

        template: _.template( $('#help-store-template').html() ),

        events: {
            'click .help-next' : 'nextHelpSlide',
            'click .help-close' : 'closeHelp'
        },

        storeHelp: [
            {
                id: 1,
                title: 'Express Reorders',
                text: 'Quickly reorder one of your five most recent orders with the tap of a button',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: ''
            }, {
                id: 2,
                title: 'Express Reorders',
                text: 'Swipe left or right to view your recent orders',
                buttonText: 'Next',
                showArrow: '',
                showHand: ''
            }, {
                id: 3,
                title: 'Express Reorders',
                text: 'Or click the arrows to navigate through your recent orders',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: ''
            }, {
                id: 4,
                title: 'Express Reorders',
                text: 'Expand your orders to see all the products and prices',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: ''
            }, {
                id: 5,
                title: 'Favorite Orders',
                text: 'Click the heart to favorite your order and give it a name',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: ''
            }, {
                id: 6,
                title: 'Place Future Orders',
                text: 'Click the calendar icon to choose a future date to place your order',
                buttonText: 'Done',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }
        ],

        initialize: function (options) {
            this.options = options || {};

            this.parentView = this.options.parentView;

            this.$view = $('#orderview');

            this.$expressElements = $('#express-reorders, #express-reorders-btn-container, #express-arrow-left, #express-arrow-right, #express-dots');
            this.$calendarIcon = $('.calendar-icon');

            this.slideEnd = 0;
            this.slideStart = 0;

            this.expressReorders = false;
            this.futureOrders = false;
        },

        render: function() {
            this.$el.html( this.template( {} ) );

            this.renderSlides();
            this.determineSlide();

            return this;
        },

        renderSlides: function() {
            $.each(this.storeHelp, function(index, slideInfo) {
                var helpSlideView = new qcssapp.Views.HelpSlideView(slideInfo);

                this.$el.append( helpSlideView.render().$el.find('.help-slide') );

            }.bind(this));
        },

        determineSlide: function() {
            this.expressReorders = this.$view.find('#express-reorders').css('display') != 'none';
            this.futureOrders = this.$view.find('.calendar-icon').css('display') != 'none';
            this.oneExpressReorder = this.$view.find('#express-dots').children().length == 1;

            if(this.expressReorders && this.futureOrders) {
                this.slideEnd = 6;
                this.slideStart = 1;

            } else if(this.expressReorders && !this.futureOrders) {
                this.slideEnd = 5;
                this.slideStart = 1;

            } else if(!this.expressReorders && this.futureOrders) {
                this.slideEnd = 6;
                this.slideStart = 6;
            }

            this.showPageContent();
        },

        showPageContent: function() {
            var $slickActive = $('.slick-active');

            switch(this.slideStart) {
                case 1:
                    this.$expressElements.addClass('active-help');
                    break;
                case 2:
                    this.$expressElements.addClass('active-help');

                    var $secondSlide = this.$el.find('.help-slide[data-slide-id="2"]');
                    $secondSlide.find('.help-text').text('Swipe left or right to view your recent orders');

                    if(this.oneExpressReorder) {
                        $secondSlide.find('.help-text').text('Once there is more than one Express Reorder you will be able to swipe left or right to view your other orders');
                        $secondSlide.find('.help-point').hide();
                        $secondSlide.find('.help-hand').hide();
                        $secondSlide.find('.help-arrow').hide();

                        var $slideInfo = $secondSlide.find('.help-info');
                        var $expressReorders = $('#express-reorders');

                        if( $slideInfo.length && $expressReorders.length ) {
                            var expressReorderTop = $expressReorders[0].offsetTop;
                            var slideHeight = $slideInfo[0].clientHeight;
                            $slideInfo.css('top', expressReorderTop - slideHeight - 20);
                        }
                    }

                    break;
                case 3:
                    this.$expressElements.addClass('active-help');
                    break;
                case 4:
                    this.$expressElements.addClass('active-help');

                    this.orderExpanded = true;
                    if(!$slickActive.find('.express-order-line').hasClass('expanded')) {
                        $slickActive.find('.expandEvent').trigger('click');
                        this.orderExpanded = false;
                    }

                    var $nextSlide = this.$el.find('.help-slide[data-slide-id="4"]');
                    $nextSlide.removeClass('active-slide');

                    setTimeout(function(){
                        $nextSlide.addClass('active-slide');

                        var $expressReorders = $('#express-reorders');
                        var $helpHandSlide4 = $('.help-slide[data-slide-id="4"] .help-hand');
                        var $helpInfoSlide4 = $('.help-slide[data-slide-id="4"] .help-info');

                        qcssapp.Functions.setHandTopReverse($expressReorders, $helpHandSlide4, 5);
                        qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide4, $helpInfoSlide4, 20);

                    }.bind(this) ,300);

                    break;
                case 5:
                    this.$expressElements.addClass('active-help');

                    if(!this.orderExpanded) {
                        $slickActive.find('.expandEvent').trigger('click');
                    }

                    break;
                case 6:
                    this.$expressElements.removeClass('active-help');
                    break;
                default:
                    break;
            }
        },

        nextHelpSlide: function() {
            //if at the end of the slide length
            if( this.slideStart == this.slideEnd ) {
                this.parentView.fadeOut();
                return;
            }

            var $currentSlide = this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]');
            $currentSlide.removeClass('active-slide').addClass('inactive-slide');

            this.slideStart++;

            if(this.oneExpressReorder && this.slideStart == 3) {
                this.slideStart++;
            }

            var $nextSlide = this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]');
            $nextSlide.addClass('active-slide');

            //if the next slide is the last, change button text to Done
            if( this.slideStart == this.slideEnd ) {
                $nextSlide.find('.help-next').text('Done');
            }

            this.showPageContent();
        },

        setSlideStyles: function() {
            var $slickActive = $('.slick-active');

            this.orderExpanded = true;
            if(!$slickActive.find('.express-order-line').hasClass('expanded')) {
                this.orderExpanded = false;
            }

            if(this.expressReorders) {
                var $expressReorders = $('#express-reorders');
                var $expressReorderBtn = $('#express-reorder-btn');
                var $expressArrowRight = $('#express-arrow-right');
                var $expressReorderContainer = $expressReorders.find('.slick-active .express-elements-container').eq(0);
                var expressReorderOffsetTop = $expressReorders.length ? $expressReorders.offset().top : 0;
                var expressReorderContainerHeight = $expressReorderContainer.length ? $expressReorderContainer[0].clientHeight : 0;

                // Slide 1
                var $helpHandSlide1 = $('.help-slide[data-slide-id="1"] .help-hand');
                var $helpInfoSlide1 = $('.help-slide[data-slide-id="1"] .help-info');

                qcssapp.Functions.setHandTopReverse($expressReorderBtn, $helpHandSlide1, 10);

                var extraPadding = this.orderExpanded && ($helpHandSlide1[0].offsetTop - $expressReorders[0].offsetTop + 20) > 25 ? $helpHandSlide1[0].offsetTop - $expressReorders[0].offsetTop + 10 : 25;
                qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide1, $helpInfoSlide1, extraPadding);

                // Slide 2
                var $helpHandSlide2 = $('.help-slide[data-slide-id="2"] .help-hand');
                var $helpInfoSlide2 = $('.help-slide[data-slide-id="2"] .help-info');
                var $helpArrowSlide2 = $('.help-slide[data-slide-id="2"] .help-arrow');

                var helpArrow2Height = $helpArrowSlide2.find('.help-arrow-line').css('height');
                helpArrow2Height = Number(helpArrow2Height.replace('px', '')) + 23;
                $helpArrowSlide2.css('top', expressReorderOffsetTop + Number(expressReorderContainerHeight - helpArrow2Height));

                qcssapp.Functions.setHandTopReverse($helpArrowSlide2, $helpHandSlide2, 25);

                var extraPadding2 = this.orderExpanded && ($helpHandSlide2[0].offsetTop - $expressReorders[0].offsetTop + 20) > 20 ? $helpHandSlide2[0].offsetTop - $expressReorders[0].offsetTop + 10 : 20;
                qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide2, $helpInfoSlide2, extraPadding2);

                //Slide 3
                var $helpHandSlide3 = $('.help-slide[data-slide-id="3"] .help-hand');
                var $helpInfoSlide3 = $('.help-slide[data-slide-id="3"] .help-info');

                var expressArrowHeight = Number($expressArrowRight.css('border-top').substring(0,2)) * 2;

                this.setHandTop($expressArrowRight, $helpHandSlide3, 0);
                var extraPadding3 = this.orderExpanded && ($helpHandSlide3[0].offsetTop - $expressReorders[0].offsetTop + 10) > 10 ? $helpHandSlide3[0].offsetTop - $expressReorders[0].offsetTop + 10 : 10;
                qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide3, $helpInfoSlide3, extraPadding3);

                //Slide 5
                var $helpHandSlide5 = $('.help-slide[data-slide-id="5"] .help-hand');
                var $helpInfoSlide5 = $('.help-slide[data-slide-id="5"] .help-info');

                var $heartIcon = $('.slick-active').find('.favorite-order-icon');

                qcssapp.Functions.setHandTopReverse($heartIcon, $helpHandSlide5, 10);

                var extraPadding5 = this.orderExpanded && ($helpHandSlide5[0].offsetTop - $expressReorders[0].offsetTop + 20) > 20 ? $helpHandSlide5[0].offsetTop - $expressReorders[0].offsetTop + 20 : 20;
                qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide5, $helpInfoSlide5, extraPadding5);
            }

            if(this.futureOrders) {
                //Slide 6
                var $calendarIcon = $('.calendar-icon');
                var $calendarIconClone = $calendarIcon.clone();
                var $helpSlide6 = $('.help-slide[data-slide-id="6"]');
                var $helpInfoSlide6 = $('.help-slide[data-slide-id="6"] .help-info');
                var $helpHandSlide6 = $('.help-slide[data-slide-id="6"] .help-hand');

                var calendarOffsetTop = $calendarIcon.length ? $calendarIcon[0].offsetTop : 0;
                var calendarRight = $calendarIcon.css('right');
                $calendarIconClone.css({'top': calendarOffsetTop, 'right': calendarRight});
                $helpSlide6.append($calendarIconClone);

                var helpHandSlide6OffsetTop = $helpHandSlide6.length ? $helpHandSlide6[0].offsetTop : 0;
                var helpHandSlide6Height = $helpHandSlide6.length ? $helpHandSlide6[0].clientHeight : 0;

                $helpInfoSlide6.css('top', helpHandSlide6OffsetTop + helpHandSlide6Height + 20);
            }

            this.hideInactiveSlides();
        },

        setHandTop: function($element, $helpHand, extraPadding) {
            if(!$element.length || !$helpHand.length) {
                return;
            }

            var elementOffsetTop = $element[0].offsetTop;
            var helpHandHeight = $helpHand[0].clientHeight;

            $helpHand.css('top', elementOffsetTop - helpHandHeight - extraPadding);
        },

        hideInactiveSlides: function() {
            if(this.slideStart != 0) {
                this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]').addClass('active-slide');
                this.$el.find('.help-slide[data-slide-id!="'+this.slideStart+'"]').addClass('inactive-slide');
            }
        },

        closeHelp: function() {
            this.parentView.fadeOut();
        }
    });
})(qcssapp, jQuery);
