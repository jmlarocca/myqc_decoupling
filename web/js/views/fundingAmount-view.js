(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FundingAmount - View for displaying spending profile options individually
     */
    qcssapp.Views.FundingAmount = Backbone.View.extend({
        internalName: "Funding Amount Option View", //NOT A STANDARD BACKBONE PROPERTY

        // Cache the template function for a single item.
        template: _.template( $('#funding-amount-template').html() ),

        render: function() {

            if(Number(this.model.get('amount')) < 0) {
                var amount = Number(this.model.get('amount')) * -1;
                this.model.set('amountText', qcssapp.Functions.formatPriceInApp(amount, '-'));
            } else {
                this.model.set('amountText', qcssapp.Functions.formatPriceInApp(this.model.get('amount')));
            }

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.setElement( this.template( this.model.attributes ) );

            return this;
        }
    });
})(qcssapp, jQuery);
