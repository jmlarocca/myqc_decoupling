;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ProductPrepOptionView - View for displaying prep options
     */
    qcssapp.Views.ProductPrepOptionView = Backbone.View.extend({
        internalName: "Product Prep Option View",

        el: '.prep-option_container', //references existing HTML element for order review - can be referenced as this.$el throughout the view

        // Cache the template function for a single item.
        template: _.template( $('#product-prep-option-template').html() ),

        //events: //events not needed for this view
        render: function() {

            this.model.set('priceText', qcssapp.Functions.formatPrice(this.model.get('price'), '+'));
            this.model.set('showPrice', '');

            if(Number(this.model.get('price')) <= 0) {
                this.model.set('priceText', '');
                this.model.set('showPrice', 'hidden');
            }

            this.setElement(this.template(this.model.toJSON()));

            this.$el.attr('data-id', this.model.get('id'));

            if(this.model.get('defaultOption')) {
                this.$el.addClass('default-prep');
            }

            delete this.model.attributes.priceText;
            delete this.model.attributes.showPrice;

            return this;
        }
    });
})(qcssapp, jQuery);
