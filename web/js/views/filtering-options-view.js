;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FilteringOptionsView - View for presenting filtering options
     */
    qcssapp.Views.FilteringOptionsView = Backbone.View.extend({
        internalName: "Filtering Options View", //NOT A STANDARD BACKBONE PROPERTY

        el: "#filtering-options-page", //references existing HTML element - can be referenced as this.$el throughout the view

        events: {
            "click .filtering-options-close": "closeButton",
            "click .filtering-orderMethodBtn": "toggleOrderMethod",
            "click .filtering-options-button": "applyOptions"
        },

        initialize: function(options) {
            this.options = options || {};

            this.$orderMethodFilter = $(".filtering-orderMethodFilter");
            this.$locationSelector = $("#location-selector");
            this.$locationSelectHeader = $(".location-select-header");

            this.reset();
            this.loadLocations();
        },

        reset: function() {
            this.$locationSelector.empty();
        },

        loadLocations: function() {
            qcssapp.Functions.showLocationSelector(this.render.bind(this));
        },

        render: function(locations) {
            qcssapp.primaryOrgLevels = new qcssapp.Collections.OrgLevels(locations);
            if (!locations) {
                qcssapp.Functions.throwFatalError("An error occurred while loading account details, please log in again",'login');
                return;
            }
            var orgLevel = qcssapp.accountModel.get("orgLevelPrimaryData").orgLevelPrimaryId;

            if (qcssapp.Functions.checkServerVersion(6, 2, 8)) {
                var pickupAvailable = $.inArray(true, locations.map(loc => loc.pickupAvailable)) !== -1;
                var deliveryAvailable = $.inArray(true, locations.map(loc => loc.deliveryAvailable)) !== -1;
                if (!pickupAvailable || !deliveryAvailable) {
                    this.$orderMethodFilter.hide();
                }
                if (!pickupAvailable) {
                    qcssapp.locationFilterSettings.orderMethod = "delivery";
                } else {
                    qcssapp.locationFilterSettings.orderMethod = "pickup";
                }
            }

            // Add the locations to the location selector
            var that = this;
            $.each(locations, function() {
                var locationSelectView = new qcssapp.Views.LocationSelectView({model: this});
                that.$locationSelector.append(locationSelectView.render().el);
            });

            // If allowing skip, add "All Locations"
            if (qcssapp.locationFilterSettings.myQCAllowOnlineOrderLocationSkip) {
                var locationSelectView = new qcssapp.Views.LocationSelectView({model: {
                    orgLevelPrimaryId: 0,
                    name: "All Locations"
                }});
                this.$locationSelector.prepend(locationSelectView.render().el);
                if (!orgLevel) {
                    orgLevel = this.$locationSelector.children().first().val();
                }
            } else {
                this.$locationSelectHeader.text("Location*");
            }

            // Make default selections
            this.$locationSelector.val(orgLevel || 0).trigger("change");
            if (qcssapp.locationFilterSettings.orderMethod === "delivery") {
                $(".filtering-orderMethodBtn.orderMethodDelivery").trigger("click");
            } else {
                $(".filtering-orderMethodBtn.orderMethodPickup").trigger("click");
            }

            // Show the view
            this.$el.removeClass("hide-page");
            setTimeout(function() {
                this.$el.parent().css("background", "rgba(0,0,0,0.5)");
            }.bind(this), 200);

            $("#filteringoptionsview").off().on("click", this.checkForClickOutsideModal);
        },

        checkForClickOutsideModal: function(e) {
            var container = $("#filtering-options-page");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.unbind("click", e.target);
                $('.filtering-options-close').click();
            }
        },

        closeButton: function() {
            // If the location is required and they have not selected a location, go back to the home menu on cancel
            if (!qcssapp.locationFilterSettings.myQCAllowOnlineOrderLocationSkip &&
                ($(".location-select").attr("data-OrgLevelId") == "0" ||
                    !qcssapp.accountModel.get("orgLevelPrimaryData").orgLevelPrimaryId))
            {
                $(".home").first().trigger("click");
            }
            this.closeView();
        },

        toggleOrderMethod: function(e) {
            var $prevFilter = $(".filtering-orderMethodBtn.selected");
            $prevFilter.removeClass("selected secondary-background-color");
            $(e.target).addClass("selected");
        },

        applyOptions: function() {
            qcssapp.locationFilterSettings.orderMethod = $(".filtering-orderMethodBtn.selected").text().toLowerCase();

            var orgLevelPrimaryId = this.$locationSelector.val();
            var orgLevelName = this.$locationSelector.find("option:selected").text();

            if ((!qcssapp.locationFilterSettings.myQCAllowOnlineOrderLocationSkip ||
                !qcssapp.locationFilterSettings.myQCAllowOnlineOrderLocationSkip) && !orgLevelPrimaryId)
            {
                $.alert("You must choose a location before continuing", "Warning");
                return;
            }

            var orgLevelData = {
                orgLevelPrimaryId: (orgLevelPrimaryId == "0" ? "" : orgLevelPrimaryId),
                name: orgLevelName
            };
            qcssapp.accountModel.set("orgLevelPrimaryData", orgLevelData);

            // After the first time prompting them, don't do that again this session
            // This property is not part of the model so it will not be saved anywhere
            qcssapp.locationFilterSettings.userWasPromptedForLocation = true;
            var dataParameters = {
                orgLevelPrimaryId: orgLevelPrimaryId
            };
            qcssapp.Functions.saveOrgLevel(dataParameters);

            // Return to the store page
            this.options.applyCallback();
            this.closeView();
        },

        closeView: function() {
            this.$el.off();
            this.$el.removeClass("active-slide").addClass("hide-page");
            this.$el.parent().css("background", "");
            setTimeout(function() {
                qcssapp.Router.navigate("show-order", {trigger: true});
            }, 300);
        }

    });

})(qcssapp, jQuery);