;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FavoriteOrderView - View for displaying favorited orders
     */
    qcssapp.Views.FavoriteOrderView = Backbone.View.extend({
        internalName: "Favorite Order View", //NOT A STANDARD BACKBONE PROPERTY

        id: 'favorite-order-page', //references existing HTML element

        events: {
            'input #favoriteOrderName': 'changeOrderName',
            'click .favorite-order-icon' : 'toggleFavoriteOrder',
            'click .add-to-cart-icon' : 'addOrderToCart',
            'click #order-arrow-left' : 'clickArrowLeft',
            'click #order-arrow-right' : 'clickArrowRight'
        },

        template: _.template( $('#favorite-order-template').html() ),

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in favorite order view");
            }

            this.$container = $('#favorite-order');

            this.$container.html(this.$el.html( this.template() ) );

            this.options = options.data || '';

            this.orderList = options.params.orderList || [];
            this.indexInList = options.params.indexInList || '';

            this.favoriteOrderModel = new qcssapp.Models.FavoriteOrder( this.options );

            this.items = this.options.items;

            this.collection = new qcssapp.Collections.FavoriteOrderItemCollection();

            this.$itemsList = $('#favorite-order-items-list');
            this.$favoriteOrderName = $('#favoriteOrderName');
            this.$arrowLeft = $('#order-arrow-left');
            this.$arrowRight = $('#order-arrow-right');
            this.$icons = $('#favorite-order-icons');

            this.updateIcons();

            this.render();

            this.setArrows();
        },

        //calls renderLine on each item in the spending profile collection
        render: function() {
            this.displayButtons();

            if(this.options.name != "") {
                this.$favoriteOrderName.val(qcssapp.Functions.htmlDecode(this.options.name));
            }

            this.collection.set(this.items);

            this.favoriteOrderModel.set('items', this.collection);

            var subtotal = 0;
            var comboInProgressID = "";

            this.collection.each(function ( mod ) {
                mod.set('id', mod.get('productID'));

                //calculate the total
                mod.set('total', mod.get('quantity') * mod.get('price'));
                mod.set('productName', mod.get('name'));

                if(mod.get('diningOptionProduct')) {
                    return true;
                }

                //if there is a prep option on the product but not a prep option set, then that signifies this product wasn't originally favorited with a prep option but a prep option set was added later
                if(mod.get('prepOption') != null && mod.get('prepOptionSetID') == null) {
                    mod.set('defaultPrepOption', mod.get('prepOption')); //use the default prep option set to hide prep option but add it behind the scenes
                    mod.set('prepOptionSetID', mod.get('prepOption').prepOptionSetID);
                    mod.set('prepOption', null);
                }

                if(mod.get('modifiers') != null && mod.get('modifiers').length > 0) {
                    $.each(mod.get('modifiers'), function() {
                        if(this.prepOption != null && this.prepOptionSetID == null) {
                            this.defaultPrepOption = this.prepOption;
                            this.prepOptionSetID = this.prepOption.prepOptionSetID;
                            this.prepOption = null;
                        }
                    });
                }

                //if the product does not contain a comboModel then add product line
                if( mod.get('comboTransLineItemId') == "" || mod.get('comboTransLineItemId') == null ) {
                    this.renderLine( mod , false );
                    subtotal += Number(mod.get('total'));
                    comboInProgressID = "";

                    //if the combo still needs to list out details, add product line as combo product line
                } else if (comboInProgressID == mod.get('comboTransLineItemId')) {
                    this.renderLine( mod, true );

                    //otherwise adding a new combo so add the title combo line first then a line for the product
                } else {
                    this.renderComboLine( mod );
                    this.renderLine( mod, true );
                    subtotal += Number(mod.get('comboTotal'));
                    comboInProgressID = mod.get('comboTransLineItemId');
                }

            }, this );

            //calculate and add subtotal,
            this.$itemsList.append('<li class="cart-line subtotal-line">Subtotal:<span class="subtotal accent-color-one">'+ qcssapp.Functions.formatPrice(subtotal) +'</span></li>');

            $('#favorite-order-items-list').find('.cart-line-healthy-icons').hide();

            if(qcssapp.CurrentOrder.storeModel && typeof qcssapp.CurrentOrder.storeModel.get('chargeAtPurchaseTime') !== 'undefined' && !qcssapp.CurrentOrder.storeModel.get('chargeAtPurchaseTime')) {
                this.$icons.addClass('open-favorite-order')
            }
        },

        renderLine: function( mod, isCombo ) {
            mod.set('total', Number( mod.get('total').toFixed(2) ) );

            if( typeof mod.get("fixedTareWeight") !== 'undefined' && mod.get("fixedTareWeight") != "" && typeof mod.get("tareName") !== 'undefined' && mod.get("tareName")!= "" ) {
                mod.set("grossWeightInfo","<div>Gross Weight: " +(Number(mod.get("quantity")) + Number(mod.get("fixedTareWeight")) ).toFixed(2)+" lbs</div>");
                mod.set("tareInfo","<div>"+ mod.get("tareName") +": "+ mod.get("fixedTareWeight") +" lbs</div>");
            }

            if(isCombo) {
                mod.set('isCombo', true);
            }

            var cartLineView = new qcssapp.Views.cartLineView({
                model:mod
            });

            cartLineView.render().$el.addClass('favorite-order-line');
            this.$itemsList.append(cartLineView.$el);
        },

        renderComboLine: function( mod ) {
            var comboLineView = new qcssapp.Views.comboLineView({
                model:mod
            });

            comboLineView.render().$el.addClass('favorite-order-line');
            this.$itemsList.append(comboLineView.render().el);
        },

        displayButtons: function() {
            if ( $('#saveFavoriteNameBtn').length ) {
                return;
            }

            new qcssapp.Views.ButtonView({
                text: "Cancel",
                appendToID: "#favorite-order-name-btn-container",
                id: "cancelFavoriteNameBtn",
                buttonType: 'customCB',
                class: "template-gen prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                parentView:this,
                callback: function() {
                    //change back favorite order name and hide buttons
                    this.parentView.$favoriteOrderName.val(this.parentView.options.name);
                    $('#saveFavoriteNameBtn, #cancelFavoriteNameBtn, #favoriteOrderErrorContainer').hide();
                }
            });

            new qcssapp.Views.ButtonView({
                text: "Save",
                appendToID: "#favorite-order-name-btn-container",
                id: "saveFavoriteNameBtn",
                buttonType: 'customCB',
                class: "save-order-btn template-gen prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                parentView:this,
                callback: function() {
                    this.parentView.saveOrderName();
                }
            });

            $('#saveFavoriteNameBtn, #cancelFavoriteNameBtn').hide();
        },

        setArrows: function() {
            if(this.orderList.length == 1) {
                this.$arrowLeft.hide();
                this.$arrowRight.hide();
            } else if (this.indexInList == 0) {
                this.$arrowLeft.hide();
            } else if (this.indexInList == this.orderList.length - 1) {
                this.$arrowRight.hide();
            }
        },

        clickArrowLeft: function() {
            var previousIndex = Number(this.indexInList) - 1;

            if(typeof this.orderList[previousIndex] !== 'undefined') {
                var previousFavoriteOrderID = this.orderList[previousIndex].id;

                var params = {
                    orderList: this.orderList,
                    indexInList: previousIndex
                };

                qcssapp.Functions.loadFavoriteOrder(previousFavoriteOrderID, params);
            }
        },

        clickArrowRight: function() {
            var nextIndex = Number(this.indexInList) + 1;

            if(typeof this.orderList[nextIndex] !== 'undefined') {
                var nextFavoriteOrderID = this.orderList[nextIndex].id;

                var params = {
                    orderList: this.orderList,
                    indexInList: nextIndex
                };

                qcssapp.Functions.loadFavoriteOrder(nextFavoriteOrderID, params);
            }
        },

        updateIcons: function() {
            var originalSetting = qcssapp.usingBranding;

            //if not using branding still set svg color
            if(!qcssapp.usingBranding) {
                qcssapp.usingBranding = true;
            }

            qcssapp.Functions.setSVGColors();
            qcssapp.usingBranding = originalSetting;

            $('img.add-to-cart-icon').hide();
            $('img.express-cart-icon').hide();

            this.$el.find('.favorite-order-icon').css({'border':'', 'margin':''});
        },

        //if the order name was changed show Save and Cancel buttons
        changeOrderName: function() {
            $('#favoriteOrderErrorContainer').hide();
            if(this.$favoriteOrderName.val() != this.options.name) {
                $('#saveFavoriteNameBtn, #cancelFavoriteNameBtn').show();
            } else {
                $('#saveFavoriteNameBtn, #cancelFavoriteNameBtn').hide();
            }
        },

        //save the change to the order name
        saveOrderName: function() {
            var name = this.$favoriteOrderName.val();

            if(name == "") {
                qcssapp.Functions.displayError("Please enter a valid name for your favorite order.");
                return;
            }

            var orderDetails = {
                'ID':this.favoriteOrderModel.get('id'),
                'NAME':name
            };

            var callback = function(callbackParams) {
                $('#saveFavoriteNameBtn, #cancelFavoriteNameBtn').hide();
                callbackParams.orderView.options.name = callbackParams.orderView.$favoriteOrderName.val()
            };

            var callbackParams = {orderView: this};

            this.updateFavoriteOrderName(orderDetails, callback, callbackParams);
        },

        updateFavoriteOrderName: function(orderHM,  callback,  callParams) {
            var endPoint = qcssapp.Location + '/api/favorite/order/name';

            qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(orderHM), '',
                function(data) {
                    if(data != 1) {
                        qcssapp.Functions.displayError('There was an issue trying to change the name of the favorite order');
                        return;
                    }

                    if(callback && typeof callback == 'function') {
                        callback(callParams);
                    }
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error trying to change favorite order name.");
                    }
                },
                '', '', '', false
            );
        },

        toggleFavoriteOrder: function() {
            var favoriteID = this.options.id;
            if(favoriteID == null || favoriteID == "") {
                qcssapp.Functions.displayError('There was an issue while trying to update this favorite order');
            }

            var callback = function() {
                var $favoriteOrderIcon = $('#favorite-order-icons').find('.favorite-order-icon');
                var src = $favoriteOrderIcon.attr('src') === 'images/icon-favorites-unchecked.svg' ? 'images/icon-favorites.svg' : 'images/icon-favorites-unchecked.svg';
                $favoriteOrderIcon.attr('src', src);
            };

            qcssapp.Functions.toggleFavoriteOrder(favoriteID, callback, false);
        },

        //if the add to cart button was clicked, add all products in the order to the cart
        addOrderToCart: function() {
            var productsToAddToCart = [];
            var that = this;

            this.collection.each(function ( mod ) {

                that.reformatFavoriteLines(mod);

                if(mod.get('diningOptionProduct')) {
                    return;
                }

                //create new product model
                var productModel = new qcssapp.Models.Product( mod.attributes );
                productModel.set('productID', productModel.get('id'));
                productsToAddToCart.push(productModel);

            }, this );

            var callback = function() {
                qcssapp.Router.navigate('cart', {trigger: true});
            };

            for (var i = 0; i < productsToAddToCart.length; i++) {
                var product = productsToAddToCart[i];

                qcssapp.Functions.addProductToCart(product);
            }

            if( qcssapp.CurrentOrder.products.length > 1 ) {
                qcssapp.Functions.checkProductCombos(callback);
                return;
            }

            callback();
        },

        reformatFavoriteLines: function(mod) {
            //don't add dining option products to the cart
            if(mod.get('diningOptionProduct')) {
                return true;
            }

            if(typeof mod.get('productName') !== 'undefined') {
                mod.set('name', mod.get('productName'));
                delete mod.attributes.productName;
            }

            //the default prep option is hidden on the Order Details page but added when going to the Cart or Express Reorder, this is because config changed for product or modifier
            if(typeof mod.get('defaultPrepOption') !== 'undefined') {
                mod.set('prepOption', mod.get('defaultPrepOption'));
                delete mod.attributes.prepOption;
            }

            if(mod.get('modifierList') != null && mod.get('modifierList').length > 0) {
                mod.set('modifiers', mod.get('modifierList'));
                $.each(mod.get('modifierList'), function() {
                    if(typeof this.defaultPrepOption !== 'undefined' && this.defaultPrepOption != null) {
                        this.prepOption = this.defaultPrepOption;
                        delete this.defaultPrepOption;
                    }
                });
            }
        }

    });
})(qcssapp, jQuery);   