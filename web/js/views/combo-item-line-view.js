;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ComboItemLineView - View for displaying Combo lines
     */
    qcssapp.Views.ComboItemLineView = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'combo-item-line',

        // Cache the template function for a single item.
        template: _.template( $('#combo-item-line-template').html() ),

        events: {
            'click .combo-line-swap': 'swapComboLine',
            'click .combo-line-info': 'customizeComboLine'
        },

        initialize: function() {
            return this;
        },

        render: function() {
            //format total to two decimals (and leading zero if below 1)
            this.model.attributes.total = Number(this.model.attributes.total).toFixed(2);

            //if quantity more than one then show equation
            if ( this.model.attributes.quantity > 1 && !this.model.get('scaleUsed') ) {
                this.model.set('equation', this.model.get('quantity') + " <span class='quantity'>x</span> " + qcssapp.Functions.formatPriceInApp(this.model.get('price')));
            } else if ( this.model.get('scaleUsed') && Number(this.model.get('quantity')) != 1 ) {
                this.model.set('equation', Number(this.model.get('quantity')).toFixed(2) + ' lbs at ' + qcssapp.Functions.formatPriceInApp(this.model.get('price')));
            } else {
                this.model.set('equation', '&nbsp;')
            }

            //if product has modifiers, create modifiers list csv
            if ( this.model.attributes.modifiers && this.model.attributes.modifiers.length > 0 ) {
                this.model.attributes.modifiersList = qcssapp.Functions.convertModifierAndPrepOptionArrayToCsv(this.model.attributes.modifiers, true);
            } else {
                this.model.attributes.modifiersList = "";
            }

            //don't show the product's price or equation unless there is an upcharge
            this.model.set('equation', '');
            this.model.set('equation', '');

            this.model.set('isCombo', true);
            var name = qcssapp.Functions.buildProductName(this.model);
            if(name != "") {
                this.model.set('productName', name);
            }

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            //need to remove the '$' from the line if it is a combo line and has no upcharge
           this.$el.find('.combo-line-total').text('');

            this.$el.attr('data-productID', this.model.attributes.productID).attr('data-cartProductID', this.model.attributes.id);
            this.setHealthyIcons();
            return this;
        },

        setHealthyIcons: function(){
            if(!qcssapp.Functions.checkServerVersion(3,0))
                return;

            var healthyFlag = false;

            if(this.model.get('healthy')){
                healthyFlag = true;
                var wellnessPath = "<img src=\""+qcssapp.healthyIndicator.wellness+"\" alt=\"Healthy\">";
                this.$el.find('.combo-line-healthy-icons').append(wellnessPath);
            }
            if(this.model.get('vegetarian')){
                healthyFlag = true;
                var vegetarianPath = "<img src=\""+qcssapp.healthyIndicator.vegetarian+"\" alt=\"Vegetarian\">";
                this.$el.find('.combo-line-healthy-icons').append(vegetarianPath);
            }
            if(this.model.get('vegan')){
                healthyFlag = true;
                var veganPath = "<img src=\""+qcssapp.healthyIndicator.vegan+"\" alt=\"Vegan\">";
                this.$el.find('.combo-line-healthy-icons').append(veganPath);
            }
            if(this.model.get('glutenFree')){
                healthyFlag = true;
                var glutenFreePath = "<img src=\""+qcssapp.healthyIndicator.glutenFree+"\" alt=\"Gluten Free\">";
                this.$el.find('.combo-line-healthy-icons').append(glutenFreePath);
            }
            if(healthyFlag){
                this.$el.find('.combo-line-name').css({'padding-bottom':'0px'});
                this.$el.find('.combo-line-modifiers').addClass('align-healthy-icon-mods');
            }

            if(this.$el.find('.combo-line-healthy-icons').children().length) {
                this.$el.find('.combo-line-healthy-icons').addClass('show-healthy-icons');
            }
        },

        swapComboLine: function() {
            qcssapp.CurrentCombo.swapIndex = $.inArray(this.model, qcssapp.ComboInProgress);

            $('.keypad-list').children().remove();

            var comboKeypadId = "";
            var comboDetailID = this.model.get('comboDetailId');
            var keypadID = this.model.get('keypadID');

            $.each(qcssapp.CurrentCombo.comboDetails, function() {
                if(this.id == comboDetailID || (comboDetailID == null && this.keypadId == keypadID)) {
                    comboKeypadId = this.keypadId;
                    return false;
                }
            });

            if(comboKeypadId == "") {
                qcssapp.Functions.displayError('Unable to swap item in combo');
                return;
            }

            qcssapp.Functions.loadKeypad(comboKeypadId);
        },

        customizeComboLine: function() {
            qcssapp.CurrentCombo.swapIndex = $.inArray(this.model, qcssapp.ComboInProgress);

            // If the product has modifiers, but it doesn't have modifier menus - probably came from express reorder where the menus aren't loaded
            if(this.model.get('modifiers') && this.model.get('modifiers').length > 0 && !(this.model.get('modifierMenus') && this.model.get('modifierMenus').length > 0)) {
                qcssapp.Functions.reloadProductFromCart(this.model);
                return;
            }

            qcssapp.Functions.loadProductFromCombo(this.model);
        }
    });
})(qcssapp, jQuery);
