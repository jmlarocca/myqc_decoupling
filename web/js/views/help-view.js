;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.HelpView - View for displaying help on certain pages
     */
    qcssapp.Views.HelpView = Backbone.View.extend({
        internalName: "Help View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'help-view',

        template: _.template( $('#help-template').html() ),

        page: {
            STORE: "#orderview",
            HOMEKEYPAD: "#keypadview",
            FAVORITES: "#favoriteview",
            CART: "#cartview",
            PURCHASEHISTORY: "#purchasesview",
            RECEIPT: "#receipt-view",
            FUNDING: "#accountfundingview"
        },

        initialize: function (options) {

            this.options = options || {};

            this.hidden = this.options.hidden;

            this.view = this.options.view;
            this.$view = $(this.view);

            this.appendElement = this.options.appendElement;
            this.$appendElement = $(this.appendElement);

            // if opening help for the receipt view, scroll to the top of the view to make sure the icons are clearly visible
            if (this.view == this.page.RECEIPT) {
                var $receipt = $("#receipt");
                if ($receipt.get(0).scrollTop > 0) {
                    $receipt.animate({scrollTop : ("0")}, "fast");
                }
            }

            this.render();
        },

        render: function() {

            this.$el.html( this.template( {} ) );

            if ( this.hidden ) {
                this.$el.addClass('hidden');
            }

            this.$appendElement.append( this.$el );

            this.renderDetailView();
        },

        renderDetailView: function() {
            var helpDetailView = null;
            var options = {parentView: this};

            if(this.view == this.page.STORE) {
                helpDetailView = new qcssapp.Views.HelpStoreView(options);

            } else if(this.view == this.page.HOMEKEYPAD) {
                helpDetailView = new qcssapp.Views.HelpHomeKeypadView(options);

            } else if(this.view == this.page.FAVORITES) {
                helpDetailView = new qcssapp.Views.HelpFavoriteView(options);

            } else if(this.view == this.page.CART) {
                helpDetailView = new qcssapp.Views.HelpCartView(options);

            } else if(this.view == this.page.PURCHASEHISTORY) {
                helpDetailView = new qcssapp.Views.HelpPurchaseHistoryView(options);

            } else if(this.view == this.page.RECEIPT) {
                helpDetailView = new qcssapp.Views.HelpReceiptView(options);

            } else if(this.view == this.page.FUNDING) {
                helpDetailView = new qcssapp.Views.HelpAccountFundingView(options);
            }

            if(helpDetailView == null) {
                return;
            }

            this.$el.append( helpDetailView.render().el );

            this.fadeIn();

            helpDetailView.setSlideStyles();
        },

        fadeIn: function(dur) {
            dur = !dur ? 150 : dur;

            this.hidden = false;
            this.$el.fadeIn(dur, function() {
                this.$el.removeClass('hidden');
            }.bind(this));
        },

        fadeOut: function(dur) {
            dur = !dur ? 150 : dur;

            this.hidden = true;
            this.$el.fadeOut(dur, function() {
                this.$el.remove();
            }.bind(this));
        }
    });
})(qcssapp, jQuery);
