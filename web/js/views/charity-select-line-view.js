;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.CharitySelectLineView - View for displaying select options for charity
     */
    qcssapp.Views.CharitySelectLineView = Backbone.View.extend({
        //is a list tag.
        tagName:  'option',

        // Cache the template function for a single item.
        template: _.template( $('#charity-select-line-template').html() ),

        render: function() {
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            this.$el.attr('value', this.model.get('id'));

            return this;
        }
    });
})(qcssapp, jQuery);
