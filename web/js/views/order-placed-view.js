;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.OrderPlacedView - View for displayed order placed details
     */
    qcssapp.Views.OrderPlacedView = Backbone.View.extend({
        internalName: "Order Placed View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#order-placed-page', //references existing HTML element for order review - can be referenced as this.$el throughout the view

        template: _.template( $('#order-placed-template').html() ),

        initialize: function ( options ) {

            this.options = options || {};

            this.transactionModel = this.options.transactionModel;

            this.total = this.transactionModel.total;
            this.orderNumber = this.transactionModel.orderNumber;
            this.orderType = typeof this.transactionModel.type !== 'undefined' ? this.transactionModel.type : '';
            this.readyTime = this.transactionModel.readyTime;
            this.transactionID = this.transactionModel.transactionID;
            this.diningOptionPAPluName = typeof this.transactionModel.diningOptionPAPluName !== 'undefined' ? this.transactionModel.diningOptionPAPluName : '';
            this.useCreditCardAsTender = typeof this.transactionModel.useCreditCardAsTender !== 'undefined' && this.transactionModel.useCreditCardAsTender ? this.transactionModel.useCreditCardAsTender : false;
            this.chargeAtPurchaseTime = typeof this.transactionModel.chargeAtPurchaseTime !== 'undefined' ? this.transactionModel.chargeAtPurchaseTime : true;

            this.nonVoucherTotal = Number(this.total);
            if (this.transactionModel.voucherTotal) {
                this.nonVoucherTotal -= Number(this.transactionModel.voucherTotal);
            }

            if ( this.orderNumber.length > 3 && !qcssapp.Functions.checkServerVersion(1,6) ) {
                this.orderNumber = this.orderNumber.substr(this.orderNumber.length-3, this.orderNumber.length);
            }

            this.storeName = "";
            if(qcssapp.Functions.checkServerVersion(2,0) && qcssapp.CurrentOrder.storeModel && typeof qcssapp.CurrentOrder.storeModel.get('header1') !== 'undefined') {
                this.storeName = qcssapp.CurrentOrder.storeModel.get('header1');
            }

            this.rewardsEarned = new qcssapp.Collections.RewardsProgram();
            this.rewardsEarned.set(this.transactionModel.loyaltyPointsEarned, {parse: true});
            this.donations = new qcssapp.Collections.Donations();
            this.donations.set(this.transactionModel.employeeDonations, {parse: true});

            this.grabAndGo = qcssapp.CurrentOrder.grabAndGo;
            this.enableFavorites = qcssapp.CurrentOrder.storeModel && typeof qcssapp.CurrentOrder.storeModel.get('enableFavorites') !== 'undefined' ? qcssapp.CurrentOrder.storeModel.get('enableFavorites') : true;
            this.chargeAtPurchaseTime = qcssapp.CurrentOrder.storeModel && typeof qcssapp.CurrentOrder.storeModel.get('chargeAtPurchaseTime') !== 'undefined' ? qcssapp.CurrentOrder.storeModel.get('chargeAtPurchaseTime') : true;
            this.showOrderTypeAndTimeMsg = qcssapp.CurrentOrder.storeModel && typeof qcssapp.CurrentOrder.storeModel.get('showOrderTypeAndTimeMsg') !== 'undefined' ? qcssapp.CurrentOrder.storeModel.get('showOrderTypeAndTimeMsg') : true;
            this.showPickupDeliveryLocationMsg = qcssapp.CurrentOrder.storeModel && typeof qcssapp.CurrentOrder.storeModel.get('showPickupDeliveryLocationMsg') !== 'undefined' ? qcssapp.CurrentOrder.storeModel.get('showPickupDeliveryLocationMsg') : true;

            if(this.grabAndGo) {
                $('#order-placed-header').find('h1').text("Transaction Complete");
            }

            this.render();

        },

        render: function() {

            this.buildOrderPlacedMessages();

            var options = {
                titleMsg: this.titleMsg,
                thankYouMsg: this.thankYouMsg,
                orderNumberMsg: this.orderNumberMsg,
                chargeMsg: this.chargeMsg,
                orderTypeAndTimeMsg: this.orderTypeAndTimeMsg,
                pickupDeliveryLocationMsg: this.pickupDeliveryLocationMsg,
                peakHoursMsg: this.peakHoursMsg,
                rewardsTitle: this.rewardsTitle,
                rewardsTable: this.rewardsTable,
                rewardsDonateMsg: this.rewardsDonateMsg
            };

            this.$el.html( this.template( options ) );

            this.displayButtons();

            //reset future order date
            qcssapp.futureOrderDate = "";

            //clear order history
            qcssapp.Functions.clearHistory();

            //clear current order
            qcssapp.Functions.emptyCart();

            return this;
        },

        buildOrderPlacedMessages: function() {
            this.titleMsg = 'Your Order Has Been Placed!';
            this.thankYouMsg = 'Thanks for ordering with Quickcharge!';
            this.orderNumberMsg = 'Your order number is: <div class="order-placed_order-num">' + this.orderNumber + '</div>';
            this.orderTypeAndTimeMsg = '';
            this.pickupDeliveryLocationMsg = '';
            this.peakHoursMsg = 'During peak hours orders may be delayed. <br>Please plan accordingly.';

            if (this.nonVoucherTotal) {
                if (!this.useCreditCardAsTender) {
                    this.chargeMsg = 'Your account has been charged' + ' <strong>$'+this.nonVoucherTotal.toFixed(2)+'</strong>';
                } else {
                    this.chargeMsg = "Your credit card has been charged" + ' <strong>$'+this.nonVoucherTotal.toFixed(2)+'</strong>';
                }
            }

            if(!this.chargeAtPurchaseTime) {
                this.chargeMsg = "Your order total is"  + ' <strong>$'+this.total.toFixed(2)+'</strong>';
            }

            if(this.storeName != "" && this.showPickupDeliveryLocationMsg) {  //create message for where the order can be picked up or delivered
                this.pickupDeliveryLocationMsg = "Your order will be delivered from <div class='order-placed_order-location'>" + this.storeName + "</div>"; //Delivery
                if(this.diningOptionPAPluName != "") {

                    this.pickupDeliveryLocationMsg = "Your order will be ready at <div class='order-placed_order-location'>" + this.storeName + "</div>";  //Eat In

                    if(this.diningOptionPAPluName.toLowerCase().indexOf('take out') != -1) {
                        this.pickupDeliveryLocationMsg = "Pick up your order at <div class='order-placed_order-location'>" + this.storeName + "</div>";  //Take Out
                    }
                } else if (this.orderType == "pickup") {
                    this.pickupDeliveryLocationMsg = "Pick up your order from <div class='order-placed_order-location'>" + this.storeName + "</div>"; //Pick up
                }
            }

            //create message for order time
            if(this.showOrderTypeAndTimeMsg) {
                if(this.pickupDeliveryLocationMsg == '') {
                    this.orderTypeAndTimeMsg = 'Your order will be ';
                    this.orderTypeAndTimeMsg += qcssapp.CurrentOrder.orderType == "delivery" ? "delivered " : "ready " ;
                }

                if(qcssapp.futureOrderDate) {
                    this.orderTypeAndTimeMsg += " at approximately " + this.readyTime + " on " + qcssapp.Functions.formatFutureDay() + ' ' + qcssapp.Functions.formatFutureDate() + ".";
                } else {
                    this.orderTypeAndTimeMsg += " at approximately " + this.readyTime + ".";
                }
            }

            if(!this.showPickupDeliveryLocationMsg && !this.showOrderTypeAndTimeMsg) {
                this.peakHoursMsg = '';
            }

            if(this.grabAndGo) {
                this.titleMsg = "You're all Set!";
                this.thankYouMsg = 'Thank you for using Quickcharge!';
                this.orderNumberMsg = '';
                this.orderTypeAndTimeMsg = '';
                this.pickupDeliveryLocationMsg = '';
                this.peakHoursMsg = '';
            }

            if (this.rewardsEarned.length > 0) {
                this.rewardsTitle = "Points Earned";
                this.rewardsTable = this.buildRewardsTable();

                // Add available donations to the matching reward program and set button text
                this.rewardsEarned.each(function(reward) {
                    this.donations.each(function(donation) {
                        if (donation.get("loyaltyProgram").id === reward.get("id")) {
                            reward.set("donations", reward.get("donations").concat(donation));
                            this.rewardsDonateMsg = "Donate Your Points";
                        }
                    }.bind(this));
                }.bind(this));
            }
        },

        // Build the table of earned rewards
        buildRewardsTable: function() {
            var rewardsTable = "<table>";
            this.rewardsEarned.each(function(reward) {
                rewardsTable += "<tr><td>" + reward.get("name") + "</td>";
                rewardsTable += "<td class='accent-color-one'>+" + reward.get("newPoints") + " point";
                if (reward.get("newPoints") !== 1) {
                    rewardsTable += "s";
                }
            });
            rewardsTable += "</table>";
            return rewardsTable;
        },

        // Create a donation page for each earned rewards program that allows donations
        donateRewardsEarned: function() {
            this.rewardsEarned.toArray().reverse().forEach(function(reward) {
                if (reward.get("donations").length > 0) {
                    var pointDonationView = new qcssapp.Views.PointDonationView({
                        programModel: reward,
                        parentView: this
                    });
                    var defaultInput = Math.min(reward.get("newPoints"), reward.get("points"));
                    pointDonationView.$el.find(".donation_enter-pts-input").val(defaultInput);
                    pointDonationView.slideIn();
                }
            }.bind(this));
        },

        // Update the points total in the rewards table
        updateProgramAfterDonation: function(donation) {
            this.rewardsEarned.each(function(reward) {
                if (donation.donation.get("loyaltyProgram").id === reward.get("id")) {
                    reward.set("points", Number(reward.get("points")) - Number(donation.points));
                    return false;
                }
            });
        },

        displayButtons: function() {

            //btn view to view receipt
            new qcssapp.Views.ButtonView({
                text: '<img class="order-placed_receipt-icon primary-font-background-color" src="images/icon-receipt.svg" >View Receipt',
                buttonType: "icon",
                appendToID: "#order-placed_view-receipt-btn-container",
                id: "order-placed_view-receipt-btn",
                class: "primary-font-background-color primary-border-background-color",
                callback: function () {
                    if ( qcssapp.Functions.checkServerVersion(1,6) ) {
                        qcssapp.Functions.viewReceipt(this.transactionID, this.chargeAtPurchaseTime, this.enableFavorites);

                    } else {
                        qcssapp.Functions.viewReceipt(this.orderNumber, true);
                    }

                    $('#show-order').find('.template-gen').remove();
                }.bind(this)
            });

            //btn view to continue
            new qcssapp.Views.ButtonView({
                text: "Home",
                buttonType: "customCB",
                appendToID: "#order-placed_home-btn-container",
                pageLinkID: "#main",
                id: "order-placed_home-btn",
                class: "primary-background-color font-color-primary",
                callback: function () {
                    qcssapp.Functions.showMask();

                    qcssapp.Functions.resetOrdering(false);

                    $('#show-order').find('.template-gen').remove();

                    //continue button takes back to home page
                    qcssapp.Functions.homeNavigationCallback();
                }
            });

            // Button to donate rewards
            if (this.rewardsDonateMsg) {
                var donateButton = this.$el.find(".order-placed_rewards-donate");
                donateButton.on("click", this.donateRewardsEarned.bind(this));
                donateButton.show();
            }

            this.updateCheckMark();
            this.updateReceiptIcon();
        },

        updateCheckMark: function() {
            var $img = this.$el.find('.order_placed-checkmark-icon');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            $.get(imgURL, function(data) {
                var $svg = $(data).find('svg');

                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');
        },

        updateReceiptIcon: function() {
            var $img = this.$el.find('.order-placed_receipt-icon');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');
            var color = qcssapp.usingBranding ? $('.primary-font-background-color').css('color') : '#ef4135';

            $.get(imgURL, function(data) {
                var $svg = $(data).find('svg');

                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                var $path = $(data).find('path');
                var $rect = $(data).find('rect');

                if(typeof $path.attr('fill') !== 'undefined') {
                    $path.attr('fill', color);
                }

                if(typeof $rect.attr('fill') !== 'undefined') {
                    $rect.attr('fill', color);
                }

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');
        }
    });

})(qcssapp, jQuery);