;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.comboLineView - View for displaying Combo Lines
     */
    qcssapp.Views.comboLineView = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'cart-line combo-title-line',

        // Cache the template function for a single item.
        template: _.template( $('#combo-cart-line-template').html() ),

        initialize: function() {
            return this;
        },

        render: function() {

            this.model.set('comboName', this.model.get('comboModel').get('name'));
            this.model.set('comboQuantity', this.model.get('comboModel').get('quantity'));

            var comboDisplayTotal = Number(this.model.get('comboModel').get('amount')).toFixed(2);

            this.model.set('comboTotalDisplay', Number(comboDisplayTotal * this.model.get('comboQuantity')).toFixed(2));
            this.model.set('comboTotal', Number(comboDisplayTotal * this.model.get('comboQuantity')).toFixed(2));

            //if quantity more than one then show equation
            if ( this.model.get('comboQuantity') > 1 ) {
                this.model.set('equation', this.model.get('comboQuantity') + " <span class='quantity'>x</span> " + qcssapp.Functions.formatPrice(comboDisplayTotal));
            } else {
                this.model.set('equation', "&nbsp;");
            }

            this.model.set('comboTotalDisplay', qcssapp.Functions.formatPrice(this.model.get('comboTotalDisplay')));

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            if(this.model.get('review')) {
                this.$el.addClass('review-line').removeClass('cart-line');
            }

            if(this.model.get('equation') == '&nbsp;') {
                this.$el.find('.cart-line-quantity').hide();
            }

            this.$el.attr('data-comboID', this.model.get('comboModel').get('comboID')).attr('data-cartComboID', this.model.get('comboModel').get('id'));

            return this;
        }
    });
})(qcssapp, jQuery);
