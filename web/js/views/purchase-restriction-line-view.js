;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.PurchaseRestrictionLineView - View for displaying Purchase Restriction Lines
     */
    qcssapp.Views.PurchaseRestrictionLineView = Backbone.View.extend({
        tagName:  'li',
        className: 'purchase-restriction-line',

        // Cache the template function for a single item.
        template: _.template( $('#purchase-restriction-line-template').html() ),

        render: function() {

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            return this;
        }
    });
})(qcssapp, jQuery);
