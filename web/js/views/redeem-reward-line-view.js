;(function (qcssapp, $) {
    'use strict';

    /* qcssapp.Views.RewardLine - View for displaying individual rewards on the redeem reward view
     */
    qcssapp.Views.RedeemRewardLine = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'tile tile-list redeem-reward-line',

        // Cache the template function for a single item.
        template: _.template( $('#redeem-reward-line-template').html() ),

        events: {
            'click': 'clickHandler'
        },

        initialize: function(options) {
            this.options = options || {};
            this.loyaltyProgramModel = this.options.loyaltyProgramModel;
            this.automatic = this.options.automatic;

            //parameters
            this.selected = false;

            //if a manual reward is already more expensive than the points on its program
            if ( !this.automatic && this.model.get('pointsToRedeem') > this.loyaltyProgramModel.get('points') ) {
                this.model.set('redeemable', false);
            }

            //event handlers
            this.listenTo(this.model, 'change:quantity', this.changeQuantityHandler);
            this.listenTo(this.loyaltyProgramModel, 'change:points', _.debounce(this.updatePoints));

            return this;
        },

        render: function() {
            //build template JSON data
            var templateSettings = this.model.toJSON();
            templateSettings.programName = this.loyaltyProgramModel.get('name');
            templateSettings.programPoints = this.loyaltyProgramModel.get('points');

            //set html with the given template and JSON data
            this.$el.html( this.template( templateSettings ) );

            //set quantity and points jquery objects for future updates
            this.$points = this.$el.find('.redeem-reward-line_balance_amount');
            this.$quantity = this.$el.find('.redeem-reward-line_quantity-container');
            this.$title = this.$el.find('.reward-container h2');

            //automatic rewards will have a quantity set already, update the UI to reflect the set quantity
            if ( this.automatic ) {
                //update the quantity display
                this.changeQuantityHandler();
            }

            return this;
        },

        changeQuantityHandler: function() {
            //determine selected state based on the quantity on the Reward model
            var newQuantity = this.model.get('quantity');
            this.selected = newQuantity > 0;

            //update the quantity display
            this.$quantity.html( newQuantity + ' selected' );

            //toggle the display of the selected state styling
            this.$quantity.toggleClass('accent-color-two', this.selected);
            this.$title.toggleClass('accent-color-two', this.selected);

            //update the border color - need to consider the accent-color-two
            var borderColor = "#fff";
            if ( this.selected ) {
                borderColor = $('.accent-color-two:first').css('color');
            }

            this.$el.css('border-color', borderColor);
            this.$el.toggleClass('selected', this.selected);

            //if modal is open, update that quantity as well
            var $modalQuantity = $('.redeem-modal_form_input');
            if ( $modalQuantity.length && $modalQuantity.is(':visible') && $('.redeem-modal').attr('data-rewardid') == this.model.get('id') ) {
                $modalQuantity.val(newQuantity);
            }

            //update the program's points to reflect the change
            var oldQuantity = this.model._previousAttributes.quantity || 0;
            var diff = newQuantity - oldQuantity;

            //only attempt to update the points if the change is non-zero
            if ( diff != 0 ) {
                this.loyaltyProgramModel.set('points', this.loyaltyProgramModel.get('points') - diff*this.model.get('pointsToRedeem'));
            }
        },

        clickHandler: function() {
            //prevent de-selecting an automatic reward
            if ( this.automatic ) {
                $.alert('You cannot remove or change an automatically redeemed reward!');
                return;
            }

            var viewModel = this.model;

            //if this Reward is not selected
            if ( !this.selected ) {
                //user cannot afford the reward
                if ( !viewModel.get('redeemable') ) {
                    $.alert('You do not have enough points in the Rewards Program <b>'+ this.loyaltyProgramModel.get('name') +'</b> to redeem this reward.');
                    return;
                }
                //if user can afford the reward

                //set the initial quantity to 1
                viewModel.set('quantity', 1);
                return;
            }
            //if this Reward is selected

            //if the Reward is a trans credit, or limited to 1 per transaction, or the user is maxed at buying 1 of these Rewards, or it is a free prod reward that can't be used with other rewards, de-select this Reward
            if ( viewModel.get('rewardTypeID') != 4 || viewModel.get('maxPerTransaction') == 1 || ( viewModel.get('quantity') == 1 && !viewModel.get('redeemable') ) || ( viewModel.get('rewardTypeID') == 4 && !viewModel.get('canUseWithOtherRewards') ) ) {
                //if only 1 maxPerTransaction, just de-select this Reward
                viewModel.set('quantity', 0);
                return;
            }

            //this is where the modal comes in
            var modalContent = '<div class="redeem-modal" data-rewardid="'+ viewModel.get('id') +'">';
            modalContent += '<div class="redeem-modal_title">'+viewModel.get('name')+'</div>';
            modalContent += '<div class="redeem-modal_points accent-color-two">'+viewModel.get('pointsToRedeem')+'<span class="redeem-modal_points_label">pts</span></div>';
            modalContent += '<div class="redeem-modal_program-name"><strong>Rewards Program: </strong>'+this.loyaltyProgramModel.get('name')+'</div>';
            modalContent += '<div class="redeem-modal_program-points"><strong>Points Balance: </strong><span class="redeem-modal_program-points_value">'+this.loyaltyProgramModel.get('points')+'</span></div>';
            modalContent += '<div class="redeem-modal_form">';
                modalContent += '<div class="redeem-modal_form_minus">-</div>';
                modalContent += '<input class="redeem-modal_form_input" type="number" />';
                modalContent += '<div class="redeem-modal_form_plus">+</div>';
            modalContent += '</div>';
            modalContent += '</div>';

            var programModel = this.loyaltyProgramModel;
            var vmQuantity = viewModel.get('quantity');
            $.confirm({
                title: '',
                closeIcon: function(){
                    viewModel.set('quantity', vmQuantity);
                },
                content: modalContent,
                buttons: {
                    update: {
                        text:'UPDATE QUANTITY',
                        btnClass: 'qcss-button align-center order-button redeem-rewards-modal-update prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {
                        }
                    },
                    remove: {
                        text:'REMOVE REWARD',
                        btnClass: 'qcss-button align-center order-button redeem-rewards-modal-remove primary-button-font primary-border-color',
                        action: function() {
                            viewModel.set('quantity', 0);
                        }
                    }
                },
                onContentReady: function() {
                    // don't let them click buttons too fast, ensure the jconfirm is hidden by mask
                    $('.jconfirm').css('z-Index', '19999');
                    var $modalQuantity = $('.redeem-modal_form_input');
                    $modalQuantity.val(vmQuantity);

                    $modalQuantity.on('change', function() {
                        var $this = $(this);

                        //prevent change if the number is negative
                        if ( $this.val() < 0 ) {
                            $.alert('The reward quantity may not be a negative number');
                            return;
                        }

                        //if the value entered is greater than the allowable per transaction
                        if ( $this.val() > viewModel.get('maxPerTransaction') ) {
                            $.alert('You cannot redeem '+ $this.val() +' of this reward, the maximum per transaction is '+ viewModel.get('maxPerTransaction') +'. Please choose a lower quantity.');
                            $this.val(viewModel.get('quantity'));
                            return;
                        }

                        //if the change results in a configuration that would cost more points than the user has, prevent it
                        if ( ($this.val() - viewModel.get('quantity')) * viewModel.get('pointsToRedeem') > programModel.get('points') ) {
                            $.alert('You cannot afford '+ $this.val() +' of this reward. Please choose a lower quantity.');
                            $this.val(viewModel.get('quantity'));
                            return;
                        }

                        viewModel.set('quantity', Number($this.val()));
                    });

                    var $minus = $('.redeem-modal_form_minus');
                    $minus.on('click', function() {
                        viewModel.decrementQuantity();
                    });

                    var $plus = $('.redeem-modal_form_plus');
                    $plus.on('click', function() {
                        viewModel.incrementQuantity();
                    });
                }
            });
        },

        updatePoints: function() {
            //update points display
            var previousPoints = this.loyaltyProgramModel._previousAttributes.points;
            var currentPoints = this.loyaltyProgramModel.get('points');

            //parameters for animating the points updating
            var diff = 0;
            var changeFunction;
            var pointsInterval;

            //stop the updating interval
            function stopInterval() {
                clearInterval(pointsInterval)
            }

            //adding reward, decreasing the points
            if ( previousPoints > currentPoints ) {
                diff = previousPoints - currentPoints;

                changeFunction = function($points, diff) {
                    var diffInterval = 1;

                    if ( diff < 60 ) {
                        diffInterval = 1;
                    } else if ( diff > 60 && diff < 120 ) {
                        diffInterval = 2;
                    } else if ( diff > 120 ) {
                        diffInterval = Math.ceil(diff/100);
                    }

                    previousPoints = previousPoints - diffInterval;
                    $points.html(previousPoints);

                    if ( previousPoints == currentPoints ) {
                        stopInterval();
                    }

                    if ( previousPoints - diffInterval < currentPoints ) {
                        previousPoints = currentPoints;
                        $points.html(previousPoints);
                        stopInterval();
                    }
                }
            } else {
            //removing reward, increasing the points
                diff = currentPoints - previousPoints;

                changeFunction = function($points, diff) {
                    var diffInterval = 1;

                    if ( diff < 60 ) {
                        diffInterval = 1;
                    } else if ( diff > 60 && diff < 120 ) {
                        diffInterval = 2;
                    } else if ( diff > 120 ) {
                        diffInterval = Math.ceil(diff/100);
                    }

                    previousPoints = previousPoints + diffInterval;
                    $points.html(previousPoints);

                    if ( previousPoints == currentPoints ) {
                        stopInterval();
                    }

                    if ( previousPoints + diffInterval > currentPoints ) {
                        previousPoints = currentPoints;
                        $points.html(previousPoints);
                        stopInterval();
                    }
                }
            }

            //determine DOM update target elements
            var $target = this.$points;
            var $modalPoints = $('.redeem-modal_program-points_value');
            if ( $modalPoints.length && $modalPoints.is(':visible') ) {
                $target = $target.add($modalPoints);
            }

            //trigger points display change
            if ( diff > 4 ) {
                pointsInterval = setInterval(changeFunction, 300/diff, $target, diff);
            } else {
                $target.html(currentPoints);
            }

            //determine if the reward is maxed for the points the user has in this program
            var maxed = this.model.get('pointsToRedeem') > this.loyaltyProgramModel.get('points');
            this.model.set('redeemable', !maxed);

            //if its maxed and not selected, show the maxed styling, otherwise remove the maxed styling
            this.$el.toggleClass('maxed', !this.selected && maxed);
        }
    });
})(qcssapp, jQuery);
