;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.NetworkOfflineView - Modal View for displaying qr code if the app is offline
     */
    qcssapp.Views.NetworkOfflineView = Backbone.View.extend({
        internalName: "Network Offline Modal View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'network-offline-container',

        template: _.template( $('#network-offline-template').html() ),

        //handle "enter" events on form fields
        events: {
            "click .offline_qr-button": "handleQRCode",
            "click .offline_connect-button": "handleConnect",
            "click #offline_qr-close-button": "handleClose"
        },

        initialize: function (options) {
            qcssapp.Functions.hideMask();
            this.options = options || {};

            this.$errorContainer = $('#offline_error-container');
            this.$errorContainer.hide();

            this.hidden = this.options.hidden;
            this.badge = null;
            this.fromLogin = false;
            this.offlineEvent = true;

            _.bindAll( this, 'handleQRCode', 'handleClose', 'handleConnect', 'displayError');

            this.render();
        },

        loadView: function(params) {
            this.badge = params.badge;
            this.fromLogin = params.fromLogin;

            var badge = this.badge.toString();
            var qrCode = kjua({text:badge});

            this.$qrCode.html(qrCode);

            this.fadeIn();

            if ( navigator && navigator.splashscreen ) {
                navigator.splashscreen.hide();
            }
        },

        render: function() {

            var templateData = {};

            this.$el.html( this.template( templateData ) );

            this.$qrCode = this.$el.find('#offline_qr-code');
            this.$qrCodeClose = this.$el.find('#offline_qr-close-button');
            this.$offlineMsgContainer = this.$el.find('.offline_msg-container');
            this.$errorContainer = this.$el.find('#offline_error-container');

            if ( this.hidden ) {
                this.$el.addClass('hidden');
            }

            $('body').append( this.$el );

            this.$qrCodeClose.fadeOut(50);
            this.$qrCode.fadeOut(150);

            this.checkLoadedImages();

            return this;
        },

        handleQRCode: function() {
            this.$errorContainer.hide();

            this.$offlineMsgContainer.fadeOut(150, function() {
                qcssapp.Functions.setBrightness( 1 );
                this.$qrCode.fadeIn(150);
                this.$qrCodeClose.fadeIn(150);
            }.bind(this));
        },

        handleClose: function() {
            this.$errorContainer.hide();

            this.$qrCode.fadeOut(150);
            this.$qrCodeClose.fadeOut(150, function() {
                qcssapp.Functions.resetBrightness();
                this.$offlineMsgContainer.fadeIn(150);
            }.bind(this));
        },

        handleConnect: function() {
            var $activeView = $('.view.active');
            this.$errorContainer.hide();

            if ( !(navigator.connection && navigator.connection.type && navigator.connection.type === Connection.NONE) ) {
                    qcssapp.network.onlineStatus = true;
            }

            if(qcssapp.network.onlineStatus) {

                //if from the login page then just load login view
                if(this.fromLogin || !$activeView.length) {
                    qcssapp.Router.navigate('login-page', {trigger: true});
                    location.reload(true);
                    //this.fadeOut();

                    //if error connecting to server try to initialize connect again
                } else if (!this.offlineEvent) {
                    $('#item-list').empty();
                    qcssapp.Functions.initializeApplication();

                    //if app was offline and not from login view, just close modal
                } else {
                    this.fadeOut();
                }

                //if on the main view but the panel doesn't have the active class, show it
                if( $activeView.attr('id') == 'mainview' && !$activeView.find('.panel').hasClass('active') ) {
                    $activeView.find('.panel').addClass('active');
                }

            } else {
                this.displayError('Connection failed');
            }
        },

        checkLoadedImages: function() {

            var image = new Image();
            image.onerror = function() {
                this.$el.find('.offline-icon').hide();
                this.$el.find('.offline-qr-icon').hide();
                this.$el.find('.offline_qr-button').text('Badge').addClass('badge-text');
            }.bind(this);

            image.src = "images/icon-connection-error.png";
        },

        fadeIn: function(dur) {
            this.hidden = false;
            this.$el.fadeIn(dur, function() {
                this.$el.removeClass('hidden');
            }.bind(this));
        },

        fadeOut: function() {
            this.hidden = true;
            this.$el.fadeOut(150, function() {
                this.$el.addClass('hidden');
            }.bind(this));
        },

        displayError: function( errorMsg ) {
            this.$errorContainer.html( errorMsg );
            this.$errorContainer.slideDown();
        }
    });
})(qcssapp, jQuery);
