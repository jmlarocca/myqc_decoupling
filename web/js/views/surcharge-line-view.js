;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.SurchargeLineView - View for displaying Surcharge Lines
     */
    qcssapp.Views.SurchargeLineView = Backbone.View.extend({
        //is a list tag.
        tagName:  'div',

        className: 'review-surcharge-detail',

        // Cache the template function for a single item.
        template: _.template( $('#review-surcharge-line-template').html() ),

        render: function() {

            this.model.set('name', qcssapp.Functions.htmlDecode(this.model.get('surcharge').name));
            this.model.set('amount', this.roundAmount(this.model.get('extendedAmount')));

            if(Number(this.model.get('amount')) < 0) {
                var amount = Number(this.model.get('amount')) * -1;
                this.model.set('amount', qcssapp.Functions.formatPriceInApp(amount, '-'));
            } else {
                this.model.set('amount', qcssapp.Functions.formatPriceInApp(this.model.get('amount')));
            }

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            return this;
        },
        roundAmount: function(num){
            return Number(Math.round(num+"e"+2)+"e-"+2).toFixed(2);
        }
    });
})(qcssapp, jQuery);
