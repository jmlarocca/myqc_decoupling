;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ReceiveView - View for displaying Stores for ordering collection
     */
    qcssapp.Views.ReceiveView = Backbone.View.extend({
        internalName: "Receive View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#show-receive', //references existing HTML element for Store Collections - can be referenced as this.$el throughout the view

        //template: //template not needed for this view

        events: {
            'click #receiveDelivery': function(e) {this.method(e, 'delivery');},
            'click #receivePickup': function(e) {this.method(e, 'pickup');},
            'click #orderTime': 'addTopASAPButton',
            'change #phoneSelect': 'changePhoneSelect',
            'change #paymentMethodSelect': 'changePaymentMethod',
            'click #addPaymentMethodButton': 'clickAddPaymentMethod',
            'change #orderLocation': 'changeOrderLocation',
            'click #addVoucherButton': 'clickAddVoucher',
            'click #validateVoucherButton': 'clickValidateVoucher',
            'keyup #voucherCode': 'inputVoucherKeyup',
            'click #removeVoucher': 'clickRemoveVoucher'
        },

        initialize: function (options) {

            this.usesSMSNotifications = false;
            this.usesEatInTakeOut = false;

            this.accountPaymentMethodModel = null;
            this.hasPaymentMethod = false;
            this.paymentProcessor = null;
            this.paymentView = null;

            this.showPhoneNumberInput = true;
            this.showCommentInput = true;
            this.showDeliveryInput = true;
            this.showOrderTypeAndTimeMsg = true;

            this.hasQuickChargeBalance = qcssapp.Functions.getStorePaymentSetting("hasQuickChargeBalance", false);
            this.allowMyQCFunding = qcssapp.Functions.getStorePaymentSetting("allowMyQCFunding", true);
            this.isCreditCardAllowed = qcssapp.Functions.isCreditCardAllowed();
            this.chargeAtPurchaseTime = qcssapp.Functions.getStoreSetting("chargeAtPurchaseTime", true);
            this.allowsVouchers = qcssapp.Functions.getStoreSetting("allowsVouchers", false);
            this.paymentMethodCollection = qcssapp.Functions.getStorePaymentSetting("paymentMethodCollection", []);

            this.$paymentMethodContainer = this.$el.find('#paymentMethodSelectContainer');
            this.$addPaymentMethodBtnContainer = this.$el.find('#addPaymentMethodBtnContainer');
            this.$addPaymentMethodBtn = this.$el.find('#addPaymentMethodButton');
            this.$paymentMethodSelect = this.$el.find('#paymentMethodSelect');
            this.$addVoucherButton = this.$el.find('#addVoucherButton');
            this.$voucherCodeContainer = this.$el.find('#voucherCodeContainer');
            this.$enterVoucherContainer = this.$el.find('#enterVoucherContainer');
            this.$voucherCodeInput = this.$el.find('#voucherCode');
            this.$appliedVoucher = this.$el.find('#appliedVoucher');

            var $receiveMethodContainer = $('#receiveMethodContainer');
            $receiveMethodContainer.hide();
            qcssapp.Functions.resetErrorMsgs();
            this.deliveryLocationErrorMsg = "You must fill out your location for a delivery order!";

            //resets
            $('.receiveBtn').removeClass('selected secondary-background-color font-color-secondary');

            var $location = $('#orderLocation');
            var $orderPhone = $('#orderPhone');
            var $comments = $('#orderComments');

            $location.val('').trigger('change');
            $orderPhone.val('').trigger('change');
            $comments.val('').trigger('change');

            if(qcssapp.orderObject.location != '') {
                $location.val(qcssapp.orderObject.location);
            }

            if(qcssapp.orderObject.phone != '') {
                $orderPhone.val(qcssapp.orderObject.phone);
            }

            if(qcssapp.orderObject.comments != '') {
                $comments.val(qcssapp.orderObject.comments);
            }

            var $orderTimeContainer = $('#orderTimeContainer');
            $orderTimeContainer.show();
            if(qcssapp.CurrentOrder.grabAndGo) {
                $orderTimeContainer.hide();
            }

            this.$orderTime = $('#orderTime');
            qcssapp.asapTime = '';

            this.currentStoreTime = moment();

            this.$el.removeClass('showTimeDiffError');

            if ( qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('clientTimezoneOffset') ) {

                var clientTimezoneOffset = qcssapp.CurrentOrder.storeModel.attributes.clientTimezoneOffset;

                if ( Number(clientTimezoneOffset) > 600 || (Number(clientTimezoneOffset)*-1) > 600) {
                    this.$el.addClass('showTimeDiffError');
                }

                this.currentStoreTime = this.currentStoreTime.add(clientTimezoneOffset,'s');
            }

            if(options.timePickerModel != "") {
                this.timePickerModel = options.timePickerModel;
                this.$orderTime.data('timePickerModel', this.timePickerModel);
                this.canUseBuffering = true;
            } else {
                this.canUseBuffering = false;
            }

            var pickupAvail = qcssapp.Functions.checkStoreOpenForPickUp(qcssapp.CurrentOrder.storeModel, this.currentStoreTime);
            var deliveryAvail = qcssapp.Functions.checkStoreOpenForDelivery(qcssapp.CurrentOrder.storeModel, this.currentStoreTime);

            //set receive method based on whats available
            if ( pickupAvail && !deliveryAvail ) {
                this.setMethodPickup(false);
            } else if ( !pickupAvail && deliveryAvail ) {
                this.setMethodDelivery(false);
            } else {
                //if both methods
                $receiveMethodContainer.show();
                if ( qcssapp.CurrentOrder.orderType == "delivery") {
                    this.setMethodDelivery(true);
                } else if (this.canUseBuffering && this.timePickerModel.orderType == "delivery") {
                    this.setMethodDelivery(true);
                } else {
                    this.setMethodPickup(true);
                }

            }

            //get the prep time to add to the ASAP time
            if(this.canUseBuffering) {
                var prepTime = Number(qcssapp.CurrentOrder.storeModel.attributes.pickUpPrepTime);
                if ( qcssapp.CurrentOrder.orderType == "delivery" ) {
                    prepTime += Number(qcssapp.CurrentOrder.storeModel.attributes.deliveryPrepTime);
                    $('#receiveDelivery').addClass('secondary-background-color font-color-secondary');
                }
                else {
                    $('#receivePickup').addClass('secondary-background-color font-color-secondary');
                }
                //get the ASAP order time in between the open and close times
                var timeASAP = qcssapp.Functions.setASAPTime(this.timePickerModel, prepTime, this.$el);
                timeASAP = qcssapp.Functions.formatASAP(timeASAP);
                this.buildTimePicker(this.timePickerModel, timeASAP);
                this.$orderTime.val("ASAP ("+timeASAP+")").trigger("change");
            } else {
                this.setupTimePicker(this.currentStoreTime);
                this.$orderTime.val('ASAP').trigger('change');
            }


            //prepopulate phone number
            if ( typeof qcssapp.accountModel.get('phone') !== 'undefined' && qcssapp.accountModel.get('phone').length > 0 ) {
                var phoneNumber = qcssapp.Functions.htmlDecode(qcssapp.accountModel.get('phone'));
                qcssapp.accountModel.set('phone', phoneNumber);
                $orderPhone.val(phoneNumber).trigger('change');
            }

            this.setPhoneNumber();

            this.setDiningOptions();

            this.setPaymentMethodOptions();

            this.setOrderDetailFields();

            //prevent buttons from being displayed more than once
            $("#receiveBtnContainer").empty();
            $("#cancelBtnContainer").empty();

            this.displayButtons();
            $("#cancelBtnContainer").hide();

            // If not using dining options, and set to skip order details page, click the Next button after it has been initialized
            if ( !(qcssapp.CurrentOrder.orderType == "pickup" && this.eatInTakeOutActive) && qcssapp.Functions.checkServerVersion(4,1) && qcssapp.Functions.skipOrderDetails()) {
                $('#receiveNext').trigger('click');
            }
        },

        method: function(e, method) {
            var $target = $(e.currentTarget);

            if ( $target.hasClass('selected') ) {
                return false;
            }

            $target.addClass('secondary-background-color font-color-secondary');
            $target.siblings().removeClass('secondary-background-color font-color-secondary');
            $target.siblings().removeAttr('style');

            if ( method == 'delivery' ) {
                this.setMethodDelivery(true);
                //make another call to time picker
            } else if ( method == 'pickup' ) {
                this.setMethodPickup(true);
            } else {
                return false;
            }

            if(this.canUseBuffering) {

                var prepTime = Number(qcssapp.CurrentOrder.storeModel.attributes.pickUpPrepTime);
                if ( qcssapp.CurrentOrder.orderType == "delivery" ) {
                    prepTime += Number(qcssapp.CurrentOrder.storeModel.attributes.deliveryPrepTime);
                }

                var successParameters = {
                    receiveView : this,
                    targetParam : $target,
                    prepTime: prepTime,
                    orderType: qcssapp.CurrentOrder.orderType,
                    function: qcssapp.Functions,
                    timePicker: qcssapp.TimePicker
                }

                var successFunction = function(data, successParameters) {
                    //pass the ASAP order time as a success parameter when the order type is changed
                    qcssapp.TimePickerModel = data;
                    var timeASAP = qcssapp.Functions.setASAPTime(data, successParameters.prepTime, this.$el);
                    timeASAP = qcssapp.Functions.formatASAP(timeASAP);
                    successParameters.receiveView.buildTimePicker(data, timeASAP);
                    successParameters.receiveView.$orderTime.val('ASAP ('+timeASAP+')').trigger('change');
                    if (data.openTimes.length == 0) {
                        successParameters.receiveView.$orderTime.val("").trigger("change");
                        successParameters.function.displayError("We're sorry this store is not currently accepting " + successParameters.orderType + " orders");
                        $("#orderTime").prop('disabled', true);
                    } else {
                        $("#orderTime").prop('disabled', false);
                    }
                    $('.receiveBtn').removeClass('selected');
                    successParameters.targetParam.addClass('selected');
                }.bind(this);

                var storeID = qcssapp.CurrentOrder.storeModel.id;
                if(storeID) { //recreates the timePicker when the order type is switched
                    qcssapp.Functions.createTimePicker(storeID, method, successFunction, successParameters);
                }
            } else {
                this.setupTimePicker(this.currentStoreTime);

                this.$orderTime.val('ASAP').trigger('change');

                $('.receiveBtn').removeClass('selected');
                $target.addClass('selected');
            }
        },

        displayButtons: function() {
            var receiveView = this;

            new qcssapp.Views.ButtonView({
                text: "Next",
                buttonType: "customCB",
                appendToID: "#receiveBtnContainer",
                id: "receiveNext",
                class: "button-flat accent-color-one",
                callback: function () {
                    qcssapp.Functions.showMask();
                    qcssapp.Functions.resetErrorMsgs();

                    var $location = $('#orderLocation');
                    var $orderTime = $('#orderTime');
                    var $orderPhone = $('#orderPhone');
                    var $phoneSelect = $('#phoneSelect');
                    var phoneNumReq = $orderPhone.attr('data-required') != "false";
                    var showDelivery = typeof $location.attr('data-show') === 'undefined' ? true : $location.attr('data-show') != 'false';

                    if ( $location.val().trim() == '' && qcssapp.CurrentOrder.orderType == 'delivery' && showDelivery) {
                        qcssapp.Functions.displayError(this.deliveryLocationErrorMsg);
                        qcssapp.Functions.hideMask();
                        $location.trigger('focus');
                        return false;
                    }

                    if ( $orderTime.val() == '' && !this.forceASAP ) {
                        qcssapp.Functions.displayError('You must select a valid time for your order!');
                        qcssapp.Functions.hideMask();
                        return false;
                    }

                    if ( phoneNumReq && $orderPhone.val().replace(/\D/g,'') == '' && this.showPhoneNumberInput) {
                        qcssapp.Functions.displayError('You must enter a valid phone number!');
                        qcssapp.Functions.hideMask();
                        $orderPhone.trigger('focus');
                        return false;
                    }

                    if ( phoneNumReq && $orderPhone.val() == '' && this.showPhoneNumberInput) {
                        qcssapp.Functions.displayError('You must enter a valid phone number!');
                        qcssapp.Functions.hideMask();
                        $orderPhone.trigger('focus');
                        return false;
                    }

                    if ( $orderPhone.val() != '' && !$orderPhone.val().match(/^[0-9-]+$/) && this.showPhoneNumberInput) {
                        qcssapp.Functions.displayError('The phone number can only consist of numbers and dashes!');
                        qcssapp.Functions.hideMask();
                        $orderPhone.trigger('focus');
                        return false;
                    }

                    if ( qcssapp.Functions.checkServerVersion(2,0) && $orderPhone.val() != '' && $orderPhone.val().replace(/\D/g,'').length != 10 && this.showPhoneNumberInput) {
                        qcssapp.Functions.displayError('You must enter a phone number 10 digits long!');
                        qcssapp.Functions.hideMask();
                        $orderPhone.trigger('focus');
                        return false;
                    }

                    if ( !qcssapp.Functions.checkServerVersion(2,0) && $orderPhone.val() != '' && $orderPhone.val().replace(/\D/g,'').length < 3 && this.showPhoneNumberInput) {
                        qcssapp.Functions.displayError('You must enter a valid phone number!');
                        qcssapp.Functions.hideMask();
                        $orderPhone.trigger('focus');
                        return false;
                    }

                    var endPoint = qcssapp.Location + '/api/ordering/order/review';

                    //set current order parameters
                    qcssapp.CurrentOrder.orderLocation = $location.val();
                    qcssapp.CurrentOrder.orderTime = $orderTime.val();
                    qcssapp.CurrentOrder.orderComments = $('#orderComments').val();

                    if(!qcssapp.Functions.checkServerVersion(2,0) || $phoneSelect.val() == "1") {  //if earlier server version or using landline, set phone property in accountModel
                        qcssapp.accountModel.set('phone', $orderPhone.val());
                    }

                    var phone = qcssapp.accountModel.get('phone');
                    if(qcssapp.Functions.checkServerVersion(2,0) && $phoneSelect.val() == "2") { //if using mobile phone set phone in order object to null and set mobile number
                        qcssapp.accountModel.set('mobilePhone', $orderPhone.val());
                        phone = '';
                    }

                    //ignore inventory counts flag added in 1.2.2, client could have 1.1 back end, need to be careful, default is false
                    var ignoreInvCounts = false;
                    var includeIgnoreInvCounts = false;
                    if ( qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('ignoreInventoryCounts') ) {
                        ignoreInvCounts = qcssapp.CurrentOrder.storeModel.attributes.ignoreInventoryCounts;
                        includeIgnoreInvCounts = true;
                    }

                    var orderObject = {
                        "type": qcssapp.CurrentOrder.orderType,
                        "location": qcssapp.CurrentOrder.orderLocation,
                        "time": qcssapp.CurrentOrder.orderTime,
                        "phone": phone,
                        "comments": qcssapp.CurrentOrder.orderComments,
                        "deliveryMinimum": qcssapp.CurrentOrder.storeModel.attributes.deliveryMinimum,
                        "personName": qcssapp.accountModel.get('name'),
                        "submittedTime": moment().format("YYYY-MM-DD HH:mm:ss.SSS"),
                        "reAuthTypeID": qcssapp.CurrentOrder.storeModel.attributes.reAuthTypeID,
                        "products": qcssapp.Functions.buildProductArray()
                    };

                    if($orderTime.val().indexOf('ASAP') != -1) {
                        var prepTime = qcssapp.CurrentOrder.orderType == "delivery" ? Number(qcssapp.CurrentOrder.storeModel.get('pickUpPrepTime')) + Number(qcssapp.CurrentOrder.storeModel.get('deliveryPrepTime')) : Number(qcssapp.CurrentOrder.storeModel.get('pickUpPrepTime'));
                        var timeASAP = qcssapp.Functions.setASAPTime(qcssapp.TimePickerModel, prepTime, this.$el );

                        timeASAP = 'ASAP (' + qcssapp.Functions.formatASAP(timeASAP) + ')';
                        orderObject.time = timeASAP;
                        $orderTime.val(timeASAP);
                    }

                    //for rewards - only available in 1.5 and up
                    if ( qcssapp.Functions.checkServerVersion(1,5) ) {
                        //set up rewards for OrderModel
                        orderObject.rewards = qcssapp.Functions.buildRewardsArray([], true);
                    }

                    //for SMS Changes - only available in 2.0 and up
                    if ( qcssapp.Functions.checkServerVersion(2,0) ) {
                        //set up mobile phone number for OrderModel
                        orderObject.mobilePhone = qcssapp.accountModel.get('mobilePhone');

                        var isMobilePhone = false;
                        if($phoneSelect.val() == "2") {
                            isMobilePhone = true;
                        }
                        orderObject.usingMobile = isMobilePhone;

                        var diningOptionLabel = "";
                        var diningOptionValue = "";
                        if($('#diningOptionContainer').css('display') == 'block') {
                            var $diningSelect = $('#diningOptionsSelect');
                            diningOptionLabel = $diningSelect.find('option:selected').text();
                            diningOptionValue = $diningSelect.val();

                            if(diningOptionValue == null && $diningSelect.find('option').length == 0) {
                                qcssapp.Functions.displayError('The Dining Options are not configured correctly for this store. Please contact the manager if the error continues.');
                                return false;
                            }
                        }

                        orderObject.diningOptionPAPluName = diningOptionLabel;
                        orderObject.diningOptionPAPluID = diningOptionValue;
                    }

                    if ( qcssapp.Functions.checkServerVersion(2,1) ) {
                        orderObject.useMealPlanDiscount = qcssapp.CurrentOrder.useMealPlanDiscount;
                    }

                    if (qcssapp.CurrentOrder.voucherCode) {
                        orderObject.voucherCode = qcssapp.CurrentOrder.voucherCode;
                    }

                    var paymentMethodName = 'Quickcharge';
                    var showPaymentMethod = false;
                    if ( qcssapp.Functions.checkServerVersion(3,0) ) {
                        var $paymentMethod = $('#paymentMethodSelect');
                        if($('#paymentMethodSelectContainer').css('display') == 'none' && $paymentMethod.val() == "2") {
                            $paymentMethod.val(1);
                        }

                        orderObject.useCreditCardAsTender = false;

                        if($paymentMethod.val() == "2") { // Credit Card as a tender
                            orderObject.useCreditCardAsTender = true;
                            // D-4216: get the ending index dynamically in case of longer card types (e.g., VISA vs MASTERCARD)
                            var indexEnd = $paymentMethod.find('option[value="2"]').text().indexOf(',')
                            paymentMethodName = $paymentMethod.find('option[value="2"]').text().substring(14, indexEnd);
                        }

                        if($paymentMethod.find('option').length > 1 || orderObject.useCreditCardAsTender || orderObject.voucherCode) {
                            showPaymentMethod = true;
                        }

                        if($paymentMethod.val() == "3" || $paymentMethod.val() == "4") {
                            qcssapp.Functions.displayError('Please add a valid Payment Method');
                            return false;
                        }
                    }

                    if( qcssapp.Functions.checkServerVersion(4,0) ) {
                        if(qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('chargeAtPurchaseTime')) {
                            orderObject.chargeAtPurchaseTime = qcssapp.CurrentOrder.storeModel.get('chargeAtPurchaseTime');
                        }

                        if(qcssapp.personModel != null) {
                            orderObject.personID = qcssapp.personModel.get('personID');
                        }

                        if(qcssapp.CurrentOrder.combos.length > 0) {
                            orderObject.combos = qcssapp.Functions.buildCombosArray();
                        }
                    }

                    if(qcssapp.CurrentOrder.grabAndGo) {
                        orderObject.grabAndGo = qcssapp.CurrentOrder.grabAndGo;
                    }

                    if(qcssapp.futureOrderDate) {
                        orderObject.futureOrderDate = qcssapp.futureOrderDate;
                    }

                    if ( includeIgnoreInvCounts ) {
                        orderObject.ignoreInventoryCounts = ignoreInvCounts;
                    }

                    var successParameters = {
                        'canUseBuffering': this.canUseBuffering,
                        'isMobilePhone': isMobilePhone,
                        'diningOptionPAPluName': diningOptionLabel,
                        'diningOptionPAPluID': diningOptionValue,
                        'showPaymentMethod': showPaymentMethod,
                        'paymentMethod': paymentMethodName
                    };

                    qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(orderObject), successParameters,
                        function(data, successParameters) {
                            if ( data.errorDetails && data.errorDetails.length > 0 && !data.advanceScreen) {
                                qcssapp.Functions.displayError(data.errorDetails, 'Warning');
                                return;
                            }

                            // If any products have a purchase restriction, do not advance
                            if (qcssapp.Functions.checkServerVersion(4, 0)) {
                                // Copy the tax totals from data to CurrentOrder (for purchase restriction calculations)
                                var indexOffset = data.products.length - qcssapp.CurrentOrder.products.length;
                                for (var i = 0; i < qcssapp.CurrentOrder.products.length; i++) {
                                    qcssapp.CurrentOrder.products[i].attributes.taxTotal = data.products[i + indexOffset].taxTotal;
                                }
                                if (data.voucherCode && qcssapp.Functions.isNumeric(data.voucherTotal)) {
                                    qcssapp.CurrentOrder.voucherTotal = Number(data.voucherTotal);
                                } else {
                                    qcssapp.CurrentOrder.voucherTotal = 0;
                                }
                                if (!data.useCreditCardAsTender && !qcssapp.Functions.checkPurchaseRestrictionOnNext()) {
                                    return;
                                }
                            }

                            // If on a older server version then it's still looking at the 'Allow Funding on Purchase' flag, if the error is that they don't have enough fund, the account group allows funding, the store allows funding on purchase and they don't have a payment method - show manual funding modal but tell them to add a payment method
                            if( !qcssapp.Functions.checkServerVersion(4,1,6) && data.errorDetails && data.errorDetails.length > 0 && data.errorDetails.indexOf('You do not have enough available funds') != -1 && qcssapp.Functions.getStoreSetting('allowFundingOnPurchase', false) && this.allowMyQCFunding && $paymentMethod.val() == "1" && !this.hasPaymentMethod) {
                                data.userBalance = qcssapp.Functions.getStorePaymentSetting('userBalance', 0);
                            }

                            // If a userBalance is returned and the account group allows funding - show manual funding modal
                            if( ((data.userBalance != null && data.userBalance != "") || data.userBalance == 0) && this.allowMyQCFunding ) {

                                var options = {
                                    userBalance: data.userBalance,
                                    accountFundingDetailModel: data.accountFundingDetailModel,
                                    customFundingAmount: data.customFundingAmount,
                                    total: data.total,
                                    container: '#receiveview',
                                    hidden: true
                                };

                                this.manualFundingView = new qcssapp.Views.ManualFundingView(options);
                                this.manualFundingView.fadeIn();
                                return;
                            }

                            //get back final cart object w/ taxes, fees, discounts, etc
                            if ( data.products.length > 0 ) {
                                //send to final review page
                                data.phone = qcssapp.Functions.htmlDecode(data.phone);
                                data.time = qcssapp.Functions.htmlDecode(data.time);
                                data.readyTime = qcssapp.Functions.htmlDecode(data.readyTime);
                                data.submittedTime = qcssapp.Functions.htmlDecode(data.submittedTime);
                                data.comments = qcssapp.Functions.htmlDecode(data.comments);
                                data.isMobilePhone = successParameters.isMobilePhone;
                                data.diningOptionPAPluID = successParameters.diningOptionPAPluID;
                                data.diningOptionPAPluName = successParameters.diningOptionPAPluName;
                                data.paymentMethod = successParameters.paymentMethod;
                                data.showPaymentMethod = successParameters.showPaymentMethod;
                                data.futureOrderDate = qcssapp.futureOrderDate;
                                data.grabAndGo = qcssapp.CurrentOrder.grabAndGo;

                                new qcssapp.Views.OrderReview(data);
                                qcssapp.Functions.addHistoryRecord(0, 'review');
                                qcssapp.Router.navigate('review', {trigger: true});

                                if ( data.errorDetails && data.errorDetails.length > 0 ) {
                                    $('#submitOrder').attr('disabled','disabled').addClass('unavailable');
                                    $('#review-footer-button-container').addClass('unavailable');
                                    qcssapp.Functions.displayError(data.errorDetails, 'Warning');
                                } else if ( data.time == "ASAP" ) {
                                    var readyTime = moment(data.readyTime, "hh:mm A");
                                    if ( readyTime.diff(receiveView.currentStoreTime, 'minutes') > 120 ) {
                                        qcssapp.Functions.displayError("This order will take longer than two hours to be ready. Please take note of the estimated time below", 'Warning');
                                    }
                                }
                            } else {
                                if ( data.errorDetails && data.errorDetails.length > 0 ) {
                                    qcssapp.Functions.displayError(data.errorDetails);
                                } else {
                                    qcssapp.Functions.displayError("An error occurred while processing your order.");
                                }
                            }
                        }.bind(this),
                        function(errorParameters, errorCode, errorDetails) {
                            if (errorCode === "E7303") {
                                $.alert("Invalid reward selection. Please go back and reselect rewards.");
                            } else {
                                $.confirm({
                                    title: errorCode,
                                    content: '<div class="content-text">' + errorDetails + '</div>',
                                    buttons: {
                                        cancel: {
                                            text: 'OK',
                                            btnClass: 'btn-default close-btn prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color'
                                        }
                                    }
                                });
                            }
                        },
                        function() {
                            qcssapp.Functions.hideMask();
                        },
                        '',
                        30000
                    );
                }.bind(this)
            });

            new qcssapp.Views.ButtonView({
                text: "Cancel",
                buttonType: "customCB",
                appendToID: "#cancelBtnContainer",
                id: "cancelReceive",
                class: "button-flat accent-color-one",
                callback: function() {
                    qcssapp.Functions.resetOrdering(false);
                    qcssapp.Router.navigate('main', {trigger: true});
                }
            });
        },

        setMethodPickup: function(showMsg) {
            var $orderTimeContainer = $('#orderTimeContainer');
            $('#receivePickup').addClass('selected');
            $('#deliveryLocation').hide();
            showMsg ?  $('.receiveMessage').show() : $('.receiveMessage').hide();
            $orderTimeContainer.find('label').html('Pickup Time *:');
            qcssapp.CurrentOrder.orderType = "pickup";
            if(this.eatInTakeOutActive) {
                $('#diningOptionContainer').show();
                this.usesEatInTakeOut = true;
            }

            if(qcssapp.futureOrderDate) {
                $orderTimeContainer.find('label').html('Pickup Time for ' + qcssapp.Functions.formatFutureDay() + ' ' + qcssapp.Functions.formatFutureDate() + ' :*');
            }
        },

        setMethodDelivery: function(showMsg) {
            var $orderTimeContainer = $('#orderTimeContainer');
            $('#receiveDelivery').addClass('selected');
            $('#deliveryLocation').show();
            showMsg ?  $('.receiveMessage').show() : $('.receiveMessage').hide();
            $orderTimeContainer.find('label').html('Delivery Time *:');
            qcssapp.CurrentOrder.orderType = "delivery";
            $('#diningOptionContainer').hide();
            this.usesEatInTakeOut = false;

            if(qcssapp.futureOrderDate) {
                $orderTimeContainer.find('label').html('Delivery Time for ' + qcssapp.Functions.formatFutureDay() + ' ' + qcssapp.Functions.formatFutureDate() + ' :*');
            }

            if(!this.showDeliveryInput) {
                $('#deliveryLocation').hide();
            }
        },

        setPhoneNumber: function() {
            var $orderPhone = $('#orderPhone');
            var $phoneSelect = $('#phoneSelect');
            var $mobileMsg = $('#mobileMsg');
            var $phoneSelectContainer = $('#phoneSelectContainer');

            $orderPhone.parent().find('label').text('Phone Number *:');
            $orderPhone.attr('data-required', true);

            var phone = qcssapp.accountModel.get('phone');
            var mobilePhone = qcssapp.accountModel.get('mobilePhone');

            if(!qcssapp.Functions.checkServerVersion(2,0)) {
                $orderPhone.parent().find('label').text('Phone Number *:');
                $phoneSelectContainer.hide();

            } else {
                $phoneSelectContainer.show();
                $phoneSelect.find('option').remove();
                $phoneSelect.append($("<option />").val("2").text("Mobile"));
                $phoneSelect.append($("<option />").val("1").text("Landline"));

                if( typeof qcssapp.CurrentOrder.storeModel.get('usesSMSNotifications') !== 'undefined' && !qcssapp.CurrentOrder.storeModel.get('usesSMSNotifications')) {
                    this.usesSMSNotifications = false;
                    $phoneSelect.val("2");
                    $orderPhone.val(mobilePhone);
                    $mobileMsg.hide();

                } else if (typeof qcssapp.CurrentOrder.storeModel.get('usesSMSNotifications') !== 'undefined'){
                    this.usesSMSNotifications = true;
                    $phoneSelect.find('option').remove();
                    $phoneSelect.append($("<option />").val("2").text("Mobile (order status updates)"));
                    $phoneSelect.append($("<option />").val("1").text("Landline/Mobile (Contact only)"));
                    $phoneSelect.val("2");
                    $orderPhone.val(mobilePhone);
                    $mobileMsg.show();
                }

                if(typeof qcssapp.CurrentOrder.storeModel.get('requirePhoneNum') !== 'undefined') {
                    if(!qcssapp.CurrentOrder.storeModel.get('requirePhoneNum')) {
                        $orderPhone.parent().find('label').text('Phone Number :');
                        $orderPhone.attr('data-required', false);
                    } else {
                        $orderPhone.parent().find('label').text('Phone Number *:');
                        $orderPhone.attr('data-required', true);
                    }
                }
            }

            $phoneSelect.selectric({disableOnMobile:false, nativeOnMobile:false});
            $phoneSelect.val($phoneSelect.find('option').attr('value')).selectric('refresh');

        },

        changePhoneSelect: function(e) {
            e.stopImmediatePropagation();

            var $phoneSelect = $('#phoneSelect');
            var $orderPhone = $('#orderPhone');
            var phone = qcssapp.accountModel.get('phone');
            var mobilePhone = qcssapp.accountModel.get('mobilePhone');
            var $mobileMsg = $('#mobileMsg');

            if ($phoneSelect.val() == "1") {
                qcssapp.accountModel.set('mobilePhone', $orderPhone.val());
                $orderPhone.val(phone);
                $mobileMsg.hide();

            } else if ($phoneSelect.val() == "2") {
                qcssapp.accountModel.set('phone', $orderPhone.val());
                $orderPhone.val(mobilePhone);
                if(this.usesSMSNotifications) {
                    $mobileMsg.show();
                }
            }
        },

        setDiningOptions: function() {
            var $diningOptionContainer = $('#diningOptionContainer');
            this.$diningOptionsSelect = $('#diningOptionsSelect');
            $diningOptionContainer.hide();

            if(!qcssapp.Functions.checkServerVersion(2,0) ) {
                return;
            }

            if(typeof qcssapp.CurrentOrder.storeModel.get('usesEatInTakeOut') !== 'undefined' && qcssapp.CurrentOrder.storeModel.get('usesEatInTakeOut')) {
                this.eatInTakeOutActive = true;
                this.$diningOptionsSelect.find('option').remove();
                this.loadDiningOptions();

            }

            // LVL3 defect 292: Remove the 'allowDelivery' constraint so dining option selector isn't hidden when delivery is disabled
            if(qcssapp.CurrentOrder.orderType == "pickup" && this.eatInTakeOutActive && qcssapp.CurrentOrder.storeModel.get('allowPickUp')) {
                $diningOptionContainer.show();
                this.usesEatInTakeOut = true;
            }
        },

        //load the Dining Options selector with the products in the store's dining options keypad
        loadDiningOptions: function() {
            var successParameters = {
                receiveView:this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/ordering/dining/option', 'POST', '', successParameters,
                function(response) {
                    successParameters.receiveView.fetchDiningSuccess(response);
                },
                function(errorParameters) {
                    errorParameters.receiveView.fetchDiningError();
                },
                function() {
                    // Once the dining options are loaded, previous inputs can be restored
                    successParameters.receiveView.restoreInputs();
                }, '', '', false, true
            );
        },

        //handles error for fetching of the collections
        fetchDiningError: function() {
            if (qcssapp.DebugMode) {
                console.log("Error loading data for "+this.internalName);
                console.log(this);
            }

            //display error message to user
            qcssapp.Functions.displayError('There was an error loading the dining options, please try again later.');
            if (!qcssapp.CurrentOrder.voucherCode) {
                qcssapp.Functions.hideMask();
            }
        },

        //handles successful fetching of the collections
        fetchDiningSuccess: function(data) {
            if (qcssapp.DebugMode) {
                console.log("Successfully loaded data for "+this.internalName);
                console.log(this);
            }
            //render the selectors
            this.renderDiningOptions(data);

            // After dining options have been loaded, go to next page
            if (qcssapp.Functions.checkServerVersion(4,1) && qcssapp.Functions.skipOrderDetails()) {
                $('#receiveNext').trigger('click');
            } else if (!qcssapp.CurrentOrder.voucherCode) {
                qcssapp.Functions.hideMask();
            }
        },

        renderDiningOptions: function(data) {
            var that = this;

            if(data == "") {
                qcssapp.Functions.displayError('The Dining Options are not configured correctly for this store.');
                return;
            }

            $.each(data, function() {
                var diningOptionView = new qcssapp.Views.DiningOptionView({
                    model:this
                });
                that.$diningOptionsSelect.append(diningOptionView.render().el);
            });

            this.$diningOptionsSelect.selectric({disableOnMobile:false, nativeOnMobile:false});
            this.$diningOptionsSelect.val(this.$diningOptionsSelect.find('option').attr('value')).selectric('refresh');

        },

        setOrderDetailFields: function() {
            this.showPhoneNumberInput = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('showPhoneNumberInput') ? qcssapp.CurrentOrder.storeModel.get('showPhoneNumberInput') : true;
            this.showCommentInput = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('showCommentInput') ? qcssapp.CurrentOrder.storeModel.get('showCommentInput') : true;
            this.showDeliveryInput = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('showDeliveryInput') ? qcssapp.CurrentOrder.storeModel.get('showDeliveryInput') : true;
            this.showOrderTypeAndTimeMsg = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('showOrderTypeAndTimeMsg') ? qcssapp.CurrentOrder.storeModel.get('showOrderTypeAndTimeMsg') : true;

            $('#phoneSelectContainer, #phoneFormField, #mobileMsg, #commentFormField').show();

            $('#orderLocation').attr('data-show', this.showDeliveryInput);
            $('#orderComments').attr('data-show', this.showCommentInput);

            if ( qcssapp.CurrentOrder.orderType == "delivery" ) {
                $('#deliveryLocation').show()
            }

            if(!this.showPhoneNumberInput) {
                $('#phoneSelectContainer, #phoneFormField, #mobileMsg').hide();
                $('#orderPhone').attr('data-required', false).val('');
            }

            if(!this.showCommentInput) {
                $('#commentFormField').hide();
            }

            if(!this.showDeliveryInput) {
                $('#deliveryLocation').hide();
                $('#orderLocation').val('');
            }
        },

        //set the Payment Method selector options and default value
        setPaymentMethodOptions: function() {
            if(!qcssapp.Functions.checkServerVersion(3,0) ) {
                this.$paymentMethodContainer.hide();
                return;
            }

            if(!this.chargeAtPurchaseTime) {
                this.$paymentMethodContainer.hide();
                this.$addVoucherButton.hide();
                return;
            }

            // Reset fields
            this.$paymentMethodSelect.find('option').remove();
            this.$paymentMethodContainer.show();
            this.$addVoucherButton.detach().appendTo(this.$paymentMethodContainer.find('label'));
            this.$addVoucherButton.hide();
            this.$voucherCodeContainer.detach().insertAfter(this.$paymentMethodContainer.find('label'));
            this.$voucherCodeContainer.hide();
            this.$voucherCodeInput.val("");
            this.$appliedVoucher.html(this.$appliedVoucher.children().last());
            this.$addPaymentMethodBtnContainer.hide();

            // Does the account have a Payment Method
            if( this.paymentMethodCollection && this.paymentMethodCollection.length > 0) {
                this.accountPaymentMethodModel = new qcssapp.Models.AccountPaymentMethod(this.paymentMethodCollection[0], {parse:true});
                this.hasPaymentMethod = true;
            }

            // QC Balance only option
            if( !this.isCreditCardAllowed ) {
                this.$paymentMethodContainer.hide();
                this.$paymentMethodSelect.append($("<option />").val("1").text("Quickcharge Balance"));
                this.$paymentMethodSelect.val("1");

            // CC as a Tender only option
            } else if ( this.isCreditCardAllowed && !this.allowMyQCFunding ) {

                // Doing CC as a tender and Payroll Deduction
                if(this.hasQuickChargeBalance) {   // Add Quickcharge Balance option first
                    this.$paymentMethodSelect.append($("<option />").val("1").text("Quickcharge Balance"));
                }

                // If the don't have payment method they must add one
                if( !this.hasPaymentMethod ) {
                    this.$paymentMethodSelect.append($("<option />").val("3").text("Add Payment Card"));
                    this.$paymentMethodSelect.val("3");

                // Otherwise show payment method and allow them to change it
                } else {
                    this.$paymentMethodSelect.append($("<option />").val("2").html(this.buildCreditCardOption()));
                    this.$paymentMethodSelect.append($("<option />").val("4").text("Change Payment Card"));
                    this.$paymentMethodSelect.val("2");
                }

                // Doing CC as a tender and Payroll Deduction
                if(this.hasQuickChargeBalance) {
                    this.$paymentMethodSelect.val("1");
                }

            // QC Balance and CC as a Tender are both options
            } else if ( this.isCreditCardAllowed && this.allowMyQCFunding ) {
                this.$paymentMethodSelect.append($("<option />").val("1").text("Quickcharge Balance"));
                this.$paymentMethodSelect.val("1");

                if( !this.hasPaymentMethod ) {
                    this.$paymentMethodSelect.append($("<option />").val("3").text("Add Payment Card"));
                } else {
                    this.$paymentMethodSelect.append($("<option />").val("2").html(this.buildCreditCardOption()));
                    this.$paymentMethodSelect.append($("<option />").val("4").text("Change Payment Card"));
                }
            }

            this.paymentMethodPreviousValue = this.$paymentMethodSelect.val();
            this.$paymentMethodSelect.attr('previous-val', this.paymentMethodPreviousValue);

            this.$paymentMethodSelect.selectric({disableOnMobile:false, nativeOnMobile:false});
            this.$paymentMethodSelect.val(this.$paymentMethodSelect.find('option').attr('value')).selectric('refresh');

            if ( this.isCreditCardAllowed && !this.allowMyQCFunding && !this.hasQuickChargeBalance && !this.hasPaymentMethod ) {
                this.$paymentMethodContainer.hide();
                this.$addPaymentMethodBtnContainer.show();
            }

            // when the customer adds a car, they'll be back to the receive view - set the new card as the payment method
            if(this.hasPaymentMethod && qcssapp.OrderMenuHistory.length && qcssapp.OrderMenuHistory[qcssapp.OrderMenuHistory.length - 1].type === "receive") {
                this.$paymentMethodSelect.val("2").selectric('refresh');
            }

            if (this.allowsVouchers) {
                this.$addVoucherButton.show();

                if (this.$addPaymentMethodBtnContainer.css("display") !== "none") {
                    // Move the add voucher button to the add payment method button container
                    this.$addVoucherButton.detach().appendTo(".addPaymentMethod-label");
                } else {
                    // Show the payment method selector, even if there is only one option
                    this.$paymentMethodContainer.show();
                }

                // If a voucher has already been applied, resend the inquire in case products/rewards/etc. have changed
                if (qcssapp.CurrentOrder.voucherCode) {
                    this.$addVoucherButton.trigger("click");
                    this.voucherInquiry(qcssapp.CurrentOrder.voucherCode, 1);
                }
            }
        },

        // Change event for Payment Method selector
        changePaymentMethod: function(e) {
            if(e) {
                e.stopImmediatePropagation();
            }

            var paymentMethodValue = this.$paymentMethodSelect.val();

            // Get previous payment method value and update it to the current value
            this.paymentMethodPreviousValue = this.$paymentMethodSelect.attr('previous-val');
            this.$paymentMethodSelect.attr('previous-val', paymentMethodValue);

            if(paymentMethodValue == "3" || paymentMethodValue == "4") {  // Add or Change Payment Card
                this.getPaymentRelatedDetails();
            }
        },

        // Click event for Payment Method selector
        clickAddPaymentMethod: function(e) {
            if(e) {
                e.stopImmediatePropagation();
            }

            var paymentMethodValue = this.$paymentMethodSelect.val();

            if(paymentMethodValue == "3" && this.$paymentMethodSelect.find('option').length == 1) {
                this.getPaymentRelatedDetails();
            }
        },

        // Change event for delivery location
        changeOrderLocation: function() {
            // If delivery location has been filled in, clear the delivery location error message
            if ($("#orderLocation").val().trim() != "" &&
                $("#receieveErrorContainer").find(".general-error-msg-content").text() == this.deliveryLocationErrorMsg) {
                qcssapp.Functions.resetErrorMsgs();
            }
        },

        // Click event for add voucher code
        clickAddVoucher: function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            if (this.$addPaymentMethodBtnContainer.css("display") !== "none") {
                // Move the voucher input to the add payment method button container
                this.$voucherCodeContainer.detach().insertAfter(".addPaymentMethod-label");
            }
            this.$voucherCodeContainer.show();
            this.$addVoucherButton.hide();
            this.$enterVoucherContainer.show();
            this.$appliedVoucher.hide();
        },

        // Click event for validate voucher code
        clickValidateVoucher: function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            var voucherCode = this.$voucherCodeInput.val().toUpperCase();

            // Ensure the voucher code is the correct length
            qcssapp.Functions.resetErrorMsgs();
            if (voucherCode.length === 8) {
                this.voucherInquiry("VCHR" + voucherCode);
            } else if (voucherCode.length === 12 && voucherCode.substr(0, 4) === "VCHR") {
                this.voucherInquiry(voucherCode);
            } else {
                qcssapp.Functions.displayError("Voucher code must be 8 characters following \"VCHR\".");
            }
        },

        // Keyup event for voucher code input
        inputVoucherKeyup: function(e) {
            // When enter is pressed, validate code
            if (e.keyCode === 13) {
                $("#validateVoucherButton").trigger("click");
            }
        },

        // Click event for remove voucher
        clickRemoveVoucher: function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            qcssapp.CurrentOrder.voucherCode = "";
            this.$appliedVoucher.html(this.$appliedVoucher.children().last());
            this.$appliedVoucher.hide();
            this.$enterVoucherContainer.show();
        },

        // Send an order inquiry to apply the voucher or determine that it is invalid
        voucherInquiry: function(voucherCode, transparency) {
            var orderObject = {
                "products": qcssapp.Functions.buildProductArray(),
                "combos": qcssapp.Functions.buildCombosArray(),
                "rewards": qcssapp.Functions.buildRewardsArray(),
                "useMealPlanDiscount": qcssapp.CurrentOrder.useMealPlanDiscount,
                "voucherCode": voucherCode
            };
            qcssapp.Functions.orderInquiry(orderObject, function(response) {
                if (response.tenders) {
                    // Find the voucher tender
                    var voucherIndex = $.inArray(true, response.tenders.map(tender => !!tender.voucherCode));
                    if (voucherIndex !== -1) {
                        var voucher = response.tenders[voucherIndex];
                        voucher.voucherCode.code = voucher.voucherCode.code.toUpperCase();

                        // Build the applied voucher text
                        var voucherText = voucher.voucherCode.voucher.name + " (";
                        voucherText += voucher.voucherCode.code + ") - ";
                        voucherText += qcssapp.Functions.formatPrice(voucher.voucherCode.voucher.fullValue);

                        qcssapp.CurrentOrder.voucherCode = voucher.voucherCode.code;
                        this.$appliedVoucher.prepend(voucherText);
                        this.$appliedVoucher.show();
                        this.$enterVoucherContainer.hide();
                        this.$voucherCodeInput.val("");
                        return;
                    }
                }
                qcssapp.CurrentOrder.voucherCode = "";
                var message = "";
                if (response.invalidTenders) {
                    // Find the invalid voucher tender
                    var invalidIndex = $.inArray(true, response.invalidTenders.map(tender => !!tender.voucherCode));
                    if (invalidIndex !== -1) {
                        message = response.invalidTenders[invalidIndex].voucherCode.notes;
                    }
                }
                qcssapp.Functions.displayError(message || "Could not apply voucher.");
            }.bind(this),
            function(errorParameters, mmhErrorCode, mmhErrorDetails) {
                qcssapp.CurrentOrder.voucherCode = "";
                qcssapp.Functions.displayError(mmhErrorDetails || "Could not apply voucher.");
            },
            {"callErrorFunction": true},
            "", "", false, false, false, transparency);
        },

        // Get the payment processor related details
        getPaymentRelatedDetails: function() {
            this.saveInputsForRedirect();

            var endPoint = qcssapp.Location + '/api/funding/account/detail';

            var paymentProcessor = qcssapp.Functions.getStorePaymentSetting('paymentProcessor', null);

            // If already have the payment processor details, don't fetch them again
            if(paymentProcessor != null && paymentProcessor.get('id') == '6') {
                this.goToFreedomPay();
                return;

            } else if (paymentProcessor != null && paymentProcessor.get('id') == '1') {
                this.goToStripePage();
                return;
            }

            //gets all details related to funding
            qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
                function(data) {
                    if( data.hasOwnProperty('accountFundingAllowed')) {
                        this.allowMyQCFunding = data['accountFundingAllowed'];
                    }

                    if ( data.hasOwnProperty('paymentProcessor') && ((data.paymentProcessor.hasOwnProperty('paymentPageFileName') && data.paymentProcessor.paymentPageFileName.length > 0) || data.paymentProcessor.name == "FreedomPay") ) {
                        var paymentProcessorModel = new qcssapp.Models.PaymentProcessor(data.paymentProcessor);

                        if(qcssapp.CurrentOrder && qcssapp.CurrentOrder.storePaymentModel) {
                            qcssapp.CurrentOrder.storePaymentModel.set('paymentProcessor', paymentProcessorModel);
                        }

                        // Go to FreedomPay
                        if ( paymentProcessorModel.get('id') == '6' ) {
                            this.goToFreedomPay();

                        // Otherwise go to Stripe
                        } else if ( paymentProcessorModel.get('id') == '1' ) {
                            this.goToStripePage();
                        }

                    } else {
                        qcssapp.Functions.displayPopup('An error occurred while attempting to get the payment processor details.', 'Error', 'OK');
                        this.$paymentMethodSelect.val(Number(this.paymentMethodPreviousValue)).selectric('refresh');
                        this.changePaymentMethod('');
                    }

                }.bind(this), '', '', '', '', true, true
            );
        },

        // Save the field inputs so that they can be restored after redirect to a browser
        saveInputsForRedirect: function() {
            qcssapp.CurrentOrder.inputSaveObject = {
                "orderLocation": $("#orderLocation").val(),
                "orderTime": $("#orderTime").val(),
                "diningOptionsSelect": $("#diningOptionsSelect").val(),
                "phoneSelect": $("#phoneSelect").val(),
                "orderPhone": $("#orderPhone").val(),
                "orderComments": $("#orderComments").val()
            };
        },

        // Restore the field inputs after redirect to a browser
        restoreInputs: function() {
            var inputs = qcssapp.CurrentOrder.inputSaveObject;
            if (inputs) {
                $("#orderLocation").val(inputs.orderLocation).trigger("change");
                $("#orderTime").val(inputs.orderTime).trigger("change");
                $("#diningOptionsSelect").val(inputs.diningOptionsSelect).trigger("change").selectric("refresh");
                $("#phoneSelect").val(inputs.phoneSelect).trigger("change").selectric("refresh");
                $("#orderPhone").val(inputs.orderPhone).trigger("change");
                $("#orderComments").val(inputs.orderComments).trigger("change");
                qcssapp.CurrentOrder.inputSaveObject = null;
            }
        },

        // Open the FreedomPay payment page
        goToFreedomPay: function() {
            var endPoint = qcssapp.Location + '/api/freedompay/createtokentransaction';

            var paymentProcessor = qcssapp.Functions.getStorePaymentSetting('paymentProcessor', null);

            if(paymentProcessor == null) {
                qcssapp.Functions.displayPopup('An error occurred while attempting to load the payment processor details.', 'Error', 'OK');
                return;
            }

            var parameters = {
                "StoreId": paymentProcessor.get('paymentProcessorStoreNum'),
                "TerminalId": paymentProcessor.get('paymentProcessorTerminalNum')
            };

            // Create token transaction
            qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(parameters), '',
                function(response) {

                    //always include current balance
                    var checkoutURL = qcssapp.Functions.htmlDecode(response.CheckoutUrl);

                    if ( response.ResponseMessage && response.ResponseMessage.length > 0 && response.ResponseMessage.toString().toLocaleLowerCase() != "null" ) {
                        qcssapp.Functions.displayPopup(response.ResponseMessage, 'Error', 'OK');
                        this.$paymentMethodSelect.val(Number(this.paymentMethodPreviousValue)).selectric('refresh');
                        this.changePaymentMethod('');
                        return;
                    } else if ( !checkoutURL || checkoutURL.length == 0 ) {
                        qcssapp.Functions.displayPopup('An error occurred while attempting to add a payment method.', 'Error', 'OK');
                        this.$paymentMethodSelect.val(Number(this.paymentMethodPreviousValue)).selectric('refresh');
                        this.changePaymentMethod('');
                        return;
                    }

                    qcssapp.Functions.hideMask();

                    var continueFn = function() {
                        qcssapp.preventUnloadLogout = true;

                        //set the resumeApp flag and order in session
                        qcssapp.Functions.setResumeAppAndOrderInSession();

                        if(qcssapp.onPhoneApp) {
                            cordova.InAppBrowser.open(checkoutURL, '_system', 'location=yes');

                        } else {
                            qcssapp.Functions.showMask();

                            //on browser - simply redirect
                            window.location = checkoutURL;
                        }
                    };

                    var cancelFn = function() {
                        if (this.$addPaymentMethodBtnContainer.is(":hidden")) {
                            this.$paymentMethodSelect.val(Number(this.paymentMethodPreviousValue)).selectric("refresh");
                            this.changePaymentMethod("");
                        }
                    }.bind(this);

                    var popupMsg = 'You will now be redirected to a secure MyQC payment page powered by FreedomPay';
                    qcssapp.Functions.displayPopup(popupMsg, 'Redirecting', 'Cancel', cancelFn, '', 'Continue', continueFn);

                }.bind(this)
            );
        },

        goToStripePage: function() {

            var paymentProcessor = qcssapp.Functions.getStorePaymentSetting('paymentProcessor', null);

            if(paymentProcessor == null) {
                qcssapp.Functions.displayPopup('An error occurred while attempting to load the payment processor details.', 'Error', 'OK');
                this.$paymentMethodSelect.val(Number(this.paymentMethodPreviousValue)).selectric('refresh');
                this.changePaymentMethod('');
                return;
            }

            var options = {
                paymentPageFileName: paymentProcessor.get('paymentPageFileName'),
                slideDownFlag: true,
                goToAgreement: false
            };

            qcssapp.Functions.showMask();

            if ( this.paymentView == null ) {
                this.paymentView = new qcssapp.Views.Payment(options);
            } else {
                this.paymentView.initialize(options);
            }

            //go to payment page
            qcssapp.Router.navigate('payment', {trigger: true});
        },

        buildCreditCardOption: function() {
            if(!this.accountPaymentMethodModel) {
                return 'Credit Card';
            }

            var paymentMethodIcon = '';

            var paymentMethodType = '';
            if(this.accountPaymentMethodModel.get('paymentMethodType')) {
                paymentMethodType = this.accountPaymentMethodModel.get('paymentMethodType').toUpperCase();
            }
            var paymentMethodLastFour = this.accountPaymentMethodModel.get('paymentMethodLastFour');
            var paymentMethodExpMonth = this.accountPaymentMethodModel.get('paymentMethodExpirationMonth');
            var paymentMethodExpYear = this.accountPaymentMethodModel.get('paymentMethodExpirationYear');

            switch ( paymentMethodType ) {
                case ('VISA'):
                    paymentMethodIcon = 'payment-method_icon--visa';
                    break;
                case ('AMEX'):
                    paymentMethodIcon = 'payment-method_icon--amex';
                    break;
                case ('M/C'):
                    paymentMethodIcon = 'payment-method_icon--mastercard';
                    break;
                case ('DCVR'):
                    paymentMethodIcon = 'payment-method_icon--discover';
                    break;
                default:
                    paymentMethodIcon = 'payment-method_icon';
                    break;
            }

            return "<div class='payment-method_select-cc'><span style='margin-right:7px;'>Credit Card - " + paymentMethodType + " XXXX " + paymentMethodLastFour + ", " + paymentMethodExpMonth + "/" + paymentMethodExpYear + "</span><span class='payment-method_cc-icon-container'><div class='payment-method_cc-icon "+paymentMethodIcon+"'></div></span></div>";
        },

        buildTimePicker: function(timeObj, setASAP) {
            //set the future time picker options
            if ( qcssapp.TimePicker ) {
                qcssapp.TimePicker.clear();
                //set the min and max times for the picker
                qcssapp.TimePicker.set('min',moment(timeObj.minFutureTime, "hh:mm A").toDate()).set('max', moment(timeObj.maxFutureTime, "hh:mm A").toDate());

            } else {

                this.$orderTime.pickatime({
                    interval: 15,
                    min: moment(timeObj.minFutureTime, "hh:mm A").toDate(),
                    max: moment(timeObj.maxFutureTime, "hh:mm A").toDate(),
                    container: '#orderTimeContainer',
                    onOpen: function() {
                        $('.scroll-indicator').addClass('hidden');
                    },
                    onClose: function () {
                        $('.scroll-indicator').removeClass('hidden');

                        if ( this.$orderTime.val() == "" ) {
                            var prepTime = Number(qcssapp.CurrentOrder.storeModel.attributes.pickUpPrepTime);
                            if ( qcssapp.CurrentOrder.orderType == "delivery" ) {
                                prepTime += Number(qcssapp.CurrentOrder.storeModel.attributes.deliveryPrepTime);
                            }
                            var timePickerModel = this.$orderTime.data('timePickerModel');
                            var timeASAP = qcssapp.Functions.setASAPTime(timePickerModel, prepTime, this.$el);
                            timeASAP = qcssapp.Functions.formatASAP(timeASAP);
                            this.$orderTime.val("ASAP ("+timeASAP+")").trigger("change");
                        }
                    }.bind(this),
                    onRender: function() {
                        $('ul.picker__list li').removeClass('picker__list-item--highlighted');
                    }
                });
            }
            qcssapp.TimePicker = this.$orderTime.pickatime('picker');

            //render the picker and enable the proper times
            qcssapp.TimePicker.render(true).set('enable', true).set('disable', false);

            if ( timeObj.enableTimes.length > 1 ) {
                qcssapp.TimePicker.set('disable', timeObj.enableTimes);

                qcssapp.TimePicker.set('disable', 'flip');
            }

            // Fix for scrollHeight bug: set time picker display to none when not open
            qcssapp.TimePicker.$root.css('display', 'none');

            //to show disabled times as 'greyed-out' look in default.time.css for the tag .picker__list-item--disabled, remove display: none and uncomment items
            qcssapp.TimePicker.on('open', function() {
                if ( $('.picker__button--clear').length == 1 ) {
                    var HTMLstr = '<li><button class="picker__button--clear" style="margin-bottom:5px;" type="button" data-clear="1" aria-controls="orderTime">ASAP</button></li>';
                    $('.picker__list').prepend($.parseHTML(HTMLstr));
                }
                $('.picker__button--clear').text('ASAP ('+qcssapp.asapTime+')');
                $('#orderTime_root').css('display', '');
            }.bind(this));

            qcssapp.TimePicker.on('close', function() {
                $('#orderTime_root').css('display', 'none');
            }.bind(this));

            this.forceASAP = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('forceASAP') ? qcssapp.CurrentOrder.storeModel.get('forceASAP') : false;
            if( this.forceASAP ) {
                $('#orderTimeContainer').hide();
            }
        },

        adjustForPrep: function(storeTimeMoment, prepTime, currentStoreTime) {
            //if given store moment (time) is before now (store is open already) then use now
            var now = currentStoreTime.clone();

            if ( now.diff(storeTimeMoment) > 0 ) {
                storeTimeMoment = now;
            }

            var mins = storeTimeMoment.minutes() + prepTime;
            var addHours = 0;

            //round the minutes the user can choose from up to the nearest 15 min interval
            if ( mins > 60 ) {
                addHours = Math.floor(mins/60);
                mins -= 60*addHours;
            }

            if ( mins > 45 && mins <= 60 ) {
                mins = 0;
                addHours += 1;
            } else if ( mins > 30 && mins <= 45 ) {
                mins = 45;
            } else if ( mins > 15 && mins <= 30 ) {
                mins = 30;
            } else if ( mins >= 0 ) {
                mins = 15;
            }

            storeTimeMoment.minutes(mins);

            if ( addHours) {
                storeTimeMoment.add(addHours, 'h');
            }

            return storeTimeMoment;
        },

        buildTimeRule: function(openMoment, closeMoment) {
            //example: { from: [17,0], to: [18,15] }
            return {
                from: [ openMoment.hours(), openMoment.minutes() ],
                to: [ closeMoment.hours(), closeMoment.minutes() ]
            };
        },

        determineOpenAndCloseTimes: function(openTimes, closeTimes, prepTime, currentStoreTime) {
            var minFutureTime = moment("11:59 PM", "hh:mm A");
            var maxFutureTime = currentStoreTime;
            var endOfDay = moment("11:45 PM", "hh:mm A");
            var openTimeMoment = "";
            var closeTimeMoment = "";
            var enableTimes = [];

            for (var i = 0; i < openTimes.length; i++) {

                openTimeMoment = this.adjustForPrep( moment(openTimes[i], "hh:mm A"), prepTime, currentStoreTime );

                //take the first openTimeMoment, then subsequently the earlier moment for each time
                minFutureTime = minFutureTime ? moment.min(minFutureTime, openTimeMoment) : openTimeMoment;

                closeTimeMoment = this.adjustForPrep( moment(closeTimes[i], "hh:mm A"), prepTime, currentStoreTime );

                //bug with the time picker when sending max times past the last possible interval (11:45 PM), if max time is after 11:45 PM, set to 11:45 PM.
                if ( closeTimeMoment.diff(endOfDay) >= 0 ) {
                    closeTimeMoment = endOfDay;
                }

                maxFutureTime = maxFutureTime ? moment.max(maxFutureTime, closeTimeMoment) : closeTimeMoment;

                enableTimes.push(this.buildTimeRule(openTimeMoment, closeTimeMoment));

            }

            return {
                enableTimes: enableTimes,
                minFutureTime: minFutureTime,
                maxFutureTime: maxFutureTime
            }
        },

        buildTimeObj: function(currentStoreTime) {
            var storeModelAttrs = qcssapp.CurrentOrder.storeModel.attributes;
            var prepTime = Number(storeModelAttrs.pickUpPrepTime);

            if ( qcssapp.CurrentOrder.orderType == "pickup" ) {

                return this.determineOpenAndCloseTimes(storeModelAttrs.pickUpOpenTimes, storeModelAttrs.pickUpCloseTimes, prepTime, currentStoreTime);

            }

            prepTime += Number(storeModelAttrs.deliveryPrepTime);

            return this.determineOpenAndCloseTimes(storeModelAttrs.deliveryOpenTimes, storeModelAttrs.deliveryCloseTimes, prepTime, currentStoreTime);

        },

        setupTimePicker: function(currentStoreTime) {
            //set the times for future ordering based on closing time and prep intervals of the store
            var timeObj = this.buildTimeObj(currentStoreTime);

            //set the future time picker options
            if ( qcssapp.TimePicker ) {
                qcssapp.TimePicker.clear();
                //set the min and max times for the picker
                qcssapp.TimePicker.set('min', timeObj.minFutureTime.toDate()).set('max', timeObj.maxFutureTime.toDate());

            } else {
                var that = this;

                this.$orderTime.pickatime({
                    interval: 15,
                    min: timeObj.minFutureTime.toDate(),
                    max: timeObj.maxFutureTime.toDate(),
                    container: '#orderTimeContainer',
                    clear: 'ASAP',
                    onClose: function() {
                        if ( that.$orderTime.val() == "" ) {
                            that.$orderTime.val("ASAP").trigger("change");
                        }
                    }
                });

                qcssapp.TimePicker = this.$orderTime.pickatime('picker');
            }

            //render the picker and enable the proper times
            qcssapp.TimePicker.render(true).set('enable', true).set('disable', false);

            if ( timeObj.enableTimes.length > 1 ) {
                qcssapp.TimePicker.set('disable', timeObj.enableTimes);

                qcssapp.TimePicker.set('disable', 'flip');
            }

            // Fix for scrollHeight bug: set time picker display to none when not open
            qcssapp.TimePicker.$root.css('display', 'none');

            var asapTime = qcssapp.asapTime;

            qcssapp.TimePicker.on('open', function() {
                if ( $('.picker__button--clear').length == 1 ) {
                    var HTMLstr = '<li><button class="picker__button--clear" style="margin-bottom:5px;" type="button" data-clear="1" aria-controls="orderTime">ASAP</button></li>';
                    $('.picker__list').prepend($.parseHTML(HTMLstr));
                }
                $('.picker__button--clear').text('ASAP ('+asapTime+')');
                $('#orderTime_root').css('display', '');
            });

            qcssapp.TimePicker.on('close', function() {
                $('#orderTime_root').css('display', 'none');
            });
        }

    });
})(qcssapp, jQuery);