;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.SpendingLimitView - View for displaying the global balance view
     */
    qcssapp.Views.NutritionInfoView = Backbone.View.extend({
        internalName: "Nutrition Info View",
        className: 'keypad-line-nutrition',

        //events: //events not needed for this view
        initialize: function() {
            this.collection = new qcssapp.Collections.NutritionCollection(this.collection);

            this.render();
        },

        render: function() {
            // Clear the Html to make sure it is empty
            this.$el.html("");
            this.collection.each(function(model) {
                this.renderLine(model);
            }, this);

            return this;
        },
        renderLine: function(model) {
            var nutritionInfo = new qcssapp.Views.NutritionInfoLineView({
                model: model
            });
            this.$el.append(nutritionInfo.render().el);
        }
    });
})(qcssapp, jQuery);
