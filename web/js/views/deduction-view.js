;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.DeductionView - View for displaying Deduction Collections
     */
    qcssapp.Views.DeductionView = Backbone.View.extend({
        internalName: "Deduction View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#deduction-list', //references existing HTML element for Deduction Collections - can be referenced as this.$el throughout the view

        //template: //template not needed for this view

        //events: //events not needed for this view

        initialize: function () {
            if (qcssapp.DebugMode) {
                console.log("initialize in deduction view");
            }

            //TODO make a model with these defaults
            this.itemsPerPage = 20;
            this.currentPage = 1;
            this.sort = "date";
            this.sortOrder = "desc";
            this.endOfList = false;

            //bind "this" reference to functions in the view
            _.bindAll(this, "fetchSuccess", "fetchError", "checkScroll");

            $('#show-deductions').on('scroll',this.checkScroll);

            //create new collection
            this.collection = new qcssapp.Collections.Deductions();

            //property to prevent further requests while loading, true because of the next fetch, turned false in fetch success
            this.loading = true;

            //populates the new collection
            this.loadCollection();
        },

        fetchError: function() { //handles error for fetching the Deduction collection
            if (qcssapp.DebugMode) {
                console.log("Error loading data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            //display error message to user
            qcssapp.Functions.displayError('There was an error loading your deductions, please try again later.');
            qcssapp.Functions.hideMask();
        },

        fetchSuccess: function(data) { //handles successful fetching the Deduction collection
            if (qcssapp.DebugMode) {
                console.log("Successfully loaded data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            if ( this.currentPage == 1 && data.length == 0 ) {
                $('#deduction-list').append('<li class="align-center italic">No Deductions Found.</li>');

                //no point in rendering nothing
                qcssapp.Functions.hideMask();
                return false;
            }

            if ( this.collection.length < this.itemsPerPage ) {
                this.endOfList = true;
            }

            //display view
            this.render();

            //reset loading once complete
            this.loading = false;

            //for infinite scroll message
            qcssapp.Functions.hideMask();
        },

        //calls renderLine on each deduction
        render: function() {
            this.collection.each(function( mod ) {
                this.renderLine( mod );
            }, this );

            if ( this.endOfList ) {
                $('#deduction-list').append('<li class="align-center primary-color endlist accent-color-one italic">End of Deduction List</li>');
            }
        },

        //builds new individual deduction view and appends to this list view
        renderLine: function( mod ) {
            var deductionLineView = new qcssapp.Views.DeductionLineView({
                model: mod
            });                                              
            this.$el.append( deductionLineView.render().el );
        },

        //determines whether to load more based on scroll position and loading status
        checkScroll: function() {
            var triggerPoint = 100; //100px from bottom of page
            var $page = $('#show-deductions');
            var pageScroll = $page.scrollTop();
            var pageViewHeight = $page.height();
            var listHeight = $('#deduction-list').height();

            if ( !this.loading && !this.endOfList && pageScroll + triggerPoint + pageViewHeight > listHeight ) {
                this.currentPage += 1;

                this.loading = true;
                qcssapp.Functions.showMask();

                //load deductions
                this.loadCollection();
            }
        },

        loadCollection: function() {
            var dataParameters = $.param({ models: this.itemsPerPage, page: this.currentPage, sort: this.sort, order: this.sortOrder });

            var successParameters = {
                deductionView:this
            };

            var errorParameters = {
                deductionView:this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/deductions', 'GET', dataParameters,successParameters,
                function(response, successParameters) {
                    $.each(response, function() {
                        this.amt = this.amt.toFixed(2);
                        var datetime = this.date.substring(0, this.date.length - 6);
                        var ampm = this.date.substring(this.date.length - 2, this.date.length);
                        var newDate = datetime +"&nbsp;"+ ampm;
                        this.date = newDate;
                    });

                    successParameters.deductionView.collection.set(response);
                    successParameters.deductionView.fetchSuccess(response);
                },
                function(errorParameters) {
                    errorParameters.deductionView.fetchError();
                },
                '',
                errorParameters
            );
        }
    });
})(qcssapp, jQuery);   