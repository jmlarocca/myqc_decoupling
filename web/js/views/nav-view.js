;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
	qcssapp.Functions.SetGlobalViewSettings();
    
    /* qcssapp.Views.NavView - View for displaying main home page navigation, home page of the app
	*/	
    qcssapp.Views.NavView = Backbone.View.extend({
        internalName: "App Nav View", //NOT A STANDARD BACKBONE PROPERTY
        
        el: '#item-list', //references existing HTML element for the navigation collection - can be referenced as this.$el throughout the view
        
        //template: //template not needed for this view
        
        //events: //events not needed for this view

        initialize: function () {
            if (qcssapp.DebugMode) {
                console.log("initialize in nav view");
            }

            //bind "this" reference to functions in the view
            _.bindAll(this, "fetchSuccess", "fetchError", "getUserDetails", "determineTourPages", 'handleFeatures', 'handleUserDetails');

            //create new collection
            this.collection = new qcssapp.Collections.Items();
            this.featureCollection = new qcssapp.Collections.Features();
            this.listenTo( this.collection, 'add', this.renderLine );

            //feature checking and getting user info
            this.checkAvailableFeatures();

            // TODO: update the version needed when we know what is being targeted.
            if (qcssapp.Functions.checkServerVersion(6, 0, 8)) {
                this.getLocationSettings();
            }
        },

        checkAvailableFeatures: function() {
            var endPoint = qcssapp.Location + '/api/myqc/check/features';
            var successParameters = {navView:this};

            qcssapp.Functions.callAPI(endPoint, 'GET', '', successParameters,
                function(result, successParameters) {
                    successParameters.navView.handleFeatures(result);
                },
                '',
                '',
                '',
                '',
                false,
                true,
                true
            );
        },

        handleFeatures: function(featureDetails) {
            //check that the APIVersion feature is valid (will be invalid if server version > app version)
            var APIVersion = _(featureDetails).findWhere({'id': 'apiversion'});
            if ( APIVersion.available != 'yes' ) {
                //add logout model so user can logout
                var logoutModel = new qcssapp.Models.Item({id: "logout", title: "Log Out"});
                var iosModel = new qcssapp.Models.Item({id:'iosStore', title:"iOS App Store"});
                var androidModel = new qcssapp.Models.Item({id:'androidStore',title:"Google Play Store"});
                this.collection.add([iosModel,androidModel,logoutModel]);
                //show error
                if ( qcssapp.onPhoneApp ) {
                    qcssapp.Functions.displayError('You must update the MyQuickcharge App from the Google Play or Apple App Store before continuing.');
                } else {
                    qcssapp.Functions.displayError('You need to reload the application as the application has been upgraded. You may need to clear cache if you keep seeing this message.');
                }

                qcssapp.Functions.hideMask();

                return;
            }

            //for an employee account, set the feature details and continue to load user details
            if ( !qcssapp.personModel ) {
                this.featureCollection.set(featureDetails);

                this.getUserDetails();
                return;
            }

            //for person login, the global accountModel will already be set
            new qcssapp.Views.DetailView();

            //for a person, adjust the feature settings
            $.each(featureDetails, function() {
                var featureDetail = this;
                if (featureDetail.id !== "funding" && featureDetail.id !== "apiversion" &&
                    featureDetail.id !== "onlineordering" && featureDetail.id !== "feedback") {
                    featureDetail.available = false;
                }
            });

            this.featureCollection.set(featureDetails);

            this.loadCollection();
        },

        //TODO: this function makes the same call and does similar work to Functions.checkAccountStatus
        //gets the user's name, account number and account type and displays the name/number on the main navigation home page
        getUserDetails: function() {
            if (qcssapp.DebugMode) {
                console.log("Getting user details...");
            }

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/accountdetails','GET','',{navView:this},
                function(result, successParameters) {
                    if (qcssapp.DebugMode) {
                        console.log("Retrieved user details:");
                        console.log(result);
                    }

                    //result set should contain many details, only concerned about name, number and type
                    if ( !result ) {
                        qcssapp.Functions.throwFatalError("An error occurred while loading account details, please log in again", "login");
                        return;
                    }

                    successParameters.navView.handleUserDetails(result);
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error loading account details");
                    }

                    qcssapp.Functions.throwFatalError("An error occurred while loading account details, please log in again", "login");
                },
                '',
                '',
                '',
                false,
                true,
                true
            );
        },

        handleUserDetails: function(userDetails) {
            var accountModel = new qcssapp.Models.AccountInfoModel();

            //check through result set for the details and set them
            $.each(userDetails, function() {
                if ( this.id == "name" ) {
                    accountModel.set('name', qcssapp.Functions.htmlDecode(this.value));
                } else  if ( this.id == "employee-number") {
                    accountModel.set('accountNumber', this.value);
                } else if ( this.id == "account-type-id" ) {
                    accountModel.set('accountType', this.value);
                } else if ( this.id == "account-status" ) {
                    accountModel.set('accountStatus', this.value);
                } else if ( this.id == "phone-number" ) {
                    accountModel.set('phone', qcssapp.Functions.htmlDecode(this.value));
                } else if ( this.id == "account-badge" ) {
                    accountModel.set('badgeNumber', this.value);
                } else if (this.id == "force-change" ) {
                    accountModel.set('forceChangeAccountGroup', this.value)
                } else if (this.id == "badgeNumPrefix") {
                    accountModel.set('badgeNumPrefix', this.value);
                } else if (this.id == "badgeNumSuffix") {
                    accountModel.set('badgeNumSuffix', this.value);
                } else if ( this.id == "mobile-phone" ) {
                    accountModel.set('mobilePhone', qcssapp.Functions.htmlDecode(this.value));
                } else if ( this.id == "email" ) {
                    accountModel.set('email', qcssapp.Functions.htmlDecode(this.value));
                } else if ( this.id == "qrCodeLength" ) {
                    accountModel.set('qrCodeLength', this.value);
                } else if ( this.id == "faqs" ) {
                    var faqCollection = new qcssapp.Collections.FAQs();
                    faqCollection.set(this['faqs']);
                    accountModel.set('FAQs', faqCollection);
                } else if ( this.id == "orgLevelPrimaryId" ){
                    accountModel.set("orgLevelPrimaryData", {
                        orgLevelPrimaryId:this.value,
                        name:accountModel.get('orgLevelPrimaryData').name
                    });
                } else if( this.id == 'orgLevelName' ){
                    accountModel.set("orgLevelPrimaryData", {
                        orgLevelPrimaryId:accountModel.get('orgLevelPrimaryData').orgLevelPrimaryId,
                        name:this.value
                    });
                } else if ( this.id == "employeeLowBalanceThreshold"){
                    accountModel.set("employeeLowBalanceThreshold", this.value);
                }
            });

            //set the account model on the global accessor
            qcssapp.accountModel = accountModel;

            qcssapp.Functions.setBadgeInSession();

            if( (qcssapp.Functions.checkServerVersion(2,0) && accountModel.get('forceChangeAccountGroup') == 'true') ||
                (qcssapp.Functions.checkServerVersion(2,1) && accountModel.get('accountType') == '4' && qcssapp.Functions.getKeepMeLogged()) )  {
                $('#accountSettings').off();
                new qcssapp.Views.AccountSettingsView(true);

                qcssapp.Router.navigate('show-accountSettings', {trigger:true});

                //check status for TOS
            } else if ( qcssapp.accountModel.get('accountStatus').toUpperCase() == "W" ) {
                qcssapp.Functions.throwFatalError('Your user agreement has changed, you will be logged out until it is accepted. <br><br> If you continue to see this issue, contact your IT department or manager.','login');
                return;
            }

            //create detail view, add information to main navigation page
            new qcssapp.Views.DetailView();

            //check if account is frozen, if it is then update freeze account links for unfreeze
            if ( qcssapp.accountModel.get('accountStatus').toUpperCase() == "F" && this.featureCollection.get('accountfreeze').get('available') != "no") {
                //TODO: update nav to look at item collection change name events for changing freeze page title based on status
                var $navFreeze = $('#nav-freeze');
                var $freezeBtnIcon = $('.freeze-btn-icon-container');
                var freezeIcon = $navFreeze.find('img');
                var $freezeBtnIconImg = $freezeBtnIcon.find('img');

                $navFreeze.text("Unfreeze Account").prepend(freezeIcon);

                $('#freeze-btn').addClass('frozen');

                $('.freeze-content').each(function() {
                    var $this = $(this);

                    $this.text($this.attr('data-frozenText') );
                });

                $freezeBtnIconImg.attr('src', $freezeBtnIcon.attr('data-frozenImg'));

                $("#show-freeze").attr('data-title','Unfreeze Account');
            }

            this.loadCollection();
        },

        loadCollection: function() {
            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/nav', 'GET', '',{navView:this},
                function(response, successParameters) {
                    successParameters.navView.collection.set(response);
                    successParameters.navView.fetchSuccess(response);
                },
                function(errorParameters) {
                    errorParameters.navView.fetchError();
                },
                '',
                {navView:this},
                '',
                '',
                '',
                true
            );
        },

        fetchError: function() { //handles error for fetching the navigation collection
            if (qcssapp.DebugMode) {
                console.log("Error loading data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            qcssapp.Functions.throwFatalError();
        },

        fetchSuccess: function(data) { //handles successful fetching the navigation collection
            if (qcssapp.DebugMode && data.length > 0) {
                console.log("Successfully loaded data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            } else if ( qcssapp.DebugMode ) {
                console.log("Communication successful, but no data returned for "+this.internalName);
            }

            this.render();
        },


        getLocationSettings: function() {
            if (qcssapp.DebugMode) {
                console.log("Getting locations settings...");
            }

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/locationSettings','GET','',{},
                function(result, successParameters) {
                    if (qcssapp.DebugMode) {
                        console.log("Retrieved location filter settings :");
                        console.log(result);
                    }

                    //result set should contain many details, only concerned about name, number and type
                    if ( !result ) {
                        qcssapp.Functions.throwFatalError("An error occurred while loading account details, please log in again",'login');
                        return;
                    }
                    qcssapp.locationFilterSettings = result;
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error loading account details");
                    }

                    qcssapp.Functions.throwFatalError("An error occurred while loading account details, please log in again",'login');
                },
                '',
                '',
                '',
                false,
                true,
                true
            );
        },


        //calls renderLine on each item in the navigation collection
        render: function() {
             //check if fingerprint auth is turned on for the device, if it is verify the fingerprint
            if(qcssapp.Functions.fingerprintEnabled() && !qcssapp.verifyFingerprintFromLogin) {
                qcssapp.Functions.verifyFingerprintOnDevice();
            }

            qcssapp.verifyFingerprintFromLogin = false;  //set to false after verifying fingerprint from login screen

            if( qcssapp.usingBranding && qcssapp.showTour ) {
                this.determineTourPages(this.collection);
            }

            //add nav menu items that will not come back from back end if server is 1.0 still
            if ( !$('#nav-freeze').length && this.featureCollection.get('accountfreeze').get('available') != "yes" ) {
                var freezeModel = new qcssapp.Models.Item({id: "freeze", title: "Freeze Account"});
                this.collection.add(freezeModel);
            }

            if ( !$('#nav-about').length ) {
                var aboutModel = new qcssapp.Models.Item({id: "about", title: "About Quickcharge"});
                this.collection.add(aboutModel);
            }

            var $logout = $('#nav-logout');
            if (!$logout.length) {
                var logoutModel = new qcssapp.Models.Item({id: "logout", title: "Log Out"});
                this.collection.add(logoutModel);
            }

            this.setFeaturePageText();

            this.setAboutView();
        },

        //creates a new view for each navigation item and assigns the model, then displays it on the page
        renderLine: function( item ) {
            var itemID = item.get('id').toLowerCase();

            //only show online ordering if available
            if ( itemID == "order" && this.featureCollection.get('onlineordering').get('available') != "yes" ) {
                return;
            }

            //only show rewards if available
            if ( itemID == "rewards" && this.featureCollection.get('rewards').get('available') != "yes" ) {
                return;
            }

            //only show freeze if available
            if ( itemID == "freeze" && this.featureCollection.get('accountfreeze').get('available') != "yes" ) {
                return;
            }

            //only show account funding if available
            if ( itemID == "accountfunding" && this.featureCollection.get('funding').get('available') != "yes" ) {
                return;
            }

            //only show leave feedback if available
            if ( itemID == "feedback" && this.featureCollection.get('feedback').get('available') != "yes" ) {
                return;
            }

            //only show deduction page in tour if this ID is found, don't show in nav menu bc user has no deductions yet
            if ( itemID == "deductiontour" || itemID == "quickpaytour" || itemID == "ordertour") {
                return;
            }

            var itemView = new qcssapp.Views.MenuItemView({
                model: item
            });
            this.$el.append( itemView.render().el );
        },

        setFeaturePageText: function() {
            $('li.navMenuLink').each(function () {
                var menuId = $(this).children()[0].id;
                var menuText = $(this).children()[0].innerText;
                if (menuId == 'nav-accountFunding') {
                    $('#accountFunding').attr('data-title', menuText);
                } else if (menuId == 'nav-rewards') {
                    $('#show-rewards').attr('data-title', menuText + ' Programs');
                    $('#program-rewards').attr('data-title', menuText);
                } else if (menuId == 'nav-balances') {
                    $('#show-balances').attr('data-title', menuText);
                } else if (menuId == 'nav-spendinglimits') {
                    $('#show-spendinglimits').attr('data-title', menuText);
                    $('#spendingProfileTitle').text('Select ' + menuText + ':');
                    $('#spendingProfileSubtitle').text(menuText + ' Details:');
                } else if (menuId == 'nav-purchases') {
                    if (menuText == " Purchase History") {
                        menuText = "Purchases";
                    }
                    $('#show-purchases').attr('data-title', menuText);
                } else if (menuId == 'nav-quickPay') {
                    $('#show-quickPay').attr('data-title', menuText);
                } else if (menuId == 'nav-deductions') {
                    $('#show-deductions').attr('data-title', menuText);
                }
            });
        },

        setAboutView: function() {
            // Show the first page elements
            $("#tour-header").show();
            qcssapp.Functions.showAboutPageElements();
            if (qcssapp.Functions.checkServerVersion(6, 1, 13)) {
                qcssapp.Functions.showAcceptedTOS();
            }

            var $tourPanel = $(".tour-panel");
            var $tourFooterClass = $(".tour-footer");
            var $tourLogoBottom = $(".tour-logo-bottom");
            var $getStartedBtns = $("#tour-logo-get-started-container");
            $tourPanel.css("height", "93%");
            if (qcssapp.showTour) {
                // Show the tour-related elements
                if (!$tourLogoBottom.is(":visible") || qcssapp.usingBranding) {
                    $tourFooterClass.css("height", "120px");
                    $getStartedBtns.hide();
                    $tourLogoBottom.show();
                    $("#tour-footer").addClass("tour-footer-iphoneX");
                }
                if (qcssapp.usingBranding) {
                    $tourFooterClass.css("height", "110px");
                }
            } else {
                // Hide the tour-related elements
                $tourFooterClass.hide();
                $getStartedBtns.hide();
                $(".tour-text-0").hide();
                $(".tour-subtext.subtext-note").hide();
                $tourPanel.swipe("disable");
            }
        },

        determineTourPages: function(collection) { //determines which pages should be added to the tour based on MyQCNavCollection
            $('#tour-page-2').removeClass('tour-panel');
            $('#tour-page-3').removeClass('tour-panel');
            var $tourPanel = $('.tour-panel');

            //loop over every panel in the index.html file starting at the 2nd one
            for(var panelIndex=2; panelIndex<$tourPanel.length; panelIndex++) {
                var found = false;

                //loop over every page name and check if there is a match
                for(var modelIndex=0; modelIndex<collection.models.length; modelIndex++) {
                    if($tourPanel.eq(panelIndex).attr('data-featurename') == collection.models[modelIndex].id) {
                        found = true;
                    } else if ($tourPanel.eq(panelIndex).attr('data-featurename') == "accountFunding" && collection.models[modelIndex].id == "funding") {
                        found = true;
                    } else if ($tourPanel.eq(panelIndex).attr('data-featurename') == "order" && collection.models[modelIndex].id == "orderTour") {
                        found = true;
                    } else if ($tourPanel.eq(panelIndex).attr('data-featurename') == "quickPay" && collection.models[modelIndex].id == "quickPayTour") {
                        found = true;
                     } else if ($tourPanel.eq(panelIndex).attr('data-featurename') == "healthyIndicatorTour") {
                        if(qcssapp.Functions.checkServerVersion(3, 0, 20)) {
                            if(qcssapp.usingBranding) {
                                var container = $('.tour-healthy-icon-container');
                                var iconCount = container.children('img').length || $('[data-tourname="healthyIndicatorTour"]').children('.tour-featured-container').children('img').length;
                                if(iconCount > 0) {
                                    found = true;
                                }
                            }
                        }
                    }

                    if(found) {
                        $tourPanel.eq(panelIndex).removeClass('disabled');
                        $('.tour-navigation-item').eq(panelIndex).removeClass('hidden');
                        break;
                    }
                }

                //if there is no match then set the panel to disabled so it won't show in the tour
                if(!found) {
                    $tourPanel.eq(panelIndex).addClass('disabled');
                    $('.tour-navigation-item').eq(panelIndex).addClass('hidden');
                }
            }
        }
    });

    //******* NAVIGATION EVENT HANDLER *******/

    //logout event
    $(document).on('click', '#nav-logout', function(e) {
        if ( !qcssapp.ssoLogin ) {
            if( qcssapp.Functions.fingerprintEnabled() ) {  //if fingerprint authentication is ON we're not expiring the DSKey unless they turn off Enable Fingerprint on Login page
                qcssapp.Functions.resetOnLogout();
            } else {
                qcssapp.Functions.logOut();
            }
        }
    });

    //click #main list item link
    $(document).on('click', '.navMenuLink', function(e) {
        var $this = $(this);
        var $main = $('#main');
        var thisID = $this.find('a').attr('id');

        //page resets
        qcssapp.Functions.resetErrorMsgs();
        qcssapp.Functions.deleteGeneratedContent();

        var selectedLocationMdl = new qcssapp.Models.UserLocationModel();
        selectedLocationMdl.fetch();
        var selectedLocation = selectedLocationMdl.get('hasSelectedLocation');

        //TODO: handle logout functionality in logout callback
        var validServerVersion = qcssapp.Functions.checkServerVersion(4,1,5);
        var shouldShowLocationSelect = false; /* TODO: remove hardcoded false here and use the below assignment to determine true/false
        (
            validServerVersion &&
            qcssapp.locationFilterSettings.myQCAllowOnlineOrderLocation &&
            qcssapp.locationFilterSettings.myQCPromptBeforeOnlineOrder &&
            qcssapp.accountModel.get('orgLevelPrimaryData').orgLevelPrimaryId == "0" &&
            !selectedLocation
        )

        ||

        (
            validServerVersion &&
            qcssapp.locationFilterSettings.myQCAllowOnlineOrderLocation &&
            !qcssapp.locationFilterSettings.myQCAllowOnlineOrderLocationSkip &&
            qcssapp.accountModel.get('orgLevelPrimaryData').orgLevelPrimaryId == "0"
        ); */

        // need to show
        //display mask
        if ( thisID && thisID.length ) {
            if(!shouldShowLocationSelect){
                qcssapp.Functions.showMask(true);
            }
        }

        if ( thisID == "nav-order" ) {
            //TODO: unnecessary off call due to not cleaning up views properly
            $('#show-order').off();
            qcssapp.Functions.checkAccountStatus(function(employeeStatus) {
                if ( employeeStatus == "F" ) {
                    qcssapp.Functions.throwFatalError('Account is frozen. You must un-freeze your account to place orders.', 'main');
                    return;
                }

                // if they have to pick a location and there isn't one set for the user and they haven't seen this yet
                // Need to set the orgLevel in th
                if(shouldShowLocationSelect){
                    var successCallback = function(){
                        new qcssapp.Views.OrderStoreView();
                        qcssapp.Router.navigate('show-order', {trigger: true});
                    };

                    qcssapp.Functions.showLocationSelector(successCallback);

                    // record that the user has seen the option to select a location
                    selectedLocationMdl.set('hasSelectedLocation', true);
                    selectedLocationMdl.save();
                    return;
                }


                new qcssapp.Views.OrderStoreView();
                qcssapp.Router.navigate('show-order', {trigger: true});

            }, true, true);
        } else if ( thisID == "nav-about" ) {
            qcssapp.Router.navigate('tour-page-0', {trigger: true});
            resetTourView();
        } else if ( thisID == "nav-purchases" ) {
            //not all views use the controller, need to destroy transaction view. if error occurs and resetOnLogout is called it won't destroy the view
            qcssapp.Controller.destroyViews('TransactionView');
            qcssapp.Controller.constructedViews = [];

            var purchasesControllerModel = new qcssapp.Models.Controller({
                id: 'TransactionView',
                route: 'show-purchases',
                destructible: true,
                destroyOn: ['NavView']
            });

            qcssapp.Controller.activateView(purchasesControllerModel);
        } else if ( thisID == "nav-deductions" ) {
            new qcssapp.Views.DeductionView();
            qcssapp.Router.navigate('show-deductions', {trigger: true});
        } else if ( thisID == "nav-funding" ) {
            new qcssapp.Views.FundingView();
            qcssapp.Router.navigate('show-funding', {trigger: true});
        } else if ( thisID == "nav-balances" ) {
            new qcssapp.Views.BalanceView();
            qcssapp.Router.navigate('show-balances', {trigger: true});
        } else if ( thisID == "nav-spendinglimits" ) {
            if(qcssapp.Functions.checkServerVersion(2,0)) {
                qcssapp.Router.navigate('show-accountSettings', {trigger: true});
                qcssapp.Functions.checkAccountStatus(function() {
                    new qcssapp.Views.AccountSettingsView();
                }, true);
            } else {
                qcssapp.Router.navigate('show-spendinglimits', {trigger: true});
                qcssapp.Functions.checkAccountStatus(function() {
                    new qcssapp.Views.spendingProfileSelectView();
                }, true);
            }
        } else if ( thisID == "nav-freeze" ) {
            //TODO: should be checking nav's featureCollection
            if ( $main.attr("data-accountfreeze") == "no" ) {
                qcssapp.Functions.hideMask();
                return false;
            } else {
                //TODO: checkAccountStatus should take callback
                qcssapp.Functions.checkAccountStatus();
                determineFreezeBtnColor();
                qcssapp.Router.navigate('show-freeze', {trigger: true});
                qcssapp.Functions.hideMask();
            }
        } else if ( thisID == "nav-accountFunding" ) {
            new qcssapp.Views.AccountFundingView();
            qcssapp.Router.navigate('account-funding', {trigger: true});
        } else if ( thisID == "nav-rewards" ) {
            //not all views use the controller, need to destroy rewards view. if error occurs and resetOnLogout is called it won't destroy the view
            qcssapp.Controller.destroyViews('RewardsProgramsView');
            qcssapp.Controller.constructedViews = [];

            var rewardsControllerModel = new qcssapp.Models.Controller({
                id: 'RewardsProgramsView',
                route: 'show-rewards',
                destructible: true,
                destroyOn: ['NavView']
            });

            qcssapp.Controller.activateView(rewardsControllerModel);
        } else if ( thisID == "nav-quickPay" ) {
            if(qcssapp.Functions.checkServerVersion(2,1) && qcssapp.accountModel.get('qrCodeLength') && qcssapp.accountModel.get('qrCodeLength') > 0 ) {
                qcssapp.Functions.generateQRCode();
                return;
            }

            qcssapp.Functions.showMask();
            var $quickPayCodeContainer = $('#quickPay-code-container');
            if ( $quickPayCodeContainer.children().length == 0 ) {
                var badgeText = qcssapp.accountModel.get('badgeNumber');

                //strip the badge prefix
                if ( qcssapp.accountModel.get('badgeNumPrefix') ) {
                    var prefix = qcssapp.accountModel.get('badgeNumPrefix');
                    if ( prefix == badgeText.substr(0, prefix.length) ) {
                        badgeText = badgeText.substr(prefix.length, 999);
                    }
                }

                //stripe the badge suffix
                if ( qcssapp.accountModel.get('badgeNumSuffix') ) {
                    var suffix = qcssapp.accountModel.get('badgeNumSuffix');
                    if ( suffix == badgeText.substr(badgeText.length - suffix.length, 999) ) {
                        badgeText = badgeText.substr(0, badgeText.length - suffix.length);
                    }
                }

                var el = kjua({text:badgeText});
                $quickPayCodeContainer.html(el);
            }

            qcssapp.Router.navigate('quickpay');
            qcssapp.Functions.executeStandardRoute('#show-quickPay');

            qcssapp.Functions.setBrightness( 1 );

            qcssapp.Functions.hideMask();
        } else if ( thisID == "nav-selectAccount" ) {
            qcssapp.verifyFingerprintFromLogin = true;
            var options = {'autoSelect': true};
            qcssapp.Functions.checkAccountStatus(function() {
                new qcssapp.Views.ManageAccountView(options);
                qcssapp.Router.navigate('manage-account', {trigger:true});
            }, true);
        } else if (thisID == "nav-faq") {
            var faqOptions = {'loginFAQ': false};
            new qcssapp.Views.FAQ(faqOptions);
            qcssapp.Router.navigate('faq', {trigger: true});
        } else if (thisID == "nav-feedback") {
            goToFeedback();
        }

        //TODO: shouldnt need this - issue with page transitions
        if ( thisID && thisID.length && thisID != "nav-logout" && !shouldShowLocationSelect) {
            $main.removeClass('active');
        }
        return false;
    });

    //******* NAVIGATION EVENT HANDLER FUNCTIONS *******/

    function determineFreezeBtnColor() {
        var $freezeBtn =  $('#freeze-btn');
        if($('.freeze-content.freeze-btn-text').text() == "Unfreeze Account") {
            $freezeBtn.removeClass('primary-background-color font-color-primary primary-gradient-color primary-border-color');
            $freezeBtn.addClass('secondary-background-color font-color-secondary secondary-gradient-color secondary-border-color');
        } else {
            $freezeBtn.removeClass('secondary-background-color font-color-secondary secondary-gradient-color secondary-border-color');
            $freezeBtn.addClass('primary-background-color font-color-primary primary-gradient-color primary-border-color');
        }

        if(!qcssapp.usingBranding) {
            $('.freeze-btn-icon-container').find('.svg').hide()
        }
    }

    function resetTourView () {
        $('.tour-navigation-item').each(function() {
            $(this).removeClass('current');
        });
        $('.tour-navigation-item').eq(0).addClass('current');
        $('.tour-navigation').css({bottom: '0px'});
        $('.arrow-left').addClass('hidden');
        $('.arrow-right').removeClass('hidden');
    }

    // Open the Leave Feedback page
    function goToFeedback() {
        // Get the URL and redirect message
        var endPoint = qcssapp.Location + "/api/myqc/feedbackSettings";
        var showError = false;
        qcssapp.Functions.callAPI(endPoint, "GET", "", "",
            function(response) {
                if (!response || !response.url || !response.message) {
                    showError = true;
                    return;
                }

                var feedbackURL = qcssapp.Functions.htmlDecode(response.url);
                var feedbackMessage = qcssapp.Functions.htmlDecode(response.message);
                var feedbackTitle = $("#nav-feedback").text().trim();

                var unsafeUrlParts = feedbackURL.split("://");
                if(unsafeUrlParts.length === 1) {
                    unsafeUrlParts.splice(0,0,"https");
                } else if (unsafeUrlParts[0] !== "https") {
                    unsafeUrlParts[0] = "https"
                }
                feedbackURL = unsafeUrlParts.join("://");

                // Display a popup message with options to cancel or continue
                var continueFn = function() {
                    // Open the URL
                    if (qcssapp.onPhoneApp) {
                        cordova.InAppBrowser.open(feedbackURL, "_system", "location=yes");
                    } else {
                        window.open(feedbackURL);
                    }
                };
                qcssapp.Functions.displayPopup(feedbackMessage, feedbackTitle, "Cancel", "", "", "Continue", continueFn);
            },
            function() {
                showError = true;
            },
            function() {
                $("#main").addClass("active");
                if (showError) {
                    qcssapp.Functions.displayError("Could not load the feedback settings.");
                }
            },
            "", "", true
        );
    }

})(qcssapp, jQuery);
