;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ProductDetailView - View for displaying product details
     */
    qcssapp.Views.ProductDetailView = Backbone.View.extend({
        internalName: "Product Detail View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#product-detail-page', //references existing HTML element for order review - can be referenced as this.$el throughout the view

        template: _.template( $('#product-detail-template').html() ),

        events: {
            'click .product-detail_close': 'closeView',
            'click .product-detail_nutritional-container': 'expandNutrition',
            'click .product-detail_more-btn, .product-detail_less-btn': 'expandDescription',
            'click .product-detail_quantity-decrease': 'decrementQuantity',
            'click .product-detail_quantity-increase': 'incrementQuantity',
            'click .product-detail_favorite': 'favoriteProduct',
            'click .product-detail_container': 'pageClick',
            'click .product-detail_button': 'submitProduct',
            'click .product-detail_swap-button': 'swapProduct',
            'click .product-detail_remove-button': 'removeProduct',
            'change .product-detail_quantity': 'validateQuantity'
        },

        initialize: function ( productModel, closeNavigationPath, editing ) {

            // Initialize variables
            this.model = productModel || {};
            this.closeNavigationPath = closeNavigationPath || '';
            this.editing = editing || false;
            this.beginningQuantity = this.model.get('quantity');

            this.validModifiers = true;
            this.scaleUsed = this.model.get('scaleUsed');
            this.enableFavorites = qcssapp.CurrentOrder.storeModel && typeof qcssapp.CurrentOrder.storeModel.get('enableFavorites') !== 'undefined' ? qcssapp.CurrentOrder.storeModel.get('enableFavorites') : false;
            this.nutritionArray = [];
            this.potentialFavorites = new qcssapp.Collections.ProductFavorites();
            this.potentialFavorites.reset();
            this.productFavoriteID = null;
            this.prepOptionCollection = new qcssapp.Collections.PrepOptions();
            this.modifierMenuCollection = new qcssapp.Collections.ModifierMenus();
            this.storeDisclaimerCollection = new qcssapp.Collections.Disclaimers();
            this.productDisclaimerCollection = new qcssapp.Collections.Disclaimers();
            this.usingIE = qcssapp.Functions.internetExplorer();
            this.modifierLoadComplete = false;
            qcssapp.selectedModCollection = new qcssapp.Collections.Modifiers();
            qcssapp.selectedModCollection.reset();

            if(qcssapp.Functions.checkServerVersion(4,1,10)){
                this.storeDisclaimerCollection = new qcssapp.Collections.Disclaimers(qcssapp.CurrentOrder.storeModel.get('disclaimers'), {parse:true});
                this.productDisclaimerCollection = new qcssapp.Collections.Disclaimers(this.model.get('disclaimers'), {parse:true});
            }

            this.render();

            _.bindAll(this, "checkScroll", "submitProduct", "swapProduct");

            // Call checkScroll when we scroll on the product detail container
            this.$container.on('scroll',this.checkScroll);

            return this;
        },

        render: function() {

            // Build the variables that will be passed into the template
            var options = this.determineProductTemplate();

            this.$el.html( this.template( options ) );

            this.$el.addClass('hide-page').attr('data-id', this.model.get('id'));
            this.$el.removeClass('active-mod-set');

            if(this.usingIE){
                this.$el.parent().addClass('ie-product-slide');
                this.$el.parent().addClass('ie-product-detail-parent-overlay');
            }

            // Initialize elements
            this.$name = this.$el.find('.product-detail_name');
            this.$image = this.$el.find('.product-detail_image');
            this.$btnPrice = this.$el.find('.product-detail_button-price');
            this.$quantity = this.$el.find('.product-detail_quantity');
            this.$description = this.$el.find('.product-detail_description');
            this.$favorite = this.$el.find('.product-detail_favorite');
            this.$tareInfo = this.$el.find('.product-detail_tare-info');
            this.$grossWeightInfo = this.$el.find('.product-detail_gross-weight-info');
            this.$container = this.$el.find('.product-detail_container');
            this.$infoContainer = this.$el.find('.product-detail_info-container');
            this.$nameContainer = this.$el.find('.product-detail_name-container');
            this.$prepOptionMenuList = this.$el.find('.product-detail_prep-option-menu-list');
            this.$modifierMenuList = this.$el.find('.product-detail_modifier-menu-list');
            this.$nutritionPreview = this.$el.find('.product-detail_nutrition-preview');
            this.$nutritionList = this.$el.find('.product-detail_nutrition-list');
            this.$nutritionContainer = this.$el.find('.product-detail_nutritional-container');
            this.$iconContainer = this.$el.find('.product-detail_icon-container');
            this.$buttonContainer = this.$el.find('.product-detail_button-container');
            this.$disclaimerList = this.$el.find('.product-detail_disclaimer-list');

            this.setImage();
            this.setIcons();
            this.setNutrition();
            this.setPrepOption();
            this.setModifiers();
            this.setFavorite();
            this.updateProductTotal();
            this.setDisclaimers();
            this.hideRemoveButtonIfInComboView();

            this.slideUp();

            this.checkIfOpenPrice();

            $("#product-detail-view").off().on('click', this.checkForClickOutsideModal);
        },

        checkForClickOutsideModal(e) {
            var container = $("#product-detail-page");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.unbind( 'click',e.target);
                $('.product-detail_close').click();
            }
        },

        // Builds the variables that will be passed into the product detail template
        determineProductTemplate: function() {
            // Determine product price text
            var priceText = qcssapp.Functions.formatPrice(this.model.get('price'));
            this.model.set('priceText', priceText);

            // Determine product quantity text
            var quantity = this.model.get('quantity');
            quantity = quantity == '' || Number(quantity) <= 0 ? 1 : quantity;
            if(this.scaleUsed) {
                quantity = Number(quantity).toFixed(2);
            }

            // Determine nutrition preview
            var nutritionPreview = '';
            var nutritionCount = 0;

            this.nutritionArray = qcssapp.Functions.buildNutritionModels(this.model);

            $.each(this.nutritionArray, function(index, nutritionModel) {
                if(nutritionModel != null) {
                    nutritionPreview = nutritionModel.get('value') + ' ' + nutritionModel.get('shortName');
                    return false;
                }

                return true;
            });

            // Count how many nutritional infos are found
            $.each(this.nutritionArray, function(index, nutritionModel) {
                if(nutritionModel != null) {
                    nutritionCount++;
                }
            });

            // If you're on a Combo Detail Keypad or you clicked on the Swap button on a product in a Combo
            var comboInProgress = qcssapp.CurrentCombo.id != "" || !$.isEmptyObject(qcssapp.ProductInProgress.substituteProduct);
            var productInCombo = this.model.get('comboDetailId') > 0;

            var upchargeOnProduct = "";

            // If this product is part of a combo determine the upcharge amount
            if(comboInProgress) {
                var basePrice = this.determineBasePrice();
                var upcharge = Number(Number(this.model.get('originalPrice')) - basePrice);
                if(upcharge > 0) {
                    upchargeOnProduct = '(' + qcssapp.Functions.formatPrice(upcharge, '+') + ')';
                } else if(upcharge < 0) {
                    upcharge = upcharge * -1;
                    upchargeOnProduct = '(' + qcssapp.Functions.formatPrice(upcharge, '-') + ')';
                }
            }

            this.model.set("grossWeightInfo","");
            this.model.set("tareInfo","");

            // If the product has a Fixed Tare, show the Tare Name and Weight and the Gross Weight next to the quantity
            if(typeof this.model.get("fixedTareWeight") !== 'undefined' && this.model.get("fixedTareWeight") != "" && typeof this.model.get("tareName") !== 'undefined' && this.model.get("tareName") != "" ) {
                this.model.set("grossWeightInfo","Gross Weight: " +(Number(this.model.get("quantity")) + Number(this.model.get("fixedTareWeight")) ).toFixed(2)+" lbs");
                this.model.set("tareInfo",this.model.get("tareName") +": "+ Number(this.model.get("fixedTareWeight")).toFixed(2) +" lbs");
            }

            return {
                name: this.model.get('name'),
                description: this.model.get('description'),
                quantity: quantity,
                priceText: this.model.get('priceText'),
                nutritionPreview: nutritionPreview,
                buttonText: this.editing || productInCombo ? "Update" : comboInProgress ? "Add to Combo" : "Add to Order",
                upcharge: upchargeOnProduct,
                tareInfo: this.model.get('tareInfo'),
                grossWeightInfo: this.model.get('grossWeightInfo'),
                hasTareInfo: this.model.get('tareInfo') ? 'tare-info' : '',
                showDescription: this.model.get('description') && this.model.get('description').trim().length ? '' : 'hidden',
                showImage: this.model.get('image').length ? '' : 'hidden',
                showNutrition: nutritionCount > 0 ? '' : 'hidden',
                showComboDesc: upchargeOnProduct.length ? '' : 'hidden',
                showSwap: productInCombo ? '' : 'hidden',  // Show Swap button if product in combo
                showPrice: !comboInProgress && !productInCombo ? '' : 'hidden', // Hide price on button is product in combo
                showQuantity: !comboInProgress && !productInCombo ? '' : 'hidden', // Hide quantity if product in combo
                showRemove: this.editing ? '' : 'hidden', // Show remove button if editing
                showTare: this.model.get('tareInfo') ? '' : 'hidden', // Show tare info
                showDisclaimers: this.productDisclaimerCollection.length > 0 || this.storeDisclaimerCollection.length > 0 ? '' : 'hidden' // Show disclaimer info
            };
        },

        // Set the image
        setImage: function() {
            // If there is no image hide the image container
            if ((!this.model.get('image') && !this.model.get('productImage')) || !qcssapp.Functions.checkServerVersion(1,5)) {
                this.$infoContainer.addClass('no-image-product');
                return;
            }

            // D-4273: show product image "productImage" or button image "image" if none is set
            var image = this.model.get('productImage') || this.model.get('image');

            // On the Browser, get the server name from the qcBaseURL
            var imagePath = qcssapp.qcBaseURL + '/webimages/'+ image;
            qcssapp.onPhoneApp = (typeof window.cordova !== "undefined");

            // On the Mobile Devices, get the path from the Location's host name
            if ( qcssapp.onPhoneApp ) {   // Cordova API detected
                var hostName = qcssapp.Functions.get_hostname(qcssapp.Location);
                imagePath = hostName + '/webimages/'+ image;
            }

            // Set image background size based on 'Shrink Image to Fit' setting
            var backgroundImageSize = 'cover';
            if(this.model.get('shrinkImageToFit')) {
                backgroundImageSize = 'contain';
            }

            // Set the image
            this.$image.attr('data-bg', imagePath);
            this.$image.addClass('lazyload');
            this.$image.css('background-size', backgroundImageSize);

            // on ios app and safari, we need to apply special styling to the view or the image will overflow
            if(navigator.userAgent.toLowerCase().indexOf('safari') > -1 || window.cordova && window.cordova.platformId && window.cordova.platformId.toLowerCase() === 'ios') {
                // shamelessly stolen from https://gist.github.com/ayamflow/b602ab436ac9f05660d9c15190f4fd7b
                this.$el.css('-webkit-mask-image', 'webkit-radial-gradient(white, black)');
            }

            this.$image.on('lazyloaded', function(e) {
                var $image = $(e.target);

                if(qcssapp.DebugMode) {
                    var name = this.model.get('name');
                    console.log('error loading: ' + name);
                }

                // handle broken images
                if($image.hasClass('broken-image')) {
                    $image.text('Sorry this image is broken');
                }

            }.bind(this));
        },

        // Set the healthy indicator icons
        setIcons: function() {
            if(!qcssapp.Functions.checkServerVersion(3,0)) {
                return;
            }

            var healthyFlag = false;

            // Check for each healthy indicator category and add that image icon to the container
            if(this.model.get('healthy')){
                healthyFlag = true;
                this.$iconContainer.append("<img src=\""+qcssapp.healthyIndicator.wellness+"\"><span>"+qcssapp.healthyIndicator.wellnessLabel+"</span>");
            }
            if(this.model.get('vegetarian')){
                healthyFlag = true;
                this.$iconContainer.append("<img src=\""+qcssapp.healthyIndicator.vegetarian+"\"><span>"+qcssapp.healthyIndicator.vegetarianLabel+"</span>");
            }
            if(this.model.get('vegan')){
                healthyFlag = true;
                this.$iconContainer.append("<img src=\""+qcssapp.healthyIndicator.vegan+"\"><span>"+qcssapp.healthyIndicator.veganLabel+"</span>");
            }
            if(this.model.get('glutenFree')){
                healthyFlag = true;
                this.$iconContainer.append("<img src=\""+qcssapp.healthyIndicator.glutenFree+"\"><span>"+qcssapp.healthyIndicator.glutenFreeLabel+"</span>");
            }

            // If there is at least one icon from the container
            if(healthyFlag) {
                this.$iconContainer.removeClass('hidden');
            }
        },

        // Set the favorite heart icon
        setFavorite: function() {
            // If Enable Favorites is OFF or the product was scanned but the product isn't on a keypad in the store, don't show heart icon
            if( !qcssapp.Functions.checkServerVersion(1,6) || !this.enableFavorites|| !this.model.get('productInStore') ) {
                this.$favorite.hide();
                return;
            }

            // If this is not set then don't check if a favorite
            if( !qcssapp.ProductInProgress.favorite ) {
                return;
            }

            // For older server versions, get the send in the product's current selections (modifiers and prep options) to check if it is a favorite
            if( !qcssapp.Functions.checkServerVersion(4,1,10) ) {
                qcssapp.Functions.getFavoriteDetails(this);
                return;
            }

            var endPoint = qcssapp.Location + '/api/favorite/product/' + this.model.get('productID');

            // Otherwise get all the potential favorites for this product (pull in all the active/inactive favorites for the product with different modifier/prep option selections)
            qcssapp.Functions.callAPI(endPoint, 'GET', '', '',
                function(response) {
                    if(response) {

                        // Set all the potential favorites
                        this.potentialFavorites.set(response);

                        // Check if the product with the selected modifiers/prep options is in the potential favorites
                        this.checkProductFavorite();
                    }
                }.bind(this),
                function() {
                    qcssapp.Functions.displayPopup('An error occurred when attempting to check if this product is a favorite', 'Error', 'OK', '', '', '', '', '', this.$el);
                }, '', '', '', true, true

            );
        },

        // Checks if the product is a favorite
        checkProductFavorite: function() {
            // For older server versions we'll need to check against the server
            if( !qcssapp.Functions.checkServerVersion(4,1,10) ) {
                qcssapp.Functions.checkFavoriteDetails(this);
                return;
            }

            // Is the product with the currently selected modifiers and prep options a favorite
            this.isProductAndModsFavorite();
        },

        // Check if the product with the currently selected modifiers and prep options is a favorite
        isProductAndModsFavorite: function() {
            // If there are no potential favorites to check against, return null (for older server version this tells us we need to check against the server)
            if(this.potentialFavorites.length == 0) {
                this.$favorite.removeClass('favorite').attr('src', 'images/icon-favorites-unchecked.svg');
                return null;
            }

            // Add the modifiers in selectedModCollection to the product's modifier property to properly check if the product is a favorite
            this.addModifiersToOrder();

            var productFavoriteModel = null;

            // If the product doesn't have modifiers or a prep option, look at the last potential favorite model, which would be the most recently favorited
            if( (!this.model.get('modifierMenus') || this.model.get('modifierMenus').length == 0) && this.prepOptionCollection.length == 0) {
                productFavoriteModel = this.potentialFavorites.models[this.potentialFavorites.length - 1];
            }

            // Otherwise loop through all the potential favorites
            this.potentialFavorites.each(function( favorite ) {

                // If the productFavoriteModel is not null, or the productID's don't match, or the modifier length's don't match, or the prepOption's don't match - just return
                if(productFavoriteModel != null || (favorite.get('productID') != this.model.get('productID'))
                  ||
                    (favorite.get('modifiers').length != this.model.get('modifiers').length) ||
                    !(
                      (
                       // if there is a prep option set for the product, make sure they match
                       favorite.get('prepOptionSetID') && this.model.get('prepOptionSetID') &&
                       favorite.get('prepOptionSetID') == this.model.get('prepOptionSetID') &&
                       favorite.get('prepOption') && this.model.get('prepOption') &&
                       favorite.get('prepOption').id == this.model.get('prepOption').get('id')
                      )

                    ||

                    (
                        // if the product doesn't have a prep option set configured, it's also valid.
                        !favorite.get('prepOptionSetID') &&
                        !this.model.get('prepOptionSetID') &&
                        !favorite.get('prepOption') &&
                        !this.model.get('prepOption')
                    )

                    )

                ) {
                    return false;
                }

                var foundAllMods = true;

                // Loop over the current product's modifiers
                $.each(this.model.get('modifiers'), function(index, mod) {
                    var productModID = mod.id;
                    var productModPrep = mod.prepOption;
                    var validMod = false;

                    // Loop over the modifier's on the favorite
                    $.each(favorite.get('modifiers'), function(index, favoriteMod) {
                        if(productModID != favoriteMod.id) {
                            return;
                        }

                        // If the modifier has the same ID as the favorite modifier and they have the same prep options or both don't have prep options - found valid modifier
                        if( (productModPrep == null && favoriteMod.prepOption == null) || (productModPrep != null && favoriteMod.prepOption != null && productModPrep.get('id') == favoriteMod.prepOption.id)) {
                            validMod = true;
                            return false;
                        }

                    }.bind(this));

                    // If any of the modifiers aren't valid, then this favorite isn't valid
                    if(!validMod) {
                        foundAllMods = false;
                        return false;
                    }

                }.bind(this));

                // If we found all matching modifiers, set the productFavoriteModel to the found favorite model
                if(foundAllMods) {
                    productFavoriteModel = favorite;
                }

                return false;

            }, this);

            // If a product favorite model wasn't found or it was but it was inactive, show emtpy favorite heart
            if(productFavoriteModel == null || !productFavoriteModel.get('active')) {
                this.$favorite.removeClass('favorite').attr('src', 'images/icon-favorites-unchecked.svg');
                this.productFavoriteID = null;
                return productFavoriteModel;
            }

            // Otherwise we found an active favorite model, fill favorite heart and set productFavoriteID to indicate the product is favorited
            this.$favorite.addClass('favorite').attr('src', 'images/icon-favorites.svg');
            this.productFavoriteID = productFavoriteModel.get('id');
            return productFavoriteModel;
        },

        // Event handler function for clicking the favorite heart icon
        favoriteProduct: function(e) {
            e.stopPropagation();

            // If the productFavoriteID has a value then the product is favorited so we need to deactivate the favorite
            if(this.productFavoriteID != null) {

                // Show the empty favorite heart icon, deactivate the favorite model from the potential favorites list, set the productFavoriteID to null to indicate this product isn't an active favorite
                var callbackRemoveFn = function() {
                    this.$favorite.addClass('favorite').attr('src', 'images/icon-favorites-unchecked.svg');

                    this.potentialFavorites.each(function( favorite ) {
                        if(favorite.get('orderingFavoriteID') == this.productFavoriteID) {
                            favorite.set('active', false);
                        }
                    }, this);

                    this.productFavoriteID = null;
                }.bind(this);

                // Set the OrderingFavorite record has inactive in the database
                qcssapp.Functions.removeFavorite(this.productFavoriteID, callbackRemoveFn, '');

                // control the heart appearing or dissappearing on the previous menu
                this.adjustItemLineView();

                return;
            }

            // If the user hasn't selected all the required modifiers, don't let them favorite the product
            if( !this.validModifiers ) {
                qcssapp.Functions.displayPopup('Please select the required modifiers before attempting to favorite the product', 'Unable to Favorite', 'OK', '', '', '', '', '', this.$el);
                return
            }

            // Add the modifiers in selectedModCollection to the product's modifier property to properly check if the product is a favorite
            this.addModifiersToOrder();

            // Check if there is a favorite model with the selected modifiers and prep options
            var productFavoriteModel = this.isProductAndModsFavorite();
            var favoriteModel = productFavoriteModel;

            // If there isn't a potential favorite for this product/modifier/prep option combination
            if(productFavoriteModel == null) {

                // Create the favoriteModel that will be sent to the back-end
                favoriteModel = qcssapp.Functions.setProductFavoriteModel(this.model);
            }

            // Set active status to true when we create a favorite
            favoriteModel.set('active', true);

            // Fill the favorite heart icon and add the favorite model to the list of potential favorites
            var callbackFn = function(favoriteModel) {
                this.$favorite.addClass('favorite').attr('src', 'images/icon-favorites.svg');
                this.addProductFavoriteModel(favoriteModel);

            }.bind(this);

            // Create favorite or update the active status of an existing favorite
            qcssapp.Functions.createFavorite(favoriteModel, callbackFn);

            // control the heart appearing or dissappearing on the previous menu
            this.adjustItemLineView();
        },

        // Add the favorite model to the list of potential favorites if it doesn't already exist
        addProductFavoriteModel: function(favorite) {
            // If there are no potential favorites, add model and set id to 0
            if(this.potentialFavorites.length == 0) {
                favorite.id = this.potentialFavorites.length;
                this.potentialFavorites.add(favorite);

            } else {
                var productFavModel = favorite;

                // Create ProductFavorite model for older server versions
                if(!favorite.attributes) {
                    productFavModel = new qcssapp.Models.ProductFavorite(favorite);
                }

                var isSame = false;

                // Check if this product is an existing potential favorite with a different active status or if it's a new combination
                this.potentialFavorites.each(function( fav ) {
                    productFavModel.set('id', fav.get('id'));

                    var activeStatus = productFavModel.get('active');
                    var originalActive = fav.get('active');
                    fav.set('active', activeStatus);

                    if( fav.equalTo(productFavModel)) {
                        isSame = true;
                        return false;
                    } else {
                        fav.set('active', originalActive);
                    }

                }, this);

                // If this favorite doesn't exist add it to the potentialFavorites
                if(!isSame) {
                    favorite.id = this.potentialFavorites.length;
                    this.potentialFavorites.add(favorite);
                }
            }

            // If the favorite was activated, update the productFavoriteID
            if(favorite.attributes) {
                this.productFavoriteID = favorite.get('active') ? favorite.get('orderingFavoriteID') : null;
            } else {
                this.productFavoriteID = favorite.active ? favorite.orderingFavoriteID : null;
            }
        },

        // Set the disclaimers
        setDisclaimers: function(){
            if(!qcssapp.Functions.checkServerVersion(4,1,10)){
                return;
            }

            // Render product disclaimers first
            if(this.productDisclaimerCollection.length){
                this.productDisclaimerCollection.each( function(disclaimerModel){
                    this.renderDisclaimerLine(disclaimerModel);
                }, this);
            }

            // Render store disclaimers after
            if(this.storeDisclaimerCollection.length){
                this.storeDisclaimerCollection.each( function(disclaimerModel){
                    this.renderDisclaimerLine(disclaimerModel);
                }, this);
            }
        },

        // Create disclaimer line element
        renderDisclaimerLine: function(disclaimerModel){
            var disclaimerLineView = new qcssapp.Views.DisclaimerLineView({
                model:disclaimerModel
            });

            this.$disclaimerList.append(disclaimerLineView.render().el);
        },

        // Set the nutrition information
        setNutrition: function() {

            // Loop over each nutrition category
            for( var x = 0; x < this.nutritionArray.length; x++) {
                var nutritionModel = this.nutritionArray[x];

                // If there is nutrition information, create a nutrition line
                if(nutritionModel != null) {
                    var nutritionLineView = new qcssapp.Views.ProductNutritionLineView({
                        model:nutritionModel
                    });

                    this.$nutritionList.append(nutritionLineView.render().el);
                }
            }
        },

        // Set the description
        setDescription: function() {
            this.descLineHeight = 15;

            // first determine the height of one description line
            var descriptionHTML = this.$description.html();
            this.$description.html('.');

            // height varies between browsers and phones
            if(this.$description.height() != 0) {
                this.descLineHeight = this.$description.height();
            }

            // reset element back to original html
            this.$description.html(descriptionHTML);

            // determine the line height of the description
            var descHeight = this.$description.height();
            var descLineCount = descHeight > this.descLineHeight ? Number(descHeight / this.descLineHeight) : 1;

            // if the line height is 4 or less, don't need to truncated
            if(descLineCount <= 4) {
                return;
            }

            // need to truncated description
            var descriptionText = this.$description.text();
            var truncatedDescText = "";

            // loop over the description, subtract a character each time and see what index the text fits on 4 lines
            for( var x=this.$description.text().length; x>0; x-- ) {
                truncatedDescText = this.$description.text();
                truncatedDescText = truncatedDescText.substring(0, x);

                this.$description.text(truncatedDescText);

                var truncatedDescHeight = this.$description.height();
                var truncatedDescLineCount = truncatedDescHeight > this.descLineHeight ? Number(truncatedDescHeight / this.descLineHeight) : 1;

                // if line count is 4 or less then we found the index where the text fits on 4 lines
                if(truncatedDescLineCount <= 4) {
                    x = x-8; // subtract an extra 8 characters for the '... more' text
                    break;
                }
            }

            // get the description substring for the visible and hidden text
            var visibleText = descriptionText.substring(0,x);
            var hiddenText = descriptionText.substring(x, descriptionText.length);

            // append the visible text, then the '... more', then the hidden text
            this.$description.text(visibleText);
            this.$description.append('<span class="product-detail_more-btn">... <span class="product-detail_more">more</span></span>');
            this.$description.append('<span class="product-detail_desc-hidden">'+hiddenText+'</span><span class="product-detail_less-btn">less</span>');
        },

        // Set the prep option for the product
        setPrepOption: function() {
            if(!qcssapp.Functions.isNumeric(this.model.get('prepOptionSetID'))) {
                return;
            }

            // Add margin to bottom of prep option list if there are no modifiers
            if(!this.model.get('modifierMenus') || this.model.get('modifierMenus').length == 0) {
                this.$prepOptionMenuList.addClass('add-prep-margin');
            }

            // Update page background color
            this.$el.addClass('active-mod-set');

            // Add listenTo event for when a prep option is changed
            this.listenTo(this.model, 'change:prepOption', this.updateView);

            var endpoint = qcssapp.Location + '/api/ordering/product/prepOption/' + this.model.get('prepOptionSetID');

            // Get the prep options for the Prep Option Set ID
            qcssapp.Functions.callAPI(endpoint, 'GET', '', '',
                function(response) {
                    if ( response ) {

                        // Set the prep option collection
                        this.prepOptionCollection.set(response, {parse: response});

                        if(this.prepOptionCollection.length == 0) {
                            return;
                        }

                        // Create the Prep Option Menu element, this line view will create each individual prep option
                        var prepMenuView = new qcssapp.Views.PrepOptionMenuView({
                            model:this.model,
                            prepOptionCollection: this.prepOptionCollection
                        });

                        this.$prepOptionMenuList.append(prepMenuView.render().el);
                    }
                }.bind(this),
                '', '', '', '', true, true
            );
        },

        // Set the modifiers
        setModifiers: function() {
            // If there are modifierMenus then modifiers exist
            if(!this.model.get('modifierMenus') || this.model.get('modifierMenus').length == 0) {
                this.modifierLoadComplete = true; //Defect 3949, this needs to be set to true for prep options to have their price updates included in the view
                return;
            }

            // Update page background
            this.$el.addClass('active-mod-set');

            this.modifierMenuCollection.set(this.model.get('modifierMenus'), {parse:this.model.get('modifierMenus')});

            // Create each menu line view
            this.modifierMenuCollection.each(function( modMenu ) {
                this.renderModMenu( modMenu );
            }, this);

            this.modifierLoadComplete = true;

            // Add listenTo events for when a modifier is added, removed or a prep option on the modifier is changed
            this.listenTo(qcssapp.selectedModCollection, 'add', this.updateView);
            this.listenTo(qcssapp.selectedModCollection, 'remove', this.updateView);
            this.listenTo(qcssapp.selectedModCollection, 'change:prepOption', this.updateView);

            this.updateView();
        },

        // Render each modifier menu line view
        renderModMenu: function( modMenu ) {
            // If we're editing the product from the Cart then modifier menu is predefined
            modMenu.predefined = this.editing;

            // If modifiers exist on the product, we must be editing the product - update the modifiers in the modifier menus if they are selected modifiers
            if( (this.model.get('modifiers') != null && this.model.get('modifiers').length > 0) || this.editing) {
                modMenu = qcssapp.Functions.updateSelectedModifiers(this.model, modMenu);
                modMenu.predefined = true;
            }

            // Create the Modifier Menu line view
            var modMenuView = new qcssapp.Views.ModifierMenuView({
                model:modMenu
            });

            this.$modifierMenuList.append(modMenuView.render().el);
        },

        // Event handler when a modifier is added, removed, or a prep option is changed - updates various elements on the page
        updateView: function() {

            // Prevent any changes until the modifier menus are set
            if(!this.modifierLoadComplete){
                return;
            }

            this.updateAddToOrderBtn();
            this.updateProductTotal();
            this.updateNutrition();
            this.checkProductFavorite();
        },

        // Checks if all required modifiers have been selected, updates Add to Order button styles
        updateAddToOrderBtn: function() {
            var modifierMenuCount = this.modifierMenuCollection.length;
            var validModMenu = this.$el.find('.menu-valid').length;

            // If all required modifiers haven't been selected, show button as disabled
            if( validModMenu < modifierMenuCount ) {
                this.$buttonContainer.addClass('unavailable');
                this.validModifiers = false;

            // Otherwise product can be added to the order
            } else {
                this.$buttonContainer.removeClass('unavailable');
                this.validModifiers = true;
            }
        },

        // Updates the price on the Add to Order button by adding all prices on modifiers and prep options
        updateProductTotal: function() {
            var productPrice = Number(this.model.get('originalPrice'));
            var productQuantity = Number(this.model.get('quantity'));
            var productPriceText = "";
            var prepOptionsExistInCurrentServerVersion = qcssapp.Functions.checkServerVersion(4,0,0)

            // Loop over each selected modifier, add the modifier price to the product total
            qcssapp.selectedModCollection.each(function( mod ) {
                var modPrice = mod.get('price');
                productPrice += Number(modPrice);

                //Added in this check here for defect 3961 (prep options didn't exist prior to MyQC server code 4.0 QCWEB 9.3)
                if (prepOptionsExistInCurrentServerVersion ) {
                    // If there is a prep option on the modifier, add it to the total
                    if (mod.get('prepOption') != null) {
                        productPrice += Number(mod.get('prepOption').get('price'));
                    }
                }

            }, this);

            //DEFECT 3961: need to check the server version for this as prepOptions didn't exist in QC until 9.3
            //If the server version isn't high enough, just skip, as there would be no prep options to account for
            if (prepOptionsExistInCurrentServerVersion ) {
                // If there is a prep option on the product, add it to the total
                if (this.model.get('prepOption') != null) {
                    productPrice += Number(this.model.get('prepOption').get('price'));
                }
            }

            // If this is a weighted product, only multiply the quantity by the original price, not the price with modifiers
            if(this.scaleUsed){
                var originalPrice = Number(this.model.get('originalPrice'));
                var additionalCost = Number(productPrice - originalPrice);
                productPriceText = Number(Math.round(((productQuantity * originalPrice) + additionalCost)+"e"+2)+"e-"+2);

            // If not weighted product, multiply quantity by the price
            } else {
                productPriceText = Number(Math.round((productQuantity * productPrice)+"e"+2)+"e-"+2);
            }

            // If there is a fixed tare, update the quantity for the tare and the gross weight
            if(typeof this.model.get("fixedTareWeight") !== 'undefined' && this.model.get("fixedTareWeight") != "" && typeof this.model.get("tareName") !== 'undefined' && this.model.get("tareName") != "" ) {
                this.$grossWeightInfo.html("Gross Weight: " +(Number(this.model.get("quantity")) + Number(this.model.get("fixedTareWeight")) ).toFixed(2)+" lbs");
                this.$tareInfo.html(this.model.get("tareName") +": "+ Number(this.model.get("fixedTareWeight")).toFixed(2) +" lbs");
            }

            // Set the price on the model as the product's total price
            this.model.set('price', productPrice);

            // Format the price in the button
            productPriceText = qcssapp.Functions.formatPrice(productPriceText);

            this.$btnPrice.html(productPriceText);
        },

        // Update the nutrition information totals
        updateNutrition: function() {
            // Get a duplicate of the product's nutrition array - always want a way to to refer to the product's original nutrition totals so we duplicate it instead of changing the original array
            var productNutritionArray = qcssapp.Functions.duplicateArray(this.nutritionArray);

            // Loop over each selected modifier
            qcssapp.selectedModCollection.each(function( mod ) {
                var modNutritionArray = mod.get('nutritionArray');

                // Loop over each nutritional information on the modifier
                for( var x = 0; x < modNutritionArray.length; x++) {
                    var modNutritionModel = modNutritionArray[x];
                    var productNutritionModel = productNutritionArray[x];

                    // If there is no nutrition info for this category, skip it
                    if(modNutritionModel == null) {
                        continue;
                    }

                    // Get the total nutrition amount for this category
                    var productTotalValue = Number(modNutritionModel.get('value'));

                    if(productNutritionModel != null){
                        productTotalValue += Number(productNutritionModel.get('value'));
                    }

                    var shortName = productNutritionModel ? productNutritionModel.get('shortName') : modNutritionModel.get('shortName');


                    var productNutritionInfo = productTotalValue + ' ' + shortName;

                    // Set totals on the new product nutrition model
                    if(productNutritionModel != null) {
                        productNutritionModel.set('value', productTotalValue);
                        productNutritionModel.set('nutritionInfo', productNutritionInfo);
                    }
                    else{// create the new nutrition model based on the modNutritionModel
                        var nutritionObj = {
                            value:  modNutritionModel.get('value'),
                            name: modNutritionModel.get('name'),
                            shortName: modNutritionModel.get('shortName'),
                            measurementLbl: modNutritionModel.get('measurementLbl'),
                            nutritionInfo: modNutritionModel.get('nutritionInfo')
                        };

                        productNutritionArray[x] = new qcssapp.Models.ProductNutritionModel(nutritionObj);
                    }
                }
            }, this);

            // Remove the current nutrition info elements
            this.$nutritionList.children().remove();

            var previewUpdated = false;
            var hasNutrition = false;

            // Loop over each new nutrition information
            for( var x = 0; x < productNutritionArray.length; x++) {
                var nutritionModel = productNutritionArray[x];

                // If there is nutrition information for this category, create nutrition line
                if(nutritionModel != null) {
                    var nutritionLineView = new qcssapp.Views.ProductNutritionLineView({
                        model:nutritionModel
                    });

                    this.$nutritionList.append(nutritionLineView.render().el);

                    // Update the preview for the first category
                    if(!previewUpdated) {
                        this.$nutritionPreview.text(nutritionModel.get('value') + ' ' + nutritionModel.get('shortName'));
                        previewUpdated = true;
                    }

                    hasNutrition = true;
                }
            }

            // If there is nutrition, show nutrition information container
            if(hasNutrition) {
                this.$nutritionContainer.slideDown(300);

            // Otherwise hide it, this may be if the product doesn't have nutrition but the modifiers do and no modifiers are selected
            } else {
                this.$nutritionContainer.slideUp(300);
            }
        },

        // defect 3894 - reject the product should it be open priced without a price
        // when products have an open price and do not have a preset price, the preset price should be used instead
        checkIfOpenPrice: function() {
            if(this.model.priceOpen && !this.model.price) {
                // notify user and return
                $.alert({
                    title: '<h4>Sorry!</h4>',
                    content: "<span style='text-align: center;display: flex;'>This product is not allowed at this time.</span>",
                    type: 'red',
                    buttons: {
                        okay: {
                            text: 'Ok',
                            btnClass: 'primary-background-color font-color-primary primary-gradient-color primary-border-color name-order-popup-button',
                            action: function () {
                                $('.product-detail_close').trigger('click');
                            }
                        }
                    }
                });
            }
        },

        // Event handler when clicking on plus sign to increment the product's quantity
        incrementQuantity: function(e) {
            e.stopPropagation(); // Stop event bubbling

            var quantity = Number(this.$quantity.val());

            // If this is a weighted product, increase quantity by 0.05
            if(this.scaleUsed) {
                quantity = Number(quantity + 0.05).toFixed(2);

            // Otherwise increase quantity by 1
            } else {
                quantity = Number(quantity + 1);
            }

            // Update quantity on product model
            this.$quantity.val(quantity);
            this.model.set('quantity', quantity);

            // Update the product total show on Add to Order button
            this.updateProductTotal();
        },

        // Event handler when clicking on minus sign to decrement the product's quantity
        decrementQuantity: function(e) {
            e.stopPropagation();

            var quantity = Number(this.$quantity.val());

            // If this is a weighted product, decrease quantity by 0.05 - check if it proposed quantity will be below 0 first
            if( this.scaleUsed ) {
                var proposedQuantityScale = Number(quantity - 0.05);
                if(proposedQuantityScale <= 0) {
                    return false;
                } else {
                    quantity = proposedQuantityScale.toFixed(2);
                }

            // Otherwise decrease quantity by 1 - check if it proposed quantity will be below 0 first
            } else {
                var proposedQuantity =  Number(quantity - 1);
                if(proposedQuantity <= 0) {
                    return false;
                } else {
                    quantity = proposedQuantity;
                }
            }

            // Update quantity on product model
            this.$quantity.val(quantity);
            this.model.set('quantity', quantity);

            // Update the product total show on Add to Order button
            this.updateProductTotal();
        },

        // Event handler to validate the quantity if the input quantity field is changed
        validateQuantity: function(e) {
            e.stopPropagation();

            var quantity = this.$quantity.val();

            // Check if the quantity is numeric, if not show error and change to default
            if ( !(qcssapp.Functions.isNumeric( this.$quantity.val() ) && Number(quantity) > 0) ) {
                qcssapp.Functions.displayPopup('Please enter a positive number', 'Invalid Quantity', 'Ok', '', '', '', '', '', this.$el);

                quantity = '1';

                if(this.scaleUsed) {
                    quantity = '1.00';
                }

            // Check if the quantity is an integer if this is not a weighted product
            } else if( !this.scaleUsed && Number(quantity) % 1 != 0) {
                qcssapp.Functions.displayPopup('Please enter a positive integer', 'Invalid Quantity', 'Ok', '', '', '', '', '', this.$el);
                quantity = '1';
            }

            if(this.scaleUsed) {
                quantity = Number(quantity).toFixed(2);
            } else {
                quantity = Number(quantity).toFixed(0);
            }

            this.$quantity.val(quantity);

            // Update quantity on product model
            this.model.set('quantity', quantity);

            // Update the product total show on Add to Order button
            this.updateProductTotal();
        },

        // Event handler when clicking on the nutrition expand arrow
        expandNutrition: function(e) {
            e.stopPropagation();

            // If the container is expanded, collapse it
            if(this.$nutritionContainer.hasClass('expand-nutrition')) {
                this.$nutritionContainer.removeClass('expand-nutrition');
                this.$nutritionList.slideUp(300);
                this.$nutritionPreview.slideDown(300);

            // If the container is collapsed, expand it
            } else {
                this.$nutritionContainer.addClass('expand-nutrition');
                this.$nutritionPreview.slideUp(200);
                this.$nutritionList.slideDown(300);
            }

            setTimeout(function() {
                qcssapp.Functions.checkScrollIndicator($('#product-detail-page').find('.scrollElement'))
            }, 300);
        },

        // Event handler when clicking on the description more button
        expandDescription: function(e) {
            e.stopPropagation();

            var $moreBtn = this.$el.find('.product-detail_more-btn');
            var $lessBtn = this.$el.find('.product-detail_less-btn');
            var $hiddenDesc = this.$el.find('.product-detail_desc-hidden');

            // If the description is expanded, collapse the overflowing description - need to append more button before the hidden overflowing description
            if(this.$description.hasClass('expanded-desc')) {
                this.$description.removeClass('expanded-desc');
                $hiddenDesc.hide();
                $lessBtn.hide();
                $('<span class="product-detail_more-btn">... <span class="product-detail_more">more</span></span>').insertBefore($hiddenDesc);

            // If the description is collapsed, expand it and show the overflowing description
            } else {
                this.$description.addClass('expanded-desc');
                $moreBtn.remove();
                $hiddenDesc.show();
                $lessBtn.show();
            }
        },

        // Event handler when submitting the product
        submitProduct: function(e) {
            e.stopPropagation();

            // If the Add to Order button is disabled, don't do anything until all required modifiers are selected
            if( !this.validModifiers ) {
                return;
            }

            // If the price is less than 0 show an error
            if(this.model.get('price') < 0) {
                qcssapp.Functions.displayPopup('Cannot add a product with a negative price to the cart', 'Invalid Price', 'OK', '', '', '', '', '', this.$el);
                return;
            }

            // Add the selected modifiers to the product model
            this.addModifiersToOrder();

            // If this is a product on a combo detail keypad, add product to the combo
            if(qcssapp.CurrentCombo.id != "") {
                this.addComboDetailProduct();
                return;
            }

            // If this combo product is being swapped for another combo product, substitute the product in the combo
            if( !$.isEmptyObject(qcssapp.ProductInProgress.substituteProduct) ) {
                $.when(qcssapp.Functions.substituteComboInCart(this.model)).done(function(){
                    this.exitView();
                }.bind(this));
                return;
            }

            qcssapp.Functions.showMask(true);

            // If editing the product from the cart, update the order
            if(this.editing) {
                this.updateOrder();

            // Otherwise add the product to the order
            } else {
                this.addToOrder();
            }
        },

        // Add the product to the order
        addToOrder: function() {

            // Checks if there are suggestive selling items we can show
            var callback = function() {
                if( qcssapp.Functions.checkServerVersion(4,0) ) {
                    // If the product was selected from a suggestive keypad, navigate back to that keypad
                    if (this.closeNavigationPath == "suggestive-selling") {
                        qcssapp.Router.navigate('suggestive-selling', {trigger: true});
                        qcssapp.Functions.hideMask();
                        return;
                    }

                    var complementaryKeypad = this.model.get('complementaryKeypadID');
                    var subDeptComplementaryKeypad = this.model.get('subDeptComplementaryKeypadID');

                    // Determine which suggestive keypad to use
                    var suggestiveKeypadID = qcssapp.Functions.getSuggestiveKeypad(complementaryKeypad, subDeptComplementaryKeypad);

                    // If there is a suggestive selling keypad then show after product page
                    if(suggestiveKeypadID != null) {
                        qcssapp.Functions.loadProductSuggestiveKeypad(suggestiveKeypadID, subDeptComplementaryKeypad);
                        return;
                    }
                }

                // If suggestive selling isn't configured on product, navigate to the Cart
                qcssapp.Functions.navigateToCart();

            }.bind(this);

            // After the product has been added to the Cart, close the view and check for combos - uses promises instead of using a callback function
            $.when( qcssapp.Functions.addProductToCart(this.model) ).done(function() {

                // Close the view
                this.exitView();

                // If there is only one product call the callback function
                if( qcssapp.CurrentOrder.products.length <= 1 ) {
                    callback();
                    return;
                }

                // If more than one product then check if there is a combo
                 qcssapp.Functions.checkProductCombos(callback);

            }.bind(this));
        },

        // Adds the selected modifiers to this.model
        addModifiersToOrder: function() {
            if(qcssapp.selectedModCollection.length == 0) {
                return;
            }

            // Reset the modifiers
            this.model.set('modifiers', []);

            // Loop over the selected modidifers and add them to the model
            qcssapp.selectedModCollection.each(function( mod ) {
                var modifiers = this.model.get('modifiers') == null ? [] : this.model.get('modifiers');
                modifiers.push(mod.attributes);

                this.model.set('modifiers', modifiers)

            }, this);
        },

        // Add the product from the Combo Detail keypad to the combo
        addComboDetailProduct: function() {

            // Check if the product price is negative - show error
            if(qcssapp.Functions.checkNegativePricing(this.model)) {
                qcssapp.Functions.displayPopup('Cannot add product to the combo. Product does not have the required pricing.', 'Invalid Price', 'OK', '', '', '', '', '', this.$el);
                return;
            }

            // Check if this is a weighted product - show error
            if(qcssapp.Functions.checkWeightedProduct(this.model)) {
                qcssapp.Functions.displayPopup('Cannot add weighted products to a combo.', 'Invalid Product', 'OK', '', '', '', '', '', this.$el);
                return;
            }

            // Adds or swaps the product in the combo
            this.updateComboDetailProduct();
        },

        hideRemoveButtonIfInComboView: function() {
            var comboInProgress = qcssapp.CurrentCombo.id != "" || !$.isEmptyObject(qcssapp.ProductInProgress.substituteProduct);
            if(comboInProgress){
                this.$el.find('.product-detail_remove-container').hide();
            }
        },

        // Updates the combo detail keypad product
        updateComboDetailProduct: function() {
            // If swapping a product from the Combo View, update the combo details on the product
            if(qcssapp.CurrentCombo.swapIndex != null) {
                var replacementProduct = qcssapp.ComboInProgress[qcssapp.CurrentCombo.swapIndex];
                this.model.set('quantity', this.model.get('quantity'));
                this.model.set('comboTransLineItemId', replacementProduct.get('comboTransLineItemId'));
                qcssapp.CurrentCombo.updatedComboTransLineId = replacementProduct.get('comboTransLineItemId');

                qcssapp.ComboInProgress.splice(qcssapp.CurrentCombo.swapIndex, 1, this.model);
                qcssapp.CurrentCombo.swapIndex = null;

            // Otherwise add product to the combo
            } else {
                qcssapp.ComboInProgress.push(this.model);
            }

            // Close the view
            this.exitView();

            // Load the next combo keypad
            qcssapp.Functions.loadComboKeypad();
        },

        // For editing products already in the cart - updates quantity and/or modifiers
        updateOrder: function() {
            // If the product is on a combo detail keypad, add or swap the product in the combo
            if(qcssapp.CurrentCombo.id != "") {
                this.updateComboDetailProduct();
                return;
            }

            // Get the index of the product in the cart based on a stored id
            var index = qcssapp.Functions.getProductCartIndexByID(qcssapp.SelectedProductID);

            // Update the customizable details on the product
            qcssapp.CurrentOrder.products[index].set('quantity', this.model.get('quantity'));
            qcssapp.CurrentOrder.products[index].set('price', this.model.get('price'));
            qcssapp.CurrentOrder.products[index].set('modifiers', this.model.get('modifiers'));
            qcssapp.CurrentOrder.products[index].set('prepOption', this.model.get('prepOption'));

            // Get the number of products in the cart
            var numProducts = qcssapp.Functions.updateNumberOfProductsInCart();

            // Check if this is a Grab and Go order
            qcssapp.Functions.isGrabAndGoOrder();

            // Close the view
            this.exitView();

            // If there is only one product navigate to the Cart
            if( numProducts <= 1 ) {
                qcssapp.Functions.navigateToCart();
                return;
            }

            // Otherwise check for combos
            qcssapp.Functions.checkProductCombos(qcssapp.Functions.navigateToCart);
        },

        // Event handler for remove the product from the order
        removeProduct: function(e) {
            e.stopPropagation();
            qcssapp.Functions.showMask();
            qcssapp.Functions.removeProductFromCart(this.model.get("id"), false);
            this.exitView();
        },

        // Event handler when the Swap button is clicked on for a product in the combo
        swapProduct: function(e) {
            e.stopPropagation();

            // Go to the combo detail keypad to choose a product to swap
            if(this.model.get('comboDetailId') == null && qcssapp.CurrentCombo.id != "" && this.model.get('keypadID')) {
                this.exitView(function() {
                    qcssapp.Functions.loadKeypad(this.model.get('keypadID'));
                }.bind(this));
                return;
            }

            var comboModel = this.model.get('comboModel').get('details');
            var keypadID = "";

            // Find the combo detail for this product, set the comboModel on productModel
            comboModel.each(function(mod) {
                if(mod.get('id') == this.model.get('comboDetailId')) {
                    keypadID = mod.get('keypadId');
                }
            }, this);

            this.model.set('quantity', this.model.get('quantity'));

            // Go to the combo detail keypad to choose a product to swap
            if(keypadID != "") {
                this.exitView(function() {
                    qcssapp.Functions.loadKeypad(keypadID, false, undefined, undefined, this.model);
                }.bind(this));
            }
        },

        // Determine the base price for a product in a combo
        determineBasePrice: function() {
            try {
                // Are we substituting a product using the Swap button
                if(!$.isEmptyObject(qcssapp.ProductInProgress.substituteProduct)) {
                    return Number(qcssapp.ProductInProgress.substituteProduct.get('basePrice'));

                // Is there already a base price on the product
                } else if (this.model.get('basePrice') != null) {
                    return Number(this.model.get('basePrice'));

                // Are we swapping a product from the Combo View
                } else if (qcssapp.CurrentCombo.id != "" && typeof qcssapp.CurrentCombo.swapIndex !== 'undefined') {
                    return qcssapp.CurrentCombo.comboDetails[qcssapp.CurrentCombo.swapIndex].basePrice;

                // Did we go back on a combo detail keypad and decide to change selection
                } else if( qcssapp.CurrentCombo.id != "" && qcssapp.CurrentCombo.comboDetails[qcssapp.ComboInProgress.length] ) {
                    return qcssapp.CurrentCombo.comboDetails[qcssapp.ComboInProgress.length].basePrice;
                }
            } catch (e) {
                qcssapp.Functions.logError(e.message, 'product-detail-view.determineBasePrice()', 1254, 0, e.name);
            }

            return 0;
        },

        // Event handler when the page is scrolled to stick the product name to the top of the page
        checkScroll: function() {
            if(!this.$nameContainer.length) {
                return;
            }

            var top = this.$nameContainer.position().top;

            // comparing `top` to these numbers allows a smoother transition
            // and removes the glitchines described in defect 3830
            // original comparison was if(top < 35) ... else ...
            if(top < 0) {
                this.$name.addClass('sticky');
                if(this.usingIE) this.$name.addClass('ie-sticky');
            } else if(top > 37) {
                this.$name.removeClass('sticky');
                if(this.usingIE) this.$name.removeClass('ie-sticky');
            }
        },

        // Slides the image up from the bottom of the screen
        slideUp: function() {
            qcssapp.Functions.hideMask();

            setTimeout(function() {
                this.$el.removeClass('hide-page');
                this.setDescription();
                this.formatModifierName();

                setTimeout(function() {
                    this.$buttonContainer.removeClass('hide-button');
                    this.$el.parent().css('background', 'rgba(0,0,0,0.5)');
                    if(this.usingIE){
                        this.$el.addClass('ie-product-detail');
                        this.$buttonContainer.addClass('ie-button-container');
                    }

                    try {
                        var containerHeight = $('.product-detail_container').height();
                        $('.scroll-indicator').css('top', containerHeight - 10)
                    } catch (e) {
                        qcssapp.Functions.logError(e.message, 'product-detail-view.slideUp()', 1412, 0, e.name);
                    }

                }.bind(this), 200);

            }.bind(this), 100);
        },

        // Closes the product detail view and transitions the url hash to the previous view
        closeView: function(e) {
            e.stopPropagation();

            if(this.usingIE){
                this.removeIEClasses();
            }

            this.$buttonContainer.addClass('hide-button');
            this.$el.addClass('hide-page').removeClass('active-slide');
            this.$el.parent().css('background', '');
            this.$el.find('.product-detail_name').remove();
            setTimeout(function() {
                qcssapp.Router.navigate(this.closeNavigationPath, {trigger: true});
                this.$el.parent().removeClass('active-slide');
                $('#product-detail-page').off();

                this.$el.parent().removeClass('ie-product-slide');
                this.$el.parent().removeClass('ie-product-detail-parent-overlay');
            }.bind(this), 600);

            $('.scroll-indicator').css('top', '');
            this.modifierLoadComplete = false;

            this.discardChanges();
        },

        // Exits the product detail view
        exitView: function(callback) {
            this.$buttonContainer.addClass('hide-button');
            this.$el.addClass('hide-page').removeClass('active-slide');

            if(this.usingIE){
                this.removeIEClasses();
                this.$el.parent().removeClass('ie-product-slide');
                this.$el.parent().removeClass('ie-product-detail-parent-overlay');
            }

            setTimeout(function() {
                this.$el.parent().removeClass('active-slide');
                $('#product-detail-page').off();

                if(typeof callback === 'function') {
                    callback();
                }
            }.bind(this), 600);

            $('.scroll-indicator').css('top', '');
            this.modifierLoadComplete = false;
        },

        removeIEClasses: function(){
            // Remove IE classes
            this.$el.parent().removeClass('ie-product-detail-parent-overlay');
            this.$el.removeClass('ie-product-detail');
            this.$buttonContainer.removeClass('ie-button-container');
            this.$name.removeClass('ie-sticky');

            // Set margin for ie (need to remove display:block in order to slide down)
            var marginLeft = this.$el.css('margin-left');
            this.$el.css('margin-left', marginLeft );

        },

        discardChanges: function(){

            // quantity changes
            if(this.beginningQuantity != this.model.get('quantity')){
                this.model.set('quantity', this.beginningQuantity);
            }
        },

        // Aligns the modifier name in the center of the line if no other details are shown
        formatModifierName: function() {
            if(!this.$el.find('.modifier_name').length) {
                return;
            }

            var $modifierName = this.$el.find('.modifier_name').eq(0);

            var nameLineHeight = 16;

            var nameHTML = $modifierName.html();
            $modifierName.html('.');

            if($modifierName.height() != 0) {
                nameLineHeight = $modifierName.height();
            }

            $modifierName.html(nameHTML);

            $.each(this.$el.find('.modifier_container'), function(index, mod) {
                var $modName = $(mod).find('.modifier_name');
                var $modDescription = $(mod).find('.modifier_description');
                var $modNutrition = $(mod).find('.modifier_nutrition');
                var $modIcons = $(mod).find('.modifier_icons');
                var $modDetailContainer = $(mod).find('.modifier_detail-container');
                var $modDetails = $(mod).find('.modifier_details');

                // determine the line height of the name
                var nameHeight = $modName.height();
                var nameLineCount = nameHeight > nameLineHeight ? Number(nameHeight / nameLineHeight) : 1;

                if( !$modDescription.text().length && !$modNutrition.children().length && !$modIcons.children().length ) {
                    $modDetailContainer.hide();
                }

                if(nameLineCount == 1 && !$modDescription.text().length && !$modNutrition.children().length && !$modIcons.children().length ) {
                    $modDetails.addClass('align-mod');
                }
            }.bind(this));
        },

        // Event handler to close the list of prep options once the user has selected one, clicked the expand button or clicked out of the area
        pageClick: function(e) {
            e.stopPropagation();

            var $target = $(e.target);

            if($target.hasClass('modifier_prep-option-list') && $target.hasClass('show-prep-list')) {
                // do nothing
            } else if ($target.hasClass('prep-option_container') && $target.parent().hasClass('show-prep-list')) {
                // do nothing
            } else if ( ($target.hasClass('prep-option_select') || $target.hasClass('prep-option_select-container') || $target.hasClass('prep-option_detail-container') ) && $target.parent().parent().hasClass('show-prep-list')) {
                // do nothing
            } else if ( ($target.hasClass('prep-option_select-circle') || $target.hasClass('prep-option_name') || $target.hasClass('prep-option_price') ) && $target.parent().parent().parent().hasClass('show-prep-list')) {
                // do nothing
            } else if(this.$el.find('.show-prep-list').length) {
                var $openPrepList = this.$el.find('.show-prep-list');
                $openPrepList.removeClass('show-prep-list').slideUp();
                $openPrepList.parent().find('.modifier_prep-option-preview-container').slideDown();
            }
        },

        // handle the "heart" icons on favorite click
        adjustItemLineView: function() {
            if(qcssapp.OrderMenuHistory[qcssapp.OrderMenuHistory.length - 1].type
                && qcssapp.OrderMenuHistory[qcssapp.OrderMenuHistory.length - 1].type == "keypad"
                && $('.keypad-line[data-productid=' + this.id + ']').find('.line_name-span')) {

                if($('.keypad-line[data-productid=' + this.id + ']').find('img.quickpick-icon.heart-line-icon').length) {
                    $('.keypad-line[data-productid=' + this.id + ']').find('img.quickpick-icon.heart-line-icon').remove();
                } else {
                    $('.keypad-line[data-productid=' + this.id + ']').find('.line_name-span').after('<img class="quickpick-icon heart-line-icon" src="images/icon-favorites.svg">');
                }
            } else if($('.myQuickPicksBtn.selected')) {

                if($('.keypad-line[data-id=' + this.id + ']').find('img.quickpick-icon.heart-line-icon').length) {
                    $('.keypad-line[data-id=' + this.id + ']').find('img.quickpick-icon.heart-line-icon').remove();
                } else {
                    $('.keypad-line[data-id=' + this.id + ']').find('.line_name-span').after('<img class="quickpick-icon heart-line-icon" src="images/icon-favorites.svg">');
                }

                $('.myQuickPicksBtn.selected')[0].click();
            }
        }
    });

})(qcssapp, jQuery);