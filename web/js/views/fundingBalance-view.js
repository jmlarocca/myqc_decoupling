;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FundingBalance - View for displaying the global balance view
     */
    qcssapp.Views.FundingBalance = Backbone.View.extend({
        internalName: "Funding Balance View",

        el: '#accountFunding_balance_amount', //references existing HTML element for view - can be referenced as this.$el throughout the view

        // Cache the template function for a single item.
        template: _.template( $('#funding-balance-template').html() ),

        //events: //events not needed for this view

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in funding balance view");
            }

            this.options = options || {};

            this.model = this.options.model;

            //display view
            this.render();
        },

        render: function() {

            if(Number(this.model.get('balance')) < 0) {
                var balance = Number(this.model.get('balance')) * -1;
                this.model.set('balance', qcssapp.Functions.formatPriceInApp(balance, '-'));
            } else {
                this.model.set('balance', qcssapp.Functions.formatPriceInApp(this.model.get('balance')));
            }

            this.$el.html( this.template( this.model.attributes ) );
            return this;
        }
    });
})(qcssapp, jQuery);
