;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.DollarDonationView - View for displaying dollar donation functionality
     */
    qcssapp.Views.DollarDonationView = Backbone.View.extend({
        internalName: "Dollar Donation View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'donation-page',

        template: _.template( $('#balance-donation-template').html() ),

        events: {
            'click .donation_close-btn': 'slideOut',
            'change .donation_charity-select': 'selectCharity',
            'click .donation_more, .donation_less': 'expandDescription'
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in dollar donation view");
            }

            _.bindAll(this, 'render', 'displayButtons', 'submitDonation', 'slideOut');

            this.options = options || '';
            this.parentView = this.options.parentView;
            this.balanceModel = this.options.balanceModel;
            this.donations = this.options.donationCollection.models;
            this.dollars = 0.0;
            this.displayedCurrencyAmount = 0.0; //there should never be a 0 available amount if we're able to get to this page
            //logic to determine which balance type to pull available funds from (payroll-deduct vs pre-pay)
            var accountType='-1';
            if(qcssapp.accountModel){accountType = qcssapp.accountModel.get('accountType');}
            accountType = Number(accountType);
            switch (accountType) {
                case 1:
                    //if the user is a payroll deduct account we check if the user has an available balance to use in the pay period
                    if (Number(this.balanceModel.get('availValue')) > 0) {
                        this.dollars = Number(this.balanceModel.get('availValue'));
                        this.displayedCurrencyAmount = (this.balanceModel.get('avail'));
                    }
                    break;
                case 2:
                    //if the user is a prepay account, we check if they have a balance they can use
                    if (Number(this.balanceModel.get('balanceValue')) > 0) {
                        this.dollars = Number(this.balanceModel.get('balanceValue'));
                        this.displayedCurrencyAmount = (this.balanceModel.get('balance'));
                    }
                    break;
            }

            this.selectedCharity = null;

            this.render();
        },

        render: function() {
            var params = {
                name: '',
                description: '',
                dollars: this.dollars,
                displayedCurrencyAmount: this.displayedCurrencyAmount
            };

            if(this.donations.length == 1) {
                this.selectedCharity = this.donations[0];

                params = {
                    name: this.selectedCharity.get('name'),
                    description: this.selectedCharity.get('description'),
                    dollars: this.dollars,
                    displayedCurrencyAmount : this.displayedCurrencyAmount
                };
            }

            this.$el.html( this.template( params ) );

            $('#pageview').append( this.$el );

            this.$el.addClass('hidden');

            this.$container = this.$el.find('.donation_container');
            this.$donationInfo = this.$el.find('.donation_info');
            this.$charitySelect = this.$el.find('.donation_charity-select');
            this.$donationTitle = this.$el.find('.donation_title');
            this.$donationAmount = this.$el.find('.donation_enter-pts-input');
            this.$donationAmount.val(0);
            this.$donationEnterPtsContainer = this.$el.find('.donation_enter-pts-container');
            this.$donationInvalid = this.$el.find('.donation_invalid-amount');
            this.$donationDescription = this.$el.find('.donation_description');
            this.$donationMore = this.$el.find('.donation_more');
            this.$donationLess = this.$el.find('.donation_less');

            // On Donation input switch font styles
            this.$donationAmount.on('input', function() {
                this.$donationAmount.removeClass('focus-out');

                if(this.$donationAmount.val() == '') {
                    this.$donationAmount.addClass('focus-out');
                }
            }.bind(this));

            // On Donation change check if amount is invalid
            this.$donationAmount.on('change', function() {
                this.$donationEnterPtsContainer.removeClass('invalid');
                this.$donationInvalid.text('Invalid Amount');
                var donationAmt = Number(this.$donationAmount.val());
                var numToCheckDecimalPlaces='0.0';
                if(this.$donationAmount.val()){
                    numToCheckDecimalPlaces = (this.$donationAmount.val().split('.'));
                }
                if(numToCheckDecimalPlaces[1] && numToCheckDecimalPlaces[1].length>2){
                    donationAmt = numToCheckDecimalPlaces[1].slice(0,2).toString()
                    donationAmt = (numToCheckDecimalPlaces[0].toString() + '.' + donationAmt);
                    this.$donationAmount.val(donationAmt);
                    donationAmt = Number(donationAmt);
                }
                if( (donationAmt < 0.01 || donationAmt > this.dollars) ) {
                    this.$donationEnterPtsContainer.addClass('invalid');

                    if(this.$donationAmount.val() > this.dollars) {
                        this.$donationInvalid.text('Invalid Amount - You only have ' + this.dollars + ' dollars');
                    }
                }
            }.bind(this));

            this.$charitySelect.hide();
            this.$donationInfo.show();
            this.$donationTitle.show();

            // If more than one charity show charity selector
            if(this.donations.length > 1) {
                this.$charitySelect.show();
                this.$donationInfo.hide();
                this.$donationTitle.hide();

                this.$charitySelect.append('<option value="">Select a Charity</option>');

                this.displayCharitySelector();
            }

            this.displayButtons();

            return this;
        },

        displayButtons: function() {
            this.$el.find('#donation_donate-btn').remove();

            new qcssapp.Views.ButtonView({
                text: 'Donate',
                appendToID: "#balance-donation_btn-container",
                id: "donation_donate-btn",
                buttonType: 'customCB',
                class: "accent-color-two-background",
                callback: function() {
                    var dollarsToDonate = Number(this.$donationAmount.val());

                    if(dollarsToDonate == '' || dollarsToDonate < 0.01 | dollarsToDonate > this.dollars) {
                        this.$donationEnterPtsContainer.addClass('invalid');

                        if( dollarsToDonate > this.dollars ) {
                            this.$donationInvalid.text('Invalid Amount - You only have ' + this.dollars + ' dollars');
                        }

                        return;
                    }

                    var params = {
                        dollars: dollarsToDonate
                    };

                    var dollarText = dollarsToDonate == 1 ? 'dollar' : 'dollars';

                    qcssapp.Functions.displayPopup('Are you sure you want to donate<br> ' + qcssapp.Functions.formatPriceInApp(dollarsToDonate) + ' to ' + this.selectedCharity.get('name').trim() + '?', 'Submit Donation', 'Cancel', '', '', 'Donate', this.submitDonation, params);

                }.bind(this)
            });

            this.calculateDescriptionHeight();

            $('.scroll-indicator').hide();
        },

        submitDonation: function(params) {
            var dollars = params.dollars;

            var endPoint = qcssapp.Location + '/api/ordering/dollarDonation/submit';
            //var endpoint = qcssapp.Location + '/horribleThings'; //lumby , placeholder endpoint while setting up frontend validation

            var donations = [];
            var donation = {
                'donation' : this.selectedCharity,
                'amount': dollars
            };

            donations.push(donation);

            var orderObject = {
                "personName": qcssapp.accountModel.get('name'),
                "donations": donations,
                "submittedTime": moment().format("YYYY-MM-DD HH:mm:ss.SSS")
            };

            qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(orderObject), '',
                function(response) { /*Success Function*/

                    if(!response.transactionID) {
                        qcssapp.Functions.displayPopup('There was an issue completing the donation. Please try again later.', 'Error', 'OK');
                        return;
                    }

                    qcssapp.Functions.displayPopup('Thank you for donating to ' + this.selectedCharity.get('name').trim()+'!', '<div class="donation_success">&#10004;</div>Success', 'OK', this.slideOut);

                    this.parentView.updateProgramAfterDonation(dollars)

                }.bind(this),
                '', '', '', '', false, false, true
            );
        },

        displayCharitySelector: function() {
            this.donations.sort(this.sortByName);

            $.each(this.donations, function( index, mod ) {
                this.renderCharityLine( mod );
            }.bind(this) );

            this.$charitySelect.selectric({disableOnMobile:false, nativeOnMobile:false});
            this.$charitySelect.val(this.$charitySelect.find('option').attr('value')).selectric('refresh');
            this.$charitySelectric = this.$el.find('.selectric-donation_charity-select');
        },

        renderCharityLine: function( mod ) {
            var charitySelectLine = new qcssapp.Views.CharitySelectLineView({
                model:mod
            });

            this.$charitySelect.append(charitySelectLine.render().el);
        },

        selectCharity: function() {
            var charityId = this.$charitySelect.val();

            if(charityId != "0") {
                this.$donationInfo.fadeIn();
                this.$charitySelectric.find('.label').addClass('selected');
                this.$donationDescription.removeClass('expanded');
                this.$donationMore.hide();
                this.$donationLess.hide();
            }

            $.each(this.donations, function( index, mod ) {
                if(mod.get('id') == charityId) {
                    this.selectedCharity = mod;
                }
            }.bind(this) );

            if(this.selectedCharity != null) {
                this.$donationTitle.text(this.selectedCharity.get('name'));
                this.$donationDescription.html(this.selectedCharity.get('description'));

                this.calculateDescriptionHeight();
            }
        },

        sortByName: function(a, b) {
            var aName = a.get('name').toLowerCase();
            var bName = b.get('name').toLowerCase();

            return((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
        },

        calculateDescriptionHeight: function() {
            var containerHeight = this.$el[0].offsetTop;
            var titleHeight = this.donations.length > 1 ? this.$el.find('.selectric-donation_charity-select')[0].clientHeight : this.$el.find('.donation_title')[0].clientHeight;
            var balanceHeight = this.$el.find('#dollarDonationBalanceContainer')[0].clientHeight;
            var enterPtsHeight = this.$el.find('#dollarDonationEnterDollarsContainer')[0].clientHeight;
            var btnContainerHeight = this.$el.find('#balance-donation_btn-container')[0].clientHeight;

            if(containerHeight == 0 || balanceHeight == 0) {
                return;
            }

            var totalElementHeight = titleHeight + balanceHeight + enterPtsHeight + btnContainerHeight;

            var descriptionMaxHeight = containerHeight - totalElementHeight;
            this.$donationDescription.css('max-height', descriptionMaxHeight);

            if(this.$donationDescription[0].scrollHeight > descriptionMaxHeight) {
                $("<style> .donation_description.expanded {max-height:"+this.$donationDescription[0].scrollHeight+"px !important;}</style>").appendTo($('head'));

                this.$donationMore.show();

                var donationOffsetTop  = this.$donationDescription[0].offsetTop > 0 ? this.$donationDescription[0].offsetTop : 0;
                var donationHeight = descriptionMaxHeight + donationOffsetTop;
                var donationScrollHeight = this.$donationDescription[0].scrollHeight + donationOffsetTop;
                this.$donationMore.css('top', donationHeight);
                this.$donationLess.css('top', donationScrollHeight);
            }
        },

        expandDescription: function() {
            if(this.$donationDescription.hasClass('expanded')) {
                this.$donationDescription.removeClass('expanded');
                this.$donationMore.fadeIn(700);
                this.$donationLess.hide();
            } else {
                this.$donationMore.hide();
                this.$donationDescription.addClass('expanded');
                this.$donationLess.fadeIn(900);
            }
        },

        slideIn: function() {
            this.hidden = false;
            this.$el.fadeIn(150, function() {
                this.$el.removeClass('hidden');
                this.calculateDescriptionHeight();
            }.bind(this));
        },

        slideOut: function() {
            this.hidden = true;
            this.$el.addClass('hidden');

            setTimeout(function() {
                this.undelegateEvents();
                $(this.el).empty();
                this.remove();
            }.bind(this), 800);
            this.parentView.reloadBalances();
        }

    });
})(qcssapp, jQuery);