;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.GlobalBalanceView - View for displaying the global balance view
     */
    qcssapp.Views.GlobalBalanceView = Backbone.View.extend({
        internalName: "Global Balance View",

        el: '#globalBalanceContainer', //references existing HTML element for view - can be referenced as this.$el throughout the view

        // Cache the template function for a single item.
        template: _.template( $('#global-balance-template').html() ),

        //events: //events not needed for this view

        initialize: function (options , passedView) {
            if (qcssapp.DebugMode) {
                console.log("initialize in global balance view");
            }

            //set options
            this.parentView = passedView;
            this.options = options;
            if ( this.options.avail == null ) {
                this.avail = "";
            } else {
                this.avail = this.options.avail.toFixed(2);
                this.availValue = this.options.avail;
            }
            if (this.options.balance || this.options.balance == "0") {
                this.balance = this.options.balance.toFixed(2);
                this.balanceValue = this.options.balance;
            }
            if (this.options.limit || this.options.limit == "0") {
                this.limit = this.options.limit.toFixed(2);
            }
            this.lastSynced = this.options.lastSynced;

            //create new model with given options
            this.model = new qcssapp.Models.Balance({
                name: "Global Balances",
                limit: this.limit,
                balance: this.balance,
                avail: this.avail,
                balanceValue: this.balanceValue,
                availValue: this.availValue,
                lastSynced: this.lastSynced
            });
            this.donationCollection = new qcssapp.Collections.Donations();
            //display view
            this.render();
        },

        render: function() {
            this.formatBalances();
            this.$el.html( this.template( this.model.attributes ) );

            // D-4253: missing route in older server versions was causing a 404, this check prevents that
            if(qcssapp.Functions.checkServerVersion(5,0,5)) {
                this.loadDollarDonationSettings();
                return this;
            }
            // D-4253: the page would take nearly 15 seconds to show, appears the mask was simply not being hidden in a timely manner
            // this line is a back up in case we don't meet the server requirements above
            qcssapp.Functions.hideMask();
            return this;
        },

        formatBalances: function(){
            if(Number(this.model.get('balance')) < 0) {
                var balance = Number(this.model.get('balance')) * -1;
                this.model.set('balance', qcssapp.Functions.formatPriceInApp(balance, '-'));
            } else {
                this.model.set('balance', qcssapp.Functions.formatPriceInApp(this.model.get('balance')));
            }

            if(Number(this.model.get('avail')) < 0) {
                this.model.set('avail', qcssapp.Functions.formatPriceInApp(this.model.get('avail'), '-'));
            } else {
                this.model.set('avail', qcssapp.Functions.formatPriceInApp(this.model.get('avail')));
            }
        },

        loadDonationCollection: function(donationSettings) {
            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/donations', 'GET', '', '',
                function(response) {
                    if (qcssapp.DebugMode) {
                        console.log("monetary donation list: ");
                        console.log(response);
                    }
                    this.addDonations(response);
                    this.displayDonationButtons(donationSettings);
                    qcssapp.Functions.hideMask();
                }.bind(this),
                function() {
                    this.fetchDonationError();
                },'', '', '', true
            );
        },

        addDonations: function(data) {
            this.donationCollection.set(data, {parse: true});
            qcssapp.Functions.hideMask();
        },

        fetchDonationError: function() {
            qcssapp.Functions.displayError('There was an error loading your monetary donations, please try again later.');
            qcssapp.Functions.hideMask();
        },

        loadDollarDonationSettings: function() {
            var globalBalanceView = this;
            var donationSettingsDefault = {
                "DOLLARDONATIONLABELTEXT":"",
                "ALLOWDOLLARDONATIONS":false
            };
            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/balances/dollarDonationSettings', 'GET', '', '',
                function(response) {
                    // D-4253: response could be an empty string, check that it also has a length that returns truthy to avoid errors trying to render buttons in displayDonationButtons
                    if (response && response.length) {
                        if (qcssapp.DebugMode) {
                            console.log("Successfully received Dollar Donation Information");
                        }
                        globalBalanceView.loadDonationCollection(response[0]);
                    }
                    else {
                        globalBalanceView.loadDonationCollection(donationSettingsDefault);
                    }
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error loading Dollar Donation settings");
                        globalBalanceView.loadDonationCollection(donationSettingsDefault);
                    }
                },'', '', '', true, true
            );
        },

        displayDonationButtons: function(donationSettings) {
                new qcssapp.Views.ButtonView({
                text: donationSettings.DOLLARDONATIONLABELTEXT == "" ? "Donate": donationSettings.DOLLARDONATIONLABELTEXT ,
                buttonType: 'customCB', //customCB means "custom callback" I believe
                appendToID: "#dollar-donation-balance-btn-container",
                id: "balance-donate-btn",
                class: "align-center primary-background-color font-color-primary primary-gradient-color primary-border-color",
                callback: function() {
                    //open dollar donation page
                    this.goToDonation();
                }.bind(this)
            });
                var accountType = "-1"; //placeholder accountType (matches nothing)
                if(qcssapp.accountModel){accountType = qcssapp.accountModel.get('accountType');}
                accountType = Number(accountType);
                if(donationSettings.ALLOWDOLLARDONATIONS==true && this.donationCollection.length>0) {
                    switch (accountType) {
                        case 1:
                            //if the user is a payroll deduct account we check if the user has an available balance to use in the pay period
                            if (Number(this.model.get('availValue')) > 0) {
                                this.$el.find('#balance-donate-btn').show();
                            }
                            break;
                        case 2:
                            //if the user is a prepay account, we check if they have a balance they can use
                            if (Number(this.model.get('balanceValue')) > 0) {
                                this.$el.find('#balance-donate-btn').show();
                            }
                    }
                } //if the user doesn't meet any of the criteria to donate, never show the donate button
        },

        //pulls up the balance donation modal (slides up)
        goToDonation: function() {
            if($('.donation-page').length) {
                return;
            }

            var params = {
                balanceModel: this.model,
                donationCollection: this.donationCollection,
                parentView: this
            };

            var dollarDonationView = new qcssapp.Views.DollarDonationView(params);
            dollarDonationView.slideIn();
        },

        reloadBalances: function() {
            this.parentView.emptyBalanceList();
            this.parentView.loadCollection();
        },
        updateProgramAfterDonation: function(donatedDollars) {
            var accountType='-1';
            var dollars;
            if(qcssapp.accountModel){accountType = qcssapp.accountModel.get('accountType');}
            accountType = Number(accountType);
            switch (accountType) {
                case 1:
                    //if the user is a payroll deduct account
                    dollars = Number(this.model.get('availValue')) - Number(donatedDollars);
                    var payrollBalance =Number(this.model.get('balanceValue')) + Number(donatedDollars);
                    this.model.set('availValue', dollars);
                    this.model.set('avail', dollars);
                    this.model.set('balanceValue', payrollBalance);
                    this.model.set('balance', payrollBalance);
                    break;
                case 2:
                    //if the user is a prepay account
                    dollars = Number(this.model.get('balanceValue')) - Number(donatedDollars);
                    this.model.set('balanceValue', dollars);
                    this.model.set('balance', dollars);
                    break;
            }
            this.$el.find('.donation_balance-amt').text(dollars);
            if(dollars == 0) {
                this.$el.find('.balance-donate-btn').hide();
            }
        }
    });
})(qcssapp, jQuery);
