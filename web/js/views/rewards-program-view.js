;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.RewardsProgramView - View for displaying Balances Collections
     */
    qcssapp.Views.RewardsProgramView = Backbone.View.extend({
        //is a div
        tagName:  'div',
        className: 'tile tile-list',

        // Cache the template function for a single item.
        template: _.template( $('#rewards-program-template').html() ),

        events: {
            'click': 'toggleExpanded',
            'click .details-icon': 'toggleShowDetails',
            'click .rewards-icon': 'goToRewards',
            'click .history-icon': 'goToHistory',
            'click .reward-donate-btn': 'goToDonation'
        },

        initialize: function() {
            this.listenTo(this.model, 'change:expanded', this.toggleInfo);
            this.listenTo(this.model, 'change:showDetails', this.toggleDetails);

            this.detailIconSrc = 'images/icon-details-active.svg';
            this.detailIconActiveSrc = 'images/icon-details.svg';

            return this;
        },

        render: function() {
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.toJSON() ) );

            //elements
            this.$expandArrow = this.$el.find('.expand-arrow');
            this.$infoContainer = this.$el.find('.rewards-info-container');
            this.$detailsContainer = this.$el.find('.rewards-program-details');
            this.$detailImg = this.$el.find('.details-icon > img');

            //change the text under the 'gift' icon if the 'Rewards' text is changed in the Global Settings
            this.rewardsMenuText = $('#show-rewards').attr('data-title').substring(0,$('#show-rewards').attr('data-title').indexOf('Programs'));
            if(this.rewardsMenuText != "") {
                this.$el.find('#rewards-icon-text').text(this.rewardsMenuText);
            }

            // D-4507: the `display` css property would sometimes overwrite the button being shown, hiding it when these values are 0 is more consistent
            if(this.model.get('donations').length < 1 || Number(this.model.get('points')) < 1) {
                this.$el.find('.reward-donate-btn').hide();
            }

            return this;
        },

        toggleExpanded: function() {
            this.model.toggleExpanded();
        },

        //simple toggle function for displaying the info related to a program
        toggleInfo: function() {
            this.$expandArrow.toggleClass('expanded');
            this.$infoContainer.slideToggle();
        },

        toggleShowDetails: function(e) {
            e.stopPropagation();

            this.model.toggleShowDetails();
        },

        toggleDetails: function() {
            if ( this.model.get('showDetails') ) {
                this.$detailImg.attr('src', this.detailIconSrc);
            } else {
                this.$detailImg.attr('src', this.detailIconActiveSrc);
            }

            this.$detailsContainer.slideToggle();
        },

        goToRewards: function(e) {
            e.stopPropagation();

            var rewardsControllerModel = new qcssapp.Models.Controller({
                id: 'RewardView',
                route: 'program-rewards',
                options: {programModel: this.model},
                destructible: true,
                destroyOn: ['NavView', 'RewardsProgramsView']
            });

            qcssapp.Controller.activateView(rewardsControllerModel);
        },

        goToHistory: function(e) {
            e.stopPropagation();

            var historyControllerModel = new qcssapp.Models.Controller({
                id: 'RewardHistoryView',
                route: 'program-rewards-history',
                options: {programModel: this.model},
                destructible: true,
                destroyOn: ['NavView', 'RewardsProgramsView']
            });

            qcssapp.Controller.activateView(historyControllerModel);
        },

        goToDonation: function(e) {
            e.stopPropagation();

            if($('.donation-page').length) {
                return;
            }

            var params = {
                programModel: this.model,
                parentView: this
            };

            var pointDonationView = new qcssapp.Views.PointDonationView(params);
            pointDonationView.slideIn();

            $('#rewards-programs-list').off();
        },

        updateProgramAfterDonation: function(donation) {
            var points = Number(this.model.get('points')) - Number(donation.points);

            this.model.set('points', points);
            this.$el.find('.rewards-pts-amt').text(points);

            if(points == 0) {
                this.$el.find('.reward-donate-btn').hide();
            }
        }
    });
})(qcssapp, jQuery);
