;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.BalanceView - View for displaying Balance Collections
     */
    qcssapp.Views.BalanceView = Backbone.View.extend({
        internalName: "Balance View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#show-balances', //references existing HTML element for Balance Collections - can be referenced as this.$el throughout the view

        //template: //template not needed for this view

        events: {
            'click .balance-group-container': 'toggleExpanded'
        },

        initialize: function () {
            if (qcssapp.DebugMode) {
                console.log("initialize in balance view");
            }

            //bind "this" reference to functions in the view
            _.bindAll(this, "fetchSuccess", "fetchError", 'displayGlobalBalance', 'render');

            //view elements
            this.$balanceList = $('#balance-list');
            this.$globalBalanceContainer = $('#globalBalanceContainer');

            //TODO: make a balanceViewModel - decliningBalance, balanceCollection
            //view properties
            this.decliningBalance = false;
            this.collection = new qcssapp.Collections.Balances();

            //TODO: this should reference the employee model on the globalAccessor
            //check for account type, need to change the way information is displayed for account types 2 (Prepay) and 4 (Gift Card)
            var $userInfo = $('.user-information-title');
            if ( $userInfo.length && $userInfo.attr("data-accounttype") && ( $userInfo.attr('data-accounttype') == "2" || $userInfo.attr("data-accounttype") == "4" ) ) {
                //TODO: balanceViewModel listen for change:decliningBalance
                this.decliningBalance = true;
                //TODO: in event handler
                this.$el.addClass('prepay')
            }

            //populates the new collection
            this.loadCollection();
        },

        emptyBalanceList: function() {
            this.$balanceList.empty();
        },

        fetchError: function() { //handles error for fetching the balance collection
            if (qcssapp.DebugMode) {
                console.log("Error loading data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            //display error message to user
            qcssapp.Functions.displayError('There was an error loading your balances, please try again later.');
            qcssapp.Functions.hideMask();
        },


        fetchSuccess: function(data) { //handles successful fetching the balance collection
            if (qcssapp.DebugMode) {
                console.log("Successfully loaded data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            if ( data.length == 0 ) {
                this.$balanceList.append('<li class="align-center italic">No Balance Information Found.</li>');
                this.$globalBalanceContainer.hide();

                //no point in rendering nothing
                qcssapp.Functions.hideMask();
                return false;
            }

            //render this view now that we've fetched the balance collection
            this.render();
        },

        //calls renderLine on each balance group
        render: function() {
            this.collection.each(function( mod ) {
                this.renderLine( mod );
            }, this );

            this.$storeListHeaders = $('.store-list-header');

            this.displayGlobalBalance();
        },

        //builds a new group view, then creates and appends a new sub collection of stores, then displays the whole view
        renderLine: function( mod ) {
            //TODO: BalanceGroupLineView is not necessary at all - use its template to make lines in this view
            var BalanceGroupLineView = new qcssapp.Views.BalanceGroupLineView({
                model: mod
            });
            //creates the access to $el and el for the balance group, allows jQuery manipulation before appending
            var BalanceGroupLineViewRendered = BalanceGroupLineView.render();

            //append the group view to the page view
            this.$balanceList.append( BalanceGroupLineViewRendered.el );

            //to keep track of the group to populate
            var count = 0;

            //TODO: fix this each, use _.each(list, function(item){})
            //TODO: move storeCollection to balanceModel and pre-populate with stores
            //for each store in the group's store collection
            BalanceGroupLineViewRendered.storeCollection.each(function() {
                var storeView = new qcssapp.Views.StoreView({
                    model: BalanceGroupLineViewRendered.storeCollection.models[count]
                });

                BalanceGroupLineViewRendered.$el.find(".stores-container").append( storeView.render().el );

                count++;
            });

            //TODO: dont reference model attributes directly
            //TODO do this in the above each, update the model attributes before creating the view
            //if avail is empty, then this balance group line is "UNLIMITED" - so set all balances to "blank"
            if (BalanceGroupLineViewRendered.model && BalanceGroupLineViewRendered.model.attributes
                && BalanceGroupLineViewRendered.model.attributes.avail == "") {
                BalanceGroupLineViewRendered.$el.find(".balance-container").html("");
                BalanceGroupLineViewRendered.$el.find(".store-container").find("span").html("");
            }

        },

        //fetch collection to be used in this view
        loadCollection: function() {
            //TODO: dont need 2 objects containing the same data
            var successParameters = {
                balanceView:this
            };

            var errorParameters = {
                balanceView:this
            };

            //TODO: Adjust callAPI parameters
            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/balances', 'GET', '',successParameters,
                function(response, successParameters) {
                    //TODO: check balanceViewModel instead of view
                    if ( successParameters.balanceView.decliningBalance ) {
                        $.each(response, function() {
                            this.balance = -1 * this.balance;
                        });
                    }

                    successParameters.balanceView.collection.set(response, {parse:true});
                    successParameters.balanceView.fetchSuccess(response);
                },
                function(errorParameters) {
                    errorParameters.balanceView.fetchError();
                },
                '',
                errorParameters,
                '',false,true
            );
        },

        //GET request for global balance information, adding the global balance view to the page
        displayGlobalBalance: function() {
            var params = {
                balanceView: this
            };

            var viewToPass = this;

            //TODO: Adjust callAPI parameters
            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/balances/global', 'GET', '', params,
                function(data, params) {
                    if ( data ) {
                        //TODO: refer to balanceViewModel
                        if ( params.balanceView.decliningBalance ) {
                            data.balance = -1 * data.balance;
                        }

                        //TODO: pass the decliningBalance info so the globalBalanceView can do the work
                        new qcssapp.Views.GlobalBalanceView(data, viewToPass);

                        if ( params.balanceView.decliningBalance ) {
                            $('.available-container').hide();
                            params.balanceView.$storeListHeaders.html('Valid Stores:');
                        }
                    }
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error loading global balance data");
                    }
                    qcssapp.Functions.displayError('There was an error loading your global balance.');
                },
                function() {
                    //Commented this out as there is more to load on the page as of 12/11/2020 (dollar donations)
                    //qcssapp.Functions.hideMask();
                },
                '','','',true
            );
        },

        toggleExpanded: function(e) {
            var $this = $(e.currentTarget);
            $this.find('.expand-arrow').toggleClass('expanded').siblings('.stores-container').slideToggle();
        }
    });
})(qcssapp, jQuery);