;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.TxnView - View for displaying transaction Collection
     */
    qcssapp.Views.TxnView = Backbone.View.extend({
        //is a list tag.
        tagName:  'li',

        className: 'transaction-line',

        events: {
            'click .trans-type-icon': 'showLegend'
        },

        // Cache the template function for a single item.
        template: _.template( $('#transaction-new-template').html() ),

        render: function() {

            if(Number(this.model.get('amt')) < 0) {
                var amt = Number(this.model.get('amt')) * -1;
                this.model.set('amt', qcssapp.Functions.formatPriceInApp(amt, '-'));
            } else {
                this.model.set('amt', qcssapp.Functions.formatPriceInApp(this.model.get('amt')));
            }

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            if ( this.model.attributes.patransactionID == 0 || this.model.get('showReceiptsOnMyQC') == 0 ) {
                this.model.set('showReceiptsOnMyQC', false);
                this.$el.addClass('no-receipt');
            }

            if ( this.model.attributes.paorderTypeID == 1 || this.model.attributes.paorderTypeID == null || this.model.get('terminalInProfile') == false || this.model.get('transTypeID') == 15 ) {
                this.model.set('showReorder', false);
                this.$el.addClass('no-reorder');
            }

            if( typeof this.model.get('enableFavorites') !== 'undefined' && !this.model.get('enableFavorites') ) {
                this.$el.addClass('no-favorites');
            }

            if( typeof this.model.get('transTypeID') !== 'undefined' ) {

                var imagePath = '';
                var transTypeID = this.model.get('transTypeID');

                if( transTypeID == 5) {
                    imagePath = 'images/cancel-icon.png';
                } else if( transTypeID == 15) {
                    imagePath = 'images/open-icon.png';
                } else if ( transTypeID == 3 ) {
                    imagePath = 'images/transaction-refund.svg';
                } else if ( transTypeID == 2 ) {
                    imagePath = 'images/transaction-void.svg';
                } else {
                    imagePath = 'images/sale-icon.png';
                }

                if(imagePath != '') {
                    //set the image
                    this.$el.find('.trans-type-icon').css('background', 'url('+ imagePath +') center center no-repeat');
                    this.$el.find('.trans-type-icon').show();

                    if(transTypeID == 1) {
                        this.$el.find('.trans-type-icon').addClass('sale');
                    }
                }
            }

            if(typeof this.model.get('refundID') !== 'undefined' && this.model.get('refundID') != '' && this.model.get('refundID') != null) {
                this.$el.find('.list-refund').show();
            }

            return this;
        },

        showLegend: function() {

            var legend = '<div class="legend-container">' +
                '<div class="trans-type-title">Icon Legend</div>' +
                '<div class="trans-type-container"><div class="template-gen trans-type-sale-icon"></div><div class="trans-type-label">Sale Transaction</div></div>' +
                '<div class="trans-type-container"><div class="template-gen trans-type-open-icon"></div><div class="trans-type-label" style="font-size:15.5px">Open/Incomplete Transaction</div></div>' +
                '<div class="trans-type-container"><div class="template-gen trans-type-refund-icon"></div><div class="trans-type-label">Refunded Transaction</div></div>' +
                '<div class="trans-type-container"><div class="template-gen trans-type-void-icon"></div><div class="trans-type-label">Voided Transaction</div></div>' +
                '<div class="trans-type-container"><div class="template-gen trans-type-cancel-icon"></div><div class="trans-type-label">Canceled Transaction</div></div></div>';

            $.confirm({
                title: '',
                content: legend,
                buttons: {
                    cancel: {
                        text: 'Close',
                        btnClass: 'btn-default confirm-btn prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {}
                    }
                }
            });
        }
    });
})(qcssapp, jQuery);
