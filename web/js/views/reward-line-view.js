;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.RewardLineView - View for displaying individual rewards on the reward view
     */
    qcssapp.Views.RewardLineView = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'tile tile-list',

        // Cache the template function for a single item.
        template: _.template( $('#reward-line-template').html() ),

        initialize: function() {
            return this;
        },

        render: function() {
            this.rewardsMenuText = $('#show-rewards').attr('data-title').substring(0,$('#show-rewards').attr('data-title').indexOf('Programs'));
            //format value based on reward type
            switch ( this.model.attributes.typeName ) {
                case "Account Credit":
                    this.model.attributes.value = '<strong>'+this.rewardsMenuText+'Value: </strong>' + "$" + Number(this.model.attributes.value).toFixed(2);
                    break;
                case "Transaction Credit":
                    this.model.attributes.value = '<strong>'+this.rewardsMenuText+'Value: </strong>' +"$" + Number(this.model.attributes.value).toFixed(2);
                    break;
                case "Transaction Percent Off":
                    this.model.attributes.value = '<strong>'+this.rewardsMenuText+'Value: </strong>' + Number(this.model.attributes.value).toFixed(2) + '%';
                    break;
                default:
                    this.model.attributes.value = '<strong>'+this.rewardsMenuText+'Value: </strong>' +  Number(this.model.attributes.value);
                    break;
            }
            this.model.attributes.typeName = '<strong>'+this.rewardsMenuText+'Type: </strong>' +this.model.attributes.typeName
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            return this;
        }
    });
})(qcssapp, jQuery);
