;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.PointDonationView - View for displaying point donation functionality
     */
    qcssapp.Views.PointDonationView = Backbone.View.extend({
        internalName: "Point Donation View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'donation-page',

        template: _.template( $('#point-donation-template').html() ),

        events: {
            'click .donation_close-btn': 'slideOut',
            'change .donation_charity-select': 'selectCharity',
            'click .donation_more, .donation_less': 'expandDescription'
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in point donation view");
            }

            _.bindAll(this, 'render', 'displayButtons', 'submitDonation', 'slideOut');

            this.options = options || '';
            this.parentView = this.options.parentView;
            this.programModel = this.options.programModel;
            this.donations = this.programModel.get('donations');
            this.points = this.programModel.get('points') == '' ? 0 : this.programModel.get('points');

            this.selectedCharity = null;

            this.render();
        },

        //calls renderLine on each item in the combo
        render: function() {
            var params = {
                programName: this.programModel.get('name') + ' - Donation',
                name: '',
                description: '',
                points: this.points
            };

            if(this.donations.length == 1) {
                this.selectedCharity = this.donations[0];

                params.name = this.selectedCharity.get('name');
                params.description = this.selectedCharity.get('description');
                params.points = this.points;
            }

            this.$el.html( this.template( params ) );

            $('.view.active').append( this.$el );

            this.$el.addClass('hidden');

            this.$container = this.$el.find('.donation_container');
            this.$donationInfo = this.$el.find('.donation_info');
            this.$charitySelect = this.$el.find('.donation_charity-select');
            this.$donationTitle = this.$el.find('.donation_title');
            this.$donationAmount = this.$el.find('.donation_enter-pts-input');
            this.$donationEnterPtsContainer = this.$el.find('.donation_enter-pts-container');
            this.$donationInvalid = this.$el.find('.donation_invalid-amount');
            this.$donationDescription = this.$el.find('.donation_description');
            this.$donationMore = this.$el.find('.donation_more');
            this.$donationLess = this.$el.find('.donation_less');

            // On Donation input switch font styles
            this.$donationAmount.on('input', function() {
                this.$donationAmount.removeClass('focus-out');

                if(this.$donationAmount.val() == '') {
                    this.$donationAmount.addClass('focus-out');
                }
            }.bind(this));

            // On Donation change check if amount is invalid
            this.$donationAmount.on('change', function() {
                this.$donationEnterPtsContainer.removeClass('invalid');
                this.$donationInvalid.text('Invalid Amount');
                var donationAmt = Number(this.$donationAmount.val());

                if( (donationAmt < 1 || donationAmt % 1 != 0 || donationAmt > this.points) ) {
                    this.$donationEnterPtsContainer.addClass('invalid');

                    if(this.$donationAmount.val() > this.points) {
                        this.$donationInvalid.text('Invalid Amount - You only have ' + this.points + ' points');
                    }
                }
            }.bind(this));

            this.$charitySelect.hide();
            this.$donationInfo.show();
            this.$donationTitle.show();

            // If more than one charity show charity selector
            if(this.donations.length > 1) {
                this.$charitySelect.show();
                this.$donationInfo.hide();
                this.$donationTitle.hide();

                this.$charitySelect.append('<option value="">Select a Charity</option>');

                this.displayCharitySelector();
            }

            this.displayButtons();

            return this;
        },

        displayButtons: function() {
            this.$el.find('#donation_donate-btn').remove();

            var donateButton = new qcssapp.Views.ButtonView({
                text: 'Donate',
                id: "donation_donate-btn",
                buttonType: 'customCB',
                class: "accent-color-two-background",
                callback: function() {
                    var pointsToDonate = Number(this.$donationAmount.val());

                    if(pointsToDonate == '' || pointsToDonate < 1 || pointsToDonate % 1 != 0 || pointsToDonate > this.points) {
                        this.$donationEnterPtsContainer.addClass('invalid');

                        if( pointsToDonate > this.points ) {
                            this.$donationInvalid.text('Invalid Amount - You only have ' + this.points + ' points');
                        }

                        return;
                    }

                    var params = {
                        points: pointsToDonate
                    };

                    var pointText = pointsToDonate == 1 ? 'point' : 'points';

                    qcssapp.Functions.displayPopup('Are you sure you want to donate<br> ' + pointsToDonate + ' ' + pointText + ' to ' + this.selectedCharity.get('name').trim() + '?', 'Submit Donation', 'Cancel', '', '', 'Donate', this.submitDonation, params);

                }.bind(this)
            });

            this.$el.find(".donation_btn-container").append(donateButton.$el);

            this.calculateDescriptionHeight();

            $('.scroll-indicator').hide();
        },

        submitDonation: function(params) {
            var points = params.points;

            var endPoint = qcssapp.Location + '/api/ordering/donation/submit';

            var donations = [];
            var donation = {
                'donation' : this.selectedCharity,
                'points': points
            };

            donations.push(donation);

            var orderObject = {
                "personName": qcssapp.accountModel.get('name'),
                "donations": donations,
                "submittedTime": moment().format("YYYY-MM-DD HH:mm:ss.SSS")
            };

            qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(orderObject), '',
                function(response) {

                    if(!response.transactionID) {
                        qcssapp.Functions.displayPopup('There was an issue completing the donation. Please try again later.', 'Error', 'OK');
                        return;
                    }

                    qcssapp.Functions.displayPopup('Thank you for donating to ' + this.selectedCharity.get('name').trim()+'!', '<div class="donation_success">&#10004;</div>Success', 'OK', this.slideOut);

                    this.parentView.updateProgramAfterDonation(donation);

                }.bind(this),
                '', '', '', '', false, false, true
            );
        },

        displayCharitySelector: function() {
            this.donations.sort(this.sortByName);

            $.each(this.donations, function( index, mod ) {
                this.renderCharityLine( mod );
            }.bind(this) );

            this.$charitySelect.selectric({disableOnMobile:false, nativeOnMobile:false});
            this.$charitySelect.val(this.$charitySelect.find('option').attr('value')).selectric('refresh');
            this.$charitySelectric = this.$el.find('.selectric-donation_charity-select');
        },

        renderCharityLine: function( mod ) {
            var charitySelectLine = new qcssapp.Views.CharitySelectLineView({
                model:mod
            });

            this.$charitySelect.append(charitySelectLine.render().el);
        },

        selectCharity: function() {
            var charityId = this.$charitySelect.val();

            if(charityId != "0") {
                this.$donationInfo.fadeIn();
                this.$charitySelectric.find('.label').addClass('selected');
                this.$donationDescription.removeClass('expanded');
                this.$donationMore.hide();
                this.$donationLess.hide();
            }

            $.each(this.donations, function( index, mod ) {
                if(mod.get('id') == charityId) {
                    this.selectedCharity = mod;
                }
            }.bind(this) );

            if(this.selectedCharity != null) {
                this.$donationTitle.text(this.selectedCharity.get('name'));
                this.$donationDescription.html(this.selectedCharity.get('description'));

                this.calculateDescriptionHeight();
            }
        },

        sortByName: function(a, b) {
            var aName = a.get('name').toLowerCase();
            var bName = b.get('name').toLowerCase();

            return((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
        },

        calculateDescriptionHeight: function() {
            var containerHeight = this.$el[0].offsetTop;
            var titleHeight = this.donations.length > 1 ? this.$el.find('.selectric-donation_charity-select')[0].clientHeight : this.$el.find('.donation_title')[0].clientHeight;
            var balanceHeight = this.$el.find('.donation_balance-container')[0].clientHeight;
            var enterPtsHeight = this.$el.find('.donation_enter-pts-container')[0].clientHeight;
            var btnContainerHeight = this.$el.find('.donation_btn-container')[0].clientHeight;

            if(containerHeight == 0 || balanceHeight == 0) {
                return;
            }

            var totalElementHeight = titleHeight + balanceHeight + enterPtsHeight + btnContainerHeight;

            var descriptionMaxHeight = containerHeight - totalElementHeight;
            this.$donationDescription.css('max-height', descriptionMaxHeight);

            if(this.$donationDescription[0].scrollHeight > descriptionMaxHeight) {
                $("<style> .donation_description.expanded {max-height:"+this.$donationDescription[0].scrollHeight+"px !important;}</style>").appendTo($('head'));

                this.$donationMore.show();

                var donationOffsetTop  = this.$donationDescription[0].offsetTop > 0 ? this.$donationDescription[0].offsetTop : 0;
                var donationHeight = descriptionMaxHeight + donationOffsetTop;
                var donationScrollHeight = this.$donationDescription[0].scrollHeight + donationOffsetTop;
                var moreHeight = this.$donationMore[0].clientHeight;
                this.$donationMore.css('top', donationHeight - moreHeight);
                this.$donationLess.css('top', donationScrollHeight - 10);
            }
        },

        expandDescription: function() {
            if(this.$donationDescription.hasClass('expanded')) {
                this.$donationDescription.removeClass('expanded');
                this.$donationMore.fadeIn(700);
                this.$donationLess.hide();
            } else {
                this.$donationMore.hide();
                this.$donationDescription.addClass('expanded');
                this.$donationLess.fadeIn(900);
            }
        },

        slideIn: function() {
            this.hidden = false;
            this.$el.fadeIn(150, function() {
                this.$el.removeClass('hidden');
                this.calculateDescriptionHeight();
            }.bind(this));
        },

        slideOut: function() {
            this.hidden = true;
            this.$el.addClass('hidden');

            $('#rewards-programs-list').removeAttr('data-reload');

            setTimeout(function() {
                this.undelegateEvents();
                $(this.el).empty();
                this.remove();
            }.bind(this), 800);
        }

    });
})(qcssapp, jQuery);