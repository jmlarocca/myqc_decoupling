;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.OrderReview - View for displaying order review
     */
    qcssapp.Views.OrderReview = Backbone.View.extend({
        internalName: "Order Review View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#reviewview .pages', //references existing HTML element for order review - can be referenced as this.$el throughout the view

        template: _.template( $('#review-template').html() ),

        events: {
//            'click #fav-icon': 'handleFavIconClick' -- no longer using backbone event for click event, using $(document).on('click'
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in order review view");
            }

            this.options = options || '';

            if(typeof this.options.isMobilePhone !== 'undefined') {
                this.isMobilePhone = this.options.isMobilePhone;
                delete options.isMobilePhone;  //remove this property so its not added to the orderObject
            }

            if(typeof this.options.futureOrderDate !== 'undefined' && (this.options.futureOrderDate == "" || this.options.futureOrderDate == null) ) {
                delete options.futureOrderDate;  //remove this property so its not added to the orderObject
            }

            //to store the original orderObject without any modifications
            qcssapp.orderObject = $.extend(true, {}, options);

            delete qcssapp.orderObject.showPaymentMethod;
            delete qcssapp.orderObject.paymentMethod;
            delete qcssapp.orderObject.personID;
            delete qcssapp.orderObject.userBalance;
            delete qcssapp.orderObject.accountFundingDetailModel;

            this.showDiscount = false;
            this.showLocation = false;
            this.showRewards = false;
            this.showTax = false;
            this.showCreditCardFee = false;
            this.showSurcharge = false;
            this.showPaymentMethod = false;
            this.showDiningOption = false;
            this.showPhone = false;
            this.showComments = false;
            this.favoriteOrderId = '';
            this.creditCardFeeLabel = '';

            this.options.totalText = '';
            this.options.subtotalText = '';
            this.options.subtotalDiscountText = '';
            this.options.rewardsTotalText = '';
            this.options.deliveryFeeText = '';
            this.options.taxText = '';
            this.options.creditCardFeeText = '';
            this.options.voucherTotalText = '';
            this.options.nonVoucherTotalText = '';

            this.options.voucherText = '';

            if(qcssapp.Functions.checkServerVersion(3,0,62)) {
                qcssapp.orderObject.token = qcssapp.CurrentOrder.token;
            }

            if(!qcssapp.Functions.checkServerVersion(4,0,58)) {
                delete qcssapp.orderObject.grabAndGo;
            }

            $.each(qcssapp.orderObject.products, function( index, prod ) {
                delete prod.modifierMenus;
                delete prod.lastUpdateDTM;
                delete prod.lastUpdateCheck;
            });

            if(this.options.readyTime != '') {
                this.options.readyEstimate = this.options.readyTime;
                if(qcssapp.futureOrderDate) {
                    this.options.readyEstimate = this.options.readyTime + ' on ' + qcssapp.Functions.formatFutureDay() + ' ' + qcssapp.Functions.formatFutureDate();
                }
            }

            if ( qcssapp.Functions.isNumeric(this.options.tax) && Number(this.options.tax) != 0 ) {
                this.showTax = true;
            }

            //check for valid subtotal discount
            if ( qcssapp.Functions.isNumeric(this.options.subtotalDiscount) && Number(this.options.subtotalDiscount) != 0 ) {
                this.showDiscount = true;

                //almost always will be negative, comes in as -0.77, need to strip the negative to get it to -$0.77
                if ( Number(this.options.subtotalDiscount) < 0 ) {
                    this.options.subtotalDiscountText = qcssapp.Functions.formatPrice( Math.abs(Number(this.options.subtotalDiscount)), '-');
                    this.options.subtotalDiscount = "-$ " + Math.abs(Number(this.options.subtotalDiscount)).toFixed(2);
                } else {
                    this.options.subtotalDiscountText = qcssapp.Functions.formatPrice(Number(this.options.subtotalDiscount));
                    this.options.subtotalDiscount = "$ " + Number(this.options.subtotalDiscount).toFixed(2);
                }
            }

            //delivery requires location, show the location field on the order summary if delivery order
            if ( this.options.type == "delivery") {
                this.showLocation = true;
                qcssapp.orderObject.location = qcssapp.Functions.htmlDecode(qcssapp.orderObject.location);

                this.showDeliveryInput = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('showDeliveryInput') ? qcssapp.CurrentOrder.storeModel.get('showDeliveryInput') : true;
                if(!this.showDeliveryInput) {
                    this.showLocation = false;
                }
            }

            if ( qcssapp.Functions.isNumeric(this.options.subtotal) ) {
                this.options.subtotal = Number(this.options.subtotal).toFixed(2);
                this.options.subtotalText = qcssapp.Functions.formatPrice(Number(this.options.subtotal));
            }

            if ( this.options.rewardsTotal && qcssapp.Functions.isNumeric(this.options.rewardsTotal) ) {
                this.options.rewardsTotalText = qcssapp.Functions.formatPrice(Number(this.options.rewardsTotal));
                this.options.rewardsTotal = "$ " +Number(this.options.rewardsTotal).toFixed(2);
                this.showRewards = true;
            } else {
                this.options.rewardsTotal = 0;
            }

            if ( qcssapp.Functions.isNumeric(this.options.tax) ) {
                this.options.tax = Number(this.options.tax).toFixed(2);
                this.options.taxText = qcssapp.Functions.formatPrice(Number(this.options.tax));
            }

            if ( this.options.voucherCode && qcssapp.Functions.isNumeric(this.options.voucherTotal) ) {
                this.options.voucherText = "Voucher (" + this.options.voucherCode + ")";
                this.options.voucherTotal = Number(this.options.voucherTotal).toFixed(2);
                this.options.voucherTotalText = qcssapp.Functions.formatPrice(Number(this.options.voucherTotal));
            } else {
                this.options.voucherTotal = 0;
            }

            if ( qcssapp.Functions.isNumeric(this.options.total) ) {
                this.options.total = Number(this.options.total).toFixed(2);
                this.options.totalText = qcssapp.Functions.formatPrice(Number(this.options.total));

                this.options.nonVoucherTotal = (Number(this.options.total) - Number(this.options.voucherTotal)).toFixed(2);
                this.options.nonVoucherTotalText = qcssapp.Functions.formatPrice(Number(this.options.nonVoucherTotal));
            }

            this.options.prepTime = qcssapp.CurrentOrder.storeModel.attributes.pickUpPrepTime;
            if ( qcssapp.CurrentOrder.orderType == "delivery" ) {
                this.options.prepTime += qcssapp.CurrentOrder.storeModel.attributes.deliveryPrepTime;
            }

            this.options.storeName = qcssapp.CurrentOrder.storeModel.attributes.header1;

            if(this.options.phone != "") {
                this.showPhone = true;
                this.options.phone = qcssapp.Functions.htmlDecode(this.options.phone);
            }

            if(typeof this.isMobilePhone !== 'undefined' && this.isMobilePhone) {
                this.options.phone = qcssapp.Functions.htmlDecode(this.options.mobilePhone);

                if(this.options.phone != '') {
                    this.showPhone = true;
                }
            }

            this.showPhoneNumberInput = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('showPhoneNumberInput') ? qcssapp.CurrentOrder.storeModel.get('showPhoneNumberInput') : true;
            if(!this.showPhoneNumberInput) {
                this.showPhone = false;
            }

            if(this.options.comments != "") {
                this.showComments = true;
            }

            this.options.diningOption = '';
            if(typeof this.options.diningOptionPAPluName !== 'undefined' && this.options.diningOptionPAPluName != "") {
                this.showDiningOption = true;
                this.options.diningOption = this.options.diningOptionPAPluName;
            }

            if(typeof this.options.creditCardFee !== 'undefined' && Number(this.options.creditCardFee) > 0) {
                this.options.creditCardFee = this.options.creditCardFee.toFixed(2);
                this.options.creditCardFeeText = qcssapp.Functions.formatPrice(Number(this.options.creditCardFee));
                this.showCreditCardFee = true;
                this.creditCardFeeLabel = qcssapp.Functions.htmlDecode(this.options.creditCardFeeLabel);
            } else {
                this.options.creditCardFee = "";
                this.options.creditCardFeeLabel = "";
            }

            if (typeof this.options.showPaymentMethod !== 'undefined' && this.options.showPaymentMethod && this.options.chargeAtPurchaseTime &&
                (Number(this.options.voucherTotal) || Number(this.options.nonVoucherTotal)))
            {
                this.showPaymentMethod = true;
                if(this.options.paymentMethod && this.options.paymentMethod.indexOf('XXXX') != -1) {
                    this.options.paymentMethod = this.options.paymentMethod.replace('XXXX', 'ending in')
                }
            }

            this.enableFavorites = !(typeof qcssapp.CurrentOrder.storeModel.get('enableFavorites') !== 'undefined' && !qcssapp.CurrentOrder.storeModel.get('enableFavorites'));

            var that = this;
            if(typeof this.options['itemSurcharges'] !== 'undefined' && this.options['itemSurcharges'].length > 0) {
                $.each(this.options['itemSurcharges'], function() {
                    if(Number(this.amount) > 0) {
                        that.showSurcharge = true;
                    }
                });
            }

            if(qcssapp.CurrentOrder.storeModel && typeof qcssapp.CurrentOrder.storeModel.get('chargeAtPurchaseTime') !== 'undefined' && !qcssapp.CurrentOrder.storeModel.get('chargeAtPurchaseTime')) {
                this.enableFavorites = false;
            }

            if(qcssapp.Functions.checkServerVersion(3, 0) && this.enableFavorites) {
                var favoriteIconTemplate = _.template('<img src="{{= icon}}" alt="Favorite Order"  id="fav-icon" data-favID="{{= favoriteOrderId}}"/>');
                qcssapp.Functions.isFavoriteOrder('', function(data){
                    var icon;
                    if(!data || data.length < 1) {
                        icon = 'images/icon-favorites-unchecked.svg';
                    } else {
                        if(!data[0].ACTIVE) {
                            icon = 'images/icon-favorites-unchecked.svg';
                        } else {
                            icon = 'images/icon-favorites.svg';
                        }
                        that.favoriteOrderId = data[0].FAVORITEORDERID;
                    }
                    $('#review-favorite-icon').html(favoriteIconTemplate({icon: icon, favoriteOrderId: that.favoriteOrderId}));

                    that.formatButtons();
                }, true);
            }

            this.render();
        },

        render: function() {
            var that = this;

            this.$el.html( this.template( this.options ) );

            this.$el.find('.edit-order-icon').on('click', function() {
                $('#reviewview').find('.order-back-icon').trigger('click');
            });

            this.$productList = this.$el.find("#review-products");

            //show optional fields as necessary
            this.showDiscount ? $('.review-order-discount').addClass('detail-show') : $('.review-order-discount').removeClass('detail-show');
            this.showRewards ? $('.review-order-rewards').addClass('detail-show') : $('.review-order-rewards').removeClass('detail-show');
            this.showTax ? $('.review-tax').addClass('detail-show') : $('.review-tax').removeClass('detail-show');
            this.showCreditCardFee ? $('.review-credit-card-fee').addClass('detail-show') : $('.review-credit-card-fee').removeClass('detail-show');

            $('.review-location').toggle( this.showLocation );
            $('.review-surcharge').toggle( this.showSurcharge );
            $('.review-dining-option').toggle( this.showDiningOption );
            $('.review-phone').toggle( this.showPhone );
            $('.review-comment').toggle( this.showComments );

            if( !this.showComments && !this.showPhone ) {
                $('.review-separator').toggle( false );
            }

            $('.review-payment').toggle(this.showPaymentMethod);
            Number(this.options.voucherTotal) ? $('.review-voucher').addClass('detail-show') : $('.review-voucher').removeClass('detail-show');
            Number(this.options.nonVoucherTotal) ? $('.review-non-voucher').addClass('detail-show') : $('.review-non-voucher').removeClass('detail-show');

            this.showOrderTypeAndTimeMsg = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('showOrderTypeAndTimeMsg') ? qcssapp.CurrentOrder.storeModel.get('showOrderTypeAndTimeMsg') : true;
            this.showPickupDeliveryLocationMsg = qcssapp.CurrentOrder.storeModel.attributes.hasOwnProperty('showPickupDeliveryLocationMsg') ? qcssapp.CurrentOrder.storeModel.get('showPickupDeliveryLocationMsg') : true;

            var $reviewType = $('.review-type');
            $reviewType.show();

            if(qcssapp.CurrentOrder.grabAndGo || (!this.showOrderTypeAndTimeMsg && !this.showPickupDeliveryLocationMsg)) {
                $reviewType.hide();

            } else if (!this.showPickupDeliveryLocationMsg) {
                $reviewType.find('.review-order-type').hide();

            } else if (!this.showOrderTypeAndTimeMsg) {
                $reviewType.find('.review-order-time, .review-order-estimated').hide();
                $reviewType.find('.review-order-type').text($reviewType.find('.review-order-type').text().replace(',', ''));
            }

            if( this.showSurcharge ) {
                this.renderSurchargeLine();
            }

            //render products using the CurrentOrder.products for combos
            if(qcssapp.Functions.checkServerVersion(3,0)) {
                this.renderProducts();

            } else {

                //render products
                $.each(this.options.products, function() {
                    var mod = this;

                    //set the total based on the product's parameters
                    if (Number(mod.quantity) > 1 ) {
                        mod.total = Number(mod.price) * Number(mod.quantity);
                    } else {
                        mod.total = Number(mod.price);
                    }

                    mod.total = mod.total.toFixed(2);

                    var productModel = new qcssapp.Models.ReviewLine(mod);

                    //render product
                    that.renderLine(productModel);
                });

                this.formatButtons();
            }
        },

        //render products using CurrentOrder.products
        renderProducts: function() {

            //create product collection from the currentOrder objects
            this.productCollection = new qcssapp.Collections.Cart();

            var productCollectionArray = [];

            var productsList = [];

            //if they do not match then dining option product was added
            if(qcssapp.CurrentOrder.products.length != this.options.products.length && this.options.diningOptionPAPluID != "") {
                var diningOptionsProduct = this.options.products[0]; //dining option product is appened first
                var productModel = new qcssapp.Models.Product( diningOptionsProduct );
                productModel.set('productID', productModel.get('id'));
                productModel.set('diningOptionProduct', true);

                //need to add dining option product first, then the other products. Don't want to add dining option product to CurrentOrder in case you go back
                productsList.push(productModel);
                productsList = $.merge(productsList, qcssapp.CurrentOrder.products);

                qcssapp.CurrentOrder.products = productsList;
                qcssapp.Functions.updateNumberOfProductsInCart();

            } else  {
                productsList = qcssapp.CurrentOrder.products;
                qcssapp.Functions.updateNumberOfProductsInCart();
            }

            var diningOptionPAPluID = this.options.diningOptionPAPluID;
            var productIDsNeedUpdating = false;
            var that = this;

            $.each(productsList, function(index) {
                var product = _.findWhere(that.options.products,{id:this.get("productID")});
                if(product["fixedTareWeight"]!= undefined && product["fixedTareWeight"] != "" &&
                    product["tareName"] != undefined && product["tareName"]!= ""){
                    this.set("grossWeightInfo","<div>Gross Weight: " +(Number(product["quantity"]) + Number(product["fixedTareWeight"]) ).toFixed(2)+" lbs</div>");
                    this.set("tareInfo","<div>"+ product["tareName"] +": "+ product["fixedTareWeight"] +" lbs</div>");
                }
                var productLineObj = {
                    id: this.get('id'),
                    cartProductID: this.get('productID'),
                    name: this.get('name'),
                    price: this.get('price'),
                    originalPrice: this.get('originalPrice'),
                    basePrice: this.get('basePrice'),
                    quantity: this.get('quantity'),
                    total: Number(this.get('price')) * Number(this.get('quantity')),
                    modifiers: this.get('modifiers'),
                    modifiersList: this.get('modifiersList'),
                    comboModel: this.get('comboModel'),
                    comboTransLineItemId: this.get('comboTransLineItemId'),
                    upcharge: this.get('upcharge'),
                    scaleUsed: this.get('scaleUsed'),
                    tareName: this.get("tareName"),
                    fixedTareWeight: this.get("fixedTareWeight"),
                    tareInfo:this.get("tareInfo"),
                    grossWeightInfo:this.get("grossWeightInfo"),
                    prepOption: this.get('prepOption'),
                    healthy: this.get('healthy'),
                    vegetarian: this.get('vegetarian'),
                    vegan: this.get('vegan'),
                    glutenFree: this.get('glutenFree')
                };

                productCollectionArray.push(productLineObj);
            });

            this.productCollection.reset(productCollectionArray);

            var comboInProgressID = "";

            this.productCollection.each(function( mod ) {
                //calculate the total
                mod.set('total', mod.get('quantity') * mod.get('price'));

                //if the product does not contain a comboModel then add product line
                if( mod.get('comboTransLineItemId') == "" || mod.get('comboTransLineItemId') == null ) {
                    this.renderLine( mod , false );
                    comboInProgressID = "";

                    //if the combo still needs to list out details, add product line as combo product line
                } else if (comboInProgressID == mod.get('comboTransLineItemId')) {
                    this.renderLine( mod, true );

                    //otherwise adding a new combo so add the title combo line first then a line for the product
                } else {
                    this.renderComboLine( mod );
                    this.renderLine( mod, true );
                    comboInProgressID = mod.get('comboTransLineItemId');
                }

            }, this );

            if( !this.enableFavorites ) {
                this.formatButtons();
                qcssapp.Functions.hideMask();
            }
        },

        renderLine: function( mod, isCombo ) {
            if(isCombo) {
                mod.set('isCombo', true);
            }

            var reviewLineView = new qcssapp.Views.reviewLineView({
                model: mod
            });

            //render the product line
            this.$productList.append(reviewLineView.render().el);

            //if has a discount
            if ( qcssapp.Functions.isNumeric(mod.attributes.discount) ) {
                if ( Number(mod.attributes.discount) < 0 ) {
                    mod.attributes.discount = "-$ " + Math.abs(Number(mod.attributes.discount)).toFixed(2);
                } else {
                    mod.attributes.discount = "$ " + Number(mod.attributes.discount).toFixed(2);
                }

                //create new model and view for the discount
                var reviewLineDiscountModel = new qcssapp.Models.ReviewLine({name: "Product Discount", total: mod.attributes.discount});

                var reviewLineDiscountView = new qcssapp.Views.reviewLineDiscountView({
                    model:reviewLineDiscountModel
                });

                //render discount under product
                this.$productList.append(reviewLineDiscountView.render().el);
            }
        },

        renderComboLine: function( mod ) {
            mod.set('review', true);

            var comboLineView = new qcssapp.Views.comboLineView({
                model:mod
            });

            this.$productList.append(comboLineView.render().el);
        },

        renderSurchargeLine: function( ) {
            $.each(this.options.itemSurcharges, function() {
                var surchargeModel = new qcssapp.Models.SurchargeLine(this);

                var surchargeLineView = new qcssapp.Views.SurchargeLineView({
                    model: surchargeModel
                });

                $('.review-surcharge').append(surchargeLineView.render().el);

            });
        },

        toggleBtnImage: function() {
            var icon = $('#fav-icon');
            var src = icon.attr('src') === 'images/icon-favorites-unchecked.svg' ? 'images/icon-favorites.svg' : 'images/icon-favorites-unchecked.svg';
            icon.attr('src', src);
        },

        formatButtons: function() {
            var that = this;

            if(qcssapp.CurrentOrder.storeModel && typeof qcssapp.CurrentOrder.storeModel.get('chargeAtPurchaseTime') !== 'undefined' && !qcssapp.CurrentOrder.storeModel.get('chargeAtPurchaseTime')) {
                $('.review-final-msg').text('Your order will be finalized after pressing "Place Order"');
            }

            setTimeout(function() {
                var $submitOrder = $('#submitOrder');
                if($submitOrder.length) {
                    $submitOrder.remove();
                }

                var offScreen = this.isPlaceOrderOffScreen();
                var appendTo = "#review-footer-button-container";
                var isUnavailable = $(appendTo).hasClass('unavailable');

                if (offScreen) {
                    $('.scroll-indicator').css("bottom", "85px");
                    $('#placeOrderButtonContainer').empty().show();
                    appendTo = $.find('#placeOrderButtonContainer');
                } else {
                    $('#placeOrderButtonContainer').hide();
                }

                if(isUnavailable) {
                    $('#placeOrderButtonContainer').addClass('unavailable');
                } else {
                    $('#placeOrderButtonContainer').removeClass('unavailable');
                }

                new qcssapp.Views.ButtonView({
                    text: "PLACE ORDER",
                    buttonType: "customCB",
                    appendToID: appendTo,
                    id: "submitOrder",
                    class: "align-center order-button prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                    callback: function() {
                        if($('#submitOrder').parent().hasClass('unavailable')) {
                            return;
                        }

                        if ( !qcssapp.ssoLogin && (( that.options.forceReAuth && that.options.reAuthTypeID == 1 ) || that.options.reAuthTypeID == 2) ) {
                            qcssapp.Functions.showMask();
                            $('#reauth').off();
                            new qcssapp.Views.ReAuth(that.options);
                            qcssapp.Router.navigate('reauth', {trigger:true});

                        } else if ( !qcssapp.orderObject.chargeAtPurchaseTime && typeof qcssapp.CurrentOrder.storeModel.get('openTxnPopupMessage') !== 'undefined' && qcssapp.CurrentOrder.storeModel.get('openTxnPopupMessage') != null && qcssapp.CurrentOrder.storeModel.get('openTxnPopupMessage') != "" ){
                            qcssapp.Functions.openTransactionAlert(qcssapp.CurrentOrder.storeModel.get('openTxnPopupMessage'));

                        } else {
                            qcssapp.Functions.showMask();
                            qcssapp.Functions.submitOrder();
                        }

                        if (offScreen) {
                            $('.scroll-indicator').css("bottom", "");
                        }
                    }.bind(this)
                });


            }.bind(this), 200);
        },

        isPlaceOrderOffScreen: function() {
            var headerHeight = $('.review-header')['0'].clientHeight;
            var productsHeight = $('.review-order-products')['0'].clientHeight;
            var subtotalHeight = $('.review-subtotal')['0'].clientHeight;
            var totalHeight = $('.review-total')['0'].clientHeight;
            var paymentHeight = $('.review-payment')['0'].clientHeight;
            var msgHeight = $('.review-final-msg')['0'].clientHeight + 25;

            var elementsTotalHeight = headerHeight + productsHeight + subtotalHeight + totalHeight + paymentHeight + msgHeight;
            var pageHeight = $('#review-page')["0"].clientHeight;

            var heightLeft = pageHeight - elementsTotalHeight;

            return heightLeft < 60;
        }
    });

    //using page click event instead of backbone click event.  Event triggers were bubbling up with other event handler, one click would fire as multiple after visiting the page multiple times
    $(document).on('click', '#fav-icon', function(e) {
        var favoriteOrderID = $(this).attr('data-favID');
        if(favoriteOrderID !== '') {
            qcssapp.Functions.toggleFavoriteOrder(favoriteOrderID, function(data) {
                var icon = $('#fav-icon');
                var src = icon.attr('src') === 'images/icon-favorites-unchecked.svg' ? 'images/icon-favorites.svg' : 'images/icon-favorites-unchecked.svg';
                icon.attr('src', src);
            });
        } else {
            // Create new Favorite
            qcssapp.Functions.createFavoriteOrder('', function(data) {
                var icon = $('#fav-icon');
                icon.attr('data-favID', data.favoriteOrderId);

                var src = icon.attr('src') === 'images/icon-favorites-unchecked.svg' ? 'images/icon-favorites.svg' : 'images/icon-favorites-unchecked.svg';
                icon.attr('src', src);
            })
        }
    });
})(qcssapp, jQuery);