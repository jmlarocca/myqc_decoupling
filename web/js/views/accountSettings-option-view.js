(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.AccountSettingsOption - View for displaying spending profile and account group options individually
     */
    qcssapp.Views.AccountSettingsOption = Backbone.View.extend({
        internalName: "Account Settings Option View", //NOT A STANDARD BACKBONE PROPERTY

        // Cache the template function for a single item.
        template: _.template( $('#account-settings-option-template').html() ),

        render: function() {
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            if(typeof this.model.get('value') !== 'undefined') {
                this.model.set('value', qcssapp.Functions.htmlDecode(this.model.get('value')));
            }

            this.setElement( this.template( this.model.attributes ) );

            return this;
        }
    });
})(qcssapp, jQuery);

