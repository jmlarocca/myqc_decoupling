;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ModalLineView - View for displaying modal line items
     */
    qcssapp.Views.ModalLineView = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'modal-line',

        // Cache the template function for a single item.
        template: _.template( $('#modal-line-template').html() ),

        initialize: function() {

            this.model.set('priceText', this.model.get('price'));

            if(this.model.get('price').indexOf('$') != -1) {
                var price = this.model.get('price').replace('$', '');
                this.model.set('priceText', qcssapp.Functions.formatPriceInApp(price));
            }

            //determine which template to use i.e. left, right, center, bottom, or full
            this.template = _.template( $('#'+this.model.attributes.templateID).html() );

            return this;
        },

        render: function() {
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            if(this.model.get('modalLineID')) {
                this.$el.attr('data-modalLineID', this.model.get('modalLineID'));
            }

            return this;
        }
    });
})(qcssapp, jQuery);
