;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.instanceCodeSelectView - View for displaying instance code Collections
     */
    qcssapp.Views.instanceCodeSelectView = Backbone.View.extend({
        internalName: "Instance Code Select View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#loginCodeSelect', //references existing HTML element for instance code Collections - can be referenced as this.$el throughout the view

        initialize: function () {
            if (qcssapp.DebugMode) {
                console.log("initialize in instance code select view");
            }

            //bind "this" reference to functions in the view
            _.bindAll(this, "fetchSuccess", "fetchError");

            //create new collection
            this.collection = new qcssapp.Collections.instanceCodes();

            //populates the new collection, true parameter to reset/remove collection entries
            this.loadCollection();

            //initialize event listener of "add" to call "renderLine" on the collection
            this.listenTo( this.collection, 'add', this.renderLine );
            this.listenTo( this.collection, 'change', this.renderLine );

            return this;
        },

        fetchError: function() { //handles error for fetching the spending profile collection
            if (qcssapp.DebugMode) {
                console.log("Error loading data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            //display error message to user
            qcssapp.Functions.displayError('There was an error loading the instance code options, please try again later.');
            qcssapp.Functions.hideMask();
        },

        fetchSuccess: function() { //handles successful fetching the spending profile collection
            if (qcssapp.DebugMode) {
                console.log("Successfully loaded data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            //display view
            this.render();
        },

        //calls renderLine on each item in the spending profile collection
        render: function() {
            var deleteCodes = [];

            this.collection.each(function( mod ) {
                if ( mod.get("USED") === 0 ) {
                    deleteCodes.push(mod);
                    return true;
                }

                this.renderLine( mod );
            }, this );

            if ( deleteCodes.length ) {
                $.each(deleteCodes, function() {
                    this.destroy();
                });
            }
        },

        //creates a new view for each spending profile option item and assigns the model, then displays it on the page
        renderLine: function( mod ) {
            var code = mod.attributes.CODE;
            var $existingOption = $('#'+code);

            if ( $existingOption.length ) {
                $existingOption.remove();
            }
            var instanceCodeOptionView = new qcssapp.Views.instanceCodeOptionView({
                model: mod
            });
            this.$el.append( instanceCodeOptionView.render().el );
        },

        loadCollection: function() {
            //fetch collection to be used in this view
            this.collection.fetch({
                error: this.fetchError,
                success: this.fetchSuccess,
                reset:true
            });
        }
    });
})(qcssapp, jQuery);