;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.MenuItemView - View for displaying Items Collections (Collection Item Models)
     */
    qcssapp.Views.MenuItemView = Backbone.View.extend({
        //is a list tag.
        tagName:  'li',

        // Cache the template function for a single item.
        template: _.template( $('#item-template').html() ),

        render: function() {
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            // custom links for app stores
            if(this.$el.find('a').attr("id") == "nav-iosStore"){
                this.$el.find('img').attr('src','images/email/apple.png');
                this.$el.find('a').attr('onclick',"cordova.InAppBrowser.open('https://itunes.apple.com/us/app/my-quickcharge/id1059849685','_system')");
                return this;
            }
            if(this.$el.find('a').attr('id') == "nav-androidStore"){
                this.$el.find('img').attr('src','images/email/android.png');
                this.$el.find('a').attr('onclick',"cordova.InAppBrowser.open('https://play.google.com/store/apps/details?id=com.mmhayes.myqc.alpha','_system')");
                return this;
            }

            //add navMenuLink class for all menu items but logout
            if ( this.$el.find('a').attr('id') != "nav-logout" ) {
                this.$el.addClass('navMenuLink');
                return this;
            }

            if ( qcssapp.ssoLogin ) {
                // for logout - DUE TO CORS Need an href NOT a js ajax call.
                var logoutURL = qcssapp.Location+'/sso/sls?dskey='+qcssapp.Session;
                var $logoutAnchor = this.$el.find('a');

                // on mobile, need to go back over to the mobile browser
                if( typeof window.cordova !== "undefined" ) {
                    $logoutAnchor.on('click', function(e) {
                        e.preventDefault();
                        qcssapp.Functions.resetOnLogout(true);
                        new qcssapp.Views.LogoutView();
                        qcssapp.Router.navigate('logout-page', {trigger:true});
                        cordova.InAppBrowser.open(logoutURL, '_system');
                    });
                } else {
                    $logoutAnchor.prop('href', logoutURL);
                }
            }

            return this;
        }
    });
})(qcssapp, jQuery);
