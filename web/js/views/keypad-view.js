;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.KeypadView - View for displaying Keypads
     */
    qcssapp.Views.KeypadView = Backbone.View.extend({
        internalName: "Keypad View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#keypadview', //references existing HTML element for Keypads - can be referenced as this.$el throughout the view

        template: _.template( $('#keypad-template').html() ),

        events: {
            'click #myQuickPicks': 'goToMyQuickPicks'
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in keypad view");
            }

            qcssapp.Functions.resetErrorMsgs();

            this.options = options || '';

            this.keypadLineViews = [];

            this.keypadItemCollection = new qcssapp.Collections.KeypadItems();

            this.keypadItemCollection.set(this.options.items);

            this.listenTo(this.keypadItemCollection, 'change:imageLoaded', this.loadImage);

            this.render();

            this.nextOneFilled = 0;
            this.keypadLines = 0;

            var count = 0;
            $.each(this.$keypad.find('.keypad-line'), function() {
                if($(this).hasClass('hasImage')) {
                    $(this).attr('priority', count);
                    count++;
                }
            });
        },

        //creates keypad page in keypadview, calls renderLine on each keypad item
        render: function() {
            this.$el.find('.pages').append( this.template( this.options ) );
            this.$keypad = $('#keypad-'+this.options.id);
            this.$keypadList = this.$keypad.find('.keypad-list');
            var $keypadHeader = $('.keypad-header');
            this.$helpIcon = this.$el.find('.help-icon');
            this.$helpIcon.addClass('hidden');
            this.$el.find('#keypad-header').removeClass('help-keypad');

            //determine if a substitute product was passed in from a combo keypad
            this.substituteProduct = typeof this.options.substituteProduct !== 'undefined' ? this.options.substituteProduct : "";

            //if there is no keypad description don't show padding below header
            this.options.description == "" ? $keypadHeader.hide() : '';

            if(qcssapp.Functions.checkServerVersion(2,0) && typeof qcssapp.CurrentOrder.storeModel.get("enableFavorites") !== 'undefined' && qcssapp.CurrentOrder.storeModel.get("enableFavorites")) {
                this.addMyQuickPicks();
            }

            if ( this.options.items.length > 0 ) {
                var keypadID = this.options.id;
                this.keypadItemCollection.each(function (mod) {
                    mod.set('keypadID', keypadID);
                    this.renderLine(mod);
                }, this);
            }

            this.formatKeypad();

            return this;
        },

        renderLine: function( mod ) {
            //add special 'keypad-line-full' class if the image is going to cover the whole utton
            var className = 'keypad-line';

            //TODO: code this better
            var templateID = qcssapp.Functions.determineTemplate(mod);
            if ( templateID == 'keypad-line-template_full' ) {
                className = 'keypad-line keypad-line_full';
            } else if ( templateID == 'keypad-line-template_original' ) {
                className = 'keypad-line keypad-line_original';
            } else if ( templateID == 'keypad-line-template_left' ) {
                className = 'keypad-line keypad-line_left';
            } else if ( templateID == 'keypad-line-template_right' ) {
                className = 'keypad-line keypad-line_right';
            } else if ( templateID == 'keypad-line-template_center' ) {
                className = 'keypad-line keypad-line_center';
            } else if ( templateID == 'keypad-line-template_bottom' ) {
                className = 'keypad-line keypad-line_bottom';
            } else if ( templateID == 'keypad-line-template_original-below' ) {
                className = 'keypad-line keypad-line_original-below';
            } else if ( templateID == 'keypad-line-template_left-below' ) {
                className = 'keypad-line keypad-line_left-below';
            } else if ( templateID == 'keypad-line-template_right-below' ) {
                className = 'keypad-line keypad-line_right-below';
            }


            var keypadItemView = new qcssapp.Views.KeypadLineView({
                model:mod,
                className:className,
                substituteProduct: this.substituteProduct,
                keypadId: this.options.id
            });

            this.$keypadList.append(keypadItemView.render().el);

            this.keypadLineViews.push(keypadItemView);
        },

        loadImage: function(keypadLineModel) {
            this.keypadLines++;
            var $keypadLine = this.$keypad.find(".keypad-line[data-id='"+keypadLineModel.attributes.id+"']");
            var priority = $keypadLine.attr('priority');

            //if the image that was just loaded is the next one that needs to be filled, show it
            if(priority == this.nextOneFilled) {

                var $keypadLineImage = $keypadLine.offsetParent().find('.line_image');
                if($keypadLine.hasClass('keypad-line_full')) {
                    $keypadLine.removeClass('hideFull');
                } else {
                    $keypadLineImage.removeClass('hideImages');
                    $keypadLineImage.addClass('fadeInImages');
                }

                this.nextOneFilled++;

                //check if the next one that needs to be filled is a 'waiting' image
                var $waitingKeypadLines = this.$keypad.find(".keypad-line.waiting");
                if($waitingKeypadLines.length>0) {
                    this.checkWaitingImages(false);
                }

                //otherwise add class 'waiting' to the image so it can be checked later
            } else {
                $keypadLine.addClass('waiting');
                this.checkWaitingImages(false);
            }

            //check if we've gone through all the images and there are still some 'waiting'
            var $waitingKeypadLinesEnd = this.$keypad.find(".keypad-line.waiting");
            if(this.keypadLines == this.$keypad.find(".keypad-line.hasImage").length &&  $waitingKeypadLinesEnd.length>0) {
                this.atEndLoop = 0;
                this.checkWaitingImages(true);

            }
        },

        //checks the 'waiting' images if their priority is the next one that needs to be loaded
        checkWaitingImages: function(atEnd) {
            var $waitingKeypadLine = this.$keypad.find(".keypad-line.waiting[priority='"+this.nextOneFilled+"']");

            if($waitingKeypadLine.length == 1) {
                var $waitingKeypadLineImage = $waitingKeypadLine.offsetParent().find('.line_image');

                if($waitingKeypadLine.hasClass('keypad-line_full')) {
                    $waitingKeypadLine.removeClass('hideFull');
                    $waitingKeypadLine.removeClass('waiting');
                } else {
                    $waitingKeypadLineImage.removeClass('hideImages');
                    $waitingKeypadLine.removeClass('waiting');
                    $waitingKeypadLineImage.addClass('fadeInImages');
                }

                this.nextOneFilled++;
            }

            //if we've gone through all the images, look at the remainder in waiting and load them in order
            if(atEnd) {
                var $waitingKeypadLines = this.$keypad.find(".keypad-line.waiting");
                if($waitingKeypadLines.length > 0) {
                    this.atEndLoop++;
                    if(this.atEndLoop == 75) {
                        return;
                    }

                    this.checkWaitingImages(true);
                }
            }
        },

        //adds the 'My Quick Picks' keypad to the top of the menu
        addMyQuickPicks: function() {
            if( !qcssapp.Functions.checkServerVersion(1,6) ) {
                return;
            }

            var storeKeypad = qcssapp.OrderMenuHistory[0];
            var currentPage = qcssapp.OrderMenuHistory[qcssapp.OrderMenuHistory.length-1];

            //if this is the root keypad add the My Quick Picks keypad
            if (( qcssapp.OrderMenuHistory.length == 1 || (storeKeypad && storeKeypad.hasOwnProperty('id') && currentPage && currentPage.hasOwnProperty('id') && currentPage.hasOwnProperty('type') && currentPage.type == 'keypad' && storeKeypad.id == currentPage.id)) && (this.options.items.length > 0) ) {
                var string =
                    '<li class="keypad-line" id="myQuickPicks">' +
                        '<div class="keypad-line-flex">' +
                        '<div class="myQuickPicks-container accent-color-two">'+
                        '<div class="keypad-line-quickpicks">My Quick Picks</div>' +
                        '<div class="keypad-line-icons">' +
                        '<img src="images/icon-popular.svg"><img src="images/icon-favorites.svg"><img src="images/icon-recent.svg">'+
                        '<span>Popular, Favorite, & Recent Products</span></div>' +
                        '</div>'+
                        '<div class="keypad-line-info accent-color-two" style="max-width:5%;display:flex;align-items: center;justify-content:flex-end;"><img class="svg show-menu-arrow menu-arrow-svg font-color-primary" src="images/icon-menu-arrow.svg"></div>' +
                        '</div>' +
                        '</li>';
                this.$keypadList.append(string);

                if(qcssapp.Functions.checkServerVersion(3,0)) {
                    var $myQuickPicks = $('#myQuickPicks');
                    $myQuickPicks.find('img').eq(2).remove();
                    $myQuickPicks.find('span').text('Popular and Favorites');
                }

                if(qcssapp.help.allowHelpfulHints) {
                    this.$helpIcon.removeClass('hidden');
                    this.$el.find('#keypad-header').addClass('help-keypad');
                }

                var arrowColor = qcssapp.usingBranding ? $('.accent-color-two').css('color') : '#133d8d';
                qcssapp.Functions.setMenuIconSVG(this.$keypadList.find('.menu-arrow-svg'), arrowColor);

            }
            $('#my-quick-picks-page').off();
        },

        goToMyQuickPicks: function() {
            qcssapp.Functions.addHistoryRecord(0, 'favorites');
            new qcssapp.Views.MyQuickPicksView();
            qcssapp.Router.navigate('favorites', {trigger: true});
        },

        formatKeypad: function() {
            if(qcssapp.keypadPositionID == 2) {
                this.$keypad.addClass('tile-keypad');
            }
        },

        adjustKeypad: function() {
            $.each(this.keypadLineViews, function() {
                var keypadLineView = this;
                keypadLineView.adjustButton();
            });
        }

    });
})(qcssapp, jQuery);