;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.PurchaseRestrictionView - View for purchase restrictions on products based on restriction profiler
     */
    qcssapp.Views.PurchaseRestrictionView = Backbone.View.extend({
        internalName: "Purchase Restriction View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'purchase-restriction-container',

        template: _.template( $('#purchase-restriction-template').html() ),

        initialize: function (options) {
            this.options = options || {};

            this.$container = $(this.options.container);
            this.hidden = this.options.hidden;

            this.purchaseRestrictionProducts = this.options.purchaseRestrictionProducts;
            this.onCheckOut = this.options.onCheckOut;

            this.purchaseRestrictionCollection = new qcssapp.Collections.PurchaseRestrictionCollection();
            this.purchaseRestrictionCollection.set(this.purchaseRestrictionProducts);

            _.bindAll( this, 'render', 'displayButtons', 'renderLine', 'resetView', 'removeProducts' );

            this.render();
        },

        render: function() {

            var msg = qcssapp.Functions.isCreditCardAllowed() ? "You are restricted from using this Payment Method to purchase this product" : "You are restricted from purchasing this product";
            var removeMsg =  "Would you like to remove this product from your order?";

            if(this.purchaseRestrictionCollection.length > 1) {
                msg = qcssapp.Functions.isCreditCardAllowed() ? "You are restricted from using this Payment Method to purchase these products" : "You are restricted from purchasing these products";
                removeMsg = "Would you like to remove these products from your order?";
            }

            if(!this.onCheckOut) {
                msg = "You are restricted from purchasing this product"
            }

            var templateData = {
                msg: msg,
                removeMsg: removeMsg
            };

            this.$el.html( this.template( templateData ) );

            if ( this.hidden ) {
                this.$el.addClass('hidden');
            }

            this.$container.append( this.$el );

            this.displayButtons();

            //purchase restriction related fields
            this.$itemRestrictionList = this.$el.find('.purchase-restriction_item-list');
            this.$itemRestrictionRemoveMsg = this.$el.find('.purchase-restriction_item-remove-msg');
            this.$removeBtn = this.$el.find('#removePurchaseRestrictionBtn');

            if(this.onCheckOut) {
                this.purchaseRestrictionCollection.each(function( mod ) {
                    this.renderLine(mod);
                }, this );

                this.$itemRestrictionList.show();
                this.$itemRestrictionRemoveMsg.show();
                this.$removeBtn.show();
            }

            qcssapp.Functions.hideMask();

            return this;
        },

        //handles the displaying of all buttons on the account funding view
        displayButtons: function() {

            //CLOSE button declaration
            new qcssapp.Views.ButtonView({
                text: 'Close',
                appendToID: '#purchase-restriction_button-container',
                buttonType: 'customCB',
                class: 'template-gen primary-background-color font-color-primary',
                id: 'closePurchaseRestrictionBtn',
                parentView: this,
                callback: function() {
                    this.fadeOut();

                }.bind(this)
            });

            //REMOVE button declaration
            new qcssapp.Views.ButtonView({
                text: 'Remove',
                appendToID: '#purchase-restriction_button-container',
                buttonType: 'customCB',
                class: 'template-gen primary-background-color font-color-primary',
                id: 'removePurchaseRestrictionBtn',
                parentView: this,
                callback: function() {
                    this.removeProducts();

                }.bind(this)
            });
        },

        renderLine: function(mod) {
            var purchaseRestrictionLineView = new qcssapp.Views.PurchaseRestrictionLineView({
                model:mod
            });

            this.$itemRestrictionList.append(purchaseRestrictionLineView.render().el);
        },

        removeProducts: function() {
            this.purchaseRestrictionCollection.models.reverse();

            //remove the ineligible products from the order
            this.purchaseRestrictionCollection.each(function( mod ) {

                if ( mod.get('id') >= 0 ) {
                    qcssapp.CurrentOrder.products.splice(mod.get('id'), 1);
                }

                //if the product is part of a combo, break the combo and remove it
                if(typeof mod.get('comboModel') !== 'undefined' && !$.isEmptyObject(mod.get('comboModel'))) {
                    var comboID = mod.get('comboModel').id;
                    var comboModel = new qcssapp.Models.Combo;
                    qcssapp.CurrentOrder.combos.models.splice(comboID, 1, comboModel);

                    $.each(qcssapp.CurrentOrder.products, function() {
                        if(typeof this.get('comboModel') !== 'undefined' && !$.isEmptyObject(this.get('comboModel')) && this.get('comboModel').id == comboID) {
                            this.set('comboModel', '');
                            this.set('comboTransLineItemId', '');
                            this.set('comboDetailId', '');
                            this.set('upcharge', '');
                        }
                    });
                }

            }, this );

            qcssapp.Functions.updateNumberOfProductsInCart();

            //defect 4092 - Restricted items removed from the middle of an order and re-added causes an issue
            //for each product, reset the ID to be the index
            $.each(qcssapp.CurrentOrder.products, function(curIndex, curProd) {
                curProd.set("id", curIndex);
            });

            this.fadeOut();

            //if there are no more products left, go to the cart
            if( qcssapp.CurrentOrder.products.length == 0 ) {
                qcssapp.Functions.addHistoryRecord(0, 'cart');
                qcssapp.Router.navigate('cart', {trigger: true});
            } else {
                $('#receiveNext').trigger('click');
            }
        },

        resetView: function() {

        },

        fadeIn: function(dur) {
            dur = !dur ? 150 : dur;

            this.hidden = false;
            this.$el.fadeIn(dur, function() {
                this.$el.removeClass('hidden');
            }.bind(this));

            if($('.purchase-restriction_item-list').height() > $('.purchase-restriction_item-list-container').height()) {
                $.toast({
                    heading: 'Many Restricted Products Found',
                    text: 'Be sure to scroll and review the restricted products',
                    textAlign: 'center',
                    loader: false,
                    hideAfter: 5000,
                    position: 'top-center',
                    allowToastClose: false
                });
            }
        },

        fadeOut: function(dur) {
            dur = !dur ? 150 : dur;

            this.hidden = false;
            this.$el.fadeOut(dur, function() {
                this.$el.addClass('hidden');
                this.remove();
            }.bind(this));
        },

        hide: function() {
            this.hidden = true;
            this.$el.hide();
        }
    });
})(qcssapp, jQuery);
