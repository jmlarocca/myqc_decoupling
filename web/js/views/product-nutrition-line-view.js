;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ProductNutritionLineView - View for displaying the nutrition line view
     */
    qcssapp.Views.ProductNutritionLineView = Backbone.View.extend({
        internalName: "Product Nutrition Line View",

        // Cache the template function for a single item.
        template: _.template( $('#product-detail-nutrition-template').html() ),

        //events: //events not needed for this view
        render: function() {

            this.setElement(this.template(this.model.toJSON()));
            return this;
        }
    });
})(qcssapp, jQuery);
