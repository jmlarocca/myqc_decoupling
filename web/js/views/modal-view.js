;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.Modal - View for displaying Modals
     */
    qcssapp.Views.Modal = Backbone.View.extend({
        internalName: "Modal View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div', //references existing HTML element for Detail Collections - can be referenced as this.$el throughout the view
        className: 'qcss-modal',

        template: _.template( $('#modal-template').html() ),

        events: {
            'click .modal-line': 'modalLineClick'
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in " + this.internalName);
            }

            //assign the modal options
            this.options = options;

            //id -> modal ID
            this.id = this.options.id;

            //title of the modal
            this.title = this.options.title;

            //template used for rendering each modal line
            this.templateID = this.options.templateID;

            //templateID used for rendering each the column headers below the modal title
            this.columnHeaderTemplateID = this.options.columnHeaderTemplateID;

            //don't show the column header if there is no value for this field in any of the models (would be an empty column all the way down)
            this.columnHeaderDependentField = this.options.columnHeaderDependentField;

            //data used for rendering each modal line
            this.templateData = this.options.templateData;

            //gives a reference to be able to access the parent view's functions
            this.parentView = this.options.parentView;

            //the select view the modal is tied to
            this.selectorID = this.options.selectorID;

            //the text property for the selector
            this.selectorTextProperty = this.options.selectorTextProperty;

            //the value property for the selector
            this.selectorValueProperty = this.options.selectorValueProperty;

            //callback after modal line is selected
            this.callback = this.options.callback;

            this.lineData = '';

            this.template = _.template( $('#modal-template').html() );

            //render the modal
            this.render();
        },

        render: function() {

            this.renderLineData();

            //create new model with given options
            this.model = new qcssapp.Models.Modal({
                title: this.title,
                lineData : this.lineData
            });

            this.$el.html( this.template( this.model.attributes ) );

            this.styleModal();

            if(this.columnHeaderTemplateID) {
                this.renderModalColumnHeaders();
            }

            //added to the active view because there's an overlay over the page that doesn't allow you to do anything without closing or selecting a modal line
            $('.view.active').append(this.$el);

            this.displayModalButtons(); //show the Close button in the modal after its added to the page

            return this;
        },

        renderLineData: function() {
            this.$el.append('<div id="modal-data"></div>'); //temporary element to hold modal lines

            if ( this.templateData.length > 0 ) {
                var templateID = this.templateID;
                var modalLineID = '';
                var that = this;

                //if you send in a collection of models
                if(typeof this.templateData.models !== 'undefined') {
                    this.templateData.each(function (mod) {
                        mod.set('templateID', templateID);  //set the template for the modal-line-view
                        mod.set('modalLineID', mod.get(that.selectorValueProperty));

                        var $selector = $('#'+that.selectorID);
                        if($selector.val() == mod.get(that.selectorValueProperty)) { // add current text
                            mod.set(that.selectorTextProperty, mod.get(that.selectorTextProperty) + ' <span style="font-style:italic">(Current)</span>');
                        }

                        this.renderModalLine(mod);

                        if($selector.val() == mod.get(that.selectorValueProperty)) {  //remove current text
                            mod.set(that.selectorTextProperty, mod.get(that.selectorTextProperty).replace(' <span style="font-style:italic">(Current)</span>',''));
                        }

                    }, this);

                //if you send in an array-list of hashmaps
                } else {
                    $.each(this.templateData, function () {
                        var attributes = this;
                        var selectVal = that.selectorValueProperty;
                        attributes.templateID = templateID;
                        attributes.modalLineID = this[selectVal];
                        this.attributes = attributes;

                        that.renderModalLine(this);
                    });
                }
            }

            this.lineData = this.$el.find('#modal-data').html(); //get modal line html from temporary element and remove it
            this.$el.find('#modal-data').remove();
        },

        //render each modal line using the given template and data
        renderModalLine: function(mod) {
            var modalLineView = new qcssapp.Views.ModalLineView({
                model:mod
            });

            //show the currently selected value in another color
            if($('#'+this.selectorID).val() == mod.get(this.selectorValueProperty)) {
                modalLineView.render().$el.addClass('selected-modal-line');
            }

            this.$el.find('#modal-data').append(modalLineView.render().el);
        },

        renderModalColumnHeaders: function() {

            if(this.columnHeaderDependentField.length) {
                var found = false;
                if ( this.templateData.length > 0 ) {
                    this.templateData.each(function (mod) {

                       if(mod.get(this.columnHeaderDependentField) != "") {
                           found = true;
                       }

                    }, this);
                }

                if(found) {
                    this.$el.find('.modal-title').removeClass('primary-background-color primary-font-color');
                    this.$el.find('.modal-header').append($('#' + this.columnHeaderTemplateID).html());
                    this.$el.find('.modal-title').css({'color': '#000', 'background-color': '#fff'});
                } else {
                    this.$el.find('.modal-title').addClass('primary-background-color primary-font-color');
                }
            } else {
                this.$el.find('.modal-header').append($('#' + this.columnHeaderTemplateID).html());
                this.$el.find('.modal-title').css({'color': '#000', 'background-color': '#fff'});
            }

        },

        //create Close button on modal
        displayModalButtons: function() {

            new qcssapp.Views.ButtonView({
                text: 'Close',
                buttonType: "customCB",
                appendToID: "#modal-button-container",
                id: "closeModalBtn",
                parentView: this,
                class: "align-center modal-close prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                callback: function() {
                    this.parentView.closeModal();
                }
            });
        },

        //general click handling for all modals
        modalLineClick: function(e) {
            e.preventDefault();

            var $modalLine = $(e.currentTarget);
            var that = this;
            this.selectedModel = {};

            this.templateData.each(function( mod ) {
                if(mod.get(this.selectorValueProperty) == $modalLine.attr('data-modalLineID')) {
                    that.selectedModel = mod;
                }
            }, this );

            //if there is a modal callback function, call it
            if ( this.callback ) {
                this.callback(this);

            //otherwise populate the selector with the value
            } else {
                this.selectModalValue();
            }
        },

        //populate the selector with the value and text that is displayed
        selectModalValue: function() {
            var $selector = $('#'+ this.selectorID);

            if($selector) {
                if(this.selectorValueProperty) {  //set the value of the selector
                   $selector.val(this.selectedModel.get(this.selectorValueProperty))
                }

                if(this.selectorTextProperty) {  //set the text for the selector
                    var name = qcssapp.Functions.htmlDecode(this.selectedModel.get(this.selectorTextProperty));
                    $selector.text(name)
                }
            }

            this.closeModal();
        },

        //remove the modal from the active view
        closeModal:function() {
            this.$el.remove();
        },

        //update the modals width, height and position values based on screen size
        styleModal: function() {

            var $activePanel = $('.panel.active');
            var panelWidth = $activePanel.outerWidth();
            var panelHeight = $activePanel.outerHeight();

            var modalWidth = panelWidth * .85;
            var modalHeight = panelHeight * .4;
            if(panelWidth > 415) {
                modalWidth = panelWidth * .6;
                modalHeight = panelHeight * .3;
            }

            var modalLeft = 'calc(50vw - ('+modalWidth+'px / 2))';
            var modalTop =  'calc(40vh - ('+modalHeight+'px / 2))';

            this.$el.find('.modal').css({'width': modalWidth, 'top': modalTop, 'left': modalLeft});

        }

    });
})(qcssapp, jQuery);   