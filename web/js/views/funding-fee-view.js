;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FundingFee - View for displaying payment method models
     */
    qcssapp.Views.FundingFee = Backbone.View.extend({
        tagName: 'div',
        className: 'funding-fee',

        // Cache the template function for a single item.
        template: _.template( $('#funding-fee-template').html() ),

        initialize: function(options) {
            //grab given options
            this.options = options || {};

            if(this.options.templateID) {
                this.template = _.template( $('#'+this.options.templateID).html() );
            }

            //render right away
            this.render();
        },

        render: function() {

            if(Number(this.options.fundingFee) < 0) {
                var fundingFee = Number(this.options.fundingFee) * -1;
                this.options.fundingFee = qcssapp.Functions.formatPriceInApp(fundingFee, '-');
            } else {
                this.options.fundingFee = qcssapp.Functions.formatPriceInApp(this.options.fundingFee);
            }

            if(Number(this.options.fundingTotalCharge) < 0) {
                var fundingTotalCharge = Number(this.options.fundingTotalCharge) * -1;
                this.options.fundingTotalCharge = qcssapp.Functions.formatPriceInApp(fundingTotalCharge, '-');
            } else {
                this.options.fundingTotalCharge = qcssapp.Functions.formatPriceInApp(this.options.fundingTotalCharge);
            }

            if(Number(this.options.fundingAmount) < 0) {
                var fundingAmount = Number(this.options.fundingAmount) * -1;
                this.options.fundingAmount = qcssapp.Functions.formatPriceInApp(fundingAmount, '-');
            } else {
                this.options.fundingAmount = qcssapp.Functions.formatPriceInApp(this.options.fundingAmount);
            }

            if(typeof this.options.taxRateAmount !== 'undefined') {
                if(Number(this.options.taxRateAmount) < 0) {
                    var taxRateAmount = Number(this.options.taxRateAmount) * -1;
                    this.options.taxRateAmount = qcssapp.Functions.formatPriceInApp(taxRateAmount, '-');
                } else {
                    this.options.taxRateAmount = qcssapp.Functions.formatPriceInApp(this.options.taxRateAmount);
                }
            }

            if(typeof this.options.fundingTotalTaxCharge !== 'undefined')  {
                if(Number(this.options.fundingTotalTaxCharge) < 0) {
                    var fundingTotalTaxCharge = Number(this.options.fundingTotalTaxCharge) * -1;
                    this.options.fundingTotalTaxCharge = qcssapp.Functions.formatPriceInApp(fundingTotalTaxCharge, '-');
                } else {
                    this.options.fundingTotalTaxCharge = qcssapp.Functions.formatPriceInApp(this.options.fundingTotalTaxCharge);
                }
            }

            //build payment method view with given options
            this.$el.html( this.template( this.options ));

            return this;
        }
    });
})(qcssapp, jQuery);
