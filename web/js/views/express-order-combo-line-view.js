;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ExpressOrderComboLineView - View for displaying combo lines in the express order
     */
    qcssapp.Views.ExpressOrderComboLineView = Backbone.View.extend({
        //is a list tag.
        tagName:  'li',
        className: 'template-gen express-order-item-line',

        // Cache the template function for a single item.
        template: _.template( $('#express-reorder-combo-line-template').html() ),

        render: function() {

            var numberComboTotal = 0;

            if(this.model.get('comboBaseTotal') != '') {
                var numberTotal = Number(this.model.get('comboBaseTotal')).toFixed(2);
                this.model.set('comboBaseTotal', numberTotal);
                numberComboTotal = numberTotal;
            }

            if(Number(this.model.get('comboBaseTotal')) < 0) {
                var comboBaseTotal = Number(this.model.get('comboBaseTotal')) * -1;
                this.model.set('comboBaseTotal', qcssapp.Functions.formatPriceInApp(comboBaseTotal, '-'));
            } else {
                this.model.set('comboBaseTotal', qcssapp.Functions.formatPriceInApp(this.model.get('comboBaseTotal')));
            }

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            if(this.model.get('quantity') != '' && Number(this.model.get('quantity')) > 1) {
                var price = numberComboTotal;

                if(Number(price) < 0) {
                    var priceStr = Number(price) * -1;
                    price = qcssapp.Functions.formatPriceInApp(priceStr, '-');
                } else {
                    price = qcssapp.Functions.formatPriceInApp(price);
                }

                if(this.model.get('scaleUsed')) {
                    price = Number(this.model.get('quantity')).toFixed(2) + ' lbs at ' + price;
                } else {
                    price = this.model.get('quantity') + ' <span class="quantity">x</span> ' + price;
                }
                this.$el.find('.express-item-line-price').html(price);
            }

            return this;
        }
    });
})(qcssapp, jQuery);
