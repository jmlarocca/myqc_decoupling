;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.HelpPurchaseHistoryView - View for displaying help info on the purchase history page
     */
    qcssapp.Views.HelpPurchaseHistoryView = Backbone.View.extend({
        internalName: "Help Purchase History View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'help-purchase-history-view',

        template: _.template( $('#help-purchase-history-template').html() ),

        events: {
            'click .help-next' : 'nextHelpSlide',
            'click .help-close' : 'closeHelp'
        },

        purchaseHistoryHelp: [
            {
                id: 1,
                title: 'Receipts',
                text: 'Click here to view the receipt for the transaction',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 2,
                title: 'Transaction Type',
                text: 'Click here for more information about the transaction',
                buttonText: 'Done',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }
        ],

        initialize: function (options) {
            this.options = options || {};

            this.parentView = this.options.parentView;

            this.$view = $('#purchasesview');

            var viewHeight = this.$view.find('#transaction-list')[0].clientHeight + 60;

            if(viewHeight > this.$view.height()) {
                this.$view.find('.help-view').height(viewHeight);
            }

            this.foundReceipt = false;

            this.slideEnd = 2;
            this.slideStart = 1;
        },

        render: function() {
            this.$el.html( this.template( {} ) );

            this.renderSlides();

            return this;
        },

        renderSlides: function() {
            $.each(this.purchaseHistoryHelp, function(index, slideInfo) {
                var helpSlideView = new qcssapp.Views.HelpSlideView(slideInfo);

                this.$el.append( helpSlideView.render().$el.find('.help-slide') );

            }.bind(this));

            this.findTransaction();
        },

        findTransaction: function() {
            var $transactionList = this.$view.find('#transaction-list');
            this.$transaction = null;

            $.each($transactionList.find('.transaction-line'), function(index, transaction) {
                var $transaction = $(transaction);

                if(!$transaction.hasClass('no-receipt')) {
                    this.$transaction = $transaction;
                    return false;
                }

            }.bind(this));

            if(this.$transaction == null) {
                this.$transaction = $transactionList.find('.transaction-line').eq(0);
                this.$transaction.addClass('help-purchases');
                this.slideStart = 2;

            } else { //check if transaction is showing on screen
                var transactionOffsetTop = this.$transaction[0].offsetTop;
                var purchasesHeight = this.$view.height();

                if(transactionOffsetTop > purchasesHeight) {
                    $('#show-purchases').animate({scrollTop:transactionOffsetTop},200);
                }

                this.$transaction.addClass('help-purchases');
                this.foundReceipt = true;
            }
        },

        nextHelpSlide: function() {
            //if at the end of the slide length
            if( this.slideStart == this.slideEnd ) {
                this.$transaction.removeClass('help-purchases');
                this.parentView.fadeOut();
                return;
            }

            var $currentSlide = this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]');
            $currentSlide.removeClass('active-slide').addClass('inactive-slide');;

            this.slideStart++;

            var $nextSlide = this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]');
            $nextSlide.addClass('active-slide');
        },

        setSlideStyles: function() {

            if(this.foundReceipt) {
                // Slide 1
                var $helpHandSlide1 = $('.help-slide[data-slide-id="1"] .help-hand');
                var $helpInfoSlide1 = $('.help-slide[data-slide-id="1"] .help-info');
                var $receiptIcon = this.$transaction.find('.trans-receipt-icon');

                if(this.$transaction.length) {
                    var availableSpaceBelow = $('#show-purchases')[0].clientHeight - (this.$transaction[0].offsetTop + this.$transaction[0].clientHeight);
                    var availableSpaceAbove = this.$transaction[0].offsetTop;

                    if(availableSpaceBelow < $helpInfoSlide1[0].clientHeight && availableSpaceAbove > $helpInfoSlide1[0].clientHeight) {
                        $helpInfoSlide1.find('.help-point').removeClass('reverse');
                        $helpHandSlide1.addClass('flip');
                        qcssapp.Functions.setHandTopReverse($receiptIcon, $helpHandSlide1, 20);
                        qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide1, $helpInfoSlide1, 5);
                    } else {
                        qcssapp.Functions.setHandTop($receiptIcon, $helpHandSlide1, 20);
                        qcssapp.Functions.setHelpInfoTop($helpHandSlide1, $helpInfoSlide1, 5);
                    }
                }

                qcssapp.Functions.setHandLeft($receiptIcon, $helpHandSlide1);
            }

            // Slide 2
            var $helpHandSlide2 = $('.help-slide[data-slide-id="2"] .help-hand');
            var $helpInfoSlide2 = $('.help-slide[data-slide-id="2"] .help-info');
            var $transTypeIcon = this.$transaction.find('.trans-type-icon');

            if(this.$transaction.length) {
                var availableSpaceBelow2 = $('#show-purchases')[0].clientHeight - (this.$transaction[0].offsetTop + this.$transaction[0].clientHeight);
                var availableSpaceAbove2 = this.$transaction[0].offsetTop;

                if(availableSpaceBelow2 < $helpInfoSlide2[0].clientHeight && availableSpaceAbove2 > $helpInfoSlide2[0].clientHeight) {
                    $helpInfoSlide2.find('.help-point').removeClass('reverse');
                    $helpHandSlide2.addClass('flip');
                    qcssapp.Functions.setHandTopReverse($transTypeIcon, $helpHandSlide2, 15);
                    qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide2, $helpInfoSlide2, 5);
                } else {
                    qcssapp.Functions.setHandTop($transTypeIcon, $helpHandSlide2, 15);
                    qcssapp.Functions.setHelpInfoTop($helpHandSlide2, $helpInfoSlide2, 5);
                }
            }

            qcssapp.Functions.setHandLeft($transTypeIcon, $helpHandSlide2);

            this.hideInactiveSlides();
        },

        hideInactiveSlides: function() {
            if(this.slideStart != 0) {
                this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]').addClass('active-slide');
                this.$el.find('.help-slide[data-slide-id!="'+this.slideStart+'"]').addClass('inactive-slide');
            }
        },

        closeHelp: function() {
            this.parentView.fadeOut();
        }
    });
})(qcssapp, jQuery);
