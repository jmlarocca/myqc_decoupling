;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.RewardHistoryView - View for displaying Rewards History Collection
     */
    qcssapp.Views.RewardHistoryView = Backbone.View.extend({
        internalName: "Reward History View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'ul',
        className: 'info-list scrollElement',
        id: 'program-rewards-history-list', //references existing HTML element for Rewards Collection- can be referenced as this.$el throughout the view

        events: {
            'scroll': 'checkScroll'
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in rewards history view");
            }

            this.$container = $('#program-rewards-history');

            this.options = options || {};

            this.itemsPerPage = 20;
            this.currentPage = 1;
            this.sort = "date";
            this.sortOrder = "desc";
            this.endOfList = false;
            this.txnList = [];

            //bind "this" reference to functions in the view
            _.bindAll(this, "fetchSuccess", "fetchError", "checkScroll");

            //event didn't bubble properly for using the events, need to explicitly set the function trigger
            this.$container.on('scroll',this.checkScroll);

            this.programModel = this.options.programModel;
            this.collection = new qcssapp.Collections.RewardHistory();

            this.$headerContainer = this.$container.find('.program-rewards-header');

            this.createProgramHeader();

            //property to prevent further requests while loading, true because of the next fetch, turned false in fetch success
            this.loading = true;

            //populates the new collection
            this.loadCollection();

            $("#no-rewards").remove();

        },

        createProgramHeader: function() {
            var controllerModel = new qcssapp.Models.Controller({
                id: 'RewardHeaderView',
                options: this.programModel.toJSON(),
                destructible: true,
                destroyOn: ['NavView', 'RewardsProgramsView']
            });
            var headerView = qcssapp.Controller.constructView(controllerModel);
            this.$headerContainer.append(headerView.$el);
        },

        //fetch collection to be used in this view
        loadCollection: function(receiptCallback, receiptView) {
            var dataParameters = $.param({ models: this.itemsPerPage, page: this.currentPage, sort: this.sort, order: this.sortOrder });

            var parameters = {
                rewardHistoryView:this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/loyalty/history/'+this.programModel.get('id'), 'GET', dataParameters, parameters,
                function(response, successParameters) {
                    successParameters.rewardHistoryView.fetchSuccess(response);

                    if(receiptCallback) {
                        receiptCallback(receiptView, successParameters.rewardHistoryView);
                    }
                },
                function(errorParameters) {
                    errorParameters.rewardHistoryView.fetchError();
                },
                '',
                parameters
            );
        },

        fetchSuccess: function(data) { //handles successful fetching the reward history collection
            this.collection.set(data, {parse: true});

            var currentTxnList = this.txnList;

            //render this view now that we've fetched the reward history collection
            this.render();

            var newTxnList =  this.collection.toJSON();
            this.txnList = $.merge(currentTxnList, newTxnList);

            //reset loading once complete
            this.loading = false;
        },

        //calls renderLine on each reward
        render: function() {
            this.rewardsMenuText = $('#show-rewards').attr('data-title').substring(0,$('#show-rewards').attr('data-title').indexOf('Programs'));
            $('.rewards-program-header-text').text(this.rewardsMenuText+'Program');

            if ( this.currentPage == 1 ) {
                this.$container.append(this.$el);

                if (this.collection.isEmpty()) {
                    this.$container.append('<ul><li id="no-rewards" class="align-center italic" style="margin-top:25px">No'+this.rewardsMenuText+'History Found.</li></ul>');
                    return;
                }

            }

            var controllerModel = new qcssapp.Models.Controller({
                id: 'RewardHistoryLineView',
                destructible: true,
                destroyOn: ['NavView', 'RewardsProgramsView']
            });

            var that = this;
            this.collection.each(function( mod ) {
                mod.set('showReorder', false);
                if ( mod.get('paorderTypeID') != null && Number(mod.get('paorderTypeID') > 1)  ){
                    mod.set('showReorder', true);   //for showing order again button when swiping through receipts
                }
                mod.set('programID', that.programModel.attributes.id); //for swiping through receipts
                mod.txnView = that; //pass in the original txnView on the model for the receipt view
                this.renderLine( mod, controllerModel );
            }, this );

            if ( this.collection.length < this.itemsPerPage && !this.$el.find('.endlist').length) {
                this.endOfList = true;
                this.$el.append('<li class="template-gen primary-color endlist accent-color-one align-center italic">End of'+this.rewardsMenuText+'History List</li>');
            }
        },

        //builds a new reward history line view, then creates and appends
        renderLine: function( mod, controllerModel ) {
            controllerModel.set('options', {model:mod});

            var RewardHistoryLineView = qcssapp.Controller.constructView(controllerModel);

            this.$el.append( RewardHistoryLineView.render().el );
        },

        //determines whether to load more based on scroll position and loading status
        checkScroll: function() {
            var triggerPoint = 100; //100px from bottom of page
            var pageScroll = this.$el.scrollTop();
            var listHeight = this.$el.height();

            if ( !this.loading && !this.endOfList && pageScroll + triggerPoint > (listHeight * this.currentPage) ) {
                this.loading = true;

                this.currentPage += 1;

                this.loadCollection();
            }
        },

        fetchError: function() { //handles error for fetching the reward history collection
            qcssapp.Functions.displayError('There was an error loading rewards history, please try again later.');
        }
    });
})(qcssapp, jQuery);