;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FutureDateView - View for displaying date picker for ordering for the future
     */
    qcssapp.Views.FutureDateView = Backbone.View.extend({
        internalName: "Future Date View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#show-date', //references existing HTML element

        events: {
            'change #futureOrderDate': 'changeFutureOrderDate'
        },

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in future date view");
            }

            this.$futureOrderDate = $("#futureOrderDate");
            this.$futureOrderDate.val('');

            this.$datepicker = $('.datepicker-calendar');

            this.setDatePicker();

            qcssapp.Functions.hideMask();
        },

        displayButtons: function() {
            if($('#submitDateButton').length) {
                return;
            }

            //create buttons
            new qcssapp.Views.ButtonView({
                text: "SUBMIT",
                buttonType: "customCB",
                appendToID: "#dateInputContainer",
                id: "submitDateButton",
                parentView: this,
                class: "align-center prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                callback: function() {
                    if(this.parentView.$futureOrderDate.val() == "") {
                        qcssapp.Functions.displayError('Please select a valid date');
                        return;
                    }

                    var futureOrderDate = this.parentView.$futureOrderDate.val();
                    qcssapp.futureOrderDate = "";

                    var dateToday = new Date();
                    var futureDate = new Date(futureOrderDate);

                    //if the selected date is NOT today's date, set the future order date
                    if(dateToday.toLocaleDateString() != futureDate.toLocaleDateString()) {
                        qcssapp.futureOrderDate = futureOrderDate;
                    }

                    $('#show-order').off().find('.template-gen').remove();
                    new qcssapp.Views.OrderStoreView();
                    qcssapp.Router.navigate('show-order', {trigger: true});
                }
            });

        },

        //initialize the date picker calendar
        setDatePicker: function() {
            var startDate = new Date();
            var view = this;

            if(qcssapp.futureOrderDate != "") {
                startDate = new Date(qcssapp.futureOrderDate);
            }

            this.$datepicker.datepicker({
                language: 'en',
                minDate: new Date(),
                startDate: startDate,
                onSelect: function onSelect(val, date) {
                    $('#dateErrorContainer').hide();
                    view.$futureOrderDate.val(val);
                }
            });

            this.$datepicker.data('datepicker').selectDate(startDate);

            this.displayButtons();
        },

        //check the future order date input is valid
        changeFutureOrderDate: function() {
            if(this.$futureOrderDate.val() != '') {
                $('#dateErrorContainer').hide();

                //check if a valid date was inputted
                if( !Date.parse(this.$futureOrderDate.val())) {
                    qcssapp.Functions.displayError('Please enter a valid date');
                    this.$futureOrderDate.val('');
                    return;
                }

                //check if the inputted date is not before today
                if( new Date(this.$futureOrderDate.val()) < new Date() ) {
                    qcssapp.Functions.displayError('Please enter a valid date that is not before today');
                    this.$futureOrderDate.val('');
                    return;
                }

                this.$datepicker.data('datepicker').selectDate(new Date(this.$futureOrderDate.val()));
            }
        }

    });
})(qcssapp, jQuery);