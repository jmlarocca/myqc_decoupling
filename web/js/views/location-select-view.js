(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.LocationSelectView - View for displaying location selection options
     */
    qcssapp.Views.LocationSelectView = Backbone.View.extend({
        internalName: "Location Select View", //NOT A STANDARD BACKBONE PROPERTY

        // Cache the template function for a single item
        template: _.template( $("#location-select-template").html() ),

        render: function() {
            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.setElement(this.template(this.model));

            return this;
        }
    });
})(qcssapp, jQuery);
