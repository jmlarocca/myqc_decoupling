;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.spendingProfileView - View for displaying spending profile Collections
     */
    qcssapp.Views.spendingProfileSelectView = Backbone.View.extend({
        internalName: "Spending Profile View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#spendingProfileSelect', //references existing HTML element for Transaction Collections - can be referenced as this.$el throughout the view

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in spending profile view");
            }

            //bind "this" reference to functions in the view
            _.bindAll(this, "fetchSuccess", "fetchError", "renderLimits");

            //create new collection
            this.collection = new qcssapp.Collections.spendingProfile();

            this.limitCollection = new qcssapp.Collections.Limits();

            var verb = "GET";
            var dataParameters = "";
            if(typeof options !== 'undefined') {
                verb = options.verb;
                dataParameters = options.dataParameters;
                this.onInit = false;
            } else {
                this.onInit = true;
            }

            //populates the new collection, true parameter to reset/remove collection entries
            this.loadCollection(verb, dataParameters);
        },

        fetchError: function() { //handles error for fetching the spending profile collection
            if (qcssapp.DebugMode) {
                console.log("Error loading data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            //display error message to user
            qcssapp.Functions.displayError('There was an error loading the spending profile options, please try again later.');
            qcssapp.Functions.hideMask();
        },

        fetchSuccess: function() { //handles successful fetching the spending profile collection
            if (qcssapp.DebugMode) {
                console.log("Successfully loaded data for "+this.internalName);
                console.log(this);
                console.log(this.collection.toJSON());
            }

            //display view
            this.render();
        },

        //calls renderLine on each item in the spending profile collection
        render: function() {
            if ( this.collection.size() > 1 ) {
                this.collection.each(function( mod ) {
                    this.renderLine( mod );
                }, this );

                new qcssapp.Views.ButtonView({
                    text: "Save",
                    buttonType: "customCB",
                    appendToID: "#spendingProfileButtonContainer",
                    id: "saveSpendingProfileBtn",
                    parentView:this,
                    class: "template-gen prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                    callback: function() {
                        var $spendingProfileSelect = $('#spendingProfileSelect');

                        //if the original spendingProfileID doesn't equal the changed spendingProfileID, save the new one
                        if($spendingProfileSelect.data('origID') != $spendingProfileSelect.val())  {
                            var $spendingProfilePanel = $("#show-spendinglimits");

                            //update the original spending profile id to the new spending profile id
                            $spendingProfileSelect.data('origID', $spendingProfileSelect.val() );

                           //remove generated content
                            $spendingProfilePanel.find('.template-gen').remove();
                            $spendingProfilePanel.find('.template-gen-container').children().not('.persist').remove();

                            //re-create page
                            new qcssapp.Views.spendingProfileSelectView();
                            qcssapp.Functions.hideMask();

                            //show success message
                            $('#spendingProfileMsgContainer').show().text('Spending profile successfully updated!');
                        }
                    }
                });

                new qcssapp.Views.ButtonView({
                    text: "Cancel",
                    buttonType: "customCB",
                    appendToID: "#spendingProfileButtonContainer",
                    id: "cancelSpendingProfileBtn",
                    parentView:this,
                    class: "template-gen prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color",
                    callback: function() {
                        var $spendingProfileSelect = $('#spendingProfileSelect');
                        var originalID = $spendingProfileSelect.data('origID');
                        var oldSpendingProfileList = $spendingProfileSelect.data('collections');

                        var dataParameters = {'id':parseInt(originalID), 'oldSpendingProfileList':oldSpendingProfileList};
                        var options = {'verb':'POST', 'dataParameters':dataParameters};

                        new qcssapp.Views.spendingProfileSelectView(options);
                    }
                });

                var $spendingProfileSelect = $('#spendingProfileSelect');

                //if the original spendingProfileID equals the changed spendingProfileID, hide the cancel button (nothing has been changed)
                if(($spendingProfileSelect.data('origID') == $spendingProfileSelect.val()) || qcssapp.Functions.checkServerVersion(1,6)) {
                   $('#cancelSpendingProfileBtn').hide();

                //if the spendingProfileID was change AND then you hit the home button and then came back to the page, reset the spending profile to the original ID
                }  else if(this.onInit) {
                    this.collection = new qcssapp.Collections.spendingProfile();
                    this.limitCollection = new qcssapp.Collections.Limits();

                    //populates the selector with the original spendingProfileID
                    this.loadCollection('POST', {'id':$spendingProfileSelect.data('origID')});
                    return;
                }


            } else {
                $('#spendingProfileTitle').hide();
                $('#spendingProfileSelect').hide();
                $('#spendingProfileButtonContainer').hide();
            }

            //get limits
            this.displayGlobalLimit();
            this.displayLimits();
        },

        //creates a new view for each spending profile option item and assigns the model, then displays it on the page
        renderLine: function( mod ) {
            var spendingProfileOptionView = new qcssapp.Views.spendingProfileOptionView({
                model: mod
            });
            this.$el.append( spendingProfileOptionView.render().el );

            //if the original spendingProfileID has not been set, set it (this would be on page initialization)
            var $spendingProfileSelect = $('#spendingProfileSelect');
            if(mod.attributes.active && !$spendingProfileSelect.data('origID')) {
                $spendingProfileSelect.data('origID', mod.attributes.id.toString());
            }
        },

        loadCollection: function(verb, dataParameters) {
            var successParameters = {
                spendingProfileView:this
            };

            var errorParameters = {
                spendingProfileView:this
            };

            if(dataParameters != "") {
                dataParameters = JSON.stringify(dataParameters);
            }

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/spendingprofiles', verb, dataParameters, successParameters,
                function(response, successParameters) {

                    //if the user's spendingProfileID was successfully updated, fetch their new spending profile details
                    if(response.status == "success") {
                        var $spendingProfilePanel = $("#show-spendinglimits");

                        //remove generated content
                        $spendingProfilePanel.find('.template-gen').remove();
                        $spendingProfilePanel.find('.template-gen-container').children().not('.persist').remove();

                        successParameters.spendingProfileView.collection = new qcssapp.Collections.spendingProfile();

                        successParameters.spendingProfileView.limitCollection = new qcssapp.Collections.Limits();

                        //populates the new collection, true parameter to reset/remove collection entries
                        successParameters.spendingProfileView.loadCollection('GET', '');

                        qcssapp.Functions.hideMask();

                    //if successfully fetched the user's spending profile details
                    } else {
                        $.each(response, function() {
                            this.selected = this.active ? 'selected=selected' : '';
                        });

                        successParameters.spendingProfileView.collection.set(response);
                        successParameters.spendingProfileView.fetchSuccess(response);

                        if(typeof $('#spendingProfileSelect').data('collections') == 'undefined') {
                            var arr = [];

                            $.each(successParameters.spendingProfileView.collection.models, function() {
                                var obj = this.attributes;
                               arr.push(obj);
                            });
                            $('#spendingProfileSelect').data('collections', arr);
                        }
                    }
                },
                function(errorParameters) {
                    errorParameters.spendingProfileView.fetchError();
                },
                '',
                errorParameters,
                '',
                false,
                true
            );
        },

        //GET request for global balance information, adding the global balance view to the page
        displayGlobalLimit: function() {
            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/limits/global', 'GET', '','',
                function(data) {
                    if ( data ) {
                        new qcssapp.Views.GlobalLimitView(data);
                    }
                },
                function(params) {
                    if (qcssapp.DebugMode) {
                        console.log("Error loading global balance data");
                    }
                },
                '',
                '',
                '',
                false,
                true
            );
        },

        displayLimits: function() {
            var successParameters = {
                spendingProfileView:this
            };

            var errorParameters = {
                spendingProfileView:this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/myqc/limits', 'GET', '',successParameters,
                function(response, successParameters) {
                    $.each(response, function() {
                        if ( this.avail == null || Number(this.avail) == 1000000 || Number(this.limit) == 1000000 ) {
                            this.avail = "";
                            this.limit = "";
                        } else {
                            this.limit = "$" + this.limit.toFixed(2);
                        }
                    });

                    successParameters.spendingProfileView.limitCollection.set(response);
                    successParameters.spendingProfileView.renderLimits();
                },
                function(errorParameters) {
                    errorParameters.spendingProfileView.fetchError();
                },
                '',
                errorParameters,
                '',
                false,
                true
            );
        },

        renderLimits: function() {
            this.limitCollection.each(function( mod ) {
                this.renderLimit( mod );
            }, this );

            qcssapp.Functions.hideMask();
        },

        renderLimit: function( mod ) {
            var spendingLimitView = new qcssapp.Views.SpendingLimitView({
                model: mod
            });
            $('#spendingLimitsContainer').append( spendingLimitView.render().el );
        }
    });
})(qcssapp, jQuery);   