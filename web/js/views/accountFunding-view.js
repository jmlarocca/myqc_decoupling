;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.AccountFundingView - View for displaying recharge page
     */
    qcssapp.Views.AccountFundingView = Backbone.View.extend({
        internalName: 'Account Funding View', //NOT A STANDARD BACKBONE PROPERTY

        el: '#accountFunding', //references existing HTML element for - can be referenced as this.$el throughout the view

        //template: //template not needed for this view

        events: {
            'click .filterBtn': 'filter',
            'click .toggle': 'toggleAutomaticReloads'
        },

        initialize: function () {
            if (qcssapp.DebugMode) {
                console.log('initialize in account funding view');
            }

            //constants
            this.ONE_TIME_AMOUNTTYPEID = 1;
            this.RELOAD_AMOUNT_AMOUNTTYPEID = 2;
            this.RELOAD_THRESHOLD_AMOUNTTYPEID = 3;

            //view properties
            this.showOneTime = false;
            this.showAutomatic = false;
            this.hasPaymentMethod = false;
            this.automaticActive = false;
            this.allowAccountFunding = true;
            this.accountAutoFundingInfoModel = new qcssapp.Models.AccountAutoFundingInfo();
            this.accountPaymentMethodModel = null;
            this.paymentProcessorModel = new qcssapp.Models.PaymentProcessor();
            this.FundingBalanceModel = new qcssapp.Models.FundingBalance();

            //view collection properties
            this.oneTimeFundingAmountCollection = new qcssapp.Collections.FundingAmount();
            this.automaticReloadFundingAmountCollection = new qcssapp.Collections.FundingAmount();
            this.automaticThresholdFundingAmountCollection = new qcssapp.Collections.FundingAmount();

            //collection events
            this.listenTo( this.oneTimeFundingAmountCollection, 'add', this.addOneTimeFundingAmount );
            this.listenTo( this.automaticReloadFundingAmountCollection, 'add', this.addAutomaticReloadFundingAmount );
            this.listenTo( this.automaticThresholdFundingAmountCollection, 'add', this.addAutomaticThresholdFundingAmount );

            //view elements
            this.$filter = $('#accountFunding_filter');

            this.$paymentMethod = $('#accountFunding_paymentMethod');
            this.$paymentMethodCurrentMethod = $('#accountFunding_paymentMethod_saved-method');
            this.$agreementPaymentMethod = $('#payment-agreement_paymentMethod_saved-method');

            this.$oneTime = $('#accountFunding_fundOneTime');
            this.$oneTimeSelect = $('#accountFunding_fundOneTime_amount');

            this.$automatic = $('#accountFunding_fundAutomatic');
            this.$automaticReloadSelect = $('#accountFunding_fundAutomatic_reload-amount');
            this.$automaticThresholdSelect = $('#accountFunding_fundAutomatic_threshold-amount');
            this.$automaticButtonsMain = $('#accountFunding_fundAutomatic_buttons_main');
            this.$automaticButtonsModifying = $('#accountFunding_fundAutomatic_buttons_modifying');
            this.$toggle = $('.toggle');
            this.$automaticReloadsLockedMsg = $('#accountFunding_autoReloads_msg');

            //child views
            this.agreementView = null;
            this.paymentView = null;
            this.fundingView = null;

            //bind 'this' reference to functions in the view
            _.bindAll(this, 'addOneTimeFundingAmount', 'addAutomaticReloadFundingAmount', 'addAutomaticThresholdFundingAmount', 'resetPage', 'goToPaymentPage', 'loadFundingData', 'fetchSuccess', 'fetchError', 'render', 'renderFundingAmount', 'filter', 'saveAutomatic', 'toggleModifyAutomatic', 'toggleSelectorLocks', 'toggleAutomaticReloads', 'insertOrUpdateAutomaticOptions', 'removePaymentMethod', 'displayButtons', 'displayCurrentBalance', 'populateAndDisplayFundingAmountCollections', 'handleOneTimeButton', 'goToAgreementPage', 'handleAutomaticDisabled', 'setReloadAmount', 'setThresholdAmount', 'handleAutomaticEnabled');

            this.resetPage();

            //start loading view data
            this.loadFundingData();

            if(this.$toggle.removeClass('active').find('.toggle-label').html() == 'OFF') {
                this.$toggle.addClass('primary-background-color');
                this.$toggle.find('.toggle-label').addClass('font-color-primary');
            } else {
                this.$toggle.addClass('tertiary-background-color');
                this.$toggle.find('.toggle-label').addClass('font-color-tertiary');
            }

        },

        //sets the page back to original state
        resetPage: function() {
            this.showOneTime = false;
            this.showAutomatic = false;
            this.hasPaymentMethod = false;
            this.automaticActive = false;
            this.$automatic.addClass('hidden');
            this.$oneTime.addClass('hidden');
            this.$filter.addClass('hidden');
            $('#accountFunding_filter_one').addClass('selected secondary-background-color font-color-secondary');
            $('#accountFunding_filter_automatic').removeClass('selected secondary-background-color font-color-secondary');
            $('#accountFunding_filter_automatic').removeAttr('style');
            this.$el.find('.hidden').hide();
            this.$toggle.removeClass('active tertiary-background-color').find('.toggle-label').html('OFF');
            this.$toggle.find('.toggle-label').removeClass('font-color-tertiary');
            this.toggleSelectorLocks(true);
        },

        // START DATA Related Functions

        //gets all details related to funding
        loadFundingData: function() {
            if ( qcssapp.DebugMode ) {
                console.log('Loading account funding details...');
            }

            var endPoint = qcssapp.Location + '/api/funding/account/detail';

            var parameters = {
                accountFundingView: this
            };

            //gets all details related to funding
            qcssapp.Functions.callAPI(endPoint, 'GET', '', parameters,
                function(response, parameters) {
                    parameters.accountFundingView.fetchSuccess(response);
                },
                '',
                '',
                '',
                '',
                false,
                true
            );
        },

        //grabs and sets all necessary data
        fetchSuccess: function(data) {
            if ( qcssapp.DebugMode ) {
                console.log('Account funding details successfully loaded.');
            }

            //check for the payment processor information
            if ( data.hasOwnProperty('paymentProcessor') && ((data.paymentProcessor.hasOwnProperty('paymentPageFileName') && data.paymentProcessor.paymentPageFileName.length > 0) || data.paymentProcessor.name == "FreedomPay") ) {
                this.paymentProcessorModel = new qcssapp.Models.PaymentProcessor(data.paymentProcessor);
            } else {
                qcssapp.Functions.displayError('Error loading account funding information - the payment processor may be setup incorrectly. Please try again later.');
                qcssapp.Functions.hideMask();
                return false;
            }

            //check if there is a saved payment method, if so create the model and store on the view
            if ( data.hasOwnProperty('accountPaymentMethods') && data.accountPaymentMethods.length > 0 ) {
                this.hasPaymentMethod = true;

                //TODO: eventually there will be multiple payment methods and we can do _.findWhere(data.accountPaymentMethods, {'modelIsDefault':true}) to get the one to display
                this.accountPaymentMethodModel = new qcssapp.Models.AccountPaymentMethod(data.accountPaymentMethods[0], {parse:true});
            } else {
                this.hasPaymentMethod = false;
            }

            //this indicates that automatic reloads are ON with the given amounts
            if ( data.hasOwnProperty('accountAutoFundingInfo') && data.accountAutoFundingInfo.hasOwnProperty('reloadAmount') && data.accountAutoFundingInfo.reloadAmount != 'undefined' && Number(data.accountAutoFundingInfo.reloadAmount) > 0 ) {
                this.showAutomatic = true;

                //create model and store on view, set parse to take abs value of threshold
                this.accountAutoFundingInfoModel = new qcssapp.Models.AccountAutoFundingInfo(data.accountAutoFundingInfo, {parse: true});

                //if an auto reload payment method id is set OR if on earlier server version and hasPaymentMethod is true
                if ( (data.accountAutoFundingInfo.reloadPaymentMethodID != null && data.accountAutoFundingInfo.reloadPaymentMethodID != 'undefined') || (!qcssapp.Functions.checkServerVersion(2,0) && this.hasPaymentMethod) ) {
                    this.automaticActive = true;

                    //lock the selectors
                    this.toggleSelectorLocks(false);

                    this.$toggle.addClass('active tertiary-background-color').find('.toggle-label').html('ON');

                    //set toggle switch color for branding
                    this.$toggle.find('.toggle-label').addClass('font-color-tertiary');
                }
            }

            //check amounts to determine whether to show One Time fields, Automatic fields, or the filter and One Time fields
            if ( (!data.hasOwnProperty('fundingAmounts') || data.fundingAmounts.length == 0) && !this.showAutomatic ) {
                qcssapp.Functions.displayError('Account Funding is not currently available!', 'Warning');
                this.$filter.add(this.$automatic).add(this.$oneTime).addClass('hidden').hide();
            } else {
                //handles all logic related to funding amounts and whether each funding type will show
                this.populateAndDisplayFundingAmountCollections(data.fundingAmounts);
            }

            if( data.hasOwnProperty('accountFundingAllowed') && !data.accountFundingAllowed) {
                this.allowAccountFunding = false;
            }

            this.render();

            return this;
        },

        //handle error from attempting to load details
        fetchError: function() {
            return this;
        },

        //updates reload amount on the accountAutoFundingInfo model
        setReloadAmount: function() {
            this.accountAutoFundingInfoModel.set('reloadAmount', this.$automaticReloadSelect.val());
        },

        //updates reload threshold on the accountAutoFundingInfo model
        setThresholdAmount: function() {
            this.accountAutoFundingInfoModel.set('reloadThreshold', this.$automaticThresholdSelect.val());
        },

        setReloadPaymentAndPersonID: function() {
            this.accountAutoFundingInfoModel.set('reloadPaymentMethodID', this.accountPaymentMethodModel.get('id'));
            this.accountAutoFundingInfoModel.set('reloadSetUpByID', this.accountPaymentMethodModel.get('personID'));
        },

        // END DATA Related Functions

        // START Rendering Related Functions

        //render page based on view property values
        render: function() {
            //render buttons
            this.displayButtons();

            //only show payment method tile initially if there is one saved
            if ( this.hasPaymentMethod ) {
                //builds and renders the payment method template with the given data
                var paymentMethodView = new qcssapp.Views.PaymentMethod({
                    model: this.accountPaymentMethodModel
                });

                var paymentMethodHTML = paymentMethodView.el;
                this.$paymentMethodCurrentMethod.add(this.$agreementPaymentMethod).html(paymentMethodHTML);
            }

            //if not showing either funding type, need to display an error message
            if ( !this.showAutomatic && !this.showOneTime ) {
                qcssapp.Functions.displayError('Account Funding is not currently available!', 'Warning');
            } else {
                if ( this.showOneTime && this.allowAccountFunding) {
                    this.$oneTime.removeClass('hidden').show();
                }

                if ( this.showAutomatic && this.allowAccountFunding) {
                    if ( this.showOneTime) {
                        //filter should show on page reset, remove hidden class
                        this.$filter.removeClass('hidden').show();
                    } else {
                        //if showing automatic but not one time, need to just show automatic & automatic should show on page reset
                        this.$automatic.removeClass('hidden').show();
                        this.$filter.add(this.$oneTime).addClass('hidden').hide();
                    }
                }
            }

            //get and display global balance info
            this.displayCurrentBalance();
        },

        //splits the funding amounts by amountTypeID and populates each of the collections accordingly
        populateAndDisplayFundingAmountCollections: function(fundingAmounts) {
            var accountFundingView = this;
            var groupedFundingAmounts = _.groupBy(fundingAmounts, 'amountTypeID');

            // defect 3738 - sort the funding and threshold arrays so they display properly in the dropdowns
            $.each(groupedFundingAmounts, function(index, fundingGroupArray) {
                fundingGroupArray.sort( function(a, b) {
                    return a.amount - b.amount;
                })
            });

            //get all one-time funding amounts
            var oneTimeFundingAmounts = groupedFundingAmounts[this.ONE_TIME_AMOUNTTYPEID];

            if ( oneTimeFundingAmounts && oneTimeFundingAmounts.length > 0 ) {
                this.showOneTime = true;

                //populate the one-time select
                $.each(oneTimeFundingAmounts, function() {
                    var fundingAmountModel = new qcssapp.Models.FundingAmount(this);
                    accountFundingView.oneTimeFundingAmountCollection.add(fundingAmountModel);
                });

                //there will be no selected -- only default for one time funding
                var defaultOneTimeFundingAmountModel = _.findWhere(oneTimeFundingAmounts, {'defaulted': true});
                if ( defaultOneTimeFundingAmountModel ) {
                    this.$oneTimeSelect.val(defaultOneTimeFundingAmountModel.amount);
                }
            }

            //get all automatic funding amounts
            var automaticThresholdFundingAmounts = [];
            if ( groupedFundingAmounts[this.RELOAD_THRESHOLD_AMOUNTTYPEID] ) {
                automaticThresholdFundingAmounts = groupedFundingAmounts[this.RELOAD_THRESHOLD_AMOUNTTYPEID];
            }

            var automaticReloadFundingAmounts = [];
            if ( groupedFundingAmounts[this.RELOAD_AMOUNT_AMOUNTTYPEID] ) {
                automaticReloadFundingAmounts = groupedFundingAmounts[this.RELOAD_AMOUNT_AMOUNTTYPEID];
            }

            //if automatic is already on, we need to ensure the selected funding amounts exist and are the selected ones
            if ( this.automaticActive || ( !this.hasPaymentMethod && Number(this.accountAutoFundingInfoModel.get('reloadAmount')) > 0 ) ) {
                var activeReloadAmount = this.accountAutoFundingInfoModel.get('reloadAmount');
                var activeReloadFundingAmountModel = _.findWhere(automaticReloadFundingAmounts, {'amount': activeReloadAmount, 'selected': true});

                if ( !activeReloadFundingAmountModel ) {
                    automaticReloadFundingAmounts.push({
                        amount: activeReloadAmount,
                        amountTypeID: 2,
                        defaulted: false,
                        id: 999999,
                        name: 'Selected Automatic Reload Funding Amount',
                        selected: true
                    });
                }

                var activeThresholdAmount = this.accountAutoFundingInfoModel.get('reloadThreshold');
                var activeThresholdFundingAmountModel = _.findWhere(automaticThresholdFundingAmounts, {'amount': activeThresholdAmount, 'selected': true});

                if ( !activeThresholdFundingAmountModel ) {
                    automaticThresholdFundingAmounts.push({
                        amount: activeThresholdAmount,
                        amountTypeID: 3,
                        defaulted: false,
                        id: 999998,
                        name: 'Selected Automatic Reload Threshold Funding Amount',
                        selected: true
                    });
                }
            }

            if ( automaticThresholdFundingAmounts.length > 0 && automaticReloadFundingAmounts.length > 0 ) {
                this.showAutomatic = true;

                //populate threshold select
                $.each(automaticThresholdFundingAmounts, function() {
                    var fundingAmountModel = new qcssapp.Models.FundingAmount(this);
                    accountFundingView.automaticThresholdFundingAmountCollection.add(fundingAmountModel);
                });

                //if there is no selected threshold, select the default
                var thresholdSelected = _.findWhere(automaticThresholdFundingAmounts, {'selected': true});
                if ( !thresholdSelected ) {
                    var defaultThreshold = _.findWhere(automaticThresholdFundingAmounts, {'defaulted': true});
                    if ( defaultThreshold ) {
                        this.$automaticThresholdSelect.val(defaultThreshold.amount);
                    }
                } else {
                    this.$automaticThresholdSelect.val(thresholdSelected.amount);
                }

                //populate amount select
                $.each(automaticReloadFundingAmounts, function() {
                    var fundingAmountModel = new qcssapp.Models.FundingAmount(this);
                    accountFundingView.automaticReloadFundingAmountCollection.add(fundingAmountModel);
                });

                //if there is no selected reload amount, select the default
                var reloadSelected = _.findWhere(automaticReloadFundingAmounts, {'selected': true});
                if ( !reloadSelected ) {
                    var defaultReload = _.findWhere(automaticReloadFundingAmounts, {'defaulted': true});
                    if ( defaultReload ) {
                        this.$automaticReloadSelect.val(defaultReload.amount);
                    }
                } else {
                    this.$automaticReloadSelect.val(reloadSelected.amount);
                }
            }
        },

        //generic fending amount rendering, puts the fundingAmountModel as an option on the given selector
        renderFundingAmount: function(mod, $selector) {
            var fundingAmountView = new qcssapp.Views.FundingAmount({
                model:mod
            });

            $selector.append(fundingAmountView.render().el);
        },

        //GET request for global balance information, adding the global balance view to the page
        displayCurrentBalance: function() {
            var parameters = {
                accountFundingView: this
            };

            qcssapp.Functions.callAPI(qcssapp.Location + '/api/funding/account/store/balance/info', 'GET', '', parameters,
                function(data, parameters) {
                    if ( data && data.hasOwnProperty('STOREBALANCE') && qcssapp.Functions.isNumeric(data.STOREBALANCE) ) {
                        parameters.accountFundingView.FundingBalanceModel = new qcssapp.Models.FundingBalance({
                            balance: data.STOREBALANCE,
                            lastSynced: data.LASTSYNCED
                        }, {parse: true});

                        new qcssapp.Views.FundingBalance({
                            model: parameters.accountFundingView.FundingBalanceModel
                        });
                    }
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Error loading balance data");
                    }
                    qcssapp.Functions.displayError('There was an error loading your current balance.');
                }
            );
        },

        //handles the displaying of all buttons on the account funding view
        displayButtons: function() {

            var $accountFundingButtons = $('#accountFunding_paymentMethod_buttons');

            if($accountFundingButtons.find('#modifyPaymentMethodBtn').length) {
                $accountFundingButtons.find('#modifyPaymentMethodBtn').remove();
            }

            //payment method modify button
            new qcssapp.Views.ButtonView({
                text: this.hasPaymentMethod ? 'Replace' : 'Add Payment Method',
                appendToID: "#accountFunding_paymentMethod_buttons",
                buttonType: "customCB",
                class: "button-flat template-gen accent-color-one",
                id: "modifyPaymentMethodBtn",
                parentView: this,
                callback: function() {
                    this.parentView.confirmPaymentMethod();
                }
            });

            if ( this.hasPaymentMethod ) {

                if($accountFundingButtons.find('#removePaymentMethodBtn').length) {
                    $accountFundingButtons.find('#removePaymentMethodBtn').remove();
                }

                //payment method remove button
                new qcssapp.Views.ButtonView({
                    text: 'Remove',
                    appendToID: "#accountFunding_paymentMethod_buttons",
                    buttonType: "customCB",
                    class: "button-flat template-gen accent-color-one",
                    id: "removePaymentMethodBtn",
                    parentView: this,
                    callback: function() {
                        this.parentView.removePaymentMethod();
                    }
                });
            }

            //funding history button
            new qcssapp.Views.ButtonView({
                text: 'History',
                appendToID: "#accountFunding_balance",
                buttonType: "customCB",
                class: "button-flat template-gen accent-color-one",
                id: "fundingHistoryButton",
                parentView: this,
                callback: function() {
                    if ( this.fundingView == null ) {
                        this.fundingView = new qcssapp.Views.FundingView();
                    } else {
                        this.fundingView.initialize();
                    }

                    qcssapp.Router.navigate('show-funding', {trigger: true});
                }
            });

            //Only show this button if one time loading is available
            if ( this.showOneTime ) {
                //add funds one time funding button
                new qcssapp.Views.ButtonView({
                    text: "Add Funds",
                    appendToID: "#accountFunding_fundOneTime_button",
                    buttonType: "customCB",
                    class: "accent-color-one button-flat template-gen",
                    id: "oneTimePaymentButton",
                    parentView: this,
                    callback: function() {
                        this.parentView.handleOneTimeButton();
                    }
                });
            }

            //Only show these buttons if automatic reloads are available
            if ( this.showAutomatic ) {
                var automaticToggleLabel = 'Enable';

                if ( this.automaticActive ) {
                    automaticToggleLabel = 'Disable';
                }

                //enable/disable button for auto reloads
                this.toggleAutomaticButton = new qcssapp.Views.ButtonView({
                    text: automaticToggleLabel,
                    appendToID: "#accountFunding_fundAutomatic_buttons_main",
                    buttonType: "customCB",
                    class: "button-flat template-gen accent-color-one",
                    id: "toggleAutomaticButton",
                    parentView: this,
                    callback: function() {
                        if(this.parentView.automaticActive && !this.parentView.autoFundingAccess()) {
                            return;
                        }

                        this.parentView.toggleAutomaticReloads();
                    }
                });

                //unlock to modify auto reload amounts button
                this.modifyAutomaticButton = new qcssapp.Views.ButtonView({
                    text: 'Modify',
                    appendToID: "#accountFunding_fundAutomatic_buttons_main",
                    buttonType: "customCB",
                    class: "button-flat template-gen accent-color-one",
                    id: "modifyAutomaticButton",
                    parentView: this,
                    callback: function() {
                        if(!this.parentView.autoFundingAccess()) {
                            return;
                        }

                        this.parentView.toggleModifyAutomatic(true);
                    }
                });

                if ( !this.automaticActive ) {
                    this.modifyAutomaticButton.$el.hide();
                }

                //cancel modifying auto reload amounts button
                this.cancelModifyAutomaticButton = new qcssapp.Views.ButtonView({
                    text: 'Cancel',
                    appendToID: "#accountFunding_fundAutomatic_buttons_modifying",
                    buttonType: "customCB",
                    class: "button-flat template-gen accent-color-one",
                    id: "cancelModifyAutomaticButton",
                    parentView: this,
                    callback: function() {
                        if(!this.parentView.autoFundingAccess()) {
                            return;
                        }

                        this.parentView.toggleModifyAutomatic(false);
                    }
                });

                //save modified auto reload amounts button
                this.saveModifyAutomaticButton = new qcssapp.Views.ButtonView({
                    text: 'Save',
                    appendToID: "#accountFunding_fundAutomatic_buttons_modifying",
                    buttonType: "customCB",
                    class: "button-flat template-gen accent-color-one",
                    id: "saveModifyAutomaticButton",
                    parentView: this,
                    callback: function() {
                        if(this.parentView.automaticActive && !this.parentView.autoFundingAccess()) {
                            return;
                        }

                        this.parentView.saveAutomatic();
                    }
                });
            }
        },

        //adjusts page based on automatic now being disabled
        handleAutomaticDisabled: function() {
            //reset accountAutoFundingInfo model and store on view
            this.accountAutoFundingInfoModel = new qcssapp.Models.AccountAutoFundingInfo();

            if(typeof this.toggleAutomaticButton !== 'undefined') {
                //toggle 'disable' to 'enable'
                this.toggleAutomaticButton.text = 'enable';
                this.toggleAutomaticButton.render();
            }

            if(typeof this.modifyAutomaticButton !== 'undefined') {
                //hide modify button
                this.modifyAutomaticButton.$el.hide();
            }

            //toggle locks to unlocked
            this.toggleSelectorLocks(true);

            //toggle switch
            this.$toggle.removeClass('active tertiary-background-color').find('.toggle-label').html('OFF');
            this.$toggle.find('.toggle-label').removeClass('font-color-tertiary');

            //set toggle switch color for branding
            this.$toggle.addClass('primary-background-color');
            this.$toggle.find('.toggle-label').addClass('font-color-primary');

            //set flag on view
            this.automaticActive = false;
            this.toggleModifyAutomatic(false);
        },

        //adjusts page based on automatic now being enabled
        handleAutomaticEnabled: function() {
            //toggle 'disable' to 'enable'
            this.toggleAutomaticButton.text = 'disable';
            this.toggleAutomaticButton.render();
            this.modifyAutomaticButton.render();

            //show modify button
            this.modifyAutomaticButton.$el.show();

            //toggle locks to locked
            this.toggleSelectorLocks(false);

            //toggle switch
            this.$toggle.addClass('active').find('.toggle-label').html('ON');

            //set toggle switch color for branding
            this.$toggle.addClass('tertiary-background-color');
            this.$toggle.find('.toggle-label').addClass('font-color-tertiary');

            //set flag on view
            this.automaticActive = true;
            this.toggleModifyAutomatic(false);
        },

        // END Rendering Related Functions

        // START Event handler functions

        //adds given model to the one time funding amount selector
        addOneTimeFundingAmount: function(mod) {
            this.renderFundingAmount(mod, this.$oneTimeSelect);
        },

        //adds given model to the automatic amount funding amount selector
        addAutomaticReloadFundingAmount : function(mod) {
            this.renderFundingAmount(mod, this.$automaticReloadSelect);
        },

        //adds given model to the automatic threshold funding amount selector
        addAutomaticThresholdFundingAmount : function(mod) {
            this.renderFundingAmount(mod, this.$automaticThresholdSelect);
        },

        //handles the switching of automatic reloads on or off
        toggleAutomaticReloads: function() {
            if(this.automaticActive && !this.autoFundingAccess()) {
                return;
            }

            //currently on - turning off
            if ( this.automaticActive ) {
                var parameters = {
                    accountFundingView: this
                };

                //send disable POST /auto/reload/disable
                qcssapp.Functions.callAPI(qcssapp.Location + '/api/funding/auto/reload/disable',
                    'POST',
                    '',
                    parameters,
                    function(response, parameters) {
                        parameters.accountFundingView.handleAutomaticDisabled();
                    }
                );

                return;
            }

            //currently off - turn on
            this.insertOrUpdateAutomaticOptions(qcssapp.Location + '/api/funding/auto/reload/enable');
        },

        //handles the attempt to insert or update the current automatic reload amounts
        insertOrUpdateAutomaticOptions: function(endPoint) {
            //if trying to turn on, but have no payment method, error
            if ( !this.hasPaymentMethod ) {
                $.alert('Please add a Payment Method first!');
                return;
            }

            this.setReloadAmount();
            this.setThresholdAmount();

            if(qcssapp.Functions.checkServerVersion(2,0)) {
                this.setReloadPaymentAndPersonID();
            }

            //currently off - turning on
            var options = {
                paymentType: 'automatic',
                endPoint: endPoint,
                reloadAmount: this.accountAutoFundingInfoModel.get('reloadAmount'),
                reloadThreshold: this.accountAutoFundingInfoModel.get('reloadThreshold'),
                accountFundingView: this,
                slideDownFlag: true
            };

            this.goToAgreementPage(options);
        },

        //toggles the view change for when the user is modifying the automatic reload amounts
        toggleModifyAutomatic: function(modifying) {
            var accountFundingView = this;

            if ( modifying == true ) {
                //hide the 'main' buttons for automatic reloading
                this.$automaticButtonsMain.slideUp(250, function() {
                    //show the 'modifying' buttons for automatic reloading
                    accountFundingView.$automaticButtonsModifying.slideDown(250);
                });
            } else {
                //show the 'main' buttons for automatic reloading
                this.$automaticButtonsModifying.slideUp(250, function() {
                    //hide the 'modifying' buttons for automatic reloading
                    accountFundingView.$automaticButtonsMain.slideDown(250);
                });
            }

            if ( this.automaticActive ) {
                this.toggleSelectorLocks(modifying);
            }
        },

        //whether to show the lock icon on automatic selectors - TRUE is UNLOCKED, FALSE is LOCKED
        toggleSelectorLocks: function(modifying) {
            if ( modifying ) {
                //hide the locks
                this.$automaticReloadSelect.removeClass('locked').attr('disabled', false);
                this.$automaticThresholdSelect.removeClass('locked').attr('disabled', false);
                this.$automaticReloadsLockedMsg.hide();
                return;
            }

            //add the locks
            this.$automaticReloadSelect.val(this.accountAutoFundingInfoModel.get('reloadAmount')).addClass('locked').attr('disabled','disabled');
            this.$automaticThresholdSelect.val(this.accountAutoFundingInfoModel.get('reloadThreshold')).addClass('locked').attr('disabled','disabled');

            if(!this.autoFundingAccess()) {
                this.$automaticReloadsLockedMsg.show();

                if(this.accountAutoFundingInfoModel != null) {
                    var reloadSetUpByName = this.accountAutoFundingInfoModel.get('reloadSetUpByName');
                    $('#accountFunding_reloadSetUpBy').html('Automatic Reloads are already set up by <b>' + reloadSetUpByName + '</b>.');
                }
            }

        },

        //saves the user's changes to their automatic reload amounts
        saveAutomatic: function() {
            this.insertOrUpdateAutomaticOptions(qcssapp.Location + '/api/funding/auto/reload');

            this.toggleModifyAutomatic(false);
        },

        //toggles between one time payment view and automatic payment view
        filter: function(e) {
            var $this = $(e.currentTarget);
            var accountFundingView = this;

            if ( $this.hasClass('selected') ) {
                return;
            }

            $this.addClass('selected secondary-background-color font-color-secondary').siblings().removeClass('selected secondary-background-color font-color-secondary');
            $this.siblings().removeAttr('style');

            if ( e.currentTarget.id == 'accountFunding_filter_automatic' ) {
                this.$oneTime.slideUp(200, function() {
                    accountFundingView.$automatic.slideDown(200);
                });
            } else {
                this.$automatic.slideUp(200, function() {
                    accountFundingView.$oneTime.slideDown(200);
                });
            }
        },

        checkIfPaymentMethodIDIsAutoReloadID: function() {
            var employeeAutoReloadID = null;
            var paymentMethodID = null;

            if(this.accountAutoFundingInfoModel && typeof this.accountAutoFundingInfoModel.get('reloadPaymentMethodID') !== 'undefined' && this.accountAutoFundingInfoModel.get('reloadPaymentMethodID') != null) {
                employeeAutoReloadID = this.accountAutoFundingInfoModel.get('reloadPaymentMethodID')
            }

            if(this.accountPaymentMethodModel != null && typeof this.accountPaymentMethodModel.get('id') !== 'undefined') {
                paymentMethodID = this.accountPaymentMethodModel.get('id');
            }

            return employeeAutoReloadID == paymentMethodID;
        },

        autoFundingAccess: function() {
            var employeeAutoReloadID = null;
            var autoFundingAccess = true;

            if(!qcssapp.Functions.checkServerVersion(2,0)) {
                return autoFundingAccess;
            }

            //if there the account has auto funding on there will be a reloadPaymentMethodID
            if(this.accountAutoFundingInfoModel && typeof this.accountAutoFundingInfoModel.get('reloadPaymentMethodID') !== 'undefined' && this.accountAutoFundingInfoModel.get('reloadPaymentMethodID') != null) {
                employeeAutoReloadID = this.accountAutoFundingInfoModel.get('reloadPaymentMethodID')
            }

            //if on the auto-funding tab and auto funding is turned on
            if(employeeAutoReloadID != null) {

                var paymentMethodID = null;
                var paymentMethodPersonID = null;
                var reloadSetUpByPersonID = null;

                //get the payment method ID and the person ID who set up the payment method if there is one
                if(this.accountPaymentMethodModel != null) {
                    paymentMethodID = this.accountPaymentMethodModel.get('id');
                    paymentMethodPersonID = this.accountPaymentMethodModel.get('personID');
                }

                //get the person ID who set up the auto funding
                if(this.accountAutoFundingInfoModel != null) {
                    reloadSetUpByPersonID = this.accountAutoFundingInfoModel.get('reloadSetUpByPersonID');
                }

                //if logged in as a person account
                if(qcssapp.personModel) {
                    var personID = qcssapp.personModel.get('personID');  //get person ID

                    //if the auto funding's payment method is the same as the payment method set up, can look at the info of the payment method model
                    if(paymentMethodID == employeeAutoReloadID && paymentMethodID != null)  {

                        // if the person account logged in is NOT the same as the personID who set up the payment method turn off auto funding access
                        if(personID != paymentMethodPersonID) {
                            autoFundingAccess = false;
                        }

                    //otherwise get person who set reloadPaymentMethodID because payment method is different than auto reload's payment method
                    } else {

                        //if the account's auto funding was set up by a person the reloadSetUpByPersonID will be set
                        if(reloadSetUpByPersonID != null) {

                            // if the person account logged in is NOT the same as the personID who set up the payment method turn off auto funding access
                            if(reloadSetUpByPersonID != personID) {
                                autoFundingAccess = false;
                            }

                        //if auto funding wasn't set up by a person but a person is logged in, turn off access
                        } else {
                            autoFundingAccess = false;
                        }
                    }

                //otherwise logged in as an employee account
                } else {

                    //if the auto funding's payment method is the same as the payment method set up, can look at the info of the payment method model
                    if(paymentMethodID == employeeAutoReloadID && paymentMethodID != null)  {

                        // if the payment method was set up by a person turn off auto funding access
                        if(paymentMethodPersonID != null) {
                            autoFundingAccess = false;
                        }

                    //otherwise get person who set reloadPaymentMethodID because payment method is different than auto reload's payment method
                    } else {

                        //if the account's auto funding was set up by a person then turn auto funding access off
                        if(reloadSetUpByPersonID != null) {
                            autoFundingAccess = false;
                        }
                    }


                }
            }
            return autoFundingAccess
        },

        //Go to either the payments page or the agreement page depending on if there is a saved payment method
        handleOneTimeButton: function() {
            if ( !this.hasPaymentMethod ) {
                $.alert('Please add a Payment Method first!');
                return;
            }

            var options = {
                paymentType:'oneTime',
                oneTimeAmount: this.$oneTimeSelect.val(),
                endPoint: qcssapp.Location + '/api/funding/fund',
                accountFundingView: this,
                slideDownFlag : true
            };

            this.goToAgreementPage(options);
        },

        //removes the current saved payment method
        removePaymentMethod: function() {
            var endPoint = qcssapp.Location + '/api/funding/account/payment/default/inactivate';

            var parameters = {
                accountFundingView: this
            };

            //post to remove the current payment method
            qcssapp.Functions.callAPI(endPoint, 'POST', '', parameters,
                function(response, parameters) {
                    $('#modifyPaymentMethodBtn').text('Add Payment Method');
                    $('#removePaymentMethodBtn').hide();
                    $('#accountFunding_paymentMethod_saved-method').html('');

                    //clear the view's payment method model
                    parameters.accountFundingView.accountPaymentMethodModel = null;
                    parameters.accountFundingView.hasPaymentMethod = false;

                    //disable handler
                    if(parameters.accountFundingView.autoFundingAccess() || parameters.accountFundingView.checkIfPaymentMethodIDIsAutoReloadID()) {
                        parameters.accountFundingView.handleAutomaticDisabled();
                    }
                }
            );
        },

        // END Event handler functions

        // START Linking functions

        //must take options - endPoint, paymentType, oneTimeAmount/reloadAmount&reloadThreshold
        goToAgreementPage: function(options) {
            qcssapp.Functions.resetErrorMsgs();

            //always include current balance
            options.currentBalance = this.currentBalance;

            qcssapp.Functions.showMask();

            if ( this.agreementView == null ) {
                this.agreementView = new qcssapp.Views.PaymentAgreement(options);
            } else {
                this.agreementView.initialize(options);
            }

            //go to agreement page
            qcssapp.Router.navigate('payment-agreement', {trigger:true});
        },

        goToPaymentPage: function(options) {
            qcssapp.Functions.resetErrorMsgs();

            //if using freedompay
            if ( this.paymentProcessorModel.get('id') == '6' ) {
                //make call to create a token transaction
                var endPoint = qcssapp.Location + '/api/freedompay/createtokentransaction';
                var thisView = this;
                var parameters = {
                    "StoreId": this.paymentProcessorModel.get('paymentProcessorStoreNum'),
                    "TerminalId": this.paymentProcessorModel.get('paymentProcessorTerminalNum')
                };

                //create token transaction
                qcssapp.Functions.callAPI(endPoint, 'POST', JSON.stringify(parameters), '',
                    function(response, parameters) {
                        //always include current balance
                        options.currentBalance = thisView.currentBalance;
                        var checkoutURL = qcssapp.Functions.htmlDecode(response.CheckoutUrl);

                        if ( response.ResponseMessage && response.ResponseMessage.length > 0 && response.ResponseMessage.toString().toLocaleLowerCase() != "null" ) {
                            qcssapp.Functions.displayError(response.ResponseMessage);
                            return;
                        } else if ( !checkoutURL || checkoutURL.length == 0 ) {
                            qcssapp.Functions.displayError('An error occurred while attempting to add a payment method.');
                            return;
                        }

                        qcssapp.Functions.showMask();
                        qcssapp.preventUnloadLogout = true;

                        //set the resumeApp flag
                        var mdl = new qcssapp.Models.SessionModel();
                        mdl.fetch();
                        mdl.set('resumeApp', true);
                        mdl.save();

                        if ( qcssapp.onPhoneApp ) {
                            $.alert({
                                title: '',
                                content: 'You will now be redirected to a secure MyQC payment page powered by FreedomPay.',
                                buttons: {
                                    okay: {
                                        text: 'Ok',
                                        btnClass: 'primary-background-color font-color-primary primary-gradient-color primary-border-color'
                                    }
                                },
                                onClose: function() {
                                    qcssapp.Functions.hideMask();
                                    cordova.InAppBrowser.open(checkoutURL, '_system', 'location=yes');
                                }
                            });
                        } else {
                            //on browser - simply redirect
                            window.location = checkoutURL;
                        }
                    },
                    '',
                    '',
                    '',
                    '',
                    false,
                    true
                );

                return;
            }

            //always include paymentPageFileName
            options.paymentPageFileName = this.paymentProcessorModel.get('paymentPageFileName');

            //always include current balance
            options.currentBalance = this.currentBalance;

            qcssapp.Functions.showMask();

            if ( this.paymentView == null ) {
                this.paymentView = new qcssapp.Views.Payment(options);
            } else {
                this.paymentView.initialize(options);
            }

            //go to payment page
            qcssapp.Router.navigate('payment', {trigger: true});
        },

        //confirmation modal when user adds a new payment method
        confirmPaymentMethod: function() {
            var accountFundingView = this;

            $.confirm({
                title: '',
                content: "<div class='confirm-msg-content'>The payment method you are adding will be securely stored for future use within this application. You can easily remove it at any time. </br></br>Do you want to continue?</div>",
                buttons: {
                    cancel: {
                        text: 'Cancel',
                        btnClass: 'btn-default confirm-btn prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {}
                    },
                    continue: {
                        text: 'Continue',
                        btnClass: 'btn-default confirm-btn prominent-button primary-background-color font-color-primary primary-gradient-color primary-border-color',
                        action: function() {
                            var options = {
                                goToAgreement: false,
                                accountFundingView: accountFundingView,
                                slideDownFlag: true
                            };

                            accountFundingView.goToPaymentPage(options);
                        }
                    }
                }
            });
        }

        // END Linking functions
    });
})(qcssapp, jQuery);