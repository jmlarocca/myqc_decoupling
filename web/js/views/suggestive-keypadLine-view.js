;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.SuggestiveKeypadLineView - View for displaying Keypad Item Collections
     */
    qcssapp.Views.SuggestiveKeypadLineView = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'suggestive-keypad-line',

        // Cache the template function for a single item.
        template: _.template( $('#suggestive-keypad-line-template').html() ),

        events: {
            'click .suggestive-line-button' : 'addToOrder'
        },

        initialize: function() {
            if(this.model.get('priceBelowName')) {
                this.template = _.template( $('#suggestive-keypad-line-below-template').html() );
                this.$el.addClass('suggestive-keypad-line-below');
            }

            return this;
        },

        render: function() {
            if ( this.model.attributes.productID != null) {
                if ( this.model.attributes.price != null ) {
                    this.model.attributes.info =  qcssapp.Functions.formatPrice(this.model.get('price'));
                }
            } else{
                this.model.attributes.info = "&gt;";
            }

            // Description related settings
            var description = ( this.model.get('description') == null || this.model.get('description').trim() == '' ) ? 'hidden' : '';
            this.model.set('showDescription', description);

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            this.$el.attr('data-id', this.model.get('id'));

            if($('#suggestiveview').width() < 415) {
                this.$el.find('.suggestive-line-button').text('').removeClass('primary-gradient-color');
                this.$el.find('.suggestion-added').html('<img class="icon-white-check  svg" src="images/icon-white-check.svg">');
            }

            if (this.model.get('image') != "") {
                this.setImage();
            } else {
                this.$el.find('.line_image-container').hide();
            }

            if(this.model.get('modSetDetailID') != null || this.model.get('prepOptionSetID')) {
                this.$el.addClass('lineCustomizations');
            }

            // For function buttons, hide elements
            if(this.model.get('productID') == null && this.model.get('menuID') == null && this.model.get('comboID') == null) {
                this.$el.addClass('suggestive-function');
            }

            this.setIcons();
            return this;
        },

        //sets the image on the keypad button if there is one
        setImage: function() {

            if ( this.model.attributes.image != "" && qcssapp.Functions.checkServerVersion(1,5) && this.model.attributes.iconPosition != "7") {

                this.$el.addClass('hasImage');

                var imagePath = qcssapp.qcBaseURL + '/webimages/'+ this.model.attributes.image;

                qcssapp.onPhoneApp = (typeof window.cordova !== "undefined");
                if ( qcssapp.onPhoneApp ) {   // Cordova API detected
                    var hostName = qcssapp.Functions.get_hostname(qcssapp.Location); //can't have /myqc in the url
                    imagePath = hostName + '/webimages/'+ this.model.attributes.image;
                }

                //set image background size based on 'Shrink Image to Fit' setting
                var backgroundImageSize = 'cover';
                if(this.model.attributes.shrinkImageToFit) {
                    backgroundImageSize = 'contain';
                }

                //detects when the background url image is loaded
                var that = this;
                $('<img/>').attr('src', imagePath).on('load', function() {
                    $(this).remove(); // prevent memory leaks as @benweet suggested
                    that.$el.find('.line_image').css('background', 'url('+ imagePath +') no-repeat center center / '+backgroundImageSize);
                    that.model.toggleImageLoaded();
                });

                $('<img/>').attr('src', imagePath).on('error', function() {
                    $(this).remove();
                    that.model.toggleImageLoaded();
                }.bind(this));

                //hide the image for fade-in effect later
                this.$el.find('.line_image').addClass('hideImages');

                //set the keypadID for fade-in so it only loads images with the same keypad id
                this.$el.find('.line_image').attr('id', this.model.attributes.keypadID);

                this.formatImageShape();
            }
        },

        // Format the shape of the image on the menu button
        formatImageShape: function() {
            var imageShapeID = null;

            if(typeof this.model.get('imageShapeID') === 'undefined' || this.model.get('imageShapeID') == null || this.model.get('imageShapeID') == '' ) {
                imageShapeID = 1;
            } else {
                imageShapeID = this.model.get('imageShapeID');
            }

            if(imageShapeID == 2) { // Square
                this.$el.find('.line_image-container ').addClass('line_image-square');

            } else if (imageShapeID == 3) { // Circle
                this.$el.find('.line_image-container ').addClass('line_image-circle');
            }
        },

        setIcons: function(){
            if(!qcssapp.Functions.checkServerVersion(3,0))
                return;

            if( this.model.get('favorite') ) {
                this.$el.find('.line_name-span').append('<img class="line-icon heart-line-icon" src="images/icon-favorites.svg"/>');
            }

            if( this.model.get('popular') ) {
                this.$el.find('.line_name-span').append('<img class="line-icon" src="images/icon-popular.svg"/>');
            }

            var healthyFlag = false;

            if(this.model.get('healthy')){
                healthyFlag = true;
                var wellnessPath = "<img src=\""+qcssapp.healthyIndicator.wellness+"\" class=\"line-icon\" alt=\"Healthy\">";
                this.$el.find('.line_icon-container').append(wellnessPath);
            }
            if(this.model.get('vegetarian')){
                healthyFlag = true;
                var vegetarianPath = "<img src=\""+qcssapp.healthyIndicator.vegetarian+"\" class=\"line-icon\" alt=\"Vegetarian\">";
                this.$el.find('.line_icon-container').append(vegetarianPath);
            }
            if(this.model.get('vegan')){
                healthyFlag = true;
                var veganPath = "<img src=\""+qcssapp.healthyIndicator.vegan+"\" class=\"line-icon\" alt=\"Vegan\">";
                this.$el.find('.line_icon-container').append(veganPath);
            }
            if(this.model.get('glutenFree')){
                healthyFlag = true;
                var glutenFreePath = "<img src=\""+qcssapp.healthyIndicator.glutenFree+"\" class=\"line-icon\" alt=\"Gluten Free\">";
                this.$el.find('.line_icon-container').append(glutenFreePath);
            }

            if(healthyFlag) {
                this.$el.find('.line_icon-container').addClass('show-icons')
            }

            var grayscalePercent = qcssapp.Functions.getIconGrayscalePercentage();
            if(grayscalePercent != '') {
                this.$el.find('.line_icon-container img').css('filter', 'grayscale('+grayscalePercent+'%)');
            }

            var nutritionInfoView = new qcssapp.Views.NutritionInfoView({collection: this.model.get('nutritionInfo')});

            if(nutritionInfoView.collection.length == 0) {
                return;
            }

            this.$el.find('.line_nutrition-container').addClass('show-nutrition').append($(nutritionInfoView.render().el).find('.line_nutrition-info'));
        },

        addToOrder: function() {
            // If the product is not restricted, add it to the cart
            if (qcssapp.Functions.checkPurchaseRestriction(this.model.attributes)) {
                var productID = this.model.get('productID');
                if (this.model.get('prepOptionSetID') != null || this.model.get('modSetDetailID') != null) {
                    qcssapp.ProductInProgress.favorite = this.model.get('favorite');
                    qcssapp.ProductInProgress.prepOptionSetID = this.model.get('prepOptionSetID');
                    qcssapp.Functions.goToProductDetail(productID, this.model);
                } else {
                    // Counter to keep track of the suggested products currently being added
                    if (!qcssapp.CurrentOrder.suggestionsInProgress) {
                        qcssapp.CurrentOrder.suggestionsInProgress = 1;
                    } else {
                        qcssapp.CurrentOrder.suggestionsInProgress++;
                    }

                    this.updateAddToCartStyles();
                    qcssapp.Functions.loadProductUpsell(productID);
                }
            }
        },

        updateAddToCartStyles: function() {
            var $buttonAdded = this.$el.find('.suggestion-added');

            $buttonAdded.fadeIn();

            setTimeout(function() {
                $buttonAdded.fadeOut();
            }, 1500);
        }

    });
})(qcssapp, jQuery);
