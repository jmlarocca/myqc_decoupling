;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.GlobalLimitView - View for displaying the global balance view
     */
    qcssapp.Views.GlobalLimitView = Backbone.View.extend({
        internalName: "Global Limit View",

        el: '.globalSpendingLimitContainer', //references existing HTML element for view - can be referenced as this.$el throughout the view
        // Cache the template function for a single item.
        template: _.template( $('#global-spending-limits-template').html() ),

        //events: //events not needed for this view

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in global Limit view");
            }

            //set options
            this.options = options;

            //one MILLION dollars, muahahaha... You realize that's not a lot of money anymore, right?
            if ( this.options.limit == null || Number(this.options.limit) == 1000000 ) {
                this.limit = "";
            } else {
                this.limit = qcssapp.Functions.formatPriceInApp(this.options.limit);
            }
            //create new model with given options
            this.model = new qcssapp.Models.Balance({
                name: "Overall Spending Limit:",
                limit: this.limit
            });

            //display view
            this.render();
        },

        render: function() {
            this.$el.html( this.template( this.model.attributes ) );
            return this;
        }
    });
})(qcssapp, jQuery);
