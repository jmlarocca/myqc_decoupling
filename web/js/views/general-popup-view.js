;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.GeneralPopup - View for displaying popup messages
     */
    qcssapp.Views.GeneralPopup = Backbone.View.extend({
        internalName: "General Popup View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'general-popup',

        template: _.template( $('#general-popup-template').html() ),

        //events: //events not needed for this view

        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in " + this.internalName);
            }

            //set options
            this.options = options;

            this.title = this.options.title;
            this.message = this.options.message;
            this.buttonName1 = this.options.buttonName1;
            this.buttonName2 = this.options.buttonName2;

            this.callback1 = this.options.buttonCallback1;
            this.callback2 = this.options.buttonCallback2;

            this.callbackParam1 = this.options.buttonCallbackParam1;
            this.callbackParam2  = this.options.buttonCallbackParam2;

            _.bindAll(this, 'render', 'fadeIn', 'fadeOut');
        },

        render: function() {

            var params = {
                title: this.title,
                message: this.message
            };

            this.$el.html( this.template( params ) );

            $('body').append( this.$el );

            this.$el.addClass('hidden');

            this.$el.find('#general-popup_button-container').children().remove();

            new qcssapp.Views.ButtonView({
                text: this.buttonName1,
                buttonType: "icon",
                appendToID: "#general-popup_button-container",
                id: "general-popup_btn-1",
                callback: function() {
                    this.fadeOut();

                    if(typeof this.callback1 === 'function') {
                        this.callback1(this.callbackParam1);
                    }

                }.bind(this)
            });

            this.$el.find('#general-popup_btn-1').removeClass('button qcss-button');

            if(this.buttonName2) {

                new qcssapp.Views.ButtonView({
                    text: this.buttonName2,
                    buttonType: "icon",
                    appendToID: "#general-popup_button-container",
                    id: "general-popup_btn-2",
                    callback: function() {
                        this.fadeOut();

                        if(typeof this.callback2 === 'function') {
                            this.callback2(this.callbackParam2);
                        }

                    }.bind(this)
                });

                this.$el.find('#general-popup_btn-2').removeClass('button qcss-button');
            }

            this.fadeIn();

            return this;
        },

        fadeIn: function(dur) {
            dur = !dur ? 150 : dur;

            this.hidden = false;
            this.$el.fadeIn(dur, function() {
                this.$el.removeClass('hidden');
            }.bind(this));
        },

        fadeOut: function(dur) {
            dur = !dur ? 150 : dur;

            this.hidden = true;
            this.$el.fadeOut(dur, function() {
                this.$el.addClass('hidden');
                this.remove();
            }.bind(this));
        }
    });
})(qcssapp, jQuery);   