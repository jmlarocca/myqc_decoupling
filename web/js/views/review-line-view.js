;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.reviewLineView - View for displaying Cart Collections
     */
    qcssapp.Views.reviewLineView = Backbone.View.extend({
        //is a div
        tagName:  'li',
        className: 'review-line',

        // Cache the template function for a single item.
        template: _.template( $('#cart-line-template').html() ),

        initialize: function() {
            return this;
        },

        render: function() {
            this.model.set('info', qcssapp.Functions.formatPrice(this.model.get('total')));

            this.model.set('equation', '');
            this.model.set('showQuantity', '');
            this.model.set('showTareInfo', 'hidden');
            this.model.set('showModifiers', 'hidden');

            var thisPrice = Number(this.model.get('price'));

            if(typeof this.model.get('originalPrice') !== 'undefined' && this.model.get('originalPrice') != null && thisPrice !== "0.00") {
                thisPrice = Number(this.model.get('originalPrice'));
            }

            //Defect 4101: price needs to be access through a get() call, and cannot be accessed directly through a dot operator
            if( !$.isEmptyObject(this.model.get('prepOption')) && Number(this.model.get('prepOption').get("price")) > 0) {
                thisPrice += Number(this.model.get('prepOption').get("price"));
            }

            var lineModifiers = this.model.get('modifiers');

            if ( qcssapp.Functions.checkServerVersion(1,5) ) {
                if ( lineModifiers != null && lineModifiers.length > 0 )  {
                    for ( var i = 0; i < lineModifiers.length; i++ ) {
                        if ( lineModifiers[i] == null ) {
                            continue;
                        }

                        if( lineModifiers[i].price != 0 ) {
                            thisPrice = thisPrice + lineModifiers[i].price;
                        }

                        if( lineModifiers[i].prepOption != null && Number(lineModifiers[i].prepOption.price) > 0) {
                            thisPrice += lineModifiers[i].prepOption.price;
                        }
                    }
                }

                if(this.model.get('scaleUsed')){
                    var lineTotal = Number(Math.round(((this.model.get('quantity') * this.model.get('originalPrice')) + (thisPrice - this.model.get('originalPrice')))+"e"+2)+"e-"+2).toFixed(2);
                }
                else{
                    var lineTotal = Number(Math.round((this.model.get('quantity') * thisPrice)+"e"+2)+"e-"+2).toFixed(2);
                }

                this.model.set('total', lineTotal);
                this.model.set('info', qcssapp.Functions.formatPrice(this.model.get('total')));
            }

            // if quantity more than one then show equation and not using scale, show price as normal
            if ( this.model.attributes.quantity > 1 && !this.model.get('scaleUsed') ) {
                this.model.set('equation', this.model.get('quantity') + " <span class='quantity'>x</span> " + qcssapp.Functions.formatPrice(this.model.get('price')));

                // if product has a tare, show tare info
            } else if(this.model.get('scaleUsed') && this.model.get('fixedTareWeight') && this.model.get("tareName")){
                this.model.set('equation', Number(this.model.get('quantity')).toFixed(2) + ' lbs at ' + qcssapp.Functions.formatPrice(this.model.get('originalPrice')));
                this.model.set('grossWeight', (Number(this.model.get('quantity')) + Number(this.model.get('fixedTareWeight'))).toFixed(2));
                this.model.set("tareInfo","<div>"+ this.model.get("tareName") +": "+ this.model.get("fixedTareWeight") +" lbs</div>");
                this.model.set("grossWeightInfo","<div>Gross Weight: " +this.model.get("grossWeight")+" lbs</div>");

                // if product is using a scale, format for lbs
            } else if ( this.model.get('scaleUsed') && Number(this.model.get('quantity')) != 1 ) {
                this.model.set('equation', Number(this.model.get('quantity')).toFixed(2) + ' lbs at ' + qcssapp.Functions.formatPrice(this.model.get('originalPrice')));

            } else {
                this.model.set('showQuantity', 'hidden');
            }

            if(this.model.get('scaleUsed') && this.model.get("fixedTareWeight")){
                var grossWeight = this.model.get("quantity");
                this.model.set('grossWeight', grossWeight);
            }

            // format modifiers
            if ( this.model.get('modifiers') && this.model.get('modifiers').length > 0 ) {
                this.model.set('modifiers', qcssapp.Functions.htmlDecode( qcssapp.Functions.convertModifierAndPrepOptionArrayToCsv(this.model.attributes.modifiers)));
                this.model.set('showModifiers', '');
            }

            //if this product is part of a combo
            if(this.model.get('isCombo')) {
                this.$el.addClass('combo-line');

                //don't show the product's price or equation
                this.model.set('total', '');
                this.model.set('info', '');
                this.model.set('equation', '');
            }

            var name = qcssapp.Functions.buildProductName(this.model);
            if(name != "") {
                this.model.set('name', name);
            }

            this.model.set('name', qcssapp.Functions.htmlDecode( this.model.get('name') ) );

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            //need to remove the '$' from the line if it is a combo line and has no upcharge
            if(this.model.attributes.isCombo) {
                this.$el.find('.cart-line-total').text('');
            }

            if(this.model.get('tareInfo') != '') {
                this.$el.addClass('tare-line');
            }

            this.setHealthyIcons();

            return this;
        },

        setHealthyIcons: function(){
            if(!qcssapp.Functions.checkServerVersion(3,0))
                return;

            var healthyFlag = false;

            if(this.model.get('healthy')){
                healthyFlag = true;
                var healthyPath = "<img src=\""+qcssapp.healthyIndicator.wellness+"\">";
                this.$el.find('.cart-line_icon-container').append(healthyPath);
            }
            if(this.model.get('vegetarian')){
                healthyFlag = true;
                var vegetarianPath = "<img src=\""+qcssapp.healthyIndicator.vegetarian+"\">";
                this.$el.find('.cart-line_icon-container').append(vegetarianPath);
            }
            if(this.model.get('vegan')){
                healthyFlag = true;
                var veganPath = "<img src=\""+qcssapp.healthyIndicator.vegan+"\">";
                this.$el.find('.cart-line_icon-container').append(veganPath);
            }
            if(this.model.get('glutenFree')){
                healthyFlag = true;
                var glutenFree = "<img src=\""+qcssapp.healthyIndicator.glutenFree+"\">";
                this.$el.find('.cart-line_icon-container').append(glutenFree);
            }
            if(healthyFlag){
                this.$el.find('.cart-line_icon-container').addClass('show-icons');

                var grayscalePercent = qcssapp.Functions.getIconGrayscalePercentage();
                if(grayscalePercent != '') {
                    this.$el.find('.cart-line_icon-container img').css('filter', 'grayscale('+grayscalePercent+'%)');
                }
            }

        }
    });
})(qcssapp, jQuery);
