;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.HelpHomeKeypadView - View for displaying help info on the home keypad page
     */
    qcssapp.Views.HelpHomeKeypadView = Backbone.View.extend({
        internalName: "Help Home Keypad View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'help-home-keypad-view',

        template: _.template( $('#help-home-keypad-template').html() ),

        events: {
            'click .help-next' : 'nextHelpSlide',
            'click #myQuickPicks' : 'closeHelp',
            'click .help-close' : 'closeHelp'
        },

        homeKeypadHelp: [
            {
                id: 1,
                title: "Grab & Go ",
                text: "Click here to use your phone's camera to scan an item barcode for faster checkout",
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: ''
            },
            {
                id: 2,
                title: 'Popular Items',
                text: 'Items will have a <img src="images/icon-popular.svg"> icon next to them to indicate they are frequently purchased',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: 'hidden',
                reverse: 'reverse'
            }, {
                id: 3,
                title: 'Favorite Items and Orders',
                text: 'Items will have a <img src="images/icon-favorites.svg"> icon next to them to indicate they are one of your favorite items',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: 'hidden',
                reverse: 'reverse'
            }, {
                id: 4,
                title: 'My Quick Picks',
                text: 'Click here to view all popular items, favorite items and favorite orders',
                buttonText: 'Done',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }
        ],

        initialize: function (options) {
            this.options = options || {};

            this.parentView = this.options.parentView;

            this.$view = $('#keypadview');

            this.slideEnd = 4;
            this.slideStart = 2;

            if(!$('.scan-barcode-btn').hasClass('hidden')) {
                this.slideStart = 1;
            }

            this.render();
        },

        render: function() {
            this.$el.html( this.template( {} ) );

            this.renderSlides();

            return this;
        },

        renderSlides: function() {
            $.each(this.homeKeypadHelp, function(index, slideInfo) {
                var helpSlideView = new qcssapp.Views.HelpSlideView(slideInfo);

                this.$el.append( helpSlideView.render().$el.find('.help-slide') );

            }.bind(this));

            this.$myQuickPicks = this.$view.find('#myQuickPicks').clone();
            this.$scanBtn = this.$view.find('.scan-barcode-btn').clone();

            this.$el.append(this.$myQuickPicks);
            this.$el.append(this.$scanBtn);

            this.showPageContent();
        },

        nextHelpSlide: function() {
            //if at the end of the slide length
            if( this.slideStart == this.slideEnd ) {
                this.parentView.fadeOut();
                return;
            }

            var $currentSlide = this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]');
            $currentSlide.removeClass('active-slide');

            this.slideStart++;

            var $nextSlide = this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]');
            $nextSlide.addClass('active-slide');

            this.showPageContent();
            this.hideInactiveSlides();
        },

        showPageContent: function() {
            switch(this.slideStart) {
                case 1:
                    this.$scanBtn.addClass('help-scan-btn');
                    break;
                case 2:
                    this.$scanBtn.removeClass('help-scan-btn');
                    this.$myQuickPicks.addClass('help-quick-pick');
                    break;
                default:
                    break;
            }
        },

        setSlideStyles: function() {
            var $favoriteIcon = this.$myQuickPicks.find('.keypad-line-icons img').eq(1);

            // Slide 1
            var $helpInfoSlide1 = $('.help-slide[data-slide-id="1"] .help-info');
            var $helpHandSlide1 = $('.help-slide[data-slide-id="1"] .help-hand');

            qcssapp.Functions.setHandTopReverse(this.$scanBtn, $helpHandSlide1, 5);
            qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide1, $helpInfoSlide1, -10);

            // Slide 2
            var $helpInfoSlide2 = $('.help-slide[data-slide-id="2"] .help-info');
            this.setHelpInfoTopQuickPicks($helpInfoSlide2, 17);

            // Slide 3
            var $helpInfoSlide3 = $('.help-slide[data-slide-id="3"] .help-info');
            var $helpPointSlide3 = $('.help-slide[data-slide-id="3"] .help-point');
            this.setHelpInfoTopQuickPicks($helpInfoSlide3, 17);
            this.setHelpPointLeft($favoriteIcon, $helpPointSlide3);

            // Slide 4
            var $helpInfoSlide4 = $('.help-slide[data-slide-id="4"] .help-info');
            var $helpHandSlide4 = $('.help-slide[data-slide-id="4"] .help-hand');

            qcssapp.Functions.setHandTop(this.$myQuickPicks, $helpHandSlide4, 36);
            qcssapp.Functions.setHelpInfoTop($helpHandSlide4, $helpInfoSlide4, 10);

            this.hideInactiveSlides();
        },

        setHelpInfoTopQuickPicks: function($helpInfo, extraPadding) {
            var favoriteLineHeight = this.$myQuickPicks[0].clientHeight;

            $helpInfo.css('top', 60 + favoriteLineHeight + extraPadding);

            if($('body').hasClass('iphoneX')) {
                $helpInfo.css('top', 80 + favoriteLineHeight + extraPadding);
            }
        },

        setHelpPointLeft: function($icon, $helpPoint) {
            var handIconLeft = $icon.length ? $icon[0].offsetLeft : 0;
            var helpPointWidth = $helpPoint.length ? Number(Number($helpPoint[0].offsetWidth) / 2) : 0;

            $helpPoint.css('left', handIconLeft - helpPointWidth);
        },

        hideInactiveSlides: function() {
            if(this.slideStart != 0) {
                this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]').addClass('active-slide');
                this.$el.find('.help-slide[data-slide-id!="'+this.slideStart+'"]').addClass('inactive-slide');
            }
        },

        closeHelp: function() {
            this.parentView.fadeOut();
        }
    });
})(qcssapp, jQuery);
