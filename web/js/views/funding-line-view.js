;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.FundingLineView - View for displaying funding item Collections
     */
    qcssapp.Views.FundingLineView = Backbone.View.extend({
        //is a list tag.
        tagName:  'li',

        // Cache the template function for a single item.
        template: _.template( $('#funding-template').html() ),

        render: function() {

            this.model.set('amt', qcssapp.Functions.formatPriceInApp(this.model.get('amt')));

            this.model.set('fundingFeeText', qcssapp.Functions.formatPriceInApp(this.model.get('fundingFee'), '+'));

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            if ( this.model.get('fundingFee') && Number( this.model.get('fundingFee') ) > 0 ) {
                this.$el.find('.list-funded-fee').show();
            }

            return this;
        }
    });
})(qcssapp, jQuery);
