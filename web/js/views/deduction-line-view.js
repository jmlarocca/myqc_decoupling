;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.DeductionLineView - View for displaying Deduction Collections (Collection Item Models)
     */
    qcssapp.Views.DeductionLineView = Backbone.View.extend({
        //is a list tag.
        tagName:  'li',

        // Cache the template function for a single item.
        template: _.template( $('#deduction-template').html() ),

        render: function() {

            this.model.set('amt', qcssapp.Functions.formatPriceInApp(this.model.get('amt')));

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            return this;
        }
    });
})(qcssapp, jQuery);
