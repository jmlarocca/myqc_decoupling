;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.HelpAccountFundingView - View for displaying help info on the account funding page
     */
    qcssapp.Views.HelpAccountFundingView = Backbone.View.extend({
        internalName: "Help Account Funding View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'div',
        className: 'help-funding-view',

        template: _.template( $('#help-funding-template').html() ),

        events: {
            'click .help-next' : 'nextHelpSlide',
            'click .faq-question-link' : 'goToFAQPage',
            'click .help-close' : 'closeHelp',
            'change #accountFunding_fundAutomatic_reload-amount': 'changeReloadAmount',
            'change #accountFunding_fundAutomatic_threshold-amount': 'changeThresholdAmount'
        },

        fundingHelp: [
            {
                id: 1,
                title: 'Payment Method',
                text: 'Click here to add a payment method to fund your account',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 2,
                title: 'Account Funding',
                text: 'Click here to add funds one-time to your account',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: ''
            }, {
                id: 3,
                title: 'Account Funding',
                text: 'View your funding history here',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 4,
                title: 'Automatic Reloads',
                text: 'Click here to setup or modify automatic reloads for your account',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: '',
                reverse: 'reverse'
            }, {
                id: 5,
                title: 'Automatic Reloads',
                text: 'Select an Auto Reload amount',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: ''
            }, {
                id: 6,
                title: 'Automatic Reloads',
                text: 'Select a Balance Threshold amount',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: ''
            }, {
                id: 7,
                title: 'Automatic Reloads',
                text: 'Click Enable and your account will be automatically reloaded by X when your balance reaches X or below',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: ''
            }, {
                id: 8,
                title: 'FAQs',
                text: '',
                buttonText: 'Done',
                showArrow: 'hidden',
                showHand: 'hidden'
            }, {
                id: 9,
                title: 'Automatic Reloads',
                text: 'Click Modify to update the Reload amount and Balance Threshold amount',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: ''
            }, {
                id: 10,
                title: 'Automatic Reloads',
                text: 'Click Disable to turn off Automatic Reloads',
                buttonText: 'Next',
                showArrow: 'hidden',
                showHand: ''
            }, {
                id: 11,
                title: 'FAQs',
                text: '',
                buttonText: 'Done',
                showArrow: 'hidden',
                showHand: 'hidden'
            }
        ],

        initialize: function (options) {
            this.options = options || {};

            this.parentView = this.options.parentView;

            this.$view = $('#accountfundingview');
            this.$view.addClass('help-scroll');

            this.paymentMethodView = false;

            // The Account Funding page is not doing funding, only can add a Payment Method
            if(this.$view.find('#accountFunding_filter').hasClass('hidden')) {
                this.paymentMethodView = true;
            }

            this.slideEnd = 8;
            this.slideStart = 1;
            this.startedAutoFunding = false;
        },

        render: function() {
            this.$el.html( this.template( {} ) );

            this.renderSlides();
            this.showPageContent();

            return this;
        },

        renderSlides: function() {
            $.each(this.fundingHelp, function(index, slideInfo) {
                var helpSlideView = new qcssapp.Views.HelpSlideView(slideInfo);

                this.$el.append( helpSlideView.render().$el.find('.help-slide') );

            }.bind(this));

            if(this.$view.find('#accountFunding_filter_automatic').hasClass('selected')) {

                if(this.$view.find('#toggleAutomaticButton').text().toLocaleLowerCase() == 'disable' &&
                    this.$view.find('#accountFunding_fundAutomatic_buttons_modifying').css('display') == 'none') {
                    this.slideStart = 9;
                    this.slideEnd = this.checkForFundingFAQS() ? 11 : 10;
                } else if(this.$view.find('#accountFunding_fundAutomatic_buttons_modifying').css('display') != 'none') {
                    this.slideStart = 5;
                    this.slideEnd = 6;
                } else if (this.checkForFundingFAQS()) {
                    this.slideStart = 5;
                    this.slideEnd = 8;
                } else {
                    this.slideStart = 5;
                    this.slideEnd = 7;
                }

                this.startedAutoFunding = true;

            } else if (this.checkForFundingFAQS()) {
                this.slideStart = 1;
                this.slideEnd = 8;
            } else {
                this.slideStart = 1;
                this.slideEnd = 7;
            }

            if(this.paymentMethodView) {
                this.$el.find('.help-slide[data-slide-id="1"]').find('.help-next').text('Done');
                this.slideStart = 1;
                this.slideEnd = 1;
            }

            this.$paymentMethodContainer = this.$view.find('#accountFunding_paymentMethod').clone();
            this.$paymentMethodContainer.attr('id', 'accountFunding_paymentMethodDupe');

            this.$oneTimeFundingContainer = this.$view.find('#accountFunding_fundOneTime').clone();
            this.$oneTimeFundingContainer.attr('id', 'accountFunding_fundOneTimeDupe');

            this.$fundingBalanceContainer = this.$view.find('#accountFunding_balance').clone();
            this.$fundingBalanceContainer.attr('id', 'accountFunding_balanceDupe');

            this.$fundingFilterContainer = this.$view.find('#accountFunding_filter').clone();
            this.$fundingFilterContainer.attr('id', 'accountFunding_filterDupe');
            this.$fundingFilterContainer.css('display', 'flex');

            this.$autoFundingContainer = this.$view.find('#accountFunding_fundAutomatic').clone();
            this.$autoFundingContainer.attr('id', 'accountFunding_fundAutomaticDupe');

            this.$el.append(this.$paymentMethodContainer);
            this.$el.append(this.$oneTimeFundingContainer);
            this.$el.append(this.$fundingBalanceContainer);
            this.$el.append(this.$fundingFilterContainer);
            this.$el.append(this.$autoFundingContainer);

            var reloadAmount = this.$view.find('#accountFunding_fundAutomatic_reload-amount').val();
            var thresholdAmount = this.$view.find('#accountFunding_fundAutomatic_threshold-amount').val();

            this.$autoFundingContainer.find('#accountFunding_fundAutomatic_reload-amount').val(reloadAmount);
            this.$autoFundingContainer.find('#accountFunding_fundAutomatic_threshold-amount').val(thresholdAmount);
        },

        nextHelpSlide: function() {
            //if at the end of the slide length
            if( this.slideStart == this.slideEnd ) {

                // If you didn't start on the auto funding page but you're ending on it, switch back to one-time load page
                if(!this.startedAutoFunding && this.$view.find('#accountFunding_filter_automatic').hasClass('selected')) {
                    this.$view.find('#accountFunding_filter_one').eq(0).trigger('click');
                }

                this.$view.removeClass('help-scroll');
                this.parentView.fadeOut();
                return;
            }

            var $currentSlide = this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]');
            $currentSlide.removeClass('active-slide').addClass('inactive-slide');

            if(this.slideStart == 4 && !this.startedAutoFunding && this.$view.find('#toggleAutomaticButton').text().toLocaleLowerCase() == 'disable' && this.$view.find('#accountFunding_fundAutomatic_buttons_modifying').css('display') == 'none') {
                this.slideStart = 8;
                this.slideEnd = 10;

                if(this.isFAQPageActive()) {
                    this.slideEnd = 11;
                }
            }

            this.slideStart++;

            var $nextSlide = this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]');
            $nextSlide.addClass('active-slide');

            //if the next slide is the last, change button text to Done
            if( this.slideStart == this.slideEnd ) {
                $nextSlide.find('.help-next').text('Done');
            }

            this.showPageContent();
        },

        isFAQPageActive: function() {
            return this.$el.find('.help-slide[data-slide-id="11"]').find('.help-text').text() != '';
        },

        showPageContent: function() {
            switch(this.slideStart) {
                case 1:
                    var $removePaymentMethodBtn = this.$view.find('#removePaymentMethodBtn');
                    var $firstSlide = this.$el.find('.help-slide[data-slide-id="1"]');

                    if($removePaymentMethodBtn.length && $removePaymentMethodBtn.css('display') != 'none') {
                        $firstSlide.find('.help-text').text('Replace your payment method here or remove it entirely')
                    } else if(this.paymentMethodView) {
                        $firstSlide.find('.help-text').text('Click here to add a payment method');
                    } else {
                        $firstSlide.find('.help-text').text('Click here to add a payment method to fund your account');
                    }

                    this.$paymentMethodContainer.addClass('active-help');
                    break;
                case 2:
                    this.$paymentMethodContainer.removeClass('active-help');
                    this.$oneTimeFundingContainer.addClass('active-help');
                    break;
                case 3:
                    this.$paymentMethodContainer.removeClass('active-help');
                    this.$oneTimeFundingContainer.removeClass('active-help');
                    this.$fundingBalanceContainer.addClass('active-help');
                    break;
                case 4:
                    this.$paymentMethodContainer.removeClass('active-help');
                    this.$oneTimeFundingContainer.removeClass('active-help');
                    this.$fundingBalanceContainer.removeClass('active-help');
                    this.$fundingFilterContainer.addClass('active-help');
                    break;
                case 5:
                    var $fifthSlide = this.$el.find('.help-slide[data-slide-id="5"]');
                    $fifthSlide.find('.help-text').html('Select an Auto Reload amount');

                    if(this.$view.find('#accountFunding_fundAutomatic_buttons_modifying').css('display') != 'none') {
                        $fifthSlide.find('.help-text').html('Update the Auto Reload amount');
                    }

                    if(this.startedAutoFunding) {
                        this.$fundingFilterContainer.addClass('active-help');
                        this.$autoFundingContainer.addClass('active-help');
                        return;
                    }

                    this.$el.find('.help-slide[data-slide-id="5"]').removeClass('active-slide');

                    this.$autoFundingContainer.remove();
                    this.$paymentMethodContainer.remove();
                    this.$oneTimeFundingContainer.remove();
                    this.$fundingBalanceContainer.remove();

                    this.$fundingFilterContainer.find('#accountFunding_filter_one').removeClass('selected secondary-background-color font-color-secondary');
                    this.$fundingFilterContainer.find('#accountFunding_filter_automatic').addClass('selected');

                    this.$view.find('#accountFunding_filter_automatic').eq(0).trigger('click');

                    setTimeout(function() {
                        this.$autoFundingContainer = this.$view.find('#accountFunding_fundAutomatic').clone();
                        this.$autoFundingContainer.attr('id', 'accountFunding_fundAutomaticDupe');
                        this.$el.append(this.$autoFundingContainer);

                        this.$autoFundingContainer.addClass('active-help');

                        this.$el.find('.help-slide[data-slide-id="5"]').removeClass('inactive-slide');
                        this.$el.find('.help-slide[data-slide-id="6"]').removeClass('inactive-slide');
                        this.$el.find('.help-slide[data-slide-id="7"]').removeClass('inactive-slide');

                        this.setSlideStyles();
                    }.bind(this), 800);

                    break;
                case 6:
                    this.$autoFundingContainer.addClass('active-help');

                    var $sixthSlide = this.$el.find('.help-slide[data-slide-id="6"]');
                    $sixthSlide.find('.help-text').html('Select a Balance Threshold amount');

                    if(this.$view.find('#accountFunding_fundAutomatic_buttons_modifying').css('display') != 'none') {
                        $sixthSlide.find('.help-text').html('Update the Balance Threshold amount');
                    }

                    if(!this.startedAutoFunding && this.$view.find('#accountFunding_fundAutomatic_buttons_modifying').css('display') == 'none') {
                        $sixthSlide.find('.help-text').html('Or you can update the Balance Threshold amount');
                    }

                    break;
                case 7:
                    this.$autoFundingContainer.addClass('active-help');

                    var reloadAmount = this.$autoFundingContainer.find('#accountFunding_fundAutomatic_reload-amount').val();
                    var thresholdAmount = this.$autoFundingContainer.find('#accountFunding_fundAutomatic_threshold-amount').val();

                    var $seventhSlide = this.$el.find('.help-slide[data-slide-id="7"]');
                    $seventhSlide.find('.help-text').html('Click Enable and your account will be automatically reloaded by '+ qcssapp.Functions.formatPriceInApp(reloadAmount) +' when your balance reaches '+qcssapp.Functions.formatPriceInApp(thresholdAmount)+' or below. You can still do one-time loads when this is turned on');

                    if(!this.startedAutoFunding && this.$view.find('#toggleAutomaticButton').text().toLocaleLowerCase() == 'disable' && this.$view.find('#accountFunding_fundAutomatic_buttons_modifying').css('display') == 'none') {
                        $seventhSlide.find('.help-text').html('Clicking Disable will turn off Automatic Reloads');
                    }

                    break;
                case 8:
                    this.$autoFundingContainer.removeClass('active-help');
                    break;
                case 9:
                    if(this.startedAutoFunding) {
                        this.$fundingFilterContainer.addClass('active-help');
                        this.$autoFundingContainer.addClass('active-help');
                        return;
                    }

                    this.$el.find('.help-slide[data-slide-id="9"]').removeClass('active-slide');

                    this.$autoFundingContainer.remove();
                    this.$paymentMethodContainer.remove();
                    this.$oneTimeFundingContainer.remove();
                    this.$fundingBalanceContainer.remove();

                    this.$fundingFilterContainer.find('#accountFunding_filter_one').removeClass('selected secondary-background-color font-color-secondary');
                    this.$fundingFilterContainer.find('#accountFunding_filter_automatic').addClass('selected');

                    this.$view.find('#accountFunding_filter_automatic').eq(0).trigger('click');

                    setTimeout(function() {
                        this.$autoFundingContainer = this.$view.find('#accountFunding_fundAutomatic').clone();
                        this.$autoFundingContainer.attr('id', 'accountFunding_fundAutomaticDupe');
                        this.$el.append(this.$autoFundingContainer);

                        this.$autoFundingContainer.addClass('active-help');

                        this.$el.find('.help-slide[data-slide-id="9"]').removeClass('inactive-slide');
                        this.$el.find('.help-slide[data-slide-id="10"]').removeClass('inactive-slide');

                        this.setSlideStyles();
                    }.bind(this), 800);

                    break;
                case 11:
                    this.$fundingFilterContainer.removeClass('active-help');
                    this.$autoFundingContainer.removeClass('active-help');
                    break;
                default:
                    break;
            }
        },

        setSlideStyles: function() {
            var $parentPaymentMethodContainer = this.$view.find('#accountFunding_paymentMethod').eq(0);
            var $parentOneTimeFundingContainer = this.$view.find('#accountFunding_fundOneTime').eq(0);
            var $parentFundingBalanceContainer = this.$view.find('#accountFunding_balance').eq(0);
            var $parentFundingFilterContainer = this.$view.find('#accountFunding_filter').eq(0);
            var $parentAutoFundingContainer = this.$view.find('#accountFunding_fundAutomatic').eq(0);
            var $parentEnableAuthBtn = this.$view.find('#toggleAutomaticButton').eq(0);
            var $parentModifierAuthBtn = this.$view.find('#modifyAutomaticButton').eq(0);
            var $parentPaymentMethodBtn = $parentPaymentMethodContainer.find('#modifyPaymentMethodBtn');
            var $parentOneTimeFundingBtn = $parentOneTimeFundingContainer.find('#oneTimePaymentButton');
            var $parentFundingHistoryBtn = $parentFundingBalanceContainer.find('#fundingHistoryButton');
            var $parentReloadAmountSelector = $parentAutoFundingContainer.find('#accountFunding_fundAutomatic_reload-amount');
            var $parentThresholdAmountSelector = $parentAutoFundingContainer.find('#accountFunding_fundAutomatic_threshold-amount');

            // Slide 1
            var $helpInfoSlide1 = $('.help-slide[data-slide-id="1"] .help-info');
            var $helpHandSlide1 = $('.help-slide[data-slide-id="1"] .help-hand');

            this.setActiveElement($parentPaymentMethodContainer, this.$paymentMethodContainer);
            qcssapp.Functions.setHandTop($parentPaymentMethodBtn, $helpHandSlide1, 20);
            qcssapp.Functions.setHelpInfoTop($helpHandSlide1, $helpInfoSlide1);

            // Slide 2
            var $helpInfoSlide2 = $('.help-slide[data-slide-id="2"] .help-info');
            var $helpHandSlide2 = $('.help-slide[data-slide-id="2"] .help-hand');

            this.setActiveElement($parentOneTimeFundingContainer, this.$oneTimeFundingContainer);
            qcssapp.Functions.setHandTopReverse($parentOneTimeFundingBtn, $helpHandSlide2, 10);
            qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide2, $helpInfoSlide2);

            // Slide 3
            var $helpInfoSlide3 = $('.help-slide[data-slide-id="3"] .help-info');
            var $helpHandSlide3 = $('.help-slide[data-slide-id="3"] .help-hand');

            this.setActiveElement($parentFundingBalanceContainer, this.$fundingBalanceContainer);
            qcssapp.Functions.setHandTop($parentFundingHistoryBtn, $helpHandSlide3, 20);
            qcssapp.Functions.setHelpInfoTop($helpHandSlide3, $helpInfoSlide3);

            // Slide 4
            var $helpInfoSlide4 = $('.help-slide[data-slide-id="4"] .help-info');
            var $helpHandSlide4 = $('.help-slide[data-slide-id="4"] .help-hand');

            this.setActiveElement($parentFundingFilterContainer, this.$fundingFilterContainer);
            qcssapp.Functions.setHandTop(this.$fundingFilterContainer, $helpHandSlide4, 20);
            qcssapp.Functions.setHelpInfoTop($helpHandSlide4, $helpInfoSlide4);

            var scrollTop = 0;
            if(this.$view.scrollTop() > 0) {
                scrollTop = $('#accountfundingview').scrollTop();
            }

            // Slide 5
            var $helpInfoSlide5 = $('.help-slide[data-slide-id="5"] .help-info');
            var $helpHandSlide5 = $('.help-slide[data-slide-id="5"] .help-hand');
            var extraPadding5 = scrollTop == 0 ? 20 : 20 + scrollTop;

            this.setActiveElement($parentAutoFundingContainer, this.$autoFundingContainer);
            qcssapp.Functions.setHandTopReverse($parentReloadAmountSelector, $helpHandSlide5, extraPadding5);
            qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide5, $helpInfoSlide5);

            // Slide 6
            var $helpInfoSlide6 = $('.help-slide[data-slide-id="6"] .help-info');
            var $helpHandSlide6 = $('.help-slide[data-slide-id="6"] .help-hand');
            var extraPadding6 = scrollTop == 0 ? 20 : 20 + scrollTop;

            qcssapp.Functions.setHandTopReverse($parentThresholdAmountSelector, $helpHandSlide6, extraPadding6);
            qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide6, $helpInfoSlide6, 10);

            // Slide 7
            var $helpInfoSlide7 = $('.help-slide[data-slide-id="7"] .help-info');
            var $helpHandSlide7 = $('.help-slide[data-slide-id="7"] .help-hand');
            var extraPadding7 = scrollTop == 0 ? 15 : 15 + scrollTop;

            qcssapp.Functions.setHandTopReverse($parentEnableAuthBtn, $helpHandSlide7, extraPadding7);
            qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide7, $helpInfoSlide7, 95);

            // Slide 9
            var $helpInfoSlide9 = $('.help-slide[data-slide-id="9"] .help-info');
            var $helpHandSlide9 = $('.help-slide[data-slide-id="9"] .help-hand');
            var extraPadding9 = scrollTop == 0 ? 15 : 15 + scrollTop;

            qcssapp.Functions.setHandTopReverse($parentModifierAuthBtn, $helpHandSlide9, extraPadding9);
            qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide9, $helpInfoSlide9, 55);

            // Slide 10
            var $helpInfoSlide10 = $('.help-slide[data-slide-id="10"] .help-info');
            var $helpHandSlide10 = $('.help-slide[data-slide-id="10"] .help-hand');
            var extraPadding10 = scrollTop == 0 ? 15 : 15 + scrollTop;

            qcssapp.Functions.setHandTopReverse($parentEnableAuthBtn, $helpHandSlide10, extraPadding10);
            qcssapp.Functions.setHelpInfoTopReverse($helpHandSlide10, $helpInfoSlide10, 55);

            this.hideInactiveSlides();
        },

        setActiveElement: function($parentIcon, $icon) {
            if(!$parentIcon.length || !$icon.length) {
                return;
            }

            var parentIconTop = $parentIcon[0].offsetTop + 60;
            var parentIconLeft = $parentIcon[0].offsetLeft;

            if($('body').hasClass('iphoneX')) {
                parentIconTop = parentIconTop + 20;
            }

            if(this.$view.find('#accountFunding_filter_automatic').hasClass('selected') && this.$view.find('#accountFunding').scrollTop() > 0) {
                parentIconTop = $parentIcon[0].offsetTop - (this.$view.find('#accountFunding').scrollTop() - 60);

                if($parentIcon.attr('id') == 'accountFunding_filter' && ($parentIcon.height() + parentIconTop < (60 + $parentIcon.height()))) {
                    $icon.hide();
                }
            }

            $icon.css({'top': parentIconTop, 'left': parentIconLeft});
        },

        hideInactiveSlides: function() {
            if(this.slideStart != 0) {
                this.$el.find('.help-slide[data-slide-id="'+this.slideStart+'"]').addClass('active-slide');
                this.$el.find('.help-slide[data-slide-id!="'+this.slideStart+'"]').addClass('inactive-slide');
            }

            this.parentView.$el.height(this.$view[0].scrollHeight);
        },

        checkForFundingFAQS: function() {
            var $navLink = $('#nav-faq');
            var FAQCollection = qcssapp.accountModel.get('FAQs');
            var fundingFAQs = false;

            if($navLink.length == 0) {
                return false;
            }

            this.$el.find('.help-slide[data-slide-id="8"]').find('.help-title').text('');

            if(typeof FAQCollection !== 'undefined' && FAQCollection.length > 0) {
                FAQCollection.each(function( mod ) {
                    if(mod.get('linkToAccountFunding')) {
                        var question = qcssapp.Functions.htmlDecode(mod.get('question'));
                        var $questionElement = '<div class="faq-question-link" data-id="'+mod.get('id')+'">'+question+'</div>';

                        this.$el.find('.help-slide[data-slide-id="8"]').find('.help-text').append($questionElement);
                        this.$el.find('.help-slide[data-slide-id="11"]').find('.help-text').append($questionElement);

                        fundingFAQs = true;
                    }
                }, this );
            }

            if(fundingFAQs) {
                this.$el.find('.help-slide[data-slide-id="8"]').find('.help-title').text($navLink.text().trim());
                this.$el.find('.help-slide[data-slide-id="11"]').find('.help-title').text($navLink.text().trim());
            }

            return fundingFAQs;
        },

        goToFAQPage: function(e) {
            var $faq = $(e.currentTarget);
            var faqID = $faq.attr('data-id');

            qcssapp.Functions.showMask();

            if(!this.startedAutoFunding) {
                this.$view.find('#accountFunding_filter_one').eq(0).trigger('click');
            }

            this.$view.removeClass('help-scroll');
            this.parentView.fadeOut();

            var faqOptions = {loginFAQ: false, faqID: faqID};
            new qcssapp.Views.FAQ(faqOptions);
            qcssapp.Router.navigate('faq', {trigger: true});
        },

        closeHelp: function() {
            this.$view.removeClass('help-scroll');
            this.parentView.fadeOut();
        },

        changeReloadAmount: function() {
            this.$view.find('#accountFunding_fundAutomatic_reload-amount').val(this.$autoFundingContainer.find('#accountFunding_fundAutomatic_reload-amount').val());
        },

        changeThresholdAmount: function() {
            this.$view.find('#accountFunding_fundAutomatic_threshold-amount').val(this.$autoFundingContainer.find('#accountFunding_fundAutomatic_threshold-amount').val());
        }
    });
})(qcssapp, jQuery);
