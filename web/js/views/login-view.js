;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.LoginView - View for displaying the login screen
     */
    qcssapp.Views.LoginView = Backbone.View.extend({
        internalName: "Login View", //NOT A STANDARD BACKBONE PROPERTY

        //TODO: This should be the whole panel
        el: '#loginFormContainer',

        //TODO: There aren't any variables in this template - is there a reason to use a template at all?
        template: _.template( $('#login-template').html() ),

        //handle "enter" events on form fields
        events: {
            "keyup #loginPassword": "enterEventHandler",
            "keyup #loginName": "enterEventHandler",
            "click .keepLogged-info-icon": "handleKeepLogged",
            "change #enableFingerprint": "handleFingerprint"
        },

        initialize: function () {
            //start of application
            if (qcssapp.DebugMode) {
                console.log("Initialize login view");
            }

            this.$loginSelect = $('#loginCodeSelect');
            this.instanceCodeSelectView = new qcssapp.Views.instanceCodeSelectView();

            //render the view whether online or not
            this.render();

            if( !this.checkQRCodeMode() ) {
                return;
            }

            this.checkSession();

            //TODO: Move this - change the el and add this event & handler like normal
            $(document).on('click', '#forgotPasswordLink', function() {
                var $this = $(this);
                var $content = $('#forgotPasswordContent');
                var currentLocationURL = ( $('#loginCodeSelect').val() || qcssapp.Location )  + '/api/auth/login/settings';

                var locationURL = $this.attr('data-locationurl');
                if ( locationURL != 'undefined' && locationURL != "" ) {
                    if ( currentLocationURL != locationURL ) {
                        $this.removeClass('inCloud').removeClass('onGround');
                    }
                }

                if ( $this.hasClass('inCloud') ) {
                    qcssapp.Router.navigate('forgot-password-page', {trigger:true});
                } else if ( $this.hasClass('onGround') ) {
                    $content.slideToggle();
                } else {

                    qcssapp.Functions.callAPI(currentLocationURL, 'GET', '', '',
                        function(data) {
                            $this.attr('data-locationurl', currentLocationURL);

                            if ( data.hasOwnProperty('onGround') && data.onGround == "false" ) {
                                $this.addClass('inCloud');
                                $content.slideUp();
                                qcssapp.Router.navigate('forgot-password-page', {trigger:true});
                            } else {
                                $this.addClass('onGround');
                                $content.slideToggle();
                            }
                        },
                        function() {
                            if ( qcssapp.DebugMode ) {
                                console.log('Could not determine if user in cloud in forgot password button click.');
                            }
                        }, '', '');
                }
            });

            $(document).on('click', '#createAccountLink', function() {
                if(qcssapp.Functions.checkServerVersion(6,0,8) && !qcssapp.CreateAccountSettings.settingsModel && qcssapp.CreateAccountSettings.accountGroups && qcssapp.CreateAccountSettings.accountGroups.length) {
                    new qcssapp.Views.CreateAccountView(0);
                    qcssapp.Router.navigate('create-account-page', {trigger:true});
                } else {

                    if(qcssapp.CreateAccountSettings.settingsModel.get('allowMyQCPersonAccountCreation') && qcssapp.Functions.checkServerVersion(2,0)) {
                        $('#person-password-page').off();

                        if(qcssapp.onGround) {
                            qcssapp.Functions.displayError("Account creation is not configured correctly for email accounts.");
                            return;
                        }

                        qcssapp.Functions.showMask();
                        new qcssapp.Views.PersonPasswordView();
                        qcssapp.Router.navigate('person-password', {trigger:true});

                    } else if(qcssapp.CreateAccountSettings.settingsModel.get('allowMyQCAccountCreation')) {
                        var accountGroups = qcssapp.CreateAccountSettings.accountGroups.models;

                        if(qcssapp.CreateAccountSettings.settingsModel.get('accountEmailForUsername') && qcssapp.onGround) {
                            qcssapp.Functions.displayError("Account creation is not configured correctly for email accounts.");
                            return;  //are these necessary?
                        }

                        if(typeof accountGroups === 'undefined') {
                            qcssapp.Functions.displayError("No Account Groups are eligible for account creation.");
                            return;  //are these necessary?
                        }

                        //throw error if one account group is found but no associated spending profiles or account types
                        if(accountGroups.length == 1) {
                            if(accountGroups[0].attributes.spendingProfiles.length < 1) {
                                qcssapp.Functions.displayError("Cannot create an account because no Spending Profiles are mapped to the Account Group");
                                return;
                            }

                            if(accountGroups[0].attributes.accountTypeID == "") {
                                qcssapp.Functions.displayError("Cannot create an account because the Account Group does not have an Account Type configured.");
                                return;
                            }

                            //set the account type span
                            $('#accountTypeSpan').text('Account Type: ' + accountGroups[0].attributes.accountTypeName);
                        } else {
                            var found = false;
                            $.each(accountGroups, function() {
                                if(this.attributes.spendingProfiles.length > 0 && this.attributes.spendingProfiles[0].spendingProfileName != "") {
                                    found = true;
                                }
                            });
                            if(!found) {
                                qcssapp.Functions.displayError("No Spending Profiles are mapped to any Account Groups");
                                return;
                            }
                        }

                        new qcssapp.Views.CreateAccountView(0);
                        qcssapp.Router.navigate('create-account-page', {trigger:true});

                    }

                }
            });

            $(document).on('click', '#faqLink', function() {
                if(qcssapp.help.loginFAQSetID == null) {
                    qcssapp.Functions.displayError('Registration FAQs are unavailable at this time.');
                    return;
                }

                var faqOptions = {'loginFAQ': true};

                new qcssapp.Views.FAQ( faqOptions );
                qcssapp.Router.navigate('faq', {trigger: true});

            });
        },

        render: function() {
            //TODO: After changing el, change this to update the form div
            this.$el.prepend( this.template() );

            //always build page views because the user can log out back to the login screen
            this.buildPageViews(this.instanceCodeSelectView);
        },

        //determines whether to show the standard login page or to persist the session and bring user to the main nav
        checkSession: function() {
            if ( qcssapp.DebugMode ) {
                console.log('loginView.checkSession called...');
            }

            if ( qcssapp.preventStandardStartup ) {
                return;
            }

            var GSkey = qcssapp.Functions.getParameterByName("GSKey");
            var instanceUserID = qcssapp.Functions.getParameterByName("instanceUserID");
            var ssoValidation = qcssapp.Functions.getParameterByName("ssoValidation");
            var resumeApp = qcssapp.Functions.getParameterByName('resumeApp');
            var transactionID = qcssapp.Functions.getParameterByName('transid');
            var mobileBrowserSSO = qcssapp.Functions.getParameterByName('mobileBrowserSSO');
            var status = qcssapp.Functions.getParameterByName('status');
            var ssoOrigin = localStorage.getItem("SSO-Origin");
            if ( status && status.indexOf('#') != -1 ) {
                status = status.split('#')[0];
            }

            var location = window.location.origin + window.location.pathname.substr(0,window.location.pathname.length-1);
            var locationSettingsEndPoint = location + '/api/auth/login/settings';

            if ( resumeApp ) {
                if ( qcssapp.DebugMode ) {
                    console.log('handleOpenURL called...');
                }

                //if in the mobile browser and not using the mobile browser, return to the app
                if ( qcssapp.onMobileBrowser && !this.checkSessionResumeApp() ) {
                    qcssapp.Functions.hideMask();
                    window.location = 'myqc://?resumeApp=1&transid='+transactionID+'&status='+status;

                    // shouldn't exist, but just a safety measure to prevent multiple browsers from storing sessions
                    localStorage.removeItem('SSO-Origin');
                    return;
                }

                //if desktop or using mobile browser (phone app will be called from openURL)
                if ( !qcssapp.onPhoneApp ) {
                    var resumeSuccessParameters = {
                        status: status,
                        transactionID: transactionID,
                        location:location
                    };

                    var resumeSuccessFunction = function(resumeSuccessParameters) {
                        qcssapp.Functions.resumeSession();

                        qcssapp.Functions.resumeFunding(resumeSuccessParameters.status, resumeSuccessParameters.transactionID);
                    };

                    qcssapp.Functions.checkLoginSettings(locationSettingsEndPoint, resumeSuccessParameters, resumeSuccessFunction);
                }
            } else if ( ssoValidation ) {
                //replace the get parameters to hide them from the user
                if ( history && history.replaceState ) {
                    history.replaceState("?", "title", "/myqc/");
                }

                //detect mobile browser
                if( !mobileBrowserSSO && typeof window.cordova == "undefined" && (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) ) {
                    qcssapp.Functions.hideMask();

                    // D-4311
                    // remove all session111 items from local storage so the app will open instead of the web app in mobile browser
                    // if no app is present on the device, it will open in the browser
                    var keys = Object.keys(localStorage).filter( key => {
                        return key.indexOf('mmh-user-session111') !== -1;
                    });
                    for(var key of keys) {
                        localStorage.removeItem(key);
                    }

                    // Ensure this browser or app is the origin of the sso request
                    window.location = ssoOrigin != 1 ? 'myqc://?ssoValidation='+ssoValidation : window.location.origin + '/myqc?mobileBrowserSSO=true?ssoValidation=' + ssoValidation;
                    localStorage.removeItem('SSO-Origin');

                    return;
                }

                var ssoSuccessParameters = {
                    ssoValidation: ssoValidation,
                    location: location
                };

                var ssoSuccessFunction = function(ssoSuccessParameters) {
                    qcssapp.Functions.handleSSOLogin(ssoSuccessParameters.ssoValidation, ssoSuccessParameters.location);
                };

                qcssapp.Functions.checkLoginSettings(locationSettingsEndPoint, ssoSuccessParameters, ssoSuccessFunction);

            } else if ( GSkey && GSkey.length && instanceUserID && instanceUserID.length ) {
                //if logging in from cloud, will be sent GSkey and instanceUserID, used to validate the user to an instance

                //replace the get parameters to hide them from the user
                if ( history && history.replaceState ) {
                    history.replaceState("?", "title", "/myqc/");
                }

                var successParameters = {
                    location:location,
                    GSkey:GSkey,
                    instanceUserID:instanceUserID,
                    loginView: this
                };

                var successFunction = function(successParameters) {
                    successParameters.loginView.validateUser(successParameters.location, '', successParameters.GSkey, successParameters.instanceUserID);
                };

                qcssapp.Functions.checkLoginSettings(locationSettingsEndPoint, successParameters, successFunction);
            } else if ( this.checkFirstLogin() ) {
                //if user has logged in before, see if their DS key is still valid
                //new model to fetch session information
                var mdl = new qcssapp.Models.SessionModel();
                mdl.fetch();
                var userSession = mdl.get('sessionID');

                //if session is not saved in localStorage
                if (!mdl || !userSession || userSession == '' || mdl.attributes.forcePassword ||
                    (!mdl.attributes.keepLogged && !mdl.attributes.checkFingerprintOnDevice)) {
                    var loggedOutSuccessFunction = function () {
                        //check for SSO code, and if found redirect to the logout view
                        if (qcssapp.globalAuthType != "4") {
                            //otherwise just reset any generated content and stay on the login view
                            qcssapp.Functions.resetOnLogout();
                            return;
                        }

                        //if SSO code is saved but there is no valid session - user needs to reauth with IDP
                        new qcssapp.Views.LogoutView();
                        qcssapp.Router.navigate('logout-page', {trigger: true});

                        qcssapp.Functions.hideMask();

                        setTimeout(function () {
                            //TODO: extract this to a common function
                            if (navigator && navigator.splashscreen) {
                                navigator.splashscreen.hide();
                            }
                        }, 400);
                    };

                    var loggedOutEndPoint = this.$loginSelect.val() + '/api/auth/login/settings';

                    var successParameter = {location: this.$loginSelect.val()};
                    qcssapp.Functions.checkLoginSettings(loggedOutEndPoint, successParameter, loggedOutSuccessFunction);
                    return;
                }

                //get the location from the saved localStorage session
                var userLocation = mdl.get('location');
                var userCode = mdl.get('code');

                //TODO: Use the collection to determine number of saved codes
                //ensure the previous code is selected in case the user logs out
                if ( Number(this.$loginSelect.find('option').length) > 1 ) {
                    this.$loginSelect.val(userLocation);
                }

                //set the session and location
                qcssapp.Functions.setSession(userSession, userLocation, '', userCode);

                //end point location for saved session
                var savedSessionEndPoint = qcssapp.Location + '/api/auth/login/settings';

                var loginParameters = {
                    location: userLocation,
                    registerToken: true // Ensures that registerToken() will be called once the server version is known
                };
                qcssapp.Functions.checkLoginSettings(savedSessionEndPoint, loginParameters, qcssapp.Functions.loginCheck);
            } else {
                //if user has never logged in
                qcssapp.Functions.hideMask();

                setTimeout(function() {
                    //TODO: extract this to a common function
                    if ( navigator && navigator.splashscreen ) {
                        navigator.splashscreen.hide();
                    }
                }, 400);

                //on the phone - need to send user right to the enter code page
                if ( qcssapp.onPhoneApp ) {
                    qcssapp.Router.navigate('enterCode', {trigger:true});
                    return;
                }

                //on desktop/browser - save the current location as a code
                var neverLoggedInParams = {
                    instanceCodeSelectView: this.instanceCodeSelectView,
                    location: location,
                    loginView: this
                };

                var neverLoggedInFunction = function(neverLoggedInParams) {
                    var codeObj = {
                        CODE: 'Home Server',
                        ALIAS: 'Home Server',
                        LOCATION: location,
                        NAME: '',
                        USED: 1
                    };

                    neverLoggedInParams.loginView.handleSaveCode(codeObj, neverLoggedInParams.instanceCodeSelectView);

                    //check globalAuthType here and redirect to logout if SSO auth type
                    if ( qcssapp.globalAuthType == "4" ) {
                        new qcssapp.Views.LogoutView();
                        qcssapp.Router.navigate('logout-page', {trigger:true});
                        return;
                    }

                    if ( qcssapp.showTour ) {
                        qcssapp.Functions.showAboutPageElements();
                        qcssapp.Router.navigate('tour-page-0', {trigger: true});
                        return;
                    }

                    qcssapp.Router.navigate('login-page', {trigger: true});
                };

                qcssapp.Functions.checkLoginSettings(locationSettingsEndPoint, neverLoggedInParams, neverLoggedInFunction);
            }
        },

        //adds all buttons and fields that require models and templates to the login flow (enter code, tour, add code, forgot pw, choose app, tos)
        buildPageViews: function(instanceCodeSelectView) {
            var that = this;
            //Login Button
            new qcssapp.Views.ButtonView({
                text: "Login",
                appendToID: "#loginFormContainer",
                buttonType: "customCB",
                class: "primary-background-color font-color-primary primary-border-background-color",
                endPointURL:  qcssapp.Location + '/api/auth/login/accounts',
                parentView: this,
                id: "loginButton",
                callback: function() {
                    this.parentView.loginClick();
                }
            });

            //TODO: assign this to a variable and reference $el instead of jumping into DOM
            //Add Code Button
            new qcssapp.Views.ButtonView({
                text: "Add Code",
                buttonType: "link",
                appendToID: "#loginFormContainer",
                pageLinkID: "#add-application",
                id: "addInstance",
                class: "hidden primary-border-background-color primary-button-font"
            });

            var $addCode = $('#addInstance');

            if ( qcssapp.Functions.getParameterByName('showAddCode') == 1 ) {
                $addCode.show();
            }

            if (qcssapp.onPhoneApp && qcssapp.enableFingerprint && qcssapp.Functions.checkServerVersion(1,6)) {
                qcssapp.Functions.isFingerprintAvailableOnDevice(false);
            }

            qcssapp.Functions.checkLoginNameParameter();

            if ( instanceCodeSelectView.collection.length > 1 ) {
                this.$loginSelect.parent().parent().slideDown();
                $addCode.show();
            }

            //fire the login settings call (fires on change)
            this.$loginSelect.val(this.$loginSelect.find('option').last().val());

            //TODO: dont build new view unless addcode is showing - in route?
            //Save Code Button - Add code page
            new qcssapp.Views.ButtonView({
                text: "Save Code",
                buttonType: "customCB",
                appendToID: "#addApplicationContent",
                saveTargetID: "#instanceCodeInput",
                id: "saveInstanceCode",
                class: "button-flat accent-color-one",
                parentView: this,
                callback: function() {

                    //turn off checkFingerprintOnDevice in local storage
                    if(qcssapp.Functions.fingerprintEnabled()) {
                        qcssapp.loggedOutFingerprint = false;
                        $('#loginName, #loginPassword').show();
                        qcssapp.Functions.turnOffFingerprint(1);
                    }

                    //get the code
                    var $codeInput = $(this.saveTargetID);
                    var code = $codeInput.val().toLowerCase().trim();

                    this.parentView.saveCode(code, instanceCodeSelectView);
                    that.cleanupIcons();
                }
            });

            //TODO: clean up
            //Save Code Button - enter code page
            new qcssapp.Views.ButtonView({
                text: "Save Code",
                buttonType: "customCB",
                appendToID: "#enterInstanceCodeContent",
                saveTargetID: "#enterInstanceCode",
                id: "saveEnterInstanceCode",
                class: "button-flat accent-color-one",
                parentView: this,
                callback: function() {
                    //get the code
                    var $codeInput = $(this.saveTargetID);
                    var code = $codeInput.val().toLowerCase().trim();

                    this.parentView.saveCode(code, instanceCodeSelectView, 'tour-page-0');
                    that.cleanupIcons();
                }
            });

            //TODO: dont build new view unless addcode is showing - in route?
            //Cancel add code Button
            new qcssapp.Views.ButtonView({
                text: "Cancel",
                buttonType: "customCB",
                appendToID: "#addApplicationContent",
                pageLinkID: "#login-page",
                id: "cancelAddCodeBtn",
                class: "button-flat accent-color-one",
                callback: function() {
                    qcssapp.Router.navigate('login-page', {trigger:true});
                }
            });

            //TODO: clean up
            //Get Started Button
            new qcssapp.Views.ButtonView({
                text: "Get Started",
                buttonType: "customCB",
                appendToID: "#tour-logo-get-started-container",
                pageLinkID: "#login-page",
                id: "getStartedBtn",
                class: "primary-background-color font-color-primary primary-gradient-color primary-border-color",
                callback: function() {
                    qcssapp.Router.navigate('login-page', {trigger:true});
                }
            });

            //Forgot Password Button
            new qcssapp.Views.ButtonView({
                text: "Reset",
                buttonType: "customCB",
                appendToID: "#forgot-password-form-container",
                id: "resetPasswordBtn",
                class: "align-center button-flat accent-color-one",
                callback: function() {
                    var endPoint = 'https://www.mmhcloud.com/gateway/api/auth/reset/request';

                    var dataParameters = $('#forgotPWName').val();

                    if(dataParameters.trim().length === 0) {
                        qcssapp.Functions.displayError("Please enter your username or email before clicking \"Reset\"");
                        return;
                    }

                    //TODO: Adjust callAPI parameters
                    qcssapp.Functions.callAPI(endPoint, 'POST', dataParameters, '',
                        function(data) {
                            var $msgContainer = $('#forgot-password-page').find('.error-msg-container');

                            var resetResult = qcssapp.Functions.htmlDecode(data["details"]);
                            var isValid = data["isValid"];
                            var resetErrors = data["errorDetails"];

                            if (resetErrors) {
                                $msgContainer.addClass('primary-color').slideDown().html('An error has occurred! Please contact your manager if the error continues.');
                            } else if (resetResult && isValid) {
                                $msgContainer.removeClass('primary-color').slideDown().html('Successfully sent email!');
                            } else if (resetResult) { //report message from server
                                $msgContainer.addClass('primary-color').slideDown().html(resetResult);
                            }
                        },
                        function(params) {
                            //TODO: switch to displayError
                            var $msgContainer = $('#forgot-password-form-container').find('.error-msg-container');
                            $msgContainer.addClass('primary-color').slideDown().html('There was an error attempting to reset your password, please try again later.');
                        },
                        function() {
                            qcssapp.Functions.hideMask();
                        }
                    );
                }
            });

            //Cancel forgot password Button
            new qcssapp.Views.ButtonView({
                text: "Cancel",
                buttonType: "customCB",
                appendToID: "#forgot-password-form-container",
                pageLinkID: "#login-page",
                id: "cancelForgotPWBtn",
                class: "button-flat accent-color-one",
                callback: function() {
                    qcssapp.Router.navigate('login-page', {trigger:true});
                }
            });
        },

        //displays warning to user when they check the keepMeLogged in flag
        handleKeepLogged: function() {
            //TODO: should be a view property
            var $this = $('#keepLogged');

            //TODO: fix this, check error first
            if ( $this.is(':checked') ) {
                var message = "By choosing this option you will not be logged out on this device until you logout from the main menu in the application. Please do NOT select this option on public devices or mobile devices without locks.";
                qcssapp.Functions.displayPopup(message, 'Warning', 'Ok');

            }
        },

        //handles the click of the login button
        loginClick: function() {
            //if fingerprint is enabled and the user logged out OR hit cancel when verifying fingerprint... if the session is still valid, allow them to verify fingerprint and login
            if(qcssapp.loggedOutFingerprint && qcssapp.Functions.fingerprintEnabled() && qcssapp.Functions.getSessionID() != '') {
                qcssapp.verifyFingerprintFromLogin = true;
                qcssapp.Functions.verifyFingerprintOnDevice();
                return;
            }

            //get user's credentials
            var loginNameInput = $('#loginName').val();
            var loginName = String.prototype.trim.call(loginNameInput == null ? "" : loginNameInput);
            var loginPassword = $('#loginPassword').val();
            var loginCodeLocation = this.$loginSelect.val();
            var loginCode = this.$loginSelect.find('option:selected').attr('id');
            var keepLogged = $("#keepLogged").is(':checked');
            var enableFingerprint = $("#enableFingerprint").is(':checked');

            if(enableFingerprint && !keepLogged) {  //if keep logged was turned off on Logout when Enable Fingerprint was on
                keepLogged = true;
            }

            //replace iOS apostrophe for one the rest of the world uses
            if ( loginName.indexOf('\u2019') != -1 ) {
                loginName = loginName.split('\u2019').join('\'');
            }
            if ( loginPassword.indexOf('\u2019') != -1 ) {
                loginPassword = loginPassword.split('\u2019').join('\'');
            }

            //verify somewhat reasonable credentials before trying the server
            if ( loginName.length < 2 || loginPassword.length < 3 ) {
                //warn the user about invalid credentials
                if (qcssapp.DebugMode) {
                    console.log("User entered invalid credentials");
                }
                qcssapp.Functions.displayError('Please enter a valid Username and Password.');
                return;
            }

            if( !loginCodeLocation ) {
                var mdl = new qcssapp.Models.SessionModel();
                mdl.fetch();
                loginCodeLocation = mdl.get('location');

                if( !loginCodeLocation ) {
                    $('#addInstance').show();
                    qcssapp.Functions.displayError('The server information is invalid. Please enter a different App Code.');
                    return;
                }
            }

            //need to check code first
            var endPoint = 'https://www.mmhcloud.com/gateway/api/auth/code/'+loginCode;

            var successParameters = {
                loginCode:loginCode,
                loginName:loginName,
                loginPassword:loginPassword,
                keepLogged:keepLogged,
                loginCodeLocation:loginCodeLocation,
                instanceCodeSelectView:this.instanceCodeSelectView,
                loginView:this,
                preventOfflineError:true,
                checkFingerprintOnDevice:enableFingerprint
            };

            qcssapp.Functions.callAPI(endPoint, 'GET', '', successParameters,
                function(data, successParameters) {
                    //TODO: fix this, check specific first, remove else
                    if ( typeof successParameters.loginCode !== 'undefined' && successParameters.loginCode.toLowerCase() != 'home server' && successParameters.loginCode.toLowerCase() != 'default' && successParameters.loginCode.toLowerCase() != 'sso' ) {
                        if ( !data.LOCATION ) {
                            if (qcssapp.DebugMode) {
                                console.log("Checking for code update failed to return a location for code: "+successParameters.loginCode);
                            }
                        }

                        //all attributes of the code
                        var attrs = {
                            CODE: data.CODE.toLowerCase(),
                            ALIAS: data.ALIAS,
                            LOCATION: data.LOCATION.trim(),
                            NAME: data.NAME,
                            INSTANCEID: data.INSTANCEID,
                            INSTANCEUSERID: data.INSTANCEUSERID,
                            ALLOWINVALIDSSLCERTS: data.ALLOWINVALIDSSLCERTS
                        };

                        //if code is updated then set new location, otherwise use given location
                        if ( attrs.LOCATION != successParameters.loginCodeLocation ) {
                            if (qcssapp.DebugMode) {
                                console.log("Code has changed, updating session and location");
                            }

                            var mdl = successParameters.instanceCodeSelectView.collection.get(successParameters.loginCode);
                            mdl.save(attrs);

                            qcssapp.Location = attrs.LOCATION;
                        } else {
                            qcssapp.Location = successParameters.loginCodeLocation;
                        }
                        // never allow unsecure certs
                        if ( typeof cordova !== 'undefined' && cordova && cordova.plugins && cordova.plugins.certificates ) {
                            cordova.plugins.certificates.trustUnsecureCerts(false);
                        }
                    } else {
                        var location = successParameters.loginView.$loginSelect.val();
                        if ( successParameters.loginCode.toLowerCase() == 'sso' && qcssapp.onPhoneApp ) {
                            qcssapp.Location = location
                        } else {
                            location = window.location.origin + window.location.pathname;
                            qcssapp.Location = location.substr(0, location.length - 1);
                        }
                    }

                    successParameters.loginView.handleLogin(data, successParameters);
                },
                function(params) {
                    if (qcssapp.DebugMode) {
                        console.log("Error checking code validity");
                    }

                    //ensure Location is set
                    if ( params.loginCodeLocation && params.loginCodeLocation != qcssapp.Location ) {
                        qcssapp.Location = params.loginCodeLocation
                    }

                    params.loginView.handleLogin('', params);

                    qcssapp.Functions.hideMask();
                },
                '',
                successParameters,
                '',
                false,
                true,
                true
            );
        },

        //handles actually logging the user in
        handleLogin: function(data, params) {
            var loginEndPoint = qcssapp.Location + '/api/auth/login/accounts';

            var dataParameters = JSON.stringify({
                loginName:params.loginName,
                loginPassword:params.loginPassword,
                keepLogged:params.keepLogged
            });

            var loginSuccessParameters = {
                instanceCodeSelectView:params.instanceCodeSelectView,
                loginCode:params.loginCode,
                loginView:params.loginView,
                keepLogged:params.keepLogged,
                checkFingerprintOnDevice:params.checkFingerprintOnDevice
            };
			
			qcssapp.ssoLogin = false;

            qcssapp.Functions.callAPI(loginEndPoint, 'POST', dataParameters, loginSuccessParameters,
                function(response, loginSuccessParameters) {
                    loginSuccessParameters.loginView.loginSuccess(response, loginSuccessParameters);
                },
                function() {
                    if (qcssapp.DebugMode) {
                        console.log("Attempt to login failed due to server communication error");
                    }

                    qcssapp.Functions.hideMask();
                    qcssapp.Functions.displayError('Error communicating with the server, please try again later.');
                },
                '',
                '',
                30000,
                false,
                true,
                true
            );
        },

        loginSuccess: function(response, loginSuccessParameters) {
            if ( typeof response.isValid === 'undefined' ) {
                response = JSON.parse(response);
            }

            if (qcssapp.DebugMode) {
                console.log(response);
                console.log(response.details);
                console.log(response.isValid);
            }

            //check the validation object for success
            if ( (response.details != "success" && response.details != "redirect") || !response.isValid ) {
                if (qcssapp.DebugMode) {
                    console.log("Login Failure");
                }

                if ( qcssapp.onPhoneApp ) {
                    $('#addInstance').show();
                }

                if(response.details == "gift-card") {
                    qcssapp.Functions.hideMask();
                    qcssapp.Functions.displayError('Cannot log in Gift Card accounts.');
                    return;
                }

                qcssapp.Functions.hideMask();
                qcssapp.Functions.displayError('Could not log in. Please make sure that you have entered your username and password correctly.');
                return;
            }

            //fail the login if there's no dskey
            if (!response.dskey) {
                qcssapp.Functions.hideMask();
                qcssapp.Functions.displayError('Error creating session.');
                return false;
            }

            //fail if the instanceUserTypeID is not set for cloud back end 2.0+
            if ( qcssapp.Functions.checkServerVersion(2,0) && !qcssapp.onGround && (response.data == null || response.data.length == 0 || ( !response.data[0].hasOwnProperty('instanceUserTypeID') && !response.data[1].hasOwnProperty('instanceUserTypeID') ) ) ) {
                qcssapp.Functions.hideMask();
                qcssapp.Functions.displayError('Error validating account login.');
                return false;
            }

            if (qcssapp.DebugMode) {
                console.log("Ground Login Success");
            }

            //create session from device session key
            qcssapp.Functions.createSession(response.dskey, qcssapp.Location, loginSuccessParameters.loginCode, response.forcePassword, loginSuccessParameters.keepLogged, loginSuccessParameters.checkFingerprintOnDevice);

            // Register FCM token
            qcssapp.Functions.registerToken();

            //set local storage flag indicating the user has logged in once
            qcssapp.Functions.setFirstLogin();

            //set the code used to persist, rather than being deleted, because it was successfully used
            var codeCollection = loginSuccessParameters.instanceCodeSelectView.collection;
            var codeModels = codeCollection.models;

            $.each(codeModels, function () {
                var mdl = this;
                if (mdl.get('CODE') == loginSuccessParameters.loginCode && mdl.get('USED') === 0) {
                    var attrs = {
                        USED: 1
                    };

                    mdl.save(attrs);
                }
            });

            var userTypeID;

            //check the user type id (1 - Employee, 3- Person)
            if ( qcssapp.Functions.checkServerVersion(2,0) && !qcssapp.onGround ) {
                if ( response.data[0].hasOwnProperty('instanceUserTypeID') ) {
                    userTypeID = response.data[0].instanceUserTypeID;
                } else {
                    userTypeID = response.data[1].instanceUserTypeID;
                }

                if ( userTypeID == 3 ) {
                    var personModel = new qcssapp.Models.Person();
                    personModel.set('personID', response.instanceUserID);
                    qcssapp.personModel = personModel;
                }
            }

            //if the user is an employee
            if ( !userTypeID || userTypeID == 1 ) {
                //if force password, display page
                if ( response.forcePassword ) {
                    loginSuccessParameters.loginView.buildAndNavigateToForcePasswordView(response);
                } else {
                    var continueFunction = function(response) {
                        if ( response.tos && response.tos.length ) {
                            var successParameters = {
                                tos: response.tos,
                                errorDisplay: false
                            };

                            //check insufficient amount of licenses
                            qcssapp.Functions.checkAccountLicenseAvailability(successParameters);

                        } else {
                            //build main view of the application after user's successful login
                            qcssapp.Functions.initializeApplication();
                        }
                    };
                    qcssapp.Functions.checkForceChange(continueFunction, response);
                }

                return;
            }

            //if the user is a Person, still need to check force password
            if ( response.forcePassword ) {
                loginSuccessParameters.loginView.buildAndNavigateToForcePasswordView(response);
            } else {
                if(qcssapp.Functions.fingerprintEnabled() && !qcssapp.verifyFingerprintFromLogin) {
                    qcssapp.Functions.verifyFingerprintOnDevice();
                    qcssapp.verifyFingerprintFromLogin = true;
                }

                //go to select account to manage view (no back button)
                qcssapp.Functions.navigateToManageAccount(true);
            }
        },

        //cloud login - For final validation step in cloud login flow, before going into the instance; sends gskey and user ID to the instance url
        validateUser: function(location, code, gskey, instanceUserID) {
            var thisURL = location + '/api/auth/validate';

            var instanceUserTypeID = instanceUserID > 0 ? 3 : 1;

            var dataParameters = JSON.stringify({"gskey": gskey, "instanceUserID": instanceUserID, "instanceUserTypeID": instanceUserTypeID});

            var successParameters = {location:location, code:code};

            //TODO: Adjust callAPI parameters
            qcssapp.Functions.callAPI( thisURL, 'POST', dataParameters, successParameters,
                function(data, successParameters) {
                    if ( qcssapp.DebugMode ) {
                        console.log('Successful response for validateUser');
                        console.log(data);
                    }

                    //TODO: extract this to a common function
                    //hide splash screen
                    if ( navigator && navigator.splashscreen ) {
                        navigator.splashscreen.hide();
                    }

                    if ( !data.isValid ) {
                        if ( qcssapp.DebugMode ) {
                            console.log('Invalid user');
                        }

                        qcssapp.Functions.hideMask();
                        qcssapp.Functions.displayError('Error on validation.');
                        return;
                    }

                    if ( !data.dskey ) {
                        if ( qcssapp.DebugMode ) {
                            console.log('Bad DS Key');
                        }

                        var errorMsg = data.errorDetails.length > 0 ? data.errorDetails : "Error - could not create session.";

                        qcssapp.Functions.throwFatalError(errorMsg, "login");

                        qcssapp.Functions.hideMask();

                        return false;
                    }

                    qcssapp.Functions.createSession(data.dskey, successParameters.location, successParameters.code, data.forcePassword, false, false);

                    // Register FCM token
                    qcssapp.Functions.registerToken();

                    var userTypeID;

                    //check the user type id (1 - Employee, 3- Person)
                    if ( qcssapp.Functions.checkServerVersion(2,0) && !qcssapp.onGround ) {
                        if ( data.data[0].hasOwnProperty('instanceUserTypeID') ) {
                            userTypeID = data.data[0].instanceUserTypeID;
                        } else {
                            userTypeID = data.data[1].instanceUserTypeID;
                        }

                        if ( userTypeID == 3 ) {
                            var personModel = new qcssapp.Models.Person();
                            personModel.set('personID', data.instanceUserID);
                            qcssapp.personModel = personModel;
                        }
                    }

                    //if the user is an employee
                    if ( !userTypeID || userTypeID == 1 ) {
                        //if force password, display page
                        if (data.forcePassword) {
                            new qcssapp.Views.ForceView(data);

                            qcssapp.Router.navigate('force', {trigger: true});

                            qcssapp.Functions.hideMask();
                        } else {
                            var continueFunction = function (data) {
                                if (data.tos && data.tos.length) {
                                    var successParameters = {
                                        tos: data.tos
                                    };

                                    //check insufficient amount of licenses
                                    qcssapp.Functions.checkAccountLicenseAvailability(successParameters);

                                    qcssapp.Functions.hideMask();
                                } else {
                                    //build main view of the application after user's successful login
                                    qcssapp.Functions.initializeApplication();
                                }
                            };
                            qcssapp.Functions.checkForceChange(continueFunction, data);
                        }

                        return;
                    }

                    //if the user is a Person, still need to check force password
                    if ( data.forcePassword ) {
                        new qcssapp.Views.ForceView(data);

                        qcssapp.Router.navigate('force', {trigger: true});

                        qcssapp.Functions.hideMask();
                    } else {
                        //go to select account to manage view (no back button)
                        qcssapp.Functions.navigateToManageAccount(true);
                    }
                },
                function(params) {
                    if ( qcssapp.DebugMode ) {
                        console.log('ERROR response for validateUser');
                    }

                    qcssapp.Functions.hideMask();
                    qcssapp.Functions.displayError('Error on validation.');
                }
            );
        },

        //checks local storage for the first login flag, returns boolean
        checkFirstLogin: function() {
            var mdl = new qcssapp.Models.FirstLoginModel();
            mdl.fetch();

            //TODO: don't reference mdl.attributes directly
            if ( mdl.attributes ) {
                return (mdl.get("logged") == "true");
            }

            return false;
        },

        //attempts to validate and save the input code
        saveCode: function(code, instanceCodeSelectView, navigateTo) {
            //reset error container message on re-try
            qcssapp.Functions.resetErrorMsgs();

            //validation for the input field before trying to save
            if ( code.length > 2 ) {
                var endPoint = 'https://www.mmhcloud.com/gateway/api/auth/code/'+code;

                var successParameters = {
                    instanceCodeSelectView:instanceCodeSelectView,
                    navigateTo: navigateTo || 'login-page',
                    loginView: this
                };

                qcssapp.Functions.callAPI(endPoint, 'GET', '', successParameters, function(data) {
                        var $loginSelect = $('#loginCodeSelect');

                        var instanceCodeSelectView = successParameters.instanceCodeSelectView;

                        if ( !data.LOCATION ) {
                            qcssapp.Functions.displayError('Code is not valid, please try again.');
                            qcssapp.Functions.hideMask();
                            return false;
                        }

                        data.LOCATION = qcssapp.Functions.htmlDecode(data.LOCATION.trim());

                        var ssoCode = (data.LOCATION.toString().indexOf('/sso') != -1);

                        //save sso code for mobile app
                        if ( window.cordova && ssoCode ) {
                            data.LOCATION = data.LOCATION.replace('/sso', '');
                            qcssapp.Location = data.LOCATION;
                            var ssoCodeModel = instanceCodeSelectView.collection.get("SSO");
                            if ( !ssoCodeModel ) {
                                var codeModel = new qcssapp.Models.instanceCode();
                                codeModel.set('CODE', 'SSO');
                                codeModel.set('LOCATION', qcssapp.Location);
                                codeModel.set('USED', 1);
                                instanceCodeSelectView.collection.create(codeModel);
                            } else {
                                ssoCodeModel.set('LOCATION', qcssapp.Location);
                                ssoCodeModel.set('USED', 1);
                                ssoCodeModel.save();
                            }

                            cordova.InAppBrowser.open(qcssapp.Location+'/sso', '_system');
                            return;
                        } else if ( ssoCode ) {
                            //redirect for desktop app
                            window.location = qcssapp.Location+'/sso';
                            return;
                        }

                        if (qcssapp.DebugMode) {
                            console.log("Success checking code validity, saving code");
                            console.log(data);
                        }

                        //reset code input box
                        $('#enterInstanceCode').add('#instanceCodeInput').val('');

                        successParameters.loginView.handleSaveCode(data, instanceCodeSelectView);

                        var loginOptions = $loginSelect.find('option');

                        //if more than one option then show the login code selector
                        if ( loginOptions.length > 1 ) {
                            $loginSelect.parent().parent().slideDown();
                            $('#addInstance').show();
                        }

                        //reset login page error message if one exists
                        qcssapp.Functions.resetErrorMsgs();

                        var saveCodeSuccessFunction = function(successParameters) {
                            var navTo = successParameters.navigateTo === 'tour-page-0' && !qcssapp.showTour ? 'login-page' : 'tour-page-0';
                            if(navTo == 'tour-page-0') {
                                $('#tour-logo-get-started-container').show().css({position: 'absolute', bottom: '55%', left:'15%', right:'15%'});
                                $('.tour-logo-bottom, #tour-header').hide();
                                $('.tour-footer').css('height', '170px');
                                qcssapp.Functions.showAboutPageElements();
                            }
                            qcssapp.Router.navigate(navTo, {trigger:true});
                            var justAdded = $loginSelect.find('option:last').get(0);
                            loginOptions.each(function(idx) {
                                if(this.id == justAdded.id) {
                                    $(this).attr("selected","selected");
                                } else {
                                    $(this).removeAttr("selected");
                                }
                            });
                            qcssapp.Functions.hideMask();
                        };

                        successParameters.location = data.LOCATION;

                        //check login settings of new code location
                        qcssapp.Functions.checkLoginSettings(data.LOCATION+'/api/auth/login/settings', successParameters, saveCodeSuccessFunction);
                    },
                    function() {
                        if ( qcssapp.DebugMode ) {
                            console.log('Attempt to get code details failed in saveCode');
                        }

                        qcssapp.Functions.hideMask();
                        qcssapp.Functions.displayError('Error communicating with the server.');
                    },
                    '',
                    '',
                    '',
                    false,
                    true);
            } else {
                qcssapp.Functions.hideMask();
                qcssapp.Functions.displayError('Please enter a valid code.');
            }
        },

        //creates or updates a local storage record about the given code
        handleSaveCode: function(data, instanceCodeSelectView) {
            var code = data.CODE.toLowerCase();

            //all attributes of the code
            var attrs = {
                CODE: code,
                ALIAS: data.ALIAS,
                LOCATION: data.LOCATION,
                NAME: data.NAME,
                INSTANCEID: data.INSTANCEID,
                INSTANCEUSERID: data.INSTANCEUSERID,
                ALLOWINVALIDSSLCERTS: data.ALLOWINVALIDSSLCERTS,
                USED: data.USED
            };

            //check if code already saved
            var codeSaved = this.isCodeSaved(code);

            if ( codeSaved && codeSaved.codeFound ) {
                //if code already saved then get and update the model with new attrs
                var mdl = instanceCodeSelectView.collection.get(code);
                mdl.save(attrs);
            } else {
                //otherwise make new instance code model from the data and create it on the collection
                var thisCode = new qcssapp.Models.instanceCode(attrs);
                instanceCodeSelectView.collection.create( thisCode );
            }
        },

        //takes a String code and determines if the code is previously saved for this user on this browser
        isCodeSaved: function(code) {
            if (qcssapp.DebugMode) {
                console.log("Checking if code: "+code+"  is previously saved...");
            }

            //TODO: could add more properties or the whole model as needed
            var codeObj = {};
            codeObj.codeFound = false;
            codeObj.location = "";

            //check through the instanceCode collection
            var codeCollection = new qcssapp.Collections.instanceCodes();

            codeCollection.fetch();

            var codeModel = codeCollection.findWhere({CODE: code});

            if ( codeModel && codeModel.get("LOCATION") != null ) {
                codeObj.codeFound = true;
                codeObj.location = codeModel.get("LOCATION");
            }

            if (qcssapp.DebugMode && !codeObj.codeFound) {
                console.log("Code is not previously saved");
            }

            //code not found in storage, return false
            return codeObj;
        },

        //checks whether the localstorage flag for triggering resuming the session is set
        checkSessionResumeApp: function() {
            var mdl = new qcssapp.Models.SessionModel();
            mdl.fetch();
            return mdl.get('resumeApp');
        },

        buildAndNavigateToForcePasswordView: function(loginValidationObj) {
            $('#force-password-page').off();
            new qcssapp.Views.ForceView(loginValidationObj);

            qcssapp.Functions.hideMask();

            qcssapp.Router.navigate('force', {trigger:true});
        },

        //hides 'Keep Me Logged In' if 'Enable Fingerprint' is checked
        handleFingerprint: function() {
            var $keepLoggedLabel = $('label[for=keepLogged]');
            var $keepLogged = $('#keepLogged');
            var $enableFingerprint = $('#enableFingerprint');

            if ( $enableFingerprint.is(':checked') ) {
                if($enableFingerprint.attr('no-enrolled-fingerprints')) {
                    $.confirm({
                        title: '',
                        content: '<div id="no-enrolled-fingerprint" >Fingerprint Authentication is available but there are no enrolled fingerprints on this device.</div>',
                        buttons: {
                            cancel: {
                                text: 'Close',
                                btnClass: 'btn-default fingerprint-close',
                                action: function() {
                                    $('#enableFingerprint').prop('checked',false);
                                }
                            }
                        }
                    });
                } else {
                    $keepLoggedLabel.hide();
                    $keepLogged.prop('checked', true);
                }
            } else {

                //if hit cancel in verify fingerprint OR logged out then unchecked Enable Fingerprint... kill session
                if(qcssapp.loggedOutFingerprint) {
                    qcssapp.personModel = null;
                    qcssapp.accountModel = null;
                    qcssapp.Controller.constructedViews = [];
                    qcssapp.Controller.history = [];
                    $enableFingerprint.prop('checked',false);
                    $('#loginName, #loginPassword').show();
                    qcssapp.Functions.logOut();
                }

                $keepLoggedLabel.show();
                $keepLogged.prop('checked', false);
            }
        },

        //simulate click of login button when enter is pressed on the username or password field
        enterEventHandler: function(e) {
            var $loginButton = $('#loginButton');
            if (e.keyCode == 13 && $loginButton.length ) {
                $loginButton.trigger('click');
            }
        },

        cleanupIcons: function() {
            var icons = $('.tour-healthy-icon-container').children('img');
            var single = $('[data-tourname="healthyIndicatorTour"]').children('.tour-featured-container').children('img');
            icons.remove();
            single.remove();
        },

        checkQRCodeMode: function() {
            if( !qcssapp.onPhoneApp ) {
                return true;
            }

            qcssapp.network.onlineStatus = true;

            qcssapp.offlineView = new qcssapp.Views.NetworkOfflineView({hidden: true});

            //if(!navigator.onLine) {
            if( navigator.connection && navigator.connection.type && navigator.connection.type === Connection.NONE ) {
                qcssapp.network.onlineStatus = false;

                var mdl = new qcssapp.Models.SessionModel();
                mdl.fetch();
                var userSession = mdl.get('sessionID');
                var badge = mdl.get('badge');

                //if session and badge are saved in localStorage
                if ( userSession != '' && badge != '' ) {
                    var params = {
                        badge: badge,
                        fromLogin: true
                    };

                    qcssapp.offlineView.offlineEvent = true;

                    qcssapp.offlineView.loadView(params);
                    return false;
                }

                if ( navigator && navigator.splashscreen ) {
                    navigator.splashscreen.hide();
                }
            }

            return true;
        }

    });
})(qcssapp, jQuery);
