;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.ButtonView - View for displaying Buttons
     */
    qcssapp.Views.ButtonView = Backbone.View.extend({
        internalName: "Button View", //NOT A STANDARD BACKBONE PROPERTY

        tagName: 'button',
        className: 'button qcss-button',

        //template: //template not needed for this view

        events: {
            'click': 'buttonClick'
        },

        initialize: function (options) {
            //assign the button options
            this.options = options;

            //id -> button ID
            this.id = this.options.id;

            //class -> button classes
            this.class = this.options.class;

            //text -> button text
            this.text = this.options.text;

            if (qcssapp.DebugMode) {
                console.log("Initialize in button view - text: " + this.text + " - id: " + this.id);
            }
            //appendToID -> where to append the button
            this.appendToID = this.options.appendToID;

            //buttonType -> Either a "link" or "save" or "login" or "icon"
            this.buttonType = this.options.buttonType;

            //if buttonType == "link" then pageLinkID must be supplied or the button will not function
            //pageLinkID -> panel ID for switching pages
            this.pageLinkID = this.options.pageLinkID;

            //goingBack --> optional, true if a link is going back in history, defaults to false
            this.goingBack = this.options.goingBack;

            //if buttonType == "save" then both saveTargetID and endPointURL must be supplied or the button will not function
            //saveTargetID -> ID of the element to save, a select box ID for instance
            this.saveTargetID = this.options.saveTargetID;

            //messageTargetID --> optional, where to put the response of the button for buttonType of "save"
            this.messageTargetID = this.options.messageTargetID;

            //successMsg --> optional, string message for the successful save
            this.successMsg = this.options.successMsg;

            //endPointURL -> API endpoint for PUT/POST
            this.endPointURL = this.options.endPointURL;

            //gives a reference to be able to access the parent view's functions
            this.parentView = this.options.parentView;

            //prevents multiple clicks of a button while cooldown is set to on
            this.clickedCoolDown = false;
            this.clickedCoolDownTimeout = null;

            if ( this.buttonType == "login" ) {
                //appInitialized -> prevent appView from firing again
                this.appInitialized = false;
            }

            //callback after the button is clicked
            this.callback = this.options.callback;

            _.bindAll(this, 'startButtonClickCoolDown', 'resetButtonClickCoolDown');

            //render the button on the target
            this.render();
        },

        render: function() {
            if(this.buttonType === "icon") {
                this.$el.attr('id', this.id).html(this.text).appendTo(this.appendToID).addClass(this.class);
            } else {
                this.$el.attr('id',this.id).text(this.text).appendTo(this.appendToID).addClass(this.class);
            }
            return this;
        },

        startButtonClickCoolDown: function() {
            clearTimeout(this.clickedCoolDownTimeout);

            var button = this;
            this.clickedCoolDownTimeout = setTimeout( function() {
                button.resetButtonClickCoolDown();
            }, 1000);
        },

        resetButtonClickCoolDown: function() {
            this.clickedCoolDown = false;
        },

        //general button click handling for all buttons
        buttonClick: function(e) {
            e.preventDefault();
            
            if (qcssapp.DebugMode) {
                console.log("Button clicked - text: " + this.text + " - id: " + this.id);
            }

            //check if the button has recently been clicked
            if ( this.clickedCoolDown ) {
                if ( qcssapp.DebugMode ) {
                    console.log("Button was on cooldown, no action taken.");
                }

                //reset the cooldown since the user is not being patient
                this.startButtonClickCoolDown();

                return;
            }

            //prevent double clicks
            this.clickedCoolDown = true;

            //for buttons that are links
            if ( this.pageLinkID && this.buttonType == "link" ) {
                var pageNavigationLink = this.pageLinkID.toString().substr(1, 999);
                qcssapp.Router.navigate(pageNavigationLink, {trigger:true});

                if ( this.callback ) {
                    this.callback();
                }
            }
            //for single target element saving (select box)
            else  if ( this.saveTargetID && this.endPointURL && this.buttonType == "save" ) {
                var that = this;
                var selectedID = $(this.saveTargetID).val();

                qcssapp.Functions.callAPI(this.endPointURL, 'POST', JSON.stringify({"id": selectedID}),{view:that}, function(data, successParameters) {
                    qcssapp.Functions.hideMask();

                    if ( successParameters.view.messageTargetID ) {
                        if ( successParameters.view.successMsg ) {
                            $(successParameters.view.messageTargetID).slideDown().html(successParameters.view.successMsg);
                        } else {
                            $(successParameters.view.messageTargetID).slideDown().html("Save successful!");
                        }
                    } else {
                        alert("Save successful!");
                    }

                    if ( successParameters.view.callback ) {
                        successParameters.view.callback();
                    }
                },'','','');
            }

            //run the provided callback function
            else if ( this.buttonType == "customCB" || this.buttonType == "icon" && this.callback ) {
                this.callback();
            }

            this.startButtonClickCoolDown();
            
            return false;
        }
    });
})(qcssapp, jQuery);
