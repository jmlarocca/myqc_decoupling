/**
 * Created with IntelliJ IDEA.
 * User: jmdottavio
 * Date: 7/16/15
 * Time: 1:15 PM
 * To change this template use File | Settings | File Templates.
 */

;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.chooseApplicationView - View for displaying instance code Collections
     */
    qcssapp.Views.chooseApplicationView = Backbone.View.extend({
        internalName: "Choose Application View", //NOT A STANDARD BACKBONE PROPERTY

        el: '#application-list', //references existing HTML element for instance code Collections - can be referenced as this.$el throughout the view

        //template: //template not needed for this view

        events: {
            "click .instance-code-list-item": "enterInstance"
        },


        initialize: function (options) {
            if (qcssapp.DebugMode) {
                console.log("initialize in instance code select view");
            }

            //create new collection
            this.collection = new qcssapp.Collections.instanceCodes();

            //initialize event listener of "add" to call "renderLine" on the collection
            this.listenTo( this.collection, 'add', this.renderLine );

            var codes = [];

            $.each(options.codes, function() {
                var code = new qcssapp.Models.instanceCode(this);
                codes[codes.length] = code;
            });

            this.collection.set(codes);

            return this;
        },

        //creates a new view for each instance code list item and assigns the model, then displays it on the page
        renderLine: function( mod ) {
            var instanceCodeListItemView = new qcssapp.Views.instanceCodeListItemView({
                model: mod
            });
            this.$el.append( instanceCodeListItemView.render().el );
        },

        //sends the relevant information to the gateway for the instance chosen
        enterInstance: function(event) {
            qcssapp.Functions.showMask();
            $('#chooseApplicationErrorContainer').html('');
            var $this = $(event.currentTarget);

            var instanceID = $this.attr('data-instanceID');
            var instanceUserID = $this.attr('data-instanceUserID');
            var instanceLocation = $this.attr('data-location');
            var code = $this.attr('id');

            qcssapp.Functions.enterInstance(instanceID, code, instanceUserID, instanceLocation);
        }
    });
})(qcssapp, jQuery);
