/**
 * Created by mmhayes on 9/21/2020.
 */
;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.DisclaimerLineView - View for displaying Disclaimer Collections (Collection Item Models)
     */
    qcssapp.Views.DisclaimerLineView = Backbone.View.extend({
        tagName:  'li',
        className: 'disclaimer-list-item',

        // Cache the template function for a single item.
        template: _.template( $('#disclaimer-template').html() ),

        render: function() {
            this.$el.html( this.template( this.model.attributes ) );

            return this;
        }
    });
})(qcssapp, jQuery);
