;(function (qcssapp, $) {
    'use strict';

    //global view settings for all the views in this application
    qcssapp.Functions.SetGlobalViewSettings();

    /* qcssapp.Views.keypadLineView - View for displaying Keypad Item Collections
     */
    qcssapp.Views.KeypadLineView = Backbone.View.extend({
        //is a div
        tagName:  'li',

        // Cache the template function for a single item.
        template: _.template( $('#keypad-line-template').html() ),

        imagePosition: {
            FULL: "keypad-line-template_full",
            LEFT: "keypad-line-template_left",
            RIGHT: "keypad-line-template_right",
            CENTER: "keypad-line-template_center",
            BOTTOM: "keypad-line-template_bottom",
            ORIGINAL: "keypad-line-template_original",
            LEFTBELOW: "keypad-line-template_left-below",
            RIGHTBELOW: "keypad-line-template_right-below",
            ORIGINALBELOW: "keypad-line-template_original-below"
        },

        initialize: function(options) {

            this.substituteProduct = options.substituteProduct;
            this.keypadId = options.keypadId;

            this.nameLineHeight = 20;
            this.descLineHeight = 15;
            this.nutritionLineHeight = 14;

            //for backwards compatibility
            if ( qcssapp.Functions.checkServerVersion(1,5) ) {

                //determine which template to use i.e. left, right, center, bottom, or full
                this.templateID = qcssapp.Functions.determineTemplate(this.model);
                this.template = _.template( $('#'+this.templateID).html() );
            }

            return this;
        },

        render: function() {

            // Price related settings
            var price = "";

            if ( this.model.get('productID') != null || (this.model.get('comboID') != null && this.model.get('comboID') != '')) {
                if ( this.model.get('price') != null && this.model.get('scaleUsed') ) {
                    price = qcssapp.Functions.formatPrice(this.model.get('price'), '') + " / lb";
                } else {
                    price = qcssapp.Functions.formatPrice(this.model.get('price'), '');
                }
            } else{
                price = '';
            }

            // Show upcharge on product line when in Combo Wizard, if no upcharge show nothing
            if(qcssapp.CurrentCombo.id != "") {
                price = qcssapp.Functions.formatComboUpcharge(this.model);
            }

            this.model.set('info', price);

            // Description related settings
            var description = ( this.model.get('description') == null || this.model.get('description').trim() == '' ) ? 'hidden' : '';
            this.model.set('showDescription', description);

            // For function buttons, set info to blank value
            if(this.model.get('productID') == null && this.model.get('menuID') == null && this.model.get('comboID') == null) {
                this.model.set('info', '');
            }

            //this.el is what we defined in tagName. use $el to get access to jQuery html() function
            this.$el.html( this.template( this.model.attributes ) );

            this.$el.attr('data-id', this.model.get('id'));

            if ( this.model.get('productID') != null ) {
                this.$el.attr('data-productID', this.model.get('productID'));
            } else if ( this.model.get('comboID') != null ) {
                this.$el.attr('data-comboID', this.model.get('comboID'));
            } else if ( this.model.get('menuID') != null ) {
                this.$el.attr('data-menuID', this.model.get('menuID'));
            } else if(this.model.get('productID') == null && this.model.get('menuID') == null && this.model.get('comboID') == null) {
                this.$el.attr('data-function', true);
            }

            // For function buttons, set info to blank value
            if(this.model.get('productID') == null && this.model.get('menuID') == null && this.model.get('comboID') == null) {
                this.$el.find('.keypad-line-info').hide();
            } else if( this.model.get('menuID') != null && this.model.get('productID') == null && this.model.get('comboID') == null ) {
                this.$el.find('.menu-arrow-svg').addClass('show-menu-arrow');
            }

            // trigger for click on keypad line
            this.$el.on('click', function() {
                this.loadNext();
            }.bind(this));

            this.setImage();

            this.setIcons();

            this.formatButton();

            return this;
        },

        setIcons: function(){

            if(!qcssapp.Functions.checkServerVersion(3,0)) {
                return;
            }

            var healthyFlag = false;

            if(this.model.get('healthy')){
                healthyFlag = true;
                var wellnessPath = "<img src=\""+qcssapp.healthyIndicator.wellness+"\">";
                this.$el.find('.line_icon-container').append(wellnessPath);
            }
            if(this.model.get('vegetarian')){
                healthyFlag = true;
                var vegetarianPath = "<img src=\""+qcssapp.healthyIndicator.vegetarian+"\">";
                this.$el.find('.line_icon-container').append(vegetarianPath);
            }
            if(this.model.get('vegan')){
                healthyFlag = true;
                var veganPath = "<img src=\""+qcssapp.healthyIndicator.vegan+"\">";
                this.$el.find('.line_icon-container').append(veganPath);
            }
            if(this.model.get('glutenFree')){
                healthyFlag = true;
                var glutenFreePath = "<img src=\""+qcssapp.healthyIndicator.glutenFree+"\">";
                this.$el.find('.line_icon-container').append(glutenFreePath);
            }

            if(healthyFlag) {
                this.$el.find('.line_icon-container').addClass('show-icons');
            }

            var grayscalePercent = qcssapp.Functions.getIconGrayscalePercentage();
            if(grayscalePercent != '') {
                this.$el.find('.line_icon-container img').css('filter', 'grayscale('+grayscalePercent+'%)');
            }

            var nutritionInfoView = new qcssapp.Views.NutritionInfoView({collection: this.model.get('nutritionInfo')});

            if(nutritionInfoView.collection.length == 0) {
                // Format for price below
                if(this.model.get('priceBelowName') && !this.$el.find('.line_nutrition-container').hasClass('show-nutrition') ) {
                    this.$el.find('.line_info-container').addClass('price-below-margin');
                }

                // Format for price below
                if(this.model.get('priceBelowName') && (this.templateID == this.imagePosition.LEFTBELOW || this.templateID == this.imagePosition.RIGHTBELOW || this.templateID == this.imagePosition.ORIGINALBELOW)) {
                    this.$el.find('.line_info-container').addClass('align-row');
                }

                return;
            } else if(nutritionInfoView.collection.length == 1 && healthyFlag) {
                this.$el.find('.line_info-container').addClass('align-row');
            }

            this.$el.find('.line_nutrition-container').addClass('show-nutrition').append($(nutritionInfoView.render().el).find('.line_nutrition-info'));

            // Format for price below
            if(this.model.get('priceBelowName') && (this.templateID == this.imagePosition.LEFTBELOW || this.templateID == this.imagePosition.RIGHTBELOW || this.templateID == this.imagePosition.ORIGINALBELOW)) {
                this.$el.find('.line_info-container').addClass('align-row');
            }
        },

        //sets the image on the keypad button if there is one
        setImage: function() {
            if ( this.model.get('image') != "" && qcssapp.Functions.checkServerVersion(1,5) && this.model.get('iconPosition') != "" && this.model.get('iconPosition') != "7") {

                this.$el.addClass('hasImage');

                var imagePath = qcssapp.qcBaseURL + '/webimages/'+ this.model.get('image');
                //var imagePath = 'https://qc90devmsdn.mmhayes.com/webimages/'+ this.model.attributes.image;
                qcssapp.onPhoneApp = (typeof window.cordova !== "undefined");
                if ( qcssapp.onPhoneApp ) {   // Cordova API detected
                    var hostName = qcssapp.Functions.get_hostname(qcssapp.Location); //can't have /myqc in the url
                    //hostName = "https://qc90devmsdn.mmhayes.com";
                    imagePath = hostName + '/webimages/'+ this.model.get('image');
                }

                //set image background size based on 'Shrink Image to Fit' setting
                var backgroundImageSize = 'cover';
                if(this.model.get('shrinkImageToFit')) {
                    backgroundImageSize = 'contain';
                    this.$el.find('.line_image').css('background-color', '#fff');
                }

                this.$el.find('.line_image').attr('data-bg', imagePath);
                this.$el.find('.line_image').addClass('lazyload');
                this.$el.find('.line_image').css('background-size', backgroundImageSize);

                this.$el.find('.line_image').on('lazyloaded', function(e) {
                    var $image = $(e.target);

                    if(qcssapp.DebugMode) {
                        var name = $image.parent().parent().find('.line_name-span').text();
                        console.log('error loading: ' + name);
                    }

                    // handle broken images
                    if($image.hasClass('broken-image')) {
                        $image.css('background-color', '#fff');
                    }

                    setTimeout(function() {
                        this.model.toggleImageLoaded();
                    }.bind(this), 100);

                }.bind(this));

                //hide the image for fade-in effect later
                if(this.templateID == this.imagePosition.FULL) {
                    this.$el.addClass('hideFull');
                } else {
                    this.$el.find('.line_image').addClass('hideImages');
                }

                //set the keypadID for fade-in so it only loads images with the same keypad id
                this.$el.find('.line_image').attr('id', this.model.attributes.keypadID);

                this.formatImageTemplate();

                this.formatImageShape();
            }
        },

        // Format the overlay for the name and price on the Full image position buttons
        formatImageTemplate: function() {

            //show or hide overlays for icon position 'full'
            if(this.templateID == this.imagePosition.FULL) {

                //if not showing the price overlay, set the overlay colors to transparent
                if(!this.model.attributes.showNameOverlay) {
                    this.$el.find('.line-full_details').css({'opacity':'0'});
                } else { //otherwise show the overlay colors
                    this.$el.find('.line-full_details').css({'opacity':'1'});
                }

                //if not showing the price overlay, set the overlay colors to transparent
                if(!this.model.attributes.showPriceOverlay) {
                    this.$el.find('.line-full_price').css({'opacity':'0'});
                } else {  //otherwise show the overlay colors
                    this.$el.find('.line-full_price').css({'opacity':'1'});
                }
                this.$el.find('.line-full_price').removeClass('accent-color-two');
            }
        },

        // Format the shape of the image on the menu button
        formatImageShape: function() {
            var imageShapeID = null;

            if(this.templateID == this.imagePosition.FULL) {
                return;
            }

            if(typeof this.model.get('imageShapeID') === 'undefined' || this.model.get('imageShapeID') == null || this.model.get('imageShapeID') == '' ) {
                imageShapeID = 1;
            } else {
                imageShapeID = this.model.get('imageShapeID');
            }

            if(imageShapeID == 2) { // Square
                this.$el.find('.line_image-container').addClass('line_image-square');

            } else if (imageShapeID == 3) { // Circle
                this.$el.find('.line_image-container').addClass('line_image-circle');
            }

            if(this.model.get('shrinkImageToFit') && imageShapeID == 3) {
                this.$el.find('.line_image-container').addClass('shrink-to-fit-circle');
            } else if(this.model.get('shrinkImageToFit')) {
                this.$el.find('.line_image-container').addClass('shrink-to-fit');
            }
        },

        formatButton: function() {
            if(!this.model.get('fontIsBold')) {
                this.$el.addClass('keypad-line_bold');
            }

            if(this.templateID == this.imagePosition.FULL) {
                qcssapp.Functions.setMenuIconSVG(this.$el.find('.menu-arrow-svg'), '#ffffff');
            }

            var arrowColor = '#133d8d';

            if(qcssapp.usingBranding && this.templateID != this.imagePosition.FULL) {
                arrowColor = $('.accent-color-two').css('color');
                qcssapp.Functions.setMenuIconSVG(this.$el.find('.menu-arrow-svg'), arrowColor);
            }

            if((this.templateID == this.imagePosition.ORIGINAL || this.templateID == this.imagePosition.ORIGINALBELOW) && this.$el.find('.line_description').hasClass('hidden') && !this.$el.find('.line_nutrition-container').hasClass('show-nutrition') && !this.$el.find('.line_icon-container').hasClass('show-icons') && qcssapp.Functions.internetExplorer()) {
                this.$el.addClass('format-original');
            }
        },

        // Replaces element's html to get the actual height of one line without it wrapping to another line
        determineLineHeights: function() {
            var $name = this.$el.find('.line_name-span');
            var $desc = this.$el.find('.line_description');
            var $nutrition = this.$el.find('.line_nutrition-container');

            var nameHTML = $name.html();
            $name.html('Name');

            var descHTML = $desc.html();
            $desc.html('Desc');

            var nutritionHTML = $nutrition.html();
            $nutrition.html('<div class="line_nutrition-info">1<span class="line_nutrition-info-label"> Cals</span></div>');

            if($name.height() != 0) {
                this.nameLineHeight = $name.height();
            }

            if($desc.height() != 0) {
                this.descLineHeight = $desc.height();
            }

            if($nutrition.height() != 0) {
                this.nutritionLineHeight = $nutrition.height();
            }

            // reset elements back to original html
            $name.html(nameHTML);
            $desc.html(descHTML);
            $nutrition.html(nutritionHTML);
        },

        adjustButton: function() {
            var maxLineCount = 5;

            // If Full Position, max line count is 4
            if(this.templateID == this.imagePosition.FULL) {
                maxLineCount = 4;

                // If Bottom Position, max line count is 6
            } else if(this.templateID == this.imagePosition.BOTTOM) {
                maxLineCount = 6;
            }

            // Determine the base line height is for 1 name element, desc element and nutrition element
            this.determineLineHeights();

            var hasDescription = !this.$el.find('.line_description').hasClass('hidden');
            var hasNutrition = this.$el.find('.line_nutrition-container').hasClass('show-nutrition');
            var hasIcons = this.$el.find('.line_icon-container').hasClass('show-icons');
            var nutritionIconSameLine = hasNutrition && hasIcons && this.$el.find('.line_nutrition-container').children().length == 1;

            // If Full Position, default to true because it will count as 1 line
            nutritionIconSameLine = this.templateID == this.imagePosition.FULL ? true : nutritionIconSameLine;

            // Get the line counts for each element
            var nameLineCount = this.getNameLineCount();
            var descLineCount = this.getDescLineCount();
            var nutritionLineCount = this.getNutritionLineCount();

            // Adjust the nutrition font size if the line wraps
            if(nutritionLineCount > 1) {
                this.adjustNutrition();
            }

            var totalLineCount = 0;

            // Figure out what the total line count is
            totalLineCount += nameLineCount;
            totalLineCount += descLineCount;

            totalLineCount = this.updateNutritionLineCount(totalLineCount);

            // If the line count is less than the max, just need append the quickpick icons
            if(totalLineCount <= maxLineCount) {
                this.setQuickPickIcons(nameLineCount > 1, maxLineCount);
                return;
            }

            var occupiedLines = 0;
            var linesAvailable = 0;

            // If just the description is overflowing
            if(nameLineCount == 1 && descLineCount > 1) {

                // If Image Position is Hidden, Left, Right or Center
                if(this.templateID != this.imagePosition.BOTTOM && this.templateID != this.imagePosition.FULL) {

                    // If there is a description, nutrition, healthy icons and nutrition and healthy icons are not on the same line, line clamp desc to 2 lines
                    if(hasDescription && hasNutrition && hasIcons && !nutritionIconSameLine) {
                        this.$el.find('.line_description').addClass('line_clamp-2');

                        // If there is a description and nutrition or healthy icons (or both but they're on the same line), line clamp desc to 3 lines
                    } else if(hasDescription && (hasNutrition || hasIcons)) {
                        this.$el.find('.line_description').addClass('line_clamp-3');
                    }

                    // If Image Position is Bottom
                } else if(this.templateID == this.imagePosition.BOTTOM) {

                    // If there is a description, nutrition, healthy icons and nutrition and healthy icons are not on the same line, line clamp desc to 3 lines
                    if(hasDescription && hasNutrition && hasIcons && !nutritionIconSameLine) {
                        this.$el.find('.line_description').addClass('line_clamp-3');

                        // If there is a description and nutrition or healthy icons (or both but they're on the same line), line clamp desc to 4 lines
                    } else if(hasDescription && (hasNutrition || hasIcons)) {
                        this.$el.find('.line_description').addClass('line_clamp-4');
                    }

                    // If Image Position is Full
                } else if(this.templateID == this.imagePosition.FULL) {

                    // If there is a desciprtion and nutrition, line clam desc to 2 lines
                    if(hasDescription && hasNutrition) {
                        this.$el.find('.line_description').addClass('line_clamp-2');
                    }
                }

                // If in IE need to append '...' element because "text-overflow:ellipsis" style does not work
                if(qcssapp.Functions.internetExplorer()) {
                    this.$el.find('.line_overflow').show();
                }

                // Append the icons to the line_name element
                this.setQuickPickIcons(false, maxLineCount);

                // If just the name is overflowing
            } else if(nameLineCount > 1 && (!hasDescription || descLineCount == 1)) {

                // If there is only one line of description, add 1 to occupied lines
                if(hasDescription && descLineCount == 1) {
                    occupiedLines += 1;
                }

                // Determine how many lines nutrition and healthy indicators take up
                occupiedLines = this.updateNutritionLineCount(occupiedLines);

                // Get how many lines are left for the name length
                linesAvailable = maxLineCount - occupiedLines;

                // Update the name element with the line-clamp
                this.$el.find('.line_name-span').addClass('line_clamp-' + linesAvailable);

                // Append the icons to the line_name element
                this.setQuickPickIcons(false, maxLineCount);

                // If the name and description are overflowing
            } else if (nameLineCount > 1 && descLineCount > 1) {

                // Determine how many lines nutrition and healthy indicators take up
                occupiedLines = this.updateNutritionLineCount(occupiedLines);

                // Get how many lines are left
                linesAvailable = maxLineCount - occupiedLines;

                // If the name is greater than or equal to number of lines available, name will use up all space except for one line for desc
                if(nameLineCount >= linesAvailable) {
                    var nameLineAvailable = linesAvailable - 1;
                    this.$el.find('.line_name-span').addClass('line_clamp-' + nameLineAvailable);
                    this.$el.find('.line_description').addClass('line_clamp-1');

                    // Append the icons to the line_name element
                    this.setQuickPickIcons(false, maxLineCount);

                    // If there is space for the entire name show it, clamp the description down to what is left
                } else {
                    var descLineAvailable = linesAvailable - nameLineCount;
                    this.$el.find('.line_description').addClass('line_clamp-' + descLineAvailable);

                    // Append the icons to the line_name-span element
                    this.setQuickPickIcons(true, maxLineCount);

                    // If in IE need to append '...' element because "text-overflow:ellipsis" style does not work
                    if(qcssapp.Functions.internetExplorer()) {
                        this.$el.find('.line_overflow').show();
                    }
                }
            }
        },

        // Adjust the font-size of the nutrition amount and label
        adjustNutrition: function() {
            var $nutritionContainer = this.$el.find('.line_nutrition-container');

            var startingFontSize = 12;
            var startingFontLabelSize = 11;

            // Subtract 1 pixel from font size until it fits, the font-size is 12 so only do 10 loops (shouldn't reach this far)
            for(var x = 0; x<10; x++) {
                var fontSize = startingFontSize;
                var fontLabelSize = startingFontLabelSize;

                // Check if the nutrition fits on 1 line, if so break
                var nutritionHeight = $nutritionContainer.height();
                var nutritionLineCount = nutritionHeight > this.nutritionLineHeight ? Number(nutritionHeight / this.nutritionLineHeight) : 1;
                if(nutritionLineCount == 1) {
                    break;
                }

                // Update each nutrition amount and label with the decreased font-size
                $.each($nutritionContainer.find('.line_nutrition-info'), function() {
                    var $nutritionInfo = $(this);
                    var $nutritionInfoLabel = $(this).find('.line_nutrition-info-label');

                    fontSize--;
                    $nutritionInfo.css({'font-size': startingFontSize + 'px'});

                    fontLabelSize--;
                    $nutritionInfoLabel.css({'font-size': startingFontLabelSize + 'px'});
                });

                startingFontSize--;
                startingFontLabelSize--;
            }

        },

        // Add the quickpick icons to the name element
        setQuickPickIcons: function(appendToSpan, maxLineCount) {
            if( !qcssapp.Functions.checkServerVersion(1,6) ) {
                return;
            }

            // Get the original name line count before appending element
            var nameLineCountOriginal = this.getNameLineCount();

            var $element = appendToSpan ? this.$el.find('.line_name-span') : this.$el.find('.line_name');

            if( this.model.get('favorite') ) {
                $element.append('<img class="quickpick-icon heart-line-icon" src="images/icon-favorites.svg"/>');
            }

            if( this.model.get('popular') ) {
                $element.append('<img class="quickpick-icon" src="images/icon-popular.svg"/>');
            }

            // Get name line count after appending element
            var nameLineCount = this.getNameLineCount();

            // If icons were appended to parent element, line_name check name line count
            if(!appendToSpan) {

                // If the original name line count was 1 but not it is more than 1
                if(nameLineCountOriginal == 1 && nameLineCount > 1) {

                    // Remove the quickpick icons and append them to the child element, line_name-span
                    $element.find('.quickpick-icon').remove();
                    $element = this.$el.find('.line_name-span');

                    if( this.model.get('favorite') ) {
                        $element.append('<img class="quickpick-icon heart-line-icon" src="images/icon-favorites.svg"/>');
                    }

                    if( this.model.get('popular') ) {
                        $element.append('<img class="quickpick-icon" src="images/icon-popular.svg"/>');
                    }
                }
            }

            // Again check if adding the quickpick icon adds a line to the name element
            if(nameLineCountOriginal < nameLineCount) {

                var totalLineCount = this.getTotalLineCount();

                // Check if the total line count is still less than the max line count (if it is then we're okay, if not we need to clamp the name)
                if(totalLineCount > maxLineCount) {

                    // Adding quickpick icon to name span increases line count, add line-clamp
                    this.$el.find('.line_name-span').addClass('line_clamp-' + nameLineCountOriginal);

                    // Add icon to parent div element if the name span is going to be clamped
                    $element.find('.quickpick-icon').remove();
                    $element = this.$el.find('.line_name');

                    if( this.model.get('favorite') ) {
                        $element.append('<img class="quickpick-icon heart-line-icon" src="images/icon-favorites.svg"/>');
                    }

                    if( this.model.get('popular') ) {
                        $element.append('<img class="quickpick-icon" src="images/icon-popular.svg"/>');
                    }
                }
            }
        },

        // Get the name line count
        getNameLineCount: function() {
            var nameHeight = this.$el.find('.line_name-span').height();
            return nameHeight > this.nameLineHeight ? Number(nameHeight / this.nameLineHeight) : 1;
        },

        // Get the description line count
        getDescLineCount: function() {
            var descHeight = this.$el.find('.line_description').height();
            return descHeight > this.descLineHeight ? Number(descHeight / this.descLineHeight) : 1;
        },

        // Get the nutrition line count
        getNutritionLineCount: function() {
            var nutritionHeight = this.$el.find('.line_nutrition-container').height();
            return nutritionHeight > this.nutritionLineHeight ? Number(nutritionHeight / this.nutritionLineHeight) : 1;
        },

        // Get the total line count with name, description, nutrition and healthy indicators
        getTotalLineCount: function() {
            var nameLineCount = this.getNameLineCount();
            var descLineCount = this.getDescLineCount();

            var totalLineCount = 0;
            totalLineCount += nameLineCount;
            totalLineCount += descLineCount;

            return this.updateNutritionLineCount(totalLineCount);
        },

        // Determine how many lines the nutrition and healthy indicators take up
        updateNutritionLineCount: function(occupiedLines) {
            var hasNutrition = this.$el.find('.line_nutrition-container').hasClass('show-nutrition');
            var hasIcons = this.$el.find('.line_icon-container').hasClass('show-icons');
            var nutritionIconSameLine = hasNutrition && hasIcons && this.$el.find('.line_nutrition-container').children().length == 1;

            if(nutritionIconSameLine) {
                occupiedLines += 1;

            } else {
                if(hasNutrition) {
                    occupiedLines++;
                }

                if(hasIcons) {
                    occupiedLines++;
                }
            }

            return occupiedLines;
        },

        loadNext: function() {
            var keypadID = this.model.get('id');
            var productID = this.model.get('productID');
            var comboID = this.model.get('comboID');
            var menuID = this.model.get('menuID');
            var prepOptionSetID = this.model.get('prepOptionSetID');
            var productName = this.model.get('name');
            var modSetDetailID = this.model.get('modSetDetailID');
            var autoShowModSet = this.model.get('autoShowModSet');
            var favorite = this.model.get('favorite');

            qcssapp.Functions.resetErrorMsgs();

            qcssapp.Functions.clearCurrentModifiers();

            qcssapp.ProductInProgress.prepOptionSetID = prepOptionSetID;
            qcssapp.ProductInProgress.favorite = true;
            qcssapp.ProductInProgress.nutritionInfo = this.model.get('nutritionInfo');

            //if substituting a product on a combo
            if(this.substituteProduct != "") {
                qcssapp.ProductInProgress.substituteProduct = this.substituteProduct;

                if(!qcssapp.Functions.isNumeric(modSetDetailID) && !qcssapp.Functions.isNumeric(prepOptionSetID)) {
                    qcssapp.Functions.substituteProduct(productID);
                    return;
                }

                qcssapp.Functions.goToProductDetail(productID, this.model);

            } else if ( qcssapp.Functions.isNumeric(menuID) ) {
                qcssapp.Functions.loadKeypad(menuID);

            } else if ( qcssapp.Functions.isNumeric(productID) ) {

                if(qcssapp.CurrentCombo.id != "" && !qcssapp.Functions.isNumeric(modSetDetailID) && !qcssapp.Functions.isNumeric(prepOptionSetID)) {
                    qcssapp.Functions.loadProductCombo(productID, this.model); //send keypad model info in in case we need an image from it for the combo
                    return;
                }

                qcssapp.Functions.goToProductDetail(productID, this.model);

            } else if ( qcssapp.Functions.isNumeric(comboID) ) {
                qcssapp.CurrentCombo.name = productName;
                qcssapp.Functions.loadCombo(comboID, 0);
            }
        }
    });

})(qcssapp, jQuery);

