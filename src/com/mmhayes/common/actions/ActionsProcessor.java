package com.mmhayes.common.actions;

import com.mmhayes.common.dataaccess.DataHandlerProperties;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/*
 $Author: crking $: Author of last commit
 $Date: 2019-11-06 15:27:05 -0500 (Wed, 06 Nov 2019) $: Date of last commit
 $Rev: 9876 $: Revision of last commit
 Notes:
*/

public class ActionsProcessor {
    DataManager dm = new DataManager();
    commonMMHFunctions comm = new commonMMHFunctions();
    public static final String CREATEUSERID = "CREATEUSERID";

    //TODO: methods should go in here that handle the storing of a step object for a particular action
    public HashMap findStep(ArrayList<HashMap> steps, int stepNumber) {
        for (HashMap step : steps) {
            if (step.containsKey("STEPINDEX")) {
                int stepIndex = Integer.parseInt(step.get("STEPINDEX").toString());
                if (stepNumber-1 == stepIndex) {
                    return step;
                }
            }
        }
        return new HashMap();
    }

    public HashMap findStepQuestion(HashMap step, String questionID) {
        if (step.containsKey("QUESTIONS")) {
           ArrayList<HashMap> stepQuestions = commonMMHFunctions.toArrayList(step.get("QUESTIONS"));
           for (HashMap stepQuestion : stepQuestions) {
               if (stepQuestion.get("STEPQUESTIONID").toString().equals(questionID)) {
                   return stepQuestion;
               }
           }
        }
        return null;
    }

    // Get the integer ID of a step from the step HashMap
    public static Integer getStepId(HashMap step) {
        try {
            return Integer.parseInt(step.get("STEPID").toString());
        } catch (Exception e) {
            return null;
        }
    }

    // Get the answer to a Step Index (the number that appears next to the step on the front-end)
    public static String getAnswerByStepIndex(ArrayList<HashMap> steps, int stepIndex) {
        if (steps.size() >= stepIndex) {
            stepIndex--;
            //String whatevs = steps.get(stepIndex).getClass().getName();
            if (steps.get(stepIndex) != null) { // && steps.get(stepIndex).getClass().getName().compareToIgnoreCase("java.util.HashMap") == 0) {
                HashMap hm = steps.get(stepIndex);
                if (hm.containsKey("STEPANSWER")) {
                    return hm.get("STEPANSWER").toString();
                }
            }
        }
        return null;
    }

    // Get the answer to a step by the ID of that step
    public static String getAnswerByStep(ArrayList<HashMap> steps, int stepId) {
        for (HashMap hm : steps) {
            String stepAnswer = getAnswerByStep(hm, stepId);
            if (stepAnswer != null) {
                return stepAnswer;
            }
        }
        return null;
    }
    public static String getAnswerByStep(HashMap step, int stepId) {
        if (step.containsKey("STEPID") && step.get("STEPID") != null && commonMMHFunctions.tryParseInt(step.get("STEPID").toString())
                && Integer.parseInt(step.get("STEPID").toString()) == stepId && step.containsKey("STEPANSWER") && step.get("STEPANSWER") != null) {
            return step.get("STEPANSWER").toString();
        }
        return null;
    }

    /**
     * NOTE: the parameters to this may contain SQLI
     * @param userID
     * @param interfaceID
     * @param uploadedFileID
     * @param interfaceTypeID
     * @param processHistoryID
     * @return
     */
    public ArrayList<HashMap> insertInterfaceTask(String userID, String interfaceID, String uploadedFileID, String interfaceTypeID, String processHistoryID) {
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        boolean insertInterfaceTask=true;

        try {
            //this following block needs to be made more dynamic,too procedural atm - rkb
            if(insertInterfaceTask){
                //need to check for blank uploadedFileID because inserting a blank puts a 0 in the db
                if ( uploadedFileID.equals("") ) {
                    if (interfaceTypeID.equals("103") || interfaceTypeID.equals("115")) {
                        if (dm.serializeUpdate("data.actions.insertInterfaceTaskNoUploaded",
                                new Object[] {interfaceTypeID, userID, interfaceID, processHistoryID}) == -1) {
                            errorCodeList.add(comm.commonErrorMessage("Unable to insert Interface Task."));
                        }
                    } else if (interfaceTypeID.equals("108") || interfaceTypeID.equals("109")) {
                        if (dm.serializeUpdate("data.actions.insertPayrollTaskNoUploaded",
                                new Object[] {interfaceTypeID, userID, interfaceID, processHistoryID}) == -1) {
                            errorCodeList.add(comm.commonErrorMessage("Unable to insert Payroll Task."));
                        }
                    }
                } else {
                    // 103 = Account Import
                    if (interfaceTypeID.equals("103")) {
                        // CHECK interface if EmployeeInterfaceTypeID = 1 ie CSV
                        boolean isCSVAccountImportInterface = false; // INIT isCSVAccountImportInterface flag
                        String csvAccountImportFilePath = "";        // Path to be wriiten to TASK record - DEFECT 856
                        ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.interfaces.getEmpImpInterfaceData", new Object[]{interfaceID},true);
                        if(!result.isEmpty()){
                            if(result.get(0).containsKey("SOURCEFILEPATH")){
                                csvAccountImportFilePath = result.get(0).get("SOURCEFILEPATH").toString(); // Capture SourceFilePath for TASK table
                            }
                            if(result.get(0).containsKey("EMPLOYEEINTERFACETYPEID")){
                                String empTypeID = result.get(0).get("EMPLOYEEINTERFACETYPEID").toString();
                                isCSVAccountImportInterface = empTypeID.equals("1");
                            }
                        }
                        if(isCSVAccountImportInterface){
                            if (dm.serializeUpdate("data.actions.insertInterfaceTaskCSV",
                                    new Object[] {interfaceTypeID, userID, interfaceID, processHistoryID, uploadedFileID, csvAccountImportFilePath}) == -1) {
                                errorCodeList.add(comm.commonErrorMessage("Unable to insert Interface Task"));
                            }
                        } else {
                            if (dm.serializeUpdate("data.actions.insertInterfaceTask",
                                    new Object[] {interfaceTypeID, userID, interfaceID, processHistoryID, uploadedFileID}) == -1) {
                                errorCodeList.add(comm.commonErrorMessage("Unable to insert Interface Task"));
                            }
                        }
                    } else if (interfaceTypeID.equals("108") || interfaceTypeID.equals("109")) {
                        if (dm.serializeUpdate("data.actions.insertPayrollTask",
                                new Object[] {interfaceTypeID, userID, interfaceID, processHistoryID, uploadedFileID}) == -1) {
                            errorCodeList.add(comm.commonErrorMessage("Unable to insert Payroll Task."));
                        }
                    } else if (interfaceTypeID.equals("115")) {
                        if (dm.serializeUpdate("data.actions.insertInterfaceTaskNoUploaded",
                                new Object[] {interfaceTypeID, userID, interfaceID, processHistoryID}) == -1) {
                            errorCodeList.add(comm.commonErrorMessage("Unable to insert Interface Task."));
                        }
                    }
                }
            }
        } catch (Exception e) {
            errorCodeList.add(comm.commonErrorMessage("Unable to upload file."));
            Logger.logMessage("Exception occurred in method insertInterfaceTask().", Logger.LEVEL.ERROR);
            commonMMHFunctions.logStackTrace(e);
        }
        return errorCodeList;
    }

    public ArrayList<HashMap> createTerminalTask(String taskTypeID, String userID, String terminalIDs) {
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        int i = dm.serializeUpdate("data.actions.comm.insertTerminalTask", new Object[] {taskTypeID, terminalIDs, userID});
        if (i == -1) {
            errorCodeList.addAll(commonMMHFunctions.commonErrorMessageList("Could not create tasks for Terminal IDs : " + terminalIDs));
        }
        return errorCodeList;
    }

    //TODO: action history should also be logged in for a particular action

    // Method to create a new event record using the parameters specified in the properties object
    public ArrayList<HashMap> createEvent(DataHandlerProperties prop, ArrayList<HashMap> records, HttpServletRequest request) {
        // Log progress
        Logger.logMessage("Begin processing create event for table " + prop.getTableName() + ", session ID: " + request.getSession().getId(), Logger.LEVEL.DEBUG);
        // Add in the company ID for the record
        prop.setCacheCompanyId(commonMMHFunctions.getCompanyId(request));
        // Run the data validation, fields will be shared between all actions
        prop.setDataValidation();
        // Declare variables
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        // Iterate through the records
        for (HashMap r : records) {
            // Set up variable for the saved record
            HashMap saveRecord = new HashMap();
            // Remove automatically determined fields if they somehow made it into the request
            r.remove(CREATEUSERID);
            r.remove(commonMMHFunctions.COMPANYID);
            // Add in the logging columns that the user would never have the opportunity to set
            if (prop.getValidatedColumns().containsKey(CREATEUSERID)) r.put(CREATEUSERID, commonMMHFunctions.getUserId(request));
            // Add in the company ID if applicable
            if (prop.getValidatedColumns().containsKey(commonMMHFunctions.COMPANYID)) r.put(commonMMHFunctions.COMPANYID, prop.getCacheCompanyId());
            // Find and cache the list items
            r = storeEventParameters(r, prop);
            // Reserve certain fields for specific logging
            String recordId = (r.containsKey(prop.getPrimaryIdentifier()) ? r.get(prop.getPrimaryIdentifier()).toString() : "");
            if (prop.getCheckPermissions() && prop.getPermissionCache().containsKey(commonMMHFunctions.ADDPERMISSION) && !(Boolean)prop.getPermissionCache().get(commonMMHFunctions.ADDPERMISSION)) {
                // The user does not have add permissions so this record will be skipped
                errorCodeList.add(comm.commonErrorMessage("User " + commonMMHFunctions.checkSession(request) + " attempted to run an event without permissions."));
                Logger.logMessage("ERROR: User " + commonMMHFunctions.checkSession(request) + " attempted to run an event without permissions.", Logger.LEVEL.ERROR);
                Logger.logMessage(r.toString(), Logger.LEVEL.ERROR);
                continue; // No further processing on this record in the loop
            }
            Logger.logMessage("Processing event record for insert...", Logger.LEVEL.DEBUG);
            // Insert the record
            ArrayList<HashMap> insertErrorCodeList = new ArrayList<>();
            // Remove the primary key value if it's the only key (assuming a single key will be determined by the database for inserts)
            if (prop.getPrimaryKeys().size() == 1) {
                r.remove(prop.getPrimaryIdentifier());
            }
            // Validate the record only once all of the data has been manipulated
            HashMap returnObj = comm.validateRecord(prop.getDataValidation(), prop.getValidatedColumns(), r);
            // Include the validation information in the record to be saved
            if (returnObj.containsKey("validatedRecord")) {
                saveRecord = (HashMap)returnObj.get("validatedRecord");
            }
            // Include any validation errors
            if (returnObj.containsKey("errorCodeList")) {
                insertErrorCodeList.addAll((ArrayList<HashMap>)returnObj.get("errorCodeList"));
            }
            // Only if all the validation passed, save the record
            if (insertErrorCodeList.isEmpty()) {
                insertErrorCodeList.addAll(comm.addRecord(prop, saveRecord));
            } else if (!insertErrorCodeList.isEmpty() && prop.getTestingApplication()) {
                Logger.logMessage("Unable to insert previous entry, errors in data validation", Logger.LEVEL.ERROR);
                Logger.logMessage("Failed fields : " + insertErrorCodeList.toString(), Logger.LEVEL.ERROR);
                Logger.logMessage("On record : " + saveRecord, Logger.LEVEL.ERROR);
            }
            // Get the ID of the inserted record from the return of the insert record method, and remove it from the error code list
            String lastRecordID = "";
            for (int i = 0; i < insertErrorCodeList.size(); i++) {
                HashMap h = insertErrorCodeList.get(i);
                if (h.get("LASTRECORDID") != null) {
                    lastRecordID = h.get("LASTRECORDID").toString();
                    h.remove("LASTRECORDID");
                    if (h.size() == 0) {
                        insertErrorCodeList.remove(i);
                    }
                }
            }
            // Run through any child records after the main record, only if the insert was successful
            if (lastRecordID.length() > 0) {
                insertErrorCodeList.addAll(saveEventParameters(request, prop, lastRecordID));
            }
            errorCodeList.addAll(insertErrorCodeList);
        }
        return errorCodeList;
    }

    // Find event list items and cache them (not for comma-separated lists crammed into a single field)
    protected final HashMap storeEventParameters(HashMap record, DataHandlerProperties prop) {
        // Remove any existing mapping (old cache)
        prop.clearMappingArrayLists();
        // Loop through the items in the HashMap to see if any values are an ArrayList
        Iterator iter = record.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry e = (Map.Entry)iter.next();
            if (e.getValue() != null && e.getValue().getClass().equals(ArrayList.class)) {
                // When an ArrayList is present, add it to the mapping array and remove it from the values to be inserted
                prop.setMappingArrayLists(e.getKey().toString(), e.getValue());
                iter.remove();
            }
        }
        return record;
    }

    // Save event list items (not for comma-separated lists crammed into a single field)
    protected final ArrayList<HashMap> saveEventParameters(HttpServletRequest request, DataHandlerProperties prop, Object parentValue) {
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        // Check for any children (removed from the main record)
        if (prop.getMappingArrayLists().size() > 0) {
            Iterator iter = prop.getMappingArrayLists().entrySet().iterator();
            // Loop through the cached records
            while (iter.hasNext()) {
                Map.Entry child = (Map.Entry)iter.next();
                // Find the matching child properties object
                DataHandlerProperties childProp = new DataHandlerProperties();
                childProp.setIsMappingObject(true);
                for (DataHandlerProperties cp : prop.getChildren()) {
                    if (child.getKey().toString().compareToIgnoreCase(cp.getNameField()) == 0 && cp.getParentIdentifier().compareToIgnoreCase(prop.getPrimaryIdentifier()) == 0) {
                        childProp = cp;
                    }
                }
                if (childProp.getParentIdentifier().length() > 0) {
                    // Only add in parent record ID if there was a generated or existing value (i.e. greater than 0)
                    if (commonMMHFunctions.tryParseLong(parentValue.toString()) && Long.parseLong(parentValue.toString()) > 0) {
                        // Add record ID from parent
                        for (HashMap c : (ArrayList<HashMap>)child.getValue()) {
                            c.put(childProp.getParentIdentifier(), parentValue.toString());
                        }
                    }
                    // Process child records
                    Logger.logMessage("Begin processing event list items for table " + prop.getTableName() + ", session ID: " + request.getSession().getId(), Logger.LEVEL.DEBUG);
                    errorCodeList.addAll(createEvent(childProp, (ArrayList<HashMap>)child.getValue(), request));
                }
            }
        }
        // Return an empty ArrayList if there aren't any records to process
        return errorCodeList;
    }
}
