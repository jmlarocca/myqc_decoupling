package com.mmhayes.common.actions;

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.dataaccess.commonDataHandler;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
 $Author: nyu $: Author of last commit
 $Date: 2021-08-17 17:43:37 -0400 (Tue, 17 Aug 2021) $: Date of last commit
 $Rev: 14966 $: Revision of last commit
 Notes:
*/
public class ActionsHandler extends commonDataHandler {

    DataManager dm = new DataManager();
    commonMMHFunctions commFunc = new commonMMHFunctions();
    String className = this.getClass().getSimpleName();
    String LastAnswer = "";
    ActionsProcessor ap = new ActionsProcessor();
    //gets header level action information about the actionID passed
    public ArrayList<HashMap> getActionInfo(int actionID, HttpServletRequest request) {
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        try
        {
            Logger.logMessage("Entering method: "+className+"."+"getActionInfo", Logger.LEVEL.DEBUG);
            if (!commonMMHFunctions.checkSession(request).equals("")) { //check that user is logged in
                return dm.serializeSqlWithColNames("data.actions.getActionInfo",  new Object[]{new Integer(actionID)});
            } else {
                errorCodeList.add(commFunc.commonErrorMessage("Invalid Access."));
                return errorCodeList;
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            errorCodeList.add(commFunc.commonErrorMessage("Could not retrieve header info for actionID : " + actionID));
            return errorCodeList;
        }
    }

    //returns a step object - which consists of all the information for various steps/questions in the sequence (you can start at any valid step in the sequence)
    public ArrayList<HashMap<String, Object>> getSteps(int sequenceID, int startStepID, int stepIndexCount, ArrayList<HashMap> answerObject,HttpServletRequest request) {
        ArrayList<HashMap<String, Object>> stepsObject = new ArrayList<>();
        ArrayList<HashMap<String, Object>> errorCodeList = new ArrayList<>();
        Integer stepObjectIndex = 0;
        try
        {
            Logger.logMessage("Entering method: "+className+"."+"getSteps", Logger.LEVEL.DEBUG);
            if (!commonMMHFunctions.checkSession(request).equals("")) { //check that user is logged in
                ArrayList<Integer> sequenceSteps = getSequenceSteps(sequenceID, startStepID, request);
                //ArrayList<HashMap> questionsInfo = new ArrayList<>();
                ArrayList<HashMap> stepHeaderInfo;

                for (Integer stepID : sequenceSteps) {
                    //get the header information for this step
                    stepHeaderInfo = dm.serializeSqlWithColNames("data.actions.getStepInfo",  new Object[]{new Integer(stepID)});

                    //add the question information to the step header array list
                    stepHeaderInfo.get(0).put("QUESTIONS", getStepQuestions(stepID, answerObject,request));

                    //add the stepHeader information to the stepsObject
                    stepsObject.add(stepHeaderInfo.get(0));

                    //add sequenceID
                    stepsObject.get(stepObjectIndex).put("SEQUENCEID", sequenceID);

                    //add stepIndex
                    stepIndexCount++;
                    stepsObject.get(stepObjectIndex).put("STEPINDEX", stepIndexCount);
                    stepObjectIndex++;
                }
            } else {
                errorCodeList.add(commFunc.commonErrorMessage("Invalid Access."));
                return errorCodeList;
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            errorCodeList.add(commFunc.commonErrorMessage("Exception caught while trying to retrieve steps."));
            return errorCodeList;
        }
        return stepsObject;
    }

    //gets all step level information for a particular step in sequence
    private ArrayList<Integer> getSequenceSteps(int sequenceID, int startStepID, HttpServletRequest request) {
        ArrayList<Integer> sequenceSteps = new ArrayList<>();
        try
        {
            Logger.logMessage("Entering method: "+className+"."+"getSequenceSteps", Logger.LEVEL.DEBUG);
            int currentStep = startStepID;
            boolean searchForNextStep = true;

            sequenceSteps.add(currentStep); //add first step in sequence to array list

            while (searchForNextStep) { //retrieve all other steps for this sequence
                Object currentStepObj = dm.getSingleField("data.actions.getSequenceStep", new Object[]{sequenceID, currentStep});
                if (currentStepObj != null) {
                    currentStep = Integer.parseInt(currentStepObj.toString());
                    sequenceSteps.add(currentStep);
                } else {
                    searchForNextStep = false;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
        }
        return sequenceSteps;
    }

    //gets question level information for a particular step (for initially loaded steps)
    private ArrayList<HashMap> getStepQuestions(int stepID, ArrayList<HashMap> answersObject, HttpServletRequest request) {
        ArrayList<HashMap> allStepQuestions;
        ArrayList<HashMap> stepQuestions = new ArrayList<>();
        Boolean isDecision = false;
        Integer QuestionIndex = 0;
        Integer stepQuestionID = 0;
        String defaultAnswer = "";
        try
        {
            Logger.logMessage("Entering method: "+className+"."+"getStepQuestions", Logger.LEVEL.DEBUG);
            allStepQuestions = dm.serializeSqlWithColNames("data.actions.getStepQuestions",  new Object[]{new Integer(stepID)});

            //for each step question determine question information
            for (HashMap questionsHM : allStepQuestions) {
                if (questionsHM != null) {
                    isDecision = (Boolean)questionsHM.get("ISDECISION");
                    QuestionIndex = Integer.parseInt(questionsHM.get("QUESTIONNUM").toString())-1;
                    String populationStepID = questionsHM.get("POPULATIONSQLSTEPID").toString();
                    String populationQuestionID = questionsHM.get("POPULATIONSQLQUESTIONID").toString();
                    String populationArgument = getPopulationArguments(populationStepID,populationQuestionID,answersObject);
                    //set default to special string "$_NONE_$" if the default was null (returned as "")
                    if (questionsHM.get("DEFAULTANSWER").equals("")) {
                        questionsHM.put("DEFAULTANSWER", "$_NONE_$");
                    } else if (questionsHM.get("DEFAULTANSWER").equals("empty")) {
                        questionsHM.put("DEFAULTANSWER", " ");
                    }

                    //add this question's index
                    questionsHM.put("QUESTIONINDEX", QuestionIndex);

                    //call determineQuestionAnswerHTML(questionTypeID)
                    questionsHM.put("QUESTIONANSWERHTML", determineQuestionAnswerHTML(questionsHM, QuestionIndex, populationArgument,request));

                    stepQuestions.add(questionsHM);

                    if (isDecision) {
                        break;
                    }
                }  //TODO: else error?
            }
            return stepQuestions;
        }
        catch (Exception e) {
            ArrayList<HashMap> errorCodeList = new ArrayList<>();
            Logger.logException(e);
            errorCodeList.add(commFunc.commonErrorMessage("Exception caught while trying to retrieve step questions for stepID: "+stepID));
            return errorCodeList;
        }
    }

    //decides what the sequence will be (in the case of a decision step)
    public ArrayList<HashMap<String, Object>> determineNextSequence(int currentSequenceID, int currentStepID, String stepAnswer, int stepIndexCount, ArrayList<HashMap> answersObject,HttpServletRequest request) {
        ArrayList<HashMap> nextSequenceInfo;
        ArrayList<HashMap<String, Object>> stepsObject;
        ArrayList<HashMap<String, Object>> errorCodeList = new ArrayList<>();
        Integer nextSequenceID = 0;
        Integer nextStepID = 0;
        try
        {
            Logger.logMessage("Entering method: "+className+"."+"determineNextSequence", Logger.LEVEL.DEBUG);
            LastAnswer = stepAnswer;
            if (!commonMMHFunctions.checkSession(request).equals("")) { //check that user is logged in
                nextSequenceInfo = dm.serializeSqlWithColNames("data.actions.determineNextSequence", new Object[]{currentSequenceID, currentStepID, stepAnswer},true);
                HashMap nextSequenceHM = nextSequenceInfo.get(0);
                nextSequenceID = Integer.parseInt(nextSequenceHM.get("NEXTSEQUENCEID").toString());
                nextStepID = Integer.parseInt(nextSequenceHM.get("NEXTSTEPID").toString());
                stepsObject = getSteps(nextSequenceID, nextStepID, stepIndexCount, answersObject,request);
                return stepsObject;
            } else {
                errorCodeList.add(commFunc.commonErrorMessage("Invalid Access."));
                return errorCodeList;
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            errorCodeList.add(new HashMap(){{put("FINISH","1");}});
            return errorCodeList;
        }
    }

    //decides what the next questions will be for a paritcular step (in the case of a decision question)
    public ArrayList<HashMap> determineNextStepQuestions(int stepQuestionID, String questionAnswer, ArrayList<HashMap> answersObject, HttpServletRequest request) {
        //TODO: Could this method be merged into getStepQuestions somehow? Or do we need two seperate methods? -jrmitaly
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        ArrayList<HashMap> nextStepQuestions;
        Boolean isDecision;
        Integer QuestionIndex;
        //Integer stepID = 0;
        //String defaultAnswer = "";
        try
        {
            Logger.logMessage("Entering method: "+className+"."+"determineNextStepQuestions", Logger.LEVEL.DEBUG);
            if (!commonMMHFunctions.checkSession(request).equals("")) { //check that user is logged in
                nextStepQuestions = dm.serializeSqlWithColNames("data.actions.determineNextStepQuestions", new Object[]{stepQuestionID, questionAnswer});

                //for each of the next step questions
                for (HashMap questionsHM : nextStepQuestions) {
                    if (questionsHM != null) {
                        isDecision = (Boolean)questionsHM.get("ISDECISION");
                        QuestionIndex = Integer.parseInt(questionsHM.get("QUESTIONNUM").toString())-1;
                        //stepID = Integer.parseInt(questionsHM.get("STEPID").toString())-1;

                        //add this question's index
                        questionsHM.put("QUESTIONINDEX", QuestionIndex);

                        //set the new question's answer to an empty string
                        questionsHM.put("QUESTIONANSWER", "");

                        //set default to special string "$_NONE_$" if the default was null (returned as "")
                        if (questionsHM.get("DEFAULTANSWER").equals("")) {
                            questionsHM.put("DEFAULTANSWER", "$_NONE_$");
                        } else if (questionsHM.get("DEFAULTANSWER").equals("empty")) {
                            questionsHM.put("DEFAULTANSWER", "");
                        }

                        //determine the population argument
                        String populationStepID = questionsHM.get("POPULATIONSQLSTEPID").toString();
                        String populationQuestionID = questionsHM.get("POPULATIONSQLQUESTIONID").toString();
                        String populationArgument = getPopulationArguments(populationStepID,populationQuestionID,answersObject);

                        //call determineQuestionAnswerHTML(questionTypeID)
                        questionsHM.put("QUESTIONANSWERHTML", determineQuestionAnswerHTML(questionsHM, QuestionIndex, populationArgument,request));

                        if (isDecision) {
                            break;
                        }
                    }
                }
                return nextStepQuestions;
            } else {
                errorCodeList.add(commFunc.commonErrorMessage("Invalid Access."));
                return errorCodeList;
            }
        }
        catch (Exception e) {
            Logger.logMessage("Exception caught while trying to retrieve the next step questions for stepQuestionID: "+stepQuestionID, Logger.LEVEL.ERROR);
            Logger.logException(e);
            errorCodeList.add(new HashMap(){{put("LOOKNEXTSTEP","1");}});
            //   errorCodeList.add(commFunc.commonErrorMessage("Exception caught while trying to retrieve the next step questions for stepQuestionID: "+stepQuestionID));
            return errorCodeList;
        }
    }

    //determine the HTML for all question answers
    public String determineQuestionAnswerHTML(HashMap questionsHM, Integer QuestionIndex, String populationArgument, HttpServletRequest request) {
        StringBuilder HTML = new StringBuilder();
        HTML.append("");
        try
        {
            Logger.logMessage("Entering method: "+className+"."+"determineQuestionAnswerHTML", Logger.LEVEL.DEBUG);

            int stepID = Integer.parseInt(questionsHM.get("STEPID").toString());
            int stepQuestionID = Integer.parseInt(questionsHM.get("STEPQUESTIONID").toString());
            int questionTypeID = Integer.parseInt(questionsHM.get("STEPQUESTIONTYPEID").toString());
            String populationSQLProp =  questionsHM.get("POPULATIONSQLPROP").toString();
            String dataFormattingMethodName =  questionsHM.get("DATAFORMATTINGMETHOD").toString();
            String dataFormattingObjectName =  questionsHM.get("DATAFORMATTINGOBJECT").toString();
            String validationType = questionsHM.get("DATAVALIDATIONTYPE").toString();
            Boolean isDecision = (Boolean)questionsHM.get("ISDECISION");
            String defaultAnswer = null;
            String toolTipSpanID;
            Integer userID = Integer.parseInt(commonMMHFunctions.checkSession(request));
            if (userID < 0) { //TODO: (HIGH) review - for My Quickcharge
                userID = commFunc.determineMyQCUserID();
            }

            int languageID = (commonMMHFunctions.getLanguageId(request) != 0 ? commonMMHFunctions.getLanguageId(request) : 1);
            ArrayList<HashMap> questionAnswerList = new ArrayList<>();

            if (questionsHM.get("DEFAULTANSWER") != null ) {
                defaultAnswer = questionsHM.get("DEFAULTANSWER").toString();
            }

            HTML.append("<div class=\"stepAnswerHTML\" data-noHandle=\"true\">"); //open stepAnswerHTML div tag

            //call dataFormatting method for this question, if there is one
            if (!dataFormattingMethodName.equals("") && !dataFormattingObjectName.equals("")) {
                Method dataFormatterMethod;
                try {
                    Class dataFormatClass = Class.forName(dataFormattingObjectName);
                    Class[] types = {};
                    Constructor constructor = dataFormatClass.getConstructor(types);
                    Object[] parameters = {};
                    Object dataFormattingObject = constructor.newInstance(parameters);

                    Class[] methodArgs = new Class[4];
                    methodArgs[0] = String.class;
                    methodArgs[1] = int.class;
                    methodArgs[2] = String.class;
                    methodArgs[3] = int.class;
                    methodArgs[4] = String.class;
                    methodArgs[5] = int.class;

                    dataFormatterMethod = dataFormatClass.getMethod(dataFormattingMethodName, methodArgs[0], methodArgs[1], methodArgs[2], methodArgs[3], methodArgs[4], methodArgs[5]);
                    try {
                        questionAnswerList = (ArrayList<HashMap>) dataFormatterMethod.invoke(dataFormattingObject, populationSQLProp, stepQuestionID, LastAnswer, userID, populationArgument, languageID);
                    } catch (IllegalArgumentException ex) {
                        Logger.logException(ex);
                        HTML.append("IllegalArgumentException determining questionAnswerList! <br>");
                    } catch (IllegalAccessException ex) {
                        Logger.logException(ex);
                        HTML.append("IllegalAccessException determining questionAnswerList! <br>");
                    }
                } catch (SecurityException ex) {
                    Logger.logException(ex);
                    HTML.append("SecurityException determining questionAnswerList! <br>");
                } catch (NoSuchMethodException ex) {
                    Logger.logException(ex);
                    HTML.append("NoSuchMethodException determining questionAnswerList! <br>");
                }
            } else if (questionTypeID != 4 && questionTypeID != 10 && questionTypeID != 13 && questionTypeID != 16) {
                if(questionTypeID == 7 || questionTypeID == 8) {
                     questionAnswerList = dm.serializeSqlLinkedHashmap(populationSQLProp,new Object[] {stepQuestionID, LastAnswer, userID, populationArgument, languageID});
                } else {
                    questionAnswerList = dm.serializeSqlWithColNames(populationSQLProp, new Object[] {stepQuestionID, LastAnswer, userID, populationArgument, languageID});
                }
            }

            //START - Create questionAnswerHTML depending on questionTypeID
            if (questionTypeID == 1 || questionTypeID == 6 || questionTypeID == 11 || questionTypeID == 14 || questionTypeID == 15) { //select box types
                HTML.append("<select id=\"question_"+stepQuestionID+"\" "); //start of open select tag
                if (isDecision) {
                    HTML.append("data-isDecision=\"1\" ");
                } else {
                    HTML.append("data-isDecision=\"0\" ");
                }
                HTML.append("data-questionIndex=\""+QuestionIndex+"\" ");
                //HTML.append("disabled='disabled' ");N

                //for multiselects
                if (questionTypeID == 6) {
                    HTML.append("multiple=\"multiple\" ");
                }
                HTML.append(">"); //end of open select tag

                //TODO: Do ALL selects need to have this? -jrmitaly
                HTML.append("<option value=' '></option>");

                for (HashMap optionHM : questionAnswerList) { //for each hashmap in the array list
                    Boolean useThisOption = true;
                    String useOptionStr = "";

                    //TODO: (med) shouldn't all types of select boxes utilize "USEOPTION"? I would think so... -jrmitaly
                    if ( !optionHM.containsKey("USEOPTION") ) {
                        optionHM.put("USEOPTION",'1');
                    }
                    useOptionStr = optionHM.get("USEOPTION").toString();
                    if (useOptionStr.equals("0") || useOptionStr.equals("false")) {
                        useThisOption = false;
                    }
                    if (useThisOption && defaultAnswer != null && defaultAnswer.equals(optionHM.get("VALUE").toString())) {
                        HTML.append("<option value='"+optionHM.get("VALUE").toString()+"' selected='selected'>"+optionHM.get("DISPLAY").toString()+"</option>");
                    } else if (useThisOption) {
                        HTML.append("<option value='"+optionHM.get("VALUE").toString()+"'>"+optionHM.get("DISPLAY").toString()+"</option>");
                    }

                }
                HTML.append("</select>"); //close select

                if (questionTypeID == 11) {
                    HTML.append("<div class=\"hyperFindFilterIconContainer\"></div>");
                }

                //CommunicationMessage
                if (questionTypeID == 14) {
                    HTML.append("<div class=\"communicationMessageIconContainer\"></div>");
                    HTML.append("<div class=\"communicationMessageIconEditContainer \" style=\"display:none;\"></div>");
                }
                //CommunicationTarget
                if (questionTypeID == 15) {
                    HTML.append("<div class=\"communicationTargetIconContainer\"></div>");
                    HTML.append("<div class=\"communicationTargetIconEditContainer \" style=\"display:none;\"></div>");
                }

            } else if (questionTypeID == 2) { //checkbox types
                HashMap checkBoxHM =  questionAnswerList.get(0);
                //HTML.append("<div class=\"checkBoxContainer\">");

                HTML.append("<input type=\"checkbox\" class=\"styledCheckbox\" id=\"question_"+stepQuestionID+"\" "); //start of open checkbox tag
                if (isDecision) {
                    HTML.append("data-isDecision=\"1\" ");
                } else {
                    HTML.append("data-isDecision=\"0\" ");
                }
                HTML.append("data-questionIndex=\""+QuestionIndex+"\" ");
                //HTML.append("disabled='disabled' ");
                if ((defaultAnswer != null && defaultAnswer.equals("on"))) {
                    //HTML.append("value='"+checkBoxHM.get("VALUE").toString()+"' checked/> "+checkBoxHM.get("DISPLAY").toString()+" </br>");
                    HTML.append("value='"+checkBoxHM.get("VALUE").toString()+"' checked/>");
                    HTML.append("<label for=\"question_"+stepQuestionID+"\"></label><span class=\"checkboxLabelText\">"+checkBoxHM.get("DISPLAY").toString() + "</span>");
                } else {
                    //HTML.append("value='"+checkBoxHM.get("VALUE").toString()+"'/> "+checkBoxHM.get("DISPLAY").toString()+" </br>");
                    HTML.append("value='"+checkBoxHM.get("VALUE").toString()+"'/>");
                    HTML.append("<label for=\"question_"+stepQuestionID+"\"></label><span class=\"checkboxLabelText\">"+checkBoxHM.get("DISPLAY").toString() + "</span>");
                }
            } else if (questionTypeID == 3) { //radio button
                Boolean useThisOption;
                String useOptionStr;
                for (HashMap optionHM : questionAnswerList) { //for each hashmap in the array lists

                    //Build spanID for tooltips
                    toolTipSpanID = getToolTipSpanID(optionHM);

                    useThisOption = true;
                    useOptionStr = optionHM.get("USEOPTION").toString();
                    if (useOptionStr.equals("0") || useOptionStr.equals("false")) {
                        useThisOption = false;
                    }
                    if (useThisOption) {
                        HTML.append("<input type=\"radio\" name=\"question_"+stepQuestionID+"\" ");
                        if (isDecision) {
                            HTML.append("data-isDecision=\"1\" ");
                        } else {
                            HTML.append("data-isDecision=\"0\" ");
                        }
                        HTML.append("data-questionIndex="+QuestionIndex+" ");
                        if (defaultAnswer != null && defaultAnswer.equals(optionHM.get("VALUE").toString())) {
                            HTML.append("value='"+optionHM.get("VALUE").toString()+"' checked='checked'/> <span " + toolTipSpanID + " title=''>" + optionHM.get("DISPLAY").toString() + "</span> </br>");
                        } else {
                            HTML.append("value='"+optionHM.get("VALUE").toString()+"'/> <span " + toolTipSpanID + " title=''>" + optionHM.get("DISPLAY").toString() + "</span> </br>");
                        }
                    }
                }
            } else if (questionTypeID == 4 || questionTypeID == 10) { //text input
                HTML.append("<input type=\"text\" id=\"question_"+stepQuestionID+"\" ");
                if (isDecision) {
                    HTML.append("data-isDecision=\"1\" ");
                } else {
                    HTML.append("data-isDecision=\"0\" ");
                }

                HTML.append("data-questionIndex="+QuestionIndex+" ");

                if (questionTypeID == 10) {
                    HTML.append("data-validationtype=\"negativeCurrency\"");
                }
                if(!validationType.isEmpty()){
                    HTML.append("data-validationType='"+validationType+"'");
                }
                if (defaultAnswer != null && !defaultAnswer.equals("$_NONE_$")) {
                    HTML.append("value='"+defaultAnswer+"'/> ");
                } else {
                    HTML.append("/> ");
                }
            } else if (questionTypeID == 16) { //textarea
                HTML.append("<textarea id=\"question_"+stepQuestionID+"\" ");
                HTML.append("data-questionIndex="+QuestionIndex+" ");
                HTML.append("rows=\"4\" cols=\"50\"");

                //Decision logic
                if (isDecision) { HTML.append("data-isDecision=\"1\" "); }
                else            { HTML.append("data-isDecision=\"0\" "); }

                //Validation logic
                if(!validationType.isEmpty()){ HTML.append("data-validationType='"+validationType+"'"); }

                //Default answer logic
                HTML.append(">");
                if (defaultAnswer != null && !defaultAnswer.equals("$_NONE_$")) { HTML.append(defaultAnswer); }

                //Close tag
                HTML.append("</textarea>");

            } else if (questionTypeID == 5) { //date picker
                Boolean useThisOption;
                String useOptionStr;
                for (HashMap datePickerHM : questionAnswerList) { //for each hashmap in the array lists
                    useThisOption = true;
                    useOptionStr = datePickerHM.get("USEOPTION").toString();
                    if (useOptionStr.equals("0") || useOptionStr.equals("false")) {
                        useThisOption = false;
                    }
                    if (useThisOption) {
                        HTML.append("<div data-nohandle=\"true\" class=\"actionDatePicker ui-state-default ui-corner-all\">"); //open datePickerDiv tag
                        HTML.append("<label for='question_"+stepQuestionID+"'>"+datePickerHM.get("DISPLAY")+"</label>");
                        HTML.append("<input id='question_"+stepQuestionID+"' name='question_"+stepQuestionID+"' ");
                        if (isDecision) {
                            HTML.append("data-isDecision=\"1\" ");
                        } else {
                            HTML.append("data-isDecision=\"0\" ");
                        }
                        if (defaultAnswer != null && defaultAnswer.length() > 0) {
                            HTML.append("data-dateDefaultValue=\"" + defaultAnswer + "\" ");
                        }
                        HTML.append("data-datePickerType=" + datePickerHM.get("VALUE") + " ");
                        HTML.append("data-questionIndex=" + QuestionIndex + " ");
                        //HTML.append("disabled='disabled' ");
                        HTML.append("class='ui-state-default ui-corner-all' type='text' />");
                        HTML.append("</div>"); //close datePickerDiv tag
                    }
                }
            } else if (questionTypeID == 7 || questionTypeID == 8 || questionTypeID == 9) { //selector grid
                HTML.append("<table id=\"question_" + stepQuestionID + "\" ");

                if (isDecision) {
                    HTML.append("data-isDecision=\"1\" ");
                } else {
                    HTML.append("data-isDecision=\"0\" ");
                }

                HTML.append("data-questionIndex=" + QuestionIndex + " ");

                Boolean firstRow = true;
                for (HashMap <String, Object> gridHM : questionAnswerList) { //for each hashmap in the array lists
                    if (firstRow) {
                        HTML.append("<tr>");
                        for (Object key : gridHM.keySet()) {
                            HTML.append("<th>" + (key.toString().compareTo("ID") == 0 ? "ID" : commonMMHFunctions.uppercaseFirstLetters(key.toString().toLowerCase())) + "</th>");
                        }
                        HTML.append("</tr>");
                        firstRow = false;
                    }

                    HTML.append("<tr id=\""+gridHM.get("ID").toString()+"\">");

                    for (Map.Entry<String, Object> entry : gridHM.entrySet()) {
                        Object value = entry.getValue();
                        HTML.append("<td>" + value + "</td>");
                    }
                    HTML.append("</tr>");
                }
                HTML.append("</table>");
                HTML.append("<div id=\"pager_"+stepQuestionID+"\"></div>");
            }
            else if(questionTypeID == 12){
                HTML.append("<input id=\"question_"+stepQuestionID+"\" type=\"hidden\" multiple=\"multiple\""); //start of open select tag
                if (isDecision) {
                    HTML.append("data-isDecision=\"1\" ");
                } else {
                    HTML.append("data-isDecision=\"0\" ");
                }
                HTML.append("data-questionIndex=\""+QuestionIndex+"\" ");
                HTML.append("data-serverLoad=\"true\" ");
                HTML.append("class=\"serverLoad\"");
                HTML.append("/>");

            }
            else if(questionTypeID == 13){
                // NON FORM - NEW WEBFILETRANSFER
                HTML.append("Select a file to upload: <input type=\"file\" name=\"file\" id=\"fileUpload\" size=\"60\"/><br/>");
                HTML.append("<input id='fileUploadID' type='test' style='display:none' data-questionIndex="+QuestionIndex+"></input>");
            }
            //END - Create questionAnswerHTML depending on questionTypeID

            HTML.append("</div>"); //close stepAnswerHTML div tag
        }
        catch (Exception e) {
            Logger.logException(e);
        }
        return HTML.toString();
    }

    //This can be used if tabular tooltip functionality is added later
    private String getToolTipSpanID(HashMap map) {
        String retVal = "";
        if (map.containsKey("STEPQUESTIONANSWERID")) {
            retVal = "id='SQAID" + map.get("STEPQUESTIONANSWERID").toString() + "_span'";
        }

        return retVal;
    }

    //TODO: Add notes
    public ArrayList<HashMap> serverLoadOptions(HashMap loadData_DANGEROUS,HttpServletRequest request){
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        ArrayList<HashMap> options = new ArrayList<>();
        String populationSQL = "";
        Integer userID = Integer.parseInt(commonMMHFunctions.checkSession(request));
        if (userID < 0) { //TODO: (HIGH) review - for My Quickcharge
            userID = commFunc.determineMyQCUserID();
        }
        if(loadData_DANGEROUS.containsKey("QUESTIONPOP")){
            populationSQL = loadData_DANGEROUS.get("QUESTIONPOP").toString();
        }else if(loadData_DANGEROUS.containsKey("QUESTIONID")){
            String questionID = loadData_DANGEROUS.get("QUESTIONID").toString();
            populationSQL = dm.getSingleField("data.actions.GetQuestionSQL", new Object[]{questionID}).toString();
        }
        if(!populationSQL.isEmpty()){
            populationSQL+="WithArgs";//TODO: This is a user defined string... and could be modified to a different query than intended...
            if(!populationSQL.isEmpty()){
                DynamicSQL dynamicSQL = new DynamicSQL(populationSQL);
                String ids = "";
                String likeParam = "";
                if(loadData_DANGEROUS.containsKey("IDS")){
                    StringArrayToString(loadData_DANGEROUS,"IDS","IDS",true);
                    //TODO: this is just a patch, we should be using parameterization
                    String ids_DANGEROUS =loadData_DANGEROUS.get("IDS").toString();
                    ids_DANGEROUS = ids_DANGEROUS.substring(2, ids_DANGEROUS.length()-2);
                    ids = "('" + ids_DANGEROUS.replaceAll("[^0-9^,^-]", "") + "')";
                }
                if(loadData_DANGEROUS.containsKey("LIKE")){
                    String likeParam_DANGEROUS = loadData_DANGEROUS.get("LIKE").toString();
                    //TODO: this is just a patch, we should be using parameterization
                    likeParam = "'%"+likeParam_DANGEROUS.replaceAll("[^a-z^A-Z^0-9^\\.^_^\\$^\\s]", "_")+"%'";
                }
                dynamicSQL.addIDList(1, ids, true);
                dynamicSQL.addLikeCondition(2, likeParam);
                dynamicSQL.addValue(3, userID.toString());

                try {
                    options = dynamicSQL.serialize(dm);//dm.serializeSqlWithColNames(populationSQL,new Object[]{ids,likeParam,userID.toString()},false);
                }catch (Exception e){
                    errorCodeList.add(commFunc.commonErrorMessage("Unable to retrieve options"));
                    Logger.logMessage("Database error retrieiving question options", Logger.LEVEL.ERROR);
                    Logger.logException(e);
                    return errorCodeList;
                }
            }
        }else{
            errorCodeList.add(commFunc.commonErrorMessage("Unable to retrieve options"));
            Logger.logMessage("Could not find population SQL for select box", Logger.LEVEL.ERROR);
            return errorCodeList;
        }
        return options;
    }

    //TODO: Add notes
    private void StringArrayToString(HashMap record, String fieldName, String mapTo,boolean parenthesesFlag){
        StringBuilder builder = new StringBuilder();
        try{
            if(record.get(fieldName) instanceof String[]){
                String[] arr = (String[])record.get(fieldName);
                String prefix="";
                for(int i=0;i<arr.length;i++){
                    builder.append(prefix);
                    prefix=","; builder.append(arr[i]);
                }

            }else if(record.get(fieldName) instanceof ArrayList){
                ArrayList arr = (ArrayList)record.get(fieldName);
                String prefix="";
                for(int i =0;i<arr.size();i++){
                    builder.append(prefix);
                    prefix=",";
                    builder.append(arr.get(i).toString());
                }
            }
            record.remove(fieldName);
            String empIDs;
            if(builder.toString().isEmpty()){
                builder.append("''");
            }
            if(parenthesesFlag){
                empIDs = "("+builder.toString()+")";
            }else{
                empIDs = builder.toString();
            }
            record.put(mapTo,empIDs);

        }catch (Exception e){
            if(parenthesesFlag){
                record.put(mapTo,"('')");
            }else{
                record.put(mapTo,"''");
            }

        }

    }

    //TODO: Add notes
    private String getPopulationArguments(String populationStepID, String populationQuestionID, ArrayList<HashMap> answersObject){
        String populationArgument = "";
        if(!populationStepID.isEmpty()){
            if(!populationQuestionID.isEmpty()){//get the question answer
                for(HashMap step: answersObject){
                    if(step.get("STEPID").toString().equals(populationStepID)){
                        populationArgument = ap.findStepQuestion(step,populationQuestionID).get("QUESTIONANSWER").toString();
                    }
                }
            }else{//get the step answer
                for(HashMap step: answersObject){
                    if(step.get("STEPID").toString().equals(populationStepID)){
                        populationArgument = step.get("STEPANSWER").toString();
                    }
                }
            }
        }
        return populationArgument;
    }

    public ArrayList<HashMap> getOrgLevelLabels(HttpServletRequest request){
        // Init return variable
        ArrayList<HashMap> levels = new ArrayList<>();
        try {
            // Check user is logged in
            if(commonMMHFunctions.checkSession(request).equalsIgnoreCase("")){
                throw new Exception("Invalid Access.");
            }
            // Get Global Fields
            levels = dm.parameterizedExecuteQuery("data.globalSettings.getGlobalSettings", new Object[]{}, true);
        } catch(Exception ex){
            Logger.logMessage("Error in method: getOrgLevelLabels()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return levels;
    }

}