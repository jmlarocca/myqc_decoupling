package com.mmhayes.common.loyalty.models;

// API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;

// Other dependencies
import java.util.ArrayList;
import java.util.HashMap;

public class LoyaltyDonationModel {

    private Integer id;
    private String name;
    private String description;
    private LoyaltyProgramModel loyaltyProgram;
    private Integer terminalId;

    public LoyaltyDonationModel() {

    }

    public LoyaltyDonationModel(HashMap loyaltyDonationModelHM) throws Exception {
        setModelProperties(loyaltyDonationModelHM);
    }

    public LoyaltyDonationModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));
        setTerminalId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALID")));

        if (getTerminalId() == null || !PosAPIModelCache.useTerminalCache) {
            if (this.getLoyaltyProgram() == null) {
                if (CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")) != null) {
                    setLoyaltyProgram(LoyaltyProgramModel.getOneLoyaltyProgram(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")), getTerminalId()));
                }
            }
        }
        else if (PosAPIModelCache.useTerminalCache) {
            if (this.getLoyaltyProgram() == null) {
                if (CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")) != null) {
                    LoyaltyProgramModel validatedLoyaltyProgram = null;
                    validatedLoyaltyProgram = (LoyaltyProgramModel) PosAPIModelCache.getOneObject(getTerminalId(), CommonAPI.PosModelType.LOYALTY_PROGRAM, CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")));
                    if (validatedLoyaltyProgram != null) {
                        setLoyaltyProgram(validatedLoyaltyProgram);
                    } else {
                        throw new MissingDataException("Invalid Loyalty Program", getTerminalId());
                    }
                }
            }
        }

        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LoyaltyProgramModel getLoyaltyProgram() {
        return loyaltyProgram;
    }

    public void setLoyaltyProgram(LoyaltyProgramModel loyaltyProgram) {
        this.loyaltyProgram = loyaltyProgram;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }
}
