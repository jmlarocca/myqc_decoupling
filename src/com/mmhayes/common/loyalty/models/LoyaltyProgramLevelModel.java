package com.mmhayes.common.loyalty.models;

//API dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-31 16:39:34 -0500 (Thu, 31 Dec 2020) $: Date of last commit
 $Rev: 13381 $: Revision of last commit
*/
@Deprecated
public class LoyaltyProgramLevelModel {

    //Level properties
    private Integer id = null;
    private Integer programId = null;
    private String itemType = "";
    private String earningType = "";
    private String subDepartmentName = "";
    private String departmentName = "";
    private String productName = "";

    private String productPlu = null;
    private Integer revenueCenterId = null;
    private Integer points = null;

    private Integer earningTypeId = null;
    private BigDecimal maxPointsPerTransaction = null;

    //Fields used for Point calculation
    private BigDecimal totalLevelPoints = BigDecimal.ZERO;
    private ArrayList<String> pointCalculationLogList = new ArrayList<>();
    private HashMap<Integer, BigDecimal> productLinesCalcList = new HashMap<>();

    public LoyaltyProgramLevelModel() {

    }

    //Constructor- takes in a Hashmap
    public LoyaltyProgramLevelModel(HashMap loyaltyProgramHM) {
        setModelProperties(loyaltyProgramHM);
    }

    //setter for all of this model's properties
    public LoyaltyProgramLevelModel setModelProperties(HashMap modelDetailHM) {

        //setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMLEVELID")));
        setProgramId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PROGRAMID")));
        setItemType(CommonAPI.convertModelDetailToString(modelDetailHM.get("ITEMTYPE")));
        setEarningType(CommonAPI.convertModelDetailToString(modelDetailHM.get("EARNINGTYPE")));
        setEarningTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LEVELEARNINGTYPEID")));
        setSubDepartmentName(CommonAPI.convertModelDetailToString(modelDetailHM.get("SUBDEPARTMENTNAME")));
        setDepartmentName(CommonAPI.convertModelDetailToString(modelDetailHM.get("DEPARTMENTNAME")));
        setProductName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PRODUCTNAME")));
        setProductPlu(CommonAPI.convertModelDetailToString(modelDetailHM.get("PLUCODE")));
        setRevenueCenterId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("REVENUECENTERID")));
        setPoints(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POINTS")));
        setMaxPointsPerTransaction(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("MAXPERTRANSACTION")));

        return this;
    }

    public static LoyaltyProgramLevelModel createLoyaltyProgramLevelModel(HashMap loyaltyProgramLevelHM) throws Exception {

        if ((CommonAPI.convertModelDetailToString(loyaltyProgramLevelHM.get("EARNINGTYPE")) != null
                && !CommonAPI.convertModelDetailToString(loyaltyProgramLevelHM.get("EARNINGTYPE")).isEmpty())
                || CommonAPI.convertModelDetailToInteger(loyaltyProgramLevelHM.get("POINTS")) != null) {
            return new LoyaltyProgramLevelModel(loyaltyProgramLevelHM);
        } else {
            return null;
        }
    }

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getEarningType() {
        return earningType;
    }

    public void setEarningType(String earningType) {
        this.earningType = earningType;
    }

    public String getSubDepartmentName() {
        return subDepartmentName;
    }

    public void setSubDepartmentName(String subDepartmentName) {
        this.subDepartmentName = subDepartmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public BigDecimal getMaxPointsPerTransaction() {
        return maxPointsPerTransaction;
    }

    public void setMaxPointsPerTransaction(BigDecimal maxPointsPerTransaction) {
        this.maxPointsPerTransaction = maxPointsPerTransaction;
    }

    public Integer getRevenueCenterId() {
        return revenueCenterId;
    }

    public void setRevenueCenterId(Integer revenueCenterId) {
        this.revenueCenterId = revenueCenterId;
    }

    public String getProductPlu() {
        return productPlu;
    }

    public void setProductPlu(String productPlu) {
        this.productPlu = productPlu;
    }

    @JsonIgnore
    public Integer getEarningTypeId() {
        return earningTypeId;
    }

    public void setEarningTypeId(Integer earningTypeId) {
        this.earningTypeId = earningTypeId;
    }

    public HashMap<Integer, BigDecimal> getProductLinesCalcList() {
        return productLinesCalcList;
    }

    public void setProductLinesCalcList(HashMap<Integer, BigDecimal> productLinesCalcList) {
        this.productLinesCalcList = productLinesCalcList;
    }

    public BigDecimal getTotalLevelPoints() {
        return totalLevelPoints;
    }

    public void setTotalLevelPoints(BigDecimal totalLevelPoints) {
        this.totalLevelPoints = totalLevelPoints;
    }

    public ArrayList<String> getPointCalculationLogList() {
        return pointCalculationLogList;
    }

    public void setPointCalculationLogList(ArrayList<String> pointCalculationLogList) {
        this.pointCalculationLogList = pointCalculationLogList;
    }

    /**
     * Overridden toString method for a LoyaltyProgramLevelModel.
     * @return The {@link String} representation of a LoyaltyProgramLevelModel.
     */
    @Override
    public String toString () {

        String pointCalculationLogListStr = "";
        if (!DataFunctions.isEmptyCollection(pointCalculationLogList)) {
            pointCalculationLogListStr = StringFunctions.buildStringFromList(pointCalculationLogList, ",");
        }

        String productLinesCalcListStr = "";
        if (!DataFunctions.isEmptyMap(productLinesCalcList)) {
            productLinesCalcListStr += "[";
            int entriesIteratedOver = 0;
            for (Map.Entry<Integer, BigDecimal> entry : productLinesCalcList.entrySet()) {
                if (entry.getKey() != null && entry.getKey() > 0 && entry.getValue() != null && entriesIteratedOver == pointCalculationLogList.size() - 1) {
                    productLinesCalcListStr += "[" + entry.getKey() + ": " + entry.getValue().toPlainString() + "]";
                }
                else if (entry.getKey() != null && entry.getKey() > 0 && entry.getValue() != null && entriesIteratedOver < pointCalculationLogList.size() - 1) {
                    productLinesCalcListStr += "[" + entry.getKey() + ": " + entry.getValue().toPlainString() + "],";
                }
                entriesIteratedOver++;
            }
            productLinesCalcListStr += "]";
        }

        return String.format("ITEMTYPE: %s, EARNINGTYPE: %s, SUBDEPARTMENTNAME: %s, DEPARTMENTNAME: %s, PRODUCTNAME: %s, " +
                "PRODUCTPLU: %s, ID: %s, PROGRAMID: %s, REVENUECENTERID: %s, POINTS: %s, EARNINGTYPEID: %s, " +
                "MAXPOINTSPERTRANSACTION: %s, TOTALLEVELPOINTS: %s, POINTCALCULATIONLOGLIST: %s, PRODUCTLINESCALCLIST: %s",
                Objects.toString((StringFunctions.stringHasContent(itemType) ? itemType : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(earningType) ? earningType : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(subDepartmentName) ? subDepartmentName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(departmentName) ? departmentName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(productName) ? productName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(productPlu) ? productPlu : "N/A"), "N/A"),
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((programId != null && programId > 0 ? programId : "N/A"), "N/A"),
                Objects.toString((revenueCenterId != null && revenueCenterId > 0 ? revenueCenterId : "N/A"), "N/A"),
                Objects.toString((points != null && points > 0 ? points : "N/A"), "N/A"),
                Objects.toString((earningTypeId != null && earningTypeId > 0 ? earningTypeId : "N/A"), "N/A"),
                Objects.toString((maxPointsPerTransaction != null ? maxPointsPerTransaction.toPlainString() : "N/A"), "N/A"),
                Objects.toString((totalLevelPoints != null ? totalLevelPoints.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(pointCalculationLogListStr) ? pointCalculationLogListStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(productLinesCalcListStr) ? productLinesCalcListStr : "N/A"), "N/A"));

    }

}
