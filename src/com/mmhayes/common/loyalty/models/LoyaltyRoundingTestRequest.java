package com.mmhayes.common.loyalty.models;

//Other Dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-03-07 15:19:42 -0500 (Wed, 07 Mar 2018) $: Date of last commit
 $Rev: 6553 $: Revision of last commit
*/
public class LoyaltyRoundingTestRequest {

    //This is what BigDecimal.setScale uses:
    /*    public static final int ROUND_UP = 0;
    public static final int ROUND_DOWN = 1;
    public static final int ROUND_CEILING = 2;
    public static final int ROUND_FLOOR = 3;
    public static final int ROUND_HALF_UP = 4;
    public static final int ROUND_HALF_DOWN = 5;
    public static final int ROUND_HALF_EVEN = 6;
    public static final int ROUND_UNNECESSARY = 7;*/

    //Here is a sample JSON request:
    /*{
        "amount" : 1.45,
            "monetaryRoundingPrecision" : 1,

            "monetaryRoundingValue" : 4,
            "pointRoundingValue" : 5

    }*/

    private int monetaryRoundingPrecision = 0;
    private int monetaryRoundingValue = 0;
    private int pointRoundingPrecision = 0;
    private int pointRoundingValue = 0;

    private String monetaryRoundingType = "";
    private String pointRoundingType = "";
    private String monetaryRoundingPrecisionType = "";

    private BigDecimal amount = BigDecimal.ZERO;
    private BigDecimal multiplier = BigDecimal.ONE;

    private LoyaltyRoundingTestResult loyaltyRoundingTestResult = null;

    public void initFields(){

        if (getMonetaryRoundingType() != null){
            setMonetaryRoundingType(getMonetaryRoundingType().toUpperCase());
        }

        switch (getMonetaryRoundingType()){
            case "HALFUP":
                setMonetaryRoundingValue(4);
                break;
            case "HALFDOWN":
                setMonetaryRoundingValue(5);
                break;

            case "DOWN":
                setMonetaryRoundingValue(1);
                break;
            case "UP":
                setMonetaryRoundingValue(0);
                break;
        }

        if (getPointRoundingType() != null){
            setPointRoundingType(getPointRoundingType().toUpperCase());
        }

        switch (getPointRoundingType()){
            case "HALFUP":
                setPointRoundingValue(4);
                break;
            case "HALFDOWN":
                setPointRoundingValue(5);
                break;

            case "DOWN":
                setPointRoundingValue(1);
                break;

            case "UP":
                setPointRoundingValue(0);
                break;
        }


        if (getMonetaryRoundingPrecisionType() != null){
            setMonetaryRoundingPrecisionType(getMonetaryRoundingPrecisionType().toUpperCase());

        }

        switch (getMonetaryRoundingPrecisionType()){
            case "PENNY":
                 setMonetaryRoundingPrecision(2);
                break;

            case "DIME":
                setMonetaryRoundingPrecision(1);
                break;

            case "DOLLAR":
                setMonetaryRoundingPrecision(0);
                break;
        }
    }

    public void completeRounding(){

        BigDecimal tempMonetaryRounding = BigDecimal.ZERO;
        BigDecimal tempMultiplierResult = BigDecimal.ZERO;
        BigDecimal tempPointRounding = BigDecimal.ZERO;

        LoyaltyRoundingTestResult loyaltyRoundingTestResult = new LoyaltyRoundingTestResult();

        //Step 1: Apply Monetary Rounding
        tempMonetaryRounding = this.getAmount().setScale(this.getMonetaryRoundingPrecision(), this.getMonetaryRoundingValue());
        loyaltyRoundingTestResult.setMonetaryRoundingResult(tempMonetaryRounding);


        //Step 2: Apply Multiplier
        tempMultiplierResult = tempMonetaryRounding.multiply(this.getMultiplier());
        loyaltyRoundingTestResult.setMultiplierResult(tempMultiplierResult);

        //Step 3: Apply Point Rounding
        tempPointRounding = tempMultiplierResult.setScale(this.getPointRoundingPrecision(), this.getPointRoundingValue());
        loyaltyRoundingTestResult.setPointRoundingResult(tempPointRounding);

        this.setLoyaltyRoundingTestResult(loyaltyRoundingTestResult);

    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @JsonIgnore
    public int getMonetaryRoundingPrecision() {
        return monetaryRoundingPrecision;
    }

    public void setMonetaryRoundingPrecision(int monetaryRoundingPrecision) {
        this.monetaryRoundingPrecision = monetaryRoundingPrecision;
    }

    @JsonIgnore
    public int getMonetaryRoundingValue() {
        return monetaryRoundingValue;
    }

    public void setMonetaryRoundingValue(int monetaryRoundingValue) {
        this.monetaryRoundingValue = monetaryRoundingValue;
    }

    @JsonIgnore
    public int getPointRoundingPrecision() {
        return pointRoundingPrecision;
    }

    public void setPointRoundingPrecision(int pointRoundingPrecision) {
        this.pointRoundingPrecision = pointRoundingPrecision;
    }

    @JsonIgnore
    public int getPointRoundingValue() {
        return pointRoundingValue;
    }

    public void setPointRoundingValue(int pointRoundingValue) {
        this.pointRoundingValue = pointRoundingValue;
    }

    public BigDecimal getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    public LoyaltyRoundingTestResult getLoyaltyRoundingTestResult() {
        return loyaltyRoundingTestResult;
    }

    public void setLoyaltyRoundingTestResult(LoyaltyRoundingTestResult loyaltyRoundingTestResult) {
        this.loyaltyRoundingTestResult = loyaltyRoundingTestResult;
    }

    public String getMonetaryRoundingType() {
        return monetaryRoundingType;
    }

    public void setMonetaryRoundingType(String monetaryRoundingType) {
        this.monetaryRoundingType = monetaryRoundingType;
    }

    public String getPointRoundingType() {
        return pointRoundingType;
    }

    public void setPointRoundingType(String pointRoundingType) {
        this.pointRoundingType = pointRoundingType;
    }

    public String getMonetaryRoundingPrecisionType() {
        return monetaryRoundingPrecisionType;
    }

    public void setMonetaryRoundingPrecisionType(String monetaryRoundingPrecisionType) {
        this.monetaryRoundingPrecisionType = monetaryRoundingPrecisionType;
    }

}
