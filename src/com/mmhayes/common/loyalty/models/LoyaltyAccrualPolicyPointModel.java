package com.mmhayes.common.loyalty.models;

//MMHayes Dependencies
import com.mmhayes.common.transaction.models.LoyaltyRewardLineItemModel;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;

//Other Dependencies
import java.util.List;


/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-12-04 12:26:20 -0500 (Tue, 04 Dec 2018) $: Date of last commit
 $Rev: 8158 $: Revision of last commit
*/
public class LoyaltyAccrualPolicyPointModel extends LoyaltyPointModel {

    public LoyaltyAccrualPolicyPointModel(){

    }

    public LoyaltyAccrualPolicyPointModel(LoyaltyPointModel loyaltyPointModel){
        super.setLoyaltyAccrualPolicy(loyaltyPointModel.getLoyaltyAccrualPolicy());
        super.getLoyaltyAccrualPolicy().setTenders(null); // clear out the tenders for display
        super.getLoyaltyAccrualPolicy().setTypeName("");
        super.getLoyaltyAccrualPolicy().setDescription("");
        super.setPoints(loyaltyPointModel.getPoints());
    }

    @Override
    @JsonIgnore
    public Integer getEmployeeId() {
        return super.getEmployeeId();
    }

    @Override
    @JsonIgnore
    public List<LoyaltyRewardModel> getRewardsAvailable() {
        return super.getRewardsAvailable();
    }

    @Override
    @JsonIgnore
    public List<LoyaltyRewardLineItemModel> getRewardsAvailableForTransaction() {
        return super.getRewardsAvailableForTransaction();
    }

}
