package com.mmhayes.common.loyalty.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.LoyaltyProgramNotFoundException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.loyalty.collections.LoyaltyAccrualPolicyCollection;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.TenderDisplayModel;
import com.mmhayes.common.transaction.models.TenderModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-31 16:39:34 -0500 (Thu, 31 Dec 2020) $: Date of last commit
 $Rev: 13381 $: Revision of last commit
*/
public class LoyaltyProgramModel {

    private Integer id;
    private Integer ownershipGroupId = null;
    private Integer typeId = null; //Loyalty Program Type ID
    private Integer monetaryRoundingTypeId = null;
    private Integer pointRoundingTypeId = null;
    private Integer monetaryRoundingPrecisionId = null;
    private Integer earningTypeId = null;

    private String name = "";
    private String typeName = ""; //Loyalty Program Type Name
    private String description = "";
    private String monetaryRoundingTypeName = ""; //how do you round the dollar amount
    private String monetaryRoundingPrecision = ""; //dimes, dollars, pennies
    private String pointRoundingTypeName = ""; //how do you round the points
    private String earningType = "";
    private BigDecimal pointsMultiplier = null;
    private Boolean includeTax = false;
    private List<LoyaltyProgramLevelModel> levelDetails = new ArrayList<>();
    private List<TenderModel> tenders = new ArrayList<>();
    private List<Integer> payrollGroupingIds = new ArrayList<>();
    private List<LoyaltyAccrualPolicyModel> accrualPolicies = new ArrayList<>();

    //constructors
    public LoyaltyProgramModel() {

    }

    //Constructor- takes in a Hashmap
    public LoyaltyProgramModel(HashMap loyaltyProgramHM, Integer terminalId, boolean summaryMode) throws Exception {
        setModelProperties(loyaltyProgramHM, terminalId, summaryMode);
    }

    //setter for all of this model's properties

    /**
     * if summaryMode is true, Program Level for the Loyalty Program won't be returned
     */
    public LoyaltyProgramModel setModelProperties(HashMap modelDetailHM, Integer terminalId, boolean summaryMode) throws Exception {

        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PROGRAMNAME")));
        setTypeName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PROGRAMTYPENAME")));
        setTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PROGRAMTYPEID")));
        setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));

        setMonetaryRoundingTypeName(CommonAPI.convertModelDetailToString(modelDetailHM.get("MONETARYROUNDINGTYPENAME")));
        setMonetaryRoundingTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MONETARYROUNDINGTYPEID")));

        setMonetaryRoundingPrecision(CommonAPI.convertModelDetailToString(modelDetailHM.get("MONETARYROUNDINGPRECISION"))); //dimes, dollars, pennies
        setMonetaryRoundingPrecisionId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MONETARYROUNDINGPRECISIONID"))); //dimes, dollars, pennies

        setPointRoundingTypeName(CommonAPI.convertModelDetailToString(modelDetailHM.get("POINTROUNDINGTYPENAME"))); //how do you round the points
        setPointRoundingTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POINTROUNDINGTYPEID"))); //how do you round the points

        setOwnershipGroupId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("OWNERSHIPGROUPID")));
        setPointsMultiplier(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("POINTSMULTIPLIER")));
        setIncludeTax(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("INCLUDETAX")));
        setEarningType(CommonAPI.convertModelDetailToString(modelDetailHM.get("PROGRAMEARNINGTYPENAME")));
        setEarningTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYEARNINGTYPEID")));

        if (!summaryMode) {

            //Get all AccrualPolicies
            setAccrualPolicies(new LoyaltyAccrualPolicyCollection().getAllLoyaltyAccrualPoliciesByProgramId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")), terminalId, false).getCollection());


            //get the Account Groupings.  This is called when the cache is turned off
            setPayrollGroupMappings(this.getId(), terminalId);

        }

        return this;
    }

    public boolean hasAccountGroupMapped(TransactionModel transactionModel) {
        boolean result = false;

        if (this.getPayrollGroupingIds() != null && this.getPayrollGroupingIds().size() > 0) {
            if (transactionModel.getLoyaltyAccount().getPayrollGroupingId() != null) {
                for (Integer mappedAccountGroupId : this.getPayrollGroupingIds()) {
                    if (mappedAccountGroupId.equals(transactionModel.getLoyaltyAccount().getPayrollGroupingId())) {
                        result = true;
                        break;
                    }
                }
            }
        }

        return result;
    }

    public static LoyaltyProgramModel getOneLoyaltyProgram(Integer loyaltyProgramId, Integer terminalId, boolean summaryMode) throws Exception {
        DataManager dm = new DataManager();
        LoyaltyProgramModel loyaltyProgramModel = new LoyaltyProgramModel();

        //get all models in an array list
        ArrayList<HashMap> loyaltyProgramList = dm.parameterizedExecuteQuery("data.posapi30.getOneLoyaltyProgramById",
                new Object[]{loyaltyProgramId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        if (loyaltyProgramList == null || loyaltyProgramList.isEmpty()) {
            return null; //no programs were found
        } else {
            loyaltyProgramModel = new LoyaltyProgramModel(loyaltyProgramList.get(0), terminalId, summaryMode);

            //create the Tender list
            for (HashMap hashMap : loyaltyProgramList) {
                loyaltyProgramModel.getTenders().add(new TenderDisplayModel(hashMap));
            }

            return loyaltyProgramModel;
        }
    }

    //This is used by the Loyalty Endpoint
    //It throws a LoyaltyProgramNotFoundException if the Id is not found.
    public static LoyaltyProgramModel getOneActiveLoyaltyProgram(Integer loyaltyProgramId, TerminalModel terminalModel, boolean summaryMode, boolean loyaltyEndPoint) throws Exception {
        DataManager dm = new DataManager();
        LoyaltyProgramModel loyaltyProgramModel = new LoyaltyProgramModel();

        //get all models in an array list
        ArrayList<HashMap> loyaltyProgramList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveLoyaltyProgramById",
                new Object[]{loyaltyProgramId, terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        if (loyaltyProgramList == null || loyaltyProgramList.isEmpty()) {

            throw new LoyaltyProgramNotFoundException("Loyalty Program Not Found.", terminalModel.getId());

        } else {
            loyaltyProgramModel = new LoyaltyProgramModel(loyaltyProgramList.get(0), terminalModel.getId(), summaryMode);

            //create the Tender list
            for (HashMap hashMap : loyaltyProgramList) {
                if (CommonAPI.convertModelDetailToInteger(loyaltyProgramList.get(0).get("PATENDERID")) != null
                        && !CommonAPI.convertModelDetailToInteger(loyaltyProgramList.get(0).get("PATENDERID")).toString().isEmpty()) {
                    loyaltyProgramModel.getTenders().add(new TenderModel(hashMap));
                }
            }

            return loyaltyProgramModel;
        }
    }

    //summary mode
    public static LoyaltyProgramModel getOneLoyaltyProgram(Integer loyaltyProgramId, Integer terminalId) throws Exception {
        return getOneLoyaltyProgram(loyaltyProgramId, terminalId, false);
    }

    public void setPayrollGroupMappings(Integer loyaltyProgramId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        //LoyaltyProgramModel loyaltyProgramModel = new LoyaltyProgramModel();

        //get all models in an array list
        ArrayList<HashMap> payrollGroupings = dm.parameterizedExecuteQuery("data.posapi30.getAccountGroupMappingForOneLoyaltyProgramById",
                new Object[]{loyaltyProgramId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        if (payrollGroupings == null || payrollGroupings.isEmpty()) {
            //return null; //no programs were found
        } else {

            for (HashMap hashMap : payrollGroupings) {
                if (CommonAPI.convertModelDetailToInteger(hashMap.get("PAYROLLGROUPINGID")) != null) {
                    this.getPayrollGroupingIds().add(CommonAPI.convertModelDetailToInteger(hashMap.get("PAYROLLGROUPINGID")));
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoyaltyProgramModel)) return false;

        LoyaltyProgramModel that = (LoyaltyProgramModel) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    //Getters and Setters
    //getter
    public Integer getId() {
        return id;
    }

    //setter
    public void setId(Integer id) {
        this.id = id;
    }

    //getter
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    //getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getTypeName() {
        return typeName;
    }

    //setter
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    //getter
    @JsonIgnore
    public String getMonetaryRoundingTypeName() {
        return monetaryRoundingTypeName;
    }

    //setter
    public void setMonetaryRoundingTypeName(String monetaryRoundingTypeName) {
        this.monetaryRoundingTypeName = monetaryRoundingTypeName;
    }

    //getter
    @JsonIgnore
    public String getMonetaryRoundingPrecision() {
        return monetaryRoundingPrecision;
    }

    //setter
    public void setMonetaryRoundingPrecision(String monetaryRoundingPrecision) {
        this.monetaryRoundingPrecision = monetaryRoundingPrecision;
    }

    //getter
    @JsonIgnore
    public String getPointRoundingTypeName() {
        return pointRoundingTypeName;
    }

    //setter
    public void setPointRoundingTypeName(String pointRoundingTypeName) {
        this.pointRoundingTypeName = pointRoundingTypeName;
    }

    //getter
    @JsonIgnore
    public Integer getOwnershipGroupId() {
        return ownershipGroupId;
    }

    //setter
    public void setOwnershipGroupId(Integer ownershipGroupId) {
        this.ownershipGroupId = ownershipGroupId;
    }

    //getter
    @JsonIgnore
    public BigDecimal getPointsMultiplier() {
        return pointsMultiplier;
    }

    //setter
    public void setPointsMultiplier(BigDecimal pointsMultiplier) {
        this.pointsMultiplier = pointsMultiplier;
    }

    //getter
    @JsonIgnore
    public Boolean getIncludeTax() {
        return includeTax;
    }

    //setter
    public void setIncludeTax(Boolean includeTax) {
        this.includeTax = includeTax;
    }

    @JsonIgnore
    @Deprecated
    public List<LoyaltyProgramLevelModel> getLevelDetails() {
        return levelDetails;
    }

    public void setLevelDetails(List<LoyaltyProgramLevelModel> levelDetails) {
        this.levelDetails = levelDetails;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    //@JsonIgnore
    //On any json coming into the api, deserialize the object as a TenderDisplayModel
    @JsonDeserialize(as = ArrayList.class, contentAs = TenderDisplayModel.class)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<TenderModel> getTenders() {
        return tenders;
    }

    public void setTenders(List<TenderModel> tenders) {
        this.tenders = tenders;
    }

    @JsonIgnore
    public String getEarningType() {
        return earningType;
    }

    public void setEarningType(String earningType) {
        this.earningType = earningType;
    }

    @JsonIgnore
    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    @JsonIgnore
    public Integer getMonetaryRoundingTypeId() {
        return monetaryRoundingTypeId;
    }

    public void setMonetaryRoundingTypeId(Integer monetaryRoundingTypeId) {
        this.monetaryRoundingTypeId = monetaryRoundingTypeId;
    }

    @JsonIgnore
    public Integer getPointRoundingTypeId() {
        return pointRoundingTypeId;
    }

    public void setPointRoundingTypeId(Integer pointRoundingTypeId) {
        this.pointRoundingTypeId = pointRoundingTypeId;
    }

    @JsonIgnore
    public Integer getMonetaryRoundingPrecisionId() {
        return monetaryRoundingPrecisionId;
    }

    public void setMonetaryRoundingPrecisionId(Integer monetaryRoundingPrecisionId) {
        this.monetaryRoundingPrecisionId = monetaryRoundingPrecisionId;
    }

    @JsonIgnore
    public Integer getEarningTypeId() {
        return earningTypeId;
    }

    public void setEarningTypeId(Integer earningTypeId) {
        this.earningTypeId = earningTypeId;
    }

    @JsonIgnore
    public List<Integer> getPayrollGroupingIds() {
        return payrollGroupingIds;
    }

    public void setPayrollGroupingIds(List<Integer> payrollGroupingIds) {
        this.payrollGroupingIds = payrollGroupingIds;
    }

    @JsonIgnore
    public List<LoyaltyAccrualPolicyModel> getAccrualPolicies() {
        return accrualPolicies;
    }

    public void setAccrualPolicies(List<LoyaltyAccrualPolicyModel> accrualPolicies) {
        this.accrualPolicies = accrualPolicies;
    }

    /**
     * Overridden toString method for a LoyaltyProgramModel.
     * @return The {@link String} representation of a LoyaltyProgramModel.
     */
    @Override
    public String toString () {

        String levelDetailsStr = "";
        if (!DataFunctions.isEmptyCollection(levelDetails)) {
            levelDetailsStr += "[";
            for (LoyaltyProgramLevelModel levelDetail : levelDetails) {
                if (levelDetail != null && levelDetail.equals(levelDetails.get(levelDetails.size() - 1))) {
                    levelDetailsStr += levelDetail.toString();
                }
                else if (levelDetail != null && !levelDetail.equals(levelDetails.get(levelDetails.size() - 1))) {
                    levelDetailsStr += levelDetail.toString() + "; ";
                }
            }
            levelDetailsStr += "]";
        }

        String tendersStr = "";
        if (!DataFunctions.isEmptyCollection(tenders)) {
            tendersStr += "[";
            for (TenderModel tender : tenders) {
                if (tender != null && tender.equals(tenders.get(tenders.size() - 1))) {
                    tendersStr += tender.toString();
                }
                else if (tender != null && !tender.equals(tenders.get(tenders.size() - 1))) {
                    tendersStr += tender.toString() + "; ";
                }
            }
            tendersStr += "]";
        }

        String payrollGroupingIdsStr = "";
        if (!DataFunctions.isEmptyCollection(payrollGroupingIds)) {
            payrollGroupingIdsStr = StringFunctions.buildStringFromList(payrollGroupingIds, ",");
        }

        String accrualPoliciesStr = "";
        if (!DataFunctions.isEmptyCollection(accrualPolicies)) {
            accrualPoliciesStr += "[";
            for (LoyaltyAccrualPolicyModel accrualPolicy : accrualPolicies) {
                if (accrualPolicy != null && accrualPolicy.equals(accrualPolicies.get(accrualPolicies.size() - 1))) {
                    accrualPoliciesStr += accrualPolicy.toString();
                }
                else if (accrualPolicy != null && !accrualPolicy.equals(accrualPolicies.get(accrualPolicies.size() - 1))) {
                    accrualPoliciesStr += accrualPolicy.toString() + "; ";
                }
            }
            accrualPoliciesStr += "]";
        }

        return String.format("ID: %s, OWNERSHIPGROUPID: %s, TYPEID: %s, MONETARYROUNDINGTYPEID: %s, " +
                "POINTROUNDINGTYPEID: %s, MONETARYROUNDINGPRECISIONID: %s, EARNINGTYPEID: %s, NAME: %s, TYPENAME: %s, " +
                "DESCRIPTION: %s, MONETARYROUNDINGTYPENAME: %s, MONETARYROUNDINGPRECISION: %s, POINTROUNDINGTYPENAME: %s, " +
                "EARNINGTYPE: %s, POINTSMULTIPLIER: %s, INCLUDETAX: %s, LEVELDETAILS: %s, TENDERS: %s, " +
                "PAYROLLGROUPINGIDS: %s, ACCRUALPOLICIES: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((ownershipGroupId != null && ownershipGroupId > 0 ? ownershipGroupId : "N/A"), "N/A"),
                Objects.toString((typeId != null && typeId > 0 ? typeId : "N/A"), "N/A"),
                Objects.toString((monetaryRoundingTypeId != null && monetaryRoundingTypeId > 0 ? monetaryRoundingTypeId : "N/A"), "N/A"),
                Objects.toString((pointRoundingTypeId != null && pointRoundingTypeId > 0 ? pointRoundingTypeId : "N/A"), "N/A"),
                Objects.toString((monetaryRoundingPrecisionId != null && monetaryRoundingPrecisionId > 0 ? monetaryRoundingPrecisionId : "N/A"), "N/A"),
                Objects.toString((earningTypeId != null && earningTypeId > 0 ? earningTypeId : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(typeName) ? typeName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(monetaryRoundingTypeName) ? monetaryRoundingTypeName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(monetaryRoundingPrecision) ? monetaryRoundingPrecision : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(pointRoundingTypeName) ? pointRoundingTypeName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(earningType) ? earningType : "N/A"), "N/A"),
                Objects.toString((pointsMultiplier != null ? pointsMultiplier.toPlainString() : "N/A"), "N/A"),
                Objects.toString((includeTax != null ? includeTax : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(levelDetailsStr) ? levelDetailsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(tendersStr) ? tendersStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(payrollGroupingIdsStr) ? payrollGroupingIdsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(accrualPoliciesStr) ? accrualPoliciesStr : "N/A"), "N/A"));

    }

}
