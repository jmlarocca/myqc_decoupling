package com.mmhayes.common.loyalty.models;

//MMHayes dependencies

import com.mmhayes.common.account.models.*;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.TransactionBuilder;
import com.mmhayes.common.api.errorhandling.exceptions.AccountNotFoundException;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.login.LoginModel;
import com.mmhayes.common.loyalty.collections.*;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.dataaccess.DataManager;

//api dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mmhayes.common.api.CommonAPI;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.utils.Logger;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: mjbatko $: Author of last commit
 $Date: 2021-06-28 08:26:49 -0400 (Mon, 28 Jun 2021) $: Date of last commit
 $Rev: 14233 $: Revision of last commit
*/
@JsonPropertyOrder({"id", "name", "badge", "badgeAlias", "badgeAssignmentId", "number", "status", "accountType", "accountTypeId", "contact", "address1",
        "address2", "city", "state", "zipCode", "phone", "email", "available", "singleChargeLimit", "terminalGroupBalance",
        "terminalGroupLimit", "globalBalance", "globalLimit", "eligiblePayments", "receiptBalanceLabel", "receiptBalance", "receiptBalanceTypeId",
        "birthday", "spendingProfileId", "autoCloudInvite", "mobilePhone", "notes", "terminalMappedToSpendingProfile",
        "payrollGroupingId", "restrictionProfileId", "redacted", "qcDiscountsAvailable", "qcDiscountsApplied", "loyaltyPoints", "rewardsAvailable",
        "rewardsAvailableForTransaction"})
public class LoyaltyAccountModel extends AccountModel implements ILoyaltyAccountModel {

    private List<LoyaltyPointModel> loyaltyPoints = new ArrayList<>();
    private List<LoyaltyRewardModel> rewardsAvailable = new ArrayList<>();
    private List<LoyaltyRewardLineItemModel> rewardsAvailableForTransaction = new ArrayList<>();
    private boolean hasFetchedAvailableRewards = false;
    commonMMHFunctions commFunc = new commonMMHFunctions();

    public LoyaltyAccountModel() {

    }

    public LoyaltyAccountModel(Integer employeeId) {
        this.setId(employeeId);
    }

    //used to downcast AccountModel to a LoyaltyAccountModel
    public LoyaltyAccountModel(AccountModel accountModel, boolean includeRewards) throws Exception {
        this.mapAccountModelToLoyaltyAccount(accountModel);

        if (includeRewards) {  //fetch rewards
            this.fetchLoyaltyRewardsForAccount();
        }

        this.initLoyaltyAccountModel();
    }

    /**
     * If creating a Loyalty Account from a Transaction
     * check if the transaction was recorded offline
     * @param accountModel
     * @param includeRewards
     * @param transactionModel
     * @throws Exception
     */
    public LoyaltyAccountModel(AccountModel accountModel, boolean includeRewards, TransactionModel transactionModel) throws Exception {
        this.mapAccountModelToLoyaltyAccount(accountModel);

        //HP 1195 - egl - we use the transactionModel.LoyaltyAccount when fetching valid rewards.  Need to make sure the account id is set
        if (transactionModel.getLoyaltyAccount() == null || transactionModel.getLoyaltyAccount().getId() == null){
            transactionModel.setLoyaltyAccount(this);
        }

        if (includeRewards) { //fetch rewards

            if (transactionModel.wasRecordedOffline()) {
                fetchLoyaltyRewardsForAccount(transactionModel); //this will return all Rewards
            } else {
                fetchLoyaltyRewardsForAccount(); //this will return Rewards, only if the the Account Holder has enough Loyalty points
            }
        }

        this.initLoyaltyAccountModel();
    }

    private void mapAccountModelToLoyaltyAccount(AccountModel accountModel) {
        super.setId(accountModel.getId());
        super.setBadgeAlias(accountModel.getBadgeAlias());
        super.setBadge(accountModel.getBadge());
        super.setBadgeAssignmentId(accountModel.getBadgeAssignmentId());
        super.setEmail(accountModel.getEmail());
        super.setAccountTypeId(accountModel.getAccountTypeId());
        super.setAccountType(accountModel.getAccountType());

        super.setName(accountModel.getName());
        super.setNumber(accountModel.getNumber());
        super.setStatus(accountModel.getStatus());
        super.setEligiblePayments(accountModel.getEligiblePayments());

        super.setSingleChargeLimit(accountModel.getSingleChargeLimit());
        super.setAvailable(accountModel.getAvailable());

        super.setGlobalLimit(accountModel.getGlobalLimit());
        super.setGlobalBalance(accountModel.getGlobalBalance());

        super.setTerminalGroupLimit(accountModel.getTerminalGroupLimit());
        super.setTerminalGroupBalance(accountModel.getTerminalGroupBalance());

        super.setTerminal(accountModel.getTerminal());
        super.setAddress1(accountModel.getAddress1());
        super.setAddress2(accountModel.getAddress2());
        super.setCity(accountModel.getCity());
        super.setState(accountModel.getState());
        super.setZipCode(accountModel.getZipCode());
        super.setPhone(accountModel.getPhone());
        super.setContact(accountModel.getContact());

        super.setPayrollGroupingId(accountModel.getPayrollGroupingId());
        super.setReceiptBalanceTypeId(accountModel.getReceiptBalanceTypeId());
        super.setReceiptBalanceLabel(accountModel.getReceiptBalanceLabel());
        super.setReceiptBalance(accountModel.getReceiptBalance());

        super.setBirthday(accountModel.getBirthday());
        super.setAutoCloudInvite(accountModel.getAutoCloudInvite());
        super.setMobilePhone(accountModel.getMobilePhone());

        super.setSpendingProfileMappedForTerminal(accountModel.getSpendingProfileMappedForTerminal());
        super.setSpendingProfileId(accountModel.getSpendingProfileId());
        super.setNotes(accountModel.getNotes());

        super.setAccountGroup(accountModel.getAccountGroup());
        super.setQcDiscountsAvailable(accountModel.getQcDiscountsAvailable());
        super.setRedacted(accountModel.isRedacted());
    }

    private void initLoyaltyAccountModel() throws Exception {

        fetchLoyaltyPointsForAccount();
        matchUpPointsAndRewards();
    }

    public static ILoyaltyAccountModel getLoyaltyAccountById(TerminalModel terminal, BigDecimal transactionValue, Integer accountId, AccountQueryParams accountQueryParams) throws Exception {
        AccountModel accountModel = AccountModel.getAccountModelByIdForTender(terminal, (transactionValue == null) ? BigDecimal.ZERO : transactionValue, accountId, accountQueryParams);
        accountModel.setTerminal(terminal);
        LoyaltyAccountModel loyaltyAccountModel = new LoyaltyAccountModel(accountModel, true);
        return loyaltyAccountModel;
    }

    public static ILoyaltyAccountModel getLoyaltyAccountByNumber(TerminalModel terminal, BigDecimal transactionValue, String accountNumber, AccountQueryParams accountQueryParams) throws Exception {
        AccountModel accountModel = AccountModel.getAccountModelByNumberForTender(terminal, (transactionValue == null) ? BigDecimal.ZERO : transactionValue, accountNumber, accountQueryParams);
        accountModel.setTerminal(terminal);
        LoyaltyAccountModel loyaltyAccountModel = new LoyaltyAccountModel(accountModel, true);
        return loyaltyAccountModel;
    }

    public static ILoyaltyAccountModel getLoyaltyAccountByBadge(TerminalModel terminal, BigDecimal transactionValue, String badgeNumber, AccountQueryParams accountQueryParams) throws Exception {
        AccountModel accountModel = AccountModel.getAccountModelByBadgeForTender(terminal, (transactionValue == null) ? BigDecimal.ZERO : transactionValue, badgeNumber, accountQueryParams);
        accountModel.setTerminal(terminal);
        LoyaltyAccountModel loyaltyAccountModel = new LoyaltyAccountModel(accountModel, true);
        return loyaltyAccountModel;
    }

    public static ILoyaltyAccountModel getLoyaltyAccountByIdOptionalTerminal(TerminalModel terminal, BigDecimal transactionValue, Integer accountId, AccountQueryParams accountQueryParams) throws Exception {
        AccountModel accountModel = null;
        if (terminal.getId() == null){
            accountModel = AccountModel.getAccountModelById(accountId, terminal); //if there is no terminal ID, only grab a simple AccountModel
        } else {
            accountModel = AccountModel.getAccountModelByIdForTender(terminal, (transactionValue == null) ? BigDecimal.ZERO : transactionValue, accountId, accountQueryParams);
        }
        accountModel.setTerminal(terminal);
        LoyaltyAccountModel loyaltyAccountModel = new LoyaltyAccountModel(accountModel, true);
        return loyaltyAccountModel;
    }

    public static ILoyaltyAccountModel getLoyaltyAccountByBadgeOptionalTerminal(TerminalModel terminal, BigDecimal transactionValue, String badgeNumber, AccountQueryParams accountQueryParams) throws Exception {
        AccountModel accountModel = null;
        if (terminal.getId() == null){
            accountModel = AccountModel.getAccountModelByBadge(badgeNumber, terminal); //if there is no terminal ID, only grab a simple AccountModel
        } else {
            accountModel = AccountModel.getAccountModelByBadgeForTender(terminal, (transactionValue == null) ? BigDecimal.ZERO : transactionValue, badgeNumber, accountQueryParams);
        }
        accountModel.setTerminal(terminal);
        LoyaltyAccountModel loyaltyAccountModel = new LoyaltyAccountModel(accountModel, true);
        return loyaltyAccountModel;
    }

    public static ILoyaltyAccountModel getBalanceByBadgeNumber(TerminalModel terminalModel, LoginModel loginModel, AccountQueryParams accountQueryParams, String cardNumber) throws Exception {
        LoyaltyAccountModel loyaltyAccountModel = null;

        if (loginModel.getEmployeeId() != null) {
            //If Employee Id was on the DSKey, use that
            loyaltyAccountModel = (LoyaltyAccountModel) LoyaltyAccountModel.getLoyaltyAccountByIdOptionalTerminal(terminalModel, BigDecimal.ZERO, loginModel.getEmployeeId(), accountQueryParams);

            //validate that the Employee ID matches the Card Number that was passed in
            if (!loyaltyAccountModel.getBadge().equals(cardNumber)) {
                throw new AccountNotFoundException("Authorized Account cannot search for this Card number.");
            }
        } else {
            //For some Third Party's, they won't have an EmployeeId on the DSKey, this allows them pass in the card number
            loyaltyAccountModel = (LoyaltyAccountModel) LoyaltyAccountModel.getLoyaltyAccountByBadgeOptionalTerminal(terminalModel, BigDecimal.ZERO, cardNumber, accountQueryParams);
        }

        return loyaltyAccountModel;
    }

    //get employee information and account data by Id Number and Terminal Id
    public static Integer getEmployeeIdByTransactionId(Integer transactionId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        Integer employeeId = null;

        //get all models in an array list
        ArrayList<HashMap> employeeList = dm.parameterizedExecuteQuery("data.posapi30.getOneEmployeeIdByTransactionId",
                new Object[]{transactionId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        if (employeeList.size() > 0) {
            employeeId = CommonAPI.convertModelDetailToInteger(employeeList.get(0).get("EMPLOYEEID"));
        }

        return employeeId;

    }

    public void setRewardsAvailableNotesIfOffline(boolean isTerminalOffline) {

        if (isTerminalOffline) {
            for (LoyaltyRewardModel rewardModel : this.getRewardsAvailable()) {
                rewardModel.setNotes("Terminal is offline: Rewards not available");
            }
        }
    }

    public void fetchLoyaltyPointsForAccount() throws Exception {
        List<LoyaltyPointModel> loyaltyPoints = LoyaltyAccountModel.getLoyaltyPointsForAccount(this);
        this.setLoyaltyPoints(loyaltyPoints);
    }

    public void matchUpPointsAndRewards() {
        for (LoyaltyPointModel loyaltyPointModel : this.getLoyaltyPoints()) {
            for (LoyaltyRewardModel loyaltyRewardModel : this.getRewardsAvailable()) {
                if (loyaltyPointModel.getLoyaltyProgram().getId().equals(loyaltyRewardModel.getLoyaltyProgram().getId())) {
                    loyaltyPointModel.getRewardsAvailable().add(loyaltyRewardModel);
                }
            }
        }
    }

    public static List<LoyaltyPointModel> getLoyaltyPointsForAccount(ILoyaltyAccountModel loyaltyAccountModel) throws Exception {
        LoyaltyPointCollection loyaltyPoints = new LoyaltyPointCollection();

        if (loyaltyAccountModel.getId() != null) {
            loyaltyPoints = LoyaltyPointCollection.getAllLoyaltyPointsForEmployeeAndActivePrograms(loyaltyAccountModel.getId(), loyaltyAccountModel.getTerminal(), false);
        }

        return loyaltyPoints.getCollection();
    }

    //valid Loyalty Rewards for the Points on this account
    public void fetchLoyaltyRewardsForAccount() throws Exception {

        if (!hasFetchedAvailableRewards()) {
            this.setRewardsAvailable(LoyaltyAccountModel.getLoyaltyRewardsForAccount(this));
        }
        setHasFetchedAvailableRewards(true);
    }

    //valid Loyalty Rewards for the Points on this account
    public void fetchLoyaltyRewardsForAccount(TransactionModel transactionModel) throws Exception {

        if (!hasFetchedAvailableRewards()) {
            this.setRewardsAvailable(LoyaltyAccountModel.getLoyaltyRewardsForAccount(this, transactionModel));
        }
        setHasFetchedAvailableRewards(true);
    }

    public static List<LoyaltyRewardModel> getLoyaltyRewardsForAccount(ILoyaltyAccountModel loyaltyAccountModel) throws Exception {
        LoyaltyRewardCollection loyaltyRewardCollection = new LoyaltyRewardCollection();

        if (loyaltyAccountModel.getId() != null) {
            loyaltyRewardCollection = LoyaltyRewardCollection.getValidLoyaltyRewardsForAccount(loyaltyAccountModel.getTerminal(), loyaltyAccountModel);
        }
        return loyaltyRewardCollection.getCollection();
    }

    public static List<LoyaltyRewardModel> getLoyaltyRewardsForAccount(ILoyaltyAccountModel loyaltyAccountModel, TransactionModel transactionModel) throws Exception {
        LoyaltyRewardCollection loyaltyRewardCollection = new LoyaltyRewardCollection();

        if (loyaltyAccountModel.getId() != null) {
            loyaltyRewardCollection = LoyaltyRewardCollection.getValidLoyaltyRewardsForAccount(transactionModel);
        }
        return loyaltyRewardCollection.getCollection();
    }

    public void freezeAccount() throws Exception {
        super.freezeAccount();
    }

    public void unFreezeAccount() throws Exception {
        super.unFreezeAccount();
    }

    @JsonIgnore
    public void getBalanceForEmployee(TerminalModel terminalModel) throws Exception {
        super.getBalancesForEmployee(terminalModel);
    }

    public List<LoyaltyPointModel> getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(List<LoyaltyPointModel> loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    @JsonIgnore
    public List<LoyaltyRewardModel> getRewardsAvailable() {
        return rewardsAvailable;
    }

    public void setRewardsAvailable(List<LoyaltyRewardModel> rewardsAvailable) {
        this.rewardsAvailable = rewardsAvailable;
    }

    @JsonIgnore
    public List<LoyaltyRewardLineItemModel> getRewardsAvailableForTransaction() {
        return rewardsAvailableForTransaction;
    }

    public void setRewardsAvailableForTransaction(List<LoyaltyRewardLineItemModel> rewardsAvailableForTransaction) {
        this.rewardsAvailableForTransaction = rewardsAvailableForTransaction;
    }

    @JsonIgnore
    public
    @Override
    Integer getEligiblePayments() {
        return super.getEligiblePayments();
    }

    public
    @Override
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<QcDiscountModel> getQcDiscountsAvailable() {
        return super.getQcDiscountsAvailable();
    }

    public
    @Override
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<QcDiscountLineItemModel> getQcDiscountsApplied() {
        return super.getQcDiscountsApplied();
    }

    @JsonIgnore
    public boolean hasFetchedAvailableRewards() {
        return hasFetchedAvailableRewards;
    }

    public void setHasFetchedAvailableRewards(boolean hasFetchedAvailableRewards) {
        this.hasFetchedAvailableRewards = hasFetchedAvailableRewards;
    }

    public static void createLoyaltyAdjustmentTransaction(Integer employeeId, HashMap loyaltyAdjustmentTransactionHM) {
        Logger.logMessage("In method AccountsHandler.createLoyaltyAdjustmentTransaction", Logger.LEVEL.DEBUG);

        try {
            int loyaltyAdjustmentTypeID = CommonAPI.convertModelDetailToInteger(loyaltyAdjustmentTransactionHM.get("LOYALTYADJUSTMENTTYPEID"));
            String userIdStr = CommonAPI.convertModelDetailToString(loyaltyAdjustmentTransactionHM.get("USERID"));
            Integer userId = Integer.parseInt(userIdStr);

            // determine the field details that were passed from the front end
            Integer loyaltyProgramId = Integer.parseInt(loyaltyAdjustmentTransactionHM.get("LOYALTYPROGRAMID").toString());
            Integer terminalId = Integer.parseInt(loyaltyAdjustmentTransactionHM.get("TERMINALID").toString());
            Integer points = Integer.parseInt(loyaltyAdjustmentTransactionHM.get("POINTS").toString());
            String transComment = loyaltyAdjustmentTransactionHM.getOrDefault("TRANSCOMMENT", "").toString();

            // create a transaction model and set the terminal id
            TransactionModel transactionModel = new TransactionModel();
            transactionModel.setTerminalId(terminalId);

            // create a terminal model, and build the pos api cache; set the transaction to use this terminal
            TerminalModel terminalModel = TerminalModel.createTerminalModel(terminalId, false);

            // create a login model with the correct user id and attach it to the terminal model;
            // this accounts for any checks that use the login model to check the user id
            LoginModel loginModel = new LoginModel();
            loginModel.setUserId(userId);
            terminalModel.setLoginModel(loginModel);
            transactionModel.setTerminal(terminalModel);

            // set the transaction type to loyalty adjustment
            transactionModel.setTransactionTypeEnum(PosAPIHelper.TransactionType.LOYALTY_ADJUSTMENT);

            /* get the current date/time */
            java.util.Date newSaveDate = new java.util.Date();
            SimpleDateFormat newSaveDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            String newSaveDateString = newSaveDateFormat.format(newSaveDate);
            /* /get the current date/time */
            // set the timestamps on the transaction to be the current date/time
            transactionModel.setTimeStamp(newSaveDateString);
            transactionModel.setTicketOpenedDTM(newSaveDateString);

            // set the transaction comment
            transactionModel.setTransComment(transComment);
            // set the user id making this transaction directly in the transaction
            transactionModel.setUserId(userId);

            // force set the transaction drawer num to 0 (pseudo null) => necessary since the transaction isn't
            // actually occurring at the terminal it is related to
            transactionModel.setDrawerNum(0);

            // set up the objects required to create the loyalty adjustment model
            HashMap loyaltyAdjustmentHM = new HashMap();
            loyaltyAdjustmentHM.put("POINTS", points);
            loyaltyAdjustmentHM.put("LOYALTYPROGRAMID", loyaltyProgramId);
            LoyaltyAdjustmentModel loyaltyAdjustmentModel = new LoyaltyAdjustmentModel(loyaltyAdjustmentHM);

            // retrieve the loyalty program model using the loyalty program id; set the adjustment model to use this program
            LoyaltyProgramModel loyaltyProgramModel = LoyaltyProgramModel.getOneLoyaltyProgram(loyaltyProgramId, terminalId);
            loyaltyAdjustmentModel.setLoyaltyProgram(loyaltyProgramModel);

            // set the transaction's loyalty adjustment list to be a list length 1 with the current adjustment
            List<LoyaltyAdjustmentModel> loyaltyAdjustmentModelList = new ArrayList<>();
            loyaltyAdjustmentModelList.add(loyaltyAdjustmentModel);
            transactionModel.setLoyaltyAdjustments(loyaltyAdjustmentModelList);

            LoyaltyAccountModel loyaltyAccountModel = new LoyaltyAccountModel(employeeId);
            transactionModel.setLoyaltyAccount(loyaltyAccountModel);

            transactionModel.setLoyaltyAdjustmentTypeId(loyaltyAdjustmentTypeID);

            TransactionBuilder transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, transactionModel, PosAPIHelper.ApiActionType.LOYALTY_ADJUSTMENT);
            transactionBuilder.getTransactionModel().checkTransactionTypeEnum();
            transactionBuilder.buildTransaction();
            transactionBuilder.saveTransaction();

        } catch (Exception ex) {
            Logger.logMessage("Error creating loyalty adjustment transaction in AccountsHandler.createLoyaltyAdjustmentTransaction", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

}
