package com.mmhayes.common.loyalty.models;

//MMHayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.account.models.*;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;

//api dependencies
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mmhayes.common.api.CommonAPI;


//other dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-08-29 11:12:59 -0400 (Wed, 29 Aug 2018) $: Date of last commit
 $Rev: 7701 $: Revision of last commit
*/

@JsonPropertyOrder({"id", "name", "badge", "badgeAlias", "badgeAssignmentId", "number", "status", "accountType", "contact", "address1",
        "address2", "city", "state", "zipCode", "phone", "email", "available", "singleChargeLimit", "terminalGroupBalance",
        "terminalGroupLimit", "globalBalance", "globalLimit", "eligiblePayments", "payrollGroupingId", "receiptBalanceTypeId",
        "receiptBalanceLabel", "receiptBalance", "qcDiscountsAvailable", "qcDiscountsApplied", "loyaltyPoints", "rewardsAvailable",
        "rewardsAvailableForTransaction"})
@Deprecated
public class LoyaltyAccountModelWithBalances extends AccountModel implements ILoyaltyAccountModel {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<LoyaltyPointModel> loyaltyPoints = new ArrayList<>();
    List<LoyaltyRewardModel> rewardsAvailable = new ArrayList<>();
    List<LoyaltyRewardLineItemModel> rewardsAvailableForTransaction = new ArrayList<>();
    private boolean hasFetchedAvailableRewards = false;

    public LoyaltyAccountModelWithBalances() {

    }

    public LoyaltyAccountModelWithBalances(Integer employeeId) {
        this.setId(employeeId);
    }

    //used to downcast AccountModel to a LoyaltyAccountModel
    public LoyaltyAccountModelWithBalances(AccountModel accountModel) throws Exception {
        super.setId(accountModel.getId());
        super.setBadgeAlias(accountModel.getBadgeAlias());
        super.setBadge(accountModel.getBadge());
        super.setBadgeAssignmentId(accountModel.getBadgeAssignmentId());
        super.setEmail(accountModel.getEmail());
        super.setAccountTypeId(accountModel.getAccountTypeId());
        super.setAccountType(accountModel.getAccountType());

        super.setName(accountModel.getName());
        super.setNumber(accountModel.getNumber());
        super.setStatus(accountModel.getStatus());
        super.setEligiblePayments(accountModel.getEligiblePayments());

        super.setSingleChargeLimit(accountModel.getSingleChargeLimit());
        super.setAvailable(accountModel.getAvailable());

        super.setGlobalLimit(accountModel.getGlobalLimit());
        super.setGlobalBalance(accountModel.getGlobalBalance());

        super.setTerminalGroupLimit(accountModel.getTerminalGroupLimit());
        super.setTerminalGroupBalance(accountModel.getTerminalGroupBalance());

        super.setTerminal(accountModel.getTerminal());
        super.setAddress1(accountModel.getAddress1());
        super.setAddress2(accountModel.getAddress2());
        super.setCity(accountModel.getCity());
        super.setState(accountModel.getState());
        super.setZipCode(accountModel.getZipCode());
        super.setPhone(accountModel.getPhone());
        super.setContact(accountModel.getContact());

        super.setPayrollGroupingId(accountModel.getPayrollGroupingId());
        super.setReceiptBalanceTypeId(accountModel.getReceiptBalanceTypeId());
        super.setReceiptBalanceLabel(accountModel.getReceiptBalanceLabel());

        super.setBirthday(accountModel.getBirthday());
        super.setAutoCloudInvite(accountModel.getAutoCloudInvite());
        super.setSpendingProfileId(accountModel.getSpendingProfileId());
        super.setNotes(accountModel.getNotes());

        fetchLoyaltyRewardsForAccount();
        fetchLoyaltyPointsForAccount();
        matchUpPointsAndRewards();

    }

    public static ILoyaltyAccountModel getLoyaltyAccountById(TerminalModel terminal, BigDecimal transactionValue, Integer accountId) throws Exception {
        AccountModel accountModel = AccountModel.getAccountModelByIdForTender(terminal, (transactionValue == null) ? BigDecimal.ZERO : transactionValue, accountId, new AccountQueryParams());
        accountModel.setTerminal(terminal);
        LoyaltyAccountModelWithBalances loyaltyAccountModel = new LoyaltyAccountModelWithBalances(accountModel);
        return loyaltyAccountModel;
    }

    public static ILoyaltyAccountModel getLoyaltyAccountByNumber(TerminalModel terminal, BigDecimal transactionValue, String accountNumber) throws Exception {
        AccountModel accountModel = AccountModel.getAccountModelByNumberForTender(terminal, (transactionValue == null) ? BigDecimal.ZERO : transactionValue, accountNumber, new AccountQueryParams());
        accountModel.setTerminal(terminal);
        LoyaltyAccountModelWithBalances loyaltyAccountModel = new LoyaltyAccountModelWithBalances(accountModel);
        return loyaltyAccountModel;
    }

    public static ILoyaltyAccountModel getLoyaltyAccountByBadge(TerminalModel terminal, BigDecimal transactionValue, String badgeNumber) throws Exception {
        AccountModel accountModel = AccountModel.getAccountModelByBadgeForTender(terminal, (transactionValue == null) ? BigDecimal.ZERO : transactionValue, badgeNumber, new AccountQueryParams());
        accountModel.setTerminal(terminal);
        LoyaltyAccountModelWithBalances loyaltyAccountModel = new LoyaltyAccountModelWithBalances(accountModel);
        return loyaltyAccountModel;
    }

    //get employee information and account data by Id Number and Terminal Id
    public static Integer getEmployeeIdByTransactionId(Integer transactionId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        Integer employeeId = null;

        //get all models in an array list
        ArrayList<HashMap> employeeList = dm.parameterizedExecuteQuery("data.posapi30.getOneEmployeeIdByTransactionId",
                new Object[]{transactionId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        if (employeeList.size() > 0) {
            employeeId = CommonAPI.convertModelDetailToInteger(employeeList.get(0).get("EMPLOYEEID"));
        }

        return employeeId;

    }

    public void fetchLoyaltyPointsForAccount() throws Exception {

        List<LoyaltyPointModel> loyaltyPoints = LoyaltyAccountModel.getLoyaltyPointsForAccount(this);
        this.setLoyaltyPoints(loyaltyPoints);
    }

    public void matchUpPointsAndRewards() {
        for (LoyaltyPointModel loyaltyPointModel : this.getLoyaltyPoints()) {
            for (LoyaltyRewardModel loyaltyRewardModel : this.getRewardsAvailable()) {
                if (loyaltyPointModel.getLoyaltyProgram().getId().equals(loyaltyRewardModel.getLoyaltyProgram().getId())) {
                    loyaltyPointModel.getRewardsAvailable().add(loyaltyRewardModel);
                }
            }
        }
    }

    //valid Loyalty Rewards for the Points on this account
    public void fetchLoyaltyRewardsForAccount() throws Exception {

        if (!hasFetchedAvailableRewards()) {
            this.setRewardsAvailable(LoyaltyAccountModel.getLoyaltyRewardsForAccount(this));
        }
        setHasFetchedAvailableRewards(true);
        //add available Rewards to the Loyalty Account
    }

    public void freezeAccount() throws Exception{
        super.freezeAccount();
    }

    public void unFreezeAccount() throws Exception{
        super.unFreezeAccount();
    }

    public List<LoyaltyPointModel> getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(List<LoyaltyPointModel> loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    @JsonIgnore
    public List<LoyaltyRewardModel> getRewardsAvailable() {
        return rewardsAvailable;
    }

    public void setRewardsAvailable(List<LoyaltyRewardModel> rewardsAvailable) {
        this.rewardsAvailable = rewardsAvailable;
    }

    @JsonIgnore
    public List<LoyaltyRewardLineItemModel> getRewardsAvailableForTransaction() {
        return rewardsAvailableForTransaction;
    }

    public void setRewardsAvailableForTransaction(List<LoyaltyRewardLineItemModel> rewardsAvailableForTransaction) {
        this.rewardsAvailableForTransaction = rewardsAvailableForTransaction;
    }

    public
    @Override
    String getStatus() {
        return super.getStatus();
    }

    public
    @Override
    BigDecimal getTerminalGroupBalance() {
        return super.getTerminalGroupBalance();
    }

    public
    @Override
    BigDecimal getGlobalBalance() {
        return super.getGlobalBalance();
    }

    public
    @Override
    Integer getEligiblePayments() {
        return super.getEligiblePayments();
    }

    public
    @Override
    BigDecimal getGlobalLimit() {
        return super.getGlobalLimit();
    }

    public
    @Override
    BigDecimal getSingleChargeLimit() {
        return super.getSingleChargeLimit();
    }

    public
    @Override
    BigDecimal getTerminalGroupLimit() {
        return super.getTerminalGroupLimit();
    }

    public
    @Override
    String getAddress1() {
        return super.getAddress1();
    }

    public
    @Override
    String getAddress2() {
        return super.getAddress2();
    }

    public
    @Override
    String getContact() {
        return super.getContact();
    }

    public
    @Override
    String getCity() {
        return super.getCity();
    }

    public
    @Override
    String getZipCode() {
        return super.getZipCode();
    }

    public
    @Override
    String getState() {
        return super.getState();
    }

    public
    @Override
    String getPhone() {
        return super.getPhone();
    }

    public
    @Override
    List<QcDiscountModel> getQcDiscountsAvailable() {
        return super.getQcDiscountsAvailable();
    }

    public
    @Override
    List<QcDiscountLineItemModel> getQcDiscountsApplied() {
        return super.getQcDiscountsApplied();
    }

    @JsonIgnore
    public boolean hasFetchedAvailableRewards() {
        return hasFetchedAvailableRewards;
    }

    public void setHasFetchedAvailableRewards(boolean hasFetchedAvailableRewards) {
        this.hasFetchedAvailableRewards = hasFetchedAvailableRewards;
    }
}
