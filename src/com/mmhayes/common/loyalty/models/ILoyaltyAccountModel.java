package com.mmhayes.common.loyalty.models;

//MMHayes Dependencies
import com.mmhayes.common.terminal.models.TerminalModel;

//Other Dependencies
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-08-29 11:12:59 -0400 (Wed, 29 Aug 2018) $: Date of last commit
 $Rev: 7701 $: Revision of last commit
*/
public interface ILoyaltyAccountModel {

    public Integer getId();
    public List<LoyaltyPointModel> getLoyaltyPoints();
    public TerminalModel getTerminal();
    public void setTerminal(TerminalModel terminal);
    public void flipBalanceSignsForPrePayAcct();
    public void freezeAccount() throws Exception;
    public void unFreezeAccount() throws Exception;
    public void getBalancesForEmployee(TerminalModel terminalModel) throws Exception;
}
