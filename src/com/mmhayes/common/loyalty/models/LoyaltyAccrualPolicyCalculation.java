package com.mmhayes.common.loyalty.models;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.loyalty.collections.LoyaltyAccrualPolicyLevelAscComparer;
import com.mmhayes.common.transaction.collections.ProductLineItemDescComparer;
import com.mmhayes.common.transaction.collections.ProductLineItemSequenceComparerAsc;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.Logger;

//other dependencies
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-03-05 16:38:27 -0500 (Mon, 05 Mar 2018) $: Date of last commit
 $Rev: 6540 $: Revision of last commit
*/
public class LoyaltyAccrualPolicyCalculation {

    private TransactionModel transactionModel = null;
    private LoyaltyProgramModel loyaltyProgramModel = null;
    private LoyaltyPointModel loyaltyPoint = null;

    private LoyaltyAccrualPolicyModel loyaltyAccrualPolicy = null;
    private PosAPIHelper.ObjectType objectType = PosAPIHelper.ObjectType.PRODUCT;
    private PosAPIHelper.LoyaltyLevelEarningType levelEarningTypeEnum = PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT; //This gets overidden in the constructor
    private Integer pointsResult = 0;  //the final point amount returned

    //enum types
    private PosAPIHelper.LoyaltyAccrualPolicyType accrualPolicyType;
    private PosAPIHelper.LoyaltyMonetaryRoundingType monetaryRoundingType;
    private PosAPIHelper.LoyaltyMonetaryPrecisionType monetaryPrecisionType;
    private PosAPIHelper.LoyaltyPointRoundingType pointRoundingType;

    private Integer monetaryRoundingScale = 0; //used to calculate monetary rounding
    private Integer monetaryRoundingValue = BigDecimal.ROUND_HALF_UP; //used to calculate monetary rounding, default to MMHAYES "STANDARD"
    private Integer pointRoundingScale = 0; ////used to calculate point rounding, default to a scale of 0
    private Integer pointRoundingValue = BigDecimal.ROUND_HALF_UP; //used to calculate monetary rounding, default to MMHAYES "STANDARD"

    private BigDecimal pointsWorkingTotal = BigDecimal.ZERO;
    private BigDecimal pointsWorkingTotalToUseMultiplier = BigDecimal.ZERO;
    private BigDecimal pointsWorkingTotalNoMultiplier = BigDecimal.ZERO;

    private BigDecimal totalValidTenderAmount = BigDecimal.ZERO;
    private BigDecimal totalTenderAmount = BigDecimal.ZERO;
    private BigDecimal validTenderPercentage = BigDecimal.ONE;

    //create Point Model for Product Line Item
    public LoyaltyAccrualPolicyCalculation(TransactionModel transactionModel, LoyaltyProgramModel loyaltyProgramModel, LoyaltyAccrualPolicyModel loyaltyAccrualPolicyModel) {
        Logger.logMessage("Entering LoyaltyAccrualPolicyCalculation", PosAPIHelper.getLogFileName(transactionModel.getTerminal().getId()), Logger.LEVEL.DEBUG);

        this.transactionModel = transactionModel;
        this.loyaltyProgramModel = loyaltyProgramModel;
        this.objectType = PosAPIHelper.ObjectType.PRODUCT;
        this.loyaltyAccrualPolicy = loyaltyAccrualPolicyModel;

        createLoyaltyPoint();
        mapLoyaltyPointCalculationFields();
        matchUpAccrualPolicyLevels();
        calculatePoints();
        updateLoyaltyPoint();
        cleanUpItemLoyaltyPoints();
    }

    private void mapLoyaltyPointCalculationFields() {
        try {

            this.setLoyaltyAccrualPolicyType(PosAPIHelper.LoyaltyAccrualPolicyType.intToLoyaltyAccrualPolicyType(loyaltyAccrualPolicy.getTypeId()));
            this.setMonetaryRoundingType(PosAPIHelper.LoyaltyMonetaryRoundingType.intToLoyaltyMonetaryRoundingType(loyaltyAccrualPolicy.getMonetaryRoundingTypeId()));
            this.setMonetaryPrecisionType(PosAPIHelper.LoyaltyMonetaryPrecisionType.intToLoyaltyMonetaryPrecisionType(loyaltyAccrualPolicy.getMonetaryRoundingPrecisionId()));
            this.setPointRoundingType(PosAPIHelper.LoyaltyPointRoundingType.intToLoyaltyPointRoundingType(loyaltyAccrualPolicy.getPointRoundingTypeId()));

            switch (this.getMonetaryRoundingType()) {
                case STANDARD:
                case HALF_UP:
                    monetaryRoundingValue = BigDecimal.ROUND_HALF_UP;
                    break;

                case UP:
                    monetaryRoundingValue = BigDecimal.ROUND_UP;
                    break;

                case DOWN:
                    monetaryRoundingValue = BigDecimal.ROUND_DOWN;
                    break;

                case HALF_DOWN:
                    monetaryRoundingValue = BigDecimal.ROUND_HALF_DOWN;
                    break;
            }

            switch (this.getMonetaryPrecisionType()) {
                case DOLLAR:
                    monetaryRoundingScale = 0;
                    break;

                case DIME:
                    monetaryRoundingScale = 1;
                    break;

                case PENNY:
                    monetaryRoundingScale = 2;
                    break;
            }

            switch (this.getPointRoundingType()) {
                case STANDARD:
                case HALF_UP:
                    pointRoundingValue = BigDecimal.ROUND_HALF_UP;
                    break;

                case UP:
                    pointRoundingValue = BigDecimal.ROUND_UP;
                    break;

                case DOWN:
                    pointRoundingValue = BigDecimal.ROUND_DOWN;
                    break;

                case HALF_DOWN:
                    pointRoundingValue = BigDecimal.ROUND_HALF_DOWN;
                    break;
            }

        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Added the Loyalty Accrual Policy Levels found on the ProductLineItemModel.Product on to the AccrualPolicyModel
     */
    private void matchUpAccrualPolicyLevels() {

        if (accrualPolicyType.equals(PosAPIHelper.LoyaltyAccrualPolicyType.PRODUCT)) {
            for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
                if (productLineItemModel.getProduct().getLoyaltyAccrualPolicyLevels().size() > 0) {
                    for (LoyaltyAccrualPolicyLevelModel loyaltyAccrualPolicyLevelModel : productLineItemModel.getProduct().getLoyaltyAccrualPolicyLevels()) {
                        if (this.loyaltyAccrualPolicy.getId().equals(loyaltyAccrualPolicyLevelModel.getLoyaltyAccrualPolicyId())) {
                            boolean alreadyAdded = false;
                            for (LoyaltyAccrualPolicyLevelModel accrualPolicyLevel : this.loyaltyAccrualPolicy.getLevelDetails()) {
                                if (accrualPolicyLevel.getId().equals(loyaltyAccrualPolicyLevelModel.getId())) {
                                    alreadyAdded = true;
                                    break;
                                }
                            }
                            if (!alreadyAdded) {
                                loyaltyAccrualPolicy.getLevelDetails().add(loyaltyAccrualPolicyLevelModel);
                            }
                        }
                    }
                }
            }
        }
    }

     /*create the Loyalty Points Earned
    these are saved in the QC_LoyaltyAccountPoint table*/
    //region Create Transaction Point records

    //calculate Loyalty Points depending on Loyalty Program, and transaction with products
    public void calculatePoints() {
        try {
            logTrace(" Program: " + loyaltyProgramModel.getName());
            logTrace("  Accrual Policy: " + loyaltyAccrualPolicy.getName() + ", Type: " + loyaltyAccrualPolicy.getTypeName());

            setTotalTenderAmount();
            setTotalValidTenderAmount();
            checkAndSetValidTenderPercentage();
            logAccrualPolicySettings();

            String percentageLog = "";
            if (this.getValidTenderPercentage().compareTo(BigDecimal.ONE) != 1) {
                percentageLog = " (" + this.getValidTenderPercentage().multiply(new BigDecimal(100)) + "% of Total Tender)";
            }
            logTrace("   Total valid Tender Amount: " + this.getTotalValidTenderAmount() + percentageLog);

            //only do the calculations if there is valid tender
            if (totalValidTenderAmount.abs().compareTo(BigDecimal.ZERO) == 1) {
                transactionModel.prepareProductLineAdjustedAmounts(loyaltyAccrualPolicy); //this sets the adjusted extended amount taking into account taxes, discounts, rewards

                //calculate the total amount of points for products in the transaction
                //calculations are different depending on the Loyalty Program Type
                switch (accrualPolicyType) {
                    case DOLLAR:
                        calculateDollarProgramPoints();
                        break;
                    case WELLNESS:
                        calculateWellnessProgramPoints();
                        break;
                    case PRODUCT:
                        calculateProductProgramPoints();
                        break;
                }

                logTrace("   Total Product Points: " + pointsWorkingTotal);

                switch (accrualPolicyType) {
                    case DOLLAR:
                    case WELLNESS:
                        applyMonetaryRounding(); //round the calculated points/monetary amount depending on the settings on the Loyalty Program
                        applyMultiplier();       //multiply points by the multiplier set on the Loyalty Program
                        applyPartialTenderProration();
                        applyPointRounding();    //round the calculated points depending on the settings on the Loyalty Program
                        break;
                    case PRODUCT:
                        applyMonetaryRoundingForProductLevel(); //round the calculated points/monetary amount depending on the settings on the Loyalty Program
                        applyMultiplierForProductLevel();
                        refreshPointsWorkingTotalForProductLevel();
                        applyPartialTenderProration();
                        applyPointRounding();    //round the calculated points depending on the settings on the Loyalty Program
                        createItemLoyaltyPointsForProductLevel();
                        break;
                }
            }

            pointsResult = pointsWorkingTotal.intValue(); //convert BigDecimal to Integer to return
            logTrace("   Total Points: " + pointsResult);

        } catch (Exception ex) {
            throw ex;
        }
    }

    //Calculate points for Programs of Type "Dollar"
    private void calculateDollarProgramPoints() {

        try {

            if (this.getTransactionModel().getProducts() != null && !this.getTransactionModel().getProducts().isEmpty()) {
                for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                    //quantity = 1

                    if (!this.productLineIsValidForPoints(productLineItemModel)) {
                        continue;
                    }

                    if (productLineItemModel.getQuantityForLoyalty().compareTo(BigDecimal.ONE) == 0) {
                        pointsWorkingTotal = pointsWorkingTotal.add(productLineItemModel.getAdjExtAmtLoyalty());
                        logTrace("   Product: " + productLineItemModel.getProduct().getName() + " calculated at " + productLineItemModel.getAdjExtAmtLoyalty());
                    } else {
                        //quantity <> 1
                        BigDecimal workingQuantity = BigDecimal.ZERO;
                        logTrace("   Product: " + productLineItemModel.getProduct().getName());

                        //loop through each of the quantities
                        while (workingQuantity.compareTo(productLineItemModel.getQuantity()) == -1) {
                            BigDecimal adjustedQuantity = BigDecimal.ONE;
                            //check if remainder is less than zero
                            if (productLineItemModel.getQuantity().subtract(workingQuantity).compareTo(BigDecimal.ONE) == -1) {
                                adjustedQuantity = productLineItemModel.getQuantity().subtract(workingQuantity);
                            }

                            pointsWorkingTotal = pointsWorkingTotal.add(productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                            workingQuantity = workingQuantity.add(adjustedQuantity);
                            logTrace("     Quantity: " + adjustedQuantity + " calculated at " + productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                        }
                    }

                    BigDecimal calculatedProductTotal = this.calculateProductPointTotal(productLineItemModel.getAdjExtAmtLoyalty());
                    ItemLoyaltyPointModel itemLoyaltyPointModel = this.createItemLoyaltyPointForProduct(productLineItemModel, calculatedProductTotal);
                    productLineItemModel.getLoyaltyPointDetails().add(itemLoyaltyPointModel);
                    loyaltyPoint.getPointDetails().add(itemLoyaltyPointModel);
                }
            } else {
                //there are no products
                //build points for the valid tender amount
                pointsWorkingTotal = totalValidTenderAmount;
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    //Calculate points for Programs of Type "Wellness"
    //The entire cost of the product must be covered for the program to earn points
    private void calculateWellnessProgramPoints() {

        this.resetWellnessApplied();

        this.orderProductsByIndex();

        try {
            PosAPIHelper.LoyaltyLevelEarningType levelEarningTypeEnum = PosAPIHelper.LoyaltyLevelEarningType.convertIntToEnum(loyaltyAccrualPolicy.getEarningTypeId());

            for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                if (productLineItemModel.getProduct() != null && productLineItemModel.getProduct().isWellness()) {

                    if (!this.productLineIsValidForPoints(productLineItemModel)) {
                        continue;
                    }

                    BigDecimal productTotalAmount = BigDecimal.ZERO;
                    if (productLineItemModel.getQuantityForLoyalty().compareTo(BigDecimal.ONE) == 0) {
                        switch (levelEarningTypeEnum) {
                            case POINT_PER_PRODUCT:
                                productTotalAmount = productTotalAmount.add(BigDecimal.ONE);
                                productLineItemModel.setWellnessAppliedToProduct(true);
                                logTrace("   Product: " + productLineItemModel.getProduct().getName() + " calculated at " + BigDecimal.ONE);
                                break;
                            case PRICE_OF_PRODUCT:
                                productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjExtAmtLoyalty());
                                productLineItemModel.setWellnessAppliedToProduct(true);
                                logTrace("   Product: " + productLineItemModel.getProduct().getName() + " calculated at " + productLineItemModel.getAdjExtAmtLoyalty());

                                break;
                        }
                        //if quantity does not equal 1
                    } else {
                        BigDecimal workingQuantity = BigDecimal.ZERO;

                        logTrace("   Product: " + productLineItemModel.getProduct().getName());

                        for (HashMap<BigDecimal, BigDecimal> quantityHM : productLineItemModel.getQuantityLineDetails()) {

                            //loop through each of the quantities
                            HashMap.Entry<BigDecimal, BigDecimal> entry = quantityHM.entrySet().iterator().next();
                            BigDecimal adjustedQuantity = entry.getKey();
                            BigDecimal pointPerProductAmount = entry.getValue();

                            if (levelEarningTypeEnum == PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT) {
                                if (pointPerProductAmount.compareTo(BigDecimal.ZERO) == 0) {
                                    logTrace("     Product Quantity: " + adjustedQuantity + " calculated at " + BigDecimal.ZERO);
                                    continue;
                                }
                            }

                            //get the weighted quantity, this is used to calculate arbitrary points, if rewards have already been assigned (Free Product, Transaction Credit)
                            workingQuantity = workingQuantity.add(adjustedQuantity);

                            switch (levelEarningTypeEnum) {
                                case POINT_PER_PRODUCT:
                                    BigDecimal calculatedProductAmount = BigDecimal.ONE;
                                    productTotalAmount = productTotalAmount.add(calculatedProductAmount);
                                    productLineItemModel.setWellnessAppliedToProduct(true);
                                    logTrace("     Quantity: " + adjustedQuantity + " calculated at " + calculatedProductAmount);
                                    break;
                                case PRICE_OF_PRODUCT:
                                    productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                    productLineItemModel.setWellnessAppliedToProduct(true);
                                    logTrace("     Quantity: " + adjustedQuantity + " calculated at " + productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                    break;
                            }
                        }
                    }

                    //Calculate the ItemLoyaltyPoint Value
                    if (productTotalAmount.compareTo(BigDecimal.ZERO) == 1) {
                        BigDecimal calculatedProductTotal = this.calculateProductPointTotal(productTotalAmount);
                        ItemLoyaltyPointModel itemLoyaltyPointModel = this.createItemLoyaltyPointForProduct(productLineItemModel, calculatedProductTotal);
                        productLineItemModel.getLoyaltyPointDetails().add(itemLoyaltyPointModel);
                        loyaltyPoint.getPointDetails().add(itemLoyaltyPointModel);
                    }

                    pointsWorkingTotal = pointsWorkingTotal.add(productTotalAmount);

                } else { //end of "isWellness" if
                    logTrace("  Product is not a Wellness Item.  No points earned: " + productLineItemModel.getProduct().getName());
                }
            } //end of product loop
        } catch (Exception ex) {
            throw ex;
        }
    }

    //Calculate points for Programs of Type "Product"
    //The entire cost of the product must be covered for the program to earn points
    private void calculateProductProgramPoints() {
        try {

            this.resetProgramLevelApplied();

            this.orderProductsByIndex();

            //sort the program levels by product, subdepartment, department
            Collections.sort(loyaltyAccrualPolicy.getLevelDetails(), new LoyaltyAccrualPolicyLevelAscComparer());

            if (loyaltyAccrualPolicy.getLevelDetails() == null || loyaltyAccrualPolicy.getLevelDetails().isEmpty()) {
                logTrace("   No Loyalty Accrual Levels Exist. No points earned.");
            }

            for (LoyaltyAccrualPolicyLevelModel loyaltyAccrualPolicyLevelModel : loyaltyAccrualPolicy.getLevelDetails()) {

                //keep track of level totals in order to enforce max points
                BigDecimal levelTotalPoints = new BigDecimal(0);
                boolean maxPerTransactionMet = false;

                loyaltyAccrualPolicyLevelModel.setProductLinesCalcList(new HashMap<>());
                loyaltyAccrualPolicyLevelModel.setTotalLevelPoints(BigDecimal.ZERO);
                loyaltyAccrualPolicyLevelModel.setPointCalculationLogList(new ArrayList<>());

                //check the revenue center for the Loyalty Program Level
                //if the Loyalty Program Level matches the revenue center, or the revenue center is null
                if ((transactionModel.getTerminal().getRevenueCenterId().equals(loyaltyAccrualPolicyLevelModel.getRevenueCenterId())) || (loyaltyAccrualPolicyLevelModel.getRevenueCenterId() == null)) {
                    for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {

                        if (maxPerTransactionMet || productLineItemModel.productLevelAppliedToProduct()) {
                            continue;
                        }

                        BigDecimal productTotalAmount = new BigDecimal(0);
                        PosAPIHelper.LoyaltyLevelItemType levelItemTypeEnum = PosAPIHelper.LoyaltyLevelItemType.valueOf(loyaltyAccrualPolicyLevelModel.getItemType().toUpperCase());
                        PosAPIHelper.LoyaltyLevelEarningType levelEarningTypeEnum = PosAPIHelper.LoyaltyLevelEarningType.convertIntToEnum(loyaltyAccrualPolicyLevelModel.getEarningTypeId());

                        //if level doesn't apply to product, skip over this product
                        if (!doesLevelApplyToProduct(levelItemTypeEnum, productLineItemModel, loyaltyAccrualPolicyLevelModel)) {
                            continue;
                        }

                        if (!this.productLineIsValidForPoints(productLineItemModel)) {
                            continue;
                        }

                        if (productLineItemModel.getQuantityForLoyalty().compareTo(BigDecimal.ONE) == 0) {
                            switch (levelItemTypeEnum) {
                                case PLU:
                                    //if the product originally only had the subdepartment or department, skip the plu
                                    if (!productLineItemModel.originallyJustSubDepartment() && !productLineItemModel.originallyJustDepartment()) {
                                        if (loyaltyAccrualPolicyLevelModel.getProductPlu().equalsIgnoreCase(productLineItemModel.getProduct().getProductCode())) {
                                            switch (levelEarningTypeEnum) {
                                                case POINT_PER_PRODUCT:
                                                    productTotalAmount = productTotalAmount.add(BigDecimal.valueOf(loyaltyAccrualPolicyLevelModel.getPoints()));
                                                    productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT);
                                                    loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   Product: " + productLineItemModel.getProduct().getName() + " calculated at: " + loyaltyAccrualPolicyLevelModel.getPoints() + " (" + levelEarningTypeEnum.toString() + ")");
                                                    break;
                                                case PRICE_OF_PRODUCT:
                                                    productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjExtAmtLoyalty());
                                                    productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT);
                                                    loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   Product: " + productLineItemModel.getProduct().getName() + " calculated at: " + productLineItemModel.getAdjExtAmtLoyalty() + " (" + levelEarningTypeEnum.toString() + ")");
                                                    break;
                                            }
                                        } else {
                                            loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   Product is not a set for this Product level.  This product did not earn points: " + productLineItemModel.getProduct().getName());
                                        }
                                    }
                                    break;
                                case DEPARTMENT:
                                    //if the product originally only had the department, skip the subdepartment
                                    if (!productLineItemModel.originallyJustSubDepartment()) {
                                        if (loyaltyAccrualPolicyLevelModel.getDepartmentName().equalsIgnoreCase(productLineItemModel.getProduct().getDepartment())) {
                                            switch (levelEarningTypeEnum) {
                                                case POINT_PER_PRODUCT:
                                                    productTotalAmount = productTotalAmount.add(BigDecimal.valueOf(loyaltyAccrualPolicyLevelModel.getPoints()));
                                                    productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT);
                                                    loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   Department: " + productLineItemModel.getProduct().getDepartment() + " calculated at: " + loyaltyAccrualPolicyLevelModel.getPoints() + productLineItemModel.getProduct().formatProductNameForLogging() + " (" + levelEarningTypeEnum.toString() + ")");
                                                    break;
                                                case PRICE_OF_PRODUCT:
                                                    productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjExtAmtLoyalty());
                                                    productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT);
                                                    loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   Department: " + productLineItemModel.getProduct().getDepartment() + "  calculated at: " + productLineItemModel.getAdjExtAmtLoyalty() + productLineItemModel.getProduct().formatProductNameForLogging() + " (" + levelEarningTypeEnum.toString() + ")");
                                                    break;
                                            }
                                        } else {
                                            loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   Product is not a set for this Product level.  This product did not earn points: " + productLineItemModel.getProduct().getName());
                                        }
                                    }
                                    break;
                                case SUBDEPARTMENT:
                                    //if the product originally only had the subdepartment, skip the department
                                    if (!productLineItemModel.originallyJustDepartment()) {
                                        if (loyaltyAccrualPolicyLevelModel.getSubDepartmentName().equalsIgnoreCase(productLineItemModel.getProduct().getSubDepartment())) {
                                            switch (levelEarningTypeEnum) {
                                                case POINT_PER_PRODUCT:
                                                    productTotalAmount = productTotalAmount.add(BigDecimal.valueOf(loyaltyAccrualPolicyLevelModel.getPoints()));
                                                    productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT);
                                                    loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   SubDepartment: " + productLineItemModel.getProduct().getSubDepartment() + " calculated at: " + loyaltyAccrualPolicyLevelModel.getPoints() + productLineItemModel.getProduct().formatProductNameForLogging() + " (" + levelEarningTypeEnum.toString() + ")");
                                                    break;
                                                case PRICE_OF_PRODUCT:
                                                    productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjExtAmtLoyalty());
                                                    productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT);
                                                    loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   SubDepartment: " + productLineItemModel.getProduct().getSubDepartment() + " calculated at: " + productLineItemModel.getAdjExtAmtLoyalty() + productLineItemModel.getProduct().formatProductNameForLogging() + " (" + levelEarningTypeEnum.toString() + ")");
                                                    break;
                                            }
                                        } else {
                                            loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   Product is not a set for this Product level.  This product did not earn points: " + productLineItemModel.getProduct().getName());
                                        }
                                    }
                                    break;
                            }
                        } else {   //the quantity did not equal zero, loop through each
                            loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   Product: " + productLineItemModel.getProduct().getName());

                            for (HashMap<BigDecimal, BigDecimal> quantityHM : productLineItemModel.getQuantityLineDetails()) {
                                HashMap.Entry<BigDecimal, BigDecimal> entry = quantityHM.entrySet().iterator().next();
                                BigDecimal adjustedQuantity = entry.getKey();
                                BigDecimal pointPerProductAmount = entry.getValue();

                                if (levelEarningTypeEnum == PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT) {
                                    if (pointPerProductAmount.compareTo(BigDecimal.ZERO) == 0) {
                                        loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("     Product Quantity: " + adjustedQuantity + " calculated at " + BigDecimal.ZERO + " (" + levelEarningTypeEnum.toString() + ")");
                                        continue;
                                    }
                                }

                                switch (levelItemTypeEnum) {
                                    case PLU:
                                        //if the product originally only had the subdepartment or department, skip the plu
                                        if (!productLineItemModel.originallyJustSubDepartment() && !productLineItemModel.originallyJustDepartment()) {
                                            if (loyaltyAccrualPolicyLevelModel.getProductPlu().equalsIgnoreCase(productLineItemModel.getProduct().getProductCode())) {
                                                switch (levelEarningTypeEnum) {
                                                    case POINT_PER_PRODUCT:
                                                        BigDecimal calculatedProductAmount = BigDecimal.valueOf(loyaltyAccrualPolicyLevelModel.getPoints());
                                                        productTotalAmount = productTotalAmount.add(calculatedProductAmount);
                                                        productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT);
                                                        loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("     Product Quantity: " + adjustedQuantity + " calculated at " + loyaltyAccrualPolicyLevelModel.getPoints() + " (" + levelEarningTypeEnum.toString() + ")");
                                                        break;
                                                    case PRICE_OF_PRODUCT:
                                                        productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                                        productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT);
                                                        loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("     Product Quantity: " + adjustedQuantity + " calculated at " + productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity) + " (" + levelEarningTypeEnum.toString() + ")");
                                                        break;
                                                }
                                            } else {
                                                loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   Product is not a set for this Product level.  This product did not earn points: " + productLineItemModel.getProduct().getName());
                                            }
                                        }
                                        break;
                                    case DEPARTMENT:
                                        //if the product originally only had the subdepartment, skip the department
                                        if (!productLineItemModel.originallyJustSubDepartment()) {
                                            if (loyaltyAccrualPolicyLevelModel.getDepartmentName().equalsIgnoreCase(productLineItemModel.getProduct().getDepartment())) {
                                                switch (levelEarningTypeEnum) {
                                                    case POINT_PER_PRODUCT:
                                                        BigDecimal calculatedProductAmount = BigDecimal.valueOf(loyaltyAccrualPolicyLevelModel.getPoints());
                                                        productTotalAmount = productTotalAmount.add(calculatedProductAmount);
                                                        productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT);
                                                        loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("     Department Quantity: " + adjustedQuantity + " calculated at " + BigDecimal.valueOf(loyaltyAccrualPolicyLevelModel.getPoints()) + productLineItemModel.getProduct().formatProductNameForLogging() + " (" + levelEarningTypeEnum.toString() + ")");
                                                        break;
                                                    case PRICE_OF_PRODUCT:
                                                        productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                                        productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT);
                                                        loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("     Department Quantity: " + adjustedQuantity + " calculated at " + productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity) + productLineItemModel.getProduct().formatProductNameForLogging() + " (" + levelEarningTypeEnum.toString() + ")");
                                                        break;
                                                }
                                            } else {
                                                loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   Product is not a set for this Product level.  This product did not earn points: " + productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity) + productLineItemModel.getProduct().formatProductNameForLogging() + " (" + levelEarningTypeEnum.toString() + ")");
                                            }
                                        }
                                        break;
                                    case SUBDEPARTMENT:
                                        //if the product originally only had the department, skip the subdepartment
                                        if (!productLineItemModel.originallyJustDepartment()) {
                                            if (loyaltyAccrualPolicyLevelModel.getSubDepartmentName().equalsIgnoreCase(productLineItemModel.getProduct().getSubDepartment())) {
                                                switch (levelEarningTypeEnum) {
                                                    case POINT_PER_PRODUCT:
                                                        BigDecimal calculatedProductAmount = BigDecimal.valueOf(loyaltyAccrualPolicyLevelModel.getPoints());
                                                        productTotalAmount = productTotalAmount.add(calculatedProductAmount);
                                                        productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT);
                                                        loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("     SubDepartment Quantity: " + adjustedQuantity + " calculated at " + BigDecimal.valueOf(loyaltyAccrualPolicyLevelModel.getPoints()) + productLineItemModel.getProduct().formatProductNameForLogging() + " (" + levelEarningTypeEnum.toString() + ")");
                                                        break;
                                                    case PRICE_OF_PRODUCT:
                                                        productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                                        productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT);
                                                        loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("     SubDepartment Quantity: " + adjustedQuantity + " calculated at " + productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity) + productLineItemModel.getProduct().formatProductNameForLogging() + " (" + levelEarningTypeEnum.toString() + ")");
                                                        break;
                                                }
                                            } else {
                                                loyaltyAccrualPolicyLevelModel.getPointCalculationLogList().add("   Product is not a set for this Product level.  This product did not earn points: " + productLineItemModel.getProduct().getName());
                                            }
                                        }
                                        break;
                                }
                            }
                        }

                        if (productTotalAmount.compareTo(BigDecimal.ZERO) == 0) {
                            continue; // if the points for this product is zero, continue to the next product
                        }

                        if (productTotalAmount.compareTo(BigDecimal.ZERO) == 1) {
                            Integer productIndex = transactionModel.getProducts().indexOf(productLineItemModel);
                            loyaltyAccrualPolicyLevelModel.getProductLinesCalcList().put(productIndex, productTotalAmount);
                            productLineItemModel.setProductLevelAppliedToProduct(true); //level applied to product, mark product so no other levels are applied to it
                        }

                        levelTotalPoints = levelTotalPoints.add(productTotalAmount);
                    }  //end of product loop
                } //end of revenue center check

                loyaltyAccrualPolicyLevelModel.setTotalLevelPoints(loyaltyAccrualPolicyLevelModel.getTotalLevelPoints().add(levelTotalPoints));

            } // end of Loyalty Accrual Policy Level loop

            //Go back through the levels; log product calculations and check for Max Per Transaction setting
            for (LoyaltyAccrualPolicyLevelModel loyaltyAccrualPolicyLevelModel : loyaltyAccrualPolicy.getLevelDetails()) {
                if (loyaltyAccrualPolicyLevelModel.getTotalLevelPoints() != null && loyaltyAccrualPolicyLevelModel.getTotalLevelPoints().compareTo(BigDecimal.ZERO) == 1) {
                    BigDecimal levelPointsCalculated = loyaltyAccrualPolicyLevelModel.getTotalLevelPoints();

                    for (String logMessage : loyaltyAccrualPolicyLevelModel.getPointCalculationLogList()) { //Log the product calculations from above
                        logTrace(logMessage);
                    }

                    //Check the earned points for the Max Per Transaction setting
                    boolean maxTransAmountApplied = false;
                    if (loyaltyAccrualPolicyLevelModel.getMaxPointsPerTransaction() != null && !loyaltyAccrualPolicyLevelModel.getMaxPointsPerTransaction().toString().isEmpty()) {
                        //Check Max Per Transaction
                        //add the product points to the level, check the max
                        BigDecimal tempMaxPoints = levelPointsCalculated;

                        tempMaxPoints = this.applyMonetaryRounding(tempMaxPoints);
                        if (loyaltyAccrualPolicyLevelModel.getEarningTypeId() == PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT.toInt()) {
                            tempMaxPoints = this.applyMultiplier(tempMaxPoints);
                        }
                        tempMaxPoints = this.applyPointRounding(tempMaxPoints);

                        if (tempMaxPoints.compareTo(loyaltyAccrualPolicyLevelModel.getMaxPointsPerTransaction()) == 1) { //if it's equal to or greater than
                            levelPointsCalculated = loyaltyAccrualPolicyLevelModel.getMaxPointsPerTransaction();
                            loyaltyAccrualPolicyLevelModel.setTotalLevelPoints(levelPointsCalculated); //update the calculated points for this program level
                            maxTransAmountApplied = true;
                            logTrace("    Max Per Transaction set, points earned set to : " + levelPointsCalculated);
                        }
                    } else {
                        logTrace("    Program Level points calculated at : " + levelPointsCalculated);
                    }

                    if (maxTransAmountApplied) {
                        pointsWorkingTotalNoMultiplier = pointsWorkingTotalNoMultiplier.add(levelPointsCalculated);
                    } else if (loyaltyAccrualPolicyLevelModel.getEarningTypeId() == PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT.toInt()) {
                        pointsWorkingTotalToUseMultiplier = pointsWorkingTotalToUseMultiplier.add(levelPointsCalculated);
                    } else {
                        pointsWorkingTotalNoMultiplier = pointsWorkingTotalNoMultiplier.add(levelPointsCalculated);
                    }
                }
            }

            refreshPointsWorkingTotalForProductLevel();

        } catch (Exception ex) {
            throw ex;
        }
    }

    //endregion

    //Set all the properties for the Loyalty Program so the calculation can be done

    /**
     * Used for 1 product
     *
     * @param amountToRound
     * @return
     */
    private BigDecimal applyMonetaryRounding(BigDecimal amountToRound) {
        return amountToRound.setScale(monetaryRoundingScale, monetaryRoundingValue);
    }

    /**
     * Used for 1 product
     *
     * @param amountToRound
     * @return
     */
    private BigDecimal applyMultiplier(BigDecimal amountToRound) {
        if (loyaltyAccrualPolicy.getPointsMultiplier() == null || loyaltyAccrualPolicy.getPointsMultiplier().toString().isEmpty()) {
            return amountToRound.multiply(BigDecimal.ONE);
        } else {
            return amountToRound.multiply(loyaltyAccrualPolicy.getPointsMultiplier());
        }
    }

    /**
     * Used for 1 product
     *
     * @param amountToRound
     * @return
     */
    private BigDecimal applyPointRounding(BigDecimal amountToRound) {
        return amountToRound.setScale(pointRoundingScale, pointRoundingValue);
    }

    private void applyMonetaryRounding() {
        pointsWorkingTotal = pointsWorkingTotal.setScale(monetaryRoundingScale, monetaryRoundingValue);
        if (pointsWorkingTotal.compareTo(BigDecimal.ZERO) == 1) {
            logTrace("   Total Points: " + pointsWorkingTotal + " after monetary rounding (" + monetaryRoundingType.toString() + ", " + monetaryPrecisionType.toString() + ")");
        }
    }

    private void applyMonetaryRoundingForProductLevel() {
        pointsWorkingTotalNoMultiplier = pointsWorkingTotalNoMultiplier.setScale(monetaryRoundingScale, monetaryRoundingValue);
        pointsWorkingTotalToUseMultiplier = pointsWorkingTotalToUseMultiplier.setScale(monetaryRoundingScale, monetaryRoundingValue);
        refreshPointsWorkingTotalForProductLevel();

        if (loyaltyAccrualPolicy.getPointsMultiplier() != null && loyaltyAccrualPolicy.getPointsMultiplier().compareTo(BigDecimal.ZERO) == 1) {
            if (pointsWorkingTotalNoMultiplier.compareTo(BigDecimal.ZERO) == 1 && pointsWorkingTotalToUseMultiplier.compareTo(BigDecimal.ZERO) == 1) {
                logTrace("     " + pointsWorkingTotalNoMultiplier + " will not get Multiplier");
                logTrace("     " + pointsWorkingTotalToUseMultiplier + " will have Multiplier applied (" + loyaltyAccrualPolicy.getPointsMultiplier() + ")");
            }
        } else {
            logTrace("    Total Points: " + pointsWorkingTotal + " after monetary rounding");
        }
    }

    //endregion

    private void applyMultiplier() {
        if (loyaltyAccrualPolicy.getPointsMultiplier() == null || loyaltyAccrualPolicy.getPointsMultiplier().toString().isEmpty()) {
            pointsWorkingTotal = pointsWorkingTotal.multiply(BigDecimal.ONE);
        } else {
            pointsWorkingTotal = pointsWorkingTotal.multiply(loyaltyAccrualPolicy.getPointsMultiplier());
            if (pointsWorkingTotal.compareTo(BigDecimal.ZERO) == 1) {
                logTrace("   Total Points: " + pointsWorkingTotal + " after multiplier (" + loyaltyAccrualPolicy.getPointsMultiplier().toString() + ")");
            }
        }
    }

    private void applyMultiplierForProductLevel() {
        if (loyaltyAccrualPolicy.getPointsMultiplier() == null || loyaltyAccrualPolicy.getPointsMultiplier().toString().isEmpty()) {
            pointsWorkingTotalNoMultiplier = pointsWorkingTotalNoMultiplier.multiply(BigDecimal.ONE);
            pointsWorkingTotalToUseMultiplier = pointsWorkingTotalToUseMultiplier.multiply(BigDecimal.ONE);
            refreshPointsWorkingTotalForProductLevel();
        } else {
            pointsWorkingTotalToUseMultiplier = pointsWorkingTotalToUseMultiplier.multiply(loyaltyAccrualPolicy.getPointsMultiplier());
            refreshPointsWorkingTotalForProductLevel();
            logTrace("    Total Points: " + pointsWorkingTotal + " after multiplier");
        }
    }

    private void applyPointRounding() {
        pointsWorkingTotal = pointsWorkingTotal.setScale(pointRoundingScale, pointRoundingValue);
        if (pointsWorkingTotal.compareTo(BigDecimal.ZERO) == 1) {
            logTrace("   Total Points: " + pointsWorkingTotal + " after Point rounding (" + pointRoundingType.toString() + ")");
        }
    }

    private void applyPartialTenderProration() {
        if (this.getValidTenderPercentage().compareTo(BigDecimal.ONE) != 0 && pointsWorkingTotal.compareTo(BigDecimal.ZERO) != 0) {
            pointsWorkingTotal = pointsWorkingTotal.multiply(this.getValidTenderPercentage());
            logTrace("   Total Points: " + pointsWorkingTotal + " (" + this.getValidTenderPercentage().multiply(new BigDecimal(100)) + "%) Calculated Points pro-rated, Partial Tender Transaction.");
        }
    }

    private BigDecimal applyPartialTenderProration(BigDecimal productPoints) {
        if (this.getValidTenderPercentage().compareTo(BigDecimal.ONE) != 0) {
            productPoints = productPoints.multiply(this.getValidTenderPercentage());
        }
        return productPoints;
    }

    private void setTotalValidTenderAmount() {
        totalValidTenderAmount = BigDecimal.ZERO;

        //first get the tender amount
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            for (TenderModel loyaltyTenderModel : loyaltyAccrualPolicy.getTenders()) {
                if (tenderLineItemModel.getTender().getId().equals(loyaltyTenderModel.getId())) {
                    totalValidTenderAmount = totalValidTenderAmount.add(tenderLineItemModel.getExtendedAmount());
                }
            }
        }

        totalValidTenderAmount = totalValidTenderAmount.abs();
        totalValidTenderAmount = totalValidTenderAmount.add(this.getMealPlanTenderAmount());

    }

    private void setTotalTenderAmount() {
        totalTenderAmount = BigDecimal.ZERO;

        //first get the tender amount
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            totalTenderAmount = totalTenderAmount.add(tenderLineItemModel.getExtendedAmount());
        }

        totalTenderAmount = totalTenderAmount.abs();
        totalTenderAmount = totalTenderAmount.add(this.getMealPlanTenderAmount());
    }


    private BigDecimal getMealPlanTenderAmount() {
        BigDecimal mealPlanDiscountTender = BigDecimal.ZERO;

        if (this.getLoyaltyAccrualPolicy().getEarnPointsForMealPlanProducts()) {
            for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
                if (discountLineItemModel.getDiscount().isMealPlan()) {
                    mealPlanDiscountTender = mealPlanDiscountTender.add(discountLineItemModel.getExtendedAmount().abs());
                }
            }
        }

        return mealPlanDiscountTender;
    }

    private void createLoyaltyPoint() {

        if (this.getLoyaltyPoint() == null) {
            this.setLoyaltyPoint(new LoyaltyPointModel());
        }

        this.loyaltyPoint.setLoyaltyProgram(loyaltyProgramModel);
        this.loyaltyPoint.setLoyaltyAccrualPolicy(loyaltyAccrualPolicy);
        this.loyaltyPoint.setEmployeeId(transactionModel.getLoyaltyAccount().getId());
        this.loyaltyPoint.setPoints(pointsResult);
    }

    private void updateLoyaltyPoint() {
        this.loyaltyPoint.setPoints(pointsResult);
    }

    //get the weighted amount taking into account the earned Loyalty Account Points, Extended cost of the product, and total valid product amount from the transaction
    private BigDecimal getWeightedPointValue(BigDecimal pointResult, BigDecimal productExtendedCost, BigDecimal totalProductAmount) {

        BigDecimal weightedPointValue = BigDecimal.ZERO;
        BigDecimal productPercentageOfTotal = BigDecimal.ZERO;
        BigDecimal oneHundred = new BigDecimal(100);
        //figure out the point amount on the weighted average
        //e.g.:  If all products = $10, and this product amount is $2.00, the percentage is %20
        //this will calculate the point details if only a portion of the tenders are valid

        BigDecimal step1 = productExtendedCost.multiply(oneHundred);

        //check for a Zero totalProductAmount
        BigDecimal step2 = BigDecimal.ZERO;
        if (totalProductAmount.compareTo(BigDecimal.ZERO) > 0) {
            step2 = step1.divide(totalProductAmount, 10, BigDecimal.ROUND_HALF_UP);   //round this to 10 places to get more accurate values
        }

        productPercentageOfTotal = step2.divide(oneHundred);
        weightedPointValue = pointResult.multiply(productPercentageOfTotal);
        weightedPointValue = weightedPointValue.setScale(4, RoundingMode.HALF_UP);  //round it back to 2 places so the it appears as a money value

        return weightedPointValue;
    }

    private boolean doesLevelApplyToProduct(PosAPIHelper.LoyaltyLevelItemType levelItemTypeEnum, ProductLineItemModel productLineItemModel, LoyaltyAccrualPolicyLevelModel loyaltyAccrualPolicyLevelModel) {
        Boolean levelAppliesToProduct = false;
        switch (levelItemTypeEnum) {
            case PLU:
                //if the product originally only had the subdepartment or department, skip the plu
                if (!productLineItemModel.originallyJustSubDepartment() && !productLineItemModel.originallyJustDepartment()) {
                    if (loyaltyAccrualPolicyLevelModel.getProductPlu().equalsIgnoreCase(productLineItemModel.getProduct().getProductCode())) {
                        levelAppliesToProduct = true;
                    }
                }
                break;
            case DEPARTMENT:
                //if the product originally only had the subdepartment, skip the department
                if (!productLineItemModel.originallyJustSubDepartment()) {
                    if (loyaltyAccrualPolicyLevelModel.getDepartmentName().equalsIgnoreCase(productLineItemModel.getProduct().getDepartment())) {
                        levelAppliesToProduct = true;
                    }
                }
                break;
            case SUBDEPARTMENT:
                //if the product originally only had the department, skip the subdepartment
                if (!productLineItemModel.originallyJustDepartment()) {
                    if (loyaltyAccrualPolicyLevelModel.getSubDepartmentName().equalsIgnoreCase(productLineItemModel.getProduct().getSubDepartment())) {
                        levelAppliesToProduct = true;
                    }
                }
                break;
        }

        return levelAppliesToProduct;
    }

    private boolean productLineIsValidForPoints(ProductLineItemModel productLineItemModel) {
        boolean result = false;

        if (!this.getLoyaltyAccrualPolicy().getEarnPointsForDiscountedProducts() && productLineItemModel.hasDiscountApplied()) {
            return result;
        }

        if (!this.getLoyaltyAccrualPolicy().getEarnPointsForMealPlanProducts() && productLineItemModel.hasMealPlanApplied()) {
            return result;
        }

        if (productLineItemModel.getProduct().isDiningOption() && productLineItemModel.getExtendedAmount().compareTo(BigDecimal.ZERO) == 0) {
            //HP 2060 - Allow points to be created for $0 products.  Dining Options - Eat In.
            result = true;
        } else if (productLineItemModel.getAdjExtAmtLoyalty().compareTo(BigDecimal.ZERO) == 0) {
            result = false;
        } else {
            result = true;
        }

        return result;
    }

    private void logAccrualPolicySettings() {
        if (!this.getLoyaltyAccrualPolicy().getEarnPointsForDiscountedProducts()) {
            logTrace("    Earn Points for Discounted Products = false");
        }

        if (!this.getLoyaltyAccrualPolicy().getEarnPointsForMealPlanProducts()) {
            logTrace("    Earn Points for Meal Plan Products = false");
        }
    }

    private void logDebug(String message) {
        Logger.logMessage(message, PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.DEBUG);
    }

    private void logTrace(String message) {
        Logger.logMessage(message, PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.TRACE);
    }

    private ItemLoyaltyPointModel createItemLoyaltyPointForProduct(ProductLineItemModel productLineItemModel, BigDecimal productPoints) {
        ItemLoyaltyPointModel itemLoyaltyPointModel = new ItemLoyaltyPointModel();
        itemLoyaltyPointModel.setPoints(productPoints);
        itemLoyaltyPointModel.setEligibleAmount(productLineItemModel.getAdjExtAmtLoyalty());
        itemLoyaltyPointModel.setLoyaltyProgramId(this.getLoyaltyProgramModel().getId());
        itemLoyaltyPointModel.setLoyaltyAccrualPolicyId(this.getLoyaltyAccrualPolicy().getId());
        return itemLoyaltyPointModel;
    }

    private BigDecimal calculateProductPointTotal(BigDecimal productPoints) {
        BigDecimal calculatedTotal = productPoints;
        calculatedTotal = applyMonetaryRounding(calculatedTotal); //round the calculated points/monetary amount depending on the settings on the Loyalty Program
        calculatedTotal = applyMultiplier(calculatedTotal);       //multiply points by the multiplier set on the Loyalty Program
        calculatedTotal = applyPartialTenderProration(calculatedTotal);
        return calculatedTotal;
    }

    private void createItemLoyaltyPointsForProductLevel() {
        HashMap<Integer, BigDecimal> productLineTotals = new HashMap<>();
        BigDecimal totalProductPoints = BigDecimal.ZERO;

        //Tally up all the calculate points per product line for the entire Program
        for (LoyaltyAccrualPolicyLevelModel loyaltyAccrualPolicyLevelModel : loyaltyAccrualPolicy.getLevelDetails()) {
            for (Map.Entry<Integer, BigDecimal> entry : loyaltyAccrualPolicyLevelModel.getProductLinesCalcList().entrySet()) {
                Integer productIndex = entry.getKey();
                BigDecimal productPoints = entry.getValue();

                if (productPoints.compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }

                if (productLineTotals.containsKey(productIndex)) {
                    BigDecimal productTotal = productLineTotals.get(productIndex);
                    productLineTotals.put(productIndex, productTotal.add(productPoints));
                    totalProductPoints = totalProductPoints.add(productPoints);

                } else {
                    productLineTotals.put(productIndex, productPoints);
                    totalProductPoints = totalProductPoints.add(productPoints);
                }
            }
        }

        //Create the Item Loyalty Points, calculate the weighted amount of the product.  Add the Item Loyalty Point to the Product Line Item
        for (Map.Entry<Integer, BigDecimal> entry : productLineTotals.entrySet()) {
            Integer productIndex = entry.getKey();
            BigDecimal productPoints = entry.getValue();
            BigDecimal productPercentageOfTotal = BigDecimal.ONE;
            if (totalProductPoints.compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }

            productPercentageOfTotal = productPoints.divide(totalProductPoints, 4, RoundingMode.HALF_UP);
            BigDecimal itemPointAmount = pointsWorkingTotal.multiply(productPercentageOfTotal);

            if (itemPointAmount.compareTo(BigDecimal.ZERO) == 1) {
                ProductLineItemModel productLineItemModel = this.getTransactionModel().getProducts().get(productIndex);
                ItemLoyaltyPointModel itemLoyaltyPointModel = this.createItemLoyaltyPointForProduct(productLineItemModel, itemPointAmount);
                productLineItemModel.getLoyaltyPointDetails().add(itemLoyaltyPointModel);
                loyaltyPoint.getPointDetails().add(itemLoyaltyPointModel);
            }
        }
    }

    public PosAPIHelper.LoyaltyMonetaryPrecisionType convertStringPrecisionType(String monetaryPrecisionTypeString) {

        switch (monetaryPrecisionTypeString.toUpperCase()) {
            case "DIMES":
                return PosAPIHelper.LoyaltyMonetaryPrecisionType.DIME;
            case "PENNIES":
                return PosAPIHelper.LoyaltyMonetaryPrecisionType.PENNY;
            default:
                return PosAPIHelper.LoyaltyMonetaryPrecisionType.DOLLAR;
        }
    }

    private void resetProgramLevelApplied() {
        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
            productLineItemModel.setProductLevelAppliedToProduct(false);
        }
    }

    private void resetWellnessApplied() {
        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
            productLineItemModel.setWellnessAppliedToProduct(false);
        }
    }

    private void orderProductsDesc() {
        Collections.sort(transactionModel.getProducts(), new ProductLineItemDescComparer());
    }

    private void checkAndSetValidTenderPercentage() {
        if (totalTenderAmount.compareTo(totalValidTenderAmount) != 0) {
            this.setValidTenderPercentage(totalValidTenderAmount.divide(totalTenderAmount, 4, BigDecimal.ROUND_HALF_UP));
        }
    }

    /**
     * If Item Loyalty Points were created, but no Loyalty Points were calculated, remove the Item Loyalty Points off of the Products and/or Loyalty Point (QC_LoyaltyAccountPoint)
     */
    private void cleanUpItemLoyaltyPoints() {
        if (pointsResult.equals(0)) {
            this.getLoyaltyPoint().getPointDetails().clear();
            for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                Iterator<ItemLoyaltyPointModel> removeItemLoyaltyPoint = productLineItemModel.getLoyaltyPointDetails().iterator();
                while (removeItemLoyaltyPoint.hasNext()) {
                    ItemLoyaltyPointModel curItemLoyaltyPointModel = removeItemLoyaltyPoint.next();
                    if (curItemLoyaltyPointModel.getRewardGuid() != null) {
                        continue;
                    }
                    if (this.getLoyaltyAccrualPolicy().getId().equals(curItemLoyaltyPointModel.getLoyaltyAccrualPolicyId())) {
                        removeItemLoyaltyPoint.remove();
                    }
                }
            }
        }
    }

    private void refreshPointsWorkingTotalForProductLevel() {
        pointsWorkingTotal = pointsWorkingTotalNoMultiplier.add(pointsWorkingTotalToUseMultiplier);
    }

    private void orderProductsByIndex() {
        Collections.sort(transactionModel.getProducts(), new ProductLineItemSequenceComparerAsc());
    }

    public TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    public LoyaltyAccrualPolicyModel getLoyaltyAccrualPolicy() {
        return loyaltyAccrualPolicy;
    }

    public void setLoyaltyAccrualPolicy(LoyaltyAccrualPolicyModel loyaltyAccrualPolicy) {
        this.loyaltyAccrualPolicy = loyaltyAccrualPolicy;
    }

    public PosAPIHelper.LoyaltyAccrualPolicyType getLoyaltyAccrualPolicyType() {
        return accrualPolicyType;
    }

    public void setLoyaltyAccrualPolicyType(PosAPIHelper.LoyaltyAccrualPolicyType accrualPolicyType) {
        this.accrualPolicyType = accrualPolicyType;
    }

    public PosAPIHelper.LoyaltyMonetaryRoundingType getMonetaryRoundingType() {
        return monetaryRoundingType;
    }

    public void setMonetaryRoundingType(PosAPIHelper.LoyaltyMonetaryRoundingType monetaryRoundingType) {
        this.monetaryRoundingType = monetaryRoundingType;
    }

    public PosAPIHelper.LoyaltyMonetaryPrecisionType getMonetaryPrecisionType() {
        return monetaryPrecisionType;
    }

    public void setMonetaryPrecisionType(PosAPIHelper.LoyaltyMonetaryPrecisionType monetaryPrecisionType) {
        this.monetaryPrecisionType = monetaryPrecisionType;
    }

    public PosAPIHelper.LoyaltyPointRoundingType getPointRoundingType() {
        return pointRoundingType;
    }

    public void setPointRoundingType(PosAPIHelper.LoyaltyPointRoundingType pointRoundingType) {
        this.pointRoundingType = pointRoundingType;
    }

    public Integer getMonetaryRoundingScale() {
        return monetaryRoundingScale;
    }

    public void setMonetaryRoundingScale(Integer monetaryRoundingScale) {
        this.monetaryRoundingScale = monetaryRoundingScale;
    }

    public Integer getMonetaryRoundingValue() {
        return monetaryRoundingValue;
    }

    public void setMonetaryRoundingValue(Integer monetaryRoundingValue) {
        this.monetaryRoundingValue = monetaryRoundingValue;
    }

    public Integer getPointRoundingScale() {
        return pointRoundingScale;
    }

    public void setPointRoundingScale(Integer pointRoundingScale) {
        this.pointRoundingScale = pointRoundingScale;
    }

    public Integer getPointRoundingValue() {
        return pointRoundingValue;
    }

    public void setPointRoundingValue(Integer pointRoundingValue) {
        this.pointRoundingValue = pointRoundingValue;
    }

    public PosAPIHelper.ObjectType getObjectType() {
        return objectType;
    }

    public void setObjectType(PosAPIHelper.ObjectType objectType) {
        this.objectType = objectType;
    }

    public Integer getPointsResult() {
        return pointsResult;
    }

    public void setPointsResult(Integer pointsResult) {
        this.pointsResult = pointsResult;
    }

    public LoyaltyProgramModel getLoyaltyProgramModel() {
        return loyaltyProgramModel;
    }

    public void setLoyaltyProgramModel(LoyaltyProgramModel loyaltyProgramModel) {
        this.loyaltyProgramModel = loyaltyProgramModel;
    }

    public LoyaltyPointModel getLoyaltyPoint() {
        return loyaltyPoint;
    }

    public void setLoyaltyPoint(LoyaltyPointModel loyaltyPoint) {
        this.loyaltyPoint = loyaltyPoint;
    }

    public BigDecimal getPointsWorkingTotal() {
        return pointsWorkingTotal;
    }

    public void setPointsWorkingTotal(BigDecimal pointsWorkingTotal) {
        this.pointsWorkingTotal = pointsWorkingTotal;
    }

    public BigDecimal getTotalTenderAmount() {
        return totalTenderAmount;
    }

    public void setTotalTenderAmount(BigDecimal totalTenderAmount) {
        this.totalTenderAmount = totalTenderAmount;
    }

    public BigDecimal getTotalValidTenderAmount() {
        return totalValidTenderAmount;
    }

    public void setTotalValidTenderAmount(BigDecimal totalValidTenderAmount) {
        this.totalValidTenderAmount = totalValidTenderAmount;
    }

    public BigDecimal getValidTenderPercentage() {
        return validTenderPercentage;
    }

    public void setValidTenderPercentage(BigDecimal validTenderPercentage) {
        this.validTenderPercentage = validTenderPercentage;
    }

}
