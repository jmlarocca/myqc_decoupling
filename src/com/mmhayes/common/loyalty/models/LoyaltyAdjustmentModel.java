package com.mmhayes.common.loyalty.models;

// API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;

// Other dependencies
import java.util.HashMap;

public class LoyaltyAdjustmentModel {

    private LoyaltyProgramModel loyaltyProgram;
    private Integer points;

    public LoyaltyAdjustmentModel() {

    }

    public LoyaltyAdjustmentModel(HashMap loyaltyAdjustmentHM) {
        setModelProperties(loyaltyAdjustmentHM);
    }

    public LoyaltyAdjustmentModel(HashMap loyaltyAdjustmentHM, Integer terminalID) throws Exception {
        setModelProperties(loyaltyAdjustmentHM, terminalID);
    }

    public LoyaltyAdjustmentModel setModelProperties(HashMap modelDetailHM) {

        setPoints(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POINTS")));

        return this;
    }

    public LoyaltyAdjustmentModel setModelProperties(HashMap modelDetailHM, Integer terminalId) throws Exception {

        setModelProperties(modelDetailHM);

        if (terminalId == null || !PosAPIModelCache.useTerminalCache) {
            if (this.getLoyaltyProgram() == null) {
                if (CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")) != null) {
                    setLoyaltyProgram(LoyaltyProgramModel.getOneLoyaltyProgram(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")), terminalId));
                }
            }
        }
        else if (PosAPIModelCache.useTerminalCache) {
            if (this.getLoyaltyProgram() == null) {
                if (CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")) != null) {
                    LoyaltyProgramModel validatedLoyaltyProgram = null;
                    validatedLoyaltyProgram = (LoyaltyProgramModel) PosAPIModelCache.getOneObject(terminalId, CommonAPI.PosModelType.LOYALTY_PROGRAM, CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")));
                    if (validatedLoyaltyProgram != null) {
                        setLoyaltyProgram(validatedLoyaltyProgram);
                    } else {
                        throw new MissingDataException("Invalid Loyalty Program", terminalId);
                    }
                }
            }
        }

        return this;
    }

    public LoyaltyProgramModel getLoyaltyProgram() {
        return loyaltyProgram;
    }

    public void setLoyaltyProgram(LoyaltyProgramModel loyaltyProgram) {
        this.loyaltyProgram = loyaltyProgram;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

}
