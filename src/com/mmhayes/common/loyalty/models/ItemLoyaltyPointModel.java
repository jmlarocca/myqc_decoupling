package com.mmhayes.common.loyalty.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-31 16:39:34 -0500 (Thu, 31 Dec 2020) $: Date of last commit
 $Rev: 13381 $: Revision of last commit
*/
public class ItemLoyaltyPointModel {
    private Integer id;
    private Integer loyaltyProgramId;
    private BigDecimal points = BigDecimal.ZERO;
    private BigDecimal eligibleAmount = null;
    private BigDecimal refundedPoints = null;
    private BigDecimal refundedEligibleAmount = null;
    private BigDecimal originalPoints = BigDecimal.ZERO;
    private BigDecimal originalEligibleAmount = BigDecimal.ZERO;
    private Integer transLineItemId;
    private Integer loyaltyAccountPointId;
    private Integer loyaltyAccrualPolicyId = null;

    private UUID rewardGuid = null;
    private PosAPIHelper.LoyaltyRewardType loyaltyRewardType = null;

    public ItemLoyaltyPointModel(){

    }

    public ItemLoyaltyPointModel(HashMap itemLoyaltyPointHM){
        setModelProperties(itemLoyaltyPointHM);
    }

    //setter for all of this model's properties
    public ItemLoyaltyPointModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setLoyaltyAccountPointId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYACCOUNTPOINTID")));
        setLoyaltyProgramId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")));
        setTransLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSLINEITEMID")));
        setEligibleAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("ELIGIBLEAMOUNT")));
        setPoints(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("POINTS")));
        setRefundedEligibleAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDELIGIBLEAMOUNT")));
        setRefundedPoints(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REFUNDEDPOINTS")));

        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLoyaltyProgramId() {
        return loyaltyProgramId;
    }

    public void setLoyaltyProgramId(Integer loyaltyProgramId) {
        this.loyaltyProgramId = loyaltyProgramId;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    public Integer getTransLineItemId() {
        return transLineItemId;
    }

    public void setTransLineItemId(Integer transLineItemId) {
        this.transLineItemId = transLineItemId;
    }

    public Integer getLoyaltyAccountPointId() {
        return loyaltyAccountPointId;
    }

    public void setLoyaltyAccountPointId(Integer loyaltyAccountPointId) {
        this.loyaltyAccountPointId = loyaltyAccountPointId;
    }

    public BigDecimal getEligibleAmount() {

        if (eligibleAmount == null){
            eligibleAmount = BigDecimal.ZERO;
        }

        return eligibleAmount;
    }

    public void setEligibleAmount(BigDecimal eligibleAmount) {
        this.eligibleAmount = eligibleAmount;
    }

    public BigDecimal getRefundedPoints() {

        if (refundedPoints == null){
            refundedPoints = BigDecimal.ZERO;
        }

        return refundedPoints;
    }

    public void setRefundedPoints(BigDecimal refundedPoints) {
        this.refundedPoints = refundedPoints;
    }

    public BigDecimal getRefundedEligibleAmount() {

        if (refundedEligibleAmount == null){
            refundedEligibleAmount = BigDecimal.ZERO;
        }

        return refundedEligibleAmount;
    }

    public void setRefundedEligibleAmount(BigDecimal refundedEligibleAmount) {
        this.refundedEligibleAmount = refundedEligibleAmount;
    }

    @JsonIgnore
    public UUID getRewardGuid() {
        return rewardGuid;
    }

    public void setRewardGuid(UUID rewardGuid) {
        this.rewardGuid = rewardGuid;
    }

    @JsonIgnore
    public PosAPIHelper.LoyaltyRewardType getLoyaltyRewardType() {
        return loyaltyRewardType;
    }

    public void setLoyaltyRewardType(PosAPIHelper.LoyaltyRewardType loyaltyRewardType) {
        this.loyaltyRewardType = loyaltyRewardType;
    }

    @JsonIgnore
    public BigDecimal getOriginalEligibleAmount() {
        return originalEligibleAmount;
    }

    public void setOriginalEligibleAmount(BigDecimal originalEligibleAmount) {
        this.originalEligibleAmount = originalEligibleAmount;
    }

    @JsonIgnore
    public BigDecimal getOriginalPoints() {
        return originalPoints;
    }

    public void setOriginalPoints(BigDecimal originalPoints) {
        this.originalPoints = originalPoints;
    }

    @JsonIgnore
    public Integer getLoyaltyAccrualPolicyId() {
        return loyaltyAccrualPolicyId;
    }

    public void setLoyaltyAccrualPolicyId(Integer loyaltyAccrualPolicyId) {
        this.loyaltyAccrualPolicyId = loyaltyAccrualPolicyId;
    }

    /**
     * Overridden toString method for an ItemLoyaltyPointModel.
     * @return The {@link String} representation of an ItemLoyaltyPointModel.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, LOYALTYPROGRAMID: %s, POINTS: %s, ELIGIBLEAMOUNT: %s, REFUNDEDPOINTS: %s, " +
                "REFUNDEDELIGIBLEAMOUNT: %s, ORIGINALPOINTS: %s, ORIGINALELIGIBLEAMOUNT: %s, TRANSLINEITEMID: %s, " +
                "LOYALTYACCOUNTPOINTID: %s, LOYALTYACCRUALPOLICYID: %s, REWARDGUID: %s, LOYALTYREWARDTYPE: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((loyaltyProgramId != null && loyaltyProgramId > 0 ? loyaltyProgramId : "N/A"), "N/A"),
                Objects.toString((points != null ? points.toPlainString() : "N/A"), "N/A"),
                Objects.toString((eligibleAmount != null ? eligibleAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((refundedPoints != null ? refundedPoints.toPlainString() : "N/A"), "N/A"),
                Objects.toString((refundedEligibleAmount != null ? refundedEligibleAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((originalPoints != null ? originalPoints.toPlainString() : "N/A"), "N/A"),
                Objects.toString((originalEligibleAmount != null ? originalEligibleAmount.toPlainString() : "N/A"), "N/A"),
                Objects.toString((transLineItemId != null && transLineItemId > 0 ? transLineItemId : "N/A"), "N/A"),
                Objects.toString((loyaltyAccountPointId != null && loyaltyAccountPointId > 0 ? loyaltyAccountPointId : "N/A"), "N/A"),
                Objects.toString((loyaltyAccrualPolicyId != null && loyaltyAccrualPolicyId > 0 ? loyaltyAccrualPolicyId : "N/A"), "N/A"),
                Objects.toString((rewardGuid != null ? rewardGuid.toString() : "N/A"), "N/A"),
                Objects.toString((loyaltyRewardType != null ? loyaltyRewardType.toStringName() : "N/A"), "N/A"));

    }

}
