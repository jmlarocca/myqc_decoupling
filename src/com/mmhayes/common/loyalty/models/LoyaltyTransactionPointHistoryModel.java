package com.mmhayes.common.loyalty.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.commonMMHFunctions;

import java.util.HashMap;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-06-12 12:53:50 -0400 (Wed, 12 Jun 2019) $: Date of last commit
 $Rev: 8963 $: Revision of last commit
*/
public class LoyaltyTransactionPointHistoryModel {
    private Integer id = null; //this ID is representing a PATransactionID
    private Integer points = null;
    private String storeName = null;
    private String revCenterName = null;
    private String timeStamp = null;
    private boolean redeemedReward = false;
    private boolean showReceiptsOnMyQC = false;
    private boolean enableFavorites = false;
    private Integer PAOrderTypeID = null;

    @JsonIgnore
    private commonMMHFunctions commFunc = new commonMMHFunctions();

    public LoyaltyTransactionPointHistoryModel() {

    }

    //Constructor- takes in a Hashmap
    public LoyaltyTransactionPointHistoryModel(HashMap ProductHM) throws Exception {
        setModelProperties(ProductHM);
    }

    //setter for all of this model's properties
    public LoyaltyTransactionPointHistoryModel setModelProperties(HashMap modelDetailHM) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSACTIONID")));
        setPAOrderTypeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAORDERTYPEID")));
        setPoints(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POINTS")));
        setStoreName(CommonAPI.convertModelDetailToString(modelDetailHM.get("STORENAME")));
        setRevCenterName(CommonAPI.convertModelDetailToString(modelDetailHM.get("REVCENTERNAME")));
        setTimeStamp(CommonAPI.convertModelDetailToString(modelDetailHM.get("TIMESTAMP")));

        if ( CommonAPI.convertModelDetailToInteger(modelDetailHM.get("REDEEMEDREWARD")) > 0 ) {
            setRedeemedReward(true);
        }

        if ( CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWRECEIPTSONMYQC")) ) {
            setShowReceiptsOnMyQC(true);
        }

        if ( modelDetailHM.get("ENABLEFAVORITES") != null && !modelDetailHM.get("ENABLEFAVORITES").toString().equals("") ) {
            setEnableFavorites(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ENABLEFAVORITES")));
        }

        return this;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isRedeemedReward() {
        return redeemedReward;
    }

    public void setRedeemedReward(boolean redeemedReward) {
        this.redeemedReward = redeemedReward;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        //specify in and out formats for formatting
        String dateTimeInFormat = "yyyy-MM-dd HH:mm:ss.S";
        String dateTimeOutFormat = "M/dd/yyyy h:mm a"; //old way was - M/dd/yyyy H:mm:ss - jrmitaly 6/23/2015

        //format timeStamp and set
        this.timeStamp = commFunc.formatDateTime(timeStamp,dateTimeInFormat,dateTimeOutFormat);
    }

    public Integer getPAOrderTypeID() {
        return PAOrderTypeID;
    }

    public void setPAOrderTypeID(Integer PAOrderTypeID) {
        this.PAOrderTypeID = PAOrderTypeID;
    }

    public boolean isShowReceiptsOnMyQC() {
        return showReceiptsOnMyQC;
    }

    public void setShowReceiptsOnMyQC(boolean showReceiptsOnMyQC) {
        this.showReceiptsOnMyQC = showReceiptsOnMyQC;
    }

    public String getRevCenterName() {
        return revCenterName;
    }

    public void setRevCenterName(String revCenterName) {
        this.revCenterName = revCenterName;
    }

    public boolean isEnableFavorites() {
        return enableFavorites;
    }

    public void setEnableFavorites(boolean enableFavorites) {
        this.enableFavorites = enableFavorites;
    }
}
