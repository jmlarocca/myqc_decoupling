package com.mmhayes.common.loyalty.models;

//mmhayes dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.transaction.collections.*;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.Logger;

//API dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

//other dependencies
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.HashMap;

/*
 $Author: eglundin $: Author of last commit
 $Date: 2021-03-11 09:58:48 -0500 (Thu, 11 Mar 2021) $: Date of last commit
 $Rev: 13619 $: Revision of last commit
 Notes:
*/
public class LoyaltyRewardCalculation { //

    private LoyaltyRewardModel loyaltyReward = null;
    private LoyaltyPointModel loyaltyPoint = null;
    private LoyaltyRewardLineItemModel availableRewardLineItem = new LoyaltyRewardLineItemModel();
    private TransactionModel transactionModel = null;
    private PosAPIHelper.LoyaltyRewardType rewardTypeEnum;
    private PosAPIHelper.LoyaltyRewardCalcMethod rewardCalcMethodEnum;
    private BigDecimal calculatedRewardValue = BigDecimal.ZERO;
    private BigDecimal transactionSubTotal = BigDecimal.ZERO;

    public LoyaltyRewardCalculation() {
    }

    public LoyaltyRewardCalculation(TransactionModel transactionModel, LoyaltyRewardModel loyaltyReward, LoyaltyPointModel loyaltyPointModel) throws Exception {
        this.transactionModel = transactionModel;
        this.setLoyaltyReward(loyaltyReward);
        this.setLoyaltyPoint(loyaltyPointModel);
        this.getAvailableRewardLineItem().setAccount(AccountModel.createNewAccount(this.getTransactionModel().getLoyaltyAccount(), transactionModel.getTerminal()));
        this.rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(loyaltyReward.getRewardTypeId());
    }

    public void calculateRewards() throws Exception {
        rewardCalcMethodEnum = PosAPIHelper.LoyaltyRewardCalcMethod.convertIntToEnum(loyaltyReward.getRewardCalcMethodId());

        switch (rewardTypeEnum) {
            case ACCOUNT_CREDIT:
                calculateAccountCreditReward();
                break;
            case TRANSACTION_CREDIT:
                calculateTransactionCreditReward();
                availableRewardLineItem.setEligibleAmount(transactionSubTotal);
                break;
            case TRANSACTION_PERCENT_OFF:
                calculateTransactionPercentOffReward();
                break;
            case FREE_PRODUCTS:
                calculateFreeProductsReward();
                availableRewardLineItem.setEligibleAmount(calculatedRewardValue);
                break;
        }

        availableRewardLineItem.setReward(loyaltyReward);
        availableRewardLineItem.setEmployeeId(transactionModel.getLoyaltyAccount().getId());

        availableRewardLineItem.setAmount(calculatedRewardValue);
        availableRewardLineItem.setQuantity(BigDecimal.ONE);
        availableRewardLineItem.setExtendedAmount(calculatedRewardValue);

        //create loyalty points to be deducted when the Loyalty Reward is saved
        LoyaltyPointModel loyaltyPointModel = new LoyaltyPointModel(transactionModel.getLoyaltyAccount(), loyaltyPoint.getLoyaltyProgram(), "");
        loyaltyPointModel.setPoints(loyaltyReward.getPointsToRedeem());
        loyaltyPointModel.setIsReward(true);
        loyaltyPointModel.setEmployeeId(transactionModel.getLoyaltyAccount().getId());
        availableRewardLineItem.setLoyaltyPoint(loyaltyPointModel);

    }

    private void calculateAccountCreditReward() {
        //check that the terminal matches
        if ((transactionModel.getTerminal().getId() == loyaltyReward.getTerminalId()) || loyaltyReward.isAutoChooseTerminal()) {
            switch (rewardCalcMethodEnum) {
                case FIXED_AMOUNT:
                    calculatedRewardValue = loyaltyReward.getValue();
                    break;
                case MULTIPLIER:
                    if (loyaltyReward.getValue() != null && getLoyaltyReward().getPointsToRedeem() != null) {
                        calculatedRewardValue = loyaltyReward.getValue().multiply(new BigDecimal(getLoyaltyReward().getPointsToRedeem()));
                    }
                    break;
            }
        }
    }

    private void calculateTransactionCreditReward() {
        switch (rewardCalcMethodEnum) {
            case FIXED_AMOUNT:
                calculatedRewardValue = loyaltyReward.getValue();
                break;
            case MULTIPLIER:
                if (loyaltyReward.getValue() != null && getLoyaltyReward().getPointsToRedeem() != null) {
                    calculatedRewardValue = loyaltyReward.getValue().multiply(new BigDecimal(getLoyaltyReward().getPointsToRedeem()));
                }
                break;
        }
    }

    private void calculateTransactionPercentOffReward() {
        switch (rewardCalcMethodEnum) {
            case FIXED_AMOUNT:
                BigDecimal percentMultiplier;
                if (loyaltyReward.getValue() != null) {
                    percentMultiplier = loyaltyReward.getValue().divide(new BigDecimal(100));

                    if (percentMultiplier.compareTo(BigDecimal.ZERO) > 0) {
                        //transactionSubTotal
                        calculatedRewardValue = percentMultiplier.multiply(transactionSubTotal);
                    }
                }
                break;
        }
    }

    private void calculateFreeProductsReward() {
        switch (rewardCalcMethodEnum) {
            case FIXED_AMOUNT:
                calculatedRewardValue = loyaltyReward.getValue();
                break;
        }
    }

    /**
     * Check if the Loyalty Reward is Valid
     * 1. Check Loyalty Account Points is greater than the number of points to redeem
     * 2. Check for products on the transaction that are valid for the reward
     * 3. Check the Max Discount Amount for the Reward
     * 4. Check the Min Purchase Amount for the Reward
     *
     * @return
     */
    public boolean isRewardProgramValid() {
        boolean accountHasEnoughPoints = false;
        boolean hasValidProducts = false;
        boolean passesMinTransAmtCheck = false;
        boolean passesMaxTransAmtCheck = false;
        boolean validateMultipleRewardVsAvailablePoints = false;

        transactionModel.prepareProductLineAdjustedAmounts();

        //make sure we sort the products descending, so that the Free Product amount applied to the reward is the highest.
        if (1 == 1) {
            //Order the Products from highest to lowest
            //This is the default for now
            //This will give the customer a free product with the highest value
            //Starbucks style
            Collections.sort(transactionModel.getProducts(), new ProductLineItemDescComparer());
        } else {
            //This will give the customer a free product with the lowest value
            //Dunkin' Donuts style
            Collections.sort(transactionModel.getProducts(), new ProductLineItemAscComparer());
        }

        //if transaction was recorded online, do the point check
        if (transactionModel.wasRecordedOffline()) {
            accountHasEnoughPoints = true; //if transaction was recorded offline, don't do the point check
        } else {
            //if Loyalty Points are less than the minimum on the reward, return false
            if (loyaltyPoint.getPoints() < loyaltyReward.getPointsToRedeem()) {
                loyaltyReward.setNotes("Program " + loyaltyPoint.getLoyaltyProgram().getName() + " does not have enough points for Reward: " + loyaltyReward.getName());
                return false;
            } else {
                accountHasEnoughPoints = true;
            }
        }

        //are Any of the products valid on this transaction
        hasValidProducts = areAnyProductsValidForReward();

        if (hasValidProducts) {
            passesMinTransAmtCheck = this.checkMinTransactionAmount(); //does this transaction pass the Minimum Transaction Total
            passesMaxTransAmtCheck = this.checkMaxRewardAmount(); //does this transaction pass the Minimum Transaction Total
            validateMultipleRewardVsAvailablePoints = this.validateMultipleRewardsVsAvailablePoints(); //check if this Reward is valid, with any other submitted rewards
        } else {
            loyaltyReward.setNotes("No products on this transaction are available for this reward.");
        }

        return accountHasEnoughPoints &&
                hasValidProducts &&
                passesMinTransAmtCheck &&
                passesMaxTransAmtCheck &&
                validateMultipleRewardVsAvailablePoints;
    }

    /**
     * Check the Rewards against the products on the transaction
     * If the reward does not apply to any of the products on the transaction, then return false
     * If the reward does apply to the product, return true  *
     */
    private boolean areAnyProductsValidForReward() {
        boolean productResult = false;

        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
            if (this.getTransactionModel().isInquiry() && !isProductAmountValid(productLineItemModel)) {
                continue;
            }

            if (loyaltyReward.isApplyToAllPlus()) {

                this.setFreeProductRewardValue(productLineItemModel);

                if (loyaltyReward.getNotes().isEmpty()) {
                    loyaltyReward.setNotes(loyaltyReward.getNotes() + "  Reward is valid: it is set to Apply To All Plus");
                }
                return true;
            } else {
                //loop through the submitted rewards and check for matching products
                for (LoyaltyRewardModel loyaltyRewardOnProduct : productLineItemModel.getProduct().getRewards()) {
                    if (loyaltyReward.getId() != null && loyaltyRewardOnProduct.getId() != null) {
                        if (loyaltyReward.getId().equals(loyaltyRewardOnProduct.getId())) {

                            this.setFreeProductRewardValue(productLineItemModel);

                            if (loyaltyReward.getNotes().isEmpty()) {
                                loyaltyReward.setNotes(loyaltyReward.getNotes() + "  Reward is valid: it can be applied to " + productLineItemModel.getProduct().getName());
                            }

                            return true;
                        }
                    }
                }
            }
        }

        if (!productResult) {
            Logger.logMessage("Loyalty reward " + loyaltyReward.getName() + " was not valid for the products on this transaction", PosAPIHelper.getLogFileName(transactionModel.getTerminal().getId()), Logger.LEVEL.DEBUG);
        }

        return productResult;
    }

    /**
     * Calculate the Total Product Eligible Amount
     * If the Reward has Apply to All PLUs set, then all products will be added up
     * If the Reward does NOT have Apply to All PLUs set, then check the Reward -> PLU mapping
     *
     * @return
     */
    private BigDecimal getTotalProductAmountForTransCreditReward() {
        boolean productResult = false;
        BigDecimal totalProductAmountForReward = BigDecimal.ZERO;
        boolean rewardLinesSubmittedInRequest = false;

        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
            if (loyaltyReward.isApplyToAllPlus()) {
                totalProductAmountForReward = totalProductAmountForReward.add(ProductLineItemModel.getAdjValOfExtendedAmountForRewardCalculation(productLineItemModel, loyaltyReward));
                productLineItemModel.getRewardsAvailableForProduct().add(loyaltyReward); //add the reward to the product, this is used during Loyalty Reward Application
            } else {
                //loop through the submitted rewards and check for matching products
                for (LoyaltyRewardModel loyaltyRewardOnProduct : productLineItemModel.getProduct().getRewards()) {
                    if (loyaltyReward.getId() != null && loyaltyRewardOnProduct.getId() != null) {
                        if (loyaltyReward.getId().equals(loyaltyRewardOnProduct.getId())) {
                            totalProductAmountForReward = totalProductAmountForReward.add(ProductLineItemModel.getAdjValOfExtendedAmountForRewardCalculation(productLineItemModel, loyaltyReward));
                            productLineItemModel.getRewardsAvailableForProduct().add(loyaltyReward); //add the reward to the product, this is used during Loyalty Reward Application
                        }
                    }
                }
            }

            if (productLineItemModel.getRewards() != null && !productLineItemModel.getRewards().isEmpty()) {
                rewardLinesSubmittedInRequest = true;
            }
        }

        //if reward details weren't submitted, get the reward total from the Reward Line Items
        if (!rewardLinesSubmittedInRequest) {
            BigDecimal rewardTotal = BigDecimal.ZERO; //Get the total Amount of rewards and subtract that from the product total
            for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()) {
                if (!loyaltyRewardLineItemModel.getReward().getId().equals(loyaltyReward.getId())) { //Only add in the rewards if they don't equal the current Reward being checked
                    rewardTotal = rewardTotal.add(loyaltyRewardLineItemModel.getExtendedAmount().abs());
                }
            }

            totalProductAmountForReward = totalProductAmountForReward.subtract(rewardTotal);
        }

        return totalProductAmountForReward;
    }

    /**
     * If the Reward has the Max Discount field set, verify that the valid products are not greater then this amount
     * This applies only to Free Product rewards
     *
     * @return
     */
    private boolean checkMaxRewardAmount() {
        boolean maxRewardCheck = false;

        //if Max Reward Amount is > 0
        if (loyaltyReward.getMaxDiscount() != null && loyaltyReward.getMaxDiscount().compareTo(BigDecimal.ZERO) == 1) {
            switch (rewardTypeEnum) {
                case FREE_PRODUCTS:
                    for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                        if (this.getTransactionModel().isInquiry() && !isProductAmountValid(productLineItemModel)) {
                            continue;
                        }

                        if (loyaltyReward.isApplyToAllPlus()) {
                            //this product is good to go, break out of this code

                            BigDecimal adjProductAmount = ProductLineItemModel.getAdjValOfAmountForRewardCalculation(productLineItemModel, loyaltyReward);
                            if (loyaltyReward.getMaxDiscount().compareTo(adjProductAmount) == -1  //Max Discount is less than product amount
                                    && !loyaltyReward.isAllowProductsOverMaxAmt()) {

                                loyaltyReward.setNotes("Reward is Invalid: It does not allow Products over the maximum.");
                            } else {
                                maxRewardCheck = true;
                                this.setFreeProductRewardValue(productLineItemModel);
                                loyaltyReward.setNotes("Reward is valid: it can be applied to " + productLineItemModel.getProduct().getName());
                                productLineItemModel.getRewardsAvailableForProduct().add(loyaltyReward);  //Add the Reward to the product line to be used by "updateRewardsAvailableForTransaction"
                            }

                        } else {
                            for (LoyaltyRewardModel loyaltyRewardOnProduct : productLineItemModel.getProduct().getRewards()) {
                                if (loyaltyReward.getId() != null && loyaltyRewardOnProduct.getId() != null) {
                                    if (loyaltyReward.getId().equals(loyaltyRewardOnProduct.getId())) {
                                        //this product is good to go, break out of this code

                                        BigDecimal adjProductAmount = ProductLineItemModel.getAdjValOfAmountForRewardCalculation(productLineItemModel, loyaltyReward);
                                        if (loyaltyReward.getMaxDiscount().compareTo(adjProductAmount) == -1  //Max Discount is less than product amount
                                                && !loyaltyReward.isAllowProductsOverMaxAmt()) {
                                            loyaltyReward.setNotes("Reward is Invalid: It does not allow Products over the maximum.");
                                            //productLineItemModel.getRewardsAvailableForProduct().add(loyaltyReward);  //Add the Reward to the product line to be used by "updateRewardsAvailableForTransaction"
                                        } else {
                                            //maxRewardCheck = true;
                                            this.setFreeProductRewardValue(productLineItemModel);
                                            loyaltyReward.setNotes("Reward is valid: it can be applied to " + productLineItemModel.getProduct().getName());
                                            productLineItemModel.getRewardsAvailableForProduct().add(loyaltyReward);  //Add the Reward to the product line to be used by "updateRewardsAvailableForTransaction"

                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                case TRANSACTION_CREDIT:
                    //Transaction Credits are a static amount so this doesn't need to be checked
                    maxRewardCheck = true; //the Transaction Credit should not have Max Discount set
                    break;
            }
        } else {
            maxRewardCheck = true; //if the setting is not set, return true
        }

        return maxRewardCheck;
    }

    /**
     * If the Reward has the Min Purchase Amount field set, verify that the valid products total is greater then this amount
     * For Free Product Rewards, check that the Product Amount is greater than the Min Purchase Amount
     * For Transaction Credit Rewards, check the Total Eligible Product Amount
     *
     * @return
     */
    private boolean checkMinTransactionAmount() {
        boolean minTransAmtCheck = true;

        //if Min Purchase Amount is > 0
        if (loyaltyReward.getMinPurchase() != null && loyaltyReward.getMinPurchase().compareTo(BigDecimal.ZERO) == 1) {

            switch (rewardTypeEnum) {
                case FREE_PRODUCTS:

                    for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                        if (this.getTransactionModel().isInquiry() && !isProductAmountValid(productLineItemModel)) {
                            continue;
                        }

                        if (loyaltyReward.isApplyToAllPlus()) {
                            BigDecimal adjProductAmount = ProductLineItemModel.getAdjValOfAmountForRewardCalculation(productLineItemModel, loyaltyReward);
                            if (adjProductAmount.compareTo(loyaltyReward.getMinPurchase()) == -1) {
                                minTransAmtCheck = false;
                                loyaltyReward.setNotes("Reward is invalid: the Reward Minimum Product Amount is lower than the product value: " + productLineItemModel.getProduct().getName());
                            } else {
                                //this product is good to go, break out of this code
                                loyaltyReward.setNotes("Reward is valid: it can be applied to " + productLineItemModel.getProduct().getName());
                                return true; //break out of the code, we want to stop once we find a valid product
                            }
                        } else {
                            for (LoyaltyRewardModel loyaltyRewardOnProduct : productLineItemModel.getProduct().getRewards()) {
                                if (loyaltyReward.getId() != null && loyaltyRewardOnProduct.getId() != null) {
                                    if (loyaltyReward.getId().equals(loyaltyRewardOnProduct.getId())) {
                                        //if product amount is < the Minimum Purchase amount, the reward should not be available
                                        BigDecimal adjProductAmount = ProductLineItemModel.getAdjValOfAmountForRewardCalculation(productLineItemModel, loyaltyReward);
                                        if (adjProductAmount.compareTo(loyaltyReward.getMinPurchase()) == -1) {
                                            minTransAmtCheck = false;
                                            loyaltyReward.setNotes("Reward is invalid: the Reward Minimum Product Amount is lower than the product value: " + productLineItemModel.getProduct().getName());
                                        } else {
                                            //this product is good to go, break out of this code
                                            loyaltyReward.setNotes("Reward is valid: it can be applied to " + productLineItemModel.getProduct().getName());
                                            return true; //break out of the code, we want to stop once we find a valid product
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;

                case TRANSACTION_CREDIT:

                    //get the total valid product amount for this reward
                    BigDecimal totalProductsAmountForReward = this.getTotalProductAmountForTransCreditReward();

                    //Add up transaction total for Transaction Credits
                    if (totalProductsAmountForReward.compareTo(loyaltyReward.getMinPurchase()) == -1) { //transaction did not reach the min purchase amount
                        minTransAmtCheck = false;
                        loyaltyReward.setNotes("Reward is invalid: the Reward Minimum Product Amount is lower than the total eligible product amount.");
                    }
                    break;
            }
        }

        return minTransAmtCheck;

    }

    /**
     * If there are multiple rewards submitted, make sure the total number of Program points
     * covers both of the rewards
     */
    private boolean validateMultipleRewardsVsAvailablePoints() {
        boolean rewardIsValid = true;

        //tally up all the points To Redeem on the submitted Rewards
        HashMap<Integer, Integer> rewardPointsToRedeemHM = new HashMap<>();

        //only do this check if there is more than 0 reward submitted
        if (transactionModel.getRewards() != null && !transactionModel.getRewards().isEmpty() && transactionModel.getRewards().size() > 0) {
            for (LoyaltyRewardLineItemModel rewardLineItemModel : transactionModel.getRewards()) {
                if (!rewardLineItemModel.getReward().getId().equals(loyaltyReward.getId())) {
                    //this loyalty Reward gets check below
                    if (rewardPointsToRedeemHM.containsKey(rewardLineItemModel.getReward().getLoyaltyProgram().getId())) {
                        Integer currentPointValue = rewardPointsToRedeemHM.get(rewardLineItemModel.getReward().getLoyaltyProgram().getId());
                        rewardPointsToRedeemHM.put(rewardLineItemModel.getReward().getLoyaltyProgram().getId(), currentPointValue + rewardLineItemModel.getReward().getPointsToRedeem());
                    } else {
                        //this reward doesn't exist yet, add it in
                        rewardPointsToRedeemHM.put(rewardLineItemModel.getReward().getLoyaltyProgram().getId(), rewardLineItemModel.getReward().getPointsToRedeem());
                    }
                }
            }

            //now add the current reward into the list
            if (rewardPointsToRedeemHM.containsKey(loyaltyReward.getLoyaltyProgram().getId())) {
                Integer currentPointValue = rewardPointsToRedeemHM.get(loyaltyReward.getLoyaltyProgram().getId());
                rewardPointsToRedeemHM.put(loyaltyReward.getLoyaltyProgram().getId(), currentPointValue + loyaltyReward.getPointsToRedeem());
            } else {
                //this reward doesn't exist yet, add it in
                rewardPointsToRedeemHM.put(loyaltyReward.getLoyaltyProgram().getId(), loyaltyReward.getPointsToRedeem());
            }

            //now loop through the Loyalty Points for the Loyalty Account and make sure there are enough points for submitted reward and this reward
            for (LoyaltyPointModel loyaltyPointModel : transactionModel.getLoyaltyAccount().getLoyaltyPoints()) {

                //match
                Integer rewardPointsUsed = rewardPointsToRedeemHM.get(loyaltyPointModel.getLoyaltyProgram().getId());
                if (rewardPointsUsed != null) {
                    if (loyaltyPointModel.getLoyaltyProgram().getId().equals(loyaltyReward.getLoyaltyProgram().getId())) {
                        //if Loyalty Points covers the submitted reward(s) and this reward
                        if (rewardPointsUsed > loyaltyPointModel.getPoints()) {
                            loyaltyReward.setNotes("There aren't enough Loyalty Program points to cover this reward and the submitted reward");
                            return false;
                        }
                    }
                }
            }
        }

        return rewardIsValid;
    }

    /**
     * On an transaction inquire, for Free Product Rewards:
     * If the Reward is available for the product, calculate what the Reward amount should be
     */
    private void setFreeProductRewardValue(ProductLineItemModel productLineItemModel) {

        switch (rewardTypeEnum) {
            case FREE_PRODUCTS:

                //calculate the Product amount, minus any Discounts or Rewards, check for Tax Mappings
                BigDecimal adjustedAmount = BigDecimal.ZERO;

                //determine best product
                adjustedAmount = productLineItemModel.getHighestQuantityLineAmount();

                //override the Free Product Amount to the Max Discount Amount
                if (loyaltyReward.getMaxDiscount() != null
                        && loyaltyReward.getMaxDiscount().compareTo(BigDecimal.ZERO) == 1 //Max Discount is greater than zero
                        && loyaltyReward.getMaxDiscount().compareTo(adjustedAmount) == -1  //Max Discount is less than product amount
                        && loyaltyReward.isAllowProductsOverMaxAmt()) { //if Rewards are available over the Max Discount, then override the price

                    loyaltyReward.setValue(loyaltyReward.getMaxDiscount());

                } else {
                    loyaltyReward.setValue(adjustedAmount);
                }

                break;
        }
    }

    /**
     * On an transaction inquire, for Free Product Rewards:
     * If the Reward is available for the product, calculate what the Reward amount should be
     */
    private boolean isProductAmountValid(ProductLineItemModel productLineItemModel) {
        boolean isValid = false;

        //calculate the Product amount, minus any Discounts or Rewards, check for Tax Mappings
        BigDecimal calculatedProductAmountExtended = ProductLineItemModel.getAdjValOfExtendedAmountForRewardCalculation(productLineItemModel, loyaltyReward, true);

        //Divide by the Quantity for Loyalty.  The Free Product Reward will only apply to one quantity
        BigDecimal adjustedAmount = calculatedProductAmountExtended.divide(productLineItemModel.getQuantityForLoyalty(), 4, RoundingMode.HALF_UP);

        switch (rewardTypeEnum) {
            case FREE_PRODUCTS:

                if (adjustedAmount.compareTo(BigDecimal.ZERO) > 0) {
                    isValid = true;
                }

                break;
            case TRANSACTION_CREDIT:
                if (calculatedProductAmountExtended.compareTo(BigDecimal.ZERO) > 0 && productLineItemModel.getAdjExtAmtLoyalty().compareTo(BigDecimal.ZERO) > 0) {
                    isValid = true;
                }
                break;
        }

        return isValid;
    }

    public LoyaltyRewardModel getLoyaltyReward() {
        return loyaltyReward;
    }

    public void setLoyaltyReward(LoyaltyRewardModel loyaltyReward) {
        this.loyaltyReward = loyaltyReward;
    }

    public TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    public LoyaltyRewardLineItemModel getAvailableRewardLineItem() {
        return availableRewardLineItem;
    }

    public void setAvailableRewardLineItem(LoyaltyRewardLineItemModel availableRewardLineItem) {
        this.availableRewardLineItem = availableRewardLineItem;
    }

    public LoyaltyPointModel getLoyaltyPoint() {
        return loyaltyPoint;
    }

    public void setLoyaltyPoint(LoyaltyPointModel loyaltyPoint) {
        this.loyaltyPoint = loyaltyPoint;
    }
}
