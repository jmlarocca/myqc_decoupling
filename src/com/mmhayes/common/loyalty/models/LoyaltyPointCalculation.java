package com.mmhayes.common.loyalty.models;

//mmhayes dependencies

import com.mmhayes.common.loyalty.collections.LoyaltyProgramLevelAscComparer;
import com.mmhayes.common.transaction.collections.ProductLineItemSequenceComparerAsc;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.Logger;

//API dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

//other dependencies
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

/*
 $Author: nyu $: Author of last commit
 $Date: 2021-08-17 14:37:51 -0400 (Tue, 17 Aug 2021) $: Date of last commit
 $Rev: 14929 $: Revision of last commit
 Notes:
*/
@Deprecated
public class LoyaltyPointCalculation {

    private TransactionModel transactionModel = null;
    private LoyaltyProgramModel loyaltyProgramModel = null;
    private LoyaltyPointModel loyaltyPoint = null;
    private PosAPIHelper.ObjectType objectType = PosAPIHelper.ObjectType.PRODUCT;
    private PosAPIHelper.LoyaltyLevelEarningType levelEarningTypeEnum = PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT; //This gets overidden in the constructor

    //enum types
    private PosAPIHelper.LoyaltyProgramType programType;
    private PosAPIHelper.LoyaltyMonetaryRoundingType monetaryRoundingType;
    private PosAPIHelper.LoyaltyMonetaryPrecisionType monetaryPrecisionType;
    private PosAPIHelper.LoyaltyPointRoundingType pointRoundingType;

    private Integer monetaryRoundingScale = 0; //used to calculate monetary rounding
    private Integer monetaryRoundingValue = BigDecimal.ROUND_HALF_UP; //used to calculate monetary rounding, default to MMHAYES "STANDARD"
    private Integer pointRoundingScale = 0; ////used to calculate point rounding, default to a scale of 0
    private Integer pointRoundingValue = BigDecimal.ROUND_HALF_UP; //used to calculate monetary rounding, default to MMHAYES "STANDARD"
    private Integer pointsResult = 0;  //the final point amount returned

    private BigDecimal pointsWorkingTotal = BigDecimal.ZERO;
    private BigDecimal pointsWorkingTotalToUseMultiplier = BigDecimal.ZERO;
    private BigDecimal pointsWorkingTotalNoMultiplier = BigDecimal.ZERO;

    private BigDecimal totalValidTenderAmount = BigDecimal.ZERO;
    private BigDecimal totalTenderAmount = BigDecimal.ZERO;
    private BigDecimal validTenderPercentage = BigDecimal.ONE;

    public LoyaltyPointCalculation() {

    }

    //create Point Model for Product Line Item
    public LoyaltyPointCalculation(TransactionModel transactionModel, LoyaltyProgramModel loyaltyProgramModel) {
        Logger.logMessage("Entering LoyaltyPointCalculation", PosAPIHelper.getLogFileName(transactionModel.getTerminal().getId()), Logger.LEVEL.DEBUG);

        this.transactionModel = transactionModel;
        this.loyaltyProgramModel = loyaltyProgramModel;
        this.objectType = PosAPIHelper.ObjectType.PRODUCT;

        mapLoyaltyPointCalculationFields();
        calculatePoints();
        createLoyaltyPoint();
    }

     /*create the Loyalty Points Earned
    these are saved in the QC_LoyaltyAccountPoint table*/
    //region Create Transaction Point records

    //calculate Loyalty Points depending on Loyalty Program, and transaction with products
    public Integer calculatePoints() {
        try {
            Logger.logMessage("LoyaltyPointCalc: Program: " + loyaltyProgramModel.getName() + ", Type: " + loyaltyProgramModel.getTypeName(), PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.TRACE);

            if (!isAccountGroupMapped()) {
                Logger.logMessage("LoyaltyPointCalc:   Loyalty Account's Account Group is not mapped for this Loyalty Program.  No points earned.", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.TRACE);
                return pointsResult;
            }

            setTotalTenderAmount();
            setTotalValidTenderAmount();
            checkAndSetValidTenderPercentage();

            String percentageLog = "";
            if (this.getValidTenderPercentage().compareTo(BigDecimal.ONE) != 1) {
                percentageLog = " (" + this.getValidTenderPercentage().multiply(new BigDecimal(100)) + "% of Total Tender)";
            }
            Logger.logMessage("LoyaltyPointCalc:   Total valid Tender Amount: " + this.getTotalValidTenderAmount() + percentageLog, PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.TRACE);

            //only do the calculations if there is valid tender
            if (totalValidTenderAmount.abs().compareTo(BigDecimal.ZERO) == 1) {
                transactionModel.prepareProductLineAdjustedAmounts(loyaltyProgramModel); //this sets the adjusted extended amount taking into account taxes, discounts, rewards

                //calculate the total amount of points for products in the transaction
                //calculations are different depending on the Loyalty Program Type
                switch (programType) {
                    case DOLLAR:
                        calculateDollarProgramPoints();
                        break;
                    case WELLNESS:
                        calculateWellnessProgramPoints();
                        break;
                    case PRODUCT:
                        calculateProductProgramPoints();
                        break;
                }

                logTrace("LoyaltyPointCalc:   Total Product Points: " + pointsWorkingTotal);

                switch (programType) {
                    case DOLLAR:
                    case WELLNESS:
                        applyMonetaryRounding(); //round the calculated points/monetary amount depending on the settings on the Loyalty Program
                        applyMultiplier();       //multiply points by the multiplier set on the Loyalty Program
                        applyPartialTenderProration();
                        applyPointRounding();    //round the calculated points depending on the settings on the Loyalty Program
                        break;
                    case PRODUCT:
                        applyMonetaryRoundingForProductLevel(); //round the calculated points/monetary amount depending on the settings on the Loyalty Program
                        applyMultiplierForProductLevel();
                        refreshPointsWorkingTotalForProductLevel();
                        applyPartialTenderProration();
                        applyPointRounding();    //round the calculated points depending on the settings on the Loyalty Program
                        createItemLoyaltyPointsForProductLevel();
                        break;
                }
            }

            pointsResult = pointsWorkingTotal.intValue(); //convert BigDecimal to Integer to return

            logTrace("LoyaltyPointCalc:    Total Points: " + pointsResult);

            return pointsResult;

        } catch (Exception ex) {
            throw ex;
        }
    }

    //Calculate points for Programs of Type "Dollar"
    private void calculateDollarProgramPoints() {

        try {

            if (this.getTransactionModel().getProducts() != null && !this.getTransactionModel().getProducts().isEmpty()) {
                for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                    //quantity = 1
                    if (productLineItemModel.getQuantityForLoyalty().compareTo(BigDecimal.ONE) == 0) {
                        pointsWorkingTotal = pointsWorkingTotal.add(productLineItemModel.getAdjExtAmtLoyalty());
                        logTrace("LoyaltyPointCalc:   Product: " + productLineItemModel.getProduct().getName() + " calculated at " + productLineItemModel.getAdjExtAmtLoyalty());
                    } else {
                        //quantity <> 1
                        BigDecimal workingQuantity = BigDecimal.ZERO;
                        logTrace("LoyaltyPointCalc:   Product: " + productLineItemModel.getProduct().getName());

                        //loop through each of the quantities
                        while (workingQuantity.compareTo(productLineItemModel.getQuantity()) == -1) {
                            BigDecimal adjustedQuantity = BigDecimal.ONE;
                            //check if remainder is less than zero
                            if (productLineItemModel.getQuantity().subtract(workingQuantity).compareTo(BigDecimal.ONE) == -1) {
                                adjustedQuantity = productLineItemModel.getQuantity().subtract(workingQuantity);
                            }

                            pointsWorkingTotal = pointsWorkingTotal.add(productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                            workingQuantity = workingQuantity.add(adjustedQuantity);
                            logTrace("LoyaltyPointCalc:     Quantity: " + workingQuantity + " calculated at " + productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                        }
                    }

                    BigDecimal calculatedProductTotal = this.calculateProductPointTotal(productLineItemModel.getAdjExtAmtLoyalty());
                    ItemLoyaltyPointModel itemLoyaltyPointModel = this.createItemLoyaltyPointForProduct(productLineItemModel, calculatedProductTotal);
                    productLineItemModel.getLoyaltyPointDetails().add(itemLoyaltyPointModel);
                }
            } else {
                //there are no products
                //build points for the valid tender amount
                pointsWorkingTotal = totalValidTenderAmount;
            }

        } catch (Exception ex) {
            throw ex;
        }
    }

    //Calculate points for Programs of Type "Wellness"
    //The entire cost of the product must be covered for the program to earn points
    private void calculateWellnessProgramPoints() {

        this.resetWellnessApplied();

        this.orderProductsByIndex();

        try {
            PosAPIHelper.LoyaltyLevelEarningType levelEarningTypeEnum = PosAPIHelper.LoyaltyLevelEarningType.convertIntToEnum(loyaltyProgramModel.getEarningTypeId());

            for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                if (productLineItemModel.getProduct() != null && productLineItemModel.getProduct().isWellness()) {

                    if (productLineItemModel.getAdjExtAmtLoyalty().compareTo(BigDecimal.ZERO) == 0) {
                        continue; //don't create points if the calculated product amount is zero
                    }

                    BigDecimal productTotalAmount = BigDecimal.ZERO;
                    if (productLineItemModel.getQuantityForLoyalty().compareTo(BigDecimal.ONE) == 0) {
                        switch (levelEarningTypeEnum) {
                            case POINT_PER_PRODUCT:
                                productTotalAmount = productTotalAmount.add(BigDecimal.ONE);
                                productLineItemModel.setWellnessAppliedToProduct(true);
                                logTrace("LoyaltyPointCalc:   Product: " + productLineItemModel.getProduct().getName() + " calculated at " + BigDecimal.ONE);
                                break;
                            case PRICE_OF_PRODUCT:
                                productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjExtAmtLoyalty());
                                productLineItemModel.setWellnessAppliedToProduct(true);
                                logTrace("LoyaltyPointCalc:   Product: " + productLineItemModel.getProduct().getName() + " calculated at " + productLineItemModel.getAdjExtAmtLoyalty());

                                break;
                        }
                        //if quantity does not equal 1
                    } else {
                        BigDecimal workingQuantity = BigDecimal.ZERO;

                        logTrace("LoyaltyPointCalc:   Product: " + productLineItemModel.getProduct().getName());

                        //loop through each of the quantities
                        while (workingQuantity.compareTo(productLineItemModel.getQuantityForLoyalty()) == -1) {
                            BigDecimal adjustedQuantity = BigDecimal.ONE;
                            //check if remainder is less than zero
                            if (productLineItemModel.getQuantityForLoyalty().subtract(workingQuantity).compareTo(BigDecimal.ONE) == -1) {
                                adjustedQuantity = productLineItemModel.getQuantityForLoyalty().subtract(workingQuantity);
                            }

                            //get the weighted quantity, this is used to calculate arbitrary points, if rewards have already been assigned (Free Product, Transaction Credit)
                            BigDecimal weightedQuantity = adjustedQuantity.multiply(productLineItemModel.getAdjAmountPercentageLoyalty());
                            workingQuantity = workingQuantity.add(adjustedQuantity);

                            //if the remaining tender amount covers the entire product price, add the points
                            switch (levelEarningTypeEnum) {
                                case POINT_PER_PRODUCT:
                                    BigDecimal calculatedProductAmount = BigDecimal.ONE.multiply(weightedQuantity);
                                    productTotalAmount = productTotalAmount.add(calculatedProductAmount);
                                    productLineItemModel.setWellnessAppliedToProduct(true);
                                    logTrace("LoyaltyPointCalc:     Quantity: " + workingQuantity + " calculated at " + BigDecimal.ONE);
                                    break;
                                case PRICE_OF_PRODUCT:
                                    productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                    productLineItemModel.setWellnessAppliedToProduct(true);
                                    logTrace("LoyaltyPointCalc:     Quantity: " + workingQuantity + " calculated at " + productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                    break;
                            }
                        }
                    }

                    //Create Item Loyalty Points
                    if (productTotalAmount.compareTo(BigDecimal.ZERO) == 1) {
                        BigDecimal calculatedProductTotal = this.calculateProductPointTotal(productTotalAmount);
                        ItemLoyaltyPointModel itemLoyaltyPointModel = this.createItemLoyaltyPointForProduct(productLineItemModel, calculatedProductTotal);
                        productLineItemModel.getLoyaltyPointDetails().add(itemLoyaltyPointModel);
                    }

                    pointsWorkingTotal = pointsWorkingTotal.add(productTotalAmount);

                } else { //end of "isWellness" if
                    logTrace("LoyaltyPointCalc:   Product is not a Wellness Item.  No points earned: " + productLineItemModel.getProduct().getName());
                }


            } //end of product loop

        } catch (Exception ex) {
            throw ex;
        }
    }

    //Calculate points for Programs of Type "Product"
    //The entire cost of the product must be covered for the program to earn points
    private void calculateProductProgramPoints() {
        try {

            this.resetProgramLevelApplied();

            this.orderProductsByIndex(); //sort product lines by their index/sequence in the transaction

            //sort the program levels by product, subdepartment, department
            Collections.sort(loyaltyProgramModel.getLevelDetails(), new LoyaltyProgramLevelAscComparer());

            for (LoyaltyProgramLevelModel loyaltyProgramLevelModel : loyaltyProgramModel.getLevelDetails()) {

                //keep track of level totals in order to enforce max points
                BigDecimal levelTotalPoints = new BigDecimal(0);
                boolean maxPerTransactionMet = false;

                loyaltyProgramLevelModel.setProductLinesCalcList(new HashMap<>());
                loyaltyProgramLevelModel.setTotalLevelPoints(BigDecimal.ZERO);
                loyaltyProgramLevelModel.setPointCalculationLogList(new ArrayList<>());

                //check the revenue center for the Loyalty Program Level
                //if the Loyalty Program Level matches the revenue center, or the revenue center is null
                if ((transactionModel.getTerminal().getRevenueCenterId().equals(loyaltyProgramLevelModel.getRevenueCenterId())) || (loyaltyProgramLevelModel.getRevenueCenterId() == null)) {
                    for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
                        if (maxPerTransactionMet || productLineItemModel.productLevelAppliedToProduct()) {
                            continue;
                        }

                        BigDecimal productTotalAmount = new BigDecimal(0);
                        PosAPIHelper.LoyaltyLevelItemType levelItemTypeEnum = PosAPIHelper.LoyaltyLevelItemType.valueOf(loyaltyProgramLevelModel.getItemType().toUpperCase());
                        PosAPIHelper.LoyaltyLevelEarningType levelEarningTypeEnum = PosAPIHelper.LoyaltyLevelEarningType.convertIntToEnum(loyaltyProgramLevelModel.getEarningTypeId());

                        //if level doesn't apply to product, skip over this product
                        if (!doesLevelApplyToProduct(levelItemTypeEnum, productLineItemModel, loyaltyProgramLevelModel)) {
                            continue;
                        }

                        if (productLineItemModel.getProduct().isDiningOption() && productLineItemModel.getExtendedAmount().compareTo(BigDecimal.ZERO) == 0) {
                            //HP 2060 - Allow points to be created for $0 products.  Dining Options - Eat In.
                        } else if (productLineItemModel.getAdjExtAmtLoyalty().compareTo(BigDecimal.ZERO) == 0) {
                            logTrace("LoyaltyPointCalc:   Product: " + productLineItemModel.getProduct().getName() + " calculated at 0,  No points earned ");
                            continue; //don't create points if the calculated product amount is zero
                        }

                        if (productLineItemModel.getQuantityForLoyalty().compareTo(BigDecimal.ONE) == 0) {
                            //Check that the remaining tender covers all of the product
                            switch (levelItemTypeEnum) {
                                case PLU:
                                    //if the product originally only had the subdepartment or department, skip the plu
                                    if (!productLineItemModel.originallyJustSubDepartment() && !productLineItemModel.originallyJustDepartment()) {
                                        if (loyaltyProgramLevelModel.getProductPlu().equalsIgnoreCase(productLineItemModel.getProduct().getProductCode())) {
                                            switch (levelEarningTypeEnum) {
                                                case POINT_PER_PRODUCT:
                                                    productTotalAmount = productTotalAmount.add(BigDecimal.valueOf(loyaltyProgramLevelModel.getPoints()));
                                                    productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT);
                                                    loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   Product calculated at: " + loyaltyProgramLevelModel.getPoints());
                                                    break;
                                                case PRICE_OF_PRODUCT:
                                                    productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjExtAmtLoyalty());
                                                    productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT);
                                                    loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   Product calculated at: " + productLineItemModel.getAdjExtAmtLoyalty());
                                                    break;
                                            }
                                        } else {
                                            loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   Product is not a set for this Product level.  This product did not earn points: " + productLineItemModel.getProduct().getName());
                                        }
                                    }
                                    break;
                                case DEPARTMENT:
                                    //if the product originally only had the department, skip the subdepartment
                                    if (!productLineItemModel.originallyJustSubDepartment()) {
                                        if (loyaltyProgramLevelModel.getDepartmentName().equalsIgnoreCase(productLineItemModel.getProduct().getDepartment())) {
                                            switch (levelEarningTypeEnum) {
                                                case POINT_PER_PRODUCT:
                                                    productTotalAmount = productTotalAmount.add(BigDecimal.valueOf(loyaltyProgramLevelModel.getPoints()));
                                                    productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT);
                                                    loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   Department calculated at: " + loyaltyProgramLevelModel.getPoints());
                                                    break;
                                                case PRICE_OF_PRODUCT:
                                                    productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjExtAmtLoyalty());
                                                    productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT);
                                                    loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   Department calculated at: " + productLineItemModel.getAdjExtAmtLoyalty());
                                                    break;
                                            }
                                        } else {
                                            loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   Product is not a set for this Product level.  This product did not earn points: " + productLineItemModel.getProduct().getName());
                                        }
                                    }
                                    break;
                                case SUBDEPARTMENT:
                                    //if the product originally only had the subdepartment, skip the department
                                    if (!productLineItemModel.originallyJustDepartment()) {
                                        if (loyaltyProgramLevelModel.getSubDepartmentName().equalsIgnoreCase(productLineItemModel.getProduct().getSubDepartment())) {
                                            switch (levelEarningTypeEnum) {
                                                case POINT_PER_PRODUCT:
                                                    productTotalAmount = productTotalAmount.add(BigDecimal.valueOf(loyaltyProgramLevelModel.getPoints()));
                                                    productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT);
                                                    loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   SubDepartment calculated at: " + loyaltyProgramLevelModel.getPoints());
                                                    break;
                                                case PRICE_OF_PRODUCT:
                                                    productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjExtAmtLoyalty());
                                                    productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT);
                                                    loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   SubDepartment calculated at: " + productLineItemModel.getAdjExtAmtLoyalty());
                                                    break;
                                            }
                                        } else {
                                            loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   Product is not a set for this Product level.  This product did not earn points: " + productLineItemModel.getProduct().getName());
                                        }
                                    }
                                    break;
                            }
                        } else {   //the quantity did not equal zero, loop through each
                            BigDecimal workingQuantity = BigDecimal.ZERO;
                            loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   Product: " + productLineItemModel.getProduct().getName());

                            //we still have work to do
                            while (workingQuantity.compareTo(productLineItemModel.getQuantityForLoyalty()) == -1) {
                                BigDecimal adjustedQuantity = BigDecimal.ONE;

                                //check if remainder is less than zero
                                if (productLineItemModel.getQuantityForLoyalty().subtract(workingQuantity).compareTo(BigDecimal.ONE) == -1) {
                                    adjustedQuantity = productLineItemModel.getQuantityForLoyalty().subtract(workingQuantity);
                                }

                                //get the weighted quantity, this is used to calculate arbitrary points, if rewards have already been assigned (Free Product, Transaction Credit)
                                BigDecimal weightedQuantity = adjustedQuantity.multiply(productLineItemModel.getAdjAmountPercentageLoyalty());
                                workingQuantity = workingQuantity.add(adjustedQuantity);

                                //if the remaining tender amount covers the entire product price, add the points
                                switch (levelItemTypeEnum) {
                                    case PLU:
                                        //if the product originally only had the subdepartment or department, skip the plu
                                        if (!productLineItemModel.originallyJustSubDepartment() && !productLineItemModel.originallyJustDepartment()) {
                                            if (loyaltyProgramLevelModel.getProductPlu().equalsIgnoreCase(productLineItemModel.getProduct().getProductCode())) {
                                                switch (levelEarningTypeEnum) {
                                                    case POINT_PER_PRODUCT:
                                                        BigDecimal calculatedProductAmount = BigDecimal.valueOf(loyaltyProgramLevelModel.getPoints()).multiply(weightedQuantity);
                                                        productTotalAmount = productTotalAmount.add(calculatedProductAmount);
                                                        productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT);
                                                        loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:     Product Quantity: " + workingQuantity + " calculated at " + BigDecimal.valueOf(loyaltyProgramLevelModel.getPoints()).multiply(weightedQuantity));
                                                        break;
                                                    case PRICE_OF_PRODUCT:
                                                        productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                                        productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT);
                                                        loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:     Product Quantity: " + workingQuantity + " calculated at " + productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                                        break;
                                                }
                                            } else {
                                                loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   Product is not a set for this Product level.  This product did not earn points: " + productLineItemModel.getProduct().getName());
                                            }
                                        }
                                        break;
                                    case DEPARTMENT:
                                        //if the product originally only had the subdepartment, skip the department
                                        if (!productLineItemModel.originallyJustSubDepartment()) {
                                            if (loyaltyProgramLevelModel.getDepartmentName().equalsIgnoreCase(productLineItemModel.getProduct().getDepartment())) {
                                                switch (levelEarningTypeEnum) {
                                                    case POINT_PER_PRODUCT:
                                                        BigDecimal calculatedProductAmount = BigDecimal.valueOf(loyaltyProgramLevelModel.getPoints()).multiply(weightedQuantity);
                                                        productTotalAmount = productTotalAmount.add(calculatedProductAmount);
                                                        productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT);
                                                        loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:     Department Quantity: " + workingQuantity + " calculated at " + BigDecimal.valueOf(loyaltyProgramLevelModel.getPoints()).multiply(weightedQuantity));
                                                        break;
                                                    case PRICE_OF_PRODUCT:
                                                        productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                                        productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT);
                                                        loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:     Department Quantity: " + workingQuantity + " calculated at " + productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                                        break;
                                                }
                                            } else {
                                                loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   Product is not a set for this Product level.  This product did not earn points: " + productLineItemModel.getProduct().getName());
                                            }
                                        }
                                        break;
                                    case SUBDEPARTMENT:
                                        //if the product originally only had the department, skip the subdepartment
                                        if (!productLineItemModel.originallyJustDepartment()) {
                                            if (loyaltyProgramLevelModel.getSubDepartmentName().equalsIgnoreCase(productLineItemModel.getProduct().getSubDepartment())) {
                                                switch (levelEarningTypeEnum) {
                                                    case POINT_PER_PRODUCT:
                                                        BigDecimal calculatedProductAmount = BigDecimal.valueOf(loyaltyProgramLevelModel.getPoints()).multiply(weightedQuantity);
                                                        productTotalAmount = productTotalAmount.add(calculatedProductAmount);
                                                        productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.POINT_PER_PRODUCT);
                                                        loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:     SubDepartment Quantity: " + workingQuantity + " calculated at " + BigDecimal.valueOf(loyaltyProgramLevelModel.getPoints()).multiply(weightedQuantity));
                                                        break;
                                                    case PRICE_OF_PRODUCT:
                                                        productTotalAmount = productTotalAmount.add(productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                                        productLineItemModel.setLoyaltyLevelEarningType(PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT);
                                                        loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:     SubDepartment Quantity: " + workingQuantity + " calculated at " + productLineItemModel.getAdjAmountLoyalty().multiply(adjustedQuantity));
                                                        break;
                                                }
                                            } else {
                                                loyaltyProgramLevelModel.getPointCalculationLogList().add("LoyaltyPointCalc:   Product is not a set for this Product level.  This product did not earn points: " + productLineItemModel.getProduct().getName());
                                            }
                                        }
                                        break;
                                }
                            }
                        }

                        if (productTotalAmount.compareTo(BigDecimal.ZERO) == 0) {
                            continue; // if the points for this product is zero, continue to the next product
                        }

                        if (productTotalAmount.compareTo(BigDecimal.ZERO) == 1) {
                            loyaltyProgramLevelModel.getProductLinesCalcList().put(productLineItemModel.getTransactionItemNum(), productTotalAmount);
                            productLineItemModel.setProductLevelAppliedToProduct(true); //level applied to product, mark product so no other levels are applied to it
                        }

                        levelTotalPoints = levelTotalPoints.add(productTotalAmount);

                    }  //end of product loop
                } //end of revenue center check

                loyaltyProgramLevelModel.setTotalLevelPoints(loyaltyProgramLevelModel.getTotalLevelPoints().add(levelTotalPoints));
            } // end of program level loop

            //Go back through the levels; log product calculations and check for Max Per Transaction setting
            for (LoyaltyProgramLevelModel loyaltyProgramLevelModel : loyaltyProgramModel.getLevelDetails()) {
                if (loyaltyProgramLevelModel.getTotalLevelPoints() != null && loyaltyProgramLevelModel.getTotalLevelPoints().compareTo(BigDecimal.ZERO) == 1) {
                    BigDecimal levelPointsCalculated = loyaltyProgramLevelModel.getTotalLevelPoints();

                    for (String logMessage : loyaltyProgramLevelModel.getPointCalculationLogList()) { //Log the product calculations from above
                        logTrace(logMessage);
                    }

                    //Check the earned points for the Max Per Transaction setting
                    boolean maxTransAmountApplied = false;
                    if (loyaltyProgramLevelModel.getMaxPointsPerTransaction() != null && !loyaltyProgramLevelModel.getMaxPointsPerTransaction().toString().isEmpty()) {
                        //Check Max Per Transaction
                        //add the product points to the level, check the max
                        BigDecimal tempMaxPoints = levelPointsCalculated;

                        tempMaxPoints = this.applyMonetaryRounding(tempMaxPoints);
                        if (loyaltyProgramLevelModel.getEarningTypeId() == PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT.toInt()) {
                            tempMaxPoints = this.applyMultiplier(tempMaxPoints);
                        }
                        tempMaxPoints = this.applyPointRounding(tempMaxPoints);

                        if (tempMaxPoints.compareTo(loyaltyProgramLevelModel.getMaxPointsPerTransaction()) == 1) { //if it's equal to or greater than
                            levelPointsCalculated = loyaltyProgramLevelModel.getMaxPointsPerTransaction();
                            loyaltyProgramLevelModel.setTotalLevelPoints(levelPointsCalculated); //update the calculated points for this program level
                            maxTransAmountApplied = true;
                            logTrace("LoyaltyPointCalc:    Max Per Transaction set, points earned set to : " + levelPointsCalculated);
                        }
                    } else {
                        logTrace("LoyaltyPointCalc:    Program Level points calculated at : " + levelPointsCalculated);
                    }

                    if (maxTransAmountApplied) {
                        pointsWorkingTotalNoMultiplier = pointsWorkingTotalNoMultiplier.add(levelPointsCalculated);
                    } else if (loyaltyProgramLevelModel.getEarningTypeId() == PosAPIHelper.LoyaltyLevelEarningType.PRICE_OF_PRODUCT.toInt()) {
                        pointsWorkingTotalToUseMultiplier = pointsWorkingTotalToUseMultiplier.add(levelPointsCalculated);
                    } else {
                        pointsWorkingTotalNoMultiplier = pointsWorkingTotalNoMultiplier.add(levelPointsCalculated);
                    }
                }
            }

            refreshPointsWorkingTotalForProductLevel();

        } catch (Exception ex) {
            throw ex;
        }
    }

    //endregion

    //Set all the properties for the Loyalty Program so the calculation can be done

    private void mapLoyaltyPointCalculationFields() {
        try {

            this.setProgramType(PosAPIHelper.LoyaltyProgramType.intToLoyaltyProgramType(loyaltyProgramModel.getTypeId()));
            this.setMonetaryRoundingType(PosAPIHelper.LoyaltyMonetaryRoundingType.intToLoyaltyMonetaryRoundingType(loyaltyProgramModel.getMonetaryRoundingTypeId()));
            this.setMonetaryPrecisionType(PosAPIHelper.LoyaltyMonetaryPrecisionType.intToLoyaltyMonetaryPrecisionType(loyaltyProgramModel.getMonetaryRoundingPrecisionId()));
            this.setPointRoundingType(PosAPIHelper.LoyaltyPointRoundingType.intToLoyaltyPointRoundingType(loyaltyProgramModel.getPointRoundingTypeId()));

            switch (this.getMonetaryRoundingType()) {
                case STANDARD:
                case HALF_UP:
                    monetaryRoundingValue = BigDecimal.ROUND_HALF_UP;
                    break;

                case UP:
                    monetaryRoundingValue = BigDecimal.ROUND_UP;
                    break;

                case DOWN:
                    monetaryRoundingValue = BigDecimal.ROUND_DOWN;
                    break;

                case HALF_DOWN:
                    monetaryRoundingValue = BigDecimal.ROUND_HALF_DOWN;
                    break;
            }

            switch (this.getMonetaryPrecisionType()) {
                case DOLLAR:
                    monetaryRoundingScale = 0;
                    break;

                case DIME:
                    monetaryRoundingScale = 1;
                    break;

                case PENNY:
                    monetaryRoundingScale = 2;
                    break;
            }

            switch (this.getPointRoundingType()) {
                case STANDARD:
                case HALF_UP:
                    pointRoundingValue = BigDecimal.ROUND_HALF_UP;
                    break;

                case UP:
                    pointRoundingValue = BigDecimal.ROUND_UP;
                    break;

                case DOWN:
                    pointRoundingValue = BigDecimal.ROUND_DOWN;
                    break;

                case HALF_DOWN:
                    pointRoundingValue = BigDecimal.ROUND_HALF_DOWN;
                    break;
            }

        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Used for 1 product
     *
     * @param amountToRound
     * @return
     */
    private BigDecimal applyMonetaryRounding(BigDecimal amountToRound) {
        return amountToRound.setScale(monetaryRoundingScale, monetaryRoundingValue);
    }

    /**
     * Used for 1 product
     *
     * @param amountToRound
     * @return
     */
    private BigDecimal applyMultiplier(BigDecimal amountToRound) {
        if (loyaltyProgramModel.getPointsMultiplier() == null || loyaltyProgramModel.getPointsMultiplier().toString().isEmpty()) {
            return amountToRound.multiply(BigDecimal.ONE);
        } else {
            return amountToRound.multiply(loyaltyProgramModel.getPointsMultiplier());
        }
    }

    /**
     * Used for 1 product
     *
     * @param amountToRound
     * @return
     */
    private BigDecimal applyPointRounding(BigDecimal amountToRound) {
        return amountToRound.setScale(pointRoundingScale, pointRoundingValue);
    }

    private void applyMonetaryRounding() {
        pointsWorkingTotal = pointsWorkingTotal.setScale(monetaryRoundingScale, monetaryRoundingValue);
        logTrace("LoyaltyPointCalc:    Total Points: " + pointsWorkingTotal + " after monetary rounding");
    }

    private void applyMonetaryRoundingForProductLevel() {
        pointsWorkingTotalNoMultiplier = pointsWorkingTotalNoMultiplier.setScale(monetaryRoundingScale, monetaryRoundingValue);
        pointsWorkingTotalToUseMultiplier = pointsWorkingTotalToUseMultiplier.setScale(monetaryRoundingScale, monetaryRoundingValue);
        refreshPointsWorkingTotalForProductLevel();

        if (this.getLoyaltyProgramModel().getPointsMultiplier() != null && this.getLoyaltyProgramModel().getPointsMultiplier().compareTo(BigDecimal.ZERO) == 1) {
            if (pointsWorkingTotalNoMultiplier.compareTo(BigDecimal.ZERO) == 1 && pointsWorkingTotalToUseMultiplier.compareTo(BigDecimal.ZERO) == 1) {
                logTrace("LoyaltyPointCalc:     " + pointsWorkingTotalNoMultiplier + " will not get Multiplier");
                logTrace("LoyaltyPointCalc:     " + pointsWorkingTotalToUseMultiplier + " will have Multiplier applied (" + this.getLoyaltyProgramModel().getPointsMultiplier() + ")");
            }
        } else {
            logTrace("LoyaltyPointCalc:    Total Points: " + pointsWorkingTotal + " after monetary rounding");
        }
    }

    //endregion

    private void applyMultiplier() {
        if (loyaltyProgramModel.getPointsMultiplier() == null || loyaltyProgramModel.getPointsMultiplier().toString().isEmpty()) {
            pointsWorkingTotal = pointsWorkingTotal.multiply(BigDecimal.ONE);
        } else {
            pointsWorkingTotal = pointsWorkingTotal.multiply(loyaltyProgramModel.getPointsMultiplier());
            //Logger.logMessage("Applied Multiplier: " + loyaltyProgramModel.getPointsMultiplier(), PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.DEBUG);
            logTrace("LoyaltyPointCalc:    Total Points: " + pointsWorkingTotal + " after multiplier");
        }
    }

    private void applyMultiplierForProductLevel() {
        if (loyaltyProgramModel.getPointsMultiplier() == null || loyaltyProgramModel.getPointsMultiplier().toString().isEmpty()) {
            pointsWorkingTotalNoMultiplier = pointsWorkingTotalNoMultiplier.multiply(BigDecimal.ONE);
            pointsWorkingTotalToUseMultiplier = pointsWorkingTotalToUseMultiplier.multiply(BigDecimal.ONE);
            refreshPointsWorkingTotalForProductLevel();
        } else {
            pointsWorkingTotalToUseMultiplier = pointsWorkingTotalToUseMultiplier.multiply(loyaltyProgramModel.getPointsMultiplier());
            refreshPointsWorkingTotalForProductLevel();
            logTrace("LoyaltyPointCalc:    Total Points: " + pointsWorkingTotal + " after multiplier");
        }
    }

    private void applyPointRounding() {
        pointsWorkingTotal = pointsWorkingTotal.setScale(pointRoundingScale, pointRoundingValue);
        logTrace("LoyaltyPointCalc:    Total Points: " + pointsWorkingTotal + " after Point rounding");
    }

    private void applyPartialTenderProration() {
        if (this.getValidTenderPercentage().compareTo(BigDecimal.ONE) != 0 && pointsWorkingTotal.compareTo(BigDecimal.ZERO) != 0) {
            pointsWorkingTotal = pointsWorkingTotal.multiply(this.getValidTenderPercentage());
            logTrace("LoyaltyPointCalc:    Total Points: " + pointsWorkingTotal + " (" + this.getValidTenderPercentage().multiply(new BigDecimal(100)) + "%) Calculated Points pro-rated, Partial Tender Transaction.");
        }
    }

    private BigDecimal applyPartialTenderProration(BigDecimal productPoints) {
        if (this.getValidTenderPercentage().compareTo(BigDecimal.ONE) != 0) {
            productPoints = productPoints.multiply(this.getValidTenderPercentage());
        }
        return productPoints;
    }

    private void setTotalValidTenderAmount() {
        totalValidTenderAmount = BigDecimal.ZERO;

        //first get the tender amount
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            for (TenderModel loyaltyTenderModel : loyaltyProgramModel.getTenders()) {
                if (tenderLineItemModel.getTender().getId().equals(loyaltyTenderModel.getId())) {
                    totalValidTenderAmount = totalValidTenderAmount.add(tenderLineItemModel.getExtendedAmount());
                }
            }
        }
    }

    private void setTotalTenderAmount() {
        totalTenderAmount = BigDecimal.ZERO;

        //first get the tender amount
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            totalTenderAmount = totalTenderAmount.add(tenderLineItemModel.getExtendedAmount());
        }

        totalTenderAmount = totalTenderAmount.abs();
    }

    private boolean isAccountGroupMapped() {
        boolean result = false;

        if (this.getLoyaltyProgramModel().getPayrollGroupingIds() != null && this.getLoyaltyProgramModel().getPayrollGroupingIds().size() > 0) {
            if (this.getTransactionModel().getLoyaltyAccount().getPayrollGroupingId() != null) {
                for (Integer mappedAccountGroupId : this.getLoyaltyProgramModel().getPayrollGroupingIds()) {
                    if (mappedAccountGroupId.equals(this.getTransactionModel().getLoyaltyAccount().getPayrollGroupingId())) {
                        result = true;
                        break;
                    }
                }
            }
        }

        return result;
    }

    private void createLoyaltyPoint() {

        if (this.getLoyaltyPoint() == null) {
            this.setLoyaltyPoint(new LoyaltyPointModel());
        }

        this.loyaltyPoint.setLoyaltyProgram(loyaltyProgramModel);
        this.loyaltyPoint.setEmployeeId(transactionModel.getLoyaltyAccount().getId());
        this.loyaltyPoint.setPoints(pointsResult);
    }

    //get the weighted amount taking into account the earned Loyalty Account Points, Extended cost of the product, and total valid product amount from the transaction
    private BigDecimal getWeightedPointValue(BigDecimal pointResult, BigDecimal productExtendedCost, BigDecimal totalProductAmount) {

        BigDecimal weightedPointValue = BigDecimal.ZERO;
        BigDecimal productPercentageOfTotal = BigDecimal.ZERO;
        BigDecimal oneHundred = new BigDecimal(100);
        //figure out the point amount on the weighted average
        //e.g.:  If all products = $10, and this product amount is $2.00, the percentage is %20
        //this will calculate the point details if only a portion of the tenders are valid

        BigDecimal step1 = productExtendedCost.multiply(oneHundred);

        //check for a Zero totalProductAmount
        BigDecimal step2 = BigDecimal.ZERO;
        if (totalProductAmount.compareTo(BigDecimal.ZERO) > 0) {
            step2 = step1.divide(totalProductAmount, 10, BigDecimal.ROUND_HALF_UP);   //round this to 10 places to get more accurate values
        }

        productPercentageOfTotal = step2.divide(oneHundred);
        weightedPointValue = pointResult.multiply(productPercentageOfTotal);
        weightedPointValue = weightedPointValue.setScale(4, RoundingMode.HALF_UP);  //round it back to 2 places so the it appears as a money value


        return weightedPointValue;
    }

    private boolean doesLevelApplyToProduct(PosAPIHelper.LoyaltyLevelItemType levelItemTypeEnum, ProductLineItemModel productLineItemModel, LoyaltyProgramLevelModel loyaltyProgramLevelModel) {
        Boolean levelAppliesToProduct = false;
        switch (levelItemTypeEnum) {
            case PLU:
                //if the product originally only had the subdepartment or department, skip the plu
                if (!productLineItemModel.originallyJustSubDepartment() && !productLineItemModel.originallyJustDepartment()) {
                    if (loyaltyProgramLevelModel.getProductPlu().equalsIgnoreCase(productLineItemModel.getProduct().getProductCode())) {
                        levelAppliesToProduct = true;
                    }
                }
                break;
            case DEPARTMENT:
                //if the product originally only had the subdepartment, skip the department
                if (!productLineItemModel.originallyJustSubDepartment()) {
                    if (loyaltyProgramLevelModel.getDepartmentName().equalsIgnoreCase(productLineItemModel.getProduct().getDepartment())) {
                        levelAppliesToProduct = true;
                    }
                }
                break;
            case SUBDEPARTMENT:
                //if the product originally only had the department, skip the subdepartment
                if (!productLineItemModel.originallyJustDepartment()) {
                    if (loyaltyProgramLevelModel.getSubDepartmentName().equalsIgnoreCase(productLineItemModel.getProduct().getSubDepartment())) {
                        levelAppliesToProduct = true;
                    }
                }
                break;
        }

        return levelAppliesToProduct;
    }

    private void logDebug(String message) {
        Logger.logMessage(message, PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.DEBUG);
    }

    private void logTrace(String message) {
        Logger.logMessage(message, PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.TRACE);
    }

    private ItemLoyaltyPointModel createItemLoyaltyPointForProduct(ProductLineItemModel productLineItemModel, BigDecimal productPoints) {
        ItemLoyaltyPointModel itemLoyaltyPointModel = new ItemLoyaltyPointModel();
        itemLoyaltyPointModel.setPoints(productPoints);
        itemLoyaltyPointModel.setEligibleAmount(productLineItemModel.getAdjExtAmtLoyalty());
        itemLoyaltyPointModel.setLoyaltyProgramId(this.getLoyaltyProgramModel().getId());
        return itemLoyaltyPointModel;
    }

    private BigDecimal calculateProductPointTotal(BigDecimal productPoints) {
        BigDecimal calculatedTotal = productPoints;
        calculatedTotal = applyMonetaryRounding(calculatedTotal); //round the calculated points/monetary amount depending on the settings on the Loyalty Program
        calculatedTotal = applyMultiplier(calculatedTotal);       //multiply points by the multiplier set on the Loyalty Program
        calculatedTotal = applyPartialTenderProration(calculatedTotal);
        return calculatedTotal;
    }

    private void createItemLoyaltyPointsForProductLevel() {
        HashMap<Integer, BigDecimal> productLineTotals = new HashMap<>();
        BigDecimal totalProductPoints = BigDecimal.ZERO;

        //Tally up all the calculate points per product line for the entire Program
        for (LoyaltyProgramLevelModel loyaltyProgramLevelModel : this.getLoyaltyProgramModel().getLevelDetails()) {
            for (Map.Entry<Integer, BigDecimal> entry : loyaltyProgramLevelModel.getProductLinesCalcList().entrySet()) {
                Integer productIndex = entry.getKey();
                BigDecimal productPoints = entry.getValue();

                if (productPoints.compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }

                if (productLineTotals.containsKey(productIndex)) {
                    BigDecimal productTotal = productLineTotals.get(productIndex);
                    productLineTotals.put(productIndex, productTotal.add(productPoints));
                    totalProductPoints = totalProductPoints.add(productPoints);

                } else {
                    productLineTotals.put(productIndex, productPoints);
                    totalProductPoints = totalProductPoints.add(productPoints);
                }
            }
        }

        //Create the Item Loyalty Points, calculate the weighted amount of the product.  Add the Item Loyalty Point to the Product Line Item
        for (Map.Entry<Integer, BigDecimal> entry : productLineTotals.entrySet()) {
            Integer productIndex = entry.getKey();
            BigDecimal productPoints = entry.getValue();
            BigDecimal productPercentageOfTotal = BigDecimal.ONE;
            if (totalProductPoints.compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }

            productPercentageOfTotal = productPoints.divide(totalProductPoints, 4, RoundingMode.HALF_UP);
            BigDecimal itemPointAmount = pointsWorkingTotal.multiply(productPercentageOfTotal);

            if (itemPointAmount.compareTo(BigDecimal.ZERO) == 1) {
                ProductLineItemModel productLineItemModel = this.getTransactionModel().getProducts().get(productIndex);
                ItemLoyaltyPointModel itemLoyaltyPointModel = this.createItemLoyaltyPointForProduct(productLineItemModel, itemPointAmount);
                productLineItemModel.getLoyaltyPointDetails().add(itemLoyaltyPointModel);
            }

        }
    }

    public PosAPIHelper.LoyaltyMonetaryPrecisionType convertStringPrecisionType(String monetaryPrecisionTypeString) {

        switch (monetaryPrecisionTypeString.toUpperCase()) {
            case "DIMES":
                return PosAPIHelper.LoyaltyMonetaryPrecisionType.DIME;
            case "PENNIES":
                return PosAPIHelper.LoyaltyMonetaryPrecisionType.PENNY;
            default:
                return PosAPIHelper.LoyaltyMonetaryPrecisionType.DOLLAR;
        }
    }

    private void resetProgramLevelApplied() {
        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
            productLineItemModel.setProductLevelAppliedToProduct(false);
        }
    }

    private void resetWellnessApplied() {
        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
            productLineItemModel.setWellnessAppliedToProduct(false);
        }
    }

    private void checkAndSetValidTenderPercentage() {
        if (totalTenderAmount.compareTo(totalValidTenderAmount) != 0) {
            this.setValidTenderPercentage(totalValidTenderAmount.divide(totalTenderAmount, 4, BigDecimal.ROUND_HALF_UP));
        }
    }

    private void refreshPointsWorkingTotalForProductLevel() {
        pointsWorkingTotal = pointsWorkingTotalNoMultiplier.add(pointsWorkingTotalToUseMultiplier);
    }

    private void orderProductsByIndex() {
        Collections.sort(transactionModel.getProducts(), new ProductLineItemSequenceComparerAsc());
    }

    public PosAPIHelper.LoyaltyProgramType getProgramType() {
        return programType;
    }

    public void setProgramType(PosAPIHelper.LoyaltyProgramType programType) {
        this.programType = programType;
    }

    public PosAPIHelper.LoyaltyMonetaryRoundingType getMonetaryRoundingType() {
        return monetaryRoundingType;
    }

    public void setMonetaryRoundingType(PosAPIHelper.LoyaltyMonetaryRoundingType monetaryRoundingType) {
        this.monetaryRoundingType = monetaryRoundingType;
    }

    public PosAPIHelper.LoyaltyMonetaryPrecisionType getMonetaryPrecisionType() {
        return monetaryPrecisionType;
    }

    public void setMonetaryPrecisionType(PosAPIHelper.LoyaltyMonetaryPrecisionType monetaryPrecisionType) {
        this.monetaryPrecisionType = monetaryPrecisionType;
    }

    public PosAPIHelper.LoyaltyPointRoundingType getPointRoundingType() {
        return pointRoundingType;
    }

    public void setPointRoundingType(PosAPIHelper.LoyaltyPointRoundingType pointRoundingType) {
        this.pointRoundingType = pointRoundingType;
    }

    public TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    public LoyaltyProgramModel getLoyaltyProgramModel() {
        return loyaltyProgramModel;
    }

    public void setLoyaltyProgramModel(LoyaltyProgramModel loyaltyProgramModel) {
        this.loyaltyProgramModel = loyaltyProgramModel;
    }

    public Integer getMonetaryRoundingScale() {
        return monetaryRoundingScale;
    }

    public void setMonetaryRoundingScale(Integer monetaryRoundingScale) {
        this.monetaryRoundingScale = monetaryRoundingScale;
    }

    public Integer getMonetaryRoundingValue() {
        return monetaryRoundingValue;
    }

    public void setMonetaryRoundingValue(Integer monetaryRoundingValue) {
        this.monetaryRoundingValue = monetaryRoundingValue;
    }

    public Integer getPointRoundingScale() {
        return pointRoundingScale;
    }

    public void setPointRoundingScale(Integer pointRoundingScale) {
        this.pointRoundingScale = pointRoundingScale;
    }

    public Integer getPointRoundingValue() {
        return pointRoundingValue;
    }

    public void setPointRoundingValue(Integer pointRoundingValue) {
        this.pointRoundingValue = pointRoundingValue;
    }

    public LoyaltyPointModel getLoyaltyPoint() {
        return loyaltyPoint;
    }

    public void setLoyaltyPoint(LoyaltyPointModel loyaltyPoint) {
        this.loyaltyPoint = loyaltyPoint;
    }

    public Integer getPointsResult() {
        return pointsResult;
    }

    public void setPointsResult(Integer pointResult) {
        this.pointsResult = pointResult;
    }

    public BigDecimal getTotalTenderAmount() {
        return totalTenderAmount;
    }

    public void setTenderTotalAmount(BigDecimal totalTenderAmount) {
        this.totalTenderAmount = totalTenderAmount;
    }

    public BigDecimal getTotalValidTenderAmount() {
        return totalValidTenderAmount;
    }

    public void setTotalValidTenderAmount(BigDecimal totalValidTenderAmount) {
        this.totalValidTenderAmount = totalValidTenderAmount;
    }

    public BigDecimal getValidTenderPercentage() {
        return validTenderPercentage;
    }

    public void setValidTenderPercentage(BigDecimal validTenderPercentage) {
        this.validTenderPercentage = validTenderPercentage;
    }
}
