package com.mmhayes.common.loyalty.models;

//api dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidRewardSubmittedException;
import com.mmhayes.common.transaction.collections.LoyaltyRewardLineItemAscComparer;
import com.mmhayes.common.product.models.ItemRewardModel;
import com.mmhayes.common.transaction.collections.*;
import com.mmhayes.common.transaction.models.*;

//other dependencies
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.UUID;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-09-08 14:24:22 -0400 (Wed, 08 Sep 2021) $: Date of last commit
 $Rev: 15323 $: Revision of last commit
*/

public class LoyaltyRewardApplication {

    private TransactionModel transactionModel = null;

    public LoyaltyRewardApplication() {

    }

    public LoyaltyRewardApplication(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;

    }

    //apply rewards to products
    //apply Free Products, Transaction Credits, Percentage of, then Account Credits
    public TransactionModel applyRewards(boolean secondInquiry) throws Exception {

        //calculate what the adjusted amounts for loyalty are
        transactionModel.prepareProductLineAdjustedAmounts();

        if (1 == 1) {
            //Order the Products from highest to lowest
            //This is the default for now
            //This will give the customer a free product with the highest value
            //Starbucks style
            Collections.sort(transactionModel.getProducts(), new ProductLineItemDescComparer());
        } else {
            //This will give the customer a free product with the lowest value
            //Dunkin' Donuts style
            Collections.sort(transactionModel.getProducts(), new ProductLineItemAscComparer());
        }

        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getLoyaltyAccount().getRewardsAvailableForTransaction()) {
            loyaltyRewardLineItemModel.getLoyaltyPoint().setIsReward(true);
        }

        //if the Item Reward Model is included on the product, match up the Item Reward Model and Reward Line Item with a UUID
        matchUpRewardGuidForItemRewardModels();

        //For MyQC, check the ItemRewardModels and RewardLineItemModels for reward uuid
        matchUpRewardGuidForItemRewardModelsMyQC();

        //create any Item Reward Models for Auto Payout rewards
        createItemTaxDsctsAutoPayout();

        //create any Item Reward Models that haven't been created yet
        createItemTaxDscts();

        //reset products and rewards so Item Point Details can be created
        resetRewardsAndProducts();

        //create Item Loyalty Points for Auto-Redeem first.  This fixed a but for QCPOS
        createItemLoyaltyPointsAutoPayout();

        //create Item Loyalty Points
        createItemLoyaltyPoints();

        return transactionModel;
    }

    /**
     * if the transaction includes rewards, match up the rewards with the Item Reward Models.
     * Right now, QCPos is sending in the Item Reward Models on the ProductLineItemModels.
     * This method checks for those, and links up the Item Reward Models and the Loyalty Reward Line Items
     * This will only run for a SALE transaction
     */
    private void matchUpRewardGuidForItemRewardModels() {
        //sort rewards lowest to highest (amount will be a negative), so we want the biggest negative to show first
        Collections.sort(transactionModel.getRewards(), new LoyaltyRewardLineItemAscComparer("extendedAmount"));

        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
            if (loyaltyRewardLineItemModel.hasBeenApplied()) {
                continue;
            }

            PosAPIHelper.LoyaltyRewardType rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(loyaltyRewardLineItemModel.getReward().getRewardTypeId());

            if (loyaltyRewardLineItemModel.getRewardGuid() == null) {
                loyaltyRewardLineItemModel.setRewardGuid(UUID.randomUUID());
                loyaltyRewardLineItemModel.getLoyaltyPoint().setRewardGuid(loyaltyRewardLineItemModel.getRewardGuid());
            }

            switch (rewardTypeEnum) {
                case FREE_PRODUCTS:
                    for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
                        for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {
                            //check the Free Product reward on the Reward ID and the reward price
                            if (loyaltyRewardLineItemModel.getReward().getId().equals(itemRewardModel.getReward().getId())) {
                                if (!loyaltyRewardLineItemModel.hasBeenApplied()) {
                                    if (itemRewardModel.getRewardGuid() == null) {
                                        loyaltyRewardLineItemModel.setHasBeenApplied(true);
                                        //don't assign the reward guid if it has already been assigned.  This will be used with multiple free products are being applied
                                        itemRewardModel.setRewardGuid(loyaltyRewardLineItemModel.getRewardGuid());
                                    }
                                }
                            }
                        }
                    }

                    break;
                case TRANSACTION_CREDIT:
                    for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
                        for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {
                            if (loyaltyRewardLineItemModel.getReward().getId().equals(itemRewardModel.getReward().getId())) {
                                if (itemRewardModel.getRewardGuid() == null) {
                                    //don't assign the reward guid if it has already been assigned.  This will be used with multiple free products are being applied
                                    itemRewardModel.setRewardGuid(loyaltyRewardLineItemModel.getRewardGuid());
                                    loyaltyRewardLineItemModel.setHasBeenApplied(true);
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }

    /**
     * For MyQC transaction, check for Reward UUID records.  For sale, these are being submitted on the ItemRewardModels and the LoyaltyRewardLineItemModels
     */
    private void matchUpRewardGuidForItemRewardModelsMyQC() {
        if (transactionModel.getPosType().equals(PosAPIHelper.PosType.ONLINE_ORDERING)) {
            if (!transactionModel.isInquiry()) {
                for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
                    if (loyaltyRewardLineItemModel.getRewardGuid() != null) {
                        loyaltyRewardLineItemModel.setHasBeenApplied(true);
                    }
                }
            }
        }
    }

    /**
     * saved to QC_PAItemTaxDsct
     * productLineItemModel.rewards
     * Apply the reward(s) to the Auto Payout rewards first, free products first
     * then apply them to Transaction Credit
     */
    private void createItemTaxDsctsAutoPayout() throws Exception {
        //Free Products
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
            if (!rewardIsValid(loyaltyRewardLineItemModel)) {
                continue;
            }

            if (!loyaltyRewardLineItemModel.getReward().isAutoPayout()) {
                continue;
            }

            PosAPIHelper.LoyaltyRewardType rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(loyaltyRewardLineItemModel.getReward().getRewardTypeId());
            //make sure that the reward type is a Free Product

            if (rewardTypeEnum.equals(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS)) {
                if (!loyaltyRewardLineItemModel.hasBeenApplied()) {
                    //create ItemTaxDisc records for the reward
                    loyaltyRewardLineItemModel = createItemTaxDsctsFreeProduct(loyaltyRewardLineItemModel);
                }

                //if amount is positive, flip it to negative
                if (loyaltyRewardLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == 1) {
                    loyaltyRewardLineItemModel.setAmount(loyaltyRewardLineItemModel.getAmount().multiply(new BigDecimal(-1)));
                    loyaltyRewardLineItemModel.setExtendedAmount(loyaltyRewardLineItemModel.getAmount().multiply(loyaltyRewardLineItemModel.getQuantity()));
                }
            }
        }

        //Transaction Credit
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
            if (!rewardIsValid(loyaltyRewardLineItemModel)) {
                continue;
            }

            if (!loyaltyRewardLineItemModel.getReward().isAutoPayout()) {
                continue;
            }

            PosAPIHelper.LoyaltyRewardType rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(loyaltyRewardLineItemModel.getReward().getRewardTypeId());
            //make sure that the reward type is a

            if (rewardTypeEnum.equals(PosAPIHelper.LoyaltyRewardType.TRANSACTION_CREDIT)) {

                if (!loyaltyRewardLineItemModel.hasBeenApplied()) {
                    //create ItemTaxDisc records for the reward
                    loyaltyRewardLineItemModel = createItemTaxDsctsTransactionCredit(loyaltyRewardLineItemModel);
                }

                //if amount is positive, flip it to negative
                if (loyaltyRewardLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == 1) {
                    loyaltyRewardLineItemModel.setAmount(loyaltyRewardLineItemModel.getAmount().multiply(new BigDecimal(-1)));
                    loyaltyRewardLineItemModel.setExtendedAmount(loyaltyRewardLineItemModel.getAmount().multiply(loyaltyRewardLineItemModel.getQuantity()));
                }
            }
        }
    }

    /**
     * saved to QC_PAItemTaxDsct
     * productLineItemModel.rewards
     * Apply the reward(s) to free products first
     * then apply them to Transaction Credit
     */
    private void createItemTaxDscts() throws Exception {
        //Free Products
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
            if (!rewardIsValid(loyaltyRewardLineItemModel)) {
                continue;
            }

            PosAPIHelper.LoyaltyRewardType rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(loyaltyRewardLineItemModel.getReward().getRewardTypeId());
            //make sure that the reward type is a Free Product

            if (rewardTypeEnum.equals(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS)) {
                if (!loyaltyRewardLineItemModel.hasBeenApplied()) {
                    //create ItemTaxDisc records for the reward
                    loyaltyRewardLineItemModel = createItemTaxDsctsFreeProduct(loyaltyRewardLineItemModel);
                }

                //if amount is positive, flip it to negative
                if (loyaltyRewardLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == 1) {
                    loyaltyRewardLineItemModel.setAmount(loyaltyRewardLineItemModel.getAmount().multiply(new BigDecimal(-1)));
                    loyaltyRewardLineItemModel.setExtendedAmount(loyaltyRewardLineItemModel.getAmount().multiply(loyaltyRewardLineItemModel.getQuantity()));
                }
            }
        }

        //Transaction Credit
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
            if (!rewardIsValid(loyaltyRewardLineItemModel)) {
                continue;
            }

            PosAPIHelper.LoyaltyRewardType rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(loyaltyRewardLineItemModel.getReward().getRewardTypeId());
            //make sure that the reward type is a

            if (rewardTypeEnum.equals(PosAPIHelper.LoyaltyRewardType.TRANSACTION_CREDIT)) {

                if (!loyaltyRewardLineItemModel.hasBeenApplied()) {
                    //create ItemTaxDisc records for the reward
                    loyaltyRewardLineItemModel = createItemTaxDsctsTransactionCredit(loyaltyRewardLineItemModel);
                }

                //if amount is positive, flip it to negative
                if (loyaltyRewardLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == 1) {
                    loyaltyRewardLineItemModel.setAmount(loyaltyRewardLineItemModel.getAmount().multiply(new BigDecimal(-1)));
                    loyaltyRewardLineItemModel.setExtendedAmount(loyaltyRewardLineItemModel.getAmount().multiply(loyaltyRewardLineItemModel.getQuantity()));
                }
            }
        }
    }

    //productLineItemModel.rewards
    //saved to QC_PAItemTaxDsct
    private LoyaltyRewardLineItemModel createItemTaxDsctsTransactionCredit(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel) throws Exception {
        boolean submittedRewardIsValid = false;

        //first figure out what rewards are available per product
        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
            if (!rewardAndProductValid(loyaltyRewardLineItemModel, productLineItemModel)) {
                continue;
            }

            BigDecimal totalProductAmount = getTotalProductAmountTransactionCredit(loyaltyRewardLineItemModel);
            BigDecimal updatedEligibleAmount = totalProductAmount;
            BigDecimal adjustedProductExtendedAmount = getAdjustedProductExtendedAmount(loyaltyRewardLineItemModel, productLineItemModel);
            BigDecimal weightedRewardValue = getWeightedRewardValue(loyaltyRewardLineItemModel, adjustedProductExtendedAmount, totalProductAmount).abs();

            if (loyaltyRewardLineItemModel.getReward().isApplyToAllPlus()) {

                submittedRewardIsValid = true;
                BigDecimal remainingBalance = loyaltyRewardLineItemModel.getExtendedAmount().abs().subtract(loyaltyRewardLineItemModel.getAppliedValue().abs());
                if (remainingBalance.compareTo(new BigDecimal("0.0005")) == -1) {
                    //if remaining balance is < 1, apply everything to this product
                    weightedRewardValue = weightedRewardValue.abs().add(remainingBalance);
                    loyaltyRewardLineItemModel.setAppliedValue(loyaltyRewardLineItemModel.getAppliedValue().add(remainingBalance));
                    loyaltyRewardLineItemModel.setHasBeenApplied(true);
                    break;
                }

                //update the Eligible Amount with the total Product Amount
                loyaltyRewardLineItemModel.setEligibleAmount(updatedEligibleAmount);
                //figure out weightedRewardValue
                BigDecimal adjustedExtendedAmount = ProductLineItemModel.getAdjValOfExtendedAmountForReward(productLineItemModel, loyaltyRewardLineItemModel.getReward());
                weightedRewardValue = this.reCalcWeightedRewardValue(productLineItemModel, loyaltyRewardLineItemModel, weightedRewardValue, adjustedExtendedAmount);
                loyaltyRewardLineItemModel.setAppliedValue(loyaltyRewardLineItemModel.getAppliedValue().abs().add(weightedRewardValue));
                productLineItemModel = createItemTaxDsctsTransactionCreditDetails(productLineItemModel, loyaltyRewardLineItemModel, weightedRewardValue);

                //mark it as applied
                BigDecimal roundedApplied = loyaltyRewardLineItemModel.getAppliedValue().setScale(2, RoundingMode.HALF_UP);
                //compare the extendedAmount with roundedApplied
                if (roundedApplied.abs().compareTo(loyaltyRewardLineItemModel.getExtendedAmount().abs()) == 0) {
                    loyaltyRewardLineItemModel.setHasBeenApplied(true);
                    break;
                }
            } else {
                //loop through all valid rewards on the product model
                for (LoyaltyRewardModel loyaltyRewardOnProduct : productLineItemModel.getProduct().getRewards()) {
                    if (!rewardAndProductValid(loyaltyRewardLineItemModel, productLineItemModel)) {
                        continue;
                    }
                    if (!loyaltyRewardLineItemModel.getReward().getId().equals(loyaltyRewardOnProduct.getId())) {
                        //the reward is not valid for this product
                        continue;
                    } else {
                        submittedRewardIsValid = true;
                        BigDecimal remainingBalance = loyaltyRewardLineItemModel.getExtendedAmount().abs().subtract(loyaltyRewardLineItemModel.getAppliedValue().abs());

                        if (remainingBalance.compareTo(new BigDecimal("0.0005")) == -1) {
                            //if remaining balance is < 1, apply everything to this product
                            weightedRewardValue = weightedRewardValue.abs().add(remainingBalance);
                            loyaltyRewardLineItemModel.setAppliedValue(loyaltyRewardLineItemModel.getAppliedValue().add(remainingBalance));
                            loyaltyRewardLineItemModel.setHasBeenApplied(true);
                            break;
                        }

                        //update the Eligible Amount with the total Product Amount
                        loyaltyRewardLineItemModel.setEligibleAmount(updatedEligibleAmount);
                        //figure out weightedRewardValue
                        BigDecimal adjustedExtendedAmount = ProductLineItemModel.getAdjValOfExtendedAmountForReward(productLineItemModel, loyaltyRewardLineItemModel.getReward());
                        weightedRewardValue = this.reCalcWeightedRewardValue(productLineItemModel, loyaltyRewardLineItemModel, weightedRewardValue, adjustedExtendedAmount);
                        loyaltyRewardLineItemModel.setAppliedValue(loyaltyRewardLineItemModel.getAppliedValue().abs().add(weightedRewardValue));
                        productLineItemModel = createItemTaxDsctsTransactionCreditDetails(productLineItemModel, loyaltyRewardLineItemModel, weightedRewardValue);

                        //mark it as applied
                        BigDecimal roundedApplied = loyaltyRewardLineItemModel.getAppliedValue().setScale(2, RoundingMode.HALF_UP);
                        //compare the extendedAmount with roundedApplied
                        if (roundedApplied.abs().compareTo(loyaltyRewardLineItemModel.getExtendedAmount().abs()) == 0) {
                            loyaltyRewardLineItemModel.setHasBeenApplied(true);
                            break;
                        }
                    }
                } //end of rewards on product loop
            }

        } //end of product loop

        if (!submittedRewardIsValid) {
            throw new InvalidRewardSubmittedException("Submitted Reward: " + loyaltyRewardLineItemModel.getReward().getName() + " is Invalid", transactionModel.getTerminal().getId());
        }

        //After running through all the products,
        //if the applied value is less than the Reward value
        //update the reward value
        if (transactionModel.isInquiry()) {
            //round the applied value
            BigDecimal roundedApplied = loyaltyRewardLineItemModel.getAppliedValue().setScale(2, RoundingMode.HALF_UP);

            //compare the extendedAmount with roundedApplied
            if (loyaltyRewardLineItemModel.getExtendedAmount().abs().compareTo(roundedApplied) == 1) {
                loyaltyRewardLineItemModel.setAmount(roundedApplied);
                loyaltyRewardLineItemModel.setExtendedAmount(roundedApplied.multiply(loyaltyRewardLineItemModel.getQuantity()));
                loyaltyRewardLineItemModel.setHasBeenApplied(true);
            }
        }

        return loyaltyRewardLineItemModel;
    }

    /**
     * Used for creating ItemRewardModels for Transaction Credit Rewards
     *
     * @param productLineItemModel
     * @param loyaltyRewardLineItemModel
     * @param weightedRewardValue
     * @param adjustedExtendedAmount
     * @return
     */
    private BigDecimal reCalcWeightedRewardValue(ProductLineItemModel productLineItemModel, LoyaltyRewardLineItemModel loyaltyRewardLineItemModel, BigDecimal weightedRewardValue, BigDecimal adjustedExtendedAmount) {
        if (productLineItemModel.getAppliedRewardValue().compareTo(BigDecimal.ZERO) == 0) { //no partial applications have been made yet
            //if the product amount is less than the weighted reward value
            //bring the weighted reward value down to the product value
            if (adjustedExtendedAmount.compareTo(weightedRewardValue) == 0) {
                productLineItemModel.setHasRewardBeenApplied(true);
            } else if (adjustedExtendedAmount.compareTo(weightedRewardValue) < 0) { //product amount is less than reward

                //only override the price on an inquiry
                if (transactionModel.isInquiry()) {
                    weightedRewardValue = adjustedExtendedAmount;
                    loyaltyRewardLineItemModel.getReward().setNotes("Reward amount overridden to $" + weightedRewardValue + ".  Amount found to be greater than the Product amounts");
                }
                productLineItemModel.setHasRewardBeenApplied(true);
            }
        } else { //some has been applied already
            //re-calculate the weighted amount depending on what has already been applied
            weightedRewardValue = weightedRewardValue.abs();
            BigDecimal weightedRewardAndProductDifference = weightedRewardValue.subtract(adjustedExtendedAmount);

            //add what's already been applied to product, and the weighted Reward Value
            if (weightedRewardAndProductDifference.compareTo(BigDecimal.ZERO) == 0) {
                productLineItemModel.setHasRewardBeenApplied(true);
            } else if (weightedRewardAndProductDifference.compareTo(BigDecimal.ZERO) == 1) { // the reward is too high

                //only override the price on an inquiry
                if (transactionModel.isInquiry()) {
                    weightedRewardValue = adjustedExtendedAmount;
                    loyaltyRewardLineItemModel.getReward().setNotes("Reward amount overridden to $" + weightedRewardValue + ".  Amount found to be greater than the Product amounts");
                }

                productLineItemModel.setHasRewardBeenApplied(true);
            }
        }

        return weightedRewardValue;
    }

    //productLineItemModel.rewards
    //saved to QC_PAItemTaxDsct
    private ProductLineItemModel createItemTaxDsctsTransactionCreditDetails(ProductLineItemModel productLineItemModel, LoyaltyRewardLineItemModel loyaltyRewardLineItemModel, BigDecimal weightedRewardValue) {
        //As rewards gets applied, get the adjusted total Product Extended Amount
        BigDecimal updatedEligibleAmount = ProductLineItemModel.getAdjValOfExtendedAmountForRewardApplication(productLineItemModel, true);

        //flip the sign back
        //if amount is positive, flip it to negative
        if (weightedRewardValue.compareTo(BigDecimal.ZERO) == 1) {
            weightedRewardValue = weightedRewardValue.multiply(new BigDecimal(-1));
        }

        ItemRewardModel itemRewardModel = new ItemRewardModel(loyaltyRewardLineItemModel.getReward(), updatedEligibleAmount, weightedRewardValue);
        itemRewardModel.setRewardGuid(loyaltyRewardLineItemModel.getRewardGuid());
        productLineItemModel.getRewards().add(itemRewardModel);
        productLineItemModel.setAppliedRewardValue(productLineItemModel.getAppliedRewardValue().add(weightedRewardValue));

        return productLineItemModel;
    }

    //productLineItemModel.rewards
    //saved to QC_PAItemTaxDsct
    private LoyaltyRewardLineItemModel createItemTaxDsctsFreeProduct(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel) throws Exception {
        boolean submittedRewardIsValid = false;

        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
            //if the free product reward has already been applied
            //get out of the loop
            if (!rewardAndProductValid(loyaltyRewardLineItemModel, productLineItemModel)) {
                continue;
            }

            //B-05660: Change product eligibility for Maximum Reward Amount setting. 12/28/2017 egl
            if (!isProductLowerThanRewardMaxDiscount(productLineItemModel, loyaltyRewardLineItemModel)) {
                continue;
            }

            //HP 1635 - check all rewards and their Minimum Product amount
            if (!isProductGreaterThanRewardMinimum(productLineItemModel, loyaltyRewardLineItemModel)) {
                continue;
            }

            BigDecimal adjustedProductAmount = getAdjustedProductAmount(loyaltyRewardLineItemModel, productLineItemModel);

            //reward is set to "Apply to All Plus",
            // apply the reward to the first product
            if (loyaltyRewardLineItemModel.getReward().isApplyToAllPlus()) {
                submittedRewardIsValid = true;

                //As rewards gets applied, get the adjusted total Product Extended Amount
                ItemRewardModel itemRewardModel = new ItemRewardModel(loyaltyRewardLineItemModel.getReward(), adjustedProductAmount, adjustedProductAmount);
                itemRewardModel.setRewardGuid(loyaltyRewardLineItemModel.getRewardGuid());
                productLineItemModel.getRewards().add(itemRewardModel);

                loyaltyRewardLineItemModel.setEligibleAmount(adjustedProductAmount);
                loyaltyRewardLineItemModel.setAmount(adjustedProductAmount);
                loyaltyRewardLineItemModel.setExtendedAmount(adjustedProductAmount);
                loyaltyRewardLineItemModel.setHasBeenApplied(true);
                loyaltyRewardLineItemModel.setRewardGuid(loyaltyRewardLineItemModel.getRewardGuid());

                if (productLineItemModel.getQuantityForLoyalty().compareTo(new BigDecimal(productLineItemModel.getRewards().size())) == 0) {
                    productLineItemModel.setAdjExtAmtLoyalty(ProductLineItemModel.getAdjValOfExtendedAmountForTransaction(productLineItemModel).setScale(2, RoundingMode.HALF_UP));
                    if (productLineItemModel.getAdjExtAmtLoyalty().compareTo(BigDecimal.ZERO) <= 0){ //if the net amount of the product is 0, mark it as applied
                        productLineItemModel.setHasRewardBeenApplied(true);
                    }
                }

                break;
            } else {
                //loop through all valid rewards on the product model
                for (LoyaltyRewardModel loyaltyRewardOnProduct : productLineItemModel.getProduct().getRewards()) {
                    if (loyaltyRewardLineItemModel.getReward().getId() != null && loyaltyRewardOnProduct.getId() != null) {
                        if (loyaltyRewardLineItemModel.getReward().getId().equals(loyaltyRewardOnProduct.getId())) {
                            submittedRewardIsValid = true;
                            ItemRewardModel itemRewardModel = new ItemRewardModel(loyaltyRewardLineItemModel.getReward(), adjustedProductAmount, adjustedProductAmount);
                            itemRewardModel.setRewardGuid(loyaltyRewardLineItemModel.getRewardGuid());
                            productLineItemModel.getRewards().add(itemRewardModel);
                            loyaltyRewardLineItemModel.setEligibleAmount(adjustedProductAmount);
                            loyaltyRewardLineItemModel.setAmount(adjustedProductAmount.divide((loyaltyRewardLineItemModel.getQuantity()) != null ? loyaltyRewardLineItemModel.getQuantity() : BigDecimal.ONE));
                            loyaltyRewardLineItemModel.setExtendedAmount(adjustedProductAmount);
                            loyaltyRewardLineItemModel.setHasBeenApplied(true);
                            loyaltyRewardLineItemModel.setRewardGuid(loyaltyRewardLineItemModel.getRewardGuid());

                            if (productLineItemModel.getQuantityForLoyalty().compareTo(new BigDecimal(productLineItemModel.getRewards().size())) == 0) {
                                productLineItemModel.setAdjExtAmtLoyalty(ProductLineItemModel.getAdjValOfExtendedAmountForTransaction(productLineItemModel).setScale(2, RoundingMode.HALF_UP));
                                if (productLineItemModel.getAdjExtAmtLoyalty().compareTo(BigDecimal.ZERO) <= 0) { //if the net amount of the product is 0, mark it as applied
                                    productLineItemModel.setHasRewardBeenApplied(true);
                                }
                            }

                            break;
                        }
                    }
                }
            }
        }

        if (!submittedRewardIsValid) {
            throw new InvalidRewardSubmittedException("Submitted Reward: " + loyaltyRewardLineItemModel.getReward().getName() + " is Invalid", transactionModel.getTerminal().getId());
        }

        return loyaltyRewardLineItemModel;
    }

    //productLineItemModel.loyaltyPointDetails
    //saved to QC_PAItemLoyaltyPoint
    private void createItemLoyaltyPointsAutoPayout() throws Exception {

        //Order products by amount desc, so points are applied just like Item Reward Models
        Collections.sort(transactionModel.getProducts(), new ProductLineItemDescComparer());

        //Free Product
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
            if (!rewardIsValid(loyaltyRewardLineItemModel)) {
                continue;
            }

            if (!loyaltyRewardLineItemModel.getReward().isAutoPayout()) {
                continue;
            }

            PosAPIHelper.LoyaltyRewardType rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(loyaltyRewardLineItemModel.getReward().getRewardTypeId());
            //make sure that the reward type is a Free Product

            if (rewardTypeEnum.equals(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS)) {
                loyaltyRewardLineItemModel = createItemLoyaltyPointsFreeProduct(loyaltyRewardLineItemModel);
            }
        }

        //Transaction Credit
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
            if (!rewardIsValid(loyaltyRewardLineItemModel)) {
                continue;
            }

            if (!loyaltyRewardLineItemModel.getReward().isAutoPayout()) {
                continue;
            }

            PosAPIHelper.LoyaltyRewardType rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(loyaltyRewardLineItemModel.getReward().getRewardTypeId());
            //make sure that the reward type is a Transaction Credit

            if (rewardTypeEnum.equals(PosAPIHelper.LoyaltyRewardType.TRANSACTION_CREDIT)) {
                loyaltyRewardLineItemModel = createItemLoyaltyPointsTransactionCredit(loyaltyRewardLineItemModel);
            }
        }
    }

    //productLineItemModel.loyaltyPointDetails
    //saved to QC_PAItemLoyaltyPoint
    private void createItemLoyaltyPoints() throws Exception {

        //Order products by amount desc, so points are applied just like Item Reward Models
        Collections.sort(transactionModel.getProducts(), new ProductLineItemDescComparer());

        //Free Product
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
            if (!rewardIsValid(loyaltyRewardLineItemModel)) {
                continue;
            }

            PosAPIHelper.LoyaltyRewardType rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(loyaltyRewardLineItemModel.getReward().getRewardTypeId());
            //make sure that the reward type is a Free Product

            if (rewardTypeEnum.equals(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS)) {
                loyaltyRewardLineItemModel = createItemLoyaltyPointsFreeProduct(loyaltyRewardLineItemModel);
            }
        }

        //Transaction Credit
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
            if (!rewardIsValid(loyaltyRewardLineItemModel)) {
                continue;
            }

            PosAPIHelper.LoyaltyRewardType rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(loyaltyRewardLineItemModel.getReward().getRewardTypeId());
            //make sure that the reward type is a Transaction Credit

            if (rewardTypeEnum.equals(PosAPIHelper.LoyaltyRewardType.TRANSACTION_CREDIT)) {
                loyaltyRewardLineItemModel = createItemLoyaltyPointsTransactionCredit(loyaltyRewardLineItemModel);
            }
        }
    }

    private LoyaltyRewardLineItemModel createItemLoyaltyPointsTransactionCredit(LoyaltyRewardLineItemModel loyaltyRewardRedeemed) throws Exception {

        //make sure all submitted rewards have detail points
        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {

            //loop through the Item Rewards that have already been applied
            for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {
                if (itemRewardModel.getRewardGuid().equals(loyaltyRewardRedeemed.getRewardGuid())) {
                    BigDecimal totalProductAmount = getTotalProductAmountTransactionCredit(loyaltyRewardRedeemed);
                    BigDecimal adjustedProductExtendedAmount = getAdjustedProductExtendedAmountTransactionCredit(loyaltyRewardRedeemed, productLineItemModel);
                    BigDecimal weightedPointValue = getWeightedPointValue(loyaltyRewardRedeemed, adjustedProductExtendedAmount, totalProductAmount);

                    // apply the reward to the first product
                    loyaltyRewardRedeemed.setAccount(AccountModel.createNewAccount(transactionModel.getLoyaltyAccount(), this.transactionModel.getTerminal())); //upcast the LoyaltyAccount so all the points and rewards don't show on the returned json
                    loyaltyRewardRedeemed.setEmployeeId(transactionModel.getLoyaltyAccount().getId());
                    loyaltyRewardRedeemed.setRewardGuid(loyaltyRewardRedeemed.getRewardGuid());

                    loyaltyRewardRedeemed.setAppliedValue(loyaltyRewardRedeemed.getAppliedValue().add(weightedPointValue));

                    BigDecimal remainingBalance = new BigDecimal(loyaltyRewardRedeemed.getReward().getPointsToRedeem()).abs().subtract(loyaltyRewardRedeemed.getAppliedValue().abs());
                    remainingBalance = remainingBalance.setScale(2, RoundingMode.HALF_UP);

                    if (remainingBalance.compareTo(BigDecimal.ZERO) <= 0) {
                        loyaltyRewardRedeemed.setHasBeenApplied(true);
                    }

                    productLineItemModel = createItemLoyaltyPointsTransactionCreditDetails(productLineItemModel, loyaltyRewardRedeemed, weightedPointValue);
                }
            }

        } //end of Product Loop

        return loyaltyRewardRedeemed;
    }

    //productLineItemModel.loyaltyPointDetails
    //saved to QC_PAItemLoyaltyPoint
    private ProductLineItemModel createItemLoyaltyPointsTransactionCreditDetails(ProductLineItemModel productLineItemModel, LoyaltyRewardLineItemModel loyaltyRewardLineItemModel, BigDecimal weightedPointValue) {
        //create item Loyalty Point details for reward
        ItemLoyaltyPointModel itemLoyaltyPointModel = new ItemLoyaltyPointModel();
        itemLoyaltyPointModel.setRewardGuid(loyaltyRewardLineItemModel.getRewardGuid());

        //reverse the sign
        BigDecimal points = weightedPointValue.multiply(new BigDecimal(-1));

        itemLoyaltyPointModel.setPoints(points);

        //get the EligibleAmount from the itemRewardModel record, this keeps track of the running totals
        for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {
            if (itemRewardModel.getRewardGuid().equals(itemLoyaltyPointModel.getRewardGuid())) {
                itemLoyaltyPointModel.setEligibleAmount(itemRewardModel.getEligibleAmount());
            }
        }

        itemLoyaltyPointModel.setLoyaltyProgramId(loyaltyRewardLineItemModel.getLoyaltyPoint().getLoyaltyProgram().getId());
        productLineItemModel.getLoyaltyPointDetails().add(itemLoyaltyPointModel);

        return productLineItemModel;
    }

    //productLineItemModel.loyaltyPointDetails
    //saved to QC_PAItemLoyaltyPoint
    private LoyaltyRewardLineItemModel createItemLoyaltyPointsFreeProduct(LoyaltyRewardLineItemModel loyaltyRewardRedeemed) throws Exception {

        //make sure all submitted rewards have detail points
        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {

            //loop through the Item Rewards that have already been applied
            for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {

                if (itemRewardModel.getRewardGuid().equals(loyaltyRewardRedeemed.getRewardGuid())) {
                    loyaltyRewardRedeemed.setAccount(AccountModel.createNewAccount(transactionModel.getLoyaltyAccount(), this.transactionModel.getTerminal())); //upcast the LoyaltyAccount so all the points and rewards don't show on the returned json
                    loyaltyRewardRedeemed.setEmployeeId(transactionModel.getLoyaltyAccount().getId());

                    //create item Loyalty Point details for reward
                    ItemLoyaltyPointModel itemLoyaltyPointModel = new ItemLoyaltyPointModel();
                    itemLoyaltyPointModel.setRewardGuid(loyaltyRewardRedeemed.getRewardGuid());

                    //reverse the sign
                    BigDecimal points = new BigDecimal(loyaltyRewardRedeemed.getReward().getPointsToRedeem()).multiply(new BigDecimal(-1));

                    itemLoyaltyPointModel.setPoints(points);
                    itemLoyaltyPointModel.setEligibleAmount(itemRewardModel.getEligibleAmount()); //use the Eligible Amount off of the ItemRewardModel
                    itemLoyaltyPointModel.setLoyaltyProgramId(loyaltyRewardRedeemed.getLoyaltyPoint().getLoyaltyProgram().getId());

                    productLineItemModel.getLoyaltyPointDetails().add(itemLoyaltyPointModel);
                    loyaltyRewardRedeemed.setHasBeenApplied(true);


                    if (productLineItemModel.getQuantityForLoyalty().compareTo(new BigDecimal(productLineItemModel.getLoyaltyPointDetails().size())) == 0) {
                        if (productLineItemModel.getAdjExtAmtLoyalty().compareTo(BigDecimal.ZERO) <= 0){ //if the net amount of the product is 0, mark it as applied
                            productLineItemModel.setHasRewardBeenApplied(true);
                        }
                    }
                    break;
                }
            }
        }

        return loyaltyRewardRedeemed;
    }

    private void resetRewardsAndProducts() {
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItem : transactionModel.getRewards()) {
            loyaltyRewardLineItem.setHasBeenApplied(false);
            loyaltyRewardLineItem.setAppliedValue(BigDecimal.ZERO);
        }

        for (ProductLineItemModel productLineItem : transactionModel.getProducts()) {

            productLineItem.setHasRewardBeenApplied(false);
            productLineItem.setAppliedRewardValue(BigDecimal.ZERO);
        }
    }

    private boolean rewardAndProductValid(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel, ProductLineItemModel productLineItemModel) {
        return rewardIsValid(loyaltyRewardLineItemModel) && canProductGetReward(productLineItemModel);
    }

    private boolean rewardIsValid(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel) {
        boolean result = true;

        if (loyaltyRewardLineItemModel.hasBeenApplied()) { //if reward has already been applied break out
            result = false;
        }
        if (loyaltyRewardLineItemModel.getExtendedAmount().compareTo(BigDecimal.ZERO) == 0) {
            result = false;
        }
        if (loyaltyRewardLineItemModel.getReward() == null) {
            result = false;
        }

        return result;
    }

    private boolean canProductGetReward(ProductLineItemModel productLineItemModel) {
        boolean result = true;

        //if product has already had a reward applied, continue
        if (productLineItemModel.hasRewardBeenApplied()) {
            result = false;
        }
        if (productLineItemModel.getAdjExtAmtLoyalty().compareTo(BigDecimal.ZERO) == 0) {
            result = false;
        }

        return result;
    }

    /**
     * Compare the max discount amount on the reward against the product amount.
     * If the product amount is higher, the reward can't be applied.
     *
     * @param productLineItemModel
     * @param loyaltyRewardLineItemModel
     * @return
     */
    private boolean isProductLowerThanRewardMaxDiscount(ProductLineItemModel productLineItemModel, LoyaltyRewardLineItemModel loyaltyRewardLineItemModel) {
        boolean result = true;

        if (loyaltyRewardLineItemModel.getReward() != null
                && loyaltyRewardLineItemModel.getReward().getMaxDiscount() != null
                && !loyaltyRewardLineItemModel.getReward().getMaxDiscount().toString().isEmpty()) {

            BigDecimal adjustedAmount = productLineItemModel.getAdjAmountLoyalty();

            //if the product amount is less that Max Discount, and the reward is NOT checked with Alow Products Over Maximum, don't apply it
            if (adjustedAmount.compareTo(loyaltyRewardLineItemModel.getReward().getMaxDiscount()) == 1
                    && !loyaltyRewardLineItemModel.getReward().isAllowProductsOverMaxAmt()) {
                result = false;
            }
        }

        return result;
    }

    /**
     * Compare that the product is greater than or equal to the Reward's Minimum amount
     *
     * @param productLineItemModel
     * @param loyaltyRewardLineItemModel
     * @return
     */
    private boolean isProductGreaterThanRewardMinimum(ProductLineItemModel productLineItemModel, LoyaltyRewardLineItemModel loyaltyRewardLineItemModel) {
        boolean result = false;

        if (loyaltyRewardLineItemModel.getReward() != null
                && loyaltyRewardLineItemModel.getReward().getMinPurchase() != null
                && !loyaltyRewardLineItemModel.getReward().getMinPurchase().toString().isEmpty()) {

            BigDecimal adjustedAmount = productLineItemModel.getAdjAmountLoyalty();

            //if the product amount is greater than or equal to the Minimum Purchase, apply it
            if (adjustedAmount.compareTo(loyaltyRewardLineItemModel.getReward().getMinPurchase()) >= 0) {
                result = true;
            }
        } else {
            result = true;
        }

        return result;
    }

    private BigDecimal getTotalProductAmountTransactionCredit(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel) {
        BigDecimal totalProductAmount = BigDecimal.ZERO;

        for (ProductLineItemModel productLineItemModelTtl : transactionModel.getProducts()) {

            //when calculating the total number of products
            //continue if the product has already been applied
            if (productLineItemModelTtl.hasRewardBeenApplied()) {
                continue;
            }

            //don't apply rewards to zero or negative values
            if (productLineItemModelTtl.getAdjExtAmtLoyalty().compareTo(BigDecimal.ZERO) <= 0) {
                productLineItemModelTtl.setHasRewardBeenApplied(true);
                continue;
            }

            //Account for the Loyalty Reward mapping
            if (!loyaltyRewardLineItemModel.getReward().isApplyToAllPlus()) {

                Boolean productIsMapped = false;
                for (LoyaltyRewardModel mappedLoyaltyRewardModel : productLineItemModelTtl.getProduct().getRewards()) {
                    //Is Product mapped for this Loyalty Reward
                    if (loyaltyRewardLineItemModel.getReward().getId().equals(mappedLoyaltyRewardModel.getId())) {
                        productIsMapped = true;
                    }
                }

                if (!productIsMapped) {
                    continue; //don't add the product total to the transaction total if it is not mapped
                }
            }

            totalProductAmount = totalProductAmount.add(ProductLineItemModel.getAdjValOfExtendedAmountForTransactionCreditRewardApplication(productLineItemModelTtl, loyaltyRewardLineItemModel));
        }

        return totalProductAmount;
    }

    private BigDecimal getAdjustedProductExtendedAmount(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel, ProductLineItemModel productLineItemModel) {
        BigDecimal adjustedExtendedAmount = ProductLineItemModel.getAdjValOfExtendedAmountForReward(productLineItemModel, loyaltyRewardLineItemModel.getReward());
        productLineItemModel.setAdjAmountLoyalty(adjustedExtendedAmount);
        return adjustedExtendedAmount;
    }

    private BigDecimal getAdjustedProductExtendedAmountTransactionCredit(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel, ProductLineItemModel productLineItemModel) {
        BigDecimal adjustedExtendedAmount = ProductLineItemModel.getAdjValOfExtendedAmountForTransactionCreditRewardApplication(productLineItemModel, loyaltyRewardLineItemModel);
        productLineItemModel.setAdjAmountLoyalty(adjustedExtendedAmount);
        return adjustedExtendedAmount;
    }

    //Used when creating ItemTaxDsct records for Free Product rewards.
    private BigDecimal getAdjustedProductAmount(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel, ProductLineItemModel productLineItemModel) {
        BigDecimal adjustedExtendedAmount = ProductLineItemModel.getAdjValOfExtendedAmountForReward(productLineItemModel, loyaltyRewardLineItemModel.getReward());
        BigDecimal adjustedAmount = BigDecimal.ZERO;

        //quantity < 1
        if (productLineItemModel.getQuantityForLoyalty().compareTo(BigDecimal.ONE) == -1) {
            adjustedAmount = adjustedExtendedAmount;
        } else if (productLineItemModel.getQuantityForLoyalty().compareTo(BigDecimal.ONE) == 0) {
            //quantity is 1
            adjustedAmount = adjustedExtendedAmount;
        } else {
            //determine best product
            adjustedAmount = productLineItemModel.getHighestQuantityLineAmount();
        }

        if (loyaltyRewardLineItemModel.getReward() != null
                && loyaltyRewardLineItemModel.getReward().getMaxDiscount() != null
                && loyaltyRewardLineItemModel.getReward().getMaxDiscount().compareTo(BigDecimal.ZERO) == 1 //Max Discount is greater than zero
                && loyaltyRewardLineItemModel.getReward().getMaxDiscount().compareTo(adjustedAmount) == -1) { //Max Discount is less than product amount

            adjustedAmount = loyaltyRewardLineItemModel.getReward().getMaxDiscount(); //override the reward amount with the Max Discount amount
        }

        return adjustedAmount;
    }

    private BigDecimal getWeightedRewardValue(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel, BigDecimal productExtendedCost, BigDecimal totalProductAmount) {
        //First, calculate how much the amount will be for this product
        BigDecimal rewardExtAmount = loyaltyRewardLineItemModel.getExtendedAmount();
        BigDecimal weightedRewardValue = BigDecimal.ZERO;
        BigDecimal productPercentageOfTotal = BigDecimal.ZERO;
        BigDecimal oneHundred = new BigDecimal(100);
        //figure out the point amount on the weighted average
        //e.g.:  If all products = $10, and this product amount is $2.00, the percentage is %20
        //this will calculate the point details if only a portion of the tenders are valid

        BigDecimal step1 = productExtendedCost.multiply(oneHundred);

        //check for a Zero totalProductAmount
        BigDecimal step2 = BigDecimal.ZERO;
        if (totalProductAmount.compareTo(BigDecimal.ZERO) > 0) {
            step2 = step1.divide(totalProductAmount, 10, BigDecimal.ROUND_HALF_UP);   //round this to 10 places to get more accurate values
        }

        productPercentageOfTotal = step2.divide(oneHundred);
        weightedRewardValue = rewardExtAmount.multiply(productPercentageOfTotal);
        weightedRewardValue = weightedRewardValue.setScale(4, RoundingMode.HALF_UP);  //round it back to 2 places so the it appears as a money value

        return weightedRewardValue;
    }

    private BigDecimal getWeightedPointValue(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel, BigDecimal productExtendedCost, BigDecimal totalProductAmount) {

        //First, calculate how much the amount will be for this product
        BigDecimal rewardPoints = new BigDecimal(loyaltyRewardLineItemModel.getReward().getPointsToRedeem());

        BigDecimal weightedRewardValue = BigDecimal.ZERO;
        BigDecimal productPercentageOfTotal = BigDecimal.ZERO;
        BigDecimal oneHundred = new BigDecimal(100);
        //figure out the point amount on the weighted average
        //e.g.:  If all products = $10, and this product amount is $2.00, the percentage is %20
        //this will calculate the point details if only a portion of the tenders are valid

        BigDecimal step1 = productExtendedCost.multiply(oneHundred);

        //check for a Zero totalProductAmount
        BigDecimal step2 = BigDecimal.ZERO;
        if (totalProductAmount.compareTo(BigDecimal.ZERO) > 0) {
            step2 = step1.divide(totalProductAmount, 10, BigDecimal.ROUND_HALF_UP);   //round this to 10 places to get more accurate values
        }

        productPercentageOfTotal = step2.divide(oneHundred);
        weightedRewardValue = rewardPoints.multiply(productPercentageOfTotal);
        weightedRewardValue = weightedRewardValue.setScale(4, RoundingMode.HALF_UP);  //round it back to 2 places so the it appears as a money value

        return weightedRewardValue;
    }
}
