package com.mmhayes.common.loyalty.models;

//mmhayes dependencies

import com.mmhayes.common.account.models.*;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.api.errorhandling.exceptions.LoyaltyProgramNotFoundException;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/*
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-07-02 10:06:02 -0400 (Fri, 02 Jul 2021) $: Date of last commit
 $Rev: 14270 $: Revision of last commit
*/
public class LoyaltyPointModel {

    private Integer id = null;
    private Integer points = null;
    private Integer employeeId = null;
    private Integer loyaltyRewardTransLineId = null;
    private Integer transactionId = null;
    private Integer pointsAlreadyRefunded = 0;
    private Integer originalPoints = 0;
    private Integer loyaltyRewardId = null;
    private String refNumber = "";
    private boolean isReward = false;
    private boolean allPointsAlreadyRefunded = false;

    private ArrayList<ItemLoyaltyPointModel> pointDetails = new ArrayList<>();
    private UUID rewardGuid;
    private PosAPIHelper.LoyaltyRewardType loyaltyRewardType = null;

    private LoyaltyProgramModel loyaltyProgram = null;
    private LoyaltyAccrualPolicyModel loyaltyAccrualPolicy = null;
    private List<LoyaltyRewardModel> rewardsAvailable = new ArrayList<>();
    private List<LoyaltyRewardLineItemModel> rewardsAvailableForTransaction = new ArrayList<>();
    private List<LoyaltyRewardLineItemModel> rewards = new ArrayList<>();
    private List<LoyaltyAccrualPolicyPointModel> loyaltyAccrualPolicyPoints = new ArrayList<>();

    @JsonIgnore
    private AccountModel accountModel = null;

    public LoyaltyPointModel() {

    }

    public LoyaltyPointModel(AccountModel accountModel, LoyaltyProgramModel loyaltyProgramModel, String refNumber) throws Exception {
        this.accountModel = accountModel;
        this.loyaltyProgram = loyaltyProgramModel;
        this.refNumber = refNumber;
    }

    //Constructor- takes in a Hashmap
    public LoyaltyPointModel(HashMap modelDetailHM, Integer terminalId, boolean summaryMode) throws Exception {
        setModelProperties(modelDetailHM, terminalId, summaryMode);
    }

    //setter for all of this model's properties
    public LoyaltyPointModel setModelProperties(HashMap modelDetailHM, Integer terminalId, boolean summaryMode) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));     //LoyaltyAccountPointId
        setEmployeeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("EMPLOYEEID")));
        setPoints(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POINTS")));
        setTransactionId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TRANSACTIONID")));
        setLoyaltyRewardTransLineId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYREWARDLINEITEMID")));

        //Populate the Loyalty Accrual Policy so the ID gets saved on refunds and voids
        if (modelDetailHM.get("LOYALTYACCRUALPOLICYID") != null && !modelDetailHM.get("LOYALTYACCRUALPOLICYID").toString().isEmpty()
                && !CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYACCRUALPOLICYID")).toString().isEmpty()){
            LoyaltyAccrualPolicyModel loyaltyAccrualPolicyModel = new LoyaltyAccrualPolicyModel();
            loyaltyAccrualPolicyModel.setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYACCRUALPOLICYID")));
            this.setLoyaltyAccrualPolicy(loyaltyAccrualPolicyModel);
        }

        if (modelDetailHM.get("LOYALTYREWARDTYPE") != null && !CommonAPI.convertModelDetailToString(modelDetailHM.get("LOYALTYREWARDTYPE")).isEmpty()) {

            if (modelDetailHM.get("LOYALTYREWARDTYPEID") != null && !CommonAPI.convertModelDetailToString(modelDetailHM.get("LOYALTYREWARDTYPEID")).isEmpty()) {
                try {
                    this.setLoyaltyRewardType(PosAPIHelper.LoyaltyRewardType.convertIntToEnum(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYREWARDTYPEID"))));
                } catch (Exception ex) {
                    //Don't do anything with this right now
                }
            }
        }

        if (terminalId == null || !PosAPIModelCache.useTerminalCache) {
            if (this.getLoyaltyProgram() == null) {
                if (CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")) != null) {
                    setLoyaltyProgram(LoyaltyProgramModel.getOneLoyaltyProgram(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")), terminalId, summaryMode));
                }
            }
        } else {
            if (PosAPIModelCache.useTerminalCache) {
                if (this.getLoyaltyProgram() == null) {
                    if (CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")) != null) {
                        LoyaltyProgramModel validatedLoyaltyProgram = null;
                        validatedLoyaltyProgram = (LoyaltyProgramModel) PosAPIModelCache.getOneObject(terminalId, CommonAPI.PosModelType.LOYALTY_PROGRAM, CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")));
                        if (validatedLoyaltyProgram != null) {
                            setLoyaltyProgram(validatedLoyaltyProgram);
                        } else {
                            throw new MissingDataException("Invalid Loyalty Program", terminalId);
                        }
                    }
                }
            }
        }

        return this;
    }

    public static LoyaltyPointModel getOneLoyaltyPointsByProgramAndEmployee(Integer loyaltyProgramId, Integer employeeId, TerminalModel terminalModel) throws Exception {
        LoyaltyPointModel loyaltyPointModel = new LoyaltyPointModel();

        DataManager dm = new DataManager();

        //06-20-2017 egl - HP 586, 587.  Fetch the Account, if the account is invalid, this will return the error
        AccountModel accountModel = AccountModel.getAccountModelByIdForTender(terminalModel, BigDecimal.ZERO, employeeId, new AccountQueryParams());

        //06-20-2017 egl - HP 584. Fetch the Loyalty Program, if it is not found, return a valid error
        LoyaltyProgramModel loyaltyProgramModel = LoyaltyProgramModel.getOneLoyaltyProgram(loyaltyProgramId, terminalModel.getId());
        if (loyaltyProgramModel == null) {
            throw new LoyaltyProgramNotFoundException("No Loyalty Program Found");
        }

        //get all models in an array list
        ArrayList<HashMap> loyalyPointHM = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyPointsByProgramAndEmployee",
                new Object[]{loyaltyProgramId, employeeId, terminalModel.getRevenueCenterId(), terminalModel.getId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        if (loyalyPointHM != null && !loyalyPointHM.isEmpty()) {
            loyaltyPointModel = new LoyaltyPointModel(loyalyPointHM.get(0), terminalModel.getId(), false);
        } else {
            throw new MissingDataException("No Loyalty Points found", terminalModel.getId());
        }

        terminalModel.logTransactionTime();

        return loyaltyPointModel;
    }

    public static LoyaltyPointModel getOneLoyaltyPointModelByTransLineItemId(Integer iPATransLineItemId, Integer terminalId) throws Exception {

        LoyaltyPointModel loyaltyPointModel = new LoyaltyPointModel();

        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> loyalyPointHM = dm.parameterizedExecuteQuery("data.posapi30.getOneLoyaltyPointByTransLineItemId",
                new Object[]{iPATransLineItemId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        if (loyalyPointHM != null && !loyalyPointHM.isEmpty()) {
            loyaltyPointModel = new LoyaltyPointModel(loyalyPointHM.get(0), terminalId, false);
        }

        return loyaltyPointModel;

    }

    //@JsonIgnore
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public LoyaltyProgramModel getLoyaltyProgram() {
        return loyaltyProgram;
    }

    public void setLoyaltyProgram(LoyaltyProgramModel loyaltyProgram) {
        this.loyaltyProgram = loyaltyProgram;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    @JsonIgnore
    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    //@JsonIgnore //03-09-2017 egl I don't think we need to ever show this.  Doing Third Party Spec
    //04/12/2017 egl - we need this to figure out if a Loyalty Point is attached to a reward or not
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    //@JsonIgnore
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getLoyaltyRewardTransLineId() {
        return loyaltyRewardTransLineId;
    }

    public void setLoyaltyRewardTransLineId(Integer loyaltyRewardTransLineId) {
        this.loyaltyRewardTransLineId = loyaltyRewardTransLineId;
    }

    //@JsonIgnore
    @JsonGetter("isReward")
    @JsonIgnore
    //03-09-2017 egl - this is now derived from the TransactionModel.rewards collection.  We don't need to include in json.  Doing Third Party Spec
    public boolean isReward() {
        return isReward;
    }

    public void setIsReward(boolean reward) {
        isReward = reward;
    }

    @JsonIgnore
    public Integer getPointsAlreadyRefunded() {
        return pointsAlreadyRefunded;
    }

    public void setPointsAlreadyRefunded(Integer pointsAlreadyRefunded) {
        this.pointsAlreadyRefunded = pointsAlreadyRefunded;
    }

    @JsonIgnore
    public UUID getRewardGuid() {
        return rewardGuid;
    }

    public void setRewardGuid(UUID rewardGuid) {
        this.rewardGuid = rewardGuid;
    }

    /*@JsonInclude(JsonInclude.Include.NON_EMPTY)*/
    public List<LoyaltyRewardModel> getRewardsAvailable() {
        return rewardsAvailable;
    }

    public void setRewardsAvailable(List<LoyaltyRewardModel> rewardsAvailable) {
        this.rewardsAvailable = rewardsAvailable;
    }

    /*@JsonInclude(JsonInclude.Include.NON_EMPTY)*/
    public List<LoyaltyRewardLineItemModel> getRewardsAvailableForTransaction() {
        return rewardsAvailableForTransaction;
    }

    public void setRewardsAvailableForTransaction(List<LoyaltyRewardLineItemModel> rewardsAvailableForTransaction) {
        this.rewardsAvailableForTransaction = rewardsAvailableForTransaction;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<LoyaltyRewardLineItemModel> getRewards() {
        return rewards;
    }

    public void setRewards(List<LoyaltyRewardLineItemModel> rewards) {
        this.rewards = rewards;
    }

    @JsonIgnore
    public boolean areAllPointsAlreadyRefunded() {
        return allPointsAlreadyRefunded;
    }

    public void setAllPointsAlreadyRefunded(boolean allPointsAlreadyRefunded) {
        this.allPointsAlreadyRefunded = allPointsAlreadyRefunded;
    }

    @JsonIgnore
    public ArrayList<ItemLoyaltyPointModel> getPointDetails() {
        return pointDetails;
    }

    public void setPointDetails(ArrayList<ItemLoyaltyPointModel> pointDetails) {
        this.pointDetails = pointDetails;
    }

    public Integer getOriginalPoints() {
        return originalPoints;
    }

    public void setOriginalPoints(Integer originalPoints) {
        this.originalPoints = originalPoints;
    }

    @JsonIgnore
    public PosAPIHelper.LoyaltyRewardType getLoyaltyRewardType() {
        return loyaltyRewardType;
    }

    public void setLoyaltyRewardType(PosAPIHelper.LoyaltyRewardType loyaltyRewardType) {
        this.loyaltyRewardType = loyaltyRewardType;
    }

    @JsonIgnore
    public Integer getLoyaltyRewardId() {
        return loyaltyRewardId;
    }

    public void setLoyaltyRewardId(Integer loyaltyRewardId) {
        this.loyaltyRewardId = loyaltyRewardId;
    }

    //@JsonIgnore
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public LoyaltyAccrualPolicyModel getLoyaltyAccrualPolicy() {
        return loyaltyAccrualPolicy;
    }

    public void setLoyaltyAccrualPolicy(LoyaltyAccrualPolicyModel loyaltyAccrualPolicy) {
        this.loyaltyAccrualPolicy = loyaltyAccrualPolicy;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<LoyaltyAccrualPolicyPointModel> getLoyaltyAccrualPolicyPoints() {
        return loyaltyAccrualPolicyPoints;
    }

    public void setLoyaltyAccrualPolicyPoints(List<LoyaltyAccrualPolicyPointModel> loyaltyAccrualPolicyPoints) {
        this.loyaltyAccrualPolicyPoints = loyaltyAccrualPolicyPoints;
    }
}
