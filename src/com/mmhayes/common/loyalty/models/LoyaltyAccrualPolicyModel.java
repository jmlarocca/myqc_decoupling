package com.mmhayes.common.loyalty.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.loyalty.collections.LoyaltyAccrualPolicyLevelCollection;
import com.mmhayes.common.transaction.models.TenderDisplayModel;
import com.mmhayes.common.transaction.models.TenderModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-09-21 09:36:42 -0400 (Fri, 21 Sep 2018) $: Date of last commit
 $Rev: 7811 $: Revision of last commit
*/
public class LoyaltyAccrualPolicyModel {
    private Integer id;
    private Integer ownershipGroupId = null;
    private Integer typeId = null; //Loyalty Program Type ID
    private Integer monetaryRoundingTypeId = null;
    private Integer pointRoundingTypeId = null;
    private Integer monetaryRoundingPrecisionId = null;
    private Integer earningTypeId = null;

    private String name = "";
    private String typeName = ""; //Loyalty Program Type Name
    private String description = "";
    private String monetaryRoundingTypeName = ""; //how do you round the dollar amount
    private String monetaryRoundingPrecision = ""; //dimes, dollars, pennies
    private String pointRoundingTypeName = ""; //how do you round the points
    private String earningType = "";
    private BigDecimal pointsMultiplier = null;
    private Boolean includeTax = false;
    private Boolean active = false;
    private Boolean earnPointsForDiscountedProducts = false;
    private Boolean earnPointsForMealPlanProducts = false;
    private Boolean earnPointsForDonations = false;
    private List<LoyaltyAccrualPolicyLevelModel> levelDetails = new ArrayList<>();
    private List<TenderModel> tenders = new ArrayList<>();
    private List<Integer> mappedRevenueCenterIds = new ArrayList<>();

    //constructors
    public LoyaltyAccrualPolicyModel() {

    }

    //Constructor- takes in a Hashmap
    public LoyaltyAccrualPolicyModel(HashMap loyaltyAccrualPolicyHM, Integer terminalId, boolean summaryMode) throws Exception {
        setModelProperties(loyaltyAccrualPolicyHM, terminalId, summaryMode);
    }

    //setter for all of this model's properties
    /**
     * if summaryMode is true, Program Level for the Loyalty Program won't be returned
     * */
    public LoyaltyAccrualPolicyModel setModelProperties(HashMap modelDetailHM, Integer terminalId, boolean summaryMode) throws Exception {

        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYACCRUALPOLICYID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("LOYALTYACCRUALPOLICYNAME")));
        setTypeName(CommonAPI.convertModelDetailToString(modelDetailHM.get("ACCRUALPOLICYTYPENAME")));
        setTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ACCRUALPOLICYTYPEID")));
        setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("ACCRUALPOLICYDESCRIPTION")));

        setMonetaryRoundingTypeName(CommonAPI.convertModelDetailToString(modelDetailHM.get("MONETARYROUNDINGTYPENAME")));
        setMonetaryRoundingTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MONETARYROUNDINGTYPEID")));

        setMonetaryRoundingPrecision(CommonAPI.convertModelDetailToString(modelDetailHM.get("MONETARYROUNDINGPRECISION"))); //dimes, dollars, pennies
        setMonetaryRoundingPrecisionId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MONETARYROUNDINGPRECISIONID"))); //dimes, dollars, pennies

        setPointRoundingTypeName(CommonAPI.convertModelDetailToString(modelDetailHM.get("POINTROUNDINGTYPENAME"))); //how do you round the points
        setPointRoundingTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POINTROUNDINGTYPEID"))); //how do you round the points

        setOwnershipGroupId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ACCRUALPOLICYOWNERSHIPGROUPID")));
        setPointsMultiplier(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("POINTSMULTIPLIER")));
        setIncludeTax(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("INCLUDETAX")));
        setActive(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ACCRUALPOLICYISACTIVE")));
        setEarningType(CommonAPI.convertModelDetailToString(modelDetailHM.get("ACCRUALPOLICYEARNINGTYPENAME")));
        setEarningTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ACCRUALPOLICYEARNINGTYPEID")));

        setEarnPointsForDiscountedProducts(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("EARNPOINTSFORDISCOUNTEDPRODUCTS"), false));
        setEarnPointsForMealPlanProducts(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("EARNPOINTSFORMEALPLANPRODUCTS"), false));
        setEarnPointsForDonations(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("EARNPOINTSFORDONATIONS"), false));

        if (!summaryMode) {
            if (this.getTypeId().equals(PosAPIHelper.LoyaltyAccrualPolicyType.PRODUCT.toInt())) {
                LoyaltyAccrualPolicyLevelCollection laplc = new LoyaltyAccrualPolicyLevelCollection();
                setLevelDetails(laplc.getAllLoyaltyAccrualPolicyLevelsByAccrualPolicyId(getId(), terminalId).getCollection());
            }
        }

        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoyaltyAccrualPolicyModel)) return false;

        LoyaltyAccrualPolicyModel that = (LoyaltyAccrualPolicyModel) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    //Getters and Setters
    //getter
    public Integer getId() {
        return id;
    }

    //setter
    public void setId(Integer id) {
        this.id = id;
    }

    //getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    //getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTypeName() {
        return typeName;
    }

    //setter
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    //getter
    @JsonIgnore
    public String getMonetaryRoundingTypeName() {
        return monetaryRoundingTypeName;
    }

    //setter
    public void setMonetaryRoundingTypeName(String monetaryRoundingTypeName) {
        this.monetaryRoundingTypeName = monetaryRoundingTypeName;
    }

    //getter
    @JsonIgnore
    public String getMonetaryRoundingPrecision() {
        return monetaryRoundingPrecision;
    }

    //setter
    public void setMonetaryRoundingPrecision(String monetaryRoundingPrecision) {
        this.monetaryRoundingPrecision = monetaryRoundingPrecision;
    }

    //getter
    @JsonIgnore
    public String getPointRoundingTypeName() {
        return pointRoundingTypeName;
    }

    //setter
    public void setPointRoundingTypeName(String pointRoundingTypeName) {
        this.pointRoundingTypeName = pointRoundingTypeName;
    }

    //getter
    @JsonIgnore
    public Integer getOwnershipGroupId() {
        return ownershipGroupId;
    }

    //setter
    public void setOwnershipGroupId(Integer ownershipGroupId) {
        this.ownershipGroupId = ownershipGroupId;
    }

    //getter
    @JsonIgnore
    public BigDecimal getPointsMultiplier() {
        return pointsMultiplier;
    }

    //setter
    public void setPointsMultiplier(BigDecimal pointsMultiplier) {
        this.pointsMultiplier = pointsMultiplier;
    }

    //getter
    @JsonIgnore
    public Boolean getIncludeTax() {
        return includeTax;
    }

    //setter
    public void setIncludeTax(Boolean includeTax) {
        this.includeTax = includeTax;
    }

    @JsonIgnore
    public List<LoyaltyAccrualPolicyLevelModel> getLevelDetails() {
        return levelDetails;
    }

    public void setLevelDetails(List<LoyaltyAccrualPolicyLevelModel> levelDetails) {
        this.levelDetails = levelDetails;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    //@JsonIgnore
    //On any json coming into the api, deserialize the object as a TenderDisplayModel
    @JsonDeserialize(as = ArrayList.class, contentAs = TenderDisplayModel.class)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<TenderModel> getTenders() {
        return tenders;
    }

    public void setTenders(List<TenderModel> tenders) {
        this.tenders = tenders;
    }

    @JsonIgnore
    public String getEarningType() {
        return earningType;
    }

    public void setEarningType(String earningType) {
        this.earningType = earningType;
    }

    @JsonIgnore
    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    @JsonIgnore
    public Integer getMonetaryRoundingTypeId() {
        return monetaryRoundingTypeId;
    }

    public void setMonetaryRoundingTypeId(Integer monetaryRoundingTypeId) {
        this.monetaryRoundingTypeId = monetaryRoundingTypeId;
    }

    @JsonIgnore
    public Integer getPointRoundingTypeId() {
        return pointRoundingTypeId;
    }

    public void setPointRoundingTypeId(Integer pointRoundingTypeId) {
        this.pointRoundingTypeId = pointRoundingTypeId;
    }

    @JsonIgnore
    public Integer getMonetaryRoundingPrecisionId() {
        return monetaryRoundingPrecisionId;
    }

    public void setMonetaryRoundingPrecisionId(Integer monetaryRoundingPrecisionId) {
        this.monetaryRoundingPrecisionId = monetaryRoundingPrecisionId;
    }

    @JsonIgnore
    public Integer getEarningTypeId() {
        return earningTypeId;
    }

    public void setEarningTypeId(Integer earningTypeId) {
        this.earningTypeId = earningTypeId;
    }

    @JsonIgnore
    public List<Integer> getMappedRevenueCenterIds() {
        return mappedRevenueCenterIds;
    }

    public void setMappedRevenueCenterIds(List<Integer> mappedRevenueCenterIds) {
        this.mappedRevenueCenterIds = mappedRevenueCenterIds;
    }

    @JsonIgnore
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @JsonIgnore
    public Boolean getEarnPointsForDiscountedProducts() {
        return earnPointsForDiscountedProducts;
    }

    public void setEarnPointsForDiscountedProducts(Boolean earnPointsForDiscountedProducts) {
        this.earnPointsForDiscountedProducts = earnPointsForDiscountedProducts;
    }

    @JsonIgnore
    public Boolean getEarnPointsForMealPlanProducts() {
        return earnPointsForMealPlanProducts;
    }

    public void setEarnPointsForMealPlanProducts(Boolean earnPointsForMealPlanProducts) {
        this.earnPointsForMealPlanProducts = earnPointsForMealPlanProducts;
    }

    @JsonIgnore
    public Boolean getEarnPointsForDonations() {
        return earnPointsForDonations;
    }

    public void setEarnPointsForDonations(Boolean earnPointsForDonations) {
        this.earnPointsForDonations = earnPointsForDonations;
    }

    /**
     * Overridden toString method for a LoyaltyAccrualPolicyModel.
     * @return The {@link String} representation of a LoyaltyAccrualPolicyModel.
     */
    @Override
    public String toString () {

        String levelDetailsStr = "";
        if (!DataFunctions.isEmptyCollection(levelDetails)) {
            levelDetailsStr += "[";
            for (LoyaltyAccrualPolicyLevelModel levelDetail : levelDetails) {
                if (levelDetail != null && levelDetail.equals(levelDetails.get(levelDetails.size() - 1))) {
                    levelDetailsStr += levelDetail.toString();
                }
                else if (levelDetail != null && !levelDetail.equals(levelDetails.get(levelDetails.size() - 1))) {
                    levelDetailsStr += levelDetail.toString() + "; ";
                }
            }
            levelDetailsStr += "]";
        }

        String tendersStr = "";
        if (!DataFunctions.isEmptyCollection(tenders)) {
            tendersStr += "[";
            for (TenderModel tender : tenders) {
                if (tender != null && tender.equals(tenders.get(tenders.size() - 1))) {
                    tendersStr += tender.toString();
                }
                else if (tender != null && !tender.equals(tenders.get(tenders.size() - 1))) {
                    tendersStr += tender.toString() + "; ";
                }
            }
            tendersStr += "]";
        }

        String mappedRevenueCenterIdsStr = "";
        if (!DataFunctions.isEmptyCollection(mappedRevenueCenterIds)) {
            mappedRevenueCenterIdsStr = StringFunctions.buildStringFromList(mappedRevenueCenterIds, ",");
        }

        return String.format("ID: %s, OWNERSHIPGROUPID: %s, TYPEID: %s, MONETARYROUNDINGTYPEID: %s, " +
                "POINTROUNDINGTYPEID: %s, MONETARYROUNDINGPRECISIONID: %s, EARNINGTYPEID: %s, NAME: %s, TYPENAME: %s, " +
                "DESCRIPTION: %s, MONETARYROUNDINGTYPENAME: %s, MONETARYROUNDINGPRECISION: %s, " +
                "POINTROUNDINGTYPENAME: %s, EARNINGTYPE: %s, POINTSMULTIPLIER: %s, INCLUDETAX: %s, ACTIVE: %s, " +
                "EARNPOINTSFORDISCOUNTEDPRODUCTS: %s, EARNPOINTSFORMEALPLANPRODUCTS: %s, LEVELDETAILS: %s, TENDERS: %s, " +
                "MAPPEDREVENUECENTERIDS: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((ownershipGroupId != null && ownershipGroupId > 0 ? ownershipGroupId : "N/A"), "N/A"),
                Objects.toString((typeId != null && typeId > 0 ? typeId : "N/A"), "N/A"),
                Objects.toString((monetaryRoundingTypeId != null && monetaryRoundingTypeId > 0 ? monetaryRoundingTypeId : "N/A"), "N/A"),
                Objects.toString((pointRoundingTypeId != null && pointRoundingTypeId > 0 ? pointRoundingTypeId : "N/A"), "N/A"),
                Objects.toString((monetaryRoundingPrecisionId != null && monetaryRoundingPrecisionId > 0 ? monetaryRoundingPrecisionId : "N/A"), "N/A"),
                Objects.toString((earningTypeId != null && earningTypeId > 0 ? earningTypeId : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(typeName) ? typeName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(monetaryRoundingTypeName) ? monetaryRoundingTypeName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(monetaryRoundingPrecision) ? monetaryRoundingPrecision : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(pointRoundingTypeName) ? pointRoundingTypeName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(earningType) ? earningType : "N/A"), "N/A"),
                Objects.toString((pointsMultiplier != null ? pointsMultiplier.toPlainString() : "N/A"), "N/A"),
                Objects.toString((includeTax != null ? includeTax : "N/A"), "N/A"),
                Objects.toString((active != null ? active : "N/A"), "N/A"),
                Objects.toString((earnPointsForDiscountedProducts != null ? earnPointsForDiscountedProducts : "N/A"), "N/A"),
                Objects.toString((earnPointsForMealPlanProducts != null ? earnPointsForMealPlanProducts : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(levelDetailsStr) ? levelDetailsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(tendersStr) ? tendersStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(mappedRevenueCenterIdsStr ) ? mappedRevenueCenterIdsStr  : "N/A"), "N/A"));

    }

}
