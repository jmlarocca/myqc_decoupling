package com.mmhayes.common.loyalty.models;

//Other Dependencies
import java.math.BigDecimal;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-01-26 12:23:39 -0500 (Fri, 26 Jan 2018) $: Date of last commit
 $Rev: 6147 $: Revision of last commit
*/
public class LoyaltyRoundingTestResult {
    private BigDecimal monetaryRoundingResult = BigDecimal.ZERO;
    private BigDecimal multiplierResult = BigDecimal.ZERO;
    private BigDecimal pointRoundingResult = BigDecimal.ZERO;

    public BigDecimal getMonetaryRoundingResult() {
        return monetaryRoundingResult;
    }

    public void setMonetaryRoundingResult(BigDecimal monetaryRoundingResult) {
        this.monetaryRoundingResult = monetaryRoundingResult;
    }

    public BigDecimal getMultiplierResult() {
        return multiplierResult;
    }

    public void setMultiplierResult(BigDecimal multiplierResult) {
        this.multiplierResult = multiplierResult;
    }

    public BigDecimal getPointRoundingResult() {
        return pointRoundingResult;
    }

    public void setPointRoundingResult(BigDecimal pointRoundingResult) {
        this.pointRoundingResult = pointRoundingResult;
    }
}
