package com.mmhayes.common.loyalty.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.api.errorhandling.exceptions.LoyaltyRewardNotFoundException;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.collections.TaxCollection;
import com.mmhayes.common.transaction.models.TaxModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-12-31 16:39:34 -0500 (Thu, 31 Dec 2020) $: Date of last commit
 $Rev: 13381 $: Revision of last commit
*/
public class LoyaltyRewardModel {
    private Integer id = null;
    private Integer rewardTypeId = null;
    private Integer pointsToRedeem = null;
    private Integer maxPoints = null;
    private Integer revenueCenterId = null;
    private Integer mappedPosItem = null;
    private Integer terminalId = null;
    private Integer maxPerTransaction = null;
    private Integer rewardCalcMethodId = null;

    private String typeName = "";
    private String rewardCalcMethodName = "";
    private String taxIds = "";
    private String shortName = "";
    private String code = "";
    private String name = "";
    private String revenueCenterName = "";

    private BigDecimal value = null;
    private BigDecimal minPurchase = null;
    private BigDecimal maxDiscount = null;

    private boolean redeemAtPos = false;
    private boolean autoPayout = false;
    private boolean autoChooseTerminal = false;
    private boolean applyToAllPlus = false;
    private boolean canUseWithOtherRewards = false;
    private boolean allowProductsOverMaxAmt = false;

    private List<TaxModel> taxes = new ArrayList<>();
    private String notes = "";

    private LoyaltyProgramModel loyaltyProgram = null;

    public LoyaltyRewardModel() {

    }

    public LoyaltyRewardModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM, (Integer)null);
    }

    public LoyaltyRewardModel(HashMap modelDetailHM, Integer terminalId) throws Exception {
        setModelProperties(modelDetailHM, terminalId);
    }

    public LoyaltyRewardModel(final HashMap modelDetailHM, final TerminalModel terminal) throws Exception {
        setModelProperties(modelDetailHM, terminal);
    }

    //setter for all of this model's properties
    private LoyaltyRewardModel setModelProperties(HashMap modelDetailHM, Integer terminalId) throws Exception {
        setModelPropertiesCommon(modelDetailHM);

        if (terminalId == null || !PosAPIModelCache.useTerminalCache) {
            if (this.getLoyaltyProgram() == null) {
                if (CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")) != null) {
                    setLoyaltyProgram(LoyaltyProgramModel.getOneLoyaltyProgram(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")), terminalId));
                }
            }
        } else {
            if (PosAPIModelCache.useTerminalCache) {
                if (this.getLoyaltyProgram() == null) {
                    if (CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")) != null) {
                        LoyaltyProgramModel validatedLoyaltyProgram = null;
                        validatedLoyaltyProgram = (LoyaltyProgramModel) PosAPIModelCache.getOneObject(terminalId, CommonAPI.PosModelType.LOYALTY_PROGRAM, CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")));
                        if (validatedLoyaltyProgram != null) {
                            setLoyaltyProgram(validatedLoyaltyProgram);
                        } else {
                            throw new MissingDataException("Invalid Loyalty Program", terminalId);
                        }
                    }
                }
            }
        }

        if (getTaxIds() != null && !getTaxIds().isEmpty()) {
            List<String> taxIdsStrList = CommonAPI.convertCommaSeparatedStringToArrayList(getTaxIds(), false);
            for (String taxIdStr : taxIdsStrList){
                if(terminalId != null) {
                    TaxModel taxModel = (TaxModel) PosAPIModelCache.getOneObject(terminalId, CommonAPI.PosModelType.TAX, Integer.parseInt(taxIdStr));
                    getTaxes().add(taxModel);
                } else {
                    setTaxes(TaxCollection.getTaxesForReward(getTaxesForReward(), terminalId).getCollection());
                }
            }
        }

        return this;
    }

    private LoyaltyRewardModel setModelProperties(final HashMap modelDetailHM, final TerminalModel terminal) throws Exception {
        setModelPropertiesCommon(modelDetailHM);

        if (terminal == null || terminal.getId() == null || !PosAPIModelCache.useTerminalCache) {
            if (this.getLoyaltyProgram() == null) {
                if (CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")) != null) {
                    setLoyaltyProgram(LoyaltyProgramModel.getOneLoyaltyProgram(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")), terminal.getId()));
                }
            }
        } else {
            if (PosAPIModelCache.useTerminalCache) {
                if (this.getLoyaltyProgram() == null) {
                    if (CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")) != null) {
                        LoyaltyProgramModel validatedLoyaltyProgram = null;
                        validatedLoyaltyProgram = (LoyaltyProgramModel) PosAPIModelCache.getOneObject(terminal.getId(), CommonAPI.PosModelType.LOYALTY_PROGRAM, CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")));
                        if (validatedLoyaltyProgram != null) {
                            setLoyaltyProgram(validatedLoyaltyProgram);
                        } else {
                            throw new MissingDataException("Invalid Loyalty Program", terminal.getId());
                        }
                    }
                }
            }
        }

        if (getTaxIds() != null && !getTaxIds().isEmpty()) {
            try {
                List<String> taxIdsStrList = CommonAPI.convertCommaSeparatedStringToArrayList(getTaxIds(), false);
                for (String taxIdStr : taxIdsStrList){
                    TaxModel taxModel = (TaxModel) PosAPIModelCache.getOneObject(terminalId, CommonAPI.PosModelType.TAX, Integer.parseInt(taxIdStr));
                    getTaxes().add(taxModel);
                }
            }
            catch (Exception ex) {

            }
        }

        return this;
    }

     private void setModelPropertiesCommon(final HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setTypeName(CommonAPI.convertModelDetailToString(modelDetailHM.get("REWARDTYPENAME")));
        setRewardTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYREWARDTYPEID")));
        setRewardCalcMethodName(CommonAPI.convertModelDetailToString(modelDetailHM.get("REWARDCALCMETHODNAME")));
        setRewardCalcMethodId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("REWARDCALCMETHODID")));

        setPointsToRedeem(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POINTSTHRESHOLDMIN")));
        setMaxPoints(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POINTSTHRESHOLDMAX")));
        setValue(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("REWARDVALUE")));
        setRedeemAtPos(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("REDEEMATPOS")));
        setAutoPayout(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("AUTOPAYOUT")));
        setAutoChooseTerminal(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("AUTOCHOOSETERMINAL")));
        setApplyToAllPlus(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("APPLYTOALLPLUS")));
        setTaxIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("TAXIDS")));
        setShortName(CommonAPI.convertModelDetailToString(modelDetailHM.get("SHORTNAME")));
        setCode(CommonAPI.convertModelDetailToString(modelDetailHM.get("CODE")));
        setMinPurchase(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("MINPURCHASE")));
        setMaxDiscount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("MAXDISCOUNT")));
        setTerminalId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALID")));

        setCanUseWithOtherRewards(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("CANUSEWITHOTHERREWARDS")));
        setMaxPerTransaction(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MAXPERTRANSACTION")));
        setAllowProductsOverMaxAmt(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWPRODUCTSOVERMAXIMUM")));

        setRevenueCenterId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("REVENUECENTERID")));
        setRevenueCenterName(CommonAPI.convertModelDetailToString(modelDetailHM.get("REVENUECENTERNAME")));
    }

    public static LoyaltyRewardModel getLoyaltyRewardModel(Integer loyaltyRewardId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> loyaltyRewardList = dm.parameterizedExecuteQuery("data.posapi30.getOneLoyaltyRewardById",
                new Object[]{loyaltyRewardId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(loyaltyRewardList, "Loyalty Reward Program Not Found", terminalId);
        //CommonAPI.checkMoreThanOneRecord(loyaltyRewardList, "More than 1 Loyalty Reward found");

        return LoyaltyRewardModel.createLoyaltyRewardModel(loyaltyRewardList.get(0), terminalId);
    }

    public static LoyaltyRewardModel getActiveLoyaltyRewardModel(Integer loyaltyRewardId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> loyaltyRewardList = dm.parameterizedExecuteQuery("data.posapi30.getOneActiveLoyaltyRewardById",
                new Object[]{loyaltyRewardId, terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        if (loyaltyRewardList == null || loyaltyRewardList.isEmpty()) {
            throw new LoyaltyRewardNotFoundException("Loyalty Reward Program Not Found", terminalModel.getId());
        }

        //return LoyaltyRewardModel.createLoyaltyRewardModel(loyaltyRewardList.get(0), terminalModel.getId());
        return LoyaltyRewardModel.createLoyaltyRewardModel(loyaltyRewardList.get(0), terminalModel);
    }

    public static LoyaltyRewardModel getLoyaltyRewardModel(String loyaltyRewardName, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> loyaltyRewardList = dm.parameterizedExecuteQuery("data.posapi30.getOneLoyaltyRewardByName",
                new Object[]{loyaltyRewardName},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(loyaltyRewardList, "Loyalty Reward Program Not Found", terminalId);
        return LoyaltyRewardModel.createLoyaltyRewardModel(loyaltyRewardList.get(0), terminalId);
    }

    public static LoyaltyRewardModel createLoyaltyRewardModel(HashMap modelHM) throws Exception {
        return new LoyaltyRewardModel(modelHM);
    }

    public static LoyaltyRewardModel createLoyaltyRewardModel(HashMap modelHM, Integer terminalId) throws Exception {
        return new LoyaltyRewardModel(modelHM, terminalId);
    }

    public static LoyaltyRewardModel createLoyaltyRewardModel(HashMap modelHM, TerminalModel terminal) throws Exception {
        return new LoyaltyRewardModel(modelHM, terminal);
    }

    //parse out the TaxIds field in the db to get the taxes available on the reward
    private String getTaxesForReward() throws Exception {
        StringBuilder sb = new StringBuilder();
        String[] taxIds = this.getTaxIds().split(",");

        if (taxIds != null && taxIds.length > 0) {
            for (Integer iCount = 0; iCount < taxIds.length; iCount++) {
                sb.append(taxIds[iCount]);
                if (iCount != taxIds.length - 1) {
                    sb.append(",");
                }
            }

        } else {
            sb.append("''"); //add a blank string in for the name
        }

        return sb.toString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public String getRewardCalcMethodName() {
        return rewardCalcMethodName;
    }

    public void setRewardCalcMethodName(String rewardCalcMethodName) {
        this.rewardCalcMethodName = rewardCalcMethodName;
    }

    @JsonIgnore
    public Integer getRevenueCenterId() {
        return revenueCenterId;
    }

    public void setRevenueCenterId(Integer revenueCenterId) {
        this.revenueCenterId = revenueCenterId;
    }

    @JsonIgnore
    public String getRevenueCenterName() {
        return revenueCenterName;
    }

    public void setRevenueCenterName(String revenueCenterName) {
        this.revenueCenterName = revenueCenterName;
    }

    //@JsonIgnore
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getPointsToRedeem() {
        return pointsToRedeem;
    }

    public void setPointsToRedeem(Integer pointsToRedeem) {
        this.pointsToRedeem = pointsToRedeem;
    }

    @JsonIgnore
    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }

    @JsonIgnore
    public boolean isRedeemAtPos() {
        return redeemAtPos;
    }

    public void setRedeemAtPos(boolean redeemAtPos) {
        this.redeemAtPos = redeemAtPos;
    }

    //@JsonIgnore
    public boolean isAutoPayout() {
        return autoPayout;
    }

    public void setAutoPayout(boolean autoPayout) {
        this.autoPayout = autoPayout;
    }

    @JsonIgnore
    public boolean isAutoChooseTerminal() {
        return autoChooseTerminal;
    }

    public void setAutoChooseTerminal(boolean autoChooseTerminal) {
        this.autoChooseTerminal = autoChooseTerminal;
    }

    @JsonIgnore
    public boolean isApplyToAllPlus() {
        return applyToAllPlus;
    }

    public void setApplyToAllPlus(boolean applyToAllPlus) {
        this.applyToAllPlus = applyToAllPlus;
    }

    @JsonIgnore
    public String getTaxIds() {
        return taxIds;
    }

    public void setTaxIds(String taxIds) {
        this.taxIds = taxIds;
    }

    //@JsonIgnore
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @JsonIgnore
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @JsonIgnore
    public BigDecimal getMinPurchase() {
        return minPurchase;
    }

    public void setMinPurchase(BigDecimal minPurchase) {
        this.minPurchase = minPurchase;
    }

    @JsonIgnore
    public BigDecimal getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(BigDecimal maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    @JsonIgnore
    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    @JsonIgnore
    public Integer getMappedPosItem() {
        return mappedPosItem;
    }

    public void setMappedPosItem(Integer mappedPosItem) {
        this.mappedPosItem = mappedPosItem;
    }

//    @JsonIgnore
    public Integer getRewardTypeId() {
        return rewardTypeId;
    }

    public void setRewardTypeId(Integer rewardTypeId) {
        this.rewardTypeId = rewardTypeId;
    }

    @JsonIgnore
    public LoyaltyProgramModel getLoyaltyProgram() {
        return loyaltyProgram;
    }

    public void setLoyaltyProgram(LoyaltyProgramModel loyaltyProgram) {
        this.loyaltyProgram = loyaltyProgram;
    }

//    @JsonIgnore
    public Integer getMaxPerTransaction() {
        return maxPerTransaction;
    }

    public void setMaxPerTransaction(Integer maxPerTransaction) {
        this.maxPerTransaction = maxPerTransaction;
    }

//    @JsonIgnore
    @JsonGetter("canUseWithOtherRewards")
    public boolean canUseWithOtherRewards() {
        return canUseWithOtherRewards;
    }

    public void setCanUseWithOtherRewards(boolean canUseWithOtherRewards) {
        this.canUseWithOtherRewards = canUseWithOtherRewards;
    }

    public List<TaxModel> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<TaxModel> taxes) {
        this.taxes = taxes;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @JsonIgnore
    public boolean isAllowProductsOverMaxAmt() {
        return allowProductsOverMaxAmt;
    }

    public void setAllowProductsOverMaxAmt(boolean allowProductsOverMaxAmt) {
        this.allowProductsOverMaxAmt = allowProductsOverMaxAmt;
    }

    @JsonIgnore
    public Integer getRewardCalcMethodId() {
        return rewardCalcMethodId;
    }

    public void setRewardCalcMethodId(Integer rewardCalcMethodId) {
        this.rewardCalcMethodId = rewardCalcMethodId;
    }

    /**
     * Overridden toString method for a LoyaltyRewardModel.
     * @return The {@link String} representation of a LoyaltyRewardModel.
     */
    @Override
    public String toString () {
        
        String taxesStr = "";
        if (!DataFunctions.isEmptyCollection(taxes)) {
            taxesStr += "[";
            for (TaxModel tax : taxes) {
                if (tax != null && tax.equals(taxes.get(taxes.size() - 1))) {
                    taxesStr += tax.toString();
                }
                else if (tax != null && !tax.equals(taxes.get(taxes.size() - 1))) {
                    taxesStr += tax.toString() + "; ";
                }
            }
            taxesStr += "]";
        }

        return String.format("ID: %s, REWARDTYPEID: %s, POINTSTOREDEEM: %s, MAXPOINTS: %s, REVENUECENTERID: %s, " +
                "MAPPEDPOSITEM: %s, TERMINALID: %s, MAXPERTRANSACTION: %s, REWARDCALCMETHODID: %s, TYPENAME: %s, " +
                "REWARDCALCMETHODNAME: %s, TAXIDS: %s, SHORTNAME: %s, CODE: %s, NAME: %s, REVENUECENTERNAME: %s, " +
                "VALUE: %s, MINPURCHASE: %s, MAXDISCOUNT: %s, REDEEMATPOS: %s, AUTOPAYOUT: %s, AUTOCHOOSETERMINAL: %s, " +
                "APPLYTOALLPLUS: %s, CANUSEWITHOTHERREWARDS: %s, ALLOWPRODUCTSOVERMAXAMT: %s, TAXES: %s, NOTES: %s, " +
                "LOYALTYPROGRAM: %s",
                Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((rewardTypeId != null && rewardTypeId > 0 ? rewardTypeId : "N/A"), "N/A"),
                Objects.toString((pointsToRedeem != null && pointsToRedeem > 0 ? pointsToRedeem : "N/A"), "N/A"),
                Objects.toString((maxPoints != null && maxPoints > 0 ? maxPoints : "N/A"), "N/A"),
                Objects.toString((revenueCenterId != null && revenueCenterId > 0 ? revenueCenterId : "N/A"), "N/A"),
                Objects.toString((mappedPosItem != null && mappedPosItem > 0 ? mappedPosItem : "N/A"), "N/A"),
                Objects.toString((terminalId != null && terminalId > 0 ? terminalId : "N/A"), "N/A"),
                Objects.toString((maxPerTransaction != null && maxPerTransaction > 0 ? maxPerTransaction : "N/A"), "N/A"),
                Objects.toString((rewardCalcMethodId != null && rewardCalcMethodId > 0 ? rewardCalcMethodId : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(typeName) ? typeName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(rewardCalcMethodName) ? rewardCalcMethodName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(taxIds) ? taxIds : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(code) ? code : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(revenueCenterName) ? revenueCenterName : "N/A"), "N/A"),
                Objects.toString((value != null ? value.toPlainString() : "N/A"), "N/A"),
                Objects.toString((minPurchase != null ? minPurchase.toPlainString() : "N/A"), "N/A"),
                Objects.toString((maxDiscount != null ? maxDiscount.toPlainString() : "N/A"), "N/A"),
                Objects.toString(redeemAtPos, "N/A"),
                Objects.toString(autoPayout, "N/A"),
                Objects.toString(autoChooseTerminal, "N/A"),
                Objects.toString(applyToAllPlus, "N/A"),
                Objects.toString(canUseWithOtherRewards, "N/A"),
                Objects.toString(allowProductsOverMaxAmt, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(taxesStr) ? taxesStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(notes) ? notes : "N/A"), "N/A"),
                Objects.toString((loyaltyProgram != null ? loyaltyProgram.toString() : "N/A"), "N/A"));

    }

}
