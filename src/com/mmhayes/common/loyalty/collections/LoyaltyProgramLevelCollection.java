package com.mmhayes.common.loyalty.collections;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.loyalty.models.LoyaltyProgramLevelModel;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-12-04 12:26:20 -0500 (Tue, 04 Dec 2018) $: Date of last commit
 $Rev: 8158 $: Revision of last commit
*/
@Deprecated
public class LoyaltyProgramLevelCollection {
    private List<LoyaltyProgramLevelModel> collection = new ArrayList<>();

    public LoyaltyProgramLevelCollection() {

    }

    public static LoyaltyProgramLevelCollection getAllLoyaltyProgramLevels(String logFileName) throws Exception {
        DataManager dm = new DataManager();
        LoyaltyProgramLevelCollection loyaltyProgramLevelCollection = new LoyaltyProgramLevelCollection();

        //get all models in an array list
        ArrayList<HashMap> LoyaltyProgramLevelList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyProgramLevels",
                new Object[]{},
                logFileName,
                true
        );

        //product list is null or empty, throw exception
        //CommonAPI.checkIsNullOrEmptyList(LoyaltyProgramLevelList, CommonAPI.PosModelType.LOYALTY_PROGRAM);
        for (HashMap loyaltyProgramLevelHM : LoyaltyProgramLevelList) {
            //CommonAPI.checkIsNullOrEmptyObject(loyaltyProgramHM.get("ID"), "LoyaltyProgramId Id could not be found");
            //create a new model and add to the collection
            loyaltyProgramLevelCollection.getCollection().add(LoyaltyProgramLevelModel.createLoyaltyProgramLevelModel(loyaltyProgramLevelHM));
        }


        return loyaltyProgramLevelCollection;
    }

    public static LoyaltyProgramLevelCollection getAllLoyaltyProgramLevelsByProgramId(Integer programId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        LoyaltyProgramLevelCollection loyaltyProgramLevelCollection = new LoyaltyProgramLevelCollection();

        //get all models in an array list
        ArrayList<HashMap> LoyaltyProgramLevelList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyProgramLevelsByProgramId",
                new Object[]{programId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap loyaltyProgramLevelHM : LoyaltyProgramLevelList) {
            LoyaltyProgramLevelModel loyaltyProgramLevelModel = LoyaltyProgramLevelModel.createLoyaltyProgramLevelModel(loyaltyProgramLevelHM);
            if (loyaltyProgramLevelModel != null) {
                loyaltyProgramLevelCollection.getCollection().add(loyaltyProgramLevelModel);
            }

        }

        return loyaltyProgramLevelCollection;
    }

    public List<LoyaltyProgramLevelModel> getCollection() {
        return collection;
    }

    public void setCollection(List<LoyaltyProgramLevelModel> collection) {
        this.collection = collection;
    }

}