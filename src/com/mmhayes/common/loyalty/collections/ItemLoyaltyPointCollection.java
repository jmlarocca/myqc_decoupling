package com.mmhayes.common.loyalty.collections;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.loyalty.models.ItemLoyaltyPointModel;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-12-04 12:26:20 -0500 (Tue, 04 Dec 2018) $: Date of last commit
 $Rev: 8158 $: Revision of last commit
*/
public class ItemLoyaltyPointCollection {
    private ArrayList<ItemLoyaltyPointModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    //constructor
    public ItemLoyaltyPointCollection() {

    }

    //get all ItemLoyaltyPoints for a specific trans line item
    public static ItemLoyaltyPointCollection getAllItemLoyaltyPointsByTransLineId(Integer transLineItemId, Integer terminalId) throws Exception {
        ItemLoyaltyPointCollection itemLoyaltyPointCollection = new ItemLoyaltyPointCollection();

        //get all models in an array list
        ArrayList<HashMap> itemLoyaltyPointList = dm.parameterizedExecuteQuery("data.posapi30.getAllItemLoyaltyPointsByTransLineId",
                new Object[]{transLineItemId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap itemLoyaltyPointHM : itemLoyaltyPointList) {
            CommonAPI.checkIsNullOrEmptyObject(itemLoyaltyPointHM.get("ID"), "Item Loyalty Point Id could not be found", terminalId);
            //create a new model and add to the collection
            itemLoyaltyPointCollection.getCollection().add(new ItemLoyaltyPointModel(itemLoyaltyPointHM));
        }

        return itemLoyaltyPointCollection;
    }

    public static ItemLoyaltyPointCollection getAllItemLoyaltyPointsAndDetailsByTransLineId(Integer transLineItemId, Integer terminalId) throws Exception {
        ItemLoyaltyPointCollection itemLoyaltyPointCollection = new ItemLoyaltyPointCollection();

        //get all models in an array list
        ArrayList<HashMap> itemLoyaltyPointList = dm.parameterizedExecuteQuery("data.posapi30.getAllItemLoyaltyPointsAndDetailsByTransLineId",
                new Object[]{transLineItemId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap itemLoyaltyPointHM : itemLoyaltyPointList) {
            CommonAPI.checkIsNullOrEmptyObject(itemLoyaltyPointHM.get("ID"), "Item Loyalty Point Id could not be found", terminalId);
            //create a new model and add to the collection
            itemLoyaltyPointCollection.getCollection().add(new ItemLoyaltyPointModel(itemLoyaltyPointHM));
        }

        return itemLoyaltyPointCollection;
    }

    //get all ItemLoyaltyPoints for a specific trans line item
    public static ItemLoyaltyPointCollection getAllItemLoyaltyPointsByLoyaltyPointId(Integer loyaltyPointId, Integer terminalId) throws Exception {
        ItemLoyaltyPointCollection itemLoyaltyPointCollection = new ItemLoyaltyPointCollection();

        //get all models in an array list
        ArrayList<HashMap> itemLoyaltyPointList = dm.parameterizedExecuteQuery("data.posapi30.getAllItemLoyaltyPointsByLoyaltyPointId",
                new Object[]{loyaltyPointId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap itemLoyaltyPointHM : itemLoyaltyPointList) {
            CommonAPI.checkIsNullOrEmptyObject(itemLoyaltyPointHM.get("ID"), "Item Loyalty Point Id could not be found", terminalId);
            //create a new model and add to the collection
            itemLoyaltyPointCollection.getCollection().add(new ItemLoyaltyPointModel(itemLoyaltyPointHM));
        }

        return itemLoyaltyPointCollection;
    }


    //getter
    public ArrayList<ItemLoyaltyPointModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(ArrayList<ItemLoyaltyPointModel> collection) {
        this.collection = collection;
    }
}
