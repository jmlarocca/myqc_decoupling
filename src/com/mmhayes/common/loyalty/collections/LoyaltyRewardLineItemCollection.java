package com.mmhayes.common.loyalty.collections;

//mmhayes dependencies

import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.transaction.models.*;

//other dependencies
import java.util.ArrayList;
import java.util.List;

/*
 $Author: eglundin $: Author of last commit
 $Date: 2017-04-06 17:05:07 -0400 (Thu, 06 Apr 2017) $: Date of last commit
 $Rev: 3798 $: Revision of last commit
 Notes:
*/
public class LoyaltyRewardLineItemCollection {

    private List<LoyaltyRewardLineItemModel> collection = new ArrayList<LoyaltyRewardLineItemModel>();
    private static DataManager dm = new DataManager();

    public LoyaltyRewardLineItemCollection() {

    }

    public List<LoyaltyRewardLineItemModel> getCollection() {
        return collection;
    }

    public void setCollection(List<LoyaltyRewardLineItemModel> collection) {
        this.collection = collection;
    }

}
