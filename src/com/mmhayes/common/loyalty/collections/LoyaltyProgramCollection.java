package com.mmhayes.common.loyalty.collections;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.loyalty.models.*;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-04-12 16:32:05 -0400 (Fri, 12 Apr 2019) $: Date of last commit
 $Rev: 8715 $: Revision of last commit
*/
public class LoyaltyProgramCollection {
    private List<LoyaltyProgramModel> collection = new ArrayList<LoyaltyProgramModel>();
    private static DataManager dm = new DataManager();

    //default constructor - used by the API
    public LoyaltyProgramCollection() {
    }

    public static LoyaltyProgramCollection getAllActiveLoyaltyPrograms(TerminalModel terminalModel) throws Exception {
        LoyaltyProgramCollection loyaltyProgramCollection = new LoyaltyProgramCollection();

        //get all models in an array list
        ArrayList<HashMap> loyaltyProgramList = dm.parameterizedExecuteQuery("data.posapi30.getAllActiveLoyaltyPrograms",
                new Object[]{terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(loyaltyProgramList, CommonAPI.PosModelType.LOYALTY_PROGRAM, "", terminalModel.getId());


        //map the Loyalty Programs
        //and their valid tenders at the same time
        Integer tempId = null;
        LoyaltyProgramModel loyaltyProgramModel = null;
        for (HashMap loyaltyProgramHM : loyaltyProgramList) {
            CommonAPI.checkIsNullOrEmptyObject(loyaltyProgramHM.get("ID"), "LoyaltyProgramId Id could not be found", terminalModel.getId());

            if (!CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID")).equals(tempId)) {
                loyaltyProgramModel = new LoyaltyProgramModel(loyaltyProgramHM, terminalModel.getId(), false);
                loyaltyProgramCollection.getCollection().add(loyaltyProgramModel);  //create a new model and add to the collection
            }

            if (CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("PATENDERID")) != null
                    && !CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("PATENDERID")).toString().isEmpty()) {
                loyaltyProgramModel.getTenders().add(new TenderModel(loyaltyProgramHM));
            }
            tempId = CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID"));
        }

        return loyaltyProgramCollection;
    }

    public static LoyaltyProgramCollection getAllLoyaltyProgramsForRevenueCentersAndTenders(TerminalModel terminalModel, String tenderNameList) throws Exception {
        DataManager dm = new DataManager();
        LoyaltyProgramCollection loyaltyProgramCollection = new LoyaltyProgramCollection();
        ArrayList loyaltyProgramList = new ArrayList();

        //EGL 09-07-2016 - used the serializeSqlWithColNames in order to use an "IN" clause
        loyaltyProgramList = dm.serializeSqlWithColNames("data.posapi30.getAllLoyaltyProgramsForRevenueCentersAndTendersAP", new Object[]{terminalModel.getRevenueCenterId(), tenderNameList}, null, PosAPIHelper.getLogFileName(terminalModel.getId()));

        //product list is null or empty, throw exception
        List<HashMap> listHM = (List<HashMap>) loyaltyProgramList;

        //map the Loyalty Programs
        //and their valid tenders at the same time
        Integer tempId = null;
        LoyaltyProgramModel loyaltyProgramModel = null;
        for (HashMap loyaltyProgramHM : listHM) {
            CommonAPI.checkIsNullOrEmptyObject(loyaltyProgramHM.get("ID"), "LoyaltyProgramId Id could not be found", terminalModel.getId());

            if (PosAPIModelCache.useTerminalCache) {
                if (CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID")) != null) {
                    LoyaltyProgramModel validatedLoyaltyProgram = null;
                    validatedLoyaltyProgram = (LoyaltyProgramModel) PosAPIModelCache.getOneObject(terminalModel.getId(), CommonAPI.PosModelType.LOYALTY_PROGRAM, CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID")));
                    if (validatedLoyaltyProgram != null) {
                        loyaltyProgramCollection.getCollection().add(validatedLoyaltyProgram);
                    } else {
                        throw new MissingDataException("Invalid Loyalty Program", terminalModel.getId());
                    }
                }
            } else {
                if (!CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID")).equals(tempId)) {
                    loyaltyProgramModel = new LoyaltyProgramModel(loyaltyProgramHM, terminalModel.getId(), false);
                    loyaltyProgramCollection.getCollection().add(loyaltyProgramModel);  //create a new model and add to the collection
                }

                loyaltyProgramModel.getTenders().add(new TenderDisplayModel(loyaltyProgramHM));
                tempId = CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID"));
            }
        }

        return loyaltyProgramCollection;
    }

    /**
     * Give me all the Loyalty Programs in one query.  This query fetches the Tenders, and Product Levels for each Loyalty Program.
     * I did it this way in order to fetch all this from the database in one query
     * In one example using this cache cut down the queries to the database from 40 to 15     *
     * @param terminalModel : pass in the Terminal Model so the subdepartments and departments can be filtered on Revenue Center
     * @return a list of all the Loyalty Programs
     * @throws Exception
     */
    public static LoyaltyProgramCollection getAllLoyaltyProgramsForCache(TerminalModel terminalModel) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> loyaltyProgramList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyProgramsForCacheAP",
                new Object[]{terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        LoyaltyProgramCollection loyaltyProgramCollection = mapLoyaltyProgramsForCacheAP(loyaltyProgramList, terminalModel);
        return loyaltyProgramCollection;
    }

    /**
     * Map all the Loyalty Programs returned by the "getAllActiveLoyaltyProgramsForCache"
     * This method maps the Loyalty Program, Tenders, and Loyalty Program Levels
     * I did it this way in order to fetch all this from the database in one query
     * @param loyaltyProgramList
     * @param terminalModel
     * @return
     * @throws Exception
     */
    @Deprecated
    private static LoyaltyProgramCollection mapLoyaltyProgramsForCache(ArrayList<HashMap> loyaltyProgramList, TerminalModel terminalModel) throws Exception {
        LoyaltyProgramCollection loyaltyProgramCollection = new LoyaltyProgramCollection();

        //map the Loyalty Programs
        //and their valid tenders at the same time
        Integer tempId = null;
        Integer tempTenderId = null;
        Integer tempPayrollGroupingId = null;
        LoyaltyProgramModel loyaltyProgramModel = null;
        ArrayList<Integer> programLevelList = new ArrayList<>();

        for (HashMap loyaltyProgramHM : loyaltyProgramList) {
            CommonAPI.checkIsNullOrEmptyObject(loyaltyProgramHM.get("ID"), "LoyaltyProgramId Id could not be found", terminalModel.getId());

            if (!CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID")).equals(tempId)) {
                tempTenderId = null;
                tempPayrollGroupingId = null;
                loyaltyProgramModel = new LoyaltyProgramModel(loyaltyProgramHM, terminalModel.getId(), true);
                loyaltyProgramCollection.getCollection().add(loyaltyProgramModel);  //create a new model and add to the collection
                programLevelList = new ArrayList<>();
            }

            //Map the Tenders for each Loyalty Program
            if (CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("PATENDERID")) != null) {
                if (!CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("PATENDERID")).equals(tempTenderId)) {
                    loyaltyProgramModel.getTenders().add(new TenderDisplayModel(loyaltyProgramHM));
                    tempTenderId = CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("PATENDERID"));
                }
            }

            //Map the Payroll Grouping's (Account Groups) for each Loyalty Program
            if (CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("PAYROLLGROUPINGID")) != null) {
                if (!CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("PAYROLLGROUPINGID")).equals(tempPayrollGroupingId)) {
                    loyaltyProgramModel.getPayrollGroupingIds().add(CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("PAYROLLGROUPINGID")));
                    tempPayrollGroupingId = CommonAPI.convertModelDetailToInteger(CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("PAYROLLGROUPINGID")));
                }
            }

            //Map the Loyalty Program Levels for Loyalty Programs of "Product Level"
            if (CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("PROGRAMTYPENAME").toString().toUpperCase()).equals("PRODUCT LEVEL")) {
                if (CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("LOYALTYPROGRAMLEVELID")) != null) {

                    if (programLevelList.contains(CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("LOYALTYPROGRAMLEVELID")))) {
                        tempId = CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID"));
                        continue;
                    }

                    //if the product, subDepartment, and department are all blank then don't add the level
                    if (CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("PRODUCTNAME")).isEmpty()
                            && CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("SUBDEPARTMENTNAME")).isEmpty()
                            && CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("DEPARTMENTNAME")).isEmpty()) {
                        tempId = CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID"));
                        continue; //not valid
                    }

                    //if the product is found, but there is not a revenue center mapping.
                    if (!CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("PRODUCTNAME")).isEmpty()
                            && CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("PRODUCTREVCENTERMAPPINGID")).isEmpty()) {
                        tempId = CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID"));
                        continue;
                    }

                    LoyaltyProgramLevelModel loyaltyProgramLevelModel = LoyaltyProgramLevelModel.createLoyaltyProgramLevelModel(loyaltyProgramHM);
                    loyaltyProgramModel.getLevelDetails().add(loyaltyProgramLevelModel);
                    programLevelList.add(CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("LOYALTYPROGRAMLEVELID")));
                }
            }

            tempId = CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID"));
        }

        return loyaltyProgramCollection;

    }

    /**
     * Map all the Loyalty Programs returned by the "getAllActiveLoyaltyProgramsForCache"
     * This method maps the Loyalty Program, Tenders
     * I did it this way in order to fetch all this from the database in one query.
     * This was updated for Accrual Policies.
     *
     * @param loyaltyProgramList
     * @param terminalModel
     * @return
     * @throws Exception
     */
    private static LoyaltyProgramCollection mapLoyaltyProgramsForCacheAP(ArrayList<HashMap> loyaltyProgramList, TerminalModel terminalModel) throws Exception {
        LoyaltyProgramCollection loyaltyProgramCollection = new LoyaltyProgramCollection();

        //map the Loyalty Programs
        //and their valid tenders at the same time
        Integer tempId = null;
        LoyaltyProgramModel loyaltyProgramModel = null;
        LoyaltyAccrualPolicyModel loyaltyAccrualPolicyModel = null;
        ArrayList<Integer> payrollGroupingIds = new ArrayList<>();
        Boolean loyaltyProgramPayrollGroupChecked = false;
        Boolean loyaltyAccrualPolicyRevCenterChecked = false;
        Boolean loyaltyAccrualPolicyTenderChecked = false;

        for (HashMap loyaltyProgramHM : loyaltyProgramList) {
            CommonAPI.checkIsNullOrEmptyObject(loyaltyProgramHM.get("ID"), "LoyaltyProgramId Id could not be found", terminalModel.getId());

            if (!CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID")).equals(tempId)) {
                loyaltyProgramModel = new LoyaltyProgramModel(loyaltyProgramHM, terminalModel.getId(), true);
                loyaltyProgramCollection.getCollection().add(loyaltyProgramModel);  //create a new model and add to the collection
                payrollGroupingIds = new ArrayList<>();
                loyaltyProgramPayrollGroupChecked = false;
            }

            //Map the Payroll Grouping's (Account Groups) for each Loyalty Program
            if (!loyaltyProgramPayrollGroupChecked) {
                if (CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("LOYALTYPROGRAMPAYROLLGRPIDSTR")) != null) {
                    String payrollGroupMappingsStr = CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("LOYALTYPROGRAMPAYROLLGRPIDSTR"));
                    ArrayList<String> payrollGroupMappingIds = CommonAPI.convertCommaSeparatedStringToArrayList(payrollGroupMappingsStr, false);

                    for (String grpMappingStr : payrollGroupMappingIds) {
                        payrollGroupingIds.add(Integer.parseInt(grpMappingStr));
                    }
                    loyaltyProgramModel.setPayrollGroupingIds(payrollGroupingIds);
                    loyaltyProgramPayrollGroupChecked = true;
                }
            }

            //accrual Policy
            if (loyaltyProgramHM.get("LOYALTYACCRUALPOLICYID") != null && !loyaltyProgramHM.get("LOYALTYACCRUALPOLICYID").toString().isEmpty()) {
                loyaltyAccrualPolicyModel = new LoyaltyAccrualPolicyModel(loyaltyProgramHM, terminalModel.getId(), true);
                loyaltyProgramModel.getAccrualPolicies().add(loyaltyAccrualPolicyModel);
                loyaltyAccrualPolicyRevCenterChecked = false;
                loyaltyAccrualPolicyTenderChecked = false;
            }

            if (!loyaltyAccrualPolicyTenderChecked){
                //Map the Tenders for each Loyalty Accrual Policy
                if (CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("ACCRUALPOLICYTENDERMAPIDSTR")) != null) {
                    String accrualPolicyTenderMappingsStr = CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("ACCRUALPOLICYTENDERMAPIDSTR"));
                    ArrayList<String> accrualPolicyTenderMappingIds = CommonAPI.convertCommaSeparatedStringToArrayList(accrualPolicyTenderMappingsStr, false);

                    for (String tenderIdStr : accrualPolicyTenderMappingIds) {
                        Integer tenderId = Integer.parseInt(tenderIdStr);
                        TenderDisplayModel tenderDisplayModel = new TenderDisplayModel(tenderId);
                        loyaltyAccrualPolicyModel.getTenders().add(tenderDisplayModel); //Add the Tender mapping to the Loyalty Accrual Policy

                        //Add the Tender mapping to the Loyalty Program
                        boolean addTenderToProgram = true;
                        for (TenderModel tenderModel : loyaltyProgramModel.getTenders()) {
                            if (tenderModel.getId().equals(tenderId)) {
                                addTenderToProgram = false;
                                break;
                            }
                        }
                        if (addTenderToProgram) {
                            loyaltyProgramModel.getTenders().add(tenderDisplayModel);
                        }
                    }

                    loyaltyAccrualPolicyTenderChecked = true;
                }
            }

            //Map the revenue centers for each Loyalty Accrual Policy
            if (!loyaltyAccrualPolicyRevCenterChecked) {
                if (CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("ACCRUALPOLICYREVCENTERMAPIDSTR")) != null) {
                    String accrualPolicyRevCenterMappingsStr = CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("ACCRUALPOLICYREVCENTERMAPIDSTR"));
                    ArrayList<String> accrualPolicyRevCenterMappingIds = CommonAPI.convertCommaSeparatedStringToArrayList(accrualPolicyRevCenterMappingsStr, false);

                    for (String revCenterMapId : accrualPolicyRevCenterMappingIds) {  //Add the Revenue Center mappings to the Loyalty Accrual Policy
                        loyaltyAccrualPolicyModel.getMappedRevenueCenterIds().add(Integer.parseInt(revCenterMapId));
                    }
                    loyaltyAccrualPolicyRevCenterChecked = true;
                }
            }

            tempId = CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("ID"));
        }

        return loyaltyProgramCollection;
    }

    //get all Loyalty Programs for the transaction's revenue center and tenders
    //sends a comma delimited list as a parameter
    public static LoyaltyProgramCollection getValidLoyaltyPrograms(TransactionModel transactionModel) throws Exception {
        String tenderNameList = getTendersForLoyalty(transactionModel);

        return LoyaltyProgramCollection.getAllLoyaltyProgramsForRevenueCentersAndTenders(transactionModel.getTerminal(), tenderNameList);
    }

    //gets a comma separated list of all the tender names in the group of transactions
    private static String getTendersForLoyalty(TransactionModel transactionModel) throws Exception {
        List<TenderLineItemModel> tenderTransactionLineList = transactionModel.getTenders();

        StringBuilder sb = new StringBuilder();

        if (tenderTransactionLineList != null && !tenderTransactionLineList.isEmpty()) {
            //loop through tender list to get the tender name
            for (TenderLineItemModel tenderLineItemModel : tenderTransactionLineList) {

                sb.append("'");
                sb.append(tenderLineItemModel.getTender().getName());
                sb.append("'");

                if (tenderLineItemModel != tenderTransactionLineList.get(tenderTransactionLineList.size() - 1)) {
                    sb.append(",");
                }
            }
        } else {
            sb.append("''"); //add a blank string in for the name
        }

        return sb.toString();
    }

    public List<LoyaltyProgramModel> getCollection() {
        return collection;
    }

    public void setCollection(List<LoyaltyProgramModel> collection) {
        this.collection = collection;
    }

}
