package com.mmhayes.common.loyalty.collections;

//mmhayes dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.loyalty.models.*;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;

import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.api.ModelPaginator;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-08-29 11:12:59 -0400 (Wed, 29 Aug 2018) $: Date of last commit
 $Rev: 7701 $: Revision of last commit
*/
public class LoyaltyAccountCollection {
    private List<ILoyaltyAccountModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();
    private String authenticatedMacAddress = null;

    //default constructor
    public LoyaltyAccountCollection() {
    }

    //populates this collection with models
    private LoyaltyAccountCollection populateCollection(Integer accountID, HttpServletRequest request, TerminalModel terminalModel) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> accountList = dm.parameterizedExecuteQuery("data.posapi30.getOneAccountByID",
                new Object[]{
                        accountID
                },
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(accountList, "Account List is empty", terminalModel.getId());
        for (HashMap accountHM : accountList) {
            CommonAPI.checkIsNullOrEmptyObject(accountHM.get("ID"), "Account Id could not be found", terminalModel.getId());
            //create a new model and add to the collection
            getCollection().add(new LoyaltyAccountModel(AccountModel.createNewAccount(accountHM, terminalModel), true));
        }

        return this;
    }

    //gets all accounts from the database
    public static LoyaltyAccountCollection getAllAccounts(TerminalModel terminalModel) throws Exception {
        CommonAPI.checkIsNullOrEmptyObject(terminalModel.getId(), CommonAPI.PosModelType.TERMINAL, terminalModel.getId());
        LoyaltyAccountCollection accountCollection = new LoyaltyAccountCollection();

        //get all models in an array list
        ArrayList<HashMap> accountList = dm.parameterizedExecuteQuery("data.posapi30.getAllAccounts",
                new Object[]{terminalModel.getId()},
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(accountList, CommonAPI.PosModelType.ACCOUNT, "Cannot find Account for this Terminal", terminalModel.getId());
        for (HashMap accountHM : accountList) {
            //CommonAPI.checkIsNullOrEmptyObject(accountHM.get("ID"), "Product Id could not be found");
            //create a new model and add to the collection
            accountCollection.getCollection().add(new LoyaltyAccountModel(AccountModel.createNewAccount(accountHM, terminalModel), true));
        }

        return accountCollection;
    }


    //constructor - populates the collection
    public LoyaltyAccountCollection(HashMap<String, LinkedList> paginationParamsHM, HttpServletRequest request, TerminalModel terminalModel) throws Exception {

        //determine the employeeID from the request
        //setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection(new ModelPaginator(paginationParamsHM), terminalModel);

    }

    //populates this collection with models
    public List<ILoyaltyAccountModel> populateCollection(ModelPaginator modelPaginator, TerminalModel terminalModel) throws Exception {

        //set the modelCount so we can finalize the pagination properties

        //determines, sets and validates pagination properties
        modelPaginator.finalizePaginationProperties();

        String sortByColumn = ""; //what column should this result set be sorted by
        if (modelPaginator.getSort().toUpperCase().equals("NAME")) {
            sortByColumn = "RCS.Name";
        } else if (modelPaginator.getSort().toUpperCase().equals("ID")) {
            sortByColumn = "RCS.ID";
        } else if (modelPaginator.getSort().toUpperCase().equals("NUMBER")) {
            sortByColumn = "RCS.Number";
        } else if (modelPaginator.getSort().toUpperCase().equals("BADGE")) {
            sortByColumn = "RCS.Badge";
        } else if (modelPaginator.getSort().toUpperCase().equals("DATE")) { //this is the default
            sortByColumn = "RCS.Name";

        } else {
            Logger.logMessage("ERROR: Invalid sort property in TransactionCollection.populateCollection(). sort property = " + modelPaginator.getSort(), Logger.LEVEL.ERROR);
            throw new MissingDataException("Invalid sort parameter for Account Endpoint", terminalModel.getId());
        }

        //get a single page of models in an array list
        ArrayList<HashMap> accountList = dm.serializeSqlWithColNames("data.posapi30.getAllAccountPaginate",
                new Object[]{
                        terminalModel.getId(),
                        modelPaginator.getStartingModelIndex().toString(),
                        modelPaginator.getEndingModelIndex().toString(),
                        sortByColumn,
                        modelPaginator.getOrder().toUpperCase()
                },
                false,
                false
        );

        //populate this collection from an array list of hashmaps
        if (accountList != null && accountList.size() > 0) {
            for (HashMap accountHM : accountList) {

                AccountModel accountModel = AccountModel.createNewAccount(accountHM, terminalModel);
                accountModel.setTerminal(terminalModel);

                LoyaltyAccountModel loyaltyAccountModel = new LoyaltyAccountModel(accountModel, true);
                loyaltyAccountModel.flipBalanceSignsForPrePayAcct();
                getCollection().add(loyaltyAccountModel);
            }
        }

        return this.getCollection();
    }

    //getter
    public List<ILoyaltyAccountModel> getCollection() {
        return this.collection;
    }

    //getter
    private String getAuthenticatedMacAddress() {
        return this.authenticatedMacAddress;
    }

    //setter
    private void setAuthenticatedMacAddress(String authenticatedMacAddress) {
        this.authenticatedMacAddress = authenticatedMacAddress;
    }
}