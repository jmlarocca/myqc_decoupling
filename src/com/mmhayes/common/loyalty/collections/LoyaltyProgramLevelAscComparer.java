package com.mmhayes.common.loyalty.collections;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.loyalty.models.LoyaltyProgramLevelModel;
import java.util.Comparator;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-12-04 12:26:20 -0500 (Tue, 04 Dec 2018) $: Date of last commit
 $Rev: 8158 $: Revision of last commit
*/
@Deprecated
public class LoyaltyProgramLevelAscComparer implements Comparator<LoyaltyProgramLevelModel> {

    public int compare(LoyaltyProgramLevelModel loyaltyProgramLevelA, LoyaltyProgramLevelModel loyaltyProgramLevelB){

        if (PosAPIHelper.LoyaltyLevelItemType.valueOf(loyaltyProgramLevelA.getItemType()).toInt() < PosAPIHelper.LoyaltyLevelItemType.valueOf(loyaltyProgramLevelB.getItemType()).toInt()){
          return -1;
        } else if (PosAPIHelper.LoyaltyLevelItemType.valueOf(loyaltyProgramLevelA.getItemType()).toInt() > PosAPIHelper.LoyaltyLevelItemType.valueOf(loyaltyProgramLevelB.getItemType()).toInt()){
            return 1;
        }
        else {
            return 0;
        }
    }
}