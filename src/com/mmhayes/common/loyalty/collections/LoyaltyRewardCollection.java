package com.mmhayes.common.loyalty.collections;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.loyalty.models.*;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.Logger;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: jmkimber $: Author of last commit
 $Date: 2020-06-29 15:24:35 -0400 (Mon, 29 Jun 2020) $: Date of last commit
 $Rev: 12096 $: Revision of last commit
*/
public class LoyaltyRewardCollection {

    private List<LoyaltyRewardModel> collection = new ArrayList<LoyaltyRewardModel>();
    private static DataManager dm = new DataManager();

    public LoyaltyRewardCollection() {

    }

    public static LoyaltyRewardCollection getAllLoyaltyRewards(TerminalModel terminal) throws Exception {
        DataManager dm = new DataManager();
        ArrayList<LoyaltyRewardModel> loyaltyRewardList = new ArrayList<>();
        LoyaltyRewardCollection loyaltyRewardCollection = new LoyaltyRewardCollection();

        ArrayList<HashMap> listHM = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyRewards",
                new Object[]{},
                PosAPIHelper.getLogFileName(terminal.getId()),
                true
        );

        for (HashMap loyaltyRewardHM : listHM) {
            //CommonAPI.checkIsNullOrEmptyObject(productHM.get("ID"), "Product Id could not be found");
            loyaltyRewardList.add(LoyaltyRewardModel.createLoyaltyRewardModel(loyaltyRewardHM, terminal.getId()));  //create a new model and add to the collection
        }

        loyaltyRewardCollection.setCollection(loyaltyRewardList);

        return loyaltyRewardCollection;
    }

    public static LoyaltyRewardCollection getAllActiveLoyaltyRewards(TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();
        ArrayList<LoyaltyRewardModel> loyaltyRewardList = new ArrayList<>();
        LoyaltyRewardCollection loyaltyRewardCollection = new LoyaltyRewardCollection();

        ArrayList<HashMap> listHM = dm.parameterizedExecuteQuery("data.posapi30.getAllActiveLoyaltyRewards",
                new Object[]{terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        for (HashMap loyaltyRewardHM : listHM) {
            //CommonAPI.checkIsNullOrEmptyObject(productHM.get("ID"), "Product Id could not be found");
            loyaltyRewardList.add(LoyaltyRewardModel.createLoyaltyRewardModel(loyaltyRewardHM, terminalModel));  //create a new model and add to the collection
        }

        loyaltyRewardCollection.setCollection(loyaltyRewardList);

        if (loyaltyRewardCollection.getCollection() == null || loyaltyRewardCollection.getCollection().isEmpty()){
            throw new MissingDataException("No Rewards Found", terminalModel.getId());
        }
        return loyaltyRewardCollection;
    }

    //fetch valid Loyalty Reward programs for the current employee and transaction
    public static LoyaltyRewardCollection getValidLoyaltyRewardsForAccount(TerminalModel terminal, ILoyaltyAccountModel loyaltyAccountModel) throws Exception {
        DataManager dm = new DataManager();
        ArrayList<LoyaltyRewardModel> loyaltyRewardList = new ArrayList<>();
        LoyaltyRewardCollection loyaltyRewardCollection = new LoyaltyRewardCollection();

        ArrayList<HashMap> listHM = dm.parameterizedExecuteQuery("data.posapi30.getAllRewardsByRevenueCenterAndLoyaltyProgramsAndEmployee",
                new Object[]{terminal.getRevenueCenterId(),
                        loyaltyAccountModel.getId()},
                PosAPIHelper.getLogFileName(terminal.getId()),
                true
        );

        for (HashMap loyaltyRewardHM : listHM) {
            //CommonAPI.checkIsNullOrEmptyObject(productHM.get("ID"), "Product Id could not be found");
            loyaltyRewardList.add(LoyaltyRewardModel.createLoyaltyRewardModel(loyaltyRewardHM, terminal.getId()));  //create a new model and add to the collection
        }

        loyaltyRewardCollection.setCollection(loyaltyRewardList);

        return loyaltyRewardCollection;
    }

    /**
     *
     * @param transactionModel
     * @return
     * @throws Exception
     */
    public static LoyaltyRewardCollection getValidLoyaltyRewardsForAccount(TransactionModel transactionModel) throws Exception {
        DataManager dm = new DataManager();
        ArrayList<LoyaltyRewardModel> loyaltyRewardList = new ArrayList<>();
        LoyaltyRewardCollection loyaltyRewardCollection = new LoyaltyRewardCollection();

        ArrayList<HashMap> listHM = new ArrayList<>();

        if (transactionModel.wasRecordedOffline()){ //don't check the point totals on this new query
            listHM = dm.parameterizedExecuteQuery("data.posapi30.getAllRewardsByRevenueCenterAndLoyaltyProgramsAndEmployeeNoPointCheck",
                    new Object[]{transactionModel.getTerminal().getRevenueCenterId(),
                            transactionModel.getLoyaltyAccount().getId()},
                    PosAPIHelper.getLogFileName(transactionModel.getTerminal().getId()),
                    true
            );

        } else {
            listHM = dm.parameterizedExecuteQuery("data.posapi30.getAllRewardsByRevenueCenterAndLoyaltyProgramsAndEmployee",
                    new Object[]{transactionModel.getTerminal().getRevenueCenterId(),
                            transactionModel.getLoyaltyAccount().getId()},
                    PosAPIHelper.getLogFileName(transactionModel.getTerminal().getId()),
                    true
            );
        }

        for (HashMap loyaltyRewardHM : listHM) {
            //CommonAPI.checkIsNullOrEmptyObject(productHM.get("ID"), "Product Id could not be found");
            loyaltyRewardList.add(LoyaltyRewardModel.createLoyaltyRewardModel(loyaltyRewardHM, transactionModel.getTerminal().getId()));  //create a new model and add to the collection
        }

        loyaltyRewardCollection.setCollection(loyaltyRewardList);

        return loyaltyRewardCollection;
    }

    public static LoyaltyRewardCollection getRewardsForProductId(Integer productId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        //ArrayList<HashMap> rewardListHM = new ArrayList();
        ArrayList<LoyaltyRewardModel> rewardList = new ArrayList<>();
        LoyaltyRewardCollection loyaltyRewardCollection = new LoyaltyRewardCollection();

        //get all models in an array list
        ArrayList<HashMap> rewardListHM = dm.parameterizedExecuteQuery("data.posapi30.getRewardsForOneProductById",
                new Object[]{productId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap loyaltyRewardHM : rewardListHM) {
            //CommonAPI.checkIsNullOrEmptyObject(productHM.get("ID"), "Product Id could not be found");
            rewardList.add(LoyaltyRewardModel.createLoyaltyRewardModel(loyaltyRewardHM, terminalId));  //create a new model and add to the collection
        }

        loyaltyRewardCollection.setCollection(rewardList);

        return loyaltyRewardCollection;
    }

    //gets all rewards based on accountID and programID
    //Used by My Quickcharge, don't remove 04/13/2017 egl
    public LoyaltyRewardCollection getLoyaltyRewardsForProgramByRevenueCenter(Integer accountID, Integer programID, Integer terminalId) throws Exception {
        LoyaltyRewardCollection loyaltyRewardCollection = new LoyaltyRewardCollection();

        ArrayList<HashMap> rewardsList = dm.parameterizedExecuteQuery("data.loyalty.getLoyaltyRewardsForProgramByRevenueCenter", new Object[]{
                accountID,
                programID
        }, PosAPIHelper.getLogFileName(terminalId), true);

        for ( HashMap reward : rewardsList ) {
            Integer rewardID = CommonAPI.convertModelDetailToInteger(reward.get("LOYALTYREWARDID"));

            if ( rewardID == null ) {
                Logger.logMessage("Could not get rewardID in LoyaltyRewardCollection.getLoyaltyRewardsForProgramByRevenueCenter", Logger.LEVEL.DEBUG);
                continue;
            }

            LoyaltyRewardModel LRM = LoyaltyRewardModel.getLoyaltyRewardModel(rewardID, terminalId);
            loyaltyRewardCollection.getCollection().add(LRM);
        }

        return loyaltyRewardCollection;
    }

    public LoyaltyRewardCollection getLoyaltyRewardsForProgramByRevenueCenter(Integer accountID, Integer programID) throws Exception {

        return getLoyaltyRewardsForProgramByRevenueCenter(accountID, programID, null);
    }

    /**
     * Gets all of the loyalty reward redeemed by a given account per accrual policy. The rewards are split
     * down per accrual policy, except for adjustments and donations which are just split down
     * per program. The information retrieved also includes the name of the reward/donation where appropriate
     *
     * @param terminal : a model of the terminal requesting this information
     * @param accountID : the account id to get the reward history for
     * @return : a collection of loyalty rewards corresponding to the rewards the account has redeemed
     * @throws Exception
     */
    public static LoyaltyRewardCollection getLoyaltyRewardsRedeemedByAccountId(TerminalModel terminal, Integer accountID) throws Exception {
        DataManager dm = new DataManager();
        ArrayList<LoyaltyRewardModel> loyaltyRewardList = new ArrayList<>();
        LoyaltyRewardCollection loyaltyRewardCollection = new LoyaltyRewardCollection();

        // get all of the rewards redeemed
        ArrayList<HashMap> listHM = dm.parameterizedExecuteQuery("data.posapi30.getLoyaltyRewardsRedeemedByEmployee",
                new Object[]{accountID},
                PosAPIHelper.getLogFileName(terminal.getId()),
                true
        );

        // iterate through each reward
        for (HashMap loyaltyRewardHM : listHM) {

            LoyaltyRewardModel loyaltyRewardModel;

            // if it's a loyalty reward, create a limited loyalty reward model and add it to the collection
            if ((loyaltyRewardHM.get("NAME").toString().equals("Loyalty Adjustment") || loyaltyRewardHM.get("NAME").toString().equals("Loyalty Donation"))) {
                loyaltyRewardModel = new LoyaltyRewardModel();
                loyaltyRewardModel.setId(CommonAPI.convertModelDetailToInteger(loyaltyRewardHM.get("ID")));
                loyaltyRewardModel.setName(CommonAPI.convertModelDetailToString(loyaltyRewardHM.get("NAME")));
                loyaltyRewardModel.setValue(CommonAPI.convertModelDetailToBigDecimal(loyaltyRewardHM.get("REWARDVALUE")));
            }
            // otherwise, create a full loyalty reward model and add it to the collection
            else {
                loyaltyRewardModel = LoyaltyRewardModel.createLoyaltyRewardModel(loyaltyRewardHM, terminal.getId());
            }

            // create a LoyaltyProgramModel, set the relevant details, and attach it to the LoyaltyRewardModel
            LoyaltyProgramModel loyaltyProgramModel = new LoyaltyProgramModel();
            loyaltyProgramModel.setName(CommonAPI.convertModelDetailToString(loyaltyRewardHM.get("LOYALTYPROGRAMNAME")));
            loyaltyRewardModel.setLoyaltyProgram(loyaltyProgramModel);

            loyaltyRewardList.add(loyaltyRewardModel);
        }

        loyaltyRewardCollection.setCollection(loyaltyRewardList);

        return loyaltyRewardCollection;
    }

    public List<LoyaltyRewardModel> getCollection() {
        return collection;
    }

    public void setCollection(List<LoyaltyRewardModel> collection) {
        this.collection = collection;
    }
}
