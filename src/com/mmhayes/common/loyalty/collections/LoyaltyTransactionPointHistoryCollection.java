package com.mmhayes.common.loyalty.collections;

//mmhayes dependencies

import com.mmhayes.common.api.ModelPaginator;
import com.mmhayes.common.loyalty.models.LoyaltyTransactionPointHistoryModel;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;

import java.util.*;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-01-29 09:35:28 -0500 (Tue, 29 Jan 2019) $: Date of last commit
 $Rev: 8368 $: Revision of last commit
*/
public class LoyaltyTransactionPointHistoryCollection {
    private List<LoyaltyTransactionPointHistoryModel> collection = new ArrayList<LoyaltyTransactionPointHistoryModel>();
    private static DataManager dm = new DataManager();

    //default constructor - used by the API
    public LoyaltyTransactionPointHistoryCollection() {
    }

    //get history records for the given accountID associated with the given loyalty program
    public LoyaltyTransactionPointHistoryCollection getLoyaltyProgramPointsHistoryForEmployee(HashMap<String, LinkedList> paginationParamsHM, Integer accountID, Integer programID) throws  Exception{
        DataManager dm = new DataManager();

        ModelPaginator modelPaginator = new ModelPaginator(paginationParamsHM);

        //set the modelCount so we can finalize the pagination properties
        modelPaginator.setModelCount(getLoyaltyTransactionPointHistoryCount(accountID, programID));

        //determines, sets and validates pagination properties
        modelPaginator.finalizePaginationProperties();

        String sortByColumn = ""; //what column should this result set be sorted by
        if (modelPaginator.getSort().equals("date")) {
            sortByColumn = "TRANS.TransactionDate";
        } else {
            Logger.logMessage("ERROR: Invalid sort property in TransactionCollection.populateCollection(). sort property = "+modelPaginator.getSort(), Logger.LEVEL.ERROR);
        }

        ArrayList<HashMap> programHistoryList = dm.serializeSqlWithColNames("data.loyalty.getLoyaltyProgramPointsHistoryForEmployee",
                new Object[]{
                    accountID,
                    programID,
                    modelPaginator.getStartingModelIndex().toString(),
                    modelPaginator.getEndingModelIndex().toString(),
                    sortByColumn,
                    modelPaginator.getOrder().toUpperCase()
                },
                false,
                false
        );

        return populateCollection(programHistoryList);
    }

    //takes a result set and creates and sets loyaltyPointModels on the collection
    private LoyaltyTransactionPointHistoryCollection populateCollection(ArrayList<HashMap> resultList) throws Exception {
        LoyaltyTransactionPointHistoryCollection loyaltyTransactionPointHistoryCollection = new LoyaltyTransactionPointHistoryCollection();

        for (HashMap loyaltyProgramHM : resultList) {
            LoyaltyTransactionPointHistoryModel loyaltyPointModel = new LoyaltyTransactionPointHistoryModel(loyaltyProgramHM);
            loyaltyTransactionPointHistoryCollection.getCollection().add(loyaltyPointModel);  //create a new model and add to the collection
        }

        return loyaltyTransactionPointHistoryCollection;
    }

    private Integer getLoyaltyTransactionPointHistoryCount(Integer accountID, Integer programID) throws Exception {
        Object count = dm.parameterizedExecuteScalar("data.loyalty.getLoyaltyTransactionPointHistoryCount", new Object[] {accountID, programID});

        if ( count == null ) {
            return 0;
        }

        return Integer.parseInt(count.toString());
    }

    public List<LoyaltyTransactionPointHistoryModel> getCollection() {
        return collection;
    }

    public void setCollection(List<LoyaltyTransactionPointHistoryModel> collection) {
        this.collection = collection;
    }
}
