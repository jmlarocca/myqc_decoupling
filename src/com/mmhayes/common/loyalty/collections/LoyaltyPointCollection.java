package com.mmhayes.common.loyalty.collections;

//mmhayes dependencies

import com.mmhayes.common.account.models.*;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.loyalty.models.*;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.Logger;

//API dependencies

//other dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-12-18 10:59:23 -0500 (Fri, 18 Dec 2020) $: Date of last commit
 $Rev: 13321 $: Revision of last commit
*/
public class LoyaltyPointCollection {

    private List<LoyaltyPointModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    //default constructor - used by the API
    public LoyaltyPointCollection() {
    }

    public void calculatePoints(TransactionModel transactionModel, LoyaltyProgramModel loyaltyProgramModel) {
        Logger.logMessage("Entering LoyaltyPointCalculation", PosAPIHelper.getLogFileName(transactionModel.getTerminal().getId()), Logger.LEVEL.DEBUG);

        if (!loyaltyProgramModel.hasAccountGroupMapped(transactionModel)) {
            Logger.logMessage(" Program: " + loyaltyProgramModel.getName() , PosAPIHelper.getLogFileName(transactionModel.getTerminal().getId()), Logger.LEVEL.TRACE);
            Logger.logMessage("  Loyalty Account's Account Group is not mapped for this Loyalty Program.  No points earned.", PosAPIHelper.getLogFileName(transactionModel.getTerminal().getId()), Logger.LEVEL.TRACE);
            return;
        }

        boolean hasProductTypeAccrualPolicy = false;
        for (LoyaltyAccrualPolicyModel loyaltyAccrualPolicyModel : loyaltyProgramModel.getAccrualPolicies()) {

            if (!loyaltyAccrualPolicyModel.getActive()) {
                //Accrual Policy is not active
                continue;
            }

            if (loyaltyAccrualPolicyModel.getMappedRevenueCenterIds().isEmpty()) {
                //Accrual Policy has no Revenue Centers mapped, don't calculate any points
                continue;
            }

            if (!loyaltyAccrualPolicyModel.getMappedRevenueCenterIds().contains(transactionModel.getTerminal().getRevenueCenterId())) {
                //Accrual Policy is not mapped to the current Terminals' Revenue Center
                continue;
            }

            if (PosAPIHelper.LoyaltyAccrualPolicyType.intToLoyaltyAccrualPolicyType(loyaltyAccrualPolicyModel.getTypeId()).equals(PosAPIHelper.LoyaltyAccrualPolicyType.PRODUCT)){
                hasProductTypeAccrualPolicy = true;
                break;
            }
        }

        if (hasProductTypeAccrualPolicy){
            //we have a Product Type Loyalty Accrual Policy
            //fetch the levels specific for the products on the the transaction
            transactionModel.populateLoyaltyAccrualPolicyLevelsForProducts();
        }

        for (LoyaltyAccrualPolicyModel loyaltyAccrualPolicyModel : loyaltyProgramModel.getAccrualPolicies()) {

            if (!loyaltyAccrualPolicyModel.getActive()) {
                //Accrual Policy is not active
                continue;
            }

            if (loyaltyAccrualPolicyModel.getMappedRevenueCenterIds().isEmpty()) {
                //Accrual Policy has no Revenue Centers mapped, don't calculate any points
                continue;
            }

            if (!loyaltyAccrualPolicyModel.getMappedRevenueCenterIds().contains(transactionModel.getTerminal().getRevenueCenterId())) {
                //Accrual Policy is not mapped to the current Terminals' Revenue Center
                continue;
            }

            if (transactionModel.hasDonation() && !loyaltyAccrualPolicyModel.getEarnPointsForDonations()){
                //Don't earn points for Donations
                continue;
            }

            LoyaltyAccrualPolicyCalculation loyaltyAccrualPolicyCalculation = new LoyaltyAccrualPolicyCalculation(transactionModel, loyaltyProgramModel, loyaltyAccrualPolicyModel);

            if (loyaltyAccrualPolicyCalculation.getPointsResult() > 0) {
                this.getCollection().add(loyaltyAccrualPolicyCalculation.getLoyaltyPoint());
            }
        }
    }

    /*
    * Get all Loyalty Points by Employee, don't look at revenue center mappings
    * Used by My QC - 04/13/2017 egl
    */
    public static LoyaltyPointCollection getAllLoyaltyPointsForEmployee(Integer accountId, boolean summaryMode) throws Exception {
        return getAllLoyaltyPointsForEmployee(accountId, null, summaryMode);
    }

    /*
    * Get all Loyalty Points by Employee, don't look at revenue center mappings
    * Used by My QC - 04/13/2017 egl
    */
    public static LoyaltyPointCollection getAllLoyaltyPointsForEmployee(Integer accountId, Integer terminalId, boolean summaryMode) throws Exception {
        DataManager dm = new DataManager();
        LoyaltyPointCollection loyaltyPointCollection = new LoyaltyPointCollection();

        //get all models in an array list
        ArrayList<HashMap> loyaltyProgramList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyPointsByEmployee",
                new Object[]{accountId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap loyaltyProgramHM : loyaltyProgramList) {
            LoyaltyPointModel loyaltyPointModel = new LoyaltyPointModel(loyaltyProgramHM, terminalId, summaryMode);
            loyaltyPointCollection.getCollection().add(loyaltyPointModel);  //create a new model and add to the collection
        }

        return loyaltyPointCollection;
    }

    /**
     * Gets the loyalty point accrual history split down by program for a given employee
     *
     * @param accountId : the account id to get the loyalty points for
     * @param summaryMode : true or false
     * @return : a collection of the loyalty point accruals
     * @throws Exception
     */
    public static LoyaltyPointCollection getAllLoyaltyPointsHistoricalForEmployee(Integer accountId, boolean summaryMode) throws Exception {
        return getAllLoyaltyPointsHistoricalForEmployee(accountId, null, summaryMode);
    }

    /**
     * Gets the loyalty point accrual history split down by program for a given employee
     *
     * @param accountId : the account id to get the loyalty points for
     * @param terminalId : the terminal id being used to retrieve the loyalty history
     * @param summaryMode : true or false
     * @return : a collection of the loyalty point accruals
     * @throws Exception
     */
    public static LoyaltyPointCollection getAllLoyaltyPointsHistoricalForEmployee(Integer accountId, Integer terminalId, boolean summaryMode) throws Exception {
        DataManager dm = new DataManager();
        LoyaltyPointCollection loyaltyPointCollection = new LoyaltyPointCollection();

        //get all models in an array list
        ArrayList<HashMap> loyaltyProgramList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyPointsHistoricalByEmployee",
                new Object[]{accountId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        // iterate through each retrieved loyalty accrual
        for (HashMap loyaltyProgramHM : loyaltyProgramList) {
            // convert the loyalty program to a LoyaltyPointModel
            LoyaltyPointModel loyaltyPointModel = new LoyaltyPointModel(loyaltyProgramHM, terminalId, summaryMode);
            // add the LoyaltyPointModel to the collection
            loyaltyPointCollection.getCollection().add(loyaltyPointModel);  //create a new model and add to the collection
        }

        return loyaltyPointCollection;
    }

    /**
     * Gets the loyalty point accrual history split down by accrual policy for a given employee
     *
     * @param accountId : the account id to get the loyalty points for
     * @param summaryMode : true or false
     * @return : a collection of the loyalty point accruals
     * @throws Exception
     */
    public static LoyaltyPointCollection getAllLoyaltyPointsByAccrualPolicyHistoricalForEmployee(Integer accountId, boolean summaryMode) throws Exception {
        return getAllLoyaltyPointsByAccrualPolicyHistoricalForEmployee(accountId, null, summaryMode);
    }

    /**
     * Gets the loyalty point accrual history split down by accrual policy for a given employee
     *
     * @param accountId : the account id to get the loyalty points for
     * @param terminalId : the terminal id being used to retrieve the loyalty history
     * @param summaryMode : true or false
     * @return : a collection of the loyalty point accruals
     * @throws Exception
     */
    public static LoyaltyPointCollection getAllLoyaltyPointsByAccrualPolicyHistoricalForEmployee(Integer accountId, Integer terminalId, boolean summaryMode) throws Exception {
        DataManager dm = new DataManager();
        LoyaltyPointCollection loyaltyPointCollection = new LoyaltyPointCollection();

        //get all models in an array list
        ArrayList<HashMap> loyaltyProgramList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyPointsByAccrualPolicyHistoricalByEmployee",
                new Object[]{accountId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        // iterate through each retrieved loyalty accrual
        for (HashMap loyaltyProgramHM : loyaltyProgramList) {
            // converty the accrual to a LoyaltyPointModel
            LoyaltyPointModel loyaltyPointModel = new LoyaltyPointModel(loyaltyProgramHM, terminalId, summaryMode);

            // add the loyalty accrual policy to loyaltypointmodel
            if (!loyaltyProgramHM.get("LOYALTYACCRUALPOLICYID").toString().isEmpty()) {
                LoyaltyAccrualPolicyModel loyaltyAccrualPolicy = new LoyaltyAccrualPolicyModel();

                Integer loyaltyAccrualPolicyId = CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("LOYALTYACCRUALPOLICYID"));
                String loyaltyAccrualPolicyName = CommonAPI.convertModelDetailToString(loyaltyProgramHM.get("LOYALTYACCRUALPOLICYNAME"));
                Integer transLineId = CommonAPI.convertModelDetailToInteger(loyaltyProgramHM.get("PATRANSLINEITEMID"));

                loyaltyAccrualPolicy.setId(loyaltyAccrualPolicyId);
                loyaltyAccrualPolicy.setName(loyaltyAccrualPolicyName);

                loyaltyPointModel.setLoyaltyRewardTransLineId(transLineId);
                loyaltyPointModel.setLoyaltyAccrualPolicy(loyaltyAccrualPolicy);
            }

            // add the LoyaltyPointModel to the collection
            loyaltyPointCollection.getCollection().add(loyaltyPointModel);  //create a new model and add to the collection
        }

        return loyaltyPointCollection;
    }

    /*
    * Get Points for employee and active Programs
    * Check that the Loyalty Program is Active.  Check that the Loyalty Program is mapped with the revenue center
    */
    public static LoyaltyPointCollection getAllLoyaltyPointsForEmployeeAndActivePrograms(Integer accountId, TerminalModel terminalModel, boolean summaryMode) throws Exception {
        DataManager dm = new DataManager();
        LoyaltyPointCollection loyaltyPointCollection = new LoyaltyPointCollection();

        //get all models in an array list
        ArrayList<HashMap> loyaltyProgramList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyPointsForEmployeeAndActivePrograms",
                new Object[]{accountId, terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        for (HashMap loyaltyProgramHM : loyaltyProgramList) {
            LoyaltyPointModel loyaltyPointModel = new LoyaltyPointModel(loyaltyProgramHM, terminalModel.getId(), summaryMode);
            loyaltyPointCollection.getCollection().add(loyaltyPointModel);  //create a new model and add to the collection
        }

        return loyaltyPointCollection;
    }

    /**
     * Get Points for employee and active Programs
     * Check that the Loyalty Program is Active.  Check that the Loyalty Program is mapped with the revenue center
     *
     * @param accountId         = account ID
     * @param terminalModel     = current terminal the request is coming from
     * @param summaryMode       = true, if we don't want to see Program Level details
     * @param endPointModelType = if coming from the Loyalty Endpoint, we want to error out if no records are found
     */
    public static LoyaltyPointCollection getAllLoyaltyPointsForEmployeeAndActivePrograms(Integer accountId, TerminalModel terminalModel, boolean summaryMode, CommonAPI.PosModelType endPointModelType) throws Exception {
        DataManager dm = new DataManager();

        //06-29-2017 egl - HP 587.  Fetch the Account, if the account is invalid, this will return the error
        AccountModel accountModel = AccountModel.getAccountModelByIdForTender(terminalModel, BigDecimal.ZERO, accountId, new AccountQueryParams());

        LoyaltyPointCollection loyaltyPointCollection = LoyaltyPointCollection.getAllLoyaltyPointsForEmployeeAndActivePrograms(accountId, terminalModel, summaryMode);

        if (endPointModelType != null && endPointModelType.equals(CommonAPI.PosModelType.LOYALTY_PROGRAM)) {
            if (loyaltyPointCollection.getCollection() != null && loyaltyPointCollection.getCollection().size() < 1) {
                throw new MissingDataException("No Loyalty Points found", terminalModel.getId());
            }
        }

        return loyaltyPointCollection;
    }

    /*
    * Get all earned points for a transaction
    * */
    public static LoyaltyPointCollection getAllLoyaltyPointsForEmployeeAndTransactionId(Integer accountId, Integer transactionId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        LoyaltyPointCollection loyaltyPointCollection = new LoyaltyPointCollection();

        //get all models in an array list
        ArrayList<HashMap> loyaltyProgramList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyPointsByEmployeeAndTransactionId",
                new Object[]{accountId, transactionId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap loyaltyProgramHM : loyaltyProgramList) {
            LoyaltyPointModel loyaltyPointModel = new LoyaltyPointModel(loyaltyProgramHM, terminalId, false);
            loyaltyPointCollection.getCollection().add(loyaltyPointModel);  //create a new model and add to the collection
        }

        return loyaltyPointCollection;
    }

    /**
     * Get all Loyalty points for an active Program
     * It also checks that the Loyalty Program is mapped to the revenue center that is passed in
     */
    public static LoyaltyPointCollection getAllLoyaltyPointsForActivePrograms(TerminalModel terminalModel, boolean summaryMode) throws Exception {
        DataManager dm = new DataManager();
        LoyaltyPointCollection loyaltyPointCollection = new LoyaltyPointCollection();

        //get all models in an array list
        ArrayList<HashMap> loyaltyProgramList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyPointsForActivePrograms",
                new Object[]{terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        for (HashMap loyaltyProgramHM : loyaltyProgramList) {
            LoyaltyPointModel loyaltyPointModel = new LoyaltyPointModel(loyaltyProgramHM, terminalModel.getId(), summaryMode);
            loyaltyPointCollection.getCollection().add(loyaltyPointModel);  //create a new model and add to the collection
        }

        return loyaltyPointCollection;
    }

    public List<LoyaltyPointModel> getCollection() {
        return collection;
    }

    public void setCollection(List<LoyaltyPointModel> collection) {
        this.collection = collection;
    }
}
