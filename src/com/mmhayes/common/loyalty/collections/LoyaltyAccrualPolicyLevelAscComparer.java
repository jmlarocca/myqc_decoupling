package com.mmhayes.common.loyalty.collections;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.loyalty.models.LoyaltyAccrualPolicyLevelModel;

//other dependencies
import java.util.Comparator;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-06-01 10:42:07 -0400 (Thu, 01 Jun 2017) $: Date of last commit
 $Rev: 4072 $: Revision of last commit
*/
public class LoyaltyAccrualPolicyLevelAscComparer implements Comparator<LoyaltyAccrualPolicyLevelModel> {

    public int compare(LoyaltyAccrualPolicyLevelModel loyaltyAccrualPolicyLevelA, LoyaltyAccrualPolicyLevelModel loyaltyAccrualPolicyLevelB){

        if (PosAPIHelper.LoyaltyLevelItemType.valueOf(loyaltyAccrualPolicyLevelA.getItemType()).toInt() < PosAPIHelper.LoyaltyLevelItemType.valueOf(loyaltyAccrualPolicyLevelB.getItemType()).toInt()){
            return -1;
        } else if (PosAPIHelper.LoyaltyLevelItemType.valueOf(loyaltyAccrualPolicyLevelA.getItemType()).toInt() > PosAPIHelper.LoyaltyLevelItemType.valueOf(loyaltyAccrualPolicyLevelB.getItemType()).toInt()){
            return 1;
        }
        else {
            return 0;
        }
    }
}
