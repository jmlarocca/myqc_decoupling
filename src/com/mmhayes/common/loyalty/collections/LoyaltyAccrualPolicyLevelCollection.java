package com.mmhayes.common.loyalty.collections;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.loyalty.models.LoyaltyAccrualPolicyLevelModel;

//API dependencies

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-06-06 11:33:31 -0400 (Tue, 06 Jun 2017) $: Date of last commit
 $Rev: 4094 $: Revision of last commit
*/
public class LoyaltyAccrualPolicyLevelCollection {
    private List<LoyaltyAccrualPolicyLevelModel> collection = new ArrayList<>();
    DataManager dm = new DataManager();

    public LoyaltyAccrualPolicyLevelCollection() {

    }

    public LoyaltyAccrualPolicyLevelCollection getAllLoyaltyAccrualPolicyLevelsByAccrualPolicyId(Integer accrualPolicyId, Integer terminalId) throws Exception {
        //get all models in an array list
        ArrayList<HashMap> loyaltyAccrualPolicyLevelList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyAccrualPolicyLevelsByAccrualPolicyId",
                new Object[]{accrualPolicyId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap loyaltyAccrualPolicyLevelHM : loyaltyAccrualPolicyLevelList) {
            this.getCollection().add(new LoyaltyAccrualPolicyLevelModel(loyaltyAccrualPolicyLevelHM));
        }

        return this;
    }

    /**
     * Get all the Loyalty Accrual Policy Levels for a specific product.
     * This is used for Loyalty Accrual Point calculation for Product level Accrual Policies
     *
     * @param productId
     * @param terminalId
     * @return
     */
    public LoyaltyAccrualPolicyLevelCollection getAllLoyaltyAccrualPolicyLevelsByProductId(Integer productId, Integer terminalId) {
        ArrayList<HashMap> loyaltyAccrualPolicyLevelList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyAccrualPolicyLevelsByProductId",
                new Object[]{productId, terminalId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap loyaltyAccrualPolicyLevelHM : loyaltyAccrualPolicyLevelList) {
            this.getCollection().add(new LoyaltyAccrualPolicyLevelModel(loyaltyAccrualPolicyLevelHM));
        }

        return this;
    }

    public List<LoyaltyAccrualPolicyLevelModel> getCollection() {
        return collection;
    }

    public void setCollection(List<LoyaltyAccrualPolicyLevelModel> collection) {
        this.collection = collection;
    }

}