package com.mmhayes.common.loyalty.collections;

//MMHayes Dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.loyalty.models.LoyaltyAccrualPolicyModel;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-06-06 11:33:31 -0400 (Tue, 06 Jun 2017) $: Date of last commit
 $Rev: 4094 $: Revision of last commit
*/
public class LoyaltyAccrualPolicyCollection {
    private List<LoyaltyAccrualPolicyModel> collection = new ArrayList<>();
    DataManager dm = new DataManager();

    public LoyaltyAccrualPolicyCollection(){

    }

    public LoyaltyAccrualPolicyCollection getAllLoyaltyAccrualPoliciesByProgramId(Integer loyaltyProgramId, Integer terminalId, boolean summaryMode) throws Exception {
        //get all models in an array list
        ArrayList<HashMap> loyaltyAccrualPolicyList = dm.parameterizedExecuteQuery("data.posapi30.getAllLoyaltyAccrualPoliciesByProgramId",
                new Object[]{loyaltyProgramId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap loyaltyAccrualPolicyHM : loyaltyAccrualPolicyList) {
            this.getCollection().add(new LoyaltyAccrualPolicyModel(loyaltyAccrualPolicyHM, terminalId, summaryMode));
        }

        return this;
    }


    public List<LoyaltyAccrualPolicyModel> getCollection() {
        return collection;
    }

    public void setCollection(List<LoyaltyAccrualPolicyModel> collection) {
        this.collection = collection;
    }
}
