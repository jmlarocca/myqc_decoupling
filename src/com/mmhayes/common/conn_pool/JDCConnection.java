package com.mmhayes.common.conn_pool;

import java.sql.*;
import java.util.*;
import java.io.*;
import java.util.concurrent.*;
import com.mmhayes.common.utils.*;

public class JDCConnection implements Connection {

    private JDCConnectionPool pool;
    private Connection conn;
    private boolean inuse;
    private long timestamp;
    private int SPID = 0;

    public JDCConnection(Connection conn, JDCConnectionPool pool) {
        this.conn=conn;
        this.pool=pool;
        this.inuse=false;
        this.timestamp=0;

        try {
            MMHResultSet rs = new MMHResultSet();
            rs.statement = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            rs.resultSet = rs.statement.executeQuery("SELECT @@SPID");
            if (rs.resultSet.next()) {
                SPID = Integer.parseInt(rs.resultSet.getObject(1).toString());
                Logger.logMessage("New Connection SPID: " + SPID);
            }
            rs.close();
            rs = null;
        } catch (Exception ex) {
            Logger.logMessage("Attempt to determine SPID raised the following error");
            Logger.logException(ex);
        }
    }

    public int getSPID() {
        return SPID;
    }

    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return conn.isWrapperFor(iface);
    }

    public <T> T unwrap(Class<T> iface) throws  SQLException {
        return conn.unwrap(iface);
    }

    public Blob createBlob() throws SQLException {
        return conn.createBlob();
    }

    public NClob createNClob() throws SQLException {
        return conn.createNClob();
    }

    public Clob createClob() throws SQLException {
        return conn.createClob();
    }

    public SQLXML createSQLXML() throws SQLException {
        return conn.createSQLXML();
    }

    public boolean isValid(int i) throws SQLException {
        return conn.isValid(i);
    }

    public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
        return conn.createStruct(typeName, attributes);
    }

    public Properties getClientInfo() throws SQLException {
        return conn.getClientInfo();
    }

    public void setClientInfo(Properties p) throws SQLClientInfoException {
        conn.setClientInfo(p);
    }
    public void setClientInfo(String s, String s2) throws SQLClientInfoException {
        conn.setClientInfo(s,s2);
    }

    public String getClientInfo(String name) throws SQLException {
        return conn.getClientInfo(name);
    }

    public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
        return conn.createArrayOf(typeName,elements);
    }

    public synchronized boolean lease() {
        if(inuse)  {
            return false;
        } else {
            inuse=true;
            timestamp=System.currentTimeMillis();
            return true;
        }
    }
    public boolean validate() {
        boolean ret;
        try {
            conn.getMetaData();
            ret = !conn.isClosed();
            if (!ret) {
                Logger.logMessage("JDCConnection.validate returned false!");
            }
        }catch (Exception e) {
            ret = false;
            Logger.logMessage("JDCConnection.validate ERROR: " + e.getMessage());
        }
        return ret;
    }

    public boolean inUse() {
        return inuse;
    }

    public long getLastUse() {
        return timestamp;
    }

    public void close() throws SQLException {
        pool.returnConnection(this,"");
    }

    public void closeForReal() throws SQLException {
        inuse = false;
        conn.close();
        Logger.logMessage("Closed Connection SPID " + SPID);
    }

    protected void expireLease() {
        inuse=false;
    }

    protected Connection getConnection() {
        return conn;
    }

    public PreparedStatement prepareStatement(String sql) throws SQLException {
        return conn.prepareStatement(sql);
    }

    public CallableStatement prepareCall(String sql) throws SQLException {
        return conn.prepareCall(sql);
    }

    public Statement createStatement() throws SQLException {
        return conn.createStatement();
    }

    public String nativeSQL(String sql) throws SQLException {
        return conn.nativeSQL(sql);
    }

    public void setAutoCommit(boolean autoCommit) throws SQLException {
        conn.setAutoCommit(autoCommit);
    }

    public boolean getAutoCommit() throws SQLException {
        return conn.getAutoCommit();
    }

    public void commit() throws SQLException {
        conn.commit();
    }

    public void rollback() throws SQLException {
        conn.rollback();
    }

    public boolean isClosed() throws SQLException {
        return conn.isClosed();
    }

    public DatabaseMetaData getMetaData() throws SQLException {
        return conn.getMetaData();
    }

    public void setReadOnly(boolean readOnly) throws SQLException {
        conn.setReadOnly(readOnly);
    }

    public boolean isReadOnly() throws SQLException {
        return conn.isReadOnly();
    }

    public void setCatalog(String catalog) throws SQLException {
        conn.setCatalog(catalog);
    }

    public String getCatalog() throws SQLException {
        return conn.getCatalog();
    }

    public void setTransactionIsolation(int level) throws SQLException {
        conn.setTransactionIsolation(level);
    }

    public int getTransactionIsolation() throws SQLException {
        return conn.getTransactionIsolation();
    }

    public SQLWarning getWarnings() throws SQLException {
        return conn.getWarnings();
    }

    public void clearWarnings() throws SQLException {
        conn.clearWarnings();
    }
    public void setTypeMap(Map map) throws SQLException {
        conn.setTypeMap(map);
    }
    public Map getTypeMap() throws SQLException {
        return conn.getTypeMap();
    }
    public Statement createStatement(int parm1, int parm2) throws SQLException {
        return conn.createStatement(parm1, parm2);
    }
    public PreparedStatement prepareStatement(String parm1, int parm2, int parm3) throws SQLException {
        return conn.prepareStatement(parm1, parm2, parm2);
    }
    public CallableStatement prepareCall(String parm1, int parm2, int parm3) throws SQLException {
        return conn.prepareCall(parm1, parm2, parm3);
    }

    //START - ADDED FOR JDK 1.5.0
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        return conn.prepareCall(sql,resultSetType,resultSetConcurrency,resultSetHoldability);
    }

    public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        return conn.createStatement(resultSetType,resultSetConcurrency,resultSetHoldability);
    }

    public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
        return conn.prepareStatement(sql,columnNames);
    }

    public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
        return conn.prepareStatement(sql,columnIndexes);
    }

    public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
        return conn.prepareStatement(sql,autoGeneratedKeys);
    }

    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        return conn.prepareStatement(sql,resultSetType,resultSetConcurrency,resultSetHoldability);
    }
    public void releaseSavepoint(Savepoint savepoint) throws SQLException {
        conn.releaseSavepoint(savepoint);
    }
    public void rollback(Savepoint savepoint) throws SQLException {
        conn.rollback();
    }

    public Savepoint setSavepoint() throws SQLException {
        return conn.setSavepoint();
    }

    public Savepoint setSavepoint(String name) throws SQLException {
        return conn.setSavepoint(name);
    }

    public int getHoldability() throws SQLException {
        return conn.getHoldability();
    }
    public void setHoldability(int i) throws SQLException {
        conn.setHoldability(i);
    }
    //END - ADDED FOR JDK 1.5.0

    //START - ADDED FOR JDK 1.8.11
    //added when we switched to JDK 8 -jrmitaly
    public int getNetworkTimeout() throws SQLException {
        return conn.getNetworkTimeout();
    }

    //added when we switched to JDK 8 -jrmitaly
    public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
        conn.setNetworkTimeout(executor, milliseconds);
    }

    //added when we switched to JDK 8 -jrmitaly
    public void abort(Executor executor) throws SQLException {
        conn.abort(executor);
    }

    //added when we switched to JDK 8 -jrmitaly
    public String getSchema() throws SQLException {
        return conn.getSchema();
    }

    //added when we switched to JDK 8 -jrmitaly
    public void setSchema(String schema) throws SQLException {
        conn.setSchema(schema);
    }
    //END - ADDED FOR JDK 1.8.11
}
