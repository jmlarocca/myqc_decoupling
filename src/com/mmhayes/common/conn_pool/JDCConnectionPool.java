package com.mmhayes.common.conn_pool;

import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

class ConnectionReaper extends Thread {

	private JDCConnectionPool pool;
	private final long delay=300000;
    public boolean isActive = true;

	ConnectionReaper(JDCConnectionPool pool) {
		this.pool=pool;
	}

	public void run() {
		while(isActive) {
			try {
				sleep(delay);
				} catch( InterruptedException e) {isActive = false;}
				pool.reapConnections();
		}
		System.out.println("ConnectionReaper.run() completed.");
	}
}

public class JDCConnectionPool {

	private Vector connections;
	private String url, user, password;
	final private long timeout=60000;
	private ConnectionReaper reaper;
	final private int poolsize=10;
    private int poolSizeLoggingThreshold = 50;
    public static boolean isConnectionLoggingEnabled = false;

	public JDCConnectionPool(String url, String user, String password) {
		this.url = url;
		this.user = user;
		this.password = password;
		connections = new Vector(poolsize);
		reaper = new ConnectionReaper(this);
        if (MMHProperties.getAppSetting("database.connectionpool.loggingthreshold").length() > 0)
        {
            poolSizeLoggingThreshold = Integer.parseInt(MMHProperties.getAppSetting("database.connectionpool.loggingthreshold"));
        }
        // check for Connection Logging should be enabled
        String isEnabled = MMHProperties.getAppSetting("site.connection.logging.enabled");
        if(isEnabled.trim().toLowerCase().equals("true")){
            JDCConnectionPool.isConnectionLoggingEnabled = true;
        }
        reaper.start();
	}

	public synchronized void reapConnections() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.SSS a");

 		long stale = System.currentTimeMillis() - timeout;
		Enumeration connlist = connections.elements();
        int initialConnections = connections.size();

        if (initialConnections >= poolSizeLoggingThreshold && isConnectionLoggingEnabled) {
           Logger.logMessage("Entering JDCConnectionPool.reapConnections(), initial connections: " + initialConnections + "; stale threshold: " + sdf.format(new Date(stale)), Logger.LEVEL.TRACE);
        }

		while((connlist != null) && (connlist.hasMoreElements())) {

			JDCConnection conn = (JDCConnection)connlist.nextElement();

            if (initialConnections >= poolSizeLoggingThreshold && isConnectionLoggingEnabled) {
                Logger.logMessage("Inspecting SPID: " + conn.getSPID() + "; validate(): " + conn.validate() + "; inUse(): " + conn.inUse() + "; getLastUse(): " + sdf.format(new Date(conn.getLastUse())) + "; connections.size(): " + connections.size(), Logger.LEVEL.TRACE);
            }

			if((!conn.inUse()) && (stale > conn.getLastUse() || !conn.validate()) && (connections.size() > 1)) {
                if (initialConnections >= poolSizeLoggingThreshold && isConnectionLoggingEnabled) {
                    Logger.logMessage("About to reap connection SPID " + conn.getSPID(), Logger.LEVEL.TRACE);
                }
				removeConnection(conn);
                if (initialConnections >= poolSizeLoggingThreshold && isConnectionLoggingEnabled) {
                    Logger.logMessage("New connections.size(): " + connections.size(), Logger.LEVEL.TRACE);
                }
			} else { //added by jrmitaly on 12/3/2015
                if (initialConnections >= poolSizeLoggingThreshold && isConnectionLoggingEnabled) {
                    Logger.logMessage("NOT REAPING SPID " + conn.getSPID(), Logger.LEVEL.TRACE);
                }
            }
		}
        if (initialConnections - connections.size() > 0 && isConnectionLoggingEnabled) {
            Logger.logMessage("connections after reaping: " + connections.size() + "; connection reaped: " + (initialConnections - connections.size()), Logger.LEVEL.TRACE);
        }
	}

	public synchronized void closeConnections() {

		Enumeration connlist = connections.elements();

		while((connlist != null) && (connlist.hasMoreElements())) {
			JDCConnection conn = (JDCConnection)connlist.nextElement();
			removeConnection(conn);
		}
	}

	private synchronized void removeConnection(JDCConnection conn) {
        try {
            if(isConnectionLoggingEnabled){
                Logger.logMessage("JDCConnectionPool.removeConnection(" + conn.getSPID() + ")", Logger.LEVEL.TRACE);
            }
            conn.closeForReal();
        }
        catch (Exception ex) {
            Logger.logException("JDCConnectionPool.removeConnection raised error", "", ex);
        }
		connections.removeElement(conn);
        conn = null;
    }


	public synchronized JDCConnection getConnection(boolean reconnect, String logFileName) throws SQLException {
		JDCConnection c = null;
		if (reconnect) {
			closeConnections();
		}
        if(isConnectionLoggingEnabled){
            Logger.logMessage("Database Connections: " + connections.size(), logFileName, Logger.LEVEL.TRACE);
        }
		for(int i = 0; i < connections.size(); i++) {
            try {
			    c = (JDCConnection)connections.elementAt(i);
                boolean bAutoCommit = c.getAutoCommit();
                if (bAutoCommit) {
                    if (c.lease()) {
                        if(isConnectionLoggingEnabled){
                            Logger.logMessage("Leasing a connection (SPID " + c.getSPID() + ") from the pool. total connections:" + connections.size(), logFileName, Logger.LEVEL.TRACE);
                        }
				        return c;
			        }
		        }else{
                    if(isConnectionLoggingEnabled){
                        Logger.logMessage("Warning connection.getAutoCommit()=" + bAutoCommit, logFileName, Logger.LEVEL.TRACE);
                    }
                }
            } catch (SQLException exSQL) {
                Logger.logException("About to remove bad connection from pool raised SQLException", logFileName, exSQL);
                removeConnection(c);
            } catch (Exception ex) {
                Logger.logException("About to remove bad connection from pool raised Exception", logFileName, ex);
                removeConnection(c);
            }
        }
		try {
			Connection conn = DriverManager.getConnection(url, user, password);
			conn.setAutoCommit(true);
			c = new JDCConnection(conn, this);
			c.lease();
			connections.addElement(c);
            if(isConnectionLoggingEnabled){
                Logger.logMessage("Created new db connection (SPID " + c.getSPID() + ") and added it to the pool. total connections:" + connections.size(), logFileName, Logger.LEVEL.TRACE);
            }
		}
		catch (SQLException ex) {
			Logger.logException("could not create a valid Connection to add to pool (for url=" + url + "; user=" + user + ")", logFileName, ex);
		}
		return c;
	}

	public synchronized void returnConnection(JDCConnection conn, String logFileName) {
        try {
            if(isConnectionLoggingEnabled){
                Logger.logMessage("Entering JDCConnectionPool.returnConnection", logFileName, Logger.LEVEL.TRACE);
            }
            if (conn.getAutoCommit()) {
                conn.expireLease();
                if(isConnectionLoggingEnabled){
                    Logger.logMessage("Returned db connection (SPID " + conn.getSPID() + ") to pool. total connections: " + connections.size(), logFileName, Logger.LEVEL.TRACE);
                }
            }
            else
            {
                if(isConnectionLoggingEnabled){
                    Logger.logMessage("Could not return connection (SPID " + conn.getSPID() + ") to pool because AutoCommit set to false", logFileName, Logger.LEVEL.TRACE);
                }
            }
        } catch (SQLException ex) {
            Logger.logException("ERROR returning connection (SPID " + conn.getSPID() + ") to pool", logFileName, ex);
        }
	}

    public synchronized void stopConnectionReaper(){
        if(isConnectionLoggingEnabled){
            Logger.logMessage("Stopping Connection Reaper thread ... ", Logger.LEVEL.TRACE);
        }
        reaper.interrupt();
        reaper = null;
    }
}