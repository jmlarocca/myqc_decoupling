package com.mmhayes.common.receiptGen;

import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHTimeFormatString;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by nyu on 10/13/2016.
 *
 * This class contains methods for retrieving data from hashmaps
 */
public class HashMapDataFns
{
    private final static String COMMON_LOG_FILE = "ConsolidatedReceipt.log";

    // Takes a hashmap and returns the integer value of the specified key name
    public static Integer getIntVal(HashMap hm, String keyName)
    {
        int iRetVal = -1;

        keyName = keyName.toUpperCase();
        if(hm.containsKey(keyName))
        {
            Object keyValue = hm.get(keyName);
            if(keyValue != null)
            {
                if (keyValue instanceof Integer)
                {
                    iRetVal = (Integer)keyValue;
                }
                else if(keyValue instanceof Boolean)
                {
                    iRetVal = ((Boolean)keyValue) ? 1 : 0;
                }
                else if(keyValue instanceof Long)
                {
                    Long l = (Long) keyValue;
                    iRetVal = l.intValue();
                }
                else if ((StringFunctions.stringHasContent(keyValue.toString())) && (NumberUtils.isNumber(keyValue.toString()))) {
                    iRetVal = Integer.parseInt(keyValue.toString());
                }
            }
        }

        return iRetVal;
    }

    // Takes a hashmap and returns the float value of the specified key name
    public static Double getDoubleVal(HashMap hm, String keyName)
    {
        double iRetVal = -1.0;

        keyName = keyName.toUpperCase();
        if(hm.containsKey(keyName))
        {
            Object keyValue = hm.get(keyName);
            if(keyValue != null)
            {
                if (keyValue instanceof Double)
                {
                    iRetVal = (Double)keyValue;
                }
                else if (keyValue instanceof BigDecimal)
                {
                    iRetVal = ((BigDecimal)keyValue).doubleValue();
                }
                else if ((StringFunctions.stringHasContent(keyValue.toString())) && (NumberUtils.isNumber(keyValue.toString()))) {
                    iRetVal = Double.parseDouble(keyValue.toString());
                }
            }
        }

        return iRetVal;
    }

    // Takes a hashmap and returns the string value of the specified key name
    public static String getStringVal(HashMap hm, String keyName)
    {
        String retVal = null;
        keyName = keyName.toUpperCase();
        if(hm.containsKey(keyName))
        {
            Object keyValue = hm.get(keyName);
            retVal = keyValue.toString();
        }

        return retVal;
    }

    // Takes a hashmap and returns the boolean value of the specified key name
    public static Boolean getBooleanVal(HashMap hm, String keyName)
    {
        Boolean retVal = new Boolean(false);
        keyName = keyName.toUpperCase();
        if (hm.containsKey(keyName))
        {
            Object keyValue = hm.get(keyName);
            if (keyValue != null)
            {
                if (keyValue instanceof Boolean)
                {
                    retVal = (Boolean) keyValue;
                }
                else if (keyValue.toString().equals("1"))
                {
                    retVal = true;
                }
                else if (keyValue.toString().equals("0"))
                {
                    retVal = false;
                }
                else if (keyValue.toString().equalsIgnoreCase("true")) {
                    retVal = true;
                }
                else if (keyValue.toString().equalsIgnoreCase("false")) {
                    retVal = false;
                }
            }
        }

        return retVal;
    }

    // Takes a hashmap and returns the Date value of the specified key name
    public static Date getDateVal (HashMap hm, String keyName)
    {
        Date retVal = null;
        keyName = keyName.toUpperCase();
        if(hm.containsKey(keyName))
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Object keyValue = hm.get(keyName);
            if (keyValue instanceof java.sql.Timestamp)
            {
                try {
                    retVal = sdf.parse(keyValue.toString());
                } catch (ParseException e) {
                    Logger.logException("Exception parsing " + keyName + "-" + e.toString(), COMMON_LOG_FILE, e);
                }
            }else if(keyValue instanceof Date){
                retVal = (Date)keyValue;
            }
        }

        return retVal;
    }

    // Takes a hashmap and returns the Date value of the specified key name
    public static Calendar getCalendarVal (HashMap hm, String keyName)
    {
        Calendar cal = Calendar.getInstance();
        keyName = keyName.toUpperCase();
        if(hm.containsKey(keyName))
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Object keyValue = hm.get(keyName);
            if (keyValue instanceof java.sql.Timestamp)
            {
                try {
                    Date date = sdf.parse(keyValue.toString());
                    cal.setTime(date);
                } catch (ParseException e) {
                    Logger.logException("Exception parsing " + keyName + "-" + e.toString(), COMMON_LOG_FILE, e);
                }
            }
        }

        return cal;
    }

    public static String[] getStringArrVal (HashMap hm, String keyName) {
        String val = getStringVal(hm, keyName);
        if (StringFunctions.stringHasContent(val)) {
            if (val.contains(",")) {
                return val.split("\\s*,\\s*", -1);
            }
            else {
                return new String[]{val};
            }
        }
        else {
            return null;
        }
    }

    public static int[] getIntArrVal (HashMap hm, String keyName) {
        String[] strArr = getStringArrVal(hm, keyName);
        if (!DataFunctions.isEmptyGenericArr(strArr)) {
            int[] intArr = new int[strArr.length];
            for (int i = 0; i < strArr.length; i++) {
                if (NumberUtils.isNumber(strArr[i])) {
                    intArr[i] = Integer.parseInt(strArr[i]);
                }
            }
            return intArr;
        }
        else {
            return null;
        }
    }

    public static Long getLongVal (HashMap hm, String keyName) {
        Long retVal = null;

        keyName = keyName.toUpperCase();
        if (hm.containsKey(keyName)) {
            Object keyValue = hm.get(keyName);
            if ((keyValue != null) && (!DataFunctions.isEmptyMap(hm))) {
                retVal = Long.valueOf(keyValue.toString());
            }
        }

        return retVal;
    }

    @SuppressWarnings({"unchecked", "TypeMayBeWeakened"})
    public static Integer getIntVal (HashMap hm, String keyName, boolean useCaseInsensitiveKey) {

        if (!useCaseInsensitiveKey) {
            return getIntVal(hm, keyName);
        }
        else {
            int retInt = -1;

            TreeMap tm = new TreeMap(String.CASE_INSENSITIVE_ORDER);
            tm.putAll(hm);

            if (tm.containsKey(keyName)) {
                Object keyValue = tm.get(keyName);
                if(keyValue != null) {
                    if (keyValue instanceof Integer) {
                        retInt = ((Integer) keyValue);
                    }
                    else if(keyValue instanceof Boolean) {
                        retInt = ((Boolean) keyValue) ? 1 : 0;
                    }
                    else if(keyValue instanceof Long) {
                        Long l = (Long) keyValue;
                        retInt = l.intValue();
                    }
                    else if ((StringFunctions.stringHasContent(keyValue.toString())) && (NumberUtils.isNumber(keyValue.toString()))) {
                        retInt = Integer.parseInt(keyValue.toString());
                    }
                }
            }

            return retInt;
        }

    }

    @SuppressWarnings({"unchecked", "TypeMayBeWeakened"})
    public static Double getDoubleVal (HashMap hm, String keyName, boolean useCaseInsensitiveKey) {

        if (!useCaseInsensitiveKey) {
            return getDoubleVal(hm, keyName);
        }
        else {
            double retDouble = -1.0d;

            TreeMap tm = new TreeMap(String.CASE_INSENSITIVE_ORDER);
            tm.putAll(hm);

            if (tm.containsKey(keyName)) {
                Object keyValue = tm.get(keyName);
                if(keyValue != null) {
                    if (keyValue instanceof Double) {
                        retDouble = ((Double) keyValue);
                    }
                    else if (keyValue instanceof BigDecimal) {
                        retDouble = ((BigDecimal)keyValue).doubleValue();
                    }
                    else if ((StringFunctions.stringHasContent(keyValue.toString())) && (NumberUtils.isNumber(keyValue.toString()))) {
                        retDouble = Double.parseDouble(keyValue.toString());
                    }
                }
            }

            return retDouble;
        }
    }

    @SuppressWarnings({"unchecked", "TypeMayBeWeakened"})
    public static String getStringVal (HashMap hm, String keyName, boolean useCaseInsensitiveKey) {

        if (!useCaseInsensitiveKey) {
            return getStringVal(hm, keyName);
        }
        else {
            String retStr = null;

            TreeMap tm = new TreeMap(String.CASE_INSENSITIVE_ORDER);
            tm.putAll(hm);

            if (tm.containsKey(keyName)) {
                Object keyValue = tm.get(keyName);
                if (keyValue != null) {
                    retStr = keyValue.toString();
                }
            }

            return retStr;
        }
    }

    @SuppressWarnings({"unchecked", "TypeMayBeWeakened"})
    public static Boolean getBooleanVal (HashMap hm, String keyName, boolean useCaseInsensitiveKey) {

        if (!useCaseInsensitiveKey) {
            return getBooleanVal(hm, keyName);
        }
        else {
            boolean retBool = false;

            TreeMap tm = new TreeMap(String.CASE_INSENSITIVE_ORDER);
            tm.putAll(hm);

            if (tm.containsKey(keyName)) {
                Object keyValue = tm.get(keyName);
                if (keyValue != null) {
                    if (keyValue instanceof Boolean) {
                        retBool = ((Boolean) keyValue);
                    }
                    else if (keyValue.toString().equals("1")) {
                        retBool = true;
                    }
                    else if (keyValue.toString().equals("0")) {
                        retBool = false;
                    }
                    else if (keyValue.toString().equalsIgnoreCase("true")) {
                        retBool = true;
                    }
                    else if (keyValue.toString().equalsIgnoreCase("false")) {
                        retBool = false;
                    }
                }
            }

            return retBool;
        }

    }

    @SuppressWarnings({"UseOfObsoleteDateTimeApi", "unchecked", "TypeMayBeWeakened"})
    public static Date getDateVal (HashMap hm, String keyName, boolean useCaseInsensitiveKey) {

        if (!useCaseInsensitiveKey) {
            return getDateVal(hm, keyName);
        }
        else {
            Date retDate = null;

            TreeMap tm = new TreeMap(String.CASE_INSENSITIVE_ORDER);
            tm.putAll(hm);

            if (tm.containsKey(keyName)) {
                Object keyValue = tm.get(keyName);
                if ((keyValue != null) && (keyValue instanceof Timestamp)) {
                    try {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(MMHTimeFormatString.YR_MO_DY_HR_MIN_SEC);
                        retDate = simpleDateFormat.parse(keyValue.toString());
                    }
                    catch (Exception e) {
                        Logger.logException("Exception parsing "+keyName+"-"+e.toString(), COMMON_LOG_FILE, e);
                    }
                }
            }

            return retDate;
        }

    }

    @SuppressWarnings({"UseOfObsoleteDateTimeApi", "unchecked", "TypeMayBeWeakened"})
    public static Calendar getCalendarVal (HashMap hm, String keyName, boolean useCaseInsensitiveKey) {

        if (!useCaseInsensitiveKey) {
            return getCalendarVal(hm, keyName);
        }
        else {
            Calendar retCal = Calendar.getInstance();
            Date d;

            TreeMap tm = new TreeMap(String.CASE_INSENSITIVE_ORDER);
            tm.putAll(hm);

            if (tm.containsKey(keyName)) {
                Object keyValue = tm.get(keyName);
                if ((keyValue != null) && (keyValue instanceof Timestamp)) {
                    try {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(MMHTimeFormatString.YR_MO_DY_HR_MIN_SEC);
                        d = simpleDateFormat.parse(keyValue.toString());
                        retCal.setTime(d);
                    }
                    catch (Exception e) {
                        Logger.logException("Exception parsing "+keyName+"-"+e.toString(), COMMON_LOG_FILE, e);
                    }
                }
            }

            return retCal;
        }

    }

    public static LocalDateTime getLocalDateTimeVal (HashMap hm, String keyName) {
        String ldtString = getStringVal(hm, keyName);

        if (!StringFunctions.stringHasContent(ldtString)) {
            return null;
        }

        LocalDateTime ldt = null;
        DateTimeFormatter[] dateTimeFormatters = new DateTimeFormatter[]{
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"),
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S"),
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"),
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss a")
                // TODO add more formatters if needed
        };

        for (DateTimeFormatter dateTimeFormatter : dateTimeFormatters) {
            try {
                ldt = LocalDateTime.parse(ldtString, dateTimeFormatter);
                break;
            }
            catch (Exception e) {
                // try the next DateTimeFormatter
            }
        }

        return ldt;
    }

    public static LocalDateTime getLocalDateTimeVal (HashMap hm, String keyName, boolean useCaseInsensitiveKey) {
        String ldtString = getStringVal(hm, keyName, useCaseInsensitiveKey);

        if (!StringFunctions.stringHasContent(ldtString)) {
            return null;
        }

        LocalDateTime ldt = null;
        DateTimeFormatter[] dateTimeFormatters = new DateTimeFormatter[]{
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"),
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S"),
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"),
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss a")
                // TODO add more formatters if needed
        };

        for (DateTimeFormatter dateTimeFormatter : dateTimeFormatters) {
            try {
                ldt = LocalDateTime.parse(ldtString, dateTimeFormatter);
                break;
            }
            catch (Exception e) {
                // try the next DateTimeFormatter
            }
        }

        return ldt;
    }

    public static String[] getStringArrVal (HashMap hm, String keyName, boolean useCaseInsensitiveKey) {
        if (!useCaseInsensitiveKey) {
            return getStringArrVal(hm, keyName);
        }
        else {
            TreeMap tm = new TreeMap(String.CASE_INSENSITIVE_ORDER);
            tm.putAll(hm);

            if (tm.containsKey(keyName)) {
                Object keyValue = tm.get(keyName);
                if (keyValue != null) {
                    String val = keyValue.toString();
                    if (StringFunctions.stringHasContent(val)) {
                        if (val.contains(",")) {
                            return val.split("\\s*,\\s*", -1);
                        }
                        else {
                            return new String[]{val};
                        }
                    }
                    else {
                        return null;
                    }
                }
            }
        }
        return null;
    }

    public static int[] getIntArrVal (HashMap hm, String keyName, boolean useCaseInsensitiveKey) {
        if (!useCaseInsensitiveKey) {
            return getIntArrVal(hm, keyName);
        }
        else {
            TreeMap tm = new TreeMap(String.CASE_INSENSITIVE_ORDER);
            tm.putAll(hm);

            if (tm.containsKey(keyName)) {
                Object keyValue = tm.get(keyName);
                if (keyValue != null) {
                    String val = keyValue.toString();
                    if (StringFunctions.stringHasContent(val)) {
                        if (val.contains(",")) {
                            String[] strArr = val.split("\\s*,\\s*", -1);
                            int[] intArr = new int[strArr.length];
                            for (int i = 0; i < strArr.length; i++) {
                                if (NumberUtils.isNumber(strArr[i])) {
                                    intArr[i] = Integer.parseInt(strArr[i]);
                                }
                            }
                            return intArr;
                        }
                        else {
                            if (NumberUtils.isNumber(val)) {
                                return new int[]{Integer.parseInt(val)};
                            }
                        }
                    }
                    else {
                        return null;
                    }
                }
            }
        }
        return null;
    }

    /**
     * <p>Safely gets a value at the key from the map.</p>
     *
     * @param m The {@link Map} to get the value at the key from.
     * @param key The {@link K} at which to find the value in the {@link Map}.
     * @return The value at the key from the map.
     */
    public static <K, V> V getValFromMap (Map<K, V> m, K key) {
        if (DataFunctions.isEmptyMap(m)) {
            Logger.logMessage("The map passed to HashMapDataFns.getValFromMap can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }
        if (m.containsKey(key)) {
            return m.get(key);
        }
        else {
            Logger.logMessage("The key wasn't contained within the map in HashMapDataFns.getValFromMap!", Logger.LEVEL.ERROR);
            return null;
        }
    }

    /**
     * <p>Safely gets a value at the key from the map.</p>
     *
     * @param m The {@link Map} to get the value at the key from.
     * @param key The {@link K} at which to find the value in the {@link Map}.
     * @param useCaseInsensitiveKey Whether or not the case of the key should be considered a factor when trying to retrieve the value.
     * @return The value at the key from the map.
     */
    @SuppressWarnings("unchecked")
    public static <K, V> V getValFromMap (Map<K, V> m, K key, boolean useCaseInsensitiveKey) {
        if (!useCaseInsensitiveKey) {
            return getValFromMap(m, key);
        }
        else {
            TreeMap tm = new TreeMap(String.CASE_INSENSITIVE_ORDER);
            tm.putAll(m);
            if (DataFunctions.isEmptyMap(tm)) {
                Logger.logMessage("The map passed to HashMapDataFns.getValFromMap can't be null or empty!", Logger.LEVEL.ERROR);
                return null;
            }
            if (tm.containsKey(key)) {
                return ((V) tm.get(key));
            }
            else {
                Logger.logMessage("The key wasn't contained within the map in HashMapDataFns.getValFromMap!", Logger.LEVEL.ERROR);
                return null;
            }
        }
    }

    /**
     * <p>Sorts the Map by putting the keys in descending order.</p>
     *
     * @param m The {@link Map} to sort the keys within.
     * @return The given {@link Map} with the keys sorted by descending order.
     */
    public static <K extends Comparable<K>, V> LinkedHashMap<K, V> makeDescKeyMap (Map<K, V> m) {

        if (DataFunctions.isEmptyMap(m)) {
            Logger.logMessage("The map passed to HashMapDataFns.makeDescKeyMap can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // sort the keys within the Map in descending order
        ArrayList<K> keys = new ArrayList<>(m.keySet());
        Collections.sort(keys);
        Collections.reverse(keys);

        // use the reversed keys to build the Map
        LinkedHashMap<K, V> retMap = new LinkedHashMap<>();
        for (K key : keys) {
            retMap.put(key, m.get(key));
        }

        return retMap;
    }

    /**
     * <p>Formats the given {@link Map} so that it can be `pretty printed`.</p>
     *
     * @param m The {@link Map} to format.
     * @return A {@link String} representing the formatted {@link Map}.
     */
    public static <K, V> String getPrettyMapStr (Map<K, V> m) {

        // make sure the map has content
        if (DataFunctions.isEmptyMap(m)) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        Iterator<Map.Entry<K, V>> itr = m.entrySet().iterator();
        sb.append("[");
        while (itr.hasNext()) {
            Map.Entry<K, V> e = itr.next();
            sb.append("KEY: ");
            sb.append(e.getKey());
            sb.append(" = VALUE: ");
            sb.append(e.getValue());
            if (itr.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");

        return sb.toString();
    }

}
