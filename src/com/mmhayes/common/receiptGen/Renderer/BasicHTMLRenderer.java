package com.mmhayes.common.receiptGen.Renderer;

import jpos.JposException;
import jpos.POSPrinterConst;

import java.text.DecimalFormat;
import java.util.ArrayList;

/*
 $Author: nyu $: Author of last commit
 $Date: 2020-09-11 14:16:39 -0400 (Fri, 11 Sep 2020) $: Date of last commit
 $Rev: 12619 $: Revision of last commit
 Notes: Basic HTML rendering
*/

public class BasicHTMLRenderer extends CommonRenderer {

    protected StringBuilder sb = new StringBuilder();
    //private static String style = "style='border:1px solid black'";
    private static String style = "style='font-family: sans-serif'";
    private static String standaloneStyle = "font-family: sans-serif";

    @Override
    public String toString()
    {
        return sb.toString();
    }

    @Override
    public boolean begin()
    {
        sb.setLength(0);
        sb.append("<table width='35%' ");
        sb.append(style);
        sb.append(" >\n");
        return true;
    }

    @Override
    public boolean end()
    {
        sb.append("</table>");
        return true;
    }

    @Override
    public boolean centerLine(String line)
    {
        sb.append("<tr><td colspan=2 align='center' "+style+">");
        sb.append(line);
        sb.append("</td></tr>\n");

        return true;
    }

    @Override
    public boolean add2ColumnLine(String col1, String col2)
    {
        sb.append("<tr>");
        sb.append("<td "+style+">");

        col1 = fixHtmlSpacing(col1);

        sb.append(col1);
        sb.append("</td>");
        sb.append("<td align='right' "+style+">");
        sb.append(col2);
        sb.append("</td>");
        sb.append("</tr>\n");

        return true;
    }

    public boolean addOneColumnLineRightAlign(String col1)
    {
        return addOneColumnLineRightAlign(col1, false);
    }

    // Use this method if we want one column, right aligned
    public boolean addOneColumnLineRightAlign(String col1, boolean wordWrapColText)
    {
        sb.append("<tr>");
        sb.append("<td colspan='2' style=\"text-align: right;" + standaloneStyle + "\">");
        sb.append(col1);
        sb.append("</td>");
        sb.append("</tr>\n");

        return true;
    }

    // Use this method if we want one column, right aligned
    public boolean addOneColumnLineLeftAlign(String col1)
    {
        sb.append("<tr>");
        sb.append("<td colspan='2' style=\"text-align: left;\"" + standaloneStyle + ">");
        sb.append(col1);
        sb.append("</td>");
        sb.append("</tr>\n");

        return true;
    }

    @Override
    public boolean addLine(String line)
    {
        sb.append("<tr>");
        sb.append("<td colspan=2 ");
        sb.append(style);
        sb.append(">");

        line = fixHtmlSpacing(line);

        sb.append(line);
        sb.append("</td>");
        sb.append("</tr>\n");

        return true;
    }

    private String fixHtmlSpacing(String line)
    {
        int numSpaces = line.length();
        // html specific code that allows us to put some spacing at the beginning of the line
        line = line.replaceAll("^\\s+", "");
        numSpaces = numSpaces - line.length();
        for(int i=0; i<numSpaces; i++)
            line = "&nbsp;" + line;
        return line;
    }

    @Override
    public boolean addBlankLine()
    {
        sb.append("<tr><td colspan=2 >");
        sb.append(" &nbsp; </td></tr>\n");
        return true;
    }

    @Override
    public void addBarcode(String txnIDStr) {

    }

    @Override
    public boolean cutPaper()
    {
        return true;
    }

    @Override
    public boolean beginTable()
    {
        sb.append("<tr><td colspan=2>\n<table BORDER=1 width=\"100%\" style=\"font-family: sans-serif\">\n");
        return true;
    }

    public void beginNutritionTable()
    {
        //Setting hard (percentage) sizes to the first two columns of the nutri table. Giving QTY 12%, Name 25%, and letting the others distribute themselves with the remainder
        sb.append("<tr><td colspan=2 >\n<table width=\"100%\" style=\"font-family: sans-serif; border-collapse: collapse; word-break:break-word; table-layout: fixed; spacing:0; margin:0; padding:0; \">" +
                "<colgroup><col style='width:12%'><col style='width:25%'></colgroup>"+"\n");
    }

    @Override
    public boolean tableRow(ArrayList row)
    {
        sb.append("<tr>");
        for(Object o : row)
        {
            sb.append("<td width='100%'>");
            sb.append(o.toString());
            sb.append("</td>");
        }
        sb.append("</tr>\n");
        return true;
    }

    public void tableNutritionRow(ArrayList row)
    {
        sb.append("<tr>");
        for(Object o : row)
        {
            sb.append("<td style=\"border: 1px solid black; padding: 0.1rem;\">");
            sb.append(o.toString());
            sb.append("</td>");
        }
        sb.append("</tr>\n");
    }

    @Override
    public void release()
    {
        // dont need to do this
        sb.setLength(0);
    }

    @Override
    public String large(String s)
    {
        return "<H1>" + s + "<H1>";
    }

    @Override
    public boolean printNVRamBitmap(int number, String alignment)
    {
        return false;
    }

    @Override
    public String indent(int count, String s)
    {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < count; i++)
        {
            sb.append("&nbsp;");
        }
        sb.append(s);
        return sb.toString();
    }


    @Override
    public boolean endTable()
    {
        sb.append("</table>\n</td></tr>");
        return true;
    }

    @Override
    public boolean beginTableHeader()
    {
        sb.append("<thead>\n");
        return true;
    }

    public boolean beginNutritionTableHeader()
    {
        sb.append("<thead style=\"font-weight: bold;\">\n");
        return true;
    }

    @Override
    public boolean endTableHeader()
    {
        sb.append("</thead>\n");
        return true;
    }

    @Override
    public boolean beginTableFooter()
    {
        sb.append("<tfoot>\n");
        return true;
    }

    @Override
    public boolean endTableFooter()
    {
        sb.append("</tfoot>\n");
        return true;
    }

    @Override
    public Exception getLastException()
    {
        return  null;
    }

}
