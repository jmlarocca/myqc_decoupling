package com.mmhayes.common.receiptGen.Renderer;

import java.util.ArrayList;

/*
 $Author: nyu $: Author of last commit
 $Date: 2020-09-11 14:16:39 -0400 (Fri, 11 Sep 2020) $: Date of last commit
 $Rev: 12619 $: Revision of last commit
 Notes: IRenderer interface all renderers must implement
*/



public interface IRenderer {
    @Override
    String toString();

    boolean begin();

    boolean end();

    boolean centerLine(String line);

    boolean addOneColumnLineRightAlign(String col1, boolean wordWrapColText);

    boolean addOneColumnLineRightAlign(String col1);

    boolean addOneColumnLineLeftAlign(String col1);

    boolean add2ColumnLine(String col1, String col2);

    boolean addLine(String line);

    boolean addBlankLine();

    boolean cutPaper();

    boolean beginTable();
    boolean endTable();

    boolean beginTableHeader();
    boolean endTableHeader();

    boolean beginTableFooter();
    boolean endTableFooter();

    boolean tableRow(ArrayList row);

    void release();

    String large(String s);
    boolean printNVRamBitmap(int number, String alignment);

    String indent(int count, String s);

    Exception getLastException();

    void addBarcode(String txnIDStr);

}
