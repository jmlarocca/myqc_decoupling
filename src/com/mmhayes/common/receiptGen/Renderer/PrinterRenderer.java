package com.mmhayes.common.receiptGen.Renderer;


import com.mmhayes.common.utils.Logger;
import jp.co.epson.uposcommon.EpsonPOSPrinterConst;
import jpos.JposConst;
import jpos.JposException;
import jpos.POSPrinter;
import jpos.POSPrinterConst;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;


/*
 $Author: nyu $: Author of last commit
 $Date: 2020-09-11 14:16:39 -0400 (Fri, 11 Sep 2020) $: Date of last commit
 $Rev: 12619 $: Revision of last commit
 Notes: PrinterRenderer Uses jpos to render receipt to printer
*/

public class PrinterRenderer extends CommonRenderer {

    private POSPrinter ptr;
    private boolean isNCR = false;
    private boolean isESC_POS = false;
    private boolean isSNBC = false;
    boolean bCreatedPrinter = false;
    boolean bSupportsCutPaperCommand = true;
    boolean isLegacyNcr7197 = false;
    String sLogicalPrinterName = "";
    private Exception lastException = null;
    private static String logFileName = "PrinterRenderer.log";

    ArrayList TableHeaders = new ArrayList();
    ArrayList TableBody = new ArrayList();
    ArrayList TableFooters = new ArrayList();
    public static enum TABLE_POS
    {
        NONE(1),
        IN_BODY(2),
        IN_HEADER(3),
        IN_FOOTER(5);
        private final int value;
        private TABLE_POS(int value) {
            this.value = value;
        }
        public int getCodeInt() {
            return value;
        }
    };
    TABLE_POS tablePos;

    public PrinterRenderer(POSPrinter posPrinter, String logicalPrinterName, boolean bCleanupPrinterInRelease)
    {
        tablePos = TABLE_POS.NONE;

        ptr = posPrinter;
        sLogicalPrinterName = logicalPrinterName;
        if(logicalPrinterName.toLowerCase().contains("esc"))
        {
            isESC_POS = true;
        }
        if(logicalPrinterName.toLowerCase().contains("snbc"))
        {
            isSNBC = true;
            isESC_POS = false;
        }
        if(logicalPrinterName.toLowerCase().contains("printer7197")){
            isNCR = true;
            isLegacyNcr7197 = true;
        }
        if(logicalPrinterName.toLowerCase().contains("printer7199")){
            isNCR = true;
        }

        bCreatedPrinter = bCleanupPrinterInRelease;

        try {
            if (isSNBC == false)
            {
                String deviceSvcDescription = ptr.getDeviceServiceDescription();
                if (deviceSvcDescription.contains("P80"))
                {
                    bSupportsCutPaperCommand = false;
                    log("Epson Printer: detected printer is TM-P80", Logger.LEVEL.TRACE);
                }
            }
        } catch (JposException e) {
            logException("PrinterRenderer() getDeviceServiceDescription", e);
        }

    }

    private void log(String msg, Logger.LEVEL loggingLevel)
    {
        StringBuilder b = new StringBuilder(Long.toString(Thread.currentThread().getId()));
        b.append(" E ");
        b.append(msg);
        Logger.logMessage(b.toString(), logFileName, loggingLevel);

    }
    private void logException(String msg, JposException e)
    {
        lastException = e;

        msg = msg + ":" + sLogicalPrinterName + " " + e.getMessage();

        StringBuilder b = new StringBuilder(Long.toString(Thread.currentThread().getId()));
        b.append(" E ");
        b.append(msg);

        Logger.logException(msg, logFileName, e);
    }

    @Override
    public boolean begin()
    {
        boolean result = false;

        if(ptr != null)
        {
            try
            {
                ptr.transactionPrint(POSPrinterConst.PTR_S_RECEIPT, POSPrinterConst.PTR_TP_TRANSACTION);
                result = true;
            }
            catch (JposException e)
            {
                // Ignore for the SNBC printers
                if (sLogicalPrinterName.equals("SNBC_BK-T680_Printer") == false)
                {
                    logException("PrinterRenderer:Begin() start transactionPrint", e);
                }
                else
                {
                    result = true;
                }
            }
        }

        return result;
    }

    @Override
    public boolean end()
    {
        boolean result = false;

        if(ptr != null)
        {
            try
            {
                ptr.transactionPrint(POSPrinterConst.PTR_S_RECEIPT, POSPrinterConst.PTR_TP_NORMAL);
                result = true;
            }
            catch (JposException e)
            {
                // Ignore for the SNBC printers
                if (sLogicalPrinterName.equals("SNBC_BK-T680_Printer") == false)
                {
                    logException("PrinterRenderer:End() end transactionPrint", e);
                }
                else
                {
                    result = true;
                }
            }
        }
        return result;
    }

    @Override
    public void addBarcode(String txnIDStr) {
        try {
            addBarcode(txnIDStr, ptr, logFileName);
        } catch (JposException e) {
            logException("PrinterRenderer:addBarcode() ", e);
        }
    }


    @Override
    public boolean centerLine(String line)
    {
        if(ptr == null)
        {
            return false;
        }

        try {
            if (isSNBC){
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|cA" + line + "\n");
            } else {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n" + "\u001b|cA" + line);
            }
            return true;
        } catch (JposException e) {
            logException("PrinterRenderer:CenterLine() printNormal", e);
        }
        return false;
    }

    @Override
    public boolean add2ColumnLine(String col1, String col2) {
        if (ptr == null) {
            return false;
        }

        try {
            if (isSNBC) {
                    //calculate the space to pad left on the col2 value
                    customSnbcAddTwoColumnLine(col1, col2);
            } else {
                int maxLeftColCharLength = 35;
                try {
                    col1 = formatColumnText(col1, maxLeftColCharLength);
                } catch (Exception ex){
                    //if an error occurred, just use the col1 value
                }

                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n" + col1);
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + col2);
            }
            return true;
        } catch (JposException e) {
            logException("PrinterRenderer:Add2ColumnLine() printNormal", e);
        }
        return false;
    }

    //For SNBC printers the right align command isn't being recognized.  To get the alignment correct we had to calculate the padding.
    private void customSnbcAddTwoColumnLine(String col1, String col2) throws JposException {
        if (col1 == null) {
            col1 = "";
        }

        if (col1.isEmpty()) { //When col1 is empty, the alignment works fine
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, col1);
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + col2 + "\n");
        } else {
            if (col1.length() < 5) {
                col1 = String.format("%1$-5s", col1); //HP2022- pad Column 1 to 5 char so the spacing prints properly.
            }

            int totalColLength = col1.length() + col2.length();
            int maxReceiptLineLength = 46;
            int leftPadWidth;
            StringBuilder spaceMultiplied = new StringBuilder();

            //product name and amount is too long, truncate and add ellipsis
            if (totalColLength > maxReceiptLineLength) {
                int col2Length = col2.length();
                int requiredNumLinesForCol1 = maxReceiptLineLength - col2Length;

                requiredNumLinesForCol1 = requiredNumLinesForCol1 - 3; //take into account the ellipses
                String newCol1 = col1.substring(0, requiredNumLinesForCol1);
                newCol1 = newCol1 + "...";
                col1 = newCol1;
            } else { //create padding
                leftPadWidth = maxReceiptLineLength - totalColLength;
                for (Integer i = 0; i < leftPadWidth; i++) {
                    spaceMultiplied.append(" ");
                }
                col2 = spaceMultiplied.toString() + col2;
            }

            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n" + col1);
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + col2);
        }
    }

    public boolean addOneColumnLineRightAlign(String col1) {
        return addOneColumnLineRightAlign(col1, false);
    }

    public boolean addOneColumnLineRightAlign(String col1, boolean wordWrapColText) {
        if (ptr == null) {
            return false;
        }

        try {
            if (isSNBC) {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|rA" + col1 + "\n");
            } else {
                if (wordWrapColText) {
                    int maxLeftColCharLength = 35;
                    try {
                        col1 = formatColumnText(col1, maxLeftColCharLength);
                    } catch (Exception ex){
                        //if an error occurred, just use the col1 value
                    }
                }
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n" + "\u001b|rA" + col1);
            }
            return true;
        } catch (JposException e) {
            logException("PrinterRenderer:addOneColumnLineRightAlign() printNormal", e);
        }
        return false;
    }

    public boolean addOneColumnLineLeftAlign(String col1)
    {
        if(ptr == null)
        {
            return false;
        }

        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n" + col1);
            return true;
        } catch (JposException e) {
            logException("PrinterRenderer:addOneColumnLineLeftAlign() printNormal", e);
        }
        return false;
    }

    @Override
    public boolean addLine(String line)
    {
        if(ptr == null)
        {
            return false;
        }

        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n" + line);
            return true;
        } catch (JposException e) {
            logException("PrinterRenderer:AddLine() printNormal", e);
        }
        return false;

    }

    @Override
    public boolean addBlankLine()
    {
        if(ptr == null)
        {
            return false;
        }

        try {
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
            return true;
        } catch (JposException e) {
            logException("PrinterRenderer:AddBlankLine() printNormal", e);
        }
        return false;

    }

    @Override
    public boolean cutPaper()
    {
        if(ptr == null)
        {
            return false;
        }

        try {
            if(isNCR)
            {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
            }
            ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\u001b|5lF");

            if(bSupportsCutPaperCommand)
            {
                ptr.cutPaper(100);
            }
            return true;
        } catch (JposException e) {
            logException("PrinterRenderer:cutPaper()", e);
        }
        return false;

    }

    @Override
    public boolean beginTable()
    {
        tablePos = TABLE_POS.IN_BODY;

        return true;
    }


    @Override
    public boolean tableRow(ArrayList row)
    {
        switch (tablePos)
        {
            case IN_HEADER:
                TableHeaders.add(row);
                break;
            case IN_BODY:
                TableBody.add(row);
                break;
            case IN_FOOTER:
                TableFooters.add(row);
                break;
        }
        return true;
    }

    @Override
    public void release()
    {
        if (bCreatedPrinter && null != ptr)
        {
            try {
                ptr.setDeviceEnabled(false);
                ptr.release();
                ptr.close();
            } catch (JposException e) {
                logException("PrinterRenderer:Release()", e);
            }
        }
    }

    @Override
    public String large(String s)
    {
        if(isESC_POS)
        {
            return "\u001b|4C" + s;
        }
        else
        {
            return "\u001b|bC\u001b|4C" + s;
        }
    }

    @Override
    public boolean printNVRamBitmap(int number, String alignment)
    {
        int[] pram2 = new int[1];
        pram2[0] = number;
        try {
            ptr.directIO(EpsonPOSPrinterConst.PTR_DI_PRINT_FLASH_BITMAP, pram2, alignment);
        } catch (JposException e) {
            logException("PrinterRenderer:printNVRamBitmap() directIO", e);
            return false;
        }

        return true;
    }

    @Override
    public String indent(int count, String s)
    {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < count; i++)
        {
            sb.append(" ");
        }
        sb.append(s);

        return sb.toString();
    }

    @Override
    public boolean endTable()
    {
        if (isESC_POS){
           return createTableEscPos();
        }

        tablePos = TABLE_POS.NONE;

        ArrayList items = TableBody;
        ArrayList nutritionTotals = TableFooters;
        ArrayList categoryNames = TableHeaders; // This one may not be right because it was expanded to two rows


        log("Printer: creating nutritional info image", Logger.LEVEL.DEBUG);
        int width = 512;

        int height = items.size() * 35 + 200;
        int rowHeight = 35;
        ArrayList item;

        // Truncate image if necessary; NCR printer only allows up to 12 items
        int numItemsAllowed = items.size();
        int numItemRows = items.size();
        boolean imageNeedsTruncating = false;

        if ((isLegacyNcr7197 || isSNBC) && items.size() + nutritionTotals.size() - 1 > 15)
        {
            numItemsAllowed = 14 - nutritionTotals.size();
            numItemRows = numItemsAllowed + 1;
            imageNeedsTruncating = true;
            height = numItemRows * 35 + 200;
        }

        // Initializes a blank picture with a white background
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = image.createGraphics();
        Font textFont = new Font("Arial", Font.PLAIN, 18);
        Font textFontBold = new Font("Arial", Font.BOLD, 18);
        g2d.setFont(textFontBold);

        // Set background to be white
        g2d.setColor(Color.white);
        g2d.fillRect(0, 0, width, height);

        // Set font/grid color
        g2d.setColor(Color.black);
        g2d.setStroke(new BasicStroke(2));

        // Print column names
        g2d.setFont(textFontBold);

        int rowNum = 0;
        int yPos = 50;
        for (Object item1 : categoryNames)
        {
            item = (ArrayList) item1;
            g2d.drawString(item.get(0).toString(), 5,   yPos);      // Quantity
            g2d.drawString(item.get(1).toString(), 50,  yPos);     // Item name
            g2d.drawString(item.get(2).toString(), 217, yPos);    // #1
            g2d.drawString(item.get(3).toString(), 277, yPos);    // #2
            g2d.drawString(item.get(4).toString(), 337, yPos);    // #3
            g2d.drawString(item.get(5).toString(), 403, yPos);    // #4
            g2d.drawString(item.get(6).toString(), 457, yPos);    // #5
            rowNum++;
            yPos += 20;
        }

        // Draw item grid rows
        //int rowNum = 0;
        rowNum = 0;
        for (int x = 0; x < numItemRows; x++)
        {
            drawRow(0, 35 * rowNum + 85, g2d);
            rowNum++;
        }

        // Draw grid rows for totals
        int numOfDVRows = nutritionTotals.size() + rowNum;
        drawRowsForTotals(rowNum, numOfDVRows, g2d);

        // Print item nutritional info
        rowNum = 0;
        g2d.setFont(textFont);
        for (int iCount = 0; iCount < numItemsAllowed; iCount++)
        {
            item = (ArrayList) items.get(iCount);
            g2d.drawString(item.get(0).toString(), 5, (rowNum * 35) + 114);      // Quantity
            g2d.drawString(item.get(1).toString(), 50, (rowNum * 35) + 114);     // Item name
            g2d.drawString(item.get(2).toString(), 217, (rowNum * 35) + 114);    // #1
            g2d.drawString(item.get(3).toString(), 277, (rowNum * 35) + 114);    // #2
            g2d.drawString(item.get(4).toString(), 337, (rowNum * 35) + 114);    // #3
            g2d.drawString(item.get(5).toString(), 403, (rowNum * 35) + 114);    // #4
            g2d.drawString(item.get(6).toString(), 457, (rowNum * 35) + 114);    // #5

            rowNum++;
        }

        if (imageNeedsTruncating)
        {
            g2d.drawString("...", 5, (rowNum * 35) + 103);      // Quantity
            g2d.drawString("...", 50, (rowNum * 35) + 103);     // Item name
            g2d.drawString("...", 217, (rowNum * 35) + 103);    // #1
            g2d.drawString("...", 277, (rowNum * 35) + 103);    // #2
            g2d.drawString("...", 337, (rowNum * 35) + 103);    // #3
            g2d.drawString("...", 403, (rowNum * 35) + 103);    // #4
            g2d.drawString("...", 457, (rowNum * 35) + 103);    // #5
        }

        // Print nutritional totals
        if (imageNeedsTruncating)
        {
            rowNum = numItemsAllowed + 1;
        }
        else
        {
            rowNum = numItemsAllowed;
        }

        for (int x = 0; x < nutritionTotals.size(); x++)
        {
            item = (ArrayList) nutritionTotals.get(x);
            g2d.drawString(item.get(0).toString(),   0, (rowNum * 35) + 112);    // Qty
            g2d.drawString(item.get(1).toString(),  10, (rowNum * 35) + 112);    // Item name
            g2d.drawString(item.get(2).toString(), 217, (rowNum * 35) + 112);    // #1
            g2d.drawString(item.get(3).toString(), 277, (rowNum * 35) + 112);    // #2
            g2d.drawString(item.get(4).toString(), 337, (rowNum * 35) + 112);    // #3
            g2d.drawString(item.get(5).toString(), 403, (rowNum * 35) + 112);    // #4
            g2d.drawString(item.get(6).toString(), 457, (rowNum * 35) + 112);    // #5
            rowNum++;
        }

        String extension = "jpg";
        if(isNCR || isSNBC)
        {
            extension = "BMP";
        }

        try {
            // Save the picture to a file
            String sFileName = "C:/MMHayes/NutritionReceipts/Receipt." + extension;
            File file = new File(sFileName);

            deleteReceiptImage(file, 5, sFileName, "Before printBitmap");
            if (!file.createNewFile()) {
                log(" endTable createNewFile() failed to create file '" + sFileName + "'", Logger.LEVEL.ERROR);
                return false;
            }
            ImageIO.write(image, extension, file);

            if(ptr == null)
            {
                return false;
            }

            // Print neatly by setting map mode to ptr_mm_metric
            ptr.setMapMode(POSPrinterConst.PTR_MM_METRIC);

            // Output by the high quality mode
            ptr.setRecLetterQuality(true);

            int bmWidth = POSPrinterConst.PTR_BM_ASIS;

            if (isSNBC){
                bmWidth = 6000;   //override the width of the barcode
            }

            if(!isNCR && !isSNBC)
            {
                int []pram2 = new int[1];
                pram2[0] = EpsonPOSPrinterConst.PTR_DI_BITMAP_PRINTING_MULTI_TONE;
                ptr.directIO(EpsonPOSPrinterConst.PTR_DI_SET_BITMAP_PRINTING_TYPE, pram2, "");
                bmWidth = POSPrinterConst.PTR_BM_ASIS;
            }

            ptr.printBitmap(POSPrinterConst.PTR_S_RECEIPT, sFileName, bmWidth, POSPrinterConst.PTR_BM_CENTER);

            deleteReceiptImage(file, 5, sFileName, "After printBitmap");
        } catch (JposException ex) {
            logException("PrinterRenderer:EndTable() ", ex);
        } catch (IOException e) {
            Logger.logException("IOException PrinterRenderer:EndTable() " + e.getMessage(), logFileName, e);
        }

        return false;
    }

    private boolean createTableEscPos(){
        tablePos = TABLE_POS.NONE;

        ArrayList items = TableBody;
        ArrayList nutritionTotals = TableFooters;
        ArrayList categoryNames = TableHeaders; // This one may not be right because it was expanded to two rows

            try {
                ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n\n");
            } catch (JposException e) {
                logException("PrinterRenderer:EndTable() printing headers", e);
            }

            String s =  "";
            String sFmt = "%2s %14s %4s %4s %4s %4s %4s";
            for (Object item1 : categoryNames)
            {
                ArrayList item = (ArrayList) item1;
                s = String.format(sFmt,
                        item.get(0).toString(),     // Quantity
                        item.get(1).toString(),     // Item name
                        item.get(2).toString(),     // #1
                        item.get(3).toString(),     // #2
                        item.get(4).toString(),     // #3
                        item.get(5).toString(),     // #4
                        item.get(6).toString()      // #5
                );

                try {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, s + "\n");
                } catch (JposException e) {
                    logException("PrinterRenderer:EndTable() printing headers", e);
                }

                sFmt = "%2s %15s %4s %4s %4s %4s %4s";
            }

            for (Object item1 : items)
            {
                ArrayList item = (ArrayList) item1;
                s = String.format("%2s %15s %4s %4s %4s %4s %4s",
                        item.get(0).toString(),  // Quantity
                        item.get(1).toString(),   // Item name
                        item.get(2).toString(),   // #1
                        item.get(3).toString(),   // #2
                        item.get(4).toString(),   // #3
                        item.get(5).toString(),   // #4
                        item.get(6).toString()    // #5
                );
                try {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, s + "\n");
                } catch (JposException e) {
                    logException("PrinterRenderer:EndTable() printing lines", e);
                }
            }

            for (int x = 0; x < nutritionTotals.size(); x++)
            {
                ArrayList item = (ArrayList) nutritionTotals.get(x);
                s = String.format("%18.18s %4s %4s %4s %4s %4s",
                        item.get(1).toString(),   // Item name
                        item.get(2).toString(),   // #1
                        item.get(3).toString(),   // #2
                        item.get(4).toString(),   // #3
                        item.get(5).toString(),   // #4
                        item.get(6).toString()    // #5
                );
                try {
                    ptr.printNormal(POSPrinterConst.PTR_S_RECEIPT, s + "\n");
                } catch (JposException e) {
                    logException("PrinterRenderer:EndTable() printing totals", e);
                }
            }

            return true;
    }

    public void drawRow(int x, int y, Graphics2D g2d)
    {
        int rowHeight = 35;

        // Draw qty box
        g2d.drawRect(x, y, 35, rowHeight);

        // Draw item name box
        g2d.drawRect(x + 35, y, 177, rowHeight);

        // Draw category #1 box
        g2d.drawRect(x + 212, y, 60, rowHeight);

        // Draw category #2 box
        g2d.drawRect(x + 272, y, 60, rowHeight);

        // Draw category #3 box
        g2d.drawRect(x + 332, y, 60, rowHeight);

        // Draw category #4 box
        g2d.drawRect(x + 392, y, 60, rowHeight);

        // Draw category #5 box
        g2d.drawRect(x + 452, y, 60, rowHeight);
    }

    public void drawRowsForTotals(int rowNum, int numOfDVRows, Graphics g2d)
    {
        int rowHeight = 35;

        for (int x = rowNum; x < numOfDVRows; x++)
        {
            g2d.drawRect(212, 35 * rowNum + 85, 60, rowHeight);
            g2d.drawRect(272, 35 * rowNum + 85, 60, rowHeight);
            g2d.drawRect(332, 35 * rowNum + 85, 60, rowHeight);
            g2d.drawRect(392, 35 * rowNum + 85, 60, rowHeight);
            g2d.drawRect(452, 35 * rowNum + 85, 60, rowHeight);
            rowNum++;
        }
    }

    /**
     * Try n number of times to delete the file parameter
     * B-06520: this was a problem for the NCR 7199 printer due to the size of the BMP image
     * @param file
     * @param tries
     * @param sFileName
     * @param logMessage
     */
    private void deleteReceiptImage(File file, int tries, String sFileName, String logMessage){
        for (int i = 0; i < tries; i++){

            if (file.exists()) {
                System.gc(); //without this the image would be locked by Tomcat

                log(logMessage + ". endTable: file exists, trying to delete '" + sFileName + "'", Logger.LEVEL.DEBUG);
                boolean isDeleted = file.delete();
                int iTry = i + 1;
                log(" Try: " + iTry, Logger.LEVEL.DEBUG);
                log(" isDeleted: " + isDeleted + "'", Logger.LEVEL.DEBUG);
            }
        }
    }

    /**
     * To avoid a line printing too long, add spaces to force the line to wrap
     * When forcing the line to wrap, try and split the test at a space, so no words are split up
     * Example: Product Name: Pasta with sauce, bread, cheese, and broccoli
     *
     * @param columnText
     * @param maxColumnWidth
     * @return
     * @throws JposException
     */
    public String formatColumnText(String columnText, int maxColumnWidth) throws JposException {

        if (columnText != null && !columnText.isEmpty()) {
            int maxReceiptWidth = 42;
            int wrapTextAtPos = 0;

            try {
                maxReceiptWidth = ptr.getRecLineChars(); //Try and get the Receipt Line Char config setting from jpos
                log("Receipt line Max Char length set to: " + ptr.getRecLineChars(), Logger.LEVEL.DEBUG);
            } catch (Exception e) {

            }
            wrapTextAtPos = maxReceiptWidth + 1; //a word wrap will initiate at this length

            if (columnText.length() > maxColumnWidth) {
                try {
                    int bestId = getBestSplitId(columnText, maxColumnWidth); //try and word wrap on a space between words
                    if (bestId > -1) {
                        maxColumnWidth = bestId;
                    }

                    String firstPartColText = columnText.substring(0, maxColumnWidth);
                    String secondPartColText = columnText.substring(maxColumnWidth, columnText.length());

                    log("First Part of Text: " + firstPartColText, Logger.LEVEL.DEBUG);
                    log("Second Part of Text: " + secondPartColText, Logger.LEVEL.DEBUG);

                    int spacesToAdd = wrapTextAtPos - firstPartColText.length();
                    StringBuilder sb = new StringBuilder();

                    //pad the first part of text with spaces in order to force a word wrap.
                    for (int i = 0; i < spacesToAdd; i++) {
                        sb.append(" ");
                    }

                    columnText = firstPartColText + sb.toString() + secondPartColText;
                    log("New Column text: " + columnText, Logger.LEVEL.DEBUG);

                } catch (Exception ex) {
                    //If an error occurs, just return the original column text
                }
            }
        }

        return columnText;
    }

    /**
     * Try and split the string on a space (" "), so it does not word wrap in the middle of a word
     * @param columnText
     * @param maxColWidth
     * @return
     */
    private int getBestSplitId(String columnText, int maxColWidth){
        int bestId = -1;
        StringBuilder sb = new StringBuilder();
        int minId = maxColWidth - 7;

        String[] words = columnText.split(" ");
        for (int i = 0; i < words.length; i++){
            String prevStrVal = sb.toString();
            sb.append(words[i]);
            if (sb.toString().length() > minId && sb.toString().length() < maxColWidth + 1){
                bestId = sb.toString().length() + 1;
            } else if (sb.toString().length() > maxColWidth + 1) {
                // This is to handle the case where the word we just added takes us over the max column width
                // If this happens, just use the length of the string from before our most recent
                bestId = prevStrVal.length() - 1;
                break;
            }
            sb.append(" ");
        }

        return bestId;
    }

    @Override
    public boolean beginTableHeader()
    {
        tablePos = TABLE_POS.IN_HEADER;

        return false;
    }

    @Override
    public boolean endTableHeader()
    {
        tablePos = TABLE_POS.IN_BODY;
        return false;
    }

    @Override
    public boolean beginTableFooter()
    {
        tablePos = TABLE_POS.IN_FOOTER;
        return false;
    }

    @Override
    public boolean endTableFooter()
    {
        tablePos = TABLE_POS.IN_BODY;
        return false;
    }

    @Override
    public Exception getLastException()
    {
        return lastException;
    }
}
