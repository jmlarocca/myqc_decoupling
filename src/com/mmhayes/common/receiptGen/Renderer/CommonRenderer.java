package com.mmhayes.common.receiptGen.Renderer;

import com.mmhayes.common.utils.Logger;
import jpos.JposException;
import jpos.POSPrinter;
import jpos.POSPrinterConst;

/**
 * Created by admin on 7/31/2018.
 */
public abstract class CommonRenderer implements IRenderer {

    public static void addBarcode(String txnIDStr, POSPrinter ptr, String logFileName) throws JposException {

        Logger.logMessage("In CommonRenderer.addBarcode", logFileName, Logger.LEVEL.DEBUG);

        if (ptr != null) {
            Logger.logMessage(String.format("POSPrinter ptr = %s", ptr.toString()), logFileName, Logger.LEVEL.DEBUG);

            ptr.setMapMode(POSPrinterConst.PTR_MM_DOTS);
            ptr.printBarCode(POSPrinterConst.PTR_S_RECEIPT,
                    txnIDStr,
                    POSPrinterConst.PTR_BCS_Code128, 75, 75,
                    POSPrinterConst.PTR_BC_CENTER,
                    POSPrinterConst.PTR_BC_TEXT_NONE);
        }

    }
}
