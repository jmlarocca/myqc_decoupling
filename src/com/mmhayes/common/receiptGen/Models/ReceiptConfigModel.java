package com.mmhayes.common.receiptGen.Models;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.TypeData;

import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-08 13:01:35 -0400 (Mon, 08 Jul 2019) $: Date of last commit
 $Rev: 9041 $: Revision of last commit
*/
public class ReceiptConfigModel {

    //Main
    private boolean printReceipt;
    private boolean printOrderNumber;
    private boolean printTransBarCode;
    private boolean printPluCodes;

    private boolean printGrossWeight;
    private boolean printNetWeight;

    //Nutrition
    private boolean printNutritionInfo;
    private boolean needsNAFootNote;
    private boolean needsTruncateDigitsFootnote;
    //nutritionItems
    //nutritionCategories
    //nutritionTotals

    //Open Transactions
    private boolean printSuspendedTransReceipt;

    private Integer receiptTypeId = TypeData.ReceiptType.PRINTEDRECEIPT;
    private Integer terminalId = null;
    private Integer badgeNumDigitsPrinted = 0;
    private Integer numCopiesToPrint = 0; //This is used to enable printing of the Customer Copy
    //QC_PATender.PrintSignatureReceipt // This is used to enable payment gateway Merchant Copy
    //This is for Quickcharge, Credit Cards, etc.

    private Integer paymentProcessorId = null; //This will be used to determine what payment gateway gets printed
    /*PaymentProcessorID	Name
    1	Stripe
    2	Datacap NETePay
    3	Datacap NETePay EMV
    4	Merchant Link
    5	Datacap NETePay EMV PCI P2PE
    6	FreedomPay*/

    private boolean printProcessorCustomerCopy; // This is used to enable payment gateway Customer Copy

    //receipt information
    private String receiptAcctInfoLine1 = "";
    private String receiptAcctInfoLine2 = "";
    private String receiptAcctInfoLine3 = "";
    private String receiptAcctInfoLine4 = "";
    private String receiptAcctInfoLine5 = "";

    //header
    private String receiptHeader1 = "";
    private String receiptHeader2 = "";
    private String receiptHeader3 = "";
    private String receiptHeader4 = "";
    private String receiptHeader5 = "";

    //footer
    private String receiptFooter1 = "";
    private String receiptFooter2 = "";
    private String receiptFooter3 = "";
    private String receiptFooter4 = "";
    private String receiptFooter5 = "";

    private String paymentProcessorName = "";

    private DataManager dm = new DataManager();

    public ReceiptConfigModel(Integer terminalId){
        this.setTerminalId(terminalId);
    }

    public void populateTerminalReceiptConfig(){
        ArrayList<HashMap> receiptConfigList = dm.parameterizedExecuteQuery("data.posapi30.getReceiptConfig",
                new Object[]{
                        this.getTerminalId()
                },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        if (receiptConfigList != null && receiptConfigList.size() > 0){
            HashMap receiptConfigHM = receiptConfigList.get(0);
            setModelProperties(receiptConfigHM);
        }
    }

    public void setModelProperties(HashMap modelDetailHM){
        setPrintReceipt(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PRINTRECEIPT"), false));
        setPrintOrderNumber(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PAPRINTORDERNUMBER"), false));
        setPrintNutritionInfo(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PAPRINTNUTRITIONINFO"), false));
        setPrintTransBarCode(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PAPRINTTRANSBARCODE"), false));
        setPrintPluCodes(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PAPRINTPLUCODES"), false));
        setPrintSuspendedTransReceipt(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PAPRINTSUSPENDEDTXNSLIP"), false));
        setPrintGrossWeight(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PRINTTXNGROSSWEIGHT"), false));
        setPrintNetWeight(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PRINTTXNNETWEIGHT"), false));
        setBadgeNumDigitsPrinted(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PABADGENUMDIGITSPRINTED")));
        setNumCopiesToPrint(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PARECEIPTCOPIESPRINT")));
        setPaymentProcessorId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAYMENTPROCESSORID")));
        setPaymentProcessorName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTPROCESSORNAME")));

        setPrintProcessorCustomerCopy(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PRINTMERCHANTLINKCUSTCOPY"), false));

        setReceiptAcctInfoLine1(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTACCTINFOLINE1")));
        setReceiptAcctInfoLine2(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTACCTINFOLINE2")));
        setReceiptAcctInfoLine3(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTACCTINFOLINE3")));
        setReceiptAcctInfoLine4(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTACCTINFOLINE4")));
        setReceiptAcctInfoLine5(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTACCTINFOLINE5")));
        setReceiptHeader1(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTHEADER1")));
        setReceiptHeader2(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTHEADER2")));
        setReceiptHeader3(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTHEADER3")));
        setReceiptHeader4(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTHEADER4")));
        setReceiptHeader5(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTHEADER5")));
        setReceiptFooter1(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTFOOTER1")));
        setReceiptFooter2(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTFOOTER2")));
        setReceiptFooter3(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTFOOTER3")));
        setReceiptFooter4(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTFOOTER4")));
        setReceiptFooter5(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTFOOTER5")));

    }

    /**
     *
     * @param baseSettings = kiosk terminal or base terminal
     * This can be overridden by the Online Ordering terminal settings
     */
    public void validateSettings(ReceiptConfigModel baseSettings){
        Integer paymentProcessorId = baseSettings.getPaymentProcessorId();

        if (this.getPaymentProcessorId() == null){
            this.setPaymentProcessorId(paymentProcessorId);

        }
    }

    public boolean isPrintReceipt() {
        return printReceipt;
    }

    public void setPrintReceipt(boolean printReceipt) {
        this.printReceipt = printReceipt;
    }

    public boolean isPrintOrderNumber() {
        return printOrderNumber;
    }

    public void setPrintOrderNumber(boolean printOrderNumber) {
        this.printOrderNumber = printOrderNumber;
    }

    public boolean isPrintTransBarCode() {
        return printTransBarCode;
    }

    public void setPrintTransBarCode(boolean printTransBarCode) {
        this.printTransBarCode = printTransBarCode;
    }

    public boolean isPrintPluCodes() {
        return printPluCodes;
    }

    public void setPrintPluCodes(boolean printPluCodes) {
        this.printPluCodes = printPluCodes;
    }

    public void setPrintGrossWeight(boolean printGross)
    {
        printGrossWeight = printGross;
    }
    public Boolean getPrintGrossWeight()
    {
        return printGrossWeight;
    }

    public void setPrintNetWeight(boolean printNet)
    {
        printNetWeight = printNet;
    }
    public Boolean getPrintNetWeight()
    {
        return printNetWeight;
    }

    public boolean isPrintNutritionInfo() {
        return printNutritionInfo;
    }

    public void setPrintNutritionInfo(boolean printNutritionInfo) {
        this.printNutritionInfo = printNutritionInfo;
    }

    public boolean isNeedsNAFootNote() {
        return needsNAFootNote;
    }

    public void setNeedsNAFootNote(boolean needsNAFootNote) {
        this.needsNAFootNote = needsNAFootNote;
    }

    public Integer getReceiptTypeId() {
        return receiptTypeId;
    }

    public void setReceiptTypeId(Integer receiptTypeId) {
        this.receiptTypeId = receiptTypeId;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public Integer getBadgeNumDigitsPrinted() {
        return badgeNumDigitsPrinted;
    }

    public void setBadgeNumDigitsPrinted(Integer badgeNumDigitsPrinted) {
        this.badgeNumDigitsPrinted = badgeNumDigitsPrinted;
    }

    public Integer getNumCopiesToPrint() {
        return numCopiesToPrint;
    }

    public void setNumCopiesToPrint(Integer numCopiesToPrint) {
        this.numCopiesToPrint = numCopiesToPrint;
    }

    public boolean isNeedsTruncateDigitsFootnote() {
        return needsTruncateDigitsFootnote;
    }

    public Integer getPaymentProcessorId() {
        return paymentProcessorId;
    }

    public void setPaymentProcessorId(Integer paymentProcessorId) {
        this.paymentProcessorId = paymentProcessorId;
    }

    public void setNeedsTruncateDigitsFootnote(boolean needsTruncateDigitsFootnote) {
        this.needsTruncateDigitsFootnote = needsTruncateDigitsFootnote;
    }

    public boolean isPrintSuspendedTransReceipt() {
        return printSuspendedTransReceipt;
    }

    public void setPrintSuspendedTransReceipt(boolean printSuspendedTransReceipt) {
        this.printSuspendedTransReceipt = printSuspendedTransReceipt;
    }

    public String getReceiptAcctInfoLine1() {
        return receiptAcctInfoLine1;
    }

    public void setReceiptAcctInfoLine1(String receiptAcctInfoLine1) {
        this.receiptAcctInfoLine1 = receiptAcctInfoLine1;
    }

    public String getReceiptAcctInfoLine2() {
        return receiptAcctInfoLine2;
    }

    public void setReceiptAcctInfoLine2(String receiptAcctInfoLine2) {
        this.receiptAcctInfoLine2 = receiptAcctInfoLine2;
    }

    public String getReceiptAcctInfoLine3() {
        return receiptAcctInfoLine3;
    }

    public void setReceiptAcctInfoLine3(String receiptAcctInfoLine3) {
        this.receiptAcctInfoLine3 = receiptAcctInfoLine3;
    }

    public String getReceiptAcctInfoLine4() {
        return receiptAcctInfoLine4;
    }

    public void setReceiptAcctInfoLine4(String receiptAcctInfoLine4) {
        this.receiptAcctInfoLine4 = receiptAcctInfoLine4;
    }

    public String getReceiptAcctInfoLine5() {
        return receiptAcctInfoLine5;
    }

    public void setReceiptAcctInfoLine5(String receiptAcctInfoLine5) {
        this.receiptAcctInfoLine5 = receiptAcctInfoLine5;
    }

    public String getReceiptHeader1() {
        return receiptHeader1;
    }

    public void setReceiptHeader1(String receiptHeader1) {
        this.receiptHeader1 = receiptHeader1;
    }

    public String getReceiptHeader2() {
        return receiptHeader2;
    }

    public void setReceiptHeader2(String receiptHeader2) {
        this.receiptHeader2 = receiptHeader2;
    }

    public String getReceiptHeader3() {
        return receiptHeader3;
    }

    public void setReceiptHeader3(String receiptHeader3) {
        this.receiptHeader3 = receiptHeader3;
    }

    public String getReceiptHeader4() {
        return receiptHeader4;
    }

    public void setReceiptHeader4(String receiptHeader4) {
        this.receiptHeader4 = receiptHeader4;
    }

    public String getReceiptHeader5() {
        return receiptHeader5;
    }

    public void setReceiptHeader5(String receiptHeader5) {
        this.receiptHeader5 = receiptHeader5;
    }

    public String getReceiptFooter1() {
        return receiptFooter1;
    }

    public void setReceiptFooter1(String receiptFooter1) {
        this.receiptFooter1 = receiptFooter1;
    }

    public String getReceiptFooter2() {
        return receiptFooter2;
    }

    public void setReceiptFooter2(String receiptFooter2) {
        this.receiptFooter2 = receiptFooter2;
    }

    public String getReceiptFooter3() {
        return receiptFooter3;
    }

    public void setReceiptFooter3(String receiptFooter3) {
        this.receiptFooter3 = receiptFooter3;
    }

    public String getReceiptFooter4() {
        return receiptFooter4;
    }

    public void setReceiptFooter4(String receiptFooter4) {
        this.receiptFooter4 = receiptFooter4;
    }

    public String getReceiptFooter5() {
        return receiptFooter5;
    }

    public void setReceiptFooter5(String receiptFooter5) {
        this.receiptFooter5 = receiptFooter5;
    }

    public String getPaymentProcessorName() {
        return paymentProcessorName;
    }

    public void setPaymentProcessorName(String paymentProcessorName) {
        this.paymentProcessorName = paymentProcessorName;
    }

    public boolean isPrintProcessorCustomerCopy() {
        return printProcessorCustomerCopy;
    }

    public void setPrintProcessorCustomerCopy(boolean printProcessorCustomerCopy) {
        this.printProcessorCustomerCopy = printProcessorCustomerCopy;
    }

}
