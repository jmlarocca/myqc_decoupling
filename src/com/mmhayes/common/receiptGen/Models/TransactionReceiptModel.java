package com.mmhayes.common.receiptGen.Models;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.TransactionBuilder;
import com.mmhayes.common.login.LoginModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.TenderDisplayModel;
import com.mmhayes.common.transaction.models.TenderLineItemModel;
import com.mmhayes.common.transaction.models.TenderModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.Logger;

//other dependencies
import java.util.HashMap;
import java.util.Map;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-08 13:01:35 -0400 (Mon, 08 Jul 2019) $: Date of last commit
 $Rev: 9041 $: Revision of last commit
*/
public class TransactionReceiptModel {
    private TransactionModel transactionModel = null;
    private ReceiptModel receiptModel = null;
    private ReceiptConfigModel receiptConfigModel = null;
    private HashMap<Integer, TenderLineItemModel> tenderLinesHM = new HashMap<>();
    private static String logFileName = "ConsolidatedReceipt.log";

    public void populateReceiptConfig() throws Exception {

        if (this.getReceiptModel() != null) {
            if (this.getTransactionModel() != null) {
                if (this.getReceiptModel().getTerminalId() == null) {
                    //Populate the terminal if it exists
                    if (this.getTransactionModel().getTerminal() != null&& this.getTransactionModel().getTerminal().getId() != null) {
                        this.getReceiptModel().setTerminalId(this.getTransactionModel().getTerminal().getId());
                    }
                }

                //populate the kioskTerminalId if it exists
                if (this.getTransactionModel().getPosKioskTerminalId() != null) {
                    this.getReceiptModel().setPosKioskTerminalId(this.getTransactionModel().getPosKioskTerminalId());
                }
            }
        }

        ReceiptConfigModel receiptConfigModel = new ReceiptConfigModel(this.getReceiptModel().getTerminalId());
        receiptConfigModel.populateTerminalReceiptConfig();

        //Check for configuration on the QCPOS Kiosk terminal
        if (this.getReceiptModel().getPosKioskTerminalId() != null) {
            ReceiptConfigModel kioskReceiptConfigModel = new ReceiptConfigModel(this.getReceiptModel().getPosKioskTerminalId());
            kioskReceiptConfigModel.populateTerminalReceiptConfig();
            receiptConfigModel.validateSettings(kioskReceiptConfigModel); //populate any settings that are missing from the receiptConfig
        }

        this.setTenderReceiptDetails();
        this.setReceiptConfigModel(receiptConfigModel);
        this.logReceiptConfigSettings();

    }

    public void validateTransactionModel() throws Exception {
        TerminalModel terminalModel = this.getTransactionModel().getTerminal();
        if (terminalModel.getLoginModel() == null){
            terminalModel.setLoginModel(new LoginModel());
        }

        TransactionBuilder transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, this.getTransactionModel(), PosAPIHelper.ApiActionType.SALE, PosAPIHelper.TransactionType.SALE);

        this.getTransactionModel().setIsInquiry(true);
        this.getTransactionModel().setWasRecordedOffline(true);
        if (this.getTransactionModel().getLoyaltyAccount() != null) {
            this.getTransactionModel().getLoyaltyAccount().setHasFetchedAvailableRewards(true);
            this.getTransactionModel().setHasValidatedLoyaltyAccount(true);
        }
        transactionBuilder.buildTransactionReceipt();
    }

    private void setTenderReceiptDetails() throws Exception {
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()){
            if (tenderLineItemModel.getTender() != null){
                if (!this.getTenderLinesHM().containsKey(tenderLineItemModel.getTender().getId())){
                    //Fetch the TenderModel locally, to make sure all the data fields are filled into the model
                    TenderModel tenderModel = TenderModel.getTenderModel(tenderLineItemModel.getTender().getId(), this.getReceiptModel().getTerminalId());
                    TenderDisplayModel tenderDisplayModel = TenderDisplayModel.createTenderModel(tenderModel);
                    tenderLineItemModel.setTender(tenderDisplayModel);
                    this.getTenderLinesHM().put(tenderLineItemModel.getId(), tenderLineItemModel);
                } else {
                    //skip
                }
            }
        }
    }

    private void logReceiptConfigSettings() {
        Logger.LEVEL logLevel = Logger.LEVEL.TRACE;

        try {

            Logger.logMessage("*************Receipt Print Settings***************", logFileName, logLevel);
            Logger.logMessage("Terminal Id: " + (this.getReceiptModel().getTerminalId() == null ? "null" : this.getReceiptModel().getTerminalId().toString()), logFileName, logLevel);
            Logger.logMessage("Transaction Id: " + (this.getTransactionModel().getId() == null ? "null" : this.getTransactionModel().getId().toString()), logFileName, logLevel);
            Logger.logMessage("Print Receipt (Show Receipt Receipt Option): " + this.getReceiptConfigModel().isPrintReceipt(), logFileName, logLevel);

            if (this.getReceiptConfigModel().getNumCopiesToPrint() == 0) {
                Logger.logMessage("Receipt Printer Options:  No Auto Customer Receipt", logFileName, logLevel);
            } else if (this.getReceiptConfigModel().getNumCopiesToPrint() == 1) {
                Logger.logMessage("Receipt Printer Options:  Auto Customer Receipt", logFileName, logLevel);
            }

            Logger.logMessage("Print Order Number: " + this.getReceiptConfigModel().isPrintOrderNumber(), logFileName, logLevel);
            Logger.logMessage("Print NutritionInfo: " + this.getReceiptConfigModel().isPrintNutritionInfo(), logFileName, logLevel);

            Logger.logMessage("Print Credit Card Processor Receipt: " + this.getReceiptConfigModel().isPrintProcessorCustomerCopy(), logFileName, logLevel);
            Logger.logMessage("Payment Processor Id: " + this.getReceiptConfigModel().getPaymentProcessorId(), logFileName, logLevel);

            if (this.getTenderLinesHM().isEmpty()) {
                //No tender on transaction
                //Print Customer Receipt (QCPOS or KOA)
                Logger.logMessage("Zero Tender lines, Customer receipt will be printed.", logFileName, logLevel);
            } else {

                Logger.logMessage("Tender Info:", logFileName, logLevel);
                Integer tenderCount = 1;
                //For each tender, check to see if receipts need to be printed
                for (Map.Entry<Integer, TenderLineItemModel> tenderTransLinesHM : this.getTenderLinesHM().entrySet()) {
                    TenderLineItemModel tenderLineItemModel = tenderTransLinesHM.getValue();

                    Logger.logMessage(" Tender "+ tenderCount.toString() + " name: " + tenderLineItemModel.getTender().getName(), logFileName, logLevel);
                    Logger.logMessage(" Tender Type Id: " + tenderLineItemModel.getTender().getTenderTypeId(), logFileName, logLevel);
                    Logger.logMessage(" Print Signature Receipt (Print Processor Merchant Copy): " + tenderLineItemModel.getTender().isPrintSignatureReceipt(), logFileName, logLevel);
                    Logger.logMessage(" Tender meets Min Signature Amount: " + tenderLineItemModel.doesMeetMinSignatureAmount(), logFileName, logLevel);
                    tenderCount++;
                }
            }
            Logger.logMessage("*************End Receipt Print Settings***********", logFileName, logLevel);
        } catch (Exception ex) {
            Logger.logMessage("TransactionReceiptMode.logReceiptConfigSettings(): Error occurred trying to log receipt printing configuration information. ", logFileName, Logger.LEVEL.ERROR);
        }
    }

    public TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    public ReceiptModel getReceiptModel() {
        return receiptModel;
    }

    public void setReceiptModel(ReceiptModel receiptModel) {
        this.receiptModel = receiptModel;
    }

    public ReceiptConfigModel getReceiptConfigModel() {
        return receiptConfigModel;
    }

    public void setReceiptConfigModel(ReceiptConfigModel receiptConfigModel) {
        this.receiptConfigModel = receiptConfigModel;
    }

    public HashMap<Integer, TenderLineItemModel> getTenderLinesHM() {
        return tenderLinesHM;
    }

    public void setTenderLinesHM(HashMap<Integer, TenderLineItemModel> tenderLinesHM) {
        this.tenderLinesHM = tenderLinesHM;
    }

}
