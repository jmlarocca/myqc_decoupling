package com.mmhayes.common.receiptGen.Models;

import com.mmhayes.common.dataaccess.DataManager;
import java.util.ArrayList;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-08 13:01:35 -0400 (Mon, 08 Jul 2019) $: Date of last commit
 $Rev: 9041 $: Revision of last commit
*/
public class ReceiptModel {
    private String contentType = ""; //PosAPIHelper.ReceiptContentType.CUSTOMERCOPY;
    private String outputType = ""; // PosAPIHelper.ReceiptOutputType.NONE;
    private String emailAddress = "";
    private boolean giftReceipt;
    private boolean merchantCopy;
    private ArrayList<CcProcessorReceiptDetail> ccProcessorReceiptDetails = new ArrayList();

    private Integer terminalId = null;
    private Integer posKioskTerminalId = null;

    private DataManager dm = new DataManager();

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isGiftReceipt() {
        return giftReceipt;
    }

    public void setGiftReceipt(boolean giftReceipt) {
        this.giftReceipt = giftReceipt;
    }

    public boolean isMerchantCopy() {
        return merchantCopy;
    }

    public void setMerchantCopy(boolean merchantCopy) {
        this.merchantCopy = merchantCopy;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public Integer getPosKioskTerminalId() {
        return posKioskTerminalId;
    }

    public void setPosKioskTerminalId(Integer posKioskTerminalId) {
        this.posKioskTerminalId = posKioskTerminalId;
    }

    public ArrayList<CcProcessorReceiptDetail> getCcProcessorReceiptDetails() {
        return ccProcessorReceiptDetails;
    }

    public void setCcProcessorReceiptDetails(ArrayList<CcProcessorReceiptDetail> ccProcessorReceiptDetails) {
        this.ccProcessorReceiptDetails = ccProcessorReceiptDetails;
    }
}
