package com.mmhayes.common.receiptGen.Models;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-08 13:01:35 -0400 (Mon, 08 Jul 2019) $: Date of last commit
 $Rev: 9041 $: Revision of last commit
*/
public class CcProcessorReceiptDetail {
    private Integer paTenderLineItemId = null;
    private String customerCopyReceiptText = "";
    private String merchantCopyReceiptText = "";
    private String creditCardTransInfo = "";

    public CcProcessorReceiptDetail(){

    }

    public Integer getPaTenderLineItemId() {
        return paTenderLineItemId;
    }

    public void setPaTenderLineItemId(Integer paTenderLineItemId) {
        this.paTenderLineItemId = paTenderLineItemId;
    }

    public String getCustomerCopyReceiptText() {
        return customerCopyReceiptText;
    }

    public void setCustomerCopyReceiptText(String customerCopyReceiptText) {
        this.customerCopyReceiptText = customerCopyReceiptText;
    }

    public String getMerchantCopyReceiptText() {
        return merchantCopyReceiptText;
    }

    public void setMerchantCopyReceiptText(String merchantCopyReceiptText) {
        this.merchantCopyReceiptText = merchantCopyReceiptText;
    }

    public String getCreditCardTransInfo() {
        return creditCardTransInfo;
    }

    public void setCreditCardTransInfo(String creditCardTransInfo) {
        this.creditCardTransInfo = creditCardTransInfo;
    }
}
