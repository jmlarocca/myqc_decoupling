package com.mmhayes.common.receiptGen;

import com.mmhayes.common.kms.receiptGen.KitchenReceiptBuilder;
import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.kms.receiptGen.transactionData.*;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionData;
import com.mmhayes.common.receiptGen.Formatter.CustomReceiptFormatter;
import com.mmhayes.common.receiptGen.Formatter.ICustomReceiptFormatter;
import com.mmhayes.common.receiptGen.Formatter.ICustomReportFormatter;
import com.mmhayes.common.receiptGen.Formatter.IPaymentProcessorRcptFormatter;
import com.mmhayes.common.receiptGen.Models.TransactionReceiptModel;
import com.mmhayes.common.receiptGen.ReceiptData.*;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.Renderer.BasicHTMLRenderer;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.Renderer.PrinterRenderer;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import jpos.JposException;
import jpos.POSPrinter;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

/*
 $Author: jkflanagan $: Author of last commit
 $Date: 2021-03-25 13:22:50 -0400 (Thu, 25 Mar 2021) $: Date of last commit
 $Rev: 13717 $: Revision of last commit
 Notes: Orchestrates generating receipts
*/


public class ReceiptGen
{
    private DataManager dm;
    private ReceiptData receiptData;
    private IReportData reportData;
    private ICustomReceiptFormatter rcptFormatter;
    private ICustomReportFormatter reportFormatter;
    private IPaymentProcessorRcptFormatter paymentProcessorRcptFormatter;
    private IPaymentProcessorReceiptData paymentProcessorRcptData;
    private static String logFileName = "ConsolidatedReceipt.log";

    public ReceiptGen()
    {
        MMHProperties mmhProperties = new MMHProperties();
        mmhProperties.APP_PROP_PATH = System.getProperty("user.dir");
        mmhProperties.PROP_FILES_PATH = System.getProperty("user.dir");
        mmhProperties.APP_SETTINGS_FILE = "app.properties";

        dm = new DataManager();
    }

    //region Getters / Setters
    public void setReceiptFormatter(ICustomReceiptFormatter i)
    {
        rcptFormatter = i;
    }

    public ICustomReceiptFormatter getReceiptFormatter()
    {
        return rcptFormatter;
    }

    public ReceiptData getReceiptData()
    {
        return receiptData;
    }

    public void setReceiptData(ReceiptData rd)
    {
        this.receiptData = rd;
    }

    public IReportData getReportData()
    {
        return reportData;
    }

    public void setReportData(IReportData reportData)
    {
        this.reportData = reportData;
    }

    public ICustomReportFormatter getReportFormatter()
    {
        return reportFormatter;
    }

    public void setReportFormatter(ICustomReportFormatter reportFormatter)
    {
        this.reportFormatter = reportFormatter;
    }

    public IPaymentProcessorReceiptData getPaymentProcessorRcptData() {
        return paymentProcessorRcptData;
    }

    public void setPaymentProcessorRcptData(IPaymentProcessorReceiptData paymentProcessorRcptData) {
        this.paymentProcessorRcptData = paymentProcessorRcptData;
    }

    public IPaymentProcessorRcptFormatter getPaymentProcessorRcptFormatter()
    {
        return paymentProcessorRcptFormatter;
    }

    public void setPaymentProcessorRcptFormatter(IPaymentProcessorRcptFormatter paymentProcessorRcptFormatter)
    {
        this.paymentProcessorRcptFormatter = paymentProcessorRcptFormatter;
    }

    public static String getLogFileName()
    {
        return logFileName;
    }
    //endregion

    //region Creating Renderers
    public IRenderer createBasicHTMLRenderer()
    {
        return new BasicHTMLRenderer();
    }

    public IRenderer createPrinterRenderer(POSPrinter ptr, String logicalPrinterName)
    {
        boolean bCreatedPrinter = false;

        try
        {
            // If ptr is null, try to create the printer
            if (ptr == null)
            {
                // Can't create the printer without a logicalPrinterName
                if (logicalPrinterName == null || logicalPrinterName.isEmpty())
                {
                    Logger.logMessage("ReceiptGen.createPrinterRenderer ERROR - ptr is null and no logicalPrinterName was provided!", getLogFileName(), Logger.LEVEL.ERROR);
                    return null;
                }

                // Try to create and connect to the printer
                try
                {
                    ptr = new POSPrinter();
                    ptr.open(logicalPrinterName);
                    ptr.claim(0);
                    ptr.setDeviceEnabled(true);
                    ptr.printNormal(2, " \n");
                    bCreatedPrinter = true;
                }
                catch (JposException jposEx)
                {
                    ptr = null;
                    logError("Printer '" + logicalPrinterName + "' exception; " + jposEx.getMessage(), jposEx);
                }
                catch (Exception e)
                {
                    ptr = null;
                    logError("Printer '" + logicalPrinterName + "' exception; " + e.getMessage(), e);
                }

                if (ptr == null)
                {
                    return null;
                }
            }
        }
        catch (Exception ex)
        {
            logError("ReceiptGen.createPrinterRenderer exception for logicalPrinterName " + logicalPrinterName, ex);
        }

        return new PrinterRenderer(ptr, logicalPrinterName, bCreatedPrinter);
    }
    //endregion

    //region Creating Data Containers
    public ReceiptData createReceiptData()
    {
        return new ReceiptData();
    }
    //endregion

    //region Creating Formatters
    public ICustomReceiptFormatter createDefaultReceiptFormatter()
    {
        return new CustomReceiptFormatter();
    }
    //endregion

    //region Logging
    public static void logError(String error, Exception e)
    {
        StringBuilder b = new StringBuilder(Long.toString(Thread.currentThread().getId()));
        b.append(" E ");
        b.append(error);
        Logger.logException(b.toString(), logFileName, e);
    }
    //endregion

    /**
     * <p>Generates a receipt for KP/KDS/KMS for the given transactionID.</p>
     *
     * @param paTransactionID ID of the transaction to generate a receipt for.
     * @param receiptTypeID The type of receipt to generate.
     * @param renderer The {@link IRenderer} to use to generate the receipt.
     * @return Whether or not a receipt could be generated successfully for the given transaction ID.
     */
    public boolean buildFromDBOrderNum (int paTransactionID, int receiptTypeID, IRenderer renderer) {
        boolean result = false;

        try {
            // make sure we have a valid IRenderer
            if (renderer == null) {
                Logger.logMessage("No valid renderer found in ReceiptGen.buildFromDBOrderNum", logFileName, Logger.LEVEL.ERROR);
                return false;
            }

            // make sure we have a valid DataManager
            if (dm == null) {
                Logger.logMessage("No valid data manager found in ReceiptGen.buildFromDBOrderNum", logFileName, Logger.LEVEL.ERROR);
                return false;
            }

            // make sure we have a valid receipt formatter
            if (getReceiptFormatter() == null) {
                Logger.logMessage("No valid receipt formatter has been found attempting to use the default in ReceiptGen.buildFromDBOrderNum", logFileName, Logger.LEVEL.TRACE);
                setReceiptFormatter(createDefaultReceiptFormatter());
            }
            // check again if we have a valid receipt formatter
            if (getReceiptFormatter() == null) {
                Logger.logMessage("No valid receipt formatter has been found and no default receipt formatter could be created in ReceiptGen.buildFromDBOrderNum", logFileName, Logger.LEVEL.ERROR);
                return false;
            }

            // make sure we have a ReceiptData object to store the generated receipt data
            if (getReceiptData() == null) {
                Logger.logMessage("No valid receipt ReceiptData has been found to store the generated receipt information in ReceiptGen.buildFromDBOrderNum", logFileName, Logger.LEVEL.ERROR);
                return false;
            }

            // set the receipt renderer
            rcptFormatter.setRenderer(renderer);

            // get a handle to the ReceiptGen's ReceiptData object
            if(receiptTypeID == TypeData.ReceiptType.KITCHENPTRRECEIPT){
                ReceiptData receiptData = getReceiptData();
                receiptData.setReceiptTypeID(receiptTypeID);
                receiptData.setDataManager(dm);

                Logger.logMessage("*** CALLING MOI ***", logFileName, Logger.LEVEL.IMPORTANT);
                TransactionData transactionData = com.mmhayes.common.kms.receiptGen.transactionData.TransactionData.buildFromDB(dm, paTransactionID);
                receiptData.setTransactionData(transactionData);
                receiptData.setReceiptDetails(KitchenReceiptBuilder.buildReceiptDetails(dm, renderer, transactionData, logFileName));
                this.receiptData = receiptData;
            }else{
                getReceiptData().setReceiptTypeID(receiptTypeID);
                ReceiptData receiptData = null;
                try {
                    receiptData = (ReceiptData) getReceiptData();
                } catch (Exception e) {
                    logError("ReceiptData class needs to be derived from DBReceiptData", e);
                    return false;
                }

                receiptData.setDataManager(dm);
                // Get the transaction details from the database and build an arraylist of what we need to print
                receiptData.buildTransaction(paTransactionID);
                // Log receipt type
                Logger.logMessage("RECEIPT TYPE IS " + receiptTypeID + " for PATransactionID " + paTransactionID, logFileName, Logger.LEVEL.DEBUG);
                // Build receipt
                if (receiptData.getTxnData() != null) {
                    rcptFormatter.buildReceipt(getReceiptData());
                }
            }

            return true;
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to generate a receipt of type %s for the transaction with an ID of %s in ReceiptGen.buildFromDBOrderNum",
                    Objects.toString(receiptTypeID, "N/A"),
                    Objects.toString(paTransactionID, "N/A")), logFileName, Logger.LEVEL.ERROR);
        }

        return false;
    }

    /**
     * <p>Generates a receipt for KP/KDS/KMS for the given transactionID.</p>
     *
     * @param paTransactionID ID of the transaction to generate a receipt for.
     * @param receiptTypeID The type of receipt to generate.
     * @param renderer The {@link IRenderer} to use to generate the receipt.
     * @return Whether or not a receipt could be generated successfully for the given transaction ID.
     */
    public boolean buildFromServerData (int paTransactionID, int receiptTypeID, IRenderer renderer, ArrayList serverData) {
        boolean result = false;

        try {
            // make sure we have a valid IRenderer
            if (renderer == null) {
                Logger.logMessage("No valid renderer found in ReceiptGen.buildFromDBOrderNum", logFileName, Logger.LEVEL.ERROR);
                return false;
            }

            // make sure we have a valid DataManager
            if (dm == null) {
                Logger.logMessage("No valid data manager found in ReceiptGen.buildFromDBOrderNum", logFileName, Logger.LEVEL.ERROR);
                return false;
            }

            // make sure we have a valid receipt formatter
            if (getReceiptFormatter() == null) {
                Logger.logMessage("No valid receipt formatter has been found attempting to use the default in ReceiptGen.buildFromDBOrderNum", logFileName, Logger.LEVEL.TRACE);
                setReceiptFormatter(createDefaultReceiptFormatter());
            }
            // check again if we have a valid receipt formatter
            if (getReceiptFormatter() == null) {
                Logger.logMessage("No valid receipt formatter has been found and no default receipt formatter could be created in ReceiptGen.buildFromDBOrderNum", logFileName, Logger.LEVEL.ERROR);
                return false;
            }

            // make sure we have a ReceiptData object to store the generated receipt data
            if (getReceiptData() == null) {
                Logger.logMessage("No valid receipt ReceiptData has been found to store the generated receipt information in ReceiptGen.buildFromDBOrderNum", logFileName, Logger.LEVEL.ERROR);
                return false;
            }

            // set the receipt renderer
            rcptFormatter.setRenderer(renderer);

            // get a handle to the ReceiptGen's ReceiptData object
            ReceiptData receiptData = getReceiptData();
            receiptData.setReceiptTypeID(receiptTypeID);
            receiptData.setDataManager(dm);

            TransactionData transactionData = com.mmhayes.common.kms.receiptGen.transactionData.TransactionData.buildFromServerData(dm, paTransactionID, serverData);
            receiptData.setTransactionData(transactionData);
            receiptData.setReceiptDetails(KitchenReceiptBuilder.buildReceiptDetails(dm, renderer, transactionData, logFileName));
            this.receiptData = receiptData;
            return true;
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to generate a receipt of type %s for the transaction with an ID of %s in ReceiptGen.buildFromDBOrderNum",
                    Objects.toString(receiptTypeID, "N/A"),
                    Objects.toString(paTransactionID, "N/A")), logFileName, Logger.LEVEL.ERROR);
        }

        return false;
    }

    // Given receipt data, generate a receipt
    // Used for actual printed receipts and email receipts
    public boolean buildFromArrayList(ArrayList receiptDataFromFlex, ArrayList nutritionData, ArrayList loyaltySummaryData, boolean printOrderNumber, String suspTransNameLbl, String suspTransName, int receiptTypeID, IRenderer r)
    {
        // We require a renderer
        if(r == null)
        {
            Logger.logMessage("You need to supply a renderer. examples createPrinterRenderer or createBasicHTMLRenderer", logFileName, Logger.LEVEL.ERROR);
            return false;
        }

        if(getReceiptData() == null)
        {
            Logger.logMessage("You did not supply a container for receipt data, call setReceiptData(createXMLRpcReceiptDataContainer()) first", logFileName, Logger.LEVEL.ERROR);
            return false;
        }

        if(getReceiptFormatter() == null)
        {
            setReceiptFormatter(createDefaultReceiptFormatter());
            Logger.logMessage("CustomReceiptFormatter is null, using default", logFileName, Logger.LEVEL.DEBUG);
        }

        if(getReceiptFormatter() == null)
        {
            Logger.logMessage("CustomReceiptFormatter is null, Unable to Continue", logFileName, Logger.LEVEL.ERROR);
            return false;
        }

        // Set renderer
        getReceiptFormatter().setRenderer(r);

        // Set receipt type
        receiptData.setReceiptTypeID(receiptTypeID);

        // Set data manager
        receiptData.setDataManager(dm);

        // Using the receipt details from Flex, set receiptData to store what we need to print
        receiptData.buildTransaction(receiptDataFromFlex, nutritionData, loyaltySummaryData, suspTransNameLbl, suspTransName);

        // Build receipt
        if (receiptData.getTxnData() != null)
        {
            rcptFormatter.buildReceipt(getReceiptData());
        }

        return true;
    }

    // Given TransactionReceiptModel, generate a receipt
    // Used for actual printed receipts and email receipts
    public boolean buildFromTransactionModel(TransactionReceiptModel transactionReceiptModel, IRenderer r) {

        // We require a renderer
        if (r == null) {
            Logger.logMessage("You need to supply a renderer. examples createPrinterRenderer or createBasicHTMLRenderer", logFileName, Logger.LEVEL.ERROR);
            return false;
        }

        if (getReceiptData() == null) {
            Logger.logMessage("You did not supply a container for receipt data, call setReceiptData(createXMLRpcReceiptDataContainer()) first", logFileName, Logger.LEVEL.ERROR);
            return false;
        }

        if (getReceiptFormatter() == null) {
            setReceiptFormatter(createDefaultReceiptFormatter());
            Logger.logMessage("CustomReceiptFormatter is null, using default", logFileName, Logger.LEVEL.DEBUG);
        }

        if (getReceiptFormatter() == null) {
            Logger.logMessage("CustomReceiptFormatter is null, Unable to Continue", logFileName, Logger.LEVEL.ERROR);
            return false;
        }

        // Set renderer
        getReceiptFormatter().setRenderer(r);

        // Set receipt type
        receiptData.setReceiptTypeID(transactionReceiptModel.getReceiptConfigModel().getReceiptTypeId());

        // Set data manager
        receiptData.setDataManager(dm);

        // Using the receipt details from the TransactionModel, set receiptData to store what we need to print
        receiptData.buildTransaction(transactionReceiptModel);

        // Build receipt
        if (receiptData.getTxnData() != null) {
            rcptFormatter.buildReceipt(getReceiptData());
        }

        return true;
    }

    /**
     * <p>Builds a receipt (printed or email) from the given {@link CustomerReceiptData}.</p>
     *
     * @param customerReceiptData The {@link CustomerReceiptData} to use to build the receipt.
     * @param receiptTypeID The type of receipt being built.
     * @param renderer The {@link IRenderer} to use to render the receipt.
     * @return Whether or not the receipt could be build successfully.
     */
    public boolean buildFromCustomerReceiptData (CustomerReceiptData customerReceiptData, int receiptTypeID, IRenderer renderer) {

        // make sure the customer receipt data is valid
        if (customerReceiptData == null) {
            Logger.logMessage("The CustomerReceiptData passed to ReceiptGen.buildFromCustomerReceiptData can't be null!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the receipt type is valid
        if (receiptTypeID <= 0) {
            Logger.logMessage("The receipt type ID passed to ReceiptGen.buildFromCustomerReceiptData must be greater than 0!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the renderer is valid
        if (renderer == null) {
            Logger.logMessage("The renderer passed to ReceiptGen.buildFromCustomerReceiptData can't be null!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the ReceiptData is valid
        if (receiptData == null) {
            Logger.logMessage("No container for receipt data has been supplied in ReceiptGen.buildFromCustomerReceiptData, unable to " +
                    "continue, now returning false.", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure there is a valid receipt formatter
        if (rcptFormatter == null) {
            Logger.logMessage("The receipt formatter in ReceiptGen.buildFromCustomerReceiptData is null, now attempting to use the default formatter.", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.TRACE);
            rcptFormatter = createDefaultReceiptFormatter();
        }

        if (rcptFormatter == null) {
            Logger.logMessage("Unable to use the default receipt formatter in ReceiptGen.buildFromCustomerReceiptData, unable to " +
                    "continue, now returning false.", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
        }

        // set the renderer
        rcptFormatter.setRenderer(renderer);

        // set the receipt type
        receiptData.setReceiptTypeID(receiptTypeID);

        // set the data manager
        receiptData.setDataManager(dm);

        // set the customer receipt data
        receiptData.setCustomerReceiptData(customerReceiptData);

        // build the receipt
        return rcptFormatter.buildReceipt(receiptData);
    }

    //endregion

    //region Building Reports
    public boolean buildReport(IRenderer renderer)
    {
        if (renderer == null)
        {
            Logger.logMessage("You need to supply a renderer.", logFileName, Logger.LEVEL.ERROR);
            return false;
        }

        if (getReportFormatter() == null)
        {
            Logger.logMessage("ReportFormatter is null, cannot continue!", logFileName, Logger.LEVEL.TRACE);
            return false;
        }

        if (getReportData() == null)
        {
            Logger.logMessage("ReportData is null, cannot continue!", logFileName, Logger.LEVEL.TRACE);
            return false;
        }

        reportFormatter.setRenderer(renderer);

        return reportFormatter.buildReport(reportData);
    }
    //endregion

    //region Building Payment Processor Receipts
    public boolean buildPaymentProcessorRcpt(IRenderer renderer)
    {
        if (renderer == null)
        {
            Logger.logMessage("You need to supply a renderer.", logFileName, Logger.LEVEL.ERROR);
            return false;
        }

        if (getPaymentProcessorRcptFormatter() == null)
        {
            Logger.logMessage("PaymentProcessorReceiptFormatter is null, cannot continue!", logFileName, Logger.LEVEL.TRACE);
            return false;
        }

        if (getPaymentProcessorRcptData() == null)
        {
            Logger.logMessage("ReportFormatter is null, cannot continue!", logFileName, Logger.LEVEL.TRACE);
            return false;
        }

        paymentProcessorRcptFormatter.setRenderer(renderer);

        return paymentProcessorRcptFormatter.buildReceipt(paymentProcessorRcptData);
    }

    //region For Popup Receipts
    public static String buildKey(String input)
    {
        String key = input;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyDDD");
        key += sdf.format(new Date());

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(input.getBytes());
            byte[] mdbytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for(int i=0; i<mdbytes.length; i++)
            {
                sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            key = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            logError("ReceiptGen.buildKey() exception " + e.getMessage(), e);
        }

        return key;
    }
    //endregion
}
