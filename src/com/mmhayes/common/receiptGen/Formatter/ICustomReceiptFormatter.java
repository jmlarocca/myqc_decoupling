package com.mmhayes.common.receiptGen.Formatter;

import com.mmhayes.common.receiptGen.ReceiptData.ReceiptData;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;

/*
 $Author: nyu $: Author of last commit
 $Date: 2019-11-06 09:49:17 -0500 (Wed, 06 Nov 2019) $: Date of last commit
 $Rev: 9864 $: Revision of last commit
 Notes: ICustomReceiptFormatter - interface all customReceiptFormatters must implement
*/

public interface ICustomReceiptFormatter
{
    void setRenderer(IRenderer i);

    IRenderer getRenderer();

    void logError(String error, Exception e);

    boolean buildReceipt(ReceiptData rd);
}
