package com.mmhayes.common.receiptGen.Formatter;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-10-01 09:40:30 -0400 (Thu, 01 Oct 2020) $: Date of last commit
    $Rev: 12775 $: Revision of last commit
    Notes: Contains common receipt formatting methods.
*/

import com.mmhayes.common.receiptGen.ReceiptData.ReceiptData;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.Objects;

public abstract class CommonFormatter implements ICustomReceiptFormatter {

    /**
     * <p>Adds to a receipt.</p>
     *
     * @param renderer The {@link IRenderer} to use to render the receipt.
     * @param receiptContentType The {@link String} type of receipt content to add to the receipt.
     * @param log The file path {@link String} of the file to log any information related to this method to.
     */
    void addToReceipt (IRenderer renderer, String receiptContentType, String log) {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to CommonFormatter.addToReceipt can't be null or empty, unable to continue.", Logger.LEVEL.ERROR);
            return;
        }

        // make sure the renderer is valid
        if (renderer == null) {
            Logger.logMessage("The renderer passed to CommonFormatter.addToReceipt can't be null, unable to continue.", log, Logger.LEVEL.ERROR);
            return;
        }

        // make sure the receipt content type is valid
        if (!StringFunctions.stringHasContent(receiptContentType)) {
            Logger.logMessage("The receipt content type passed to CommonFormatter.addToReceipt can't be null, unable to continue.", log, Logger.LEVEL.ERROR);
            return;
        }
        String[] validReceiptContentTypes = new String[]{TypeData.ReceiptContentType.BLANK_LINE, TypeData.ReceiptContentType.CUT_PAPER};
        if (!StringFunctions.rkMultiContains(validReceiptContentTypes, receiptContentType, 37)) {
            Logger.logMessage(String.format("The receipt content type of %s which was passed passed to CommonFormatter.addToReceipt is invalid, " +
                    "valid receipt content types are BLANK_LINE and CUT_PAPER, unable to continue.",
                    String.format(receiptContentType, "N/A")), log, Logger.LEVEL.ERROR);
            return;
        }

        switch (receiptContentType) {
            case TypeData.ReceiptContentType.BLANK_LINE:
                if (!renderer.addBlankLine()) {
                    Logger.logMessage("Encountered an error while attempting to add a blank line to the receipt in CommonFormatter.addToReceipt!", log, Logger.LEVEL.ERROR);
                }
                break;
            case TypeData.ReceiptContentType.CUT_PAPER:
                if (!renderer.cutPaper()) {
                    Logger.logMessage("Encountered an error while attempting to cut the receipt paper in CommonFormatter.addToReceipt!", log, Logger.LEVEL.ERROR);
                }
                break;
        }
    }

    /**
     * <p>Adds content to a receipt.</p>
     *
     * @param renderer The {@link IRenderer} to use to render the receipt.
     * @param receiptContentType The {@link String} type of receipt content to add to the receipt.
     * @param content The {@link String} of content to add to the receipt.
     * @param log The file path {@link String} of the file to log any information related to this method to.
     */
    void addContentToReceipt (IRenderer renderer, String receiptContentType, String content, String log) {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to CommonFormatter.addContentToReceipt can't be null or empty, unable to continue.", Logger.LEVEL.ERROR);
            return;
        }

        // make sure the renderer is valid
        if (renderer == null) {
            Logger.logMessage("The renderer passed to CommonFormatter.addContentToReceipt can't be null, unable to continue.", log, Logger.LEVEL.ERROR);
            return;
        }

        // make sure the content is valid
        if (content == null) {
            Logger.logMessage("The content passed to CommonFormatter.addContentToReceipt can't be null, unable to continue.", log, Logger.LEVEL.ERROR);
            return;
        }

        // make sure the receipt content type is valid
        if (!StringFunctions.stringHasContent(receiptContentType)) {
            Logger.logMessage("The receipt content type passed to CommonFormatter.addContentToReceipt can't be null, unable to continue.", log, Logger.LEVEL.ERROR);
            return;
        }
        String[] validReceiptContentTypes = new String[]{
                TypeData.ReceiptContentType.LINE,
                TypeData.ReceiptContentType.CENTER_LINE,
                TypeData.ReceiptContentType.CENTER_LARGE_LINE,
                TypeData.ReceiptContentType.ONE_COL_LEFT};
        if (!StringFunctions.rkMultiContains(validReceiptContentTypes, receiptContentType, 37)) {
            Logger.logMessage(String.format("The receipt content type of %s which was passed passed to CommonFormatter.addContentToReceipt is invalid, " +
                    "valid receipt content types are LINE, CENTER_LINE, CENTER_LARGE_LINE, and ONE_COL_LEFT, unable to continue.",
                    String.format(receiptContentType, "N/A")), log, Logger.LEVEL.ERROR);
            return;
        }

        switch (receiptContentType) {
            case TypeData.ReceiptContentType.LINE:
                if (!renderer.addLine(content)) {
                    Logger.logMessage(String.format("Encountered an error while attempting to add a line with the content: %s, to the receipt in CommonFormatter.addContentToReceipt!",
                            Objects.toString(content, "N/A")), log, Logger.LEVEL.ERROR);
                }
                break;
            case TypeData.ReceiptContentType.CENTER_LINE:
                if (!renderer.centerLine(content)) {
                    Logger.logMessage(String.format("Encountered an error while attempting to add a center line with the content: %s, to the receipt in CommonFormatter.addContentToReceipt!",
                            Objects.toString(content, "N/A")), log, Logger.LEVEL.ERROR);
                }
                break;
            case TypeData.ReceiptContentType.CENTER_LARGE_LINE:
                if (!renderer.centerLine(renderer.large(content))) {
                    Logger.logMessage(String.format("Encountered an error while attempting to add a large center line with the content: %s, to the receipt in CommonFormatter.addContentToReceipt!",
                            Objects.toString(content, "N/A")), log, Logger.LEVEL.ERROR);
                }
                break;
            case TypeData.ReceiptContentType.ONE_COL_LEFT:
                if (!renderer.addOneColumnLineLeftAlign(content)) {
                    Logger.logMessage(String.format("Encountered an error while attempting to add a single left aligned column with the content: %s, to the receipt in CommonFormatter.addContentToReceipt!",
                            Objects.toString(content, "N/A")), log, Logger.LEVEL.ERROR);
                }
                break;
        }

    }

    /**
     * <p>Adds content to a receipt.</p>
     *
     * @param renderer The {@link IRenderer} to use to render the receipt.
     * @param receiptContentType The {@link String} type of receipt content to add to the receipt.
     * @param content The {@link String} of content to add to the receipt.
     * @param content2 Additional {@link String} of content to add to the receipt.
     * @param log The file path {@link String} of the file to log any information related to this method to.
     */
    void addMultiContentToReceipt (IRenderer renderer, String receiptContentType, String content, String content2, String log) {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to CommonFormatter.addMultiContentToReceipt can't be null or empty, unable to continue.", Logger.LEVEL.ERROR);
            return;
        }

        // make sure the renderer is valid
        if (renderer == null) {
            Logger.logMessage("The renderer passed to CommonFormatter.addMultiContentToReceipt can't be null, unable to continue.", log, Logger.LEVEL.ERROR);
            return;
        }

        // make sure the content is valid
        if (content == null) {
            Logger.logMessage("The content passed to CommonFormatter.addMultiContentToReceipt can't be null, unable to continue.", log, Logger.LEVEL.ERROR);
            return;
        }

        // make sure the additional content is valid
        if (content2 == null) {
            Logger.logMessage("The additional content passed to CommonFormatter.addMultiContentToReceipt can't be null, unable to continue.", log, Logger.LEVEL.ERROR);
            return;
        }

        // make sure the receipt content type is valid
        if (!StringFunctions.stringHasContent(receiptContentType)) {
            Logger.logMessage("The receipt content type passed to CommonFormatter.addMultiContentToReceipt can't be null, unable to continue.", log, Logger.LEVEL.ERROR);
            return;
        }
        String[] validReceiptContentTypes = new String[]{TypeData.ReceiptContentType.CENTER_LARGE_LINE, TypeData.ReceiptContentType.TWO_COL};
        if (!StringFunctions.rkMultiContains(validReceiptContentTypes, receiptContentType, 37)) {
            Logger.logMessage(String.format("The receipt content type of %s which was passed passed to CommonFormatter.addMultiContentToReceipt is invalid, " +
                    "valid receipt content types are CENTER_LARGE_LINE, and TWO_COL, unable to continue.",
                    String.format(receiptContentType, "N/A")), log, Logger.LEVEL.ERROR);
            return;
        }

        switch (receiptContentType) {
            case TypeData.ReceiptContentType.CENTER_LARGE_LINE:
                if (!renderer.centerLine(content + renderer.large(content2))) {
                    Logger.logMessage(String.format("Encountered an error while attempting to add a large center line with the content: %s, to the receipt in CommonFormatter.addMultiContentToReceipt!",
                            Objects.toString(content, "N/A")), log, Logger.LEVEL.ERROR);
                }
                break;
            case TypeData.ReceiptContentType.TWO_COL:
                if (!renderer.add2ColumnLine(content, content2)) {
                    Logger.logMessage(String.format("Encountered an error while attempting to add a two column line with the content: %s, to the receipt in CommonFormatter.addMultiContentToReceipt!",
                            Objects.toString(content, "N/A")), log, Logger.LEVEL.ERROR);
                }
                break;
        }
    }

    /**
     * <p>Adds a barcode to the receipt.</p>
     *
     * @param rcptRenderer The {@link IRenderer} to use to render the barcode on the receipt.
     * @param receiptData The {@link ReceiptData} which contains information necessary to build the barcode.
     * @param logFileName The file path {@link String} of the file to log any information related to this method to.
     */
    void doBarcode (IRenderer rcptRenderer, ReceiptData receiptData, String logFileName) {
        Logger.logMessage("Entering doBarcode()", logFileName, Logger.LEVEL.DEBUG);

        Logger.logMessage(String.format("getPrintTxnBarcode()=%s, receiptData.getTxnData().getTransactionID()=%s, render class=%s",
                (receiptData.getPrintTxnBarcode() ? "true" : "false"),
                (receiptData.getTxnData().getTransactionID() != null ? receiptData.getTxnData().getTransactionID() : "NULL"),
                rcptRenderer.getClass().getCanonicalName()),
                logFileName, Logger.LEVEL.DEBUG);

        if (receiptData.getPrintTxnBarcode() && receiptData.getTxnData().getTransactionID() != null) {
            Logger.logMessage("Adding Barcode", logFileName, Logger.LEVEL.DEBUG);
            rcptRenderer.addBlankLine();
            rcptRenderer.addBlankLine();
            String barCodeToPrint = ("QCPOS" + receiptData.getTxnData().getTransactionID());
            rcptRenderer.addBarcode(barCodeToPrint);
            rcptRenderer.addBlankLine();
        }
    }
}
