package com.mmhayes.common.receiptGen.Formatter;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.product.models.ProductModel;
import com.mmhayes.common.receiptGen.ReceiptData.*;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.Renderer.BasicHTMLRenderer;
import com.mmhayes.common.receiptGen.TransactionData.*;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHTimeFormatString;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/*
 $Author: rawithers $: Author of last commit
 $Date: 2021-09-28 18:30:50 -0400 (Tue, 28 Sep 2021) $: Date of last commit
 $Rev: 15471 $: Revision of last commit
 Notes: CustomReceiptFormatter - controls what to display and where to display it
*/
public class CustomReceiptFormatter extends CommonFormatter
{
    private static String logFileName = "ConsolidatedReceipt.log";
    private DecimalFormat df = new DecimalFormat("#,###,###,##0.00");
    private IRenderer rcptRenderer;
    private String currencyType = "$";
    private boolean currencyBeforePrice = true;
    private boolean decimalInPrice = true;
    private ArrayList<String> printedOrderNumbers = null;

    public CustomReceiptFormatter()
    {
        df.setRoundingMode(RoundingMode.HALF_UP);
    }

    @Override
    public void setRenderer(IRenderer i)
    {
        rcptRenderer = i;
    }
    @Override
    public IRenderer getRenderer()
    {
        return rcptRenderer;
    }
    @Override
    public void logError(String error, Exception e)
    {
        ReceiptGen.logError(error, e);
    }

    String getReceiptText()
    {
        return rcptRenderer.toString();
    }

    @Override
    public boolean buildReceipt(ReceiptData receiptData)
    {

        if ((receiptData != null) && (receiptData.getCustomerReceiptData() != null)) {
            return buildReceiptFromCustomerReceiptData(receiptData);
        }
        else {
            if(rcptRenderer == null)
            {
                return false;
            }

            if(receiptData == null)
            {
                return false;
            }

            if(!rcptRenderer.begin())
            {
                return false;
            }

            printedOrderNumbers = new ArrayList<>();

            currencyType = receiptData.getCurrencyType();
            currencyBeforePrice = receiptData.isCurrencyBeforePrice();
            decimalInPrice = receiptData.isDecimalInPrice();

            boolean totalHeaderPrinted = false;
            boolean subtotalPrinted = false;
            boolean isPrinterInErrorState = false;
            int itemCount = receiptData.getTxnData().getItemCount();

            if (receiptData.getTxnData().getTrainingModeTransTypeID() != 0)
            {
                printTrainingModeNote();
                rcptRenderer.addBlankLine();
            }

            // Print receipt header
            ArrayList receiptHeader = receiptData.getReceiptHeaders();
            if(receiptHeader != null)
            {
                for (Object aReceiptHeader : receiptHeader)
                {
                    printHeaderLine(aReceiptHeader.toString());
                }
            }

            // Print transaction header info
            printRcptIdInfo(receiptData);

            // Get transaction type ID
            int transactionTypeID = receiptData.getTxnData().getTransTypeID();

            // Get transaction details
            ArrayList transArgs = receiptData.getTxnData().getTransItems();

            String lastOrderNumPrinted = "";

            int currentComboId = 0;

            //HP 4825 - Move any combo items next to each other so they print together
            ArrayList comboSetIDs = new ArrayList<>();
            int comboSetCounter = 0;
            for (int i = 0; i < transArgs.size(); i++)
            {
                Object transItem = transArgs.get(i);

                if (transItem instanceof Plu)
                {
                    Plu product = (Plu) transItem;
                    boolean productInCombo = false;

                    //Check if the product is part of a combo
                    if (product.getPaComboTransLineItemID() > 0 || (product.getIsModifier() && product.getParentProduct().getPaComboTransLineItemID() > 0))
                    {
                        int prodComboID = 0;
                        if (product.getPaComboTransLineItemID() > 0)
                        {
                            prodComboID = product.getPaComboTransLineItemID();
                        }
                        else
                        {
                            prodComboID = product.getParentProduct().getPaComboTransLineItemID();
                        }

                        if (comboSetIDs != null)
                        {
                            boolean foundCombo = false;
                            //See if we've handled this combo yet
                            for (Object comboID : comboSetIDs)
                            {
                                int cID = (int) comboID;
                                if (prodComboID == cID)
                                {
                                    foundCombo = true;
                                    break;
                                }
                            }
                            //We only want to do this loop once for all items in this combo
                            if (!foundCombo)
                            {
                               //Add the combo set to the parsed list so it doesnt hit again
                                comboSetIDs.add(prodComboID);
                                //Reset the combo set counter
                                comboSetCounter = 0;
                                //Find the other products in the combo
                                for (int j = (i+1); j < transArgs.size(); j++)
                                {
                                    Object transItem2 = transArgs.get(j);

                                    if (transItem2 instanceof Plu)
                                    {
                                        Plu product2 = (Plu) transItem2;
                                        if (product2.getPaComboTransLineItemID() > 0 || (product2.getIsModifier() && product2.getParentProduct().getPaComboTransLineItemID() > 0))
                                        {
                                            int prodComboID2 = 0;
                                            if (product2.getPaComboTransLineItemID() > 0)
                                            {
                                                prodComboID2 = product2.getPaComboTransLineItemID();
                                            }
                                            else
                                            {
                                                prodComboID2 = product2.getParentProduct().getPaComboTransLineItemID();
                                            }
                                            //If it matches the combo we're looking for
                                            if (prodComboID2 == prodComboID)
                                            {
                                                //Inc the combo set counter
                                                comboSetCounter++;
                                                //Have it swap positions with whatever item is following the combo product we started with + the number of items we've added to it
                                                //Initial Combo Product - Object transItem = transArgs.get(i);
                                                //Current product - Object transItem2 = transArgs.get(j);
                                                int newPosition = (i+comboSetCounter);
                                                //Item in that position
                                                Object transItemSwap = transArgs.get(newPosition);
                                                int swappedPosition = j;
                                                //Put the one being moved into the incremental slot
                                                transArgs.set(newPosition, transItem2);
                                                //Put the one being removed into the moved product's slot
                                                transArgs.set(swappedPosition, transItemSwap);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


            // Loop through and add any missing modifiers to their parent products (POS sent receipts will not have them attached yet, DB created receipts already will)
            if (transArgs != null)
            {
                for (Object transItem : transArgs)
                {
                    if (transItem instanceof Plu)
                    {
                        Plu product = (Plu) transItem;
                        if (product.getIsModifier())
                        {
                            Plu parentPlu = product.getParentProduct();
                            //Add this modifier to its parent product's mods (POS receipt)
                            if (parentPlu!=null)
                            {
                                //Find this parent plu in the list
                                for (Object parentSearch : transArgs)
                                {
                                    if (parentSearch instanceof Plu)
                                    {
                                        Plu parent = (Plu) parentSearch;
                                        if (parentPlu.getPATransLineItemID()==parent.getPATransLineItemID())
                                        {
                                            //Matched the parent, make sure this mod is in its list
                                            boolean foundMod = false;
                                            for (Plu modifierPlu : parentPlu.getModifiers())
                                            {
                                                if (modifierPlu.getPATransLineItemModID()==product.getPATransLineItemModID())
                                                {
                                                    foundMod = true;
                                                    break;
                                                }
                                            }
                                            if (!foundMod)
                                            {
                                                //Add current product (modifier) to parent's modifier arraylist
                                                parentPlu.addModifier(product);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


            // Loop through and print each transaction detail
            if(transArgs != null)
            {
                for (Object transItem : transArgs)
                {
                    if (transItem instanceof Plu)
                    {
                        Plu product = (Plu) transItem;

                        boolean productInCombo = false;

                        // Check if the product is part of a combo
                        if(product.getPaComboTransLineItemID() > 0 || (product.getIsModifier() && product.getParentProduct().getPaComboTransLineItemID() > 0))
                        {
                            productInCombo = true;
                        }

                        boolean isFirstCombo = productInCombo && currentComboId == 0;
                        boolean isProductDifferentCombo = productInCombo && currentComboId != 0 && product.getPaComboTransLineItemID() > 0 && product.getPaComboTransLineItemID() != currentComboId;
                        boolean isModProductDifferentCombo = productInCombo && currentComboId != 0 && product.getIsModifier() && product.getParentProduct().getPaComboTransLineItemID() > 0 && product.getParentProduct().getPaComboTransLineItemID() != currentComboId;

                        // If this is the first combo OR this product is part of a different combo OR the modifier is part of a different combo
                        if( isFirstCombo || isProductDifferentCombo || isModProductDifferentCombo )
                        {
                            currentComboId = product.getPaComboTransLineItemID();

                            if(product.getIsModifier() && product.getParentProduct().getPaComboTransLineItemID() > 0) {
                                currentComboId = product.getParentProduct().getPaComboTransLineItemID();
                            }

                            // Find combo item for this product
                            for (Object transComboItem : transArgs)
                            {
                                if (transComboItem instanceof Combo)
                                {
                                    Combo combo = (Combo) transComboItem;

                                    // Print Combo and Combo Product line
                                    if(combo.getPATransLineItemID() == product.getPaComboTransLineItemID())
                                    {
                                        lastOrderNumPrinted = printComboItem(combo, transactionTypeID, receiptData, lastOrderNumPrinted);
                                        lastOrderNumPrinted = handleProductLine(product, receiptData, lastOrderNumPrinted, transactionTypeID, true);
                                        break;
                                    }
                                }
                            }
                        }

                        // If product is in combo and product is part of the current combo being built, print combo product line
                        else if(productInCombo && (currentComboId == product.getPaComboTransLineItemID() || (product.getIsModifier() && currentComboId == product.getParentProduct().getPaComboTransLineItemID())))
                        {
                            lastOrderNumPrinted = handleProductLine(product, receiptData, lastOrderNumPrinted, transactionTypeID, true);
                        }

                        // Otherwise print normal product line
                        else
                        {
                            lastOrderNumPrinted = handleProductLine(product, receiptData, lastOrderNumPrinted, transactionTypeID, false);
                        }
                    }
                    else if (transItem instanceof ReceivedOnAcct)
                    {
                        ReceivedOnAcct raItem = (ReceivedOnAcct) transItem;
                        printRAItem(raItem, transactionTypeID);
                    }
                    else if (transItem instanceof PaidOut)
                    {
                        PaidOut paidOutItem = (PaidOut) transItem;
                        printPaidOutItem(paidOutItem, transactionTypeID);
                    }
                    else if (transItem instanceof Donation)
                    {
                        Donation dntn = (Donation) transItem;
                        //check to make sure there is an actual donation
                        if(!dntn.getName().equals("") && dntn.getAmount()!=0){
                            printDonationItem(dntn, transactionTypeID);
                        }
                    }
                }
            }

        if (itemCount > 0)
        {
            // Loop through and print subtotal discounts
            ArrayList<Discount> subtotalDscts = receiptData.getTxnData().getOrderedSubtotalDscts();
            for (Discount subtotalDsct : subtotalDscts)
            {
                if (!subtotalPrinted)
                {
                    printSubtotal(receiptData.getTxnData().getSubtotal(), transactionTypeID);
                    subtotalPrinted = true;
                }

                printSubtotalDiscount(subtotalDsct.getName(), subtotalDsct.getTotalItemPrice(), transactionTypeID, subtotalDsct.getShortName());

            }

            // Loop through and print non free product rewards
            for (LoyaltyReward reward : receiptData.getTxnData().getRewards())
            {
                if (reward.isFreeProductReward() == false)
                {
                    if (!subtotalPrinted)
                    {
                        printSubtotal(receiptData.getTxnData().getSubtotal(), transactionTypeID);
                        subtotalPrinted = true;
                    }

                    printLoyaltyReward(reward, reward.getShortName(), transactionTypeID);
                }
            }

            // Loop through and print subtotal surcharges
            for (Surcharge surcharge : receiptData.getTxnData().getSubtotalSurcharges())
            {
                if (!subtotalPrinted)
                {
                    printSubtotal(receiptData.getTxnData().getSubtotal(), transactionTypeID);
                    subtotalPrinted = true;
                }

                if (surcharge.getApplicationTypeID() == TypeData.SurchargeApplicationType.SUBTOTAL)
                {
                    printSubtotalSurcharge(surcharge.getName(), surcharge.getTotalItemPrice(), transactionTypeID, surcharge.getShortName());
                }
            }

            // Loop through and print taxes
            HashMap<Integer,Tax> taxes = receiptData.getTxnData().getTaxes();
            for (Tax tax : taxes.values())
            {
                if (!subtotalPrinted)
                {
                    printSubtotal(receiptData.getTxnData().getSubtotal(), transactionTypeID);
                    subtotalPrinted = true;
                }

                printTax(tax.getName(), tax.getTotalItemPrice(), transactionTypeID, tax.getShortName());
            }

            // Print total amount
            if (!totalHeaderPrinted)
            {
                if (!subtotalPrinted && transactionTypeID != TypeData.TranType.PO && transactionTypeID != TypeData.TranType.RA)
                {
                    printSubtotal(receiptData.getTxnData().getSubtotal(), transactionTypeID);
                    subtotalPrinted = true;
                }

                printTotalHeader(receiptData.getTxnData().getTransactionTotal(), transactionTypeID, transactionTypeID);
                totalHeaderPrinted = true;
            }
        }

            // Loop through and print tenders
            ArrayList<Tender> tenders = receiptData.getTxnData().getTenders();
            for (Tender tndr : tenders)
            {
                // Print the tender
                if (tndr.getTotalItemPrice().doubleValue() < 0 && transactionTypeID != TypeData.TranType.VOID && transactionTypeID != TypeData.TranType.REFUND && transactionTypeID != TypeData.TranType.PO)
                {
                    continue;
                }

                printTenderItem(tndr.getTotalItemPrice(), tndr.getNameForReceipt(), transactionTypeID, tndr.getTenderTypeID(), receiptData.getReceiptTypeID());

                // Print credit card details, if there are any
                if (tndr.getTenderTypeID() == TypeData.TenderType.CREDITCARD)
                {
                    printCreditCardInfo(tndr, receiptData.getPaymentGatewayID());
                }

                // If it's a QC tender, see if we need to print acct info lines
                if ( (receiptData.getReceiptAcctInfoLines().size() > 0) && (tndr.getTenderTypeID() == TypeData.TenderType.QUICKCHARGE) && (tndr.getEmployeeData() != null) )
                {
                    printAcctInfo(receiptData.getReceiptAcctInfoLines(), tndr.getEmployeeData(), receiptData.getReceiptTypeID(), receiptData.getBadgeNumDigitsPrinted(), transactionTypeID);
                }

                printIntegratedGiftCardBalance(tndr);

                // Print tender comment
                if (tndr.getTenderComment().equals("") == false)
                {
                    printTenderComment(tndr.getCommentPromptText(), tndr.getTenderComment());
                }
            }

			// Print transaction comment if the terminal is **NOT** QC POS
			// For QC POS terminals, the transaction comment will get printed with the tender
			if ( (receiptData.getTxnData().getTerminalData() != null)
					&& (receiptData.getTxnData().getTerminalData().getTerminalModelID() != TypeData.TerminalModelType.QC_POS)
					&& (!receiptData.getTxnData().getTransComment().isEmpty()) ) {
				printTenderComment("Cmt", receiptData.getTxnData().getTransComment());
			}

            // Print change due
            if ( (transactionTypeID != TypeData.TranType.VOID)
                    && (transactionTypeID != TypeData.TranType.REFUND)
                    && (transactionTypeID < TypeData.TranType.ENDSHIFT || transactionTypeID == TypeData.TranType.RA)
                    && (!receiptData.getIsMerchantCopy()) && (transactionTypeID != TypeData.TranType.CANCEL) )
            {
                if (itemCount > 0)
                {
                    printTotalDetails(receiptData.getTxnData().getChangeAmount());
                }
            }

            // Print item count
            if ( (transactionTypeID != TypeData.TranType.CANCEL)  && (itemCount > 0) )
            {
                if ( (transactionTypeID == TypeData.TranType.OPEN) && (receiptData.getTxnData().getTenders().size() > 0) )
                {
                    rcptRenderer.addBlankLine();
                }
                printItemCount(itemCount, transactionTypeID, receiptData.getIsMerchantCopy());
            }

            // Print net & gross weight totals
            if (transactionTypeID != TypeData.TranType.CANCEL)
            {
                //Gross Weight
                if (receiptData.getPrintGrossWeight())
                {
                    //Only print the total if there is one
                    if (receiptData.getTxnData().getGrossWeightTotal().compareTo(BigDecimal.ZERO) > 0)
                    {
                        if ( (transactionTypeID == TypeData.TranType.OPEN) && (receiptData.getTxnData().getTenders().size() > 0) )
                        {
                            rcptRenderer.addBlankLine();
                        }
                        printGrossWeightTotals(receiptData.getTxnData().getGrossWeightTotal(), receiptData.getIsMerchantCopy());
                    }
                }

                //Net Weight
                if (receiptData.getPrintNetWeight())
                {
                    //Only print the total if there is one
                    if (receiptData.getTxnData().getNetWeightTotal().compareTo(BigDecimal.ZERO) > 0)
                    {
                        if ( (transactionTypeID == TypeData.TranType.OPEN) && (receiptData.getTxnData().getTenders().size() > 0) )
                        {
                            rcptRenderer.addBlankLine();
                        }
                        printNetWeightTotals(receiptData.getTxnData().getNetWeightTotal(), receiptData.getIsMerchantCopy());
                    }
                }
            }

            // Print signature line
            if (receiptData.getIsMerchantCopy())
            {
                printSignatureLine();
                if (receiptData.getPaymentGatewayID() == TypeData.PaymentGateway.SIMPLIFY_FUSEBOX)
                {
                    printCardholderAgreement(tenders.get(0));
                }
            }

            // Print receipt footers
            ArrayList receiptFooters = receiptData.getReceiptFooters();
            if(receiptFooters != null)
            {
                for (int x = 0; x < receiptFooters.size(); x++)
                {
                    printFooterLine(receiptFooters.get(x).toString(), 0 == x, receiptFooters.size() == x+1);
                }
            }

            // Print merchant or customer copy label
            if (receiptData.getIsMerchantCopy())
            {
                rcptRenderer.addBlankLine();
                printMerchantCopyLbl();
            }
            else
            {
                rcptRenderer.addBlankLine();
                printCustomerCopyLbl(receiptData);
            }

            // Print nutrition info
            if (receiptData.getNutritionData() != null && (receiptData.getTxnData().getTransTypeID() == TypeData.TranType.SALE || receiptData.getTxnData().getTransTypeID() == TypeData.TranType.OPEN) )
            {
                rcptRenderer.addBlankLine();
                printNutritionInfo(receiptData.getNutritionData());
            }

            // Print loyalty info
            if (receiptData.getLoyaltyData() != null && receiptData.getLoyaltyData().size() > 0)
            {
                rcptRenderer.addBlankLine();
                printLoyaltySummary(receiptData.getLoyaltyData(), receiptData.getTxnData().getLoyaltyAcct(), receiptData.getReceiptTypeID(), receiptData.getTxnData().getTransTypeID(), receiptData.getBadgeNumDigitsPrinted());
            }

            doBarcode(rcptRenderer, receiptData, logFileName);

            if (receiptData.getTxnData().getTrainingModeTransTypeID() != 0)
            {
                printTrainingModeNote();
            }

            finishPrint();
            rcptRenderer.end();

            return true;
        }

    }

    public String handleProductLine(Plu product, ReceiptData receiptData, String lastOrderNumPrinted, int transactionTypeID, boolean isComboProduct)
    {
        lastOrderNumPrinted = printOrigOrderNumDivider(product.getOriginalOrderNum(), receiptData.getOrderNumber(), receiptData.getReceiptTypeID(), lastOrderNumPrinted);

        if(isComboProduct)
        {
            printComboProductItem(product, receiptData.shouldPrintProductCodes(), receiptData);
        }
        else
        {
            printProductItem(product, transactionTypeID, receiptData.shouldPrintProductCodes(), receiptData);
        }

        if ( (product.getItemSurcharges().size() > 0) && (product.getParentProduct() == null) )
        {
            ArrayList<Surcharge> itemSurcharges = product.getItemSurcharges();

            for (Surcharge itemSurchg : itemSurcharges)
            {
                printItemSurcharge(itemSurchg, transactionTypeID, isComboProduct, receiptData);
            }
        }
        if (product.getItemDiscount() != null && !isComboProduct)
        {
            Discount itemDsct = product.getItemDiscount();
            printItemDiscount(itemDsct, new Double(product.getQuantity()).intValue(), transactionTypeID, -1);
        }

        if ( (product.getFreeProductRwds().size() > 0) && (product.getParentProduct() == null) )
        {
            ArrayList<LoyaltyReward> arrFreeProdRwds = product.getFreeProductRwds();

            for (LoyaltyReward freeProdRwd : arrFreeProdRwds)
            {
                printFreeProdReward(freeProdRwd, freeProdRwd.getShortName(), transactionTypeID, isComboProduct, receiptData);
            }
        }

        return lastOrderNumPrinted;
    }

    //--------------------------------------------------------------------------
    //  Printing Transaction Header Methods
    //--------------------------------------------------------------------------
    public boolean printHeaderLine(String headerLine)
    {
        rcptRenderer.centerLine(headerLine);

        return true;
    }

    public boolean printRcptIdInfo(ReceiptData receiptData)
    {
        String terminalID = Integer.toString(receiptData.getTxnData().getTerminalID());
        String transTypeName  = TypeData.getTransactionTypeName(receiptData.getTxnData().getTransTypeID());
        int transTypeID = receiptData.getTxnData().getTransTypeID();
        int transStatusID = receiptData.getTxnData().getTransactionStatus();
        int orderTypeID = receiptData.getTxnData().getOrderTypeID();
        String newTransID = receiptData.getTxnData().getTransactionID();
        String origTransID = receiptData.getTxnData().getOrigTransactionID();
        String cashierName = receiptData.getTxnData().getCashierName();
        String date = receiptData.getTxnData().getTransDate();
        String time = receiptData.getTxnData().getTransTime();
        String dateNoYear = receiptData.getTxnData().getTransDateNoYear();
        String timeNoSeconds = receiptData.getTxnData().getTransTimeNoSeconds();
        Boolean printOrigTxnNum = false;
        String suspTxnNameLabel = receiptData.getTxnData().getSuspTxnNameLabel();
        String suspTxnName = receiptData.getTxnData().getSuspTxnName();
        boolean useOpenTxnRcptRules = false;

        if ( (transTypeName.toUpperCase() == "REFUND") || (transTypeName.toUpperCase() == "VOID") )
        {
            printOrigTxnNum = true;
        }

        if ( (transTypeID == TypeData.TranType.OPEN) || (transTypeID == TypeData.TranType.CANCEL && transStatusID == TypeData.TransactionStatus.EXPIRED) || (transTypeID == TypeData.TranType.SALE) )
        {
            useOpenTxnRcptRules = true;
        }

        rcptRenderer.addBlankLine();

        // If transaction type is OPEN, print SUSPENDED at the top
        if (transTypeID == TypeData.TranType.OPEN)
        {
            rcptRenderer.centerLine(rcptRenderer.large("****  OPEN  ****"));
            rcptRenderer.addBlankLine();
        }
        else if ( (transTypeID == TypeData.TranType.CANCEL) && (transStatusID == TypeData.TransactionStatus.EXPIRED) )
        {
            rcptRenderer.centerLine(rcptRenderer.large("****  EXPIRED  ****"));
            rcptRenderer.addBlankLine();
        }

        // Print order numbers only for Sale, Open, and Cancel transactions with an EXPIRED status
        String orderNumber = receiptData.getOrderNumber();
        if( (receiptData.getPrintOrderNumber()) && (transTypeID == TypeData.TranType.SALE || useOpenTxnRcptRules) && (orderNumber.equals("") == false))
        {
            rcptRenderer.centerLine("Order Number: " + rcptRenderer.large(orderNumber));
            rcptRenderer.addBlankLine();
        }

        if ((transTypeID == TypeData.TranType.SALE || transTypeID == TypeData.TranType.OPEN) && receiptData.getPrintOrderTypeAndTimeMsg()) {
            if (orderTypeID > 0 && orderTypeID != TypeData.OrderType.NORMAL_SALE) {
                String pickupOrDeliveryText = "";

                switch (orderTypeID){
                    case TypeData.OrderType.PICKUP:
                        pickupOrDeliveryText = "Pickup on";
                        break;
                    case TypeData.OrderType.DELIVERY:
                        pickupOrDeliveryText = "Delivery on";
                        break;
                }

                String message = pickupOrDeliveryText + " " + dateNoYear + " at " + timeNoSeconds;
                rcptRenderer.centerLine(rcptRenderer.large(message));
                rcptRenderer.addBlankLine();
            }
        }

        // If transaction type is OPEN/CANCEL&EXPIRED and we have a transaction name, print that
        if ( (useOpenTxnRcptRules) && (suspTxnName != null) )
        {
            if (!suspTxnName.equals(""))
            {
                String printLine = suspTxnNameLabel + ": " + suspTxnName;
                if (suspTxnNameLabel.equals(""))
                {
                    printLine = suspTxnName;
                }
                rcptRenderer.addOneColumnLineLeftAlign(printLine);
                rcptRenderer.addBlankLine();
            }
        }

        rcptRenderer.add2ColumnLine(date, time);
        rcptRenderer.add2ColumnLine("Cashier: " + cashierName, "TID: " + terminalID);

        if (origTransID.compareTo("-1") == 0)
        {
            origTransID = "";
        }

        if (origTransID.equals(""))
        {
            rcptRenderer.addOneColumnLineLeftAlign(transTypeName + ": " + newTransID);
        }
        else
        {
            // Reprints already have transTypeName in the data, do not add it again:
           if(newTransID.toLowerCase().indexOf(transTypeName.toLowerCase())!=-1)
           {
               // Do not add "ORIG: ID#"  unless void or refund
               if (printOrigTxnNum)
               {
                   rcptRenderer.add2ColumnLine(newTransID, !origTransID.equals("") ? "ORIG: " + origTransID : "");
               }
               else
               {
                   rcptRenderer.addOneColumnLineLeftAlign(newTransID);
               }
           }
            else
           {

               // Do not add "ORIG: ID#"  unless void or refund
               if (printOrigTxnNum)
               {
                   rcptRenderer.add2ColumnLine(transTypeName + ": " + newTransID, !origTransID.equals("") ? "ORIG: " + origTransID : "");
               }
               else
               {
                   rcptRenderer.addOneColumnLineLeftAlign(transTypeName + ": " + newTransID);
               }
           }
        }

        rcptRenderer.addBlankLine();

        return true;
    }

    //--------------------------------------------------------------------------
    //  Printing Transaction Items (PLUs, discounts, RA's, PO's)
    //--------------------------------------------------------------------------
    // This is a copy of printSaleItem
    public void printProductItem(Plu product, int transTypeID, boolean printPluCodes, ReceiptData receiptData)
    {

        Logger.logMessage("Entered CustomReceiptFormatter.printProductItem", Logger.LEVEL.TRACE);

        try {
            int quantity = (int)Math.round(product.getQuantity());
            BigDecimal totalItemPrice;
            //Parent products with modifiers need a different total displayed (all items added together)
            if ( (product.getIsModifier()) || (!product.hasModifiers()) )
            {
                totalItemPrice = product.getTotalItemPrice();
            }
            else
            {
                totalItemPrice = product.getTotalItemPriceForDisplay();
            }
            Boolean popUpReceipt = false;
            //MyQC Receipts
            if (receiptData.getReceiptTypeID() == TypeData.ReceiptType.POPUPRECEIPT)
            {
                popUpReceipt = true;
            }

            int indent = 4;

            // Print item price
            String rightColumn = "";
            if (transTypeID == 2 || transTypeID == 3)
            {
                rightColumn = "(" + formatPrice(df.format(totalItemPrice)) + ")";
            }
            else
            {
                rightColumn = formatPrice(df.format(totalItemPrice));
            }

            //Don't print prices on the right for modifiers
            if(product.getIsModifier())
            {
                rightColumn = "";
            }

            // Print product name
            String productName = product.getName();
            String indentStr = "    ";
            String modifierIndentStr = "  ";
            String wellnessIcon = " <img class='receipt-wellness-icon'>";

            if (product.getIsModifier() == false)
            {
                String qtyStr = ((Integer)quantity).toString();
                Integer qtyStrSize = qtyStr.length();
                //Remove length of qty string from indent, but always leave one space
                if (qtyStrSize<4)
                {
                    indent -= qtyStrSize;
                }
                else
                {
                    indent = 1;
                }
                //Build indentStr
                String nameIndentStr = "";
                for (int i = 0; i < indent; i++)
                {
                    nameIndentStr += " ";
                }
                productName = ((Integer)quantity).toString() + nameIndentStr + productName;
                //Reset indent
                indent = 4;

                if (displayWellnessIcon(product, receiptData)) {
                    // Add the wellness icon to the end of the product name
                    productName += wellnessIcon;
                }
            }
            else if (product.getIsModifier())
            {
                productName = indentStr + modifierIndentStr + productName;

                if (displayWellnessIcon(product, receiptData)) {
                    // Display the wellness icon between the product name and price
                    productName += wellnessIcon;
                }

                if (product.getUnitPrice()>0)
                {
                    if (product.getTaxDsctStr().equals("") == false)
                    {
                        productName += " (" + formatPrice(df.format(product.getUnitPrice())) + ")" ;
                    }
                    else
                    {
                        productName += " (" + formatPrice(df.format(product.getUnitPrice())) + ")";
                    }
                }
            }

            rcptRenderer.add2ColumnLine(rcptRenderer.indent(0, productName), rightColumn);

            // Print plu code, if any
            if (product.getPluCode().equals("") == false && printPluCodes)
            {
                rcptRenderer.addLine(indentStr + modifierIndentStr + "Product#: " + product.getPluCode());
            }

            Boolean isManualWeightedItem = false;
            //Manually weighed items are delineated by having "MAN WT" in their comment field  (NTEP requires we list them on the receipt)
            if ((product.getIsManualWeight() == true) || ((product.getItemComment().length() > 0)  && (product.getItemComment().equals("MAN WT")) ) )
            {
                isManualWeightedItem = true;
            }

            String nextLine = indentStr;
            Double pluPrice = product.getUnitPrice();
            //If this is a myQC receipt and the plu has a prep option, use the pre-prep price, since the prep option price will display on a separate line
            if ( (popUpReceipt) && (product.getPrepOption() != null)  )
            {
                //Only matters if the prep option has a price
                if (product.getPrepOption().getPrepOptionPrice()>0)
                {
                    //Can Use the base price when it is set (txns with combos)
                    if (product.getBasePrice()>0)
                    {
                        pluPrice = product.getBasePrice();
                    }
                    else
                    {
                        //Dev Bug Fix 4/8/20 - If this is the POS txn viewer, the price will NOT already include the prepOption, so we're going to make sure before we remove it
                        if (product.getAmount() > product.getUnitPrice())
                        {
                            //Rebuild the unit price by removing the prep option
                            if ( (product.getUnitPrice() - product.getPrepOption().getPrepOptionPrice()) >= 0)
                            {
                                pluPrice = product.getUnitPrice() - product.getPrepOption().getPrepOptionPrice();
                            }
                        }
                    }
                }
            }

            // Print unit price
            if (product.getIsWeightedItem())
            {
                // Print unit price PER POUND
                if (product.getTaxDsctStr().equals(""))
                {
                    nextLine += modifierIndentStr + formatPrice(df.format(pluPrice)) + " /lb";
                }
                else
                {
                    nextLine += modifierIndentStr+ formatPrice(df.format(pluPrice)) + " /lb" + " " + product.getTaxDsctStr() + ")";
                }

                // Print net weight (stored as the Quantity for weighted items)
                rcptRenderer.addLine(nextLine);

                if (isManualWeightedItem)
                {
                    rcptRenderer.addLine(indentStr+modifierIndentStr+"Weight: " + product.getNetWeight() + " lbs (Manual Wt)");
                }
                else
                {
                    rcptRenderer.addLine(indentStr+modifierIndentStr+"Weight: " + product.getNetWeight() + " lbs");
                }
            }
            else
            {
                // Print unit price for everything except modifiers
                if (!product.getIsModifier())
                {
                    if (product.getTaxDsctStr().equals("") == false)
                    {
                        nextLine += modifierIndentStr + formatPrice(df.format(pluPrice)) + " each" + " " + product.getTaxDsctStr() + ")";
                    }
                    else
                    {
                        nextLine += modifierIndentStr + formatPrice(df.format(pluPrice)) + " each";
                    }
                    rcptRenderer.addLine(nextLine);
                }
            }

            //Add prep option to printed receipts
            if (product.getPrepOption() != null)
            {
                boolean isDefaultPrep = product.getPrepOption().getDefaultOption();
                boolean displayDefaults = product.getPrepOption().getDisplayDefaultReceipts();
                // Check if the prep option is a default option and display defaults on receipts is on, or the prep option is not a default
                //OR If the default prep option has a price, we will ALWAYS display it, regardless of the setting
                boolean canPrintPrep = ( (( isDefaultPrep && displayDefaults ) || ( product.getPrepOption().getPrepOptionPrice() > 0 ) ) || !isDefaultPrep );

                if (canPrintPrep)
                {
                    // Add prep options to a new line
                    String prepPrintName = product.getPrepOption().getName();
                    if (popUpReceipt)
                    {
                        prepPrintName = product.getPrepOption().getOriginalName();
                        prepPrintName = "&nbsp;&nbsp;&nbsp;&nbsp;"+"+ " + prepPrintName;
                    }
                    else
                    {
                        prepPrintName = modifierIndentStr +  " + " + prepPrintName;
                    }

                    if (prepPrintName.length() >0)
                    {
                        if (product.getPrepOption().getPrepOptionPrice() > 0)
                        {
                            String prepPrintPrice = formatPrice(df.format(product.getPrepOption().getPrepOptionPrice())).toString();
                            prepPrintName += " (" + prepPrintPrice + ")";
                        }

                        rcptRenderer.addLine(rcptRenderer.indent(indent, prepPrintName));
                    }
                }
            }
            // Print item comment (Unless manual weight flag, ignore those)
            if ( (product.getItemComment().length() > 0)  && (!product.getItemComment().equals("MAN WT")) )
            {
                rcptRenderer.addLine("   Cmt: " + product.getItemComment());
            }
        } catch (Exception e) {
            ReceiptGen.logError("CustomReceiptFormatter.printProduct error", e);
        }
    }

    public void printComboProductItem(Plu product, boolean printPluCodes, ReceiptData receiptData)
    {
        try
        {
            int indent = 2;
            String indentStr = "    ";
            String modifierIndentStr = "  ";
            String wellnessIcon = " <img class='receipt-wellness-icon'>";
            Boolean popUpReceipt = false;

            // Print product name
            String productName = product.getName();
            // Show button text for MyQC Receipts
            if (receiptData.getReceiptTypeID() == TypeData.ReceiptType.POPUPRECEIPT )
            {
                popUpReceipt = true;
                indentStr = "&nbsp;&nbsp;&nbsp;&nbsp;";
                modifierIndentStr = "&nbsp;&nbsp;";
            }

            if( !product.getIsModifier() )
            {
                productName = indentStr + productName;

                if (displayWellnessIcon(product, receiptData)) {
                    // Display the wellness icon between the product name and upcharge
                    productName += wellnessIcon;
                }

                // Print upcharge after product name or wellness icon
                double unitPrice = product.getUnitPrice();
                double basePrice = product.getBasePrice();
                double prepPrice = 0;
                double upcharge = 0;
                if (product.getPrepOption()!=null)
                {
                    prepPrice = product.getPrepOption().getPrepOptionPrice();
                }
                //Only remove prep option price from myQc receipts
                if (popUpReceipt)
                {
                    //popUp can be myQc or QC Txn viewer
                    //myQC's amount will include the prep price, txn viewer will not
                    if ( product.getAmount() == (unitPrice + basePrice + prepPrice) )
                    {
                        upcharge = (unitPrice - basePrice - prepPrice);
                    }
                    else
                    {
                        //Else this is a Txn viewer receipt
                        upcharge = (unitPrice - basePrice);
                    }
                }
                else
                {
                    upcharge = (unitPrice - basePrice);
                }

                if (upcharge > 0)
                {
                    productName += " (+" + formatPrice(df.format(upcharge)) + ")";
                }
                else if (upcharge < 0)
                {
                    upcharge = upcharge * -1;
                    productName += " (-" + formatPrice(df.format(upcharge)) + ")";
                }
            }
            else if (product.getIsModifier())
            {
                productName = indentStr + modifierIndentStr + productName;

                if (displayWellnessIcon(product, receiptData)) {
                    // Display the wellness icon between the product name and price
                    productName += wellnessIcon;
                }

                if (product.getUnitPrice()>0)
                {
                    if (product.getTaxDsctStr().equals("") == false)
                    {
                        productName += " (" + formatPrice(df.format(product.getUnitPrice())) + ")";
                    }
                    else
                    {
                        productName += " (" + formatPrice(df.format(product.getUnitPrice())) + ")";
                    }
                }
            }

            // Print tax and dsct info
            if (product.getTaxDsctStr().equals("") == false)
            {
                productName += " " + product.getTaxDsctStr() + ")";
            }

            // Print name and price
            rcptRenderer.addOneColumnLineLeftAlign(rcptRenderer.indent(indent, productName));

            // Print prep option
            if(product.getPrepOption() != null)
            {
                boolean isDefaultPrep = product.getPrepOption().getDefaultOption();
                boolean displayDefaults = product.getPrepOption().getDisplayDefaultReceipts();
                // Check if the prep option is a default option and display defaults on receipts is on, or the prep option is not a default
                //OR If the prep option has a price, we will ALWAYS display it, regardless of the setting
                boolean canPrintPrep = ( (( isDefaultPrep && displayDefaults ) || ( product.getPrepOption().getPrepOptionPrice() > 0 ) ) || !isDefaultPrep );

                if (canPrintPrep)
                {
                    // Add prep options to a new line
                    String prepPrintName = product.getPrepOption().getName();
                    prepPrintName = indentStr + modifierIndentStr +  " + " + prepPrintName;

                    if (prepPrintName.length() >0)
                    {
                        if (product.getPrepOption().getPrepOptionPrice() > 0)
                        {
                            String prepPrintPrice = df.format(product.getPrepOption().getPrepOptionPrice());
                            prepPrintName += " (" + formatPrice(df.format(prepPrintPrice)) + ")";
                        }

                        rcptRenderer.addLine(rcptRenderer.indent(indent, prepPrintName));
                    }
                }
            }
            // Print plu code, if any
            if (product.getPluCode().equals("") == false && printPluCodes)
            {
                String pluCode = indentStr + "Product#: " + product.getPluCode();
                if (product.getIsModifier())
                {
                    pluCode = indentStr + modifierIndentStr + "Product#: " + product.getPluCode();
                }
                rcptRenderer.addLine(rcptRenderer.indent(indent, pluCode));
            }

            String comment = "";
            // Print item comment (Unless manual weight flag, ignore those)
            if ( (product.getItemComment().length() > 0)  && (!product.getItemComment().equals("MAN WT")) && !product.getIsModifier() )
            {
                comment = indentStr + " Cmt: " + product.getItemComment();
                rcptRenderer.addLine(rcptRenderer.indent(indent, comment));
            }
            else if ((product.getItemComment().length() > 0)  && (!product.getItemComment().equals("MAN WT")))
            {
                comment = indentStr + "    Cmt: " +  product.getItemComment();
                rcptRenderer.addLine(rcptRenderer.indent(indent, product.getItemComment()));
            }
        }
        catch (Exception error)
        {
            ReceiptGen.logError("CustomReceiptFormatter.printComboProductItem error", error);
        }
    }

    // Whether to display a wellness icon on a MyQC receipt
    private boolean displayWellnessIcon(Plu product, ReceiptData receiptData) {
        if (receiptData.getDisplayWellnessIcons()) {
            Object productWellness = ProductModel.getProductDetail(product.getPluID(), "WELLNESS");
            return productWellness != null && (boolean)productWellness;
        }
        return false;
    }

    // This is a copy of printSaleItem, just simplified for RA items
    public void printRAItem(ReceivedOnAcct raItem, int transTypeID)
    {
        try {
            String nameToDisplay = raItem.getNameForReceipt();
            int quantity = (int)Math.round(raItem.getQuantity());
            BigDecimal totalItemPrice = raItem.getTotalItemPrice();
            int indent = 0;

            // Print item name
            String rightColumn = "";
            if (transTypeID == 2 || transTypeID == 3)
            {
                rightColumn = "(" + formatPrice(df.format(totalItemPrice)) + ")";
            }
            else
            {
                rightColumn = "" + formatPrice(df.format(totalItemPrice));
            }

            rcptRenderer.add2ColumnLine(rcptRenderer.indent(indent, nameToDisplay), rightColumn);

            String nextLine = "   " + ((Integer)quantity).toString();

            // Print amount
            nextLine += " at " + formatPrice(df.format(raItem.getAmount())) + " each";
            rcptRenderer.addLine(nextLine);

            // Print item comment
            if (raItem.getItemComment().length() > 0)
            {
                rcptRenderer.addLine("   " + raItem.getItemComment());
            }
        } catch (Exception e) {
            ReceiptGen.logError("CustomReceiptFormatter.printProduct error", e);
        }
    }

    // This is a copy of printSaleItem, just simplified for paid out items
    public void printPaidOutItem(PaidOut paidOutItem, int transTypeID)
    {
        try {
            String nameToDisplay = paidOutItem.getNameForReceipt();
            int quantity = (int)Math.round(paidOutItem.getQuantity());
            BigDecimal totalItemPrice = paidOutItem.getTotalItemPrice();
            int indent = 0;

            // Print item name
            String rightColumn = "";
            if (transTypeID == 2 || transTypeID == 3)
            {
                rightColumn = "(" + formatPrice(df.format(totalItemPrice)) + ")";
            }
            else
            {
                rightColumn = formatPrice(df.format(totalItemPrice));
            }

            rcptRenderer.add2ColumnLine(rcptRenderer.indent(indent, nameToDisplay), rightColumn);

            String nextLine = "   " + ((Integer)quantity).toString();

            // Print amount
            nextLine += " at " + formatPrice(df.format(paidOutItem.getAmount())) + " each";
            rcptRenderer.addLine(nextLine);

            // Print item comment
            if (paidOutItem.getItemComment().length() > 0)
            {
                rcptRenderer.addLine("   " + paidOutItem.getItemComment());
            }
        } catch (Exception e) {
            ReceiptGen.logError("CustomReceiptFormatter.printPaidOutItem error", e);
        }
    }

    // This is a copy of printSaleItem, just simplified for paid out items
    public void printDonationItem(Donation donation, int transTypeID)
    {
        try {
            String nameToDisplay = donation.getNameForReceipt();
            int quantity = (int)Math.round(donation.getQuantity());
            BigDecimal totalItemPrice = donation.getTotalItemPrice();
            int indent = 0;

            // Print item name
            String rightColumn = "";
            if (transTypeID == 2 || transTypeID == 3)
            {
                rightColumn = "(" + formatPrice(df.format(totalItemPrice)) + ")";
            }
            else
            {
                rightColumn = formatPrice(df.format(totalItemPrice));
            }

            rcptRenderer.add2ColumnLine(rcptRenderer.indent(indent, nameToDisplay), rightColumn);

            String nextLine = "   " + ((Integer)quantity).toString();

            // Print amount
            nextLine += " at " + formatPrice(df.format(donation.getAmount())) + " each";
            rcptRenderer.addLine(nextLine);

            // Print item comment
            if (donation.getItemComment().length() > 0)
            {
                rcptRenderer.addLine("   " + donation.getItemComment());
            }
        } catch (Exception e) {
            ReceiptGen.logError("CustomReceiptFormatter.printDonationItem error", e);
        }
    }

    // This is a copy of printSaleItem, just simplified for combo items
    public String printComboItem(Combo comboItem, int transTypeID, ReceiptData receiptData, String lastOrderNumPrinted)
    {
        try
        {
            lastOrderNumPrinted = printOrigOrderNumDivider(comboItem.getOriginalOrderNum(), receiptData.getOrderNumber(), receiptData.getReceiptTypeID(), lastOrderNumPrinted);

            int quantity = (int)Math.round(comboItem.getQuantity());
            BigDecimal totalItemPrice = comboItem.getTotalItemPrice();
            //Make sure voids/refunds display the price and qty as a negative
            if (transTypeID == 2 || transTypeID == 3)
            {
                quantity = Math.abs(quantity) * -1;
                totalItemPrice = totalItemPrice.abs().multiply(new BigDecimal(-1));
            }
            int indent = 4;
            String comboName = comboItem.getName();
            String indentStr = "    ";
            // Print item name
            String rightColumn = "";
            if (transTypeID == 2 || transTypeID == 3)
            {
                rightColumn = "(" + formatPrice(df.format(totalItemPrice)) + ")";
            }
            else
            {
                rightColumn = formatPrice(df.format(totalItemPrice));
            }
            String qtyStr = ((Integer)quantity).toString();
            Integer qtyStrSize = qtyStr.length();
            //Remove length of qty string from indent, but always leave one space
            if (qtyStrSize < 4)
            {
                indent -= qtyStrSize;
            }
            else
            {
                indent = 1;
            }
            //Build indentStr
            String nameIndentStr = "";
            for (int i = 0; i < indent; i++)
            {
                nameIndentStr += " ";
            }
            comboName = ((Integer)quantity).toString() + nameIndentStr + comboName;
            //Reset indent
            indent = 4;

            rcptRenderer.add2ColumnLine(rcptRenderer.indent(0, comboName), rightColumn);

            String nextLine = indentStr;
            String comboIndentStr = "  ";

            nextLine += comboIndentStr + formatPrice(df.format(comboItem.getRootPrice())) + " each";
            rcptRenderer.addLine(nextLine);

            // Print item comment
            if (comboItem.getItemComment().length() > 0)
            {
                rcptRenderer.addLine(nextLine += comboIndentStr+" Cmt: " + comboItem.getItemComment());
            }
        }
        catch (Exception error)
        {
            ReceiptGen.logError("CustomReceiptFormatter.printComboItem error", error);
        }

        return lastOrderNumPrinted;
    }

    //--------------------------------------------------------------------------
    //  Printing Discounts
    //--------------------------------------------------------------------------
    public boolean printItemDiscount(Discount itemDsct, int itemDsctQty, int transTypeID, double openDsctPercentAmt)
    {
        int allowedDsctNameLength = 0;
        String line = "";
        String code = itemDsct.getShortName();
        String itemDsctName = itemDsct.getName();
        int itemDsctTypeID = itemDsct.getDiscountTypeID();
        BigDecimal itemDsctAmt = itemDsct.getTotalItemPrice();
        int indent = 3;

        // Shorten name if needed
        if (code.length() > 2)
        {
            code = code.substring(0, 2);
        }

        allowedDsctNameLength = 12 - code.length();
        if (allowedDsctNameLength > itemDsctName.length())
        {
            allowedDsctNameLength = itemDsctName.length();
        }

        // Print discount name and code
        if (itemDsctName.length() > allowedDsctNameLength && code.length() > 0)
        {
            line += "   " + code + ": " + itemDsctName.substring(0, allowedDsctNameLength);
        }
        else if (code.length() == 0)
        {
            line += "   " + itemDsctName.substring(0, allowedDsctNameLength);
        }
        else
        {
            line += "   " + code + ": " + itemDsctName;
        }

        // Print percent amount for open item % discounts
        if (itemDsctTypeID == 1 && openDsctPercentAmt != -1)
        {
            line += "(-" + openDsctPercentAmt + "%)";
        }

        // Print discount quantity
        line += " (" + itemDsctQty + " at ";

        // Print discount amount
        BigDecimal itemDiscountUnitPrice = new BigDecimal(itemDsctAmt.doubleValue() / itemDsctQty).setScale(2, RoundingMode.HALF_UP);
        if (itemDsctTypeID == 1 && openDsctPercentAmt != -1)
        {
            line += formatPrice(df.format(itemDiscountUnitPrice)) + ")";
        }
        else
        {
            line += formatPrice(df.format(itemDiscountUnitPrice)) + " each)";
        }
        rcptRenderer.addLine(rcptRenderer.indent(indent, line));

        return true;
    }

    public boolean printSubtotalDiscount(String discountName, BigDecimal discountAmt, int transTypeID, String code)
    {
        int allowedDsctNameLength = 0;
        String line = "";

        // Shorten name if needed
        if (code.length() > 2)
        {
            code = code.substring(0, 2);
        }
        allowedDsctNameLength = 24 - code.length();

        // Print code & discount name
        if (discountName.length() > allowedDsctNameLength && code.length() > 0)
        {
            line   += "   " + code + ": " + discountName.substring(0, allowedDsctNameLength);
        }
        else if (code.length() == 0)
        {
            line += "   " + discountName;
        }
        else
        {
            line += "   " + code + ": " + discountName;
        }

        // Print discount amount
        if (transTypeID == 2 || transTypeID == 3)
        {
            rcptRenderer.add2ColumnLine(line, "(" + formatPrice(df.format(discountAmt)) + ")");
        }
        else
        {
            rcptRenderer.add2ColumnLine(line, formatPrice(df.format(discountAmt)));
        }

        return true;
    }

    //--------------------------------------------------------------------------
    //  Printing Surcharges
    //--------------------------------------------------------------------------
    public boolean printSubtotalSurcharge(String surchargeName, BigDecimal surchargeAmt, int transTypeID, String code)
    {
        int allowedSurchargeNameLength = 0;
        String line = "";

        // Shorten name if needed
        if (code.length() > 2)
        {
            code = code.substring(0, 2);
        }
        allowedSurchargeNameLength = 24 - code.length();

        // Print code & discount name
        if (surchargeName.length() > allowedSurchargeNameLength && code.length() > 0)
        {
            line   += "   " + code + ": " + surchargeName.substring(0, allowedSurchargeNameLength);
        }
        else if (code.length() == 0)
        {
            line += "   " + surchargeName;
        }
        else
        {
            line += "   " + code + ": " + surchargeName;
        }

        // Print discount amount
        if (transTypeID == TypeData.TranType.VOID || transTypeID == TypeData.TranType.REFUND)
        {
            rcptRenderer.add2ColumnLine(line, "(" + formatPrice(df.format(surchargeAmt)) + ")");
        }
        else
        {
            rcptRenderer.add2ColumnLine(line, formatPrice(df.format(surchargeAmt)));
        }

        return true;
    }

    public void printItemSurcharge(Surcharge itemSurcharge, int transTypeID, boolean isCombo, ReceiptData receiptData)
    {
        int allowedSurchargeNameLength = 0;
        String shortName = itemSurcharge.getShortName();
        String surchargeName = itemSurcharge.getName();
        BigDecimal surchargeAmt = itemSurcharge.getTotalItemPrice();
        String line = "";
        String spaces = "   ";

        if(isCombo) {
            spaces =  "         ";
        }

        // Shorten name if needed
        if (shortName.length() > 2)
        {
            shortName = shortName.substring(0, 2);
        }
        allowedSurchargeNameLength = 24 - shortName.length();

        // Print code & surcharge name
        if (surchargeName.length() > allowedSurchargeNameLength && shortName.length() > 0)
        {
            line   += spaces + shortName + ": " + surchargeName.substring(0, allowedSurchargeNameLength);
        }
        else if (shortName.length() == 0)
        {
            line += spaces + surchargeName;
        }
        else
        {
            line += spaces + shortName + ": " + surchargeName;
        }

        // Print surcharge amount
        int qty = Math.abs(new Double(itemSurcharge.getQuantity()).intValue());
        BigDecimal surchargePrice = new BigDecimal(String.valueOf(itemSurcharge.getAmount())).setScale(2, RoundingMode.HALF_UP);
        double surchargeUnitPrice = Math.abs(new Double(surchargePrice.toString()));

        if (transTypeID == TypeData.TranType.VOID || transTypeID == TypeData.TranType.REFUND)
        {
            // Print reward quantity
            line += " (-" + qty + " at ";

            // Print reward amount
            line += "-" + formatPrice(df.format(surchargeUnitPrice)) + " each)";
        }
        else
        {
            // Print reward quantity
            line += " (" + qty + " at ";

            // Print reward amount
            line += formatPrice(df.format(surchargeUnitPrice)) + " each)";
        }
        //MyQC needs extra spaces
        if (receiptData.getReceiptTypeID() == TypeData.ReceiptType.POPUPRECEIPT)
        {
            line = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+line;
        }
        rcptRenderer.addLine(line);
    }
    //--------------------------------------------------------------------------
    //  Printing Rewards
    //--------------------------------------------------------------------------
    // For printing rewards that are NOT of type Free Product
    public void printLoyaltyReward(LoyaltyReward reward, String shortName, int transTypeID)
    {
        int allowedDsctNameLength = 0;
        String line = "";

        // Shorten name if needed
        if (shortName.length() > 2)
        {
            shortName = shortName.substring(0, 2);
        }
        allowedDsctNameLength = 24 - shortName.length();

        // Print code & discount name
        if (reward.getRewardName().length() > allowedDsctNameLength && shortName.length() > 0)
        {
            line   += "   " + shortName + ": " + reward.getRewardName().substring(0, allowedDsctNameLength);
        }
        else if (shortName.length() == 0)
        {
            line += "   " + reward.getRewardName();
        }
        else
        {
            line += "   " + shortName + ": " + reward.getRewardName();
        }

        // Print reward amount
        if (transTypeID == TypeData.TranType.VOID || transTypeID == TypeData.TranType.REFUND)
        {
            rcptRenderer.add2ColumnLine(line, "(" + formatPrice(df.format(reward.getExtendedAmt())) + ")");
        }
        else
        {
            rcptRenderer.add2ColumnLine(line, formatPrice(df.format(reward.getAmount())));
        }
    }

    public void printFreeProdReward(LoyaltyReward reward, String shortName, int transTypeID, boolean isComboProduct, ReceiptData receiptData)
    {
        int allowedRwdNameLength = 0;
        String line = "";
        String spaces =  "   ";
        int indent = 3;

        if(isComboProduct) {
            spaces =  "         ";
        }

        // Shorten name if needed
        if (shortName.length() > 2)
        {
            shortName = shortName.substring(0, 2);
        }
        allowedRwdNameLength = 12 - shortName.length();

        // Print code & discount name
        if (reward.getRewardName().length() > allowedRwdNameLength && shortName.length() > 0)
        {
            line   += spaces + shortName + ": " + reward.getRewardName().substring(0, allowedRwdNameLength);
        }
        else if (shortName.length() == 0)
        {
            if (reward.getRewardName().length() > allowedRwdNameLength)
            {
                line += spaces + reward.getRewardName().substring(0, allowedRwdNameLength);
            }
            else
            {
                line += spaces + reward.getRewardName();
            }
        }
        else
        {
            line += spaces + shortName + ": " + reward.getRewardName();
        }

        if (transTypeID == TypeData.TranType.VOID || transTypeID == TypeData.TranType.REFUND)
        {
            int rwdQty = Math.abs(new Double(reward.getQuantity()).intValue());
            double rwdAmt = Math.abs(reward.getAmount());

            // Print reward quantity
            line += " (-" + rwdQty + " at ";

            // Print reward amount
            line += formatPrice(df.format(rwdAmt)) + " each)";
        }
        else
        {
            // Print reward quantity
            line += " (" + new Double(reward.getQuantity()).intValue() + " at ";

            // Print reward amount
            line += formatPrice(df.format(reward.getAmount())) + " each)";
        }

        //MyQC needs extra spaces
        if (receiptData.getReceiptTypeID() == TypeData.ReceiptType.POPUPRECEIPT)
        {
            line = "&nbsp;&nbsp;"+line;
        }
        rcptRenderer.addLine(rcptRenderer.indent(indent, line));
    }

    //--------------------------------------------------------------------------
    //  Printing Tenders
    //--------------------------------------------------------------------------
    public boolean printTenderItem(BigDecimal tenderAmount, String tenderName, int transactionTypeID, int tenderTypeID, int receiptTypeID)
    {
        try {
            if (transactionTypeID == TypeData.TranType.VOID) {
                rcptRenderer.addOneColumnLineRightAlign(tenderName + " voided:   (" + formatPrice(df.format(tenderAmount)) + ")");
            } else if (transactionTypeID == TypeData.TranType.REFUND) {
                rcptRenderer.addOneColumnLineRightAlign(tenderName + " due:   (" + formatPrice(df.format(tenderAmount)) + ")");
            } else {
                //Handle the tendered vs redeemed text based on if the tender is a voucher
                String tendered_redeemed;
                if (tenderTypeID == TypeData.TenderType.VOUCHER) { tendered_redeemed = " redeemed"; }
                else                                             { tendered_redeemed = " tendered"; }

                //Shorten tender name if needed - only for printed receipts
                //If we still have access to the receiptTypeID, it's probably 4 where we want it to not be limited
                if (receiptTypeID != 4 && tenderName.length() > 20) { tenderName = tenderName.substring(0, 18) + ".."; }
                rcptRenderer.addOneColumnLineRightAlign(tenderName + tendered_redeemed + ":   " + formatPrice(df.format(tenderAmount)), false);
            }
        } catch (Exception ex) {
            ReceiptGen.logError("CustomReceiptFormatter.printTenderItem error", ex);
        }

        return true;
    }

    public boolean printAcctInfo(ArrayList<String> acctInfo, EmployeeData emp, int receiptTypeID, int badgeNumDigitsPrinted, int transactionTypeID)
    {
        // Delineate what balance/account data to allow (based on settings) to print, based on receipt type
        // Printed : All (Account & Balance)
        // Gift : None
        // Emailed : All
        // PopUp : No Balances
        // Kitchen : None
        Boolean showAccountData = false;
        Boolean showBalanceData = false;
        switch(receiptTypeID)
        {
            case (TypeData.ReceiptType.PRINTEDRECEIPT):
                showAccountData = true;
                showBalanceData = true;
                break;
            case (TypeData.ReceiptType.GIFTRECEIPT):
                showAccountData = false;
                showBalanceData = false;
                break;
            case (TypeData.ReceiptType.EMAILEDRECEIPT):
                showAccountData = true;
                showBalanceData = true;
                break;
            case (TypeData.ReceiptType.POPUPRECEIPT):
                showAccountData = true;
                showBalanceData = false;
                break;
            case (TypeData.ReceiptType.KITCHENPTRRECEIPT):
                showAccountData = false;
                showBalanceData = false;
                break;
        }

        if (transactionTypeID == TypeData.TranType.OPEN)
        {
            showAccountData = true;
            showBalanceData = false;
        }

        for (String anAcctInfo : acctInfo)
        {
            Integer strLen = acctInfo.toString().length();
            if ( (anAcctInfo.toLowerCase().contains("[name]")) && (showAccountData) )
            {
                printEmployeeName("Name: " + emp.getName());
            }
            else if ( (anAcctInfo.toLowerCase().contains("[acctnum]")) && (showAccountData) )
            {
                printEmployeeNumber("Acct No: " + emp.getEmployeeNumber());
            }
            else if ( (anAcctInfo.toLowerCase().contains("[globalacctbal]")) && (showBalanceData) )
            {
                String globalAcctBalStr = formatPrice(df.format(emp.getGlobalAvailable()));
                printGlobalAcctBal("Global Available: " + globalAcctBalStr);
            }
            else if ( (anAcctInfo.toLowerCase().contains("[storeacctbal]")) && (showBalanceData) )
            {
                String storeAvailStr = formatPrice(df.format(emp.getStoreAvailable()));
                printStoreAcctBal("Store Available: " + storeAvailStr);
            }
            else if ( (anAcctInfo.toLowerCase().contains("[badge]")) && (showAccountData) )
            {
                if (badgeNumDigitsPrinted==-1)
                {
                    printBadgeNum("Badge: " + emp.getBadgeNumber());
                }
                else
                {
                    printBadgeNum("Badge: " + emp.getModifiedBadgeNumber(badgeNumDigitsPrinted));
                }

            }
            else if ( (anAcctInfo.toLowerCase().contains("[terminalprofilebalancetype]")) && (showBalanceData) )
            {
                String balanceTypeInfo = emp.getReceiptBalancePromptTxt();
                int receiptBalTypeID = emp.getReceiptBalanceTypeID();

                // If there is text set for the receipt balance type ID, use it
                // Otherwise, use Store Balance or Global Balance, depending on the receipt type ID
                if (receiptBalTypeID == 1 || receiptBalTypeID == 2)
                {
                    if (balanceTypeInfo.equals(""))
                    {
                        balanceTypeInfo = "Store Balance: ";
                    }
                    else
                    {
                        balanceTypeInfo = balanceTypeInfo + ": ";
                    }
                }
                else if (receiptBalTypeID == 3 || receiptBalTypeID == 4)
                {
                    if (balanceTypeInfo.equals(""))
                    {
                        balanceTypeInfo = "Global Balance: ";
                    }
                    else
                    {
                        balanceTypeInfo = balanceTypeInfo + ": ";
                    }
                }

                // Print the balance, depending on the receipt balance type ID

                switch (receiptBalTypeID)
                {
                    case 1:
                        // Store balance
                        String storeBalanceStr = formatPrice(df.format(emp.getStoreBalance()));
                        balanceTypeInfo = balanceTypeInfo + storeBalanceStr;
                        break;
                    case 2:
                        // Store available
                        String storeAvailStr = formatPrice(df.format(emp.getStoreAvailable()));
                        balanceTypeInfo = balanceTypeInfo + storeAvailStr;
                        break;
                    case 3:
                        // Global balance
                        String globalBalanceStr = formatPrice(df.format(emp.getGlobalBalance()));
                        balanceTypeInfo = balanceTypeInfo + globalBalanceStr;
                        break;
                    case 4:
                        // Global available
                        String globalAvailStr = formatPrice(df.format(emp.getGlobalAvailable()));
                        balanceTypeInfo = balanceTypeInfo + globalAvailStr;
                        break;
                    default:
                        Logger.logMessage("CustomRcptFormatter.printAcctInfo error, unknown receipt balance type ID=" + Integer.toString(emp.getReceiptBalanceTypeID()), ReceiptGen.getLogFileName(), Logger.LEVEL.ERROR);
                }
                printBalanceTypeInfo(balanceTypeInfo);
            }
            else if ( (anAcctInfo.toLowerCase().contains("[dailylimitbal]")) && (showBalanceData) )
            {
                String dailyLimitBalStr = "Daily Limit Balance: " + formatPrice(df.format(emp.getDailyLimitBal()));
                printBalanceTypeInfo(dailyLimitBalStr);
            }
            else
            {
                Integer strLenAfter = acctInfo.toString().length();
                //Only print this if data was added to the info string in this method:
                if(strLenAfter > strLen)
                {
                    rcptRenderer.add2ColumnLine("", anAcctInfo.toString());
                }
            }
        }

        return true;
    }

    public boolean printBalanceTypeInfo(String balanceTypeInfo)
    {
        boolean result = true;

        try {
            rcptRenderer.add2ColumnLine("", balanceTypeInfo);
        } catch(Exception ex) {
            result = false;
            Logger.logException(ex, "");
        }

        return result;
    }

    public boolean printEmployeeName(String name)
    {
        rcptRenderer.addOneColumnLineRightAlign(name);

        return true;
    }

    public boolean printEmployeeNumber(String number)
    {
        rcptRenderer.addOneColumnLineRightAlign(number);

        return true;
    }

    public boolean printGlobalAcctBal(String globalBal)
    {
        rcptRenderer.addOneColumnLineRightAlign(globalBal);

        return true;
    }

    public boolean printStoreAcctBal(String storeBal)
    {
        rcptRenderer.addOneColumnLineRightAlign(storeBal);

        return true;
    }

    public boolean printBadgeNum(String badgeNum)
    {
        rcptRenderer.addOneColumnLineRightAlign(badgeNum);

        return true;
    }

    public void printCreditCardInfo(Tender tndr, int paymentGatewayID)
    {
        if (tndr.getCVM().equals("") == false)
        {
            printCCEMVInfo(tndr.getAcctNo(), tndr.getAuthCode(), tndr.getMerchID(), tndr);
        }
        else
        {
            switch (paymentGatewayID)
            {
                case TypeData.PaymentGateway.SIMPLIFY_FUSEBOX:
                    printSimplifyCCInfo(tndr);
                    break;
                default:
                    //Not EMV - Do not print this unless the show last 4 digits parameter is set to true
                    if(tndr.getPrintAccountNo())
                    {
                        printCCInfoNonEMV(tndr.getAcctNo(), tndr.getAuthCode(), tndr.getCardholderName());
                    }
            }
        }
    }

    // Call this if we only want to print the acctNo and authcode
    // (for non EMV CC transactions)
    private boolean printCCInfoNonEMV(String acctNo, String authCode, String cardholderName)
    {
        // Cardholder name will not always be available (dependent on payment gateway)
        if (!cardholderName.equals(""))
        {
            rcptRenderer.addOneColumnLineRightAlign(cardholderName);
        }

        rcptRenderer.addOneColumnLineRightAlign(acctNo);

        // Auth code will not always be sent back in EMV
        if (!authCode.equals(""))
        {
            rcptRenderer.addOneColumnLineRightAlign("Auth #: " + authCode);
        }
        return true;
    }

    private boolean printCCEMVInfo(String acctNo, String authCode, String merchID, Tender tndr)
    {
        rcptRenderer.addOneColumnLineRightAlign(acctNo);

        // Auth code will not always be sent back in EMV
        if (!authCode.equals(""))
        {
            rcptRenderer.addOneColumnLineRightAlign("Auth #: " + authCode);
        }
        if (!merchID.equals(""))
        {
            rcptRenderer.addOneColumnLineRightAlign("Merch ID #: " + merchID);
        }
        printEMVField("Invoice: ", tndr.getInvoiceNo());
        printEMVField("Reference: ", tndr.getRefNo());
        printEMVField("Entry Method: ", tndr.getEntryMethod());
        printEMVField("Application Label: ", tndr.getAppLbl());
        printEMVField("AID: ", tndr.getAID());
        printEMVField("TVR: ", tndr.getTVR());
        printEMVField("IAD: ", tndr.getIAD());
        printEMVField("TSI: ", tndr.getTSI());
        printEMVField("ARC: ", tndr.getARC());
        printEMVField("CVM: ", tndr.getCVM());

        if (tndr.getCardholderAgreement().equals("") == false)
        {
            rcptRenderer.addBlankLine();
            rcptRenderer.addLine(tndr.getCardholderAgreement());
            rcptRenderer.addBlankLine();
        }

        return true;
    }

    private void printSimplifyCCInfo(Tender tndr)
    {
        String[] simplifyRcptDetails = tndr.getPaymentGatewayCCDetails().split(",");
        String cardholderName = "";
        String cardholderNamePrefix = "Cardholder Name: ";
        // Try to find the cardholder name so we can print that first
        for (String rcptDetail : simplifyRcptDetails)
        {

            if (rcptDetail.contains(cardholderNamePrefix))
            {
                cardholderName = rcptDetail.substring(cardholderNamePrefix.length(), rcptDetail.length());
                printEMVField("", cardholderName);
                break;
            }
        }

        printEMVField("", tndr.getAcctNo());
        printEMVField("Auth: ", tndr.getAuthCode());
        printEMVField("Ref #: ", tndr.getInvoiceNo());
        printEMVField("Merchant #: ", tndr.getMerchID());

        for (String rcptDetail : simplifyRcptDetails)
        {
            if (!rcptDetail.contains(cardholderNamePrefix))
            {
                printEMVField("", rcptDetail);
            }
        }

        printEMVField("", "");
    }

    private void printCardholderAgreement(Tender tndr)
    {
        if (!tndr.getCardholderAgreement().equals(""))
        {
            rcptRenderer.addBlankLine();
            rcptRenderer.addOneColumnLineLeftAlign(tndr.getCardholderAgreement());
        }
    }

    private void printEMVField(String name, String value)
    {
        if (value != null && value.length() > 0)
        {
            rcptRenderer.addOneColumnLineRightAlign(name + value);
        }
    }

    public boolean printTenderComment(String commentPromptText, String comment)
    {
        // rcptRenderer.AddLine("    " + comment);
        // If there's no prompt text specified, we default to "Cmt"
        if (commentPromptText.equals(""))
        {
            commentPromptText = "Cmt";
        }

        // Add prompt text to the beginning of the comment
        comment = commentPromptText + ": " + comment;

        // Print
        rcptRenderer.add2ColumnLine("", comment);

        return true;
    }

    public void printIntegratedGiftCardBalance(Tender tndr)
    {
        if (!tndr.getIntegratedGiftCardBalance().isEmpty())
        {
            rcptRenderer.add2ColumnLine("", tndr.getIntegratedGiftCardBalance());
        }
    }
    //--------------------------------------------------------------------------
    //  Printing Taxes
    //--------------------------------------------------------------------------
    public boolean printTax(String taxName, BigDecimal taxAmt, int transTypeID, String code)
    {
        if (transTypeID == 2 || transTypeID == 3)
        {

            if (code.equals(""))
            {
                rcptRenderer.add2ColumnLine("   " + taxName, "(" + formatPrice(df.format(taxAmt)) + ")");
            }
            else
            {
                rcptRenderer.add2ColumnLine("   " + code + ": " + taxName, "(" + formatPrice(df.format(taxAmt)) + ")");
            }
        }
        else
        {

            if (code.equals(""))
            {
                rcptRenderer.add2ColumnLine("   " + taxName, formatPrice(df.format(taxAmt)));
            }
            else
            {
                rcptRenderer.add2ColumnLine("   " + code + ": " + taxName, formatPrice(df.format(taxAmt)));
            }
        }

        return true;
    }

    //--------------------------------------------------------------------------
    //  Printing Transaction Totals
    //--------------------------------------------------------------------------
    public boolean printSubtotal(BigDecimal subtotal, int transTypeID)
    {
        rcptRenderer.addBlankLine();

        if (transTypeID == 2 || transTypeID == 3)
        {
            rcptRenderer.add2ColumnLine("Merchandise Subtotal: ", "(" + formatPrice(df.format(subtotal)) + ")");
        }
        else
        {
            rcptRenderer.add2ColumnLine("Merchandise Subtotal: ", formatPrice(df.format(subtotal)));
        }

        return true;
    }

    public boolean printTotalHeader(BigDecimal purchaseTotal, int transTypeID, int  transactionTypeID)
    {
        rcptRenderer.addBlankLine();
        rcptRenderer.addBlankLine();

        if (transTypeID == 2 || transTypeID == 3)
        {
            rcptRenderer.addOneColumnLineRightAlign("TOTAL:   (" + formatPrice(df.format(purchaseTotal)) + ")");
        }
        else
        {
            rcptRenderer.addOneColumnLineRightAlign("TOTAL:   " + formatPrice(df.format(purchaseTotal)));
        }

        rcptRenderer.addBlankLine();

        return true;
    }

    public boolean printTotalDetails(BigDecimal changeDue)
    {
        rcptRenderer.addOneColumnLineRightAlign("Change due:   " + formatPrice(df.format(changeDue)));
        rcptRenderer.addBlankLine();

        return true;
    }

    //--------------------------------------------------------------------------
    //  Printing Transaction Footer Methods
    //--------------------------------------------------------------------------

    public boolean printGrossWeightTotals(BigDecimal grossWeightTotal, Boolean isMerchantCopy)
    {
        if (isMerchantCopy)
        {
            rcptRenderer.addBlankLine();
        }
        rcptRenderer.addOneColumnLineRightAlign("Gross Weight:  " + df.format(grossWeightTotal)+" lbs");

        return true;
    }

    public boolean printNetWeightTotals(BigDecimal netWeightTotal, Boolean isMerchantCopy)
    {
        if (isMerchantCopy)
        {
            rcptRenderer.addBlankLine();
        }

        rcptRenderer.addOneColumnLineRightAlign("Net Weight:  " + df.format(netWeightTotal)+" lbs");

        return true;
    }

    public boolean printItemCount(Integer itemCount, int transactionTypeID, boolean isMerchantCopy)
    {
        if (isMerchantCopy)
        {
            rcptRenderer.addBlankLine();
        }

        switch (transactionTypeID)
        {
            case TypeData.TranType.REFUND:
                rcptRenderer.addOneColumnLineRightAlign("Number of Items Refunded: " + itemCount.toString());
                break;
            case TypeData.TranType.VOID:
                rcptRenderer.addOneColumnLineRightAlign("Number of Items Voided: " + itemCount.toString());
                break;
            case TypeData.TranType.OPEN:
                rcptRenderer.addOneColumnLineRightAlign("Number of Items: " + itemCount.toString());
                break;
            default:
                rcptRenderer.addOneColumnLineRightAlign("Number of Items Sold: " + itemCount.toString());
        }

        return true;
    }

    public boolean printFooterLine(String footerLine, boolean firstLine, boolean lastLine)
    {
        if (firstLine)
        {
            rcptRenderer.addBlankLine();
        }
        rcptRenderer.centerLine(footerLine);
        if(lastLine)
        {
            rcptRenderer.addBlankLine();
        }
        return true;
    }

    public boolean printMerchantCopyLbl()
    {
        rcptRenderer.centerLine("-----     MERCHANT COPY    -----");

        return true;
    }

    public boolean printCustomerCopyLbl(ReceiptData rcptData)
    {
        int rcptTypeID = rcptData.getReceiptTypeID();

        if (rcptTypeID == TypeData.ReceiptType.PRINTEDRECEIPT || rcptTypeID == TypeData.ReceiptType.POPUPRECEIPT || rcptTypeID == TypeData.ReceiptType.EMAILEDRECEIPT)
        {
            rcptRenderer.centerLine("-----     CUSTOMER COPY    -----");
        }
        else
        {
            // When would this ever print ... ???
            rcptRenderer.centerLine("-----      REPRINT     -----");
        }

        return true;
    }

    public boolean finishPrint()
    {
        rcptRenderer.addBlankLine();
        rcptRenderer.cutPaper();

        return true;
    }

    public boolean printTrainingModeNote()
    {
        rcptRenderer.centerLine("====== TRAINING MODE ======");
        return true;
    }

    public boolean printSignatureLine()
    {
        rcptRenderer.addBlankLine();
        rcptRenderer.addBlankLine();
        rcptRenderer.addBlankLine();
        rcptRenderer.centerLine("X_____________________________________");
        rcptRenderer.centerLine("SIGNATURE");

        return true;
    }

    //--------------------------------------------------------------------------
    //  Other
    //--------------------------------------------------------------------------
    public boolean printNutritionInfo(NutritionData nutritionData)
    {
        ArrayList<NutritionCategory> nutritionCategoryNames = nutritionData.getNutritionCategories();
        boolean isHTMLRenderer = false;

        if(nutritionCategoryNames == null || nutritionCategoryNames.size() < 1)
        {
            return false;
        }

        if (rcptRenderer instanceof BasicHTMLRenderer)
        {
            isHTMLRenderer = true;
        }

        rcptRenderer.addLine("Nutrition Facts");
        if (isHTMLRenderer)
        {
            ((BasicHTMLRenderer) rcptRenderer).beginNutritionTable();
            ((BasicHTMLRenderer) rcptRenderer).beginNutritionTableHeader();
        }
        else
        {
            rcptRenderer.beginTable();
            rcptRenderer.beginTableHeader();
        }

        ArrayList heading1 = new ArrayList();
        ArrayList heading2 = new ArrayList();
        heading1.add("Qty");
        heading1.add("Item Name");
        heading2.add("");
        heading2.add("");

        for (NutritionCategory cat : nutritionCategoryNames)
        {
            heading1.add(cat.getShortName());
            heading2.add(cat.getMeasurementLbl());
        }

        if (isHTMLRenderer)
        {
            ((BasicHTMLRenderer) rcptRenderer).tableNutritionRow(heading1);
            ((BasicHTMLRenderer) rcptRenderer).tableNutritionRow(heading2);
        }
        else
        {
            rcptRenderer.tableRow(heading1);
            rcptRenderer.tableRow(heading2);
        }
        rcptRenderer.endTableHeader();

        if (nutritionData.getNutritionItemsFlex().size() > 0)
        {
            for (Object item : nutritionData.getNutritionItemsFlex())
            {
                ArrayList itemRow = (ArrayList)item;

                if (isHTMLRenderer)
                {
                    ((BasicHTMLRenderer) rcptRenderer).tableNutritionRow(itemRow);
                }
                else
                {
                    rcptRenderer.tableRow(itemRow);
                }
            }
        }
        else
        {
            for (NutritionItem item : nutritionData.getNutritionItemsDB())
            {
                ArrayList itemRow = item.convertToArrayList();

                if (isHTMLRenderer)
                {
                    ((BasicHTMLRenderer) rcptRenderer).tableNutritionRow(itemRow);
                }
                else
                {
                    rcptRenderer.tableRow(itemRow);
                }
            }
        }

        rcptRenderer.beginTableFooter();
        ArrayList nutritionTotals = nutritionData.getNutritionTotals();
        for(int i = 0; i < nutritionTotals.size(); i++)
        {
            ArrayList itemRow = new ArrayList();
            itemRow.add("");
            if(i == 0)
            {
                itemRow.add("Totals");
            }
            itemRow.addAll((ArrayList)nutritionTotals.get(i));

            if (isHTMLRenderer)
            {
                ((BasicHTMLRenderer) rcptRenderer).tableNutritionRow(itemRow);
            }
            else
            {
                rcptRenderer.tableRow(itemRow);
            }
        }
        rcptRenderer.endTableFooter();
        rcptRenderer.endTable();

        // Print no data available footnote if necessary
        if (nutritionData.getNeedsNAFootnote())
        {
            rcptRenderer.addLine("*** - signifies that no data is available");
        }

        // Print number truncate footnote if necessary
        if (nutritionData.getNeedsTruncateDigitsFootnote())
        {
            rcptRenderer.addLine("### - signifies that the value exceeded the allotted space");
        }

        rcptRenderer.addBlankLine();

        return true;
    }

    public boolean printLoyaltySummary(HashMap<Integer,LoyaltyDetail> loyaltySummaryData, EmployeeData loyaltyAcct, int receiptTypeID, int transTypeID, int badgeNumDigitsPrinted)
    {

        ArrayList<String> printArrAcct = new ArrayList<String>();
        ArrayList<String> printArrProgram = new ArrayList<String>();
        // Cycle account info
        if (loyaltyAcct != null)
        {
            if (loyaltyAcct.getName().equals("") == false)
            {
                printArrAcct.add("Name: " + loyaltyAcct.getName());
            }
            if (loyaltyAcct.getBadgeNumber().equals("") == false)
            {
                if (badgeNumDigitsPrinted==-1)
                {
                    printArrAcct.add("Badge: " + loyaltyAcct.getBadgeNumber());
                }
                else
                {
                    printArrAcct.add("Badge: " + loyaltyAcct.getModifiedBadgeNumber(badgeNumDigitsPrinted));
                }

            }
            if (loyaltyAcct.getEmployeeNumber().equals("") == false)
            {
                printArrAcct.add("Acct No: " + loyaltyAcct.getEmployeeNumber());
            }
        }
        else
        {
            Logger.logMessage("CustomReceiptFormatter.printLoyaltySummary: loyaltyAcct is null", logFileName, Logger.LEVEL.TRACE);
        }

        // Cycle program point info
        for (LoyaltyDetail detail : loyaltySummaryData.values())
        {
            String programName = detail.getProgramName();
            Integer pointsEarned = new Integer(detail.getPointsEarned());
            Integer pointsRedeemed = new Integer(detail.getPointsRedeemed());
            String pointsBalance = detail.getPointsBalance();
            boolean printedPointsEarned = false;
            boolean printedPointsRedeemed = false;
            boolean printedPointsBal = false;

            // Print program name
            printArrProgram.add(programName);

            // Don't print the line if the value is 0
            if (pointsEarned != 0)
            {
                // Points Earned should always be positive for SALES
                // Points Earned should always be negative for VOIDS and REFUNDS
                if (pointsEarned < 0 && transTypeID == TypeData.TranType.SALE)
                {
                    pointsEarned = pointsEarned * -1;
                }
                else if (pointsEarned > 0 && (transTypeID == TypeData.TranType.VOID || transTypeID == TypeData.TranType.REFUND))
                {
                    pointsEarned = pointsEarned * -1;
                }
                printArrProgram.add("     Points Earned: " + pointsEarned.toString());
                printedPointsEarned = true;
            }

            // Don't print the line if the value is 0
            if (pointsRedeemed != 0)
            {
                // Points Redeemed should always be positive for SALES
                // Points Redeemed should always be negative for VOIDS and REFUNDS
                if (pointsRedeemed < 0 && transTypeID == TypeData.TranType.SALE)
                {
                    pointsRedeemed = pointsRedeemed * -1;
                }
                else if (pointsRedeemed > 0 && (transTypeID == TypeData.TranType.VOID || transTypeID == TypeData.TranType.REFUND))
                {
                    pointsRedeemed = pointsRedeemed * -1;
                }
                printArrProgram.add("     Points Redeemed: " + pointsRedeemed.toString());
                printedPointsRedeemed = true;
            }

            if (receiptTypeID == TypeData.ReceiptType.PRINTEDRECEIPT && pointsBalance.equals("") == false)
            {
                // HP 964: only print the points balance if points were earned or redeemed for this program
                if (pointsEarned != 0 || pointsRedeemed != 0)
                {
                    printArrProgram.add("     Points Balance: " + pointsBalance);
                    printedPointsBal = true;
                }
            }

            // If we're not printing Points Earned, Points Redeemed, or Points Balance, then don't print the program name either
            if (!printedPointsEarned && !printedPointsRedeemed && !printedPointsBal)
            {
                printArrProgram.remove(printArrProgram.size() - 1);
            }
        }
        //Only print header and account if there are rewards info to show
        if (printArrProgram.size() > 0)
        {
            rcptRenderer.centerLine("-----     LOYALTY SUMMARY    -----");
            rcptRenderer.addBlankLine();

            //Print account info
            for (String sA: printArrAcct)
            {
                rcptRenderer.addLine(sA);
            }

            if (printArrAcct.size() > 0)
            {
                rcptRenderer.addBlankLine();
            }

            //Print reward info
            for (String sP: printArrProgram)
            {
                rcptRenderer.addLine(sP);
            }
        }
        else
        {
            Logger.logMessage("CustomReceiptFormatter.printLoyaltySummary: printArrProgram size is 0, will not print any loyalty info!", logFileName, Logger.LEVEL.TRACE);
        }
        return true;
    }

    public String printOrigOrderNumDivider(String origOrderNum, String orderNumber, int receiptTypeID, String lastOrderNumPrinted)
    {
        if ( (receiptTypeID != TypeData.ReceiptType.KITCHENPTRRECEIPT) && (!lastOrderNumPrinted.equals(origOrderNum))
                && (((StringFunctions.stringHasContent(origOrderNum)) && (!printedOrderNumbers.contains(origOrderNum)))
                || ((StringFunctions.stringHasContent(orderNumber)) && (!printedOrderNumbers.contains(orderNumber)))))
        {
            String origOrderNumToPrint = "";
            rcptRenderer.addBlankLine();

            if ( (lastOrderNumPrinted.equals("") == false) && (origOrderNum.equals("") == true) )
            {
                origOrderNumToPrint = "----   Order Number: " + orderNumber + "   ----";
                lastOrderNumPrinted = orderNumber;
                printedOrderNumbers.add(orderNumber);
            }
            else
            {
                origOrderNumToPrint = "----   Order Number: " + origOrderNum + "   ----";
                lastOrderNumPrinted = origOrderNum;
                printedOrderNumbers.add(origOrderNum);
            }

            rcptRenderer.centerLine(origOrderNumToPrint);
            rcptRenderer.addBlankLine();
        }

        return lastOrderNumPrinted;
    }

    // Format the price for international currencies
    public String formatPrice(String price) {
        String formattedPrice = currencyType + price;

        // Format the currency position
        if(!currencyBeforePrice) {
            formattedPrice = price + currencyType;
        }

        // Format the decimal type
        if(!decimalInPrice) {
            formattedPrice = formattedPrice.replace(".", ",");
        }

        return formattedPrice;
    }

    /**
     * <p>Builds a receipt from the given {@link ReceiptData} which contains {@link CustomerReceiptData}.</p>
     *
     * @param receiptData The {@link ReceiptData} which contains {@link CustomerReceiptData} to use to build the receipt.
     * @return Whether or not a receipt could be built from the given {@link ReceiptData} which contains {@link CustomerReceiptData}.
     */
    private boolean buildReceiptFromCustomerReceiptData (ReceiptData receiptData) {

        // make sure the receipt data is valid
        if (receiptData == null) {
            Logger.logMessage("The receipt data passed to CustomReceiptFormatter.buildReceiptFromCustomerReceiptData can't be null, now returning false!",
                    CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the customer receipt data is valid
        CustomerReceiptData customerReceiptData = receiptData.getCustomerReceiptData();
        if (customerReceiptData == null) {
            Logger.logMessage("The customer receipt data passed to CustomReceiptFormatter.buildReceiptFromCustomerReceiptData can't be null, now returning false!",
                    CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure we have a valid renderer
        if (rcptRenderer == null) {
            Logger.logMessage("The receipt renderer in CustomReceiptFormatter.buildReceiptFromCustomerReceiptData can't be null, now returning false!",
                    CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the renderer can render
        if (!rcptRenderer.begin()) {
            Logger.logMessage("The receipt renderer in CustomReceiptFormatter.buildReceiptFromCustomerReceiptData is unable to begin rendering, now returning false!",
                    CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // print training mode info
        if (customerReceiptData.getTrainingModeTransTypeID() != 0) {
            addContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.CENTER_LINE, "====== TRAINING MODE ======", CustomerReceiptData.CUST_RCPT_LOG);
            addToReceipt(rcptRenderer, TypeData.ReceiptContentType.BLANK_LINE, CustomerReceiptData.CUST_RCPT_LOG);
        }

        // print the receipt header
        ArrayList<String> receiptHeaders = customerReceiptData.getReceiptHeaders();
        if (!DataFunctions.isEmptyCollection(receiptHeaders)) {
            for (String receiptHeader : receiptHeaders) {
                addContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.CENTER_LINE, receiptHeader, CustomerReceiptData.CUST_RCPT_LOG);
            }
        }

        // print the transaction header information
        printTransactionHeaderInformation(receiptData);

        // information for how to display price on the receipt
        currencyType = receiptData.getCurrencyType();
        currencyBeforePrice = receiptData.isCurrencyBeforePrice();
        decimalInPrice = receiptData.isDecimalInPrice();

        // variables to maintain state while looping through transaction line items
        boolean totalHeaderPrinted = false;
        boolean subtotalPrinted = false;
        int itemCount = TransactionData.getItemCount(customerReceiptData);
        String lastOrderNumberPrinted = "";
        int currentComboID = 0;

        // TODO KEEP ADDING TO RECEIPT

        if (!rcptRenderer.end()) {
            Logger.logMessage("The receipt renderer in CustomReceiptFormatter.buildReceiptFromCustomerReceiptData is unable to stop rendering, now returning false!",
                    CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        return true;
    }

    /**
     * <p>Adds the transaction header information to the receipt.</p>
     *
     * @param receiptData The {@link ReceiptData} which contains {@link CustomerReceiptData} to use to build the receipt.
     */
    public void printTransactionHeaderInformation (ReceiptData receiptData) {

        // make sure the receipt data is valid
        if (receiptData == null) {
            Logger.logMessage("The receipt data passed to CustomReceiptFormatter.printTransactionHeaderInformation can't be null, unable to continue",
                    CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return;
        }

        // make sure the customer receipt data is valid
        CustomerReceiptData customerReceiptData = receiptData.getCustomerReceiptData();
        if (customerReceiptData == null) {
            Logger.logMessage("The customer receipt data passed to CustomReceiptFormatter.printTransactionHeaderInformation can't be null, unable to continue",
                    CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return;
        }

        int transTypeID = customerReceiptData.getTransTypeID();
        int transStatusID = customerReceiptData.getTransactionStatusID();
        boolean printOriginalTransactionID = ((transTypeID == TypeData.TranType.REFUND) || (transTypeID == TypeData.TranType.VOID));
        boolean useOpenTransactionReceiptRules = ((transTypeID == TypeData.TranType.OPEN)
                || ((transTypeID == TypeData.TranType.CANCEL) && (transStatusID == TypeData.TransactionStatus.EXPIRED))
                || (transTypeID == TypeData.TranType.SALE));

        addToReceipt(rcptRenderer, TypeData.ReceiptContentType.BLANK_LINE, CustomerReceiptData.CUST_RCPT_LOG);

        // add OPEN or EXPIRED to receipt
        if (transTypeID == TypeData.TranType.OPEN) {
            addContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.CENTER_LARGE_LINE, "****  OPEN  ****", CustomerReceiptData.CUST_RCPT_LOG);
            addToReceipt(rcptRenderer, TypeData.ReceiptContentType.BLANK_LINE, CustomerReceiptData.CUST_RCPT_LOG);
        }
        else if ((transTypeID == TypeData.TranType.CANCEL) && (transStatusID == TypeData.TransactionStatus.EXPIRED)) {
            addContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.CENTER_LARGE_LINE, "****  EXPIRED  ****", CustomerReceiptData.CUST_RCPT_LOG);
            addToReceipt(rcptRenderer, TypeData.ReceiptContentType.BLANK_LINE, CustomerReceiptData.CUST_RCPT_LOG);
        }

        // add the order number to the receipts
        String orderNumber = customerReceiptData.getOrderNumber();
        if ((customerReceiptData.getPrintOrderNumber()) && ((transTypeID == TypeData.TranType.SALE) || (useOpenTransactionReceiptRules)) && (StringFunctions.stringHasContent(orderNumber))) {
            addMultiContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.CENTER_LARGE_LINE, "Order Number: ", orderNumber, CustomerReceiptData.CUST_RCPT_LOG);
            addToReceipt(rcptRenderer, TypeData.ReceiptContentType.BLANK_LINE, CustomerReceiptData.CUST_RCPT_LOG);
        }

        int orderTypeID = customerReceiptData.getOrderTypeID();
        if (((transTypeID == TypeData.TranType.SALE) || (transTypeID == TypeData.TranType.OPEN))
                && (receiptData.getPrintOrderTypeAndTimeMsg())
                && (orderTypeID > 0)
                && (orderTypeID != TypeData.OrderType.NORMAL_SALE)) {
            String pickupDeliveryText = "";
            if (orderTypeID == TypeData.OrderType.PICKUP) {
                pickupDeliveryText = "Pickup on";
            }
            else if (orderTypeID == TypeData.OrderType.DELIVERY) {
                pickupDeliveryText = "Delivery on";
            }

            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(MMHTimeFormatString.YR_MO_DY_SLASHES_HR_MIN_SEC_AMPM);
            String transDateStr = customerReceiptData.getTransDate() + " " + customerReceiptData.getTransTime();
            LocalDateTime transactionDate = LocalDateTime.parse(transDateStr, dateTimeFormatter);
            // make sure we could parse the transaction date
            String message = "";
            if (transactionDate == null) {
                Logger.logMessage(String.format("An error occurred while trying to parse the transaction date String of %s using the pattern %s in CustomReceiptFormatter.printTransactionHeaderInformation," +
                        "unable to create a %s message for the receipt, adding a blank line to the receipt and continuing.",
                        Objects.toString(transDateStr, "N/A"),
                        Objects.toString(MMHTimeFormatString.YR_MO_DY_SLASHES_HR_MIN_SEC_AMPM, "N/A"),
                        Objects.toString((orderTypeID == TypeData.OrderType.PICKUP ? "pickup" : "delivery"), "N/A")), CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            }
            else {
                dateTimeFormatter = DateTimeFormatter.ofPattern(MMHTimeFormatString.MO_DY);
                String dateNoYear = transactionDate.format(dateTimeFormatter);
                dateTimeFormatter = DateTimeFormatter.ofPattern(MMHTimeFormatString.HR_MIN_AMPM);
                String timeNoSeconds = transactionDate.format(dateTimeFormatter);
                message = pickupDeliveryText + " " + dateNoYear + " at " + timeNoSeconds;
            }

            if (StringFunctions.stringHasContent(message)) {
                addContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.CENTER_LARGE_LINE, message, CustomerReceiptData.CUST_RCPT_LOG);
            }
            addToReceipt(rcptRenderer, TypeData.ReceiptContentType.BLANK_LINE, CustomerReceiptData.CUST_RCPT_LOG);
        }

        // add the suspended label and name to the receipt
        if ((useOpenTransactionReceiptRules) && (StringFunctions.stringHasContent(customerReceiptData.getSuspTransName()))) {
            String suspLine;
            if (StringFunctions.stringHasContent(customerReceiptData.getSuspTransNameLbl())) {
                suspLine = customerReceiptData.getSuspTransNameLbl() + ": " + customerReceiptData.getSuspTransName();
            }
            else {
                suspLine = customerReceiptData.getSuspTransName();
            }
            addContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.ONE_COL_LEFT, suspLine, CustomerReceiptData.CUST_RCPT_LOG);
            addToReceipt(rcptRenderer, TypeData.ReceiptContentType.BLANK_LINE, CustomerReceiptData.CUST_RCPT_LOG);
        }

        addMultiContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.TWO_COL, customerReceiptData.getTransDate(), customerReceiptData.getTransTime(), CustomerReceiptData.CUST_RCPT_LOG);
        addMultiContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.TWO_COL, "Cashier: " + customerReceiptData.getCashierName(), "TID: " + customerReceiptData.getTerminalID(), CustomerReceiptData.CUST_RCPT_LOG);

        String originalTransactionID = customerReceiptData.getOriginalTranID();
        if (originalTransactionID.equalsIgnoreCase("-1")) {
            originalTransactionID = "";
        }
        if (originalTransactionID.equalsIgnoreCase("")) {
            addContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.ONE_COL_LEFT, TypeData.getTransactionTypeName(transTypeID) + ": " + customerReceiptData.getTransactionID(), CustomerReceiptData.CUST_RCPT_LOG);
        }
        else {
            // reprints already have the transaction type name, don't add it again
            if (StringFunctions.rkContains(TypeData.getTransactionTypeName(transTypeID).toLowerCase(), customerReceiptData.getTransactionID().toLowerCase(), 37)) {
                if (printOriginalTransactionID) {
                    addMultiContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.TWO_COL, customerReceiptData.getTransactionID(),
                            (StringFunctions.stringHasContent(customerReceiptData.getOriginalTranID()) ? "ORIG: " + customerReceiptData.getOriginalTranID() : ""), CustomerReceiptData.CUST_RCPT_LOG);
                }
                else {
                    addContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.ONE_COL_LEFT, customerReceiptData.getTransactionID(), CustomerReceiptData.CUST_RCPT_LOG);
                }
            }
            else {
                if (printOriginalTransactionID) {
                    addMultiContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.TWO_COL, TypeData.getTransactionTypeName(transTypeID) + ": " + customerReceiptData.getTransactionID(),
                            (StringFunctions.stringHasContent(customerReceiptData.getOriginalTranID()) ? "ORIG: " + customerReceiptData.getOriginalTranID() : ""), CustomerReceiptData.CUST_RCPT_LOG);
                }
                else {
                    addContentToReceipt(rcptRenderer, TypeData.ReceiptContentType.ONE_COL_LEFT, TypeData.getTransactionTypeName(transTypeID) + ": " + customerReceiptData.getTransactionID(), CustomerReceiptData.CUST_RCPT_LOG);
                }
            }
        }
        addToReceipt(rcptRenderer, TypeData.ReceiptContentType.BLANK_LINE, CustomerReceiptData.CUST_RCPT_LOG);
    }

}
