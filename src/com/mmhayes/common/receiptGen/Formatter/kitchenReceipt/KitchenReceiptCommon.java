package com.mmhayes.common.receiptGen.Formatter.kitchenReceipt;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-28 07:51:11 -0400 (Wed, 28 Apr 2021) $: Date of last commit
    $Rev: 13861 $: Revision of last commit
    Notes: Contains functionality that may be shared across KP/KDS/KMS.
*/

import com.mmhayes.common.receiptGen.Commands.JPOS_COMMAND;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.utils.stringMatchingUtils.RabinKarp;

import java.util.*;

/**
 * <p>Contains functionality that may be shared across KP/KDS/KMS.</p>
 *
 */
public class KitchenReceiptCommon {

    public static final int TAB_CHARS = 4;

    /**
     * <p>Encodes the given {@link String} using base 64.</p>
     *
     * @param s The {@link String} to encode.
     * @return The given {@link String} encoded using base 64.
     */
    public static String encode (String s) {
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(s.getBytes());
    }

    /**
     * <p>Decodes the given base 64 encoded {@link String}.</p>
     *
     * @param s The base 64 encoded {@link String} to decode.
     * @return The decoded version of the given base 64 encoded {@link String}.
     */
    public static String decode (String s) {
        Base64.Decoder decoder = Base64.getDecoder();
        return new String(decoder.decode(s));
    }

    /**
     * <p>Remove JPOS command {@link String} from the given {@link String}.</p>
     *
     * @param s The {@link String} to remove JPOS commands from.
     * @return The given {@link String} with any JPOS commands removed.
     */
    public static String removeJPOSCommands (String s) {

        // make sure the String has some content
        if (!StringFunctions.stringHasContent(s)) {
            return s;
        }

        // the list of JPOS commands we should remove
        String[] jposCommandsToRemove = new String[]{JPOS_COMMAND.ALIGN_CENTER, JPOS_COMMAND.ALIGN_RIGHT, JPOS_COMMAND.LARGE_FONT};

        for (String command : jposCommandsToRemove) {
            s = RabinKarp.removePattern(command, s, 37);
        }

        return s;
    }

    /**
     * <p>Restores JPOS commands that had been removed from the receipt line.</p>
     *
     * @param s The {@link String} to add JPOS commands to.
     * @param isCentered Whether or not the receipt line should be centered.
     * @param isRightJustified Whether or not the receipt line should be right justified.
     * @param isLargeFont Whether or not the receipt line should be in a large font.
     * @return The given {@link String} with all JPOS commands restored.
     */
    public static String addJPOSCommands (String s, boolean isCentered, boolean isRightJustified, boolean isLargeFont) {

        String jposCommands = "";
        if (isCentered) {
            jposCommands += JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER;
        }
        if (isRightJustified) {
            jposCommands += JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_RIGHT;
        }
        if (isLargeFont) {
            jposCommands += JPOS_COMMAND.ESC + JPOS_COMMAND.LARGE_FONT;
        }

        return jposCommands + s;
    }

    /**
     * <p>Creates a single line on a receipt with two columns, one is left justified while the other is right justified.</p>
     *
     * @param maxCharsPerLine The maximum number of characters that should appear on a line on the receipt.
     * @param left The {@link String} that should appear in the left column.
     * @param right The {@link String} that should appear in the right column.
     * @return A single line {@link String} for the receipt with two columns, one is left justified while the other is right justified.
     */
    public static String twoColumn (int maxCharsPerLine, String left, String right) {
        int leftColChars = ((maxCharsPerLine * 2) / 3); // give the left column 2/3 of available characters
        int rightColChars = maxCharsPerLine - leftColChars; // give the right column 1/3 of available characters

        int leftLen = (StringFunctions.stringHasContent(left) ? left.length() : 0);
        int rightLen = (StringFunctions.stringHasContent(right) ? right.length() : 0);

        // truncate the left column if it's too long
        if (leftLen >= leftColChars) {
            left = left.substring(0, leftColChars - 4) + "...";
        }

        // truncate the right column if it's too long
        if (rightLen > rightColChars) {
            right = right.substring(0, rightColChars - 3) + "...";
        }

        // calculate the number of columns that should be in-between the left and right columns on the receipt
        leftLen = (StringFunctions.stringHasContent(left) ? left.length() : 0);
        rightLen = (StringFunctions.stringHasContent(right) ? right.length() : 0);
        int numSpaces = maxCharsPerLine - (leftLen + rightLen);
        String spaces = StringFunctions.repeat(' ', numSpaces);

        return String.format("%s%s%s", left, spaces, right);
    }

    /**
     * <p>Appropriately splits the given {@link String} if it is too long for the receipt line.</p>
     *
     * @param maxCharsPerLine The maximum number of characters that should appear on a line on the receipt.
     * @param s The {@link String} to check.
     * @return An {@link ArrayList} of {@link String} corresponding to the new receipt lines of the intelligently formatted {@link String} passed tio this method.
     */
    public static ArrayList<String> fixWordWrap (int maxCharsPerLine, String s) {
        ArrayList<String> newLines = new ArrayList<>();

        try {
            // keep track of formatting for the line
            boolean isCentered = false;
            boolean isRightAligned = false;
            boolean isLargeFont = false;
            boolean isModifierLine = false;

            if (StringFunctions.stringHasContent(s)) {
                if (!DataFunctions.isEmptyCollection(RabinKarp.search(JPOS_COMMAND.ALIGN_CENTER, s, 37))) {
                    isCentered = true;
                }
                if (!DataFunctions.isEmptyCollection(RabinKarp.search(JPOS_COMMAND.ALIGN_RIGHT, s, 37))) {
                    isRightAligned = true;
                }
                if (!DataFunctions.isEmptyCollection(RabinKarp.search(JPOS_COMMAND.LARGE_FONT, s, 37))) {
                    isLargeFont = true;
                }
                if (s.startsWith("\t")) {
                    isModifierLine = true;
                }

                // remove JPOS commands so that they don't contribute to the length of the receipt line
                String unformattedLine = removeJPOSCommands(s);

                if (isModifierLine) {
                    unformattedLine = unformattedLine.replaceAll("\\t", "");
                }

                int unformattedLineLen = (StringFunctions.stringHasContent(unformattedLine) ? unformattedLine.length() : 0);

                // the String will fit on the line, add the formatting back in
                if (((isModifierLine) && (unformattedLineLen + TAB_CHARS <= maxCharsPerLine))) {
                    unformattedLine = addJPOSCommands(unformattedLine, isCentered, isRightAligned, isLargeFont);
                    newLines.add("\t"+unformattedLine);
                }
                // the String will fit on the line, add the formatting back in
                else if ((!isModifierLine) && (unformattedLineLen <= maxCharsPerLine)) {
                    unformattedLine = addJPOSCommands(unformattedLine, isCentered, isRightAligned, isLargeFont);
                    newLines.add(unformattedLine);
                }
                // the String won't fit on the line, it needs to be split appropriately
                else {
                    // split the receipt line into words
                    String[] words = unformattedLine.split("\\s+", -1);
                    ArrayList<String> receiptLineSplit = smartSplit(maxCharsPerLine, words, isModifierLine);
                    if (!DataFunctions.isEmptyCollection(receiptLineSplit)) {
                        // add the JPOS commands back in for the line
                        for (String line : receiptLineSplit) {
                            // remove any explicitly defined new lines
                            String formattedLine = line.replaceAll("(\\\\n)", "");
                            newLines.add(addJPOSCommands(formattedLine, isCentered, isRightAligned, isLargeFont));
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to fix the word wrap for the String %s in KitchenReceiptCommon.fixWordWrap",
                    Objects.toString(s, "N/A")), Logger.LEVEL.ERROR);
        }

        return newLines;
    }

    /**
     * <p>Utility method to intelligently split up the given {@link String} array of words so that length of a line on the
     * receipt doesn't surpass the maximum number of characters that can be on a line on the receipt</p>
     *
     * @param maxCharsPerLine The maximum number of characters that can be on a line on the receipt.
     * @param words The {@link String} array of words to split into the receipt lines.
     * @param isModifierLine Whether or not this line is for a modifier on the receipt.
     * @return An {@link ArrayList} of {@link String} that corresponds to the lines on the receipt which have been intelligently split.
     */
    private static ArrayList<String> smartSplit (int maxCharsPerLine, String[] words, boolean isModifierLine) {
        ArrayList<String> splitLines = new ArrayList<>();

        try {
            // remove any words that are just empty Strings
            words = DataFunctions.removeEmptyStringsFromStringArr(words);

            if (!DataFunctions.isEmptyGenericArr(words)) {
                if (isModifierLine) {
                    String currentLine = "";
                    for (int i = 0; i < words.length; i++) {
                        // add the first word to the current line
                        if (i == 0) {
                            currentLine += "\t"+words[i]+" ";
                        }
                        else {
                            // make sure adding the word doesn't make the current line too long, if it does then start a new line
                            if (TAB_CHARS + currentLine.length() + words[i].length() + " ".length() <= maxCharsPerLine) {
                                // add the word to the current line
                                currentLine += words[i]+" ";
                            }
                            else {
                                // add the last line
                                splitLines.add(currentLine);
                                // create a new line
                                currentLine = "\t"+words[i]+" ";
                            }
                            // if we reached the last word then add the current line
                            if (i == words.length - 1) {
                                splitLines.add(currentLine);
                            }
                        }
                    }
                }
                else {
                    String currentLine = "";
                    for (int i = 0; i < words.length; i++) {
                        // add the first word to the current line
                        if (i == 0) {
                            currentLine += words[i]+" ";
                        }
                        else {
                            // make sure adding the word doesn't make the current line too long, if it does then start a new line
                            if (currentLine.length() + words[i].length() + " ".length() <= maxCharsPerLine) {
                                // add the word to the current line
                                currentLine += words[i]+" ";
                            }
                            else {
                                // add the last line
                                splitLines.add(currentLine);
                                // create a new line
                                currentLine = words[i]+" ";
                            }
                            // if we reached the last word then add the current line
                            if (i == words.length - 1) {
                                splitLines.add(currentLine);
                            }
                        }
                    }
                }
            }

        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to intelligently split the words for the receipt line in KitchenReceiptCommon.smartSplit", Logger.LEVEL.ERROR);
        }

        return splitLines;
    }

}