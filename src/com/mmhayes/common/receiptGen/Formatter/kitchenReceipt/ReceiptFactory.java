package com.mmhayes.common.receiptGen.Formatter.kitchenReceipt;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-27 16:48:53 -0400 (Wed, 27 May 2020) $: Date of last commit
    $Rev: 11826 $: Revision of last commit
    Notes: Creates an instance of an IReceipt.
*/

import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.Objects;

/**
 * <p>Creates an instance of an {@link IReceipt}.</p>
 *
 */
public class ReceiptFactory {

    /**
     * <p>Gets a receipt of the given kitchen receipt type.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kitchenReceiptType The type of kitchen receipt to get an {@link IReceipt} instance for.
     * @return The {@link IReceipt} instance of the given kitchen receipt type.
     */
    public IReceipt getReceipt (String log, String kitchenReceiptType) {
        IReceipt receipt = null;

        try {
            if (!StringFunctions.stringHasContent(kitchenReceiptType)) {
                Logger.logMessage("The kitchen receipt type passed to ReceiptFactory.getReceipt can't be null or empty!", log, Logger.LEVEL.ERROR);
                return null;
            }

            switch (kitchenReceiptType) {
                case TypeData.KitchenReceiptType.KP:
                    receipt = new KPReceipt();
                    break;
                case TypeData.KitchenReceiptType.KDS:
                    receipt = new KDSReceipt();
                    break;
                case TypeData.KitchenReceiptType.KMS:
                    receipt = new KMSReceipt();
                    break;
                default:
                    Logger.logMessage(String.format("Encountered an unknown kitchen receipt type of %s in ReceiptFactory.getReceipt",
                            Objects.toString(kitchenReceiptType, "N/A")), log, Logger.LEVEL.ERROR);
                    break;
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to get the receipt of the given kitchen receipt type in ReceiptFactory.getReceipt", log, Logger.LEVEL.ERROR);
        }

        return receipt;
    }

}