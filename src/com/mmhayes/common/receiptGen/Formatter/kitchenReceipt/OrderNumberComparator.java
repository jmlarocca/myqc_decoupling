package com.mmhayes.common.receiptGen.Formatter.kitchenReceipt;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-28 13:04:26 -0400 (Thu, 28 May 2020) $: Date of last commit
    $Rev: 11829 $: Revision of last commit
    Notes: Compares two order number Strings to determine which is larger..
*/

import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.util.*;

/**
 * <p>Compares two order number {@link String} to determine which is larger.</p>
 *
 */
public class OrderNumberComparator implements Comparator<String> {

    /**
     * <p>Compares given Strings to determine which is larger.</p>
     *
     * @param s1 A {@link String} to compare.
     * @param s2 A {@link String} to compare.
     * @return The comparison result.
     */
    @Override
    public int compare (String s1, String s2) {
        if ((StringFunctions.stringHasContent(s1)) && (StringFunctions.stringHasContent(s2)) && (NumberUtils.isNumber(s1)) && (NumberUtils.isNumber(s2))) {
            int s1Int = Integer.parseInt(s1);
            int s2Int = Integer.parseInt(s2);
            if (s1Int > s2Int) {
                return 1;
            }
            else if (s1Int == s2Int) {
                return 0;
            }
        }
        return -1;
    }

}