package com.mmhayes.common.receiptGen.Formatter.kitchenReceipt;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-27 16:48:53 -0400 (Wed, 27 May 2020) $: Date of last commit
    $Rev: 11826 $: Revision of last commit
    Notes: Interface that should be implemented by all kitchen receipt types.
*/

import com.mmhayes.common.receiptGen.ReceiptData.ReceiptData;
import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.detail.IPrintJobDetail;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;

import java.util.ArrayList;

/**
 * <p>Interface that should be implemented by all kitchen receipt types.</p>
 *
 */
public interface IReceipt {

    /**
     * <p>Builds a receipt for KP/KDS/KMS.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param renderer The {@link IRenderer} to be used to render the receipt.
     * @param receiptData The {@link ReceiptData} to use to build the receipt.
     * @param printJobDetails An {@link ArrayList} of {@link IPrintJobDetail} that should appear on the receipt.
     * @param hasPrintedOrDisplayedProducts Whether or not there are products that have already been printed or displayed within the transaction.
     * @param hasUnprintedOrUndisplayedProducts Whether or not there products that haven't been printed or displayed within the transaction.
     * @param hasUnprintedOrUndisplayedExpeditorProducts Whether or not there are expeditor products that haven't been printed or displayed within the transaction.
     * @param hasProductsFromPreviousOrder Whether or not there are products from the previous order within the transaction.
     * @param hasProductsFromCurrentOrder Whether or not there are products from the current order within the transaction.
     */
    void buildReceipt(String log,
                      IRenderer renderer,
                      ReceiptData receiptData,
                      ArrayList<IPrintJobDetail> printJobDetails,
                      boolean hasPrintedOrDisplayedProducts,
                      boolean hasUnprintedOrUndisplayedProducts,
                      boolean hasUnprintedOrUndisplayedExpeditorProducts,
                      boolean hasProductsFromPreviousOrder,
                      boolean hasProductsFromCurrentOrder);

}