package com.mmhayes.common.receiptGen.Formatter.kitchenReceipt;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-27 16:48:53 -0400 (Wed, 27 May 2020) $: Date of last commit
    $Rev: 11826 $: Revision of last commit
    Notes: Represents a receipt that will be displayed on the kitchen display system.
*/

import com.mmhayes.common.receiptGen.ReceiptData.ReceiptData;
import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.detail.IPrintJobDetail;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;

import java.util.ArrayList;

/**
 * <p>Represents a receipt that will be printed on the kitchen display system.</p>
 *
 */
public class KDSReceipt implements IReceipt {

    @Override
    public void buildReceipt (String log,
                              IRenderer renderer,
                              ReceiptData receiptData,
                              ArrayList<IPrintJobDetail> printJobDetails,
                              boolean hasPrintedOrDisplayedProducts,
                              boolean hasUnprintedOrUndisplayedProducts,
                              boolean hasUnprintedOrUndisplayedExpeditorProducts,
                              boolean hasProductsFromPreviousOrder,
                              boolean hasProductsFromCurrentOrder) {
        // TODO
    }

}