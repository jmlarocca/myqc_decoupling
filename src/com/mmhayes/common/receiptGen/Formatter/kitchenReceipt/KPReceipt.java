package com.mmhayes.common.receiptGen.Formatter.kitchenReceipt;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-29 16:39:47 -0400 (Fri, 29 May 2020) $: Date of last commit
    $Rev: 11836 $: Revision of last commit
    Notes: Represents a receipt that will be printed on kitchen printers.
*/

import com.mmhayes.common.receiptGen.ReceiptData.KPJobDetail;
import com.mmhayes.common.receiptGen.ReceiptData.ReceiptData;
import com.mmhayes.common.receiptGen.ReceiptData.TransactionData;
import com.mmhayes.common.receiptGen.ReceiptData.TransactionHeaderData;
import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.detail.IPrintJobDetail;
import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.detail.KPPrintJobDetail;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.Product;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.*;

/**
 * <p>Represents a receipt that will be printed on kitchen printers.</p>
 *
 */
public class KPReceipt implements IReceipt {

    /**
     * <p>Constructs the details for a receipt that will be sent to a kitchen printer.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param renderer The {@link IRenderer} to be used to render the receipt.
     * @param receiptData The {@link ReceiptData} to use to build the receipt.
     * @param printJobDetails An {@link ArrayList} of {@link IPrintJobDetail} that should appear on the receipt.
     * @param hasPrintedOrDisplayedProducts Whether or not there are products that have already been printed or displayed within the transaction.
     * @param hasUnprintedOrUndisplayedProducts Whether or not there products that haven't been printed or displayed within the transaction.
     * @param hasUnprintedOrUndisplayedExpeditorProducts Whether or not there are expeditor products that haven't been printed or displayed within the transaction.
     * @param hasProductsFromPreviousOrder Whether or not there are products from the previous order within the transaction.
     * @param hasProductsFromCurrentOrder Whether or not there are products from the current order within the transaction.
     */
    @Override
    public void buildReceipt (String log,
                              IRenderer renderer,
                              ReceiptData receiptData,
                              ArrayList<IPrintJobDetail> printJobDetails,
                              boolean hasPrintedOrDisplayedProducts,
                              boolean hasUnprintedOrUndisplayedProducts,
                              boolean hasUnprintedOrUndisplayedExpeditorProducts,
                              boolean hasProductsFromPreviousOrder,
                              boolean hasProductsFromCurrentOrder) {

        try {
            // make sure we have a valid renderer
            if (renderer == null) {
                Logger.logMessage("The renderer passed to KPReceipt.buildReceipt can't be null, unable to build the receipt!", log, Logger.LEVEL.ERROR);
                return;
            }

            // make sure we have valid ReceiptData
            if (receiptData == null) {
                Logger.logMessage("The ReceiptData passed to KPReceipt.buildReceipt can't be null, unable to build the receipt!", log, Logger.LEVEL.ERROR);
                return;
            }

            // make sure we have print job details
            if (DataFunctions.isEmptyCollection(printJobDetails)) {
                Logger.logMessage("No print job details found in KPReceipt.buildReceipt, unable to build the receipt!", log, Logger.LEVEL.ERROR);
                return;
            }

            // sort the print job details into the order they will appear on the receipt
            printJobDetails = sortKPPrintJobDetails(log, receiptData, printJobDetails);

            if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                // this ArrayList will store our new print job details
                ArrayList<IPrintJobDetail> newPrintJobDetails = new ArrayList<>();
                // TODO ADD THE PRINT JOB HEADER DETAILS
                // TODO ADD THE PRINT JOB DETAILS
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // get the ID of the printer we are building the receipt for
            int printerID = 0;
            if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                // we know the IPrintJobDetail is an instance of a KPJobDetail since we are building a KP receipt
                KPJobDetail kpJobDetail = ((KPJobDetail) printJobDetails.get(0));
                printerID = kpJobDetail.getPrinterID();
            }
            Logger.logMessage(String.format("There was a problem trying to build the receipt for the printer with an ID of %s in KPReceipt.buildReceipt",
                    Objects.toString((printerID != 0 ? printerID : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Sorts the print job details so that they appear in the correct order on the receipt.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param receiptData The {@link ReceiptData} to use to build the receipt.
     * @param printJobDetails An {@link ArrayList} of {@link IPrintJobDetail} that should appear on the receipt.
     * @return An {@link ArrayList} of {@link IPrintJobDetail} corresponding to the sorted print job details.
     */
    private ArrayList<IPrintJobDetail> sortKPPrintJobDetails (String log, ReceiptData receiptData, ArrayList<IPrintJobDetail> printJobDetails) {
        ArrayList<IPrintJobDetail> sortedPrintJobDetails = new ArrayList<>();

        try {
            // make sure we have valid ReceiptData
            if (receiptData == null) {
                Logger.logMessage("The ReceiptData passed to KPReceipt.sortKPPrintJobDetails can't be null, unable to sort the print job details!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have print job details
            if (DataFunctions.isEmptyCollection(printJobDetails)) {
                Logger.logMessage("No print job details found in KPReceipt.sortKPPrintJobDetails, unable to sort the print job details!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // sort the print job details as being printed or unprinted
            HashMap<String, ArrayList<IPrintJobDetail>> printJobDetailsBySimplePrintStatus = sortPrintJobDetailsBySimplePrintStatus(log, receiptData, printJobDetails);
            ArrayList<IPrintJobDetail> unprintedPrintJobDetails = new ArrayList<>();
            ArrayList<IPrintJobDetail> printedPrintJobDetails = new ArrayList<>();
            if (!DataFunctions.isEmptyMap(printJobDetailsBySimplePrintStatus)) {
                if ((printJobDetailsBySimplePrintStatus.containsKey(TypeData.SimplePrintStatus.UNPRINTED))
                        && (!DataFunctions.isEmptyCollection(printJobDetailsBySimplePrintStatus.get(TypeData.SimplePrintStatus.UNPRINTED)))) {
                    unprintedPrintJobDetails = printJobDetailsBySimplePrintStatus.get(TypeData.SimplePrintStatus.UNPRINTED);
                }

                if ((printJobDetailsBySimplePrintStatus.containsKey(TypeData.SimplePrintStatus.PRINTED))
                        && (!DataFunctions.isEmptyCollection(printJobDetailsBySimplePrintStatus.get(TypeData.SimplePrintStatus.PRINTED)))) {
                    printedPrintJobDetails = printJobDetailsBySimplePrintStatus.get(TypeData.SimplePrintStatus.PRINTED);
                }
            }

            // sort the unprinted print job details by descending order number
            if (!DataFunctions.isEmptyCollection(unprintedPrintJobDetails)) {
                unprintedPrintJobDetails = sortPrintJobDetailsByOrderNumber(log, receiptData, unprintedPrintJobDetails);
            }

            // sort the printed print job details by descending order number
            if (!DataFunctions.isEmptyCollection(printedPrintJobDetails)) {
                printedPrintJobDetails = sortPrintJobDetailsByOrderNumber(log, receiptData, printedPrintJobDetails);
            }

            // add the print job details to the sorted print job details
            if (!DataFunctions.isEmptyCollection(unprintedPrintJobDetails)) {
                sortedPrintJobDetails.addAll(unprintedPrintJobDetails);
            }
            if (!DataFunctions.isEmptyCollection(printedPrintJobDetails)) {
                sortedPrintJobDetails.addAll(printedPrintJobDetails);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // get the ID of the printer we are building the receipt for
            int printerID = 0;
            if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                // we know the IPrintJobDetail is an instance of a KPJobDetail since we are building a KP receipt
                KPJobDetail kpJobDetail = ((KPJobDetail) printJobDetails.get(0));
                printerID = kpJobDetail.getPrinterID();
            }
            Logger.logMessage(String.format("There was a problem trying to sort the print job details for a receipt that will be sent " +
                    "to the printer with an ID of %s in KPReceipt.sortKPPrintJobDetails",
                    Objects.toString((printerID != 0 ? printerID : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return sortedPrintJobDetails;
    }

    /**
     * <p>Sorts the print job details based on whether or not they have or haven't been printed yet.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param receiptData The {@link ReceiptData} to use to build the receipt.
     * @param printJobDetails An {@link ArrayList} of {@link IPrintJobDetail} that should appear on the receipt.
     * @return A {@link HashMap} whose key a {@link String} designating the print job as being "PRINTED" or "UNPRINTED" and whose value is
     * an {@link ArrayList} of {@link IPrintJobDetail} that are either unprinted or printed.
     */
    private HashMap<String, ArrayList<IPrintJobDetail>> sortPrintJobDetailsBySimplePrintStatus (String log, ReceiptData receiptData, ArrayList<IPrintJobDetail> printJobDetails) {
        HashMap<String, ArrayList<IPrintJobDetail>> sortedPrintJobDetails = new HashMap<>();
        ArrayList<IPrintJobDetail> commonSimpleStatusPrintJobDetails;

        try {
            // make sure we have print job details
            if (DataFunctions.isEmptyCollection(printJobDetails)) {
                Logger.logMessage("No print job details found in KPReceipt.sortPrintJobDetailsBySimplePrintStatus, unable to sort the " +
                        "print job details based on whether or not they have been printed!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have valid ReceiptData
            if (receiptData == null) {
                Logger.logMessage("The ReceiptData passed to KPReceipt.sortPrintJobDetailsBySimplePrintStatus can't be null, unable to sort the print job details!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have valid TransactionData
            if (receiptData.getTxnData() == null) {
                Logger.logMessage("The TransactionData within the ReceiptData passed to KPReceipt.sortPrintJobDetailsBySimplePrintStatus can't be null, " +
                        "unable to sort the print job details!", log, Logger.LEVEL.ERROR);
                return null;
            }

            TransactionData transactionData = receiptData.getTxnData();

            // iterate through the print job details
            for (IPrintJobDetail printJobDetail : printJobDetails) {
                KPPrintJobDetail kpPrintJobDetail = ((KPPrintJobDetail) printJobDetail);
                // get the product contained within the print job detail
                Product product = transactionData.getProductFromLineItemID(kpPrintJobDetail.getTransLineItemID());
                String simplePrintStatus = TypeData.SimplePrintStatus.UNPRINTED;
                if ((product != null) && (product.hasBeenPrintedOrDisplayed())) {
                    simplePrintStatus = TypeData.SimplePrintStatus.PRINTED;
                }

                // add the print job detail to the list of printed or unprinted print hib details
                if (sortedPrintJobDetails.containsKey(simplePrintStatus)) {
                    commonSimpleStatusPrintJobDetails = sortedPrintJobDetails.get(simplePrintStatus);
                }
                else {
                    commonSimpleStatusPrintJobDetails = new ArrayList<>();
                }
                commonSimpleStatusPrintJobDetails.add(printJobDetail);
                sortedPrintJobDetails.put(simplePrintStatus, commonSimpleStatusPrintJobDetails);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to sort the print job details based on whether or not they have been printed in KPReceipt.sortPrintJobDetailsBySimplePrintStatus", log, Logger.LEVEL.ERROR);
        }

        return sortedPrintJobDetails;
    }

    /**
     * <p>Sorts the given print job details by descending order number.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param receiptData The {@link ReceiptData} to use to build the receipt.
     * @param printJobDetails An {@link ArrayList} of {@link IPrintJobDetail} that should appear on the receipt.
     * @return An {@link ArrayList} of {@link IPrintJobDetail} corrsponding the the print job details sorted by descending order number.
     */
    @SuppressWarnings("Convert2streamapi")
    private ArrayList<IPrintJobDetail> sortPrintJobDetailsByOrderNumber (String log, ReceiptData receiptData, ArrayList<IPrintJobDetail> printJobDetails) {
        ArrayList<IPrintJobDetail> sortedPrintJobDetails = new ArrayList<>();

        try {
            // make sure we have print job details
            if (DataFunctions.isEmptyCollection(printJobDetails)) {
                Logger.logMessage("No print job details found in KPReceipt.sortPrintJobDetailsByOrderNumber, unable to sort the " +
                        "print job details based on whether or not they have been printed!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have valid ReceiptData
            if (receiptData == null) {
                Logger.logMessage("The ReceiptData passed to KPReceipt.sortPrintJobDetailsByOrderNumber can't be null, unable to sort the print job details!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have valid TransactionData
            if (receiptData.getTxnData() == null) {
                Logger.logMessage("The TransactionData within the ReceiptData passed to KPReceipt.sortPrintJobDetailsByOrderNumber can't be null, " +
                        "unable to sort the print job details!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have valid TransactionHeaderData
            if (receiptData.getTxnData().getTxnHdrData() == null) {
                Logger.logMessage("The TransactionDataHeaderData within the TransactionData within the ReceiptData passed to KPReceipt.sortPrintJobDetailsByOrderNumber can't be null, " +
                        "unable to sort the print job details!", log, Logger.LEVEL.ERROR);
                return null;
            }

            TransactionData transactionData = receiptData.getTxnData();
            TransactionHeaderData transactionHeaderData = transactionData.getTxnHdrData();

            String transactionOrderNumber = transactionHeaderData.getOrderNum();

            HashMap<String, ArrayList<IPrintJobDetail>> printJobDetailsByOrderNumber = new HashMap<>();
            ArrayList<IPrintJobDetail> commonOrderNumberPrintJobDetails;
            for (IPrintJobDetail printJobDetail : printJobDetails) {
                KPPrintJobDetail kpPrintJobDetail = ((KPPrintJobDetail) printJobDetail);
                // get the product contained within the print job detail
                Product product = transactionData.getProductFromLineItemID(kpPrintJobDetail.getTransLineItemID());
                if (product != null) {
                    String originalOrderNumber = product.getOriginalOrderNumber();
                    String orderNumber = (StringFunctions.stringHasContent(originalOrderNumber) ? originalOrderNumber : transactionOrderNumber);
                    orderNumber = (orderNumber.contains("-") ? orderNumber.replaceAll("-", "") : orderNumber);
                    // sort the print job details
                    if (printJobDetailsByOrderNumber.containsKey(orderNumber)) {
                        commonOrderNumberPrintJobDetails = printJobDetailsByOrderNumber.get(orderNumber);
                    }
                    else {
                        commonOrderNumberPrintJobDetails = new ArrayList<>();
                    }
                    commonOrderNumberPrintJobDetails.add(printJobDetail);
                    printJobDetailsByOrderNumber.put(orderNumber, commonOrderNumberPrintJobDetails);
                }
            }

            // make sure if the print job detail is for a dining option that it is listed first for the order number
            if (!DataFunctions.isEmptyMap(printJobDetailsByOrderNumber)) {
                for (Map.Entry<String, ArrayList<IPrintJobDetail>> printJobDetailsByOrderNumberEntry : printJobDetailsByOrderNumber.entrySet()) {
                    int diningOptionIndex = 0;
                    ArrayList<IPrintJobDetail> printJobDetailsForOrderNumber = printJobDetailsByOrderNumberEntry.getValue();
                    if (!DataFunctions.isEmptyCollection(printJobDetailsForOrderNumber)) {
                        for (IPrintJobDetail printJobDetail : printJobDetails) {
                            KPPrintJobDetail kpPrintJobDetail = ((KPPrintJobDetail) printJobDetail);
                            // get the product contained within the print job detail
                            Product product = transactionData.getProductFromLineItemID(kpPrintJobDetail.getTransLineItemID());
                            if (product.getIsDiningOption()) {
                                break; // dining option found, no need to keep processing
                            }
                            diningOptionIndex++;
                        }

                        // if there is a dining option then remove it and append it to the beginning of the list
                        if (diningOptionIndex > 0) {
                            IPrintJobDetail diningOptionPrintJobDetail = printJobDetailsForOrderNumber.remove(diningOptionIndex);
                            printJobDetailsForOrderNumber.add(0, diningOptionPrintJobDetail);
                        }
                    }
                }
            }

            // sort the print job details by descending order number
            NavigableMap<String, ArrayList<IPrintJobDetail>> descendingOrderNumberPrintJobDetails = null;
            if (!DataFunctions.isEmptyMap(printJobDetailsByOrderNumber)) {
                // transform the HashMap to a TreeMap so we can sort by the keys, use our custom OrderNumberComparator to do the sorting
                TreeMap<String, ArrayList<IPrintJobDetail>> sortedPrintJobDetailsByOrderNumber = new TreeMap<>(new OrderNumberComparator());
                sortedPrintJobDetailsByOrderNumber.putAll(printJobDetailsByOrderNumber);
                // reverse the TreeMap which will have all our print job details sorted by descending order number
                descendingOrderNumberPrintJobDetails = sortedPrintJobDetailsByOrderNumber.descendingMap();
            }

            // add the sorted print job details from the NavigableMap
            if ((!DataFunctions.isEmptyMap(descendingOrderNumberPrintJobDetails)) && (!DataFunctions.isEmptyCollection(descendingOrderNumberPrintJobDetails.values()))) {
                for (ArrayList<IPrintJobDetail> printJobDetailsWithCommonOrderNumber : descendingOrderNumberPrintJobDetails.values()) {
                    if (!DataFunctions.isEmptyCollection(printJobDetailsWithCommonOrderNumber)) {
                        sortedPrintJobDetails.addAll(printJobDetailsWithCommonOrderNumber);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to sort the print job details based on order number in KPReceipt.sortPrintJobDetailsByOrderNumber", log, Logger.LEVEL.ERROR);
        }

        return sortedPrintJobDetails;
    }

}