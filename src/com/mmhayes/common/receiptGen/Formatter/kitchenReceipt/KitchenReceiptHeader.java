package com.mmhayes.common.receiptGen.Formatter.kitchenReceipt;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-29 16:39:47 -0400 (Fri, 29 May 2020) $: Date of last commit
    $Rev: 11836 $: Revision of last commit
    Notes: Represents a header on a receipt that will be sent to KP/KDS/KMS.
*/

import com.mmhayes.common.receiptGen.ReceiptData.ReceiptData;

/**
 * <p>Represents a header on a receipt that will be sent to KP/KDS/KMS.</p>
 *
 */
public class KitchenReceiptHeader {

    // private member variables of a KitchenReceiptHeader
    private ReceiptData receiptData = null;
    private String transDate = "";
    private String transTime = "";
    private String cashierName = "";
    private String terminalID = "";
    private int transTypeID = 0;
    private String transTypeName = "";
    private String paTransactionID = "";
    private boolean useAddOn = false;
    private String printerOrStationName = "";
    private String orderNumber = "";
    private int orderTypeID = 0;
    private String personName = "";
    private String phone = "";
    private String pickupDeliveryNote = "";
    private String deliveryLocation = "";
    private String transName = "";
    private String transComment = "";

    /**
     * <p>Constructor for a {@link KitchenReceiptHeader}.</p>
     *
     * @param receiptData The {@link ReceiptData} to use to build the receipt.
     */
    public KitchenReceiptHeader (ReceiptData receiptData) {
        this.receiptData = receiptData;
    }

    /**
     * <p>Sets the transaction date {@link String} for the {@link KitchenReceiptHeader}</p>
     *
     * @param transDate The date {@link String} of when the transaction took place.
     * @return The {@link KitchenReceiptHeader} with it's transDate property set.
     */
    public KitchenReceiptHeader addTransactionDate (String transDate) {
        this.transDate = transDate;
        return this;
    }

    /**
     * <p>Sets the transaction time {@link String} for the {@link KitchenReceiptHeader}</p>
     *
     * @param transTime The time {@link String} of when the transaction took place.
     * @return The {@link KitchenReceiptHeader} with it's transTime property set.
     */
    public KitchenReceiptHeader addTransactionTime (String transTime) {
        this.transTime = transTime;
        return this;
    }

    /**
     * <p>Sets the name {@link String} of the cashier for the {@link KitchenReceiptHeader}</p>
     *
     * @param cashierName The name {@link String} of the cashier.
     * @return The {@link KitchenReceiptHeader} with it's cashierName property set.
     */
    public KitchenReceiptHeader addCashierName (String cashierName) {
        this.cashierName = cashierName;
        return this;
    }

    /**
     * <p>Sets the ID {@link String} of the terminal the transaction took place on for the {@link KitchenReceiptHeader}</p>
     *
     * @param terminalID ID {@link String} of the terminal the transaction took place on.
     * @return The {@link KitchenReceiptHeader} with it's terminalID property set.
     */
    public KitchenReceiptHeader addTerminalID (String terminalID) {
        this.terminalID = terminalID;
        return this;
    }

    /**
     * <p>Sets the ID of the type of transaction for the {@link KitchenReceiptHeader}</p>
     *
     * @param transTypeID ID of the type of transaction.
     * @return The {@link KitchenReceiptHeader} with it's transTypeID property set.
     */
    public KitchenReceiptHeader addTransTypeID (int transTypeID) {
        this.transTypeID = transTypeID;
        return this;
    }

    /**
     * <p>Sets the name {@link String} of the type of transaction for the {@link KitchenReceiptHeader}</p>
     *
     * @param transTypeName Name {@link String} of the type of transaction.
     * @return The {@link KitchenReceiptHeader} with it's transTypeName property set.
     */
    public KitchenReceiptHeader addTransTypeName (String transTypeName) {
        this.transTypeName = transTypeName;
        return this;
    }

    /**
     * <p>Sets the ID {@link String} of the transaction for the {@link KitchenReceiptHeader}</p>
     *
     * @param paTransactionID ID {@link String} of the transaction.
     * @return The {@link KitchenReceiptHeader} with it's paTransactionID property set.
     */
    public KitchenReceiptHeader addPATransactionID (String paTransactionID) {
        this.paTransactionID = paTransactionID;
        return this;
    }

    /**
     * <p>Whether or not to add an "ADD ON" header for the {@link KitchenReceiptHeader}</p>
     *
     * @param useAddOn Whether or not to add an "ADD ON" header.
     * @return The {@link KitchenReceiptHeader} with it's useAddOn property set.
     */
    public KitchenReceiptHeader useAddOn (boolean useAddOn) {
        this.useAddOn = useAddOn;
        return this;
    }

    /**
     * <p>Sets the name {@link String} of printer or station for the {@link KitchenReceiptHeader}</p>
     *
     * @param printerOrStationName The name {@link String} of printer or station.
     * @return The {@link KitchenReceiptHeader} with it's printerOrStationName property set.
     */
    public KitchenReceiptHeader addPrinterOrStationName (String printerOrStationName) {
        this.printerOrStationName = printerOrStationName;
        return this;
    }

    /**
     * <p>Sets the order number {@link String} for the {@link KitchenReceiptHeader}</p>
     *
     * @param orderNumber The order number {@link String}.
     * @return The {@link KitchenReceiptHeader} with it's orderNumber property set.
     */
    public KitchenReceiptHeader addOrderNumber (String orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    /**
     * <p>Sets the ID {@link String} of the type of order for the {@link KitchenReceiptHeader}</p>
     *
     * @param orderTypeID ID {@link String} of the type of order.
     * @return The {@link KitchenReceiptHeader} with it's orderTypeID property set.
     */
    public KitchenReceiptHeader addOrderTypeID (int orderTypeID) {
        this.orderTypeID = orderTypeID;
        return this;
    }

    /**
     * <p>Sets the name {@link String} of the person who placed the order for the {@link KitchenReceiptHeader}</p>
     *
     * @param personName The name {@link String} of the person who placed the order.
     * @return The {@link KitchenReceiptHeader} with it's personName property set.
     */
    public KitchenReceiptHeader addPersonName (String personName) {
        this.personName = personName;
        return this;
    }

    /**
     * <p>Sets the phone number {@link String} of the person who placed the order for the {@link KitchenReceiptHeader}</p>
     *
     * @param phone The phone number {@link String} of the person who placed the order.
     * @return The {@link KitchenReceiptHeader} with it's phone property set.
     */
    public KitchenReceiptHeader addPhoneNumber (String phone) {
        this.phone = phone;
        return this;
    }

    /**
     * <p>Sets a comment {@link String} related to pickup or delivery of the order for the {@link KitchenReceiptHeader}</p>
     *
     * @param pickupDeliveryNote A comment {@link String} related to pickup or delivery of the order.
     * @return The {@link KitchenReceiptHeader} with it's pickupDeliveryNote property set.
     */
    public KitchenReceiptHeader addPickupDeliveryNote (String pickupDeliveryNote) {
        this.pickupDeliveryNote = pickupDeliveryNote;
        return this;
    }

    /**
     * <p>Sets the location {@link String} where the order should be delivered for the {@link KitchenReceiptHeader}</p>
     *
     * @param deliveryLocation The location {@link String} where the order should be delivered.
     * @return The {@link KitchenReceiptHeader} with it's deliveryLocation property set.
     */
    public KitchenReceiptHeader addDeliveryLocation (String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
        return this;
    }

    /**
     * <p>Sets the name {@link String} of the transaction for the {@link KitchenReceiptHeader}</p>
     *
     * @param transName The name {@link String} of the transaction.
     * @return The {@link KitchenReceiptHeader} with it's transName property set.
     */
    public KitchenReceiptHeader addTransName (String transName) {
        this.transName = transName;
        return this;
    }

    /**
     * <p>Sets a comment {@link String} that applies to the transaction for the {@link KitchenReceiptHeader}</p>
     *
     * @param transComment A comment {@link String} that applies to the transaction.
     * @return The {@link KitchenReceiptHeader} with it's transComment property set.
     */
    public KitchenReceiptHeader addTransComment (String transComment) {
        this.transComment = transComment;
        return this;
    }

}