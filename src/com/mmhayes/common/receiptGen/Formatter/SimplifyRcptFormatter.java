package com.mmhayes.common.receiptGen.Formatter;

import com.mmhayes.common.receiptGen.ReceiptData.FreedomPayReceiptData;
import com.mmhayes.common.receiptGen.ReceiptData.IPaymentProcessorReceiptData;
import com.mmhayes.common.receiptGen.ReceiptData.SimplifyReceiptData;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;

import java.util.ArrayList;

/**
 * Created by nyu on 12/2/2019.
 */
public class SimplifyRcptFormatter implements IPaymentProcessorRcptFormatter
{
    private static String logFileName = "PaymentProcessorRcpt.log";

    private IRenderer rcptRenderer;

    public SimplifyRcptFormatter()
    {
    }

    @Override
    public void setRenderer(IRenderer i)
    {
        rcptRenderer = i;
    }

    @Override
    public IRenderer getRenderer()
    {
        return rcptRenderer;
    }

    @Override
    public void logError(String error, Exception e)
    {
        ReceiptGen.logError(error, e);
    }

    @Override
    public boolean buildReceipt(IPaymentProcessorReceiptData rcptData)
    {
        buildReceipt(rcptData, false);
        return buildReceipt(rcptData, true);
    }

    public boolean buildReceipt(IPaymentProcessorReceiptData rcptData, boolean isMerchantCopy)
    {
        if(rcptRenderer == null)
        {
            return false;
        }

        if(rcptData == null)
        {
            return false;
        }

        if(!rcptRenderer.begin())
        {
            return false;
        }

        SimplifyReceiptData smpRcptData = (SimplifyReceiptData) rcptData;

        // Print receipt header
        ArrayList receiptHeader = smpRcptData.getReceiptHeaderLines();
        if(receiptHeader != null)
        {
            for (Object aReceiptHeader : receiptHeader)
            {
                printHeaderLine(aReceiptHeader.toString());
            }
        }
        rcptRenderer.addBlankLine();
        rcptRenderer.addBlankLine();
        rcptRenderer.add2ColumnLine(((SimplifyReceiptData) rcptData).getDate(), ((SimplifyReceiptData) rcptData).getTime());
        rcptRenderer.add2ColumnLine("Cashier: " + ((SimplifyReceiptData) rcptData).getCashierName(), "TID: " + ((SimplifyReceiptData) rcptData).getTerminalID());
        rcptRenderer.addOneColumnLineLeftAlign(((SimplifyReceiptData) rcptData).getTransTypeName());

        rcptRenderer.addBlankLine();

        rcptRenderer.addOneColumnLineRightAlign("TOTAL:   $" + ((SimplifyReceiptData) rcptData).getTotalAmount());
        rcptRenderer.addBlankLine();

        rcptRenderer.addOneColumnLineRightAlign(((SimplifyReceiptData) rcptData).getCardholderName());
        rcptRenderer.addOneColumnLineRightAlign(((SimplifyReceiptData) rcptData).getMaskedAcctNum());
        rcptRenderer.addOneColumnLineRightAlign("Ref #: " + ((SimplifyReceiptData) rcptData).getRefNum());
        rcptRenderer.addOneColumnLineRightAlign("Merchant #: " + ((SimplifyReceiptData) rcptData).getMerchantNum());
        rcptRenderer.addOneColumnLineRightAlign("Card Entry: " + ((SimplifyReceiptData) rcptData).getCardEntryMode());
        rcptRenderer.addOneColumnLineRightAlign("Sys Trace Audit #: " + ((SimplifyReceiptData) rcptData).getSysTraceAuditNum());
        printEMVDetails(((SimplifyReceiptData) rcptData).getEmvRcptDetails());

        rcptRenderer.addBlankLine();
        ArrayList receiptFooter = smpRcptData.getReceiptFooterLines();
        if(receiptFooter != null)
        {
            for (Object receiptFooterLine : receiptFooter)
            {
                printHeaderLine(receiptFooterLine.toString());
            }
        }
        rcptRenderer.addBlankLine();
        rcptRenderer.addBlankLine();

        if (isMerchantCopy)
        {
            rcptRenderer.centerLine("-----     MERCHANT COPY    -----");
        }
        else
        {
            rcptRenderer.centerLine("-----     CUSTOMER COPY    -----");
        }

        rcptRenderer.cutPaper();
        rcptRenderer.end();

        return true;
    }

    private boolean printHeaderLine(String headerLine)
    {
        rcptRenderer.centerLine(headerLine);

        return true;
    }

    private void printEMVDetails(String emvDetails)
    {
        String[] simplifyRcptDetails = emvDetails.split(",");

        for (String rcptDetail : simplifyRcptDetails)
        {
            rcptRenderer.addOneColumnLineRightAlign(rcptDetail);
        }
    }
}
