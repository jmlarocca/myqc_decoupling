package com.mmhayes.common.receiptGen.Formatter;

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.ReceiptData.FreedomPayReceiptData;
import com.mmhayes.common.receiptGen.ReceiptData.IPaymentProcessorReceiptData;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;

/**
 * Created by nyu on 6/26/2018.
 */
public class FreedomPayRcptFormatter implements IPaymentProcessorRcptFormatter
{
    private static String logFileName = "PaymentProcessorRcpt.log";

    private IRenderer rcptRenderer;

    public FreedomPayRcptFormatter()
    {
    }

    @Override
    public void setRenderer(IRenderer i)
    {
        rcptRenderer = i;
    }

    @Override
    public IRenderer getRenderer()
    {
        return rcptRenderer;
    }

    @Override
    public void logError(String error, Exception e)
    {
        ReceiptGen.logError(error, e);
    }

    @Override
    public boolean buildReceipt(IPaymentProcessorReceiptData rcptData)
    {
        if(rcptRenderer == null)
        {
            return false;
        }

        if(rcptData == null)
        {
            return false;
        }

        if(!rcptRenderer.begin())
        {
            return false;
        }

        FreedomPayReceiptData fpRcptData = (FreedomPayReceiptData) rcptData;

        rcptRenderer.addOneColumnLineLeftAlign(fpRcptData.getReceiptContent());

        rcptRenderer.addBlankLine();
        rcptRenderer.cutPaper();
        rcptRenderer.end();

        return true;
    }
}
