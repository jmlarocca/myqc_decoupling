package com.mmhayes.common.receiptGen.Formatter;

import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.receiptGen.ReceiptData.ReceiptData;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TransactionData.*;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;

/*
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-09-29 16:12:36 -0400 (Tue, 29 Sep 2020) $: Date of last commit
 $Rev: 12755 $: Revision of last commit
 Notes: *ReceiptFormatter - controls what to display and where to display it
*/


public class GiftReceiptFormatter implements ICustomReceiptFormatter
{
    private static String logFileName = "ConsolidatedReceipt.log";
    private IRenderer rcptRenderer;
    @Override
    public void setRenderer(IRenderer i)
    {
        rcptRenderer = i;
    }

    @Override
    public IRenderer getRenderer()
    {
        return rcptRenderer;
    }

    @Override
    public void logError(String error, Exception e)
    {
        ReceiptGen.logError(error, e);
    }

    @Override
    public boolean buildReceipt(ReceiptData receiptData)
    {

        if ((receiptData != null) && (receiptData.getCustomerReceiptData() != null)) {
            // TODO BUILD THE RECEIPT USING THE CustomerReceiptData
            return true;
        }
        else {
            // we require a renderer
            if (rcptRenderer == null)
            {
                return false;
            }

            // if begin fails we should not continue
            if (!rcptRenderer.begin())
            {
                return false;
            }

            int itemCount = receiptData.getTxnData().getItemCount();

            // Print receipt header
            ArrayList receiptHeader = receiptData.getReceiptHeaders();
            if(receiptHeader != null)
            {
                for (Object aReceiptHeader : receiptHeader)
                {
                    printHeaderLine(aReceiptHeader.toString());
                }
            }

            // Print transaction header info
            printRcptIdInfo(receiptData);

            // Get transaction type ID
            int transactionTypeID = receiptData.getTxnData().getTransTypeID();

            // Get transaction details
            ArrayList transArgs = receiptData.getTxnData().getTransItems();
            // Loop through and add any missing modifiers to their parent products (POS sent receipts will not have them attached yet, DB created receipts already will)
            if (transArgs != null)
            {
                for (Object transItem : transArgs)
                {
                    if (transItem instanceof Plu)
                    {
                        Plu product = (Plu) transItem;
                        if (product.getIsModifier())
                        {
                            Plu targetParent = product.getParentProduct();
                            //Add this modifier to its parent product's mods (POS receipt)
                            if (targetParent != null)
                            {
                                //Find this parent plu in the list
                                for (Object txnObj : transArgs)
                                {
                                    if ( (txnObj instanceof Plu) && !(((Plu) txnObj).getIsModifier()) && (((Plu) txnObj).hasModifiers()) )
                                    {
                                        Plu parent = (Plu) txnObj;
                                        if (targetParent.getPATransLineItemID() == parent.getPATransLineItemID())
                                        {
                                            //Matched the parent, make sure this mod is in its list
                                            boolean foundMod = false;
                                            for (Plu modifierPlu : targetParent.getModifiers())
                                            {
                                                if (modifierPlu.getPaTransLineItemModID() == product.getPaTransLineItemModID())
                                                {
                                                    foundMod = true;
                                                    break;
                                                }
                                            }
                                            if (!foundMod)
                                            {
                                                //Add current product (modifier) to parent's modifier arraylist
                                                targetParent.addModifier(product);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Loop through and print each transaction detail
            if(transArgs != null)
            {
                for (Object transItem : transArgs)
                {
                    if (transItem instanceof Plu)
                    {
                        Plu product = (Plu) transItem;
                        printProductItem(product, transactionTypeID, receiptData.shouldPrintProductCodes());
                    }
                }
            }

            // Print item count
            printItemCount(itemCount, receiptData.getTxnData().getTransTypeID());

            // Print receipt footers
            ArrayList receiptFooters = receiptData.getReceiptFooters();
            if(receiptFooters != null)
            {
                for (int x = 0; x < receiptFooters.size(); x++)
                {
                    printFooterLine(receiptFooters.get(x).toString(), 0 == x, receiptFooters.size() == x+1);
                }
            }

            // Finish the receipt
            finishPrint();

            return true;
        }
    }

    //--------------------------------------------------------------------------
    //  Printing Transaction Header Methods
    //--------------------------------------------------------------------------
    public boolean printHeaderLine(String headerLine)
    {
        rcptRenderer.centerLine(headerLine);

        return true;
    }

    public boolean printRcptIdInfo(ReceiptData receiptData)
    {
        String date = receiptData.getTxnData().getTransDate();
        String time = receiptData.getTxnData().getTransTime();
        String cashierName = receiptData.getTxnData().getCashierName();
        String terminalID = Integer.toString(receiptData.getTxnData().getTerminalID());
        String newTransID = receiptData.getTxnData().getTransactionID();
        String transTypeName = TypeData.getTransactionTypeName(receiptData.getTxnData().getTransTypeID());

        rcptRenderer.addBlankLine();
        rcptRenderer.add2ColumnLine(date, time);
        rcptRenderer.add2ColumnLine("Cashier: " + cashierName, "TID: " + terminalID);
        rcptRenderer.add2ColumnLine(transTypeName + ": " + newTransID, "");
        rcptRenderer.addBlankLine();

        return true;
    }

    //--------------------------------------------------------------------------
    //  Printing Transaction Items (PLUs, discounts, RA's, PO's)
    //--------------------------------------------------------------------------
    public void printProductItem(Plu product, int transTypeID, boolean printPluCodes)
    {
        try
        {
            int quantity = (int)Math.round(product.getQuantity());

            int indent = 4;

            String rightColumn = "";

            // Print product name
            String productName = product.getName();
            String indentStr = "    ";
            String modifierIndentStr = "  ";


            if (product.getIsModifier() == false)
            {
                String qtyStr = ((Integer)quantity).toString();
                Integer qtyStrSize = qtyStr.length();
                //Remove length of qty string from indent, but always leave one space
                if (qtyStrSize<4)
                {
                    indent -= qtyStrSize;
                }
                else
                {
                    indent = 1;
                }
                //Build indentStr
                String nameIndentStr = "";
                for (int i = 0; i < indent; i++)
                {
                    nameIndentStr += " ";
                }
                productName = ((Integer)quantity).toString() + nameIndentStr + productName;
                //Reset indent
                indent = 4;
            }
            else if (product.getIsModifier())
            {
                productName = indentStr + modifierIndentStr + productName;
            }

            rcptRenderer.add2ColumnLine(rcptRenderer.indent(0, productName), rightColumn);

            // Print plu code, if any
            if (product.getPluCode().equals("") == false && printPluCodes)
            {
                rcptRenderer.addLine(indentStr + modifierIndentStr + "Product#: " + product.getPluCode());
            }

            //Add prep option to printed receipts
            if (product.getPrepOption() != null)
            {
                boolean isDefaultPrep = product.getPrepOption().getDefaultOption();
                boolean displayDefaults = product.getPrepOption().getDisplayDefaultReceipts();
                // Check if the prep option is a default option and display defaults on receipts is on, or the prep option is not a default
                //OR If the default prep option has a price, we will ALWAYS display it, regardless of the setting
                boolean canPrintPrep = ( (( isDefaultPrep && displayDefaults ) || ( product.getPrepOption().getPrepOptionPrice() > 0 ) ) || !isDefaultPrep );

                if (canPrintPrep)
                {
                    // Add prep options to a new line
                    String prepPrintName = product.getPrepOption().getName();
                    prepPrintName = modifierIndentStr +  " + " + prepPrintName;

                    if (prepPrintName.length() >0)
                    {
                        rcptRenderer.addLine(rcptRenderer.indent(indent, prepPrintName));
                    }
                }
            }
            // Print item comment (Unless manual weight flag, ignore those)
            if ( (product.getItemComment().length() > 0)  && (!product.getItemComment().equals("MAN WT")) )
            {
                rcptRenderer.addLine("   Cmt: " + product.getItemComment());
            }
        }
        catch (Exception e)
        {
            ReceiptGen.logError("GiftReceiptFormatter.printProduct error", e);
        }
    }

    //--------------------------------------------------------------------------
    //  Printing Transaction Footer Methods
    //--------------------------------------------------------------------------
    public boolean printItemCount(Integer itemCount, int transTypeID)
    {
        rcptRenderer.addBlankLine();

        if (transTypeID == TypeData.TranType.REFUND)
        {
            rcptRenderer.addOneColumnLineRightAlign("Number of Items Refunded: " + itemCount.toString());
        }
        else if (transTypeID == TypeData.TranType.VOID)
        {
            rcptRenderer.addOneColumnLineRightAlign("Number of Items Voided: " + itemCount.toString());
        }
        else
        {
            rcptRenderer.addOneColumnLineRightAlign("Number of Items Sold: " + itemCount.toString());
        }

        return true;
    }

    public boolean printFooterLine(String footerLine, boolean firstLine, boolean lastLine)
    {
        if (firstLine)
        {
            rcptRenderer.addBlankLine();
        }

        rcptRenderer.centerLine(footerLine);
        if(lastLine)
        {
            rcptRenderer.addBlankLine();
        }
        return true;
    }

    public boolean finishPrint()
    {
        rcptRenderer.addBlankLine();
        rcptRenderer.cutPaper();
        rcptRenderer.end();

        return true;
    }
}
