package com.mmhayes.common.receiptGen.Formatter;

import com.mmhayes.common.receiptGen.ReceiptData.IPaymentProcessorReceiptData;
import com.mmhayes.common.receiptGen.ReceiptData.ReceiptData;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;

/**
 * Created by nyu on 6/26/2018.
 */
public interface IPaymentProcessorRcptFormatter
{
    void setRenderer(IRenderer i);

    IRenderer getRenderer();

    void logError(String error, Exception e);

    boolean buildReceipt(IPaymentProcessorReceiptData rd);
}
