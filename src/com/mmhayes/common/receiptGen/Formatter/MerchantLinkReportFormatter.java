package com.mmhayes.common.receiptGen.Formatter;

import com.mmhayes.common.receiptGen.ReceiptData.IReportData;
import com.mmhayes.common.receiptGen.ReceiptData.MerchantLinkReportData;
import com.mmhayes.common.receiptGen.ReceiptData.ReceiptData;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.BasicHTMLRenderer;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by nyu on 8/3/2017.
 */
public class MerchantLinkReportFormatter implements ICustomReportFormatter
{
    private static String logFileName = "MLReportWorker.log";
    private IRenderer rcptRenderer;

    public MerchantLinkReportFormatter()
    {
    }

    @Override
    public void setRenderer(IRenderer i)
    {
        rcptRenderer = i;
    }

    @Override
    public IRenderer getRenderer()
    {
        return rcptRenderer;
    }

    @Override
    public void logError(String error, Exception e)
    {
        ReceiptGen.logError(error, e);
    }
    
    @Override
    public boolean buildReport(IReportData reportData)
    {
        ArrayList<String> reportDetails = ((MerchantLinkReportData) reportData).getReportDetails();

        // Beginning of report
        if(!rcptRenderer.begin())
        {
            return false;
        }

        for (String reportDetail : reportDetails)
        {
            if (reportDetail.equals("null") == true)
            {
                rcptRenderer.addOneColumnLineLeftAlign("");
            }
            else
            {
                rcptRenderer.addOneColumnLineLeftAlign(reportDetail);
            }
        }

        rcptRenderer.addBlankLine();

        if (rcptRenderer instanceof BasicHTMLRenderer)
        {
            rcptRenderer.endTable();
        }
        else
        {
            rcptRenderer.cutPaper();
        }
        rcptRenderer.end();
        
        return true;
    }
}
