package com.mmhayes.common.receiptGen.Formatter;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kitchenPrinters.KitchenPrinterCommon;
import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.*;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TransactionData.Plu;
import com.mmhayes.common.receiptGen.TransactionData.TransactionItem;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import com.sun.org.apache.xml.internal.serialize.Printer;
import nu.xom.jaxen.function.StringFunction;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
//import com.mmhayes.common.utils.Logger;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
//import java.util.logging.Logger;

/*
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-09-04 17:08:30 -0400 (Fri, 04 Sep 2020) $: Date of last commit
 $Rev: 12575 $: Revision of last commit
 Notes: *ReceiptFormatter - controls what to display and where to display it
*/

public class KitchenPrinterReceiptFormatter extends CommonFormatter {
    private static final String SUB_HEADER_DASHES = "---";
    private static final String SUB_HEADER_SPACES = "      ";
    private static final String SUB_HEADER_STARS = "*****";
    private static String logFileName = "KitchenPrinter.log";

    private DataManager dm;
    private IRenderer rcptRenderer;

    public final static String JPOS_ALIGN_CENTER = "\u001b|cA";
    public final static String JPOS_ALIGN_RIGHT  = "\u001b|rA";
    public final static String JPOS_LARGE_FNT  = "\u001b|4C";
    public static final String JPOS_CUT = "---CUT---";
    public static final String BARCODE = "---BARCODE---";
    private static final String ALREADY_SENT_HEADER = "****     ALREADY SENT     ****";
    private static final String NEW_PRODUCTS_HEADER = "****     NEW PRODUCTS     ****";
    private static ConcurrentHashMap<BigDecimal, Integer> printedOrderNumbersHM = new ConcurrentHashMap<>();
    private int prevPrinterID = -1;
    private String prevOrigOrderNumber = "";
    private int printStatusID = PrintJobStatusType.WAITING.getTypeID();
    private int prevPrinterHostID = 1;
    private static final int MAX_PRINT_CHARS_LARGE_FONT = 16;
    private static final int MAX_PRINT_CHARS_SMALL_FONT = 36;

    public KitchenPrinterReceiptFormatter(DataManager d)
    {
        dm = d;
    }

    @Override
    public void setRenderer(IRenderer i)
    {
        rcptRenderer = i;
    }

    @Override
    public IRenderer getRenderer()
    {
        return rcptRenderer;
    }

    @Override
    public void logError(String error, Exception e)
    {
        ReceiptGen.logError(error, e);
    }

    //--------------------------------------------------------------------------
    //  Building Kitchen Printer Receipt Details
    //--------------------------------------------------------------------------
    @Override
    public boolean buildReceipt (ReceiptData rcptData) {

        boolean buildReceiptSuccess = false;

        try {

            // make sure we have renderer
            if (rcptRenderer == null) {
                return false;
            }

            // make sure the renderer is able to render
            if (!rcptRenderer.begin()) {
                return false;
            }

            // add a blank line for the html summary
            rcptRenderer.addBlankLine();

            // make sure there is a print job that actually needs to be printed
            if ((rcptData != null) && (rcptData.getKpJobInfo() == null)) {
                return false;
            }

            // determine if the transaction is complete
            boolean isTxnComplete = isTxnComplete(rcptData);

            // determine if the transaction was made on a remote order terminal
            boolean isTxnOnROT = isTxnOnROT(rcptData);

            // get information for the receipt header from the kitchen printer job info
            KitchenPrinterJobInfo kpJobInfo = rcptData.getKpJobInfo();
            KPJobHeader kpJobHeader = kpJobInfo.getKpJobHeader();
            int orderTypeID = 0;
            if (kpJobHeader != null) {

                // modify the KPJobHeader for kiosk transactions that were sent directly to the printer host
                kpJobHeader = modifyKPJobHeaderForKioskTransactions(rcptData);

                // log the KPJobHeader information
                Logger.logMessage(String.format("********** START JOB HEADER FOR TRANSACTION ID %s **********",
                        Objects.toString(kpJobHeader.getPATransactionID(), "NULL")), logFileName, Logger.LEVEL.DEBUG);
                Logger.logMessage(kpJobHeader.toString(), logFileName, Logger.LEVEL.DEBUG);
                Logger.logMessage(String.format("********** END JOB HEADER FOR TRANSACTION ID %s **********",
                        Objects.toString(kpJobHeader.getPATransactionID(), "NULL")), logFileName, Logger.LEVEL.DEBUG);

                // if the paOrderTypeID doesn't exist use the default order type
                if (kpJobHeader.getOrderTypeID() <= 0) {
                    orderTypeID = TypeData.OrderType.NORMAL_SALE;
                }
            }

            // get the current order number
            String orderNumber = rcptData.getOrderNumber();

            // determine information about products within the transaction
            boolean hasPreviouslyPrintedProducts = false;
            boolean hasUnprintedProducts = false;
            boolean hasUnprintedExpeditorProducts = false;
            boolean hasProductsFromPrevOrder = false;
            boolean hasProductsInCurrOrder = false;
            HashMap<String, ArrayList<Plu>> sortedKPReceiptGroups = sortKPReceiptGroups(rcptData);
            if ((sortedKPReceiptGroups != null) && (!sortedKPReceiptGroups.isEmpty())) {
                hasPreviouslyPrintedProducts = ((sortedKPReceiptGroups.containsKey(TypeData.ProductKPReceiptGroupType.PRINTED_PLU))
                        && (sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.PRINTED_PLU) != null)
                        && (!sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.PRINTED_PLU).isEmpty()));
                hasUnprintedProducts = ((sortedKPReceiptGroups.containsKey(TypeData.ProductKPReceiptGroupType.UNPRINTED_PLU))
                        && (sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.UNPRINTED_PLU) != null)
                        && (!sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.UNPRINTED_PLU).isEmpty()));
                hasUnprintedExpeditorProducts = ((sortedKPReceiptGroups.containsKey(TypeData.ProductKPReceiptGroupType.UNPRINTED_EXPEDITOR_PLU))
                        && (sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.UNPRINTED_EXPEDITOR_PLU) != null)
                        && (!sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.UNPRINTED_EXPEDITOR_PLU).isEmpty()));
                hasProductsFromPrevOrder = ((sortedKPReceiptGroups.containsKey(TypeData.ProductKPReceiptGroupType.PREV_ORDER_PLU))
                        && (sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.PREV_ORDER_PLU) != null)
                        && (!sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.PREV_ORDER_PLU).isEmpty()));
                hasProductsInCurrOrder = ((sortedKPReceiptGroups.containsKey(TypeData.ProductKPReceiptGroupType.CURR_ORDER_PLU))
                        && (sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.CURR_ORDER_PLU) != null)
                        && (!sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.CURR_ORDER_PLU).isEmpty()));
            }

            if (hasPreviouslyPrintedProducts || hasUnprintedProducts) {

                // get the standard header data for the transaction within the receipt data
                ArrayList<String> standardHeaderData = getStandardHeaderData(rcptData);

                // store the modified KPJobDetails
                ArrayList<KPJobDetail> newDetails = new ArrayList<>();

                ArrayList<Plu> productsNeedingPrintStatusUpdate = new ArrayList<>();
                boolean hasPrintedHeader = false;
                boolean hasPrintedOrigOrderNum = true;
                boolean hasPrintedNewProductsHdr = false;
                boolean hasPrintedAlreadySentHdr = false;

                // create print lines for each detail
                ArrayList<KPJobDetail> reorderedKPDetails = kpJobInfo.sortDetails(rcptData.getTxnData(), orderNumber);

                if ((reorderedKPDetails != null) && (!reorderedKPDetails.isEmpty())) {
                    for (KPJobDetail kpJobDetail : reorderedKPDetails) {
                        // get information from the kpJobDetail
                        int printerID = kpJobDetail.getPrinterID();
                        int printerHostID = kpJobDetail.getPrinterHostID();
                        String printerName = kpJobDetail.getPrinterName();
                        boolean printOnExpeditor = kpJobDetail.getIsExpeditor();
                        boolean printOnRemote = (kpJobDetail.getPrinterTypeID() == TypeData.PrinterType.REMOTE_ORDER_PRINTER);
                        boolean noExpeditorTktsUnlessProdAdded = kpJobDetail.getNoExpeditorTktsUnlessProdAdded();
                        int printerHardwareTypeID = kpJobDetail.getPrinterHardwareTypeID();
                        boolean printOnlyNew = kpJobDetail.isPrinterPrintsOnlyNew();
                        int transLineItemID = kpJobDetail.getPaTransLineItemID();
                        double quantity = kpJobDetail.getQuantity();
                        boolean isModifier = kpJobDetail.getIsModifier();
                        int papluID = kpJobDetail.getPAPluID();
                        String productName = kpJobDetail.getProductName();
                        String hideStation = kpJobDetail.getHideStation();
                        String originalOrderNumber = "";
                        boolean printPrepReceipt = (kpJobDetail.getPrinterTypeID() == TypeData.PrinterType.KITCHEN_PRINTER);
                        boolean printedInPreviousOrder = false;
                        boolean printExpeditorReceipt = false;
                        boolean printRemoteOrderReceipt = false;

                        // get the product to print for this KPJobDetail
                        Plu product = rcptData.getTxnData().getProduct(transLineItemID);
                        if (product != null) {
                            printedInPreviousOrder = product.hasBeenPrinted();
                            if (!printedInPreviousOrder) {
                                productsNeedingPrintStatusUpdate.add(product);
                            }
                            originalOrderNumber = product.getOriginalOrderNum();
                        }

                        if (printOnExpeditor) {
                            printExpeditorReceipt = true;
                        }

                        if (printOnRemote) {
                            printRemoteOrderReceipt = true;
                        }

                        Logger.logMessage(String.format("Print Line -> IS PRODUCT NEW: %s, PRODUCT NAME: %s, QUANTITY: %.4f ON PRINTER: %s",
                                Objects.toString((!printedInPreviousOrder ? "YES" : "NO"), "NULL"),
                                Objects.toString(productName, "NULL"),
                                quantity,
                                Objects.toString(printerName, "NULL")));

                        // check if we finished printing everything that should have printed on the printer
                        if (printerID != prevPrinterID) {
                            // finish the receipt for the previous printer and start a new one for the current printer
                            Logger.logMessage("Now switching to the printer "+Objects.toString(printerName, "NULL")+".....", logFileName, Logger.LEVEL.DEBUG);
                            finishReceipt(rcptData, hasPrintedHeader, newDetails);
                            prevPrinterID = printerID;
                            prevPrinterHostID = printerHostID;
                            prevOrigOrderNumber = "";
                            hasPrintedHeader = false;
                            hasPrintedOrigOrderNum = true;
                            hasPrintedNewProductsHdr = false;
                            hasPrintedAlreadySentHdr = false;
                        }

                        // determine whether or not to create a new KPJobDetail
                        Logger.logMessage("************** START DETERMINE SHOULD CREATE NEW KP JOB DETAIL **************", logFileName, Logger.LEVEL.DEBUG);
                        Logger.logMessage(String.format("CHECKING IF A NEW KP JOB DETAIL SHOULD BE CREATED FOR THE " +
                                "PRODUCT: %s ON THE PRINTER: %s",
                                Objects.toString(productName, "NULL"),
                                Objects.toString(printerName, "NULL")), logFileName, Logger.LEVEL.DEBUG);
                        boolean shouldCreateNewKPJobDetail = shouldCreateNewKPJobDetail(printPrepReceipt,
                                                                                        printOnExpeditor,
                                                                                        noExpeditorTktsUnlessProdAdded,
                                                                                        printExpeditorReceipt,
                                                                                        hasUnprintedExpeditorProducts,
                                                                                        isTxnOnROT,
                                                                                        printRemoteOrderReceipt,
                                                                                        printedInPreviousOrder,
                                                                                        printOnlyNew,
                                                                                        hasUnprintedProducts,
                                                                                        printerHardwareTypeID);

                        // create a new KPJobDetail for applicable products
                        if (shouldCreateNewKPJobDetail) {
                            // if the receipt doesn't have a header then add one
                            if (!hasPrintedHeader) {
                                // determine whether or not to add the ADD ON header
                                boolean useAddOn = ((hasProductsFromPrevOrder) && ((hasUnprintedProducts) && (hasProductsInCurrOrder)) && (printOnlyNew));
                                // build KPJobDetails for the header
                                if (orderTypeID == TypeData.OrderType.NORMAL_SALE) {
                                    // create standard header
                                    createStandardHeaderDetails(printerID, printerName, printStatusID, printerHostID, printerHardwareTypeID,
                                            orderNumber, useAddOn, kpJobHeader, standardHeaderData, newDetails);
                                }
                                else if (printOnExpeditor) {
                                    // create the expeditor header
                                    createExpeditorHeaderDetails(kpJobDetail, kpJobHeader, rcptData, standardHeaderData, newDetails);
                                }
                                else if (printOnRemote) {
                                    // create the remote order header
                                    createRemoteOrderHeaderDetails(kpJobDetail, kpJobHeader, rcptData, standardHeaderData, newDetails);
                                }
                                else {
                                    // create the header for an online order not being sent to an expeditor or remote order printer
                                    createStandardOnlineOrderHeader(kpJobDetail, kpJobHeader, rcptData, useAddOn, standardHeaderData, newDetails);
                                }
                                hasPrintedHeader = true;
                            }

                            // check if we should add an already sent header
                            if ((!hasPrintedAlreadySentHdr) && (shouldAlreadySentHeaderPrint(product, printOnlyNew, printPrepReceipt, printExpeditorReceipt, printRemoteOrderReceipt))) {
                                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                                        .printerID(printerID)
                                        .printStatusID(printStatusID)
                                        .printerHostID(printerHostID)
                                        .papluID(-1)
                                        .quantity(0.0d)
                                        .isModifier(false)
                                        .hideStation("")
                                        .line(" ")
                                        .fixWordWrap(true)
                                        .build(),
                                        newDetails);
                                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                                        .printerID(printerID)
                                        .printStatusID(printStatusID)
                                        .printerHostID(printerHostID)
                                        .papluID(-1)
                                        .quantity(0.0d)
                                        .isModifier(false)
                                        .hideStation("")
                                        .line(JPOS_ALIGN_CENTER+ALREADY_SENT_HEADER)
                                        .fixWordWrap(true)
                                        .build(),
                                        newDetails);
                                hasPrintedAlreadySentHdr = true;
                            }

                            // check if we should add a new products header
                            if ((!hasPrintedNewProductsHdr) && (shouldNewProdHeaderPrint(product, printOnlyNew, printPrepReceipt, printExpeditorReceipt, printRemoteOrderReceipt))) {
                                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                                        .printerID(printerID)
                                        .printStatusID(printStatusID)
                                        .printerHostID(printerHostID)
                                        .papluID(-1)
                                        .quantity(0.0d)
                                        .isModifier(false)
                                        .hideStation("")
                                        .line(" ")
                                        .fixWordWrap(true)
                                        .build(),
                                        newDetails);
                                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                                        .printerID(printerID)
                                        .printStatusID(printStatusID)
                                        .printerHostID(printerHostID)
                                        .papluID(-1)
                                        .quantity(0.0d)
                                        .isModifier(false)
                                        .hideStation("")
                                        .line(JPOS_ALIGN_CENTER+NEW_PRODUCTS_HEADER)
                                        .fixWordWrap(true)
                                        .build(),
                                        newDetails);
                                hasPrintedNewProductsHdr = true;
                            }

                            // check if we should add an original order number sub-header
                            if (shouldOrigOrderNumHeaderPrint(originalOrderNumber, orderNumber)) {
                                prevOrigOrderNumber = (originalOrderNumber.isEmpty() ? orderNumber : originalOrderNumber);
                                hasPrintedOrigOrderNum = false;
                            }

                            // add the original order number sub-header
                            if (!hasPrintedOrigOrderNum) {
                                Logger.logMessage("Now printing the original order number sub header.....", logFileName, Logger.LEVEL.DEBUG);
                                String originalOrderNumberSubHeader = JPOS_ALIGN_CENTER + SUB_HEADER_DASHES + "  " + "Order Number: " +
                                        (!originalOrderNumber.isEmpty() ? originalOrderNumber : orderNumber) + "  " + SUB_HEADER_DASHES;
                                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                                        .printerID(printerID)
                                        .printStatusID(printStatusID)
                                        .printerHostID(printerHostID)
                                        .papluID(-1)
                                        .quantity(0.0d)
                                        .isModifier(false)
                                        .hideStation("")
                                        .line(" ")
                                        .fixWordWrap(true)
                                        .build(),
                                        newDetails);
                                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                                        .printerID(printerID)
                                        .printStatusID(printStatusID)
                                        .printerHostID(printerHostID)
                                        .papluID(-1)
                                        .quantity(0.0d)
                                        .isModifier(false)
                                        .hideStation("")
                                        .line(originalOrderNumberSubHeader)
                                        .fixWordWrap(true)
                                        .build(),
                                        newDetails);
                                hasPrintedOrigOrderNum = true;
                            }

                            // add the line
                            String line = String.format("%2dX %s", Math.round(quantity), productName);
                            if (isModifier) {
                                line = String.format("\t %s", productName);
                            }
                            // don't fix word wrap on KDS
                            boolean fixWordWrap = true;
                            if (printerHardwareTypeID == TypeData.PrinterHardwareType.KDS) {
                                fixWordWrap = false;
                            }
                            createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                                    .paTransLineItemID(product.getPATransLineItemID())
                                    .paTransLineItemModID(product.getPaTransLineItemModID())
                                    .printerID(printerID)
                                    .printStatusID(printStatusID)
                                    .printerHostID(printerHostID)
                                    .papluID(papluID)
                                    .quantity(quantity)
                                    .isModifier(isModifier)
                                    .hideStation(hideStation)
                                    .line(line)
                                    .fixWordWrap(fixWordWrap)
                                    .build(),
                                    newDetails);
                        }
                        else {
                            // skip creating a new KPJobDetail for the product
                            Logger.logMessage("Skipped product "+Objects.toString(productName, "NULL")+".",
                                    logFileName, Logger.LEVEL.DEBUG);
                        }
                    }
                }

                finishReceipt(rcptData, hasPrintedHeader, newDetails);

                rcptRenderer.end();

                // update trans line status
                updatePrinterQueueDetailStatus(productsNeedingPrintStatusUpdate, PrintJobStatusType.WAITING.getTypeID());

                // update the KitchenPrinterJobInfo's KPJobDetails
                kpJobInfo.setDetails(newDetails);
                kpJobInfo.createPlainTextDetails(rcptData);
            }

            buildReceiptSuccess = true;

        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to build the receipt for the transaction with an ID of " +
                    Objects.toString(rcptData.getTxnData().getTransactionID(), "NULL")+" in " +
                    "KitchenPrinterReceiptFormatter.buildReceipt", logFileName, Logger.LEVEL.ERROR);
        }

        return buildReceiptSuccess;
    }

    /**
     * Adds a barcode to the receipt if applicable and adds a JPOS command to cut the receipt.
     *
     * @param receiptData {@link ReceiptData} Data on the receipt.
     * @param hasPrintedHeader Whether or not the receipt has a printed header.
     * @param newDetails {@link ArrayList<KPJobDetail>} The KPJobDetails to add to the print queue.
     */
    @SuppressWarnings("SpellCheckingInspection")
    private void finishReceipt (ReceiptData receiptData, boolean hasPrintedHeader, ArrayList<KPJobDetail> newDetails) {

        try {
            if ((prevPrinterID != -1) && (hasPrintedHeader)) {
                if (receiptData.getPrintTxnBarcode()) {
                    createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                            .printerID(prevPrinterID)
                            .printStatusID(printStatusID)
                            .printerHostID(prevPrinterHostID)
                            .papluID(-1)
                            .quantity(0.0d)
                            .isModifier(false)
                            .hideStation("")
                            .line(BARCODE+receiptData.getTxnData().getTransactionID())
                            .fixWordWrap(true)
                            .build(),
                            newDetails);
                }
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(prevPrinterID)
                        .printStatusID(printStatusID)
                        .printerHostID(prevPrinterHostID)
                        .papluID(-1)
                        .quantity(0.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(JPOS_CUT)
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to finish the receipt for the printer with an ID of " +
                    Objects.toString(prevPrinterID, "NULL")+" in KitchenPrinterReceiptFormatter.finishReceipt",
                    logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Get standard header information for the transaction within the receipt data.
     *
     * @param receiptData {@link ReceiptData} The receipt data that contains the transaction.
     * @return {@link ArrayList<String>} The standard header data.
     */
    @SuppressWarnings({"OverlyComplexMethod"})
    private ArrayList<String> getStandardHeaderData (ReceiptData receiptData) {
        ArrayList<String> standardHeaderData = new ArrayList<>();

        try {
            if ((receiptData != null) && (receiptData.getTxnData() != null)) {
                TransactionData txnData = receiptData.getTxnData();
                String transDate = (((txnData.getTransDate() != null) && (!txnData.getTransDate().isEmpty())) ? txnData.getTransDate() : "");
                String transTime = (((txnData.getTransTime() != null) && (!txnData.getTransTime().isEmpty())) ? txnData.getTransTime() : "");
                String cashierName = (((txnData.getCashierName() != null) && (!txnData.getCashierName().isEmpty())) ? txnData.getCashierName() : "");
                String paTransactionID = (((txnData.getTransactionID() != null) && (!txnData.getTransactionID().isEmpty()) && (NumberUtils.isNumber(txnData.getTransactionID())))
                        ? txnData.getTransactionID() : "");
                int transactionTypeID = (txnData.getTransTypeID() > 0 ? txnData.getTransTypeID() : 0);
                String transTypeName = (transactionTypeID > 0 ? TypeData.getTransactionTypeName(transactionTypeID) : "");
                int terminalID = (txnData.getTerminalID() > 0 ? txnData.getTerminalID() : 0);

                // add standard header lines
                standardHeaderData.add(twoColumn(transDate, transTime));
                standardHeaderData.add(twoColumn("Cashier: "+cashierName, "TID: "+Integer.toString(terminalID)));
                standardHeaderData.add(twoColumn(transTypeName+" : "+paTransactionID, ""));

                // alert the kitchen of refunded/voided orders
                if (transactionTypeID != TypeData.TranType.SALE) {
                    String msg = "";
                    if (TypeData.TranType.VOID == transactionTypeID) {
                        msg = "Order VOID";
                    }
                    else if (TypeData.TranType.REFUND == transactionTypeID) {
                        msg = "Order REFUND";
                    }
                    if (!msg.isEmpty()) {
                        standardHeaderData.add(JPOS_ALIGN_CENTER + msg);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to get the standard header data for the transaction with " +
                    "and ID of "+Objects.toString(receiptData.getTxnData().getTransactionID(), "NULL")+" in " +
                    "KitchenPrinterReceiptFormatter.getStandardHeaderData", logFileName, Logger.LEVEL.ERROR);
        }

        return standardHeaderData;
    }

//    /**
//     * Adds the modified KPJobDetail to the new KPJobDetails.
//     *
//     * @param printerID ID of the printer the KPJobDetail should be executed on.
//     * @param printStatusID Print status of the KPJobDetail.
//     * @param printerHostID ID of the printer host controlling the printer in the KPJobDetail.
//     * @param papluID ID of the product in the KPJobDetail.
//     * @param quantity Amount of the product in the transaction.
//     * @param isModifier Whether or not the product in the KPJobDetail is a modifier.
//     * @param hideStation ID of the KDS station to hide the KPJobDetail on.
//     * @param line {@link String} The line to print on the receipt for the KPJobDetail.
//     * @param fixWordWrap Whether or not to format lines on the receipt to prevent word wrap.
//     * @param newDetails {@link ArrayList<KPJobDetail>} The KPJobDetails to add to the print queue.
//     */
//    @Deprecated
//    @SuppressWarnings({"TypeMayBeWeakened", "SpellCheckingInspection"})
//    private void createPrintQueueDetail (int printerID,
//                                         int printStatusID,
//                                         int printerHostID,
//                                         int papluID,
//                                         double quantity,
//                                         boolean isModifier,
//                                         int hideStation,
//                                         String line,
//                                         boolean fixWordWrap,
//                                         ArrayList<KPJobDetail> newDetails) {
//
//        try {
//            // fix any word wrapping
//            ArrayList<String> newLines = new ArrayList<>();
//            if (fixWordWrap) {
//                newLines = fixWordWrap(line);
//            }
//            else {
//                newLines.add(line);
//            }
//
//            if ((newLines != null) && (!newLines.isEmpty())) {
//                newLines.forEach(newLine -> {
//                    // add the line to the renderer with no JPOS commands for the html summary
//                    if (rcptRenderer != null) {
//                        rcptRenderer.addLine(purgeJPOSCommands(newLine));
//                    }
//
//                    // add the KPJobDetail to the new KPJobDetails
//                    newDetails.add(new KPJobDetailCreator.KPJobDetailBuilder()
//                            .printerID(printerID)
//                            .printStatusID(printStatusID)
//                            .printerHostID(printerHostID)
//                            .papluID(papluID)
//                            .quantity(quantity)
//                            .isModifier(isModifier)
//                            .hideStation(hideStation)
//                            .lineDetail(encode(newLine))
//                            .build().convertCreatorToKPJobDetail());
//                });
//            }
//        }
//        catch (Exception e) {
//            Logger.logException(e);
//            Logger.logMessage("There was a problem trying to add the KPJobDetail to the new KPJobDetails for the " +
//                    "product with an ID of "+Objects.toString(papluID, "NULL")+" in " +
//                    "KitchenPrinterReceiptFormatter.createPrintQueueDetail", logFileName, Logger.LEVEL.ERROR);
//        }
//
//    }

    /**
     * Adds the modified KPJobDetail to the new KPJobDetails.
     *
     * @param printQueueDetail {@link PrintQueueDetail} The PrintQueueDetail object to add to the new KPJobDetails.
     * @param newDetails {@link ArrayList<KPJobDetail>} The KPJobDetails to add to the print queue.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "SpellCheckingInspection"})
    private void createPrintQueueDetail (PrintQueueDetail printQueueDetail,
                                         ArrayList<KPJobDetail> newDetails) {

        try {
            // fix any word wrapping
            ArrayList<String> newLines = new ArrayList<>();
            if (printQueueDetail.getFixWordWrap()) {
                newLines = fixWordWrap(printQueueDetail.getLine());
            }
            else {
                newLines.add(printQueueDetail.getLine());
            }

            if ((newLines != null) && (!newLines.isEmpty())) {
                newLines.forEach(newLine -> {
                    // add the line to the renderer with no JPOS commands for the html summary
                    if (rcptRenderer != null) {
                        rcptRenderer.addLine(purgeJPOSCommands(newLine));
                    }

                    // add the KPJobDetail to the new KPJobDetails
                    newDetails.add(new KPJobDetailCreator.KPJobDetailBuilder()
                            .paTransLineItemID(printQueueDetail.getPaTransLineItemID())
                            .paTransLineItemModID(printQueueDetail.getPaTransLineItemModID())
                            .printerID(printQueueDetail.getPrinterID())
                            .printStatusID(printQueueDetail.getPrintStatusID())
                            .printerHostID(printQueueDetail.getPrinterHostID())
                            .papluID(printQueueDetail.getPapluID())
                            .quantity(printQueueDetail.getQuantity())
                            .isModifier(printQueueDetail.getIsModifier())
                            .hideStation(printQueueDetail.getHideStation())
                            .lineDetail(encode(newLine))
                            .build().convertCreatorToKPJobDetail());
                });
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to add the KPJobDetail to the new " +
                    "KPJobDetails with the following params, PATRANSLINEITEMID: %s, PATRANSLINEITEMMODID: %s, " +
                    "PRINTERID: %s, PRINTSTATUSID: %s, PRINTERHOSTID: %s, PAPLUID: %s, quantity: %s, ISMODIFIER: %s, " +
                    "and HIDESTATION: %s in KitchenPrinterReceiptFormatter.createPrintQueueDetail",
                    Objects.toString(printQueueDetail.getPaTransLineItemID(), "NULL"),
                    Objects.toString(printQueueDetail.getPaTransLineItemModID(), "NULL"),
                    Objects.toString(printQueueDetail.getPrinterID(), "NULL"),
                    Objects.toString(printQueueDetail.getPrintStatusID(), "NULL"),
                    Objects.toString(printQueueDetail.getPrinterHostID(), "NULL"),
                    Objects.toString(printQueueDetail.getPapluID(), "NULL"),
                    Objects.toString(printQueueDetail.getQuantity(), "NULL"),
                    Objects.toString(printQueueDetail.getIsModifier(), "NULL"),
                    Objects.toString(printQueueDetail.getHideStation(), "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Create KPJobDetails for lines in the receipt's header.
     *
     * @param printerID ID of the printer the KPJobDetail should be executed on.
     * @param printerName {@link String} Name of the printer that will execute the KPJobDetail.
     * @param printStatusID Print status of the KPJobDetail.
     * @param printerHostID ID of the printer host controlling the printer in the KPJobDetail.
     * @param printerHardwareTypeID Type of hardware that will execute the KPJobDetail.
     * @param orderNumber Order number to add to the receipt's header.
     * @param useAddOn Whether or not to print the ADD ON header.
     * @param kpJobHeader {@link KPJobHeader} The KPJobHeader object which contains information for the receipt's header.
     * @param headerData {@link ArrayList<String>} Standard header data.
     * @param newDetails {@link ArrayList<KPJobDetail>} The KPJobDetails to add to the print queue.
     */
    @SuppressWarnings("OverlyComplexMethod")
    private void createStandardHeaderDetails (int printerID,
                                              String printerName,
                                              int printStatusID,
                                              int printerHostID,
                                              int printerHardwareTypeID,
                                              String orderNumber,
                                              boolean useAddOn,
                                              KPJobHeader kpJobHeader,
                                              ArrayList<String> headerData,
                                              ArrayList<KPJobDetail> newDetails) {

        try {
            // add the standard header data
            if ((headerData != null) && (!headerData.isEmpty())) {
                headerData.forEach(line -> {
                    createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                            .printerID(printerID)
                            .printStatusID(printStatusID)
                            .printerHostID(printerHostID)
                            .papluID(-1)
                            .quantity(-1.0d)
                            .isModifier(false)
                            .hideStation("")
                            .line((StringFunctions.stringHasContent(line) ? line : ""))
                            .fixWordWrap(true)
                            .build(),
                            newDetails);
                });
            }

            // add the printer name
            createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                    .printerID(printerID)
                    .printStatusID(printStatusID)
                    .printerHostID(printerHostID)
                    .papluID(-1)
                    .quantity(-1.0d)
                    .isModifier(false)
                    .hideStation("")
                    .line(JPOS_ALIGN_CENTER+(StringFunctions.stringHasContent(printerName) ? printerName : ""))
                    .fixWordWrap(true)
                    .build(),
                    newDetails);

            // add the order number
            createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                    .printerID(printerID)
                    .printStatusID(printStatusID)
                    .printerHostID(printerHostID)
                    .papluID(-1)
                    .quantity(-1.0d)
                    .isModifier(false)
                    .hideStation("")
                    .line(JPOS_ALIGN_CENTER+JPOS_LARGE_FNT+(StringFunctions.stringHasContent(orderNumber) ? orderNumber : ""))
                    .fixWordWrap(true)
                    .build(),
                    newDetails);

            // check if we should print ADD ON
            if (useAddOn) {
                // add an empty line
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(JPOS_ALIGN_CENTER)
                        .fixWordWrap(true)
                        .build(),
                        newDetails);

                // add ADD ON
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(JPOS_ALIGN_CENTER+JPOS_LARGE_FNT+"**** ADD ON ****")
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }

            // add an empty line
            createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                    .printerID(printerID)
                    .printStatusID(printStatusID)
                    .printerHostID(printerHostID)
                    .papluID(-1)
                    .quantity(-1.0d)
                    .isModifier(false)
                    .hideStation("")
                    .line(" ")
                    .fixWordWrap(true)
                    .build(),
                    newDetails);

            // determine if we should add an empty line
            boolean addEmptyLine = false;

            // add person name
            if (StringFunctions.stringHasContent(kpJobHeader.getPersonName())) {

                // only add the customer name if it isn't the same as the transaction name
                boolean custNameNotEqualTxnName = true;
                if (StringFunctions.stringHasContent(kpJobHeader.getTransName()) && (kpJobHeader.getTransName().equalsIgnoreCase(kpJobHeader.getPersonName()))) {
                    custNameNotEqualTxnName = false;
                }

                if (custNameNotEqualTxnName) {
                    String personName = KitchenPrinterCommon.correctAposAndQues(kpJobHeader.getPersonName());
                    createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                                    .printerID(printerID)
                                    .printStatusID(printStatusID)
                                    .printerHostID(printerHostID)
                                    .papluID(-1)
                                    .quantity(-1.0d)
                                    .isModifier(false)
                                    .hideStation("")
                                    .line("Customer: " + personName)
                                    .fixWordWrap(true)
                                    .build(),
                            newDetails);
                    addEmptyLine = true;
                }
            }

            // add transaction name
            if (StringFunctions.stringHasContent(kpJobHeader.getTransName())) {
                String label = "";
                if (StringFunctions.stringHasContent(kpJobHeader.getTransNameLabel())) {
                    label = kpJobHeader.getTransNameLabel()+ ": ";
                }
                String transName = KitchenPrinterCommon.correctAposAndQues(label + kpJobHeader.getTransName());
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(transName)
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
                addEmptyLine = true;
            }

            // add transaction comment    
            if (StringFunctions.stringHasContent(kpJobHeader.getTransComment())) {
                String transComment = KitchenPrinterCommon.correctAposAndQues(kpJobHeader.getTransComment());
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line("Comment: " + transComment)
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
                addEmptyLine = true;
            }

            // add empty line if applicable
            if (addEmptyLine) {
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(" ")
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to build the KPJobDetails for the standard header in " +
                    "KitchenPrinterReceiptFormatter.createStandardHeaderDetails", logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Create the header for expeditor receipts.
     *
     * @param kpJobDetail {@link KPJobDetail} The KPJobDetail object which contains information for the receipt's header.
     * @param kpJobHeader {@link KPJobHeader} The KPJobHeader object which contains information for the receipt's header.
     * @param receiptData {@link ReceiptData} Data for the receipt.
     * @param headerData {@link ArrayList<String>} Standard header data.
     * @param newDetails {@link ArrayList<KPJobDetail>} The KPJobDetails to add to the print queue.
     */
    @SuppressWarnings({"ConstantConditions", "OverlyComplexMethod"})
    private void createExpeditorHeaderDetails (KPJobDetail kpJobDetail,
                                               KPJobHeader kpJobHeader,
                                               ReceiptData receiptData,
                                               ArrayList<String> headerData,
                                               ArrayList<KPJobDetail> newDetails) {

        try {
            int printerID = kpJobDetail.getPrinterID();
            String printerName = (StringFunctions.stringHasContent(kpJobDetail.getPrinterName()) ? kpJobDetail.getPrinterName() : "");
            int printerHostID = kpJobDetail.getPrinterHostID();
            String orderNumber = (StringFunctions.stringHasContent(receiptData.getOrderNumber()) ? receiptData.getOrderNumber() : "");
            int orderTypeID = kpJobHeader.getOrderTypeID();
            String personName = (StringFunctions.stringHasContent(kpJobHeader.getPersonName()) ? kpJobHeader.getPersonName() : "");
            String phone = (StringFunctions.stringHasContent(kpJobHeader.getPhoneNumber()) ? kpJobHeader.getPhoneNumber() : "");
            String transComment = (StringFunctions.stringHasContent(kpJobHeader.getTransComment()) ? kpJobHeader.getTransComment() : "");
            String pickupDeliveryNote = (StringFunctions.stringHasContent(kpJobHeader.getPickupDeliveryNote()) ? kpJobHeader.getPickupDeliveryNote() : "");
            String deliveryLocation = (StringFunctions.stringHasContent(receiptData.getTxnData().getDeliveryLocation()) ? receiptData.getTxnData().getDeliveryLocation() : "");

            // add the standard header data
            if ((headerData != null) && (!headerData.isEmpty())) {
                headerData.forEach(line -> {
                    createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                            .printerID(printerID)
                            .printStatusID(printStatusID)
                            .printerHostID(printerHostID)
                            .papluID(-1)
                            .quantity(-1.0d)
                            .isModifier(false)
                            .hideStation("")
                            .line((StringFunctions.stringHasContent(line) ? line : ""))
                            .fixWordWrap(true)
                            .build(),
                            newDetails);
                });
            }

            // add printer name
            createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                    .printerID(printerID)
                    .printStatusID(printStatusID)
                    .printerHostID(printerHostID)
                    .papluID(-1)
                    .quantity(-1.0d)
                    .isModifier(false)
                    .hideStation("")
                    .line(JPOS_ALIGN_CENTER+printerName)
                    .fixWordWrap(true)
                    .build(),
                    newDetails);

            // add order number
            createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                    .printerID(printerID)
                    .printStatusID(printStatusID)
                    .printerHostID(printerHostID)
                    .papluID(-1)
                    .quantity(-1.0d)
                    .isModifier(false)
                    .hideStation("")
                    .line(JPOS_ALIGN_CENTER + JPOS_LARGE_FNT + orderNumber)
                    .fixWordWrap(true)
                    .build(),
                    newDetails);

            if (orderTypeID != TypeData.OrderType.NORMAL_SALE) {
                // add order type
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(JPOS_ALIGN_CENTER + JPOS_LARGE_FNT + "**************")
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(JPOS_ALIGN_CENTER + JPOS_LARGE_FNT + (orderTypeID == TypeData.OrderType.DELIVERY ? "DELIVERY" : "PICKUP"))
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(JPOS_ALIGN_CENTER + JPOS_LARGE_FNT + "**************")
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }

            // add the person name and phone number, only add the person name if it is different than the transaction name
            String line;
            if ((StringFunctions.stringHasContent(kpJobHeader.getTransName())) && (personName.equalsIgnoreCase(kpJobHeader.getTransName()))) {
                line = phone;
            }
            else {
                line = personName+" "+phone;
            }
            if (!line.isEmpty()) {
                line = KitchenPrinterCommon.correctAposAndQues(line);
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(line)
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }

            // add the delivery location
            if ((orderTypeID == TypeData.OrderType.DELIVERY) && (StringFunctions.stringHasContent(deliveryLocation))) {
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(deliveryLocation)
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }

            // add transaction name
            if (StringFunctions.stringHasContent(kpJobHeader.getTransName())) {
                String label = "";
                if (StringFunctions.stringHasContent(kpJobHeader.getTransNameLabel())) {
                    label = kpJobHeader.getTransNameLabel()+ ": ";
                }
                String transName = KitchenPrinterCommon.correctAposAndQues(label + kpJobHeader.getTransName());
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(transName)
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }

            // add the transaction comment
            if (!transComment.isEmpty()) {
                // replace any carriage returns in the transaction comment
                transComment = transComment.replaceAll("[\r\n]", "%%%");
                String[] newLines = transComment.split("%%%");

                if (newLines.length > 0) {
                    Arrays.stream(newLines).forEach(newLine -> {
                        newLine = KitchenPrinterCommon.correctAposAndQues(newLine);
                        createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                                .printerID(printerID)
                                .printStatusID(printStatusID)
                                .printerHostID(printerHostID)
                                .papluID(-1)
                                .quantity(-1.0d)
                                .isModifier(false)
                                .hideStation("")
                                .line("Comment: "+newLine)
                                .fixWordWrap(true)
                                .build(),
                                newDetails);
                    });
                }
            }

            // add the pickup or delivery note
            if (!pickupDeliveryNote.isEmpty()) {
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(pickupDeliveryNote)
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }

            // add an empty line
            createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                    .printerID(printerID)
                    .printStatusID(printStatusID)
                    .printerHostID(printerHostID)
                    .papluID(-1)
                    .quantity(-1.0d)
                    .isModifier(false)
                    .hideStation("")
                    .line(" ")
                    .fixWordWrap(true)
                    .build(),
                    newDetails);

        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to create the expeditor header details in " +
                    "KitchenPrinterReceiptFormatter.createExpeditorHeaderDetails", logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Create the header for remote order receipts.
     *
     * NOTE: WE ARE CURRENTLY USING THE THE EXPEDITOR HEADER FOR THIS. AT LEAST FOR NOW. IN CASE THIS CHANGES WE CAN MODIFY THIS METHOD.
     *
     * @param kpJobDetail {@link KPJobDetail} The KPJobDetail object which contains information for the receipt's header.
     * @param kpJobHeader {@link KPJobHeader} The KPJobHeader object which contains information for the receipt's header.
     * @param receiptData {@link ReceiptData} Data for the receipt.
     * @param headerData {@link ArrayList<String>} Standard header data.
     * @param newDetails {@link ArrayList<KPJobDetail>} The KPJobDetails to add to the print queue.
     */
    private void createRemoteOrderHeaderDetails (KPJobDetail kpJobDetail,
                                                 KPJobHeader kpJobHeader,
                                                 ReceiptData receiptData,
                                                 ArrayList<String> headerData,
                                                 ArrayList<KPJobDetail> newDetails) {
        createExpeditorHeaderDetails(kpJobDetail, kpJobHeader, receiptData, headerData, newDetails);
    }

    /**
     * Create the header for standard online order receipts.
     *
     * @param kpJobDetail {@link KPJobDetail} The KPJobDetail object which contains information for the receipt's header.
     * @param kpJobHeader {@link KPJobHeader} The KPJobHeader object which contains information for the receipt's header.
     * @param receiptData {@link ReceiptData} Data for the receipt.
     * @param useAddOn Whether or not to print the ADD ON header.
     * @param headerData {@link ArrayList<String>} Standard header data.
     * @param newDetails {@link ArrayList<KPJobDetail>} The KPJobDetails to add to the print queue.
     */
    @SuppressWarnings("ConstantConditions")
    private void createStandardOnlineOrderHeader (KPJobDetail kpJobDetail,
                                                  KPJobHeader kpJobHeader,
                                                  ReceiptData receiptData,
                                                  boolean useAddOn,
                                                  ArrayList<String> headerData,
                                                  ArrayList<KPJobDetail> newDetails) {

        try {
            int printerID = kpJobDetail.getPrinterID();
            String printerName = (StringFunctions.stringHasContent(kpJobDetail.getPrinterName()) ? kpJobDetail.getPrinterName() : "");
            int printerHostID = kpJobDetail.getPrinterHostID();
            String orderNumber = (StringFunctions.stringHasContent(receiptData.getOrderNumber()) ? receiptData.getOrderNumber() : "");
            int orderTypeID = kpJobHeader.getOrderTypeID();
            String personName = (StringFunctions.stringHasContent(kpJobHeader.getPersonName()) ? kpJobHeader.getPersonName() : "");
            String transComment = (StringFunctions.stringHasContent(kpJobHeader.getTransComment()) ? kpJobHeader.getTransComment() : "");
            String pickupDeliveryNote = (StringFunctions.stringHasContent(kpJobHeader.getPickupDeliveryNote()) ? kpJobHeader.getPickupDeliveryNote() : "");
            String deliveryLocation = (StringFunctions.stringHasContent(receiptData.getTxnData().getDeliveryLocation()) ? receiptData.getTxnData().getDeliveryLocation() : "");

            // add the standard header data
            if ((headerData != null) && (!headerData.isEmpty())) {
                headerData.forEach(line -> {
                    createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                            .printerID(printerID)
                            .printStatusID(printStatusID)
                            .printerHostID(printerHostID)
                            .papluID(-1)
                            .quantity(-1.0d)
                            .isModifier(false)
                            .hideStation("")
                            .line((StringFunctions.stringHasContent(line) ? line : ""))
                            .fixWordWrap(true)
                            .build(),
                            newDetails);
                });
            }

            // add printer name
            createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                    .printerID(printerID)
                    .printStatusID(printStatusID)
                    .printerHostID(printerHostID)
                    .papluID(-1)
                    .quantity(-1.0d)
                    .isModifier(false)
                    .hideStation("")
                    .line(JPOS_ALIGN_CENTER+printerName)
                    .fixWordWrap(true)
                    .build(),
                    newDetails);

            // add order number
            createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                    .printerID(printerID)
                    .printStatusID(printStatusID)
                    .printerHostID(printerHostID)
                    .papluID(-1)
                    .quantity(-1.0d)
                    .isModifier(false)
                    .hideStation("")
                    .line(JPOS_ALIGN_CENTER+JPOS_LARGE_FNT+orderNumber)
                    .fixWordWrap(true)
                    .build(),
                    newDetails);

            // check if we should print ADD ON
            if (useAddOn) {
                // add an empty line
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(JPOS_ALIGN_CENTER)
                        .fixWordWrap(true)
                        .build(),
                        newDetails);

                // add ADD ON
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(JPOS_ALIGN_CENTER+JPOS_LARGE_FNT+"**** ADD ON ****")
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }

            // add an empty line
            createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                    .printerID(printerID)
                    .printStatusID(printStatusID)
                    .printerHostID(printerHostID)
                    .papluID(-1)
                    .quantity(-1.0d)
                    .isModifier(false)
                    .hideStation("")
                    .line(" ")
                    .fixWordWrap(true)
                    .build(),
                    newDetails);

            // add the pickup or delivery note
            if (orderTypeID != TypeData.OrderType.NORMAL_SALE) {
                if (StringFunctions.stringHasContent(pickupDeliveryNote)) {
                    createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                            .printerID(printerID)
                            .printStatusID(printStatusID)
                            .printerHostID(printerHostID)
                            .papluID(-1)
                            .quantity(-1.0d)
                            .isModifier(false)
                            .hideStation("")
                            .line(JPOS_ALIGN_CENTER+JPOS_LARGE_FNT+pickupDeliveryNote)
                            .fixWordWrap(true)
                            .build(),
                            newDetails);
                }
                // add empty line
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(" ")
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }

            // add person name
            if (StringFunctions.stringHasContent(personName)) {

                // only add the customer name if it isn't the same as the transaction name
                boolean custNameNotEqualTxnName = true;
                if (StringFunctions.stringHasContent(kpJobHeader.getTransName()) && (kpJobHeader.getTransName().equalsIgnoreCase(kpJobHeader.getPersonName()))) {
                    custNameNotEqualTxnName = false;
                }

                if (custNameNotEqualTxnName) {
                    personName = KitchenPrinterCommon.correctAposAndQues(personName);
                    createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                                    .printerID(printerID)
                                    .printStatusID(printStatusID)
                                    .printerHostID(printerHostID)
                                    .papluID(-1)
                                    .quantity(-1.0d)
                                    .isModifier(false)
                                    .hideStation("")
                                    .line("Customer: "+personName)
                                    .fixWordWrap(true)
                                    .build(),
                            newDetails);
                }
            }

            // add delivery location
            if (StringFunctions.stringHasContent(deliveryLocation)) {
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(deliveryLocation)
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }

            // add transaction name
            if (StringFunctions.stringHasContent(kpJobHeader.getTransName())) {
                String label = "";
                if (StringFunctions.stringHasContent(kpJobHeader.getTransNameLabel())) {
                    label = kpJobHeader.getTransNameLabel()+ ": ";
                }
                String transName = KitchenPrinterCommon.correctAposAndQues(label + kpJobHeader.getTransName());
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line(transName)
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }

            // add transaction comment
            if (StringFunctions.stringHasContent(transComment)) {
                transComment = KitchenPrinterCommon.correctAposAndQues(transComment);
                createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                        .printerID(printerID)
                        .printStatusID(printStatusID)
                        .printerHostID(printerHostID)
                        .papluID(-1)
                        .quantity(-1.0d)
                        .isModifier(false)
                        .hideStation("")
                        .line("Comment: " + transComment)
                        .fixWordWrap(true)
                        .build(),
                        newDetails);
            }

            // add empty line
            createPrintQueueDetail(new PrintQueueDetail.PrintQueueDetailBuilder()
                    .printerID(printerID)
                    .printStatusID(printStatusID)
                    .printerHostID(printerHostID)
                    .papluID(-1)
                    .quantity(-1.0d)
                    .isModifier(false)
                    .hideStation("")
                    .line(" ")
                    .fixWordWrap(true)
                    .build(),
                    newDetails);

        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to create the standard online order header details in " +
                    "KitchenPrinterReceiptFormatter.createStandardOnlineOrderHeader", logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Updates the trans line status of the given products in the database.
     *
     * @param products {@link ArrayList<Plu>} Products to update the trans line status for.
     * @param newPrintStatusID The new trans line status ID.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private void updatePrinterQueueDetailStatus (ArrayList<Plu> products, int newPrintStatusID) {

        try {
            if ((products != null) && (!products.isEmpty())) {
                ArrayList<Integer> transLineItemIDs = products.stream().map(TransactionItem::getPATransLineItemID).collect(Collectors.toCollection(ArrayList::new));

                if (!transLineItemIDs.isEmpty()) {
                    if (dm.parameterizedExecuteNonQuery("data.newKitchenPrinter.UpdateTransLineStatusNew",
                            new Object[]{newPrintStatusID, DataFunctions.convertIntArrToStrForSQL(transLineItemIDs.stream().mapToInt(Integer::intValue).toArray())}) <= 0) {
                        throw new Exception("Unable to update the trans line status!!!");
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to update the trans line status of the products within " +
                    "the transaction in KitchenPrinterReceiptFormatter.updatePrinterQueueDetailStatus", logFileName,
                    Logger.LEVEL.ERROR);
        }

    }

    private boolean shouldOrigOrderNumHeaderPrint(Plu plu, boolean printOnlyNewItems, boolean printerIsExpediter, int printerHardwareTypeID, boolean hasPreviouslyPrintedProducts)
    {
        boolean printOrigOrderNumHeader = false;

        if (printerHardwareTypeID == TypeData.PrinterHardwareType.KDS)
        {
            printOrigOrderNumHeader = true;
        }
        else
        {
            String originalOrderNum = plu.getOriginalOrderNum();

            if ( (originalOrderNum.isEmpty() == false) || (originalOrderNum.isEmpty() && hasPreviouslyPrintedProducts && printOnlyNewItems == false) )
            {
                if (printOnlyNewItems == false || printerIsExpediter)
                {
                    // Print orig num header if product is from a prev order AND (Print Only New Items is OFF *OR* Printer Is Expediter)
                    printOrigOrderNumHeader = true;
                }
                else
                {
                    boolean prodWasPreviouslyPrinted = plu.hasBeenPrinted();

                    if (prodWasPreviouslyPrinted == false)
                    {
                        // Print orig num header if product is from a prev order AND was not previously printed
                        printOrigOrderNumHeader = true;
                    }
                }
            }
        }

        return printOrigOrderNumHeader;
    }

    /**
     * Determines whether or not to add an original order number sub-header.
     *
     * @param origOrderNum {@link String} The original order number.
     * @param orderNumber  {@link String} The current order number.
     * @return Whether or not to print an original order number sub-header.
     */
    private boolean shouldOrigOrderNumHeaderPrint (String origOrderNum, String orderNumber) {
        boolean printOrigOrderNumHeader = false;

        try {
            origOrderNum = (origOrderNum.isEmpty() ? orderNumber : origOrderNum);

            if ((!origOrderNum.equalsIgnoreCase(prevOrigOrderNumber)) || (prevOrigOrderNumber.isEmpty())) {
                printOrigOrderNumHeader = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine whether or not to add an original " +
                    "order number sub-header for the original order number "+Objects.toString(origOrderNum, "NULL") +
                    " and the current order number "+Objects.toString(orderNumber, "NULL")+" in " +
                    "KitchenPrinterReceiptFormatter.shouldOrigOrderNumHeaderPrint", logFileName, Logger.LEVEL.ERROR);
        }

        return printOrigOrderNumHeader;
    }

    /**
     * Determines whether or not to print an already sent header on the receipt.
     *
     * @param plu {@link Plu} The product to check if we should add an already sent header for.
     * @param printOnlyNewItems Whether or not the printer executing this KPJobDetail only prints new products.
     * @param printPrepReceipt Whether or not this header is for a prep printer receipt.
     * @param printExpeditorReceipt Whether or not this header is for an expeditor printer receipt.
     * @param printRemoteOrderReceipt Whether or not this header is for a remote order printer receipt.
     * @return Whether or not to print an already sent header on the receipt.
     */
    private boolean shouldAlreadySentHeaderPrint (Plu plu,
                                                  boolean printOnlyNewItems,
                                                  boolean printPrepReceipt,
                                                  boolean printExpeditorReceipt,
                                                  boolean printRemoteOrderReceipt) {
        boolean printAlreadySentHeader = false;

        try {
            boolean prodWasPreviouslyPrinted = plu.hasBeenPrinted();

            if ((printPrepReceipt) && (!printOnlyNewItems) && (prodWasPreviouslyPrinted)) {
                printAlreadySentHeader = true;
            }
            if (((printExpeditorReceipt) || (printRemoteOrderReceipt)) && (prodWasPreviouslyPrinted)) {
                printAlreadySentHeader = true;
            }

        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine whether or not to add an already sent " +
                    "header to the receipt for the product "+Objects.toString(plu.getName(), "NULL")+" in " +
                    "KitchenPrinterReceiptFormatter.shouldAlreadySentHeaderPrint", logFileName, Logger.LEVEL.ERROR);
        }

        return printAlreadySentHeader;
    }

    /**
     * Determines whether or not to print a new products header on the receipt.
     *
     * @param plu {@link Plu} The product to check if we should add a new product header for.
     * @param printOnlyNewItems Whether or not the printer executing this KPJobDetail only prints new products.
     * @param printPrepReceipt Whether or not this header is for a prep printer receipt.
     * @param printExpeditorReceipt Whether or not this header is for an expeditor printer receipt.
     * @param printRemoteOrderReceipt Whether or not this header is for a remote order printer receipt.
     * @return Whether or not to print a new products on the receipt.
     */
    private boolean shouldNewProdHeaderPrint (Plu plu,
                                              boolean printOnlyNewItems,
                                              boolean printPrepReceipt,
                                              boolean printExpeditorReceipt,
                                              boolean printRemoteOrderReceipt) {
        boolean printNewProductsHeader = false;

        try {
            boolean prodWasPreviouslyPrinted = plu.hasBeenPrinted();

            if ((printPrepReceipt) && (!printOnlyNewItems) && (!prodWasPreviouslyPrinted)) {
                printNewProductsHeader = true;
            }
            if (((printExpeditorReceipt) || (printRemoteOrderReceipt)) && (!prodWasPreviouslyPrinted)) {
                printNewProductsHeader = true;
            }

        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine whether or not to add a new product " +
                    "header to the receipt for the product "+Objects.toString(plu.getName(), "NULL")+" in " +
                    "KitchenPrinterReceiptFormatter.shouldAlreadySentHeaderPrint", logFileName, Logger.LEVEL.ERROR);
        }

        return printNewProductsHeader;
    }

    /**
     * Determines whether or not a new KPJobDetail should be created within the receipt and rendered.
     *
     * @param printPrepReceipt Whether or not this print jobs should print on a prep printer.
     * @param printOnExpeditor Whether or not the product in this print job should be printed on the expeditor.
     * @param noExpeditorTktsUnlessProdAdded Whether or not the expeditor printer will print additional open
     *         transaction products for with the show on expeditor setting turned on.
     * @param printExpeditorReceipt Whether or not an expeditor printer is configured and should print an expeditor receipt.
     * @param hasUnprintedExpeditorProducts Whether or not there are unprinted products that should be printed on the expeditor.
     * @param isTxnOnROT Whether or not the transaction was made on a remote order terminal.
     * @param printRemoteOrderReceipt Whether or not a remote order printer is configured and should print a remote
     *         order receipt.
     * @param printedInPreviousOrder Whether or not the product for this KPPrintJob detail was printed in a previous
     *         order within the transaction.
     * @param printOnlyNew Whether or not the configured prep printer only prints new products.
     * @param hasUnprintedProducts Whether or not there are unprinted products within the transaction.
     * @param printerHardwareTypeID The type of hardware the print job will be executed on.
     * @return Whether or not a new KPJobDetail should be created within the receipt and rendered.
     */
    @SuppressWarnings({"SpellCheckingInspection", "OverlyComplexMethod"})
    private boolean shouldCreateNewKPJobDetail (boolean printPrepReceipt,
                                                boolean printOnExpeditor,
                                                boolean noExpeditorTktsUnlessProdAdded,
                                                boolean printExpeditorReceipt,
                                                boolean hasUnprintedExpeditorProducts,
                                                boolean isTxnOnROT,
                                                boolean printRemoteOrderReceipt,
                                                boolean printedInPreviousOrder,
                                                boolean printOnlyNew,
                                                boolean hasUnprintedProducts,
                                                int printerHardwareTypeID) {
        boolean shouldCreateNewKPJobDetail = false;

        try {
            Logger.logMessage("PRINTPREPRECEIPT: "+(printPrepReceipt ? "Y" : "N"), logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("PRINTONEXPEDITOR: "+(printOnExpeditor ? "Y" : "N"), logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("NOEXPEDITORTKTSUNLESSPRODADDED: "+(noExpeditorTktsUnlessProdAdded ? "Y" : "N"), logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("PRINTEXPEDITORRECEIPT: "+(printExpeditorReceipt ? "Y" : "N"), logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("HASUNPRINTEDEXPEDITORPRODUCTS: "+(hasUnprintedExpeditorProducts ? "Y" : "N"), logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("ISTXNONROT: "+(isTxnOnROT ? "Y" : "N"), logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("PRINTREMOTEORDERRECEIPT: "+(printRemoteOrderReceipt ? "Y" : "N"), logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("PRINTEDINPREVIOUSORDER: "+(printedInPreviousOrder ? "Y" : "N"), logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("PRINTONLYNEW: "+(printOnlyNew ? "Y" : "N"), logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("HASUNPRINTEDPRODUCTS: "+(hasUnprintedProducts ? "Y" : "N"), logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("PRINTERHARDWARETYPEID: "+Objects.toString(printerHardwareTypeID, "NULL"), logFileName, Logger.LEVEL.DEBUG);

            if (printPrepReceipt) {
                if ((printOnlyNew) && (!printedInPreviousOrder)) {
                    shouldCreateNewKPJobDetail = true;
                }
                if (!printOnlyNew) {
                    shouldCreateNewKPJobDetail = true;
                }
            }
            else if (printExpeditorReceipt) {
                if (((printerHardwareTypeID == TypeData.PrinterHardwareType.PHYSICAL_PRINTER) && (noExpeditorTktsUnlessProdAdded) && (!printedInPreviousOrder) && (printOnExpeditor))
                        || ((printerHardwareTypeID == TypeData.PrinterHardwareType.PHYSICAL_PRINTER) && (noExpeditorTktsUnlessProdAdded) && (hasUnprintedExpeditorProducts))) {
                    shouldCreateNewKPJobDetail = true;
                }
                if ((printerHardwareTypeID == TypeData.PrinterHardwareType.PHYSICAL_PRINTER) && (!noExpeditorTktsUnlessProdAdded) && (printOnExpeditor)) {
                    shouldCreateNewKPJobDetail = true;
                }
                if ((printerHardwareTypeID == TypeData.PrinterHardwareType.KDS) && (!printedInPreviousOrder) && (printOnExpeditor)) {
                    shouldCreateNewKPJobDetail = true;
                }
            }
            else if (printRemoteOrderReceipt) {
                if ((printerHardwareTypeID == TypeData.PrinterHardwareType.PHYSICAL_PRINTER) && (isTxnOnROT)) {
                    shouldCreateNewKPJobDetail = true;
                }
                if ((printerHardwareTypeID == TypeData.PrinterHardwareType.KDS) && (!printedInPreviousOrder)) {
                    shouldCreateNewKPJobDetail = true;
                }
            }

            Logger.logMessage((shouldCreateNewKPJobDetail ? "A NEW KP JOB DETAIL WILL BE CREATED" : "NO NEW KP JOB DETAIL WILL BE CREATED"), logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("************** END DETERMINE SHOULD CREATE NEW KP JOB DETAIL **************", logFileName, Logger.LEVEL.DEBUG);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying determine whether or not to create a new " +
                    "KPOJobDetail with the following params PRINTPREPRECEIPT: %s, PRINTONEXPEDITOR: %s, " +
                    "NOEXPEDITORTKTSUNLESSPRODADDED: %s, PRINTEXPEDITORRECEIPT: %s, HASUNPRINTEDEXPEDITORPRODUCTS: %s, " +
                    "ISTXNONROT: %s, PRINTREMOTEORDERRECEIPT: %s, PRINTEDINPREVIOUSORDER: %s, PRINTONLYNEW: %s, " +
                    "HASUNPRINTEDPRODUCTS: %s and PRINTERHARDWARETYPEID: %s in " +
                    "KitchenPrinterReceiptFormatter.shouldCreateNewKPJobDetail",
                    Objects.toString((printPrepReceipt ? "true" : "false"), "NULL"),
                    Objects.toString((printOnExpeditor ? "true" : "false"), "NULL"),
                    Objects.toString((noExpeditorTktsUnlessProdAdded ? "true" : "false"), "NULL"),
                    Objects.toString((printExpeditorReceipt ? "true" : "false"), "NULL"),
                    Objects.toString((hasUnprintedExpeditorProducts ? "true" : "false"), "NULL"),
                    Objects.toString((isTxnOnROT ? "true" : "false"), "NULL"),
                    Objects.toString((printRemoteOrderReceipt ? "true" : "false"), "NULL"),
                    Objects.toString((printedInPreviousOrder ? "true" : "false"), "NULL"),
                    Objects.toString((printOnlyNew ? "true" : "false"), "NULL"),
                    Objects.toString((hasUnprintedProducts ? "true" : "false"), "NULL"),
                    Objects.toString(printerHardwareTypeID, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return shouldCreateNewKPJobDetail;
    }

    /**
     * Utility method to determine which order numbers should be displayed within the previous order numbers.
     *
     * @param receiptData {@link ReceiptData} Data for the receipt.
     * @param isTxnComplete Whether or not the transaction is complete.
     * @return {@link String} The order numbers to display under the previous order numbers on KDS.
     */
    @SuppressWarnings({"unchecked", "OverlyComplexMethod"})
    private String processPrintedOrderNums (ReceiptData receiptData, boolean isTxnComplete) {
        String prevOrderNumsToShowOnKDS = "";

        try {
            if (receiptData != null) {
                // set the transaction ID
                BigDecimal txnID = new BigDecimal(receiptData.getTxnData().getTransactionID());

                // check for existing transactions within this transactions chain i.e. previous transactions that are open
                String txnChain = "";
                // PATransactionID
                ArrayList<HashMap> queryRes = dm.parameterizedExecuteQuery("data.newKitchenPrinter.GetTxnsInChain", new Object[]{txnID}, true);
                if ((queryRes != null) && (!queryRes.isEmpty())) {
                   for (HashMap hm : queryRes) {
                       if ((hm != null) && (!hm.isEmpty()) && (hm.containsKey("PATRANSACTIONID")) && (!hm.get("PATRANSACTIONID").toString().isEmpty())) {
                           if (queryRes.indexOf(hm) == queryRes.size()-1) {
                               txnChain += hm.get("PATRANSACTIONID").toString();
                           }
                           else {
                               txnChain += hm.get("PATRANSACTIONID").toString()+",";
                           }
                       }
                   }
                }
                // log the transaction chain
                Logger.logMessage(String.format("CURRENT TRANSACTION ID: %s, TRANSACTIONS IN CHAIN: %s",
                        Objects.toString(txnID.toPlainString(), "NULL"),
                        Objects.toString(txnChain, "NULL")), logFileName, Logger.LEVEL.DEBUG);

                if (isTxnComplete) {
                    // get any previous order numbers that should be shown on KDS
                    if (!txnChain.isEmpty()) {
                        String[] txnsInChain = txnChain.split("\\s*,\\s*", -1);
                        if (txnsInChain.length > 0) {
                            // get the order number associated with the transaction from the printedOrderNumbersHM
                            String prevOrderNums = "";
                            for (int i = 0; i < txnsInChain.length; i++) {
                                BigDecimal aTxnID = new BigDecimal(txnsInChain[i]);
                                if (printedOrderNumbersHM.containsKey(aTxnID)) {
                                    prevOrderNums += printedOrderNumbersHM.get(aTxnID) + ",";
                                }
                            }
                            if (!prevOrderNums.isEmpty()) {
                                // remove trailing comma
                                prevOrderNums = prevOrderNums.substring(0, prevOrderNums.length() - 1);
                                prevOrderNumsToShowOnKDS = prevOrderNums;
                            }
                            // remove previous transactions in the chain from the printedOrderNumbersHM because they are no longer needed
                            printedOrderNumbersHM.entrySet().removeIf(entry -> Arrays.asList(txnsInChain).contains(entry.getKey().toPlainString()));
                        }
                    }
                }
                else {
                    // add this order number to the HashSet of printed order numbers
                    int thisOrderNumber = 0;
                    // if it's local order numbers are in the format of terminalID-orderNum, we only want the order number
                    if (receiptData.getOrderNumber().contains("-")) {
                        // get text after the hyphen
                        Pattern pattern = Pattern.compile("(?<=-).*");
                        Matcher matcher = pattern.matcher(receiptData.getOrderNumber());
                        if (matcher.find()) {
                            if (NumberUtils.isNumber(matcher.group(0))) {
                                thisOrderNumber = Integer.parseInt(matcher.group(0));
                            }
                        }
                    }
                    else if (NumberUtils.isNumber(receiptData.getOrderNumber())) {
                       thisOrderNumber = Integer.parseInt(receiptData.getOrderNumber());
                    }
                    if (thisOrderNumber > 0) {
                        printedOrderNumbersHM.put(txnID, thisOrderNumber);
                    }

                    // get any previous order numbers that should be shown on KDS
                    if (!txnChain.isEmpty()) {
                        String[] txnsInChain = txnChain.split("\\s*,\\s*", -1);
                        if (txnsInChain.length > 0) {
                            // get the order number associated with the transaction from the printedOrderNumbersHM
                            String prevOrderNums = "";
                            for (int i = 0; i < txnsInChain.length; i++) {
                                BigDecimal aTxnID = new BigDecimal(txnsInChain[i]);
                                if (printedOrderNumbersHM.containsKey(aTxnID)) {
                                    prevOrderNums += printedOrderNumbersHM.get(aTxnID) + ",";
                                }
                            }
                            if (!prevOrderNums.isEmpty()) {
                                // remove trailing comma
                                prevOrderNums = prevOrderNums.substring(0, prevOrderNums.length() - 1);
                                prevOrderNumsToShowOnKDS = prevOrderNums;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to determine which order numbers have " +
                    "already been printed for the transaction with an ID of %s in " +
                    "KitchenPrinterReceiptFormatter.processPrintedOrderNums",
                    Objects.toString(receiptData.getTxnData().getTransactionID(), "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return prevOrderNumsToShowOnKDS;
    }

    /**
     * Iterates through the products within the transaction and groups them based on whether they were printed or not.
     *
     * @param receiptData {@link ReceiptData} Data to print for the receipt.
     * @return {@link HashMap<String, ArrayList<Plu>>} Products within the transaction grouped based on whether they were printed or not.
     */
    @SuppressWarnings({"Convert2streamapi", "OverlyComplexMethod"})
    private HashMap<String, ArrayList<Plu>> sortKPReceiptGroups (ReceiptData receiptData) {
        HashMap<String, ArrayList<Plu>> sortedKPReceiptGroups = new HashMap<>();

        try {
            if ((receiptData != null) && (receiptData.getTxnData() != null) && (receiptData.getTxnData().getTransItems() != null)
                    && (!receiptData.getTxnData().getTransItems().isEmpty())) {
                ArrayList<Plu> productGroup;
                for (Object txnItem : receiptData.getTxnData().getTransItems()) {
                    if ((txnItem != null) && (txnItem instanceof Plu)) {
                        Plu product = ((Plu) txnItem);

                        String originalOrderNumber = product.getOriginalOrderNum();
                        // if the product is a modifier check if the parent product was from a previous order
                        if (product.getIsModifier()) {
                            Plu parent = product.getParentProduct();
                            if (parent != null) {
                                originalOrderNumber = parent.getOriginalOrderNum();
                            }
                        }
                        boolean productHasBeenPrinted = product.hasBeenPrinted();

                        boolean productPrintsOnExpeditor = product.getPrintsOnExpeditor();

                        if (originalOrderNumber.isEmpty()) {
                            if (sortedKPReceiptGroups.containsKey(TypeData.ProductKPReceiptGroupType.CURR_ORDER_PLU)) {
                                productGroup = sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.CURR_ORDER_PLU);
                            }
                            else {
                                productGroup = new ArrayList<>();
                            }
                            productGroup.add(product);
                            sortedKPReceiptGroups.put(TypeData.ProductKPReceiptGroupType.CURR_ORDER_PLU, productGroup);
                        }
                        else {
                            if (sortedKPReceiptGroups.containsKey(TypeData.ProductKPReceiptGroupType.PREV_ORDER_PLU)) {
                                productGroup = sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.PREV_ORDER_PLU);
                            }
                            else {
                                productGroup = new ArrayList<>();
                            }
                            productGroup.add(product);
                            sortedKPReceiptGroups.put(TypeData.ProductKPReceiptGroupType.PREV_ORDER_PLU, productGroup);
                        }

                        if (productHasBeenPrinted) {
                            if (sortedKPReceiptGroups.containsKey(TypeData.ProductKPReceiptGroupType.PRINTED_PLU)) {
                                productGroup = sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.PRINTED_PLU);
                            }
                            else {
                                productGroup = new ArrayList<>();
                            }
                            productGroup.add(product);
                            sortedKPReceiptGroups.put(TypeData.ProductKPReceiptGroupType.PRINTED_PLU, productGroup);
                        }
                        else {
                            if (productPrintsOnExpeditor) {
                                if (sortedKPReceiptGroups.containsKey(TypeData.ProductKPReceiptGroupType.UNPRINTED_EXPEDITOR_PLU)) {
                                    productGroup = sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.UNPRINTED_EXPEDITOR_PLU);
                                }
                                else {
                                    productGroup = new ArrayList<>();
                                }
                                productGroup.add(product);
                                sortedKPReceiptGroups.put(TypeData.ProductKPReceiptGroupType.UNPRINTED_EXPEDITOR_PLU, productGroup);
                            }

                            if (sortedKPReceiptGroups.containsKey(TypeData.ProductKPReceiptGroupType.UNPRINTED_PLU)) {
                                productGroup = sortedKPReceiptGroups.get(TypeData.ProductKPReceiptGroupType.UNPRINTED_PLU);
                            }
                            else {
                                productGroup = new ArrayList<>();
                            }
                            productGroup.add(product);
                            sortedKPReceiptGroups.put(TypeData.ProductKPReceiptGroupType.UNPRINTED_PLU, productGroup);
                        }
                    }
                }

                if (!sortedKPReceiptGroups.isEmpty()) {
                    Logger.logMessage("************** START SORT KP RECEIPT GROUP PLU **************", logFileName, Logger.LEVEL.DEBUG);
                    sortedKPReceiptGroups.forEach((k,v) -> {
                        Logger.logMessage("KP RECEIPT GROUP: "+Objects.toString(k, "NULL"), logFileName, Logger.LEVEL.DEBUG);
                        if ((v != null) && (!v.isEmpty())) {
                            v.forEach(plu -> {
                                Logger.logMessage("    PRODUCT: "+Objects.toString((((plu != null) && (plu.getName() != null)) ? plu.getName() : "NULL"), "NULL"), logFileName, Logger.LEVEL.DEBUG);
                            });
                        }
                    });
                    Logger.logMessage("************** END SORT KP RECEIPT GROUP PLU **************", logFileName, Logger.LEVEL.DEBUG);
                }

            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to sort the products within the transaction " +
                    "with an ID of %s into their respective KP Receipt Groups in " +
                    "KitchenPrinterReceiptFormatter.sortKPReceiptGroups",
                    Objects.toString(receiptData.getTxnData().getTransactionID(), "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return sortedKPReceiptGroups;
    }

    /**
     * Queries the database to determine whether or not the transaction has been completed.
     *
     * @param receiptData {@link ReceiptData} Data for the receipt.
     * @return Whether or not the transaction has been completed.
     */
    private boolean isTxnComplete (ReceiptData receiptData) {
        boolean isTxnComplete = false;

        try {
            if (receiptData != null) {
                // set the transaction ID
                BigDecimal txnID = new BigDecimal(receiptData.getTxnData().getTransactionID());

                // check if the transaction has been completed
                int txnStatusID = 0;
                Object queryRes = dm.parameterizedExecuteScalar("data.newKitchenPrinter.GetTxnStatus", new Object[]{txnID});
                if ((queryRes != null) && (NumberUtils.isNumber(queryRes.toString()))) {
                    txnStatusID = Integer.parseInt(queryRes.toString());
                }
                isTxnComplete = (txnStatusID == TypeData.TransactionStatus.COMPLETED);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the transaction " +
                    "with an ID of %s has been completed in KitchenPrinterReceiptFormatter.isTxnComplete",
                    Objects.toString(receiptData.getTxnData().getTransactionID(), "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return isTxnComplete;
    }

    /**
     * Queries the database to determine whether or not the transaction was made on a remote order terminal.
     *
     * @param receiptData {@link ReceiptData} Data for the receipt.
     * @return Whether or not the transaction was made on a remote order terminal.
     */
    private boolean isTxnOnROT (ReceiptData receiptData) {
        boolean txnOnROT = false;

        try {
            if (receiptData != null) {
                Object queryRes = dm.parameterizedExecuteScalar("data.newKitchenPrinter.GetRemoteTxnOnRemoteTerminal",
                        new Object[]{receiptData.getTxnData().getTerminalID()});
                if (queryRes != null) {
                    if (queryRes.toString().equalsIgnoreCase("1")) {
                        txnOnROT = true;
                    }
                    else if (queryRes.toString().equalsIgnoreCase("0")) {
                        txnOnROT = false;
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the transaction " +
                    "with an ID of %s was made on a remote order terminal in KitchenPrinterReceiptFormatter.isTxnInROT",
                    Objects.toString(receiptData.getTxnData().getTransactionID(), "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return txnOnROT;
    }

    /**
     * <p>Modifies the current {@link KPJobHeader} for transactions that were from a kiosk and have the "Send Orders Directly to Printer Host" setting checked.</p>
     *
     * @param receiptData The {@link ReceiptData} the {@link KPJobHeader} belongs to.
     * @return Modified {@link KPJobHeader} for transactions that were from a kiosk and have the "Send Orders Directly to Printer Host" setting checked.
     */
    @SuppressWarnings("unchecked")
    private KPJobHeader modifyKPJobHeaderForKioskTransactions (ReceiptData receiptData) {

        try {
            if ((receiptData != null) && (receiptData.getKpJobInfo() != null) && (receiptData.getKpJobInfo().getKpJobHeader() != null)) {
                KPJobHeader currKPJobHeader = receiptData.getKpJobInfo().getKpJobHeader();
                // determine whether or not we need to modify the KPJobHeader
                if (shouldModifyKPJobHeaderForKioskTransaction(currKPJobHeader.getPATransactionID())) {
                    Logger.logMessage(String.format("The transaction with an ID of %s was a transaction made on a kiosk " +
                            "with the \"Send Orders Directly to Printer Host\" setting enabled and will need to have it print queue header modified " +
                            "in KitchenPrinterReceiptFormatter.modifyKPJobHeaderForKioskTransactions",
                            Objects.toString(currKPJobHeader.getPATransactionID(), "NULL")), logFileName, Logger.LEVEL.DEBUG);

                    // query the database to get the information necessary to update the KPJobHeader
                    HashMap kpJobHeaderUpdatedInfo = new HashMap();
                    ArrayList<HashMap> queryRes = dm.parameterizedExecuteQuery("data.ReceiptGen.GetPickupDeliveryNoteInfo", new Object[]{currKPJobHeader.getPATransactionID()}, logFileName, true);
                    if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
                        kpJobHeaderUpdatedInfo = queryRes.get(0);
                    }

                    if (!DataFunctions.isEmptyMap(kpJobHeaderUpdatedInfo)) {
                        int orderTypeID = HashMapDataFns.getIntVal(kpJobHeaderUpdatedInfo, "PAORDERTYPEID");
                        // update the order type ID
                        if (orderTypeID > 0) {
                            currKPJobHeader.setIsOnlineOrder(true);
                            currKPJobHeader.setOrderTypeID(orderTypeID);
                        }
                        LocalDateTime transactionDate = new Timestamp(HashMapDataFns.getDateVal(kpJobHeaderUpdatedInfo, "TRANSACTIONDATE").getTime()).toLocalDateTime();
                        int prepTimeInMins = HashMapDataFns.getIntVal(kpJobHeaderUpdatedInfo, "PREPTIME");
                        String diningOption = HashMapDataFns.getStringVal(kpJobHeaderUpdatedInfo, "DININGOPTION");
                        String pickupDeliveryNote = buildPickupDeliveryNote(orderTypeID, transactionDate, prepTimeInMins, diningOption);
                        // update the pickup delivery note
                        if (StringFunctions.stringHasContent(pickupDeliveryNote)) {
                            currKPJobHeader.setPickupDeliveryNote(pickupDeliveryNote);
                        }
                        // return the updated KPJobHeader
                        return currKPJobHeader;
                    }
                }
                else {
                    // no return the current KPJobHeader
                    Logger.logMessage(String.format("The transaction with an ID of %s wasn't a transaction made on a kiosk " +
                            "with the \"Send Orders Directly to Printer Host\" setting enabled in KitchenPrinterReceiptFormatter.modifyKPJobHeaderForKioskTransactions",
                            Objects.toString(currKPJobHeader.getPATransactionID(), "NULL")), logFileName, Logger.LEVEL.DEBUG);
                    return currKPJobHeader;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the KPJobHeader would need to be " +
                    "modified for the transaction with an ID of %s in KitchenPrinterReceiptFormatter.modifyKPJobHeaderForKioskTransactions",
                    Objects.toString(receiptData.getKpJobInfo().getKpJobHeader().getPATransactionID(), "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return null;
    }

    /**
     * <p>Queries the database to determine whether or not the {@link KPJobHeader} for the given transaction ID should be modified.</p>
     *
     * @param paTransactionID ID of the transaction to check.
     * @return Whether or not the {@link KPJobHeader} for the given transaction ID should be modified.
     */
    private boolean shouldModifyKPJobHeaderForKioskTransaction (int paTransactionID) {
        boolean shouldModify = false;

        try {
            if (paTransactionID > 0) {
                Object queryRes = dm.parameterizedExecuteScalar("data.ReceiptGen.GetKioskTxnToModify", new Object[]{paTransactionID}, logFileName);
                if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (queryRes.toString().trim().equalsIgnoreCase("1"))) {
                    shouldModify = true;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the KPJobHeader for the transaction " +
                    "with an ID of %s should have it's KPJobHeader modified in KitchenPrinterReceiptFormatter.shouldModifyKPJobHeaderForKioskTransaction",
                    Objects.toString(paTransactionID, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return shouldModify;
    }

    /**
     * <p>Utility method to build a pickup delivery note for a transaction from a kiosk that was sent directly to the printer host.</p>
     *
     * @param orderTypeID Whether the order was pickup or delivery.
     * @param transactionDate The {@link LocalDateTime} of when the tranction was made.
     * @param prepTimeInMins Number of minutes it should take to prepare the order from the kiosk.
     * @param diningOption The name of dining option as a {@link String} if applicable.
     * @return A pickup delivery note {@link String} for a transaction from a kiosk that was sent directly to the printer host.
     */
    private String buildPickupDeliveryNote (int orderTypeID, LocalDateTime transactionDate, int prepTimeInMins, String diningOption) {
        String pickupDeliveryNote = "";

        try {
            Logger.logMessage(String.format("Now trying to build a pickup / delivery note given the params: ORDERTYPEID = %s, TRANSACTIONDATE = %s, " +
                    "PREPTIMEINMINUTES = %s and DININGOPTION = %s in KitchenPrinterReceiptFormatter.buildPickupDeliveryNote",
                    Objects.toString(orderTypeID, "N/A"),
                    Objects.toString(transactionDate, "N/A"),
                    Objects.toString(prepTimeInMins, "N/A"),
                    Objects.toString(diningOption, "N/A")), logFileName, Logger.LEVEL.DEBUG);

            if (transactionDate != null) {
                // convert ready time into AM/PM equivalent
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("h:mm a");

                if (orderTypeID == 2) {
                    // delivery
                    pickupDeliveryNote += "Has to be delivered at "+transactionDate.format(formatter)+".";
                }
                else if (orderTypeID == 3) {
                    // pickup
                    pickupDeliveryNote += "Will be picked up at "+transactionDate.format(formatter)+".";
                }

                // add the dining option if there is one
                if (StringFunctions.stringHasContent(diningOption)) {
                    pickupDeliveryNote += " \\n For "+diningOption+".";
                }
            }

            Logger.logMessage(String.format("A pickup / delivery note of %s has been generated in KitchenPrinterReceiptFormatter.buildPickupDeliveryNote",
                    Objects.toString(pickupDeliveryNote, "N/A")), logFileName, Logger.LEVEL.DEBUG);
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to build the pickup/delivery note with the " +
                    "params: ORDERTYPEID = %s, TRANSACTIONDATE = %s, PREPTIMEINMINUTES = %s and DININGOPTION = %s in KitchenPrinterReceiptFormatter.buildPickupDeliveryNote",
                    Objects.toString(orderTypeID, "N/A"),
                    Objects.toString(transactionDate, "N/A"),
                    Objects.toString(prepTimeInMins, "N/A"),
                    Objects.toString(diningOption, "N/A")), logFileName, Logger.LEVEL.ERROR);
        }

        return pickupDeliveryNote;
    }

    //--------------------------------------------------------------------------
    //  String formatting methods
    //--------------------------------------------------------------------------

    /**
     * Takes two Strings and formats them on the receipt so that one String appears left justified and the
     * other is right justified.
     *
     * @param left {@link String} The left justified String.
     * @param right {@link String} The right justified String.
     * @return {@link String} The new line with the two given Strings formatted as being left and right justified.
     */
    private String twoColumn (String left, String right) {
        String line = "";

        try {
            // determine the width of each column on the receipt
            int leftWidth = ((MAX_PRINT_CHARS_SMALL_FONT * 2) / 3);
            int rightWidth = (MAX_PRINT_CHARS_SMALL_FONT - leftWidth);

            if (left.length() > leftWidth) {
                left = left.substring(0, leftWidth);
            }
            if (right.length() > rightWidth) {
                right = right.substring(0, rightWidth - 1);
            }

            line = String.format("%s"+JPOS_ALIGN_RIGHT+"%s", left, right);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to left justify the String %s and right " +
                    "justify the String %s on the receipt in KitchenPrinterReceiptFormatter.twoColumn",
                    Objects.toString(left, "NULL"),
                    Objects.toString(right, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return line;
    }

    /**
     * Converts a String to be base 64 encoded.
     *
     * @param input {@link String} The String to encode.
     * @return {@link String} The base 64 encoded String.
     */
    private String encode (String input) {
        String encodedInput = "";

        try {
            encodedInput = Base64.encodeBase64String(input.getBytes());
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying encode the String "+Objects.toString(input, "NULL")+" to " +
                    "base 64 in KitchenPrinterReceiptFormatter.encode", logFileName, Logger.LEVEL.ERROR);
        }

        return encodedInput;
    }

    /**
     * Removes JPOS commands from the given line.
     *
     * @param line {@link String} The line to purge of JPOS commands.
     * @return {@link String} The line with JPOS commands removed.
     */
    @SuppressWarnings("SpellCheckingInspection")
    private String purgeJPOSCommands (String line) {
        String purgedLine = "";

        try {
            purgedLine = line;
            purgedLine = purgedLine.replace("\u001B", "");
            purgedLine = purgedLine.replace("|cA", "");
            purgedLine = purgedLine.replace("|rA", "");
            purgedLine = purgedLine.replace("|4C", "");
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to purge JPOS commands from the String " +
                    Objects.toString(line, "NULL")+" in KitchenPrinterReceiptFormatter.purgeJPOSCommands",
                    logFileName, Logger.LEVEL.ERROR);
        }

        return purgedLine;
    }

    /**
     * Adds JPOS commands to the line based on the given boolean flags.
     *
     * @param line {@link String} The line to add JPOS commands to.
     * @param isCentered Whether or not the line should be centered.
     * @param isRightJustified Whether or not the line should be right justified.
     * @param isLargeFont Whether or not the line should be printed in large font.
     * @return {@link String} The line with any applicable JPOS commands.
     */
    @SuppressWarnings("SpellCheckingInspection")
    private String addJPOSCommands (String line, boolean isCentered, boolean isRightJustified, boolean isLargeFont) {
        String formattedLine = "";

        Logger.logMessage(String.format("ADDING JPOS COMMANDS BACK IN FOR THE LINE: %s, ISCENTERED: %s, " +
                "ISRIGHTJUSTIFIED: %s, ISLARGEFONT: %s",
                Objects.toString(line, "NULL"),
                Objects.toString((isCentered ? "Y" : "N"), "NULL"),
                Objects.toString((isRightJustified ? "Y" : "N"), "NULL"),
                Objects.toString((isLargeFont ? "Y" : "N"), "NULL")), logFileName, Logger.LEVEL.DEBUG);

        try {
            String jposCommands = "";
            if (isCentered) {
                jposCommands += JPOS_ALIGN_CENTER;
            }
            if (isRightJustified) {
                jposCommands += JPOS_ALIGN_RIGHT;
            }
            if (isLargeFont) {
                jposCommands += JPOS_LARGE_FNT;
            }
            formattedLine = jposCommands + line;
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to add applicable JPOS commands for the line " +
                    Objects.toString(line, "NULL")+" in KitchenPrinterReceiptFormatter.addJPOSCommands",
                    logFileName, Logger.LEVEL.ERROR);
        }

        Logger.logMessage("FORMATTED LINE "+formattedLine, logFileName, Logger.LEVEL.DEBUG);

        return formattedLine;
    }

    /**
     * Splits the line on the receipt into multiple lines, if it is longer than the maximum number of characters.
     *
     * @param line {@link String} The line to check if it should be split.
     * @return {@link ArrayList<String>} The possibly split line into multiple lines, if it wasn't split the ArrayList
     *         will only contain the given line.
     */
    @SuppressWarnings({"OverlyComplexMethod", "SpellCheckingInspection"})
    private ArrayList<String> fixWordWrap (String line) {
        ArrayList<String> newLines = new ArrayList<>();

        try {
            boolean isCentered = false;
            boolean isRightJustified = false;
            boolean isLargeFont = false;
            int maxCharsForLine = MAX_PRINT_CHARS_SMALL_FONT;

            // check for JPOS commands within the line
            if (line.contains(JPOS_ALIGN_CENTER)) {
                isCentered = true;
            }
            if (line.contains(JPOS_ALIGN_RIGHT)) {
                isRightJustified = true;
            }
            if (line.contains(JPOS_LARGE_FNT)) {
                isLargeFont = true;
                maxCharsForLine = MAX_PRINT_CHARS_LARGE_FONT;
            }

            // remove any JPOS commands from the line because they don't contribute to the length of the line
            String unformattedLine = purgeJPOSCommands(line);
            ArrayList<String> unformattedLines = new ArrayList<>();
            // check for explicitly defined new line
            if (unformattedLine.contains("\\n")) {
                unformattedLine = unformattedLine.replace("\\n", "%%%");
                unformattedLines = new ArrayList<>(Arrays.asList(unformattedLine.split("%%%")));
            }
            // check for line shorter than the max number of characters
            else if (unformattedLine.length() < maxCharsForLine) {
                newLines.add(line);
            }
            else {
                unformattedLines.add(unformattedLine);
            }

            // determine where to split the lines that are too long
            if (!unformattedLines.isEmpty()) {
                for (String currUnformattedLine : unformattedLines) {
                    if (currUnformattedLine != null) {
                        // keep trying to split up the currUnformattedLine until we are no longer able to
                        while (!currUnformattedLine.isEmpty()) {
                            // make sure there isn't a String that's only a single space
                            if ((currUnformattedLine.length() == 1) && (currUnformattedLine.equals(" "))) {
                                currUnformattedLine = "";
                            }

                            String newLine;
                            if (currUnformattedLine.length() > maxCharsForLine) {
                                // try to find a place to split the line
                                String tempSplit = currUnformattedLine.substring(0, maxCharsForLine - 1);
                                // try to split the tempSplit at the last space in the String
                                int splitIndex = tempSplit.lastIndexOf(" ");

                                // split the temp split at the split index
                                if (splitIndex != -1) {
                                    newLine = currUnformattedLine.substring(0, splitIndex);
                                    // add the JPOS commands back into the line
                                    newLine = addJPOSCommands(newLine, isCentered, isRightJustified, isLargeFont);
                                    newLines.add(newLine);
                                    // set the currUnformattedLine to whatever is left after the split
                                    currUnformattedLine = currUnformattedLine.substring(splitIndex).trim();
                                }
                                // no split index found, so we can add the temp split to the new lines
                                else {
                                    newLine = currUnformattedLine;
                                    // add the JPOS commands back into the line
                                    newLine = addJPOSCommands(newLine, isCentered, isRightJustified, isLargeFont);
                                    newLines.add(newLine);
                                    currUnformattedLine = "";
                                }
                            }
                            // the line is shorter than the max so we can add it to the new lines
                            else {
                                newLine = currUnformattedLine;
                                // add the JPOS commands back into the line
                                newLine = addJPOSCommands(newLine, isCentered, isRightJustified, isLargeFont);
                                newLines.add(newLine);
                                currUnformattedLine = "";
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to format the line "+Objects.toString(line, "NULL")+" to " +
                    "prevent word wrap in KitchenPrinterReceiptFormatter.fixWordWrap",
                    logFileName, Logger.LEVEL.ERROR);
        }

        return newLines;
    }

}
