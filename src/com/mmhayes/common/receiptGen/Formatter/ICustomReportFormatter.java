package com.mmhayes.common.receiptGen.Formatter;

import com.mmhayes.common.receiptGen.ReceiptData.IReportData;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;

/**
 * Created by nyu on 8/3/2017.
 */
public interface ICustomReportFormatter
{
    void setRenderer(IRenderer i);

    IRenderer getRenderer();

    void logError(String error, Exception e);

    boolean buildReport(IReportData reportData);
}
