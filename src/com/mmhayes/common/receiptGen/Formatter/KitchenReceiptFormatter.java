package com.mmhayes.common.receiptGen.Formatter;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-27 16:48:53 -0400 (Wed, 27 May 2020) $: Date of last commit
    $Rev: 11826 $: Revision of last commit
    Notes: Generates receipts for KP/KDS/KMS.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.Formatter.kitchenReceipt.IReceipt;
import com.mmhayes.common.receiptGen.Formatter.kitchenReceipt.ReceiptFactory;
import com.mmhayes.common.receiptGen.ReceiptData.ReceiptData;
import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.detail.IPrintJobDetail;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.ITransLineItem;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.Product;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>Generates receipts for KP/KDS/KMS.</p>
 *
 */
public class KitchenReceiptFormatter implements ICustomReceiptFormatter {

    // log file where information and errors related to this class should be logged
    private final String KITCHEN_RECEIPT_FORMATTER_LOG = "KitchenReceiptFormatter.log";

    // private member variables of a KitchenReceiptFormatter
    private DataManager dataManager;
    private IRenderer renderer;

    /**
     * <p>Constructor for a {@link KitchenReceiptFormatter}.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     */
    public KitchenReceiptFormatter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    /**
     * <p>Sets the renderer to be used to build the kitchen printer/display receipt for a transaction.</p>
     *
     * @param renderer The {@link IRenderer} to be used to build the kitchen printer/display receipt for a transaction.
     */
    @Override
    public void setRenderer (IRenderer renderer) {
        this.renderer = renderer;
    }

    /**
     * <p>Gets the renderer to be used to build the kitchen printer/display receipt for a transaction.</p>
     *
     * @return The {@link IRenderer} to be used to build the kitchen printer/display receipt for a transaction.
     */
    @Override
    public IRenderer getRenderer () {
        return renderer;
    }

    /**
     * <p>Logs the given error {@link String} and {@link Exception}.</p>
     *
     * @param error The error {@link String} to log.
     * @param e The {@link Exception} to log.
     */
    @Override
    public void logError (String error, Exception e) {
        ReceiptGen.logError(error, e);
    }

    /**
     * <p>Builds the receipt from the given {@link ReceiptData}.</p>
     *
     * @param receiptData The {@link ReceiptData} to use to build the receipt.
     * @return Whether or not the receipt could be built successfully.
     */
    @Override
    public boolean buildReceipt (ReceiptData receiptData) {
        boolean receiptBuiltSuccessfully = false;

        try {
            // make sure we have a valid DataManager
            if (!isDataManagerValid()) {
                Logger.logMessage("The DataManager passed to KitchenReceiptFormatter.buildReceipt is null, unable to build the receipt!", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
                return false;
            }

            // make sure we have a valid renderer
            if (!isRendererValid()) {
                Logger.logMessage("The IRenderer passed to KitchenReceiptFormatter.buildReceipt is null, unable to build the receipt!", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
                return false;
            }

            // make sure the renderer can actually build the receipt
            if (!isRendererAbleToRender()) {
                Logger.logMessage("The IRenderer passed to KitchenReceiptFormatter.buildReceipt isn't null but is unable to build the receipt!", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
                return false;
            }

            // make sure there is information to build the receipt
            if (!isTherePrintJobInfo(receiptData)) {
                Logger.logMessage("No print job information found in KitchenReceiptFormatter.buildReceipt, unable to build the receipt!", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
                return false;
            }

            // sort the products in the transactions based on whether or not they have been printed/displayed
            HashMap<String, ArrayList<Product>> sortedProducts = sortProductsInTransactionBasedOnStatus(receiptData);

            // use the sorted products to determine some information about the products within the transaction
            boolean hasPrintedOrDisplayedProducts = hasPrintedOrDisplayedProducts(sortedProducts);
            boolean hasUnprintedOrUndisplayedProducts = hasUnprintedOrUndisplayedProducts(sortedProducts);
            boolean hasUnprintedOrUndisplayedExpeditorProducts = hasUnprintedOrUndisplayedExpeditorProducts(sortedProducts);
            boolean hasProductsFromPreviousOrder = hasProductsFromPreviousOrder(sortedProducts);
            boolean hasProductsFromCurrentOrder = hasProductsFromCurrentOrder(sortedProducts);

            if ((hasPrintedOrDisplayedProducts) || (hasUnprintedOrUndisplayedProducts)) {
                HashMap<String, HashMap<Integer, ArrayList<IPrintJobDetail>>> sortedPrintJobDetails = receiptData.getPrintJobInfo().sortPrintJobDetails();
                if (!DataFunctions.isEmptyMap(sortedPrintJobDetails)) {
                    ReceiptFactory receiptFactory = new ReceiptFactory();
                    for (Map.Entry<String, HashMap<Integer, ArrayList<IPrintJobDetail>>> sortedPrintJobDetailsByTypeEntry : sortedPrintJobDetails.entrySet()) {
                        String kitchenReceiptType = sortedPrintJobDetailsByTypeEntry.getKey();
                        HashMap<Integer, ArrayList<IPrintJobDetail>> sortedPrintJobDetailsByPrinterOrStation = sortedPrintJobDetailsByTypeEntry.getValue();
                        if ((StringFunctions.stringHasContent(kitchenReceiptType)) && (!DataFunctions.isEmptyMap(sortedPrintJobDetailsByPrinterOrStation))) {
                            for (Map.Entry<Integer, ArrayList<IPrintJobDetail>> printJobDetailsForPrinterOrStationEntry : sortedPrintJobDetailsByPrinterOrStation.entrySet()) {
                                ArrayList<IPrintJobDetail> printJobDetails = printJobDetailsForPrinterOrStationEntry.getValue();
                                if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                                    if (kitchenReceiptType.equalsIgnoreCase(TypeData.KitchenReceiptType.KP)) {
                                        // create a kitchen printer receipt
                                        IReceipt kpReceipt = receiptFactory.getReceipt(KITCHEN_RECEIPT_FORMATTER_LOG, TypeData.KitchenReceiptType.KP);
                                        kpReceipt.buildReceipt(KITCHEN_RECEIPT_FORMATTER_LOG, renderer, receiptData, printJobDetails, hasPrintedOrDisplayedProducts,
                                                hasUnprintedOrUndisplayedProducts, hasUnprintedOrUndisplayedExpeditorProducts, hasProductsFromPreviousOrder, hasProductsFromCurrentOrder);
                                    }
                                    else if (kitchenReceiptType.equalsIgnoreCase(TypeData.KitchenReceiptType.KDS)) {
                                        // create a kitchen display system receipt
                                        IReceipt kdsReceipt = receiptFactory.getReceipt(KITCHEN_RECEIPT_FORMATTER_LOG, TypeData.KitchenReceiptType.KDS);
                                        kdsReceipt.buildReceipt(KITCHEN_RECEIPT_FORMATTER_LOG, renderer, receiptData, printJobDetails, hasPrintedOrDisplayedProducts,
                                                hasUnprintedOrUndisplayedProducts, hasUnprintedOrUndisplayedExpeditorProducts, hasProductsFromPreviousOrder, hasProductsFromCurrentOrder);
                                    }
                                    else if (kitchenReceiptType.equalsIgnoreCase(TypeData.KitchenReceiptType.KMS)) {
                                        // create a kitchen management system receipt
                                        IReceipt kmsReceipt = receiptFactory.getReceipt(KITCHEN_RECEIPT_FORMATTER_LOG, TypeData.KitchenReceiptType.KMS);
                                        kmsReceipt.buildReceipt(KITCHEN_RECEIPT_FORMATTER_LOG, renderer, receiptData, printJobDetails, hasPrintedOrDisplayedProducts,
                                                hasUnprintedOrUndisplayedProducts, hasUnprintedOrUndisplayedExpeditorProducts, hasProductsFromPreviousOrder, hasProductsFromCurrentOrder);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            receiptBuiltSuccessfully = true;
        }
        catch (Exception e) {
            Logger.logException(e, KITCHEN_RECEIPT_FORMATTER_LOG);
            Logger.logMessage("An error occurred while trying to build the receipt for a kitchen printer/display in KitchenReceiptFormatter.buildReceipt", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
        }

        return receiptBuiltSuccessfully;
    }

    /**
     * <p>Determines whether or not the {@link DataManager} to be used by the {@link KitchenReceiptFormatter} is valid or not.</p>
     *
     * @return Whether or not the {@link DataManager} to be used by the {@link KitchenReceiptFormatter} is valid or not.
     */
    private boolean isDataManagerValid () {
        return (dataManager != null);
    }

    /**
     * <p>Determines whether or not the {@link IRenderer} to be used by the {@link KitchenReceiptFormatter} is valid or not.</p>
     *
     * @return Whether or not the {@link IRenderer} to be used by the {@link KitchenReceiptFormatter} is valid or not.
     */
    private boolean isRendererValid () {
        return (renderer != null);
    }

    /**
     * <p>Determines whether or not the {@link IRenderer} to be used by the {@link KitchenReceiptFormatter} can build the receipt.</p>
     *
     * @return Whether or not the {@link IRenderer} to be used by the {@link KitchenReceiptFormatter} can build the receipt.
     */
    private boolean isRendererAbleToRender () {
        return (renderer.begin());
    }

    /**
     * <p>Determines whether or not there is a receipt to be built from the print job within the given {@link ReceiptData}.</p>
     *
     * @param receiptData The {@link ReceiptData} to use to get the print job stored within.
     * @return Whether or not there is a receipt to be built from the print job within the given {@link ReceiptData}.
     */
    private boolean isTherePrintJobInfo (ReceiptData receiptData) {
        return ((receiptData != null) && (receiptData.getPrintJobInfo() != null));
    }

    /**
     * <p>Sorts the products within the transaction stored in the given {@link ReceiptData} based on whether or not they have been printed.</p>
     *
     * @param receiptData The {@link ReceiptData} which contaiins the transaction to sort the products within.
     * @return A {@link HashMap} whose key {@link String} is the ProductPrintDisplayStatus and whose value is an {@link ArrayList} of {@link Product} who share that status.
     */
    @SuppressWarnings("Convert2streamapi")
    private HashMap<String, ArrayList<Product>> sortProductsInTransactionBasedOnStatus (ReceiptData receiptData) {
        HashMap<String, ArrayList<Product>> sortedProducts = new HashMap<>();
        ArrayList<Product> productGroup;

        try {
            // make sure we have valid ReceiptData
            if (receiptData == null) {
                Logger.logMessage("The ReceiptData passed to KitchenReceiptFormatter.sortProductsInTransactionBasedOnStatus was null, unable " +
                        "to sort the products within the transaction based on their print or display status!", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have valid TransactionData
            if (receiptData.getTxnData() == null) {
                Logger.logMessage("The TransactionData found within the ReceiptData passed to KitchenReceiptFormatter.sortProductsInTransactionBasedOnStatus was null, unable " +
                        "to sort the products within the transaction based on their print or display status!", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have transaction line items
            if (DataFunctions.isEmptyCollection(receiptData.getTxnData().getTxnLineItems())) {
                Logger.logMessage("Np transaction line items found in KitchenReceiptFormatter.sortProductsInTransactionBasedOnStatus, unable " +
                        "to sort the products within the transaction based on their print or display status!", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
                return null;
            }

            for (ITransLineItem transLineItem : receiptData.getTxnData().getTxnLineItems()) {
                if ((transLineItem != null) && (transLineItem instanceof Product)) {
                    Product product = ((Product) transLineItem);

                    String originalOrderNumber = product.getOriginalOrderNumber();
                    if (product.getIsModifier()) {
                        Product parentProduct = product.getParentProduct();
                        if (parentProduct != null) {
                            originalOrderNumber = parentProduct.getOriginalOrderNumber();
                        }
                    }

                    boolean productHasBeenPrintedOrDisplayed = product.hasBeenPrintedOrDisplayed();
                    boolean showProductOnExpeditor = product.getPrintsOnExpeditor();

                    // product is part of the current order
                    if (!StringFunctions.stringHasContent(originalOrderNumber)) {
                        if (sortedProducts.containsKey(TypeData.ProductPrintDisplayStatus.CURRENT_ORDER)) {
                            productGroup = sortedProducts.get(TypeData.ProductPrintDisplayStatus.CURRENT_ORDER);
                        }
                        else {
                            productGroup = new ArrayList<>();
                        }
                        productGroup.add(product);
                        sortedProducts.put(TypeData.ProductPrintDisplayStatus.CURRENT_ORDER, productGroup);
                    }
                    // product is from a previous order
                    else {
                        if (sortedProducts.containsKey(TypeData.ProductPrintDisplayStatus.PREVIOUS_ORDER)) {
                            productGroup = sortedProducts.get(TypeData.ProductPrintDisplayStatus.PREVIOUS_ORDER);
                        }
                        else {
                            productGroup = new ArrayList<>();
                        }
                        productGroup.add(product);
                        sortedProducts.put(TypeData.ProductPrintDisplayStatus.PREVIOUS_ORDER, productGroup);
                    }

                    if (productHasBeenPrintedOrDisplayed) {
                        if (sortedProducts.containsKey(TypeData.ProductPrintDisplayStatus.PRINTED)) {
                            productGroup = sortedProducts.get(TypeData.ProductPrintDisplayStatus.PRINTED);
                        }
                        else {
                            productGroup = new ArrayList<>();
                        }
                        productGroup.add(product);
                        sortedProducts.put(TypeData.ProductPrintDisplayStatus.PRINTED, productGroup);
                    }
                    else {
                        // check for unprinted or undisplayed expeditor products
                        if (showProductOnExpeditor) {
                            if (sortedProducts.containsKey(TypeData.ProductPrintDisplayStatus.UNPRINTED_EXPEDITOR)) {
                                productGroup = sortedProducts.get(TypeData.ProductPrintDisplayStatus.UNPRINTED_EXPEDITOR);
                            }
                            else {
                                productGroup = new ArrayList<>();
                            }
                            productGroup.add(product);
                            sortedProducts.put(TypeData.ProductPrintDisplayStatus.UNPRINTED_EXPEDITOR, productGroup);
                        }
                        
                        if (sortedProducts.containsKey(TypeData.ProductPrintDisplayStatus.UNPRINTED)) {
                            productGroup = sortedProducts.get(TypeData.ProductPrintDisplayStatus.UNPRINTED);
                        }
                        else {
                            productGroup = new ArrayList<>();
                        }
                        productGroup.add(product);
                        sortedProducts.put(TypeData.ProductPrintDisplayStatus.UNPRINTED, productGroup);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, KITCHEN_RECEIPT_FORMATTER_LOG);
            // determine the ID of the transaction
            BigDecimal paTransactionID = null;
            if ((receiptData != null) && (receiptData.getTxnData() != null) && (receiptData.getTxnData().getTxnHdrData() != null)) {
                paTransactionID = receiptData.getTxnData().getTxnHdrData().getPaTransactionID();
            }
            Logger.logMessage(String.format("There was a problem trying to sort the product line items within the transaction with " +
                    "an ID of %s based on whether or not they have been printed in KitchenReceiptFormatter.sortProductsInTransactionBasedOnStatus",
                    Objects.toString((paTransactionID != null ? paTransactionID.toPlainString() : "N/A"), "N/A")), KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
        }

        return sortedProducts;
    }

    /**
     * <p>Determines whether or not there are products that have been printed or displayed in the given sorted products.</p>
     *
     * @param sortedProducts A {@link HashMap} whose key {@link String} is the ProductPrintDisplayStatus and whose value is an {@link ArrayList} of {@link Product} who share that status.
     * @return Whether or not there are products that have been printed or displayed in the given sorted products.
     */
    private boolean hasPrintedOrDisplayedProducts (HashMap<String, ArrayList<Product>> sortedProducts) {
        boolean hasPrintedOrDisplayedProducts = false;

        if (!DataFunctions.isEmptyMap(sortedProducts)) {
            Logger.logMessage("The sorted products passed to KitchenReceiptFormatter.hasPrintedOrDisplayedProducts can't be null or empty!", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        if ((sortedProducts.containsKey(TypeData.ProductPrintDisplayStatus.PRINTED)) && (!DataFunctions.isEmptyCollection(sortedProducts.get(TypeData.ProductPrintDisplayStatus.PRINTED)))) {
            hasPrintedOrDisplayedProducts = true;
        }

        return hasPrintedOrDisplayedProducts;
    }

    /**
     * <p>Determines whether or not there are products that are unprinted or undisplayed in the given sorted products.</p>
     *
     * @param sortedProducts A {@link HashMap} whose key {@link String} is the ProductPrintDisplayStatus and whose value is an {@link ArrayList} of {@link Product} who share that status.
     * @return Whether or not there are products that are unprinted or undisplayed in the given sorted products.
     */
    private boolean hasUnprintedOrUndisplayedProducts (HashMap<String, ArrayList<Product>> sortedProducts) {
        boolean hasUnprintedOrUndisplayedProducts = false;

        if (!DataFunctions.isEmptyMap(sortedProducts)) {
            Logger.logMessage("The sorted products passed to KitchenReceiptFormatter.hasUnprintedOrUndisplayedProducts can't be null or empty!", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        if ((sortedProducts.containsKey(TypeData.ProductPrintDisplayStatus.UNPRINTED)) && (!DataFunctions.isEmptyCollection(sortedProducts.get(TypeData.ProductPrintDisplayStatus.UNPRINTED)))) {
            hasUnprintedOrUndisplayedProducts = true;
        }

        return hasUnprintedOrUndisplayedProducts;
    }

    /**
     * <p>Determines whether or not there are expeditor products that are unprinted or undisplayed in the given sorted products.</p>
     *
     * @param sortedProducts A {@link HashMap} whose key {@link String} is the ProductPrintDisplayStatus and whose value is an {@link ArrayList} of {@link Product} who share that status.
     * @return Whether or not there are expeditor products that are unprinted or undisplayed in the given sorted products.
     */
    private boolean hasUnprintedOrUndisplayedExpeditorProducts (HashMap<String, ArrayList<Product>> sortedProducts) {
        boolean hasUnprintedOrUndisplayedExpeditorProducts = false;

        if (!DataFunctions.isEmptyMap(sortedProducts)) {
            Logger.logMessage("The sorted products passed to KitchenReceiptFormatter.hasUnprintedOrUndisplayedExpeditorProducts can't be null or empty!", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        if ((sortedProducts.containsKey(TypeData.ProductPrintDisplayStatus.UNPRINTED_EXPEDITOR)) && (!DataFunctions.isEmptyCollection(sortedProducts.get(TypeData.ProductPrintDisplayStatus.UNPRINTED_EXPEDITOR)))) {
            hasUnprintedOrUndisplayedExpeditorProducts = true;
        }

        return hasUnprintedOrUndisplayedExpeditorProducts;
    }

    /**
     * <p>Determines whether or not there are products from the previous order in the given sorted products.</p>
     *
     * @param sortedProducts A {@link HashMap} whose key {@link String} is the ProductPrintDisplayStatus and whose value is an {@link ArrayList} of {@link Product} who share that status.
     * @return Whether or not there are products from the previous order in the given sorted products.
     */
    private boolean hasProductsFromPreviousOrder (HashMap<String, ArrayList<Product>> sortedProducts) {
        boolean hasProductsFromPreviousOrder = false;

        if (!DataFunctions.isEmptyMap(sortedProducts)) {
            Logger.logMessage("The sorted products passed to KitchenReceiptFormatter.hasProductsFromPreviousOrder can't be null or empty!", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        if ((sortedProducts.containsKey(TypeData.ProductPrintDisplayStatus.PREVIOUS_ORDER)) && (!DataFunctions.isEmptyCollection(sortedProducts.get(TypeData.ProductPrintDisplayStatus.PREVIOUS_ORDER)))) {
            hasProductsFromPreviousOrder = true;
        }

        return hasProductsFromPreviousOrder;
    }

    /**
     *  <p>Determines whether or not there are products from the current order in the given sorted products.</p>
     *
     * @param sortedProducts A {@link HashMap} whose key {@link String} is the ProductPrintDisplayStatus and whose value is an {@link ArrayList} of {@link Product} who share that status.
     * @return Whether or not there are products from the current order in the given sorted products.
     */
    private boolean hasProductsFromCurrentOrder (HashMap<String, ArrayList<Product>> sortedProducts) {
        boolean hasProductsFromCurrentOrder = false;

        if (!DataFunctions.isEmptyMap(sortedProducts)) {
            Logger.logMessage("The sorted products passed to KitchenReceiptFormatter.hasProductsFromCurrentOrder can't be null or empty!", KITCHEN_RECEIPT_FORMATTER_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        if ((sortedProducts.containsKey(TypeData.ProductPrintDisplayStatus.CURRENT_ORDER)) && (!DataFunctions.isEmptyCollection(sortedProducts.get(TypeData.ProductPrintDisplayStatus.CURRENT_ORDER)))) {
            hasProductsFromCurrentOrder = true;
        }

        return hasProductsFromCurrentOrder;
    }


}