package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-12 15:23:14 -0400 (Tue, 12 May 2020) $: Date of last commit
    $Rev: 11713 $: Revision of last commit
    Notes: Helper class to build a print job header.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

/**
 * Helper class to build a print job header.
 * NOTE: this class uses the builder pattern.
 *
 */
public class KPJobHeaderCreator {

    // private member variables of a KPJobHeaderCreator
    private final DataManager dataManager; // required
    private final int paTransactionID; // required
    private final int terminalID; // required
    private final boolean isOnlineOrder; // optional
    private final boolean isRemoteOrder; // optional
    private final String transactionDate; // optional
    private final String orderNumber; // optional
    private final int transTypeID; // optional
    private final int printStatusID; // optional
    private final String queueDateTime;  // optional
    private final int orderTypeID; // optional
    private final String estimatedOrderTime; // optional
    private final String address; // optional
    private final String personName; // optional
    private final String phoneNumber; // optional
    private final String transName; // optional
    private final String transNameLabel; // optional
    private final String transComment; // optional
    private final String pickupDeliveryNote; // optional

    /**
     * Constructor for a KPJobHeaderCreator.
     *
     * @param builder {@link KPJobHeaderBuilder} Builder class for KPJobHeaderCreator.
     */
    public KPJobHeaderCreator (KPJobHeaderBuilder builder) {
        this.dataManager = builder.dataManager;
        this.paTransactionID = builder.paTransactionID;
        this.terminalID = builder.terminalID;
        this.isOnlineOrder = builder.isOnlineOrder;
        this.isRemoteOrder = builder.isRemoteOrder;
        this.transactionDate = builder.transactionDate;
        this.orderNumber = builder.orderNumber;
        this.transTypeID = builder.transTypeID;
        this.printStatusID = builder.printStatusID;
        this.queueDateTime = builder.queueDateTime;
        this.orderTypeID = builder.orderTypeID;
        this.estimatedOrderTime = builder.estimatedOrderTime;
        this.address = builder.address;
        this.personName = builder.personName;
        this.phoneNumber = builder.phoneNumber;
        this.transName = builder.transName;
        this.transNameLabel = builder.transNameLabel;
        this.transComment = builder.transComment;
        this.pickupDeliveryNote = builder.pickupDeliveryNote;
    }

    /**
     * Utility method to convert a KPJobHeaderCreator to a KPJobHeader.
     *
     * @return {@link KPJobHeader} The converted KPJobHeaderCreator.
     */
    public KPJobHeader convertCreatorToKPJobHeader () {
        KPJobHeader kpJobHeader = new KPJobHeader(this.paTransactionID, this.terminalID, this.dataManager);

        try {
            kpJobHeader.setIsOnlineOrder(isOnlineOrder);
            kpJobHeader.setIsRemoteOrder(isRemoteOrder);

            if ((transactionDate != null) && (!transactionDate.isEmpty())) {
                kpJobHeader.setTransactionDate(this.transactionDate);
            }

            if ((orderNumber != null) && (!orderNumber.isEmpty())) {
                kpJobHeader.setOrderNumber(this.orderNumber);
            }
            if (transTypeID > 0) {
                kpJobHeader.setTransTypeID(transTypeID);
            }

            if (printStatusID > 0) {
                kpJobHeader.setPrintStatusID(printStatusID);
            }

            if ((queueDateTime != null) && (!queueDateTime.isEmpty())) {
                kpJobHeader.setQueueDateTime(this.queueDateTime);
            }

            if (orderTypeID > 0) {
                kpJobHeader.setOrderTypeID(orderTypeID);
            }

            if ((estimatedOrderTime != null) && (!estimatedOrderTime.isEmpty())) {
                kpJobHeader.setEstimatedOrderTime(this.estimatedOrderTime);
            }

            if (StringFunctions.stringHasContent(address)) {
                kpJobHeader.setAddress(address);
            }

            if ((personName != null) && (!personName.isEmpty())) {
                kpJobHeader.setPersonName(this.personName);
            }

            if ((phoneNumber != null) && (!phoneNumber.isEmpty())) {
                kpJobHeader.setPhoneNumber(this.phoneNumber);
            }

            if ((transName != null) && (!transName.isEmpty())) {
                kpJobHeader.setTransName(this.transName);
            }

            if ((transNameLabel != null) && (!transNameLabel.isEmpty())) {
                kpJobHeader.setTransNameLabel(this.transNameLabel);
            }

            if ((transComment != null) && (!transComment.isEmpty())) {
                kpJobHeader.setTransComment(this.transComment);
            }

            if ((pickupDeliveryNote != null) && (!pickupDeliveryNote.isEmpty())) {
                kpJobHeader.setPickupDeliveryNote(this.pickupDeliveryNote);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to convert the KPJobHeaderCreator to a KPJobHeader in " +
                    "KPJobHeaderCreator.convertCreatorToKPJobHeader", Logger.LEVEL.ERROR);
        }

        return kpJobHeader;
    }

    /**
     * Builder class for a KPJobHeaderCreator.
     *
     */
    public static class KPJobHeaderBuilder {

        // private member variables of a KPJobHeaderBuilder
        private final DataManager dataManager;
        private final int paTransactionID;
        private final int terminalID;
        private boolean isOnlineOrder;
        private boolean isRemoteOrder;
        private String transactionDate;
        private String orderNumber;
        private int transTypeID;
        private int printStatusID;
        private String queueDateTime;
        private int orderTypeID;
        private String estimatedOrderTime;
        private String address;
        private String personName;
        private String phoneNumber;
        private String transName;
        private String transNameLabel;
        private String transComment;
        private String pickupDeliveryNote;

        /**
         * Constructor for a KPJobHeaderBuilder.
         *
         * @param dataManager {@link DataManager} DataManager to be used by the KPJobHeaderBuilder.
         * @param paTransactionID The ID of the transaction.
         * @param terminalID The ID of the terminal the transaction took place on.
         */
        public KPJobHeaderBuilder (DataManager dataManager, int paTransactionID, int terminalID) {
            this.dataManager = dataManager;
            this.paTransactionID = paTransactionID;
            this.terminalID = terminalID;
        }

        /**
         * Adds an optional isOnlineOrder to the KPJobHeaderBuilder.
         *
         * @param isOnlineOrder Whether or not the transaction was an online order.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the isOnlineOrder field included.
         */
        public KPJobHeaderBuilder isOnlineOrder (boolean isOnlineOrder) {
            this.isOnlineOrder = isOnlineOrder;
            return this;
        }

        /**
         * Adds an optional isRemoteOrder to the KPJobHeaderBuilder.
         *
         * @param isRemoteOrder Whether or not the transaction was an Remote order.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the isRemoteOrder field included.
         */
        public KPJobHeaderBuilder isRemoteOrder (boolean isRemoteOrder) {
            this.isRemoteOrder = isRemoteOrder;
            return this;
        }

        /**
         * Adds an optional transactionDate to the KPJobHeaderBuilder.
         *
         * @param transactionDate {@link String} Date of the transaction.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the transactionDate field included.
         */
        public KPJobHeaderBuilder transactionDate (String transactionDate) {
            this.transactionDate = transactionDate;
            return this;
        }

        /**
         * Adds an optional orderNumber to the KPJobHeaderBuilder.
         *
         * @param orderNumber {@link String} Number of the order.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the orderNumber field included.
         */
        public KPJobHeaderBuilder orderNumber (String orderNumber) {
            this.orderNumber = orderNumber;
            return this;
        }

        /**
         * Adds an optional transTypeID to the KPJobHeaderBuilder.
         *
         * @param transTypeID The ID of the type of transaction that took place.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the transTypeID field included.
         */
        public KPJobHeaderBuilder transTypeID (int transTypeID) {
            this.transTypeID = transTypeID;
            return this;
        }

        /**
         * Adds an optional printStatusID to the KPJobHeaderBuilder.
         *
         * @param printStatusID The status of the print job.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the printStatusID field included.
         */
        public KPJobHeaderBuilder printStatusID (int printStatusID) {
            this.printStatusID = printStatusID;
            return this;
        }

        /**
         * Adds an optional queueDateTime to the KPJobHeaderBuilder.
         *
         * @param queueDateTime {@link String} Time the print job was added to the print queue.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the queueDateTime field included.
         */
        public KPJobHeaderBuilder queueDateTime (String queueDateTime) {
            this.queueDateTime = queueDateTime;
            return this;
        }

        /**
         * Adds an optional orderTypeID to the KPJobHeaderBuilder.
         *
         * @param orderTypeID The ID of the type of order that was placed.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the orderTypeID field included.
         */
        public KPJobHeaderBuilder orderTypeID (int orderTypeID) {
            this.orderTypeID = orderTypeID;
            return this;
        }

        /**
         * Adds an optional estimatedOrderTime to the KPJobHeaderBuilder.
         *
         * @param estimatedOrderTime {@link String} Time estimate of when the order will be complete.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the estimatedOrderTime field included.
         */
        public KPJobHeaderBuilder estimatedOrderTime (String estimatedOrderTime) {
            this.estimatedOrderTime = estimatedOrderTime;
            return this;
        }

        /**
         * Adds an optional address to the KPJobHeaderBuilder.
         *
         * @param address {@link String} Where the order should be delivered to.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the address field included.
         */
        public KPJobHeaderBuilder address (String address) {
            this.address = address;
            return this;
        }

        /**
         * Adds an optional personName to the KPJobHeaderBuilder.
         *
         * @param personName {@link String} Name of the person who placed the order.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the personName field included.
         */
        public KPJobHeaderBuilder personName (String personName) {
            this.personName = personName;
            return this;
        }

        /**
         * Adds an optional phoneNumber to the KPJobHeaderBuilder.
         *
         * @param phoneNumber {@link String} Contact phone number.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the phoneNumber field included.
         */
        public KPJobHeaderBuilder phoneNumber (String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        /**
         * Adds an optional transName to the KPJobHeaderBuilder.
         *
         * @param transName {@link String} Name of the transaction.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the transName field included.
         */
        public KPJobHeaderBuilder transName (String transName) {
            this.transName = transName;
            return this;
        }

        /**
         * Adds an optional transNameLabel to the KPJobHeaderBuilder.
         *
         * @param transNameLabel {@link String} Label for the transaction name.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the transNameLabel field included.
         */
        public KPJobHeaderBuilder transNameLabel (String transNameLabel) {
            this.transNameLabel = transNameLabel;
            return this;
        }

        /**
         * Adds an optional transComment to the KPJobHeaderBuilder.
         *
         * @param transComment {@link String} Comment related to the transaction.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the transComment field included.
         */
        public KPJobHeaderBuilder transComment (String transComment) {
            this.transComment = transComment;
            return this;
        }

        /**
         * Adds an optional pickupDeliveryNote to the KPJobHeaderBuilder.
         *
         * @param pickupDeliveryNote {@link String} Note related to pickup or delivery.
         * @return {@link KPJobHeaderBuilder} The KPJobHeaderBuilder with the pickupDeliveryNote field included.
         */
        public KPJobHeaderBuilder pickupDeliveryNote (String pickupDeliveryNote) {
            this.pickupDeliveryNote = pickupDeliveryNote;
            return this;
        }

        /**
         * Builds and returns the KPJobHeaderCreator.
         *
         * @return {@link KPJobHeaderCreator} The newly constructed KPJobHeaderCreator.
         */
        public KPJobHeaderCreator build () {
            return (new KPJobHeaderCreator(this));
        }

    }

}
