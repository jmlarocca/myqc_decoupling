package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-27 08:36:18 -0400 (Wed, 27 May 2020) $: Date of last commit
    $Rev: 11819 $: Revision of last commit
    Notes: Contains header information for a transaction.
*/

import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Contains header information for a transaction.</p>
 *
 * Transaction header information is information that is common amongst all line details in a transactions.
 */
public class TransactionHeaderData {

    // date formatters
    private static final DateTimeFormatter TRANS_DATE = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    private static final DateTimeFormatter TRANS_DATE_NO_YEAR = DateTimeFormatter.ofPattern("M/dd");
    private static final DateTimeFormatter TRANS_TIME = DateTimeFormatter.ofPattern("hh:mm:ss a");
    private static final DateTimeFormatter TRANS_TIME_NO_SECONDS = DateTimeFormatter.ofPattern("h:mm a");
    private static final DateTimeFormatter TRANS_DATE_TIME = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss a");
    private static final DateTimeFormatter TRANS_DATE_TIME_NO_AM_PM = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    // private member variables of a TransactionHeaderData
    private BigDecimal paTransactionID = null;
    private BigDecimal originalTransactionID = null;
    private int transTypeID = 0;
    private int terminalID = 0;
    private int posKioskTerminalID = 0;
    private int paTransStatusID = 0;
    private String cashierName = "";
    private String transComment = "";
    private String deliveryLocation = "";
    private int userID = 0;
    private int orderTypeID = 0;
    private LocalDateTime transactionDate = null;
    private String transDate = "";
    private String transDateNoYear = "";
    private String transTime = "";
    private String transTimeNoSeconds = "";
    private String transDateTime = "";
    private int revenueCenterID = 0;
    private String suspendedTransactionName = "";
    private String suspendedTransactionNameLabel = "";
    private LocalDateTime estimatedOrderTime = null;
    private String personName = "";
    private String phone = "";
    private String transName = "";
    private String orderNum = "";
    private String address = "";
    private String paTransNameLabel = "";
    private boolean remoteOrderTerminal = false;
    private boolean onlineOrder = false;
    private LocalDateTime queueTime = null;
    private LocalDateTime printerQueueEstimatedTime = null;
    private int printerQueueOrderTypeID = 0;
    private String printerQueuePhone = "";
    private String printerQueueComment = "";
    private String pickupDeliveryNote = "";
    private String printerQueueOrderNum = "";
    private int kmsOrderStatusID = 0;
    private boolean printOrderNumber = false;
    private boolean printProductCodes = false;
    private boolean printNutritionInfo = false;
    private int badgeNumDigitsPrinted = 0;
    private boolean printTransactionBarCode = false;
    private boolean printGrossWeight = false;
    private boolean printNetWeight = false;
    private int prepTime = 0;
    private boolean recordTransactionsLocally = false;
    private String diningOptionName = "";

    /**
     * <p>Builds a {@link TransactionHeaderData} from the given {@link HashMap}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param hm The {@link HashMap} to use to build the {@link TransactionHeaderData}.
     * @return The {@link TransactionHeaderData} built from the given {@link HashMap}.
     */
    public static TransactionHeaderData buildFromHM (String log, HashMap hm) {
        TransactionHeaderData transactionHeaderData = new TransactionHeaderData();

        try {
            if (!DataFunctions.isEmptyMap(hm)) {
                String paTransactionIDStr = HashMapDataFns.getStringVal(hm, "PATRANSACTIONID");
                // remove the TerminalID from the PATransactionID if it's in there
                if ((StringFunctions.stringHasContent(paTransactionIDStr)) && (paTransactionIDStr.contains("T"))) {
                    paTransactionIDStr = paTransactionIDStr.substring(0, paTransactionIDStr.indexOf("T"));
                }
                if ((StringFunctions.stringHasContent(paTransactionIDStr)) && (NumberUtils.isNumber(paTransactionIDStr)) && (new BigDecimal(paTransactionIDStr).compareTo(BigDecimal.ZERO) > 0)) {
                    transactionHeaderData.paTransactionID = new BigDecimal(paTransactionIDStr);
                }
                else {
                    transactionHeaderData.paTransactionID = null;
                }
                String originalTransactionIDStr = HashMapDataFns.getStringVal(hm, "PATRANSIDORIGINAL");
                if ((StringFunctions.stringHasContent(originalTransactionIDStr)) && (NumberUtils.isNumber(originalTransactionIDStr)) && (new BigDecimal(originalTransactionIDStr).compareTo(BigDecimal.ZERO) > 0)) {
                    transactionHeaderData.originalTransactionID = new BigDecimal(originalTransactionIDStr);
                }
                else {
                    transactionHeaderData.originalTransactionID = null;
                }
                transactionHeaderData.transTypeID = HashMapDataFns.getIntVal(hm, "TRANSTYPEID");
                transactionHeaderData.terminalID = HashMapDataFns.getIntVal(hm, "TERMINALID");
                transactionHeaderData.posKioskTerminalID = HashMapDataFns.getIntVal(hm, "POSKIOSKTERMINALID");
                transactionHeaderData.paTransStatusID = HashMapDataFns.getIntVal(hm, "PATRANSSTATUSID");
                transactionHeaderData.cashierName = HashMapDataFns.getStringVal(hm, "CASHIERNAME");
                transactionHeaderData.transComment = HashMapDataFns.getStringVal(hm, "TRANSCOMMENT");
                transactionHeaderData.deliveryLocation = HashMapDataFns.getStringVal(hm, "DELIVERYLOCATION");
                transactionHeaderData.userID = HashMapDataFns.getIntVal(hm, "USERID");
                transactionHeaderData.orderTypeID = HashMapDataFns.getIntVal(hm, "PAORDERTYPEID");
                Date txnDateAsDate = HashMapDataFns.getDateVal(hm, "TRANSACTIONDATE");
                if (txnDateAsDate != null) {
                    transactionHeaderData.transactionDate = new Timestamp(txnDateAsDate.getTime()).toLocalDateTime();
                }
                else {
                    transactionHeaderData.transactionDate = null;
                }
                // set the transaction date in other formats for convenience
                if (transactionHeaderData.transactionDate != null) {
                    transactionHeaderData.transDate = transactionHeaderData.transactionDate.format(TRANS_DATE);
                    transactionHeaderData.transDateNoYear = transactionHeaderData.transactionDate.format(TRANS_DATE_NO_YEAR);
                    transactionHeaderData.transTime = transactionHeaderData.transactionDate.format(TRANS_TIME);
                    transactionHeaderData.transTimeNoSeconds = transactionHeaderData.transactionDate.format(TRANS_TIME_NO_SECONDS);
                    transactionHeaderData.transDateTime = transactionHeaderData.transactionDate.format(TRANS_DATE_TIME);
                }
                transactionHeaderData.revenueCenterID = HashMapDataFns.getIntVal(hm, "REVENUECENTERID");
                transactionHeaderData.suspendedTransactionName = HashMapDataFns.getStringVal(hm, "TRANSNAME");
                transactionHeaderData.suspendedTransactionNameLabel = HashMapDataFns.getStringVal(hm, "PATRANSNAMELABEL");
                Date estOrdTimeAsDate = HashMapDataFns.getDateVal(hm, "ESTIMATEDORDERTIME");
                if (estOrdTimeAsDate != null) {
                    transactionHeaderData.estimatedOrderTime = new Timestamp(estOrdTimeAsDate.getTime()).toLocalDateTime();
                }
                else {
                    transactionHeaderData.estimatedOrderTime = null;
                }
                transactionHeaderData.personName = HashMapDataFns.getStringVal(hm, "PERSONNAME");
                transactionHeaderData.phone = HashMapDataFns.getStringVal(hm, "PHONE");
                transactionHeaderData.transName = HashMapDataFns.getStringVal(hm, "TRANSNAME");
                transactionHeaderData.orderNum = HashMapDataFns.getStringVal(hm, "ORDERNUM");
                transactionHeaderData.address = HashMapDataFns.getStringVal(hm, "ADDRESS");
                transactionHeaderData.paTransNameLabel = HashMapDataFns.getStringVal(hm, "PATRANSNAMELABEL");
                transactionHeaderData.remoteOrderTerminal = HashMapDataFns.getBooleanVal(hm, "REMOTEORDERTERMINAL");
                transactionHeaderData.onlineOrder = HashMapDataFns.getBooleanVal(hm, "ONLINEORDER");
                Date queueTimeAsDate = HashMapDataFns.getDateVal(hm, "QUEUETIME");
                if (queueTimeAsDate != null) {
                    transactionHeaderData.queueTime = new Timestamp(queueTimeAsDate.getTime()).toLocalDateTime();
                }
                else {
                    transactionHeaderData.queueTime = null;
                }
                Date pqEstOrdTimeAsDate = HashMapDataFns.getDateVal(hm, "PRINTERQUEUEESTIMATEDTIME");
                if (pqEstOrdTimeAsDate != null) {
                    transactionHeaderData.printerQueueEstimatedTime = new Timestamp(pqEstOrdTimeAsDate.getTime()).toLocalDateTime();
                }
                else {
                    transactionHeaderData.printerQueueEstimatedTime = null;
                }
                transactionHeaderData.printerQueueOrderTypeID = HashMapDataFns.getIntVal(hm, "PRINTERQUEUEORDERTYPEID");
                transactionHeaderData.printerQueuePhone = HashMapDataFns.getStringVal(hm, "PRINTERQUEUEPHONE");
                transactionHeaderData.printerQueueComment = HashMapDataFns.getStringVal(hm, "PRINTERQUEUECOMMENT");
                transactionHeaderData.pickupDeliveryNote = HashMapDataFns.getStringVal(hm, "PICKUPDELIVERYNOTE");
                transactionHeaderData.printerQueueOrderNum = HashMapDataFns.getStringVal(hm, "PRINTERQUEUEORDERNUM");
                transactionHeaderData.kmsOrderStatusID = HashMapDataFns.getIntVal(hm, "KMSORDERSTATUSID");
                transactionHeaderData.printOrderNumber = HashMapDataFns.getBooleanVal(hm, "PAPRINTORDERNUMBER");
                transactionHeaderData.printProductCodes = HashMapDataFns.getBooleanVal(hm, "PAPRINTPLUCODES");
                transactionHeaderData.printNutritionInfo = HashMapDataFns.getBooleanVal(hm, "PAPRINTNUTRITIONINFO");
                transactionHeaderData.badgeNumDigitsPrinted = HashMapDataFns.getIntVal(hm, "PABADGENUMDIGITSPRINTED");
                transactionHeaderData.printTransactionBarCode = HashMapDataFns.getBooleanVal(hm, "PAPRINTTRANSBARCODE");
                transactionHeaderData.printGrossWeight = HashMapDataFns.getBooleanVal(hm, "PRINTTXNGROSSWEIGHT");
                transactionHeaderData.printNetWeight = HashMapDataFns.getBooleanVal(hm, "PRINTTXNNETWEIGHT");
                transactionHeaderData.prepTime = HashMapDataFns.getIntVal(hm, "PREPTIME");
                transactionHeaderData.recordTransactionsLocally = HashMapDataFns.getBooleanVal(hm, "RECORDTRANSACTIONSLOCALLY");
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build a TransactionHeaderData from the given HashMap in TransactionHeaderData.buildFromHM", log, Logger.LEVEL.ERROR);
        }

        return transactionHeaderData;
    }

    /**
     * <p>Overridden toString() method for a {@link TransactionHeaderData}.</p>
     *
     * @return A {@link String} representation of this {@link TransactionHeaderData}.
     */
    @Override
    public String toString () {

        return String.format("PATRANSACTIONID: %s, ORIGINALTRANSACTIONID: %s, TRANSTYPEID: %s, TERMINALID: %s, POSKIOSKTERMINALID: %s, PATRANSSTATUSID: %s, " +
                "CASHIERNAME: %s, TRANSCOMMENT: %s, DELIVERYLOCATION: %s, USERID: %s, ORDERTYPEID: %s, TRANSACTIONDATE: %s, TRANSDATE: %s, TRANSDATENOYEAR: %s, " +
                "TRANSTIME: %s, TRANSTIMENOSECONDS: %s, TRANSDATETIME: %s, REVENUECENTERID: %s, SUSPENDEDTRANSACTIONNAME: %s, SUSPENDEDTRANSACTIONNAMELABEL: %s, " +
                "ESTIMATEDORDERTIME: %s, PERSONNAME: %s, PHONE: %s, TRANSNAME: %s, ORDERNUM: %s, ADDRESS: %s, PATRANSNAMELABEL: %s, REMOTEORDERTERMINAL: %s, " +
                "ONLINEORDER: %s, QUEUETIME: %s, PRINTERQUEUEESTIMATEDTIME: %s, PRINTERQUEUEORDERTYPEID: %s, PRINTERQUEUEPHONE: %s, PRINTERQUEUECOMMENT: %s, " +
                "PUCKUPDELIVERYNOTE: %s, PRINTERQUEUEORDERNUM: %s, KMSORDERSTATUSID: %s PRINTORDERNUMBER: %s, PRINTPRODUCTCODES: %s, PRINTNUTRITIONINFO: %s, " +
                "BADGENUMDIGITSPRINTED: %s, PRINTTRANSACTIONBARCODE: %s, PRINTGROSSWEIGHT: %s, PRINTNETWEIGHT: %s, PREPTIME: %s, RECORDTRANSACTIONSLOCALLY: %s, " +
                "DININGOPTIONNAME: %s",
                Objects.toString(((paTransactionID != null) && (StringFunctions.stringHasContent(paTransactionID.toPlainString())) ? paTransactionID.toPlainString() : "N/A"), "N/A"),
                Objects.toString(((originalTransactionID != null) && (StringFunctions.stringHasContent(originalTransactionID.toPlainString())) ? originalTransactionID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((transTypeID != -1 ? transTypeID : "N/A"), "N/A"),
                Objects.toString((terminalID != -1 ? terminalID : "N/A"), "N/A"),
                Objects.toString((posKioskTerminalID != -1 ? posKioskTerminalID : "N/A"), "N/A"),
                Objects.toString((paTransStatusID != -1 ? paTransStatusID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(cashierName) ? cashierName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transComment) ? transComment : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(deliveryLocation) ? deliveryLocation : "N/A"), "N/A"),
                Objects.toString((userID != -1 ? userID : "N/A"), "N/A"),
                Objects.toString((orderTypeID != -1 ? orderTypeID : "N/A"), "N/A"),
                Objects.toString((((transactionDate != null) && (StringFunctions.stringHasContent(transactionDate.format(TRANS_DATE_TIME_NO_AM_PM)))) ? transactionDate.format(TRANS_DATE_TIME_NO_AM_PM) : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transDate) ? transDate : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transDateNoYear) ? transDateNoYear : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transTime) ? transTime : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transTimeNoSeconds) ? transTimeNoSeconds : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transDateTime) ? transDateTime : "N/A"), "N/A"),
                Objects.toString((revenueCenterID != -1 ? revenueCenterID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(suspendedTransactionName) ? suspendedTransactionName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(suspendedTransactionNameLabel) ? suspendedTransactionNameLabel : "N/A"), "N/A"),
                Objects.toString((((estimatedOrderTime!= null) && (StringFunctions.stringHasContent(estimatedOrderTime.format(TRANS_DATE_TIME_NO_AM_PM)))) ? estimatedOrderTime.format(TRANS_DATE_TIME_NO_AM_PM) : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(personName) ? personName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(phone) ? phone : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transName) ? transName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(orderNum) ? orderNum : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(address) ? address : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(paTransNameLabel) ? paTransNameLabel : "N/A"), "N/A"),
                Objects.toString(remoteOrderTerminal, "N/A"),
                Objects.toString(onlineOrder, "N/A"),
                Objects.toString((((queueTime != null) && (StringFunctions.stringHasContent(queueTime.format(TRANS_DATE_TIME_NO_AM_PM)))) ? queueTime.format(TRANS_DATE_TIME_NO_AM_PM) : "N/A"), "N/A"),
                Objects.toString((((printerQueueEstimatedTime != null) && (StringFunctions.stringHasContent(printerQueueEstimatedTime.format(TRANS_DATE_TIME_NO_AM_PM)))) ? printerQueueEstimatedTime.format(TRANS_DATE_TIME_NO_AM_PM) : "N/A"), "N/A"),
                Objects.toString((printerQueueOrderTypeID != -1 ? printerQueueOrderTypeID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(printerQueuePhone) ? printerQueuePhone : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(printerQueueComment) ? printerQueueComment : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(pickupDeliveryNote) ? pickupDeliveryNote : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(printerQueueOrderNum) ? printerQueueOrderNum : "N/A"), "N/A"),
                Objects.toString((kmsOrderStatusID != -1 ? kmsOrderStatusID : "N/A"), "N/A"),
                Objects.toString(printOrderNumber, "N/A"),
                Objects.toString(printProductCodes, "N/A"),
                Objects.toString(printNutritionInfo, "N/A"),
                Objects.toString((badgeNumDigitsPrinted != -1 ? badgeNumDigitsPrinted : "N/A"), "N/A"),
                Objects.toString(printTransactionBarCode, "N/A"),
                Objects.toString(printGrossWeight, "N/A"),
                Objects.toString(printNetWeight, "N/A"),
                Objects.toString((prepTime != -1 ? prepTime : "N/A"), "N/A"),
                Objects.toString(recordTransactionsLocally, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(diningOptionName) ? diningOptionName : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link TransactionHeaderData}.
     * Two {@link TransactionHeaderData} are defined as being equal if they have the same paTransactionID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link TransactionHeaderData}.
     * @return Whether or not the {@link Object} is equal to this {@link TransactionHeaderData}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a TransactionHeaderData and check if the obj's paTransactionID is equal to this TransactionHeaderData's paTransactionID
        TransactionHeaderData transactionHeaderData = ((TransactionHeaderData) obj);
        return Objects.equals(transactionHeaderData.paTransactionID, paTransactionID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link TransactionHeaderData}.</p>
     *
     * @return The unique hash code for this {@link TransactionHeaderData}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(paTransactionID);
    }

    /**
     * <p>Getter for the paTransactionID field of the {@link TransactionHeaderData}.</p>
     *
     * @return The paTransactionID field of the {@link TransactionHeaderData}.
     */
    public BigDecimal getPaTransactionID () {
        return paTransactionID;
    }

    /**
     * <p>Setter for the paTransactionID field of the {@link TransactionHeaderData}.</p>
     *
     * @param paTransactionID The paTransactionID field of the {@link TransactionHeaderData}.
     */
    public void setPaTransactionID (BigDecimal paTransactionID) {
        this.paTransactionID = paTransactionID;
    }

    /**
     * <p>Getter for the originalTransactionID field of the {@link TransactionHeaderData}.</p>
     *
     * @return The originalTransactionID field of the {@link TransactionHeaderData}.
     */
    public BigDecimal getOriginalTransactionID () {
        return originalTransactionID;
    }

    /**
     * <p>Setter for the originalTransactionID field of the {@link TransactionHeaderData}.</p>
     *
     * @param originalTransactionID The originalTransactionID field of the {@link TransactionHeaderData}.
     */
    public void setOriginalTransactionID (BigDecimal originalTransactionID) {
        this.originalTransactionID = originalTransactionID;
    }

    /**
     * <p>Getter for the transTypeID field of the {@link TransactionHeaderData}.</p>
     *
     * @return The transTypeID field of the {@link TransactionHeaderData}.
     */
    public int getTransTypeID () {
        return transTypeID;
    }

    /**
     * <p>Setter for the transTypeID field of the {@link TransactionHeaderData}.</p>
     *
     * @param transTypeID The transTypeID field of the {@link TransactionHeaderData}.
     */
    public void setTransTypeID (int transTypeID) {
        this.transTypeID = transTypeID;
    }

    /**
     * <p>Getter for the terminalID field of the {@link TransactionHeaderData}.</p>
     *
     * @return The terminalID field of the {@link TransactionHeaderData}.
     */
    public int getTerminalID () {
        return terminalID;
    }

    /**
     * <p>Setter for the terminalID field of the {@link TransactionHeaderData}.</p>
     *
     * @param terminalID The terminalID field of the {@link TransactionHeaderData}.
     */
    public void setTerminalID (int terminalID) {
        this.terminalID = terminalID;
    }

    /**
     * <p>Getter for the posKioskTerminalID field of the {@link TransactionHeaderData}.</p>
     *
     * @return The posKioskTerminalID field of the {@link TransactionHeaderData}.
     */
    public int getPosKioskTerminalID () {
        return posKioskTerminalID;
    }

    /**
     * <p>Setter for the posKioskTerminalID field of the {@link TransactionHeaderData}.</p>
     *
     * @param posKioskTerminalID The posKioskTerminalID field of the {@link TransactionHeaderData}.
     */
    public void setPosKioskTerminalID (int posKioskTerminalID) {
        this.posKioskTerminalID = posKioskTerminalID;
    }

    /**
     * <p>Getter for the paTransStatusID field of the {@link TransactionHeaderData}.</p>
     *
     * @return The paTransStatusID field of the {@link TransactionHeaderData}.
     */
    public int getPaTransStatusID () {
        return paTransStatusID;
    }

    /**
     * <p>Setter for the paTransStatusID field of the {@link TransactionHeaderData}.</p>
     *
     * @param paTransStatusID The paTransStatusID field of the {@link TransactionHeaderData}.
     */
    public void setPaTransStatusID (int paTransStatusID) {
        this.paTransStatusID = paTransStatusID;
    }

    /**
     * <p>Getter for the cashierName field of the {@link TransactionHeaderData}.</p>
     *
     * @return The cashierName field of the {@link TransactionHeaderData}.
     */
    public String getCashierName () {
        return cashierName;
    }

    /**
     * <p>Setter for the cashierName field of the {@link TransactionHeaderData}.</p>
     *
     * @param cashierName The cashierName field of the {@link TransactionHeaderData}.
     */
    public void setCashierName (String cashierName) {
        this.cashierName = cashierName;
    }

    /**
     * <p>Getter for the transComment field of the {@link TransactionHeaderData}.</p>
     *
     * @return The transComment field of the {@link TransactionHeaderData}.
     */
    public String getTransComment () {
        return transComment;
    }

    /**
     * <p>Setter for the transComment field of the {@link TransactionHeaderData}.</p>
     *
     * @param transComment The transComment field of the {@link TransactionHeaderData}.
     */
    public void setTransComment (String transComment) {
        this.transComment = transComment;
    }

    /**
     * <p>Getter for the deliveryLocation field of the {@link TransactionHeaderData}.</p>
     *
     * @return The deliveryLocation field of the {@link TransactionHeaderData}.
     */
    public String getDeliveryLocation () {
        return deliveryLocation;
    }

    /**
     * <p>Setter for the deliveryLocation field of the {@link TransactionHeaderData}.</p>
     *
     * @param deliveryLocation The deliveryLocation field of the {@link TransactionHeaderData}.
     */
    public void setDeliveryLocation (String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    /**
     * <p>Getter for the userID field of the {@link TransactionHeaderData}.</p>
     *
     * @return The userID field of the {@link TransactionHeaderData}.
     */
    public int getUserID () {
        return userID;
    }

    /**
     * <p>Setter for the userID field of the {@link TransactionHeaderData}.</p>
     *
     * @param userID The userID field of the {@link TransactionHeaderData}.
     */
    public void setUserID (int userID) {
        this.userID = userID;
    }

    /**
     * <p>Getter for the orderTypeID field of the {@link TransactionHeaderData}.</p>
     *
     * @return The orderTypeID field of the {@link TransactionHeaderData}.
     */
    public int getOrderTypeID () {
        return orderTypeID;
    }

    /**
     * <p>Setter for the orderTypeID field of the {@link TransactionHeaderData}.</p>
     *
     * @param orderTypeID The orderTypeID field of the {@link TransactionHeaderData}.
     */
    public void setOrderTypeID (int orderTypeID) {
        this.orderTypeID = orderTypeID;
    }

    /**
     * <p>Getter for the transactionDate field of the {@link TransactionHeaderData}.</p>
     *
     * @return The transactionDate field of the {@link TransactionHeaderData}.
     */
    public LocalDateTime getTransactionDate () {
        return transactionDate;
    }

    /**
     * <p>Setter for the transactionDate field of the {@link TransactionHeaderData}.</p>
     *
     * @param transactionDate The transactionDate field of the {@link TransactionHeaderData}.
     */
    public void setTransactionDate (LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * <p>Getter for the transDate field of the {@link TransactionHeaderData}.</p>
     *
     * @return The transDate field of the {@link TransactionHeaderData}.
     */
    public String getTransDate () {
        return transDate;
    }

    /**
     * <p>Setter for the transDate field of the {@link TransactionHeaderData}.</p>
     *
     * @param transDate The transDate field of the {@link TransactionHeaderData}.
     */
    public void setTransDate (String transDate) {
        this.transDate = transDate;
    }

    /**
     * <p>Getter for the transDateNoYear field of the {@link TransactionHeaderData}.</p>
     *
     * @return The transDateNoYear field of the {@link TransactionHeaderData}.
     */
    public String getTransDateNoYear () {
        return transDateNoYear;
    }

    /**
     * <p>Setter for the transDateNoYear field of the {@link TransactionHeaderData}.</p>
     *
     * @param transDateNoYear The transDateNoYear field of the {@link TransactionHeaderData}.
     */
    public void setTransDateNoYear (String transDateNoYear) {
        this.transDateNoYear = transDateNoYear;
    }

    /**
     * <p>Getter for the transTime field of the {@link TransactionHeaderData}.</p>
     *
     * @return The transTime field of the {@link TransactionHeaderData}.
     */
    public String getTransTime () {
        return transTime;
    }

    /**
     * <p>Setter for the transTime field of the {@link TransactionHeaderData}.</p>
     *
     * @param transTime The transTime field of the {@link TransactionHeaderData}.
     */
    public void setTransTime (String transTime) {
        this.transTime = transTime;
    }

    /**
     * <p>Getter for the transTimeNoSeconds field of the {@link TransactionHeaderData}.</p>
     *
     * @return The transTimeNoSeconds field of the {@link TransactionHeaderData}.
     */
    public String getTransTimeNoSeconds () {
        return transTimeNoSeconds;
    }

    /**
     * <p>Setter for the transTimeNoSeconds field of the {@link TransactionHeaderData}.</p>
     *
     * @param transTimeNoSeconds The transTimeNoSeconds field of the {@link TransactionHeaderData}.
     */
    public void setTransTimeNoSeconds (String transTimeNoSeconds) {
        this.transTimeNoSeconds = transTimeNoSeconds;
    }

    /**
     * <p>Getter for the transDateTime field of the {@link TransactionHeaderData}.</p>
     *
     * @return The transDateTime field of the {@link TransactionHeaderData}.
     */
    public String getTransDateTime () {
        return transDateTime;
    }

    /**
     * <p>Setter for the transDateTime field of the {@link TransactionHeaderData}.</p>
     *
     * @param transDateTime The transDateTime field of the {@link TransactionHeaderData}.
     */
    public void setTransDateTime (String transDateTime) {
        this.transDateTime = transDateTime;
    }

    /**
     * <p>Getter for the revenueCenterID field of the {@link TransactionHeaderData}.</p>
     *
     * @return The revenueCenterID field of the {@link TransactionHeaderData}.
     */
    public int getRevenueCenterID () {
        return revenueCenterID;
    }

    /**
     * <p>Setter for the revenueCenterID field of the {@link TransactionHeaderData}.</p>
     *
     * @param revenueCenterID The revenueCenterID field of the {@link TransactionHeaderData}.
     */
    public void setRevenueCenterID (int revenueCenterID) {
        this.revenueCenterID = revenueCenterID;
    }

    /**
     * <p>Getter for the suspendedTransactionName field of the {@link TransactionHeaderData}.</p>
     *
     * @return The suspendedTransactionName field of the {@link TransactionHeaderData}.
     */
    public String getSuspendedTransactionName () {
        return suspendedTransactionName;
    }

    /**
     * <p>Setter for the suspendedTransactionName field of the {@link TransactionHeaderData}.</p>
     *
     * @param suspendedTransactionName The suspendedTransactionName field of the {@link TransactionHeaderData}.
     */
    public void setSuspendedTransactionName (String suspendedTransactionName) {
        this.suspendedTransactionName = suspendedTransactionName;
    }

    /**
     * <p>Getter for the suspendedTransactionNameLabel field of the {@link TransactionHeaderData}.</p>
     *
     * @return The suspendedTransactionNameLabel field of the {@link TransactionHeaderData}.
     */
    public String getSuspendedTransactionNameLabel () {
        return suspendedTransactionNameLabel;
    }

    /**
     * <p>Setter for the suspendedTransactionNameLabel field of the {@link TransactionHeaderData}.</p>
     *
     * @param suspendedTransactionNameLabel The suspendedTransactionNameLabel field of the {@link TransactionHeaderData}.
     */
    public void setSuspendedTransactionNameLabel (String suspendedTransactionNameLabel) {
        this.suspendedTransactionNameLabel = suspendedTransactionNameLabel;
    }

    /**
     * <p>Getter for the estimatedOrderTime field of the {@link TransactionHeaderData}.</p>
     *
     * @return The estimatedOrderTime field of the {@link TransactionHeaderData}.
     */
    public LocalDateTime getEstimatedOrderTime () {
        return estimatedOrderTime;
    }

    /**
     * <p>Setter for the estimatedOrderTime field of the {@link TransactionHeaderData}.</p>
     *
     * @param estimatedOrderTime The estimatedOrderTime field of the {@link TransactionHeaderData}.
     */
    public void setEstimatedOrderTime (LocalDateTime estimatedOrderTime) {
        this.estimatedOrderTime = estimatedOrderTime;
    }

    /**
     * <p>Getter for the personName field of the {@link TransactionHeaderData}.</p>
     *
     * @return The personName field of the {@link TransactionHeaderData}.
     */
    public String getPersonName () {
        return personName;
    }

    /**
     * <p>Setter for the personName field of the {@link TransactionHeaderData}.</p>
     *
     * @param personName The personName field of the {@link TransactionHeaderData}.
     */
    public void setPersonName (String personName) {
        this.personName = personName;
    }

    /**
     * <p>Getter for the phone field of the {@link TransactionHeaderData}.</p>
     *
     * @return The phone field of the {@link TransactionHeaderData}.
     */
    public String getPhone () {
        return phone;
    }

    /**
     * <p>Setter for the phone field of the {@link TransactionHeaderData}.</p>
     *
     * @param phone The phone field of the {@link TransactionHeaderData}.
     */
    public void setPhone (String phone) {
        this.phone = phone;
    }

    /**
     * <p>Getter for the transName field of the {@link TransactionHeaderData}.</p>
     *
     * @return The transName field of the {@link TransactionHeaderData}.
     */
    public String getTransName () {
        return transName;
    }

    /**
     * <p>Setter for the transName field of the {@link TransactionHeaderData}.</p>
     *
     * @param transName The transName field of the {@link TransactionHeaderData}.
     */
    public void setTransName (String transName) {
        this.transName = transName;
    }

    /**
     * <p>Getter for the orderNum field of the {@link TransactionHeaderData}.</p>
     *
     * @return The orderNum field of the {@link TransactionHeaderData}.
     */
    public String getOrderNum () {
        return orderNum;
    }

    /**
     * <p>Setter for the orderNum field of the {@link TransactionHeaderData}.</p>
     *
     * @param orderNum The orderNum field of the {@link TransactionHeaderData}.
     */
    public void setOrderNum (String orderNum) {
        this.orderNum = orderNum;
    }

    /**
     * <p>Getter for the address field of the {@link TransactionHeaderData}.</p>
     *
     * @return The address field of the {@link TransactionHeaderData}.
     */
    public String getAddress () {
        return address;
    }

    /**
     * <p>Setter for the address field of the {@link TransactionHeaderData}.</p>
     *
     * @param address The address field of the {@link TransactionHeaderData}.
     */
    public void setAddress (String address) {
        this.address = address;
    }

    /**
     * <p>Getter for the paTransNameLabel field of the {@link TransactionHeaderData}.</p>
     *
     * @return The paTransNameLabel field of the {@link TransactionHeaderData}.
     */
    public String getPaTransNameLabel () {
        return paTransNameLabel;
    }

    /**
     * <p>Setter for the paTransNameLabel field of the {@link TransactionHeaderData}.</p>
     *
     * @param paTransNameLabel The paTransNameLabel field of the {@link TransactionHeaderData}.
     */
    public void setPaTransNameLabel (String paTransNameLabel) {
        this.paTransNameLabel = paTransNameLabel;
    }

    /**
     * <p>Getter for the remoteOrderTerminal field of the {@link TransactionHeaderData}.</p>
     *
     * @return The remoteOrderTerminal field of the {@link TransactionHeaderData}.
     */
    public boolean getRemoteOrderTerminal () {
        return remoteOrderTerminal;
    }

    /**
     * <p>Setter for the remoteOrderTerminal field of the {@link TransactionHeaderData}.</p>
     *
     * @param remoteOrderTerminal The remoteOrderTerminal field of the {@link TransactionHeaderData}.
     */
    public void setRemoteOrderTerminal (boolean remoteOrderTerminal) {
        this.remoteOrderTerminal = remoteOrderTerminal;
    }

    /**
     * <p>Getter for the onlineOrder field of the {@link TransactionHeaderData}.</p>
     *
     * @return The onlineOrder field of the {@link TransactionHeaderData}.
     */
    public boolean getOnlineOrder () {
        return onlineOrder;
    }

    /**
     * <p>Setter for the onlineOrder field of the {@link TransactionHeaderData}.</p>
     *
     * @param onlineOrder The onlineOrder field of the {@link TransactionHeaderData}.
     */
    public void setOnlineOrder (boolean onlineOrder) {
        this.onlineOrder = onlineOrder;
    }

    /**
     * <p>Getter for the queueTime field of the {@link TransactionHeaderData}.</p>
     *
     * @return The queueTime field of the {@link TransactionHeaderData}.
     */
    public LocalDateTime getQueueTime () {
        return queueTime;
    }

    /**
     * <p>Setter for the queueTime field of the {@link TransactionHeaderData}.</p>
     *
     * @param queueTime The queueTime field of the {@link TransactionHeaderData}.
     */
    public void setQueueTime (LocalDateTime queueTime) {
        this.queueTime = queueTime;
    }

    /**
     * <p>Getter for the printerQueueEstimatedTime field of the {@link TransactionHeaderData}.</p>
     *
     * @return The printerQueueEstimatedTime field of the {@link TransactionHeaderData}.
     */
    public LocalDateTime getPrinterQueueEstimatedTime () {
        return printerQueueEstimatedTime;
    }

    /**
     * <p>Setter for the printerQueueEstimatedTime field of the {@link TransactionHeaderData}.</p>
     *
     * @param printerQueueEstimatedTime The printerQueueEstimatedTime field of the {@link TransactionHeaderData}.
     */
    public void setPrinterQueueEstimatedTime (LocalDateTime printerQueueEstimatedTime) {
        this.printerQueueEstimatedTime = printerQueueEstimatedTime;
    }

    /**
     * <p>Getter for the printerQueueOrderTypeID field of the {@link TransactionHeaderData}.</p>
     *
     * @return The printerQueueOrderTypeID field of the {@link TransactionHeaderData}.
     */
    public int getPrinterQueueOrderTypeID () {
        return printerQueueOrderTypeID;
    }

    /**
     * <p>Setter for the printerQueueOrderTypeID field of the {@link TransactionHeaderData}.</p>
     *
     * @param printerQueueOrderTypeID The printerQueueOrderTypeID field of the {@link TransactionHeaderData}.
     */
    public void setPrinterQueueOrderTypeID (int printerQueueOrderTypeID) {
        this.printerQueueOrderTypeID = printerQueueOrderTypeID;
    }

    /**
     * <p>Getter for the printerQueuePhone field of the {@link TransactionHeaderData}.</p>
     *
     * @return The printerQueuePhone field of the {@link TransactionHeaderData}.
     */
    public String getPrinterQueuePhone () {
        return printerQueuePhone;
    }

    /**
     * <p>Setter for the printerQueuePhone field of the {@link TransactionHeaderData}.</p>
     *
     * @param printerQueuePhone The printerQueuePhone field of the {@link TransactionHeaderData}.
     */
    public void setPrinterQueuePhone (String printerQueuePhone) {
        this.printerQueuePhone = printerQueuePhone;
    }

    /**
     * <p>Getter for the printerQueueComment field of the {@link TransactionHeaderData}.</p>
     *
     * @return The printerQueueComment field of the {@link TransactionHeaderData}.
     */
    public String getPrinterQueueComment () {
        return printerQueueComment;
    }

    /**
     * <p>Setter for the printerQueueComment field of the {@link TransactionHeaderData}.</p>
     *
     * @param printerQueueComment The printerQueueComment field of the {@link TransactionHeaderData}.
     */
    public void setPrinterQueueComment (String printerQueueComment) {
        this.printerQueueComment = printerQueueComment;
    }

    /**
     * <p>Getter for the pickupDeliveryNote field of the {@link TransactionHeaderData}.</p>
     *
     * @return The pickupDeliveryNote field of the {@link TransactionHeaderData}.
     */
    public String getPickupDeliveryNote () {
        return pickupDeliveryNote;
    }

    /**
     * <p>Setter for the pickupDeliveryNote field of the {@link TransactionHeaderData}.</p>
     *
     * @param pickupDeliveryNote The pickupDeliveryNote field of the {@link TransactionHeaderData}.
     */
    public void setPickupDeliveryNote (String pickupDeliveryNote) {
        this.pickupDeliveryNote = pickupDeliveryNote;
    }

    /**
     * <p>Getter for the printerQueueOrderNum field of the {@link TransactionHeaderData}.</p>
     *
     * @return The printerQueueOrderNum field of the {@link TransactionHeaderData}.
     */
    public String getPrinterQueueOrderNum () {
        return printerQueueOrderNum;
    }

    /**
     * <p>Setter for the printerQueueOrderNum field of the {@link TransactionHeaderData}.</p>
     *
     * @param printerQueueOrderNum The printerQueueOrderNum field of the {@link TransactionHeaderData}.
     */
    public void setPrinterQueueOrderNum (String printerQueueOrderNum) {
        this.printerQueueOrderNum = printerQueueOrderNum;
    }

    /**
     * <p>Getter for the kmsOrderStatusID field of the {@link TransactionHeaderData}.</p>
     *
     * @return The kmsOrderStatusID field of the {@link TransactionHeaderData}.
     */
    public int getKmsOrderStatusID () {
        return kmsOrderStatusID;
    }

    /**
     * <p>Setter for the kmsOrderStatusID field of the {@link TransactionHeaderData}.</p>
     *
     * @param kmsOrderStatusID The kmsOrderStatusID field of the {@link TransactionHeaderData}.
     */
    public void setKmsOrderStatusID (int kmsOrderStatusID) {
        this.kmsOrderStatusID = kmsOrderStatusID;
    }

    /**
     * <p>Getter for the printOrderNumber field of the {@link TransactionHeaderData}.</p>
     *
     * @return The printOrderNumber field of the {@link TransactionHeaderData}.
     */
    public boolean getPrintOrderNumber () {
        return printOrderNumber;
    }

    /**
     * <p>Setter for the printOrderNumber field of the {@link TransactionHeaderData}.</p>
     *
     * @param printOrderNumber The printOrderNumber field of the {@link TransactionHeaderData}.
     */
    public void setPrintOrderNumber (boolean printOrderNumber) {
        this.printOrderNumber = printOrderNumber;
    }

    /**
     * <p>Getter for the printProductCodes field of the {@link TransactionHeaderData}.</p>
     *
     * @return The printProductCodes field of the {@link TransactionHeaderData}.
     */
    public boolean getPrintProductCodes () {
        return printProductCodes;
    }

    /**
     * <p>Setter for the printProductCodes field of the {@link TransactionHeaderData}.</p>
     *
     * @param printProductCodes The printProductCodes field of the {@link TransactionHeaderData}.
     */
    public void setPrintProductCodes (boolean printProductCodes) {
        this.printProductCodes = printProductCodes;
    }

    /**
     * <p>Getter for the printNutritionInfo field of the {@link TransactionHeaderData}.</p>
     *
     * @return The printNutritionInfo field of the {@link TransactionHeaderData}.
     */
    public boolean getPrintNutritionInfo () {
        return printNutritionInfo;
    }

    /**
     * <p>Setter for the printNutritionInfo field of the {@link TransactionHeaderData}.</p>
     *
     * @param printNutritionInfo The printNutritionInfo field of the {@link TransactionHeaderData}.
     */
    public void setPrintNutritionInfo (boolean printNutritionInfo) {
        this.printNutritionInfo = printNutritionInfo;
    }

    /**
     * <p>Getter for the badgeNumDigitsPrinted field of the {@link TransactionHeaderData}.</p>
     *
     * @return The badgeNumDigitsPrinted field of the {@link TransactionHeaderData}.
     */
    public int getBadgeNumDigitsPrinted () {
        return badgeNumDigitsPrinted;
    }

    /**
     * <p>Setter for the badgeNumDigitsPrinted field of the {@link TransactionHeaderData}.</p>
     *
     * @param badgeNumDigitsPrinted The badgeNumDigitsPrinted field of the {@link TransactionHeaderData}.
     */
    public void setBadgeNumDigitsPrinted (int badgeNumDigitsPrinted) {
        this.badgeNumDigitsPrinted = badgeNumDigitsPrinted;
    }

    /**
     * <p>Getter for the printTransactionBarCode field of the {@link TransactionHeaderData}.</p>
     *
     * @return The printTransactionBarCode field of the {@link TransactionHeaderData}.
     */
    public boolean getPrintTransactionBarCode () {
        return printTransactionBarCode;
    }

    /**
     * <p>Setter for the printTransactionBarCode field of the {@link TransactionHeaderData}.</p>
     *
     * @param printTransactionBarCode The printTransactionBarCode field of the {@link TransactionHeaderData}.
     */
    public void setPrintTransactionBarCode (boolean printTransactionBarCode) {
        this.printTransactionBarCode = printTransactionBarCode;
    }

    /**
     * <p>Getter for the printGrossWeight field of the {@link TransactionHeaderData}.</p>
     *
     * @return The printGrossWeight field of the {@link TransactionHeaderData}.
     */
    public boolean getPrintGrossWeight () {
        return printGrossWeight;
    }

    /**
     * <p>Setter for the printGrossWeight field of the {@link TransactionHeaderData}.</p>
     *
     * @param printGrossWeight The printGrossWeight field of the {@link TransactionHeaderData}.
     */
    public void setPrintGrossWeight (boolean printGrossWeight) {
        this.printGrossWeight = printGrossWeight;
    }

    /**
     * <p>Getter for the printNetWeight field of the {@link TransactionHeaderData}.</p>
     *
     * @return The printNetWeight field of the {@link TransactionHeaderData}.
     */
    public boolean getPrintNetWeight () {
        return printNetWeight;
    }

    /**
     * <p>Setter for the printNetWeight field of the {@link TransactionHeaderData}.</p>
     *
     * @param printNetWeight The printNetWeight field of the {@link TransactionHeaderData}.
     */
    public void setPrintNetWeight (boolean printNetWeight) {
        this.printNetWeight = printNetWeight;
    }

    /**
     * <p>Getter for the prepTime field of the {@link TransactionHeaderData}.</p>
     *
     * @return The prepTime field of the {@link TransactionHeaderData}.
     */
    public int getPrepTime () {
        return prepTime;
    }

    /**
     * <p>Setter for the prepTime field of the {@link TransactionHeaderData}.</p>
     *
     * @param prepTime The prepTime field of the {@link TransactionHeaderData}.
     */
    public void setPrepTime (int prepTime) {
        this.prepTime = prepTime;
    }

    /**
     * <p>Getter for the recordTransactionsLocally field of the {@link TransactionHeaderData}.</p>
     *
     * @return The recordTransactionsLocally field of the {@link TransactionHeaderData}.
     */
    public boolean getRecordTransactionsLocally () {
        return recordTransactionsLocally;
    }

    /**
     * <p>Setter for the recordTransactionsLocally field of the {@link TransactionHeaderData}.</p>
     *
     * @param recordTransactionsLocally The recordTransactionsLocally field of the {@link TransactionHeaderData}.
     */
    public void setRecordTransactionsLocally (boolean recordTransactionsLocally) {
        this.recordTransactionsLocally = recordTransactionsLocally;
    }

    /**
     * <p>Getter for the diningOptionName field of the {@link TransactionHeaderData}.</p>
     *
     * @return The diningOptionName field of the {@link TransactionHeaderData}.
     */
    public String getDiningOptionName () {
        return diningOptionName;
    }

    /**
     * <p>Setter for the diningOptionName field of the {@link TransactionHeaderData}.</p>
     *
     * @param diningOptionName The diningOptionName field of the {@link TransactionHeaderData}.
     */
    public void setDiningOptionName (String diningOptionName) {
        this.diningOptionName = diningOptionName;
    }
    
}