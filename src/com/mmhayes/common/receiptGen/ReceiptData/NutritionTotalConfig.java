package com.mmhayes.common.receiptGen.ReceiptData;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by nyu on 7/26/2017.
 */
public class NutritionTotalConfig
{
    private final static String COMMON_LOG_FILE = "ConsolidatedReceipt.log";

    private int nutritionTotalsID = 0;
    private String dvCalorieValLbl = "";
    private BigDecimal nutritionInfo1Total;
    private BigDecimal nutritionInfo2Total;
    private BigDecimal nutritionInfo3Total;
    private BigDecimal nutritionInfo4Total;
    private BigDecimal nutritionInfo5Total;

    public NutritionTotalConfig()
    {
    }

    /**
     * <p>Constructor for a {@link NutritionTotalConfig} which allows all the properties to be set from the constructor.</p>
     *
     * @param nutritionTotalsID ID of the nutrition total configuration.
     * @param dvCalorieValLbl The label {@link String} for daily calories.
     * @param nutritionInfo1Total The nutrition info 1 total as a {@link BigDecimal}.
     * @param nutritionInfo2Total The nutrition info 2 total as a {@link BigDecimal}.
     * @param nutritionInfo3Total The nutrition info 3 total as a {@link BigDecimal}.
     * @param nutritionInfo4Total The nutrition info 4 total as a {@link BigDecimal}.
     * @param nutritionInfo5Total The nutrition info 5 total as a {@link BigDecimal}.
     */
    public NutritionTotalConfig (int nutritionTotalsID,
                                 String dvCalorieValLbl,
                                 BigDecimal nutritionInfo1Total,
                                 BigDecimal nutritionInfo2Total,
                                 BigDecimal nutritionInfo3Total,
                                 BigDecimal nutritionInfo4Total,
                                 BigDecimal nutritionInfo5Total) {
        this.nutritionTotalsID = nutritionTotalsID;
        this.dvCalorieValLbl = dvCalorieValLbl;
        this.nutritionInfo1Total = nutritionInfo1Total;
        this.nutritionInfo2Total = nutritionInfo2Total;
        this.nutritionInfo3Total = nutritionInfo3Total;
        this.nutritionInfo4Total = nutritionInfo4Total;
        this.nutritionInfo5Total = nutritionInfo5Total;
    }

    public void setNutritionTotalsID(int nutritionTotalsID)
    {
        this.nutritionTotalsID = nutritionTotalsID;
    }

    public void setDvCalorieValLbl(String dvCalorieValLbl)
    {
        this.dvCalorieValLbl = dvCalorieValLbl;
    }

    public void setNutritionInfo1Total(String nutritionInfo1Total)
    {
        if (nutritionInfo1Total.equals("") == false)
        {
            this.nutritionInfo1Total = new BigDecimal(nutritionInfo1Total);
        }
    }

    public void setNutritionInfo2Total(String nutritionInfo2Total)
    {
        if (nutritionInfo2Total.equals("") == false)
        {
            this.nutritionInfo2Total = new BigDecimal(nutritionInfo2Total);
        }
    }

    public void setNutritionInfo3Total(String nutritionInfo3Total)
    {
        if (nutritionInfo3Total.equals("") == false)
        {
            this.nutritionInfo3Total = new BigDecimal(nutritionInfo3Total);
        }
    }

    public void setNutritionInfo4Total(String nutritionInfo4Total)
    {
        if (nutritionInfo4Total.equals("") == false)
        {
            this.nutritionInfo4Total = new BigDecimal(nutritionInfo4Total);
        }
    }

    public void setNutritionInfo5Total(String nutritionInfo5Total)
    {
        if (nutritionInfo5Total.equals("") == false)
        {
            this.nutritionInfo5Total = new BigDecimal(nutritionInfo5Total);
        }
    }

    public int getNutritionTotalsID()
    {
        return nutritionTotalsID;
    }

    public String getDvCalorieValLbl()
    {
        return dvCalorieValLbl;
    }

    public BigDecimal getNutritionInfo1Total()
    {
        return nutritionInfo1Total;
    }

    public BigDecimal getNutritionInfo2Total()
    {
        return nutritionInfo2Total;
    }

    public BigDecimal getNutritionInfo3Total()
    {
        return nutritionInfo3Total;
    }

    public BigDecimal getNutritionInfo4Total()
    {
        return nutritionInfo4Total;
    }

    public BigDecimal getNutritionInfo5Total()
    {
        return nutritionInfo5Total;
    }
}
