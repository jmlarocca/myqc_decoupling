package com.mmhayes.common.receiptGen.ReceiptData;

/*
    $Author: nyu $: Author of last commit
    $Date: 2020-10-07 12:42:40 -0400 (Wed, 07 Oct 2020) $: Date of last commit
    $Rev: 12813 $: Revision of last commit
    Notes: Contains methods for building KDS print jobs.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.TransactionData.Plu;
import com.mmhayes.common.receiptGen.TransactionData.PrepOption;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHTree;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.math.NumberUtils;

import java.util.*;

/**
 * <p>Contains methods for building KDS print jobs.</p>
 *
 */
public class KitchenDisplaySystemJobInfo {

    // private member variables for a KitchenDisplaySystemJobInfo
    private static final String KDS_LOG = "KDSJobInfo.log";
    public static final String PRODUCT_INDICATOR = "PROD__";
    public static final String MODIFIER_INDICATOR = "MOD__";

    /**
     * <p>Determines which products within the transaction will be displayed on each KDS station.</p>
     *
     * @param receiptData The {@link ReceiptData} containing information about the transaction that may be used to build the detail that should be sent to KDS.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to the details that should be sent to KDS.
     */
    public static ArrayList<KPJobDetail> getKDSPrintJobs (ReceiptData receiptData) {

        // make sure we have ReceiptData
        if (receiptData == null) {
            Logger.logMessage("The ReceiptData instance passed to KitchenDisplaySystemJobInfo.getKDSPrintJobs can't be null!", KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the ReceiptData contains valid TransactionData
        if (receiptData.getTxnData() == null) {
            Logger.logMessage("The ReceiptData instance passed to KitchenDisplaySystemJobInfo.getKDSPrintJobs doesn't contain any valid transaction information!", KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the TransactionData contains a DataManager
        if (receiptData.getTxnData().getDataManager() == null) {
            Logger.logMessage("The TransactionData instance within the ReceiptData instance passed to KitchenDisplaySystemJobInfo.getKDSPrintJobs " +
                    "doesn't contain a valid DataManager!", KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        TransactionData transactionData = receiptData.getTxnData();
        DataManager dataManager = transactionData.getDataManager();

        // sort the product transaction line items within the transaction based on the parent -> child product relationship
        ArrayList<MMHTree<Plu>> productsInTransaction = getProductsInTransaction(transactionData);
        if (DataFunctions.isEmptyCollection(productsInTransaction)) {
            Logger.logMessage("No parent products have been found within KitchenDisplaySystemJobInfo.getKDSPrintJobs, unable " +
                    "to get any print job details that should be sent to KDS", KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // get which KDS prep stations are mapped to each parent product as well as whether or not the parent product should
        // be on an expeditor station, and whether or not the parent product is a dining option
        productsInTransaction = getKDSPrepStationsMappedToParentProductsInTransaction(dataManager, transactionData, productsInTransaction);

        // check if the transaction was a remote order
        boolean isRemoteOrder = isRemoteOrder(dataManager, transactionData);

        // check whether or not to send remote orders to the expeditor for the revenue center
        boolean remoteOrdersUseExpeditor = remoteOrdersUseExpeditor(dataManager, transactionData);

        // get the printer ID of the KDS expeditor station in the revenue center if it exists
        int kdsExpeditorPrinterID = getKDSExpeditorPrinterID(dataManager, transactionData);
        boolean kdsExpeditorStationPresent = kdsExpeditorPrinterID > 0;

        // get the printer ID of the KDS remote order station in the revenue center if it exists
        int kdsRemoteOrderPrinterID = getKDSRemoteOrderPrinterID(dataManager, transactionData);
        boolean kdsRemoteOrderStationPresent = kdsRemoteOrderPrinterID > 0;

        Logger.logMessage(String.format("The transaction with an ID of %s %s made remotely.",
                Objects.toString(transactionData.getTransactionID(), "N/A"),
                Objects.toString(isRemoteOrder ? "was" : "wasn't", "N/A")), KDS_LOG, Logger.LEVEL.TRACE);

        Logger.logMessage(String.format("The revenue center with an ID of %s %s use the expeditor for remote orders.",
                Objects.toString(transactionData.getRevenueCenterID(), "N/A"),
                Objects.toString(remoteOrdersUseExpeditor ? "should" : "shouldn't", "N/A")), KDS_LOG, Logger.LEVEL.TRACE);

        if (kdsExpeditorStationPresent) {
            Logger.logMessage(String.format("Found a KDS expeditor station within the revenue center with an ID of %s and it has a printer ID of %s.",
                    Objects.toString(transactionData.getRevenueCenterID(), "N/A"),
                    Objects.toString(kdsExpeditorPrinterID, "N/A")), KDS_LOG, Logger.LEVEL.TRACE);
        }
        else {
            Logger.logMessage(String.format("No KDS expeditor station is present within the revenue center with an ID of %s.",
                    Objects.toString(transactionData.getRevenueCenterID(), "N/A")), KDS_LOG, Logger.LEVEL.TRACE);
        }

        if (kdsRemoteOrderStationPresent) {
            Logger.logMessage(String.format("Found a KDS remote order station within the revenue center with an ID of %s and it has a printer ID of %s.",
                    Objects.toString(transactionData.getRevenueCenterID(), "N/A"),
                    Objects.toString(kdsRemoteOrderPrinterID, "N/A")), KDS_LOG, Logger.LEVEL.TRACE);
        }
        else {
            Logger.logMessage(String.format("No KDS remote order station is present within the revenue center with an ID of %s.",
                    Objects.toString(transactionData.getRevenueCenterID(), "N/A")), KDS_LOG, Logger.LEVEL.TRACE);
        }

        // get the printer ID to station ID lookup
        HashMap<Integer, Integer> printerIDToStationIDLookup = getPrinterIDToStationIDLookup(dataManager);

        // get and return the KDS print job details
        ArrayList<KPJobDetail> kdsPrintJobDetails = buildKDSPrintJobDetails(transactionData, productsInTransaction, isRemoteOrder,
                remoteOrdersUseExpeditor, kdsExpeditorPrinterID, kdsRemoteOrderPrinterID, printerIDToStationIDLookup);
        if (!DataFunctions.isEmptyCollection(kdsPrintJobDetails)) {
            logKDSPrintJobDetailsByPrinterID(kdsPrintJobDetails);
            return kdsPrintJobDetails;
        }

        return null;
    }

    /**
     * <p>Iterates through the transaction line items and organizes the products/modifiers within the transaction by their parent product.</p>
     *
     * @param transactionData The {@link TransactionData} instance which contains the line items within the transaction.
     * @return An {@link ArrayList} of a {@link MMHTree} of {@link Plu} corresponding to the product line items within the transaction organized by their parent product.
     */
    @SuppressWarnings({"Convert2streamapi", "TypeMayBeWeakened"})
    private static ArrayList<MMHTree<Plu>> getProductsInTransaction (TransactionData transactionData) {

        // make sure there are transaction line items within the transaction
        if (DataFunctions.isEmptyCollection(transactionData.getTransItems())) {
            Logger.logMessage(String.format("No transaction line items were found within KitchenDisplaySystemJobInfo.getProductsInTransaction, " +
                    "unable to get the products within the transaction with an ID of %s!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // iterate through all the transaction line items and get all the product line items
        ArrayList<Plu> allProducts = new ArrayList<>();
        for (Object transLineItemObj : transactionData.getTransItems()) {
            if ((transLineItemObj != null) && (transLineItemObj instanceof Plu)) {
                Plu product = (Plu) transLineItemObj;
                if (!product.hasBeenPrinted()) {
                    // add the unprinted product to the list of all products within the transaction
                    allProducts.add(product);
                }
            }
        }

        // make sure there were products within the transaction
        if (DataFunctions.isEmptyCollection(allProducts)) {
            Logger.logMessage(String.format("No unprinted products were found within the transaction line items in KitchenDisplaySystemJobInfo.getProductsInTransaction, " +
                    "unable to get the products within the transaction with an ID of %s!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // sort all the products in the transaction by their parent product
        ArrayList<MMHTree<Plu>> productsInTransaction = sortProductsInTransactionByParent(transactionData, allProducts);
        if (!DataFunctions.isEmptyCollection(productsInTransaction)) {
            // log the products in the transaction sorted by their parent
            logProductsInTransaction(productsInTransaction);
            return productsInTransaction;
        }

        return null;
    }

    /**
     * <p>Iterates through all the products in the transaction and sorts them by their parent product if applicable.</p>
     *
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @param productsInTransaction An {@link ArrayList} of {@link Plu} corresponding to all the products within the transaction.
     * @return An {@link ArrayList} of a {@link MMHTree} of {@link Plu} corresponding to the product line items within the transaction organized by their parent product.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "Convert2streamapi", "OverlyComplexMethod"})
    private static ArrayList<MMHTree<Plu>> sortProductsInTransactionByParent (TransactionData transactionData, ArrayList<Plu> productsInTransaction) {

        // the ArrayList to build and return
        ArrayList<MMHTree<Plu>> productsSortedByTheirParentProducts = new ArrayList<>();

        // sort all the products into two groups, one for parent products and one for modifiers
        ArrayList<Plu> parentProducts = new ArrayList<Plu>();
        ArrayList<Plu> modifierProducts = new ArrayList<Plu>();
        for (Plu product : productsInTransaction) {
            if ((product != null) && (product.getPaTransLineItemModID() <= 0)) {
                parentProducts.add(product);
            }
            else if ((product != null) && (product.getPaTransLineItemModID() > 0)) {
                modifierProducts.add(product);
            }
        }

        // make sure we have valid parent products
        if (DataFunctions.isEmptyCollection(parentProducts)) {
            Logger.logMessage(String.format("No valid parent products have been found in KitchenDisplaySystemJobInfo.sortProductsInTransactionByParent, " +
                    "unable to sort the products within the transaction with an ID of %s by their parent products!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // start a new MMHTree for each parent product
        for (Plu parentProduct : parentProducts) {
            productsSortedByTheirParentProducts.add(new MMHTree<>(parentProduct));
        }

        // append modifiers to each parent product tree
        if (!DataFunctions.isEmptyCollection(modifierProducts)) {
            for (Plu modifierProduct : modifierProducts) {
                Plu parentProduct = null;
                for (Plu potentialParentProduct : parentProducts) {
                    if ((modifierProduct.getPluID() != potentialParentProduct.getPluID()) && (modifierProduct.getPATransLineItemID() == potentialParentProduct.getPATransLineItemID())) {
                        parentProduct = potentialParentProduct;
                    }
                }
                if (parentProduct != null) {
                    // find the parent product tree so this modifier may be appended to it
                    for (MMHTree<Plu> parentProductTree : productsSortedByTheirParentProducts) {
                        if (parentProductTree.getData().equals(parentProduct)) {
                            parentProductTree.addChild(new MMHTree<>(modifierProduct));
                        }
                    }
                }
            }
        }

        return productsSortedByTheirParentProducts;
    }

    /**
     * <p>Utility method to log the products within the transaction sorted by their parent product.</p>
     *
     * @param productsInTransaction An {@link ArrayList} of a {@link MMHTree} of {@link Plu} corresponding to the product line items within the transaction organized by their parent product.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private static void logProductsInTransaction (ArrayList<MMHTree<Plu>> productsInTransaction) {

        Logger.logMessage("Now logging the products within the transaction sorted by their parent product.", KDS_LOG, Logger.LEVEL.TRACE);
        for (MMHTree<Plu> parentProductTree : productsInTransaction) {
            parentProductTree.logTree(parentProductTree, "--", KDS_LOG, Logger.LEVEL.TRACE);
        }

    }

    /**
     * <p>Iterates through the parent products within the transaction and gets any valid KDS prep stations mapped to the product.</p>
     *
     * @param dataManager The {@link DataManager} instance to use to query the database.
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @param productsInTransaction An {@link ArrayList} of {@link Plu} corresponding to all the products within the transaction.
     * @return An {@link ArrayList} of {@link Plu} corresponding to all the products within the transaction with the
     * isDiningOption, printsOnExpeditor, and mappedKDSPrepStationPrinterIDs fields set for each parent product within the transaction.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "Convert2streamapi"})
    private static ArrayList<MMHTree<Plu>> getKDSPrepStationsMappedToParentProductsInTransaction (DataManager dataManager,
                                                                                                  TransactionData transactionData,
                                                                                                  ArrayList<MMHTree<Plu>> productsInTransaction) {
        // make sure the ID of the revenue center in which the transaction took place is valid
        if (transactionData.getRevenueCenterID() <= 0) {
            Logger.logMessage(String.format("Found a revenue center ID that was less than or equal to 0 in KitchenDisplaySystemJobInfo.getKDSPrepStationsMappedToParentProductsInTransaction, " +
                    "unable to get valid KDS prep stations mapped to parent products within the transaction with an ID of %s!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // get the IDs of parent products within the transaction
        ArrayList<Integer> parentProductIDs = new ArrayList<>();
        for (MMHTree<Plu> parentProductTree : productsInTransaction) {
            parentProductIDs.add(parentProductTree.getData().getPluID());
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetKDSPrepStationsInRCMappedToProductsInTxn").addIDList(1, parentProductIDs).addIDList(2, transactionData.getRevenueCenterID());
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                int paPluID = HashMapDataFns.getIntVal(hm, "PAPLUID");
                boolean isDiningOption = HashMapDataFns.getBooleanVal(hm, "ISDININGOPTION");
                boolean printsOnExpeditor = HashMapDataFns.getBooleanVal(hm, "PRINTSONEXPEDITOR");
                int[] mappedKDSPrepStationPrinterIDs = HashMapDataFns.getIntArrVal(hm, "MAPPEDKDSPREPSTATIONS");
                // get the parent product the information is for
                for (MMHTree<Plu> parentProductTree : productsInTransaction) {
                    if (parentProductTree.getData().getPluID() == paPluID) {
                        Plu parentProduct = parentProductTree.getData();
                        parentProduct.setIsDiningOption(isDiningOption);
                        parentProduct.setPrintsOnExpeditor(printsOnExpeditor);
                        parentProduct.setMappedKDSPrepStationPrinterIDs(mappedKDSPrepStationPrinterIDs);
                    }
                }
            }
        }

        return productsInTransaction;
    }

    /**
     * <p>Queries the database to determine whether or not the order was made remotely.</p>
     *
     * @param dataManager The {@link DataManager} instance to use to query the database.
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @return Whether or not the order was made remotely.
     */
    private static boolean isRemoteOrder (DataManager dataManager, TransactionData transactionData) {

        // make sure we have a valid terminal ID for the transaction
        if (transactionData.getTerminalID() <= 0) {
            Logger.logMessage(String.format("The terminal ID for the transaction with an ID of %s in KitchenDisplaySystemJobInfo.isRemoteOrder must be greater than 0!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetIsRemoteOrderByTerminalID").addIDList(1, transactionData.getTerminalID());
        Object queryRes = sql.getSingleField(dataManager);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (queryRes.toString().equalsIgnoreCase("true"))) {
            return true;
        }
        else if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (queryRes.toString().equalsIgnoreCase("1"))) {
            return true;
        }

        return false;
    }

    /**
     * <p>Queries the database to determine whether or not remote orders should go to the expeditor.</p>
     *
     * @param dataManager The {@link DataManager} instance to use to query the database.
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @return Whether or not remote order should be sent to the expeditor.
     */
    private static boolean remoteOrdersUseExpeditor (DataManager dataManager, TransactionData transactionData) {

        // make sure we have a valid revenue center ID for the transaction
        if (transactionData.getTerminalID() <= 0) {
            Logger.logMessage(String.format("The revenue center ID for the transaction with an ID of %s in KitchenDisplaySystemJobInfo.remoteOrdersUseExpeditor must be greater than 0!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetRemoteOrdersUseExpeditor").addIDList(1, transactionData.getRevenueCenterID());
        Object queryRes = sql.getSingleField(dataManager);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (queryRes.toString().equalsIgnoreCase("true"))) {
            return true;
        }
        else if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (queryRes.toString().equalsIgnoreCase("1"))) {
            return true;
        }

        return false;
    }

    /**
     * <p>Queries the database to determine the ID of the expeditor station in the revenue center if it is present.</p>
     *
     * @param dataManager The {@link DataManager} instance to use to query the database.
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @return The ID of the expeditor station in the revenue center if it is present.
     */
    private static int getKDSExpeditorPrinterID (DataManager dataManager, TransactionData transactionData) {

        // make sure we have a valid revenue center ID for the transaction
        if (transactionData.getTerminalID() <= 0) {
            Logger.logMessage(String.format("The revenue center ID for the transaction with an ID of %s in KitchenDisplaySystemJobInfo.getKDSExpeditorPrinterID must be greater than 0!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return -1;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetKDSExpeditorPrinterID").addIDList(1, transactionData.getRevenueCenterID());
        Object queryRes = sql.getSingleField(dataManager);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString()))) {
            return Integer.parseInt(queryRes.toString());
        }

        return -1;
    }

    /**
     * <p>Queries the database to determine the ID of the remote order station in the revenue center if it is present.</p>
     *
     * @param dataManager The {@link DataManager} instance to use to query the database.
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @return The ID of the remote order station in the revenue center if it is present.
     */
    private static int getKDSRemoteOrderPrinterID (DataManager dataManager, TransactionData transactionData) {

        // make sure we have a valid revenue center ID for the transaction
        if (transactionData.getTerminalID() <= 0) {
            Logger.logMessage(String.format("The revenue center ID for the transaction with an ID of %s in KitchenDisplaySystemJobInfo.getKDSRemoteOrderPrinterID must be greater than 0!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return -1;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetKDSRemoteOrderPrinterID").addIDList(1, transactionData.getRevenueCenterID());
        Object queryRes = sql.getSingleField(dataManager);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString()))) {
            return Integer.parseInt(queryRes.toString());
        }

        return -1;
    }

    /**
     * <p>Queries the database to get the KDS station IDs associated with each printer ID.</p>
     *
     * @param dataManager The {@link DataManager} instance to use to query the database.
     * @return A {@link HashMap} whose {@link Integer} key is for a KDS printer ID and whose {@link Integer} value is for the corresponding KDS station ID.
     */
    private static HashMap<Integer, Integer> getPrinterIDToStationIDLookup (DataManager dataManager) {

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetKDSPrinterIDToStationIDMappings");
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            HashMap<Integer, Integer> printerIDToStationIDLookup = new HashMap<>();
            for (HashMap hm : queryRes) {
                int printerID = HashMapDataFns.getIntVal(hm, "PRINTERID");
                int kdsStationID = HashMapDataFns.getIntVal(hm, "STATIONID");
                if ((printerID > 0) && (kdsStationID > 0)) {
                    printerIDToStationIDLookup.put(printerID, kdsStationID);
                }
            }
            if (!DataFunctions.isEmptyMap(printerIDToStationIDLookup)) {
                return printerIDToStationIDLookup;
            }
        }

        return null;
    }

    /**
     * <p>Creates print job details that should be sent to KDS.</p>
     *
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @param productsInTransaction An {@link ArrayList} of a {@link MMHTree} of {@link Plu} corresponding to the product line items within the transaction organized by their parent product.
     * @param isRemoteOrder Whether or not the order was placed remotely.
     * @param remoteOrdersUseExpeditor Whether or not remote orders placed within the revenue center should be sent to the expeditor.
     * @param kdsExpeditorPrinterID The printer ID of the KDS expeditor station within the same revenue center as the transaction.
     * @param kdsRemoteOrderPrinterID The printer ID of the KDS remote order station within the same revenue center as the transaction.
     * @param printerIDToStationIDLookup A {@link HashMap} whose {@link Integer} key is for a KDS printer ID and whose {@link Integer} value is for the corresponding KDS station ID.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to print job details that should be sent to KDS.
     */
    @SuppressWarnings("OverlyComplexMethod")
    private static ArrayList<KPJobDetail> buildKDSPrintJobDetails (TransactionData transactionData,
                                                                   ArrayList<MMHTree<Plu>> productsInTransaction,
                                                                   boolean isRemoteOrder,
                                                                   boolean remoteOrdersUseExpeditor,
                                                                   int kdsExpeditorPrinterID,
                                                                   int kdsRemoteOrderPrinterID,
                                                                   HashMap<Integer, Integer> printerIDToStationIDLookup) {
        // the KDS print job details to return
        ArrayList<KPJobDetail> kdsPrintJobDetails = new ArrayList<>();

        // get the dining option within the transaction if it exists
        MMHTree<Plu> diningOptionTree = getDiningOptionTree(productsInTransaction);
        if (diningOptionTree != null) {
            Logger.logMessage(String.format("Found the dining option %s within the transaction with an ID of %s.",
                    Objects.toString(diningOptionTree.getData().getName(), "N/A"),
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.TRACE);
            // remove the dining option from the products in the transaction
            if (!productsInTransaction.remove(diningOptionTree)) {
                Logger.logMessage(String.format("A problem occurred trying to remove the dining option %s from the list of products within the transaction with an ID of %s!",
                        Objects.toString(diningOptionTree.getData().getName(), "N/A"),
                        Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            }
        }
        else {
            Logger.logMessage(String.format("No dining option has been found within the transaction with an ID of %s.",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.TRACE);
        }

        // get print job details for KDS prep stations
        ArrayList<KPJobDetail> kdsPrepPrintJobDetails = getKDSPrepPrintJobDetails(transactionData, productsInTransaction, isRemoteOrder, remoteOrdersUseExpeditor, kdsExpeditorPrinterID, printerIDToStationIDLookup);
        if (!DataFunctions.isEmptyCollection(kdsPrepPrintJobDetails)) {
            kdsPrintJobDetails.addAll(kdsPrepPrintJobDetails);
        }

        // get print job details for the KDS expeditor station
        if (kdsExpeditorPrinterID > 0) {
            ArrayList<KPJobDetail> kdsExpeditorPrintJobDetails = getKDSExpeditorPrintJobDetails(transactionData, productsInTransaction, kdsPrintJobDetails, isRemoteOrder, remoteOrdersUseExpeditor, kdsExpeditorPrinterID);
            if (!DataFunctions.isEmptyCollection(kdsExpeditorPrintJobDetails)) {
                kdsPrintJobDetails.addAll(kdsExpeditorPrintJobDetails);
            }
        }

        // get print job details for the KDS remote order station
        if (kdsRemoteOrderPrinterID > 0) {
            ArrayList<KPJobDetail> kdsRemoteOrderPrintJobDetails = getKDSRemoteOrderPrintJobDetails(transactionData, productsInTransaction, isRemoteOrder, kdsRemoteOrderPrinterID);
            if (!DataFunctions.isEmptyCollection(kdsRemoteOrderPrintJobDetails)) {
                kdsPrintJobDetails.addAll(kdsRemoteOrderPrintJobDetails);
            }
        }

        if (diningOptionTree != null) {
            // get print job details for the dining option on prep stations
            ArrayList<KPJobDetail> dinOptPJDForPrepStns = getDiningOptionPrintJobDetailsForKDSPrepStations(transactionData, kdsPrintJobDetails, diningOptionTree, kdsExpeditorPrinterID, printerIDToStationIDLookup);
            if (!DataFunctions.isEmptyCollection(dinOptPJDForPrepStns)) {
                for (KPJobDetail dinOptPrintJobDetail : dinOptPJDForPrepStns) {
                    // get the index at which to insert the dining option print job detail so that it will appear first on the KDS station
                    int insertionIndex = getKDSPrintJobDetailInsertionIndex(kdsPrintJobDetails, dinOptPrintJobDetail);
                    if (insertionIndex >= 0) {
                        kdsPrintJobDetails.add(insertionIndex, dinOptPrintJobDetail);
                    }
                    else {
                        Logger.logMessage(String.format("Unable to determine the index within the existing print job details " +
                                "to insert the dining option %s being sent to the KDS prep station with a printer ID of %s!",
                                Objects.toString(diningOptionTree.getData().getName(), "N/A"),
                                Objects.toString(dinOptPrintJobDetail.getPrinterID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
                    }
                }
            }

            // get the print job details for the dining option on the expeditor station
            if (kdsExpeditorPrinterID > 0) {
                ArrayList<KPJobDetail> dinOptPJDForExpStn = getDiningOptionPrintJobDetailsForTheKDSExpeditorStation(transactionData, kdsPrintJobDetails, diningOptionTree, isRemoteOrder, remoteOrdersUseExpeditor, kdsExpeditorPrinterID);
                if (!DataFunctions.isEmptyCollection(dinOptPJDForExpStn)) {
                    for (KPJobDetail dinOptPrintJobDetail : dinOptPJDForExpStn) {
                        // get the index at which to insert the dining option print job detail so that it will appear first on the KDS station
                        int insertionIndex = getKDSPrintJobDetailInsertionIndex(kdsPrintJobDetails, dinOptPrintJobDetail);
                        if (insertionIndex >= 0) {
                            kdsPrintJobDetails.add(insertionIndex, dinOptPrintJobDetail);
                        }
                        else {
                            // it's possible we might only have a dining option print job detail for a KDS expeditor station
                            kdsPrintJobDetails.add(0, dinOptPrintJobDetail);
                        }
                    }
                }
            }

            // get the print job details for the dining option on the remote order station
            if (kdsRemoteOrderPrinterID > 0) {
                ArrayList<KPJobDetail> dinOptPJDForROStn = getDiningOptionPrintJobDetailsForTheKDSRemoteOrderStation(transactionData, kdsPrintJobDetails, diningOptionTree, isRemoteOrder, kdsRemoteOrderPrinterID);
                if (!DataFunctions.isEmptyCollection(dinOptPJDForROStn)) {
                    for (KPJobDetail dinOptPrintJobDetail : dinOptPJDForROStn) {
                        // get the index at which to insert the dining option print job detail so that it will appear first on the KDS station
                        int insertionIndex = getKDSPrintJobDetailInsertionIndex(kdsPrintJobDetails, dinOptPrintJobDetail);
                        if (insertionIndex >= 0) {
                            kdsPrintJobDetails.add(insertionIndex, dinOptPrintJobDetail);
                        }
                        else {
                            Logger.logMessage(String.format("Unable to determine the index within the existing print job details " +
                                    "to insert the dining option %s being sent to the KDS remote order station with a printer ID of %s!",
                                    Objects.toString(diningOptionTree.getData().getName(), "N/A"),
                                    Objects.toString(dinOptPrintJobDetail.getPrinterID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
                        }
                    }
                }
            }
        }

        return kdsPrintJobDetails;
    }

    /**
     * <p>Iterates through the parent products within the transaction and returns the dining option if it is found.</p>
     *
     * @param productsInTransaction An {@link ArrayList} of a {@link MMHTree} of {@link Plu} corresponding to the product line items within the transaction organized by their parent product.
     * @return A {@link MMHTree} of {@link Plu} corresponding to the dining option within the transaction.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private static MMHTree<Plu> getDiningOptionTree (ArrayList<MMHTree<Plu>> productsInTransaction) {

        // search for the dining option
        for (MMHTree<Plu> parentProductTree : productsInTransaction) {
            if (parentProductTree.getData().isDiningOption()) {
                return parentProductTree;
            }
        }

        return null;
    }

    /**
     * <p>Creates print job details that should be sent to KDS prep stations.</p>
     *
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @param productsInTransaction An {@link ArrayList} of a {@link MMHTree} of {@link Plu} corresponding to the product line items within the transaction organized by their parent product.
     * @param isRemoteOrder Whether or not the order was placed remotely.
     * @param remoteOrdersUseExpeditor Whether or not remote orders placed within the revenue center should be sent to the expeditor.
     * @param kdsExpeditorPrinterID The printer ID of the KDS expeditor station within the same revenue center as the transaction.
     * @param printerIDToStationIDLookup A {@link HashMap} whose {@link Integer} key is for a KDS printer ID and whose {@link Integer} value is for the corresponding KDS station ID.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to print job details that should be sent to KDS prep stations.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    private static ArrayList<KPJobDetail> getKDSPrepPrintJobDetails (TransactionData transactionData,
                                                                     ArrayList<MMHTree<Plu>> productsInTransaction,
                                                                     boolean isRemoteOrder,
                                                                     boolean remoteOrdersUseExpeditor,
                                                                     int kdsExpeditorPrinterID,
                                                                     HashMap<Integer, Integer> printerIDToStationIDLookup) {
        // the KDS prep print job details to return
        ArrayList<KPJobDetail> kdsPrepPrintJobDetails = new ArrayList<>();

        for (MMHTree<Plu> parentProductTree : productsInTransaction) {
            // refer to the flow chart on SVN, https://mmhsource/svn/docs/pos/Development/Specs/KP_AND_KDS/KDS%20Flow%20Diagrams/HideOnExpeditorFlowForProduct.jpg
            if (kdsExpeditorPrinterID > 0) {
                // determine the station ID of the KDS expeditor station
                Integer hiddenKDSStationID = HashMapDataFns.getValFromMap(printerIDToStationIDLookup, kdsExpeditorPrinterID);
                if ((hiddenKDSStationID == null) || (hiddenKDSStationID <= 0)) {
                    hiddenKDSStationID = -1;
                }
                if (parentProductTree.getData().getPrintsOnExpeditor()) {
                    ArrayList<KPJobDetail> printJobDetails = createKDSPrepPrintJobDetailsForParentProductTree(transactionData, parentProductTree, -1);
                    if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                        kdsPrepPrintJobDetails.addAll(printJobDetails);
                    }
                }
                else {
                    if (isRemoteOrder) {
                        if (remoteOrdersUseExpeditor) {
                            ArrayList<KPJobDetail> printJobDetails = createKDSPrepPrintJobDetailsForParentProductTree(transactionData, parentProductTree, -1);
                            if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                                kdsPrepPrintJobDetails.addAll(printJobDetails);
                            }
                        }
                        else {
                            ArrayList<KPJobDetail> printJobDetails = createKDSPrepPrintJobDetailsForParentProductTree(transactionData, parentProductTree, hiddenKDSStationID);
                            if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                                kdsPrepPrintJobDetails.addAll(printJobDetails);
                            }
                        }
                    }
                    else {
                        ArrayList<KPJobDetail> printJobDetails = createKDSPrepPrintJobDetailsForParentProductTree(transactionData, parentProductTree, hiddenKDSStationID);
                        if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                            kdsPrepPrintJobDetails.addAll(printJobDetails);
                        }
                    }
                }
            }
            else {
                ArrayList<KPJobDetail> printJobDetails = createKDSPrepPrintJobDetailsForParentProductTree(transactionData, parentProductTree, -1);
                if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                    kdsPrepPrintJobDetails.addAll(printJobDetails);
                }
            }
        }

        return kdsPrepPrintJobDetails;
    }

    /**
     * <p>Creates KDS prep print job details for the given parent product tree.</p>
     *
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @param parentProductTree A {@link MMHTree} of {@link Plu} corresponding to a parent product within the transaction.
     * @param hiddenKDSStationID The ID of the KDS station to hide the product on if applicable.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to print job details that should be sent to KDS prep stations.
     */
    private static ArrayList<KPJobDetail> createKDSPrepPrintJobDetailsForParentProductTree (TransactionData transactionData, MMHTree<Plu> parentProductTree, int hiddenKDSStationID) {

        // make sure we have a valid printer host to handle the print job details
        if (transactionData.getPrinterHostID() <= 0) {
            Logger.logMessage(String.format("There isn't a printer host that can process the print jobs created from the transaction with an ID of %s!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // the KDS print job details to return
        ArrayList<KPJobDetail> kdsPrintJobDetailsForParentProductTree = new ArrayList<>();

        // get the printer IDs of the KDS prep stations mapped to the product
        int[] mappedKDSPrepStationPrinterIDs = parentProductTree.getData().getMappedKDSPrepStationPrinterIDs();

        String hiddenKDSStationIDStr = "";
        if (hiddenKDSStationID > 0) {
            hiddenKDSStationIDStr = String.valueOf(hiddenKDSStationID);
        }

        if (DataFunctions.isEmptyIntArr(mappedKDSPrepStationPrinterIDs)) {
            Logger.logMessage(String.format("No KDS prep stations are mapped to the product %s!", Objects.toString(parentProductTree.getData().getName(), "N/A")), KDS_LOG, Logger.LEVEL.TRACE);
        }
        else {
            for (int kdsPrepStationPrinterID : mappedKDSPrepStationPrinterIDs) {
                kdsPrintJobDetailsForParentProductTree.add(new KPJobDetail()
                        .printerID(kdsPrepStationPrinterID)
                        .printStatusID(PrintStatusType.WAITING)
                        .printerHostID(transactionData.getPrinterHostID())
                        .papluID(parentProductTree.getData().getPluID())
                        .quantity(parentProductTree.getData().getQuantity())
                        .isModifier(false)
                        .lineDetail(buildKDSText(parentProductTree.getData(), (parentProductTree.getData() != null ? parentProductTree.getData().getPATransLineItemID() : 0), kdsPrepStationPrinterID, false))
                        .hiddenStation(hiddenKDSStationIDStr));
            }

            // create print job details for modifiers
            ArrayList<MMHTree<Plu>> modifierTrees = parentProductTree.getChildren();
            if (!DataFunctions.isEmptyCollection(modifierTrees)) {
                for (MMHTree<Plu> modifierTree : modifierTrees) {
                    for (int kdsPrepStationPrinterID : mappedKDSPrepStationPrinterIDs) {
                        kdsPrintJobDetailsForParentProductTree.add(new KPJobDetail()
                                .printerID(kdsPrepStationPrinterID)
                                .printStatusID(PrintStatusType.WAITING)
                                .printerHostID(transactionData.getPrinterHostID())
                                .papluID(modifierTree.getData().getPluID())
                                .quantity(modifierTree.getData().getQuantity())
                                .isModifier(true)
                                .lineDetail(buildKDSText(modifierTree.getData(), (modifierTree.getData() != null ? modifierTree.getData().getPATransLineItemID() : 0), kdsPrepStationPrinterID, true))
                                .hiddenStation(hiddenKDSStationIDStr));
                    }
                }
            }
        }

        return kdsPrintJobDetailsForParentProductTree;
    }

    /**
     * <p>Creates print job details that should be sent to the KDS expeditor station.</p>
     *
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @param productsInTransaction An {@link ArrayList} of a {@link MMHTree} of {@link Plu} corresponding to the product line items within the transaction organized by their parent product.
     * @param existingKDSPrintJobDetails An {@link ArrayList} of {@link KPJobDetail} corresponding to the KDS print job details for KDS prep stations.
     * @param isRemoteOrder Whether or not the order was placed remotely.
     * @param remoteOrdersUseExpeditor Whether or not remote orders placed within the revenue center should be sent to the expeditor.
     * @param kdsExpeditorPrinterID The printer ID of the KDS expeditor station within the same revenue center as the transaction.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to print job details that should be sent to the KDS expeditor station.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    private static ArrayList<KPJobDetail> getKDSExpeditorPrintJobDetails (TransactionData transactionData,
                                                                          ArrayList<MMHTree<Plu>> productsInTransaction,
                                                                          ArrayList<KPJobDetail> existingKDSPrintJobDetails,
                                                                          boolean isRemoteOrder,
                                                                          boolean remoteOrdersUseExpeditor,
                                                                          int kdsExpeditorPrinterID) {
        // the KDS expeditor print job details to return
        ArrayList<KPJobDetail> kdsExpeditorPrintJobDetails = new ArrayList<>();

        for (MMHTree<Plu> parentProductTree : productsInTransaction) {
            // refer to the flow chart on SVN, https://mmhsource/svn/docs/pos/Development/Specs/KP_AND_KDS/KDS%20Flow%20Diagrams/CreateKDSExpPrintJobDetailFlowForProduct.jpg
            if (isRemoteOrder) {
                if (remoteOrdersUseExpeditor) {
                    if (!isProductOnPrepStation(parentProductTree, existingKDSPrintJobDetails)) {
                        ArrayList<KPJobDetail> printJobDetails = createKDSExpeditorPrintJobDetailsForParentProductTree(transactionData, parentProductTree, kdsExpeditorPrinterID);
                        if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                            kdsExpeditorPrintJobDetails.addAll(printJobDetails);
                        }
                    }
                }
                else {
                    if (parentProductTree.getData().getPrintsOnExpeditor()) {
                        if (!isProductOnPrepStation(parentProductTree, existingKDSPrintJobDetails)) {
                            ArrayList<KPJobDetail> printJobDetails = createKDSExpeditorPrintJobDetailsForParentProductTree(transactionData, parentProductTree, kdsExpeditorPrinterID);
                            if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                                kdsExpeditorPrintJobDetails.addAll(printJobDetails);
                            }
                        }
                    }
                }
            }
            else {
                if (parentProductTree.getData().getPrintsOnExpeditor()) {
                    if (!isProductOnPrepStation(parentProductTree, existingKDSPrintJobDetails)) {
                        ArrayList<KPJobDetail> printJobDetails = createKDSExpeditorPrintJobDetailsForParentProductTree(transactionData, parentProductTree, kdsExpeditorPrinterID);
                        if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                            kdsExpeditorPrintJobDetails.addAll(printJobDetails);
                        }
                    }
                }
            }
        }

        return kdsExpeditorPrintJobDetails;
    }

    /**
     * <p>Creates KDS expeditor print job details for the given parent product tree.</p>
     *
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @param parentProductTree A {@link MMHTree} of {@link Plu} corresponding to a parent product within the transaction.
     * @param kdsExpeditorPrinterID The printer ID of the KDS expeditor station.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to print job details that should be sent to the KDS expeditor station.
     */
    @SuppressWarnings("Convert2streamapi")
    private static ArrayList<KPJobDetail> createKDSExpeditorPrintJobDetailsForParentProductTree (TransactionData transactionData,
                                                                                                 MMHTree<Plu> parentProductTree,
                                                                                                 int kdsExpeditorPrinterID) {
        // make sure we have a valid printer host to handle the print job details
        if (transactionData.getPrinterHostID() <= 0) {
            Logger.logMessage(String.format("There isn't a printer host that can process the print jobs created from the transaction with an ID of %s!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // the KDS print job details to return
        ArrayList<KPJobDetail> kdsPrintJobDetailsForParentProductTree = new ArrayList<>();

        // create the print job detail for the parent product
        kdsPrintJobDetailsForParentProductTree.add(new KPJobDetail()
                .printerID(kdsExpeditorPrinterID)
                .printStatusID(PrintStatusType.WAITING)
                .printerHostID(transactionData.getPrinterHostID())
                .papluID(parentProductTree.getData().getPluID())
                .quantity(parentProductTree.getData().getQuantity())
                .isModifier(false)
                .lineDetail(buildKDSText(parentProductTree.getData(), (parentProductTree.getData() != null ? parentProductTree.getData().getPATransLineItemID() : 0), kdsExpeditorPrinterID, false))
                .hiddenStation(""));

        // create print job details for modifiers
        ArrayList<MMHTree<Plu>> modifierTrees = parentProductTree.getChildren();
        if (!DataFunctions.isEmptyCollection(modifierTrees)) {
            for (MMHTree<Plu> modifierTree : modifierTrees) {
                kdsPrintJobDetailsForParentProductTree.add(new KPJobDetail()
                        .printerID(kdsExpeditorPrinterID)
                        .printStatusID(PrintStatusType.WAITING)
                        .printerHostID(transactionData.getPrinterHostID())
                        .papluID(modifierTree.getData().getPluID())
                        .quantity(modifierTree.getData().getQuantity())
                        .isModifier(true)
                        .lineDetail(buildKDSText(modifierTree.getData(), (modifierTree.getData() != null ? modifierTree.getData().getPATransLineItemID() : 0), kdsExpeditorPrinterID, true))
                        .hiddenStation(""));
            }
        }

        return kdsPrintJobDetailsForParentProductTree;
    }

    /**
     * <p>Iterates through the KDS print job details for prep stations to check if a print job detail already exists for the given product.</p>
     *
     * @param parentProductTree A {@link MMHTree} of {@link Plu} corresponding to the parent product to check.
     * @param existingKDSPrintJobDetails An {@link ArrayList} of {@link KPJobDetail} corresponding to the KDS print job details for KDS prep stations.
     * @return Whether or not a print job detail already exists for the given product.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private static boolean isProductOnPrepStation (MMHTree<Plu> parentProductTree, ArrayList<KPJobDetail> existingKDSPrintJobDetails) {

        // no print job details for prep stations exist
        if (DataFunctions.isEmptyCollection(existingKDSPrintJobDetails)) {
            return false;
        }

        for (KPJobDetail kdsPrepPrintJobDetail : existingKDSPrintJobDetails) {
            if (parentProductTree.getData().getPluID() == kdsPrepPrintJobDetail.getPAPluID()) {
                return true;
            }
        }

        return false;
    }

    /**
     * <p>Creates print job details that should be sent to the KDS remote order station.</p>
     *
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @param productsInTransaction An {@link ArrayList} of a {@link MMHTree} of {@link Plu} corresponding to the product line items within the transaction organized by their parent product.
     * @param isRemoteOrder Whether or not the order was placed remotely.
     * @param kdsRemoteOrderPrinterID The printer ID of the KDS remote order station within the same revenue center as the transaction.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to print job details that should be sent to the KDS remote order station.
     */
    @SuppressWarnings({"Convert2streamapi", "TypeMayBeWeakened"})
    private static ArrayList<KPJobDetail> getKDSRemoteOrderPrintJobDetails (TransactionData transactionData,
                                                                            ArrayList<MMHTree<Plu>> productsInTransaction,
                                                                            boolean isRemoteOrder,
                                                                            int kdsRemoteOrderPrinterID) {
        // make sure we have a valid printer host to handle the print job details
        if (transactionData.getPrinterHostID() <= 0) {
            Logger.logMessage(String.format("There isn't a printer host that can process the print jobs created from the transaction with an ID of %s!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // the KDS remote order print job details to return
        ArrayList<KPJobDetail> kdsRemoteOrderPrintJobDetails = new ArrayList<>();

        for (MMHTree<Plu> parentProductTree : productsInTransaction) {
            // refer to the flow chart on SVN, https://mmhsource/svn/docs/pos/Development/Specs/KP_AND_KDS/KDS%20Flow%20Diagrams/CreateKDSRemOrdPrintJobDetailFlowForProduct.jpg
            if (isRemoteOrder) {
                // create the print job detail for the parent product
                kdsRemoteOrderPrintJobDetails.add(new KPJobDetail()
                        .printerID(kdsRemoteOrderPrinterID)
                        .printStatusID(PrintStatusType.WAITING)
                        .printerHostID(transactionData.getPrinterHostID())
                        .papluID(parentProductTree.getData().getPluID())
                        .quantity(parentProductTree.getData().getQuantity())
                        .isModifier(false)
                        .lineDetail(buildKDSText(parentProductTree.getData(),(parentProductTree.getData() != null ? parentProductTree.getData().getPATransLineItemID() : 0), kdsRemoteOrderPrinterID, false))
                        .hiddenStation(""));

                // create print job details for modifiers
                ArrayList<MMHTree<Plu>> modifierTrees = parentProductTree.getChildren();
                if (!DataFunctions.isEmptyCollection(modifierTrees)) {
                    for (MMHTree<Plu> modifierTree : modifierTrees) {
                        kdsRemoteOrderPrintJobDetails.add(new KPJobDetail()
                                .printerID(kdsRemoteOrderPrinterID)
                                .printStatusID(PrintStatusType.WAITING)
                                .printerHostID(transactionData.getPrinterHostID())
                                .papluID(modifierTree.getData().getPluID())
                                .quantity(modifierTree.getData().getQuantity())
                                .isModifier(true)
                                .lineDetail(buildKDSText(modifierTree.getData(), (modifierTree.getData() != null ? modifierTree.getData().getPATransLineItemID() : 0), kdsRemoteOrderPrinterID, true))
                                .hiddenStation(""));
                    }
                }
            }
        }

        return kdsRemoteOrderPrintJobDetails;
    }

    /**
     * <p>Creates print job details for the dining option to be sent to KDS prep stations.</p>
     *
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @param existingKDSPrintJobDetails An {@link ArrayList} of {@link KPJobDetail} corresponding to the KDS print job details.
     * @param diningOptionTree A {@link MMHTree} of {@link Plu} corresponding to the dining option within the transaction.
     * @param kdsExpeditorPrinterID The printer ID of the KDS expeditor station.
     * @param printerIDToStationIDLookup A {@link HashMap} whose {@link Integer} key is for a KDS printer ID and whose {@link Integer} value is for the corresponding KDS station ID.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to the print job details for the dining option to be sent to KDS prep stations.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private static ArrayList<KPJobDetail> getDiningOptionPrintJobDetailsForKDSPrepStations (TransactionData transactionData,
                                                                                            ArrayList<KPJobDetail> existingKDSPrintJobDetails,
                                                                                            MMHTree<Plu> diningOptionTree,
                                                                                            int kdsExpeditorPrinterID,
                                                                                            HashMap<Integer, Integer> printerIDToStationIDLookup) {
        // make sure we have a valid printer host to handle the print job details
        if (transactionData.getPrinterHostID() <= 0) {
            Logger.logMessage(String.format("There isn't a printer host that can process the print jobs created from the transaction with an ID of %s!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure there are existing KDS print job details
        if (DataFunctions.isEmptyCollection(existingKDSPrintJobDetails)) {
            Logger.logMessage(String.format("No existing print job details were found in KitchenDisplaySystemJobInfo.getDiningOptionPrintJobDetailsForKDSPrepStations, " +
                    "unable to create any print job details for the dining option %s.",
                    Objects.toString(diningOptionTree.getData().getName(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // the KDS prep station print job details to return for the dining option
        ArrayList<KPJobDetail> dinOptPJDForPrepStns = new ArrayList<>();

        // refer to the flow chart on SVN, https://mmhsource/svn/docs/pos/Development/Specs/KP_AND_KDS/KDS%20Flow%20Diagrams/AddDiningOptionToPrepStationFlow.jpg

        // get the KDS prep stations mapped to the dining option
        int[] kdsPrepStnPrinterIDsForDinOpt = diningOptionTree.getData().getMappedKDSPrepStationPrinterIDs();
        if (!DataFunctions.isEmptyIntArr(kdsPrepStnPrinterIDsForDinOpt)) {
            for (int printerID : kdsPrepStnPrinterIDsForDinOpt) {
                // check if there is a non dining option being sent to the KDS prep station, dining options can't be sent by themselves
                if (isNonDiningOptionBeingSentToStation(existingKDSPrintJobDetails, printerID)) {
                    if (kdsExpeditorPrinterID > 0) {
                        // show the dining option on the prep station and hide it on the expeditor station
                        String kdsExpeditorStationIDStr = "";
                        Integer kdsExpeditorStationID = HashMapDataFns.getValFromMap(printerIDToStationIDLookup, kdsExpeditorPrinterID);
                        if ((kdsExpeditorStationID != null) && (kdsExpeditorStationID > 0)) {
                            kdsExpeditorStationIDStr = String.valueOf(kdsExpeditorStationID);
                        }
                        dinOptPJDForPrepStns.add(new KPJobDetail()
                                .printerID(printerID)
                                .printStatusID(PrintStatusType.WAITING)
                                .printerHostID(transactionData.getPrinterHostID())
                                .papluID(diningOptionTree.getData().getPluID())
                                .quantity(diningOptionTree.getData().getQuantity())
                                .isModifier(false)
                                .lineDetail(buildKDSText(diningOptionTree.getData(), (diningOptionTree.getData() != null ? diningOptionTree.getData().getPATransLineItemID() : 0), printerID, false))
                                .hiddenStation(kdsExpeditorStationIDStr));
                    }
                    else {
                        // show the dining option on the prep station
                        dinOptPJDForPrepStns.add(new KPJobDetail()
                                .printerID(printerID)
                                .printStatusID(PrintStatusType.WAITING)
                                .printerHostID(transactionData.getPrinterHostID())
                                .papluID(diningOptionTree.getData().getPluID())
                                .quantity(diningOptionTree.getData().getQuantity())
                                .isModifier(false)
                                .lineDetail(buildKDSText(diningOptionTree.getData(), (diningOptionTree.getData() != null ? diningOptionTree.getData().getPATransLineItemID() : 0), printerID, false))
                                .hiddenStation(""));
                    }
                }
            }
        }
        else {
            Logger.logMessage(String.format("No KDS prep stations were found to be mapped to the dining option %s in " +
                    "KitchenDisplaySystemJobInfo.getDiningOptionPrintJobDetailsForKDSPrepStations.",
                    Objects.toString(diningOptionTree.getData().getName(), "N/A")), KDS_LOG, Logger.LEVEL.TRACE);
        }

        return dinOptPJDForPrepStns;
    }

    /**
     * <p>Creates print job details for the dining option to be sent to KDS expeditor station.</p>
     *
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @param existingKDSPrintJobDetails An {@link ArrayList} of {@link KPJobDetail} corresponding to the KDS print job details.
     * @param diningOptionTree A {@link MMHTree} of {@link Plu} corresponding to the dining option within the transaction.
     * @param isRemoteOrder Whether or not the order was placed remotely.
     * @param remoteOrdersUseExpeditor Whether or not remote orders placed within the revenue center should be sent to the expeditor.
     * @param kdsExpeditorPrinterID The printer ID of the KDS expeditor station.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to the print job details for the dining option to be sent to KDS prep station.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    private static ArrayList<KPJobDetail> getDiningOptionPrintJobDetailsForTheKDSExpeditorStation (TransactionData transactionData,
                                                                                                   ArrayList<KPJobDetail> existingKDSPrintJobDetails,
                                                                                                   MMHTree<Plu> diningOptionTree,
                                                                                                   boolean isRemoteOrder,
                                                                                                   boolean remoteOrdersUseExpeditor,
                                                                                                   int kdsExpeditorPrinterID) {
        // make sure we have a valid printer host to handle the print job details
        if (transactionData.getPrinterHostID() <= 0) {
            Logger.logMessage(String.format("There isn't a printer host that can process the print jobs created from the transaction with an ID of %s!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure there are existing KDS print job details
        if (DataFunctions.isEmptyCollection(existingKDSPrintJobDetails)) {
            Logger.logMessage(String.format("No existing print job details were found in KitchenDisplaySystemJobInfo.getDiningOptionPrintJobDetailsForTheKDSExpeditorStation, " +
                    "unable to create any print job details for the dining option %s.",
                    Objects.toString(diningOptionTree.getData().getName(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // the KDS expeditor station print job details to return for the dining option
        ArrayList<KPJobDetail> dinOptPJDForExpStn = new ArrayList<>();

        // refer to the flow chart on SVN, https://mmhsource/svn/docs/pos/Development/Specs/KP_AND_KDS/KDS%20Flow%20Diagrams/AddDiningOptionToExpeditorStationFlow.jpg

        if (isRemoteOrder) {
            if (remoteOrdersUseExpeditor) {
                // check if a non dining option is being sent to the KDS expeditor station
                if (isNonDiningOptionBeingSentToStation(existingKDSPrintJobDetails, kdsExpeditorPrinterID)) {
                    dinOptPJDForExpStn.add(new KPJobDetail()
                            .printerID(kdsExpeditorPrinterID)
                            .printStatusID(PrintStatusType.WAITING)
                            .printerHostID(transactionData.getPrinterHostID())
                            .papluID(diningOptionTree.getData().getPluID())
                            .quantity(diningOptionTree.getData().getQuantity())
                            .isModifier(false)
                            .lineDetail(buildKDSText(diningOptionTree.getData(), (diningOptionTree.getData() != null ? diningOptionTree.getData().getPATransLineItemID() : 0), kdsExpeditorPrinterID, false))
                            .hiddenStation(""));
                }
                else {
                    // check if the KDS expeditor station will pick up a non dining option automatically
                    if (willExpeditorStationPickupNonDiningOption(existingKDSPrintJobDetails, diningOptionTree.getData().getPluID())) {
                        dinOptPJDForExpStn.add(new KPJobDetail()
                                .printerID(kdsExpeditorPrinterID)
                                .printStatusID(PrintStatusType.WAITING)
                                .printerHostID(transactionData.getPrinterHostID())
                                .papluID(diningOptionTree.getData().getPluID())
                                .quantity(diningOptionTree.getData().getQuantity())
                                .isModifier(false)
                                .lineDetail(buildKDSText(diningOptionTree.getData(), (diningOptionTree.getData() != null ? diningOptionTree.getData().getPATransLineItemID() : 0), kdsExpeditorPrinterID, false))
                                .hiddenStation(""));
                    }
                }
            }
            else {
                if (diningOptionTree.getData().getPrintsOnExpeditor()) {
                    // check if a non dining option is being sent to the KDS expeditor station
                    if (isNonDiningOptionBeingSentToStation(existingKDSPrintJobDetails, kdsExpeditorPrinterID)) {
                        dinOptPJDForExpStn.add(new KPJobDetail()
                                .printerID(kdsExpeditorPrinterID)
                                .printStatusID(PrintStatusType.WAITING)
                                .printerHostID(transactionData.getPrinterHostID())
                                .papluID(diningOptionTree.getData().getPluID())
                                .quantity(diningOptionTree.getData().getQuantity())
                                .isModifier(false)
                                .lineDetail(buildKDSText(diningOptionTree.getData(), (diningOptionTree.getData() != null ? diningOptionTree.getData().getPATransLineItemID() : 0), kdsExpeditorPrinterID, false))
                                .hiddenStation(""));
                    }
                    else {
                        // check if the KDS expeditor station will pick up a non dining option automatically
                        if (willExpeditorStationPickupNonDiningOption(existingKDSPrintJobDetails, diningOptionTree.getData().getPluID())) {
                            dinOptPJDForExpStn.add(new KPJobDetail()
                                    .printerID(kdsExpeditorPrinterID)
                                    .printStatusID(PrintStatusType.WAITING)
                                    .printerHostID(transactionData.getPrinterHostID())
                                    .papluID(diningOptionTree.getData().getPluID())
                                    .quantity(diningOptionTree.getData().getQuantity())
                                    .isModifier(false)
                                    .lineDetail(buildKDSText(diningOptionTree.getData(), (diningOptionTree.getData() != null ? diningOptionTree.getData().getPATransLineItemID() : 0), kdsExpeditorPrinterID, false))
                                    .hiddenStation(""));
                        }
                    }
                }
            }
        }
        else {
            if (diningOptionTree.getData().getPrintsOnExpeditor()) {
                // check if a non dining option is being sent to the KDS expeditor station
                if (isNonDiningOptionBeingSentToStation(existingKDSPrintJobDetails, kdsExpeditorPrinterID)) {
                    dinOptPJDForExpStn.add(new KPJobDetail()
                            .printerID(kdsExpeditorPrinterID)
                            .printStatusID(PrintStatusType.WAITING)
                            .printerHostID(transactionData.getPrinterHostID())
                            .papluID(diningOptionTree.getData().getPluID())
                            .quantity(diningOptionTree.getData().getQuantity())
                            .isModifier(false)
                            .lineDetail(buildKDSText(diningOptionTree.getData(), (diningOptionTree.getData() != null ? diningOptionTree.getData().getPATransLineItemID() : 0), kdsExpeditorPrinterID, false))
                            .hiddenStation(""));
                }
                else {
                    // check if the KDS expeditor station will pick up a non dining option automatically
                    if (willExpeditorStationPickupNonDiningOption(existingKDSPrintJobDetails, diningOptionTree.getData().getPluID())) {
                        dinOptPJDForExpStn.add(new KPJobDetail()
                                .printerID(kdsExpeditorPrinterID)
                                .printStatusID(PrintStatusType.WAITING)
                                .printerHostID(transactionData.getPrinterHostID())
                                .papluID(diningOptionTree.getData().getPluID())
                                .quantity(diningOptionTree.getData().getQuantity())
                                .isModifier(false)
                                .lineDetail(buildKDSText(diningOptionTree.getData(), (diningOptionTree.getData() != null ? diningOptionTree.getData().getPATransLineItemID() : 0), kdsExpeditorPrinterID, false))
                                .hiddenStation(""));
                    }
                }
            }
        }

        return dinOptPJDForExpStn;
    }

    /**
     * <p>Creates print job details for the dining option to be sent to KDS remote order station.</p>
     *
     * @param transactionData The {@link TransactionData} instance containing that contains information about the transaction.
     * @param existingKDSPrintJobDetails An {@link ArrayList} of {@link KPJobDetail} corresponding to the KDS print job details.
     * @param diningOptionTree A {@link MMHTree} of {@link Plu} corresponding to the dining option within the transaction.
     * @param isRemoteOrder Whether or not the order was placed remotely.
     * @param kdsRemoteOrderPrinterID The printer ID of the KDS remote order station.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to the print job details for the dining option to be sent to KDS remote order station.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private static ArrayList<KPJobDetail> getDiningOptionPrintJobDetailsForTheKDSRemoteOrderStation (TransactionData transactionData,
                                                                                                     ArrayList<KPJobDetail> existingKDSPrintJobDetails,
                                                                                                     MMHTree<Plu> diningOptionTree,
                                                                                                     boolean isRemoteOrder,
                                                                                                     int kdsRemoteOrderPrinterID) {
        // make sure we have a valid printer host to handle the print job details
        if (transactionData.getPrinterHostID() <= 0) {
            Logger.logMessage(String.format("There isn't a printer host that can process the print jobs created from the transaction with an ID of %s!",
                    Objects.toString(transactionData.getTransactionID(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure there are existing KDS print job details
        if (DataFunctions.isEmptyCollection(existingKDSPrintJobDetails)) {
            Logger.logMessage(String.format("No existing print job details were found in KitchenDisplaySystemJobInfo.getDiningOptionPrintJobDetailsForTheKDSRemoteOrderStation, " +
                    "unable to create any print job details for the dining option %s.",
                    Objects.toString(diningOptionTree.getData().getName(), "N/A")), KDS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // the KDS remote order station print job details to return for the dining option
        ArrayList<KPJobDetail> dinOptPJDForROStn = new ArrayList<>();

        // refer to the flow chart on SVN, https://mmhsource/svn/docs/pos/Development/Specs/KP_AND_KDS/KDS%20Flow%20Diagrams/AddDiningOptionToRemoteOrderStationFlow.jpg

        if ((isRemoteOrder) && (isNonDiningOptionBeingSentToStation(existingKDSPrintJobDetails, kdsRemoteOrderPrinterID))) {
            dinOptPJDForROStn.add(new KPJobDetail()
                    .printerID(kdsRemoteOrderPrinterID)
                    .printStatusID(PrintStatusType.WAITING)
                    .printerHostID(transactionData.getPrinterHostID())
                    .papluID(diningOptionTree.getData().getPluID())
                    .quantity(diningOptionTree.getData().getQuantity())
                    .isModifier(false)
                    .lineDetail(buildKDSText(diningOptionTree.getData(), (diningOptionTree.getData() != null ? diningOptionTree.getData().getPATransLineItemID() : 0), kdsRemoteOrderPrinterID, false))
                    .hiddenStation(""));
        }

        return dinOptPJDForROStn;
    }

    /**
     * <p>Builds the text that will appear on KDS for the given product.</p>
     *
     * @param product The {@link Plu} to generate the text for that will be displayed on KDS.
     * @param paTransLineItemID The PATransLineItemID for the transaction line item.
     * @param isModifier Whether or not the given product is a modifier.
     * @return The text {@link String} that will appear on KDS for the given product.
     */
    private static String buildKDSText (Plu product, int paTransLineItemID, int printerID, boolean isModifier) {

        String indicatorText = (isModifier ? MODIFIER_INDICATOR : PRODUCT_INDICATOR);
        String lineItemIDText = "";
        if (paTransLineItemID > 0) {
            lineItemIDText = String.valueOf(paTransLineItemID) + "__";
        }
        String printerIDText = "";
        if (printerID > 0) {
            printerIDText = String.valueOf(printerID) + "__";
        }
        String productText = product.getName();
        PrepOption prepOption = product.getPrepOption();
        String prepOptionText = "";
        if (prepOption != null) {
            if (((prepOption.isDisplayDefaultKitchen()) && (prepOption.isDefaultOption())) || (!prepOption.isDefaultOption())) {
                if (StringFunctions.stringHasContent(prepOption.getOriginalName())) {
                    prepOptionText = " - "+prepOption.getOriginalName();
                }
            }
        }

        return Base64.encodeBase64String((indicatorText + lineItemIDText + printerIDText + productText + prepOptionText).getBytes());
    }

    /**
     * <p>Iterates through the existing print job details to check whether or not a non dining option is being sent to the KDS station with the given printer ID.</p>
     *
     * @param existingKDSPrintJobDetails An {@link ArrayList} of {@link KPJobDetail} corresponding to the KDS print job details.
     * @param printerID The printer ID of the KDS station to check.
     * @return Whether or not a non dining option is being sent to the KDS station with the given printer ID.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private static boolean isNonDiningOptionBeingSentToStation (ArrayList<KPJobDetail> existingKDSPrintJobDetails, int printerID) {

        for (KPJobDetail printJobDetail : existingKDSPrintJobDetails) {
            if (printerID == printJobDetail.getPrinterID()) {
                return true;
            }
        }

        return false;
    }

    /**
     * <p>Iterates through the existing print job details to check whether or not a non dining option is being sent to a
     * KDS station that will be picked up automatically by the KDS expeditor station.</p>
     *
     * @param existingKDSPrintJobDetails An {@link ArrayList} of {@link KPJobDetail} corresponding to the KDS print job details.
     * @param diningOptionPAPluID The poroduct ID of the dining option.
     * @return Whether or not a non dining option is being sent to a KDS station that will be picked up automatically by the KDS expeditor station.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private static boolean willExpeditorStationPickupNonDiningOption (ArrayList<KPJobDetail> existingKDSPrintJobDetails, int diningOptionPAPluID) {

        for (KPJobDetail printJobDetail : existingKDSPrintJobDetails) {
            if ((printJobDetail.getPAPluID() != diningOptionPAPluID) && (!StringFunctions.stringHasContent(printJobDetail.getHideStation()))) {
                return true;
            }
        }

        return false;
    }

    /**
     * <p>Determines at which index the KDS print job detail for the dining option should be inserted so that it is the first item to appear on the station.</p>
     *
     * @param existingKDSPrintJobDetails An {@link ArrayList} of {@link KPJobDetail} corresponding to the KDS print job details.
     * @param pjdForDiningOpt The {@link KPJobDetail} for the dining option on to be sent to KDS.
     * @return The index the KDS print job detail for the dining option should be inserted so that it is the first item to appear on the station.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private static int getKDSPrintJobDetailInsertionIndex (ArrayList<KPJobDetail> existingKDSPrintJobDetails, KPJobDetail pjdForDiningOpt) {

        if (DataFunctions.isEmptyCollection(existingKDSPrintJobDetails)) {
            return -1;
        }

        for (int index = 0; index < existingKDSPrintJobDetails.size(); index++) {
            if (existingKDSPrintJobDetails.get(index).getPrinterID() == pjdForDiningOpt.getPrinterID()) {
                return index;
            }
        }

        return -1;
    }

    /**
     * <p>Utility method to log the KDS print job details sorted by their destined printer ID.</p>
     *
     * @param kdsPrintJobDetails An {@link ArrayList} of {@link KPJobDetail} corresponding to the KDS print job details.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private static void logKDSPrintJobDetailsByPrinterID (ArrayList<KPJobDetail> kdsPrintJobDetails) {

        // sort the KDS print job details by printer ID
        HashMap<Integer, ArrayList<KPJobDetail>> sortedDetails = new HashMap<>();
        ArrayList<KPJobDetail> commonKDSStationDetails;
        for (KPJobDetail kdsPrintJobDetail : kdsPrintJobDetails) {
            if (sortedDetails.containsKey(kdsPrintJobDetail.getPrinterID())) {
                commonKDSStationDetails = sortedDetails.get(kdsPrintJobDetail.getPrinterID());
            }
            else {
                commonKDSStationDetails = new ArrayList<>();
            }
            commonKDSStationDetails.add(kdsPrintJobDetail);
            sortedDetails.put(kdsPrintJobDetail.getPrinterID(), commonKDSStationDetails);
        }

        // do the logging
        Logger.logMessage("NOW LOGGING THE KDS PRINT JOB DETAILS!", KDS_LOG, Logger.LEVEL.TRACE);
        for (Map.Entry<Integer, ArrayList<KPJobDetail>> entry : sortedDetails.entrySet()) {
            Logger.logMessage(String.format("FOR PRINTER ID: %s", Objects.toString(entry.getKey(), "N/A")), KDS_LOG, Logger.LEVEL.TRACE);
            ArrayList<KPJobDetail> details = entry.getValue();
            if (!DataFunctions.isEmptyCollection(details)) {
                for (KPJobDetail kdsPrintJobDetail : details) {
                    Logger.logMessage("--" + kdsPrintJobDetail.toStringCompact(), KDS_LOG, Logger.LEVEL.TRACE);
                }
            }
        }

    }

}