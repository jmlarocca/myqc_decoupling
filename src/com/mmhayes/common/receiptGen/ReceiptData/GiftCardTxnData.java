package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-15 14:50:29 -0400 (Fri, 15 May 2020) $: Date of last commit
    $Rev: 11756 $: Revision of last commit
    Notes: Contains information regarding a gift card used in the transaction.
*/

import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Contains information regarding a gift card used in the transaction.</p>
 *
 */
public class GiftCardTxnData {

    // private member variables of a GiftCardTxnData
    private String gcTxnDataStr = "";
    private String delimiter = "";
    private HashMap<String, String> gcDetails = new HashMap<>();

    /**
     * <p>Constructor for a {@link GiftCardTxnData}.</p>
     *
     * @param gcTxnDataStr The gift card transaction data as a {@link String}.
     * @param delimiter The delimiter {@link String} on which to split the gift card transaction data {@link String} to get the gift card details.
     */
    public GiftCardTxnData (String gcTxnDataStr, String delimiter) {
        this.gcTxnDataStr = gcTxnDataStr;
        this.delimiter = delimiter;
        initDetails();
    }

    /**
     * <p>Sets the gift card details for a {@link GiftCardTxnData}.</p>
     *
     */
    @SuppressWarnings("Duplicates")
    public void initDetails () {

        try {
            if (StringFunctions.stringHasContent(gcTxnDataStr)) {
                String[] details = gcTxnDataStr.split(delimiter, -1);
                if (!DataFunctions.isEmptyGenericArr(details)) {
                    for (String detail : details) {
                        if (StringFunctions.stringHasContent(detail)) {
                            String[] splitDetail = detail.split("\\s*=\\s*", -1);
                            if ((!DataFunctions.isEmptyGenericArr(splitDetail)) && (splitDetail.length >= 2)) {
                                gcDetails.put(splitDetail[0], splitDetail[1]);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to get the gift card details from the String %s using a delimiter of %s in GiftCardTxnData.initDetails",
                    Objects.toString(gcTxnDataStr, "N/A"),
                    Objects.toString(delimiter, "N/A")), Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Utility method to get the gift card detail value {@link String} at the given key {@link String}.</p>
     *
     * @param key The key {@link String} at which to get the detail from the gift card detail {@link HashMap}.
     * @return The gift card detail value {@link String} at the given key {@link String}.
     */
    public String getDetailValue (String key) {
        String value = "";

        try {
            if ((StringFunctions.stringHasContent(key)) && (!DataFunctions.isEmptyMap(gcDetails)) && (gcDetails.containsKey(key))) {
                value = HashMapDataFns.getStringVal(gcDetails, key);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("Unable to get the detail at the key of %s from the gift card details in GiftCardTxnData.getDetailValue",
                    Objects.toString(key, "N/A")), Logger.LEVEL.ERROR);
        }

        return value;
    }
    
    /**
     * <p>Overridden toString() method for a {@link GiftCardTxnData}.</p>
     *
     * @return A {@link String} representation of this {@link GiftCardTxnData}.
     */
    @Override
    public String toString () {

        return String.format("GCTXNDATASTR: %s, DELIMITER: %s, GCDETAILS: %s",
                Objects.toString((StringFunctions.stringHasContent(gcTxnDataStr) ? gcTxnDataStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(delimiter) ? delimiter : "N/A"), "N/A"),
                Objects.toString((!DataFunctions.isEmptyMap(gcDetails) ? Collections.singletonList(gcDetails).toString() : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link GiftCardTxnData}.
     * Two {@link GiftCardTxnData} are defined as being equal if they have the same gcTxnDataStr.</p>
     *
     * @param obj The {@link Object} to compare against this {@link GiftCardTxnData}.
     * @return Whether or not the {@link Object} is equal to this {@link GiftCardTxnData}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a GiftCardTxnData and check if the obj's gcTxnDataStr is equal to this GiftCardTxnData's gcTxnDataStr
        GiftCardTxnData giftCardTxnData = ((GiftCardTxnData) obj);
        return Objects.equals(giftCardTxnData.gcTxnDataStr, gcTxnDataStr);
    }

    /**
     * <p>Overridden hashCode() method for a {@link GiftCardTxnData}.</p>
     *
     * @return The unique hash code for this {@link GiftCardTxnData}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(gcTxnDataStr);
    }

}
