package com.mmhayes.common.receiptGen.ReceiptData;

import com.mmhayes.common.utils.Logger;

import java.util.ArrayList;

/**
 * Created by nyu on 12/2/2019.
 */
public class SimplifyReceiptData implements IPaymentProcessorReceiptData
{
    private final static String logFileName = "PaymentProcessorRcpt.log";
    private ArrayList<String> receiptHeaderLines = new ArrayList<String>();
    private ArrayList<String> receiptFooterLines = new ArrayList<String>();
    private String date = "";
    private String time = "";
    private String cashierName = "";
    private String terminalID = "";
    private String transTypeName = "";
    private String totalAmount = "";
    private String cardholderName = "";
    private String maskedAcctNum = "";
    private String refNum = "";
    private String merchantNum = "";
    private String cardEntryMode = "";
    private String sysTraceAuditNum = "";
    private String emvRcptDetails = "";

    public SimplifyReceiptData()
    {
    }

    public void setReceiptInfo(ArrayList rcptInfo)
    {
        if (rcptInfo.size() == 0)
        {
            Logger.logMessage("Simplify Receipt Data - receipt info is empty!", logFileName, Logger.LEVEL.TRACE);
            return;
        }

        receiptHeaderLines = (ArrayList)rcptInfo.get(0);
        date = rcptInfo.get(1).toString();
        time = rcptInfo.get(2).toString();
        cashierName = rcptInfo.get(3).toString();
        terminalID = rcptInfo.get(4).toString();
        transTypeName = rcptInfo.get(5).toString();
        totalAmount = rcptInfo.get(6).toString();
        cardholderName = rcptInfo.get(7).toString();
        maskedAcctNum = rcptInfo.get(8).toString();
        refNum = rcptInfo.get(9).toString();
        merchantNum = rcptInfo.get(10).toString();
        cardEntryMode = rcptInfo.get(11).toString();
        sysTraceAuditNum = rcptInfo.get(12).toString();
        emvRcptDetails = rcptInfo.get(13).toString();
        receiptFooterLines = (ArrayList)rcptInfo.get(14);
    }

    public ArrayList<String> getReceiptHeaderLines()
    {
        return receiptHeaderLines;
    }

    public void setReceiptHeaderLines(ArrayList<String> receiptHeaderLines)
    {
        this.receiptHeaderLines = receiptHeaderLines;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public String getCashierName()
    {
        return cashierName;
    }

    public void setCashierName(String cashierName)
    {
        this.cashierName = cashierName;
    }

    public String getTerminalID()
    {
        return terminalID;
    }

    public void setTerminalID(String terminalID)
    {
        this.terminalID = terminalID;
    }

    public String getTransTypeName()
    {
        return transTypeName;
    }

    public void setTransTypeName(String transTypeName)
    {
        this.transTypeName = transTypeName;
    }

    public String getTotalAmount()
    {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount)
    {
        this.totalAmount = totalAmount;
    }

    public String getCardholderName()
    {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName)
    {
        this.cardholderName = cardholderName;
    }

    public String getMaskedAcctNum()
    {
        return maskedAcctNum;
    }

    public void setMaskedAcctNum(String maskedAcctNum)
    {
        this.maskedAcctNum = maskedAcctNum;
    }

    public String getRefNum()
    {
        return refNum;
    }

    public void setRefNum(String refNum)
    {
        this.refNum = refNum;
    }

    public String getMerchantNum()
    {
        return merchantNum;
    }

    public void setMerchantNum(String merchantNum)
    {
        this.merchantNum = merchantNum;
    }

    public String getCardEntryMode()
    {
        return cardEntryMode;
    }

    public void setCardEntryMode(String cardEntryMode)
    {
        this.cardEntryMode = cardEntryMode;
    }

    public String getSysTraceAuditNum()
    {
        return sysTraceAuditNum;
    }

    public void setSysTraceAuditNum(String sysTraceAuditNum)
    {
        this.sysTraceAuditNum = sysTraceAuditNum;
    }

    public String getEmvRcptDetails()
    {
        return emvRcptDetails;
    }

    public void setEmvRcptDetails(String emvRcptDetails)
    {
        this.emvRcptDetails = emvRcptDetails;
    }

    public ArrayList<String> getReceiptFooterLines()
    {
        return receiptFooterLines;
    }

    public void setReceiptFooterLines(ArrayList<String> receiptFooterLines)
    {
        this.receiptFooterLines = receiptFooterLines;
    }
}
