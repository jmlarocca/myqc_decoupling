package com.mmhayes.common.receiptGen.ReceiptData;

import com.mmhayes.common.receiptGen.TypeData;

import java.util.ArrayList;

/**
 * Created by nyu on 8/3/2017.
 */
public class MerchantLinkReportData implements IReportData
{
    private int reportType;
    private ArrayList<String> reportDetails = new ArrayList<String>();
    
    public MerchantLinkReportData()
    {
    }

    public ArrayList<String> getReportDetails()
    {
        return reportDetails;
    }

    public void setReportDetails(ArrayList<String> reportDetails)
    {
        this.reportDetails = reportDetails;
    }

    public int getReportType()
    {
        return reportType;
    }

    public boolean setReportType(int reportType)
    {
        boolean isValidReportType = true;

        // Make sure this is a valid report type
        if (reportType == TypeData.MerchantLinkReport.EMV_CONFIG_RPT)
        {
            this.reportType = TypeData.MerchantLinkReport.EMV_CONFIG_RPT;
        }
        else if (reportType == TypeData.MerchantLinkReport.CHIP_TXN_RPT)
        {
            this.reportType = TypeData.MerchantLinkReport.CHIP_TXN_RPT;
        }
        else
        {
            isValidReportType = false;
        }

        return isValidReportType;
    }

    public String getEmailSubjectLine()
    {
        String subjectLine = "";

        switch (reportType)
        {
            case TypeData.MerchantLinkReport.EMV_CONFIG_RPT:
                subjectLine = "Merchant Link EMV Configuration Report";
                break;
            case TypeData.MerchantLinkReport.CHIP_TXN_RPT:
                subjectLine = "Merchant Link Chip Transaction Report";
                break;
            default:
                subjectLine = "Merchant Link Report";
                break;
        }

        return subjectLine;
    }
}
