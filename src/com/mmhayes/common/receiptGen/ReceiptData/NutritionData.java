package com.mmhayes.common.receiptGen.ReceiptData;

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.ITransLineItem;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.Product;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TransactionData.Plu;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;
import redstone.xmlrpc.XmlRpcArray;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by nyu on 1/9/2017.
 */
public class NutritionData
{
    private final static String COMMON_LOG_FILE = "ConsolidatedReceipt.log";

    private boolean needsNAFootnote = false;
    private boolean needsTruncateDigitsFootnote = false;
    private ArrayList nutritionTotals = new ArrayList();
    private ArrayList<NutritionTotalConfig> nutritionTotalsConfig = new ArrayList<NutritionTotalConfig>();
    private ArrayList<NutritionCategory> nutritionCategories = new ArrayList<NutritionCategory>();
    private ArrayList nutritionItemsFlex = new ArrayList();         // Keeping this so there's backwards compatibility
    private ArrayList<NutritionItem> nutritionItemsDB = new ArrayList<NutritionItem>();

    protected commonMMHFunctions commDM = new commonMMHFunctions();

    public NutritionData ()
    {
    }

    /**
     * <p>Builds a {@link NutritionData} object from the given {@link TransactionData}.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param transactionData The {@link TransactionData} to use to build the {@link NutritionData}.
     * @return A {@link NutritionData} object built from the given {@link TransactionData}.
     */
    @SuppressWarnings("unchecked")
    public static NutritionData buildNutritionDataV2 (DataManager dataManager, TransactionData transactionData) {
        NutritionData nutritionData = null;

        try {
            // make sure we have a valid DataManager
            if (dataManager == null) {
                Logger.logMessage("The DataManager passed to NutritionData.buildNutritionDataV2 can't be null, unable to build a NutritionData object!", Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have valid transaction data
            if (transactionData == null) {
                Logger.logMessage("The TransactionData passed to NutritionData.buildNutritionDataV2 can't be null, unable to build a NutritionData object!", Logger.LEVEL.ERROR);
                return null;
            }

            // get all nutrition data, including categories and total configurations
            ArrayList<HashMap> allNutritionData = DataFunctions.purgeAlOfHm(dataManager.parameterizedExecuteQuery("data.ReceiptGen.GetAllNutritionInfo", new Object[]{}, true));
            if (!DataFunctions.isEmptyCollection(allNutritionData)) {
                nutritionData = new NutritionData();
                for (HashMap hm : allNutritionData) {
                    // create nutrition categories and total configurations
                    if (HashMapDataFns.getIntVal(hm, "PANUTRITIONCATEGORYID") > 0) {
                        // create a nutrition category
                        String name =  HashMapDataFns.getStringVal(hm, "NAME");
                        String shortName =  HashMapDataFns.getStringVal(hm, "SHORTNAME");
                        String measurementLabel = HashMapDataFns.getStringVal(hm, "MEASUREMENTLBL");
                        NutritionCategory nutritionCategory = new NutritionCategory(name, shortName, measurementLabel);
                        nutritionData.addNutritionCategory(nutritionCategory);
                    }
                    else if (HashMapDataFns.getIntVal(hm, "PANUTRITIONTOTALSID") > 0) {
                        // create a nutrition total configuration
                        int paNutritionTotalsID = HashMapDataFns.getIntVal(hm, "PANUTRITIONTOTALSID");
                        String dvCalorieValLbl = HashMapDataFns.getStringVal(hm, "DVCALORIEVALLBL");
                        String nutritionInfo1TotalStr = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO1TOTAL");
                        BigDecimal nutritionInfo1Total = BigDecimal.ZERO;
                        BigDecimal nutritionInfo2Total = BigDecimal.ZERO;
                        BigDecimal nutritionInfo3Total = BigDecimal.ZERO;
                        BigDecimal nutritionInfo4Total = BigDecimal.ZERO;
                        BigDecimal nutritionInfo5Total = BigDecimal.ZERO;
                        if ((StringFunctions.stringHasContent(nutritionInfo1TotalStr)) && NumberUtils.isNumber(nutritionInfo1TotalStr)) {
                            nutritionInfo1Total = new BigDecimal(nutritionInfo1TotalStr);
                        }
                        String nutritionInfo2TotalStr = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO2TOTAL");
                        if ((StringFunctions.stringHasContent(nutritionInfo2TotalStr)) && NumberUtils.isNumber(nutritionInfo2TotalStr)) {
                            nutritionInfo2Total = new BigDecimal(nutritionInfo2TotalStr);
                        }
                        String nutritionInfo3TotalStr = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO3TOTAL");
                        if ((StringFunctions.stringHasContent(nutritionInfo3TotalStr)) && NumberUtils.isNumber(nutritionInfo3TotalStr)) {
                            nutritionInfo3Total = new BigDecimal(nutritionInfo3TotalStr);
                        }
                        String nutritionInfo4TotalStr = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO4TOTAL");
                        if ((StringFunctions.stringHasContent(nutritionInfo4TotalStr)) && NumberUtils.isNumber(nutritionInfo4TotalStr)) {
                            nutritionInfo4Total = new BigDecimal(nutritionInfo4TotalStr);
                        }
                        String nutritionInfo5TotalStr = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO5TOTAL");
                        if ((StringFunctions.stringHasContent(nutritionInfo5TotalStr)) && NumberUtils.isNumber(nutritionInfo5TotalStr)) {
                            nutritionInfo5Total = new BigDecimal(nutritionInfo5TotalStr);
                        }

                        NutritionTotalConfig nutritionTotalConfig =
                                new NutritionTotalConfig(paNutritionTotalsID, dvCalorieValLbl, nutritionInfo1Total, nutritionInfo2Total, nutritionInfo3Total, nutritionInfo4Total, nutritionInfo5Total);
                        nutritionData.addNutritionTotalConfiguration(nutritionTotalConfig);
                    }
                }
                calculateNutritionTotals(dataManager, transactionData, nutritionData);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to build the NutritionData from the given TransactionData in NutritionData.buildNutritionDataV2", Logger.LEVEL.ERROR);
        }

        return nutritionData;
    }
    
    public NutritionData (ArrayList nutritionDataArgs)
    {
        // Set needsTruncateDigitsFootnote
        if (nutritionDataArgs.size() >= 5)
        {
            needsTruncateDigitsFootnote = new Boolean(nutritionDataArgs.get(4).toString()).booleanValue();
        }

        // Set needsNAFootnote
        if (nutritionDataArgs.size() >= 4)
        {
            needsNAFootnote = new Boolean(nutritionDataArgs.get(3).toString()).booleanValue();
        }

        // Set nutrition totals
        if (nutritionDataArgs.size() >= 3)
        {
            nutritionTotals = (ArrayList) nutritionDataArgs.get(2);
        }

        // Set nutrition categories
        if (nutritionDataArgs.size() >= 2)
        {
            ArrayList nutriCategories = (ArrayList) nutritionDataArgs.get(1);

            for (int x = 0; x < nutriCategories.size(); x++)
            {
                ArrayList nutriCategory = (ArrayList) nutriCategories.get(x);

                if (nutriCategory.size() >= 2)
                {
                    String shortName = nutriCategory.get(0).toString();
                    String measurementLbl = nutriCategory.get(1).toString();

                    NutritionCategory category = new NutritionCategory(shortName, shortName, measurementLbl);

                    nutritionCategories.add(category);
                }
            }
        }

        // Set list of products
        if (nutritionDataArgs.size() >= 1)
        {
            nutritionItemsFlex = (ArrayList) nutritionDataArgs.get(0);
        }
    }

    //--------------------------------------------------------------------------
    //  Building Nutrition Info from DB (for popup and MyQC receipts)
    //--------------------------------------------------------------------------
    /**
     * Calculates the nutrition totals for products in the {@link TransactionData} for the given {@link NutritionData} instance.
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param transactionData The {@link TransactionData} to use to calculate the nutrition totals.
     * @param nutritionData The {@link NutritionData} instance to calculate the totals for.
     */
    @SuppressWarnings("Convert2streamapi")
    private static void calculateNutritionTotals (DataManager dataManager, TransactionData transactionData, NutritionData nutritionData) {

        try {
            // make sure we have a valid DataManager
            if (dataManager == null) {
                Logger.logMessage("The DataManager passed to NutritionData.calculateNutritionTotals can't be null, unable to calculate the nutrition totals!", Logger.LEVEL.ERROR);
                return;
            }

            // make sure we have valid transaction data
            if (transactionData == null) {
                Logger.logMessage("The TransactionData passed to NutritionData.calculateNutritionTotals can't be null, unable to calculate the nutrition totals!", Logger.LEVEL.ERROR);
                return;
            }

            // make sure we have valid line items within the transaction
            if (DataFunctions.isEmptyCollection(transactionData.getTxnLineItems())) {
                Logger.logMessage("No line items found within the TransactionData passed to NutritionData.calculateNutritionTotals, unable to calculate the nutrition totals!", Logger.LEVEL.ERROR);
                return;
            }

            // make sure we have a valid NutritionData instance
            if (nutritionData == null) {
                Logger.logMessage("The NutritionData instance passed to NutritionData.calculateNutritionTotals can't be null, unable to calculate the nutrition totals!", Logger.LEVEL.ERROR);
                return;
            }

            BigDecimal nutritionInfo1Total = BigDecimal.ZERO;
            BigDecimal nutritionInfo2Total = BigDecimal.ZERO;
            BigDecimal nutritionInfo3Total = BigDecimal.ZERO;
            BigDecimal nutritionInfo4Total = BigDecimal.ZERO;
            BigDecimal nutritionInfo5Total = BigDecimal.ZERO;

            // get the products within the transaction
            for (ITransLineItem transLineItem : transactionData.getTxnLineItems()) {
                if ((transLineItem != null) && (transLineItem instanceof Product)) {
                    Product product = ((Product) transLineItem);
                    // create a NutritionItem
                    NutritionItem nutritionItem = new NutritionItem();
                    nutritionItem.setPluID(product.getProductID());
                    nutritionItem.setProductName(product.getName());
                    nutritionItem.setQuantity(String.valueOf(product.getQuantityAsInt()));
                    HashMap<String, String> productNutritionInfo = product.getProductNutritionInfo();
                    if (!DataFunctions.isEmptyMap(productNutritionInfo)) {
                        String nutritionInfo1 = HashMapDataFns.getStringVal(productNutritionInfo, "NUTRITIONINFO1");
                        String nutritionInfo2 = HashMapDataFns.getStringVal(productNutritionInfo, "NUTRITIONINFO2");
                        String nutritionInfo3 = HashMapDataFns.getStringVal(productNutritionInfo, "NUTRITIONINFO3");
                        String nutritionInfo4 = HashMapDataFns.getStringVal(productNutritionInfo, "NUTRITIONINFO4");
                        String nutritionInfo5 = HashMapDataFns.getStringVal(productNutritionInfo, "NUTRITIONINFO5");

                        // if no value is set use ***, if the nutritional value is greater than 9999 use ###
                        // CATEGORY 1
                        if (!StringFunctions.stringHasContent(nutritionInfo1)) {
                            nutritionItem.setNutritionInfo1(nutritionInfo1);
                            nutritionInfo1 = "***";
                        }
                        else {
                            BigDecimal productNutritionInfo1Total = nutritionItem.calculateNutritionTotal(nutritionInfo1);
                            nutritionItem.setNutritionInfo1(productNutritionInfo1Total.toPlainString());
                            nutritionInfo1Total = nutritionInfo1Total.add(productNutritionInfo1Total);
                        }
                        // CATEGORY 2
                        if (!StringFunctions.stringHasContent(nutritionInfo2)) {
                            nutritionItem.setNutritionInfo2(nutritionInfo2);
                            nutritionInfo2 = "***";
                        }
                        else {
                            BigDecimal productNutritionInfo2Total = nutritionItem.calculateNutritionTotal(nutritionInfo2);
                            nutritionItem.setNutritionInfo2(productNutritionInfo2Total.toPlainString());
                            nutritionInfo2Total = nutritionInfo2Total.add(productNutritionInfo2Total);
                        }
                        // CATEGORY 3
                        if (!StringFunctions.stringHasContent(nutritionInfo3)) {
                            nutritionItem.setNutritionInfo3(nutritionInfo3);
                            nutritionInfo3 = "***";
                        }
                        else {
                            BigDecimal productNutritionInfo3Total = nutritionItem.calculateNutritionTotal(nutritionInfo3);
                            nutritionItem.setNutritionInfo3(productNutritionInfo3Total.toPlainString());
                            nutritionInfo3Total = nutritionInfo3Total.add(productNutritionInfo3Total);
                        }
                        // CATEGORY 4
                        if (!StringFunctions.stringHasContent(nutritionInfo4)) {
                            nutritionItem.setNutritionInfo4(nutritionInfo4);
                            nutritionInfo4 = "***";
                        }
                        else {
                            BigDecimal productNutritionInfo4Total = nutritionItem.calculateNutritionTotal(nutritionInfo4);
                            nutritionItem.setNutritionInfo4(productNutritionInfo4Total.toPlainString());
                            nutritionInfo4Total = nutritionInfo4Total.add(productNutritionInfo4Total);
                        }
                        // CATEGORY 5
                        if (!StringFunctions.stringHasContent(nutritionInfo5)) {
                            nutritionItem.setNutritionInfo5(nutritionInfo5);
                            nutritionInfo5 = "***";
                        }
                        else {
                            BigDecimal productNutritionInfo5Total = nutritionItem.calculateNutritionTotal(nutritionInfo5);
                            nutritionItem.setNutritionInfo5(productNutritionInfo5Total.toPlainString());
                            nutritionInfo5Total = nutritionInfo5Total.add(productNutritionInfo5Total);
                        }

                        if ((nutritionInfo1.equalsIgnoreCase("***"))
                                || (nutritionInfo2.equalsIgnoreCase("***"))
                                || (nutritionInfo3.equalsIgnoreCase("***"))
                                || (nutritionInfo4.equalsIgnoreCase("***"))
                                || (nutritionInfo5.equalsIgnoreCase("***"))) {
                            // add the not available footnote
                            nutritionData.addNAFootnote();
                        }
                    }
                    nutritionData.addNutiritionItemToNutritionItemsDB(nutritionItem);
                }
            }

            // clean up and set the nutrition totals
            ArrayList<String> nutritionTotals = new ArrayList<>();
            nutritionTotals.add(cleanUpNutritionTotalV2(nutritionInfo1Total, nutritionData));
            nutritionTotals.add(cleanUpNutritionTotalV2(nutritionInfo2Total, nutritionData));
            nutritionTotals.add(cleanUpNutritionTotalV2(nutritionInfo3Total, nutritionData));
            nutritionTotals.add(cleanUpNutritionTotalV2(nutritionInfo4Total, nutritionData));
            nutritionTotals.add(cleanUpNutritionTotalV2(nutritionInfo5Total, nutritionData));
            nutritionData.addNutritionTotals(nutritionTotals);

            // calculate the daily value percentages
            calculateDailyValuePercentagesV2(nutritionData, nutritionInfo1Total, nutritionInfo2Total, nutritionInfo3Total, nutritionInfo4Total, nutritionInfo5Total);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to calculate the nutrition totals in NutritionData.calculateNutritionTotals", Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Cleans up the given nutrition info total.</p>
     *
     * @param nutritionTotal The {@link BigDecimal} nutrition total to clean up.
     * @param nutritionData The {@link NutritionData} instance to possibly add a truncate digits foot note to.
     * @return The cleaned up nutrition total as a {@link String}.
     */
    private static String cleanUpNutritionTotalV2 (BigDecimal nutritionTotal, NutritionData nutritionData) {
        String nutritionTotalStr;
        BigDecimal maxValLimit = new BigDecimal("9999");

        if (nutritionTotal.compareTo(maxValLimit) == 1) {
            nutritionTotalStr = "###";
            nutritionData.addTruncateDigitsFootnote();
        }
        else {
            nutritionTotalStr = nutritionTotal.setScale(0, RoundingMode.HALF_UP).toPlainString();
        }

        return nutritionTotalStr;
    }

    /**
     * <p>Calculates the daily value percentages based on the products in the transaction.</p>
     *
     * @param nutritionData The {@link NutritionData} instance the calculated daily value percentages should apply to.
     * @param nutritionInfo1Total The {@link BigDecimal} nutrtrion information 1 total for products within the transaction.
     * @param nutritionInfo2Total The {@link BigDecimal} nutrtrion information 2 total for products within the transaction.
     * @param nutritionInfo3Total The {@link BigDecimal} nutrtrion information 3 total for products within the transaction.
     * @param nutritionInfo4Total The {@link BigDecimal} nutrtrion information 4 total for products within the transaction.
     * @param nutritionInfo5Total The {@link BigDecimal} nutrtrion information 5 total for products within the transaction.
     */
    private static void calculateDailyValuePercentagesV2 (NutritionData nutritionData,
                                                          BigDecimal nutritionInfo1Total,
                                                          BigDecimal nutritionInfo2Total,
                                                          BigDecimal nutritionInfo3Total,
                                                          BigDecimal nutritionInfo4Total,
                                                          BigDecimal nutritionInfo5Total) {
        try {
            ArrayList<String> dailyValueTotals;
            ArrayList<NutritionTotalConfig> nutritionTotalConfigs = nutritionData.getNutritionTotalsConfig();
            if (!DataFunctions.isEmptyCollection(nutritionTotalConfigs)) {
                for (NutritionTotalConfig nutritionTotalConfig : nutritionTotalConfigs) {
                    dailyValueTotals = new ArrayList<>();
                    String dailyValueNutritionInfo1 = "***";
                    String dailyValueNutritionInfo2 = "***";
                    String dailyValueNutritionInfo3 = "***";
                    String dailyValueNutritionInfo4 = "***";
                    String dailyValueNutritionInfo5 = "***";

                    // add the daily value calorie label
                    dailyValueTotals.add(nutritionTotalConfig.getDvCalorieValLbl());

                    // CATEGORY 1
                    if (nutritionTotalConfig.getNutritionInfo1Total() != null) {
                        BigDecimal nutritionMaxValue = nutritionTotalConfig.getNutritionInfo1Total();
                        BigDecimal category1DailyValuesTotal = nutritionInfo1Total.divide(nutritionMaxValue, 4, RoundingMode.HALF_UP);
                        category1DailyValuesTotal = category1DailyValuesTotal.multiply(new BigDecimal(100));
                        dailyValueNutritionInfo1 = determineDailyValuePercentageV2(nutritionData, category1DailyValuesTotal);
                    }
                    else {
                        nutritionData.addNAFootnote();
                    }
                    dailyValueTotals.add(dailyValueNutritionInfo1);
                    // CATEGORY 2
                    if (nutritionTotalConfig.getNutritionInfo2Total() != null) {
                        BigDecimal nutritionMaxValue = nutritionTotalConfig.getNutritionInfo2Total();
                        BigDecimal category2DailyValuesTotal = nutritionInfo2Total.divide(nutritionMaxValue, 4, RoundingMode.HALF_UP);
                        category2DailyValuesTotal = category2DailyValuesTotal.multiply(new BigDecimal(200));
                        dailyValueNutritionInfo2 = determineDailyValuePercentageV2(nutritionData, category2DailyValuesTotal);
                    }
                    else {
                        nutritionData.addNAFootnote();
                    }
                    dailyValueTotals.add(dailyValueNutritionInfo2);
                    // CATEGORY 3
                    if (nutritionTotalConfig.getNutritionInfo3Total() != null) {
                        BigDecimal nutritionMaxValue = nutritionTotalConfig.getNutritionInfo3Total();
                        BigDecimal category3DailyValuesTotal = nutritionInfo3Total.divide(nutritionMaxValue, 4, RoundingMode.HALF_UP);
                        category3DailyValuesTotal = category3DailyValuesTotal.multiply(new BigDecimal(300));
                        dailyValueNutritionInfo3 = determineDailyValuePercentageV2(nutritionData, category3DailyValuesTotal);
                    }
                    else {
                        nutritionData.addNAFootnote();
                    }
                    dailyValueTotals.add(dailyValueNutritionInfo3);
                    // CATEGORY 4
                    if (nutritionTotalConfig.getNutritionInfo4Total() != null) {
                        BigDecimal nutritionMaxValue = nutritionTotalConfig.getNutritionInfo4Total();
                        BigDecimal category4DailyValuesTotal = nutritionInfo4Total.divide(nutritionMaxValue, 4, RoundingMode.HALF_UP);
                        category4DailyValuesTotal = category4DailyValuesTotal.multiply(new BigDecimal(400));
                        dailyValueNutritionInfo4 = determineDailyValuePercentageV2(nutritionData, category4DailyValuesTotal);
                    }
                    else {
                        nutritionData.addNAFootnote();
                    }
                    dailyValueTotals.add(dailyValueNutritionInfo4);
                    // CATEGORY 5
                    if (nutritionTotalConfig.getNutritionInfo5Total() != null) {
                        BigDecimal nutritionMaxValue = nutritionTotalConfig.getNutritionInfo5Total();
                        BigDecimal category5DailyValuesTotal = nutritionInfo5Total.divide(nutritionMaxValue, 4, RoundingMode.HALF_UP);
                        category5DailyValuesTotal = category5DailyValuesTotal.multiply(new BigDecimal(500));
                        dailyValueNutritionInfo5 = determineDailyValuePercentageV2(nutritionData, category5DailyValuesTotal);
                    }
                    else {
                        nutritionData.addNAFootnote();
                    }
                    dailyValueTotals.add(dailyValueNutritionInfo5);

                    nutritionData.addNutritionTotals(dailyValueTotals);
                }
            }
            else {
                Logger.logMessage("No nutrition total configurations found in NutritionData..calculateDailyValuePercentagesV2, unable to calculate the daily value percentages!", Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to calculate the daily value percentages in NutritionData.calculateDailyValuePercentagesV2", Logger.LEVEL.ERROR);
        }
    }

    /**
     * <p>Determines how to display the daily value percentage on the receipt.</p>
     *
     * @param nutritionData The {@link NutritionData} to add any truncate digits footnote to.
     * @param dailyValueTotalX The daily value percentage as a {@link BigDecimal} to add to the receipt.
     * @return The daily value percentage as a {@link String}.
     */
    private static String determineDailyValuePercentageV2 (NutritionData nutritionData, BigDecimal dailyValueTotalX) {
        String dailyValueTotal;
        BigDecimal threeDigitMax = new BigDecimal("999");
        BigDecimal fourDigitMax = new BigDecimal("9999");

        // if total is less than 999 print it with a percentage (%)
        if (dailyValueTotalX.compareTo(threeDigitMax) <= 0) {
            dailyValueTotal = dailyValueTotalX.setScale(0, RoundingMode.HALF_UP).toPlainString()+"%";
        }
        // if total is greater than 999 but less than 9999 print it with without a percentage
        else if ((dailyValueTotalX.compareTo(threeDigitMax) == 1) && (dailyValueTotalX.compareTo(fourDigitMax) <= 0)) {
            dailyValueTotal = dailyValueTotalX.setScale(0, RoundingMode.HALF_UP).toPlainString();
        }
        // if total is greater than 9999 add the truncate digits footnote
        else {
            dailyValueTotal = "###";
            nutritionData.addTruncateDigitsFootnote();
        }

        return dailyValueTotal;
    }

    public void buildNutritionData(TransactionData txnData, DataManager dm)
    {
        try
        {
            getNutriCategoriesFromDB(dm);
            getNutriTotalConfigFromDB(dm);
            calculateNutriTotals(txnData, dm);
        }
        catch (Exception e)
        {
            ReceiptGen.logError("NutritionData.buildNutritionData error", e);
        }
    }

    private void getNutriCategoriesFromDB(DataManager dm)
    {
        try
        {
            ArrayList<HashMap> dbNutriCategories = dm.parameterizedExecuteQuery("data.ReceiptGen.GetNutritonCategories", new Object[]{}, COMMON_LOG_FILE, true);

            for (HashMap hm : dbNutriCategories)
            {
                String shortName = HashMapDataFns.getStringVal(hm, "SHORTNAME");
                String measurementLbl = HashMapDataFns.getStringVal(hm, "MEASUREMENTLBL");
                String name = HashMapDataFns.getStringVal(hm, "NAME");

                NutritionCategory nutriCat = new NutritionCategory(name, shortName, measurementLbl);
                nutritionCategories.add(nutriCat);
            }
        }
        catch (Exception e)
        {
            ReceiptGen.logError("NutritionData.getNutriCategoriesFromDB error", e);
        }
    }

    private void getNutriTotalConfigFromDB(DataManager dm)
    {
        try
        {
            ArrayList<HashMap> dbNutriTotals = dm.parameterizedExecuteQuery("data.ReceiptGen.GetNutritionTotals", new Object[]{}, COMMON_LOG_FILE, true);

            for (HashMap hm : dbNutriTotals)
            {
                int nutritionTotalsID = HashMapDataFns.getIntVal(hm, "PANUTRITIONTOTALSID");
                String dvCalorieValLbl = HashMapDataFns.getStringVal(hm, "DVCALORIEVALLBL");
                String nutritonInfo1Total = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO1TOTAL");
                String nutritonInfo2Total = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO2TOTAL");
                String nutritonInfo3Total = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO3TOTAL");
                String nutritonInfo4Total = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO4TOTAL");
                String nutritonInfo5Total = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO5TOTAL");

                NutritionTotalConfig totalConfig = new NutritionTotalConfig();

                totalConfig.setNutritionTotalsID(nutritionTotalsID);
                totalConfig.setDvCalorieValLbl(dvCalorieValLbl);
                totalConfig.setNutritionInfo1Total(nutritonInfo1Total);
                totalConfig.setNutritionInfo2Total(nutritonInfo2Total);
                totalConfig.setNutritionInfo3Total(nutritonInfo3Total);
                totalConfig.setNutritionInfo4Total(nutritonInfo4Total);
                totalConfig.setNutritionInfo5Total(nutritonInfo5Total);

                nutritionTotalsConfig.add(totalConfig);
            }
        }
        catch (Exception e)
        {
            ReceiptGen.logError("NutritionData.getNutriTotalConfigFromDB error", e);
        }
    }

    private void calculateNutriTotals(TransactionData txnData, DataManager dm)
    {
        try
        {
            ArrayList pluIDArrList = new ArrayList();
            BigDecimal nutritionTotals1 = BigDecimal.ZERO;
            BigDecimal nutritionTotals2 = BigDecimal.ZERO;
            BigDecimal nutritionTotals3 = BigDecimal.ZERO;
            BigDecimal nutritionTotals4 = BigDecimal.ZERO;
            BigDecimal nutritionTotals5 = BigDecimal.ZERO;
            
            // Compile plu IDs of products in the transaction
            // Also start to create nutrition items
            for (int x = 0; x < txnData.getTransItems().size(); x++)
            {
                if (txnData.getTransItems().get(x) instanceof Plu)
                {
                    Plu currentProduct = (Plu)txnData.getTransItems().get(x);
                    pluIDArrList.add(currentProduct.getPluID());

                    NutritionItem item = new NutritionItem();
                    item.setPluID(currentProduct.getPluID());
                    //Converting QTY to an Int (to save space) since we are only using whole numbers right now anyway
                    Integer quantAsInt = new Double(currentProduct.getQuantity()).intValue();
                    item.setQuantity(quantAsInt.toString());
                    item.setProductName(currentProduct.getName());

                    nutritionItemsDB.add(item);
                }
            }

            String pluIDStr = commDM.ArrayListToCSV(pluIDArrList);

            if (pluIDStr.isEmpty())
            {
                return;
            }

            // Get nutrition data for products in transaction from DB
            ArrayList<HashMap> prodNutriDBInfo = dm.serializeSqlWithColNames("data.ReceiptGen.GetProductNutriInfo", new Object[]{pluIDStr}, null, COMMON_LOG_FILE, false);

            // Create HashMap (key = PAPluID, value = nutri HashMap)
            HashMap<Integer, HashMap<String, String>> PaPluNutriInfo = new HashMap<Integer, HashMap<String, String>>();

            // Iterate through all NutritionItems records
            //      For each NutritionItems, set NutritionInfo 1-5 (if it exists)
            //      Update the nutritionTotals[X] value
            for (HashMap hm : prodNutriDBInfo)
            {
                int hmPluID = HashMapDataFns.getIntVal(hm, "PAPLUID");
                String strProdNutriInfo1 = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO1");
                String strProdNutriInfo2 = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO2");
                String strProdNutriInfo3 = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO3");
                String strProdNutriInfo4 = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO4");
                String strProdNutriInfo5 = HashMapDataFns.getStringVal(hm, "NUTRITIONINFO5");

                HashMap<String, String> nutriMap = new HashMap<>();
                nutriMap.put("NUTRITIONINFO1", strProdNutriInfo1);
                nutriMap.put("NUTRITIONINFO2", strProdNutriInfo2);
                nutriMap.put("NUTRITIONINFO3", strProdNutriInfo3);
                nutriMap.put("NUTRITIONINFO4", strProdNutriInfo4);
                nutriMap.put("NUTRITIONINFO5", strProdNutriInfo5);

                PaPluNutriInfo.put(hmPluID, nutriMap);
            }

            for (NutritionItem nutriItem : nutritionItemsDB)
            {
                int currentPluID = nutriItem.getPluID();
                if(PaPluNutriInfo.containsKey(currentPluID))
                {
                    //Get the corresponding HashMap of nutri data for this Plu
                    HashMap thisPluNutriHM = PaPluNutriInfo.get(currentPluID);

                    String strProdNutriInfoOne = HashMapDataFns.getStringVal(thisPluNutriHM, "NUTRITIONINFO1");
                    String strProdNutriInfoTwo = HashMapDataFns.getStringVal(thisPluNutriHM, "NUTRITIONINFO2");
                    String strProdNutriInfoThree = HashMapDataFns.getStringVal(thisPluNutriHM, "NUTRITIONINFO3");
                    String strProdNutriInfoFour = HashMapDataFns.getStringVal(thisPluNutriHM, "NUTRITIONINFO4");
                    String strProdNutriInfoFive = HashMapDataFns.getStringVal(thisPluNutriHM, "NUTRITIONINFO5");

                    // Update nutritionalItem with category value
                    //      Use *** if there is no value set for the category
                    //      Use ### if the nutritional value is over 9999
                    // Update nutritionTotals[x] value
                    if (strProdNutriInfoOne.equals("")) {
                        nutriItem.setNutritionInfo1(strProdNutriInfoOne);
                        strProdNutriInfoOne = "***";
                    } else {
                        BigDecimal prodNutriInfoOne = nutriItem.calculateNutritionTotal(strProdNutriInfoOne);
                        nutriItem.setNutritionInfo1(prodNutriInfoOne.toString());
                        nutritionTotals1 = nutritionTotals1.add(prodNutriInfoOne);
                    }

                    if (strProdNutriInfoTwo.equals("")) {
                        nutriItem.setNutritionInfo2(strProdNutriInfoTwo);
                        strProdNutriInfoTwo = "***";
                    } else {
                        BigDecimal prodNutriInfoTwo = nutriItem.calculateNutritionTotal(strProdNutriInfoTwo);
                        nutriItem.setNutritionInfo2(prodNutriInfoTwo.toString());
                        nutritionTotals2 = nutritionTotals2.add(prodNutriInfoTwo);
                    }

                    if (strProdNutriInfoThree.equals("")) {
                        nutriItem.setNutritionInfo3(strProdNutriInfoThree);
                        strProdNutriInfoThree = "***";
                    } else {
                        BigDecimal prodNutriInfoThree = nutriItem.calculateNutritionTotal(strProdNutriInfoThree);
                        nutriItem.setNutritionInfo3(prodNutriInfoThree.toString());
                        nutritionTotals3 = nutritionTotals3.add(prodNutriInfoThree);
                    }

                    if (strProdNutriInfoFour.equals("")) {
                        nutriItem.setNutritionInfo4(strProdNutriInfoFour);
                        strProdNutriInfoFour = "***";
                    } else {
                        BigDecimal prodNutriInfoFour = nutriItem.calculateNutritionTotal(strProdNutriInfoFour);
                        nutriItem.setNutritionInfo4(prodNutriInfoFour.toString());
                        nutritionTotals4 = nutritionTotals4.add(prodNutriInfoFour);
                    }

                    if (strProdNutriInfoFive.equals("")) {
                        nutriItem.setNutritionInfo5(strProdNutriInfoFive);
                        strProdNutriInfoFive = "***";
                    } else {
                        BigDecimal prodNutriInfoFive = nutriItem.calculateNutritionTotal(strProdNutriInfoFive);
                        nutriItem.setNutritionInfo5(prodNutriInfoFive.toString());
                        nutritionTotals5 = nutritionTotals5.add(prodNutriInfoFive);
                    }

                    if (strProdNutriInfoOne.equals("***") || strProdNutriInfoTwo.equals("***") || strProdNutriInfoThree.equals("***") || strProdNutriInfoFour.equals("***") || strProdNutriInfoFive.equals("***")) {
                        needsNAFootnote = true;
                    }
                }
            }

            // Modify nutrition totals, if needed
            // Add them to the NutritionTotals AL
            ArrayList<String> totals = new ArrayList<String>();
            totals.add(cleanUpNutritionTotal(nutritionTotals1));
            totals.add(cleanUpNutritionTotal(nutritionTotals2));
            totals.add(cleanUpNutritionTotal(nutritionTotals3));
            totals.add(cleanUpNutritionTotal(nutritionTotals4));
            totals.add(cleanUpNutritionTotal(nutritionTotals5));
            nutritionTotals.add(totals);

            // Calculate and add daily value percentages
            calculateDailyValuePercentages(nutritionTotals1, nutritionTotals2, nutritionTotals3, nutritionTotals4, nutritionTotals5);

        }
        catch (Exception e)
        {
            ReceiptGen.logError("NutritionData.calculateNutriTotals error", e);
        }
    }

    // Modifies the nutrition total print value if needed
    // Returns the string equivalent of the nutritionTotal
    private String cleanUpNutritionTotal(BigDecimal nutritionTotal)
    {
        String strNutritionTotal = "";
        BigDecimal maxValLimit = new BigDecimal("9999");
        
        if (nutritionTotal.compareTo(maxValLimit) == 1)
        {
            strNutritionTotal = "###";
            needsTruncateDigitsFootnote = true;
        }
        else
        {
            strNutritionTotal = nutritionTotal.setScale(0, RoundingMode.HALF_UP).toPlainString();
        }

        return strNutritionTotal;
    }

    private void calculateDailyValuePercentages(BigDecimal nutritionTotal1, BigDecimal nutritionTotal2, BigDecimal nutritionTotal3, BigDecimal nutritionTotal4, BigDecimal nutritionTotal5)
    {
        ArrayList<String> dvTotals = new ArrayList<String>();
        
        for (NutritionTotalConfig totalConfig : nutritionTotalsConfig)
        {
            dvTotals = new ArrayList<String>();
            String strDVTotalsOne = "***";
            String strDVTotalsTwo = "***";
            String strDVTotalsThree = "***";
            String strDVTotalsFour = "***";
            String strDVTotalsFive = "***";

            // Add Daily Value Calorie Label
            dvTotals.add(totalConfig.getDvCalorieValLbl());

            // Calculate daily value percentage for each category
            // For totals, if the length of the value is:
            //      - 3 digits or less: Print with "%"
            //      - 4 digits long: Print without "%"
            //      - 5 digits long: Print "###"

            // CATEGORY 1
            if (totalConfig.getNutritionInfo1Total() != null)
            {
                BigDecimal nutritionMaxVal = totalConfig.getNutritionInfo1Total();
                BigDecimal dvTotalsOne = nutritionTotal1.divide(nutritionMaxVal);
                dvTotalsOne = dvTotalsOne.multiply(new BigDecimal("100"));

                strDVTotalsOne = determineDailyValuePercentage(dvTotalsOne);
            }
            else
            {
                needsNAFootnote = true;
            }
            dvTotals.add(strDVTotalsOne);

            // CATEGORY 2
            if (totalConfig.getNutritionInfo2Total() != null)
            {
                BigDecimal nutritionMaxVal = totalConfig.getNutritionInfo2Total();
                BigDecimal dvTotalsTwo = nutritionTotal2.divide(nutritionMaxVal, 4, RoundingMode.HALF_UP);
                dvTotalsTwo = dvTotalsTwo.multiply(new BigDecimal("100"));

                strDVTotalsTwo = determineDailyValuePercentage(dvTotalsTwo);
            }
            else
            {
                needsNAFootnote = true;
            }
            dvTotals.add(strDVTotalsTwo);

            // CATEGORY 3
            if (totalConfig.getNutritionInfo3Total() != null)
            {
                BigDecimal nutritionMaxVal = totalConfig.getNutritionInfo3Total();
                BigDecimal dvTotalsThree = nutritionTotal3.divide(nutritionMaxVal, 4, RoundingMode.HALF_UP);
                dvTotalsThree = dvTotalsThree.multiply(new BigDecimal("100"));

                strDVTotalsThree = determineDailyValuePercentage(dvTotalsThree);
            }
            else
            {
                needsNAFootnote = true;
            }
            dvTotals.add(strDVTotalsThree);

            // CATEGORY 4
            if (totalConfig.getNutritionInfo4Total() != null)
            {
                BigDecimal nutritionMaxVal = totalConfig.getNutritionInfo4Total();
                BigDecimal dvTotalsFour = nutritionTotal4.divide(nutritionMaxVal, 4, RoundingMode.HALF_UP);
                dvTotalsFour = dvTotalsFour.multiply(new BigDecimal("100"));

                strDVTotalsFour = determineDailyValuePercentage(dvTotalsFour);
            }
            else
            {
                needsNAFootnote = true;
            }
            dvTotals.add(strDVTotalsFour);

            // CATEGORY 5
            if (totalConfig.getNutritionInfo5Total() != null)
            {
                BigDecimal nutritionMaxVal = totalConfig.getNutritionInfo5Total();
                BigDecimal dvTotalsFive = nutritionTotal5.divide(nutritionMaxVal, 4, RoundingMode.HALF_UP);
                dvTotalsFive = dvTotalsFive.multiply(new BigDecimal("100"));

                strDVTotalsFive = determineDailyValuePercentage(dvTotalsFive);
            }
            else
            {
                needsNAFootnote = true;
            }
            dvTotals.add(strDVTotalsFive);

            nutritionTotals.add(dvTotals);
        }
    }

    private String determineDailyValuePercentage(BigDecimal dvTotalsX)
    {
        String strDVTotalVal = "***";
        BigDecimal threeDigitMax = new BigDecimal("999");
        BigDecimal fourDigitMax = new BigDecimal("9999");

        if (dvTotalsX.compareTo(threeDigitMax) <= 0)
        {
            // If total is <= 999, print it with a % sign
            strDVTotalVal = dvTotalsX.setScale(0, RoundingMode.HALF_UP).toPlainString() + "%";
        }
        else if (dvTotalsX.compareTo(threeDigitMax) == 1 && dvTotalsX.compareTo(fourDigitMax) <= 0)
        {
            // If total is > 999 AND total is <= 9999, print it with NO % sign
            strDVTotalVal = dvTotalsX.setScale(0, RoundingMode.HALF_UP).toPlainString();
        }
        else
        {
            strDVTotalVal = "###";
            needsTruncateDigitsFootnote = true;
        }

        return strDVTotalVal;

    }
    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public void addNAFootnote () {
        this.needsNAFootnote = true;
    }

    public boolean getNeedsNAFootnote()
    {
        return needsNAFootnote;
    }

    public void addTruncateDigitsFootnote () {
        this.needsTruncateDigitsFootnote = true;
    }

    public boolean getNeedsTruncateDigitsFootnote()
    {
        return needsTruncateDigitsFootnote;
    }

    @SuppressWarnings("unchecked")
    public void addNutritionTotals (ArrayList<String> nutritionTotalsToAdd) {
        if (!DataFunctions.isEmptyCollection(this.nutritionTotals)) {
            this.nutritionTotals = new ArrayList();
            if (!DataFunctions.isEmptyCollection(nutritionTotalsToAdd)) {
                this.nutritionTotals.add(nutritionTotalsToAdd);
            }
        }
        else {
            if (!DataFunctions.isEmptyCollection(nutritionTotalsToAdd)) {
                this.nutritionTotals.add(nutritionTotalsToAdd);
            }
        }
    }

    public ArrayList getNutritionTotals()
    {
        return nutritionTotals;
    }

    public void addNutritionCategory (NutritionCategory nutritionCategory) {
        if (!DataFunctions.isEmptyCollection(this.nutritionCategories)) {
            if (nutritionCategory != null) {
                this.nutritionCategories.add(nutritionCategory);
            }
        }
        else {
            this.nutritionCategories = new ArrayList<>();
            if (nutritionCategory != null) {
                this.nutritionCategories.add(nutritionCategory);
            }
        }
    }

    public void addNutritionTotalConfiguration (NutritionTotalConfig nutritionTotalConfig) {
        if (!DataFunctions.isEmptyCollection(this.nutritionTotalsConfig)) {
            if (nutritionTotalConfig != null) {
                this.nutritionTotalsConfig.add(nutritionTotalConfig);
            }
        }
        else {
            this.nutritionTotalsConfig = new ArrayList<>();
            if (nutritionTotalConfig != null) {
                this.nutritionTotalsConfig.add(nutritionTotalConfig);
            }
        }
    }

    public ArrayList<NutritionTotalConfig> getNutritionTotalsConfig () {return nutritionTotalsConfig;}

    public ArrayList<NutritionCategory> getNutritionCategories()
    {
        return nutritionCategories;
    }

    public ArrayList getNutritionItemsFlex()
    {
        return nutritionItemsFlex;
    }

    public void addNutiritionItemToNutritionItemsDB (NutritionItem nutritionItem) {
        if (DataFunctions.isEmptyCollection(this.nutritionItemsDB)) {
            this.nutritionItemsDB = new ArrayList<>();
            if (nutritionItem != null) {
                this.nutritionItemsDB.add(nutritionItem);
            }
        }
        else {
            if (nutritionItem != null) {
                this.nutritionItemsDB.add(nutritionItem);
            }
        }
    }

    public ArrayList<NutritionItem> getNutritionItemsDB()
    {
        return nutritionItemsDB;
    }

}
