package com.mmhayes.common.receiptGen.ReceiptData;

/**
 * Created by nyu on 6/26/2018.
 */
public class FreedomPayReceiptData implements IPaymentProcessorReceiptData
{
    private final static String logFileName = "PaymentProcessorRcpt.log";
    private String receiptContent;

    public FreedomPayReceiptData()
    {
    }

    public String getReceiptContent()
    {
        return receiptContent;
    }

    public void setReceiptContent(String receiptContent)
    {
        this.receiptContent = receiptContent;
    }
}
