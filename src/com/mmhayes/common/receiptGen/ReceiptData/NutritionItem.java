package com.mmhayes.common.receiptGen.ReceiptData;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * Created by nyu on 7/26/2017.
 */
public class NutritionItem
{
    private int pluID = 0;
    private String quantity = "";
    private String productName = "";
    private String nutritionInfo1 = "";
    private String nutritionInfo2 = "";
    private String nutritionInfo3 = "";
    private String nutritionInfo4 = "";
    private String nutritionInfo5 = "";
    
    public NutritionItem()
    {
        
    }

    public int getPluID()
    {
        return pluID;
    }

    public BigDecimal calculateNutritionTotal(String nutritionAmtForQtyOne)
    {
        BigDecimal nutritionTotal = new BigDecimal(nutritionAmtForQtyOne);

        // The product's total nutritional value is (nutritionalValue * productQuantity)
        nutritionTotal = nutritionTotal.multiply(new BigDecimal(quantity));

        return nutritionTotal;
    }
    


    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public ArrayList convertToArrayList()
    {
        ArrayList arrNutriItem = new ArrayList();

        arrNutriItem.add(quantity);
        arrNutriItem.add(productName);
        arrNutriItem.add(nutritionInfo1);
        arrNutriItem.add(nutritionInfo2);
        arrNutriItem.add(nutritionInfo3);
        arrNutriItem.add(nutritionInfo4);
        arrNutriItem.add(nutritionInfo5);

        return arrNutriItem;
    }
    
    public void setPluID(int pluID)
    {
        this.pluID = pluID;
    }

    public String getQuantity()
    {
        return quantity;
    }

    public void setQuantity(String quantity)
    {
        this.quantity = quantity;
    }

    public String getProductName()
    {
        return productName;
    }

    public void setProductName(String productName)
    {
        this.productName = productName;
    }

    public String getNutritionInfo1()
    {
        return nutritionInfo1;
    }

    public void setNutritionInfo1(String nutriInfo1)
    {
        if (nutriInfo1.equals("") == false)
        {
            if (new Double(nutriInfo1) > 9999)
            {
                this.nutritionInfo1 = "###";
            }
            else
            {
                this.nutritionInfo1 = new BigDecimal(nutriInfo1).setScale(0, RoundingMode.HALF_UP).toPlainString();
            }
        }
        else
        {
            this.nutritionInfo1 = "***";
        }
    }

    public String getNutritionInfo2()
    {
        return nutritionInfo2;
    }

    public void setNutritionInfo2(String nutriInfo2)
    {
        if (nutriInfo2.equals("") == false)
        {
            if (new Double(nutriInfo2) > 9999)
            {
                this.nutritionInfo2 = "###";
            }
            else
            {
                this.nutritionInfo2 = new BigDecimal(nutriInfo2).setScale(0, RoundingMode.HALF_UP).toPlainString();
            }
        }
        else
        {
            this.nutritionInfo2 = "***";
        }
    }

    public String getNutritionInfo3()
    {
        return nutritionInfo3;
    }

    public void setNutritionInfo3(String nutriInfo3)
    {
        if (nutriInfo3.equals("") == false)
        {
            if (new Double(nutriInfo3) > 9999)
            {
                this.nutritionInfo3 = "###";
            }
            else
            {
                this.nutritionInfo3 = new BigDecimal(nutriInfo3).setScale(0, RoundingMode.HALF_UP).toPlainString();
            }
        }
        else
        {
            this.nutritionInfo3 = "***";
        }
    }

    public String getNutritionInfo4()
    {
        return nutritionInfo4;
    }

    public void setNutritionInfo4(String nutriInfo4)
    {
        if (nutriInfo4.equals("") == false)
        {
            if (new Double(nutriInfo4) > 9999)
            {
                this.nutritionInfo4 = "###";
            }
            else
            {
                this.nutritionInfo4 = new BigDecimal(nutriInfo4).setScale(0, RoundingMode.HALF_UP).toPlainString();
            }
        }
        else
        {
            this.nutritionInfo4 = "***";
        }
    }

    public String getNutritionInfo5()
    {
        return nutritionInfo5;
    }

    public void setNutritionInfo5(String nutriInfo5)
    {
        if (nutriInfo5.equals("") == false)
        {
            if (new Double(nutriInfo5) > 9999)
            {
                this.nutritionInfo5 = "###";
            }
            else
            {
                this.nutritionInfo5 = new BigDecimal(nutriInfo5).setScale(0, RoundingMode.HALF_UP).toPlainString();
            }
        }
        else
        {
            this.nutritionInfo5 = "***";
        }
    }
}
