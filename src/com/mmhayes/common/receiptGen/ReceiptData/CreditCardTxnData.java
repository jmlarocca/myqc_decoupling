package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-15 14:50:29 -0400 (Fri, 15 May 2020) $: Date of last commit
    $Rev: 11756 $: Revision of last commit
    Notes: Contains information regarding a credit card used in the transaction.
*/

import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Contains information regarding a credit card used in the transaction.</p>
 *
 */
public class CreditCardTxnData {

    // private member variables of a CreditCardTxnData
    private String ccTxnInfo = "";
    private String delimiter = "";
    private HashMap<String, String> ccDetails = new HashMap<>();
    // credit card information, found in the details
    private String cardTypeName = "";
    private String accountNumber = "";
    private String authCode = "";
    private String merchantID = "";
    private String cardholderName = "";
    private String entryMethod = "";
    private String cvm = "";
    private String invoiceNumber = "";
    private String referenceNumber = "";
    private String appLabel = "";
    private String aid = "";
    private String tvr = "";
    private String iad = "";
    private String tsi = "";
    private String arc = "";
    private String cardholderAgreement = "";

    /**
     * <p>Constructor for a {@link CreditCardTxnData}.</p>
     *
     * @param creditCardTransInfo Credit card transaction data as a {@link String}.
     * @param delimiter The delimiter {@link String} on which to split the credit card transaction data {@link String} to get the credit card details.
     */
    public CreditCardTxnData (String creditCardTransInfo, String delimiter) {
        this.ccTxnInfo = creditCardTransInfo;
        this.delimiter = delimiter;
        initDetails();
        setCreditCardInfo();
    }

    /**
     * <p>Sets the credit card details for a {@link CreditCardTxnData}.</p>
     *
     */
    @SuppressWarnings("Duplicates")
    public void initDetails () {

        try {
            if (StringFunctions.stringHasContent(ccTxnInfo)) {
                String[] details = ccTxnInfo.split(delimiter, -1);
                if (!DataFunctions.isEmptyGenericArr(details)) {
                    for (String detail : details) {
                        if (StringFunctions.stringHasContent(detail)) {
                            String[] splitDetail = detail.split("\\s*=\\s*", -1);
                            if ((!DataFunctions.isEmptyGenericArr(splitDetail)) && (splitDetail.length >= 2)) {
                                ccDetails.put(splitDetail[0], splitDetail[1]);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to get the credit card details from the String %s using a delimiter of %s in CreditCardTxnData.initDetails",
                    Objects.toString(ccTxnInfo, "N/A"),
                    Objects.toString(delimiter, "N/A")), Logger.LEVEL.ERROR);
        }

    }


    /**
     * <p>Sets the credit card information.</p>
     *
     */
    private void setCreditCardInfo () {
        this.cardTypeName = getDetailValue("CardType");
        this.accountNumber = getDetailValue("AcctNo");
        this.authCode = getDetailValue("AuthCode");
        this.merchantID = getDetailValue("MerchID");
        this.cardholderName = getDetailValue("NameOnCard");
        if (StringFunctions.stringHasContent(getDetailValue("CVM"))) {
            this.entryMethod = getDetailValue("EntryMethod");
            this.cvm = getDetailValue("CVM");
            this.invoiceNumber = getDetailValue("InvoiceNo");
            this.referenceNumber = getDetailValue("RefNo");
            this.appLabel = getDetailValue("AppLbl");
            this.aid = getDetailValue("AID");
            this.tvr = getDetailValue("TVR");
            this.iad = getDetailValue("IAD");
            this.tsi = getDetailValue("TSI");
            this.arc = getDetailValue("ARC");
            if (StringFunctions.stringHasContent(this.entryMethod)) {
                if ((this.entryMethod.equalsIgnoreCase("CHIP")) && (this.cvm.equalsIgnoreCase("PIN VERIFIED"))) {
                    this.cardholderAgreement = "BY ENTERING A VERIFIED PIN, CARDHOLDER AGREES TO PAY ISSUER SUCH TOTAL IN ACCORDANCE WITH ISSUER'S AGREEMENT WITH CARDHOLDER";
                }
                else {
                    this.cardholderAgreement = "I AGREE TO PAY THE ABOVE TOTAL AMOUNT ACCORDING TO CARD ISSUER AGREEMENT (MERCHANT AGREEMENT IF CREDIT VOUCHER)";
                }
            }
        }
    }

    /**
     * <p>Utility method to get the credit card detail value {@link String} at the given key {@link String}.</p>
     *
     * @param key The key {@link String} at which to get the detail from the credit card detail {@link HashMap}.
     * @return The credit card detail value {@link String} at the given key {@link String}.
     */
    public String getDetailValue (String key) {
        String value = "";

        try {
            if ((StringFunctions.stringHasContent(key)) && (!DataFunctions.isEmptyMap(ccDetails)) && (ccDetails.containsKey(key))) {
                value = HashMapDataFns.getStringVal(ccDetails, key);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("Unable to get the detail at the key of %s from the credit card details in CreditCardTxnData.getDetailValue",
                    Objects.toString(key, "N/A")), Logger.LEVEL.ERROR);
        }

        return value;
    }

    /**
     * <p>Overridden toString() method for a {@link CreditCardTxnData}.</p>
     *
     * @return A {@link String} representation of this {@link CreditCardTxnData}.
     */
    @Override
    public String toString () {

        return String.format("GCTXNDATASTR: %s, DELIMITER: %s, GCDETAILS: %s",
                Objects.toString((StringFunctions.stringHasContent(ccTxnInfo) ? ccTxnInfo : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(delimiter) ? delimiter : "N/A"), "N/A"),
                Objects.toString((!DataFunctions.isEmptyMap(ccDetails) ? Collections.singletonList(ccDetails).toString() : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link CreditCardTxnData}.
     * Two {@link CreditCardTxnData} are defined as being equal if they have the same ccTxnInfo.</p>
     *
     * @param obj The {@link Object} to compare against this {@link CreditCardTxnData}.
     * @return Whether or not the {@link Object} is equal to this {@link CreditCardTxnData}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a CreditCardTxnData and check if the obj's ccTxnInfo is equal to this CreditCardTxnData's ccTxnInfo
        CreditCardTxnData creditCardTxnData = ((CreditCardTxnData) obj);
        return Objects.equals(creditCardTxnData.ccTxnInfo, ccTxnInfo);
    }

    /**
     * <p>Overridden hashCode() method for a {@link CreditCardTxnData}.</p>
     *
     * @return The unique hash code for this {@link CreditCardTxnData}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(ccTxnInfo);
    }

    /**
     * <p>Getter for the cardTypeName field of the {@link CreditCardTxnData}.</p>
     *
     * @return The cardTypeName field of the {@link CreditCardTxnData}.
     */
    public String getCardTypeName () {
        return cardTypeName;
    }

    /**
     * <p>Setter for the cardTypeName field of the {@link CreditCardTxnData}.</p>
     *
     * @param cardTypeName The cardTypeName field of the {@link CreditCardTxnData}.
     */
    public void setCardTypeName (String cardTypeName) {
        this.cardTypeName = cardTypeName;
    }

    /**
     * <p>Getter for the accountNumber field of the {@link CreditCardTxnData}.</p>
     *
     * @return The accountNumber field of the {@link CreditCardTxnData}.
     */
    public String getAccountNumber () {
        return accountNumber;
    }

    /**
     * <p>Setter for the accountNumber field of the {@link CreditCardTxnData}.</p>
     *
     * @param accountNumber The accountNumber field of the {@link CreditCardTxnData}.
     */
    public void setAccountNumber (String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * <p>Getter for the authCode field of the {@link CreditCardTxnData}.</p>
     *
     * @return The authCode field of the {@link CreditCardTxnData}.
     */
    public String getAuthCode () {
        return authCode;
    }

    /**
     * <p>Setter for the authCode field of the {@link CreditCardTxnData}.</p>
     *
     * @param authCode The authCode field of the {@link CreditCardTxnData}.
     */
    public void setAuthCode (String authCode) {
        this.authCode = authCode;
    }

    /**
     * <p>Getter for the merchantID field of the {@link CreditCardTxnData}.</p>
     *
     * @return The merchantID field of the {@link CreditCardTxnData}.
     */
    public String getMerchantID () {
        return merchantID;
    }

    /**
     * <p>Setter for the merchantID field of the {@link CreditCardTxnData}.</p>
     *
     * @param merchantID The merchantID field of the {@link CreditCardTxnData}.
     */
    public void setMerchantID (String merchantID) {
        this.merchantID = merchantID;
    }

    /**
     * <p>Getter for the cardholderName field of the {@link CreditCardTxnData}.</p>
     *
     * @return The cardholderName field of the {@link CreditCardTxnData}.
     */
    public String getCardholderName () {
        return cardholderName;
    }

    /**
     * <p>Setter for the cardholderName field of the {@link CreditCardTxnData}.</p>
     *
     * @param cardholderName The cardholderName field of the {@link CreditCardTxnData}.
     */
    public void setCardholderName (String cardholderName) {
        this.cardholderName = cardholderName;
    }

    /**
     * <p>Getter for the entryMethod field of the {@link CreditCardTxnData}.</p>
     *
     * @return The entryMethod field of the {@link CreditCardTxnData}.
     */
    public String getEntryMethod () {
        return entryMethod;
    }

    /**
     * <p>Setter for the entryMethod field of the {@link CreditCardTxnData}.</p>
     *
     * @param entryMethod The entryMethod field of the {@link CreditCardTxnData}.
     */
    public void setEntryMethod (String entryMethod) {
        this.entryMethod = entryMethod;
    }

    /**
     * <p>Getter for the cvm field of the {@link CreditCardTxnData}.</p>
     *
     * @return The cvm field of the {@link CreditCardTxnData}.
     */
    public String getCvm () {
        return cvm;
    }

    /**
     * <p>Setter for the cvm field of the {@link CreditCardTxnData}.</p>
     *
     * @param cvm The cvm field of the {@link CreditCardTxnData}.
     */
    public void setCvm (String cvm) {
        this.cvm = cvm;
    }

    /**
     * <p>Getter for the invoiceNumber field of the {@link CreditCardTxnData}.</p>
     *
     * @return The invoiceNumber field of the {@link CreditCardTxnData}.
     */
    public String getInvoiceNumber () {
        return invoiceNumber;
    }

    /**
     * <p>Setter for the invoiceNumber field of the {@link CreditCardTxnData}.</p>
     *
     * @param invoiceNumber The invoiceNumber field of the {@link CreditCardTxnData}.
     */
    public void setInvoiceNumber (String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    /**
     * <p>Getter for the referenceNumber field of the {@link CreditCardTxnData}.</p>
     *
     * @return The referenceNumber field of the {@link CreditCardTxnData}.
     */
    public String getReferenceNumber () {
        return referenceNumber;
    }

    /**
     * <p>Setter for the referenceNumber field of the {@link CreditCardTxnData}.</p>
     *
     * @param referenceNumber The referenceNumber field of the {@link CreditCardTxnData}.
     */
    public void setReferenceNumber (String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    /**
     * <p>Getter for the appLabel field of the {@link CreditCardTxnData}.</p>
     *
     * @return The appLabel field of the {@link CreditCardTxnData}.
     */
    public String getAppLabel () {
        return appLabel;
    }

    /**
     * <p>Setter for the appLabel field of the {@link CreditCardTxnData}.</p>
     *
     * @param appLabel The appLabel field of the {@link CreditCardTxnData}.
     */
    public void setAppLabel (String appLabel) {
        this.appLabel = appLabel;
    }

    /**
     * <p>Getter for the aid field of the {@link CreditCardTxnData}.</p>
     *
     * @return The aid field of the {@link CreditCardTxnData}.
     */
    public String getAid () {
        return aid;
    }

    /**
     * <p>Setter for the aid field of the {@link CreditCardTxnData}.</p>
     *
     * @param aid The aid field of the {@link CreditCardTxnData}.
     */
    public void setAid (String aid) {
        this.aid = aid;
    }

    /**
     * <p>Getter for the tvr field of the {@link CreditCardTxnData}.</p>
     *
     * @return The tvr field of the {@link CreditCardTxnData}.
     */
    public String getTvr () {
        return tvr;
    }

    /**
     * <p>Setter for the tvr field of the {@link CreditCardTxnData}.</p>
     *
     * @param tvr The tvr field of the {@link CreditCardTxnData}.
     */
    public void setTvr (String tvr) {
        this.tvr = tvr;
    }

    /**
     * <p>Getter for the iad field of the {@link CreditCardTxnData}.</p>
     *
     * @return The iad field of the {@link CreditCardTxnData}.
     */
    public String getIad () {
        return iad;
    }

    /**
     * <p>Setter for the iad field of the {@link CreditCardTxnData}.</p>
     *
     * @param iad The iad field of the {@link CreditCardTxnData}.
     */
    public void setIad (String iad) {
        this.iad = iad;
    }

    /**
     * <p>Getter for the tsi field of the {@link CreditCardTxnData}.</p>
     *
     * @return The tsi field of the {@link CreditCardTxnData}.
     */
    public String getTsi () {
        return tsi;
    }

    /**
     * <p>Setter for the tsi field of the {@link CreditCardTxnData}.</p>
     *
     * @param tsi The tsi field of the {@link CreditCardTxnData}.
     */
    public void setTsi (String tsi) {
        this.tsi = tsi;
    }

    /**
     * <p>Getter for the arc field of the {@link CreditCardTxnData}.</p>
     *
     * @return The arc field of the {@link CreditCardTxnData}.
     */
    public String getArc () {
        return arc;
    }

    /**
     * <p>Setter for the arc field of the {@link CreditCardTxnData}.</p>
     *
     * @param arc The arc field of the {@link CreditCardTxnData}.
     */
    public void setArc (String arc) {
        this.arc = arc;
    }

    /**
     * <p>Getter for the cardholderAgreement field of the {@link CreditCardTxnData}.</p>
     *
     * @return The cardholderAgreement field of the {@link CreditCardTxnData}.
     */
    public String getCardholderAgreement () {
        return cardholderAgreement;
    }

    /**
     * <p>Setter for the cardholderAgreement field of the {@link CreditCardTxnData}.</p>
     *
     * @param cardholderAgreement The cardholderAgreement field of the {@link CreditCardTxnData}.
     */
    public void setCardholderAgreement (String cardholderAgreement) {
        this.cardholderAgreement = cardholderAgreement;
    }

}