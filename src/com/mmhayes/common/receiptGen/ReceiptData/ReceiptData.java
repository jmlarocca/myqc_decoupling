package com.mmhayes.common.receiptGen.ReceiptData;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.loyalty.models.LoyaltyPointModel;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.Models.*;
import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.PrintJobInfo;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TransactionData.EmployeeData;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import org.owasp.esapi.ESAPI;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;
import java.util.Objects;

/**
 * Created by MEBrown on 6/24/2015.
 */
public class ReceiptData
{
    private int receiptTypeID;
    private final static String COMMON_LOG_FILE = "ConsolidatedReceipt.log";
    protected DataManager dm;

    private boolean popup = false;                  // Not sure what this is for?

    // Transaction data - this should be constructed regardless what type of receipt this is
    private TransactionData txnData;
    private com.mmhayes.common.kms.receiptGen.transactionData.TransactionData transactionData;

    // Nutrition data - only needed for printed/emailed/popup
    private NutritionData nutritionData;

    // Loyalty data
    private HashMap<Integer,LoyaltyDetail> loyaltyData = new HashMap<Integer,LoyaltyDetail>();          // Key: RewardProgramID, Value: LoyaltyDetail (how many points were earned/redeemed for a specific reward program)

    // Receipt settings
    private boolean printOrderNumber = false;       // From QC_Terminals.PAPrintOrderNumber
    private boolean printProductCodes = false;      // From QC_RevenueCenter.PAPrintPLUCodes
    private boolean printNutritionInfo = false;     // From QC_Terminals.PAPrintNutritionInfo
    private boolean printOrderTypeAndTimeMsg = true; // From QC_MyQCTerminal.ShowOrderTypeAndTimeMsg
    private int badgeNumDigitsPrinted = -1;          // From QC_Terminals.PABadgeNumDigitsPrinted
    private boolean displayWellnessIcons = false;   // Only for MyQC receipts -- from QC_Globals.WellnessOnReceipts

    // General receipt variables
    private String logoAlignment;                   // What uses the logo stuff??

    private String orderNumber = "";
    private ArrayList<String> receiptHeaders = new ArrayList();
    private ArrayList<String> receiptFooters = new ArrayList();
    private ArrayList<String> receiptAcctInfoLines = new ArrayList();

    // QCPOS specific receipt variables
    private boolean isMerchantCopy = false;
    private boolean printTxnBarcode = false;
    private boolean printGrossWeight = false;
    private boolean printNetWeight = false;

    // KP/KDS/KMS variables
    private PrintJobInfo printJobInfo;
    private ArrayList<HashMap> receiptDetails = new ArrayList<>();

    // Currency variables
    private String currencyType = "$";
    private boolean currencyBeforePrice = true;
    private boolean decimalInPrice = true;

    // Kitchen printer specific receipt variables
    private KitchenPrinterJobInfo kpJobInfo;

    // For credit cards; the payment gateway assigned to the terminal
    private int paymentGatewayID = -1;

    public ReceiptData()
    {
        receiptTypeID = TypeData.ReceiptType.PRINTEDRECEIPT;        // We'll make this the default
    }

    // allow CustomerReceiptData to be part of ReceiptData
    private CustomerReceiptData customerReceiptData = null;

    public String getLogFileName(int terminalId)
    {
        if (terminalId <= 0)
        {
            return COMMON_LOG_FILE;
        }
        else
        {
            return "QCPOS_" + terminalId + "_T.log";
        }
    }

    void logError(String error, Exception e)
    {
        ReceiptGen.logError(error, e);
    }

    //--------------------------------------------------------------------------
    //  Building Transaction and Receipt Data details
    //--------------------------------------------------------------------------

    /**
     * <p>Builds the {@link TransactionData} for the transaction with the given ID.</p>
     *
     * @param paTransactionID The ID of the transaction to build the {@link TransactionData} for.
     */
    public void buildTransactionV2 (long paTransactionID, TransactionData transactionData) {

        try {
            Logger.logMessage(String.format("Attempting to build the TransactionData for the transaction with an ID of %s in ReceiptData.buildTransactionV2",
                    Objects.toString(paTransactionID)), getLogFileName(0), Logger.LEVEL.DEBUG);

            // make sure we have a valid DataManager
            if (dm == null) {
                Logger.logMessage("No valid data manager found in ReceiptData.buildTransactionV2", getLogFileName(0), Logger.LEVEL.ERROR);
                return;
            }

            // create a new TransactionData
            txnData = transactionData;//new TransactionData();
            txnData.setDataManager(dm);

            // get the transaction information from the database
//            ArrayList<HashMap> transactionInfo = //txnData.getTransactionInfoV2(paTransactionID);
//            if (DataFunctions.isEmptyCollection(transactionInfo)) {
//                Logger.logMessage(String.format("No transaction data found in the database, unable to build a TransactionData object for the transaction with an ID of %s ReceiptData.buildTransactionV2",
//                        Objects.toString(paTransactionID, "N/A")), getLogFileName(0), Logger.LEVEL.ERROR);
//                return;
//            }

            // set the header information
            txnData.setTxnHdrData(transactionData.getTxnHdrData());
//                    TransactionHeaderData.buildFromHM(getLogFileName(0), transactionInfo.get(0)));

            // set the receipt headers, footers and account information lines
            //TODO: do we need these?
//            setReceiptHeaders(transactionInfo.get(0));
//            setReceiptFooters(transactionInfo.get(0));
//            setReceiptAcctInfoLines(transactionInfo.get(0));
//            // set the transaction line items
//            txnData.buildLineItemsV2(transactionInfo);

            // determine the print jobs
            if (receiptTypeID == TypeData.ReceiptType.KITCHENPTRRECEIPT) {
                printJobInfo = new PrintJobInfo(getLogFileName(0), dm, txnData);
            }
            else {
                ArrayList loyaltyTxnPointInfo = txnData.getLoyaltyTxnPointInfo(new Long(paTransactionID).intValue());
                if (!DataFunctions.isEmptyCollection(loyaltyTxnPointInfo)) {
                    setLoyaltyDataFromDB(loyaltyTxnPointInfo, txnData.getTransTypeID());
                }
            }

            // build and set the nutrition information
            if ((txnData.getTxnHdrData().getPrintNutritionInfo()) && (receiptTypeID != TypeData.ReceiptType.KITCHENPTRRECEIPT)) {
                nutritionData = NutritionData.buildNutritionDataV2(dm, txnData);
            }
        }
        catch (Exception e) {
            Logger.logException(e, getLogFileName(0));
            Logger.logMessage(String.format("There was a problem trying to build the TransactionData for the transaction with an ID of %s in ReceiptData.buildTransactionV2",
                    Objects.toString(paTransactionID, "N/A")), getLogFileName(0), Logger.LEVEL.ERROR);
        }

    }

    public void buildTransaction (int paTransactionID)
    {
        Logger.logMessage(String.format("In buildTransaction, paTransactionID=%d", paTransactionID), getLogFileName(0), Logger.LEVEL.DEBUG);
        try
        {
            if (dm == null)
            {
                Logger.logMessage("Invalid DataManager passed to ReceiptData.BuildTransaction", getLogFileName(0), Logger.LEVEL.ERROR);
            }

            txnData = new TransactionData();
            txnData.setDataManager(dm);

            // Get transaction info
            ArrayList transactionInfo = txnData.getTransactionInfo(paTransactionID);
            if (transactionInfo == null || transactionInfo.size() == 0)
            {
                return;
            }

            // Get/set transaction header details
            txnData.buildHeaderInfo((HashMap) transactionInfo.get(0));

            // Set headers, footers, and acct info lines
            setReceiptHeaders((HashMap) transactionInfo.get(0));
            setReceiptFooters((HashMap) transactionInfo.get(0));
            setReceiptAcctInfoLines((HashMap) transactionInfo.get(0));

            // Set currency details
            setCurrency((HashMap) transactionInfo.get(0));

            // Set order number and whether or not it should print
            printOrderNumber = HashMapDataFns.getBooleanVal((HashMap) transactionInfo.get(0), "PAPRINTORDERNUMBER");
            buildOrderNumberStr();

            // Set whether or not product codes should be printed
            printProductCodes = HashMapDataFns.getBooleanVal((HashMap) transactionInfo.get(0), "PAPRINTPLUCODES");

            // Set whether or not nutritional info should be printed
            printNutritionInfo = HashMapDataFns.getBooleanVal((HashMap) transactionInfo.get(0), "PAPRINTNUTRITIONINFO");

            // Set how many digits of a badge number should be printed
            badgeNumDigitsPrinted = HashMapDataFns.getIntVal((HashMap) transactionInfo.get(0), "PABADGENUMDIGITSPRINTED");

            // Set whether or not a barcode should be printed
            printTxnBarcode = HashMapDataFns.getBooleanVal((HashMap) transactionInfo.get(0), "PAPRINTTRANSBARCODE");

            // Set whether or not gross weights should be printed
            printGrossWeight = HashMapDataFns.getBooleanVal((HashMap) transactionInfo.get(0), "PRINTTXNGROSSWEIGHT");

            // Set whether or not net weights should be printed
            printNetWeight = HashMapDataFns.getBooleanVal((HashMap) transactionInfo.get(0), "PRINTTXNNETWEIGHT");

            // Set whether or not order type and time info should be printed
            printOrderTypeAndTimeMsg = CommonAPI.convertModelDetailToBoolean(((HashMap) transactionInfo.get(0)).get("SHOWORDERTYPEANDTIMEMSG"), true);

            // Get/set transaction line items
            txnData.buildLineItems(receiptTypeID, transactionInfo, receiptAcctInfoLines);

            // If the receiptType is KITCHENPTRRECEIPT, then we need to get extra information!
            if (receiptTypeID == TypeData.ReceiptType.KITCHENPTRRECEIPT)
            {
                kpJobInfo = new KitchenPrinterJobInfo(new Integer(txnData.getTransactionID()).intValue(), txnData.getTerminalID(), txnData.getRevenueCenterID(), txnData.getTransTypeID(), dm);
                kpJobInfo.createJobDetails(txnData);
            }
            else
            {
                ArrayList loyaltyTxnPointInfo = txnData.getLoyaltyTxnPointInfo(paTransactionID);

                if (loyaltyTxnPointInfo.size() > 0)
                {
                    setLoyaltyDataFromDB(loyaltyTxnPointInfo, txnData.getTransTypeID());
                }
            }

            // Build and set nutrition info
            if (printNutritionInfo && receiptTypeID != TypeData.ReceiptType.KITCHENPTRRECEIPT)
            {
                // We need to pull and compile nutritional information for the transaction before we can create
                // the NutritionData object
                nutritionData = new NutritionData();
                nutritionData.buildNutritionData(txnData, dm);
            }
        }
        catch (Exception e)
        {
            logError("Failed to build transaction", e);
            throw e;
        }
    }

    public void buildTransaction (ArrayList receiptArgs, ArrayList nutritionDataArgs, ArrayList loyaltySummaryData, String suspTxnNameLbl, String suspTxnName)
    {
        if(dm == null)
        {
            Logger.logMessage("Invalid DataManager passed to ReceiptData.BuildTransaction", getLogFileName(0), Logger.LEVEL.ERROR);
        }

        if (receiptArgs.size() == 0)
        {
            Logger.logMessage("ReceiptData.buildTransaction: receiptArgs is empty!");
            return;
        }

        // Set receipt type
        receiptTypeID = TypeData.ReceiptType.PRINTEDRECEIPT;

        // Create TransactionData
        txnData = new TransactionData();
        txnData.setDataManager(dm);

        // Parse out main receipt details from the arraylist
        int transTypeID = new Integer(receiptArgs.get(0).toString()).intValue();
        int trainingModeTransTypeID = new Integer(receiptArgs.get(1).toString()).intValue();
        boolean isMerchantCopy = new Boolean(receiptArgs.get(3).toString()).booleanValue();
        ArrayList<String> receiptHeaders = (ArrayList<String>)receiptArgs.get(4);
        int terminalID = new Integer(receiptArgs.get(5).toString()).intValue();
        String transactionID = receiptArgs.get(6).toString();
        String originalTranID = receiptArgs.get(7).toString();
        String cashierName = receiptArgs.get(8).toString();
        String transDate = receiptArgs.get(9).toString();
        String transTime = receiptArgs.get(10).toString();
        ArrayList<String> receiptFooters = (ArrayList<String>)receiptArgs.get(15);
        ArrayList<ArrayList> transDetails = (ArrayList)receiptArgs.get(11);
        int userID = new Integer(receiptArgs.get(16).toString()).intValue();
        int revCenterID = new Integer(receiptArgs.get(17).toString()).intValue();
        boolean printOrderNumber = new Boolean(receiptArgs.get(18).toString()).booleanValue();
        boolean printNutritionInfo = new Boolean(receiptArgs.get(19).toString()).booleanValue();
        boolean printPLUCodes = new Boolean(receiptArgs.get(20).toString()).booleanValue();
        ArrayList<String> rcptAcctInfoLines = (ArrayList<String>)receiptArgs.get(21);
        boolean printTxnBarcode = receiptArgs.size() > 22 ? new Boolean(receiptArgs.get(22).toString()).booleanValue() : false;
        orderNumber = receiptArgs.size() >= 24 ? receiptArgs.get(23).toString() : "";
        int transactionStatusID = receiptArgs.size() > 24 ? new Integer(receiptArgs.get(24).toString()).intValue() : 0;
        int paymentGatewayID = receiptArgs.size() > 25 ? new Integer(receiptArgs.get(25).toString()).intValue() : 0;

        //TODO -QCPOS needs to send over orderType or orderTypeID in order to print "delivery" or "pickup" on the receipt

        try {
            //Format User Friendly time for Delivery/Pickup
            String transactionDateStr = transDate + " " + transTime;
            Date transactionDate = txnData.getDateVal(transactionDateStr);
            String transDateNoYear =txnData.getFormattedDateTimeStr(transactionDate, "M/dd");
            txnData.setTransDateNoYear(transDateNoYear);
            String transTimeNoSeconds = txnData.getFormattedDateTimeStr(transactionDate, "h:mm a");
            txnData.setTransTimeNoSeconds(transTimeNoSeconds);
        }
        catch (Exception e)
        {
            //If an error occurs, set it to the unformatted date or time
            txnData.setTransDateNoYear(transDate);
            txnData.setTransTimeNoSeconds(transTime);
        }

        // Set main info for TransactionData
        txnData.setTransTypeID(transTypeID);
        txnData.setTrainingModeTransTypeID(trainingModeTransTypeID);
        this.printTxnBarcode = printTxnBarcode;
        this.isMerchantCopy = isMerchantCopy;
        this.receiptHeaders = receiptHeaders;
        txnData.setTerminalID(terminalID);
        txnData.setTransactionID(transactionID);
        txnData.setOrigTransactionID(originalTranID);
        txnData.setCashierName(cashierName);
        txnData.setTransDate(transDate);
        txnData.setTransTime(transTime);
        txnData.setUserID(userID);
        txnData.setRevCenterID(revCenterID);
        txnData.setSuspTxnNameLabel(suspTxnNameLbl);
        txnData.setSuspTxnName(suspTxnName);
        txnData.setTransactionStatus(transactionStatusID);
        this.printOrderNumber = printOrderNumber;
        this.printProductCodes = printPLUCodes;
        this.printNutritionInfo = printNutritionInfo;
        this.receiptAcctInfoLines = rcptAcctInfoLines;
        this.receiptFooters = receiptFooters;
        this.paymentGatewayID = paymentGatewayID;

        try {
            //Format User Friendly time for Delivery/Pickup
            String transactionDateStr = transDate + " " + transTime;
            Date transactionDate = txnData.getDateVal(transactionDateStr);
            String transDateNoYear =txnData. getFormattedDateTimeStr(transactionDate, "M/dd");
            txnData.setTransDateNoYear(transDateNoYear);
            String transTimeNoSeconds = txnData.getFormattedDateTimeStr(transactionDate, "h:mm a");
            txnData.setTransTimeNoSeconds(transTimeNoSeconds);
        }
        catch (Exception e)
        {
            //If an error occurs, set it to the unformatted date or time
            txnData.setTransDateNoYear(transDate);
            txnData.setTransTimeNoSeconds(transTime);
        }

        // Build transaction details
        txnData.buildLineItems(transDetails, rcptAcctInfoLines);

        // Build and set nutrition data
        if (nutritionDataArgs != null && nutritionDataArgs.size() > 0)
        {
            nutritionData = new NutritionData(nutritionDataArgs);
        }

        // Build and set loyalty data
        if (loyaltySummaryData != null && loyaltySummaryData.size() > 0)
        {
            setLoyaltyData(loyaltySummaryData);
        }
    }

    public void buildTransaction(TransactionReceiptModel transactionReceiptModel)
    {
        if(dm == null)
        {
            Logger.logMessage("Invalid DataManager passed to ReceiptData.BuildTransaction", getLogFileName(0), Logger.LEVEL.ERROR);
        }

        if (transactionReceiptModel == null || transactionReceiptModel.getTransactionModel() == null)
        {
            Logger.logMessage("ReceiptData.buildTransaction: transactionModel is null!");
            return;
        }

        //ReceiptType  PRINTEDRECEIPT = 1; GIFTRECEIPT = 2; EMAILEDRECEIPT = 3; POPUPRECEIPT = 4; KITCHENPTRRECEIPT = 5;

        // Set Output type: NONE, PRINT, EMAIL
        switch (transactionReceiptModel.getReceiptModel().getOutputType().toUpperCase()){
            case "PRINT":
                receiptTypeID = 1;
                break;
            case "EMAIL":
                receiptTypeID = 3;
                break;
            default:
                break;
        }

        //NONE, GIFT, POPUP, KITCHENPRINTER;
        switch (transactionReceiptModel.getReceiptModel().getContentType().toUpperCase()){
            case "GIFT":
                receiptTypeID = 2;
                break;
            case "CUSTOMERCOPY":
                break;
            case "MERCHANTCOPY":
                break;
            case "POPUP":
                receiptTypeID = 4;
                break;
            case "KITCHENPRINTER":
                receiptTypeID = 5;
                break;
            default:
                break;
        }

        // Create TransactionData
        txnData = new TransactionData();
        txnData.setDataManager(dm);

        // Parse out main receipt details from the TransactionReceiptModel
        boolean isMerchantCopy = transactionReceiptModel.getReceiptModel().isMerchantCopy();
        boolean printOrderNumber = transactionReceiptModel.getReceiptConfigModel().isPrintOrderNumber();
        boolean printNutritionInfo = transactionReceiptModel.getReceiptConfigModel().isPrintNutritionInfo();
        boolean printPLUCodes = transactionReceiptModel.getReceiptConfigModel().isPrintPluCodes();
        boolean printTxnBarcode = transactionReceiptModel.getReceiptConfigModel().isPrintTransBarCode();

        int transTypeID = transactionReceiptModel.getTransactionModel().getTransactionTypeId();
        int trainingModeTransTypeID = (transactionReceiptModel.getTransactionModel().isTrainingTransactionType()) ? 1 : 0;
        int terminalID = transactionReceiptModel.getTransactionModel().getTerminal().getId();
        int userID = (transactionReceiptModel.getTransactionModel().getUserId() != null) ? transactionReceiptModel.getTransactionModel().getUserId() : -1;
        int revCenterID = transactionReceiptModel.getTransactionModel().getTerminal().getRevenueCenterId();
        int orderTypeId = transactionReceiptModel.getTransactionModel().getOrderTypeId();

        String transactionID = transactionReceiptModel.getTransactionModel().getId().toString();
        String originalTranID = (transactionReceiptModel.getTransactionModel().getOriginalTransactionId() != null) ? transactionReceiptModel.getTransactionModel().getOriginalTransactionId().toString() : "";
        String cashierName = transactionReceiptModel.getTransactionModel().getUserName();

        //Parse out the date and time
        String fullDateTimeStr =  ESAPI.encoder().decodeForHTML(transactionReceiptModel.getTransactionModel().getTimeStamp());

        Date transactionDate = this.txnData.getDateVal(fullDateTimeStr);
        String transDate = this.txnData.getFormattedDateTimeStr(transactionDate, "MM/dd/yyyy");
        String transDateNoYear = this.txnData.getFormattedDateTimeStr(transactionDate, "M/dd");
        String transTime = this.txnData.getFormattedDateTimeStr(transactionDate, "hh:mm:ss a");
        String transTimeNoSeconds = this.txnData.getFormattedDateTimeStr(transactionDate, "h:mm a");
        String transDateTime = this.txnData.getFormattedDateTimeStr(transactionDate, "yyyy-MM-dd HH:mm:ss a");

        ArrayList<String> receiptHeaders = new ArrayList();

        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptHeader1().isEmpty()){
            receiptHeaders.add(transactionReceiptModel.getReceiptConfigModel().getReceiptHeader1());
        }
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptHeader2().isEmpty()){
            receiptHeaders.add(transactionReceiptModel.getReceiptConfigModel().getReceiptHeader2());
        }
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptHeader3().isEmpty()){
            receiptHeaders.add(transactionReceiptModel.getReceiptConfigModel().getReceiptHeader3());
        }
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptHeader4().isEmpty()){
            receiptHeaders.add(transactionReceiptModel.getReceiptConfigModel().getReceiptHeader4());
        }
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptHeader5().isEmpty()){
            receiptHeaders.add(transactionReceiptModel.getReceiptConfigModel().getReceiptHeader5());
        }

        ArrayList<String> receiptFooters = new ArrayList();
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptFooter1().isEmpty()){
            receiptFooters.add(transactionReceiptModel.getReceiptConfigModel().getReceiptFooter1());
        }
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptFooter2().isEmpty()){
            receiptFooters.add(transactionReceiptModel.getReceiptConfigModel().getReceiptFooter2());
        }
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptFooter3().isEmpty()){
            receiptFooters.add(transactionReceiptModel.getReceiptConfigModel().getReceiptFooter3());
        }
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptFooter4().isEmpty()){
            receiptFooters.add(transactionReceiptModel.getReceiptConfigModel().getReceiptFooter4());
        }
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptFooter5().isEmpty()){
            receiptFooters.add(transactionReceiptModel.getReceiptConfigModel().getReceiptFooter5());
        }

        ArrayList<String> rcptAcctInfoLines = new ArrayList();
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptAcctInfoLine1().isEmpty()){
            rcptAcctInfoLines.add(transactionReceiptModel.getReceiptConfigModel().getReceiptAcctInfoLine1());
        }
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptAcctInfoLine2().isEmpty()){
            rcptAcctInfoLines.add(transactionReceiptModel.getReceiptConfigModel().getReceiptAcctInfoLine2());
        }
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptAcctInfoLine3().isEmpty()){
            rcptAcctInfoLines.add(transactionReceiptModel.getReceiptConfigModel().getReceiptAcctInfoLine3());
        }
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptAcctInfoLine4().isEmpty()){
            rcptAcctInfoLines.add(transactionReceiptModel.getReceiptConfigModel().getReceiptAcctInfoLine4());
        }
        if (!transactionReceiptModel.getReceiptConfigModel().getReceiptAcctInfoLine5().isEmpty()){
            rcptAcctInfoLines.add(transactionReceiptModel.getReceiptConfigModel().getReceiptAcctInfoLine5());
        }

        orderNumber = transactionReceiptModel.getTransactionModel().getOrderNumber();

        // Set main info for TransactionData
        txnData.setTransTypeID(transTypeID);
        txnData.setTrainingModeTransTypeID(trainingModeTransTypeID);
        this.printTxnBarcode = printTxnBarcode;
        this.isMerchantCopy = isMerchantCopy;
        this.receiptHeaders = receiptHeaders;
        txnData.setTerminalID(terminalID);
        txnData.setTransactionID(transactionID);
        txnData.setOrigTransactionID(originalTranID);
        txnData.setCashierName(cashierName);
        txnData.setTransDate(transDate);
        txnData.setTransDateNoYear(transDateNoYear);
        txnData.setTransTime(transTime);
        txnData.setTransTimeNoSeconds(transTimeNoSeconds);
        txnData.setUserID(userID);
        txnData.setOrderTypeID(orderTypeId);
        txnData.setRevCenterID(revCenterID);
        this.printOrderNumber = printOrderNumber;
        this.printProductCodes = printPLUCodes;
        this.printNutritionInfo = printNutritionInfo;
        this.receiptAcctInfoLines = rcptAcctInfoLines;
        this.receiptFooters = receiptFooters;
        buildOrderNumberStr();

        //HTML Decode any of the object names
        txnData.cleanLineItems(transactionReceiptModel.getTransactionModel());
        // Build transaction details
        txnData.buildLineItems(transactionReceiptModel.getTransactionModel());

        // If the receiptType is KITCHENPTRRECEIPT, then we need to get extra information!
        if (receiptTypeID == TypeData.ReceiptType.KITCHENPTRRECEIPT) {
            // TODO COMMENT OUT AND REPLACE WITH PRINTJOBINFO WHEN READY
            kpJobInfo = new KitchenPrinterJobInfo(new Integer(txnData.getTransactionID()).intValue(), txnData.getTerminalID(), txnData.getRevenueCenterID(), txnData.getTransTypeID(), dm);
            kpJobInfo.createJobDetails(txnData);
            // TODO MAKE SURE THIS ISN'T COMMENTED OUT WHEN READY
//                printJobInfo = new PrintJobInfo(txnData, dm);
//                printJobInfo.generatePrintJobs();
        } else {
            setLoyaltyDataFromTransactionModel(transactionReceiptModel.getTransactionModel());
        }

        // Build and set nutrition info
        if (this.printNutritionInfo && receiptTypeID != TypeData.ReceiptType.KITCHENPTRRECEIPT) {
            // We need to pull and compile nutritional information for the transaction before we can create
            // the NutritionData object
            Logger.logMessage("Printing Nutrition Information", getLogFileName(0), Logger.LEVEL.DEBUG);
            nutritionData = new NutritionData();
            nutritionData.buildNutritionData(txnData, dm);
            this.fixNutritionDataIfNeeded(nutritionData);
        }
    }

    //--------------------------------------------------------------------------
    //  Setting Receipt Data Details
    //--------------------------------------------------------------------------
    // Returns true if QC_PATransactions.OrderNum exists
    // If false, then order number will need to be derived from PATransactionID
    public boolean useStoredOrderNumber()
    {
        boolean useStoredOrderNum = false;

        Object orderNumColExists = dm.getSingleField("data.ReceiptGen.CheckForOrderNumCol", new Object[]{});

        if (orderNumColExists != null)
        {
            useStoredOrderNum = new Boolean(orderNumColExists.toString()).booleanValue();
        }

        return useStoredOrderNum;
    }

    public void buildOrderNumberStr()
    {
        try
        {
            String paTransactionID = txnData.getTransactionID();

            // Strip out "T" and terminal ID if it's there
            if (paTransactionID.indexOf("T") != -1)
            {
                paTransactionID = paTransactionID.substring(0, paTransactionID.indexOf("T"));
            }

            Object orderNumObj = dm.getSingleField("data.ReceiptGen.GetTxnOrderNumber", new Object[]{paTransactionID});

            if (orderNumObj != null)
            {
                orderNumber = orderNumObj.toString();
            }
        }
        catch (Exception e)
        {
            ReceiptGen.logError("ReceiptData.buildOrderNumberStr error", e);
        }
    }

    public void setReceiptHeaders(HashMap txnDetail)
    {
        try
        {
            for (int x = 1; x <= 5; x++)
            {
                if (HashMapDataFns.getStringVal(txnDetail, "PARECEIPTHEADER" + Integer.toString(x)).equals("") == false)
                {
                    receiptHeaders.add(HashMapDataFns.getStringVal(txnDetail, "PARECEIPTHEADER" + Integer.toString(x)));
                }
            }
        }
        catch (Exception e)
        {
            logError("ReceiptData.setReceiptHeaders error", e);
        }
    }

    public void setReceiptFooters(HashMap txnDetail)
    {
        try
        {
            for (int x = 1; x <= 5; x++)
            {
                if (HashMapDataFns.getStringVal(txnDetail, "PARECEIPTFOOTER" + Integer.toString(x)).equals("") == false)
                {
                    receiptFooters.add(HashMapDataFns.getStringVal(txnDetail, "PARECEIPTFOOTER" + Integer.toString(x)));
                }
            }
        }
        catch (Exception e)
        {
            logError("ReceiptData.setReceiptFooters error", e);
        }
    }

    public void setReceiptAcctInfoLines(HashMap txnDetail)
    {
        try
        {
            for (int x = 1; x <= 5; x++)
            {
                if (HashMapDataFns.getStringVal(txnDetail, "PARECEIPTACCTINFOLINE" + Integer.toString(x)).equals("") == false)
                {
                    receiptAcctInfoLines.add(HashMapDataFns.getStringVal(txnDetail, "PARECEIPTACCTINFOLINE" + Integer.toString(x)));
                }
            }
        }
        catch (Exception e)
        {
            logError("ReceiptData.setReceiptAcctInfoLines error", e);
        }
    }

    public void setLoyaltyData(ArrayList loyaltySummaryData)
    {
        try
        {
            for (int x = 0; x < loyaltySummaryData.size(); x++)
            {
                ArrayList<String> summaryDetail = (ArrayList<String>) loyaltySummaryData.get(x);

                if (x == 0)
                {
                    if (summaryDetail.size() > 0)
                    {
                        EmployeeData loyaltyAcct = new EmployeeData();

                        for (String acctDetail : summaryDetail)
                        {
                            String[] detailSplit = acctDetail.split(":");

                            if (detailSplit[0].contains("Name"))
                            {
                                loyaltyAcct.setName(detailSplit[1]);
                            }
                            else if (detailSplit[0].contains("Badge"))
                            {
                                loyaltyAcct.setBadgeNumber(detailSplit[1]);
                            }
                            else if (detailSplit[0].contains("Acct No"))
                            {
                                loyaltyAcct.setEmployeeNumber(detailSplit[1]);
                            }
                        }

                        txnData.setLoyaltyAcct(loyaltyAcct);
                    }
                }
                else
                {
                    if (summaryDetail.size() >= 4)
                    {
                        int rewardProgramID = new Integer(summaryDetail.get(0).toString()).intValue();
                        String programName = summaryDetail.get(1).toString();
                        String pointsBalance = summaryDetail.get(2).toString();
                        String pointsEarned = summaryDetail.get(3).toString();
                        String pointsRedeemed = summaryDetail.get(4).toString();

                        LoyaltyDetail detail = new LoyaltyDetail();
                        detail.setProgramName(programName);
                        detail.setPointsBalance(pointsBalance);
                        detail.setPointsEarned(pointsEarned);
                        detail.setPointsRedeemed(pointsRedeemed);

                        loyaltyData.put(rewardProgramID, detail);
                    }
                }
            }
        }
        catch (Exception e)
        {
            logError("ReceiptData.setLoyaltyData error", e);
        }
    }

    public void setLoyaltyDataFromDBV2 (ArrayList<HashMap> loyaltyTxnPointInfo) {

    }

    public void setLoyaltyDataFromDB(ArrayList loyaltyTxnPointInfo, int transTypeID)
    {
        String employeeID = "";

        for (int x = 0; x < loyaltyTxnPointInfo.size(); x++)
        {
            HashMap pointDetail = (HashMap)loyaltyTxnPointInfo.get(x);

            // Get employeeID, if we don't have it already
            if (employeeID.equals(""))
            {
                employeeID = HashMapDataFns.getStringVal(pointDetail, "EMPLOYEEID");
            }

            // Get reward program ID and number of points earned/redeemed
            Integer rewardProgramID = HashMapDataFns.getIntVal(pointDetail, "LOYALTYPROGRAMID");
            String programName = HashMapDataFns.getStringVal(pointDetail, "LOYALTYPROGRAMNAME");
            String pointsRedeemed = "0";
            String pointsEarned = "0";
            int numPoints = HashMapDataFns.getIntVal(pointDetail, "POINTS");
            boolean isVoidOrRefund = (transTypeID == TypeData.TranType.VOID || transTypeID == TypeData.TranType.REFUND);

            if ( (numPoints > 0 && !isVoidOrRefund) || (numPoints < 0 && isVoidOrRefund))
            {
                pointsEarned = new Integer(numPoints).toString();
            }
            else
            {
                pointsRedeemed = new Integer(numPoints).toString();
            }

            if (loyaltyData.containsKey(rewardProgramID))
            {
                // Update existing detail
                int oldNumPointsEarned = new Integer(loyaltyData.get(rewardProgramID).getPointsEarned()).intValue();
                int oldNumPointsRedeemed = new Integer(loyaltyData.get(rewardProgramID).getPointsRedeemed()).intValue();

                int newNumPointsEarned = oldNumPointsEarned + new Integer(pointsEarned).intValue();
                int newNumPointsRedeemed = oldNumPointsRedeemed + new Integer(pointsRedeemed).intValue();

                loyaltyData.get(rewardProgramID).setPointsEarned(new Integer(newNumPointsEarned).toString());
                loyaltyData.get(rewardProgramID).setPointsRedeemed(new Integer(newNumPointsRedeemed).toString());
            }
            else
            {
                // Add new detail
                LoyaltyDetail detail = new LoyaltyDetail();
                detail.setProgramName(programName);
                detail.setPointsEarned(pointsEarned);
                detail.setPointsRedeemed(pointsRedeemed);

                loyaltyData.put(rewardProgramID, detail);
            }
        }

        // If we have an employee ID, get the other loyalty account info
        if (employeeID.equals("") == false)
        {
            ArrayList loyaltyAcctInfo = txnData.getLoyaltyAcctInfo(employeeID);

            if (loyaltyAcctInfo.size() > 0)
            {
                HashMap hmLoyaltyAcctInfo = (HashMap) loyaltyAcctInfo.get(0);

                String name = HashMapDataFns.getStringVal(hmLoyaltyAcctInfo, "NAME");
                String badgeNum = HashMapDataFns.getStringVal(hmLoyaltyAcctInfo, "BADGE");
                String employeeNum = HashMapDataFns.getStringVal(hmLoyaltyAcctInfo, "EMPLOYEENUMBER");

                EmployeeData loyaltyAcct = new EmployeeData();
                loyaltyAcct.setName(name);
                loyaltyAcct.setBadgeNumber(badgeNum);
                loyaltyAcct.setEmployeeNumber(employeeNum);
                loyaltyAcct.setEmployeeID(employeeID);

                txnData.setLoyaltyAcct(loyaltyAcct);
            }
        }
    }

    public void setLoyaltyDataFromTransactionModel(TransactionModel transactionModel) {

        if (transactionModel.getLoyaltyAccount() != null) {
            String employeeID = "";
            //Get the Point balances into a HashMap
            HashMap<Integer, Integer> pointsBalanceHM = new HashMap<>();
            for (LoyaltyPointModel loyaltyPointModelTtl : transactionModel.getLoyaltyAccount().getLoyaltyPoints()) {
                pointsBalanceHM.put(loyaltyPointModelTtl.getLoyaltyProgram().getId(), loyaltyPointModelTtl.getPoints());
            }

            //Points Earned
            for (LoyaltyPointModel loyaltyPointModel : transactionModel.getLoyaltyPointsEarned()) {
                if (employeeID.equals("")) {
                    employeeID = transactionModel.getLoyaltyAccount().getId().toString();
                }

                // Get reward program ID and number of points earned/redeemed
                Integer rewardProgramID = loyaltyPointModel.getLoyaltyProgram().getId();
                String programName = loyaltyPointModel.getLoyaltyProgram().getName();
                String pointsRedeemed = "0";
                String pointsEarned = "0";

                pointsEarned = loyaltyPointModel.getPoints().toString();

                if (loyaltyData.containsKey(rewardProgramID) && !loyaltyData.get(rewardProgramID).getPointsEarned().isEmpty()) {
                    // Update existing detail
                    int oldNumPointsEarned = new Integer(loyaltyData.get(rewardProgramID).getPointsEarned()).intValue();

                    int newNumPointsEarned = oldNumPointsEarned + new Integer(pointsEarned).intValue();

                    loyaltyData.get(rewardProgramID).setPointsEarned(new Integer(newNumPointsEarned).toString());

                    if (pointsBalanceHM.containsKey(rewardProgramID)) {
                        loyaltyData.get(rewardProgramID).setPointsBalance(pointsBalanceHM.get(rewardProgramID).toString());
                    }
                } else {
                    // Add new detail
                    LoyaltyDetail detail = new LoyaltyDetail();
                    detail.setProgramName(programName);
                    detail.setPointsEarned(pointsEarned);
                    detail.setPointsRedeemed(pointsRedeemed);

                    //Set the Points Balance field
                    if (pointsBalanceHM.containsKey(rewardProgramID)) {
                        detail.setPointsBalance(pointsBalanceHM.get(rewardProgramID).toString());
                    }

                    loyaltyData.put(rewardProgramID, detail);
                }
            }

            //Points Redeemed
            for (LoyaltyPointModel loyaltyPointModel : transactionModel.getLoyaltyPointsRedeemed()) {
                if (employeeID.equals("")) {
                    employeeID = transactionModel.getLoyaltyAccount().getId().toString();
                }
                // Get reward program ID and number of points earned/redeemed
                Integer rewardProgramID = loyaltyPointModel.getLoyaltyProgram().getId();
                String programName = loyaltyPointModel.getLoyaltyProgram().getName();
                String pointsRedeemed = "0";
                String pointsEarned = "0";

                pointsRedeemed = loyaltyPointModel.getPoints().toString();

                if (loyaltyData.containsKey(rewardProgramID) && !loyaltyData.get(rewardProgramID).getPointsRedeemed().isEmpty()) {
                    // Update existing detail
                    int oldNumPointsRedeemed = new Integer(loyaltyData.get(rewardProgramID).getPointsRedeemed()).intValue();

                    int newNumPointsRedeemed = oldNumPointsRedeemed + new Integer(pointsRedeemed).intValue();

                    loyaltyData.get(rewardProgramID).setPointsRedeemed(new Integer(newNumPointsRedeemed).toString());

                    //Set the Points Balance field
                    if (pointsBalanceHM.containsKey(rewardProgramID)) {
                        loyaltyData.get(rewardProgramID).setPointsBalance(pointsBalanceHM.get(rewardProgramID).toString());
                    }
                } else {
                    // Add new detail
                    LoyaltyDetail detail = new LoyaltyDetail();
                    detail.setProgramName(programName);
                    detail.setPointsEarned(pointsEarned);
                    detail.setPointsRedeemed(pointsRedeemed);

                    //Set the Points Balance field
                    if (pointsBalanceHM.containsKey(rewardProgramID)) {
                        detail.setPointsBalance(pointsBalanceHM.get(rewardProgramID).toString());
                    }

                    loyaltyData.put(rewardProgramID, detail);
                }
            }

            // If we have an employee ID, get the other loyalty account info
            if (employeeID.equals("") == false) {
                ArrayList loyaltyAcctInfo = txnData.getLoyaltyAcctInfo(employeeID);

                if (transactionModel.getLoyaltyAccount() != null) {

                    String name = ESAPI.encoder().decodeForHTML(transactionModel.getLoyaltyAccount().getName());
                    String badgeNum = transactionModel.getLoyaltyAccount().getBadge();
                    String employeeNum = transactionModel.getLoyaltyAccount().getNumber();

                    EmployeeData loyaltyAcct = new EmployeeData();
                    loyaltyAcct.setName(name);
                    loyaltyAcct.setBadgeNumber(badgeNum);
                    loyaltyAcct.setEmployeeNumber(employeeNum);
                    loyaltyAcct.setEmployeeID(employeeID);

                    txnData.setLoyaltyAcct(loyaltyAcct);
                }
            }
        }
    }

    private void fixNutritionDataIfNeeded(NutritionData nutritionData) {
        if (receiptTypeID == TypeData.ReceiptType.PRINTEDRECEIPT) {
            int maxPluNameLen = 16;

            try {
                if (!nutritionData.getNutritionItemsDB().isEmpty()) {
                    for (NutritionItem nutritionItem : nutritionData.getNutritionItemsDB()) {

                        if (nutritionItem.getProductName().length() > maxPluNameLen) {
                            nutritionItem.setProductName(nutritionItem.getProductName().substring(0, maxPluNameLen));
                            Logger.logMessage("NutritionData- product name truncated", Logger.LEVEL.DEBUG);
                        }
                    }
                }
            } catch (Exception ex) {
                Logger.logMessage("ReceiptData.fixNutritionDataIfNeeded: Could not truncate product name successfully.", Logger.LEVEL.ERROR);
            }
        }
    }

    private void setCurrency(HashMap txnDetail) {
        if (receiptTypeID != TypeData.ReceiptType.POPUPRECEIPT) {
            return;
        }

        String currencyType = CommonAPI.convertModelDetailToString(txnDetail.get("MYQCCURRENCYTYPE"));
        if(currencyType.equals("$*")) {
            currencyType = "$";
        }

        setCurrencyType(currencyType);

        setCurrencyBeforePrice(CommonAPI.convertModelDetailToBoolean(txnDetail.get("MYQCCURRENCYBEFOREPRICE")));
        setDecimalInPrice(CommonAPI.convertModelDetailToBoolean(txnDetail.get("MYQCDECIMALINPRICE")));
    }

    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public DataManager getDataManager()
    {
        return dm;
    }

    public void setDataManager(DataManager dm)
    {
        this.dm = dm;
    }

    public void setPopupVal(boolean newValue)
    {
        popup = newValue;
    }

    public boolean getPopupVal()
    {
        return popup;
    }

    public int getReceiptTypeID()
    {
        return receiptTypeID;
    }

    public void setReceiptTypeID(int newRcptTypeID)
    {
        receiptTypeID = newRcptTypeID;
    }

    public TransactionData getTxnData()
    {
        return txnData;
    }

    public Boolean getPrintTxnBarcode()
    {
        return printTxnBarcode;
    }

    public void setPrintGrossWeight(boolean printGross)
    {
        printGrossWeight = printGross;
    }
    public Boolean getPrintGrossWeight()
    {
        return printGrossWeight;
    }

    public void setPrintNetWeight(boolean printNet)
    {
        printNetWeight = printNet;
    }
    public Boolean getPrintNetWeight()
    {
        return printNetWeight;
    }


    public ArrayList getReceiptHeaders()
    {
        return receiptHeaders;
    }

    public ArrayList getReceiptFooters()
    {
        return receiptFooters;
    }

    public ArrayList getReceiptAcctInfoLines()
    {
        return receiptAcctInfoLines;
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }

    public boolean getPrintOrderNumber()
    {
        return printOrderNumber;
    }

    public boolean getIsMerchantCopy()
    {
        return isMerchantCopy;
    }

    public boolean shouldPrintProductCodes()
    {
        return printProductCodes;
    }

    public KitchenPrinterJobInfo getKpJobInfo()
    {
        return kpJobInfo;
    }

    public NutritionData getNutritionData() { return nutritionData; }

    public HashMap<Integer,LoyaltyDetail> getLoyaltyData()
    {
        return loyaltyData;
    }

    public int getBadgeNumDigitsPrinted()
    {
        return badgeNumDigitsPrinted;
    }

    public boolean getDisplayWellnessIcons() {
        return displayWellnessIcons;
    }

    public void setDisplayWellnessIcons(boolean displayWellnessIcons) {
        this.displayWellnessIcons = displayWellnessIcons;
    }

    public int getPaymentGatewayID()
    {
        return paymentGatewayID;
    }

    public void setPaymentGatewayID(int gatewayID)
    {
        paymentGatewayID = gatewayID;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public boolean isCurrencyBeforePrice() {
        return currencyBeforePrice;
    }

    public void setCurrencyBeforePrice(boolean currencyBeforePrice) {
        this.currencyBeforePrice = currencyBeforePrice;
    }

    public boolean isDecimalInPrice() {
        return decimalInPrice;
    }

    public void setDecimalInPrice(boolean decimalInPrice) {
        this.decimalInPrice = decimalInPrice;
    }

    /**
     * <p>Getter for the printJobInfo field of the {@link ReceiptData}.</p>
     *
     * @return The printJobInfo field of the {@link ReceiptData}.
     */
    public PrintJobInfo getPrintJobInfo () {
        return printJobInfo;
    }

    /**
     * <p>Setter for the printJobInfo field of the {@link ReceiptData}.</p>
     *
     * @param printJobInfo The printJobInfo field of the {@link ReceiptData}.
     */
    public void setPrintJobInfo (PrintJobInfo printJobInfo) {
        this.printJobInfo = printJobInfo;
    }

    /**
     * <p>Getter for the receiptDetails field of the {@link ReceiptData}.</p>
     *
     * @return The receiptDetails field of the {@link ReceiptData}.
     */
    public ArrayList<HashMap> getReceiptDetails () {
        return receiptDetails;
    }

    /**
     * <p>Setter for the receiptDetails field of the {@link ReceiptData}.</p>
     *
     * @param receiptDetails The receiptDetails field of the {@link ReceiptData}.
     */
    public void setReceiptDetails (ArrayList<HashMap> receiptDetails) {
        this.receiptDetails = receiptDetails;
    }

    public com.mmhayes.common.kms.receiptGen.transactionData.TransactionData getTransactionData() {
        return transactionData;
    }

    public void setTransactionData(com.mmhayes.common.kms.receiptGen.transactionData.TransactionData transactionData) {
        this.transactionData = transactionData;
    }

    public boolean getPrintOrderTypeAndTimeMsg()
    {
        return printOrderTypeAndTimeMsg;
    }

    /**
     * <p>Getter for the customerReceiptData field of the {@link ReceiptData}.</p>
     *
     * @return The customerReceiptData field of the {@link ReceiptData}.
     */
    public CustomerReceiptData getCustomerReceiptData () {
        return customerReceiptData;
    }

    /**
     * <p>Setter for the customerReceiptData field of the {@link ReceiptData}.</p>
     *
     * @param customerReceiptData The customerReceiptData field of the {@link ReceiptData}.
     */
    public void setCustomerReceiptData (CustomerReceiptData customerReceiptData) {
        this.customerReceiptData = customerReceiptData;
    }

}
