package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: nyu $: Author of last commit
    $Date: 2020-05-11 19:11:49 -0400 (Mon, 11 May 2020) $: Date of last commit
    $Rev: 11699 $: Revision of last commit
    Notes: Contains information on the terminal from which a transaction was placed that is relevant to rendering a receipt.
*/

import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.TypeData;

import java.util.HashMap;

/**
 * <p>Contains information on the terminal from which a transaction was placed that is relevant to rendering a receipt.</p>
 *
 */
public class TerminalData {

    // private member variables of a TerminalData
    private int terminalID = 0;
    private String name = "";
    private String hostname = "";
    private int terminalTypeID = 0;
    private int terminalModelID = 0;
    private int revenueCenterID = 0;
    private String macAddress = "";
    private boolean remoteOrderTerminal = false;
    private boolean isKioskTerminal = false;
    private String txnNameLabel = "";

    /**
     * <p>Creates a new {@link TerminalData} and sets it's fields based on the data in the given {@link HashMap}.</p>
     *
     * @param hm The {@link HashMap} containing data corresponding to fields of a {@link TerminalData}.
     * @return The created {@link TerminalData}.
     */
    public TerminalData (HashMap hm) {
        this.terminalID = HashMapDataFns.getIntVal(hm, "TERMINALID");
        this.name = HashMapDataFns.getStringVal(hm, "NAME");
        this.hostname = HashMapDataFns.getStringVal(hm, "PASS");
        this.terminalTypeID = HashMapDataFns.getIntVal(hm, "TERMINALTYPEID");
        this.terminalModelID = HashMapDataFns.getIntVal(hm, "TERMINALMODELID");
        this.revenueCenterID = HashMapDataFns.getIntVal(hm, "REVENUECENTERID");
        this.macAddress = HashMapDataFns.getStringVal(hm, "MACADDRESS");
        this.remoteOrderTerminal = HashMapDataFns.getBooleanVal(hm, "REMOTEORDERTERMINAL");
        this.isKioskTerminal = ((terminalTypeID == TypeData.TerminalType.MM_HAYES_POS) && (terminalModelID == TypeData.TerminalModelType.QC_POS_KIOSK));
        this.txnNameLabel = HashMapDataFns.getStringVal(hm, "PATRANSNAMELABEL");
    }

    /**
     * <p>Getter for the terminalID field of the {@link TerminalData}.</p>
     *
     * @return The terminalID field of the {@link TerminalData}.
     */
    public int getTerminalID () {
        return terminalID;
    }

    /**
     * <p>Setter for the terminalID field of the {@link TerminalData}.</p>
     *
     * @param terminalID The terminalID field of the {@link TerminalData}.
     */
    public void setTerminalID (int terminalID) {
        this.terminalID = terminalID;
    }

    /**
     * <p>Getter for the name field of the {@link TerminalData}.</p>
     *
     * @return The name field of the {@link TerminalData}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link TerminalData}.</p>
     *
     * @param name The name field of the {@link TerminalData}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the hostname field of the {@link TerminalData}.</p>
     *
     * @return The hostname field of the {@link TerminalData}.
     */
    public String getHostname () {
        return hostname;
    }

    /**
     * <p>Setter for the hostname field of the {@link TerminalData}.</p>
     *
     * @param hostname The hostname field of the {@link TerminalData}.
     */
    public void setHostname (String hostname) {
        this.hostname = hostname;
    }

    /**
     * <p>Getter for the terminalTypeID field of the {@link TerminalData}.</p>
     *
     * @return The terminalTypeID field of the {@link TerminalData}.
     */
    public int getTerminalTypeID () {
        return terminalTypeID;
    }

    /**
     * <p>Setter for the terminalTypeID field of the {@link TerminalData}.</p>
     *
     * @param terminalTypeID The terminalTypeID field of the {@link TerminalData}.
     */
    public void setTerminalTypeID (int terminalTypeID) {
        this.terminalTypeID = terminalTypeID;
    }

    /**
     * <p>Getter for the terminalModelID field of the {@link TerminalData}.</p>
     *
     * @return The terminalModelID field of the {@link TerminalData}.
     */
    public int getTerminalModelID () {
        return terminalModelID;
    }

    /**
     * <p>Setter for the terminalModelID field of the {@link TerminalData}.</p>
     *
     * @param terminalModelID The terminalModelID field of the {@link TerminalData}.
     */
    public void setTerminalModelID (int terminalModelID) {
        this.terminalModelID = terminalModelID;
    }

    /**
     * <p>Getter for the revenueCenterID field of the {@link TerminalData}.</p>
     *
     * @return The revenueCenterID field of the {@link TerminalData}.
     */
    public int getRevenueCenterID () {
        return revenueCenterID;
    }

    /**
     * <p>Setter for the revenueCenterID field of the {@link TerminalData}.</p>
     *
     * @param revenueCenterID The revenueCenterID field of the {@link TerminalData}.
     */
    public void setRevenueCenterID (int revenueCenterID) {
        this.revenueCenterID = revenueCenterID;
    }

    /**
     * <p>Getter for the macAddress field of the {@link TerminalData}.</p>
     *
     * @return The macAddress field of the {@link TerminalData}.
     */
    public String getMacAddress () {
        return macAddress;
    }

    /**
     * <p>Setter for the macAddress field of the {@link TerminalData}.</p>
     *
     * @param macAddress The macAddress field of the {@link TerminalData}.
     */
    public void setMacAddress (String macAddress) {
        this.macAddress = macAddress;
    }

    /**
     * <p>Getter for the remoteOrderTerminal field of the {@link TerminalData}.</p>
     *
     * @return The remoteOrderTerminal field of the {@link TerminalData}.
     */
    public boolean getRemoteOrderTerminal () {
        return remoteOrderTerminal;
    }

    /**
     * <p>Setter for the remoteOrderTerminal field of the {@link TerminalData}.</p>
     *
     * @param remoteOrderTerminal The remoteOrderTerminal field of the {@link TerminalData}.
     */
    public void setRemoteOrderTerminal (boolean remoteOrderTerminal) {
        this.remoteOrderTerminal = remoteOrderTerminal;
    }

    /**
     * <p>Getter for the isKioskTerminal field of the {@link TerminalData}.</p>
     *
     * @return The isKioskTerminal field of the {@link TerminalData}.
     */
    public boolean getIsKioskTerminal () {
        return isKioskTerminal;
    }

    /**
     * <p>Setter for the isKioskTerminal field of the {@link TerminalData}.</p>
     *
     * @param isKioskTerminal The isKioskTerminal field of the {@link TerminalData}.
     */
    public void setIsKioskTerminal (boolean isKioskTerminal) {
        this.isKioskTerminal = isKioskTerminal;
    }

}