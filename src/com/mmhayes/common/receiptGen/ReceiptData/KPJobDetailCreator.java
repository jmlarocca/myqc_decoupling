package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-12 15:23:14 -0400 (Tue, 12 May 2020) $: Date of last commit
    $Rev: 11713 $: Revision of last commit
    Notes: Helper class to build a print job detail.
*/

import com.mmhayes.common.utils.Logger;

/**
 * Helper class to build a print job detail.
 * NOTE: this class uses the builder pattern.
 *
 */
public class KPJobDetailCreator {

    // private member variables of a KPJobDetailCreator
    private final int paTransLineItemID; // optional
    private final String lineDetail; // optional
    private final int printerID; // optional
    private final int paTransLineItemModID; // optional
    private final String printerName; // optional
    private final boolean printerPrintsOnlyNew; // optional
    private final int printStatusID; // optional
    private final int printerHostID; // optional
    private final int printerTypeID; // optional
    private final int papluID; // optional
    private final double quantity; // optional
    private final boolean isModifier; // optional
    private final boolean isExpeditor; // optional
    private final boolean isRemote; // optional
    private final boolean noExpeditorTktsUnlessProdAdded; // optional
    private final String productName; // optional
    private final int printerHardwareTypeID; // optional
    private final String hideStation; // optional

    /**
     * Constructor for a KPJobDetailCreator.
     *
     * @param builder {@link KPJobDetailBuilder} Builder class for a KPJobDetailCreator.
     */
    public KPJobDetailCreator (KPJobDetailBuilder builder) {
        this.paTransLineItemID = builder.paTransLineItemID;
        this.lineDetail = builder.lineDetail;
        this.printerID = builder.printerID;
        this.paTransLineItemModID = builder.paTransLineItemModID;
        this.printerName = builder.printerName;
        this.printerPrintsOnlyNew = builder.printerPrintsOnlyNew;
        this.printStatusID = builder.printStatusID;
        this.printerHostID = builder.printerHostID;
        this.printerTypeID = builder.printerTypeID;
        this.papluID = builder.papluID;
        this.quantity = builder.quantity;
        this.isModifier = builder.isModifier;
        this.isExpeditor = builder.isExpeditor;
        this.isRemote = builder.isRemote;
        this.noExpeditorTktsUnlessProdAdded = builder.noExpeditorTktsUnlessProdAdded;
        this.productName = builder.productName;
        this.printerHardwareTypeID = builder.printerHardwareTypeID;
        this.hideStation = builder.hideStation;
    }

    /**
     * Utility method to convert a KPJobDetailCreator to a KPJobDetail.
     *
     * @return {@link KPJobDetail} The converted KPJobDetailCreator.
     */
    public KPJobDetail convertCreatorToKPJobDetail () {
        KPJobDetail kpJobDetail = new KPJobDetail();

        try {
            if (this.paTransLineItemID > 0) {
                kpJobDetail.setPaTransLineItemID(this.paTransLineItemID);
            }
            if ((this.lineDetail != null) && (!this.lineDetail.isEmpty())) {
                kpJobDetail.setLineDetail(this.lineDetail);
            }
            if (this.printerID > 0) {
                kpJobDetail.setPrinterID(this.printerID);
            }
            if (this.paTransLineItemModID > 0) {
                kpJobDetail.setPaTransLineItemModID(this.paTransLineItemModID);
            }
            if ((this.printerName != null) && (!this.printerName.isEmpty())) {
                kpJobDetail.setPrinterName(this.printerName);
            }
            kpJobDetail.setPrinterPrintsOnlyNew(this.printerPrintsOnlyNew);
            if (this.printStatusID > 0) {
                kpJobDetail.setPrintStatusID(this.printStatusID);
            }
            if (this.printerHostID > 0) {
                kpJobDetail.setPrinterHostID(this.printerHostID);
            }
            if (this.printerTypeID > 0) {
                kpJobDetail.setPrinterTypeID(this.printerTypeID);
            }
            if (this.papluID > 0) {
                kpJobDetail.setPAPluID(this.papluID);
            }
            if (this.quantity > 0.0d) {
                kpJobDetail.setQuantity(this.quantity);
            }
            kpJobDetail.setIsModifier(this.isModifier);
            kpJobDetail.setIsExpeditor(this.isExpeditor);
            kpJobDetail.setIsRemote(this.isRemote);
            kpJobDetail.setNoExpeditorTktsUnlessProdAdded(this.noExpeditorTktsUnlessProdAdded);
            if ((this.productName != null) && (!this.productName.isEmpty())) {
                kpJobDetail.setProductName(this.productName);
            }
            if (this.printerHardwareTypeID > 0) {
                kpJobDetail.setPrinterHardwareTypeID(this.printerHardwareTypeID);
            }
            if ((this.hideStation != null) && (!this.hideStation.isEmpty())) {
                kpJobDetail.setHideStation(this.hideStation);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to convert the KPJobDetailCreator to a KPJobDetail in " +
                    "KPJobDetailCreator.convertCreatorToKPJobDetail", Logger.LEVEL.ERROR);
        }

        return kpJobDetail;
    }

    /**
     * Builder class for a KPJobDetailCreator.
     * 
     */
    public static class KPJobDetailBuilder {
        
        // private member variables of a KPJobDetailBuilder.
        private int paTransLineItemID;
        private String lineDetail;
        private int printerID;
        private int paTransLineItemModID; 
        private String printerName; 
        private boolean printerPrintsOnlyNew; 
        private int printStatusID; 
        private int printerHostID;
        private int printerTypeID;
        private int papluID; 
        private double quantity; 
        private boolean isModifier; 
        private boolean isExpeditor; 
        private boolean isRemote;
        private boolean noExpeditorTktsUnlessProdAdded;
        private String productName; 
        private int printerHardwareTypeID; 
        private String hideStation;

        /**
         * Constructor for a KPJobDetailBuilder.
         *
         */
        public KPJobDetailBuilder () {}

        /**
         * Adds an optional paTransLineItemID to the KPJobDetailBuilder.
         *
         * @param paTransLineItemID ID of the transaction line item this print job pertains to.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the paTransLineItemID field included.
         */
        public KPJobDetailBuilder paTransLineItemID (int paTransLineItemID) {
            this.paTransLineItemID = paTransLineItemID;
            return this;
        }

        /**
         * Adds an optional lineDetail to the KPJobDetailBuilder.
         *
         * @param lineDetail {@link String} The to be print for this print job.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the lineDetail field included.
         */
        public KPJobDetailBuilder lineDetail (String lineDetail) {
            this.lineDetail = lineDetail;
            return this;
        }

        /**
         * Adds an optional printerID to the KPJobDetailBuilder.
         *
         * @param printerID ID of the printer this print job should be executed on.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the printerID field included.
         */
        public KPJobDetailBuilder printerID (int printerID) {
            this.printerID = printerID;
            return this;
        }

        /**
         * Adds an optional paTransLineItemModID to the KPJobDetailBuilder.
         *
         * @param paTransLineItemModID The ID of the modifier within the transaction line item.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the paTransLineItemModID field included.
         */
        public KPJobDetailBuilder paTransLineItemModID (int paTransLineItemModID) {
            this.paTransLineItemModID = paTransLineItemModID;
            return this;
        }
        
        /**
         * Adds an optional printerName to the KPJobDetailBuilder.
         *
         * @param printerName {@link String} Name of the printer this print job should be executed on.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the printerName field included.
         */
        public KPJobDetailBuilder printerName (String printerName) {
            this.printerName = printerName;
            return this;
        }

        /**
         * Adds an optional printerPrintsOnlyNew to the KPJobDetailBuilder.
         *
         * @param printerPrintsOnlyNew Whether or not the printer only prints new products.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the printerPrintsOnlyNew field included.
         */
        public KPJobDetailBuilder printerPrintsOnlyNew (boolean printerPrintsOnlyNew) {
            this.printerPrintsOnlyNew = printerPrintsOnlyNew;
            return this;
        }

        /**
         * Adds an optional printStatusID to the KPJobDetailBuilder.
         *
         * @param printStatusID Status of this print job.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the printStatusID field included.
         */
        public KPJobDetailBuilder printStatusID (int printStatusID) {
            this.printStatusID = printStatusID;
            return this;
        }

        /**
         * Adds an optional printerHostID to the KPJobDetailBuilder.
         *
         * @param printerHostID ID of the printer host responsible for submitting this print job.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the printerHostID field included.
         */
        public KPJobDetailBuilder printerHostID (int printerHostID) {
            this.printerHostID = printerHostID;
            return this;
        }

        /**
         * Adds an optional printerTypeID to the KPJobDetailBuilder.
         *
         * @param printerTypeID ID of the printer host responsible for submitting this print job.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the printerTypeID field included.
         */
        public KPJobDetailBuilder printerTypeID (int printerTypeID) {
            this.printerTypeID = printerTypeID;
            return this;
        }

        /**
         * Adds an optional papluID to the KPJobDetailBuilder.
         *
         * @param papluID ID of the product that is being printed in this print job.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the papluID field included.
         */
        public KPJobDetailBuilder papluID (int papluID) {
            this.papluID = papluID;
            return this;
        }

        /**
         * Adds an optional quantity to the KPJobDetailBuilder.
         *
         * @param quantity Amount of the product that is in this print job.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the quantity field included.
         */
        public KPJobDetailBuilder quantity (double quantity) {
            this.quantity = quantity;
            return this;
        }

        /**
         * Adds an optional isModifier to the KPJobDetailBuilder.
         *
         * @param isModifier Whether or not the product in this print job is a modifier.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the isModifier field included.
         */
        public KPJobDetailBuilder isModifier (boolean isModifier) {
            this.isModifier = isModifier;
            return this;
        }

        /**
         * Adds an optional isExpeditor to the KPJobDetailBuilder.
         *
         * @param isExpeditor Whether or not the product in this print job should be printed on an expeditor printer.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the isExpeditor field included.
         */
        public KPJobDetailBuilder isExpeditor (boolean isExpeditor) {
            this.isExpeditor = isExpeditor;
            return this;
        }

        /**
         * Adds an optional isRemote to the KPJobDetailBuilder.
         *
         * @param isRemote Whether or not the product in this print job should be printed on a remote order printer.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the isRemote field included.
         */
        public KPJobDetailBuilder isRemote (boolean isRemote) {
            this.isRemote = isRemote;
            return this;
        }

        /**
         * Adds an optional noExpeditorTktsUnlessProdAdded to the KPJobDetailBuilder.
         *
         * @param noExpeditorTktsUnlessProdAdded Whether or not the expeditor printer will only print products when
         *         new expeditor products are added.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the noExpeditorTktsUnlessProdAdded field included.
         */
        public KPJobDetailBuilder noExpeditorTktsUnlessProdAdded (boolean noExpeditorTktsUnlessProdAdded) {
            this.noExpeditorTktsUnlessProdAdded = noExpeditorTktsUnlessProdAdded;
            return this;
        }

        /**
         * Adds an optional productName to the KPJobDetailBuilder.
         *
         * @param productName {@link String} Name of the product that is being printed in this print job.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the productName field included.
         */
        public KPJobDetailBuilder productName (String productName) {
            this.productName = productName;
            return this;
        }

        /**
         * Adds an optional printerHardwareTypeID to the KPJobDetailBuilder.
         *
         * @param printerHardwareTypeID Type of hardware the printer for this print job is; KP, KDS, etc...
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the printerHardwareTypeID field included.
         */
        public KPJobDetailBuilder printerHardwareTypeID (int printerHardwareTypeID) {
            this.printerHardwareTypeID = printerHardwareTypeID;
            return this;
        }

        /**
         * Adds an optional hideStation to the KPJobDetailBuilder.
         *
         * @param hideStation {@link String} ID of the KDS station on which the product in this print job should be hidden.
         * @return {@link KPJobDetailBuilder} The KPJobDetailBuilder with the hideStation field included.
         */
        public KPJobDetailBuilder hideStation (String hideStation) {
            this.hideStation = hideStation;
            return this;
        }

        /**
         * Builds and returns the KPJobDetailCreator.
         *
         * @return {@link KPJobDetailCreator} The newly constructed KPJobDetailCreator.
         */
        public KPJobDetailCreator build () {
            return (new KPJobDetailCreator(this));
        }

    }

}
