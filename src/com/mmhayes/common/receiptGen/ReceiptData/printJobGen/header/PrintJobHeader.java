package com.mmhayes.common.receiptGen.ReceiptData.printJobGen.header;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-27 08:36:18 -0400 (Wed, 27 May 2020) $: Date of last commit
    $Rev: 11819 $: Revision of last commit
    Notes: Represents the header information for a print job.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.TransactionHeaderData;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * <p>Represents the header information for a print job.</p>
 *
 */
public class PrintJobHeader {

    // private member variables of a PrintJobHeader
    private static final DateTimeFormatter TRANS_DATE_TIME_NO_AM_PM = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private BigDecimal transactionID = null;
    private boolean onlineOrder = false;
    private LocalDateTime transactionDate = null;
    private LocalDateTime queueTime = null;
    private int printStatusID = 0;
    private int terminalID = 0;
    private int paOrderTypeID = 0;
    private String personName = "";
    private String phone = "";
    private String transComment = "";
    private String pickUpDeliveryNote = "";
    private String transName = "";
    private String transNameLabel = "";
    private LocalDateTime estimatedOrderTime = null;
    private String orderNum = "";
    private int transTypeID = 0;
    private String prevOrderNums = "";
    private int kmsOrderStatusID = 0;

    /**
     * <p>Builds and returns a {@link PrintJobHeader} built from the given {@link TransactionHeaderData}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param txnHeaderData The {@link TransactionHeaderData} to use to build the {@link PrintJobHeader}.
     * @return The {@link PrintJobHeader} built from the given {@link TransactionHeaderData}.
     */
    public static PrintJobHeader buildFromTxnHeaderData (String log, DataManager dataManager, TransactionHeaderData txnHeaderData) {
        PrintJobHeader printJobHeader = null;

        try {
            if (txnHeaderData != null) {
                printJobHeader = new PrintJobHeader();
                printJobHeader.transactionID = txnHeaderData.getPaTransactionID();
                printJobHeader.onlineOrder = txnHeaderData.getOnlineOrder();
                printJobHeader.transactionDate = txnHeaderData.getTransactionDate();
                printJobHeader.queueTime = txnHeaderData.getQueueTime();
                printJobHeader.printStatusID = PrintJobStatusType.WAITING.getTypeID();
                printJobHeader.terminalID = txnHeaderData.getTerminalID();
                int orderTypeID = txnHeaderData.getOrderTypeID();
                if (orderTypeID <= 0) {
                    // default to normal sale if the order type is unknown
                    orderTypeID = TypeData.OrderType.NORMAL_SALE;
                }
                printJobHeader.paOrderTypeID = orderTypeID;
                printJobHeader.personName = txnHeaderData.getPersonName();
                String txnPhone = txnHeaderData.getPhone();
                String pqPhone = txnHeaderData.getPrinterQueuePhone();
                printJobHeader.phone = sanitizePhoneNumber(log, (StringFunctions.stringHasContent(txnPhone) ? txnPhone : pqPhone), true);
                String txnComment = txnHeaderData.getTransComment();
                String pqComment = txnHeaderData.getPrinterQueueComment();
                printJobHeader.transComment = StringFunctions.stringHasContent(txnComment) ? txnComment : pqComment;
                printJobHeader.pickUpDeliveryNote = txnHeaderData.getPickupDeliveryNote();
                printJobHeader.transName = txnHeaderData.getTransName();
                printJobHeader.transNameLabel = txnHeaderData.getPaTransNameLabel();
                printJobHeader.estimatedOrderTime = txnHeaderData.getEstimatedOrderTime();
                String txnOrderNum = txnHeaderData.getOrderNum();
                String pqOrderNum = txnHeaderData.getPrinterQueueOrderNum();
                printJobHeader.orderNum = StringFunctions.stringHasContent(txnOrderNum) ? txnOrderNum : pqOrderNum;
                printJobHeader.transTypeID = txnHeaderData.getTransTypeID();
                HashMap<Integer, String> prevTxnInfo = getPreviousTxnInfo(log, dataManager, txnHeaderData.getPaTransactionID());
                if (!DataFunctions.isEmptyMap(prevTxnInfo)) {
                    printJobHeader.prevOrderNums = StringFunctions.buildStringFromList(new ArrayList<>(prevTxnInfo.values()), ",");
                }
                else {
                    printJobHeader.prevOrderNums = "";
                }
                printJobHeader.kmsOrderStatusID = txnHeaderData.getKmsOrderStatusID();

                // if this transaction was made on a kiosk with the "Send Orders Directly to Printer Host" setting enabled then the pickup/delivery note needs to be built manually
                if ((txnHeaderData.getPosKioskTerminalID() > 0) && (txnHeaderData.getRecordTransactionsLocally())) {
                    String pickupDeliveryNote =
                            buildPickupDeliveryNote(log, txnHeaderData.getOrderTypeID(), txnHeaderData.getTransactionDate(), txnHeaderData.getPrepTime(), txnHeaderData.getDiningOptionName());
                    if (StringFunctions.stringHasContent(pickupDeliveryNote)) {
                        printJobHeader.pickUpDeliveryNote = pickupDeliveryNote;
                    }
                }
            }
            else {
                Logger.logMessage("No transaction header data passed to PrintJobHeader.buildFromTxnHeaderData, unable to create a print job header!", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // determine the ID of the transaction the print job is for
            BigDecimal transactionID = null;
            if ((txnHeaderData != null) && (txnHeaderData.getPaTransactionID() != null)) {
                transactionID = txnHeaderData.getPaTransactionID();
            }
            Logger.logMessage(String.format("There was a problem trying to create a print job header for the transaction with an ID " +
                    "of %s from the given transaction header data in PrintJobHeader.buildFromTxnHeaderData",
                    Objects.toString((transactionID != null ? transactionID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return printJobHeader;
    }

    /**
     * <p>Validates the given phone number {@link String}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param phone The phone number {@link String} to validate.
     * @param addUSCode Whether or not to add the US country code to the given phone number {@link String}.
     * @return The validated phone number {@link String}.
     */
    public static String sanitizePhoneNumber (String log, String phone, boolean addUSCode) {
        String validatedPhoneNumber = "";

        try {
            if (StringFunctions.stringHasContent(phone)) {
                // remove any non numeric characters from the phone number
                validatedPhoneNumber = phone.replaceAll("[^0-9]", "");

                // append the US country code
                if ((addUSCode) && (StringFunctions.stringHasContent(validatedPhoneNumber)) && (validatedPhoneNumber.length() == 10)) {
                    validatedPhoneNumber = "1"+validatedPhoneNumber;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to validate the phone number %s in PrintJobHeader.sanitizePhoneNumber",
                    Objects.toString(phone, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return validatedPhoneNumber;
    }

    /**
     * <p>Builds the pickup delivery note for a transaction that was made on a kiosk with the "Send Orders Directly to Printer Host" setting enabled.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param orderTypeID ID of for the type of order (pickup or delivery).
     * @param transactionDate Date of the transaction as a {@link LocalDateTime}.
     * @param prepTimeInMinutes Approximately how long it will take to prepare the order in minutes.
     * @param diningOptionName The name {@link String} of the dining option in the transaction if there is one.
     * @return The manually crafted pickup/delivery note {@link String}.
     */
    @SuppressWarnings("Duplicates")
    public static String buildPickupDeliveryNote (String log, int orderTypeID, LocalDateTime transactionDate, int prepTimeInMinutes, String diningOptionName) {
        String pickupDeliveryNote = "";

        try {
            // determine what time the order will be ready
            LocalDateTime readyTime = null;
            if ((transactionDate != null) && (prepTimeInMinutes > 0)) {
                readyTime = transactionDate.plusMinutes(prepTimeInMinutes);
            }
            else if ((transactionDate != null) && (prepTimeInMinutes <= 0)) {
                readyTime = transactionDate;
            }

            if (readyTime != null) {
                // format the date and use AM/PM at the end
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("h:mm a");

                // DELIVERY
                if (orderTypeID == 2) {
                    pickupDeliveryNote += "Has to be delivered at "+readyTime.format(formatter)+".";
                }
                // PICKUP
                else if (orderTypeID == 3) {
                    pickupDeliveryNote += "Will be picked up at "+readyTime.format(formatter)+".";
                }

                // if there is a dining option add it to the pickup/delivery note
                if (StringFunctions.stringHasContent(diningOptionName)) {
                    pickupDeliveryNote += " \\n For "+diningOptionName+".";
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build the pickup/delivery note for the transaction " +
                    "made on a kiosk with the \"Send Orders Directly to Printer Host\" setting enabled given the params: ORDERTYPEID: %s, " +
                    "TRANSACTIONDATE: %s, PREPTIMEINMINUTES: %s, DININGOPTIONNAME: %s in PrintJobHeader.buildPickupDeliveryNote",
                    Objects.toString(orderTypeID, "N/A"),
                    Objects.toString(transactionDate, "N/A"),
                    Objects.toString(prepTimeInMinutes, "N/A"),
                    Objects.toString(diningOptionName, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return pickupDeliveryNote;
    }

    /**
     * <p>Gets transaction IDs and order numbers that were part of the open transaction.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param transactionID ID of the transaction to cheack as a {@link BigDecimal}.
     * @return A {@link HashMap} whose {@link Integer} key is a transaction ID within the open transaction and whose {@link String} value is an order number within the open transaction.
     */
    @SuppressWarnings("unchecked")
    public static HashMap<Integer, String> getPreviousTxnInfo (String log, DataManager dataManager, BigDecimal transactionID) {
        HashMap<Integer, String> previousTxnInfo = new HashMap<>();

        try {
            // make sure the transaction ID is valid
            if (transactionID == null) {
                Logger.logMessage("Transaction ID can't be null in PrintJobHeader.getPreviousTxnInfo, unable to determine " +
                        "any transaction IDs or order numbers within an open transaction!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure the DataManager is valid
            if (dataManager == null) {
                Logger.logMessage("DataManager can't be null in PrintJobHeader.getPreviousTxnInfo, unable to determine " +
                        "any transaction IDs or order numbers within an open transaction!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // query the database to get transaction IDs and order numbers within the open transaction
            ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(dataManager.parameterizedExecuteQuery("data.ReceiptGen.GetPrevTxnInfo", new Object[]{transactionID.toPlainString()}, true));
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                for (HashMap hm : queryRes) {
                    int prevTxnID = HashMapDataFns.getIntVal(hm, "PATRANSACTIONID");
                    String prevOrderNum = HashMapDataFns.getStringVal(hm, "ORDERNUM");
                    previousTxnInfo.put(prevTxnID, prevOrderNum);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to get transaction IDs and order numbers within the transaction with an ID of %s in PrintJobHeader.getPreviousTxnInfo",
                    Objects.toString((transactionID != null ? transactionID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return previousTxnInfo;
    }

    /**
     * <p>Overridden toString() method for a {@link PrintJobHeader}.</p>
     *
     * @return A {@link String} representation of this {@link PrintJobHeader}.
     */
    @Override
    public String toString () {

        return String.format("TRANSACTIONID: %s, ONLINEORDER: %s, TRANSACTIONDATE: %s, QUEUETIME: %s, PRINTSTATUSID: %s, TERMINALID: %s, " +
                "PAORDERTYPEID: %s, PERSONNAME: %s, PHONE: %s, TRANSCOMMENT: %s, PICKUPDELIVERYNOTE: %s, TRANSNAME: %s, TRANSNAMELABEL: %s, " +
                "ESTIMATEDORDERTIME: %s, ORDERNUM: %s, TRANSTYPEID: %s, PREVORDERNUMS: %s, KMSORDERSTATUSID: %s",
                Objects.toString((((transactionID != null) && (StringFunctions.stringHasContent(transactionID.toPlainString()))) ? transactionID.toPlainString() : "N/A"), "N/A"),
                Objects.toString(onlineOrder, "N/A"),
                Objects.toString((((transactionDate != null) && (StringFunctions.stringHasContent(transactionDate.format(TRANS_DATE_TIME_NO_AM_PM)))) ? transactionDate.format(TRANS_DATE_TIME_NO_AM_PM) : "N/A"), "N/A"),
                Objects.toString((((queueTime != null) && (StringFunctions.stringHasContent(queueTime.format(TRANS_DATE_TIME_NO_AM_PM)))) ? queueTime.format(TRANS_DATE_TIME_NO_AM_PM) : "N/A"), "N/A"),
                Objects.toString((printStatusID != -1 ? printStatusID : "N/A"), "N/A"),
                Objects.toString((terminalID != -1 ? terminalID : "N/A"), "N/A"),
                Objects.toString((paOrderTypeID != -1 ? paOrderTypeID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(personName) ? personName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(phone) ? phone : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transComment) ? transComment : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(pickUpDeliveryNote) ? pickUpDeliveryNote : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transName) ? transName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transNameLabel) ? transNameLabel : "N/A"), "N/A"),
                Objects.toString((((estimatedOrderTime!= null) && (StringFunctions.stringHasContent(estimatedOrderTime.format(TRANS_DATE_TIME_NO_AM_PM)))) ? estimatedOrderTime.format(TRANS_DATE_TIME_NO_AM_PM) : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(orderNum) ? orderNum : "N/A"), "N/A"),
                Objects.toString((transTypeID != -1 ? transTypeID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(prevOrderNums) ? prevOrderNums : "N/A"), "N/A"),
                Objects.toString((kmsOrderStatusID != -1 ? kmsOrderStatusID : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link PrintJobHeader}.
     * Two {@link PrintJobHeader} are defined as being equal if they have the same transactionID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link PrintJobHeader}.
     * @return Whether or not the {@link Object} is equal to this {@link PrintJobHeader}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a PrintJobHeader and check if the obj's transactionID is equal to this PrintJobHeader's transactionID
        PrintJobHeader printJobHeader = ((PrintJobHeader) obj);
        return Objects.equals(printJobHeader.transactionID, transactionID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link PrintJobHeader}.</p>
     *
     * @return The unique hash code for this {@link PrintJobHeader}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(transactionID);
    }

    /**
     * <p>Getter for the transactionID field of the {@link PrintJobHeader}.</p>
     *
     * @return The transactionID field of the {@link PrintJobHeader}.
     */
    public BigDecimal getTransactionID () {
        return transactionID;
    }

    /**
     * <p>Setter for the transactionID field of the {@link PrintJobHeader}.</p>
     *
     * @param transactionID The transactionID field of the {@link PrintJobHeader}.
     */
    public void setTransactionID (BigDecimal transactionID) {
        this.transactionID = transactionID;
    }

    /**
     * <p>Getter for the onlineOrder field of the {@link PrintJobHeader}.</p>
     *
     * @return The onlineOrder field of the {@link PrintJobHeader}.
     */
    public boolean getOnlineOrder () {
        return onlineOrder;
    }

    /**
     * <p>Setter for the onlineOrder field of the {@link PrintJobHeader}.</p>
     *
     * @param onlineOrder The onlineOrder field of the {@link PrintJobHeader}.
     */
    public void setOnlineOrder (boolean onlineOrder) {
        this.onlineOrder = onlineOrder;
    }

    /**
     * <p>Getter for the transactionDate field of the {@link PrintJobHeader}.</p>
     *
     * @return The transactionDate field of the {@link PrintJobHeader}.
     */
    public LocalDateTime getTransactionDate () {
        return transactionDate;
    }

    /**
     * <p>Setter for the transactionDate field of the {@link PrintJobHeader}.</p>
     *
     * @param transactionDate The transactionDate field of the {@link PrintJobHeader}.
     */
    public void setTransactionDate (LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * <p>Getter for the queueTime field of the {@link PrintJobHeader}.</p>
     *
     * @return The queueTime field of the {@link PrintJobHeader}.
     */
    public LocalDateTime getQueueTime () {
        return queueTime;
    }

    /**
     * <p>Setter for the queueTime field of the {@link PrintJobHeader}.</p>
     *
     * @param queueTime The queueTime field of the {@link PrintJobHeader}.
     */
    public void setQueueTime (LocalDateTime queueTime) {
        this.queueTime = queueTime;
    }

    /**
     * <p>Getter for the printStatusID field of the {@link PrintJobHeader}.</p>
     *
     * @return The printStatusID field of the {@link PrintJobHeader}.
     */
    public int getPrintStatusID () {
        return printStatusID;
    }

    /**
     * <p>Setter for the printStatusID field of the {@link PrintJobHeader}.</p>
     *
     * @param printStatusID The printStatusID field of the {@link PrintJobHeader}.
     */
    public void setPrintStatusID (int printStatusID) {
        this.printStatusID = printStatusID;
    }

    /**
     * <p>Getter for the terminalID field of the {@link PrintJobHeader}.</p>
     *
     * @return The terminalID field of the {@link PrintJobHeader}.
     */
    public int getTerminalID () {
        return terminalID;
    }

    /**
     * <p>Setter for the terminalID field of the {@link PrintJobHeader}.</p>
     *
     * @param terminalID The terminalID field of the {@link PrintJobHeader}.
     */
    public void setTerminalID (int terminalID) {
        this.terminalID = terminalID;
    }

    /**
     * <p>Getter for the paOrderTypeID field of the {@link PrintJobHeader}.</p>
     *
     * @return The paOrderTypeID field of the {@link PrintJobHeader}.
     */
    public int getPaOrderTypeID () {
        return paOrderTypeID;
    }

    /**
     * <p>Setter for the paOrderTypeID field of the {@link PrintJobHeader}.</p>
     *
     * @param paOrderTypeID The paOrderTypeID field of the {@link PrintJobHeader}.
     */
    public void setPaOrderTypeID (int paOrderTypeID) {
        this.paOrderTypeID = paOrderTypeID;
    }

    /**
     * <p>Getter for the personName field of the {@link PrintJobHeader}.</p>
     *
     * @return The personName field of the {@link PrintJobHeader}.
     */
    public String getPersonName () {
        return personName;
    }

    /**
     * <p>Setter for the personName field of the {@link PrintJobHeader}.</p>
     *
     * @param personName The personName field of the {@link PrintJobHeader}.
     */
    public void setPersonName (String personName) {
        this.personName = personName;
    }

    /**
     * <p>Getter for the phone field of the {@link PrintJobHeader}.</p>
     *
     * @return The phone field of the {@link PrintJobHeader}.
     */
    public String getPhone () {
        return phone;
    }

    /**
     * <p>Setter for the phone field of the {@link PrintJobHeader}.</p>
     *
     * @param phone The phone field of the {@link PrintJobHeader}.
     */
    public void setPhone (String phone) {
        this.phone = phone;
    }

    /**
     * <p>Getter for the transComment field of the {@link PrintJobHeader}.</p>
     *
     * @return The transComment field of the {@link PrintJobHeader}.
     */
    public String getTransComment () {
        return transComment;
    }

    /**
     * <p>Setter for the transComment field of the {@link PrintJobHeader}.</p>
     *
     * @param transComment The transComment field of the {@link PrintJobHeader}.
     */
    public void setTransComment (String transComment) {
        this.transComment = transComment;
    }

    /**
     * <p>Getter for the pickUpDeliveryNote field of the {@link PrintJobHeader}.</p>
     *
     * @return The pickUpDeliveryNote field of the {@link PrintJobHeader}.
     */
    public String getPickUpDeliveryNote () {
        return pickUpDeliveryNote;
    }

    /**
     * <p>Setter for the pickUpDeliveryNote field of the {@link PrintJobHeader}.</p>
     *
     * @param pickUpDeliveryNote The pickUpDeliveryNote field of the {@link PrintJobHeader}.
     */
    public void setPickUpDeliveryNote (String pickUpDeliveryNote) {
        this.pickUpDeliveryNote = pickUpDeliveryNote;
    }

    /**
     * <p>Getter for the transName field of the {@link PrintJobHeader}.</p>
     *
     * @return The transName field of the {@link PrintJobHeader}.
     */
    public String getTransName () {
        return transName;
    }

    /**
     * <p>Setter for the transName field of the {@link PrintJobHeader}.</p>
     *
     * @param transName The transName field of the {@link PrintJobHeader}.
     */
    public void setTransName (String transName) {
        this.transName = transName;
    }

    /**
     * <p>Getter for the transNameLabel field of the {@link PrintJobHeader}.</p>
     *
     * @return The transNameLabel field of the {@link PrintJobHeader}.
     */
    public String getTransNameLabel () {
        return transNameLabel;
    }

    /**
     * <p>Setter for the transNameLabel field of the {@link PrintJobHeader}.</p>
     *
     * @param transNameLabel The transNameLabel field of the {@link PrintJobHeader}.
     */
    public void setTransNameLabel (String transNameLabel) {
        this.transNameLabel = transNameLabel;
    }

    /**
     * <p>Getter for the estimatedOrderTime field of the {@link PrintJobHeader}.</p>
     *
     * @return The estimatedOrderTime field of the {@link PrintJobHeader}.
     */
    public LocalDateTime getEstimatedOrderTime () {
        return estimatedOrderTime;
    }

    /**
     * <p>Setter for the estimatedOrderTime field of the {@link PrintJobHeader}.</p>
     *
     * @param estimatedOrderTime The estimatedOrderTime field of the {@link PrintJobHeader}.
     */
    public void setEstimatedOrderTime (LocalDateTime estimatedOrderTime) {
        this.estimatedOrderTime = estimatedOrderTime;
    }

    /**
     * <p>Getter for the orderNum field of the {@link PrintJobHeader}.</p>
     *
     * @return The orderNum field of the {@link PrintJobHeader}.
     */
    public String getOrderNum () {
        return orderNum;
    }

    /**
     * <p>Setter for the orderNum field of the {@link PrintJobHeader}.</p>
     *
     * @param orderNum The orderNum field of the {@link PrintJobHeader}.
     */
    public void setOrderNum (String orderNum) {
        this.orderNum = orderNum;
    }

    /**
     * <p>Getter for the transTypeID field of the {@link PrintJobHeader}.</p>
     *
     * @return The transTypeID field of the {@link PrintJobHeader}.
     */
    public int getTransTypeID () {
        return transTypeID;
    }

    /**
     * <p>Setter for the transTypeID field of the {@link PrintJobHeader}.</p>
     *
     * @param transTypeID The transTypeID field of the {@link PrintJobHeader}.
     */
    public void setTransTypeID (int transTypeID) {
        this.transTypeID = transTypeID;
    }

    /**
     * <p>Getter for the prevOrderNums field of the {@link PrintJobHeader}.</p>
     *
     * @return The prevOrderNums field of the {@link PrintJobHeader}.
     */
    public String getPrevOrderNums () {
        return prevOrderNums;
    }

    /**
     * <p>Setter for the prevOrderNums field of the {@link PrintJobHeader}.</p>
     *
     * @param prevOrderNums The prevOrderNums field of the {@link PrintJobHeader}.
     */
    public void setPrevOrderNums (String prevOrderNums) {
        this.prevOrderNums = prevOrderNums;
    }

    /**
     * <p>Getter for the kmsOrderStatusID field of the {@link PrintJobHeader}.</p>
     *
     * @return The kmsOrderStatusID field of the {@link PrintJobHeader}.
     */
    public int getKmsOrderStatusID () {
        return kmsOrderStatusID;
    }

    /**
     * <p>Setter for the kmsOrderStatusID field of the {@link PrintJobHeader}.</p>
     *
     * @param kmsOrderStatusID The kmsOrderStatusID field of the {@link PrintJobHeader}.
     */
    public void setKmsOrderStatusID (int kmsOrderStatusID) {
        this.kmsOrderStatusID = kmsOrderStatusID;
    }

}