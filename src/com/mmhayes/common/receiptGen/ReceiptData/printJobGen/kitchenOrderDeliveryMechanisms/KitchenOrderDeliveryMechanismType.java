package com.mmhayes.common.receiptGen.ReceiptData.printJobGen.kitchenOrderDeliveryMechanisms;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-20 13:02:43 -0400 (Wed, 20 May 2020) $: Date of last commit
    $Rev: 11797 $: Revision of last commit
    Notes: Represents various mechanisms by which an or may be sent to the kitchen.
*/

/**
 * <p>Represents various mechanisms by which an or may be sent to the kitchen.</p>
 *
 */
public class KitchenOrderDeliveryMechanismType {

    public static final int KP_PREP = 1;
    public static final int KP_EXPEDITOR = 2;
    public static final int KP_REMOTE = 3;
    public static final int KP_REMOTE_EXPEDITOR = 4;
    public static final int KDS_PREP = 5;
    public static final int KDS_EXPEDITOR = 6;
    public static final int KDS_REMOTE = 7;
    public static final int KDS_REMOTE_EXPEDITOR = 8;
    public static final int KMS_PREP = 9;
    public static final int KMS_EXPEDITOR = 10;
    public static final int KMS_REMOTE = 11;
    public static final int KMS_REMOTE_EXPEDITOR = 12;
    public static final int KMS_MIRROR_PREP = 13;
    public static final int KMS_MIRROR_EXPEDITOR = 14;
    public static final int KMS_MIRROR_REMOTE = 15;
    public static final int KMS_MIRROR_REMOTE_EXPEDITOR = 16;

}