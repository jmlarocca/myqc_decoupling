package com.mmhayes.common.receiptGen.ReceiptData.printJobGen.kitchenOrderDeliveryMechanisms;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-21 15:52:22 -0400 (Thu, 21 May 2020) $: Date of last commit
    $Rev: 11801 $: Revision of last commit
    Notes: Represents a mechanism by which an or may be sent to the kitchen.
*/

import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a mechanism by which an or may be sent to the kitchen.</p>
 *
 */
public class KitchenOrderDeliveryMechanism {

    // private member variables of a KitchenOrderDeliveryMechanism
    private int kitchenOrderDeliveryMechanismType = 0;
    private boolean remoteOrdersUseExpeditor = false;
    private int printerID = 0;
    private String logicalName = "";
    private String description = "";
    private boolean isExpeditorPrinter = false;
    private int ownershipGroupID = 0;
    private int printerHostID = 0;
    private boolean active = false;
    private String name = "";
    private int printerTypeID = 0;
    private int expireJobMins = 0;
    private boolean printsDigitalOrdersOnly = false;
    private int stationID = 0;
    private int printerHardwareTypeID = 0;
    private boolean printOnlyNewItems = false;
    private boolean noExpeditorTktsUnlessProdAdded = false;
    private int printerModelID = 0;
    private boolean usesEscPos = false;
    private int printerComTypeID = 0;
    private int kmsStationID = 0;
    private int kmsStationTypeID = 0;
    private int kmsStationModeID = 0;
    private int kmsStationLayoutID = 0;
    private int numColsPerPage = 0;
    private boolean displayTitleBar = false;
    private boolean displayStationName = false;
    private boolean displayDateTime = false;
    private int titleBarAlignmentID = 0;
    private String logoFileName = "";
    private String logoTokenizedFileName = "";
    private String token = "";
    private boolean displayOrderNumber = false;
    private boolean displayCustomerName = false;
    private boolean displayLegend = false;
    private boolean displayRecent = false;
    private int mirrorKMSStationID = 0;
    private int backupKMSStationID = 0;
    private int titleStyleProfileID = 0;
    private int contentStyleProfileID = 0;
    private int rushOrderStyleProfileID = 0;
    private int orderStatusProfileID = 0;
    private int orderTimerProfileID = 0;
    private int completedOrderTimeDisplayed = 0;
    private boolean bumpProductsToCompleted = false;
    private boolean displayModsInProgress = false;
    private boolean bumpIndividualProducts = false;
    private int touchControlStyleProfileID = 0;
    private boolean printOrdersOnCompleted = false;

    /**
     * <p>Builds a {@link KitchenOrderDeliveryMechanism} object from the given {@link HashMap}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kodmRec The {@link HashMap} to use to build the {@link KitchenOrderDeliveryMechanism}.
     * @return A {@link KitchenOrderDeliveryMechanism} object built from the given {@link HashMap}.
     */
    public static KitchenOrderDeliveryMechanism buildFromHM (String log, HashMap kodmRec) {
        KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism = null;

        try {
            if (!DataFunctions.isEmptyMap(kodmRec)) {
                kitchenOrderDeliveryMechanism = new KitchenOrderDeliveryMechanism();
                kitchenOrderDeliveryMechanism.kitchenOrderDeliveryMechanismType = HashMapDataFns.getIntVal(kodmRec, "KITCHENORDERDELIVERYMECHANISMTYPE");
                kitchenOrderDeliveryMechanism.remoteOrdersUseExpeditor = HashMapDataFns.getBooleanVal(kodmRec, "REMOTEORDERSUSEEXPEDITOR");
                kitchenOrderDeliveryMechanism.printerID = HashMapDataFns.getIntVal(kodmRec, "PRINTERID");
                kitchenOrderDeliveryMechanism.logicalName = HashMapDataFns.getStringVal(kodmRec, "LOGICALNAME");
                kitchenOrderDeliveryMechanism.description = HashMapDataFns.getStringVal(kodmRec, "DESCRIPTION");
                kitchenOrderDeliveryMechanism.isExpeditorPrinter = HashMapDataFns.getBooleanVal(kodmRec, "ISEXPEDITORPRINTER");
                kitchenOrderDeliveryMechanism.ownershipGroupID = HashMapDataFns.getIntVal(kodmRec, "OWNERSHIPGROUPID");
                kitchenOrderDeliveryMechanism.printerHostID = HashMapDataFns.getIntVal(kodmRec, "PRINTERHOSTID");
                kitchenOrderDeliveryMechanism.active = HashMapDataFns.getBooleanVal(kodmRec, "ACTIVE");
                kitchenOrderDeliveryMechanism.name = HashMapDataFns.getStringVal(kodmRec, "NAME");
                kitchenOrderDeliveryMechanism.printerTypeID = HashMapDataFns.getIntVal(kodmRec, "PRINTERTYPEID");
                kitchenOrderDeliveryMechanism.expireJobMins = HashMapDataFns.getIntVal(kodmRec, "EXPIREJOBMINS");
                kitchenOrderDeliveryMechanism.printsDigitalOrdersOnly = HashMapDataFns.getBooleanVal(kodmRec, "PRINTSDIGITALORDERSONLY");
                kitchenOrderDeliveryMechanism.stationID = HashMapDataFns.getIntVal(kodmRec, "STATIONID");
                kitchenOrderDeliveryMechanism.printerHardwareTypeID = HashMapDataFns.getIntVal(kodmRec, "PRINTERHARDWARETYPEID");
                kitchenOrderDeliveryMechanism.printOnlyNewItems = HashMapDataFns.getBooleanVal(kodmRec, "PRINTONLYNEWITEMS");
                kitchenOrderDeliveryMechanism.noExpeditorTktsUnlessProdAdded = HashMapDataFns.getBooleanVal(kodmRec, "NOEXPEDITORTKTSUNLESSPRODADDED");
                kitchenOrderDeliveryMechanism.printerModelID = HashMapDataFns.getIntVal(kodmRec, "PRINTERMODELID");
                kitchenOrderDeliveryMechanism.usesEscPos = HashMapDataFns.getBooleanVal(kodmRec, "USESESCPOS");
                kitchenOrderDeliveryMechanism.printerComTypeID = HashMapDataFns.getIntVal(kodmRec, "PRINTERCOMTYPEID");
                kitchenOrderDeliveryMechanism.kmsStationID = HashMapDataFns.getIntVal(kodmRec, "KMSSTATIONID");
                kitchenOrderDeliveryMechanism.kmsStationTypeID = HashMapDataFns.getIntVal(kodmRec, "KMSSTATIONTYPEID");
                kitchenOrderDeliveryMechanism.kmsStationModeID = HashMapDataFns.getIntVal(kodmRec, "KMSSTATIONMODEID");
                kitchenOrderDeliveryMechanism.kmsStationLayoutID = HashMapDataFns.getIntVal(kodmRec, "KMSSTATIONLAYOUTID");
                kitchenOrderDeliveryMechanism.numColsPerPage = HashMapDataFns.getIntVal(kodmRec, "NUMCOLSPERPAGE");
                kitchenOrderDeliveryMechanism.displayTitleBar = HashMapDataFns.getBooleanVal(kodmRec, "DISPLAYTITLEBAR");
                kitchenOrderDeliveryMechanism.displayStationName = HashMapDataFns.getBooleanVal(kodmRec, "DISPLAYSTATIONNAME");
                kitchenOrderDeliveryMechanism.displayDateTime = HashMapDataFns.getBooleanVal(kodmRec, "DISPLAYDATETIME");
                kitchenOrderDeliveryMechanism.titleBarAlignmentID = HashMapDataFns.getIntVal(kodmRec, "TITLEBARALIGNMENTID");
                kitchenOrderDeliveryMechanism.logoFileName = HashMapDataFns.getStringVal(kodmRec, "LOGOFILENAME");
                kitchenOrderDeliveryMechanism.logoTokenizedFileName = HashMapDataFns.getStringVal(kodmRec, "LOGOTOKENIZEDFILENAME");
                kitchenOrderDeliveryMechanism.token = HashMapDataFns.getStringVal(kodmRec, "TOKEN");
                kitchenOrderDeliveryMechanism.displayOrderNumber = HashMapDataFns.getBooleanVal(kodmRec, "DISPLAYORDERNUMBER");
                kitchenOrderDeliveryMechanism.displayCustomerName = HashMapDataFns.getBooleanVal(kodmRec, "DISPLAYCUSTOMERNAME");
                kitchenOrderDeliveryMechanism.displayLegend = HashMapDataFns.getBooleanVal(kodmRec, "DISPLAYLEGEND");
                kitchenOrderDeliveryMechanism.displayRecent = HashMapDataFns.getBooleanVal(kodmRec, "DISPLAYRECENT");
                kitchenOrderDeliveryMechanism.mirrorKMSStationID = HashMapDataFns.getIntVal(kodmRec, "MIRRORKMSSTATIONID");
                kitchenOrderDeliveryMechanism.backupKMSStationID = HashMapDataFns.getIntVal(kodmRec, "BACKUPKMSSTATIONID");
                kitchenOrderDeliveryMechanism.titleStyleProfileID = HashMapDataFns.getIntVal(kodmRec, "TITLESTYLEPROFILEID");
                kitchenOrderDeliveryMechanism.contentStyleProfileID = HashMapDataFns.getIntVal(kodmRec, "CONTENTSTYLEPROFILEID");
                kitchenOrderDeliveryMechanism.rushOrderStyleProfileID = HashMapDataFns.getIntVal(kodmRec, "RUSHORDERSTYLEPROFILEID");
                kitchenOrderDeliveryMechanism.orderStatusProfileID = HashMapDataFns.getIntVal(kodmRec, "ORDERSTATUSPROFILEID");
                kitchenOrderDeliveryMechanism.orderTimerProfileID = HashMapDataFns.getIntVal(kodmRec, "ORDERTIMERPROFILEID");
                kitchenOrderDeliveryMechanism.completedOrderTimeDisplayed = HashMapDataFns.getIntVal(kodmRec, "COMPLETEDORDERTIMEDISPLAYED");
                kitchenOrderDeliveryMechanism.bumpProductsToCompleted = HashMapDataFns.getBooleanVal(kodmRec, "BUMPPRODUCTSTOCOMPLETED");
                kitchenOrderDeliveryMechanism.displayModsInProgress = HashMapDataFns.getBooleanVal(kodmRec, "DISPLAYMODSINPROGRESS");
                kitchenOrderDeliveryMechanism.bumpIndividualProducts = HashMapDataFns.getBooleanVal(kodmRec, "BUMPINDIVIDUALPRODUCTS");
                kitchenOrderDeliveryMechanism.touchControlStyleProfileID = HashMapDataFns.getIntVal(kodmRec, "TOUCHCONTROLSTYLEPROFILEID");
                kitchenOrderDeliveryMechanism.printOrdersOnCompleted = HashMapDataFns.getBooleanVal(kodmRec, "PRINTORDERSONCOMPLETED");
            }
            else {
                Logger.logMessage("The HashMap passed to KitchenOrderDeliveryMechanism.buildFromHM can't be null or empty!", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build the KitchenOrderDeliveryMechanism from the given HashMap in KitchenOrderDeliveryMechanism.buildFromHM", log, Logger.LEVEL.ERROR);
        }

        return kitchenOrderDeliveryMechanism;
    }

    /**
     * <p>Overridden toString() method for a {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return A {@link String} representation of this {@link KitchenOrderDeliveryMechanism}.
     */
    @Override
    public String toString () {

        return String.format("KITCHENORDERDELIVERYMECHANISMTYPE: %s, REMOTEORDERSUSEEXPEDITOR: %s, PRINTERID: %s, LOGICALNAME: %s, DESCRIPTION: %s, " +
                "ISEXPEDITORPRINTER: %s, OWNERSHIPGROUPID: %s, PRINTERHOSTID: %s, ACTIVE: %s, NAME: %s, PRINTERTYPEID: %s, EXPIREJOBMINS: %s, " +
                "PRINTSDIGITALORDERSONLY: %s, STATIONID: %s, PRINTERHARDWARETYPEID: %s, PRINTONLYNEWITEMS: %s, NOEXPEDITORTKTSUNLESSPRODADDED: %s, " +
                "PRINTERMODELID: %s, USESESCPOS: %s, PRINTERCOMTYPEID: %s, KMSSTATIONID: %s, KMSSTATIONTYPEID: %s, KMSSTATIONMODEID: %s, " +
                "KMSSTATIONLAYOUTID: %s, NUMCOLSPERPAGE: %s, DISPLAYTITLEBAR: %s, DISPLAYSTATIONNAME: %s, DISPLAYDATETIME: %s, TITLEBARALIGNMENTID: %s, " +
                "LOGOFILENAME: %s, LOGOTOKENIZEDFILENAME: %s, TOKEN: %s, DISPLAYORDERNUMBER: %s, DISPLAYCUSTOMERNAME: %s, DISPLAYLEGEND: %s, " +
                "DISPLAYRECENT: %s, MIRRORKMSSTATIONID: %s, BACKUPKMSSTATIONID: %s, TITLESTYLEPROFILEID: %s, CONTENTSTYLEPROFILEID: %s, " +
                "RUSHORDERSTYLEPROFILEID: %s, ORDERSTATUSPROFILEID: %s, ORDERTIMERPROFILEID: %s, COMPLETEDORDERTIMEDISPLAYED: %s, BUMPPRODUCTSTOCOMPLETED: %s, " +
                "DISPLAYMODSINPROGRESS: %s, BUMPINDIVIDUALPRODUCTS: %s, TOUCHCONTROLSTYLEPROFILEID: %s, PRINTORDERSONCOMPLETED: %s",
                Objects.toString((kitchenOrderDeliveryMechanismType != -1 ? kitchenOrderDeliveryMechanismType : "N/A"), "N/A"),
                Objects.toString(remoteOrdersUseExpeditor, "N/A"),
                Objects.toString((printerID != -1 ? printerID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(logicalName) ? logicalName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString(isExpeditorPrinter, "N/A"),
                Objects.toString((ownershipGroupID != -1 ? ownershipGroupID : "N/A"), "N/A"),
                Objects.toString((printerHostID != -1 ? printerHostID : "N/A"), "N/A"),
                Objects.toString(active, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((printerTypeID != -1 ? printerTypeID : "N/A"), "N/A"),
                Objects.toString((expireJobMins != -1 ? expireJobMins : "N/A"), "N/A"),
                Objects.toString(printsDigitalOrdersOnly, "N/A"),
                Objects.toString((stationID != -1 ? stationID : "N/A"), "N/A"),
                Objects.toString((printerHardwareTypeID != -1 ? printerHardwareTypeID : "N/A"), "N/A"),
                Objects.toString(printOnlyNewItems, "N/A"),
                Objects.toString(noExpeditorTktsUnlessProdAdded, "N/A"),
                Objects.toString((printerModelID != -1 ? printerModelID : "N/A"), "N/A"),
                Objects.toString(usesEscPos, "N/A"),
                Objects.toString((printerComTypeID != -1 ? printerComTypeID : "N/A"), "N/A"),
                Objects.toString((kmsStationID != -1 ? kmsStationID : "N/A"), "N/A"),
                Objects.toString((kmsStationTypeID != -1 ? kmsStationTypeID : "N/A"), "N/A"),
                Objects.toString((kmsStationModeID != -1 ? kmsStationModeID : "N/A"), "N/A"),
                Objects.toString((kmsStationLayoutID != -1 ? kmsStationLayoutID : "N/A"), "N/A"),
                Objects.toString((numColsPerPage != -1 ? numColsPerPage : "N/A"), "N/A"),
                Objects.toString(displayTitleBar, "N/A"),
                Objects.toString(displayStationName, "N/A"),
                Objects.toString(displayDateTime, "N/A"),
                Objects.toString((titleBarAlignmentID != -1 ? titleBarAlignmentID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(logoFileName) ? logoFileName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(logoTokenizedFileName) ? logoTokenizedFileName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(token) ? token : "N/A"), "N/A"),
                Objects.toString(displayOrderNumber, "N/A"),
                Objects.toString(displayCustomerName, "N/A"),
                Objects.toString(displayLegend, "N/A"),
                Objects.toString(displayRecent, "N/A"),
                Objects.toString((mirrorKMSStationID != -1 ? mirrorKMSStationID : "N/A"), "N/A"),
                Objects.toString((backupKMSStationID != -1 ? backupKMSStationID : "N/A"), "N/A"),
                Objects.toString((titleStyleProfileID != -1 ? titleStyleProfileID : "N/A"), "N/A"),
                Objects.toString((contentStyleProfileID != -1 ? contentStyleProfileID : "N/A"), "N/A"),
                Objects.toString((rushOrderStyleProfileID != -1 ? rushOrderStyleProfileID : "N/A"), "N/A"),
                Objects.toString((orderStatusProfileID != -1 ? orderStatusProfileID : "N/A"), "N/A"),
                Objects.toString((orderTimerProfileID != -1 ? orderTimerProfileID : "N/A"), "N/A"),
                Objects.toString((completedOrderTimeDisplayed != -1 ? completedOrderTimeDisplayed : "N/A"), "N/A"),
                Objects.toString(bumpProductsToCompleted, "N/A"),
                Objects.toString(displayModsInProgress, "N/A"),
                Objects.toString(bumpIndividualProducts, "N/A"),
                Objects.toString((touchControlStyleProfileID != -1 ? touchControlStyleProfileID : "N/A"), "N/A"),
                Objects.toString(printOrdersOnCompleted, "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link KitchenOrderDeliveryMechanism}.
     * Two {@link KitchenOrderDeliveryMechanism} are defined as being equal if they have the same printer or KMS station ID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KitchenOrderDeliveryMechanism}.
     * @return Whether or not the {@link Object} is equal to this {@link KitchenOrderDeliveryMechanism}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KitchenOrderDeliveryMechanism and check if the obj's printer or KMS station ID is equal to this KitchenOrderDeliveryMechanism's printer or KMS station ID
        KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism = ((KitchenOrderDeliveryMechanism) obj);
        return ((Objects.equals(kitchenOrderDeliveryMechanism.printerID, printerID)) || (Objects.equals(kitchenOrderDeliveryMechanism.kmsStationID, kmsStationID)));
    }

    /**
     * <p>Overridden hashCode() method for a {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The unique hash code for this {@link KitchenOrderDeliveryMechanism}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(printerID, kmsStationID);
    }

    /**
     * <p>Getter for the kitchenOrderDeliveryMechanismType field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The kitchenOrderDeliveryMechanismType field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getKitchenOrderDeliveryMechanismType () {
        return kitchenOrderDeliveryMechanismType;
    }

    /**
     * <p>Setter for the kitchenOrderDeliveryMechanismType field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param kitchenOrderDeliveryMechanismType The kitchenOrderDeliveryMechanismType field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setKitchenOrderDeliveryMechanismType (int kitchenOrderDeliveryMechanismType) {
        this.kitchenOrderDeliveryMechanismType = kitchenOrderDeliveryMechanismType;
    }

    /**
     * <p>Getter for the remoteOrdersUseExpeditor field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The remoteOrdersUseExpeditor field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getRemoteOrdersUseExpeditor () {
        return remoteOrdersUseExpeditor;
    }

    /**
     * <p>Setter for the remoteOrdersUseExpeditor field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param remoteOrdersUseExpeditor The remoteOrdersUseExpeditor field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setRemoteOrdersUseExpeditor (boolean remoteOrdersUseExpeditor) {
        this.remoteOrdersUseExpeditor = remoteOrdersUseExpeditor;
    }

    /**
     * <p>Getter for the printerID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The printerID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getPrinterID () {
        return printerID;
    }

    /**
     * <p>Setter for the printerID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param printerID The printerID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setPrinterID (int printerID) {
        this.printerID = printerID;
    }

    /**
     * <p>Getter for the logicalName field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The logicalName field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public String getLogicalName () {
        return logicalName;
    }

    /**
     * <p>Setter for the logicalName field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param logicalName The logicalName field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setLogicalName (String logicalName) {
        this.logicalName = logicalName;
    }

    /**
     * <p>Getter for the description field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The description field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public String getDescription () {
        return description;
    }

    /**
     * <p>Setter for the description field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param description The description field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setDescription (String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the isExpeditorPrinter field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The isExpeditorPrinter field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getIsExpeditorPrinter () {
        return isExpeditorPrinter;
    }

    /**
     * <p>Setter for the isExpeditorPrinter field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param isExpeditorPrinter The isExpeditorPrinter field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setIsExpeditorPrinter (boolean isExpeditorPrinter) {
        this.isExpeditorPrinter = isExpeditorPrinter;
    }

    /**
     * <p>Getter for the ownershipGroupID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The ownershipGroupID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getOwnershipGroupID () {
        return ownershipGroupID;
    }

    /**
     * <p>Setter for the ownershipGroupID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param ownershipGroupID The ownershipGroupID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setOwnershipGroupID (int ownershipGroupID) {
        this.ownershipGroupID = ownershipGroupID;
    }

    /**
     * <p>Getter for the printerHostID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The printerHostID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getPrinterHostID () {
        return printerHostID;
    }

    /**
     * <p>Setter for the printerHostID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param printerHostID The printerHostID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setPrinterHostID (int printerHostID) {
        this.printerHostID = printerHostID;
    }

    /**
     * <p>Getter for the active field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The active field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getActive () {
        return active;
    }

    /**
     * <p>Setter for the active field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param active The active field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setActive (boolean active) {
        this.active = active;
    }

    /**
     * <p>Getter for the name field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The name field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param name The name field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the printerTypeID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The printerTypeID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getPrinterTypeID () {
        return printerTypeID;
    }

    /**
     * <p>Setter for the printerTypeID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param printerTypeID The printerTypeID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setPrinterTypeID (int printerTypeID) {
        this.printerTypeID = printerTypeID;
    }

    /**
     * <p>Getter for the expireJobMins field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The expireJobMins field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getExpireJobMins () {
        return expireJobMins;
    }

    /**
     * <p>Setter for the expireJobMins field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param expireJobMins The expireJobMins field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setExpireJobMins (int expireJobMins) {
        this.expireJobMins = expireJobMins;
    }

    /**
     * <p>Getter for the printsDigitalOrdersOnly field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The printsDigitalOrdersOnly field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getPrintsDigitalOrdersOnly () {
        return printsDigitalOrdersOnly;
    }

    /**
     * <p>Setter for the printsDigitalOrdersOnly field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param printsDigitalOrdersOnly The printsDigitalOrdersOnly field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setPrintsDigitalOrdersOnly (boolean printsDigitalOrdersOnly) {
        this.printsDigitalOrdersOnly = printsDigitalOrdersOnly;
    }

    /**
     * <p>Getter for the stationID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The stationID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getStationID () {
        return stationID;
    }

    /**
     * <p>Setter for the stationID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param stationID The stationID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setStationID (int stationID) {
        this.stationID = stationID;
    }

    /**
     * <p>Getter for the printerHardwareTypeID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The printerHardwareTypeID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getPrinterHardwareTypeID () {
        return printerHardwareTypeID;
    }

    /**
     * <p>Setter for the printerHardwareTypeID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param printerHardwareTypeID The printerHardwareTypeID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setPrinterHardwareTypeID (int printerHardwareTypeID) {
        this.printerHardwareTypeID = printerHardwareTypeID;
    }

    /**
     * <p>Getter for the printOnlyNewItems field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The printOnlyNewItems field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getPrintOnlyNewItems () {
        return printOnlyNewItems;
    }

    /**
     * <p>Setter for the printOnlyNewItems field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param printOnlyNewItems The printOnlyNewItems field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setPrintOnlyNewItems (boolean printOnlyNewItems) {
        this.printOnlyNewItems = printOnlyNewItems;
    }

    /**
     * <p>Getter for the noExpeditorTktsUnlessProdAdded field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The noExpeditorTktsUnlessProdAdded field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getNoExpeditorTktsUnlessProdAdded () {
        return noExpeditorTktsUnlessProdAdded;
    }

    /**
     * <p>Setter for the noExpeditorTktsUnlessProdAdded field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param noExpeditorTktsUnlessProdAdded The noExpeditorTktsUnlessProdAdded field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setNoExpeditorTktsUnlessProdAdded (boolean noExpeditorTktsUnlessProdAdded) {
        this.noExpeditorTktsUnlessProdAdded = noExpeditorTktsUnlessProdAdded;
    }

    /**
     * <p>Getter for the printerModelID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The printerModelID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getPrinterModelID () {
        return printerModelID;
    }

    /**
     * <p>Setter for the printerModelID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param printerModelID The printerModelID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setPrinterModelID (int printerModelID) {
        this.printerModelID = printerModelID;
    }

    /**
     * <p>Getter for the usesEscPos field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The usesEscPos field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getUsesEscPos () {
        return usesEscPos;
    }

    /**
     * <p>Setter for the usesEscPos field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param usesEscPos The usesEscPos field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setUsesEscPos (boolean usesEscPos) {
        this.usesEscPos = usesEscPos;
    }

    /**
     * <p>Getter for the printerComTypeID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The printerComTypeID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getPrinterComTypeID () {
        return printerComTypeID;
    }

    /**
     * <p>Setter for the printerComTypeID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param printerComTypeID The printerComTypeID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setPrinterComTypeID (int printerComTypeID) {
        this.printerComTypeID = printerComTypeID;
    }

    /**
     * <p>Getter for the kmsStationID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The kmsStationID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getKmsStationID () {
        return kmsStationID;
    }

    /**
     * <p>Setter for the kmsStationID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param kmsStationID The kmsStationID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setKmsStationID (int kmsStationID) {
        this.kmsStationID = kmsStationID;
    }

    /**
     * <p>Getter for the kmsStationTypeID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The kmsStationTypeID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getKmsStationTypeID () {
        return kmsStationTypeID;
    }

    /**
     * <p>Setter for the kmsStationTypeID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param kmsStationTypeID The kmsStationTypeID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setKmsStationTypeID (int kmsStationTypeID) {
        this.kmsStationTypeID = kmsStationTypeID;
    }

    /**
     * <p>Getter for the kmsStationModeID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The kmsStationModeID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getKmsStationModeID () {
        return kmsStationModeID;
    }

    /**
     * <p>Setter for the kmsStationModeID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param kmsStationModeID The kmsStationModeID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setKmsStationModeID (int kmsStationModeID) {
        this.kmsStationModeID = kmsStationModeID;
    }

    /**
     * <p>Getter for the kmsStationLayoutID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The kmsStationLayoutID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getKmsStationLayoutID () {
        return kmsStationLayoutID;
    }

    /**
     * <p>Setter for the kmsStationLayoutID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param kmsStationLayoutID The kmsStationLayoutID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setKmsStationLayoutID (int kmsStationLayoutID) {
        this.kmsStationLayoutID = kmsStationLayoutID;
    }

    /**
     * <p>Getter for the numColsPerPage field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The numColsPerPage field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getNumColsPerPage () {
        return numColsPerPage;
    }

    /**
     * <p>Setter for the numColsPerPage field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param numColsPerPage The numColsPerPage field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setNumColsPerPage (int numColsPerPage) {
        this.numColsPerPage = numColsPerPage;
    }

    /**
     * <p>Getter for the displayTitleBar field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The displayTitleBar field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getDisplayTitleBar () {
        return displayTitleBar;
    }

    /**
     * <p>Setter for the displayTitleBar field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param displayTitleBar The displayTitleBar field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setDisplayTitleBar (boolean displayTitleBar) {
        this.displayTitleBar = displayTitleBar;
    }

    /**
     * <p>Getter for the displayStationName field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The displayStationName field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getDisplayStationName () {
        return displayStationName;
    }

    /**
     * <p>Setter for the displayStationName field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param displayStationName The displayStationName field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setDisplayStationName (boolean displayStationName) {
        this.displayStationName = displayStationName;
    }

    /**
     * <p>Getter for the displayDateTime field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The displayDateTime field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getDisplayDateTime () {
        return displayDateTime;
    }

    /**
     * <p>Setter for the displayDateTime field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param displayDateTime The displayDateTime field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setDisplayDateTime (boolean displayDateTime) {
        this.displayDateTime = displayDateTime;
    }

    /**
     * <p>Getter for the titleBarAlignmentID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The titleBarAlignmentID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getTitleBarAlignmentID () {
        return titleBarAlignmentID;
    }

    /**
     * <p>Setter for the titleBarAlignmentID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param titleBarAlignmentID The titleBarAlignmentID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setTitleBarAlignmentID (int titleBarAlignmentID) {
        this.titleBarAlignmentID = titleBarAlignmentID;
    }

    /**
     * <p>Getter for the logoFileName field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The logoFileName field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public String getLogoFileName () {
        return logoFileName;
    }

    /**
     * <p>Setter for the logoFileName field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param logoFileName The logoFileName field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setLogoFileName (String logoFileName) {
        this.logoFileName = logoFileName;
    }

    /**
     * <p>Getter for the logoTokenizedFileName field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The logoTokenizedFileName field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public String getLogoTokenizedFileName () {
        return logoTokenizedFileName;
    }

    /**
     * <p>Setter for the logoTokenizedFileName field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param logoTokenizedFileName The logoTokenizedFileName field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setLogoTokenizedFileName (String logoTokenizedFileName) {
        this.logoTokenizedFileName = logoTokenizedFileName;
    }

    /**
     * <p>Getter for the token field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The token field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public String getToken () {
        return token;
    }

    /**
     * <p>Setter for the token field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param token The token field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setToken (String token) {
        this.token = token;
    }

    /**
     * <p>Getter for the displayOrderNumber field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The displayOrderNumber field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getDisplayOrderNumber () {
        return displayOrderNumber;
    }

    /**
     * <p>Setter for the displayOrderNumber field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param displayOrderNumber The displayOrderNumber field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setDisplayOrderNumber (boolean displayOrderNumber) {
        this.displayOrderNumber = displayOrderNumber;
    }

    /**
     * <p>Getter for the displayCustomerName field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The displayCustomerName field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getDisplayCustomerName () {
        return displayCustomerName;
    }

    /**
     * <p>Setter for the displayCustomerName field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param displayCustomerName The displayCustomerName field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setDisplayCustomerName (boolean displayCustomerName) {
        this.displayCustomerName = displayCustomerName;
    }

    /**
     * <p>Getter for the displayLegend field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The displayLegend field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getDisplayLegend () {
        return displayLegend;
    }

    /**
     * <p>Setter for the displayLegend field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param displayLegend The displayLegend field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setDisplayLegend (boolean displayLegend) {
        this.displayLegend = displayLegend;
    }

    /**
     * <p>Getter for the displayRecent field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The displayRecent field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getDisplayRecent () {
        return displayRecent;
    }

    /**
     * <p>Setter for the displayRecent field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param displayRecent The displayRecent field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setDisplayRecent (boolean displayRecent) {
        this.displayRecent = displayRecent;
    }

    /**
     * <p>Getter for the mirrorKMSStationID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The mirrorKMSStationID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getMirrorKMSStationID () {
        return mirrorKMSStationID;
    }

    /**
     * <p>Setter for the mirrorKMSStationID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param mirrorKMSStationID The mirrorKMSStationID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setMirrorKMSStationID (int mirrorKMSStationID) {
        this.mirrorKMSStationID = mirrorKMSStationID;
    }

    /**
     * <p>Getter for the backupKMSStationID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The backupKMSStationID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getBackupKMSStationID () {
        return backupKMSStationID;
    }

    /**
     * <p>Setter for the backupKMSStationID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param backupKMSStationID The backupKMSStationID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setBackupKMSStationID (int backupKMSStationID) {
        this.backupKMSStationID = backupKMSStationID;
    }

    /**
     * <p>Getter for the titleStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The titleStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getTitleStyleProfileID () {
        return titleStyleProfileID;
    }

    /**
     * <p>Setter for the titleStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param titleStyleProfileID The titleStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setTitleStyleProfileID (int titleStyleProfileID) {
        this.titleStyleProfileID = titleStyleProfileID;
    }

    /**
     * <p>Getter for the contentStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The contentStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getContentStyleProfileID () {
        return contentStyleProfileID;
    }

    /**
     * <p>Setter for the contentStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param contentStyleProfileID The contentStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setContentStyleProfileID (int contentStyleProfileID) {
        this.contentStyleProfileID = contentStyleProfileID;
    }

    /**
     * <p>Getter for the rushOrderStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The rushOrderStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getRushOrderStyleProfileID () {
        return rushOrderStyleProfileID;
    }

    /**
     * <p>Setter for the rushOrderStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param rushOrderStyleProfileID The rushOrderStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setRushOrderStyleProfileID (int rushOrderStyleProfileID) {
        this.rushOrderStyleProfileID = rushOrderStyleProfileID;
    }

    /**
     * <p>Getter for the orderStatusProfileID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The orderStatusProfileID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getOrderStatusProfileID () {
        return orderStatusProfileID;
    }

    /**
     * <p>Setter for the orderStatusProfileID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param orderStatusProfileID The orderStatusProfileID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setOrderStatusProfileID (int orderStatusProfileID) {
        this.orderStatusProfileID = orderStatusProfileID;
    }

    /**
     * <p>Getter for the orderTimerProfileID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The orderTimerProfileID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getOrderTimerProfileID () {
        return orderTimerProfileID;
    }

    /**
     * <p>Setter for the orderTimerProfileID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param orderTimerProfileID The orderTimerProfileID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setOrderTimerProfileID (int orderTimerProfileID) {
        this.orderTimerProfileID = orderTimerProfileID;
    }

    /**
     * <p>Getter for the completedOrderTimeDisplayed field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The completedOrderTimeDisplayed field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getCompletedOrderTimeDisplayed () {
        return completedOrderTimeDisplayed;
    }

    /**
     * <p>Setter for the completedOrderTimeDisplayed field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param completedOrderTimeDisplayed The completedOrderTimeDisplayed field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setCompletedOrderTimeDisplayed (int completedOrderTimeDisplayed) {
        this.completedOrderTimeDisplayed = completedOrderTimeDisplayed;
    }

    /**
     * <p>Getter for the bumpProductsToCompleted field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The bumpProductsToCompleted field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getBumpProductsToCompleted () {
        return bumpProductsToCompleted;
    }

    /**
     * <p>Setter for the bumpProductsToCompleted field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param bumpProductsToCompleted The bumpProductsToCompleted field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setBumpProductsToCompleted (boolean bumpProductsToCompleted) {
        this.bumpProductsToCompleted = bumpProductsToCompleted;
    }

    /**
     * <p>Getter for the displayModsInProgress field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The displayModsInProgress field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getDisplayModsInProgress () {
        return displayModsInProgress;
    }

    /**
     * <p>Setter for the displayModsInProgress field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param displayModsInProgress The displayModsInProgress field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setDisplayModsInProgress (boolean displayModsInProgress) {
        this.displayModsInProgress = displayModsInProgress;
    }

    /**
     * <p>Getter for the bumpIndividualProducts field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The bumpIndividualProducts field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getBumpIndividualProducts () {
        return bumpIndividualProducts;
    }

    /**
     * <p>Setter for the bumpIndividualProducts field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param bumpIndividualProducts The bumpIndividualProducts field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setBumpIndividualProducts (boolean bumpIndividualProducts) {
        this.bumpIndividualProducts = bumpIndividualProducts;
    }

    /**
     * <p>Getter for the touchControlStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The touchControlStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public int getTouchControlStyleProfileID () {
        return touchControlStyleProfileID;
    }

    /**
     * <p>Setter for the touchControlStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param touchControlStyleProfileID The touchControlStyleProfileID field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setTouchControlStyleProfileID (int touchControlStyleProfileID) {
        this.touchControlStyleProfileID = touchControlStyleProfileID;
    }

    /**
     * <p>Getter for the printOrdersOnCompleted field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @return The printOrdersOnCompleted field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public boolean getPrintOrdersOnCompleted () {
        return printOrdersOnCompleted;
    }

    /**
     * <p>Setter for the printOrdersOnCompleted field of the {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param printOrdersOnCompleted The printOrdersOnCompleted field of the {@link KitchenOrderDeliveryMechanism}.
     */
    public void setPrintOrdersOnCompleted (boolean printOrdersOnCompleted) {
        this.printOrdersOnCompleted = printOrdersOnCompleted;
    }

}