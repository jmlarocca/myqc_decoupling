package com.mmhayes.common.receiptGen.ReceiptData.printJobGen.detail;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-20 13:02:43 -0400 (Wed, 20 May 2020) $: Date of last commit
    $Rev: 11797 $: Revision of last commit
    Notes: Interface that should be implemented by all print job detail types.
*/

/**
 * <p>Interface that should be implemented by all print job detail types.</p>
 *
 */
public interface IPrintJobDetail {}