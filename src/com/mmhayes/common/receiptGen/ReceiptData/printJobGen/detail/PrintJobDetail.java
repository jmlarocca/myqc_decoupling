package com.mmhayes.common.receiptGen.ReceiptData.printJobGen.detail;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-19 16:33:45 -0400 (Tue, 19 May 2020) $: Date of last commit
    $Rev: 11789 $: Revision of last commit
    Notes: Contains common functionality amongst all print job detail types.
*/

import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * <p>Contains common functionality amongst all print job detail types.</p>
 *
 */
public abstract class PrintJobDetail {

    // protected member variables of a PrintJobDetail
    protected BigDecimal transLineItemID = null;
    protected int printerID = 0;
    protected int printStatusID = 0;
    protected int printerHostID = 0;
    protected int productID = 0;
    protected double quantity = 0.0d;
    protected boolean isModifier = false;
    protected String lineDetail = "";
    protected String hideStation = "";
    protected int kmsStationID = 0;
    protected int kmsOrderStatusID = 0;

    /**
     * <p>Overridden toString() method for a {@link KPPrintJobDetail}.</p>
     *
     * @return A {@link String} representation of this {@link KPPrintJobDetail}.
     */
    @Override
    public String toString () {

        return String.format("TRANSLINEITEMID: %s, PRINTERID: %s, PRINTSTATUSID: %s, PRINTERHOSTID: %s, PRODUCTID: %s, QUANTITY: %s, ISMODIFIER: %s, " +
                "LINEDETAIL: %s, HIDESTATION: %s, KMSSTATIONID: %s, KMSORDERSTATUSID: %s",
                Objects.toString((transLineItemID != null ? transLineItemID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((printerID != -1 ? printerID : "N/A"), "N/A"),
                Objects.toString((printStatusID != -1 ? printStatusID : "N/A"), "N/A"),
                Objects.toString((printerHostID != -1 ? printerHostID : "N/A"), "N/A"),
                Objects.toString((productID != -1 ? productID : "N/A"), "N/A"),
                Objects.toString((quantity != -1.0d ? quantity : "N/A"), "N/A"),
                Objects.toString(isModifier, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(lineDetail) ? lineDetail : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(hideStation) ? hideStation : "N/A"), "N/A"),
                Objects.toString((kmsStationID != -1 ? kmsStationID : "N/A"), "N/A"),
                Objects.toString((kmsOrderStatusID != -1 ? kmsOrderStatusID : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link KPPrintJobDetail}.
     * Two {@link KPPrintJobDetail} are defined as being equal if they have the same printerID and productID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KPPrintJobDetail}.
     * @return Whether or not the {@link Object} is equal to this {@link KPPrintJobDetail}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KPPrintJobDetail and check if the obj's kpPrintJobDetailID is equal to this KPPrintJobDetail's kpPrintJobDetailID
        KPPrintJobDetail kpPrintJobDetail = ((KPPrintJobDetail) obj);
        return ((Objects.equals(kpPrintJobDetail.printerID, printerID)) && (Objects.equals(kpPrintJobDetail.productID, productID)));
    }

    /**
     * <p>Overridden hashCode() method for a {@link KPPrintJobDetail}.</p>
     *
     * @return The unique hash code for this {@link KPPrintJobDetail}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(printerID, productID);
    }

    /**
     * <p>Getter for the transLineItemID field of the {@link PrintJobDetail}.</p>
     *
     * @return The transLineItemID field of the {@link PrintJobDetail}.
     */
    public BigDecimal getTransLineItemID () {
        return transLineItemID;
    }

    /**
     * <p>Setter for the transLineItemID field of the {@link PrintJobDetail}.</p>
     *
     * @param transLineItemID The transLineItemID field of the {@link PrintJobDetail}.
     */
    public void setTransLineItemID (BigDecimal transLineItemID) {
        this.transLineItemID = transLineItemID;
    }

    /**
     * <p>Getter for the printerID field of the {@link PrintJobDetail}.</p>
     *
     * @return The printerID field of the {@link PrintJobDetail}.
     */
    public int getPrinterID () {
        return printerID;
    }

    /**
     * <p>Setter for the printerID field of the {@link PrintJobDetail}.</p>
     *
     * @param printerID The printerID field of the {@link PrintJobDetail}.
     */
    public void setPrinterID (int printerID) {
        this.printerID = printerID;
    }

    /**
     * <p>Getter for the printStatusID field of the {@link PrintJobDetail}.</p>
     *
     * @return The printStatusID field of the {@link PrintJobDetail}.
     */
    public int getPrintStatusID () {
        return printStatusID;
    }

    /**
     * <p>Setter for the printStatusID field of the {@link PrintJobDetail}.</p>
     *
     * @param printStatusID The printStatusID field of the {@link PrintJobDetail}.
     */
    public void setPrintStatusID (int printStatusID) {
        this.printStatusID = printStatusID;
    }

    /**
     * <p>Getter for the printerHostID field of the {@link PrintJobDetail}.</p>
     *
     * @return The printerHostID field of the {@link PrintJobDetail}.
     */
    public int getPrinterHostID () {
        return printerHostID;
    }

    /**
     * <p>Setter for the printerHostID field of the {@link PrintJobDetail}.</p>
     *
     * @param printerHostID The printerHostID field of the {@link PrintJobDetail}.
     */
    public void setPrinterHostID (int printerHostID) {
        this.printerHostID = printerHostID;
    }

    /**
     * <p>Getter for the productID field of the {@link PrintJobDetail}.</p>
     *
     * @return The productID field of the {@link PrintJobDetail}.
     */
    public int getProductID () {
        return productID;
    }

    /**
     * <p>Setter for the productID field of the {@link PrintJobDetail}.</p>
     *
     * @param productID The productID field of the {@link PrintJobDetail}.
     */
    public void setProductID (int productID) {
        this.productID = productID;
    }

    /**
     * <p>Getter for the quantity field of the {@link PrintJobDetail}.</p>
     *
     * @return The quantity field of the {@link PrintJobDetail}.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link PrintJobDetail}.</p>
     *
     * @param quantity The quantity field of the {@link PrintJobDetail}.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the isModifier field of the {@link PrintJobDetail}.</p>
     *
     * @return The isModifier field of the {@link PrintJobDetail}.
     */
    public boolean getIsModifier () {
        return isModifier;
    }

    /**
     * <p>Setter for the isModifier field of the {@link PrintJobDetail}.</p>
     *
     * @param isModifier The isModifier field of the {@link PrintJobDetail}.
     */
    public void setIsModifier (boolean isModifier) {
        this.isModifier = isModifier;
    }

    /**
     * <p>Getter for the lineDetail field of the {@link PrintJobDetail}.</p>
     *
     * @return The lineDetail field of the {@link PrintJobDetail}.
     */
    public String getLineDetail () {
        return lineDetail;
    }

    /**
     * <p>Setter for the lineDetail field of the {@link PrintJobDetail}.</p>
     *
     * @param lineDetail The lineDetail field of the {@link PrintJobDetail}.
     */
    public void setLineDetail (String lineDetail) {
        this.lineDetail = lineDetail;
    }

    /**
     * <p>Getter for the hideStation field of the {@link PrintJobDetail}.</p>
     *
     * @return The hideStation field of the {@link PrintJobDetail}.
     */
    public String getHideStation () {
        return hideStation;
    }

    /**
     * <p>Setter for the hideStation field of the {@link PrintJobDetail}.</p>
     *
     * @param hideStation The hideStation field of the {@link PrintJobDetail}.
     */
    public void setHideStation (String hideStation) {
        this.hideStation = hideStation;
    }

    /**
     * <p>Getter for the kmsStationID field of the {@link PrintJobDetail}.</p>
     *
     * @return The kmsStationID field of the {@link PrintJobDetail}.
     */
    public int getKmsStationID () {
        return kmsStationID;
    }

    /**
     * <p>Setter for the kmsStationID field of the {@link PrintJobDetail}.</p>
     *
     * @param kmsStationID The kmsStationID field of the {@link PrintJobDetail}.
     */
    public void setKmsStationID (int kmsStationID) {
        this.kmsStationID = kmsStationID;
    }

    /**
     * <p>Getter for the kmsOrderStatusID field of the {@link PrintJobDetail}.</p>
     *
     * @return The kmsOrderStatusID field of the {@link PrintJobDetail}.
     */
    public int getKmsOrderStatusID () {
        return kmsOrderStatusID;
    }

    /**
     * <p>Setter for the kmsOrderStatusID field of the {@link PrintJobDetail}.</p>
     *
     * @param kmsOrderStatusID The kmsOrderStatusID field of the {@link PrintJobDetail}.
     */
    public void setKmsOrderStatusID (int kmsOrderStatusID) {
        this.kmsOrderStatusID = kmsOrderStatusID;
    }

}