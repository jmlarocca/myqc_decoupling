package com.mmhayes.common.receiptGen.ReceiptData.printJobGen.detail;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-20 13:02:43 -0400 (Wed, 20 May 2020) $: Date of last commit
    $Rev: 11797 $: Revision of last commit
    Notes: Represents various print job detail types.
*/

/**
 * <p>Represents various print job detail types.</p>
 *
 */
public class PrintJobDetailType {

    public static final int KP = 1;
    public static final int KDS = 2;
    public static final int KMS = 3;

}