package com.mmhayes.common.receiptGen.ReceiptData.printJobGen.detail;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-21 15:52:22 -0400 (Thu, 21 May 2020) $: Date of last commit
    $Rev: 11801 $: Revision of last commit
    Notes: Creates a print job detail of a certain type.
*/

import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.kitchenOrderDeliveryMechanisms.KitchenOrderDeliveryMechanism;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.ITransLineItem;
import com.mmhayes.common.utils.Logger;

/**
 * <p>Creates a print job detail of a certain type.</p>
 *
 */
public class PrintJobDetailFactory {

    /**
     * <p>Creates and instance of an {@link IPrintJobDetail} of the given print job detail type.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param printJobDetailType The type of print job detail to create.
     * @param kitchenOrderDeliveryMechanism The mechanism by which an order will be delivered to the kitchen represented by a {@link KitchenOrderDeliveryMechanism} object.
     * @param hiddenKDSStationIDs The station IDs of KDS stations on which the trans line item should be hidden (KDS).
     * @param transLineItem The {@link ITransLineItem} that should be used to create the {@link IPrintJobDetail}.
     * @return An {@link IPrintJobDetail} of the given print job detail type.
     */
    public IPrintJobDetail buildPrintJobDetail (String log,
                                                int printJobDetailType,
                                                KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism,
                                                String hiddenKDSStationIDs,
                                                ITransLineItem transLineItem) {
        IPrintJobDetail printJobDetail = null;

        try {
            switch (printJobDetailType) {
                case PrintJobDetailType.KP:
                    printJobDetail = KPPrintJobDetail.buildFromTransLineItem(log, kitchenOrderDeliveryMechanism, transLineItem);
                    break;
                case PrintJobDetailType.KDS:
                    printJobDetail = KDSPrintJobDetail.buildFromTransLineItem(log, kitchenOrderDeliveryMechanism, hiddenKDSStationIDs, transLineItem);
                    break;
                case PrintJobDetailType.KMS:
                    printJobDetail = KMSPrintJobDetail.buildFromTransLineItem(log, kitchenOrderDeliveryMechanism, transLineItem);
                    break;
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build a print job detail for the trans line item in PrintJobDetailFactory.buildPrintJobDetail", log, Logger.LEVEL.ERROR);
        }

        return printJobDetail;
    }

}