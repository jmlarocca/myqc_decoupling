package com.mmhayes.common.receiptGen.ReceiptData.printJobGen.detail;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-28 13:04:26 -0400 (Thu, 28 May 2020) $: Date of last commit
    $Rev: 11829 $: Revision of last commit
    Notes: Represents a detail in a KMS print job.
*/

import com.mmhayes.common.kms.types.KMSOrderStatus;
import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.kitchenOrderDeliveryMechanisms.KitchenOrderDeliveryMechanism;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.ITransLineItem;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.Product;
import com.mmhayes.common.utils.Logger;

/**
 * <p>Represents a detail in a KMS print job.</p>
 *
 */
public class KMSPrintJobDetail extends PrintJobDetail implements IPrintJobDetail {

    /**
     * <p>Builds a {@link KMSPrintJobDetail} from the given {@link ITransLineItem}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kitchenOrderDeliveryMechanism The mechanism by which an order will be delivered to the kitchen represented by a {@link KitchenOrderDeliveryMechanism} object.
     * @param transLineItem The {@link ITransLineItem} that should be used to create the {@link KMSPrintJobDetail}.
     * @return The {@link KMSPrintJobDetail} built from the given {@link ITransLineItem}.
     */
    public static KMSPrintJobDetail buildFromTransLineItem (String log, KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism, ITransLineItem transLineItem) {
        KMSPrintJobDetail kmsPrintJobDetail = null;

        try {
            // make sure the print job detail has a way of being sent to the kitchen
            if (kitchenOrderDeliveryMechanism == null) {
                Logger.logMessage("The mechanism by which an order will be delivered to the kitchen can't be null in KMSPrintJobDetail.buildFromTransLineItem!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure the print job detail has a valid KMS station
            if (kitchenOrderDeliveryMechanism.getKmsStationID() <= 0) {
                Logger.logMessage("The ID of the KMS station for the print job detail is invalid in KMSPrintJobDetail.buildFromTransLineItem, the " +
                        "KMS station ID should be greater than 0!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have a trans line item to create the print job detail
            if (transLineItem == null) {
                Logger.logMessage("The transaction line item passed to KMSPrintJobDetail.buildFromTransLineItem can't be null!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // create the print job detail
            if (transLineItem instanceof Product) {
                // cast the ITransLineItem to a Product
                Product product = ((Product) transLineItem);
                kmsPrintJobDetail = new KMSPrintJobDetail();
                kmsPrintJobDetail.transLineItemID = product.getPaTransLineItemID();
                kmsPrintJobDetail.kmsStationID = kitchenOrderDeliveryMechanism.getKmsStationID();
                kmsPrintJobDetail.kmsOrderStatusID = KMSOrderStatus.WAITING;
                kmsPrintJobDetail.printerHostID = kitchenOrderDeliveryMechanism.getPrinterHostID();
                kmsPrintJobDetail.productID = product.getProductID();
                kmsPrintJobDetail.quantity = product.getQuantity();
                kmsPrintJobDetail.isModifier = product.getIsModifier();
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build the kitchen management system print job detail in KMSPrintJobDetail.buildFromTransLineItem", log, Logger.LEVEL.ERROR);
        }

        return kmsPrintJobDetail;
    }

}