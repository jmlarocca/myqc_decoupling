package com.mmhayes.common.receiptGen.ReceiptData.printJobGen.detail;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-28 13:04:26 -0400 (Thu, 28 May 2020) $: Date of last commit
    $Rev: 11829 $: Revision of last commit
    Notes: Represents a detail in a KP print job.
*/

import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.kitchenOrderDeliveryMechanisms.KitchenOrderDeliveryMechanism;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.ITransLineItem;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.Product;
import com.mmhayes.common.utils.Logger;

/**
 * <p>Represents a detail in a KP print job.</p>
 *
 */
public class KPPrintJobDetail extends PrintJobDetail implements IPrintJobDetail {

    /**
     * <p>Builds a {@link KPPrintJobDetail} from the given {@link ITransLineItem}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kitchenOrderDeliveryMechanism The mechanism by which an order will be delivered to the kitchen represented by a {@link KitchenOrderDeliveryMechanism} object.
     * @param transLineItem The {@link ITransLineItem} that should be used to create the {@link KPPrintJobDetail}.
     * @return The {@link KPPrintJobDetail} built from the given {@link ITransLineItem}.
     */
    public static KPPrintJobDetail buildFromTransLineItem (String log, KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism, ITransLineItem transLineItem) {
        KPPrintJobDetail kpPrintJobDetail = null;

        try {
            // make sure the print job detail has a way of being sent to the kitchen
            if (kitchenOrderDeliveryMechanism == null) {
                Logger.logMessage("The mechanism by which an order will be delivered to the kitchen can't be null in KPPrintJobDetail.buildFromTransLineItem!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure the print job detail has a valid printer
            if (kitchenOrderDeliveryMechanism.getPrinterID() <= 0) {
                Logger.logMessage("The ID of the printer for the print job detail is invalid in KPPrintJobDetail.buildFromTransLineItem, the " +
                        "printer ID should be greater than 0!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have a trans line item to create the print job detail
            if (transLineItem == null) {
                Logger.logMessage("The transaction line item passed to KPPrintJobDetail.buildFromTransLineItem can't be null!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // create the print job detail
            if (transLineItem instanceof Product) {
                // cast the ITransLineItem to a Product
                Product product = ((Product) transLineItem);
                kpPrintJobDetail = new KPPrintJobDetail();
                kpPrintJobDetail.transLineItemID = product.getPaTransLineItemID();
                kpPrintJobDetail.printerID = kitchenOrderDeliveryMechanism.getPrinterID();
                kpPrintJobDetail.printStatusID = PrintJobStatusType.WAITING.getTypeID();
                kpPrintJobDetail.printerHostID = kitchenOrderDeliveryMechanism.getPrinterHostID();
                kpPrintJobDetail.productID = product.getProductID();
                kpPrintJobDetail.quantity = product.getQuantity();
                kpPrintJobDetail.isModifier = product.getIsModifier();
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build the kitchen printer print job detail in KPPrintJobDetail.buildFromTransLineItem", log, Logger.LEVEL.ERROR);
        }

        return kpPrintJobDetail;
    }

}