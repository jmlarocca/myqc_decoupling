package com.mmhayes.common.receiptGen.ReceiptData.printJobGen;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-28 13:04:26 -0400 (Thu, 28 May 2020) $: Date of last commit
    $Rev: 11829 $: Revision of last commit
    Notes: Stores the header information and details for a print job.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.ReceiptData.TransactionData;
import com.mmhayes.common.receiptGen.ReceiptData.TransactionHeaderData;
import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.detail.*;
import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.header.PrintJobHeader;
import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.kitchenOrderDeliveryMechanisms.KitchenOrderDeliveryMechanism;
import com.mmhayes.common.receiptGen.ReceiptData.printJobGen.kitchenOrderDeliveryMechanisms.KitchenOrderDeliveryMechanismType;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.ITransLineItem;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.Product;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep.IKitchenPrep;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep.KDSPrep;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep.KMSPrep;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep.KPPrep;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;

/**
 * <p>Stores the header information and details for a print job.</p>
 *
 */
public class PrintJobInfo {

    // private member variables of a PrintJobInfo
    private String log;
    private DataManager dataManager;
    private TransactionData transactionData;
    private ArrayList<KitchenOrderDeliveryMechanism> kitchenOrderDeliveryMechanisms;
    private HashMap<Integer, ArrayList<KitchenOrderDeliveryMechanism>> kodmByType;
    private PrintJobHeader printJobHeader;
    private ArrayList<IPrintJobDetail> printJobDetails;

    /**
     * <p>Constructor for a {@link PrintJobInfo}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param transactionData The {@link TransactionData} to use to determine the print jobs to create.
     */
    public PrintJobInfo (String log, DataManager dataManager, TransactionData transactionData) {
        this.log = log;
        this.dataManager = dataManager;
        this.transactionData = transactionData;
        setKitchenOrderDeliveryMechanisms();
        setPrintJobHeader();
        setPrintJobDetails();
    }

    /**
     * <p>Sets an {@link ArrayList} of {@link KitchenOrderDeliveryMechanism} corresponding to the printers and displays within the revenue center in which the transaction took place.</p>
     *
     */
    @SuppressWarnings("unchecked")
    private void setKitchenOrderDeliveryMechanisms () {

        try {
            // make sure we have a valid DataManager
            if (dataManager == null) {
                Logger.logMessage("DataManager can't ne null in PrintJobInfo.setKitchenOrderDeliveryMechanisms, unable to determine the printers " +
                        "and stations within the revenue center in which the transaction took place!", log, Logger.LEVEL.ERROR);
                return;
            }

            // make sure we have valid transaction data
            if (transactionData == null) {
                Logger.logMessage("No transaction data found in PrintJobInfo.setKitchenOrderDeliveryMechanisms, unable to determine the printers " +
                        "and stations within the revenue center in which the transaction took place!", log, Logger.LEVEL.ERROR);
                return;
            }

            // make sure the transaction data contains valid header information
            if (transactionData.getTxnHdrData() == null) {
                Logger.logMessage("No transaction header data found in PrintJobInfo.setKitchenOrderDeliveryMechanisms, unable to determine the printers " +
                        "and stations within the revenue center in which the transaction took place!", log, Logger.LEVEL.ERROR);
                return;
            }

            // make sure we know which revenue center the transaction took place in
            if (transactionData.getTxnHdrData().getRevenueCenterID() <= 0) {
                Logger.logMessage("Can't determine the revenue center in which the transaction took place in PrintJobInfo.setKitchenOrderDeliveryMechanisms, unable to " +
                        "determine the printers and stations within the revenue center in which the transaction took place!", log, Logger.LEVEL.ERROR);
                return;
            }

            // get the ID of the revenue center in which the transaction took place
            int revenueCenterID = transactionData.getTxnHdrData().getRevenueCenterID();

            // get all printers and stations within the revenue center
            ArrayList<KitchenOrderDeliveryMechanism> tempKitchenOrderDeliveryMechanisms = new ArrayList<>();
            ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(dataManager.parameterizedExecuteQuery("data.ReceiptGen.GetAllOrderDeliveryMechanisms", new Object[]{revenueCenterID}, true));
            for (HashMap hm : queryRes) {
                KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism = KitchenOrderDeliveryMechanism.buildFromHM(log, hm);
                if (kitchenOrderDeliveryMechanism != null) {
                    tempKitchenOrderDeliveryMechanisms.add(kitchenOrderDeliveryMechanism);
                }
            }
            if (!DataFunctions.isEmptyCollection(tempKitchenOrderDeliveryMechanisms)) {
                this.kitchenOrderDeliveryMechanisms = tempKitchenOrderDeliveryMechanisms;
                // sort the kitchen order delivery mechanisms by kitchen order delivery mechanism type
                sortKitchenOrderDeliveryMechanismsByType();
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // determine the ID of the transaction the print job is for
            BigDecimal transactionID = null;
            if ((transactionData != null) && (transactionData.getTxnHdrData() != null) && (transactionData.getTxnHdrData().getPaTransactionID() != null)) {
                transactionID = transactionData.getTxnHdrData().getPaTransactionID();
            }
            Logger.logMessage(String.format("There was a problem trying determine the revenue center in which the transaction %s took place in PrintJobInfo.setKitchenOrderDeliveryMechanisms",
                    Objects.toString((transactionID != null ? transactionID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Sorts the {@link ArrayList} of {@link KitchenOrderDeliveryMechanism} into a {@link HashMap} whose key is an {@link Integer} representing
     * the type of the kitchen order delivery mechanism and whose value is an {@link ArrayList} of {@link KitchenOrderDeliveryMechanism} corresponding to
     * {@link KitchenOrderDeliveryMechanism} that are of the type stored in the {@link HashMap} key.</p>
     *
     */
    private void sortKitchenOrderDeliveryMechanismsByType () {

        try {
            if (!DataFunctions.isEmptyCollection(this.kitchenOrderDeliveryMechanisms)) {
                this.kodmByType = new HashMap<>();
                ArrayList<KitchenOrderDeliveryMechanism> commonTypeKitchenOrderDeliveryMechanisms;
                for (KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism : this.kitchenOrderDeliveryMechanisms) {
                    int kitchenOrderDeliveryMechanismType = kitchenOrderDeliveryMechanism.getKitchenOrderDeliveryMechanismType();
                    if (this.kodmByType.containsKey(kitchenOrderDeliveryMechanismType)) {
                        commonTypeKitchenOrderDeliveryMechanisms = this.kodmByType.get(kitchenOrderDeliveryMechanismType);
                    }
                    else {
                        commonTypeKitchenOrderDeliveryMechanisms = new ArrayList<>();
                    }
                    commonTypeKitchenOrderDeliveryMechanisms.add(kitchenOrderDeliveryMechanism);
                    this.kodmByType.put(kitchenOrderDeliveryMechanismType, commonTypeKitchenOrderDeliveryMechanisms);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // determine the ID of the transaction the print job is for
            BigDecimal transactionID = null;
            if ((transactionData != null) && (transactionData.getTxnHdrData() != null) && (transactionData.getTxnHdrData().getPaTransactionID() != null)) {
                transactionID = transactionData.getTxnHdrData().getPaTransactionID();
            }
            Logger.logMessage(String.format("There was a problem trying to sort the kitchen order delivery mechanisms by type for the transaction %s in PrintJobInfo.sortKitchenOrderDeliveryMechanismsByType",
                    Objects.toString((transactionID != null ? transactionID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Sets the {@link PrintJobHeader} information for a print job.</p>
     *
     */
    private void setPrintJobHeader () {

        try {
            // make sure we have valid transaction data
            if (transactionData == null) {
                Logger.logMessage("No transaction data found in PrintJobInfo.setPrintJobHeader, unable to create a print job header!", log, Logger.LEVEL.ERROR);
                return;
            }

            // make sure the transaction data contains valid header information
            if (transactionData.getTxnHdrData() == null) {
                Logger.logMessage("No transaction header data found in PrintJobInfo.setPrintJobHeader, unable to create a print job header!", log, Logger.LEVEL.ERROR);
                return;
            }

            // get the TransactionHeaderData
            TransactionHeaderData txnHeaderData = transactionData.getTxnHdrData();
            PrintJobHeader tempPrintJobHeader = PrintJobHeader.buildFromTxnHeaderData(log, dataManager, txnHeaderData);
            if (tempPrintJobHeader != null) {
                this.printJobHeader = tempPrintJobHeader;
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // determine the ID of the transaction the print job is for
            BigDecimal transactionID = null;
            if ((transactionData != null) && (transactionData.getTxnHdrData() != null) && (transactionData.getTxnHdrData().getPaTransactionID() != null)) {
                transactionID = transactionData.getTxnHdrData().getPaTransactionID();
            }
            Logger.logMessage(String.format("There was a problem trying to create a print job header for the transaction with an ID of %s in PrintJobInfo.setPrintJobHeader",
                    Objects.toString((transactionID != null ? transactionID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Sets an {@link ArrayList} of {@link IPrintJobDetail} corresponding to the details in the print job.</p>
     *
     */
    private void setPrintJobDetails () {

        try {
            // make sure we have valid transaction data
            if (transactionData == null) {
                Logger.logMessage("No transaction data found in PrintJobInfo.setPrintJobDetails, unable to create print job details!", log, Logger.LEVEL.ERROR);
                return;
            }

            // make sure the transaction data contains valid line items
            if (DataFunctions.isEmptyCollection(transactionData.getTxnLineItems())) {
                Logger.logMessage("No transaction line item data found in PrintJobInfo.setPrintJobDetails, unable to create print job details!", log, Logger.LEVEL.ERROR);
                return;
            }

            // get the transaction line items
            ArrayList<ITransLineItem> transLineItems = transactionData.getTxnLineItems();

            // create and set the print job details
            ArrayList<IPrintJobDetail> tempPrintJobDetails = createPrintJobDetails(transLineItems);
            if (!DataFunctions.isEmptyCollection(tempPrintJobDetails)) {
                this.printJobDetails = tempPrintJobDetails;
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // determine the ID of the transaction the print job is for
            BigDecimal transactionID = null;
            if ((transactionData != null) && (transactionData.getTxnHdrData() != null) && (transactionData.getTxnHdrData().getPaTransactionID() != null)) {
                transactionID = transactionData.getTxnHdrData().getPaTransactionID();
            }
            Logger.logMessage(String.format("There was a problem trying to create the print job details for the transaction with an ID of %s in PrintJobInfo.setPrintJobDetails",
                    Objects.toString((transactionID != null ? transactionID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Builds an {@link ArrayList} of {@link IPrintJobDetail} corresponding to the print jobs details in the print job.</p>
     *
     * @param transLineItems An {@link ArrayList} of {@link ITransLineItem} corresponding to the line items in the transaction.
     * @return An {@link ArrayList} of {@link IPrintJobDetail} corresponding to the print jobs details in the print job.
     */
    @SuppressWarnings("Convert2streamapi")
    private ArrayList<IPrintJobDetail> createPrintJobDetails (ArrayList<ITransLineItem> transLineItems) {
        ArrayList<IPrintJobDetail> printJobDetails = new ArrayList<>();

        try {
            // make sure there are printers/stations to send the print job details to
            if (DataFunctions.isEmptyMap(kodmByType)) {
                Logger.logMessage("Unable to determine a mechanism by which to send an order to the kitchen in PrintJobInfo.createPrintJobDetails, " +
                        "unable to create the print job details!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // get the products within the transaction
            ArrayList<Product> products = getProductTransLineItems(transLineItems);

            // make sure there are products within the transaction
            if (DataFunctions.isEmptyCollection(products)) {
                Logger.logMessage("No products were found within the transaction in PrintJobInfo.createPrintJobDetails, unable to create the print job details!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // get the dining option if there is one
            Product diningOption = getDiningOption(products);
            if (diningOption != null) {
                products = removeDiningOptionFromProducts(diningOption, products);

                // make sure we still have valid products
                if (DataFunctions.isEmptyCollection(products)) {
                    Logger.logMessage("No products were found within the transaction in PrintJobInfo.createPrintJobDetails after " +
                            "removing the dining option, unable to create the print job details!", log, Logger.LEVEL.ERROR);
                    return null;
                }
            }

            // create a factory to create the print job details
            PrintJobDetailFactory printJobDetailFactory = new PrintJobDetailFactory();

            // iterate through the product line items and create print job details for each product
            for (Product product : products) {
                if (product != null) {
                    ArrayList<IPrintJobDetail> printJobDetailsForProduct = createPrintJobDetailsForProduct(product, printJobDetailFactory);
                    if (!DataFunctions.isEmptyCollection(printJobDetailsForProduct)) {
                        printJobDetails.addAll(printJobDetailsForProduct);
                    }
                }
            }

            // create print job details for the dining option
            if (diningOption != null) {
                ArrayList<IPrintJobDetail> diningOptionPrintJobDetails = createDiningOptionPrintJobDetails(diningOption, printJobDetailFactory, printJobDetails);
                if (!DataFunctions.isEmptyCollection(diningOptionPrintJobDetails)) {
                    printJobDetails.addAll(diningOptionPrintJobDetails);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // determine the ID of the transaction the print job is for
            BigDecimal transactionID = null;
            if ((transactionData != null) && (transactionData.getTxnHdrData() != null) && (transactionData.getTxnHdrData().getPaTransactionID() != null)) {
                transactionID = transactionData.getTxnHdrData().getPaTransactionID();
            }
            Logger.logMessage(String.format("There was a problem trying to build the print job details for the transaction with an ID of %s in PrintJobInfo.createPrintJobDetails",
                    Objects.toString((transactionID != null ? transactionID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return printJobDetails;
    }

    /**
     * <p>Iterates through the trans line items and retrieves the trans line items that are products.</p>
     *
     * @param transLineItems The {@link ArrayList} of {@link ITransLineItem} to search for products.
     * @return An {@link ArrayList} of {@link Product} found within the given {@link ArrayList} of {@link ITransLineItem}.
     */
    @SuppressWarnings("Convert2streamapi")
    private ArrayList<Product> getProductTransLineItems (ArrayList<ITransLineItem> transLineItems) {
        ArrayList<Product> products = new ArrayList<>();

        if (!DataFunctions.isEmptyCollection(transLineItems)) {
            for (ITransLineItem transLineItem : transLineItems) {
                if ((transLineItem != null) && (transLineItem instanceof Product)) {
                    products.add(((Product) transLineItem));
                }
            }
        }
        else {
            Logger.logMessage("The transaction line items passed to PrintJobInfo.getProductTransLineItems can't be null or empty!", log, Logger.LEVEL.ERROR);
        }

        return products;
    }

    /**
     * <p>Iterates through the given products to find a dining option if there is one.</p>
     *
     * @param products An {@link ArrayList} of {@link Product} corresponding to the products within a transaction.
     * @return The dining option {@link Product}.
     */
    private Product getDiningOption (ArrayList<Product> products) {
        Product diningOption = null;

        if (!DataFunctions.isEmptyCollection(products)) {
            for (Product product : products) {
                if (product.getIsDiningOption()) {
                    diningOption = product;
                    break; // dining option found, no need to keep processing
                }
            }
        }

        return diningOption;
    }

    /**
     * <p>Iterates through the products and removes the dining option from those products.</p>
     *
     * @param diningOption The dining option {@link Product} to remove.
     * @param products An {@link ArrayList} of {@link Product} corresponding to the products within a transaction.
     * @return An {@link ArrayList} of {@link Product} corresponding to the products within a transaction with the dining option removed.
     */
    private ArrayList<Product> removeDiningOptionFromProducts (Product diningOption, ArrayList<Product> products) {

        if ((diningOption != null) && (!DataFunctions.isEmptyCollection(products))) {
            Iterator<Product> productIterator = products.iterator();
            while (productIterator.hasNext()) {
                Product currentProduct = productIterator.next();
                if (diningOption.equals(currentProduct)) {
                    productIterator.remove();
                    break; // the dining option has been removed, no need to keep processing
                }
            }
        }

        return products;
    }

    /**
     * <p>Creates print job details for the given {@link Product}.</p>
     *
     * @param product The {@link Product} to create print job details for.
     * @param printJobDetailFactory The {@link PrintJobDetailFactory} use use to create an {@link IPrintJobDetail} instance.
     * @return An {@link ArrayList} of {@link IPrintJobDetail} corresponding to the list of print job details for the given {@link Product}.
     */
    private ArrayList<IPrintJobDetail> createPrintJobDetailsForProduct (Product product, PrintJobDetailFactory printJobDetailFactory) {
        ArrayList<IPrintJobDetail> printJobDetailsForProduct = new ArrayList<>();

        try {
            // make sure there are printers/stations to send the print job details to
            if (DataFunctions.isEmptyMap(kodmByType)) {
                Logger.logMessage("Unable to determine a mechanism by which to send an order to the kitchen in PrintJobInfo.createPrintJobDetailsForProduct, " +
                        "unable to create the print job details!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure there is a valid product to create print jobs for
            if (product == null) {
                Logger.logMessage("The product passed to PrintJobInfo.createPrintJobDetailsForProduct can't be null!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have valid transaction data
            if (transactionData == null) {
                Logger.logMessage("No transaction data found in PrintJobInfo.createPrintJobDetailsForProduct, unable to create print job details for the product!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure the transaction data contains valid header information
            if (transactionData.getTxnHdrData() == null) {
                Logger.logMessage("No transaction header data found in PrintJobInfo.createPrintJobDetailsForProduct, unable to create print job details for the product!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // get the TransactionHeaderData
            TransactionHeaderData txnHeaderData = transactionData.getTxnHdrData();
            // use the transaction header data to determine whether or not the transaction was made remotely
            boolean isRemoteTransaction = ((txnHeaderData.getOnlineOrder()) || (txnHeaderData.getRemoteOrderTerminal()));
            // use a kitchen order delivery mechanism to determine whether or not expeditors should serve as remotes
            boolean remoteOrdersUseExpeditor = kodmByType.entrySet().stream().findFirst().orElse(null).getValue().get(0).getRemoteOrdersUseExpeditor();

            // determine print job details for prep printers and stations
            ArrayList<IPrintJobDetail> prepPrintJobDetailsForProduct = createPrepPrintJobDetailsForProduct(printJobDetailFactory, product);
            if (!DataFunctions.isEmptyCollection(prepPrintJobDetailsForProduct)) {
                printJobDetailsForProduct.addAll(prepPrintJobDetailsForProduct);
            }

            if (isRemoteTransaction) {
                if (remoteOrdersUseExpeditor) {
                    // determine any print jobs that should be sent to kitchen expeditors serving as remotes
                    ArrayList<IPrintJobDetail> remotePrintJobDetailsForProduct = createRemotePrintJobDetailsForProduct(printJobDetailFactory, product, true);
                    if (!DataFunctions.isEmptyCollection(remotePrintJobDetailsForProduct)) {
                        printJobDetailsForProduct.addAll(remotePrintJobDetailsForProduct);
                    }
                }
                else {
                    // determine any print jobs that should be sent to kitchen expeditors
                    ArrayList<IPrintJobDetail> expeditorPrintJobDetailsForProduct = createExpeditorPrintJobDetailsForProduct(printJobDetailFactory, product);
                    if (!DataFunctions.isEmptyCollection(expeditorPrintJobDetailsForProduct)) {
                        printJobDetailsForProduct.addAll(expeditorPrintJobDetailsForProduct);
                    }
                    // determine any print jobs that should be sent to kitchen remotes
                    ArrayList<IPrintJobDetail> remotePrintJobDetailsForProduct = createRemotePrintJobDetailsForProduct(printJobDetailFactory, product, false);
                    if (!DataFunctions.isEmptyCollection(remotePrintJobDetailsForProduct)) {
                        printJobDetailsForProduct.addAll(remotePrintJobDetailsForProduct);
                    }
                }
            }
            else {
                // determine any print jobs that should be sent to kitchen expeditors
                ArrayList<IPrintJobDetail> expeditorPrintJobDetailsForProduct = createExpeditorPrintJobDetailsForProduct(printJobDetailFactory, product);
                if (!DataFunctions.isEmptyCollection(expeditorPrintJobDetailsForProduct)) {
                    printJobDetailsForProduct.addAll(expeditorPrintJobDetailsForProduct);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // determine the ID of the transaction the print job is for
            BigDecimal transactionID = null;
            if ((transactionData != null) && (transactionData.getTxnHdrData() != null) && (transactionData.getTxnHdrData().getPaTransactionID() != null)) {
                transactionID = transactionData.getTxnHdrData().getPaTransactionID();
            }
            Logger.logMessage(String.format("There was a problem trying to build the print job details for the product %s within the transaction with an ID of %s in PrintJobInfo.createPrintJobDetailsForProduct",
                    Objects.toString((product != null ? product.getName() : "N/A"), "N/A"),
                    Objects.toString((transactionID != null ? transactionID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return printJobDetailsForProduct;
    }

    /**
     * <p>Determines any print job details that need to be created for prep printers and stations for the given {@link Product}.</p>
     *
     * @param printJobDetailFactory The {@link PrintJobDetailFactory} that will be used to build print job details.
     * @param product The {@link Product} to use to determine any print job details for prep printers and stations.
     * @return An {@link ArrayList} of {@link IPrintJobDetail} corresponding to any print job details for the given {@link Product} that should be sent to prep printers and stations.
     */
    @SuppressWarnings("Duplicates")
    private ArrayList<IPrintJobDetail> createPrepPrintJobDetailsForProduct (PrintJobDetailFactory printJobDetailFactory, Product product) {
        ArrayList<IPrintJobDetail> prepPrintJobDetailsForProduct = new ArrayList<>();

        try {
            // make sure we have a valid factory
            if (printJobDetailFactory == null) {
                Logger.logMessage("The print job detail factory passed to PrintJobInfo.createPrepPrintJobDetailsForProduct can't be null!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have a valid product
            if (product == null) {
                Logger.logMessage("The product passed to PrintJobInfo.createPrepPrintJobDetailsForProduct can't be null!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // check if the product has any mapped prep printers or stations
            ArrayList<IKitchenPrep> kitchenPreps = product.getKitchenPreps();
            if (!DataFunctions.isEmptyCollection(kitchenPreps)) {
                for (IKitchenPrep kitchenPrep : kitchenPreps) {
                    // try to get the kitchen order delivery mechanism equivalent of the kitchen prep
                    KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism = null;
                    if (kitchenPrep != null) {
                        kitchenOrderDeliveryMechanism = getKODMEquivalentForKitchenPrep(kitchenPrep);
                    }
                    if (kitchenOrderDeliveryMechanism != null) {
                        // create the print job(s)
                        if (kitchenOrderDeliveryMechanism.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KP_PREP) {
                            IPrintJobDetail kpPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KP, kitchenOrderDeliveryMechanism, null, product);
                            if (kpPrintJobDetail != null) {
                                prepPrintJobDetailsForProduct.add(kpPrintJobDetail);
                            }
                        }
                        else if (kitchenOrderDeliveryMechanism.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KDS_PREP) {
                            // check if the product should be hidden on a KDS expeditor
                            String hiddenStation = "";
                            if (!product.getPrintsOnExpeditor()) {
                                hiddenStation = String.valueOf(kitchenOrderDeliveryMechanism.getStationID());
                            }
                            IPrintJobDetail kdsPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KDS, kitchenOrderDeliveryMechanism, hiddenStation, product);
                            if (kdsPrintJobDetail != null) {
                                prepPrintJobDetailsForProduct.add(kdsPrintJobDetail);
                            }
                        }
                        else if (kitchenOrderDeliveryMechanism.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KMS_PREP) {
                            IPrintJobDetail kmsPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, kitchenOrderDeliveryMechanism, null, product);
                            if (kmsPrintJobDetail != null) {
                                prepPrintJobDetailsForProduct.add(kmsPrintJobDetail);
                            }
                            // check for any mirrored stations
                            ArrayList<KitchenOrderDeliveryMechanism> mirrorStations = getKMSMirrorOrderDeliveryMechanisms(KitchenOrderDeliveryMechanismType.KMS_MIRROR_PREP, kitchenOrderDeliveryMechanism);
                            if (!DataFunctions.isEmptyCollection(mirrorStations)) {
                                // create a print job detail for each mirror station
                                for (KitchenOrderDeliveryMechanism mirrorStation : mirrorStations) {
                                    IPrintJobDetail kmsMirrorPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, mirrorStation, null, product);
                                    if (kmsMirrorPrintJobDetail != null) {
                                        prepPrintJobDetailsForProduct.add(kmsMirrorPrintJobDetail);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // determine the ID of the transaction the print job is for
            BigDecimal transactionID = null;
            if ((transactionData != null) && (transactionData.getTxnHdrData() != null) && (transactionData.getTxnHdrData().getPaTransactionID() != null)) {
                transactionID = transactionData.getTxnHdrData().getPaTransactionID();
            }
            Logger.logMessage(String.format("There was a problem trying to build the print job details that should be sent to prep printers " +
                    "and stations for the product %s within the transaction with an ID of %s in PrintJobInfo.createPrepPrintJobDetailsForProduct",
                    Objects.toString((product != null ? product.getName() : "N/A"), "N/A"),
                    Objects.toString((transactionID != null ? transactionID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return prepPrintJobDetailsForProduct;
    }

    /**
     * <p>Iterates through the kitchen order delivery mechanisms to find and equivalent {@link KitchenOrderDeliveryMechanism} to the given {@link IKitchenPrep}.</p>
     *
     * @param kitchenPrep The {@link IKitchenPrep} to find an equivalent {@link KitchenOrderDeliveryMechanism} for.
     * @return An equivalent {@link KitchenOrderDeliveryMechanism} for the given {@link IKitchenPrep}.
     */
    private KitchenOrderDeliveryMechanism getKODMEquivalentForKitchenPrep (IKitchenPrep kitchenPrep) {
        KitchenOrderDeliveryMechanism kodmEquivalent = null;

        try {
            if ((kitchenPrep != null) && (!DataFunctions.isEmptyMap(kodmByType))) {
                if (kitchenPrep instanceof KPPrep) {
                    KPPrep kpPrep = ((KPPrep) kitchenPrep);
                    ArrayList<KitchenOrderDeliveryMechanism> prepKODMs = new ArrayList<>();
                    if (kodmByType.containsKey(KitchenOrderDeliveryMechanismType.KP_PREP)) {
                        prepKODMs = kodmByType.get(KitchenOrderDeliveryMechanismType.KP_PREP);
                    }
                    if (!DataFunctions.isEmptyCollection(prepKODMs)) {
                        // iterate through the kitchen order delivery prep mechanisms and try to find an equivalent
                        for (KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism : prepKODMs) {
                            if ((kitchenOrderDeliveryMechanism != null) && (kitchenOrderDeliveryMechanism.getPrinterID() == kpPrep.getPrinterID())) {
                                kodmEquivalent = kitchenOrderDeliveryMechanism;
                                break; // the equivalent has been found, no need to keep processing
                            }
                        }
                    }
                }
                else if (kitchenPrep instanceof KDSPrep) {
                    KDSPrep kdsPrep = ((KDSPrep) kitchenPrep);
                    ArrayList<KitchenOrderDeliveryMechanism> prepKODMs = new ArrayList<>();
                    if (kodmByType.containsKey(KitchenOrderDeliveryMechanismType.KDS_PREP)) {
                        prepKODMs = kodmByType.get(KitchenOrderDeliveryMechanismType.KDS_PREP);
                    }
                    if (!DataFunctions.isEmptyCollection(prepKODMs)) {
                        // iterate through the kitchen order delivery prep mechanisms and try to find an equivalent
                        for (KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism : prepKODMs) {
                            if ((kitchenOrderDeliveryMechanism != null) && (kitchenOrderDeliveryMechanism.getPrinterID() == kdsPrep.getPrinterID())) {
                                kodmEquivalent = kitchenOrderDeliveryMechanism;
                                break; // the equivalent has been found, no need to keep processing
                            }
                        }
                    }
                }
                else if (kitchenPrep instanceof KMSPrep) {
                    KMSPrep kmsPrep = ((KMSPrep) kitchenPrep);
                    ArrayList<KitchenOrderDeliveryMechanism> prepKODMs = new ArrayList<>();
                    if (kodmByType.containsKey(KitchenOrderDeliveryMechanismType.KMS_PREP)) {
                        prepKODMs = kodmByType.get(KitchenOrderDeliveryMechanismType.KMS_PREP);
                    }
                    if (!DataFunctions.isEmptyCollection(prepKODMs)) {
                        // iterate through the kitchen order delivery prep mechanisms and try to find an equivalent
                        for (KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism : prepKODMs) {
                            if ((kitchenOrderDeliveryMechanism != null) && (kitchenOrderDeliveryMechanism.getKmsStationID() == kmsPrep.getKmsStationID())) {
                                kodmEquivalent = kitchenOrderDeliveryMechanism;
                                break; // the equivalent has been found, no need to keep processing
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("A problem occurred while trying to find a kitchen order delivery mechanism equivalent to the kitchen " +
                    "prep in PrintJobInfo.getKODMEquivalentForKitchenPrep", log, Logger.LEVEL.ERROR);
        }

        return kodmEquivalent;
    }

    /**
     * <p>Determines any print job details that need to be created for expeditor printers and stations for the given {@link Product}.</p>
     *
     * @param printJobDetailFactory The {@link PrintJobDetailFactory} that will be used to build print job details.
     * @param product The {@link Product} to use to determine any print job details for expeditor printers and stations.
     * @return An {@link ArrayList} of {@link IPrintJobDetail} corresponding to any print job details for the given {@link Product} that should be sent to expeditor printers and stations.
     */
    @SuppressWarnings("Duplicates")
    private ArrayList<IPrintJobDetail>  createExpeditorPrintJobDetailsForProduct (PrintJobDetailFactory printJobDetailFactory, Product product) {
        ArrayList<IPrintJobDetail> expeditorPrintJobDetailsForProduct = new ArrayList<>();

        try {
            // make sure there are printers/stations to send the print job details to
            if (DataFunctions.isEmptyMap(kodmByType)) {
                Logger.logMessage("Unable to determine a mechanism by which to send an order to the kitchen in PrintJobInfo.createExpeditorPrintJobDetailsForProduct!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have a valid factory
            if (printJobDetailFactory == null) {
                Logger.logMessage("The print job detail factory passed to PrintJobInfo.createExpeditorPrintJobDetailsForProduct can't be null!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have a valid product
            if (product == null) {
                Logger.logMessage("The product passed to PrintJobInfo.createExpeditorPrintJobDetailsForProduct can't be null!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure the product should show on the expeditor, if it's a modifier it should follow the parent product
            boolean showProductOnExpeditor = true;
            if ((!product.getPrintsOnExpeditor()) || ((product.getIsModifier()) && ((product.getParentProduct() != null) && (!product.getParentProduct().getPrintsOnExpeditor())))) {
                showProductOnExpeditor = false;
            }

            if (showProductOnExpeditor) {
                ArrayList<KitchenOrderDeliveryMechanism> kpExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KP_EXPEDITOR);
                if (!DataFunctions.isEmptyCollection(kpExpeditors)) {
                    for (KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism : kpExpeditors) {
                        IPrintJobDetail kpPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KP, kitchenOrderDeliveryMechanism, null, product);
                        if (kpPrintJobDetail != null) {
                            expeditorPrintJobDetailsForProduct.add(kpPrintJobDetail);
                        }
                    }
                }
                ArrayList<KitchenOrderDeliveryMechanism> kdsExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KDS_EXPEDITOR);
                if (!DataFunctions.isEmptyCollection(kdsExpeditors)) {
                    // check if the product was on a kds prep station, if so, we don't need to send it to the expeditor it gets picked up automatically
                    if (!productSentToKDSPrepStation(product)) {
                        for (KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism : kdsExpeditors) {
                            IPrintJobDetail kdsPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KDS, kitchenOrderDeliveryMechanism, null, product);
                            if (kdsPrintJobDetail != null) {
                                expeditorPrintJobDetailsForProduct.add(kdsPrintJobDetail);
                            }
                        }
                    }
                }
                ArrayList<KitchenOrderDeliveryMechanism> kmsExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KMS_EXPEDITOR);
                if (!DataFunctions.isEmptyCollection(kmsExpeditors)) {
                    for (KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism : kmsExpeditors) {
                        IPrintJobDetail kmsPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, kitchenOrderDeliveryMechanism, null, product);
                        if (kmsPrintJobDetail != null) {
                            expeditorPrintJobDetailsForProduct.add(kmsPrintJobDetail);
                        }
                        // check for any mirrored stations
                        ArrayList<KitchenOrderDeliveryMechanism> mirrorStations = getKMSMirrorOrderDeliveryMechanisms(KitchenOrderDeliveryMechanismType.KMS_MIRROR_EXPEDITOR, kitchenOrderDeliveryMechanism);
                        if (!DataFunctions.isEmptyCollection(mirrorStations)) {
                            // create a print job detail for each mirror station
                            for (KitchenOrderDeliveryMechanism mirrorStation : mirrorStations) {
                                IPrintJobDetail kmsMirrorPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, mirrorStation, null, product);
                                if (kmsMirrorPrintJobDetail != null) {
                                    expeditorPrintJobDetailsForProduct.add(kmsMirrorPrintJobDetail);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // determine the ID of the transaction the print job is for
            BigDecimal transactionID = null;
            if ((transactionData != null) && (transactionData.getTxnHdrData() != null) && (transactionData.getTxnHdrData().getPaTransactionID() != null)) {
                transactionID = transactionData.getTxnHdrData().getPaTransactionID();
            }
            Logger.logMessage(String.format("There was a problem trying to build the print job details that should be sent to expeditor printers " +
                    "and stations for the product %s within the transaction with an ID of %s in PrintJobInfo.createExpeditorPrintJobDetailsForProduct",
                    Objects.toString((product != null ? product.getName() : "N/A"), "N/A"),
                    Objects.toString((transactionID != null ? transactionID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return expeditorPrintJobDetailsForProduct;
    }

    /**
     * <p>Checks whether or not the given {@link Product} will be displayed on a KDS prep station.</p>
     *
     * @param product The {@link Product} to check.
     * @return Whether or not the given {@link Product} will be displayed on a KDS prep station.
     */
    @SuppressWarnings("Convert2streamapi")
    private boolean productSentToKDSPrepStation (Product product) {
        boolean sentToKDSStation = false;

        try {
            // make sure we have a valid product
            if (product == null) {
                Logger.logMessage("The product passed to PrintJobInfo.productSentToKDSPrepStation can't be null!", log, Logger.LEVEL.ERROR);
                return false;
            }

            // check if there are any KDS kitchen preps mapped to the product
            ArrayList<IKitchenPrep> kitchenPreps = product.getKitchenPreps();
            ArrayList<KDSPrep> kdsPreps = new ArrayList<>();
            for (IKitchenPrep kitchenPrep : kitchenPreps) {
                if (kitchenPrep instanceof KDSPrep) {
                    kdsPreps.add(((KDSPrep) kitchenPrep));
                }
            }

            // check if there is a kitchen order delivery mechanism for a KDS prep, if so then we know the product will display on the expeditor
            if (!DataFunctions.isEmptyCollection(kdsPreps)) {
                for (KDSPrep kdsPrep : kdsPreps) {
                    if (getKODMEquivalentForKitchenPrep(kdsPrep) != null) {
                        sentToKDSStation = true;
                        break; // we know the product will be on a KDS prep station no need to keep processing
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the product %s has been sent to a KDS prep station in PrintJobInfo.productSentToKDSPrepStation",
                    Objects.toString((product != null ? product.getName() : "N/A"), "N/A")));
        }

        return sentToKDSStation;
    }

    /**
     * <p>Determines any print job details that need to be created for remote order printers and stations for the given {@link Product}.</p>
     *
     * @param printJobDetailFactory The {@link PrintJobDetailFactory} that will be used to build print job details.
     * @param product The {@link Product} to use to determine any print job details for remote order printers and stations.
     * @param remoteOrdersUseExpeditor Whether or not remote orders should be sent to the expeditor printer or station.
     * @return An {@link ArrayList} of {@link IPrintJobDetail} corresponding to any print job details for the given {@link Product} that should be sent to remote order printers and stations.
     */
    @SuppressWarnings("Duplicates")
    private ArrayList<IPrintJobDetail> createRemotePrintJobDetailsForProduct (PrintJobDetailFactory printJobDetailFactory, Product product, boolean remoteOrdersUseExpeditor) {
        ArrayList<IPrintJobDetail> remotePrintJobDetailsForProduct = new ArrayList<>();

        try {
            // make sure there are printers/stations to send the print job details to
            if (DataFunctions.isEmptyMap(kodmByType)) {
                Logger.logMessage("Unable to determine a mechanism by which to send an order to the kitchen in PrintJobInfo.createRemotePrintJobDetailsForProduct!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have a valid factory
            if (printJobDetailFactory == null) {
                Logger.logMessage("The print job detail factory passed to PrintJobInfo.createRemotePrintJobDetailsForProduct can't be null!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have a valid product
            if (product == null) {
                Logger.logMessage("The product passed to PrintJobInfo.createRemotePrintJobDetailsForProduct can't be null!", log, Logger.LEVEL.ERROR);
                return null;
            }

            if (remoteOrdersUseExpeditor) {
                // send the product to the expeditor printer or station
                ArrayList<KitchenOrderDeliveryMechanism> kpRemoteExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KP_REMOTE_EXPEDITOR);
                if (!DataFunctions.isEmptyCollection(kpRemoteExpeditors)) {
                    for (KitchenOrderDeliveryMechanism kpRemoteExpeditor : kpRemoteExpeditors) {
                        IPrintJobDetail kpPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KP, kpRemoteExpeditor, null, product);
                        if (kpPrintJobDetail != null) {
                            remotePrintJobDetailsForProduct.add(kpPrintJobDetail);
                        }
                    }
                }
                ArrayList<KitchenOrderDeliveryMechanism> kdsRemoteExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KDS_REMOTE_EXPEDITOR);
                if (!DataFunctions.isEmptyCollection(kdsRemoteExpeditors)) {
                    // check if the product was on a kds prep station, if so, we don't need to send it to the expeditor it gets picked up automatically
                    if (!productSentToKDSPrepStation(product)) {
                        for (KitchenOrderDeliveryMechanism kdsRemoteExpeditor : kdsRemoteExpeditors) {
                            IPrintJobDetail kdsPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KDS, kdsRemoteExpeditor, null, product);
                            if (kdsPrintJobDetail != null) {
                                remotePrintJobDetailsForProduct.add(kdsPrintJobDetail);
                            }    
                        }
                    }
                }
                ArrayList<KitchenOrderDeliveryMechanism> kmsRemoteExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KMS_REMOTE_EXPEDITOR);
                if (!DataFunctions.isEmptyCollection(kmsRemoteExpeditors)) {
                    for (KitchenOrderDeliveryMechanism kmsRemoteExpeditor : kmsRemoteExpeditors) {
                        IPrintJobDetail kmsPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, kmsRemoteExpeditor, null, product);
                        if (kmsPrintJobDetail != null) {
                            remotePrintJobDetailsForProduct.add(kmsPrintJobDetail);
                        }
                        // check for any mirrored stations
                        ArrayList<KitchenOrderDeliveryMechanism> mirrorStations = getKMSMirrorOrderDeliveryMechanisms(KitchenOrderDeliveryMechanismType.KMS_MIRROR_REMOTE_EXPEDITOR, kmsRemoteExpeditor);
                        if (!DataFunctions.isEmptyCollection(mirrorStations)) {
                            // create a print job detail for each mirror station
                            for (KitchenOrderDeliveryMechanism mirrorStation : mirrorStations) {
                                IPrintJobDetail kmsMirrorPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, mirrorStation, null, product);
                                if (kmsMirrorPrintJobDetail != null) {
                                    remotePrintJobDetailsForProduct.add(kmsMirrorPrintJobDetail);
                                }
                            }
                        }
                    }
                }
            }
            else {
                // send the product to the remote order printer or station
                ArrayList<KitchenOrderDeliveryMechanism> kpRemotes = kodmByType.get(KitchenOrderDeliveryMechanismType.KP_REMOTE);
                if (!DataFunctions.isEmptyCollection(kpRemotes)) {
                    for (KitchenOrderDeliveryMechanism kpRemote : kpRemotes) {
                        IPrintJobDetail kpPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KP, kpRemote, null, product);
                        if (kpPrintJobDetail != null) {
                            remotePrintJobDetailsForProduct.add(kpPrintJobDetail);
                        }
                    }
                }
                ArrayList<KitchenOrderDeliveryMechanism> kdsRemotes = kodmByType.get(KitchenOrderDeliveryMechanismType.KDS_REMOTE);
                if (!DataFunctions.isEmptyCollection(kdsRemotes)) {
                    for (KitchenOrderDeliveryMechanism kdsRemote : kdsRemotes) {
                        IPrintJobDetail kdsPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KDS, kdsRemote, null, product);
                        if (kdsPrintJobDetail != null) {
                            remotePrintJobDetailsForProduct.add(kdsPrintJobDetail);
                        }
                    }
                }
                ArrayList<KitchenOrderDeliveryMechanism> kmsRemotes = kodmByType.get(KitchenOrderDeliveryMechanismType.KMS_REMOTE);
                if (!DataFunctions.isEmptyCollection(kmsRemotes)) {
                    for (KitchenOrderDeliveryMechanism kmsRemote : kmsRemotes) {
                        IPrintJobDetail kmsPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, kmsRemote, null, product);
                        if (kmsPrintJobDetail != null) {
                            remotePrintJobDetailsForProduct.add(kmsPrintJobDetail);
                        }
                        // check for any mirrored stations
                        ArrayList<KitchenOrderDeliveryMechanism> mirrorStations = getKMSMirrorOrderDeliveryMechanisms(KitchenOrderDeliveryMechanismType.KMS_MIRROR_REMOTE, kmsRemote);
                        if (!DataFunctions.isEmptyCollection(mirrorStations)) {
                            // create a print job detail for each mirror station
                            for (KitchenOrderDeliveryMechanism mirrorStation : mirrorStations) {
                                IPrintJobDetail kmsMirrorPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, mirrorStation, null, product);
                                if (kmsMirrorPrintJobDetail != null) {
                                    remotePrintJobDetailsForProduct.add(kmsMirrorPrintJobDetail);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // determine the ID of the transaction the print job is for
            BigDecimal transactionID = null;
            if ((transactionData != null) && (transactionData.getTxnHdrData() != null) && (transactionData.getTxnHdrData().getPaTransactionID() != null)) {
                transactionID = transactionData.getTxnHdrData().getPaTransactionID();
            }
            Logger.logMessage(String.format("There was a problem trying to build the print job details that should be sent to remote order printers " +
                    "and stations for the product %s within the transaction with an ID of %s in PrintJobInfo.createRemotePrintJobDetailsForProduct",
                    Objects.toString((product != null ? product.getName() : "N/A"), "N/A"),
                    Objects.toString((transactionID != null ? transactionID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return remotePrintJobDetailsForProduct;
    }

    /**
     * <p>Gets mirror stations mapped to the given {@link KitchenOrderDeliveryMechanism}.</p>
     *
     * @param kitchenOrderDeliveryMechanismType The type of mirror stations to find.
     * @param kmsStation The {@link KitchenOrderDeliveryMechanism} to find mirroe stations for.
     * @return Am {@link ArrayList} of {@link KitchenOrderDeliveryMechanism} corresponding to KMS stations mirroring the given {@link KitchenOrderDeliveryMechanism}.
     */
    @SuppressWarnings("Convert2streamapi")
    private ArrayList<KitchenOrderDeliveryMechanism> getKMSMirrorOrderDeliveryMechanisms (int kitchenOrderDeliveryMechanismType, KitchenOrderDeliveryMechanism kmsStation) {
        ArrayList<KitchenOrderDeliveryMechanism> mappedMirrorStations = new ArrayList<>();

        try {
            // make sure there are printers/stations to send the print job details to
            if (DataFunctions.isEmptyMap(kodmByType)) {
                Logger.logMessage("Unable to determine a mechanism by which to send an order to the kitchen in PrintJobInfo.getKMSMirrorOrderDeliveryMechanisms, " +
                        "unable to create the print job details!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have a valid KMS station
            if (kmsStation == null) {
                Logger.logMessage("The KMS station passed to PrintJobInfo.getKMSMirrorOrderDeliveryMechanisms can't be null!", log, Logger.LEVEL.ERROR);
                return null;
            }

            ArrayList<KitchenOrderDeliveryMechanism> mirrorStations = kodmByType.get(kitchenOrderDeliveryMechanismType);
            if (!DataFunctions.isEmptyCollection(mirrorStations)) {
                for (KitchenOrderDeliveryMechanism mirrorStation : mirrorStations) {
                    if (mirrorStation.getKmsStationID() == kmsStation.getKmsStationID()) {
                        mappedMirrorStations.add(mirrorStation);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to find KMS stations mirroring the KMS station %s in PrintJobInfo.getKMSMirrorOrderDeliveryMechanisms",
                    Objects.toString((kmsStation != null ? kmsStation.getName() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return mappedMirrorStations;
    }

    /**
     * <p>Creates any print job details for the dining option within the transaction.</p>
     *
     * @param diningOption The dining option {@link Product} to create print job details for.
     * @param printJobDetailFactory The {@link PrintJobDetailFactory} use use to create an {@link IPrintJobDetail} instance.
     * @param existingPrintJobDetails An {@link ArrayList} of {@link IPrintJobDetail} corresponding to print job details that have been created for products that aren't dining options.
     * @return An {@link ArrayList} of {@link IPrintJobDetail} print job details corresponding to the print job details for the given dining option {@link Product}.
     */
    @SuppressWarnings({"Duplicates", "Convert2streamapi"})
    private ArrayList<IPrintJobDetail> createDiningOptionPrintJobDetails (Product diningOption, PrintJobDetailFactory printJobDetailFactory, ArrayList<IPrintJobDetail> existingPrintJobDetails) {
        ArrayList<IPrintJobDetail> diningOptionPrintJobDetails = new ArrayList<>();

        try {
            // make sure there are printers/stations to send the print job details to
            if (DataFunctions.isEmptyMap(kodmByType)) {
                Logger.logMessage("Unable to determine a mechanism by which to send an order to the kitchen in PrintJobInfo.createDiningOptionPrintJobDetails, " +
                        "unable to create the print job details!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure there is a valid dining option to create print jobs for
            if (diningOption == null) {
                Logger.logMessage("The dining option passed to PrintJobInfo.createDiningOptionPrintJobDetails can't be null!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure we have valid transaction data
            if (transactionData == null) {
                Logger.logMessage("No transaction data found in PrintJobInfo.createDiningOptionPrintJobDetails, unable to create print job details for the dining option!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // make sure the transaction data contains valid header information
            if (transactionData.getTxnHdrData() == null) {
                Logger.logMessage("No transaction header data found in PrintJobInfo.createDiningOptionPrintJobDetails, unable to create print job details for the dining option!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // get the TransactionHeaderData
            TransactionHeaderData txnHeaderData = transactionData.getTxnHdrData();
            // use the transaction header data to determine whether or not the transaction was made remotely
            boolean isRemoteTransaction = ((txnHeaderData.getOnlineOrder()) || (txnHeaderData.getRemoteOrderTerminal()));
            // use a kitchen order delivery mechanism to determine whether or not expeditors should serve as remotes
            boolean remoteOrdersUseExpeditor = kodmByType.entrySet().stream().findFirst().orElse(null).getValue().get(0).getRemoteOrdersUseExpeditor();

            // determine print job details for prep printers and stations
            ArrayList<IKitchenPrep> kitchenPreps = diningOption.getKitchenPreps();
            if (!DataFunctions.isEmptyCollection(kitchenPreps)) {
                for (IKitchenPrep kitchenPrep : kitchenPreps) {
                    KitchenOrderDeliveryMechanism kitchenOrderDeliveryMechanism = getKODMEquivalentForKitchenPrep(kitchenPrep);
                    if (kitchenOrderDeliveryMechanism != null) {
                        if ((kitchenOrderDeliveryMechanism.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KP_PREP)
                                && (isThereAProductOnPrinterOrStation(diningOption, kitchenOrderDeliveryMechanism, existingPrintJobDetails))) {
                            IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KP, kitchenOrderDeliveryMechanism, null, diningOption);
                            if (diningOptionPrintJobDetail != null) {
                                diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                            }
                        }
                        else if ((kitchenOrderDeliveryMechanism.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KDS_PREP)
                                && (isThereAProductOnPrinterOrStation(diningOption, kitchenOrderDeliveryMechanism, existingPrintJobDetails))) {
                            String hiddenStation = "";
                            if (!diningOption.getPrintsOnExpeditor()) {
                                hiddenStation = String.valueOf(kitchenOrderDeliveryMechanism.getStationID());
                            }
                            IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KDS, kitchenOrderDeliveryMechanism, hiddenStation, diningOption);
                            if (diningOptionPrintJobDetail != null) {
                                diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                            }
                        }
                        else if ((kitchenOrderDeliveryMechanism.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KMS_PREP)
                                && (isThereAProductOnPrinterOrStation(diningOption, kitchenOrderDeliveryMechanism, existingPrintJobDetails))) {
                            IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, kitchenOrderDeliveryMechanism, null, diningOption);
                            if (diningOptionPrintJobDetail != null) {
                                diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                            }
                            // check for any mirrored stations
                            ArrayList<KitchenOrderDeliveryMechanism> mirrorStations = getKMSMirrorOrderDeliveryMechanisms(KitchenOrderDeliveryMechanismType.KMS_MIRROR_PREP, kitchenOrderDeliveryMechanism);
                            if (!DataFunctions.isEmptyCollection(mirrorStations)) {
                                // create a print job detail for each mirror station
                                for (KitchenOrderDeliveryMechanism mirrorStation : mirrorStations) {
                                    IPrintJobDetail kmsMirrorPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, mirrorStation, null, diningOption);
                                    if (kmsMirrorPrintJobDetail != null) {
                                        diningOptionPrintJobDetails.add(kmsMirrorPrintJobDetail);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (isRemoteTransaction) {
                if (remoteOrdersUseExpeditor) {
                    // determine any print jobs that should be sent to kitchen expeditors serving as remotes
                    ArrayList<KitchenOrderDeliveryMechanism> kpRemoteExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KP_REMOTE_EXPEDITOR);
                    if (!DataFunctions.isEmptyCollection(kpRemoteExpeditors)) {
                        for (KitchenOrderDeliveryMechanism kpRemoteExpeditor : kpRemoteExpeditors) {
                            if (isThereAProductOnPrinterOrStation(diningOption, kpRemoteExpeditor, existingPrintJobDetails)) {
                                IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KP, kpRemoteExpeditor, null, diningOption);
                                if (diningOptionPrintJobDetail != null) {
                                    diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                                }
                            }
                        }
                    }
                    ArrayList<KitchenOrderDeliveryMechanism> kdsRemoteExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KDS_REMOTE_EXPEDITOR);
                    if (!DataFunctions.isEmptyCollection(kdsRemoteExpeditors)) {
                        for (KitchenOrderDeliveryMechanism kdsRemoteExpeditor : kdsRemoteExpeditors) {
                            if (isThereAProductOnPrinterOrStation(diningOption, kdsRemoteExpeditor, existingPrintJobDetails)) {
                                IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KDS, kdsRemoteExpeditor, null, diningOption);
                                if (diningOptionPrintJobDetail != null) {
                                    diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                                }
                            }
                        }
                    }
                    ArrayList<KitchenOrderDeliveryMechanism> kmsRemoteExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KMS_REMOTE_EXPEDITOR);
                    if (!DataFunctions.isEmptyCollection(kmsRemoteExpeditors)) {
                        for (KitchenOrderDeliveryMechanism kmsRemoteExpeditor : kmsRemoteExpeditors) {
                            if (isThereAProductOnPrinterOrStation(diningOption, kmsRemoteExpeditor, existingPrintJobDetails)) {
                                IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, kmsRemoteExpeditor, null, diningOption);
                                if (diningOptionPrintJobDetail != null) {
                                    diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                                }
                                // check for any mirrored stations
                                ArrayList<KitchenOrderDeliveryMechanism> mirrorStations = getKMSMirrorOrderDeliveryMechanisms(KitchenOrderDeliveryMechanismType.KMS_MIRROR_REMOTE_EXPEDITOR, kmsRemoteExpeditor);
                                if (!DataFunctions.isEmptyCollection(mirrorStations)) {
                                    // create a print job detail for each mirror station
                                    for (KitchenOrderDeliveryMechanism mirrorStation : mirrorStations) {
                                        IPrintJobDetail kmsMirrorPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, mirrorStation, null, diningOption);
                                        if (kmsMirrorPrintJobDetail != null) {
                                            diningOptionPrintJobDetails.add(kmsMirrorPrintJobDetail);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    // determine any print jobs that should be sent to kitchen expeditors, make sure the dining option should show on the expeditor
                    if (diningOption.getPrintsOnExpeditor()) {
                        // determine any print jobs that should be sent to kitchen expeditors
                        ArrayList<KitchenOrderDeliveryMechanism> kpExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KP_EXPEDITOR);
                        if (!DataFunctions.isEmptyCollection(kpExpeditors)) {
                            for (KitchenOrderDeliveryMechanism kpExpeditor : kpExpeditors) {
                                if (isThereAProductOnPrinterOrStation(diningOption, kpExpeditor, existingPrintJobDetails)) {
                                    IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KP, kpExpeditor, null, diningOption);
                                    if (diningOptionPrintJobDetail != null) {
                                        diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                                    }
                                }
                            }
                        }
                        ArrayList<KitchenOrderDeliveryMechanism> kdsExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KDS_EXPEDITOR);
                        if (!DataFunctions.isEmptyCollection(kdsExpeditors)) {
                            for (KitchenOrderDeliveryMechanism kdsExpeditor : kdsExpeditors) {
                                if (isThereAProductOnPrinterOrStation(diningOption, kdsExpeditor, existingPrintJobDetails)) {
                                    IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KDS, kdsExpeditor, null, diningOption);
                                    if (diningOptionPrintJobDetail != null) {
                                        diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                                    }
                                }
                            }
                        }
                        ArrayList<KitchenOrderDeliveryMechanism> kmsExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KMS_EXPEDITOR);
                        if (!DataFunctions.isEmptyCollection(kmsExpeditors)) {
                            for (KitchenOrderDeliveryMechanism kmsExpeditor : kmsExpeditors) {
                                if (isThereAProductOnPrinterOrStation(diningOption, kmsExpeditor, existingPrintJobDetails)) {
                                    IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, kmsExpeditor, null, diningOption);
                                    if (diningOptionPrintJobDetail != null) {
                                        diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                                    }
                                    // check for any mirrored stations
                                    ArrayList<KitchenOrderDeliveryMechanism> mirrorStations = getKMSMirrorOrderDeliveryMechanisms(KitchenOrderDeliveryMechanismType.KMS_MIRROR_EXPEDITOR, kmsExpeditor);
                                    if (!DataFunctions.isEmptyCollection(mirrorStations)) {
                                        // create a print job detail for each mirror station
                                        for (KitchenOrderDeliveryMechanism mirrorStation : mirrorStations) {
                                            IPrintJobDetail kmsMirrorPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, mirrorStation, null, diningOption);
                                            if (kmsMirrorPrintJobDetail != null) {
                                                diningOptionPrintJobDetails.add(kmsMirrorPrintJobDetail);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // determine any print jobs that should be sent to kitchen remotes
                    ArrayList<KitchenOrderDeliveryMechanism> kpRemotes = kodmByType.get(KitchenOrderDeliveryMechanismType.KP_REMOTE);
                    if (!DataFunctions.isEmptyCollection(kpRemotes)) {
                        for (KitchenOrderDeliveryMechanism kpRemote : kpRemotes) {
                            if (isThereAProductOnPrinterOrStation(diningOption, kpRemote, existingPrintJobDetails)) {
                                IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KP, kpRemote, null, diningOption);
                                if (diningOptionPrintJobDetail != null) {
                                    diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                                }
                            }
                        }
                    }
                    ArrayList<KitchenOrderDeliveryMechanism> kdsRemotes = kodmByType.get(KitchenOrderDeliveryMechanismType.KDS_REMOTE);
                    if (!DataFunctions.isEmptyCollection(kdsRemotes)) {
                        for (KitchenOrderDeliveryMechanism kdsRemote : kdsRemotes) {
                            if (isThereAProductOnPrinterOrStation(diningOption, kdsRemote, existingPrintJobDetails)) {
                                IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KDS, kdsRemote, null, diningOption);
                                if (diningOptionPrintJobDetail != null) {
                                    diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                                }
                            }
                        }
                    }
                    ArrayList<KitchenOrderDeliveryMechanism> kmsRemotes = kodmByType.get(KitchenOrderDeliveryMechanismType.KMS_REMOTE);
                    if (!DataFunctions.isEmptyCollection(kmsRemotes)) {
                        for (KitchenOrderDeliveryMechanism kmsRemote : kmsRemotes) {
                            if (isThereAProductOnPrinterOrStation(diningOption, kmsRemote, existingPrintJobDetails)) {
                                IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, kmsRemote, null, diningOption);
                                if (diningOptionPrintJobDetail != null) {
                                    diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                                }
                                // check for any mirrored stations
                                ArrayList<KitchenOrderDeliveryMechanism> mirrorStations = getKMSMirrorOrderDeliveryMechanisms(KitchenOrderDeliveryMechanismType.KMS_MIRROR_REMOTE, kmsRemote);
                                if (!DataFunctions.isEmptyCollection(mirrorStations)) {
                                    // create a print job detail for each mirror station
                                    for (KitchenOrderDeliveryMechanism mirrorStation : mirrorStations) {
                                        IPrintJobDetail kmsMirrorPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, mirrorStation, null, diningOption);
                                        if (kmsMirrorPrintJobDetail != null) {
                                            diningOptionPrintJobDetails.add(kmsMirrorPrintJobDetail);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                // determine any print jobs that should be sent to kitchen expeditors, make sure the dining option should show on the expeditor
                if (diningOption.getPrintsOnExpeditor()) {
                    // determine any print jobs that should be sent to kitchen expeditors
                    ArrayList<KitchenOrderDeliveryMechanism> kpExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KP_EXPEDITOR);
                    if (!DataFunctions.isEmptyCollection(kpExpeditors)) {
                        for (KitchenOrderDeliveryMechanism kpExpeditor : kpExpeditors) {
                            if (isThereAProductOnPrinterOrStation(diningOption, kpExpeditor, existingPrintJobDetails)) {
                                IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KP, kpExpeditor, null, diningOption);
                                if (diningOptionPrintJobDetail != null) {
                                    diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                                }
                            }
                        }
                    }
                    ArrayList<KitchenOrderDeliveryMechanism> kdsExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KDS_EXPEDITOR);
                    if (!DataFunctions.isEmptyCollection(kdsExpeditors)) {
                        for (KitchenOrderDeliveryMechanism kdsExpeditor : kdsExpeditors) {
                            if (isThereAProductOnPrinterOrStation(diningOption, kdsExpeditor, existingPrintJobDetails)) {
                                IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KDS, kdsExpeditor, null, diningOption);
                                if (diningOptionPrintJobDetail != null) {
                                    diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                                }
                            }
                        }
                    }
                    ArrayList<KitchenOrderDeliveryMechanism> kmsExpeditors = kodmByType.get(KitchenOrderDeliveryMechanismType.KMS_EXPEDITOR);
                    if (!DataFunctions.isEmptyCollection(kmsExpeditors)) {
                        for (KitchenOrderDeliveryMechanism kmsExpeditor : kmsExpeditors) {
                            if (isThereAProductOnPrinterOrStation(diningOption, kmsExpeditor, existingPrintJobDetails)) {
                                IPrintJobDetail diningOptionPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, kmsExpeditor, null, diningOption);
                                if (diningOptionPrintJobDetail != null) {
                                    diningOptionPrintJobDetails.add(diningOptionPrintJobDetail);
                                }
                                // check for any mirrored stations
                                ArrayList<KitchenOrderDeliveryMechanism> mirrorStations = getKMSMirrorOrderDeliveryMechanisms(KitchenOrderDeliveryMechanismType.KMS_MIRROR_EXPEDITOR, kmsExpeditor);
                                if (!DataFunctions.isEmptyCollection(mirrorStations)) {
                                    // create a print job detail for each mirror station
                                    for (KitchenOrderDeliveryMechanism mirrorStation : mirrorStations) {
                                        IPrintJobDetail kmsMirrorPrintJobDetail = printJobDetailFactory.buildPrintJobDetail(log, PrintJobDetailType.KMS, mirrorStation, null, diningOption);
                                        if (kmsMirrorPrintJobDetail != null) {
                                            diningOptionPrintJobDetails.add(kmsMirrorPrintJobDetail);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // determine the ID of the transaction the print job is for
            BigDecimal transactionID = null;
            if ((transactionData != null) && (transactionData.getTxnHdrData() != null) && (transactionData.getTxnHdrData().getPaTransactionID() != null)) {
                transactionID = transactionData.getTxnHdrData().getPaTransactionID();
            }
            Logger.logMessage(String.format("There was a problem trying to build the print job details for the dining option product %s within " +
                    "the transaction with an ID of %s in PrintJobInfo.createDiningOptionPrintJobDetails",
                    Objects.toString((diningOption != null ? diningOption.getName() : "N/A"), "N/A"),
                    Objects.toString((transactionID != null ? transactionID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return diningOptionPrintJobDetails;
    }

    /**
     * <p>Check whether or not a product that isn't a dining option has been sent to the printer or station we want to add the dining option to.</p>
     *
     * @param diningOption The dining option {@link Product} to check.
     * @param kodm The {@link KitchenOrderDeliveryMechanism} to add the dining option to.
     * @param existingPrintJobDetails An {@link ArrayList} of {@link IPrintJobDetail} corresponding to print job details that have been created for products that aren't dining options.
     * @return Whether or not a product that isn't a dining option has been sent to the printer or station we want to add the dining option to.
     */
    @SuppressWarnings({"Convert2streamapi", "Duplicates"})
    private boolean isThereAProductOnPrinterOrStation (Product diningOption, KitchenOrderDeliveryMechanism kodm, ArrayList<IPrintJobDetail> existingPrintJobDetails) {
        boolean isThereAProductOnPrinterOrStation = false;

        try {
            // make sure we have a valid mechanism by which to send the dining option to the kitchen
            if (kodm == null) {
                Logger.logMessage("The kitchen order delivery mechanism passed to PrintJobInfo.isThereAProductOnPrinterOrStation can't be null!", log, Logger.LEVEL.ERROR);
                return false;
            }

            // check that we have existing print job details for products that aren't dining options since we can't print/display a dining option by itself
            if (DataFunctions.isEmptyCollection(existingPrintJobDetails)) {
                Logger.logMessage("No existing print job details found in PrintJobInfo.isThereAProductOnPrinterOrStation!", log, Logger.LEVEL.ERROR);
            }

            boolean addingDiningOptionToPrep = ((kodm.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KP_PREP)
                    || (kodm.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KDS_PREP)
                    || (kodm.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KMS_PREP));

            boolean addingDiningOptionToExpeditor = ((kodm.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KP_EXPEDITOR)
                    || (kodm.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KDS_EXPEDITOR)
                    || (kodm.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KMS_EXPEDITOR));

            boolean addingDiningOptionToRemote = ((kodm.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KP_REMOTE)
                    || (kodm.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KDS_REMOTE)
                    || (kodm.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KMS_REMOTE));

            boolean addingDiningOptionToRemoteExpeditor = ((kodm.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KP_REMOTE_EXPEDITOR)
                    || (kodm.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KDS_REMOTE_EXPEDITOR)
                    || (kodm.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KMS_REMOTE_EXPEDITOR));

            int printerID = kodm.getPrinterID();
            int kmsStationID = kodm.getKmsStationID();

            if (addingDiningOptionToPrep) {
                if (printerID > 0) {
                    // check if a product that isn't a dining option was sent to the prep printer or KDS prep station
                    for (IPrintJobDetail existingPrintJobDetail : existingPrintJobDetails) {
                        if (existingPrintJobDetail instanceof KPPrintJobDetail) {
                            KPPrintJobDetail kpPrintJobDetail = ((KPPrintJobDetail) existingPrintJobDetail);
                            if (kpPrintJobDetail.getPrinterID() == printerID) {
                                isThereAProductOnPrinterOrStation = true;
                                break; // a product that wasn't a dining option product was on the prep printer, we know we can add the dining option, no need to keep processing
                            }
                        }
                        else if (existingPrintJobDetail instanceof KDSPrintJobDetail) {
                            KDSPrintJobDetail kdsPrintJobDetail = ((KDSPrintJobDetail) existingPrintJobDetail);
                            if (kdsPrintJobDetail.getPrinterID() == printerID) {
                                isThereAProductOnPrinterOrStation = true;
                                break; // a product that wasn't a dining option product was on the KDS prep station, we know we can add the dining option, no need to keep processing
                            }
                        }
                    }
                }
                else if (kmsStationID > 0) {
                    // check if a product that isn't a dining option was sent to the KMS prep station
                    for (IPrintJobDetail existingPrintJobDetail : existingPrintJobDetails) {
                        if (existingPrintJobDetail instanceof KMSPrintJobDetail) {
                            KMSPrintJobDetail kmsPrintJobDetail = ((KMSPrintJobDetail) existingPrintJobDetail);
                            if (kmsPrintJobDetail.getKmsStationID() == kmsStationID) {
                                isThereAProductOnPrinterOrStation = true;
                                break; // a product that wasn't a dining option product was on the KMS prep station, we know we can add the dining option, no need to keep processing
                            }
                        }
                    }
                }
            }
            else if (addingDiningOptionToExpeditor) {
                if (printerID > 0) {
                    boolean isInstanceOfKDSExpeditor = false;
                    boolean isProductOnKDSExpeditorStation = false;
                    // check if a product that isn't a dining option was sent to the prep printer or KDS expeditor station
                    for (IPrintJobDetail existingPrintJobDetail : existingPrintJobDetails) {
                        if (existingPrintJobDetail instanceof KPPrintJobDetail) {
                            KPPrintJobDetail kpPrintJobDetail = ((KPPrintJobDetail) existingPrintJobDetail);
                            if (kpPrintJobDetail.getPrinterID() == printerID) {
                                isThereAProductOnPrinterOrStation = true;
                                break; // a product that wasn't a dining option product was on the expeditor printer, we know we can add the dining option, no need to keep processing
                            }
                        }
                        else if (existingPrintJobDetail instanceof KDSPrintJobDetail) {
                            isInstanceOfKDSExpeditor = true;
                            KDSPrintJobDetail kdsPrintJobDetail = ((KDSPrintJobDetail) existingPrintJobDetail);
                            if (kdsPrintJobDetail.getPrinterID() == printerID) {
                                isThereAProductOnPrinterOrStation = true;
                                isProductOnKDSExpeditorStation = true;
                                break; // a product that wasn't a dining option product was on the KDS expeditor station, we know we can add the dining option, no need to keep processing
                            }
                        }
                    }
                    // if we haven't found a product on the expeditor station make sure one hasn't been added from a prep station
                    if ((isInstanceOfKDSExpeditor) && (!isProductOnKDSExpeditorStation) && (diningOption != null)) {
                        ArrayList<IKitchenPrep> kitchenPreps = diningOption.getKitchenPreps();
                        if (!DataFunctions.isEmptyCollection(kitchenPreps)) {
                            for (IKitchenPrep kitchenPrep : kitchenPreps) {
                                KitchenOrderDeliveryMechanism kodmEquiv = getKODMEquivalentForKitchenPrep(kitchenPrep);
                                boolean shouldAddToExpeditorKDSStation = false;
                                if (kodmEquiv.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KDS_PREP) {
                                    for (IPrintJobDetail existingPrintJobDetail : existingPrintJobDetails) {
                                        if (existingPrintJobDetail instanceof KDSPrintJobDetail) {
                                            KDSPrintJobDetail existingKDSPrintJobDetail = ((KDSPrintJobDetail) existingPrintJobDetail);
                                            if ((existingKDSPrintJobDetail.getPrinterID() == kodmEquiv.getPrinterID())
                                                    && (StringFunctions.stringHasContent(existingKDSPrintJobDetail.getHideStation()))
                                                    && (existingKDSPrintJobDetail.getHideStation().equalsIgnoreCase(String.valueOf(kodmEquiv.getStationID())))) {
                                                shouldAddToExpeditorKDSStation = true;
                                                isThereAProductOnPrinterOrStation = true;
                                                break; // we know we can add the dining option to the KDS expeditor station, no need to keep processing
                                            }
                                        }
                                    }
                                }
                                if (shouldAddToExpeditorKDSStation) {
                                    break; // we know we can add the dining option to the kds expeditor station, no need to keep processing
                                }
                            }
                        }
                    }
                }
                if (kmsStationID > 0) {
                    // check if a product that isn't a dining option was sent to the KMS expeditor station
                    for (IPrintJobDetail existingPrintJobDetail : existingPrintJobDetails) {
                        if (existingPrintJobDetail instanceof KMSPrintJobDetail) {
                            KMSPrintJobDetail kmsPrintJobDetail = ((KMSPrintJobDetail) existingPrintJobDetail);
                            if (kmsPrintJobDetail.getKmsStationID() == kmsStationID) {
                                isThereAProductOnPrinterOrStation = true;
                                break; // a product that wasn't a dining option product was on the KMS expeditor station, we know we can add the dining option, no need to keep processing
                            }
                        }
                    }
                }
            }
            else if (addingDiningOptionToRemote) {
                if (printerID > 0) {
                    // check if a product that isn't a dining option was sent to the remote order printer or KDS remote order station
                    for (IPrintJobDetail existingPrintJobDetail : existingPrintJobDetails) {
                        if (existingPrintJobDetail instanceof KPPrintJobDetail) {
                            KPPrintJobDetail kpPrintJobDetail = ((KPPrintJobDetail) existingPrintJobDetail);
                            if (kpPrintJobDetail.getPrinterID() == printerID) {
                                isThereAProductOnPrinterOrStation = true;
                                break; // a product that wasn't a dining option product was on the remote order printer, we know we can add the dining option, no need to keep processing
                            }
                        }
                        else if (existingPrintJobDetail instanceof KDSPrintJobDetail) {
                            KDSPrintJobDetail kdsPrintJobDetail = ((KDSPrintJobDetail) existingPrintJobDetail);
                            if (kdsPrintJobDetail.getPrinterID() == printerID) {
                                isThereAProductOnPrinterOrStation = true;
                                break; // a product that wasn't a dining option product was on the KDS remote order station, we know we can add the dining option, no need to keep processing
                            }
                        }
                    }
                }
                else if (kmsStationID > 0) {
                    // check if a product that isn't a dining option was sent to the KMS remote order station
                    for (IPrintJobDetail existingPrintJobDetail : existingPrintJobDetails) {
                        if (existingPrintJobDetail instanceof KMSPrintJobDetail) {
                            KMSPrintJobDetail kmsPrintJobDetail = ((KMSPrintJobDetail) existingPrintJobDetail);
                            if (kmsPrintJobDetail.getKmsStationID() == kmsStationID) {
                                isThereAProductOnPrinterOrStation = true;
                                break; // a product that wasn't a dining option product was on the KMS remote order station, we know we can add the dining option, no need to keep processing
                            }
                        }
                    }
                }
            }
            else if (addingDiningOptionToRemoteExpeditor) {
                if (printerID > 0) {
                    boolean isInstanceOfKDSExpeditor = false;
                    boolean isProductOnKDSExpeditorStation = false;
                    // check if a product that isn't a dining option was sent to the remote order expeditor printer or KDS remote order station
                    for (IPrintJobDetail existingPrintJobDetail : existingPrintJobDetails) {
                        if (existingPrintJobDetail instanceof KPPrintJobDetail) {
                            KPPrintJobDetail kpPrintJobDetail = ((KPPrintJobDetail) existingPrintJobDetail);
                            if (kpPrintJobDetail.getPrinterID() == printerID) {
                                isThereAProductOnPrinterOrStation = true;
                                break; // a product that wasn't a dining option product was on the remote order expeditor printer, we know we can add the dining option, no need to keep processing
                            }
                        }
                        else if (existingPrintJobDetail instanceof KDSPrintJobDetail) {
                            isInstanceOfKDSExpeditor = true;
                            KDSPrintJobDetail kdsPrintJobDetail = ((KDSPrintJobDetail) existingPrintJobDetail);
                            if (kdsPrintJobDetail.getPrinterID() == printerID) {
                                isThereAProductOnPrinterOrStation = true;
                                isProductOnKDSExpeditorStation = true;
                                break; // a product that wasn't a dining option product was on the KDS remote order expeditor station, we know we can add the dining option, no need to keep processing
                            }
                        }
                    }
                    // if we haven't found a product on the remote order expeditor station make sure one hasn't been added from a prep station
                    if ((isInstanceOfKDSExpeditor) && (!isProductOnKDSExpeditorStation) && (diningOption != null)) {
                        ArrayList<IKitchenPrep> kitchenPreps = diningOption.getKitchenPreps();
                        if (!DataFunctions.isEmptyCollection(kitchenPreps)) {
                            for (IKitchenPrep kitchenPrep : kitchenPreps) {
                                KitchenOrderDeliveryMechanism kodmEquiv = getKODMEquivalentForKitchenPrep(kitchenPrep);
                                boolean shouldAddToExpeditorKDSStation = false;
                                if (kodmEquiv.getKitchenOrderDeliveryMechanismType() == KitchenOrderDeliveryMechanismType.KDS_PREP) {
                                    for (IPrintJobDetail existingPrintJobDetail : existingPrintJobDetails) {
                                        if (existingPrintJobDetail instanceof KDSPrintJobDetail) {
                                            KDSPrintJobDetail existingKDSPrintJobDetail = ((KDSPrintJobDetail) existingPrintJobDetail);
                                            if ((existingKDSPrintJobDetail.getPrinterID() == kodmEquiv.getPrinterID())
                                                    && (StringFunctions.stringHasContent(existingKDSPrintJobDetail.getHideStation()))
                                                    && (existingKDSPrintJobDetail.getHideStation().equalsIgnoreCase(String.valueOf(kodmEquiv.getStationID())))) {
                                                shouldAddToExpeditorKDSStation = true;
                                                isThereAProductOnPrinterOrStation = true;
                                                break; // we know we can add the dining option to the KDS remote order expeditor station, no need to keep processing
                                            }
                                        }
                                    }
                                }
                                if (shouldAddToExpeditorKDSStation) {
                                    break; // we know we can add the dining option to the kds remote order expeditor station, no need to keep processing
                                }
                            }
                        }
                    }
                }
                else if (kmsStationID > 0) {
                    // check if a product that isn't a dining option was sent to the KMS remote order station
                    for (IPrintJobDetail existingPrintJobDetail : existingPrintJobDetails) {
                        if (existingPrintJobDetail instanceof KMSPrintJobDetail) {
                            KMSPrintJobDetail kmsPrintJobDetail = ((KMSPrintJobDetail) existingPrintJobDetail);
                            if (kmsPrintJobDetail.getKmsStationID() == kmsStationID) {
                                isThereAProductOnPrinterOrStation = true;
                                break; // a product that wasn't a dining option product was on the KMS remote order expeditor station, we know we can add the dining option, no need to keep processing
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            // determine whether or not to log the printer ID or KMS station ID
            int printerID = 0;
            if ((kodm != null) && (kodm.getPrinterID() > 0)) {
                printerID = kodm.getPrinterID();
            }
            int kmsStationID = 0;
            if ((kodm != null) && (kodm.getKmsStationID() > 0)) {
                kmsStationID = kodm.getKmsStationID();
            }
            int idToLog = 0;
            if (printerID > 0) {
                idToLog = printerID;
            }
            else if (kmsStationID > 0) {
                idToLog = kmsStationID;
            }
            Logger.logMessage(String.format("There was a problem trying to determine whether or not any print job details will be sent to the " +
                    "kitchen order display mechanism with an ID of %s which we intend to send the dining option to in PrintJobInfo.isThereAProductOnPrinterOrStation",
                    Objects.toString(idToLog > 0 ? idToLog : "N/A", "N/A")), log, Logger.LEVEL.ERROR);
        }

        return isThereAProductOnPrinterOrStation;
    }

    /**
     * <p>Sorts the print job details by the printer or station they will be sent to.</p>
     *
     * @return A {@link HashMap} whose key is the receipt type {@link String} the given print job detail will be sent to and whose
     * value is a {@link HashMap} whose key is the {@link Integer} ID of the printer or station the print job
     * detail will be sent to and whose value is an {@link ArrayList} of {@link IPrintJobDetail} corresponding to the print job
     * details that will be sent to that printer or station.
     */
    public HashMap<String, HashMap<Integer, ArrayList<IPrintJobDetail>>> sortPrintJobDetails () {
        HashMap<String, HashMap<Integer, ArrayList<IPrintJobDetail>>> sortedPrintJobDetails = new HashMap<>();
        HashMap<Integer, ArrayList<IPrintJobDetail>> printJobDetailsSortedByDestination;
        ArrayList<IPrintJobDetail> commonDestinationPrintJobDetails;

        try {
            // make sure we have print job details to sort
            if (DataFunctions.isEmptyCollection(printJobDetails)) {
                Logger.logMessage("Unable to sort the print job details in PrintJobInfo.sortPrintJobDetails, no print job details were found!", log, Logger.LEVEL.ERROR);
                return null;
            }

            for (IPrintJobDetail printJobDetail : printJobDetails) {
                String printJobDetailReceiptType = getPrintJobDetailReceiptType(printJobDetail);
                if (StringFunctions.stringHasContent(printJobDetailReceiptType)) {
                    // determine the ID of the printer or station the print job detail will be printed/displayed on
                    int destinationID;
                    if (printJobDetailReceiptType.equalsIgnoreCase(TypeData.KitchenReceiptType.KP)) {
                        destinationID = ((KPPrintJobDetail) printJobDetail).getPrinterID();
                    }
                    else if (printJobDetailReceiptType.equalsIgnoreCase(TypeData.KitchenReceiptType.KDS)) {
                        destinationID = ((KDSPrintJobDetail) printJobDetail).getPrinterID();
                    }
                    else {
                        destinationID = ((KMSPrintJobDetail) printJobDetail).getKmsStationID();
                    }

                    if (sortedPrintJobDetails.containsKey(printJobDetailReceiptType)) {
                        printJobDetailsSortedByDestination = sortedPrintJobDetails.get(printJobDetailReceiptType);
                    }
                    else {
                        printJobDetailsSortedByDestination = new HashMap<>();
                    }

                    if ((!DataFunctions.isEmptyMap(printJobDetailsSortedByDestination)) && (printJobDetailsSortedByDestination.containsKey(destinationID))) {
                        commonDestinationPrintJobDetails = printJobDetailsSortedByDestination.get(destinationID);
                    }
                    else {
                        commonDestinationPrintJobDetails = new ArrayList<>();
                    }

                    commonDestinationPrintJobDetails.add(printJobDetail);
                    printJobDetailsSortedByDestination.put(destinationID, commonDestinationPrintJobDetails);
                    sortedPrintJobDetails.put(printJobDetailReceiptType, printJobDetailsSortedByDestination);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("", log, Logger.LEVEL.ERROR);
        }

        return sortedPrintJobDetails;
    }

    /**
     * <p>Determines the type of receipt the given print job detail will be printed/displayed on.</p>
     *
     * @param printJobDetail The {@link IPrintJobDetail} to get the receipt type for.
     * @return The type of receipt the given print job detail will be printed/displayed on as a {@link String}.
     */
    private String getPrintJobDetailReceiptType (IPrintJobDetail printJobDetail) {
        String printJobDetailReceiptType = "";

        if (printJobDetail != null) {
            if (printJobDetail instanceof KPPrintJobDetail) {
                printJobDetailReceiptType = TypeData.KitchenReceiptType.KP;
            }
            else if (printJobDetail instanceof KDSPrintJobDetail) {
                printJobDetailReceiptType = TypeData.KitchenReceiptType.KDS;
            }
            else if (printJobDetail instanceof KMSPrintJobDetail) {
                printJobDetailReceiptType = TypeData.KitchenReceiptType.KMS;
            }
        }

        return printJobDetailReceiptType;
    }

    /**
     * <p>Getter for the printJobHeader field of the {@link PrintJobInfo}.</p>
     *
     * @return The printJobHeader field of the {@link PrintJobInfo}.
     */
    public PrintJobHeader getPrintJobHeader () {
        return printJobHeader;
    }

    /**
     * <p>Setter for the printJobHeader field of the {@link PrintJobInfo}.</p>
     *
     * @param printJobHeader The printJobHeader field of the {@link PrintJobInfo}.
     */
    public void setPrintJobHeader (PrintJobHeader printJobHeader) {
        this.printJobHeader = printJobHeader;
    }

    /**
     * <p>Getter for the printJobDetails field of the {@link PrintJobInfo}.</p>
     *
     * @return The printJobDetails field of the {@link PrintJobInfo}.
     */
    public ArrayList<IPrintJobDetail> getPrintJobDetails () {
        return printJobDetails;
    }

    /**
     * <p>Setter for the printJobDetails field of the {@link PrintJobInfo}.</p>
     *
     * @param printJobDetails The printJobDetails field of the {@link PrintJobInfo}.
     */
    public void setPrintJobDetails (ArrayList<IPrintJobDetail> printJobDetails) {
        this.printJobDetails = printJobDetails;
    }

}