package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2019-11-27 09:51:33 -0500 (Wed, 27 Nov 2019) $: Date of last commit
    $Rev: 10124 $: Revision of last commit
    Notes: Utility class to help determine which printers / stations products should print / be displayed on.
*/

import com.mmhayes.common.receiptGen.TransactionData.Plu;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;

import java.util.ArrayList;

/**
 * Utility class to help determine which printers / stations products should print / be displayed on.
 *
 */
public class KPPrinterDeterminator {

    /**
     * Determines which KP prep printers the dining option should print to.
     *
     * @param mappedPrinters The KP prep printers the dining option is mapped to.
     * @param nonDiningOptions {@link ArrayList<Plu>} Items in the transaction that aren't dining options.
     * @param kpPrepPrinters {@link ArrayList<PrinterData>} The KP prep printers within the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The KP prep printers the dining option should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess", "OverlyComplexMethod"})
    public static ArrayList<PrinterData> getKPPrepPrintersForDiningOption (int[] mappedPrinters,
                                                                           ArrayList<Plu> nonDiningOptions,
                                                                           ArrayList<PrinterData> kpPrepPrinters) {
        ArrayList<PrinterData> kpPrepPrintersForDiningOption = new ArrayList<>();

        try {
            if ((!DataFunctions.isEmptyIntArr(mappedPrinters)) && (!DataFunctions.isEmptyCollection(kpPrepPrinters))) {
                for (int mappedPrinter : mappedPrinters) {
                    PrinterData kpPrepPrinter = null;
                    for (PrinterData prep : kpPrepPrinters) {
                        if (mappedPrinter == prep.getPrinterID()) {
                            kpPrepPrinter = prep;
                            break;
                        }
                    }

                    boolean productsGoingToPrepPrinter = false;
                    if (kpPrepPrinter != null) {
                        if (!DataFunctions.isEmptyCollection(nonDiningOptions)) {
                            for (Plu nonDiningOption : nonDiningOptions) {
                                if (!DataFunctions.isEmptyIntArr(nonDiningOption.getMappedPrinters())) {
                                    for (int nonDiningOptionPrinter : nonDiningOption.getMappedPrinters()) {
                                        if (kpPrepPrinter.getPrinterID() == nonDiningOptionPrinter) {
                                            productsGoingToPrepPrinter = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (productsGoingToPrepPrinter) {
                        kpPrepPrintersForDiningOption.add(kpPrepPrinter);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KP prep printers should print the dining option in " +
                    "KPPrinterDeterminator.getKPPrepPrintersForDiningOption", Logger.LEVEL.ERROR);
        }

        return kpPrepPrintersForDiningOption;
    }
    
    /**
     * Determines which KDS prep printers the dining option should print to.
     *
     * @param mappedPrinters The KDS prep printers the dining option is mapped to.
     * @param nonDiningOptions {@link ArrayList<Plu>} Items in the transaction that aren't dining options.
     * @param kdsPrepPrinters {@link ArrayList<PrinterData>} The KDS prep printers within the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The KDS prep printers the dining option should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess", "OverlyComplexMethod"})
    public static ArrayList<PrinterData> getKDSPrepPrintersForDiningOption (int[] mappedPrinters,
                                                                            ArrayList<Plu> nonDiningOptions,
                                                                            ArrayList<PrinterData> kdsPrepPrinters) {
        ArrayList<PrinterData> kdsPrepPrintersForDiningOption = new ArrayList<>();

        try {
            if ((!DataFunctions.isEmptyIntArr(mappedPrinters)) && (!DataFunctions.isEmptyCollection(kdsPrepPrinters))) {
                for (int mappedPrinter : mappedPrinters) {
                    PrinterData kdsPrepPrinter = null;
                    for (PrinterData prep : kdsPrepPrinters) {
                        if (mappedPrinter == prep.getPrinterID()) {
                            kdsPrepPrinter = prep;
                            break;
                        }
                    }

                    boolean productsGoingToPrepPrinter = false;
                    if (kdsPrepPrinter != null) {
                        if (!DataFunctions.isEmptyCollection(nonDiningOptions)) {
                            for (Plu nonDiningOption : nonDiningOptions) {
                                if (!DataFunctions.isEmptyIntArr(nonDiningOption.getMappedPrinters())) {
                                    for (int nonDiningOptionPrinter : nonDiningOption.getMappedPrinters()) {
                                        if (kdsPrepPrinter.getPrinterID() == nonDiningOptionPrinter) {
                                            productsGoingToPrepPrinter = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (productsGoingToPrepPrinter) {
                        kdsPrepPrintersForDiningOption.add(kdsPrepPrinter);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KDS prep printers should print the dining option in " +
                    "KPPrinterDeterminator.getKDSPrepPrintersForDiningOption", Logger.LEVEL.ERROR);
        }

        return kdsPrepPrintersForDiningOption;
    }

    /**
     * Determines which KP expeditor printers the dining option should print to.
     *
     * @param kpExpeditorPrinters {@link ArrayList<PrinterData>} The KP expeditor printers within the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The KP expeditor printers the dining option should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKPExpeditorPrintersForDiningOption (ArrayList<PrinterData> kpExpeditorPrinters) {
        ArrayList<PrinterData> kpExpeditorPrintersForDiningOption = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(kpExpeditorPrinters)) {
                kpExpeditorPrintersForDiningOption.addAll(kpExpeditorPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KP expeditor printers should print the dining option in " +
                    "KPPrinterDeterminator.getKPExpeditorPrintersForDiningOption", Logger.LEVEL.ERROR);
        }

        return kpExpeditorPrintersForDiningOption;
    }

    /**
     * Determines which KDS expeditor printers the dining option should print to.
     *
     * @param kdsExpeditorPrinters {@link ArrayList<PrinterData>} The KDS expeditor printers within the revenue center in which the transaction took place.
     * @param productOnKDSPrep Whether or not the dining option has already been sent to a KDS prep station.
     * @return {@link ArrayList<PrinterData>} The KDS expeditor printers the dining option should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKDSExpeditorPrintersForDiningOption (ArrayList<PrinterData> kdsExpeditorPrinters,
                                                                                 boolean productOnKDSPrep) {
        ArrayList<PrinterData> kdsExpeditorPrintersForDiningOption = new ArrayList<>();

        try {
            if ((!DataFunctions.isEmptyCollection(kdsExpeditorPrinters)) && (!productOnKDSPrep)) {
                kdsExpeditorPrintersForDiningOption.addAll(kdsExpeditorPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KDS expeditor printers should print the dining option in " +
                    "KPPrinterDeterminator.getKDSExpeditorPrintersForDiningOption", Logger.LEVEL.ERROR);
        }

        return kdsExpeditorPrintersForDiningOption;
    }

    /**
     * Determines which KP remote order printers the dining option should print to.
     *
     * @param kpRemoteOrderPrinters {@link ArrayList<PrinterData>} The KP remote order printers within the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The KP remote order printers the dining option should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKPRemoteOrderPrintersForDiningOption (ArrayList<PrinterData> kpRemoteOrderPrinters) {
        ArrayList<PrinterData> kpRemoteOrderPrintersForDiningOption = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(kpRemoteOrderPrinters)) {
                kpRemoteOrderPrintersForDiningOption.addAll(kpRemoteOrderPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KP remote order printers should print the dining option in " +
                    "KPPrinterDeterminator.getKPRemoteOrderPrintersForDiningOption", Logger.LEVEL.ERROR);
        }

        return kpRemoteOrderPrintersForDiningOption;
    }

    /**
     * Determines which KDS remote order printers the dining option should print to.
     *
     * @param kdsRemoteOrderPrinters {@link ArrayList<PrinterData>} The KDS remote order printers within the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The KDS remote order printers the dining option should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKDSRemoteOrderPrintersForDiningOption (ArrayList<PrinterData> kdsRemoteOrderPrinters) {
        ArrayList<PrinterData> kdsRemoteOrderPrintersForDiningOption = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(kdsRemoteOrderPrinters)) {
                kdsRemoteOrderPrintersForDiningOption.addAll(kdsRemoteOrderPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KDS remote order printers should print the dining option in " +
                    "KPPrinterDeterminator.getKDSRemoteOrderPrintersForDiningOption", Logger.LEVEL.ERROR);
        }

        return kdsRemoteOrderPrintersForDiningOption;
    }

    /**
     * Determines which KP expeditor as remote order printers the dining option should print to.
     *
     * @param kpExpeditorAsRemoteOrderPrinters {@link ArrayList<PrinterData>} The KP expeditor as remote order printers within the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The KP expeditor as remote order printers the dining option should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKPExpeditorAsRemoteOrderPrintersForDiningOption (ArrayList<PrinterData> kpExpeditorAsRemoteOrderPrinters) {
        ArrayList<PrinterData> kpExpeditorAsRemoteOrderPrintersForDiningOption = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(kpExpeditorAsRemoteOrderPrinters)) {
                kpExpeditorAsRemoteOrderPrintersForDiningOption.addAll(kpExpeditorAsRemoteOrderPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KP expeditor as remote order printers should print the dining option in " +
                    "KPPrinterDeterminator.getKPExpeditorAsRemoteOrderPrintersForDiningOption", Logger.LEVEL.ERROR);
        }

        return kpExpeditorAsRemoteOrderPrintersForDiningOption;
    }

    /**
     * Determines which KDS expeditor as remote order printers the dining option should print to.
     *
     * @param kdsExpeditorAsRemoteOrderPrinters {@link ArrayList<PrinterData>} The KDS expeditor as remote order printers within the revenue center in which the transaction took place.
     * @param productOnKDSPrep Whether or not the dining option has already been sent to a KDS prep station.
     * @return {@link ArrayList<PrinterData>} The KDS expeditor as remote order printers the dining option should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKDSExpeditorAsRemoteOrderPrintersForDiningOption (ArrayList<PrinterData> kdsExpeditorAsRemoteOrderPrinters,
                                                                                              boolean productOnKDSPrep) {
        ArrayList<PrinterData> kdsExpeditorAsRemoteOrderPrintersForDiningOption = new ArrayList<>();

        try {
            if ((!DataFunctions.isEmptyCollection(kdsExpeditorAsRemoteOrderPrinters)) && (!productOnKDSPrep)) {
                kdsExpeditorAsRemoteOrderPrintersForDiningOption.addAll(kdsExpeditorAsRemoteOrderPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KDS expeditor as remote order printers should print the dining option in " +
                    "KPPrinterDeterminator.getKDSExpeditorAsRemoteOrderPrintersForDiningOption", Logger.LEVEL.ERROR);
        }

        return kdsExpeditorAsRemoteOrderPrintersForDiningOption;
    }

    /**
     * Determines which KP prep printers the product should print to.
     *
     * @param mappedPrinters The prep KP printers the product is mapped to.
     * @param kpPrepPrinters {@link ArrayList<PrinterData>} The KP prep printers within the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The KP prep printers the product should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKPPrepPrintersForProduct (int[] mappedPrinters,
                                                                      ArrayList<PrinterData> kpPrepPrinters) {
        ArrayList<PrinterData> kpPrepPrintersForProduct = new ArrayList<>();

        try {
            if ((!DataFunctions.isEmptyIntArr(mappedPrinters)) && (!DataFunctions.isEmptyCollection(kpPrepPrinters))) {
                for (int mappedPtrID : mappedPrinters) {
                    for (PrinterData ptr : kpPrepPrinters) {
                        if (mappedPtrID == ptr.getPrinterID()) {
                            kpPrepPrintersForProduct.add(ptr);
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KP prep printers should print the product in " +
                    "KPPrinterDeterminator.getKPPrepPrintersForProduct", Logger.LEVEL.ERROR);
        }

        return kpPrepPrintersForProduct;
    }

    /**
     * Determines which KDS prep printers the product should print to.
     *
     * @param mappedPrinters The KDS prep printers the product is mapped to.
     * @param kdsPrepPrinters {@link ArrayList<PrinterData>} The KDS prep printers within the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The KDS prep printers the product should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKDSPrepPrintersForProduct (int[] mappedPrinters,
                                                                       ArrayList<PrinterData> kdsPrepPrinters) {
        ArrayList<PrinterData> kdsPrepPrintersForProduct = new ArrayList<>();

        try {
            if ((!DataFunctions.isEmptyIntArr(mappedPrinters)) && (!DataFunctions.isEmptyCollection(kdsPrepPrinters))) {
                for (int mappedPtrID : mappedPrinters) {
                    for (PrinterData ptr : kdsPrepPrinters) {
                        if (mappedPtrID == ptr.getPrinterID()) {
                            kdsPrepPrintersForProduct.add(ptr);
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KDS prep printers should print the product in " +
                    "KPPrinterDeterminator.getKDSPrepPrintersForProduct", Logger.LEVEL.ERROR);
        }

        return kdsPrepPrintersForProduct;
    }

    /**
     * Determines which KP expeditor printers the product should print to.
     *
     * @param kpExpeditorPrinters {@link ArrayList<PrinterData>} The KP expeditor printers within the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The KP expeditor printers the product should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKPExpeditorPrintersForProduct (ArrayList<PrinterData> kpExpeditorPrinters) {
        ArrayList<PrinterData> kpExpeditorPrintersForProduct = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(kpExpeditorPrinters)) {
                kpExpeditorPrintersForProduct.addAll(kpExpeditorPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KP expeditor printers should print the product in " +
                    "KPPrinterDeterminator.getKPExpeditorPrintersForProduct", Logger.LEVEL.ERROR);
        }

        return kpExpeditorPrintersForProduct;
    }

    /**
     * Determines which KDS expeditor printers the product should print to.
     *
     * @param kdsExpeditorPrinters {@link ArrayList<PrinterData>} The KDS expeditor printers within the revenue center in which the transaction took place.
     * @param productOnKDSPrep Whether or not the product has already been sent to a KDS prep station.
     * @return {@link ArrayList<PrinterData>} The KDS expeditor printers the product should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKDSExpeditorPrintersForProduct (ArrayList<PrinterData> kdsExpeditorPrinters,
                                                                            boolean productOnKDSPrep) {
        ArrayList<PrinterData> kdsExpeditorPrintersForProduct = new ArrayList<>();

        try {
            if ((!DataFunctions.isEmptyCollection(kdsExpeditorPrinters)) && (!productOnKDSPrep)) {
                kdsExpeditorPrintersForProduct.addAll(kdsExpeditorPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KDS expeditor printers should print the product in " +
                    "KPPrinterDeterminator.getKDSExpeditorPrintersForProduct", Logger.LEVEL.ERROR);
        }

        return kdsExpeditorPrintersForProduct;
    }

    /**
     * Determines which KP remote order printers the product should print to.
     *
     * @param kpRemoteOrderPrinters {@link ArrayList<PrinterData>} The KP remote order printers within the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The KP remote order printers the product should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKPRemoteOrderPrintersForProduct (ArrayList<PrinterData> kpRemoteOrderPrinters) {
        ArrayList<PrinterData> kpRemoteOrderPrintersForProduct = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(kpRemoteOrderPrinters)) {
                kpRemoteOrderPrintersForProduct.addAll(kpRemoteOrderPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KP remote order printers should print the product in " +
                    "KPPrinterDeterminator.getKPRemoteOrderPrintersForProduct", Logger.LEVEL.ERROR);
        }

        return kpRemoteOrderPrintersForProduct;
    }

    /**
     * Determines which KDS remote order printers the product should print to.
     *
     * @param kdsRemoteOrderPrinters {@link ArrayList<PrinterData>} The KDS remote order printers within the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The KDS remote order printers the product should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKDSRemoteOrderPrintersForProduct (ArrayList<PrinterData> kdsRemoteOrderPrinters) {
        ArrayList<PrinterData> kdsRemoteOrderPrintersForProduct = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(kdsRemoteOrderPrinters)) {
                kdsRemoteOrderPrintersForProduct.addAll(kdsRemoteOrderPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KDS remote order printers should print the product in " +
                    "KPPrinterDeterminator.getKDSRemoteOrderPrintersForProduct", Logger.LEVEL.ERROR);
        }

        return kdsRemoteOrderPrintersForProduct;
    }

    /**
     * Determines which KP expeditor as remote order printers the product should print to.
     *
     * @param kpExpeditorAsRemoteOrderPrinters {@link ArrayList<PrinterData>} The KP expeditor as remote order printers within the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The KP expeditor as remote order printers the product should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKPExpeditorAsRemoteOrderPrintersForProduct (ArrayList<PrinterData> kpExpeditorAsRemoteOrderPrinters) {
        ArrayList<PrinterData> kpExpeditorAsRemoteOrderPrintersForProduct = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(kpExpeditorAsRemoteOrderPrinters)) {
                kpExpeditorAsRemoteOrderPrintersForProduct.addAll(kpExpeditorAsRemoteOrderPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KP expeditor as remote order printers should print the product in " +
                    "KPPrinterDeterminator.getKPExpeditorAsRemoteOrderPrintersForProduct", Logger.LEVEL.ERROR);
        }

        return kpExpeditorAsRemoteOrderPrintersForProduct;
    }

    /**
     * Determines which KDS expeditor as remote order printers the product should print to.
     *
     * @param kdsExpeditorAsRemoteOrderPrinters {@link ArrayList<PrinterData>} The KDS expeditor as remote order printers within the revenue center in which the transaction took place.
     * @param productOnKDSPrep Whether or not the product has already been sent to a KDS prep station.
     * @return {@link ArrayList<PrinterData>} The KDS expeditor as remote order printers the product should print on.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "WeakerAccess"})
    public static ArrayList<PrinterData> getKDSExpeditorAsRemoteOrderPrintersForProduct (ArrayList<PrinterData> kdsExpeditorAsRemoteOrderPrinters,
                                                                                         boolean productOnKDSPrep) {
        ArrayList<PrinterData> kdsExpeditorAsRemoteOrderPrintersForProduct = new ArrayList<>();

        try {
            if ((!DataFunctions.isEmptyCollection(kdsExpeditorAsRemoteOrderPrinters)) && (!productOnKDSPrep)) {
                kdsExpeditorAsRemoteOrderPrintersForProduct.addAll(kdsExpeditorAsRemoteOrderPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine which KDS expeditor as remote order printers should print the product in " +
                    "KPPrinterDeterminator.getKDSExpeditorAsRemoteOrderPrintersForProduct", Logger.LEVEL.ERROR);
        }

        return kdsExpeditorAsRemoteOrderPrintersForProduct;
    }

}
