package com.mmhayes.common.receiptGen.ReceiptData;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-06-16 16:57:03 -0400 (Tue, 16 Jun 2020) $: Date of last commit
    $Rev: 12005 $: Revision of last commit
    Notes: Utils for KDS
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.printing.PrintStatus;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.TransactionData.Plu;
import com.mmhayes.common.receiptGen.TransactionData.PrepOption;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.xalan.xsltc.compiler.util.Type;
import org.apache.xpath.operations.Number;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Determines KDS print jobs that should be added to the print queue.
 *
 */
public class KDSJobInfo {

    // private member variables of a KDSJobInfo
    private static final String KDS_JOB_INFO_LOG = "KDSJobInfo.log";
    private ReceiptData receiptData = null;
    private DataManager dataManager = null;
    private HashMap printerToStationMappings = null;

    public static final String PRODUCT_INDICATOR = "__PROD__";
    public static final String MODIFIER_INDICATOR = "__MOD__";

    /**
     * Constructor for a KDSJobInfo.
     *
     * @param receiptData {@link ReceiptData} Information for printing on physical kitchen printer receipts.
     * @param dataManager {@link DataManager} DataManager to be used by the KDSJobInfo.
     * @param printerToStationMappings {@link HashMap} Mappings between printer and station IDs.
     */
    public KDSJobInfo (ReceiptData receiptData, DataManager dataManager, HashMap printerToStationMappings) {
        this.receiptData = receiptData;
        this.dataManager = dataManager;
        this.printerToStationMappings = printerToStationMappings;
    }

    /**
     * Determines which products in the transaction should be displayed on which KDS stations.
     *
     * @return {@link ArrayList<KPJobDetail>} Which products in the transaction should be displayed on which KDS stations.
     */
    @SuppressWarnings("Convert2streamapi")
    public ArrayList<KPJobDetail> getKDSPrintJobs () {
        ArrayList<KPJobDetail> kdsPrintJobs = new ArrayList<>();

        try {
            // get the KDS stations in the same revenue center as the transaction
            ArrayList<PrinterData> kdsStationsInRevCntr = getKDSStationsInRevCntr(receiptData.getTxnData().getRevenueCenterID());
            if (!DataFunctions.isEmptyCollection(kdsStationsInRevCntr)) {
                // get the products in the transaction
                ArrayList<Plu> products = new ArrayList<>();
                for (Object transItem : receiptData.getTxnData().getTransItems()) {
                    if (transItem instanceof Plu) {
                        products.add(((Plu) transItem));
                    }
                }

                // get the KDS prep stations the products are mapped to
                if (!DataFunctions.isEmptyCollection(products)) {
                    ArrayList<HashMap> productToPrepStationMappings = new ArrayList<>();
                    // get all the product IDs
                    //ArrayList<Integer> productIDs = new ArrayList<>(products.stream().map(Plu::getPluID).collect(Collectors.toList()));

                    ArrayList<Integer> productIDs = new ArrayList<>();
                    for (Plu plu : products){
                        productIDs.add(plu.getPluID());
                    }

                    if (!DataFunctions.isEmptyCollection(productIDs)) {
                        int printerHostID = (kdsStationsInRevCntr.get(0) != null ? kdsStationsInRevCntr.get(0).getPrinterHostID() : -1);
                        productToPrepStationMappings = getProductToPrepStationMappings(productIDs, receiptData.getTxnData().getRevenueCenterID(), printerHostID);
                    }

                    if (!DataFunctions.isEmptyCollection(productToPrepStationMappings)) {
                        // set additional properties for products
                        for (Plu product : products) {
                            int pluID = product.getPluID();
                            for (HashMap mappingHM : productToPrepStationMappings) {
                                int mappingPluID = HashMapDataFns.getIntVal(mappingHM, "PAPLUID");
                                if (pluID == mappingPluID) {
                                    boolean printsOnExpeditor = HashMapDataFns.getBooleanVal(mappingHM, "PRINTSONEXPEDITOR");
                                    boolean isDiningOption = HashMapDataFns.getBooleanVal(mappingHM, "ISDININGOPTION");
                                    String mappedStationsStr = HashMapDataFns.getStringVal(mappingHM, "MAPPEDPRINTERS");
                                    ArrayList<Integer> mappedKDSPrepStations = new ArrayList<>();
                                    if (StringFunctions.stringHasContent(mappedStationsStr)) {
                                        String[] mappedStationsStrArr = mappedStationsStr.split("\\s*,\\s*", -1);
                                        if (!DataFunctions.isEmptyGenericArr(mappedStationsStrArr)) {
                                            for (String mapping : mappedStationsStrArr) {
                                                if ((StringFunctions.stringHasContent(mapping)) && (NumberUtils.isNumber(mapping))) {
                                                    mappedKDSPrepStations.add(Integer.parseInt(mapping));
                                                }
                                            }
                                        }
                                    }
                                    product.setPrintsOnExpeditor(printsOnExpeditor);
                                    product.setIsDiningOption(isDiningOption);
                                    product.setKDSPrepStationsMapped(mappedKDSPrepStations);
                                    break;
                                }
                            }
                        }
                        kdsPrintJobs = buildKDSPrintJobs(products, kdsStationsInRevCntr);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, KDS_JOB_INFO_LOG);
            Logger.logMessage(String.format("There was a problem trying to determine which products should be displayed " +
                    "on which stations for the transaction %s in KDSJobInfo.getKDSPrintJobs",
                    Objects.toString(receiptData.getTxnData().getTransactionID(), "NULL")), KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
        }

        return kdsPrintJobs;
    }

    /**
     * Utility method to check whether or not the given transaction was remote.
     *
     * @param transactionID ID of the transaction to check.
     * @return Whether or not the transaction was remote.
     */
    private boolean isTransactionRemote (int transactionID) {
        boolean isTransactionRemote = false;

        try {
            Object queryRes = dataManager.parameterizedExecuteScalar("data.kitchenPrinter.GetRemoteTxn", new Object[]{transactionID});
            if ((queryRes != null) && (!queryRes.toString().isEmpty()) && (queryRes.toString().equalsIgnoreCase("1"))) {
                isTransactionRemote = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, KDS_JOB_INFO_LOG);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the transaction " +
                    "with an ID of %s was remote or not in KDSJobInfo.isTransactionRemote",
                    Objects.toString(transactionID, "NULL")), KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
        }

        return isTransactionRemote;
    }

    /**
     * Queries the database to get all the KDS stations within the given revenue center.
     *
     * @param revenueCenterID ID of the revenue center to get the KDS stations within.
     * @return {@link ArrayList<PrinterData>} All the KDS stations within the given revenue center.
     */
    @SuppressWarnings({"unchecked", "Convert2streamapi"})
    private ArrayList<PrinterData> getKDSStationsInRevCntr (int revenueCenterID) {
        ArrayList<PrinterData> kdsStationsInRevCntr = new ArrayList<>();

        try {
            ArrayList<HashMap> queryRes = dataManager.parameterizedExecuteQuery("data.kitchenPrinter.GetKDSStationsInRevenueCenter", new Object[]{revenueCenterID}, true);
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                for (HashMap hm : queryRes) {
                    if (!DataFunctions.isEmptyMap(hm)) {
                        kdsStationsInRevCntr.add(new PrinterData(hm));
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, KDS_JOB_INFO_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the KDS stations within the revenue " +
                    "center with an ID of %s in KDSJonInfo.getKDSStationsInRevCntr",
                    Objects.toString(revenueCenterID, "NULL")), KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
        }

        return kdsStationsInRevCntr;
    }

    /**
     * Gets the KDS prep stations the given products are mapped to.
     *
     * @param productIDs {@link ArrayList<Integer>} IDs of the products to get the mapped KDS prep stations for.
     * @param revenueCenterID The ID of the revenue center the transaction was made in.
     * @param printerHostID The ID of the printer host that will execute the print job.
     * @return {@link ArrayList<HashMap>} The KDS prep stations the given products are mapped to.
     */
    @SuppressWarnings({"unchecked", "Convert2streamapi", "TypeMayBeWeakened"})
    private ArrayList<HashMap> getProductToPrepStationMappings (ArrayList<Integer> productIDs, int revenueCenterID, int printerHostID) {
        ArrayList<HashMap> productToPrepStationMappings = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(productIDs)) {
                ArrayList<String> productIDStrs = new ArrayList<>(productIDs.stream().map(String::valueOf).collect(Collectors.toList()));
                DynamicSQL dynamicSQL =
                        new DynamicSQL("data.kitchenPrinter.GetKDSPrepStationsMappedToProducts")
                        .addIDList(1, StringFunctions.buildStringFromList(productIDStrs, "?"))
                        .addIDList(2, String.valueOf(revenueCenterID))
                        .addIDList(3, String.valueOf(printerHostID));
                ArrayList<HashMap> queryRes = dynamicSQL.serialize(dataManager);
                if (!DataFunctions.isEmptyCollection(queryRes)) {
                    for (HashMap hm : queryRes) {
                        if (!DataFunctions.isEmptyMap(hm)) {
                            productToPrepStationMappings.add(hm);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, KDS_JOB_INFO_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the prep stations mapped to products " +
                    "with the following IDs %s in KDSJobInfo.getProductToPrepStationMappings",
                    Objects.toString(DataFunctions.convertIntArrayListToStr(productIDs), "NULL")));
        }

        return productToPrepStationMappings;
    }

    /**
     * Builds the print jobs that should be sent to KDS.
     *
     * @param products {@link ArrayList<Plu>} The products in the transaction.
     * @param kdsStations {@link ArrayList<PrinterData>} The configured KDS stations within the revenue center.
     * @return {@link ArrayList<KPJobDetail>} The print jobs that should be sent to KDS.
     */
    @SuppressWarnings({"Convert2streamapi", "ConstantConditions"})
    private ArrayList<KPJobDetail> buildKDSPrintJobs (ArrayList<Plu> products,
                                                      ArrayList<PrinterData> kdsStations) {
        ArrayList<KPJobDetail> kdsPrintJobs = new ArrayList<>();

        try {
            // get the ID of the printer labeled as [ No Printer ]
            int expeditorNoPtrID = 0;
            Object queryRes = dataManager.parameterizedExecuteScalar("data.kitchenPrinter.GetExpPrinterNoPrinter", new Object[]{});
            if ((queryRes != null) && (!queryRes.toString().isEmpty()) && (NumberUtils.isNumber(queryRes.toString()))) {
                expeditorNoPtrID = Integer.parseInt(queryRes.toString());
            }

            // check if the transaction was remote
            boolean isTransactionRemote = isTransactionRemote(Integer.parseInt(receiptData.getTxnData().getTransactionID()));

            // check if an expeditor station has been configured
            PrinterData expeditorStation = getExpeditorStation(isTransactionRemote, expeditorNoPtrID, kdsStations);
            if (expeditorStation != null) {
                Logger.logMessage(String.format("%s is an expeditor station that %s configured as a remote order station, to display the transaction.",
                        Objects.toString(expeditorStation.getName(), "N/A"),
                        Objects.toString(expeditorStation.getIsRemoteOrderExpeditorPrinter() ? "is" : "isn't", "N/A")), KDS_JOB_INFO_LOG, Logger.LEVEL.TRACE);
            }
            else {
                Logger.logMessage("No expeditor station will be used to display the transaction.", KDS_JOB_INFO_LOG, Logger.LEVEL.TRACE);
            }

            // check if a remote order station has been configured
            PrinterData remoteOrderStation = getRemoteOrderStation(isTransactionRemote, kdsStations);
            if (remoteOrderStation != null) {
                Logger.logMessage(String.format("%s is a remote order station that will be used to display the transaction.",
                        Objects.toString(remoteOrderStation.getName(), "N/A")), KDS_JOB_INFO_LOG, Logger.LEVEL.TRACE);
            }
            else {
                Logger.logMessage("No remote order station will be used to display the transaction.", KDS_JOB_INFO_LOG, Logger.LEVEL.TRACE);
            }

            // sort products so that modifiers follow their parent product
            HashMap<Plu, ArrayList<Plu>> sortedProducts = sortProducts(products);

            // get the dining option if there is one
            Plu diningOption = getDiningOption(sortedProducts);
            if (diningOption != null) {
                Logger.logMessage(String.format("The dining option %s has been found within the transaction.",
                        Objects.toString(diningOption.getName(), "N/A")), KDS_JOB_INFO_LOG, Logger.LEVEL.TRACE);
            }
            else {
                Logger.logMessage("No dining option has been found within the transaction.", KDS_JOB_INFO_LOG, Logger.LEVEL.TRACE);
            }

            ArrayList<KPJobDetail> nonDiningOptionPrintJobDetails = createNonDiningOptionPrintJobDetails(kdsStations, sortedProducts, expeditorStation, remoteOrderStation);

            if ((diningOption == null) && (!DataFunctions.isEmptyCollection(nonDiningOptionPrintJobDetails))) {
                kdsPrintJobs.addAll(nonDiningOptionPrintJobDetails);
            }
            else if ((diningOption != null) && (!DataFunctions.isEmptyCollection(nonDiningOptionPrintJobDetails))) {
                ArrayList<KPJobDetail> allPrintJobDetails = getPrintJobDetailsWithDiningOptionDetails(diningOption, nonDiningOptionPrintJobDetails, kdsStations, expeditorStation, remoteOrderStation);
                if (!DataFunctions.isEmptyCollection(allPrintJobDetails)) {
                    kdsPrintJobs.addAll(allPrintJobDetails);
                }
            }
            else if (DataFunctions.isEmptyCollection(nonDiningOptionPrintJobDetails)) {
                Logger.logMessage("No non dining option print job details have been created successfully in KDSJobInfo.buildKDSPrintJobs!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            }

            if ((!DataFunctions.isEmptyCollection(kdsPrintJobs)) && (!DataFunctions.isEmptyMap(sortPrintJobDetailsByPrinter(kdsPrintJobs)))) {
                Logger.logMessage("*** START LOGGING KDS PRINT JOB DETAILS ***", KDS_JOB_INFO_LOG, Logger.LEVEL.TRACE);
                sortPrintJobDetailsByPrinter(kdsPrintJobs).forEach((k, v) -> {
                    Logger.logMessage(String.format("FOR PRINTER ID: %s", Objects.toString(k, "N/A")), KDS_JOB_INFO_LOG, Logger.LEVEL.TRACE);
                    if (!DataFunctions.isEmptyCollection(v)) {
                        v.forEach(d -> Logger.logMessage("--- "+d.toStringCompact(), KDS_JOB_INFO_LOG, Logger.LEVEL.TRACE));
                    }
                });
                Logger.logMessage("*** END LOGGING KDS PRINT JOB DETAILS ***", KDS_JOB_INFO_LOG, Logger.LEVEL.TRACE);
            }

            // add the KDS print jobs to the KPJobInfoDetails so they can be printed locally
            if (!DataFunctions.isEmptyCollection(kdsPrintJobs)) {
                ArrayList<KPJobDetail> details = receiptData.getKpJobInfo().getDetails();
                details.addAll(kdsPrintJobs);
                receiptData.getKpJobInfo().setDetails(details);
            }
        }
        catch (Exception e) {
            Logger.logException(e, KDS_JOB_INFO_LOG);
            Logger.logMessage(String.format("There was a problem trying to build the KDS print jobs for the " +
                    "transaction with an ID of %s in KDSJobInfo.buildKDSPrintJobs",
                    Objects.toString(receiptData.getTxnData().getTransactionID(), "NULL")), KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
        }

        return kdsPrintJobs;
    }

    /**
     * <p>Iterates through the stations to check if there is an expeditor station configured.</p>
     *
     * @param isTransactionRemote Whether or not the transaction was made remotely.
     * @param expeditorNoPtrID ID of an invalid expeditor printer.
     * @param stations An {@link ArrayList} of {@link PrinterData} corresponding to the KDS stations to search through.
     * @return The {@link PrinterData} for the expeditor station.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    private PrinterData getExpeditorStation (boolean isTransactionRemote, int expeditorNoPtrID, ArrayList<PrinterData> stations) {

        if (DataFunctions.isEmptyCollection(stations)) {
            Logger.logMessage("No stations were passed to KDSJobInfo.getExpeditorStation, no expeditor station will be within the revenue center!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        for (PrinterData station : stations) {
            if ((station.getPrinterTypeID() == TypeData.PrinterType.EXPEDITOR_PRINTER)
                    && (!station.getRemoteOrderUseExpeditor())
                    && (station.getPrinterID() != expeditorNoPtrID)) {
                station.setIsRemoteOrderExpeditorPrinter(false);
                return station;
            }
            else if ((station.getPrinterTypeID() == TypeData.PrinterType.EXPEDITOR_PRINTER)
                    && (station.getRemoteOrderUseExpeditor())
                    && (!isTransactionRemote)
                    && (station.getPrinterID() != expeditorNoPtrID)) {
                station.setIsRemoteOrderExpeditorPrinter(false);
                return station;
            }
            else if ((station.getPrinterTypeID() == TypeData.PrinterType.EXPEDITOR_PRINTER)
                    && (station.getRemoteOrderUseExpeditor())
                    && (isTransactionRemote)
                    && (station.getPrinterID() != expeditorNoPtrID)) {
                station.setIsRemoteOrderExpeditorPrinter(true);
                return station;
            }
        }

        return null;
    }

    /**
     * <p>Iterates through the stations to check if there is a remote order station configured.</p>
     *
     * @param isTransactionRemote Whether or not the transaction was made remotely.
     * @param stations An {@link ArrayList} of {@link PrinterData} corresponding to the KDS stations to search through.
     * @return The {@link PrinterData} for the remote order station.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private PrinterData getRemoteOrderStation (boolean isTransactionRemote, ArrayList<PrinterData> stations) {

        if (DataFunctions.isEmptyCollection(stations)) {
            Logger.logMessage("No stations were passed to KDSJobInfo.getRemoteOrderStation, no remote order station will be within the revenue center!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        for (PrinterData station : stations) {
            if ((station.getPrinterTypeID() == TypeData.PrinterType.REMOTE_ORDER_PRINTER) && (isTransactionRemote)) {
                return station;
            }
        }

        return null;
    }

    /**
     * Sort the products so that any modifiers are listed under their parent product.
     *
     * @param products {@link ArrayList<Plu>} The products to sort.
     * @return {@link HashMap<Plu, ArrayList<Plu>>} The sorted products.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private HashMap<Plu, ArrayList<Plu>> sortProducts (ArrayList<Plu> products) {
        HashMap<Plu, ArrayList<Plu>> sortedProducts = new HashMap<>();
        ArrayList<Plu> modsForPlu;

        try {
            if (!DataFunctions.isEmptyCollection(products)) {
                for (Plu product : products) {
                    if (product.getIsModifier()) {
                        Plu parentProduct = product.getParentProduct();
                        if (sortedProducts.containsKey(parentProduct)) {
                            modsForPlu = sortedProducts.get(parentProduct);
                        }
                        else {
                            modsForPlu = new ArrayList<>();
                        }
                        modsForPlu.add(product);
                        sortedProducts.put(parentProduct, modsForPlu);
                    }
                    else {
                        sortedProducts.put(product, new ArrayList<>());
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, KDS_JOB_INFO_LOG);
            Logger.logMessage("There was a problem trying to sort the modifiers by their parent product in " +
                    "KDSJobInfo.sortProducts", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
        }

        return sortedProducts;
    }

    /**
     * <p>Iterates through the given products and checks for a dining option product.</p>
     *
     * @param sortedProducts A {@link HashMap} whose key is a parent {@link Plu} and whose value is an {@link ArrayList} of modifier {@link Plu} that belong to the parent product.
     * @return The dining option {@link Plu}.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private Plu getDiningOption (HashMap<Plu, ArrayList<Plu>> sortedProducts) {

        if (DataFunctions.isEmptyMap(sortedProducts)) {
            Logger.logMessage("No products found in KDSJobInfo.getDiningOption, unable to determine if there is a dining option!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        Plu diningOption = null;
        for (Plu product : sortedProducts.keySet()) {
            if (product.isDiningOption()) {
                diningOption = product;
                break; // dining option found no need to keep processing
            }
        }
        // remove the dining option for the sorted products
        if (diningOption != null) {
            sortedProducts.entrySet().removeIf(entry -> entry.getKey().isDiningOption());
        }

        return diningOption;
    }

    /**
     * <p>Creates print job details for all products within the transaction that aren't dining options.</p>
     *
     * @param stations An {@link ArrayList} of {@link PrinterData} corresponding to KDS stations within the revenue center.
     * @param sortedProducts A {@link HashMap} whose key is a {@link Plu} and whose key is an {@link ArrayList} of {@link Plu} corresponding to the modifiers for the parent product.
     * @param expeditorStation The {@link PrinterData} for the expeditor station in the same revenue center as the transaction.
     * @param remoteOrderStation The {@link PrinterData} for the remote order station in the same revenue center as the transaction.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to the print job details for non dining option products.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    private ArrayList<KPJobDetail> createNonDiningOptionPrintJobDetails (ArrayList<PrinterData> stations,
                                                                         HashMap<Plu, ArrayList<Plu>> sortedProducts,
                                                                         PrinterData expeditorStation,
                                                                         PrinterData remoteOrderStation) {
        ArrayList<KPJobDetail> nonDiningOptionPrintJobDetails = new ArrayList<>();

        if (DataFunctions.isEmptyCollection(stations)) {
            Logger.logMessage("No KDS stations found in KDSJobInfo.createNonDiningOptionPrintJobs, unable to create print job " +
                    "details for non dining option products!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyMap(sortedProducts)) {
            Logger.logMessage("No products found within the transaction in KDSJobInfo.createNonDiningOptionPrintJobs, unable " +
                    "to create print job details for non dining option products!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        for (Map.Entry<Plu, ArrayList<Plu>> productEntry : sortedProducts.entrySet()) {
            Plu product = productEntry.getKey();
            ArrayList<Plu> modifiers = productEntry.getValue();

            // make sure the product isn't null
            if (product == null) {
                continue;
            }

            // make sure the product hasn't been printed yet
            if (product.hasBeenPrinted()) {
                continue;
            }

            boolean productSentToPrepStation = false; // track whether or not the product was sent to a prep station

            final int printStatusID = PrintStatusType.WAITING;
            final int printerHostID = stations.get(0).getPrinterHostID();

            // create print job details for prep stations mapped to the product
            ArrayList<PrinterData> mappedPrepStations = getPrepStationsMappedToProduct(product, stations);
            if (!DataFunctions.isEmptyCollection(mappedPrepStations)) {
                for (PrinterData prepStation : mappedPrepStations) {
                    String hiddenStation = "";
                    if ((expeditorStation != null) && (!product.getPrintsOnExpeditor()) && (!expeditorStation.getIsRemoteOrderExpeditorPrinter())) {
                        hiddenStation = String.valueOf(expeditorStation.getStationID());
                    }
                    ArrayList<KPJobDetail> details = getDetailsForProductOnStation(product, modifiers, prepStation.getPrinterID(), printStatusID, printerHostID, hiddenStation);
                    if (!DataFunctions.isEmptyCollection(details)) {
                        productSentToPrepStation = true;
                        nonDiningOptionPrintJobDetails.addAll(details);
                    }
                }
            }

            // create print job details for the expeditor station
            if ((expeditorStation != null)
                    && (!expeditorStation.getIsRemoteOrderExpeditorPrinter())
                    && (product.getPrintsOnExpeditor())
                    && (!productSentToPrepStation)) {
                ArrayList<KPJobDetail> details = getDetailsForProductOnStation(product, modifiers, expeditorStation.getPrinterID(), printStatusID, printerHostID, "");
                if (!DataFunctions.isEmptyCollection(details)) {
                    nonDiningOptionPrintJobDetails.addAll(details);
                }
            }

            // create print jobs for an expeditor station functioning as a remote order station
            if ((expeditorStation != null)
                    && (expeditorStation.getIsRemoteOrderExpeditorPrinter())
                    && (!productSentToPrepStation)) {
                ArrayList<KPJobDetail> details = getDetailsForProductOnStation(product, modifiers, expeditorStation.getPrinterID(), printStatusID, printerHostID, "");
                if (!DataFunctions.isEmptyCollection(details)) {
                    nonDiningOptionPrintJobDetails.addAll(details);
                }
            }

            // create print jobs for the remote order station
            if (remoteOrderStation != null) {
                ArrayList<KPJobDetail> details = getDetailsForProductOnStation(product, modifiers, remoteOrderStation.getPrinterID(), printStatusID, printerHostID, "");
                if (!DataFunctions.isEmptyCollection(details)) {
                    nonDiningOptionPrintJobDetails.addAll(details);
                }
            }
        }

        return nonDiningOptionPrintJobDetails;
    }

    /**
     * <p>Finds all the prep stations mapped to the given {@link Plu}.</p>
     *
     * @param product The {@link Plu} to find the mapped prep stations for.
     * @param stations An {@link ArrayList} of {@link PrinterData} corresponding to KDS stations within the revenue center.
     * @return An {@link ArrayList} of {@link PrinterData} corresponding to the prep stations mapped to the given {@link Plu}.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private ArrayList<PrinterData> getPrepStationsMappedToProduct (Plu product, ArrayList<PrinterData> stations) {
        ArrayList<PrinterData> prepStations = new ArrayList<>();

        if (product == null) {
            Logger.logMessage("The product passed to KDSJobInfo.getPrepStationsMappedToProduct can't be null!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyCollection(stations)) {
            Logger.logMessage(String.format("No KDS stations found in KDSJobInfo.getPrepStationsMappedToProduct, unable to find prep stations mapped to %s!",
                    Objects.toString(product.getName(), "N/A")), KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<Integer> mappedPrepStationIDs = product.getKDSPrepStationsMapped();
        if (!DataFunctions.isEmptyCollection(mappedPrepStationIDs)) {
            for (int mappedPrepStationID : mappedPrepStationIDs) {
                PrinterData prepStation = getPrepStationByID(mappedPrepStationID, stations);
                if (prepStation != null) {
                    prepStations.add(prepStation);
                }
            }
        }

        return prepStations;
    }

    /**
     * <p>Iterates through the stations to check if there is a prep station with the given printer ID.</p>
     *
     * @param printerID ID of the prep printer to find.
     * @param stations An {@link ArrayList} of {@link PrinterData} corresponding to the KDS stations to search through.
     * @return The {@link PrinterData} for the prep station.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private PrinterData getPrepStationByID (int printerID, ArrayList<PrinterData> stations) {

        if (DataFunctions.isEmptyCollection(stations)) {
            Logger.logMessage(String.format("No stations found in KDSJobInfo.getPrepStationByID, unable to find the prep station with a printer ID of %s!",
                    Objects.toString(printerID, "N/A")), KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // try to find the prep station with the given printer ID
        for (PrinterData station : stations) {
            if ((station != null) && (station.getPrinterID() == printerID)) {
                return station;
            }
        }

        return null;
    }

    /**
     * <p>Creates print job details for the given {@link Plu} on a given station.</p>
     *
     * @param product The {@link Plu} to create print job details for.
     * @param modifiers An {@link ArrayList} of {@link Plu} corresponding to the modifiers for the given {@link Plu}.
     * @param printerID The printer ID of the KDS station.
     * @param printStatusID The ID of the print status for the print job detail.
     * @param printerHostID The ID of the print host that will handle the print job details.
     * @param hiddenStation The {@link String} ID of the station that the print job detail should be hidden on.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to print job details for the given {@link Plu} on the given station.
     */
    @SuppressWarnings({"Convert2streamapi", "TypeMayBeWeakened"})
    private ArrayList<KPJobDetail> getDetailsForProductOnStation (Plu product,
                                                                  ArrayList<Plu> modifiers,
                                                                  int printerID,
                                                                  int printStatusID,
                                                                  int printerHostID,
                                                                  String hiddenStation) {
        ArrayList<KPJobDetail> details = new ArrayList<>();

        if (product == null) {
            Logger.logMessage("The product passed to KDSJobInfo.getDetailsForProductOnStation can't be null!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KDSJobInfo.getDetailsForProductOnStation must be greater than 0!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // create a print job detail for the product
        details.add(new KPJobDetail()
                .printerID(printerID)
                .printStatusID(printStatusID)
                .printerHostID(printerHostID)
                .papluID(product.getPluID())
                .quantity(product.getQuantity())
                .isModifier(product.getIsModifier())
                .lineDetail(buildKDSText(product, false))
                .hiddenStation(hiddenStation));
        // create print job details for any modifiers
        if (!DataFunctions.isEmptyCollection(modifiers)) {
            for (Plu modifier : modifiers) {
                if (modifier != null) {
                    details.add(new KPJobDetail()
                            .printerID(printerID)
                            .printStatusID(printStatusID)
                            .printerHostID(printerHostID)
                            .papluID(modifier.getPluID())
                            .quantity(modifier.getQuantity())
                            .isModifier(modifier.getIsModifier())
                            .lineDetail(buildKDSText(modifier, true))
                            .hiddenStation(hiddenStation));
                }
            }
        }

        return details;
    }

    /**
     * Builds the text to be displayed on KDS.
     *
     * @param plu {@link Plu} The product or modifier to build the name for.
     * @param isModifier Whether or not the given plu is a modifier, if not then it's a product.
     * @return {@link String} The text to be displayed on KDS.
     */
    @SuppressWarnings("SimplifiableBooleanExpression")
    private String buildKDSText (Plu plu, boolean isModifier) {
        String kdsText = "";

        try {
            if (plu != null) {
                String indicatorText = (isModifier ? MODIFIER_INDICATOR : PRODUCT_INDICATOR);
                String pluText = plu.getName();
                PrepOption prepOption = plu.getPrepOption();
                String prepOptionText = "";
                if (prepOption != null) {
                    if (((prepOption.getDefaultOption()) && (prepOption.getDisplayDefaultKitchen())) || (!prepOption.getDefaultOption())) {
                        if (StringFunctions.stringHasContent(prepOption.getOriginalName())) {
                            prepOptionText = " - "+prepOption.getOriginalName();
                        }
                    }
                }
                kdsText = Base64.encodeBase64String((indicatorText+pluText+prepOptionText).getBytes());
            }
        }
        catch (Exception e) {
            Logger.logException(e, KDS_JOB_INFO_LOG);
            Logger.logMessage(String.format("There was a problem trying to build the KDS text for the %s named %s in KDSJobInfo.buildKDSText",
                    Objects.toString((isModifier ? "modifier" : "product"), "NULL"),
                    Objects.toString(plu.getName(), "NULL")), KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
        }

        return kdsText;
    }

    /**
     * <p>Gets all KDS print job details including print job details for the dining option.</p>
     *
     * @param diningOption The {@link Plu} dining option within the transaction.
     * @param nonDiningOptionPrintJobDetails An {@link ArrayList} of {@link KPJobDetail} for products that aren't dining options.
     * @param stations An {@link ArrayList} of {@link PrinterData} corresponding to the KDS stations the revenue center.
     * @param expeditorStation The {@link PrinterData} for the expeditor station in the same revenue center as the transaction.
     * @param remoteOrderStation The {@link PrinterData} for the remote order station in the same revenue center as the transaction.
     * @return An {@link ArrayList} of {@link KPJobDetail} corresponding to all KDS print job details including print jobs details for the dining option within the transaction.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "Convert2streamapi", "OverlyComplexMethod"})
    private ArrayList<KPJobDetail> getPrintJobDetailsWithDiningOptionDetails (Plu diningOption,
                                                                              ArrayList<KPJobDetail> nonDiningOptionPrintJobDetails,
                                                                              ArrayList<PrinterData> stations,
                                                                              PrinterData expeditorStation,
                                                                              PrinterData remoteOrderStation) {
        ArrayList<KPJobDetail> allPrintJobDetails = new ArrayList<>();

        if (diningOption == null) {
            Logger.logMessage("The dining option product passed to KDSJobInfo.getPrintJobDetailsWithDiningOptionDetails can't be null!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyCollection(nonDiningOptionPrintJobDetails)) {
            Logger.logMessage("The print job details for non dining option products passed to KDSJobInfo.getPrintJobDetailsWithDiningOptionDetails " +
                    "can't be null or empty!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyCollection(stations)) {
            Logger.logMessage("No valid KDS stations found in KDSJobInfo.getPrintJobDetailsWithDiningOptionDetails, " +
                    "unable to get all the print job details!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // sort the non dining option print job details by printer
        HashMap<Integer, ArrayList<KPJobDetail>> sortedDetails = sortPrintJobDetailsByPrinter(nonDiningOptionPrintJobDetails);
        if (!DataFunctions.isEmptyMap(sortedDetails)) {
            for (Map.Entry<Integer, ArrayList<KPJobDetail>> sortedDetailsEntry : sortedDetails.entrySet()) {
                int printerID = sortedDetailsEntry.getKey();
                ArrayList<KPJobDetail> printJobDetails = sortedDetailsEntry.getValue();

                if ((expeditorStation != null) && (expeditorStation.getPrinterID() == printerID)) {
                    // the printer ID is for an expeditor station
                    boolean isRemoteExpeditorStation = expeditorStation.getIsRemoteOrderExpeditorPrinter();
                    // hide the dining option on the expeditor
                    if ((((!diningOption.getPrintsOnExpeditor()) && (isRemoteExpeditorStation)) || ((diningOption.getPrintsOnExpeditor()) && (!isRemoteExpeditorStation)))
                            && (!DataFunctions.isEmptyCollection(printJobDetails))) {
                        allPrintJobDetails.add(0, new KPJobDetail()
                                .printerID(printerID)
                                .printStatusID(PrintStatusType.WAITING)
                                .printerHostID(stations.get(0).getPrinterHostID())
                                .papluID(diningOption.getPluID())
                                .quantity(1.0d)
                                .isModifier(diningOption.getIsModifier())
                                .lineDetail(buildKDSText(diningOption, false))
                                .hiddenStation(""));
                    }
                }
                else if ((remoteOrderStation != null) && (remoteOrderStation.getPrinterID() == printerID)) {
                    // the printer ID is for a remote order station
                    if (!DataFunctions.isEmptyCollection(printJobDetails)) {
                        allPrintJobDetails.add(0, new KPJobDetail()
                                .printerID(printerID)
                                .printStatusID(PrintStatusType.WAITING)
                                .printerHostID(stations.get(0).getPrinterHostID())
                                .papluID(diningOption.getPluID())
                                .quantity(1.0d)
                                .isModifier(diningOption.getIsModifier())
                                .lineDetail(buildKDSText(diningOption, false))
                                .hiddenStation(""));
                    }
                }
                else {
                    // the printer ID is for a prep station
                    if ((!DataFunctions.isEmptyCollection(diningOption.getKDSPrepStationsMapped()))
                            && (diningOption.getKDSPrepStationsMapped().contains(printerID))
                            && (!DataFunctions.isEmptyCollection(printJobDetails))) {
                        String hiddenStation = "";
                        if (expeditorStation != null) {
                            hiddenStation = String.valueOf(expeditorStation.getStationID());
                        }
                        allPrintJobDetails.add(0, new KPJobDetail()
                                .printerID(printerID)
                                .printStatusID(PrintStatusType.WAITING)
                                .printerHostID(stations.get(0).getPrinterHostID())
                                .papluID(diningOption.getPluID())
                                .quantity(1.0d)
                                .isModifier(diningOption.getIsModifier())
                                .lineDetail(buildKDSText(diningOption, false))
                                .hiddenStation(hiddenStation));
                    }
                }
            }

            // check if there were products sent to the expeditor but there isn't any expeditor specific print job details
            boolean nonDiningOptionOnPrepAndExpeditor = false;
            for (int printerID : sortedDetails.keySet()) {
                PrinterData prepStation = getPrepStationByID(printerID, stations);
                if ((prepStation != null) && (!DataFunctions.isEmptyCollection(sortedDetails.get(printerID)))) {
                    for (KPJobDetail kpJobDetail : sortedDetails.get(printerID)) {
                        Plu product = getProductByID(kpJobDetail.getPAPluID());
                        if ((product != null) && (product.getPrintsOnExpeditor())) {
                            nonDiningOptionOnPrepAndExpeditor = true;
                            break;
                        }
                    }
                    if (nonDiningOptionOnPrepAndExpeditor) {
                        break;
                    }
                }
            }
            // create the print job if necessary
            if ((nonDiningOptionOnPrepAndExpeditor) && (expeditorStation != null)
                    && (((!expeditorStation.getIsRemoteOrderExpeditorPrinter()) && (diningOption.getPrintsOnExpeditor())) || (expeditorStation.getIsRemoteOrderExpeditorPrinter()))) {
                allPrintJobDetails.add(0, new KPJobDetail()
                        .printerID(expeditorStation.getPrinterID())
                        .printStatusID(PrintStatusType.WAITING)
                        .printerHostID(stations.get(0).getPrinterHostID())
                        .papluID(diningOption.getPluID())
                        .quantity(1.0d)
                        .isModifier(diningOption.getIsModifier())
                        .lineDetail(buildKDSText(diningOption, false))
                        .hiddenStation(""));
            }
        }

        // add all the print job details to the list to return
        if (!DataFunctions.isEmptyMap(sortedDetails)) {
            for (ArrayList<KPJobDetail> details : sortedDetails.values()) {
                allPrintJobDetails.addAll(details);
            }
        }

        return allPrintJobDetails;
    }

    /**
     * <p>Sorts the given print job details by their printer ID.</p>
     *
     * @param details An {@link ArrayList} of {@link KPJobDetail} corresponding to the print job details to sort.
     * @return A {@link HashMap} whose key is the {@link Integer} ID of the printer and whose value is an {@link ArrayList} of {@link KPJobDetail} that will be sent to the printer.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private HashMap<Integer, ArrayList<KPJobDetail>> sortPrintJobDetailsByPrinter (ArrayList<KPJobDetail> details) {
        HashMap<Integer, ArrayList<KPJobDetail>> sortedDetails = new HashMap<>();
        ArrayList<KPJobDetail> detailsForPrinter;

        if (DataFunctions.isEmptyCollection(details)) {
            Logger.logMessage("No print job details passed to KDSJobInfo.sortPrintJobDetailsByPrinter, unable to sort the print job details!", KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // do the sorting
        for (KPJobDetail kpJobDetail : details) {
            if (sortedDetails.containsKey(kpJobDetail.getPrinterID())) {
                detailsForPrinter = sortedDetails.get(kpJobDetail.getPrinterID());
            }
            else {
                detailsForPrinter = new ArrayList<>();
            }
            detailsForPrinter.add(kpJobDetail);
            sortedDetails.put(kpJobDetail.getPrinterID(), detailsForPrinter);
        }

        return sortedDetails;
    }

    /**
     * <p>Gets the products object for the given product ID.</p>
     *
     * @param paPluID ID of the product to get.
     * @return The {@link Plu} for the given product ID.
     */
    @SuppressWarnings("Convert2streamapi")
    private Plu getProductByID (int paPluID) {
        Plu plu = null;

        if (receiptData == null) {
            Logger.logMessage(String.format("There must receipt data in KDSJobInfo.getProductByID, unable to get the Plu with an ID of %s!",
                    Objects.toString(paPluID, "N/A")), KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if (receiptData.getTxnData() == null) {
            Logger.logMessage(String.format("There must transaction data in KDSJobInfo.getProductByID, unable to get the Plu with an ID of %s!",
                    Objects.toString(paPluID, "N/A")), KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyCollection(receiptData.getTxnData().getTransItems())) {
            Logger.logMessage(String.format("There must be products within the transaction in KDSJobInfo.getProductByID, " +
                    "unable to get the Plu with an ID of %s!",
                    Objects.toString(paPluID, "N/A")), KDS_JOB_INFO_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        for (Object o : receiptData.getTxnData().getTransItems()) {
            if ((o != null) && (o instanceof Plu)) {
                Plu product = ((Plu) o);
                if (product.getPluID() == paPluID) {
                    plu = product;
                    break;
                }
            }
        }

        return plu;
    }

}
