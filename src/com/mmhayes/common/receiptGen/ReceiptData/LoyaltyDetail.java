package com.mmhayes.common.receiptGen.ReceiptData;

/**
 * Created by nyu on 4/12/2017.
 */
public class LoyaltyDetail
{
    private final static String COMMON_LOG_FILE = "ConsolidatedReceipt.log";
    
    private String programName = "";
    private String pointsBalance = "";
    private String pointsEarned = "";
    private String pointsRedeemed = "";

    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public String getProgramName()
    {
        return programName;
    }

    public String getPointsBalance()
    {
        return pointsBalance;
    }

    public String getPointsEarned()
    {
        return pointsEarned;
    }

    public String getPointsRedeemed()
    {
        return pointsRedeemed;
    }
    
    public void setProgramName(String prgmName)
    {
        if (prgmName != null)
        {
            programName = prgmName;
        }
    }

    public void setPointsBalance(String ptsBal)
    {
        if (ptsBal != null)
        {
            pointsBalance = ptsBal;
        }
    }

    public void setPointsEarned(String ptsEarned)
    {
        if (ptsEarned != null)
        {
            pointsEarned = ptsEarned;
        }
    }

    public void setPointsRedeemed(String ptsRedeemed)
    {
        if (ptsRedeemed != null)
        {
            pointsRedeemed = ptsRedeemed;
        }
    }
}
