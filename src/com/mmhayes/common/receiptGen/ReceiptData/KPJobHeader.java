package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-12 15:23:14 -0400 (Tue, 12 May 2020) $: Date of last commit
    $Rev: 11713 $: Revision of last commit
    Notes: Class representing the header to be printed on a receipt.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Class representing the header to be printed on a receipt.
 *
 */
public class KPJobHeader {

    // log file for the KPJobHeader
    private static final String KPJH_LOG = "KitchenPrinter.log";

    // variables related to the transaction
    private int paTransactionID = 0;
    private boolean isOnlineOrder = false;
    private boolean isRemoteOrder = false;
    private String transactionDate = "";
    private int terminalID = 0;
    private String orderNumber = "";
    private int transTypeID = 0;
    private String prevOrderNumsForKDS = "";

    // variables related to the kitchen printer
    private int printStatusID = PrintJobStatusType.WAITING.getTypeID();
    private String queueDateTime = "";
    private int orderTypeID = 0;
    private String estimatedOrderTime = "";

    // variables related to contact/notes
    private String address = "";
    private String personName = "";
    private String phoneNumber = "";
    private String transName = "";
    private String transNameLabel = "";
    private String transComment = "";
    private String pickupDeliveryNote = "";

    // data manager to be used by the KPJobHeader
    private DataManager kpjhDataManager = null;

    /**
     * Constructor for a KPJobHeader.
     *
     * @param paTransactionID ID of the transaction.
     * @param terminalID ID of the terminal the transaction took place on.
     * @param kpjhDataManager The DataManager to be used by the KPJobHeader
     */
    public KPJobHeader (int paTransactionID, int terminalID, DataManager kpjhDataManager) {
        this.paTransactionID = paTransactionID;
        this.terminalID = terminalID;
        this.kpjhDataManager = kpjhDataManager;
    }

    /**
     * Overriden toString method.
     *
     * @return {@link String} String version of a KPJobHeader.
     */
    @Override
    public String toString () {
        return String.format("PATRANSACTIONID: %s, ISONLINEORDER: %s, ISREMOTEORDER: %s, TRANSACTIONDATE: %s, " +
                "TERMINALID: %s, ORDERNUMBER: %s, TRANSTYPEID: %s, PREVORDERNUMSFORKDS: %s, PRINTSTATUSID: %s, " +
                "QUEUEDATETIME: %s, ORDERTYPEID: %s, ESTIMATEDORDERTIME: %s, ADDRESS: %s, PERSONNAME: %s, " +
                "PHONENUMBER: %s, TRANSNAME: %s, TRANSNAMELABEL: %s, TRANSCOMMENT: %s, PICKUPDELIVERYNOTE: %s",
                Objects.toString(paTransactionID, "NULL"),
                Objects.toString((isOnlineOrder ? "true" : "false"), "NULL"),
                Objects.toString((isRemoteOrder ? "true" : "false"), "NULL"),
                Objects.toString(transactionDate, "NULL"),
                Objects.toString(terminalID, "NULL"),
                Objects.toString(orderNumber, "NULL"),
                Objects.toString(transTypeID, "NULL"),
                Objects.toString(prevOrderNumsForKDS, "NULL"),
                Objects.toString(printStatusID, "NULL"),
                Objects.toString(queueDateTime, "NULL"),
                Objects.toString(orderTypeID, "NULL"),
                Objects.toString(estimatedOrderTime, "NULL"),
                Objects.toString(address, "NULL"),
                Objects.toString(personName, "NULL"),
                Objects.toString(phoneNumber, "NULL"),
                Objects.toString(transName, "NULL"),
                Objects.toString(transNameLabel, "NULL"),
                Objects.toString(transComment, "NULL"),
                Objects.toString(pickupDeliveryNote, "NULL"));
    }

    /**
     * Query the database and set the results for information that should be in the header of the receipt to be printed.
     *
     */
    @SuppressWarnings("unchecked")
    public void getPrintQueueHeaderInfo () {

        try {
            if (kpjhDataManager != null) {

                // query the database
                ArrayList<HashMap> printJobHeaderInfo = kpjhDataManager.parameterizedExecuteQuery(
                        "data.ReceiptGen.GetTrxDetail", new Object[]{paTransactionID}, KPJH_LOG, true);

                if ((printJobHeaderInfo != null) && (!printJobHeaderInfo.isEmpty())) {

                    HashMap headerInfo = printJobHeaderInfo.get(0);

                    isOnlineOrder = HashMapDataFns.getBooleanVal(headerInfo, "ONLINEORDER");
                    transactionDate = HashMapDataFns.getStringVal(headerInfo, "TRANSACTIONDATE");
                    queueDateTime = HashMapDataFns.getStringVal(headerInfo, "QUEUETIME");
                    estimatedOrderTime = HashMapDataFns.getStringVal(headerInfo, "ESTIMATEDORDERTIME");
                    orderTypeID = HashMapDataFns.getIntVal(headerInfo, "PAORDERTYPEID");
                    if (orderTypeID == -1) {
                        // this will be our default order type value
                        orderTypeID = TypeData.OrderType.NORMAL_SALE;
                    }
                    address = HashMapDataFns.getStringVal(headerInfo, "ADDRESS");
                    personName = HashMapDataFns.getStringVal(headerInfo, "PERSONNAME");
                    String printerQueuePhoneNum = HashMapDataFns.getStringVal(headerInfo, "PRINTERQUEUEPHONE");
                    String txnPhoneNum = HashMapDataFns.getStringVal(headerInfo, "PHONE");
                    phoneNumber = sanitizePhoneNumber(
                            (printerQueuePhoneNum.isEmpty() ? txnPhoneNum : printerQueuePhoneNum), true);
                    transName = HashMapDataFns.getStringVal(headerInfo, "TRANSNAME");
                    transNameLabel = HashMapDataFns.getStringVal(headerInfo, "TRANSNAMELABEL");
                    String transactionComment = HashMapDataFns.getStringVal(headerInfo, "TRANSCOMMENT");
                    String printerQueueCmt = HashMapDataFns.getStringVal(headerInfo, "PRINTERQUEUECOMMENT");
                    transComment = (transactionComment.isEmpty() ? printerQueueCmt : transactionComment);
                    pickupDeliveryNote = HashMapDataFns.getStringVal(headerInfo, "PICKUPDELIVERYNOTE");
                    String printerQueueOrderNum = HashMapDataFns.getStringVal(headerInfo, "PRINTERQUEUEORDERNUM");
                    String txnOrderNum = HashMapDataFns.getStringVal(headerInfo, "ORDERNUM");
                    orderNumber = (printerQueueOrderNum.isEmpty() ? txnOrderNum : printerQueueOrderNum);
                    transTypeID = HashMapDataFns.getIntVal(headerInfo, "TRANSTYPEID");
                }
                else {
                    Logger.logMessage("Unexpected response received from the query data.ReceiptGen.GetTrxDetail in " +
                            "KPJobHeader.getPrintQueueHeaderInfo", KPJH_LOG, Logger.LEVEL.ERROR);
                }

            }
            else {
                Logger.logMessage("Unable to get the header information for the receipt because there isn't a valid " +
                        "DataManager in KPJobHeader.getPrintQueueHeaderInfo", KPJH_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPJH_LOG);
            Logger.logMessage("There was a problem trying to get the header information for the receipt in " +
                    "KPJobHeader.getPrintQueueHeaderInfo", KPJH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Removes non-numeric characters from the given phone number and appends the country code for the United States
     * if the condition to do so is met.
     *
     * @param phoneNumber {@link String} The phone number to sanitize.
     * @param addUSCountryCode Where or not the country code for the United States should be appended to the phone number.
     * @return {@link String} The sanitized phone number.
     */
    public static String sanitizePhoneNumber (String phoneNumber, boolean addUSCountryCode) {
        String sanitizedPhoneNumber = "";

        try {
            // remove any non-numeric characters from the phone number
            if ((phoneNumber != null) && (!phoneNumber.isEmpty())) {
                sanitizedPhoneNumber = phoneNumber.replaceAll("[^0-9]", "");
            }

            // append the US country code to the phone number
            if ((addUSCountryCode) && (sanitizedPhoneNumber.length() == 10)) {
                sanitizedPhoneNumber = "1" + sanitizedPhoneNumber;
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPJH_LOG);
            Logger.logMessage("There was a problem trying to sanitize the phone number " +
                            Objects.toString(phoneNumber, "NULL")+" in KPJobHeader.sanitizePhoneNumber", KPJH_LOG,
                    Logger.LEVEL.ERROR);
        }

        return sanitizedPhoneNumber;
    }

    /**
     * Setter for the KPJobHeader's paTransactionID field.
     *
     * @param paTransactionID The ID of the transaction.
     */
    public void setPATransactionID (int paTransactionID) {
        this.paTransactionID = paTransactionID;
    }

    /**
     * Getter for the KPJobHeader's paTransactionID field.
     *
     * @return The ID of the transaction.
     */
    public int getPATransactionID () {
        return paTransactionID;
    }

    /**
     * Setter for the KPJobHeader's isOnlineOrder field.
     *
     * @param isOnlineOrder Whether or not the transaction was an online order.
     */
    public void setIsOnlineOrder (boolean isOnlineOrder) {
        this.isOnlineOrder = isOnlineOrder;
    }

    /**
     * Getter for the KPJobHeader's isOnlineOrder field.
     *
     * @return Whether or not the transaction was an online order.
     */
    public boolean getIsOnlineOrder () {
        return isOnlineOrder;
    }

    /**
     * Setter for the KPJobHeader's isRemoteOrder field.
     *
     * @param isRemoteOrder Whether or not the transaction was a remote order.
     */
    public void setIsRemoteOrder (boolean isRemoteOrder) {
        this.isRemoteOrder = isRemoteOrder;
    }

    /**
     * Getter for the KPJobHeader's isRemoteOrder field.
     *
     * @return Whether or not the transaction was a remote order.
     */
    public boolean getIsRemoteOrder () {
        return isRemoteOrder;
    }

    /**
     * Setter for the KPJobHeader's transactionDate field.
     *
     * @param transactionDate {@link String} Date of the transaction.
     */
    public void setTransactionDate (String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * Getter for the KPJobHeader's transactionDate field.
     *
     * @return {@link String} Date of the transaction.
     */
    public String getTransactionDate () {
        return transactionDate;
    }

    /**
     * Setter for the KPJobHeader's terminalID field.
     *
     * @param terminalID The ID of the terminal the transaction took place on.
     */
    public void setTerminalID (int terminalID) {
        this.terminalID = terminalID;
    }

    /**
     * Getter for the KPJobHeader's terminalID field.
     *
     * @return The ID of the terminal the transaction took place on.
     */
    public int getTerminalID () {
        return terminalID;
    }

    /**
     * Setter for the KPJobHeader's orderNumber field.
     *
     * @param orderNumber {@link String} Number of the order.
     */
    public void setOrderNumber (String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * Getter for the KPJobHeader's orderNumber field.
     *
     * @return {@link String} Number of the order.
     */
    public String getOrderNumber () {
        return orderNumber;
    }

    /**
     * Setter for the KPJobHeader's transTypeID field.
     *
     * @param transTypeID The ID of the type of transaction that took place.
     */
    public void setTransTypeID (int transTypeID) {
        this.transTypeID = transTypeID;
    }

    /**
     * Getter for the KPJobHeader's transTypeID field.
     *
     * @return The ID of the type of transaction that took place.
     */
    public int getTransTypeID () {
        return transTypeID;
    }

    /**
     * Setter for the KPJobHeader's prevOrderNumsForKDS field.
     *
     * @param prevOrderNumsForKDS {@link String} Previous order numbers of an open transaction to be displayed on KDS.
     */
    public void setPrevOrderNumsForKDS (String prevOrderNumsForKDS) {
        this.prevOrderNumsForKDS = prevOrderNumsForKDS;
    }

    /**
     * Getter for the KPJobHeader's prevOrderNumsForKDS field.
     *
     * @return {@link String} Previous order numbers of an open transaction to be displayed on KDS.
     */
    public String getPrevOrderNumsForKDS () {
        return prevOrderNumsForKDS;
    }

    /**
     * Setter for the KPJobHeader's printStatusID field.
     *
     * @param printStatusID The status of the print job.
     */
    public void setPrintStatusID (int printStatusID) {
        this.printStatusID = printStatusID;
    }

    /**
     * Getter for the KPJobHeader's printStatusID field.
     *
     * @return The status of the print job.
     */
    public int getPrintStatusID () {
        return printStatusID;
    }

    /**
     * Setter for the KPJobHeader's queueDateTime field.
     *
     * @param queueDateTime {@link String} Time the print job was added to the print queue.
     */
    public void setQueueDateTime (String queueDateTime) {
        this.queueDateTime = queueDateTime;
    }

    /**
     * Getter for the KPJobHeader's queueDateTime field.
     *
     * @return {@link String} Time the print job was added to the print queue.
     */
    public String getQueueDateTime () {
        return queueDateTime;
    }

    /**
     * Setter for the KPJobHeader's orderTypeID field.
     *
     * @param orderTypeID The ID of the type of order that was placed.
     */
    public void setOrderTypeID (int orderTypeID) {
        this.orderTypeID = orderTypeID;
    }

    /**
     * Getter for the KPJobHeader's orderTypeID field.
     *
     * @return The ID of the type of order that was placed.
     */
    public int getOrderTypeID () {
        return orderTypeID;
    }

    /**
     * Setter for the KPJobHeader's estimatedOrderTime field.
     *
     * @param estimatedOrderTime {@link String} Time estimate of when the order will be complete.
     */
    public void setEstimatedOrderTime (String estimatedOrderTime) {
        this.estimatedOrderTime = estimatedOrderTime;
    }

    /**
     * Getter for the KPJobHeader's estimatedOrderTime field.
     *
     * @return {@link String} Time estimate of when the order will be complete.
     */
    public String getEstimatedOrderTime () {
        return estimatedOrderTime;
    }

    /**
     * <p>Getter for the address field of the {@link KPJobHeader}.</p>
     *
     * @return The address field of the {@link KPJobHeader}.
     */
    public String getAddress () {
        return address;
    }

    /**
     * <p>Setter for the address field of the {@link KPJobHeader}.</p>
     *
     * @param address The address field of the {@link KPJobHeader}.
     */
    public void setAddress (String address) {
        this.address = address;
    }

    /**
     * Setter for the KPJobHeader's personName field.
     *
     * @param personName {@link String} Name of the person who placed the order.
     */
    public void setPersonName (String personName) {
        this.personName = personName;
    }

    /**
     * Getter for the KPJobHeader's personName field.
     *
     * @return {@link String} Name of the person who placed the order.
     */
    public String getPersonName () {
        return personName;
    }

    /**
     * Setter for the KPJobHeader's phoneNumber field.
     *
     * @param phoneNumber {@link String} Contact phone number.
     */
    public void setPhoneNumber (String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Getter for the KPJobHeader's phoneNumber field.
     *
     * @return {@link String} Contact phone number.
     */
    public String getPhoneNumber () {
        return phoneNumber;
    }

    /**
     * Setter for the KPJobHeader's transName field.
     *
     * @param transName {@link String} Name of the transaction.
     */
    public void setTransName (String transName) {
        this.transName = transName;
    }

    /**
     * Getter for the KPJobHeader's transName field.
     *
     * @return {@link String} Name of the transaction.
     */
    public String getTransName () {
        return transName;
    }

    /**
     * Setter for the KPJobHeader's transNameLabel field.
     *
     * @param transNameLabel {@link String} Label for the transaction name.
     */
    public void setTransNameLabel (String transNameLabel) {
        this.transNameLabel = transNameLabel;
    }

    /**
     * Getter for the KPJobHeader's transNameLabel field.
     *
     * @return {@link String} Label for the transaction name.
     */
    public String getTransNameLabel () {
        return transNameLabel;
    }

    /**
     * Setter for the KPJobHeader's transComment field.
     *
     * @param transComment {@link String} Comment related to the transaction.
     */
    public void setTransComment (String transComment) {
        this.transComment = transComment;
    }

    /**
     * Getter for the KPJobHeader's transComment field.
     *
     * @return {@link String} Comment related to the transaction.
     */
    public String getTransComment () {
        return transComment;
    }

    /**
     * Setter for the KPJobHeader's pickupDeliveryNote field.
     *
     * @param pickupDeliveryNote {@link String} Note related to pickup or delivery.
     */
    public void setPickupDeliveryNote (String pickupDeliveryNote) {
        this.pickupDeliveryNote = pickupDeliveryNote;
    }

    /**
     * Getter for the KPJobHeader's pickupDeliveryNote field.
     *
     * @return {@link String} Note related to pickup or delivery.
     */
    public String getPickupDeliveryNote () {
        return pickupDeliveryNote;
    }

}

