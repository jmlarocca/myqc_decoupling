package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-15 14:50:29 -0400 (Fri, 15 May 2020) $: Date of last commit
    $Rev: 11756 $: Revision of last commit
    Notes: Contains information about the account which made the transaction.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Contains information about the account which made the transaction.</p>
 *
 */
public class AccountData {

    // private member variables of an AccountData
    private String name = "";
    private String badgeAssignmentID = "";
    private String badgeNumber = "";
    private String employeeID = "";
    private String employeeNumber = "";
    private int numSplits;
    private int receiptBalanceTypeID = 1;
    private String receiptBalancePromptTxt = "";
    private double globalBalance = 0.0d;
    private double globalAvailable = 0.0d;
    private double storeBalance = 0.0d;
    private double storeAvailable = 0.0d;
    private double dailyLimitBal = 0.0d;

    /**
     * <p>Constructor for an {@link AccountData}.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param terminalID ID of the terminal the transaction was made on.
     * @param tenderAmount The amount that the transaction was for.
     * @param employeeID The ID of the account as a {@link String} that made the transaction.
     * @param badgeAssignmentID The ID of the badge as a {@link String} used to tender the transaction.
     * @param splits The number of splits used to tender the transaction.
     */
    public AccountData (DataManager dataManager, int terminalID, double tenderAmount, String employeeID, String badgeAssignmentID, int splits) {
        this.employeeID = employeeID;
        this.badgeAssignmentID = badgeAssignmentID;
        this.numSplits = splits;
        lookupAccountInfo(dataManager, terminalID, tenderAmount);
    }

    /**
     * <p>Sets additional information about the account that made the transaction.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param terminalID ID of the terminal the transaction was made on.
     * @param tenderAmount The amount that the transaction was for.
     */
    private void lookupAccountInfo (DataManager dataManager, int terminalID, double tenderAmount) {

        try {
            ArrayList<HashMap> accountDetails = getAccountDetails(dataManager, terminalID, tenderAmount);
            if ((!DataFunctions.isEmptyCollection(accountDetails)) && (accountDetails.size() == 1) && (!DataFunctions.isEmptyMap(accountDetails.get(0)))) {
                HashMap accountDetailsHM = accountDetails.get(0);

                double globalLimit = HashMapDataFns.getDoubleVal(accountDetailsHM, "GLOBALLIMIT");
                this.globalBalance = HashMapDataFns.getDoubleVal(accountDetailsHM, "GLOBALBALANCE");
                this.globalAvailable = globalLimit - globalBalance;

                double storeLimit = HashMapDataFns.getDoubleVal(accountDetailsHM, "STORELIMIT");
                this.storeBalance = HashMapDataFns.getDoubleVal(accountDetailsHM, "STOREBALANCE");
                this.storeAvailable = storeLimit - storeBalance;

                this.name = HashMapDataFns.getStringVal(accountDetailsHM, "NAME");
                this.employeeNumber = HashMapDataFns.getStringVal(accountDetailsHM, "EMPLOYEENUMBER");

                if ((StringFunctions.stringHasContent(this.badgeAssignmentID)) && (NumberUtils.isNumber(badgeAssignmentID))) {
                    BigDecimal badgeAssignmentIDAsBD = new BigDecimal(this.badgeAssignmentID);
                    if (badgeAssignmentIDAsBD.compareTo(BigDecimal.ZERO) > 0) {
                        this.badgeNumber = HashMapDataFns.getStringVal(accountDetailsHM, "BADGENUMBER");
                    }
                    else {
                        this.badgeNumber = HashMapDataFns.getStringVal(accountDetailsHM, "BADGE");
                    }
                }
                else {
                    this.badgeNumber = HashMapDataFns.getStringVal(accountDetailsHM, "BADGE");
                }

                this.receiptBalanceTypeID = HashMapDataFns.getIntVal(accountDetailsHM, "RECEIPTBALANCETYPEID");
                this.receiptBalancePromptTxt = HashMapDataFns.getStringVal(accountDetailsHM, "RECEIPTBALANCEPROMPT");
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to get additional account details for the account with an ID of %s in AccountData.lookupAccountInfo",
                    Objects.toString(this.employeeID, "N/A")), Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Queries the database to get additional information about the account that made the transaction.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param terminalID ID of the terminal the transaction was made on.
     * @param tenderAmount The amount that the transaction was for.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to additional account details.
     */
    @SuppressWarnings("unchecked")
    private ArrayList<HashMap> getAccountDetails (DataManager dataManager, int terminalID, double tenderAmount) {
        ArrayList<HashMap> accountDetails = new ArrayList<>();

        try {
            // get badge information
            int badgeID = 0;
            String badgeNumber = "";
            if (StringFunctions.stringHasContent(this.badgeAssignmentID)) {
                ArrayList<HashMap> queryRes = dataManager.parameterizedExecuteQuery("data.posanywhere.BadgeNumberLookup", new Object[]{badgeAssignmentID}, true);
                if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
                    HashMap hm = queryRes.get(0);
                    badgeID = HashMapDataFns.getIntVal(hm, "BADGEID");
                    badgeNumber = HashMapDataFns.getStringVal(hm, "BADGENUMBER");
                }
            }

            if ((dataManager != null) && (terminalID > 0) && (StringFunctions.stringHasContent(this.employeeID))) {
                ArrayList<HashMap> queryRes = dataManager.parameterizedExecuteQuery("data.posanywhere.PersonTerminalLookupBP", new Object[]{terminalID, tenderAmount, this.employeeID}, true);
                if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
                    HashMap hm = queryRes.get(0);

                    // add badge information into the HashMap
                    hm.put("BADGEID", badgeID);
                    hm.put("BADGENUMBER", badgeNumber);

                    int numPayments = HashMapDataFns.getIntVal(hm, "NUMPAYMENTS");
                    if (numPayments > 1) {
                        double available;

                        double globalLimit = HashMapDataFns.getDoubleVal(hm, "GLOBALLIMIT");
                        double globalBalance = HashMapDataFns.getDoubleVal(hm, "GLOBALBALANCE");
                        double globalAvailable = globalLimit - globalBalance;

                        double storeLimit = HashMapDataFns.getDoubleVal(hm, "STORELIMIT");
                        double storeBalance = HashMapDataFns.getDoubleVal(hm, "STOREBALANCE");
                        double storeAvailable = storeLimit - storeBalance;

                        double transactionLimit = HashMapDataFns.getDoubleVal(hm, "SINGLECHARGELIMIT");

                        // calculate how much is available
                        if (globalAvailable < storeAvailable) {
                            if (globalAvailable < transactionLimit) {
                                available = globalAvailable;
                            }
                            else {
                                available = transactionLimit;
                            }
                        }
                        else {
                            if (storeAvailable < transactionLimit) {
                                available = storeAvailable;
                            }
                            else {
                                available = transactionLimit;
                            }
                        }

                        if (available < tenderAmount) {
                            queryRes = dataManager.parameterizedExecuteQuery("data.posanywhere.PersonTerminalLookup", new Object[]{terminalID, available, this.employeeID}, true);

                            // add badge information into the HashMap
                            hm.put("BADGEID", badgeID);
                            hm.put("BADGENUMBER", badgeNumber);

                            accountDetails = queryRes;
                        }
                        else {
                            accountDetails = queryRes;
                        }
                    }
                    else {
                        accountDetails = queryRes;
                    }
                }
                else {
                    accountDetails = queryRes;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to get the account details for the account with an ID of %s in AccountData.getAccountDetails",
                    Objects.toString(this.employeeID, "N/A")), Logger.LEVEL.ERROR);
        }

        return accountDetails;
    }

    /**
     * <p>Overridden toString() method for a {@link AccountData}.</p>
     *
     * @return A {@link String} representation of this {@link AccountData}.
     */
    @Override
    public String toString () {

        return String.format("NAME: %s, BADGEASSIGNMENTID: %s, BADGENUMBER: %s, EMPLOYEEID: %s, EMPLOYEENUMBER: %s, NUMSPLITS: %s, " +
                "RECEIPTBALANCETYPEID: %s, RECEIPTBALANCEPROMPTTXT: %s, GLOBALBALANCE: %s, GLOBALAVAILABLE: %s, STOREBALANCE: %s, STOREAVAILABLE: %s, " +
                "DAILYLIMITBAL: %s",
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(badgeAssignmentID) ? badgeAssignmentID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(badgeNumber) ? badgeNumber : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(employeeID) ? employeeID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(employeeNumber) ? employeeNumber : "N/A"), "N/A"),
                Objects.toString((numSplits != -1 ? numSplits : "N/A"), "N/A"),
                Objects.toString((receiptBalanceTypeID != -1 ? receiptBalanceTypeID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(receiptBalancePromptTxt) ? receiptBalancePromptTxt : "N/A"), "N/A"),
                Objects.toString((globalBalance != -1.0d ? globalBalance : "N/A"), "N/A"),
                Objects.toString((globalAvailable != -1.0d ? globalAvailable : "N/A"), "N/A"),
                Objects.toString((storeBalance != -1.0d ? storeBalance : "N/A"), "N/A"),
                Objects.toString((storeAvailable != -1.0d ? storeAvailable : "N/A"), "N/A"),
                Objects.toString((dailyLimitBal != -1.0d ? dailyLimitBal : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link AccountData}.
     * Two {@link AccountData} are defined as being equal if they have the same employeeID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link AccountData}.
     * @return Whether or not the {@link Object} is equal to this {@link AccountData}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a AccountData and check if the obj's employeeID is equal to this AccountData's employeeID
        AccountData accountData = ((AccountData) obj);
        return Objects.equals(accountData.employeeID, employeeID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link AccountData}.</p>
     *
     * @return The unique hash code for this {@link AccountData}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(employeeID);
    }

    /**
     * <p>Getter for the name field of the {@link AccountData}.</p>
     *
     * @return The name field of the {@link AccountData}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link AccountData}.</p>
     *
     * @param name The name field of the {@link AccountData}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the badgeAssignmentID field of the {@link AccountData}.</p>
     *
     * @return The badgeAssignmentID field of the {@link AccountData}.
     */
    public String getBadgeAssignmentID () {
        return badgeAssignmentID;
    }

    /**
     * <p>Setter for the badgeAssignmentID field of the {@link AccountData}.</p>
     *
     * @param badgeAssignmentID The badgeAssignmentID field of the {@link AccountData}.
     */
    public void setBadgeAssignmentID (String badgeAssignmentID) {
        this.badgeAssignmentID = badgeAssignmentID;
    }

    /**
     * <p>Getter for the badgeNumber field of the {@link AccountData}.</p>
     *
     * @return The badgeNumber field of the {@link AccountData}.
     */
    public String getBadgeNumber () {
        return badgeNumber;
    }

    /**
     * <p>Setter for the badgeNumber field of the {@link AccountData}.</p>
     *
     * @param badgeNumber The badgeNumber field of the {@link AccountData}.
     */
    public void setBadgeNumber (String badgeNumber) {
        this.badgeNumber = badgeNumber;
    }

    /**
     * <p>Getter for the employeeID field of the {@link AccountData}.</p>
     *
     * @return The employeeID field of the {@link AccountData}.
     */
    public String getEmployeeID () {
        return employeeID;
    }

    /**
     * <p>Setter for the employeeID field of the {@link AccountData}.</p>
     *
     * @param employeeID The employeeID field of the {@link AccountData}.
     */
    public void setEmployeeID (String employeeID) {
        this.employeeID = employeeID;
    }

    /**
     * <p>Getter for the employeeNumber field of the {@link AccountData}.</p>
     *
     * @return The employeeNumber field of the {@link AccountData}.
     */
    public String getEmployeeNumber () {
        return employeeNumber;
    }

    /**
     * <p>Setter for the employeeNumber field of the {@link AccountData}.</p>
     *
     * @param employeeNumber The employeeNumber field of the {@link AccountData}.
     */
    public void setEmployeeNumber (String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    /**
     * <p>Getter for the numSplits field of the {@link AccountData}.</p>
     *
     * @return The numSplits field of the {@link AccountData}.
     */
    public int getNumSplits () {
        return numSplits;
    }

    /**
     * <p>Setter for the numSplits field of the {@link AccountData}.</p>
     *
     * @param numSplits The numSplits field of the {@link AccountData}.
     */
    public void setNumSplits (int numSplits) {
        this.numSplits = numSplits;
    }

    /**
     * <p>Getter for the receiptBalanceTypeID field of the {@link AccountData}.</p>
     *
     * @return The receiptBalanceTypeID field of the {@link AccountData}.
     */
    public int getReceiptBalanceTypeID () {
        return receiptBalanceTypeID;
    }

    /**
     * <p>Setter for the receiptBalanceTypeID field of the {@link AccountData}.</p>
     *
     * @param receiptBalanceTypeID The receiptBalanceTypeID field of the {@link AccountData}.
     */
    public void setReceiptBalanceTypeID (int receiptBalanceTypeID) {
        this.receiptBalanceTypeID = receiptBalanceTypeID;
    }

    /**
     * <p>Getter for the receiptBalancePromptTxt field of the {@link AccountData}.</p>
     *
     * @return The receiptBalancePromptTxt field of the {@link AccountData}.
     */
    public String getReceiptBalancePromptTxt () {
        return receiptBalancePromptTxt;
    }

    /**
     * <p>Setter for the receiptBalancePromptTxt field of the {@link AccountData}.</p>
     *
     * @param receiptBalancePromptTxt The receiptBalancePromptTxt field of the {@link AccountData}.
     */
    public void setReceiptBalancePromptTxt (String receiptBalancePromptTxt) {
        this.receiptBalancePromptTxt = receiptBalancePromptTxt;
    }

    /**
     * <p>Getter for the globalBalance field of the {@link AccountData}.</p>
     *
     * @return The globalBalance field of the {@link AccountData}.
     */
    public double getGlobalBalance () {
        return globalBalance;
    }

    /**
     * <p>Setter for the globalBalance field of the {@link AccountData}.</p>
     *
     * @param globalBalance The globalBalance field of the {@link AccountData}.
     */
    public void setGlobalBalance (double globalBalance) {
        this.globalBalance = globalBalance;
    }

    /**
     * <p>Getter for the globalAvailable field of the {@link AccountData}.</p>
     *
     * @return The globalAvailable field of the {@link AccountData}.
     */
    public double getGlobalAvailable () {
        return globalAvailable;
    }

    /**
     * <p>Setter for the globalAvailable field of the {@link AccountData}.</p>
     *
     * @param globalAvailable The globalAvailable field of the {@link AccountData}.
     */
    public void setGlobalAvailable (double globalAvailable) {
        this.globalAvailable = globalAvailable;
    }

    /**
     * <p>Getter for the storeBalance field of the {@link AccountData}.</p>
     *
     * @return The storeBalance field of the {@link AccountData}.
     */
    public double getStoreBalance () {
        return storeBalance;
    }

    /**
     * <p>Setter for the storeBalance field of the {@link AccountData}.</p>
     *
     * @param storeBalance The storeBalance field of the {@link AccountData}.
     */
    public void setStoreBalance (double storeBalance) {
        this.storeBalance = storeBalance;
    }

    /**
     * <p>Getter for the storeAvailable field of the {@link AccountData}.</p>
     *
     * @return The storeAvailable field of the {@link AccountData}.
     */
    public double getStoreAvailable () {
        return storeAvailable;
    }

    /**
     * <p>Setter for the storeAvailable field of the {@link AccountData}.</p>
     *
     * @param storeAvailable The storeAvailable field of the {@link AccountData}.
     */
    public void setStoreAvailable (double storeAvailable) {
        this.storeAvailable = storeAvailable;
    }

    /**
     * <p>Getter for the dailyLimitBal field of the {@link AccountData}.</p>
     *
     * @return The dailyLimitBal field of the {@link AccountData}.
     */
    public double getDailyLimitBal () {
        return dailyLimitBal;
    }

    /**
     * <p>Setter for the dailyLimitBal field of the {@link AccountData}.</p>
     *
     * @param dailyLimitBal The dailyLimitBal field of the {@link AccountData}.
     */
    public void setDailyLimitBal (double dailyLimitBal) {
        this.dailyLimitBal = dailyLimitBal;
    }

}