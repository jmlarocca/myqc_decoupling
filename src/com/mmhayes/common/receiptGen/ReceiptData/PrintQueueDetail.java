package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-04 17:08:30 -0400 (Fri, 04 Sep 2020) $: Date of last commit
    $Rev: 12575 $: Revision of last commit
    Notes: Helper class to build a print queue detail.
*/

import java.util.Objects;

/**
 * Helper class to build a print queue detail.
 *
 */
public class PrintQueueDetail {

    // private member variables of a PrintQueueDetail
    private final int paTransLineItemID;
    private final int paTransLineItemModID;
    private final int printerID;
    private final int printStatusID;
    private final int printerHostID;
    private final int papluID;
    private final double quantity;
    private final boolean isModifier;
    private final String hideStation;
    private final String line;
    private final boolean fixWordWrap;

    /**
     * Constructor for a PrintQueueDetail.
     *
     * @param builder {@link PrintQueueDetailBuilder} Builder class for a PrintQueueDetail.
     */
    public PrintQueueDetail (PrintQueueDetailBuilder builder) {
        this.paTransLineItemID = builder.paTransLineItemID;
        this.paTransLineItemModID = builder.paTransLineItemModID;
        this.printerID = builder.printerID;
        this.printStatusID = builder.printStatusID;
        this.printerHostID = builder.printerHostID;
        this.papluID = builder.papluID;
        this.quantity = builder.quantity;
        this.isModifier = builder.isModifier;
        this.hideStation = builder.hideStation;
        this.line = builder.line;
        this.fixWordWrap = builder.fixWordWrap;
    }

    /**
     * <p>Getter for the paTransLineItemID field of the {@link PrintQueueDetail}.</p>
     *
     * @return The paTransLineItemID field of the {@link PrintQueueDetail}.
     */
    public int getPaTransLineItemID () {
        return paTransLineItemID;
    }

    /**
     * <p>Getter for the paTransLineItemModID field of the {@link PrintQueueDetail}.</p>
     *
     * @return The paTransLineItemModID field of the {@link PrintQueueDetail}.
     */
    public int getPaTransLineItemModID () {
        return paTransLineItemModID;
    }

    /**
     * Getter for the PrintQueueDetail's printerID field.
     *
     * @return The printerID field.
     */
    public int getPrinterID () {
        return printerID;
    }

    /**
     * Getter for the PrintQueueDetail's printStatusID field.
     *
     * @return The printStatusID field.
     */
    public int getPrintStatusID () {
        return printStatusID;
    }

    /**
     * Getter for the PrintQueueDetail's printerHostID field.
     *
     * @return The printerHostID field.
     */
    public int getPrinterHostID () {
        return printerHostID;
    }

    /**
     * Getter for the PrintQueueDetail's papluID field.
     *
     * @return The papluID field.
     */
    public int getPapluID () {
        return papluID;
    }

    /**
     * Getter for the PrintQueueDetail's quantity field.
     *
     * @return The quantity field.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * Getter for the PrintQueueDetail's isModifier field.
     *
     * @return The isModifier field.
     */
    public boolean getIsModifier () {
        return isModifier;
    }

    /**
     * Getter for the PrintQueueDetail's hideStation field.
     *
     * @return The hideStation field.
     */
    public String getHideStation () {
        return hideStation;
    }

    /**
     * Getter for the PrintQueueDetail's line field.
     *
     * @return The line field.
     */
    public String getLine () {
        return line;
    }

    /**
     * Getter for the PrintQueueDetail's fixWordWrap field.
     *
     * @return The fixWordWrap field.
     */
    public boolean getFixWordWrap () {
        return fixWordWrap;
    }

    /**
     * <p>Overridden toString method for a {@link PrintQueueDetail}.</p>
     *
     * @return The {@link PrintQueueDetail} as a {@link String}.
     */
    @Override
    public String toString () {

        return String.format("PATRANSLINEITEMID: %s, PATRANSLINEITEMMODID: %s, PRINTERID: %s, PRINTSTATUSID: %s, PRINTERHOSTID: %s, PAPLUID: %s, QUANTITY: %s, ISMODIFIER: %s, " +
                "HIDESTATION: %s, LINE: %s, FIXWORDWRAP: %s",
                Objects.toString(paTransLineItemID, "NULL"),
                Objects.toString(paTransLineItemModID, "NULL"),
                Objects.toString(printerID, "NULL"),
                Objects.toString(printStatusID, "NULL"),
                Objects.toString(printerHostID, "NULL"),
                Objects.toString(papluID, "NULL"),
                Objects.toString(quantity, "NULL"),
                Objects.toString(isModifier, "NULL"),
                Objects.toString(hideStation, "NULL"),
                Objects.toString(line, "NULL"),
                Objects.toString(fixWordWrap, "NULL"));

    }

    /**
     * Builder class for a PrintQueueDetail.
     *
     */
    public static class PrintQueueDetailBuilder {

        // private member variables of a PrintQueueDetailBuilder
        private int paTransLineItemID;
        private int paTransLineItemModID;
        private int printerID;
        private int printStatusID;
        private int printerHostID;
        private int papluID;
        private double quantity;
        private boolean isModifier;
        private String hideStation;
        private String line;
        private boolean fixWordWrap;

        /**
         * Constructor for a PrintQueueDetailBuilder.
         *
         */
        public PrintQueueDetailBuilder () {}

        /**
         * Adds an optional paTransLineItemID field to the PrintQueueDetailBuilder.
         *
         * @param paTransLineItemID ID of trans line item.
         * @return {@link PrintQueueDetailBuilder} The PrintQueueDetailBuilder with the paTransLineItemID field included.
         */
        public PrintQueueDetailBuilder paTransLineItemID (int paTransLineItemID) {
            this.paTransLineItemID = paTransLineItemID;
            return this;
        }

        /**
         * Adds an optional paTransLineItemModID field to the PrintQueueDetailBuilder.
         *
         * @param paTransLineItemModID ID of the modifier trans line item.
         * @return {@link PrintQueueDetailBuilder} The PrintQueueDetailBuilder with the paTransLineItemModID field included.
         */
        public PrintQueueDetailBuilder paTransLineItemModID (int paTransLineItemModID) {
            this.paTransLineItemModID = paTransLineItemModID;
            return this;
        }

        /**
         * Adds an optional printerID field to the PrintQueueDetailBuilder.
         *
         * @param printerID ID of the printer this print job should be executed on.
         * @return {@link PrintQueueDetailBuilder} The PrintQueueDetailBuilder with the printerID field included.
         */
        public PrintQueueDetailBuilder printerID (int printerID) {
            this.printerID = printerID;
            return this;
        }

        /**
         * Adds an optional printStatusID field to the PrintQueueDetailBuilder.
         *
         * @param printStatusID Status of this print job.
         * @return {@link PrintQueueDetailBuilder} The PrintQueueDetailBuilder with the printStatusID field included.
         */
        public PrintQueueDetailBuilder printStatusID (int printStatusID) {
            this.printStatusID = printStatusID;
            return this;
        }

        /**
         * Adds an optional printerHostID field to the PrintQueueDetailBuilder.
         *
         * @param printerHostID ID of the printer host responsible for submitting this print job.
         * @return {@link PrintQueueDetailBuilder} The PrintQueueDetailBuilder with the printerHostID field included.
         */
        public PrintQueueDetailBuilder printerHostID (int printerHostID) {
            this.printerHostID = printerHostID;
            return this;
        }

        /**
         * Adds an optional papluID field to the PrintQueueDetailBuilder.
         *
         * @param papluID ID of the product that is being printed in this print job.
         * @return {@link PrintQueueDetailBuilder} The PrintQueueDetailBuilder with the papluID field included.
         */
        public PrintQueueDetailBuilder papluID (int papluID) {
            this.papluID = papluID;
            return this;
        }

        /**
         * Adds an optional quantity field to the PrintQueueDetailBuilder.
         *
         * @param quantity Amount of the product that is in this print job.
         * @return {@link PrintQueueDetailBuilder} The PrintQueueDetailBuilder with the quantity field included.
         */
        public PrintQueueDetailBuilder quantity (double quantity) {
            this.quantity = quantity;
            return this;
        }

        /**
         * Adds an optional isModifier field to the PrintQueueDetailBuilder.
         *
         * @param isModifier Whether or not the product in this print job is a modifier.
         * @return {@link PrintQueueDetailBuilder} The PrintQueueDetailBuilder with the isModifier field included.
         */
        public PrintQueueDetailBuilder isModifier (boolean isModifier) {
            this.isModifier = isModifier;
            return this;
        }

        /**
         * Adds an optional hideStation field to the PrintQueueDetailBuilder.
         *
         * @param hideStation {@link String} ID of the KDS station on which the product in this print job should be hidden.
         * @return {@link PrintQueueDetailBuilder} The PrintQueueDetailBuilder with the hideStation field included.
         */
        public PrintQueueDetailBuilder hideStation (String hideStation) {
            this.hideStation = hideStation;
            return this;
        }

        /**
         * Adds an optional line field to the PrintQueueDetailBuilder.
         *
         * @param line {@link String} The line to print on the receipt for the KPJobDetail.
         * @return {@link PrintQueueDetailBuilder} The PrintQueueDetailBuilder with the line field included.
         */
        public PrintQueueDetailBuilder line (String line) {
            this.line = line;
            return this;
        }

        /**
         * Adds an optional fixWordWrap field to the PrintQueueDetailBuilder.
         *
         * @param fixWordWrap Whether or not to format lines on the receipt to prevent word wrap.
         * @return {@link PrintQueueDetailBuilder} The PrintQueueDetailBuilder with the fixWordWrap field included.
         */
        public PrintQueueDetailBuilder fixWordWrap (boolean fixWordWrap) {
            this.fixWordWrap = fixWordWrap;
            return this;
        }

        /**
         * Builds and returns the PrintQueueDetail.
         *
         * @return {@link PrintQueueDetail} The newly constructed PrintQueueDetail.
         */
        public PrintQueueDetail build () {
            return (new PrintQueueDetail(this));
        }

    }

}
