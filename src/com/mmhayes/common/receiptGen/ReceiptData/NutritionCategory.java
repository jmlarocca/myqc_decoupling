package com.mmhayes.common.receiptGen.ReceiptData;

/**
 * Created by nyu on 1/9/2017.
 */
public class NutritionCategory
{
    private final static String COMMON_LOG_FILE = "ConsolidatedReceipt.log";

    private String name = "";
    private String shortName = "";
    private String measurementLbl = "";

    public NutritionCategory(String name, String shortName)
    {
        this.name = name;
        this.shortName = shortName;
    }

    public NutritionCategory(String name, String shortName, String measurementLbl)
    {
        this.name = name;
        this.shortName = shortName;
        this.measurementLbl = measurementLbl;
    }

    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public String getName()
    {
        return name;
    }

    public String getShortName()
    {
        return shortName;
    }

    public String getMeasurementLbl()
    {
        return measurementLbl;
    }
}
