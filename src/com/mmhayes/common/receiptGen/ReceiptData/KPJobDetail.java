package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-07-14 15:46:08 -0400 (Tue, 14 Jul 2020) $: Date of last commit
    $Rev: 12154 $: Revision of last commit
    Notes: A print job detail to be printed on a receipt.
*/

import com.mmhayes.common.printing.PrintJobStatusType;
import org.apache.commons.codec.binary.Base64;

import java.util.Objects;

/**
 * Class to store details about a particular print job.
 *
 */
public class KPJobDetail {

    // fields within the KPJobDetail scope
    private int paTransLineItemID = 0;
    private int paTransLineItemModID = 0;
    private int printerID = 0;
    private String printerName = "";
    private boolean printerPrintsOnlyNew = false;
    private int printStatusID = PrintJobStatusType.WAITING.getTypeID();
    private int printerHostID = 0;
    private int printerTypeID = 0;
    private int papluID = 0;
    private double quantity = 0.0;
    private boolean isModifier = false;
    private boolean isExpeditor = false;
    private boolean isRemote = false;
    private boolean noExpeditorTktsUnlessProdAdded = false;
    private String productName = "";
    private String lineDetail = "";
    private int printerHardwareTypeID = 0;
    private String hideStation = "";

    /**
     * <p>Empty constructor for a {@link KPJobDetail}.</p>
     *
     * @return A {@link KPJobDetail} instance.
     */
    public KPJobDetail () {}

    /**
     * <p>Constructor for a {@link KPJobDetail} with it's printerID field set.</p>
     *
     * @param printerID ID of the printer this detail will be sent to.
     * @return The {@link KPJobDetail} with it's printerID field set.
     */
    public KPJobDetail printerID (int printerID) {
        this.printerID = printerID;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPJobDetail} with it's printStatusID field set.</p>
     *
     * @param printStatusID Print status ID of this detail.
     * @return The {@link KPJobDetail} with it's printStatusID field set.
     */
    public KPJobDetail printStatusID (int printStatusID) {
        this.printStatusID = printStatusID;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPJobDetail} with it's printerHostID field set.</p>
     *
     * @param printerHostID ID of the printer host that will execute this detail.
     * @return The {@link KPJobDetail} with it's printerHostID field set.
     */
    public KPJobDetail printerHostID (int printerHostID) {
        this.printerHostID = printerHostID;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPJobDetail} with it's papluID field set.</p>
     *
     * @param papluID ID of the product srepresented by this detail.
     * @return The {@link KPJobDetail} with it's papluID field set.
     */
    public KPJobDetail papluID (int papluID) {
        this.papluID = papluID;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPJobDetail} with it's quantity field set.</p>
     *
     * @param quantity Amount of product represented by this detail.
     * @return The {@link KPJobDetail} with it's quantity field set.
     */
    public KPJobDetail quantity (double quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPJobDetail} with it's isModifier field set.</p>
     *
     * @param isModifier Whether or not this detail is for a modifier.
     * @return The {@link KPJobDetail} with it's isModifier field set.
     */
    public KPJobDetail isModifier (boolean isModifier) {
        this.isModifier = isModifier;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPJobDetail} with it's lineDetail field set.</p>
     *
     * @param lineDetail The line detail {@link String}.
     * @return The {@link KPJobDetail} with it's lineDetail field set.
     */
    public KPJobDetail lineDetail (String lineDetail) {
        this.lineDetail = lineDetail;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPJobDetail} with it's hideStation field set.</p>
     * 
     * @param hiddenStation The hidden station {@link String}.
     * @return The {@link KPJobDetail} with it's hideStation field set.
     */
    public KPJobDetail hiddenStation (String hiddenStation) {
        this.hideStation = hiddenStation;
        return this;
    }

    /**
     * Overidden toString method for a KPJobDetail.
     *
     * @return {@link String} The resultant String.
     */
    @Override
    public String toString () {
        return String.format("PATRANSLINEITEMID: %s, PATRANSLINEITEMMODID: %s, PRINTERID: %s, PRINTERNAME: %s, " +
                "PRINTERPRINTSONLYNEW: %s, PRINTSTATUSID: %s, PRINTERHOSTID: %s, PRINTERTYPEID: %s, PAPLUID: %s, " +
                "QUANTITY: %s, ISMODIFIER: %s, ISEXPEDITOR: %s, ISREMOTE: %s, NOEXPEDITORTKTSUNLESSPRODADDED: %s, " +
                "PRODUCTNAME: %s, LINEDETAIL: %s, PRINTERHARDWARETYPEID: %s, HIDESTATION: %s",
                Objects.toString(paTransLineItemID, "NULL"),
                Objects.toString(paTransLineItemModID, "NULL"),
                Objects.toString(printerID, "NULL"),
                Objects.toString(printerName, "NULL"),
                Objects.toString((printerPrintsOnlyNew ? "true" : "false"), "NULL"),
                Objects.toString(printStatusID, "NULL"),
                Objects.toString(printerHostID, "NULL"),
                Objects.toString(printerTypeID, "NULL"),
                Objects.toString(papluID, "NULL"),
                Objects.toString(quantity, "NULL"),
                Objects.toString((isModifier ? "true" : "false"), "NULL"),
                Objects.toString((isExpeditor ? "true" : "false"), "NULL"),
                Objects.toString((isRemote ? "true" : "false"), "NULL"),
                Objects.toString((noExpeditorTktsUnlessProdAdded ? "true" : "false"), "NULL"),
                Objects.toString(productName, "NULL"),
                Objects.toString(lineDetail, "NULL"),
                Objects.toString(printerHardwareTypeID, "NULL"),
                Objects.toString(hideStation, "NULL"));
    }

    /**
     * <p>Compact version of a {@link KPJobDetail}.</p>
     *
     * @return The {@link KPJobDetail} as a {@link String}.
     */
    public String toStringCompact () {
        return String.format("PRINTERID: %s, PRINTSTATUSID: %s, PRINTERHOSTID: %s, PAPLUID: %s, QUANTITY: %s, ISMODIFIER: %s, LINEDETAIL: %s, HIDESTATION: %s",
                Objects.toString(printerID, "NULL"),
                Objects.toString(printStatusID, "NULL"),
                Objects.toString(printerHostID, "NULL"),
                Objects.toString(papluID, "NULL"),
                Objects.toString(quantity, "NULL"),
                Objects.toString((isModifier ? "true" : "false"), "NULL"),
                Objects.toString(new String(Base64.decodeBase64(lineDetail)), "NULL"),
                Objects.toString(hideStation, "NULL"));
    }

    /**
     * Setter for the KPJobDetail's paTransLineItemID field.
     *
     * @param paTransLineItemID The transaction's line item ID.
     */
    public void setPaTransLineItemID (int paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
    }

    /**
     * Getter for the KPJobDetail's paTransLineItemID field.
     *
     * @return The transaction's line item ID.
     */
    public int getPaTransLineItemID () {
        return paTransLineItemID;
    }

    /**
     * Setter for the KPJobDetail's paTransLineItemModID field.
     *
     * @param paTransLineItemModID The transaction's modifier line item ID.
     */
    public void setPaTransLineItemModID (int paTransLineItemModID) {
        this.paTransLineItemModID = paTransLineItemModID;
    }

    /**
     * Getter for the KPJobDetail's paTransLineItemModID field.
     *
     * @return The transaction's modifier line item ID.
     */
    public int getPaTransLineItemModID () {
        return paTransLineItemModID;
    }

    /**
     * Setter for the KPJobDetail's printerID field.
     *
     * @param printerID The ID of the printer.
     */
    public void setPrinterID (int printerID) {
        this.printerID = printerID;
    }

    /**
     * Getter for the KPJobDetail's printerID field.
     *
     * @return The ID of the printer.
     */
    public int getPrinterID () {
        return printerID;
    }

    /**
     * Setter for the KPJobDetail's printerName field.
     *
     * @param printerName {@link String} The name of the printer.
     */
    public void setPrinterName (String printerName) {
        this.printerName = printerName;
    }

    /**
     * Getter for the KPJobDetail's printerName field.
     *
     * @return {@link String} The name of the printer.
     */
    public String getPrinterName () {
        return printerName;
    }

    /**
     * Setter for the KPJobDetail's printerPrintsOnlyNew field.
     *
     * @param printerPrintsOnlyNew Whether or not the printer only prints new products.
     */
    public void setPrinterPrintsOnlyNew (boolean printerPrintsOnlyNew) {
        this.printerPrintsOnlyNew = printerPrintsOnlyNew;
    }

    /**
     * Getter for the KPJobDetail's printerPrintsOnlyNew field.
     *
     * @return Whether or not the printer only prints new products.
     */
    public boolean isPrinterPrintsOnlyNew () {
        return printerPrintsOnlyNew;
    }

    /**
     * Setter for the KPJobDetail's printStatusID field.
     *
     * @param printStatusID The status of the print job.
     */
    public void setPrintStatusID (int printStatusID) {
        this.printStatusID = printStatusID;
    }

    /**
     * Getter for the KPJobDetail's printStatusID field.
     *
     * @return The status of the print job.
     */
    public int getPrintStatusID () {
        return printStatusID;
    }

    /**
     * Setter for the KPJobDetail's printerHostID field.
     *
     * @param printerHostID The ID of the printer host.
     */
    public void setPrinterHostID (int printerHostID) {
        this.printerHostID = printerHostID;
    }

    /**
     * Getter for the KPJobDetail's printerHostID field.
     *
     * @return The ID of the printer host.
     */
    public int getPrinterHostID () {
        return printerHostID;
    }

    /**
     * Setter for the KPJobDetail's printerTypeID field.
     *
     * @param printerTypeID The ID of the printer host.
     */
    public void setPrinterTypeID (int printerTypeID) {
        this.printerTypeID = printerTypeID;
    }

    /**
     * Getter for the KPJobDetail's printerTypeID field.
     *
     * @return The ID of the printer host.
     */
    public int getPrinterTypeID () {
        return printerTypeID;
    }

    /**
     * Setter for the KPJobDetail's papluID field.
     *
     * @param papluID The ID of the product.
     */
    public void setPAPluID (int papluID) {
        this.papluID = papluID;
    }

    /**
     * Getter for the KPJobDetail's papluID field.
     *
     * @return The ID of the product.
     */
    public int getPAPluID () {
        return papluID;
    }

    /**
     * Setter for the KPJobDetail's quantity field.
     *
     * @param quantity Amount of the product.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * Getter for the KPJobDetail's quantity field.
     *
     * @return Amount of the product.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * Setter for the KPJobDetail's isModifier field.
     *
     * @param isModifier Whether or not the product is a modifier.
     */
    public void setIsModifier (boolean isModifier) {
        this.isModifier = isModifier;
    }

    /**
     * Getter for the KPJobDetail's isModifier field.
     *
     * @return Whether or not the product is a modifier.
     */
    public boolean getIsModifier () {
        return isModifier;
    }

    /**
     * Setter for the KPJobDetail's isExpeditor field.
     *
     * @param isExpeditor Whether or not the printer is an expeditor.
     */
    public void setIsExpeditor (boolean isExpeditor) {
        this.isExpeditor = isExpeditor;
    }

    /**
     * Getter for the KPJobDetail's isExpeditor field.
     *
     * @return Whether or not the printer is an expeditor.
     */
    public boolean getIsExpeditor () {
        return isExpeditor;
    }

    /**
     * Setter for the KPJobDetail's isRemote field.
     *
     * @param isRemote Whether or not the printer is a remote order printer.
     */
    public void setIsRemote (boolean isRemote) {
        this.isRemote = isRemote;
    }

    /**
     * Getter for the KPJobDetail's isExpeditor field.
     *
     * @return Whether or not the printer is a remote order printer.
     */
    public boolean getIsRemote () {
        return isRemote;
    }

    /**
     * Setter for the KPJobDetail's noExpeditorTktsUnlessProdAdded field.
     *
     * @param noExpeditorTktsUnlessProdAdded Whether or not the expeditor printer will only print products when
     *         new expeditor products are added.
     */
    public void setNoExpeditorTktsUnlessProdAdded (boolean noExpeditorTktsUnlessProdAdded) {
        this.noExpeditorTktsUnlessProdAdded = noExpeditorTktsUnlessProdAdded;
    }

    /**
     * Getter for the KPJobDetail's noExpeditorTktsUnlessProdAdded field.
     *
     * @return Whether or not the expeditor printer will only print products when new expeditor products are added.
     */
    public boolean getNoExpeditorTktsUnlessProdAdded () {
        return noExpeditorTktsUnlessProdAdded;
    }

    /**
     * Setter for the KPJobDetail's productName field.
     *
     * @param productName {@link String} The name of the product.
     */
    public void setProductName (String productName) {
        this.productName = productName;
    }

    /**
     * Getter for the KPJobDetail's productName field.
     *
     * @return {@link String} The name of the product.
     */
    public String getProductName () {
        return productName;
    }

    /**
     * Setter for the KPJobDetail's lineDetail field.
     *
     * @param lineDetail {@link String} Base 64 encoded line to print.
     */
    public void setLineDetail (String lineDetail) {
        this.lineDetail = lineDetail;
    }

    /**
     * Getter for the KPJobDetail's lineDetail field.
     *
     * @return {@link String} Base 64 encoded line to print.
     */
    public String getLineDetail () {
        return lineDetail;
    }

    /**
     * Setter for the KPJobDetail's printerHardwareTypeID field.
     *
     * @param printerHardwareTypeID The type of hardware the printer is.
     */
    public void setPrinterHardwareTypeID (int printerHardwareTypeID) {
        this.printerHardwareTypeID = printerHardwareTypeID;
    }

    /**
     * Getter for the KPJobDetail's printerHardwareTypeID field.
     *
     * @return The type of hardware the printer is.
     */
    public int getPrinterHardwareTypeID () {
        return printerHardwareTypeID;
    }

    /**
     * Setter for the KPJobDetail's hideStation field.
     *
     * @param hideStation {@link String} Station ID of the KDS station to hide the product on.
     */
    public void setHideStation (String hideStation) {
        this.hideStation = hideStation;
    }

    /**
     * Getter for the KPJobDetail's hideStation field.
     *
     * @return {@link String} Station ID of the KDS station to hide the product on.
     */
    public String getHideStation () {
        return hideStation;
    }

    /**
     * Overridden equals method.
     *
     * @param o {@link Object} The object we are comparing against this instance.
     * @return Whether or not the objects are equal.
     */
    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }

        if ((o == null) || (getClass() != o.getClass())) {
            return false;
        }

        // cast o to a KPJobDetail
        KPJobDetail kpJobDetail = (KPJobDetail) o;
        return ((Objects.equals(kpJobDetail.papluID, papluID)) && (Objects.equals(kpJobDetail.paTransLineItemID, paTransLineItemID)));
    }

    /**
     * Overridden hashCode method.
     *
     * @return The new hash code.
     */
    @Override
    public int hashCode () {
        return Objects.hash(papluID, paTransLineItemID);
    }

}
