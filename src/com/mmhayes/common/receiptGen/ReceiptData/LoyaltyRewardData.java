package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-18 15:39:12 -0400 (Mon, 18 May 2020) $: Date of last commit
    $Rev: 11775 $: Revision of last commit
    Notes: Represents a loyalty reward.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a loyalty reward.</p>
 *
 */
public class LoyaltyRewardData {

    // private member variables of a LoyaltyRewardData
    private int loyaltyRewardID = 0;
    private int ownershipGroupID = 0;
    private int loyaltyRewardTypeID = 0;
    private int loyaltyRewardCalcMethodID = 0;
    private String name = "";
    private int pointsThresholdMin = 0;
    private int pointsThresholdMax = 0;
    private double rewardValue = 0.0d;
    private boolean redeemAtPOS = false;
    private boolean autoPayout = false;
    private boolean autoChooseTerminal = false;
    private boolean applyToAllPLUs = false;
    private String taxIDs = "";
    private String shortName = "";
    private double minPurchase = 0.0d;
    private double maxDiscount = 0.0d;
    private int terminalID = 0;
    private boolean active = false;
    private boolean canUseWithOtherRewards = false;
    private int maxPerTransaction = 0;
    private int loyaltyProgramId = 0;
    private boolean allowProductsOverMaximum = false;

    /**
     * <p>Constructs a new {@link LoyaltyRewardData} from the given {@link HashMap}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param hm A {@link HashMap} with data corresponding to the loyalty reward to build a {@link LoyaltyRewardData} for.
     * @return The {@link LoyaltyRewardData} built from the given {@link HashMap}.
     */
    public static LoyaltyRewardData buildFromHM (String log, HashMap hm) {
        LoyaltyRewardData loyaltyRewardData = null;

        try {
            if (!DataFunctions.isEmptyMap(hm)) {
                loyaltyRewardData = new LoyaltyRewardData();
                loyaltyRewardData.loyaltyRewardID = HashMapDataFns.getIntVal(hm, "LOYALTYREWARDID");
                loyaltyRewardData.ownershipGroupID = HashMapDataFns.getIntVal(hm, "OWNERSHIPGROUPID");
                loyaltyRewardData.loyaltyRewardTypeID = HashMapDataFns.getIntVal(hm, "LOYALTYREWARDTYPEID");
                loyaltyRewardData.loyaltyRewardCalcMethodID = HashMapDataFns.getIntVal(hm, "LOYALTYREWARDCALCMETHODID");
                loyaltyRewardData.name = HashMapDataFns.getStringVal(hm, "NAME");
                loyaltyRewardData.pointsThresholdMin = HashMapDataFns.getIntVal(hm, "POINTSTHRESHOLDMIN");
                loyaltyRewardData.pointsThresholdMax = HashMapDataFns.getIntVal(hm, "POINTSTHRESHOLDMAX");
                loyaltyRewardData.rewardValue = HashMapDataFns.getDoubleVal(hm, "REWARDVALUE");
                loyaltyRewardData.redeemAtPOS = HashMapDataFns.getBooleanVal(hm, "REDEEMATPOS");
                loyaltyRewardData.autoPayout = HashMapDataFns.getBooleanVal(hm, "AUTOPAYOUT");
                loyaltyRewardData.autoChooseTerminal = HashMapDataFns.getBooleanVal(hm, "AUTOCHOOSETERMINAL");
                loyaltyRewardData.applyToAllPLUs = HashMapDataFns.getBooleanVal(hm, "APPLYTOALLPLUS");
                loyaltyRewardData.taxIDs = HashMapDataFns.getStringVal(hm, "TAXIDS");
                loyaltyRewardData.shortName = HashMapDataFns.getStringVal(hm, "SHORTNAME");
                loyaltyRewardData.minPurchase = HashMapDataFns.getDoubleVal(hm, "MINPURCHASE");
                loyaltyRewardData.maxDiscount = HashMapDataFns.getDoubleVal(hm, "MAXDISCOUNT");
                loyaltyRewardData.terminalID = HashMapDataFns.getIntVal(hm, "TERMINALID");
                loyaltyRewardData.active = HashMapDataFns.getBooleanVal(hm, "ACTIVE");
                loyaltyRewardData.canUseWithOtherRewards = HashMapDataFns.getBooleanVal(hm, "CANUSEWITHOTHERREWARDS");
                loyaltyRewardData.maxPerTransaction = HashMapDataFns.getIntVal(hm, "MAXPERTRANSACTION");
                loyaltyRewardData.loyaltyProgramId = HashMapDataFns.getIntVal(hm, "LOYALTYPROGRAMID");
                loyaltyRewardData.allowProductsOverMaximum = HashMapDataFns.getBooleanVal(hm, "ALLOWPRODUCTSOVERMAXIMUM");
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build a LoyaltyRewardData object with the given HashMap in LoyaltyRewardData.buildFromHM", log, Logger.LEVEL.ERROR);
        }

        return loyaltyRewardData;
    }

    /**
     * <p>Queries the database to get the {@link LoyaltyRewardData} object with the given loyalty reward ID.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param loyaltyRewardID ID of the {@link LoyaltyRewardData} to get.
     * @return The {@link LoyaltyRewardData} object with the given loyalty reward ID.
     */
    @SuppressWarnings("unchecked")
    public static LoyaltyRewardData buildFromID (String log, DataManager dataManager, int loyaltyRewardID) {
        LoyaltyRewardData loyaltyRewardData = null;

        try {
            if (loyaltyRewardID > 0) {
                ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(dataManager.parameterizedExecuteQuery("data.ReceiptGen.GetLoyaltyRewardByID", new Object[]{loyaltyRewardID}, true));
                if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1)) {
                    LoyaltyRewardData loyaltyRewardDataFromQuery = buildFromHM(log, queryRes.get(0));
                    if (loyaltyRewardDataFromQuery != null) {
                        loyaltyRewardData = loyaltyRewardDataFromQuery;
                    }
                }
            }
            else {
                Logger.logMessage("Loyalty program ID passed to LoyaltyRewardData.buildFromID must be greater than 0!", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build a LoyaltyRewardData object with the given loyalty reward ID of %s in LoyaltyRewardData.buildFromID",
                    Objects.toString(loyaltyRewardID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return loyaltyRewardData;
    }

    /**
     * <p>Overridden toString() method for a {@link LoyaltyRewardData}.</p>
     *
     * @return A {@link String} representation of this {@link LoyaltyRewardData}.
     */
    @Override
    public String toString () {

        return String.format("LOYALTYREWARDID: %s, OWNERSHIPGROUPID: %s, LOYALTYREWARDTYPEID: %s, LOYALTYREWARDCALCMETHODID: %s, NAME: %s, " +
                "POINTSTHRESHOLDMIN: %s, POINTSTHRESHOLDMAX: %s, REWARDVALUE: %s, REDEEMATPOS: %s, AUTOPAYOUT: %s, AUTOCHOOSETERMINAL: %s, " +
                "APPLYTOALLPLUS: %s, TAXIDS: %s, SHORTNAME: %s, MINPURCHASE: %s, MAXDISCOUNT: %s, TERMINALID: %s, ACTIVE: %s, CANUSEWITHOTHERREWARDS: %s, " +
                "MAXPERTRANSACTION: %s, LOYALTYPROGRAMID: %s, ALLOWPRODUCTSOVERMAXIMUM: %s",
                Objects.toString((loyaltyRewardID != -1 ? loyaltyRewardID : "N/A"), "N/A"),
                Objects.toString((ownershipGroupID != -1 ? ownershipGroupID : "N/A"), "N/A"),
                Objects.toString((loyaltyRewardTypeID != -1 ? loyaltyRewardTypeID : "N/A"), "N/A"),
                Objects.toString((loyaltyRewardCalcMethodID != -1 ? loyaltyRewardCalcMethodID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((pointsThresholdMin != -1 ? pointsThresholdMin : "N/A"), "N/A"),
                Objects.toString((pointsThresholdMax != -1 ? pointsThresholdMax : "N/A"), "N/A"),
                Objects.toString((rewardValue != -1.0d ? rewardValue : "N/A"), "N/A"),
                Objects.toString(redeemAtPOS, "N/A"),
                Objects.toString(autoPayout, "N/A"),
                Objects.toString(autoChooseTerminal, "N/A"),
                Objects.toString(applyToAllPLUs, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(taxIDs) ? taxIDs : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((minPurchase != -1.0d ? minPurchase : "N/A"), "N/A"),
                Objects.toString((maxDiscount != -1.0d ? maxDiscount : "N/A"), "N/A"),
                Objects.toString((terminalID != -1 ? terminalID : "N/A"), "N/A"),
                Objects.toString(active, "N/A"),
                Objects.toString(canUseWithOtherRewards, "N/A"),
                Objects.toString((maxPerTransaction != -1 ? maxPerTransaction : "N/A"), "N/A"),
                Objects.toString((loyaltyProgramId != -1 ? loyaltyProgramId : "N/A"), "N/A"),
                Objects.toString(allowProductsOverMaximum, "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link LoyaltyRewardData}.
     * Two {@link LoyaltyRewardData} are defined as being equal if they have the same loyaltyRewardID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link LoyaltyRewardData}.
     * @return Whether or not the {@link Object} is equal to this {@link LoyaltyRewardData}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a LoyaltyRewardData and check if the obj's loyaltyRewardID is equal to this LoyaltyRewardData's loyaltyRewardID
        LoyaltyRewardData loyaltyReward = ((LoyaltyRewardData) obj);
        return Objects.equals(loyaltyReward.loyaltyRewardID, loyaltyRewardID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link LoyaltyRewardData}.</p>
     *
     * @return The unique hash code for this {@link LoyaltyRewardData}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(loyaltyRewardID);
    }

    /**
     * <p>Getter for the loyaltyRewardID field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The loyaltyRewardID field of the {@link LoyaltyRewardData}.
     */
    public int getLoyaltyRewardID () {
        return loyaltyRewardID;
    }

    /**
     * <p>Setter for the loyaltyRewardID field of the {@link LoyaltyRewardData}.</p>
     *
     * @param loyaltyRewardID The loyaltyRewardID field of the {@link LoyaltyRewardData}.
     */
    public void setLoyaltyRewardID (int loyaltyRewardID) {
        this.loyaltyRewardID = loyaltyRewardID;
    }

    /**
     * <p>Getter for the ownershipGroupID field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The ownershipGroupID field of the {@link LoyaltyRewardData}.
     */
    public int getOwnershipGroupID () {
        return ownershipGroupID;
    }

    /**
     * <p>Setter for the ownershipGroupID field of the {@link LoyaltyRewardData}.</p>
     *
     * @param ownershipGroupID The ownershipGroupID field of the {@link LoyaltyRewardData}.
     */
    public void setOwnershipGroupID (int ownershipGroupID) {
        this.ownershipGroupID = ownershipGroupID;
    }

    /**
     * <p>Getter for the loyaltyRewardTypeID field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The loyaltyRewardTypeID field of the {@link LoyaltyRewardData}.
     */
    public int getLoyaltyRewardTypeID () {
        return loyaltyRewardTypeID;
    }

    /**
     * <p>Setter for the loyaltyRewardTypeID field of the {@link LoyaltyRewardData}.</p>
     *
     * @param loyaltyRewardTypeID The loyaltyRewardTypeID field of the {@link LoyaltyRewardData}.
     */
    public void setLoyaltyRewardTypeID (int loyaltyRewardTypeID) {
        this.loyaltyRewardTypeID = loyaltyRewardTypeID;
    }

    /**
     * <p>Getter for the loyaltyRewardCalcMethodID field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The loyaltyRewardCalcMethodID field of the {@link LoyaltyRewardData}.
     */
    public int getLoyaltyRewardCalcMethodID () {
        return loyaltyRewardCalcMethodID;
    }

    /**
     * <p>Setter for the loyaltyRewardCalcMethodID field of the {@link LoyaltyRewardData}.</p>
     *
     * @param loyaltyRewardCalcMethodID The loyaltyRewardCalcMethodID field of the {@link LoyaltyRewardData}.
     */
    public void setLoyaltyRewardCalcMethodID (int loyaltyRewardCalcMethodID) {
        this.loyaltyRewardCalcMethodID = loyaltyRewardCalcMethodID;
    }

    /**
     * <p>Getter for the name field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The name field of the {@link LoyaltyRewardData}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link LoyaltyRewardData}.</p>
     *
     * @param name The name field of the {@link LoyaltyRewardData}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the pointsThresholdMin field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The pointsThresholdMin field of the {@link LoyaltyRewardData}.
     */
    public int getPointsThresholdMin () {
        return pointsThresholdMin;
    }

    /**
     * <p>Setter for the pointsThresholdMin field of the {@link LoyaltyRewardData}.</p>
     *
     * @param pointsThresholdMin The pointsThresholdMin field of the {@link LoyaltyRewardData}.
     */
    public void setPointsThresholdMin (int pointsThresholdMin) {
        this.pointsThresholdMin = pointsThresholdMin;
    }

    /**
     * <p>Getter for the pointsThresholdMax field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The pointsThresholdMax field of the {@link LoyaltyRewardData}.
     */
    public int getPointsThresholdMax () {
        return pointsThresholdMax;
    }

    /**
     * <p>Setter for the pointsThresholdMax field of the {@link LoyaltyRewardData}.</p>
     *
     * @param pointsThresholdMax The pointsThresholdMax field of the {@link LoyaltyRewardData}.
     */
    public void setPointsThresholdMax (int pointsThresholdMax) {
        this.pointsThresholdMax = pointsThresholdMax;
    }

    /**
     * <p>Getter for the rewardValue field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The rewardValue field of the {@link LoyaltyRewardData}.
     */
    public double getRewardValue () {
        return rewardValue;
    }

    /**
     * <p>Setter for the rewardValue field of the {@link LoyaltyRewardData}.</p>
     *
     * @param rewardValue The rewardValue field of the {@link LoyaltyRewardData}.
     */
    public void setRewardValue (double rewardValue) {
        this.rewardValue = rewardValue;
    }

    /**
     * <p>Getter for the redeemAtPOS field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The redeemAtPOS field of the {@link LoyaltyRewardData}.
     */
    public boolean getRedeemAtPOS () {
        return redeemAtPOS;
    }

    /**
     * <p>Setter for the redeemAtPOS field of the {@link LoyaltyRewardData}.</p>
     *
     * @param redeemAtPOS The redeemAtPOS field of the {@link LoyaltyRewardData}.
     */
    public void setRedeemAtPOS (boolean redeemAtPOS) {
        this.redeemAtPOS = redeemAtPOS;
    }

    /**
     * <p>Getter for the autoPayout field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The autoPayout field of the {@link LoyaltyRewardData}.
     */
    public boolean getAutoPayout () {
        return autoPayout;
    }

    /**
     * <p>Setter for the autoPayout field of the {@link LoyaltyRewardData}.</p>
     *
     * @param autoPayout The autoPayout field of the {@link LoyaltyRewardData}.
     */
    public void setAutoPayout (boolean autoPayout) {
        this.autoPayout = autoPayout;
    }

    /**
     * <p>Getter for the autoChooseTerminal field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The autoChooseTerminal field of the {@link LoyaltyRewardData}.
     */
    public boolean getAutoChooseTerminal () {
        return autoChooseTerminal;
    }

    /**
     * <p>Setter for the autoChooseTerminal field of the {@link LoyaltyRewardData}.</p>
     *
     * @param autoChooseTerminal The autoChooseTerminal field of the {@link LoyaltyRewardData}.
     */
    public void setAutoChooseTerminal (boolean autoChooseTerminal) {
        this.autoChooseTerminal = autoChooseTerminal;
    }

    /**
     * <p>Getter for the applyToAllPLUs field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The applyToAllPLUs field of the {@link LoyaltyRewardData}.
     */
    public boolean getApplyToAllPLUs () {
        return applyToAllPLUs;
    }

    /**
     * <p>Setter for the applyToAllPLUs field of the {@link LoyaltyRewardData}.</p>
     *
     * @param applyToAllPLUs The applyToAllPLUs field of the {@link LoyaltyRewardData}.
     */
    public void setApplyToAllPLUs (boolean applyToAllPLUs) {
        this.applyToAllPLUs = applyToAllPLUs;
    }

    /**
     * <p>Getter for the taxIDs field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The taxIDs field of the {@link LoyaltyRewardData}.
     */
    public String getTaxIDs () {
        return taxIDs;
    }

    /**
     * <p>Setter for the taxIDs field of the {@link LoyaltyRewardData}.</p>
     *
     * @param taxIDs The taxIDs field of the {@link LoyaltyRewardData}.
     */
    public void setTaxIDs (String taxIDs) {
        this.taxIDs = taxIDs;
    }

    /**
     * <p>Getter for the shortName field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The shortName field of the {@link LoyaltyRewardData}.
     */
    public String getShortName () {
        return shortName;
    }

    /**
     * <p>Setter for the shortName field of the {@link LoyaltyRewardData}.</p>
     *
     * @param shortName The shortName field of the {@link LoyaltyRewardData}.
     */
    public void setShortName (String shortName) {
        this.shortName = shortName;
    }

    /**
     * <p>Getter for the minPurchase field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The minPurchase field of the {@link LoyaltyRewardData}.
     */
    public double getMinPurchase () {
        return minPurchase;
    }

    /**
     * <p>Setter for the minPurchase field of the {@link LoyaltyRewardData}.</p>
     *
     * @param minPurchase The minPurchase field of the {@link LoyaltyRewardData}.
     */
    public void setMinPurchase (double minPurchase) {
        this.minPurchase = minPurchase;
    }

    /**
     * <p>Getter for the maxDiscount field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The maxDiscount field of the {@link LoyaltyRewardData}.
     */
    public double getMaxDiscount () {
        return maxDiscount;
    }

    /**
     * <p>Setter for the maxDiscount field of the {@link LoyaltyRewardData}.</p>
     *
     * @param maxDiscount The maxDiscount field of the {@link LoyaltyRewardData}.
     */
    public void setMaxDiscount (double maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    /**
     * <p>Getter for the terminalID field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The terminalID field of the {@link LoyaltyRewardData}.
     */
    public int getTerminalID () {
        return terminalID;
    }

    /**
     * <p>Setter for the terminalID field of the {@link LoyaltyRewardData}.</p>
     *
     * @param terminalID The terminalID field of the {@link LoyaltyRewardData}.
     */
    public void setTerminalID (int terminalID) {
        this.terminalID = terminalID;
    }

    /**
     * <p>Getter for the active field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The active field of the {@link LoyaltyRewardData}.
     */
    public boolean getActive () {
        return active;
    }

    /**
     * <p>Setter for the active field of the {@link LoyaltyRewardData}.</p>
     *
     * @param active The active field of the {@link LoyaltyRewardData}.
     */
    public void setActive (boolean active) {
        this.active = active;
    }

    /**
     * <p>Getter for the canUseWithOtherRewards field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The canUseWithOtherRewards field of the {@link LoyaltyRewardData}.
     */
    public boolean getCanUseWithOtherRewards () {
        return canUseWithOtherRewards;
    }

    /**
     * <p>Setter for the canUseWithOtherRewards field of the {@link LoyaltyRewardData}.</p>
     *
     * @param canUseWithOtherRewards The canUseWithOtherRewards field of the {@link LoyaltyRewardData}.
     */
    public void setCanUseWithOtherRewards (boolean canUseWithOtherRewards) {
        this.canUseWithOtherRewards = canUseWithOtherRewards;
    }

    /**
     * <p>Getter for the maxPerTransaction field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The maxPerTransaction field of the {@link LoyaltyRewardData}.
     */
    public int getMaxPerTransaction () {
        return maxPerTransaction;
    }

    /**
     * <p>Setter for the maxPerTransaction field of the {@link LoyaltyRewardData}.</p>
     *
     * @param maxPerTransaction The maxPerTransaction field of the {@link LoyaltyRewardData}.
     */
    public void setMaxPerTransaction (int maxPerTransaction) {
        this.maxPerTransaction = maxPerTransaction;
    }

    /**
     * <p>Getter for the loyaltyProgramId field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The loyaltyProgramId field of the {@link LoyaltyRewardData}.
     */
    public int getLoyaltyProgramId () {
        return loyaltyProgramId;
    }

    /**
     * <p>Setter for the loyaltyProgramId field of the {@link LoyaltyRewardData}.</p>
     *
     * @param loyaltyProgramId The loyaltyProgramId field of the {@link LoyaltyRewardData}.
     */
    public void setLoyaltyProgramId (int loyaltyProgramId) {
        this.loyaltyProgramId = loyaltyProgramId;
    }

    /**
     * <p>Getter for the allowProductsOverMaximum field of the {@link LoyaltyRewardData}.</p>
     *
     * @return The allowProductsOverMaximum field of the {@link LoyaltyRewardData}.
     */
    public boolean getAllowProductsOverMaximum () {
        return allowProductsOverMaximum;
    }

    /**
     * <p>Setter for the allowProductsOverMaximum field of the {@link LoyaltyRewardData}.</p>
     *
     * @param allowProductsOverMaximum The allowProductsOverMaximum field of the {@link LoyaltyRewardData}.
     */
    public void setAllowProductsOverMaximum (boolean allowProductsOverMaximum) {
        this.allowProductsOverMaximum = allowProductsOverMaximum;
    }

}