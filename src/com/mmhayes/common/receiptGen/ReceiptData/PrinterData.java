package com.mmhayes.common.receiptGen.ReceiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-06-16 16:57:03 -0400 (Tue, 16 Jun 2020) $: Date of last commit
    $Rev: 12005 $: Revision of last commit
    Notes: Represents a printer for executing print jobs.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Represents a printer for executing print jobs.
 *
 */
public class PrinterData {

    // private member variables of a PrinterData
    private int printerID = 0;
    private String logicalName = "";
    private String description = "";
    private boolean isExpeditorPrinter = false;
    private int ownershipGroupID = 0;
    private int printerHostID = 0;
    private boolean active = false;
    private String name = "";
    private int printerTypeID = 0;
    private int expireJobMins = 0;
    private boolean printsDigitalOrdersOnly = false;
    private int stationID = 0;
    private int printerHardwareTypeID = 0;
    private boolean printOnlyNewItems = false;
    private boolean noExpeditorTktsUnlessProdAdded = false;
    private int expeditorPrinterID = 0;
    private int remoteOrderPrinterID = 0;
    private boolean remoteOrderUseExpeditor = false;
    private boolean isRemoteOrderExpeditorPrinter = false;

    /**
     * Constructor for a printer data.
     *
     * @param hm {@link HashMap} HashMap containg the data from the QC_Printer table which can be used to create a PrinterData.
     */
    public PrinterData (HashMap hm) {
        printerID = HashMapDataFns.getIntVal(hm, "PRINTERID");
        logicalName = HashMapDataFns.getStringVal(hm, "LOGICALNAME");
        description = HashMapDataFns.getStringVal(hm, "DESCRIPTION");
        isExpeditorPrinter = HashMapDataFns.getBooleanVal(hm, "ISEXPEDITORPRINTER");
        ownershipGroupID = HashMapDataFns.getIntVal(hm, "OWNERSHIPGROUPID");
        printerHostID = HashMapDataFns.getIntVal(hm, "PRINTCONTROLLERID");
        active = HashMapDataFns.getBooleanVal(hm, "ACTIVE");
        name = HashMapDataFns.getStringVal(hm, "NAME");
        printerTypeID = HashMapDataFns.getIntVal(hm, "PRINTERTYPEID");
        expireJobMins = HashMapDataFns.getIntVal(hm, "EXPIREJOBMINS");
        printsDigitalOrdersOnly = HashMapDataFns.getBooleanVal(hm, "PRINTSDIGITALORDERSONLY");
        stationID = HashMapDataFns.getIntVal(hm, "STATIONID");
        printerHardwareTypeID = HashMapDataFns.getIntVal(hm, "PRINTERHARDWARETYPEID");
        printOnlyNewItems = HashMapDataFns.getBooleanVal(hm, "PRINTONLYNEWITEMS");
        noExpeditorTktsUnlessProdAdded = HashMapDataFns.getBooleanVal(hm, "NOEXPEDITORTKTSUNLESSPRODADDED");
        expeditorPrinterID = HashMapDataFns.getIntVal(hm, "EXPEDITORPRINTERID");
        remoteOrderPrinterID = HashMapDataFns.getIntVal(hm, "REMOTEORDERPRINTERID");
        remoteOrderUseExpeditor = HashMapDataFns.getBooleanVal(hm, "REMOTEORDERSUSEEXPEDITOR");
    }

    /**
     * Constructor for a PrinterData with the given printer ID and revenue center ID.
     *
     * @param printerID ID of the printer.
     * @param revenueCenterID ID of the revenue center the printer is in.
     */
    public PrinterData (int printerID, int revenueCenterID) {
        this(getPrinterData(printerID, revenueCenterID));
    }

    /**
     * Gets the printer data for the printer with the given printer ID and revenue center ID.
     *
     * @param printerID ID of the printer.
     * @param revenueCenterID ID of the revenue center the printer is in.
     * @return {@link HashMap} The printer data for the printer with the given printer ID and revenue center ID.
     */
    @SuppressWarnings("unchecked")
    private static HashMap getPrinterData (int printerID, int revenueCenterID) {
        HashMap printerData = new HashMap();

        try {
            if ((printerID > 0) && (revenueCenterID > 0)) {
                ArrayList<HashMap> queryRes = new DataManager().parameterizedExecuteQuery("data.kitchenPrinter.GetPtrData", new Object[]{printerID, revenueCenterID}, true);
                if ((!DataFunctions.isEmptyCollection(queryRes)) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
                    printerData = queryRes.get(0);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to get the printer data for the printer with " +
                    "and ID of %s within the revenue center with an ID of %s in PrinterData.getPrinterData",
                    Objects.toString(printerID, "NULL"),
                    Objects.toString(revenueCenterID, "NULL")), Logger.LEVEL.ERROR);
        }

        return printerData;
    }

    /**
     * Overridden toString method for a PrinterData.
     *
     * @return {@link String} String representation of a printer data.
     */
    @SuppressWarnings("SpellCheckingInspection")
    @Override
    public String toString () {
        return String.format("PRINTERID: %s, LOGICALNAME: %s, DESCRIPTION: %s, ISEXPEDITORPRINTER: %s, " +
                "OWNERSHIPGROUPID: %s, PRINTERHOSTID: %s, ACTIVE: %s, NAME: %s, PRINTERTYPEID: %s, EXPIREJOBMINS: %s, " +
                "PRINTSDIGITALORDERSONLY: %s, STATIONID: %s, PRINTERHARDWARETYPEID: %s, PRINTONLYNEWITEMS: %s, " +
                "NOEXPEDITORTKTSUNLESSPRODADDED: %s, EXPEDITORPRINTERID: %s, REMOTEORDERPRINTERID: %s, " +
                "REMOTEORDERUSEEXPEDITOR: %s, ISREMOTEORDEREXPEDITORPRINTER: %s",
                Objects.toString(printerID, "NULL"),
                Objects.toString(logicalName, "NULL"),
                Objects.toString(description, "NULL"),
                Objects.toString((isExpeditorPrinter ? "true" : "false"), "NULL"),
                Objects.toString(ownershipGroupID, "NULL"),
                Objects.toString(printerHostID, "NULL"),
                Objects.toString((active ? "true" : "false"), "NULL"),
                Objects.toString(name, "NULL"),
                Objects.toString(printerTypeID, "NULL"),
                Objects.toString(expireJobMins, "NULL"),
                Objects.toString((printsDigitalOrdersOnly ? "true" : "false"), "NULL"),
                Objects.toString(stationID, "NULL"),
                Objects.toString(printerHardwareTypeID, "NULL"),
                Objects.toString((printOnlyNewItems ? "true" : "false"), "NULL"),
                Objects.toString((noExpeditorTktsUnlessProdAdded ? "true" : "false"), "NULL"),
                Objects.toString(expeditorPrinterID, "NULL"),
                Objects.toString(remoteOrderPrinterID, "NULL"),
                Objects.toString((remoteOrderUseExpeditor ? "true" : "false"), "NULL"),
                Objects.toString(isRemoteOrderExpeditorPrinter, "NULL"));
    }

    /**
     * Getter for the PrinterData's printerID field.
     *
     * @return The printerID field.
     */
    public int getPrinterID () {
        return printerID;
    }

    /**
     * Setter for the PrinterData's printerID field.
     *
     * @param printerID The PrinterData's printerID field.
     */
    public void setPrinterID (int printerID) {
        this.printerID = printerID;
    }

    /**
     * Getter for the PrinterData's logicalName field.
     *
     * @return The logicalName field.
     */
    public String getLogicalName () {
        return logicalName;
    }

    /**
     * Setter for the PrinterData's logicalName field.
     *
     * @param logicalName The PrinterData's logicalName field.
     */
    public void setLogicalName (String logicalName) {
        this.logicalName = logicalName;
    }

    /**
     * Getter for the PrinterData's description field.
     *
     * @return The description field.
     */
    public String getDescription () {
        return description;
    }

    /**
     * Setter for the PrinterData's description field.
     *
     * @param description The PrinterData's description field.
     */
    public void setDescription (String description) {
        this.description = description;
    }

    /**
     * Getter for the PrinterData's isExpeditorPrinter field.
     *
     * @return The isExpeditorPrinter field.
     */
    public boolean getIsExpeditorPrinter () {
        return isExpeditorPrinter;
    }

    /**
     * Setter for the PrinterData's isExpeditorPrinter field.
     *
     * @param isExpeditorPrinter The PrinterData's isExpeditorPrinter field.
     */
    public void setIsExpeditorPrinter (boolean isExpeditorPrinter) {
        this.isExpeditorPrinter = isExpeditorPrinter;
    }

    /**
     * Getter for the PrinterData's ownershipGroupID field.
     *
     * @return The ownershipGroupID field.
     */
    public int getOwnershipGroupID () {
        return ownershipGroupID;
    }

    /**
     * Setter for the PrinterData's ownershipGroupID field.
     *
     * @param ownershipGroupID The PrinterData's ownershipGroupID field.
     */
    public void setOwnershipGroupID (int ownershipGroupID) {
        this.ownershipGroupID = ownershipGroupID;
    }

    /**
     * Getter for the PrinterData's printerHostID field.
     *
     * @return The printerHostID field.
     */
    public int getPrinterHostID () {
        return printerHostID;
    }

    /**
     * Setter for the PrinterData's printerHostID field.
     *
     * @param printerHostID The PrinterData's printerHostID field.
     */
    public void setPrinterHostID (int printerHostID) {
        this.printerHostID = printerHostID;
    }

    /**
     * Getter for the PrinterData's active field.
     *
     * @return The active field.
     */
    public boolean getActive () {
        return active;
    }

    /**
     * Setter for the PrinterData's active field.
     *
     * @param active The PrinterData's active field.
     */
    public void setActive (boolean active) {
        this.active = active;
    }

    /**
     * Getter for the PrinterData's name field.
     *
     * @return The name field.
     */
    public String getName () {
        return name;
    }

    /**
     * Setter for the PrinterData's name field.
     *
     * @param name The PrinterData's name field.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * Getter for the PrinterData's printerTypeID field.
     *
     * @return The printerTypeID field.
     */
    public int getPrinterTypeID () {
        return printerTypeID;
    }

    /**
     * Setter for the PrinterData's printerTypeID field.
     *
     * @param printerTypeID The PrinterData's printerTypeID field.
     */
    public void setPrinterTypeID (int printerTypeID) {
        this.printerTypeID = printerTypeID;
    }

    /**
     * Getter for the PrinterData's expireJobMins field.
     *
     * @return The expireJobMins field.
     */
    public int getExpireJobMins () {
        return expireJobMins;
    }

    /**
     * Setter for the PrinterData's expireJobMins field.
     *
     * @param expireJobMins The PrinterData's expireJobMins field.
     */
    public void setExpireJobMins (int expireJobMins) {
        this.expireJobMins = expireJobMins;
    }

    /**
     * Getter for the PrinterData's printsDigitalOrdersOnly field.
     *
     * @return The printsDigitalOrdersOnly field.
     */
    public boolean getPrintsDigitalOrdersOnly () {
        return printsDigitalOrdersOnly;
    }

    /**
     * Setter for the PrinterData's printsDigitalOrdersOnly field.
     *
     * @param printsDigitalOrdersOnly The PrinterData's printsDigitalOrdersOnly field.
     */
    public void setPrintsDigitalOrdersOnly (boolean printsDigitalOrdersOnly) {
        this.printsDigitalOrdersOnly = printsDigitalOrdersOnly;
    }

    /**
     * Getter for the PrinterData's stationID field.
     *
     * @return The stationID field.
     */
    public int getStationID () {
        return stationID;
    }

    /**
     * Setter for the PrinterData's stationID field.
     *
     * @param stationID The PrinterData's stationID field.
     */
    public void setStationID (int stationID) {
        this.stationID = stationID;
    }

    /**
     * Getter for the PrinterData's printerHardwareTypeID field.
     *
     * @return The printerHardwareTypeID field.
     */
    public int getPrinterHardwareTypeID () {
        return printerHardwareTypeID;
    }

    /**
     * Setter for the PrinterData's printerHardwareTypeID field.
     *
     * @param printerHardwareTypeID The PrinterData's printerHardwareTypeID field.
     */
    public void setPrinterHardwareTypeID (int printerHardwareTypeID) {
        this.printerHardwareTypeID = printerHardwareTypeID;
    }

    /**
     * Getter for the PrinterData's printOnlyNewItems field.
     *
     * @return The printOnlyNewItems field.
     */
    public boolean getPrintOnlyNewItems () {
        return printOnlyNewItems;
    }

    /**
     * Setter for the PrinterData's printOnlyNewItems field.
     *
     * @param printOnlyNewItems The PrinterData's printOnlyNewItems field.
     */
    public void setPrintOnlyNewItems (boolean printOnlyNewItems) {
        this.printOnlyNewItems = printOnlyNewItems;
    }

    /**
     * Getter for the PrinterData's noExpeditorTktsUnlessProdAdded field.
     *
     * @return The noExpeditorTktsUnlessProdAdded field.
     */
    public boolean getNoExpeditorTktsUnlessProdAdded () {
        return noExpeditorTktsUnlessProdAdded;
    }

    /**
     * Setter for the PrinterData's noExpeditorTktsUnlessProdAdded field.
     *
     * @param noExpeditorTktsUnlessProdAdded The PrinterData's noExpeditorTktsUnlessProdAdded field.
     */
    public void setNoExpeditorTktsUnlessProdAdded (boolean noExpeditorTktsUnlessProdAdded) {
        this.noExpeditorTktsUnlessProdAdded = noExpeditorTktsUnlessProdAdded;
    }

    /**
     * Getter for the PrinterData's expeditorPrinterID field.
     *
     * @return The expeditorPrinterID field.
     */
    public int getExpeditorPrinterID () {
        return expeditorPrinterID;
    }

    /**
     * Setter for the PrinterData's expeditorPrinterID field.
     *
     * @param expeditorPrinterID The PrinterData's expeditorPrinterID field.
     */
    public void setExpeditorPrinterID (int expeditorPrinterID) {
        this.expeditorPrinterID = expeditorPrinterID;
    }

    /**
     * Getter for the PrinterData's remoteOrderPrinterID field.
     *
     * @return The remoteOrderPrinterID field.
     */
    public int getRemoteOrderPrinterID () {
        return remoteOrderPrinterID;
    }

    /**
     * Setter for the PrinterData's remoteOrderPrinterID field.
     *
     * @param remoteOrderPrinterID The PrinterData's remoteOrderPrinterID field.
     */
    public void setRemoteOrderPrinterID (int remoteOrderPrinterID) {
        this.remoteOrderPrinterID = remoteOrderPrinterID;
    }

    /**
     * Getter for the PrinterData's remoteOrderUseExpeditor field.
     *
     * @return The remoteOrderUseExpeditor field.
     */
    public boolean getRemoteOrderUseExpeditor () {
        return remoteOrderUseExpeditor;
    }

    /**
     * Setter for the PrinterData's remoteOrderUseExpeditor field.
     *
     * @param remoteOrderUseExpeditor The PrinterData's remoteOrderUseExpeditor field.
     */
    public void setRemoteOrderUseExpeditor (boolean remoteOrderUseExpeditor) {
        this.remoteOrderUseExpeditor = remoteOrderUseExpeditor;
    }

    /**
     * <p>Getter for the isRemoteOrderExpeditorPrinter field of the {@link PrinterData}.</p>
     *
     * @return The isRemoteOrderExpeditorPrinter field of the {@link PrinterData}.
     */
    public boolean getIsRemoteOrderExpeditorPrinter () {
        return isRemoteOrderExpeditorPrinter;
    }

    /**
     * <p>Setter for the isRemoteOrderExpeditorPrinter field of the {@link PrinterData}.</p>
     *
     * @param isRemoteOrderExpeditorPrinter The isRemoteOrderExpeditorPrinter field of the {@link PrinterData}.
     */
    public void setIsRemoteOrderExpeditorPrinter (boolean isRemoteOrderExpeditorPrinter) {
        this.isRemoteOrderExpeditorPrinter = isRemoteOrderExpeditorPrinter;
    }

}
