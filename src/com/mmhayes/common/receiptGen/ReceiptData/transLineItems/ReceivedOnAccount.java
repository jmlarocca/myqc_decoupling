package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-15 10:25:09 -0400 (Fri, 15 May 2020) $: Date of last commit
    $Rev: 11752 $: Revision of last commit
    Notes: Represents a received on account line item.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.GiftCardTxnData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a received on account line item.</p>
 *
 */
public class ReceivedOnAccount implements ITransLineItem {

    // private member variables of a ReceivedOnAccount
    private int receivedOnAccountID = 0;
    private int receivedOnAccountTypeID = 0;
    private BigDecimal paTransLineItemID = null;
    private String name = "";
    private BigDecimal badgeNumber = null;
    private BigDecimal employeeID = null;
    private double quantity = 0.0d;
    private double refundedQuantity = 0.0d;
    private double amount = 0.0d;
    private double refundedAmount = 0.0d;
    private GiftCardTxnData giftCardTxnData = null;

    /**
     * <p>Sets the data for a {@link ReceivedOnAccount} transaction line item.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param lineItemRec The {@link HashMap} containing information about the transaction line item.
     */
    @Override
    public void setLineItemData (String log, DataManager dataManager, HashMap lineItemRec) {

        if (!DataFunctions.isEmptyMap(lineItemRec)) {
            this.receivedOnAccountID = HashMapDataFns.getIntVal(lineItemRec, "PAITEMID");
            this.receivedOnAccountTypeID = HashMapDataFns.getIntVal(lineItemRec, "PARECEIVEDACCTTYPEID");
            String paTransLineItemIDStr = HashMapDataFns.getStringVal(lineItemRec, "PATRANSLINEITEMID");
            if ((StringFunctions.stringHasContent(paTransLineItemIDStr)) && (NumberUtils.isNumber(paTransLineItemIDStr))) {
                this.paTransLineItemID = new BigDecimal(paTransLineItemIDStr);
            }
            this.name = HashMapDataFns.getStringVal(lineItemRec, "ITEMNAME");
            String badgeNumberStr = HashMapDataFns.getStringVal(lineItemRec, "BADGEASSIGNMENTID");
            if ((StringFunctions.stringHasContent(badgeNumberStr)) && (NumberUtils.isNumber(badgeNumberStr))) {
                this.badgeNumber = new BigDecimal(badgeNumberStr);
            }
            String employeeIDStr = HashMapDataFns.getStringVal(lineItemRec, "EMPLOYEEID");
            if ((StringFunctions.stringHasContent(employeeIDStr)) && (NumberUtils.isNumber(employeeIDStr))) {
                this.employeeID = new BigDecimal(employeeIDStr);
            }
            this.quantity = HashMapDataFns.getDoubleVal(lineItemRec, "QUANTITY");
            this.refundedQuantity = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDQUANTITY");
            this.amount = HashMapDataFns.getDoubleVal(lineItemRec, "AMOUNT");
            this.refundedAmount = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDAMOUNT");
            String gcTxnDataStr = HashMapDataFns.getStringVal(lineItemRec, "CREDITCARDTRANSINFO");
            if (StringFunctions.stringHasContent(gcTxnDataStr)) {
                this.giftCardTxnData = new GiftCardTxnData(gcTxnDataStr, ";");
            }
        }
        else {
            Logger.logMessage("The ReceivedOnAccount record can't be acquired from an empty HashMap in ReceivedOnAccount.setLineItemData", log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Overridden toString() method for a {@link ReceivedOnAccount}.</p>
     *
     * @return A {@link String} representation of this {@link ReceivedOnAccount}.
     */
    @Override
    public String toString () {

        return String.format("RECEIVEDONACCOUNTID: %s, RECEIVEDONACCOUNTTYPEID: %s, PATRANSLINEITEMID: %s, NAME: %s, BADGENUMBER: %s, " +
                "EMPLOYEEID: %s, QUANTITY: %s, REFUNDEDQUANTITY: %s, AMOUNT: %s, REFUNDEDAMOUNT: %s, GIFTCARDTXNDATA: %s",
                Objects.toString((receivedOnAccountID != -1 ? receivedOnAccountID : "N/A"), "N/A"),
                Objects.toString((receivedOnAccountTypeID != -1 ? receivedOnAccountTypeID : "N/A"), "N/A"),
                Objects.toString((((paTransLineItemID != null) && (StringFunctions.stringHasContent(paTransLineItemID.toPlainString()))) ? paTransLineItemID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((((badgeNumber != null) && (StringFunctions.stringHasContent(badgeNumber.toPlainString()))) ? badgeNumber.toPlainString() : "N/A"), "N/A"),
                Objects.toString((((employeeID != null) && (StringFunctions.stringHasContent(employeeID.toPlainString()))) ? employeeID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((quantity != -1.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQuantity != -1.0d ? refundedQuantity : "N/A"), "N/A"),
                Objects.toString((amount != -1.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount != -1.0d ? refundedAmount : "N/A"), "N/A"),
                Objects.toString((giftCardTxnData != null ? giftCardTxnData.toString() : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link ReceivedOnAccount}.
     * Two {@link ReceivedOnAccount} are defined as being equal if they have the same receivedOnAccountID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link ReceivedOnAccount}.
     * @return Whether or not the {@link Object} is equal to this {@link ReceivedOnAccount}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a ReceivedOnAccount and check if the obj's receivedOnAccountID is equal to this ReceivedOnAccount's receivedOnAccountID
        ReceivedOnAccount receivedOnAccount = ((ReceivedOnAccount) obj);
        return Objects.equals(receivedOnAccount.receivedOnAccountID, receivedOnAccountID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link ReceivedOnAccount}.</p>
     *
     * @return The unique hash code for this {@link ReceivedOnAccount}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(receivedOnAccountID);
    }

    /**
     * <p>Getter for the receivedOnAccountID field of the {@link ReceivedOnAccount}.</p>
     *
     * @return The receivedOnAccountID field of the {@link ReceivedOnAccount}.
     */
    public int getReceivedOnAccountID () {
        return receivedOnAccountID;
    }

    /**
     * <p>Setter for the receivedOnAccountID field of the {@link ReceivedOnAccount}.</p>
     *
     * @param receivedOnAccountID The receivedOnAccountID field of the {@link ReceivedOnAccount}.
     */
    public void setReceivedOnAccountID (int receivedOnAccountID) {
        this.receivedOnAccountID = receivedOnAccountID;
    }

    /**
     * <p>Getter for the receivedOnAccountTypeID field of the {@link ReceivedOnAccount}.</p>
     *
     * @return The receivedOnAccountTypeID field of the {@link ReceivedOnAccount}.
     */
    public int getReceivedOnAccountTypeID () {
        return receivedOnAccountTypeID;
    }

    /**
     * <p>Setter for the receivedOnAccountTypeID field of the {@link ReceivedOnAccount}.</p>
     *
     * @param receivedOnAccountTypeID The receivedOnAccountTypeID field of the {@link ReceivedOnAccount}.
     */
    public void setReceivedOnAccountTypeID (int receivedOnAccountTypeID) {
        this.receivedOnAccountTypeID = receivedOnAccountTypeID;
    }

    /**
     * <p>Getter for the paTransLineItemID field of the {@link ReceivedOnAccount}.</p>
     *
     * @return The paTransLineItemID field of the {@link ReceivedOnAccount}.
     */
    public BigDecimal getPaTransLineItemID () {
        return paTransLineItemID;
    }

    /**
     * <p>Setter for the paTransLineItemID field of the {@link ReceivedOnAccount}.</p>
     *
     * @param paTransLineItemID The paTransLineItemID field of the {@link ReceivedOnAccount}.
     */
    public void setPaTransLineItemID (BigDecimal paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
    }

    /**
     * <p>Getter for the name field of the {@link ReceivedOnAccount}.</p>
     *
     * @return The name field of the {@link ReceivedOnAccount}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link ReceivedOnAccount}.</p>
     *
     * @param name The name field of the {@link ReceivedOnAccount}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the badgeNumber field of the {@link ReceivedOnAccount}.</p>
     *
     * @return The badgeNumber field of the {@link ReceivedOnAccount}.
     */
    public BigDecimal getBadgeNumber () {
        return badgeNumber;
    }

    /**
     * <p>Setter for the badgeNumber field of the {@link ReceivedOnAccount}.</p>
     *
     * @param badgeNumber The badgeNumber field of the {@link ReceivedOnAccount}.
     */
    public void setBadgeNumber (BigDecimal badgeNumber) {
        this.badgeNumber = badgeNumber;
    }

    /**
     * <p>Getter for the employeeID field of the {@link ReceivedOnAccount}.</p>
     *
     * @return The employeeID field of the {@link ReceivedOnAccount}.
     */
    public BigDecimal getEmployeeID () {
        return employeeID;
    }

    /**
     * <p>Setter for the employeeID field of the {@link ReceivedOnAccount}.</p>
     *
     * @param employeeID The employeeID field of the {@link ReceivedOnAccount}.
     */
    public void setEmployeeID (BigDecimal employeeID) {
        this.employeeID = employeeID;
    }

    /**
     * <p>Getter for the quantity field of the {@link ReceivedOnAccount}.</p>
     *
     * @return The quantity field of the {@link ReceivedOnAccount}.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link ReceivedOnAccount}.</p>
     *
     * @param quantity The quantity field of the {@link ReceivedOnAccount}.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the refundedQuantity field of the {@link ReceivedOnAccount}.</p>
     *
     * @return The refundedQuantity field of the {@link ReceivedOnAccount}.
     */
    public double getRefundedQuantity () {
        return refundedQuantity;
    }

    /**
     * <p>Setter for the refundedQuantity field of the {@link ReceivedOnAccount}.</p>
     *
     * @param refundedQuantity The refundedQuantity field of the {@link ReceivedOnAccount}.
     */
    public void setRefundedQuantity (double refundedQuantity) {
        this.refundedQuantity = refundedQuantity;
    }

    /**
     * <p>Getter for the amount field of the {@link ReceivedOnAccount}.</p>
     *
     * @return The amount field of the {@link ReceivedOnAccount}.
     */
    public double getAmount () {
        return amount;
    }

    /**
     * <p>Setter for the amount field of the {@link ReceivedOnAccount}.</p>
     *
     * @param amount The amount field of the {@link ReceivedOnAccount}.
     */
    public void setAmount (double amount) {
        this.amount = amount;
    }

    /**
     * <p>Getter for the refundedAmount field of the {@link ReceivedOnAccount}.</p>
     *
     * @return The refundedAmount field of the {@link ReceivedOnAccount}.
     */
    public double getRefundedAmount () {
        return refundedAmount;
    }

    /**
     * <p>Setter for the refundedAmount field of the {@link ReceivedOnAccount}.</p>
     *
     * @param refundedAmount The refundedAmount field of the {@link ReceivedOnAccount}.
     */
    public void setRefundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    /**
     * <p>Getter for the giftCardTxnData field of the {@link ReceivedOnAccount}.</p>
     *
     * @return The giftCardTxnData field of the {@link ReceivedOnAccount}.
     */
    public GiftCardTxnData getGiftCardTxnData () {
        return giftCardTxnData;
    }

    /**
     * <p>Setter for the giftCardTxnData field of the {@link ReceivedOnAccount}.</p>
     *
     * @param giftCardTxnData The giftCardTxnData field of the {@link ReceivedOnAccount}.
     */
    public void setGiftCardTxnData (GiftCardTxnData giftCardTxnData) {
        this.giftCardTxnData = giftCardTxnData;
    }

}