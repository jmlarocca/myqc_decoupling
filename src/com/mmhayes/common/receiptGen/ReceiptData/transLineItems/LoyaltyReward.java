package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-15 10:25:09 -0400 (Fri, 15 May 2020) $: Date of last commit
    $Rev: 11752 $: Revision of last commit
    Notes: Represents a loyalty reward line item.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a loyalty reward line item.</p>
 *
 */
public class LoyaltyReward implements ITransLineItem {

    // private member variables of a LoyaltyReward
    private int loyaltyRewardID = 0;
    private int loyaltyRewardTypeID = 0;
    private BigDecimal paTransLineItemID = null;
    private String name = "";
    private String shortName = "";
    private double quantity = 0.0d;
    private double refundedQuantity = 0.0d;
    private double amount = 0.0d;
    private double refundedAmount = 0.0d;
    private boolean applyToAllProducts = false;

    /**
     * <p>Sets the data for a {@link LoyaltyReward} transaction line item.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param lineItemRec The {@link HashMap} containing information about the transaction line item.
     */
    @Override
    public void setLineItemData (String log, DataManager dataManager, HashMap lineItemRec) {

        if (!DataFunctions.isEmptyMap(lineItemRec)) {
            this.loyaltyRewardID = HashMapDataFns.getIntVal(lineItemRec, "PAITEMID");
            this.loyaltyRewardTypeID = HashMapDataFns.getIntVal(lineItemRec, "LOYALTYREWARDTYPEID");
            String paTransLineItemIDStr = HashMapDataFns.getStringVal(lineItemRec, "PATRANSLINEITEMID");
            if ((StringFunctions.stringHasContent(paTransLineItemIDStr)) && (NumberUtils.isNumber(paTransLineItemIDStr))) {
                this.paTransLineItemID = new BigDecimal(paTransLineItemIDStr);
            }
            this.name = HashMapDataFns.getStringVal(lineItemRec, "ITEMNAME");
            this.shortName = HashMapDataFns.getStringVal(lineItemRec, "LOYALTYREWARDSHORTNAME");
            this.quantity = HashMapDataFns.getDoubleVal(lineItemRec, "QUANTITY");
            this.refundedQuantity = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDQUANTITY");
            this.amount = HashMapDataFns.getDoubleVal(lineItemRec, "AMOUNT");
            this.refundedAmount = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDAMOUNT");
            this.applyToAllProducts = HashMapDataFns.getBooleanVal(lineItemRec, "APPLYTOALLPLUS");
        }
        else {
            Logger.logMessage("The LoyaltyReward record can't be acquired from an empty HashMap in LoyaltyReward.setLineItemData", log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Overridden toString() method for a {@link LoyaltyReward}.</p>
     *
     * @return A {@link String} representation of this {@link LoyaltyReward}.
     */
    @Override
    public String toString () {

        return String.format("LOYALTYREWARDID: %s, LOYALTYREWARDTYPEID: %s, PATRANSLINEITEMID: %s, NAME: %s, SHORTNAME: %s, QUANTITY: %s, " +
                "REFUNDEDQUANTITY: %s, AMOUNT: %s, REFUNDEDAMOUNT: %s, APPLYTOALLPRODUCTS: %s",
                Objects.toString((loyaltyRewardID != -1 ? loyaltyRewardID : "N/A"), "N/A"),
                Objects.toString((loyaltyRewardTypeID != -1 ? loyaltyRewardTypeID : "N/A"), "N/A"),
                Objects.toString((((paTransLineItemID != null) && (StringFunctions.stringHasContent(paTransLineItemID.toPlainString()))) ? paTransLineItemID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((quantity != -1.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQuantity != -1.0d ? refundedQuantity : "N/A"), "N/A"),
                Objects.toString((amount != -1.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount != -1.0d ? refundedAmount : "N/A"), "N/A"),
                Objects.toString(applyToAllProducts, "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link LoyaltyReward}.
     * Two {@link LoyaltyReward} are defined as being equal if they have the same loyaltyRewardID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link LoyaltyReward}.
     * @return Whether or not the {@link Object} is equal to this {@link LoyaltyReward}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a LoyaltyReward and check if the obj's loyaltyRewardID is equal to this LoyaltyReward's loyaltyRewardID
        LoyaltyReward loyaltyReward = ((LoyaltyReward) obj);
        return Objects.equals(loyaltyReward.loyaltyRewardID, loyaltyRewardID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link LoyaltyReward}.</p>
     *
     * @return The unique hash code for this {@link LoyaltyReward}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(loyaltyRewardID);
    }

    /**
     * <p>Getter for the loyaltyRewardID field of the {@link LoyaltyReward}.</p>
     *
     * @return The loyaltyRewardID field of the {@link LoyaltyReward}.
     */
    public int getLoyaltyRewardID () {
        return loyaltyRewardID;
    }

    /**
     * <p>Setter for the loyaltyRewardID field of the {@link LoyaltyReward}.</p>
     *
     * @param loyaltyRewardID The loyaltyRewardID field of the {@link LoyaltyReward}.
     */
    public void setLoyaltyRewardID (int loyaltyRewardID) {
        this.loyaltyRewardID = loyaltyRewardID;
    }

    /**
     * <p>Getter for the loyaltyRewardTypeID field of the {@link LoyaltyReward}.</p>
     *
     * @return The loyaltyRewardTypeID field of the {@link LoyaltyReward}.
     */
    public int getLoyaltyRewardTypeID () {
        return loyaltyRewardTypeID;
    }

    /**
     * <p>Setter for the loyaltyRewardTypeID field of the {@link LoyaltyReward}.</p>
     *
     * @param loyaltyRewardTypeID The loyaltyRewardTypeID field of the {@link LoyaltyReward}.
     */
    public void setLoyaltyRewardTypeID (int loyaltyRewardTypeID) {
        this.loyaltyRewardTypeID = loyaltyRewardTypeID;
    }

    /**
     * <p>Getter for the paTransLineItemID field of the {@link LoyaltyReward}.</p>
     *
     * @return The paTransLineItemID field of the {@link LoyaltyReward}.
     */
    public BigDecimal getPaTransLineItemID () {
        return paTransLineItemID;
    }

    /**
     * <p>Setter for the paTransLineItemID field of the {@link LoyaltyReward}.</p>
     *
     * @param paTransLineItemID The paTransLineItemID field of the {@link LoyaltyReward}.
     */
    public void setPaTransLineItemID (BigDecimal paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
    }

    /**
     * <p>Getter for the name field of the {@link LoyaltyReward}.</p>
     *
     * @return The name field of the {@link LoyaltyReward}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link LoyaltyReward}.</p>
     *
     * @param name The name field of the {@link LoyaltyReward}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the shortName field of the {@link LoyaltyReward}.</p>
     *
     * @return The shortName field of the {@link LoyaltyReward}.
     */
    public String getShortName () {
        return shortName;
    }

    /**
     * <p>Setter for the shortName field of the {@link LoyaltyReward}.</p>
     *
     * @param shortName The shortName field of the {@link LoyaltyReward}.
     */
    public void setShortName (String shortName) {
        this.shortName = shortName;
    }

    /**
     * <p>Getter for the quantity field of the {@link LoyaltyReward}.</p>
     *
     * @return The quantity field of the {@link LoyaltyReward}.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link LoyaltyReward}.</p>
     *
     * @param quantity The quantity field of the {@link LoyaltyReward}.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the refundedQuantity field of the {@link LoyaltyReward}.</p>
     *
     * @return The refundedQuantity field of the {@link LoyaltyReward}.
     */
    public double getRefundedQuantity () {
        return refundedQuantity;
    }

    /**
     * <p>Setter for the refundedQuantity field of the {@link LoyaltyReward}.</p>
     *
     * @param refundedQuantity The refundedQuantity field of the {@link LoyaltyReward}.
     */
    public void setRefundedQuantity (double refundedQuantity) {
        this.refundedQuantity = refundedQuantity;
    }

    /**
     * <p>Getter for the amount field of the {@link LoyaltyReward}.</p>
     *
     * @return The amount field of the {@link LoyaltyReward}.
     */
    public double getAmount () {
        return amount;
    }

    /**
     * <p>Setter for the amount field of the {@link LoyaltyReward}.</p>
     *
     * @param amount The amount field of the {@link LoyaltyReward}.
     */
    public void setAmount (double amount) {
        this.amount = amount;
    }

    /**
     * <p>Getter for the refundedAmount field of the {@link LoyaltyReward}.</p>
     *
     * @return The refundedAmount field of the {@link LoyaltyReward}.
     */
    public double getRefundedAmount () {
        return refundedAmount;
    }

    /**
     * <p>Setter for the refundedAmount field of the {@link LoyaltyReward}.</p>
     *
     * @param refundedAmount The refundedAmount field of the {@link LoyaltyReward}.
     */
    public void setRefundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    /**
     * <p>Getter for the applyToAllProducts field of the {@link LoyaltyReward}.</p>
     *
     * @return The applyToAllProducts field of the {@link LoyaltyReward}.
     */
    public boolean getApplyToAllProducts () {
        return applyToAllProducts;
    }

    /**
     * <p>Setter for the applyToAllProducts field of the {@link LoyaltyReward}.</p>
     *
     * @param applyToAllProducts The applyToAllProducts field of the {@link LoyaltyReward}.
     */
    public void setApplyToAllProducts (boolean applyToAllProducts) {
        this.applyToAllProducts = applyToAllProducts;
    }

}