package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-18 10:29:09 -0400 (Mon, 18 May 2020) $: Date of last commit
    $Rev: 11768 $: Revision of last commit
    Notes: Represents a combo line item.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a combo line item.</p>
 *
 */
public class Combo implements ITransLineItem {

    // private member variables of a Combo
    private int comboID = 0;
    private BigDecimal paTransLineItemID = null;
    private String name = "";
    private String itemComment = "";
    private String originalOrderNum = "";
    private double quantity = 0.0d;
    private double refundedQuantity = 0.0d;
    private double amount = 0.0d;
    private double refundedAmount = 0.0d;
    private double rootPrice = 0.0d;

    /**
     * <p>Sets the data for a {@link Combo} transaction line item.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param lineItemRec The {@link HashMap} containing information about the transaction line item.
     */
    @Override
    public void setLineItemData (String log, DataManager dataManager, HashMap lineItemRec) {

        if (!DataFunctions.isEmptyMap(lineItemRec)) {
            this.comboID = HashMapDataFns.getIntVal(lineItemRec, "PAITEMID");
            String paTransLineItemIDStr = HashMapDataFns.getStringVal(lineItemRec, "PATRANSLINEITEMID");
            if ((StringFunctions.stringHasContent(paTransLineItemIDStr)) && (NumberUtils.isNumber(paTransLineItemIDStr))) {
                this.paTransLineItemID = new BigDecimal(paTransLineItemIDStr);
            }
            this.name = HashMapDataFns.getStringVal(lineItemRec, "ITEMNAME");
            this.itemComment = HashMapDataFns.getStringVal(lineItemRec, "ITEMCOMMENT");
            this.originalOrderNum = HashMapDataFns.getStringVal(lineItemRec, "ORIGINALORDERNUM");
            this.quantity = HashMapDataFns.getDoubleVal(lineItemRec, "QUANTITY");
            this.refundedQuantity = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDQUANTITY");
            this.amount = HashMapDataFns.getDoubleVal(lineItemRec, "AMOUNT");
            this.refundedAmount = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDAMOUNT");
        }
        else {
            Logger.logMessage("The Combo record can't be acquired from an empty HashMap in Combo.setLineItemData", log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Overridden toString() method for a {@link Combo}.</p>
     *
     * @return A {@link String} representation of this {@link Combo}.
     */
    @Override
    public String toString () {

        return String.format("COMBOID: %s, PATRANSLINEITEMID: %s, NAME: %s, ITEMCOMMENT: %s, ORIGINALORDERNUM: %s, QUANTITY: %s, REFUNDEDQUANTITY: %s, AMOUNT: %s, REFUNDEDAMOUNT: %s, ROOTPRICE: %s",
                Objects.toString((comboID != -1 ? comboID : "N/A"), "N/A"),
                Objects.toString((((paTransLineItemID != null) && (StringFunctions.stringHasContent(paTransLineItemID.toPlainString()))) ? paTransLineItemID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(itemComment) ? itemComment : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(originalOrderNum) ? originalOrderNum : "N/A"), "N/A"),
                Objects.toString((quantity != -1.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQuantity != -1.0d ? refundedQuantity : "N/A"), "N/A"),
                Objects.toString((amount != -1.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount != -1.0d ? refundedAmount : "N/A"), "N/A"),
                Objects.toString((rootPrice != -1.0d ? rootPrice : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link Combo}.
     * Two {@link Combo} are defined as being equal if they have the same comboID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link Combo}.
     * @return Whether or not the {@link Object} is equal to this {@link Combo}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a Combo and check if the obj's comboID is equal to this Combo's comboID
        Combo combo = ((Combo) obj);
        return Objects.equals(combo.comboID, comboID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link Combo}.</p>
     *
     * @return The unique hash code for this {@link Combo}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(comboID);
    }

    /**
     * <p>Getter for the comboID field of the {@link Combo}.</p>
     *
     * @return The comboID field of the {@link Combo}.
     */
    public int getComboID () {
        return comboID;
    }

    /**
     * <p>Setter for the comboID field of the {@link Combo}.</p>
     *
     * @param comboID The comboID field of the {@link Combo}.
     */
    public void setComboID (int comboID) {
        this.comboID = comboID;
    }

    /**
     * <p>Getter for the paTransLineItemID field of the {@link Combo}.</p>
     *
     * @return The paTransLineItemID field of the {@link Combo}.
     */
    public BigDecimal getPaTransLineItemID () {
        return paTransLineItemID;
    }

    /**
     * <p>Setter for the paTransLineItemID field of the {@link Combo}.</p>
     *
     * @param paTransLineItemID The paTransLineItemID field of the {@link Combo}.
     */
    public void setPaTransLineItemID (BigDecimal paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
    }

    /**
     * <p>Getter for the name field of the {@link Combo}.</p>
     *
     * @return The name field of the {@link Combo}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link Combo}.</p>
     *
     * @param name The name field of the {@link Combo}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the itemComment field of the {@link Combo}.</p>
     *
     * @return The itemComment field of the {@link Combo}.
     */
    public String getItemComment () {
        return itemComment;
    }

    /**
     * <p>Setter for the itemComment field of the {@link Combo}.</p>
     *
     * @param itemComment The itemComment field of the {@link Combo}.
     */
    public void setItemComment (String itemComment) {
        this.itemComment = itemComment;
    }

    /**
     * <p>Getter for the originalOrderNum field of the {@link Combo}.</p>
     *
     * @return The originalOrderNum field of the {@link Combo}.
     */
    public String getOriginalOrderNum () {
        return originalOrderNum;
    }

    /**
     * <p>Setter for the originalOrderNum field of the {@link Combo}.</p>
     *
     * @param originalOrderNum The originalOrderNum field of the {@link Combo}.
     */
    public void setOriginalOrderNum (String originalOrderNum) {
        this.originalOrderNum = originalOrderNum;
    }

    /**
     * <p>Getter for the quantity field of the {@link Combo}.</p>
     *
     * @return The quantity field of the {@link Combo}.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link Combo}.</p>
     *
     * @param quantity The quantity field of the {@link Combo}.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the refundedQuantity field of the {@link Combo}.</p>
     *
     * @return The refundedQuantity field of the {@link Combo}.
     */
    public double getRefundedQuantity () {
        return refundedQuantity;
    }

    /**
     * <p>Setter for the refundedQuantity field of the {@link Combo}.</p>
     *
     * @param refundedQuantity The refundedQuantity field of the {@link Combo}.
     */
    public void setRefundedQuantity (double refundedQuantity) {
        this.refundedQuantity = refundedQuantity;
    }

    /**
     * <p>Getter for the amount field of the {@link Combo}.</p>
     *
     * @return The amount field of the {@link Combo}.
     */
    public double getAmount () {
        return amount;
    }

    /**
     * <p>Setter for the amount field of the {@link Combo}.</p>
     *
     * @param amount The amount field of the {@link Combo}.
     */
    public void setAmount (double amount) {
        this.amount = amount;
    }

    /**
     * <p>Getter for the refundedAmount field of the {@link Combo}.</p>
     *
     * @return The refundedAmount field of the {@link Combo}.
     */
    public double getRefundedAmount () {
        return refundedAmount;
    }

    /**
     * <p>Setter for the refundedAmount field of the {@link Combo}.</p>
     *
     * @param refundedAmount The refundedAmount field of the {@link Combo}.
     */
    public void setRefundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    /**
     * <p>Getter for the rootPrice field of the {@link Combo}.</p>
     *
     * @return The rootPrice field of the {@link Combo}.
     */
    public double getRootPrice () {
        return rootPrice;
    }

    /**
     * <p>Setter for the rootPrice field of the {@link Combo}.</p>
     *
     * @param rootPrice The rootPrice field of the {@link Combo}.
     */
    public void setRootPrice (double rootPrice) {
        this.rootPrice = rootPrice;
    }

}