package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-27 12:53:53 -0400 (Wed, 27 May 2020) $: Date of last commit
    $Rev: 11824 $: Revision of last commit
    Notes: Represents a product line item.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kms.types.KMSOrderStatus;
import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.LoyaltyRewardData;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep.IKitchenPrep;
import com.mmhayes.common.receiptGen.TransactionData.PrepOption;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a product line item.</p>
 *
 */
public class Product implements ITransLineItem {

    // private member variables of a Product
    private int productID = 0;
    private BigDecimal paTransLineItemID = null;
    private BigDecimal paTransLineItemModID = null;
    private String name = "";
    private String productCode = "";
    private String originalOrderNumber = "";
    private boolean isModifier = false;
    private boolean isWeightedItem = false;
    private boolean isManualWeightedItem = false;
    private boolean isDiningOption = false;
    private boolean printsOnExpeditor = false;
    private double quantity = 0.0d;
    private double netWeight = 0.0d;
    private BigDecimal grossWeight = null;
    private BigDecimal fixedNetWeight = null;
    private double nutritionInfo1 = 0.0d;
    private double nutritionInfo2 = 0.0d;
    private double nutritionInfo3 = 0.0d;
    private double nutritionInfo4 = 0.0d;
    private double nutritionInfo5 = 0.0d;
    private double refundedQuantity = 0.0d;
    private double amount = 0.0d;
    private double refundedAmount = 0.0d;
    private BigDecimal paComboTransLineItemID = null;
    private int paComboDetailID = 0;
    private double comboPrice = 0.0d;
    private double basePrice = 0.0d;
    private int printStatusID = 0;
    private int kmsOrderStatusID = 0;
    private PrepOption prepOption = null;

    // parent product if this product is a modifier
    private Product parentProduct = null;

    // modifiers for this product if this product is a parent product
    private ArrayList<Product> modifiers = new ArrayList<>();

    // loyalty rewards for this product
    private ArrayList<LoyaltyRewardData> loyaltyRewards = new ArrayList<>();

    // item discount for this product
    private Discount itemDiscount = null;

    // free product rewards for this product
    private ArrayList<LoyaltyRewardData> freeProductRewards = new ArrayList<>();

    // item surcharges for this product
    private ArrayList<Surcharge> itemSurcharges = new ArrayList<>();

    // tax discount String for this product
    private String taxDiscountStr = "";

    // any KP/KDS/KMS preps mapped to the product
    private ArrayList<IKitchenPrep> kitchenPreps = new ArrayList<>();

    /**
     * <p>Sets the data for a {@link Product} transaction line item.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param lineItemRec The {@link HashMap} containing information about the transaction line item.
     */
    @SuppressWarnings("ConstantConditions")
    @Override
    public void setLineItemData (String log, DataManager dataManager, HashMap lineItemRec) {

        if (!DataFunctions.isEmptyMap(lineItemRec)) {
            this.productID = HashMapDataFns.getIntVal(lineItemRec, "PAITEMID");
            String paTransLineItemIDStr = HashMapDataFns.getStringVal(lineItemRec, "PATRANSLINEITEMID");
            if ((StringFunctions.stringHasContent(paTransLineItemIDStr)) && (NumberUtils.isNumber(paTransLineItemIDStr))) {
                this.paTransLineItemID = new BigDecimal(paTransLineItemIDStr);
            }
            String paTransLineItemModIDStr = HashMapDataFns.getStringVal(lineItemRec, "PATRANSLINEITEMMODID");
            if ((StringFunctions.stringHasContent(paTransLineItemModIDStr)) && (NumberUtils.isNumber(paTransLineItemModIDStr))) {
                this.paTransLineItemModID = new BigDecimal(paTransLineItemModIDStr);
            }
            this.name = HashMapDataFns.getStringVal(lineItemRec, "ITEMNAME");
            this.productCode = HashMapDataFns.getStringVal(lineItemRec, "PLUCODE");
            this.originalOrderNumber = HashMapDataFns.getStringVal(lineItemRec, "ORIGINALORDERNUM");
            this.isModifier = HashMapDataFns.getBooleanVal(lineItemRec, "ISMODIFIER");
            this.isWeightedItem = HashMapDataFns.getBooleanVal(lineItemRec, "SCALEUSED");
            this.isDiningOption = HashMapDataFns.getBooleanVal(lineItemRec, "ISDININGOPTION");
            this.printsOnExpeditor = HashMapDataFns.getBooleanVal(lineItemRec, "PRINTSONEXPEDITOR");
            String manualWeightCheck = HashMapDataFns.getStringVal(lineItemRec, "ITEMCOMMENT");
            if ((StringFunctions.stringHasContent(manualWeightCheck)) && (manualWeightCheck.equalsIgnoreCase("MAN WT"))) {
                this.isManualWeightedItem = true;
            }
            if (this.isWeightedItem) {
                int transTypeID = HashMapDataFns.getIntVal(lineItemRec, "TRANSTYPEID");
                if ((transTypeID == TypeData.TranType.REFUND) || (transTypeID == TypeData.TranType.VOID)) {
                    this.quantity = -1.0d;
                }
                this.netWeight = Math.abs(HashMapDataFns.getDoubleVal(lineItemRec, "QUANTITY"));
            }
            else {
                this.quantity = HashMapDataFns.getDoubleVal(lineItemRec, "QUANTITY");
            }
            this.grossWeight = new BigDecimal(Math.abs(HashMapDataFns.getDoubleVal(lineItemRec, "GROSSWEIGHT")));
            this.fixedNetWeight = new BigDecimal(Math.abs(HashMapDataFns.getDoubleVal(lineItemRec, "FIXEDNETWEIGHT")));
            this.nutritionInfo1 = HashMapDataFns.getDoubleVal(lineItemRec, "NUTRITIONINFO1");
            this.nutritionInfo2 = HashMapDataFns.getDoubleVal(lineItemRec, "NUTRITIONINFO2");
            this.nutritionInfo3 = HashMapDataFns.getDoubleVal(lineItemRec, "NUTRITIONINFO3");
            this.nutritionInfo4 = HashMapDataFns.getDoubleVal(lineItemRec, "NUTRITIONINFO4");
            this.nutritionInfo5 = HashMapDataFns.getDoubleVal(lineItemRec, "NUTRITIONINFO5");
            this.refundedQuantity = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDQUANTITY");
            this.amount = HashMapDataFns.getDoubleVal(lineItemRec, "AMOUNT");
            this.refundedAmount = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDAMOUNT");
            String paComboTransLineItemIDStr = HashMapDataFns.getStringVal(lineItemRec, "PACOMBOTRANSLINEITEMID");
            if ((StringFunctions.stringHasContent(paComboTransLineItemIDStr)) && (NumberUtils.isNumber(paComboTransLineItemIDStr))) {
                this.paComboTransLineItemID = new BigDecimal(paComboTransLineItemIDStr);
            }
            this.paComboDetailID = HashMapDataFns.getIntVal(lineItemRec, "PACOMBODETAILID");
            this.comboPrice = HashMapDataFns.getDoubleVal(lineItemRec, "COMBOPRICE");
            this.basePrice = HashMapDataFns.getDoubleVal(lineItemRec, "BASEPRICE");
            this.printStatusID = HashMapDataFns.getIntVal(lineItemRec, "PRINTSTATUSID");
            this.kmsOrderStatusID = HashMapDataFns.getIntVal(lineItemRec, "LINEITEMKMSORDERSTATUSID");
            this.prepOption = new PrepOption(lineItemRec);
            if ((this.prepOption != null) && (this.prepOption.getPrepOptionPrice() != 0.0d)) {
                this.amount = this.amount - this.prepOption.getPrepOptionPrice();
            }
        }
        else {
            Logger.logMessage("The Product record can't be acquired from an empty HashMap in Product.setLineItemData", log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Overridden toString() method for a {@link Product}.</p>
     *
     * @return A {@link String} representation of this {@link Product}.
     */
    @SuppressWarnings("Convert2streamapi")
    @Override
    public String toString () {

        // build the modifiers String
        String modifiersStr = StringFunctions.collectionToBracketedString(modifiers);

        // build the loyalty rewards String
        String loyaltyRewardsStr = StringFunctions.collectionToBracketedString(loyaltyRewards);

        // build the free product rewards String
        String freeProductRewardsStr = StringFunctions.collectionToBracketedString(freeProductRewards);

        // build the item surcharges String
        String itemSurchargesStr = StringFunctions.collectionToBracketedString(itemSurcharges);

        // build the kitchen preps String
        String kitchenPrepsStr = StringFunctions.collectionToBracketedString(kitchenPreps);

        return String.format("PRODUCTID: %s, PATRANSLINEITEMID: %s, PATRANSLINEITEMMODID: %s, NAME: %s, PRODUCTCODE: %s, ORIGINALORDERNUMBER: %s, ISMODIFIER: %s, " +
                "ISWEIGHTEDITEM: %s, ISMANUALWEIGHTEDITEM: %s, ISDININGOPTION: %s, PRINTSONEXPEDITOR: %s, QUANTITY: %s, NETWEIGHT: %s, GROSSWEIGHT: %s, " +
                "FIXEDNETWEIGHT: %s, NUTRITIONINFO1: %s, NUTRITIONINFO2: %s, NUTRITIONINFO3: %s, NUTRITIONINFO4: %s, NUTRITIONINFO5: %s, REFUNDEDQUANTITY: %s, " +
                "AMOUNT: %s, REFUNDEDAMOUNT: %s, PACOMBOTRANSLINEITEMID: %s, PACOMBODETAILID: %s, COMBOPRICE: %s, BASEPRICE: %s, PRINTSTATUSID: %s, " +
                "KMSORDERSTATUSID: %s, PREPOPTION: %s, PARENTPRODUCT: %s, MODIFIERS: %s, LOYALTYREWARDS: %s, ITEMDISCOUNT: %s, FREEPRODUCTREWARDS: %s, " +
                "ITEMSURCHARGES: %s, TAXDISCOUNTSTR: %s, KITCHENPREPS: %s",
                Objects.toString((productID != -1 ? productID : "N/A"), "N/A"),
                Objects.toString((((paTransLineItemID != null) && (StringFunctions.stringHasContent(paTransLineItemID.toPlainString()))) ? paTransLineItemID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((((paTransLineItemModID != null) && (StringFunctions.stringHasContent(paTransLineItemModID.toPlainString()))) ? paTransLineItemModID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(productCode) ? productCode : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(originalOrderNumber) ? originalOrderNumber : "N/A"), "N/A"),
                Objects.toString(isModifier, "N/A"),
                Objects.toString(isWeightedItem, "N/A"),
                Objects.toString(isManualWeightedItem, "N/A"),
                Objects.toString(isDiningOption, "N/A"),
                Objects.toString(printsOnExpeditor, "N/A"),
                Objects.toString((quantity != -1.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((netWeight != -1.0d ? netWeight : "N/A"), "N/A"),
                Objects.toString((((grossWeight != null) && (StringFunctions.stringHasContent(grossWeight.toPlainString()))) ? grossWeight.toPlainString() : "N/A"), "N/A"),
                Objects.toString((((fixedNetWeight != null) && (StringFunctions.stringHasContent(fixedNetWeight.toPlainString()))) ? fixedNetWeight.toPlainString() : "N/A"), "N/A"),
                Objects.toString((nutritionInfo1 != -1.0d ? nutritionInfo1 : "N/A"), "N/A"),
                Objects.toString((nutritionInfo2 != -1.0d ? nutritionInfo2 : "N/A"), "N/A"),
                Objects.toString((nutritionInfo3 != -1.0d ? nutritionInfo3 : "N/A"), "N/A"),
                Objects.toString((nutritionInfo4 != -1.0d ? nutritionInfo4 : "N/A"), "N/A"),
                Objects.toString((nutritionInfo5 != -1.0d ? nutritionInfo5 : "N/A"), "N/A"),
                Objects.toString((refundedQuantity != -1.0d ? refundedQuantity : "N/A"), "N/A"),
                Objects.toString((amount != -1.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount != -1.0d ? refundedAmount : "N/A"), "N/A"),
                Objects.toString((((paComboTransLineItemID != null) && (StringFunctions.stringHasContent(paComboTransLineItemID.toPlainString()))) ? paComboTransLineItemID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((paComboDetailID != -1 ? paComboDetailID : "N/A"), "N/A"),
                Objects.toString((comboPrice != -1.0d ? comboPrice : "N/A"), "N/A"),
                Objects.toString((basePrice != -1.0d ? basePrice : "N/A"), "N/A"),
                Objects.toString((printStatusID != -1 ? printStatusID : "N/A"), "N/A"),
                Objects.toString((kmsOrderStatusID != -1 ? kmsOrderStatusID : "N/A"), "N/A"),
                Objects.toString((prepOption != null ? prepOption.toString() : "N/A"), "N/A"),
                Objects.toString((parentProduct != null ? "["+parentProduct.toString()+"]" : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(modifiersStr) ? modifiersStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(loyaltyRewardsStr) ? loyaltyRewardsStr : "N/A"), "N/A"),
                Objects.toString((itemDiscount != null ? itemDiscount.toString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(freeProductRewardsStr) ? freeProductRewardsStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(itemSurchargesStr) ? itemSurchargesStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(taxDiscountStr) ? taxDiscountStr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(kitchenPrepsStr) ? kitchenPrepsStr : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link Product}.
     * Two {@link Product} are defined as being equal if they have the same productID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link Product}.
     * @return Whether or not the {@link Object} is equal to this {@link Product}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a Product and check if the obj's productID is equal to this Product's productID
        Product product = ((Product) obj);
        return Objects.equals(product.productID, productID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link Product}.</p>
     *
     * @return The unique hash code for this {@link Product}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(productID);
    }

    /**
     * <p>Utility method to add a modifier to this product's {@link ArrayList} of {@link Product} corresponding to the modifiers.</p>
     *
     * @param modifier The modifier {@link Product} to add.
     */
    public void addModifier (Product modifier) {
        if (modifier != null) {
            modifiers.add(modifier);
        }
    }

    /**
     * <p>Utility method to add a loyalty reward to this product's {@link ArrayList} of {@link LoyaltyRewardData} corresponding to the loyalty rewards.</p>
     *
     * @param loyaltyReward The {@link LoyaltyRewardData} to add.
     */
    public void addLoyaltyReward (LoyaltyRewardData loyaltyReward) {
        if (loyaltyReward != null) {
            loyaltyRewards.add(loyaltyReward);
        }
    }

    /**
     * <p>Utility method to add a free product reward to this product's {@link ArrayList} of {@link LoyaltyRewardData} corresponding to the free product rewards.</p>
     *
     * @param loyaltyReward The {@link LoyaltyRewardData} to add.
     */
    public void addFreeProductReward (LoyaltyRewardData loyaltyReward) {
        if (loyaltyReward != null) {
            freeProductRewards.add(loyaltyReward);
        }
    }

    /**
     * <p>Utility method to add an item surcharge to this product's {@link ArrayList} of {@link Surcharge} corresponding to the item surcharges.</p>
     *
     * @param itemSurcharge The item {@link Surcharge} to add.
     */
    public void addItemSurcharge (Surcharge itemSurcharge) {
        if (itemSurcharge != null) {
            itemSurcharges.add(itemSurcharge);
        }
    }

    /**
     * <p>Builds a {@link String} of tax rate and discount codes applied to this product.</p>
     *
     * @param shortName A {@link String} of tax rate and discount codes applied to this product.
     */
    public void addToTaxDiscountStr (String shortName) {

        try {
            if (!StringFunctions.stringHasContent(taxDiscountStr)) {
                taxDiscountStr += "("+shortName;
            }
            else {
                // remove the last character of this String ")" which indicated where the String used to end
                taxDiscountStr = StringFunctions.removeLastChar(taxDiscountStr);
                // check if the tax rate or discount code has already been added
                boolean codeAdded = false;
                String tempTaxDiscountStr = taxDiscountStr.substring(1, taxDiscountStr.length());
                String[] existingCodes = tempTaxDiscountStr.split("\\s*,\\s*", -1);
                if (!DataFunctions.isEmptyGenericArr(existingCodes)) {
                    for (String code : existingCodes) {
                        if (code.equalsIgnoreCase(shortName)) {
                            codeAdded = true;
                            break;
                        }
                    }
                }

                // if the code hasn't been added yet add it now
                if (!codeAdded) {
                    taxDiscountStr += ","+shortName+")";
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying add to the taxDiscountStr for the product %s in Product.addToTaxDiscountStr",
                    Objects.toString(name, "N/A")), Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Utility method to add KP/KDS/KMS prep printer/station to this product's {@link ArrayList} of {@link IKitchenPrep} corresponding to the KP/KDS/KMS prep printers/stations.</p>
     *
     * @param kitchenPrep The item {@link IKitchenPrep} to add.
     */
    public void addKichenPrep (IKitchenPrep kitchenPrep) {
        if (kitchenPrep != null) {
            kitchenPreps.add(kitchenPrep);
        }
    }

    /**
     * <p>Converts this product's quantity into an int and returns it.</p>
     *
     * @return This product's quantity as an int.
     */
    public int getQuantityAsInt () {
        return new Double(this.quantity).intValue();
    }

    /**
     * <p>Returns the nutrition information for this product.</p>
     *
     * @return A {@link HashMap} whose key is the nutrition information line {@link String} and whose value is what is stored in the nutrition information line as a {@link String}.
     */
    public HashMap<String, String> getProductNutritionInfo () {
        HashMap<String, String> productNutritionInfo = new HashMap<>();
        productNutritionInfo.put("NUTRITIONINFO1", String.valueOf(this.nutritionInfo1));
        productNutritionInfo.put("NUTRITIONINFO2", String.valueOf(this.nutritionInfo2));
        productNutritionInfo.put("NUTRITIONINFO3", String.valueOf(this.nutritionInfo3));
        productNutritionInfo.put("NUTRITIONINFO4", String.valueOf(this.nutritionInfo4));
        productNutritionInfo.put("NUTRITIONINFO5", String.valueOf(this.nutritionInfo5));
        return productNutritionInfo;
    }

    /**
     * <p>Determines whether or not this product has been printed/displayed.</p>
     *
     * @return Whether or not this product has been printed/displayed.
     */
    public boolean hasBeenPrintedOrDisplayed () {
        return ((printStatusID == PrintJobStatusType.WAITING.getTypeID()) || (kmsOrderStatusID == KMSOrderStatus.WAITING));
    }

    /**
     * <p>Getter for the productID field of the {@link Product}.</p>
     *
     * @return The productID field of the {@link Product}.
     */
    public int getProductID () {
        return productID;
    }

    /**
     * <p>Setter for the productID field of the {@link Product}.</p>
     *
     * @param productID The productID field of the {@link Product}.
     */
    public void setProductID (int productID) {
        this.productID = productID;
    }

    /**
     * <p>Getter for the paTransLineItemID field of the {@link Product}.</p>
     *
     * @return The paTransLineItemID field of the {@link Product}.
     */
    public BigDecimal getPaTransLineItemID () {
        return paTransLineItemID;
    }

    /**
     * <p>Setter for the paTransLineItemID field of the {@link Product}.</p>
     *
     * @param paTransLineItemID The paTransLineItemID field of the {@link Product}.
     */
    public void setPaTransLineItemID (BigDecimal paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
    }

    /**
     * <p>Getter for the paTransLineItemModID field of the {@link Product}.</p>
     *
     * @return The paTransLineItemModID field of the {@link Product}.
     */
    public BigDecimal getPaTransLineItemModID () {
        return paTransLineItemModID;
    }

    /**
     * <p>Setter for the paTransLineItemModID field of the {@link Product}.</p>
     *
     * @param paTransLineItemModID The paTransLineItemModID field of the {@link Product}.
     */
    public void setPaTransLineItemModID (BigDecimal paTransLineItemModID) {
        this.paTransLineItemModID = paTransLineItemModID;
    }

    /**
     * <p>Getter for the name field of the {@link Product}.</p>
     *
     * @return The name field of the {@link Product}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link Product}.</p>
     *
     * @param name The name field of the {@link Product}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the productCode field of the {@link Product}.</p>
     *
     * @return The productCode field of the {@link Product}.
     */
    public String getProductCode () {
        return productCode;
    }

    /**
     * <p>Setter for the productCode field of the {@link Product}.</p>
     *
     * @param productCode The productCode field of the {@link Product}.
     */
    public void setProductCode (String productCode) {
        this.productCode = productCode;
    }

    /**
     * <p>Getter for the originalOrderNumber field of the {@link Product}.</p>
     *
     * @return The originalOrderNumber field of the {@link Product}.
     */
    public String getOriginalOrderNumber () {
        return originalOrderNumber;
    }

    /**
     * <p>Setter for the originalOrderNumber field of the {@link Product}.</p>
     *
     * @param originalOrderNumber The originalOrderNumber field of the {@link Product}.
     */
    public void setOriginalOrderNumber (String originalOrderNumber) {
        this.originalOrderNumber = originalOrderNumber;
    }

    /**
     * <p>Getter for the isModifier field of the {@link Product}.</p>
     *
     * @return The isModifier field of the {@link Product}.
     */
    public boolean getIsModifier () {
        return isModifier;
    }

    /**
     * <p>Setter for the isModifier field of the {@link Product}.</p>
     *
     * @param isModifier The isModifier field of the {@link Product}.
     */
    public void setIsModifier (boolean isModifier) {
        this.isModifier = isModifier;
    }

    /**
     * <p>Getter for the isWeightedItem field of the {@link Product}.</p>
     *
     * @return The isWeightedItem field of the {@link Product}.
     */
    public boolean getIsWeightedItem () {
        return isWeightedItem;
    }

    /**
     * <p>Setter for the isWeightedItem field of the {@link Product}.</p>
     *
     * @param isWeightedItem The isWeightedItem field of the {@link Product}.
     */
    public void setIsWeightedItem (boolean isWeightedItem) {
        this.isWeightedItem = isWeightedItem;
    }

    /**
     * <p>Getter for the isManualWeightedItem field of the {@link Product}.</p>
     *
     * @return The isManualWeightedItem field of the {@link Product}.
     */
    public boolean getIsManualWeightedItem () {
        return isManualWeightedItem;
    }

    /**
     * <p>Setter for the isManualWeightedItem field of the {@link Product}.</p>
     *
     * @param isManualWeightedItem The isManualWeightedItem field of the {@link Product}.
     */
    public void setIsManualWeightedItem (boolean isManualWeightedItem) {
        this.isManualWeightedItem = isManualWeightedItem;
    }

    /**
     * <p>Getter for the isDiningOption field of the {@link Product}.</p>
     *
     * @return The isDiningOption field of the {@link Product}.
     */
    public boolean getIsDiningOption () {
        return isDiningOption;
    }

    /**
     * <p>Setter for the isDiningOption field of the {@link Product}.</p>
     *
     * @param isDiningOption The isDiningOption field of the {@link Product}.
     */
    public void setIsDiningOption (boolean isDiningOption) {
        this.isDiningOption = isDiningOption;
    }

    /**
     * <p>Getter for the printsOnExpeditor field of the {@link Product}.</p>
     *
     * @return The printsOnExpeditor field of the {@link Product}.
     */
    public boolean getPrintsOnExpeditor () {
        return printsOnExpeditor;
    }

    /**
     * <p>Setter for the printsOnExpeditor field of the {@link Product}.</p>
     *
     * @param printsOnExpeditor The printsOnExpeditor field of the {@link Product}.
     */
    public void setPrintsOnExpeditor (boolean printsOnExpeditor) {
        this.printsOnExpeditor = printsOnExpeditor;
    }

    /**
     * <p>Getter for the quantity field of the {@link Product}.</p>
     *
     * @return The quantity field of the {@link Product}.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link Product}.</p>
     *
     * @param quantity The quantity field of the {@link Product}.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the netWeight field of the {@link Product}.</p>
     *
     * @return The netWeight field of the {@link Product}.
     */
    public double getNetWeight () {
        return netWeight;
    }

    /**
     * <p>Setter for the netWeight field of the {@link Product}.</p>
     *
     * @param netWeight The netWeight field of the {@link Product}.
     */
    public void setNetWeight (double netWeight) {
        this.netWeight = netWeight;
    }

    /**
     * <p>Getter for the grossWeight field of the {@link Product}.</p>
     *
     * @return The grossWeight field of the {@link Product}.
     */
    public BigDecimal getGrossWeight () {
        return grossWeight;
    }

    /**
     * <p>Setter for the grossWeight field of the {@link Product}.</p>
     *
     * @param grossWeight The grossWeight field of the {@link Product}.
     */
    public void setGrossWeight (BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    /**
     * <p>Getter for the fixedNetWeight field of the {@link Product}.</p>
     *
     * @return The fixedNetWeight field of the {@link Product}.
     */
    public BigDecimal getFixedNetWeight () {
        return fixedNetWeight;
    }

    /**
     * <p>Setter for the fixedNetWeight field of the {@link Product}.</p>
     *
     * @param fixedNetWeight The fixedNetWeight field of the {@link Product}.
     */
    public void setFixedNetWeight (BigDecimal fixedNetWeight) {
        this.fixedNetWeight = fixedNetWeight;
    }

    /**
     * <p>Getter for the nutritionInfo1 field of the {@link Product}.</p>
     *
     * @return The nutritionInfo1 field of the {@link Product}.
     */
    public double getNutritionInfo1 () {
        return nutritionInfo1;
    }

    /**
     * <p>Setter for the nutritionInfo1 field of the {@link Product}.</p>
     *
     * @param nutritionInfo1 The nutritionInfo1 field of the {@link Product}.
     */
    public void setNutritionInfo1 (double nutritionInfo1) {
        this.nutritionInfo1 = nutritionInfo1;
    }

    /**
     * <p>Getter for the nutritionInfo2 field of the {@link Product}.</p>
     *
     * @return The nutritionInfo2 field of the {@link Product}.
     */
    public double getNutritionInfo2 () {
        return nutritionInfo2;
    }

    /**
     * <p>Setter for the nutritionInfo2 field of the {@link Product}.</p>
     *
     * @param nutritionInfo2 The nutritionInfo2 field of the {@link Product}.
     */
    public void setNutritionInfo2 (double nutritionInfo2) {
        this.nutritionInfo2 = nutritionInfo2;
    }

    /**
     * <p>Getter for the nutritionInfo3 field of the {@link Product}.</p>
     *
     * @return The nutritionInfo3 field of the {@link Product}.
     */
    public double getNutritionInfo3 () {
        return nutritionInfo3;
    }

    /**
     * <p>Setter for the nutritionInfo3 field of the {@link Product}.</p>
     *
     * @param nutritionInfo3 The nutritionInfo3 field of the {@link Product}.
     */
    public void setNutritionInfo3 (double nutritionInfo3) {
        this.nutritionInfo3 = nutritionInfo3;
    }

    /**
     * <p>Getter for the nutritionInfo4 field of the {@link Product}.</p>
     *
     * @return The nutritionInfo4 field of the {@link Product}.
     */
    public double getNutritionInfo4 () {
        return nutritionInfo4;
    }

    /**
     * <p>Setter for the nutritionInfo4 field of the {@link Product}.</p>
     *
     * @param nutritionInfo4 The nutritionInfo4 field of the {@link Product}.
     */
    public void setNutritionInfo4 (double nutritionInfo4) {
        this.nutritionInfo4 = nutritionInfo4;
    }

    /**
     * <p>Getter for the nutritionInfo5 field of the {@link Product}.</p>
     *
     * @return The nutritionInfo5 field of the {@link Product}.
     */
    public double getNutritionInfo5 () {
        return nutritionInfo5;
    }

    /**
     * <p>Setter for the nutritionInfo5 field of the {@link Product}.</p>
     *
     * @param nutritionInfo5 The nutritionInfo5 field of the {@link Product}.
     */
    public void setNutritionInfo5 (double nutritionInfo5) {
        this.nutritionInfo5 = nutritionInfo5;
    }

    /**
     * <p>Getter for the refundedQuantity field of the {@link Product}.</p>
     *
     * @return The refundedQuantity field of the {@link Product}.
     */
    public double getRefundedQuantity () {
        return refundedQuantity;
    }

    /**
     * <p>Setter for the refundedQuantity field of the {@link Product}.</p>
     *
     * @param refundedQuantity The refundedQuantity field of the {@link Product}.
     */
    public void setRefundedQuantity (double refundedQuantity) {
        this.refundedQuantity = refundedQuantity;
    }

    /**
     * <p>Getter for the amount field of the {@link Product}.</p>
     *
     * @return The amount field of the {@link Product}.
     */
    public double getAmount () {
        return amount;
    }

    /**
     * <p>Setter for the amount field of the {@link Product}.</p>
     *
     * @param amount The amount field of the {@link Product}.
     */
    public void setAmount (double amount) {
        this.amount = amount;
    }

    /**
     * <p>Getter for the refundedAmount field of the {@link Product}.</p>
     *
     * @return The refundedAmount field of the {@link Product}.
     */
    public double getRefundedAmount () {
        return refundedAmount;
    }

    /**
     * <p>Setter for the refundedAmount field of the {@link Product}.</p>
     *
     * @param refundedAmount The refundedAmount field of the {@link Product}.
     */
    public void setRefundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    /**
     * <p>Getter for the paComboTransLineItemID field of the {@link Product}.</p>
     *
     * @return The paComboTransLineItemID field of the {@link Product}.
     */
    public BigDecimal getPaComboTransLineItemID () {
        return paComboTransLineItemID;
    }

    /**
     * <p>Setter for the paComboTransLineItemID field of the {@link Product}.</p>
     *
     * @param paComboTransLineItemID The paComboTransLineItemID field of the {@link Product}.
     */
    public void setPaComboTransLineItemID (BigDecimal paComboTransLineItemID) {
        this.paComboTransLineItemID = paComboTransLineItemID;
    }

    /**
     * <p>Getter for the paComboDetailID field of the {@link Product}.</p>
     *
     * @return The paComboDetailID field of the {@link Product}.
     */
    public int getPaComboDetailID () {
        return paComboDetailID;
    }

    /**
     * <p>Setter for the paComboDetailID field of the {@link Product}.</p>
     *
     * @param paComboDetailID The paComboDetailID field of the {@link Product}.
     */
    public void setPaComboDetailID (int paComboDetailID) {
        this.paComboDetailID = paComboDetailID;
    }

    /**
     * <p>Getter for the comboPrice field of the {@link Product}.</p>
     *
     * @return The comboPrice field of the {@link Product}.
     */
    public double getComboPrice () {
        return comboPrice;
    }

    /**
     * <p>Setter for the comboPrice field of the {@link Product}.</p>
     *
     * @param comboPrice The comboPrice field of the {@link Product}.
     */
    public void setComboPrice (double comboPrice) {
        this.comboPrice = comboPrice;
    }

    /**
     * <p>Getter for the basePrice field of the {@link Product}.</p>
     *
     * @return The basePrice field of the {@link Product}.
     */
    public double getBasePrice () {
        return basePrice;
    }

    /**
     * <p>Setter for the basePrice field of the {@link Product}.</p>
     *
     * @param basePrice The basePrice field of the {@link Product}.
     */
    public void setBasePrice (double basePrice) {
        this.basePrice = basePrice;
    }

    /**
     * <p>Getter for the printStatusID field of the {@link Product}.</p>
     *
     * @return The printStatusID field of the {@link Product}.
     */
    public int getPrintStatusID () {
        return printStatusID;
    }

    /**
     * <p>Setter for the printStatusID field of the {@link Product}.</p>
     *
     * @param printStatusID The printStatusID field of the {@link Product}.
     */
    public void setPrintStatusID (int printStatusID) {
        this.printStatusID = printStatusID;
    }

    /**
     * <p>Getter for the kmsOrderStatusID field of the {@link Product}.</p>
     *
     * @return The kmsOrderStatusID field of the {@link Product}.
     */
    public int getKmsOrderStatusID () {
        return kmsOrderStatusID;
    }

    /**
     * <p>Setter for the kmsOrderStatusID field of the {@link Product}.</p>
     *
     * @param kmsOrderStatusID The kmsOrderStatusID field of the {@link Product}.
     */
    public void setKmsOrderStatusID (int kmsOrderStatusID) {
        this.kmsOrderStatusID = kmsOrderStatusID;
    }

    /**
     * <p>Getter for the prepOption field of the {@link Product}.</p>
     *
     * @return The prepOption field of the {@link Product}.
     */
    public PrepOption getPrepOption () {
        return prepOption;
    }

    /**
     * <p>Setter for the prepOption field of the {@link Product}.</p>
     *
     * @param prepOption The prepOption field of the {@link Product}.
     */
    public void setPrepOption (PrepOption prepOption) {
        this.prepOption = prepOption;
    }

    /**
     * <p>Getter for the parentProduct field of the {@link Product}.</p>
     *
     * @return The parentProduct field of the {@link Product}.
     */
    public Product getParentProduct () {
        return parentProduct;
    }

    /**
     * <p>Setter for the parentProduct field of the {@link Product}.</p>
     *
     * @param parentProduct The parentProduct field of the {@link Product}.
     */
    public void setParentProduct (Product parentProduct) {
        this.parentProduct = parentProduct;
    }

    /**
     * <p>Getter for the modifiers field of the {@link Product}.</p>
     *
     * @return The modifiers field of the {@link Product}.
     */
    public ArrayList<Product> getModifiers () {
        return modifiers;
    }

    /**
     * <p>Setter for the modifiers field of the {@link Product}.</p>
     *
     * @param modifiers The modifiers field of the {@link Product}.
     */
    public void setModifiers (ArrayList<Product> modifiers) {
        this.modifiers = modifiers;
    }

    /**
     * <p>Getter for the loyaltyRewards field of the {@link Product}.</p>
     *
     * @return The loyaltyRewards field of the {@link Product}.
     */
    public ArrayList<LoyaltyRewardData> getLoyaltyRewards () {
        return loyaltyRewards;
    }

    /**
     * <p>Setter for the loyaltyRewards field of the {@link Product}.</p>
     *
     * @param loyaltyRewards The loyaltyRewards field of the {@link Product}.
     */
    public void setLoyaltyRewards (ArrayList<LoyaltyRewardData> loyaltyRewards) {
        this.loyaltyRewards = loyaltyRewards;
    }

    /**
     * <p>Getter for the itemDiscount field of the {@link Product}.</p>
     *
     * @return The itemDiscount field of the {@link Product}.
     */
    public Discount getItemDiscount () {
        return itemDiscount;
    }

    /**
     * <p>Setter for the itemDiscount field of the {@link Product}.</p>
     *
     * @param itemDiscount The itemDiscount field of the {@link Product}.
     */
    public void setItemDiscount (Discount itemDiscount) {
        this.itemDiscount = itemDiscount;
    }

    /**
     * <p>Getter for the freeProductRewards field of the {@link Product}.</p>
     *
     * @return The freeProductRewards field of the {@link Product}.
     */
    public ArrayList<LoyaltyRewardData> getFreeProductRewards () {
        return freeProductRewards;
    }

    /**
     * <p>Setter for the freeProductRewards field of the {@link Product}.</p>
     *
     * @param freeProductRewards The freeProductRewards field of the {@link Product}.
     */
    public void setFreeProductRewards (ArrayList<LoyaltyRewardData> freeProductRewards) {
        this.freeProductRewards = freeProductRewards;
    }

    /**
     * <p>Getter for the itemSurcharges field of the {@link Product}.</p>
     *
     * @return The itemSurcharges field of the {@link Product}.
     */
    public ArrayList<Surcharge> getItemSurcharges () {
        return itemSurcharges;
    }

    /**
     * <p>Setter for the itemSurcharges field of the {@link Product}.</p>
     *
     * @param itemSurcharges The itemSurcharges field of the {@link Product}.
     */
    public void setItemSurcharges (ArrayList<Surcharge> itemSurcharges) {
        this.itemSurcharges = itemSurcharges;
    }

    /**
     * <p>Getter for the taxDiscountStr field of the {@link Product}.</p>
     *
     * @return The taxDiscountStr field of the {@link Product}.
     */
    public String getTaxDiscountStr () {
        return taxDiscountStr;
    }

    /**
     * <p>Setter for the taxDiscountStr field of the {@link Product}.</p>
     *
     * @param taxDiscountStr The taxDiscountStr field of the {@link Product}.
     */
    public void setTaxDiscountStr (String taxDiscountStr) {
        this.taxDiscountStr = taxDiscountStr;
    }

    /**
     * <p>Getter for the kitchenPreps field of the {@link Product}.</p>
     *
     * @return The kitchenPreps field of the {@link Product}.
     */
    public ArrayList<IKitchenPrep> getKitchenPreps () {
        return kitchenPreps;
    }

    /**
     * <p>Setter for the kitchenPreps field of the {@link Product}.</p>
     *
     * @param kitchenPreps The kitchenPreps field of the {@link Product}.
     */
    public void setKitchenPreps (ArrayList<IKitchenPrep> kitchenPreps) {
        this.kitchenPreps = kitchenPreps;
    }

}