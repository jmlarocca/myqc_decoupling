package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-18 15:39:12 -0400 (Mon, 18 May 2020) $: Date of last commit
    $Rev: 11775 $: Revision of last commit
    Notes: Represents a discount line item.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.GiftCardTxnData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a discount line item.</p>
 *
 */
public class Discount implements ITransLineItem {

    // private member variables of a Discount
    private int discountID = 0;
    private int discountTypeID = 0;
    private BigDecimal paTransLineItemID = null;
    private String name = "";
    private String shortName = "";
    private double quantity = 0.0d;
    private double refundedQuantity = 0.0d;
    private double amount = 0.0d;
    private double refundedAmount = 0.0d;
    private boolean isSubtotalDiscount = false;

    /**
     * <p>Sets the data for a {@link Discount} transaction line item.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param lineItemRec The {@link HashMap} containing information about the transaction line item.
     */
    @Override
    public void setLineItemData (String log, DataManager dataManager, HashMap lineItemRec) {

        if (!DataFunctions.isEmptyMap(lineItemRec)) {
            this.discountID = HashMapDataFns.getIntVal(lineItemRec, "PAITEMID");
            this.discountTypeID = HashMapDataFns.getIntVal(lineItemRec, "DISCOUNTTYPEID");
            String paTransLineItemIDStr = HashMapDataFns.getStringVal(lineItemRec, "PATRANSLINEITEMID");
            if ((StringFunctions.stringHasContent(paTransLineItemIDStr)) && (NumberUtils.isNumber(paTransLineItemIDStr))) {
                this.paTransLineItemID = new BigDecimal(paTransLineItemIDStr);
            }
            this.name = HashMapDataFns.getStringVal(lineItemRec, "ITEMNAME");
            this.shortName = HashMapDataFns.getStringVal(lineItemRec, "SHORTNAME");
            this.quantity = HashMapDataFns.getDoubleVal(lineItemRec, "QUANTITY");
            this.refundedQuantity = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDQUANTITY");
            this.amount = HashMapDataFns.getDoubleVal(lineItemRec, "AMOUNT");
            this.refundedAmount = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDAMOUNT");
            this.isSubtotalDiscount = HashMapDataFns.getBooleanVal(lineItemRec, "SUBTOTALDISCOUNT");
        }
        else {
            Logger.logMessage("The Discount record can't be acquired from an empty HashMap in Discount.setLineItemData", log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Overridden toString() method for a {@link Discount}.</p>
     *
     * @return A {@link String} representation of this {@link Discount}.
     */
    @Override
    public String toString () {

        return String.format("DISCOUNTID: %s, DISCOUNTTYPEID: %s, PATRANSLINEITEMID: %s, NAME: %s, SHORTNAME: %s, QUANTITY: %s, " +
                "REFUNDEDQUANTITY: %s, AMOUNT: %s, REFUNDEDAMOUNT: %s, ISSUBTOTALDISCOUNT: %s",
                Objects.toString((discountID != -1 ? discountID : "N/A"), "N/A"),
                Objects.toString((discountTypeID != -1 ? discountTypeID : "N/A"), "N/A"),
                Objects.toString((((paTransLineItemID != null) && (StringFunctions.stringHasContent(paTransLineItemID.toPlainString()))) ? paTransLineItemID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((quantity != -1.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQuantity != -1.0d ? refundedQuantity : "N/A"), "N/A"),
                Objects.toString((amount != -1.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount != -1.0d ? refundedAmount : "N/A"), "N/A"),
                Objects.toString(isSubtotalDiscount, "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link Discount}.
     * Two {@link Discount} are defined as being equal if they have the same discountID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link Discount}.
     * @return Whether or not the {@link Object} is equal to this {@link Discount}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a Discount and check if the obj's discountID is equal to this Discount's discountID
        Discount discount = ((Discount) obj);
        return Objects.equals(discount.discountID, discountID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link Discount}.</p>
     *
     * @return The unique hash code for this {@link Discount}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(discountID);
    }

    /**
     * <p>Gets the extended amount.</p>
     *
     * @return The extended amount.
     */
    public double getExtendedAmount () {
        return quantity * amount;
    }

    /**
     * <p>Getter for the discountID field of the {@link Discount}.</p>
     *
     * @return The discountID field of the {@link Discount}.
     */
    public int getDiscountID () {
        return discountID;
    }

    /**
     * <p>Setter for the discountID field of the {@link Discount}.</p>
     *
     * @param discountID The discountID field of the {@link Discount}.
     */
    public void setDiscountID (int discountID) {
        this.discountID = discountID;
    }

    /**
     * <p>Getter for the discountTypeID field of the {@link Discount}.</p>
     *
     * @return The discountTypeID field of the {@link Discount}.
     */
    public int getDiscountTypeID () {
        return discountTypeID;
    }

    /**
     * <p>Setter for the discountTypeID field of the {@link Discount}.</p>
     *
     * @param discountTypeID The discountTypeID field of the {@link Discount}.
     */
    public void setDiscountTypeID (int discountTypeID) {
        this.discountTypeID = discountTypeID;
    }

    /**
     * <p>Getter for the paTransLineItemID field of the {@link Discount}.</p>
     *
     * @return The paTransLineItemID field of the {@link Discount}.
     */
    public BigDecimal getPaTransLineItemID () {
        return paTransLineItemID;
    }

    /**
     * <p>Setter for the paTransLineItemID field of the {@link Discount}.</p>
     *
     * @param paTransLineItemID The paTransLineItemID field of the {@link Discount}.
     */
    public void setPaTransLineItemID (BigDecimal paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
    }

    /**
     * <p>Getter for the name field of the {@link Discount}.</p>
     *
     * @return The name field of the {@link Discount}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link Discount}.</p>
     *
     * @param name The name field of the {@link Discount}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the shortName field of the {@link Discount}.</p>
     *
     * @return The shortName field of the {@link Discount}.
     */
    public String getShortName () {
        return shortName;
    }

    /**
     * <p>Setter for the shortName field of the {@link Discount}.</p>
     *
     * @param shortName The shortName field of the {@link Discount}.
     */
    public void setShortName (String shortName) {
        this.shortName = shortName;
    }

    /**
     * <p>Getter for the quantity field of the {@link Discount}.</p>
     *
     * @return The quantity field of the {@link Discount}.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link Discount}.</p>
     *
     * @param quantity The quantity field of the {@link Discount}.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the refundedQuantity field of the {@link Discount}.</p>
     *
     * @return The refundedQuantity field of the {@link Discount}.
     */
    public double getRefundedQuantity () {
        return refundedQuantity;
    }

    /**
     * <p>Setter for the refundedQuantity field of the {@link Discount}.</p>
     *
     * @param refundedQuantity The refundedQuantity field of the {@link Discount}.
     */
    public void setRefundedQuantity (double refundedQuantity) {
        this.refundedQuantity = refundedQuantity;
    }

    /**
     * <p>Getter for the amount field of the {@link Discount}.</p>
     *
     * @return The amount field of the {@link Discount}.
     */
    public double getAmount () {
        return amount;
    }

    /**
     * <p>Setter for the amount field of the {@link Discount}.</p>
     *
     * @param amount The amount field of the {@link Discount}.
     */
    public void setAmount (double amount) {
        this.amount = amount;
    }

    /**
     * <p>Getter for the refundedAmount field of the {@link Discount}.</p>
     *
     * @return The refundedAmount field of the {@link Discount}.
     */
    public double getRefundedAmount () {
        return refundedAmount;
    }

    /**
     * <p>Setter for the refundedAmount field of the {@link Discount}.</p>
     *
     * @param refundedAmount The refundedAmount field of the {@link Discount}.
     */
    public void setRefundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    /**
     * <p>Getter for the isSubtotalDiscount field of the {@link Discount}.</p>
     *
     * @return The isSubtotalDiscount field of the {@link Discount}.
     */
    public boolean getIsSubtotalDiscount () {
        return isSubtotalDiscount;
    }

    /**
     * <p>Setter for the isSubtotalDiscount field of the {@link Discount}.</p>
     *
     * @param isSubtotalDiscount The isSubtotalDiscount field of the {@link Discount}.
     */
    public void setIsSubtotalDiscount (boolean isSubtotalDiscount) {
        this.isSubtotalDiscount = isSubtotalDiscount;
    }

}