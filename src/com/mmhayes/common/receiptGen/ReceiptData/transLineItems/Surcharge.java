package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-18 10:29:09 -0400 (Mon, 18 May 2020) $: Date of last commit
    $Rev: 11768 $: Revision of last commit
    Notes: Represents a surcharge line item.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a surcharge line item.</p>
 *
 */
public class Surcharge implements ITransLineItem {

    // private member variables of a Surcharge
    private int surchargeID = 0;
    private int applicationTypeID = 0;
    private BigDecimal paTransLineItemID = null;
    private BigDecimal linkedPATransLineItemID = null;
    private String name = "";
    private String shortName = "";
    private double quantity = 0.0d;
    private double refundedQuantity = 0.0d;
    private double amount = 0.0d;
    private double refundedAmount = 0.0d;

    /**
     * <p>Sets the data for a {@link Surcharge} transaction line item.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param lineItemRec The {@link HashMap} containing information about the transaction line item.
     */
    @Override
    public void setLineItemData (String log, DataManager dataManager, HashMap lineItemRec) {

        if (!DataFunctions.isEmptyMap(lineItemRec)) {
            this.surchargeID = HashMapDataFns.getIntVal(lineItemRec, "PAITEMID");
            this.applicationTypeID = HashMapDataFns.getIntVal(lineItemRec, "PASURCHARGEAPPLICATIONTYPEID");
            String paTransLineItemIDStr = HashMapDataFns.getStringVal(lineItemRec, "PATRANSLINEITEMID");
            if ((StringFunctions.stringHasContent(paTransLineItemIDStr)) && (NumberUtils.isNumber(paTransLineItemIDStr))) {
                this.paTransLineItemID = new BigDecimal(paTransLineItemIDStr);
            }
            String linkedPATranslineItemIDStr = HashMapDataFns.getStringVal(lineItemRec, "LINKEDPATRANSLINEITEMID");
            if ((StringFunctions.stringHasContent(linkedPATranslineItemIDStr)) && (NumberUtils.isNumber(linkedPATranslineItemIDStr))) {
                this.linkedPATransLineItemID = new BigDecimal(linkedPATranslineItemIDStr);
            }
            this.name = HashMapDataFns.getStringVal(lineItemRec, "ITEMNAME");
            this.shortName = HashMapDataFns.getStringVal(lineItemRec, "SURCHARGESHORTNAME");
            this.quantity = HashMapDataFns.getDoubleVal(lineItemRec, "QUANTITY");
            this.refundedQuantity = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDQUANTITY");
            this.amount = HashMapDataFns.getDoubleVal(lineItemRec, "AMOUNT");
            this.refundedAmount = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDAMOUNT");
        }
        else {
            Logger.logMessage("The Surcharge record can't be acquired from an empty HashMap in Surcharge.setLineItemData", log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Overridden toString() method for a {@link Surcharge}.</p>
     *
     * @return A {@link String} representation of this {@link Surcharge}.
     */
    @Override
    public String toString () {

        return String.format("SURCHARGEID: %s, APPLICATIONTYPEID: %s, PATRANSLINEITEMID: %s, LINKEDPATRANSLINEITEMID: %s, NAME: %s, " +
                "SHORTNAME: %s, QUANTITY: %s, REFUNDEDQUANTITY: %s, AMOUNT: %s, REFUNDEDAMOUNT: %s",
                Objects.toString((surchargeID != -1 ? surchargeID : "N/A"), "N/A"),
                Objects.toString((applicationTypeID != -1 ? applicationTypeID : "N/A"), "N/A"),
                Objects.toString((((paTransLineItemID != null) && (StringFunctions.stringHasContent(paTransLineItemID.toPlainString()))) ? paTransLineItemID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((((linkedPATransLineItemID != null) && (StringFunctions.stringHasContent(linkedPATransLineItemID.toPlainString()))) ? linkedPATransLineItemID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((quantity != -1.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQuantity != -1.0d ? refundedQuantity : "N/A"), "N/A"),
                Objects.toString((amount != -1.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount != -1.0d ? refundedAmount : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link Surcharge}.
     * Two {@link Surcharge} are defined as being equal if they have the same surchargeID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link Surcharge}.
     * @return Whether or not the {@link Object} is equal to this {@link Surcharge}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a Surcharge and check if the obj's surchargeID is equal to this Surcharge's surchargeID
        Surcharge surcharge = ((Surcharge) obj);
        return Objects.equals(surcharge.surchargeID, surchargeID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link Surcharge}.</p>
     *
     * @return The unique hash code for this {@link Surcharge}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(surchargeID);
    }

    /**
     * <p>Determines whether or not this {@link Surcharge} is a subtotal surcharge.</p>
     *
     * @return Whether or not this {@link Surcharge} is a subtotal surcharge.
     */
    public boolean isSubtotalSurcharge () {
        return (applicationTypeID == TypeData.SurchargeApplicationType.SUBTOTAL);
    }

    /**
     * <p>Getter for the surchargeID field of the {@link Surcharge}.</p>
     *
     * @return The surchargeID field of the {@link Surcharge}.
     */
    public int getSurchargeID () {
        return surchargeID;
    }

    /**
     * <p>Setter for the surchargeID field of the {@link Surcharge}.</p>
     *
     * @param surchargeID The surchargeID field of the {@link Surcharge}.
     */
    public void setSurchargeID (int surchargeID) {
        this.surchargeID = surchargeID;
    }

    /**
     * <p>Getter for the applicationTypeID field of the {@link Surcharge}.</p>
     *
     * @return The applicationTypeID field of the {@link Surcharge}.
     */
    public int getApplicationTypeID () {
        return applicationTypeID;
    }

    /**
     * <p>Setter for the applicationTypeID field of the {@link Surcharge}.</p>
     *
     * @param applicationTypeID The applicationTypeID field of the {@link Surcharge}.
     */
    public void setApplicationTypeID (int applicationTypeID) {
        this.applicationTypeID = applicationTypeID;
    }

    /**
     * <p>Getter for the paTransLineItemID field of the {@link Surcharge}.</p>
     *
     * @return The paTransLineItemID field of the {@link Surcharge}.
     */
    public BigDecimal getPaTransLineItemID () {
        return paTransLineItemID;
    }

    /**
     * <p>Setter for the paTransLineItemID field of the {@link Surcharge}.</p>
     *
     * @param paTransLineItemID The paTransLineItemID field of the {@link Surcharge}.
     */
    public void setPaTransLineItemID (BigDecimal paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
    }

    /**
     * <p>Getter for the linkedPATransLineItemID field of the {@link Surcharge}.</p>
     *
     * @return The linkedPATransLineItemID field of the {@link Surcharge}.
     */
    public BigDecimal getLinkedPATransLineItemID () {
        return linkedPATransLineItemID;
    }

    /**
     * <p>Setter for the linkedPATransLineItemID field of the {@link Surcharge}.</p>
     *
     * @param linkedPATransLineItemID The linkedPATransLineItemID field of the {@link Surcharge}.
     */
    public void setLinkedPATransLineItemID (BigDecimal linkedPATransLineItemID) {
        this.linkedPATransLineItemID = linkedPATransLineItemID;
    }

    /**
     * <p>Getter for the name field of the {@link Surcharge}.</p>
     *
     * @return The name field of the {@link Surcharge}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link Surcharge}.</p>
     *
     * @param name The name field of the {@link Surcharge}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the shortName field of the {@link Surcharge}.</p>
     *
     * @return The shortName field of the {@link Surcharge}.
     */
    public String getShortName () {
        return shortName;
    }

    /**
     * <p>Setter for the shortName field of the {@link Surcharge}.</p>
     *
     * @param shortName The shortName field of the {@link Surcharge}.
     */
    public void setShortName (String shortName) {
        this.shortName = shortName;
    }

    /**
     * <p>Getter for the quantity field of the {@link Surcharge}.</p>
     *
     * @return The quantity field of the {@link Surcharge}.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link Surcharge}.</p>
     *
     * @param quantity The quantity field of the {@link Surcharge}.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the refundedQuantity field of the {@link Surcharge}.</p>
     *
     * @return The refundedQuantity field of the {@link Surcharge}.
     */
    public double getRefundedQuantity () {
        return refundedQuantity;
    }

    /**
     * <p>Setter for the refundedQuantity field of the {@link Surcharge}.</p>
     *
     * @param refundedQuantity The refundedQuantity field of the {@link Surcharge}.
     */
    public void setRefundedQuantity (double refundedQuantity) {
        this.refundedQuantity = refundedQuantity;
    }

    /**
     * <p>Getter for the amount field of the {@link Surcharge}.</p>
     *
     * @return The amount field of the {@link Surcharge}.
     */
    public double getAmount () {
        return amount;
    }

    /**
     * <p>Setter for the amount field of the {@link Surcharge}.</p>
     *
     * @param amount The amount field of the {@link Surcharge}.
     */
    public void setAmount (double amount) {
        this.amount = amount;
    }

    /**
     * <p>Getter for the refundedAmount field of the {@link Surcharge}.</p>
     *
     * @return The refundedAmount field of the {@link Surcharge}.
     */
    public double getRefundedAmount () {
        return refundedAmount;
    }

    /**
     * <p>Setter for the refundedAmount field of the {@link Surcharge}.</p>
     *
     * @param refundedAmount The refundedAmount field of the {@link Surcharge}.
     */
    public void setRefundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

}