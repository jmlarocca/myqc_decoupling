package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-14 16:03:47 -0400 (Thu, 14 May 2020) $: Date of last commit
    $Rev: 11744 $: Revision of last commit
    Notes: Contains the different types of line items that may be built.
*/

/**
 * <p>Contains the different types of line items that may be built.</p>
 *
 */
public class TransLineItemType {

    public static final int COMBO = 36;
    public static final int DISCOUNT = 7;
    public static final int LOYALTY_REWARD = 30;
    public static final int PAID_OUT = 10;
    public static final int PRODUCT = 2;
    public static final int RECEIVED_ON_ACCOUNT = 11;
    public static final int SURCHARGE = 32;
    public static final int TAX_RATE = 6;
    public static final int TENDER = 3;

}