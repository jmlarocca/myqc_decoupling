package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-18 10:29:09 -0400 (Mon, 18 May 2020) $: Date of last commit
    $Rev: 11768 $: Revision of last commit
    Notes: Represents a tax rate line item.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a tax rate line item.</p>
 *
 */
public class TaxRate implements ITransLineItem {

    // private member variables of a TaxRate
    private int taxRateID = 0;
    private BigDecimal paTransLineItemID = null;
    private String name = "";
    private String shortName = "";
    private double quantity = 0.0d;
    private double refundedQuantity = 0.0d;
    private double amount = 0.0d;
    private double refundedAmount = 0.0d;

    /**
     * <p>Sets the data for a {@link TaxRate} transaction line item.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param lineItemRec The {@link HashMap} containing information about the transaction line item.
     */
    @Override
    public void setLineItemData (String log, DataManager dataManager, HashMap lineItemRec) {

        if (!DataFunctions.isEmptyMap(lineItemRec)) {
            this.taxRateID = HashMapDataFns.getIntVal(lineItemRec, "PAITEMID");
            String paTransLineItemIDStr = HashMapDataFns.getStringVal(lineItemRec, "PATRANSLINEITEMID");
            if ((StringFunctions.stringHasContent(paTransLineItemIDStr)) && (NumberUtils.isNumber(paTransLineItemIDStr))) {
                this.paTransLineItemID = new BigDecimal(paTransLineItemIDStr);
            }
            this.name = HashMapDataFns.getStringVal(lineItemRec, "ITEMNAME");
            this.shortName = HashMapDataFns.getStringVal(lineItemRec, "TAXRATESHORTNAME");
            this.quantity = HashMapDataFns.getDoubleVal(lineItemRec, "QUANTITY");
            this.refundedQuantity = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDQUANTITY");
            this.amount = HashMapDataFns.getDoubleVal(lineItemRec, "AMOUNT");
            this.refundedAmount = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDAMOUNT");
        }
        else {
            Logger.logMessage("The TaxRate record can't be acquired from an empty HashMap in TaxRate.setLineItemData", log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Overridden toString() method for a {@link TaxRate}.</p>
     *
     * @return A {@link String} representation of this {@link TaxRate}.
     */
    @Override
    public String toString () {

        return String.format("TAXRATEID: %s, PATRANSLINEITEMID: %s, NAME: %s, SHORTNAME: %s, QUANTITY: %s, REFUNDEDQUANTITY: %s, AMOUNT: %s, REFUNDEDAMOUNT: %s",
                Objects.toString((taxRateID != -1 ? taxRateID : "N/A"), "N/A"),
                Objects.toString((((paTransLineItemID != null) && (StringFunctions.stringHasContent(paTransLineItemID.toPlainString()))) ? paTransLineItemID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((quantity != -1.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQuantity != -1.0d ? refundedQuantity : "N/A"), "N/A"),
                Objects.toString((amount != -1.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount != -1.0d ? refundedAmount : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link TaxRate}.
     * Two {@link TaxRate} are defined as being equal if they have the same taxRateID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link TaxRate}.
     * @return Whether or not the {@link Object} is equal to this {@link TaxRate}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a TaxRate and check if the obj's taxRateID is equal to this TaxRate's taxRateID
        TaxRate taxRate = ((TaxRate) obj);
        return Objects.equals(taxRate.taxRateID, taxRateID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link TaxRate}.</p>
     *
     * @return The unique hash code for this {@link TaxRate}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(taxRateID);
    }

    /**
     * <p>Gets the extended amount.</p>
     *
     * @return The extended amount.
     */
    public double getExtendedAmount () {
        return quantity * amount;
    }

    /**
     * <p>Getter for the taxRateID field of the {@link TaxRate}.</p>
     *
     * @return The taxRateID field of the {@link TaxRate}.
     */
    public int getTaxRateID () {
        return taxRateID;
    }

    /**
     * <p>Setter for the taxRateID field of the {@link TaxRate}.</p>
     *
     * @param taxRateID The taxRateID field of the {@link TaxRate}.
     */
    public void setTaxRateID (int taxRateID) {
        this.taxRateID = taxRateID;
    }

    /**
     * <p>Getter for the paTransLineItemID field of the {@link TaxRate}.</p>
     *
     * @return The paTransLineItemID field of the {@link TaxRate}.
     */
    public BigDecimal getPaTransLineItemID () {
        return paTransLineItemID;
    }

    /**
     * <p>Setter for the paTransLineItemID field of the {@link TaxRate}.</p>
     *
     * @param paTransLineItemID The paTransLineItemID field of the {@link TaxRate}.
     */
    public void setPaTransLineItemID (BigDecimal paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
    }

    /**
     * <p>Getter for the name field of the {@link TaxRate}.</p>
     *
     * @return The name field of the {@link TaxRate}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link TaxRate}.</p>
     *
     * @param name The name field of the {@link TaxRate}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the shortName field of the {@link TaxRate}.</p>
     *
     * @return The shortName field of the {@link TaxRate}.
     */
    public String getShortName () {
        return shortName;
    }

    /**
     * <p>Setter for the shortName field of the {@link TaxRate}.</p>
     *
     * @param shortName The shortName field of the {@link TaxRate}.
     */
    public void setShortName (String shortName) {
        this.shortName = shortName;
    }

    /**
     * <p>Getter for the quantity field of the {@link TaxRate}.</p>
     *
     * @return The quantity field of the {@link TaxRate}.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link TaxRate}.</p>
     *
     * @param quantity The quantity field of the {@link TaxRate}.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the refundedQuantity field of the {@link TaxRate}.</p>
     *
     * @return The refundedQuantity field of the {@link TaxRate}.
     */
    public double getRefundedQuantity () {
        return refundedQuantity;
    }

    /**
     * <p>Setter for the refundedQuantity field of the {@link TaxRate}.</p>
     *
     * @param refundedQuantity The refundedQuantity field of the {@link TaxRate}.
     */
    public void setRefundedQuantity (double refundedQuantity) {
        this.refundedQuantity = refundedQuantity;
    }

    /**
     * <p>Getter for the amount field of the {@link TaxRate}.</p>
     *
     * @return The amount field of the {@link TaxRate}.
     */
    public double getAmount () {
        return amount;
    }

    /**
     * <p>Setter for the amount field of the {@link TaxRate}.</p>
     *
     * @param amount The amount field of the {@link TaxRate}.
     */
    public void setAmount (double amount) {
        this.amount = amount;
    }

    /**
     * <p>Getter for the refundedAmount field of the {@link TaxRate}.</p>
     *
     * @return The refundedAmount field of the {@link TaxRate}.
     */
    public double getRefundedAmount () {
        return refundedAmount;
    }

    /**
     * <p>Setter for the refundedAmount field of the {@link TaxRate}.</p>
     *
     * @param refundedAmount The refundedAmount field of the {@link TaxRate}.
     */
    public void setRefundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

}