package com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-19 12:17:20 -0400 (Tue, 19 May 2020) $: Date of last commit
    $Rev: 11785 $: Revision of last commit
    Notes: Represents a KDS prep station.
*/

import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a KDS prep station.</p>
 *
 */
public class KDSPrep implements IKitchenPrep {

    // private member variables of a KDSPrep
    private int printerID = 0;
    private String name = "";
    private String logicalName = "";
    private String description = "";
    private boolean isExpeditorPrinter = false;
    private int ownershipGroupID = 0;
    private int printerHostID = 0;
    private boolean active = false;
    private int printerTypeID = 0;
    private int expireJobMins = 0;
    private boolean printsDigitalOrdersOnly = false;
    private int stationID = 0;
    private int printerHardwareTypeID = 0;
    private boolean printOnlyNewItems = false;
    private boolean noExpeditorTktsUnlessProdAdded = false;

    /**
     * <p>Creates a {@link KDSPrep} from the given {@link HashMap}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kdsPrepStationRec The {@link HashMap} to use to build the {@link KDSPrep}.
     * @return The {@link KDSPrep} built from the given {@link HashMap}.
     */
    public static KDSPrep buildFromHM (String log, HashMap kdsPrepStationRec) {
        KDSPrep kdsPrep = new KDSPrep();

        try {
            if (!DataFunctions.isEmptyMap(kdsPrepStationRec)) {
                kdsPrep.printerID = HashMapDataFns.getIntVal(kdsPrepStationRec, "PRINTERID");
                kdsPrep.name = HashMapDataFns.getStringVal(kdsPrepStationRec, "NAME");
                kdsPrep.logicalName = HashMapDataFns.getStringVal(kdsPrepStationRec, "LOGICALNAME");
                kdsPrep.description = HashMapDataFns.getStringVal(kdsPrepStationRec, "DESCRIPTION");
                kdsPrep.isExpeditorPrinter = HashMapDataFns.getBooleanVal(kdsPrepStationRec, "ISEXPEDITORPRINTER");
                kdsPrep.ownershipGroupID = HashMapDataFns.getIntVal(kdsPrepStationRec, "OWNERSHIPGROUPID");
                kdsPrep.printerHostID = HashMapDataFns.getIntVal(kdsPrepStationRec, "PRINTERHOSTID");
                kdsPrep.active = HashMapDataFns.getBooleanVal(kdsPrepStationRec, "ACTIVE");
                kdsPrep.printerTypeID = HashMapDataFns.getIntVal(kdsPrepStationRec, "PRINTERTYPEID");
                kdsPrep.expireJobMins = HashMapDataFns.getIntVal(kdsPrepStationRec, "EXPIREJOBMINS");
                kdsPrep.printsDigitalOrdersOnly = HashMapDataFns.getBooleanVal(kdsPrepStationRec, "PRINTSDIGITALORDERSONLY");
                kdsPrep.stationID = HashMapDataFns.getIntVal(kdsPrepStationRec, "STATIONID");
                kdsPrep.printerHardwareTypeID = HashMapDataFns.getIntVal(kdsPrepStationRec, "PRINTERHARDWARETYPEID");
                kdsPrep.printOnlyNewItems = HashMapDataFns.getBooleanVal(kdsPrepStationRec, "PRINTONLYNEWITEMS");
                kdsPrep.noExpeditorTktsUnlessProdAdded = HashMapDataFns.getBooleanVal(kdsPrepStationRec, "NOEXPEDITORTKTSUNLESSPRODADDED");
            }
            else {
                Logger.logMessage("The HashMap passed to KDSPrep.buildFromHM can't be null or empty!", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build a KDSPrep from the given HashMap in KDSPrep.buildFromHM", log, Logger.LEVEL.ERROR);
        }

        return kdsPrep;
    }

    /**
     * <p>Overridden toString() method for a {@link KDSPrep}.</p>
     *
     * @return A {@link String} representation of this {@link KDSPrep}.
     */
    @Override
    public String toString () {

        return String.format("PRINTERID: %s, NAME: %s, LOGICALNAME: %s, DESCRIPTION: %s, ISEXPEDITORPRINTER: %s, OWNERSHIPGROUPID: %s, " +
                "PRINTERHOSTID: %s, ACTIVE: %s, PRINTERTYPEID: %s, EXPIREJOBMINS: %s, PRINTSDIGITALORDERSONLY: %s, STATIONID: %s, " +
                "PRINTERHARDWARETYPEID: %s, PRINTONLYNEWITEMS: %s, NOEXPEDITORTKTSUNLESSPRODADDED: %s",
                Objects.toString((printerID != -1 ? printerID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(logicalName) ? logicalName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString(isExpeditorPrinter, "N/A"),
                Objects.toString((ownershipGroupID != -1 ? ownershipGroupID : "N/A"), "N/A"),
                Objects.toString((printerHostID != -1 ? printerHostID : "N/A"), "N/A"),
                Objects.toString(active, "N/A"),
                Objects.toString((printerTypeID != -1 ? printerTypeID : "N/A"), "N/A"),
                Objects.toString((expireJobMins != -1 ? expireJobMins : "N/A"), "N/A"),
                Objects.toString(printsDigitalOrdersOnly, "N/A"),
                Objects.toString((stationID != -1 ? stationID : "N/A"), "N/A"),
                Objects.toString((printerHardwareTypeID != -1 ? printerHardwareTypeID : "N/A"), "N/A"),
                Objects.toString(printOnlyNewItems, "N/A"),
                Objects.toString(noExpeditorTktsUnlessProdAdded, "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link KDSPrep}.
     * Two {@link KDSPrep} are defined as being equal if they have the same printerID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KDSPrep}.
     * @return Whether or not the {@link Object} is equal to this {@link KDSPrep}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KDSPrep and check if the obj's printerID is equal to this KDSPrep's printerID
        KDSPrep kdsPrep = ((KDSPrep) obj);
        return Objects.equals(kdsPrep.printerID, printerID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link KDSPrep}.</p>
     *
     * @return The unique hash code for this {@link KDSPrep}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(printerID);
    }

    /**
     * <p>Getter for the printerID field of the {@link KDSPrep}.</p>
     *
     * @return The printerID field of the {@link KDSPrep}.
     */
    public int getPrinterID () {
        return printerID;
    }

    /**
     * <p>Setter for the printerID field of the {@link KDSPrep}.</p>
     *
     * @param printerID The printerID field of the {@link KDSPrep}.
     */
    public void setPrinterID (int printerID) {
        this.printerID = printerID;
    }

    /**
     * <p>Getter for the name field of the {@link KDSPrep}.</p>
     *
     * @return The name field of the {@link KDSPrep}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link KDSPrep}.</p>
     *
     * @param name The name field of the {@link KDSPrep}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the logicalName field of the {@link KDSPrep}.</p>
     *
     * @return The logicalName field of the {@link KDSPrep}.
     */
    public String getLogicalName () {
        return logicalName;
    }

    /**
     * <p>Setter for the logicalName field of the {@link KDSPrep}.</p>
     *
     * @param logicalName The logicalName field of the {@link KDSPrep}.
     */
    public void setLogicalName (String logicalName) {
        this.logicalName = logicalName;
    }

    /**
     * <p>Getter for the description field of the {@link KDSPrep}.</p>
     *
     * @return The description field of the {@link KDSPrep}.
     */
    public String getDescription () {
        return description;
    }

    /**
     * <p>Setter for the description field of the {@link KDSPrep}.</p>
     *
     * @param description The description field of the {@link KDSPrep}.
     */
    public void setDescription (String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the isExpeditorPrinter field of the {@link KDSPrep}.</p>
     *
     * @return The isExpeditorPrinter field of the {@link KDSPrep}.
     */
    public boolean getIsExpeditorPrinter () {
        return isExpeditorPrinter;
    }

    /**
     * <p>Setter for the isExpeditorPrinter field of the {@link KDSPrep}.</p>
     *
     * @param isExpeditorPrinter The isExpeditorPrinter field of the {@link KDSPrep}.
     */
    public void setIsExpeditorPrinter (boolean isExpeditorPrinter) {
        this.isExpeditorPrinter = isExpeditorPrinter;
    }

    /**
     * <p>Getter for the ownershipGroupID field of the {@link KDSPrep}.</p>
     *
     * @return The ownershipGroupID field of the {@link KDSPrep}.
     */
    public int getOwnershipGroupID () {
        return ownershipGroupID;
    }

    /**
     * <p>Setter for the ownershipGroupID field of the {@link KDSPrep}.</p>
     *
     * @param ownershipGroupID The ownershipGroupID field of the {@link KDSPrep}.
     */
    public void setOwnershipGroupID (int ownershipGroupID) {
        this.ownershipGroupID = ownershipGroupID;
    }

    /**
     * <p>Getter for the printerHostID field of the {@link KDSPrep}.</p>
     *
     * @return The printerHostID field of the {@link KDSPrep}.
     */
    public int getPrinterHostID () {
        return printerHostID;
    }

    /**
     * <p>Setter for the printerHostID field of the {@link KDSPrep}.</p>
     *
     * @param printerHostID The printerHostID field of the {@link KDSPrep}.
     */
    public void setPrinterHostID (int printerHostID) {
        this.printerHostID = printerHostID;
    }

    /**
     * <p>Getter for the active field of the {@link KDSPrep}.</p>
     *
     * @return The active field of the {@link KDSPrep}.
     */
    public boolean getActive () {
        return active;
    }

    /**
     * <p>Setter for the active field of the {@link KDSPrep}.</p>
     *
     * @param active The active field of the {@link KDSPrep}.
     */
    public void setActive (boolean active) {
        this.active = active;
    }

    /**
     * <p>Getter for the printerTypeID field of the {@link KDSPrep}.</p>
     *
     * @return The printerTypeID field of the {@link KDSPrep}.
     */
    public int getPrinterTypeID () {
        return printerTypeID;
    }

    /**
     * <p>Setter for the printerTypeID field of the {@link KDSPrep}.</p>
     *
     * @param printerTypeID The printerTypeID field of the {@link KDSPrep}.
     */
    public void setPrinterTypeID (int printerTypeID) {
        this.printerTypeID = printerTypeID;
    }

    /**
     * <p>Getter for the expireJobMins field of the {@link KDSPrep}.</p>
     *
     * @return The expireJobMins field of the {@link KDSPrep}.
     */
    public int getExpireJobMins () {
        return expireJobMins;
    }

    /**
     * <p>Setter for the expireJobMins field of the {@link KDSPrep}.</p>
     *
     * @param expireJobMins The expireJobMins field of the {@link KDSPrep}.
     */
    public void setExpireJobMins (int expireJobMins) {
        this.expireJobMins = expireJobMins;
    }

    /**
     * <p>Getter for the printsDigitalOrdersOnly field of the {@link KDSPrep}.</p>
     *
     * @return The printsDigitalOrdersOnly field of the {@link KDSPrep}.
     */
    public boolean getPrintsDigitalOrdersOnly () {
        return printsDigitalOrdersOnly;
    }

    /**
     * <p>Setter for the printsDigitalOrdersOnly field of the {@link KDSPrep}.</p>
     *
     * @param printsDigitalOrdersOnly The printsDigitalOrdersOnly field of the {@link KDSPrep}.
     */
    public void setPrintsDigitalOrdersOnly (boolean printsDigitalOrdersOnly) {
        this.printsDigitalOrdersOnly = printsDigitalOrdersOnly;
    }

    /**
     * <p>Getter for the stationID field of the {@link KDSPrep}.</p>
     *
     * @return The stationID field of the {@link KDSPrep}.
     */
    public int getStationID () {
        return stationID;
    }

    /**
     * <p>Setter for the stationID field of the {@link KDSPrep}.</p>
     *
     * @param stationID The stationID field of the {@link KDSPrep}.
     */
    public void setStationID (int stationID) {
        this.stationID = stationID;
    }

    /**
     * <p>Getter for the printerHardwareTypeID field of the {@link KDSPrep}.</p>
     *
     * @return The printerHardwareTypeID field of the {@link KDSPrep}.
     */
    public int getPrinterHardwareTypeID () {
        return printerHardwareTypeID;
    }

    /**
     * <p>Setter for the printerHardwareTypeID field of the {@link KDSPrep}.</p>
     *
     * @param printerHardwareTypeID The printerHardwareTypeID field of the {@link KDSPrep}.
     */
    public void setPrinterHardwareTypeID (int printerHardwareTypeID) {
        this.printerHardwareTypeID = printerHardwareTypeID;
    }

    /**
     * <p>Getter for the printOnlyNewItems field of the {@link KDSPrep}.</p>
     *
     * @return The printOnlyNewItems field of the {@link KDSPrep}.
     */
    public boolean getPrintOnlyNewItems () {
        return printOnlyNewItems;
    }

    /**
     * <p>Setter for the printOnlyNewItems field of the {@link KDSPrep}.</p>
     *
     * @param printOnlyNewItems The printOnlyNewItems field of the {@link KDSPrep}.
     */
    public void setPrintOnlyNewItems (boolean printOnlyNewItems) {
        this.printOnlyNewItems = printOnlyNewItems;
    }

    /**
     * <p>Getter for the noExpeditorTktsUnlessProdAdded field of the {@link KDSPrep}.</p>
     *
     * @return The noExpeditorTktsUnlessProdAdded field of the {@link KDSPrep}.
     */
    public boolean getNoExpeditorTktsUnlessProdAdded () {
        return noExpeditorTktsUnlessProdAdded;
    }

    /**
     * <p>Setter for the noExpeditorTktsUnlessProdAdded field of the {@link KDSPrep}.</p>
     *
     * @param noExpeditorTktsUnlessProdAdded The noExpeditorTktsUnlessProdAdded field of the {@link KDSPrep}.
     */
    public void setNoExpeditorTktsUnlessProdAdded (boolean noExpeditorTktsUnlessProdAdded) {
        this.noExpeditorTktsUnlessProdAdded = noExpeditorTktsUnlessProdAdded;
    }

}