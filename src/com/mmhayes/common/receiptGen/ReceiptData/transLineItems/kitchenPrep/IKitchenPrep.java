package com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-19 12:17:20 -0400 (Tue, 19 May 2020) $: Date of last commit
    $Rev: 11785 $: Revision of last commit
    Notes: Interface that should be implemented by all kitchen preps.
*/

/**
 * <p>Interface that should be implemented by all kitchen preps.</p>
 *
 */
public interface IKitchenPrep {}