package com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-19 12:17:20 -0400 (Tue, 19 May 2020) $: Date of last commit
    $Rev: 11785 $: Revision of last commit
    Notes: Factory to create kitchen preps.
*/

import com.mmhayes.common.utils.Logger;

import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Factory to create kitchen preps.</p>
 *
 */
public class KitchenPrepFactory {

    /**
     * <p>Builds and returns an {@link IKitchenPrep} instance of the given type from the given {@link HashMap}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kitchenPrepType The type of {@link IKitchenPrep} instance to build.
     * @param ptrStnRec The {@link HashMap} record to use to create the instance of an {@link IKitchenPrep}.
     * @return An {@link IKitchenPrep} instance of the given type built from the given {@link HashMap}.
     */
    public IKitchenPrep buildKitchenPrepFromHM (String log, int kitchenPrepType, HashMap ptrStnRec) {
        IKitchenPrep kitchenPrep = null;

        try {
            switch (kitchenPrepType) {
                case KitchenPrepType.KP_PREP:
                    kitchenPrep = KPPrep.buildFromHM(log, ptrStnRec);
                    break;
                case KitchenPrepType.KDS_PREP:
                    kitchenPrep = KDSPrep.buildFromHM(log, ptrStnRec);
                    break;
                case KitchenPrepType.KMS_PREP:
                    kitchenPrep = KMSPrep.buildFromHM(log, ptrStnRec);
                    break;
                default:
                    Logger.logMessage(String.format("Encountered an unknown kitchen prep type of %s in KitchenPrepFactory.buildKitchenPrepFromHM",
                            Objects.toString(kitchenPrepType, "N/A")), log, Logger.LEVEL.ERROR);
                    break;
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build an IKitchenPrep instance from the given HashMap in KitchenPrepFactory.buildKitchenPrepFromHM", log, Logger.LEVEL.ERROR);
        }

        return kitchenPrep;
    }

}