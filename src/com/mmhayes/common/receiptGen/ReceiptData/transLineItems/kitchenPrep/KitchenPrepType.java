package com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-19 12:17:20 -0400 (Tue, 19 May 2020) $: Date of last commit
    $Rev: 11785 $: Revision of last commit
    Notes: Contains different types of kitchen preps that can be built.
*/

/**
 * <p>Contains different types of kitchen preps that can be built.</p>
 *
 */
public class KitchenPrepType {

    public static final int KP_PREP = 1;
    public static final int KDS_PREP = 2;
    public static final int KMS_PREP = 3;

}