package com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-19 12:17:20 -0400 (Tue, 19 May 2020) $: Date of last commit
    $Rev: 11785 $: Revision of last commit
    Notes: Represents a physical kitchen prep printer.
*/

import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a physical kitchen prep printer.</p>
 *
 */
public class KPPrep implements IKitchenPrep {

    // private member variables of a KPPrep
    private int printerID = 0;
    private String name = "";
    private String logicalName = "";
    private String description = "";
    private boolean isExpeditorPrinter = false;
    private int ownershipGroupID = 0;
    private int printerHostID = 0;
    private boolean active = false;
    private int printerTypeID = 0;
    private int expireJobMins = 0;
    private boolean printsDigitalOrdersOnly = false;
    private int printerHardwareTypeID = 0;
    private boolean printOnlyNewItems = false;
    private boolean noExpeditorTktsUnlessProdAdded = false;
    private int printerModelID = 0;
    private boolean usesEscPos = false;
    private int printerComTypeID = 0;

    /**
     * <p>Creates a {@link KPPrep} from the given {@link HashMap}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kpPrepPrinterRec The {@link HashMap} to use to build the {@link KPPrep}.
     * @return The {@link KPPrep} built from the given {@link HashMap}.
     */
    public static KPPrep buildFromHM (String log, HashMap kpPrepPrinterRec) {
        KPPrep kpPrep = new KPPrep();

        try {
            if (!DataFunctions.isEmptyMap(kpPrepPrinterRec)) {
                kpPrep.printerID = HashMapDataFns.getIntVal(kpPrepPrinterRec, "PRINTERID");
                kpPrep.name = HashMapDataFns.getStringVal(kpPrepPrinterRec, "NAME");
                kpPrep.logicalName = HashMapDataFns.getStringVal(kpPrepPrinterRec, "LOGICALNAME");
                kpPrep.description = HashMapDataFns.getStringVal(kpPrepPrinterRec, "DESCRIPTION");
                kpPrep.isExpeditorPrinter = HashMapDataFns.getBooleanVal(kpPrepPrinterRec, "ISEXPEDITORPRINTER");
                kpPrep.ownershipGroupID = HashMapDataFns.getIntVal(kpPrepPrinterRec, "OWNERSHIPGROUPID");
                kpPrep.printerHostID = HashMapDataFns.getIntVal(kpPrepPrinterRec, "PRINTERHOSTID");
                kpPrep.active = HashMapDataFns.getBooleanVal(kpPrepPrinterRec, "ACTIVE");
                kpPrep.printerTypeID = HashMapDataFns.getIntVal(kpPrepPrinterRec, "PRINTERTYPEID");
                kpPrep.expireJobMins = HashMapDataFns.getIntVal(kpPrepPrinterRec, "EXPIREJOBMINS");
                kpPrep.printsDigitalOrdersOnly = HashMapDataFns.getBooleanVal(kpPrepPrinterRec, "PRINTSDIGITALORDERSONLY");
                kpPrep.printerHardwareTypeID = HashMapDataFns.getIntVal(kpPrepPrinterRec, "PRINTERHARDWARETYPEID");
                kpPrep.printOnlyNewItems = HashMapDataFns.getBooleanVal(kpPrepPrinterRec, "PRINTONLYNEWITEMS");
                kpPrep.noExpeditorTktsUnlessProdAdded = HashMapDataFns.getBooleanVal(kpPrepPrinterRec, "NOEXPEDITORTKTSUNLESSPRODADDED");
                kpPrep.printerModelID = HashMapDataFns.getIntVal(kpPrepPrinterRec, "PRINTERMODELID");
                kpPrep.usesEscPos = HashMapDataFns.getBooleanVal(kpPrepPrinterRec, "USESESCPOS");
                kpPrep.printerComTypeID = HashMapDataFns.getIntVal(kpPrepPrinterRec, "PRINTERCOMTYPEID");
            }
            else {
                Logger.logMessage("The HashMap passed to KPPrep.buildFromHM can't be null or empty!", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build a KPPrep from the given HashMap in KPPrep.buildFromHM", log, Logger.LEVEL.ERROR);
        }

        return kpPrep;
    }

    /**
     * <p>Overridden toString() method for a {@link KPPrep}.</p>
     *
     * @return A {@link String} representation of this {@link KPPrep}.
     */
    @Override
    public String toString () {

        return String.format("PRINTERID: %s, NAME: %s, LOGICALNAME: %s, DESCRIPTION: %s, ISEXPEDITORPRINTER: %s, OWNERSHIPGROUPID: %s, " +
                "PRINTERHOSTID: %s, ACTIVE: %s, PRINTERTYPEID: %s, EXPIREJOBMINS: %s, PRINTSDIGITALORDERSONLY: %s, PRINTERHARDWARETYPEID: %s, " +
                "PRINTONLYNEWITEMS: %s, NOEXPEDITORTKTSUNLESSPRODADDED: %s, PRINTERMODELID: %s, USESESCPOS: %s, PRINTERCOMTYPEID: %s",
                Objects.toString((printerID != -1 ? printerID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(logicalName) ? logicalName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString(isExpeditorPrinter, "N/A"),
                Objects.toString((ownershipGroupID != -1 ? ownershipGroupID : "N/A"), "N/A"),
                Objects.toString((printerHostID != -1 ? printerHostID : "N/A"), "N/A"),
                Objects.toString(active, "N/A"),
                Objects.toString((printerTypeID != -1 ? printerTypeID : "N/A"), "N/A"),
                Objects.toString((expireJobMins != -1 ? expireJobMins : "N/A"), "N/A"),
                Objects.toString(printsDigitalOrdersOnly, "N/A"),
                Objects.toString((printerHardwareTypeID != -1 ? printerHardwareTypeID : "N/A"), "N/A"),
                Objects.toString(printOnlyNewItems, "N/A"),
                Objects.toString(noExpeditorTktsUnlessProdAdded, "N/A"),
                Objects.toString((printerModelID != -1 ? printerModelID : "N/A"), "N/A"),
                Objects.toString(usesEscPos, "N/A"),
                Objects.toString((printerComTypeID != -1 ? printerComTypeID : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link KPPrep}.
     * Two {@link KPPrep} are defined as being equal if they have the same printerID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KPPrep}.
     * @return Whether or not the {@link Object} is equal to this {@link KPPrep}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KPPrep and check if the obj's printerID is equal to this KPPrep's printerID
        KPPrep kpPrep = ((KPPrep) obj);
        return Objects.equals(kpPrep.printerID, printerID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link KPPrep}.</p>
     *
     * @return The unique hash code for this {@link KPPrep}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(printerID);
    }

    /**
     * <p>Getter for the printerID field of the {@link KPPrep}.</p>
     *
     * @return The printerID field of the {@link KPPrep}.
     */
    public int getPrinterID () {
        return printerID;
    }

    /**
     * <p>Setter for the printerID field of the {@link KPPrep}.</p>
     *
     * @param printerID The printerID field of the {@link KPPrep}.
     */
    public void setPrinterID (int printerID) {
        this.printerID = printerID;
    }

    /**
     * <p>Getter for the name field of the {@link KPPrep}.</p>
     *
     * @return The name field of the {@link KPPrep}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link KPPrep}.</p>
     *
     * @param name The name field of the {@link KPPrep}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the logicalName field of the {@link KPPrep}.</p>
     *
     * @return The logicalName field of the {@link KPPrep}.
     */
    public String getLogicalName () {
        return logicalName;
    }

    /**
     * <p>Setter for the logicalName field of the {@link KPPrep}.</p>
     *
     * @param logicalName The logicalName field of the {@link KPPrep}.
     */
    public void setLogicalName (String logicalName) {
        this.logicalName = logicalName;
    }

    /**
     * <p>Getter for the description field of the {@link KPPrep}.</p>
     *
     * @return The description field of the {@link KPPrep}.
     */
    public String getDescription () {
        return description;
    }

    /**
     * <p>Setter for the description field of the {@link KPPrep}.</p>
     *
     * @param description The description field of the {@link KPPrep}.
     */
    public void setDescription (String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the isExpeditorPrinter field of the {@link KPPrep}.</p>
     *
     * @return The isExpeditorPrinter field of the {@link KPPrep}.
     */
    public boolean getIsExpeditorPrinter () {
        return isExpeditorPrinter;
    }

    /**
     * <p>Setter for the isExpeditorPrinter field of the {@link KPPrep}.</p>
     *
     * @param isExpeditorPrinter The isExpeditorPrinter field of the {@link KPPrep}.
     */
    public void setIsExpeditorPrinter (boolean isExpeditorPrinter) {
        this.isExpeditorPrinter = isExpeditorPrinter;
    }

    /**
     * <p>Getter for the ownershipGroupID field of the {@link KPPrep}.</p>
     *
     * @return The ownershipGroupID field of the {@link KPPrep}.
     */
    public int getOwnershipGroupID () {
        return ownershipGroupID;
    }

    /**
     * <p>Setter for the ownershipGroupID field of the {@link KPPrep}.</p>
     *
     * @param ownershipGroupID The ownershipGroupID field of the {@link KPPrep}.
     */
    public void setOwnershipGroupID (int ownershipGroupID) {
        this.ownershipGroupID = ownershipGroupID;
    }

    /**
     * <p>Getter for the printerHostID field of the {@link KPPrep}.</p>
     *
     * @return The printerHostID field of the {@link KPPrep}.
     */
    public int getPrinterHostID () {
        return printerHostID;
    }

    /**
     * <p>Setter for the printerHostID field of the {@link KPPrep}.</p>
     *
     * @param printerHostID The printerHostID field of the {@link KPPrep}.
     */
    public void setPrinterHostID (int printerHostID) {
        this.printerHostID = printerHostID;
    }

    /**
     * <p>Getter for the active field of the {@link KPPrep}.</p>
     *
     * @return The active field of the {@link KPPrep}.
     */
    public boolean getActive () {
        return active;
    }

    /**
     * <p>Setter for the active field of the {@link KPPrep}.</p>
     *
     * @param active The active field of the {@link KPPrep}.
     */
    public void setActive (boolean active) {
        this.active = active;
    }

    /**
     * <p>Getter for the printerTypeID field of the {@link KPPrep}.</p>
     *
     * @return The printerTypeID field of the {@link KPPrep}.
     */
    public int getPrinterTypeID () {
        return printerTypeID;
    }

    /**
     * <p>Setter for the printerTypeID field of the {@link KPPrep}.</p>
     *
     * @param printerTypeID The printerTypeID field of the {@link KPPrep}.
     */
    public void setPrinterTypeID (int printerTypeID) {
        this.printerTypeID = printerTypeID;
    }

    /**
     * <p>Getter for the expireJobMins field of the {@link KPPrep}.</p>
     *
     * @return The expireJobMins field of the {@link KPPrep}.
     */
    public int getExpireJobMins () {
        return expireJobMins;
    }

    /**
     * <p>Setter for the expireJobMins field of the {@link KPPrep}.</p>
     *
     * @param expireJobMins The expireJobMins field of the {@link KPPrep}.
     */
    public void setExpireJobMins (int expireJobMins) {
        this.expireJobMins = expireJobMins;
    }

    /**
     * <p>Getter for the printsDigitalOrdersOnly field of the {@link KPPrep}.</p>
     *
     * @return The printsDigitalOrdersOnly field of the {@link KPPrep}.
     */
    public boolean getPrintsDigitalOrdersOnly () {
        return printsDigitalOrdersOnly;
    }

    /**
     * <p>Setter for the printsDigitalOrdersOnly field of the {@link KPPrep}.</p>
     *
     * @param printsDigitalOrdersOnly The printsDigitalOrdersOnly field of the {@link KPPrep}.
     */
    public void setPrintsDigitalOrdersOnly (boolean printsDigitalOrdersOnly) {
        this.printsDigitalOrdersOnly = printsDigitalOrdersOnly;
    }

    /**
     * <p>Getter for the printerHardwareTypeID field of the {@link KPPrep}.</p>
     *
     * @return The printerHardwareTypeID field of the {@link KPPrep}.
     */
    public int getPrinterHardwareTypeID () {
        return printerHardwareTypeID;
    }

    /**
     * <p>Setter for the printerHardwareTypeID field of the {@link KPPrep}.</p>
     *
     * @param printerHardwareTypeID The printerHardwareTypeID field of the {@link KPPrep}.
     */
    public void setPrinterHardwareTypeID (int printerHardwareTypeID) {
        this.printerHardwareTypeID = printerHardwareTypeID;
    }

    /**
     * <p>Getter for the printOnlyNewItems field of the {@link KPPrep}.</p>
     *
     * @return The printOnlyNewItems field of the {@link KPPrep}.
     */
    public boolean getPrintOnlyNewItems () {
        return printOnlyNewItems;
    }

    /**
     * <p>Setter for the printOnlyNewItems field of the {@link KPPrep}.</p>
     *
     * @param printOnlyNewItems The printOnlyNewItems field of the {@link KPPrep}.
     */
    public void setPrintOnlyNewItems (boolean printOnlyNewItems) {
        this.printOnlyNewItems = printOnlyNewItems;
    }

    /**
     * <p>Getter for the noExpeditorTktsUnlessProdAdded field of the {@link KPPrep}.</p>
     *
     * @return The noExpeditorTktsUnlessProdAdded field of the {@link KPPrep}.
     */
    public boolean getNoExpeditorTktsUnlessProdAdded () {
        return noExpeditorTktsUnlessProdAdded;
    }

    /**
     * <p>Setter for the noExpeditorTktsUnlessProdAdded field of the {@link KPPrep}.</p>
     *
     * @param noExpeditorTktsUnlessProdAdded The noExpeditorTktsUnlessProdAdded field of the {@link KPPrep}.
     */
    public void setNoExpeditorTktsUnlessProdAdded (boolean noExpeditorTktsUnlessProdAdded) {
        this.noExpeditorTktsUnlessProdAdded = noExpeditorTktsUnlessProdAdded;
    }

    /**
     * <p>Getter for the printerModelID field of the {@link KPPrep}.</p>
     *
     * @return The printerModelID field of the {@link KPPrep}.
     */
    public int getPrinterModelID () {
        return printerModelID;
    }

    /**
     * <p>Setter for the printerModelID field of the {@link KPPrep}.</p>
     *
     * @param printerModelID The printerModelID field of the {@link KPPrep}.
     */
    public void setPrinterModelID (int printerModelID) {
        this.printerModelID = printerModelID;
    }

    /**
     * <p>Getter for the usesEscPos field of the {@link KPPrep}.</p>
     *
     * @return The usesEscPos field of the {@link KPPrep}.
     */
    public boolean getUsesEscPos () {
        return usesEscPos;
    }

    /**
     * <p>Setter for the usesEscPos field of the {@link KPPrep}.</p>
     *
     * @param usesEscPos The usesEscPos field of the {@link KPPrep}.
     */
    public void setUsesEscPos (boolean usesEscPos) {
        this.usesEscPos = usesEscPos;
    }

    /**
     * <p>Getter for the printerComTypeID field of the {@link KPPrep}.</p>
     *
     * @return The printerComTypeID field of the {@link KPPrep}.
     */
    public int getPrinterComTypeID () {
        return printerComTypeID;
    }

    /**
     * <p>Setter for the printerComTypeID field of the {@link KPPrep}.</p>
     *
     * @param printerComTypeID The printerComTypeID field of the {@link KPPrep}.
     */
    public void setPrinterComTypeID (int printerComTypeID) {
        this.printerComTypeID = printerComTypeID;
    }

}