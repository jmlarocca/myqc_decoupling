package com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-19 12:17:20 -0400 (Tue, 19 May 2020) $: Date of last commit
    $Rev: 11785 $: Revision of last commit
    Notes: Represents a KMS prep station.
*/

import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a KMS prep station.</p>
 *
 */
public class KMSPrep implements IKitchenPrep {

    // private member variables of a KMSPrep
    private int kmsStationID = 0;
    private String name = "";
    private String description = "";
    private int ownershipGroupID = 0;
    private boolean active = false;
    private int kmsStationTypeID = 0;
    private int kmsStationModeID = 0;
    private int kmsStationLayoutID = 0;
    private int numColsPerPage = 0;
    private boolean displayTitleBar = false;
    private boolean displayStationName = false;
    private boolean displayDateTime = false;
    private int titleBarAlignmentID = 0;
    private String logoFileName = "";
    private String logoTokenizedFileName = "";
    private String token = "";
    private boolean displayOrderNumber = false;
    private boolean displayCustomerName = false;
    private boolean displayLegend = false;
    private boolean displayRecent = false;
    private int mirrorKMSStationID = 0;
    private int backupKMSStationID = 0;
    private int titleStyleProfileID = 0;
    private int contentStyleProfileID = 0;
    private int rushOrderStyleProfileID = 0;
    private int orderStatusProfileID = 0;
    private int orderTimerProfileID = 0;
    private int completedOrderTimeDisplayed = 0;
    private boolean bumpProductsToCompleted = false;
    private boolean displayModsInProgress = false;
    private boolean bumpIndividualProducts = false;
    private int touchControlStyleProfileID = 0;
    private boolean printOrdersOnCompleted = false;

    /**
     * <p>Creates a {@link KMSPrep} from the given {@link HashMap}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kmsPrepStationRec The {@link HashMap} to use to build the {@link KMSPrep}.
     * @return The {@link KMSPrep} built from the given {@link HashMap}.
     */
    public static KMSPrep buildFromHM (String log, HashMap kmsPrepStationRec) {
        KMSPrep kmsPrep = new KMSPrep();

        try {
            if (!DataFunctions.isEmptyMap(kmsPrepStationRec)) {
                kmsPrep.kmsStationID = HashMapDataFns.getIntVal(kmsPrepStationRec, "KMSSTATIONID");
                kmsPrep.name = HashMapDataFns.getStringVal(kmsPrepStationRec, "NAME");
                kmsPrep.description = HashMapDataFns.getStringVal(kmsPrepStationRec, "DESCRIPTION");
                kmsPrep.ownershipGroupID = HashMapDataFns.getIntVal(kmsPrepStationRec, "OWNERSHIPGROUPID");
                kmsPrep.active = HashMapDataFns.getBooleanVal(kmsPrepStationRec, "ACTIVE");
                kmsPrep.kmsStationTypeID = HashMapDataFns.getIntVal(kmsPrepStationRec, "KMSSTATIONTYPEID");
                kmsPrep.kmsStationModeID = HashMapDataFns.getIntVal(kmsPrepStationRec, "KMSSTATIONMODEID");
                kmsPrep.kmsStationLayoutID = HashMapDataFns.getIntVal(kmsPrepStationRec, "KMSSTATIONLAYOUTID");
                kmsPrep.numColsPerPage = HashMapDataFns.getIntVal(kmsPrepStationRec, "NUMCOLSPERPAGE");
                kmsPrep.displayTitleBar = HashMapDataFns.getBooleanVal(kmsPrepStationRec, "DISPLAYTITLEBAR");
                kmsPrep.displayStationName = HashMapDataFns.getBooleanVal(kmsPrepStationRec, "DISPLAYSTATIONNAME");
                kmsPrep.displayDateTime = HashMapDataFns.getBooleanVal(kmsPrepStationRec, "DISPLAYDATETIME");
                kmsPrep.titleBarAlignmentID = HashMapDataFns.getIntVal(kmsPrepStationRec, "TITLEBARALIGNMENTID");
                kmsPrep.logoFileName = HashMapDataFns.getStringVal(kmsPrepStationRec, "LOGOFILENAME");
                kmsPrep.logoTokenizedFileName = HashMapDataFns.getStringVal(kmsPrepStationRec, "LOGOTOKENIZEDFILENAME");
                kmsPrep.token = HashMapDataFns.getStringVal(kmsPrepStationRec, "TOKEN");
                kmsPrep.displayOrderNumber = HashMapDataFns.getBooleanVal(kmsPrepStationRec, "DISPLAYORDERNUMBER");
                kmsPrep.displayCustomerName = HashMapDataFns.getBooleanVal(kmsPrepStationRec, "DISPLAYCUSTOMERNAME");
                kmsPrep.displayLegend = HashMapDataFns.getBooleanVal(kmsPrepStationRec, "DISPLAYLEGEND");
                kmsPrep.displayRecent = HashMapDataFns.getBooleanVal(kmsPrepStationRec, "DISPLAYRECENT");
                kmsPrep.mirrorKMSStationID = HashMapDataFns.getIntVal(kmsPrepStationRec, "MIRRORKMSSTATIONID");
                kmsPrep.backupKMSStationID = HashMapDataFns.getIntVal(kmsPrepStationRec, "BACKUPKMSSTATIONID");
                kmsPrep.titleStyleProfileID = HashMapDataFns.getIntVal(kmsPrepStationRec, "TITLESTYLEPROFILEID");
                kmsPrep.contentStyleProfileID = HashMapDataFns.getIntVal(kmsPrepStationRec, "CONTENTSTYLEPROFILEID");
                kmsPrep.rushOrderStyleProfileID = HashMapDataFns.getIntVal(kmsPrepStationRec, "RUSHORDERSTYLEPROFILEID");
                kmsPrep.orderStatusProfileID = HashMapDataFns.getIntVal(kmsPrepStationRec, "ORDERSTATUSPROFILEID");
                kmsPrep.orderTimerProfileID = HashMapDataFns.getIntVal(kmsPrepStationRec, "ORDERTIMERPROFILEID");
                kmsPrep.completedOrderTimeDisplayed = HashMapDataFns.getIntVal(kmsPrepStationRec, "COMPLETEDORDERTIMEDISPLAYED");
                kmsPrep.bumpProductsToCompleted = HashMapDataFns.getBooleanVal(kmsPrepStationRec, "BUMPPRODUCTSTOCOMPLETED");
                kmsPrep.displayModsInProgress = HashMapDataFns.getBooleanVal(kmsPrepStationRec, "DISPLAYMODSINPROGRESS");
                kmsPrep.bumpIndividualProducts = HashMapDataFns.getBooleanVal(kmsPrepStationRec, "BUMPINDIVIDUALPRODUCTS");
                kmsPrep.touchControlStyleProfileID = HashMapDataFns.getIntVal(kmsPrepStationRec, "TOUCHCONTROLSTYLEPROFILEID");
                kmsPrep.printOrdersOnCompleted = HashMapDataFns.getBooleanVal(kmsPrepStationRec, "PRINTORDERSONCOMPLETED");
            }
            else {
                Logger.logMessage("The HashMap passed to KMSPrep.buildFromHM can't be null or empty!", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build a KMSPrep from the given HashMap in KMSPrep.buildFromHM", log, Logger.LEVEL.ERROR);
        }

        return kmsPrep;
    }

    /**
     * <p>Overridden toString() method for a {@link KMSPrep}.</p>
     *
     * @return A {@link String} representation of this {@link KMSPrep}.
     */
    @Override
    public String toString () {

        return String.format("KMSSTATIONID: %s, NAME: %s, DESCRIPTION: %s, OWNERSHIPGROUPID: %s, ACTIVE: %s, KMSSTATIONTYPEID: %s, " +
                "KMSSTATIONMODEID: %s, KMSSTATIONLAYOUTID: %s, NUMCOLSPERPAGE: %s, DISPLAYTITLEBAR: %s, DISPLAYSTATIONNAME: %s, DISPLAYDATETIME: %s, " +
                "TITLEBARALIGNMENTID: %s, LOGOFILENAME: %s, LOGOTOKENIZEDFILENAME: %s, TOKEN: %s, DISPLAYORDERNUMBER: %s, DISPLAYCUSTOMERNAME: %s, " +
                "DISPLAYLEGEND: %s, DISPLAYRECENT: %s, MIRRORKMSSTATIONID: %s, BACKUPKMSSTATIONID: %s, TITLESTYLEPROFILEID: %s, CONTENTSTYLEPROFILEID: %s, " +
                "RUSHORDERSTYLEPROFILEID: %s, ORDERSTATUSPROFILEID: %s, ORDERTIMERPROFILEID: %s, COMPLETEDORDERTIMEDISPLAYED: %s, BUMPPRODUCTSTOCOMPLETED: %s, " +
                "DISPLAYMODSINPROGRESS: %s, BUMPINDIVIDUALPRODUCTS: %s, TOUCHCONTROLSTYLEPROFILEID: %s, PRINTORDERSONCOMPLETED: %s",
                Objects.toString((kmsStationID != -1 ? kmsStationID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString((ownershipGroupID != -1 ? ownershipGroupID : "N/A"), "N/A"),
                Objects.toString(active, "N/A"),
                Objects.toString((kmsStationTypeID != -1 ? kmsStationTypeID : "N/A"), "N/A"),
                Objects.toString((kmsStationModeID != -1 ? kmsStationModeID : "N/A"), "N/A"),
                Objects.toString((kmsStationLayoutID != -1 ? kmsStationLayoutID : "N/A"), "N/A"),
                Objects.toString((numColsPerPage != -1 ? numColsPerPage : "N/A"), "N/A"),
                Objects.toString(displayTitleBar, "N/A"),
                Objects.toString(displayStationName, "N/A"),
                Objects.toString(displayDateTime, "N/A"),
                Objects.toString((titleBarAlignmentID != -1 ? titleBarAlignmentID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(logoFileName) ? logoFileName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(logoTokenizedFileName) ? logoTokenizedFileName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(token) ? token : "N/A"), "N/A"),
                Objects.toString(displayOrderNumber, "N/A"),
                Objects.toString(displayCustomerName, "N/A"),
                Objects.toString(displayLegend, "N/A"),
                Objects.toString(displayRecent, "N/A"),
                Objects.toString((mirrorKMSStationID != -1 ? mirrorKMSStationID : "N/A"), "N/A"),
                Objects.toString((backupKMSStationID != -1 ? backupKMSStationID : "N/A"), "N/A"),
                Objects.toString((titleStyleProfileID != -1 ? titleStyleProfileID : "N/A"), "N/A"),
                Objects.toString((contentStyleProfileID != -1 ? contentStyleProfileID : "N/A"), "N/A"),
                Objects.toString((rushOrderStyleProfileID != -1 ? rushOrderStyleProfileID : "N/A"), "N/A"),
                Objects.toString((orderStatusProfileID != -1 ? orderStatusProfileID : "N/A"), "N/A"),
                Objects.toString((orderTimerProfileID != -1 ? orderTimerProfileID : "N/A"), "N/A"),
                Objects.toString((completedOrderTimeDisplayed != -1 ? completedOrderTimeDisplayed : "N/A"), "N/A"),
                Objects.toString(bumpProductsToCompleted, "N/A"),
                Objects.toString(displayModsInProgress, "N/A"),
                Objects.toString(bumpIndividualProducts, "N/A"),
                Objects.toString((touchControlStyleProfileID != -1 ? touchControlStyleProfileID : "N/A"), "N/A"),
                Objects.toString(printOrdersOnCompleted, "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link KMSPrep}.
     * Two {@link KMSPrep} are defined as being equal if they have the same kmsStationID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KMSPrep}.
     * @return Whether or not the {@link Object} is equal to this {@link KMSPrep}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KMSPrep and check if the obj's kmsStationID is equal to this KMSPrep's kmsStationID
        KMSPrep kmsPrep = ((KMSPrep) obj);
        return Objects.equals(kmsPrep.kmsStationID, kmsStationID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link KMSPrep}.</p>
     *
     * @return The unique hash code for this {@link KMSPrep}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(kmsStationID);
    }

    /**
     * <p>Getter for the kmsStationID field of the {@link KMSPrep}.</p>
     *
     * @return The kmsStationID field of the {@link KMSPrep}.
     */
    public int getKmsStationID () {
        return kmsStationID;
    }

    /**
     * <p>Setter for the kmsStationID field of the {@link KMSPrep}.</p>
     *
     * @param kmsStationID The kmsStationID field of the {@link KMSPrep}.
     */
    public void setKmsStationID (int kmsStationID) {
        this.kmsStationID = kmsStationID;
    }

    /**
     * <p>Getter for the name field of the {@link KMSPrep}.</p>
     *
     * @return The name field of the {@link KMSPrep}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link KMSPrep}.</p>
     *
     * @param name The name field of the {@link KMSPrep}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the description field of the {@link KMSPrep}.</p>
     *
     * @return The description field of the {@link KMSPrep}.
     */
    public String getDescription () {
        return description;
    }

    /**
     * <p>Setter for the description field of the {@link KMSPrep}.</p>
     *
     * @param description The description field of the {@link KMSPrep}.
     */
    public void setDescription (String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the ownershipGroupID field of the {@link KMSPrep}.</p>
     *
     * @return The ownershipGroupID field of the {@link KMSPrep}.
     */
    public int getOwnershipGroupID () {
        return ownershipGroupID;
    }

    /**
     * <p>Setter for the ownershipGroupID field of the {@link KMSPrep}.</p>
     *
     * @param ownershipGroupID The ownershipGroupID field of the {@link KMSPrep}.
     */
    public void setOwnershipGroupID (int ownershipGroupID) {
        this.ownershipGroupID = ownershipGroupID;
    }

    /**
     * <p>Getter for the active field of the {@link KMSPrep}.</p>
     *
     * @return The active field of the {@link KMSPrep}.
     */
    public boolean getActive () {
        return active;
    }

    /**
     * <p>Setter for the active field of the {@link KMSPrep}.</p>
     *
     * @param active The active field of the {@link KMSPrep}.
     */
    public void setActive (boolean active) {
        this.active = active;
    }

    /**
     * <p>Getter for the kmsStationTypeID field of the {@link KMSPrep}.</p>
     *
     * @return The kmsStationTypeID field of the {@link KMSPrep}.
     */
    public int getKmsStationTypeID () {
        return kmsStationTypeID;
    }

    /**
     * <p>Setter for the kmsStationTypeID field of the {@link KMSPrep}.</p>
     *
     * @param kmsStationTypeID The kmsStationTypeID field of the {@link KMSPrep}.
     */
    public void setKmsStationTypeID (int kmsStationTypeID) {
        this.kmsStationTypeID = kmsStationTypeID;
    }

    /**
     * <p>Getter for the kmsStationModeID field of the {@link KMSPrep}.</p>
     *
     * @return The kmsStationModeID field of the {@link KMSPrep}.
     */
    public int getKmsStationModeID () {
        return kmsStationModeID;
    }

    /**
     * <p>Setter for the kmsStationModeID field of the {@link KMSPrep}.</p>
     *
     * @param kmsStationModeID The kmsStationModeID field of the {@link KMSPrep}.
     */
    public void setKmsStationModeID (int kmsStationModeID) {
        this.kmsStationModeID = kmsStationModeID;
    }

    /**
     * <p>Getter for the kmsStationLayoutID field of the {@link KMSPrep}.</p>
     *
     * @return The kmsStationLayoutID field of the {@link KMSPrep}.
     */
    public int getKmsStationLayoutID () {
        return kmsStationLayoutID;
    }

    /**
     * <p>Setter for the kmsStationLayoutID field of the {@link KMSPrep}.</p>
     *
     * @param kmsStationLayoutID The kmsStationLayoutID field of the {@link KMSPrep}.
     */
    public void setKmsStationLayoutID (int kmsStationLayoutID) {
        this.kmsStationLayoutID = kmsStationLayoutID;
    }

    /**
     * <p>Getter for the numColsPerPage field of the {@link KMSPrep}.</p>
     *
     * @return The numColsPerPage field of the {@link KMSPrep}.
     */
    public int getNumColsPerPage () {
        return numColsPerPage;
    }

    /**
     * <p>Setter for the numColsPerPage field of the {@link KMSPrep}.</p>
     *
     * @param numColsPerPage The numColsPerPage field of the {@link KMSPrep}.
     */
    public void setNumColsPerPage (int numColsPerPage) {
        this.numColsPerPage = numColsPerPage;
    }

    /**
     * <p>Getter for the displayTitleBar field of the {@link KMSPrep}.</p>
     *
     * @return The displayTitleBar field of the {@link KMSPrep}.
     */
    public boolean getDisplayTitleBar () {
        return displayTitleBar;
    }

    /**
     * <p>Setter for the displayTitleBar field of the {@link KMSPrep}.</p>
     *
     * @param displayTitleBar The displayTitleBar field of the {@link KMSPrep}.
     */
    public void setDisplayTitleBar (boolean displayTitleBar) {
        this.displayTitleBar = displayTitleBar;
    }

    /**
     * <p>Getter for the displayStationName field of the {@link KMSPrep}.</p>
     *
     * @return The displayStationName field of the {@link KMSPrep}.
     */
    public boolean getDisplayStationName () {
        return displayStationName;
    }

    /**
     * <p>Setter for the displayStationName field of the {@link KMSPrep}.</p>
     *
     * @param displayStationName The displayStationName field of the {@link KMSPrep}.
     */
    public void setDisplayStationName (boolean displayStationName) {
        this.displayStationName = displayStationName;
    }

    /**
     * <p>Getter for the displayDateTime field of the {@link KMSPrep}.</p>
     *
     * @return The displayDateTime field of the {@link KMSPrep}.
     */
    public boolean getDisplayDateTime () {
        return displayDateTime;
    }

    /**
     * <p>Setter for the displayDateTime field of the {@link KMSPrep}.</p>
     *
     * @param displayDateTime The displayDateTime field of the {@link KMSPrep}.
     */
    public void setDisplayDateTime (boolean displayDateTime) {
        this.displayDateTime = displayDateTime;
    }

    /**
     * <p>Getter for the titleBarAlignmentID field of the {@link KMSPrep}.</p>
     *
     * @return The titleBarAlignmentID field of the {@link KMSPrep}.
     */
    public int getTitleBarAlignmentID () {
        return titleBarAlignmentID;
    }

    /**
     * <p>Setter for the titleBarAlignmentID field of the {@link KMSPrep}.</p>
     *
     * @param titleBarAlignmentID The titleBarAlignmentID field of the {@link KMSPrep}.
     */
    public void setTitleBarAlignmentID (int titleBarAlignmentID) {
        this.titleBarAlignmentID = titleBarAlignmentID;
    }

    /**
     * <p>Getter for the logoFileName field of the {@link KMSPrep}.</p>
     *
     * @return The logoFileName field of the {@link KMSPrep}.
     */
    public String getLogoFileName () {
        return logoFileName;
    }

    /**
     * <p>Setter for the logoFileName field of the {@link KMSPrep}.</p>
     *
     * @param logoFileName The logoFileName field of the {@link KMSPrep}.
     */
    public void setLogoFileName (String logoFileName) {
        this.logoFileName = logoFileName;
    }

    /**
     * <p>Getter for the logoTokenizedFileName field of the {@link KMSPrep}.</p>
     *
     * @return The logoTokenizedFileName field of the {@link KMSPrep}.
     */
    public String getLogoTokenizedFileName () {
        return logoTokenizedFileName;
    }

    /**
     * <p>Setter for the logoTokenizedFileName field of the {@link KMSPrep}.</p>
     *
     * @param logoTokenizedFileName The logoTokenizedFileName field of the {@link KMSPrep}.
     */
    public void setLogoTokenizedFileName (String logoTokenizedFileName) {
        this.logoTokenizedFileName = logoTokenizedFileName;
    }

    /**
     * <p>Getter for the token field of the {@link KMSPrep}.</p>
     *
     * @return The token field of the {@link KMSPrep}.
     */
    public String getToken () {
        return token;
    }

    /**
     * <p>Setter for the token field of the {@link KMSPrep}.</p>
     *
     * @param token The token field of the {@link KMSPrep}.
     */
    public void setToken (String token) {
        this.token = token;
    }

    /**
     * <p>Getter for the displayOrderNumber field of the {@link KMSPrep}.</p>
     *
     * @return The displayOrderNumber field of the {@link KMSPrep}.
     */
    public boolean getDisplayOrderNumber () {
        return displayOrderNumber;
    }

    /**
     * <p>Setter for the displayOrderNumber field of the {@link KMSPrep}.</p>
     *
     * @param displayOrderNumber The displayOrderNumber field of the {@link KMSPrep}.
     */
    public void setDisplayOrderNumber (boolean displayOrderNumber) {
        this.displayOrderNumber = displayOrderNumber;
    }

    /**
     * <p>Getter for the displayCustomerName field of the {@link KMSPrep}.</p>
     *
     * @return The displayCustomerName field of the {@link KMSPrep}.
     */
    public boolean getDisplayCustomerName () {
        return displayCustomerName;
    }

    /**
     * <p>Setter for the displayCustomerName field of the {@link KMSPrep}.</p>
     *
     * @param displayCustomerName The displayCustomerName field of the {@link KMSPrep}.
     */
    public void setDisplayCustomerName (boolean displayCustomerName) {
        this.displayCustomerName = displayCustomerName;
    }

    /**
     * <p>Getter for the displayLegend field of the {@link KMSPrep}.</p>
     *
     * @return The displayLegend field of the {@link KMSPrep}.
     */
    public boolean getDisplayLegend () {
        return displayLegend;
    }

    /**
     * <p>Setter for the displayLegend field of the {@link KMSPrep}.</p>
     *
     * @param displayLegend The displayLegend field of the {@link KMSPrep}.
     */
    public void setDisplayLegend (boolean displayLegend) {
        this.displayLegend = displayLegend;
    }

    /**
     * <p>Getter for the displayRecent field of the {@link KMSPrep}.</p>
     *
     * @return The displayRecent field of the {@link KMSPrep}.
     */
    public boolean getDisplayRecent () {
        return displayRecent;
    }

    /**
     * <p>Setter for the displayRecent field of the {@link KMSPrep}.</p>
     *
     * @param displayRecent The displayRecent field of the {@link KMSPrep}.
     */
    public void setDisplayRecent (boolean displayRecent) {
        this.displayRecent = displayRecent;
    }

    /**
     * <p>Getter for the mirrorKMSStationID field of the {@link KMSPrep}.</p>
     *
     * @return The mirrorKMSStationID field of the {@link KMSPrep}.
     */
    public int getMirrorKMSStationID () {
        return mirrorKMSStationID;
    }

    /**
     * <p>Setter for the mirrorKMSStationID field of the {@link KMSPrep}.</p>
     *
     * @param mirrorKMSStationID The mirrorKMSStationID field of the {@link KMSPrep}.
     */
    public void setMirrorKMSStationID (int mirrorKMSStationID) {
        this.mirrorKMSStationID = mirrorKMSStationID;
    }

    /**
     * <p>Getter for the backupKMSStationID field of the {@link KMSPrep}.</p>
     *
     * @return The backupKMSStationID field of the {@link KMSPrep}.
     */
    public int getBackupKMSStationID () {
        return backupKMSStationID;
    }

    /**
     * <p>Setter for the backupKMSStationID field of the {@link KMSPrep}.</p>
     *
     * @param backupKMSStationID The backupKMSStationID field of the {@link KMSPrep}.
     */
    public void setBackupKMSStationID (int backupKMSStationID) {
        this.backupKMSStationID = backupKMSStationID;
    }

    /**
     * <p>Getter for the titleStyleProfileID field of the {@link KMSPrep}.</p>
     *
     * @return The titleStyleProfileID field of the {@link KMSPrep}.
     */
    public int getTitleStyleProfileID () {
        return titleStyleProfileID;
    }

    /**
     * <p>Setter for the titleStyleProfileID field of the {@link KMSPrep}.</p>
     *
     * @param titleStyleProfileID The titleStyleProfileID field of the {@link KMSPrep}.
     */
    public void setTitleStyleProfileID (int titleStyleProfileID) {
        this.titleStyleProfileID = titleStyleProfileID;
    }

    /**
     * <p>Getter for the contentStyleProfileID field of the {@link KMSPrep}.</p>
     *
     * @return The contentStyleProfileID field of the {@link KMSPrep}.
     */
    public int getContentStyleProfileID () {
        return contentStyleProfileID;
    }

    /**
     * <p>Setter for the contentStyleProfileID field of the {@link KMSPrep}.</p>
     *
     * @param contentStyleProfileID The contentStyleProfileID field of the {@link KMSPrep}.
     */
    public void setContentStyleProfileID (int contentStyleProfileID) {
        this.contentStyleProfileID = contentStyleProfileID;
    }

    /**
     * <p>Getter for the rushOrderStyleProfileID field of the {@link KMSPrep}.</p>
     *
     * @return The rushOrderStyleProfileID field of the {@link KMSPrep}.
     */
    public int getRushOrderStyleProfileID () {
        return rushOrderStyleProfileID;
    }

    /**
     * <p>Setter for the rushOrderStyleProfileID field of the {@link KMSPrep}.</p>
     *
     * @param rushOrderStyleProfileID The rushOrderStyleProfileID field of the {@link KMSPrep}.
     */
    public void setRushOrderStyleProfileID (int rushOrderStyleProfileID) {
        this.rushOrderStyleProfileID = rushOrderStyleProfileID;
    }

    /**
     * <p>Getter for the orderStatusProfileID field of the {@link KMSPrep}.</p>
     *
     * @return The orderStatusProfileID field of the {@link KMSPrep}.
     */
    public int getOrderStatusProfileID () {
        return orderStatusProfileID;
    }

    /**
     * <p>Setter for the orderStatusProfileID field of the {@link KMSPrep}.</p>
     *
     * @param orderStatusProfileID The orderStatusProfileID field of the {@link KMSPrep}.
     */
    public void setOrderStatusProfileID (int orderStatusProfileID) {
        this.orderStatusProfileID = orderStatusProfileID;
    }

    /**
     * <p>Getter for the orderTimerProfileID field of the {@link KMSPrep}.</p>
     *
     * @return The orderTimerProfileID field of the {@link KMSPrep}.
     */
    public int getOrderTimerProfileID () {
        return orderTimerProfileID;
    }

    /**
     * <p>Setter for the orderTimerProfileID field of the {@link KMSPrep}.</p>
     *
     * @param orderTimerProfileID The orderTimerProfileID field of the {@link KMSPrep}.
     */
    public void setOrderTimerProfileID (int orderTimerProfileID) {
        this.orderTimerProfileID = orderTimerProfileID;
    }

    /**
     * <p>Getter for the completedOrderTimeDisplayed field of the {@link KMSPrep}.</p>
     *
     * @return The completedOrderTimeDisplayed field of the {@link KMSPrep}.
     */
    public int getCompletedOrderTimeDisplayed () {
        return completedOrderTimeDisplayed;
    }

    /**
     * <p>Setter for the completedOrderTimeDisplayed field of the {@link KMSPrep}.</p>
     *
     * @param completedOrderTimeDisplayed The completedOrderTimeDisplayed field of the {@link KMSPrep}.
     */
    public void setCompletedOrderTimeDisplayed (int completedOrderTimeDisplayed) {
        this.completedOrderTimeDisplayed = completedOrderTimeDisplayed;
    }

    /**
     * <p>Getter for the bumpProductsToCompleted field of the {@link KMSPrep}.</p>
     *
     * @return The bumpProductsToCompleted field of the {@link KMSPrep}.
     */
    public boolean getBumpProductsToCompleted () {
        return bumpProductsToCompleted;
    }

    /**
     * <p>Setter for the bumpProductsToCompleted field of the {@link KMSPrep}.</p>
     *
     * @param bumpProductsToCompleted The bumpProductsToCompleted field of the {@link KMSPrep}.
     */
    public void setBumpProductsToCompleted (boolean bumpProductsToCompleted) {
        this.bumpProductsToCompleted = bumpProductsToCompleted;
    }

    /**
     * <p>Getter for the displayModsInProgress field of the {@link KMSPrep}.</p>
     *
     * @return The displayModsInProgress field of the {@link KMSPrep}.
     */
    public boolean getDisplayModsInProgress () {
        return displayModsInProgress;
    }

    /**
     * <p>Setter for the displayModsInProgress field of the {@link KMSPrep}.</p>
     *
     * @param displayModsInProgress The displayModsInProgress field of the {@link KMSPrep}.
     */
    public void setDisplayModsInProgress (boolean displayModsInProgress) {
        this.displayModsInProgress = displayModsInProgress;
    }

    /**
     * <p>Getter for the bumpIndividualProducts field of the {@link KMSPrep}.</p>
     *
     * @return The bumpIndividualProducts field of the {@link KMSPrep}.
     */
    public boolean getBumpIndividualProducts () {
        return bumpIndividualProducts;
    }

    /**
     * <p>Setter for the bumpIndividualProducts field of the {@link KMSPrep}.</p>
     *
     * @param bumpIndividualProducts The bumpIndividualProducts field of the {@link KMSPrep}.
     */
    public void setBumpIndividualProducts (boolean bumpIndividualProducts) {
        this.bumpIndividualProducts = bumpIndividualProducts;
    }

    /**
     * <p>Getter for the touchControlStyleProfileID field of the {@link KMSPrep}.</p>
     *
     * @return The touchControlStyleProfileID field of the {@link KMSPrep}.
     */
    public int getTouchControlStyleProfileID () {
        return touchControlStyleProfileID;
    }

    /**
     * <p>Setter for the touchControlStyleProfileID field of the {@link KMSPrep}.</p>
     *
     * @param touchControlStyleProfileID The touchControlStyleProfileID field of the {@link KMSPrep}.
     */
    public void setTouchControlStyleProfileID (int touchControlStyleProfileID) {
        this.touchControlStyleProfileID = touchControlStyleProfileID;
    }

    /**
     * <p>Getter for the printOrdersOnCompleted field of the {@link KMSPrep}.</p>
     *
     * @return The printOrdersOnCompleted field of the {@link KMSPrep}.
     */
    public boolean getPrintOrdersOnCompleted () {
        return printOrdersOnCompleted;
    }

    /**
     * <p>Setter for the printOrdersOnCompleted field of the {@link KMSPrep}.</p>
     *
     * @param printOrdersOnCompleted The printOrdersOnCompleted field of the {@link KMSPrep}.
     */
    public void setPrintOrdersOnCompleted (boolean printOrdersOnCompleted) {
        this.printOrdersOnCompleted = printOrdersOnCompleted;
    }

}