package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-14 16:03:47 -0400 (Thu, 14 May 2020) $: Date of last commit
    $Rev: 11744 $: Revision of last commit
    Notes: Interface that should be implemented by all transaction line items for the factory pattern.
*/

import com.mmhayes.common.dataaccess.DataManager;

import java.util.HashMap;

/**
 * <p>Interface that should be implemented by all transaction line items for the factory pattern.</p>
 *
 */
public interface ITransLineItem {

    /**
     * <p>Sets the data for a transaction line item</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param lineItemRec The {@link HashMap} containing information about the transaction line item.
     */
    void setLineItemData (String log, DataManager dataManager, HashMap lineItemRec);

}