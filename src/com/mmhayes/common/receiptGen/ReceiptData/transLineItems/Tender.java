package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-15 14:50:29 -0400 (Fri, 15 May 2020) $: Date of last commit
    $Rev: 11756 $: Revision of last commit
    Notes: Represents a tender line item.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.AccountData;
import com.mmhayes.common.receiptGen.ReceiptData.CreditCardTxnData;
import com.mmhayes.common.receiptGen.ReceiptData.GiftCardTxnData;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a tender line item.</p>
 *
 */
public class Tender implements ITransLineItem {

    // private member variables of a Tender
    private int tenderID = 0;
    private int tenderTypeID = 0;
    private BigDecimal paTransLineItemID = null;
    private String name = "";
    private String itemComment = "";
    private String commentPromptText = "";
    private double quantity = 0.0d;
    private double refundedQuantity = 0.0d;
    private double amount = 0.0d;
    private double refundedAmount = 0.0d;
    private boolean printAccountNumber = false;
    private AccountData accountData = null;
    private CreditCardTxnData creditCardTxnData = null;
    private GiftCardTxnData giftCardTxnData = null;

    /**
     * <p>Sets the data for a {@link Tender} transaction line item.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param lineItemRec The {@link HashMap} containing information about the transaction line item.
     */
    @Override
    public void setLineItemData (String log, DataManager dataManager, HashMap lineItemRec) {

        if (!DataFunctions.isEmptyMap(lineItemRec)) {
            this.tenderID = HashMapDataFns.getIntVal(lineItemRec, "PAITEMID");
            this.tenderTypeID = HashMapDataFns.getIntVal(lineItemRec, "PATENDERTYPEID");
            String paTransLineItemIDStr = HashMapDataFns.getStringVal(lineItemRec, "PATRANSLINEITEMID");
            if ((StringFunctions.stringHasContent(paTransLineItemIDStr)) && (NumberUtils.isNumber(paTransLineItemIDStr))) {
                this.paTransLineItemID = new BigDecimal(paTransLineItemIDStr);
            }
            this.name = HashMapDataFns.getStringVal(lineItemRec, "ITEMNAME");
            this.itemComment = HashMapDataFns.getStringVal(lineItemRec, "ITEMCOMMENT");
            this.commentPromptText = HashMapDataFns.getStringVal(lineItemRec, "COMMENTPROMPTTEXT");
            this.quantity = HashMapDataFns.getDoubleVal(lineItemRec, "QUANTITY");
            this.refundedQuantity = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDQUANTITY");
            this.amount = HashMapDataFns.getDoubleVal(lineItemRec, "AMOUNT");
            this.refundedAmount = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDAMOUNT");
            this.printAccountNumber = HashMapDataFns.getBooleanVal(lineItemRec, "PRINTACCTNO");

            String accountIDStr = HashMapDataFns.getStringVal(lineItemRec, "EMPLOYEEID");
            int terminalID = HashMapDataFns.getIntVal(lineItemRec, "TERMINALID");
            String badgeAssignmentID = HashMapDataFns.getStringVal(lineItemRec, "BADGEASSIGNMENTID");
            int splits = HashMapDataFns.getIntVal(lineItemRec, "SPLITS");
            if (StringFunctions.stringHasContent(accountIDStr)) {
                this.accountData = new AccountData(dataManager, terminalID, this.amount, accountIDStr, badgeAssignmentID, splits);
            }

            String creditCardTransInfo = HashMapDataFns.getStringVal(lineItemRec, "CREDITCARDTRANSINFO");
            if (StringFunctions.stringHasContent(creditCardTransInfo)) {
                if (this.tenderTypeID == TypeData.TenderType.INTEGRATED_GIFT_CARD) {
                    this.giftCardTxnData = new GiftCardTxnData(creditCardTransInfo, ";");
                }
                else {
                    this.creditCardTxnData = new CreditCardTxnData(creditCardTransInfo, ";");
                }
            }
        }
        else {
            Logger.logMessage("The Tender record can't be acquired from an empty HashMap in Tender.setLineItemData", log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Overridden toString() method for a {@link Tender}.</p>
     *
     * @return A {@link String} representation of this {@link Tender}.
     */
    @Override
    public String toString () {

        return String.format("TENDERID: %s, TENDERTYPEID: %s, PATRANSLINEITEMID: %s, NAME: %s, ITEMCOMMENT: %s, COMMENTPROMPTTEXT: %s, " +
                "QUANTITY: %s, REFUNDEDQUANTITY: %s, AMOUNT: %s, REFUNDEDAMOUNT: %s, PRINTACCOUNTNUMBER: %s, ACCOUNTDATA: %s, CREDITCARDTXNDATA: %s, " +
                "GIFTCARDTXNDATA: %s",
                Objects.toString((tenderID != -1 ? tenderID : "N/A"), "N/A"),
                Objects.toString((tenderTypeID != -1 ? tenderTypeID : "N/A"), "N/A"),
                Objects.toString((((paTransLineItemID != null) && (StringFunctions.stringHasContent(paTransLineItemID.toPlainString()))) ? paTransLineItemID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(itemComment) ? itemComment : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(commentPromptText) ? commentPromptText : "N/A"), "N/A"),
                Objects.toString((quantity != -1.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQuantity != -1.0d ? refundedQuantity : "N/A"), "N/A"),
                Objects.toString((amount != -1.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount != -1.0d ? refundedAmount : "N/A"), "N/A"),
                Objects.toString(printAccountNumber, "N/A"),
                Objects.toString((accountData != null ? accountData.toString() : "N/A"), "N/A"),
                Objects.toString((creditCardTxnData != null ? creditCardTxnData.toString() : "N/A"), "N/A"),
                Objects.toString((giftCardTxnData != null ? giftCardTxnData.toString() : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link Tender}.
     * Two {@link Tender} are defined as being equal if they have the same tenderID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link Tender}.
     * @return Whether or not the {@link Object} is equal to this {@link Tender}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a Tender and check if the obj's tenderID is equal to this Tender's tenderID
        Tender tender = ((Tender) obj);
        return Objects.equals(tender.tenderID, tenderID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link Tender}.</p>
     *
     * @return The unique hash code for this {@link Tender}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(tenderID);
    }

    /**
     * <p>Getter for the tenderID field of the {@link Tender}.</p>
     *
     * @return The tenderID field of the {@link Tender}.
     */
    public int getTenderID () {
        return tenderID;
    }

    /**
     * <p>Setter for the tenderID field of the {@link Tender}.</p>
     *
     * @param tenderID The tenderID field of the {@link Tender}.
     */
    public void setTenderID (int tenderID) {
        this.tenderID = tenderID;
    }

    /**
     * <p>Getter for the tenderTypeID field of the {@link Tender}.</p>
     *
     * @return The tenderTypeID field of the {@link Tender}.
     */
    public int getTenderTypeID () {
        return tenderTypeID;
    }

    /**
     * <p>Setter for the tenderTypeID field of the {@link Tender}.</p>
     *
     * @param tenderTypeID The tenderTypeID field of the {@link Tender}.
     */
    public void setTenderTypeID (int tenderTypeID) {
        this.tenderTypeID = tenderTypeID;
    }

    /**
     * <p>Getter for the paTransLineItemID field of the {@link Tender}.</p>
     *
     * @return The paTransLineItemID field of the {@link Tender}.
     */
    public BigDecimal getPaTransLineItemID () {
        return paTransLineItemID;
    }

    /**
     * <p>Setter for the paTransLineItemID field of the {@link Tender}.</p>
     *
     * @param paTransLineItemID The paTransLineItemID field of the {@link Tender}.
     */
    public void setPaTransLineItemID (BigDecimal paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
    }

    /**
     * <p>Getter for the name field of the {@link Tender}.</p>
     *
     * @return The name field of the {@link Tender}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link Tender}.</p>
     *
     * @param name The name field of the {@link Tender}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the itemComment field of the {@link Tender}.</p>
     *
     * @return The itemComment field of the {@link Tender}.
     */
    public String getItemComment () {
        return itemComment;
    }

    /**
     * <p>Setter for the itemComment field of the {@link Tender}.</p>
     *
     * @param itemComment The itemComment field of the {@link Tender}.
     */
    public void setItemComment (String itemComment) {
        this.itemComment = itemComment;
    }

    /**
     * <p>Getter for the commentPromptText field of the {@link Tender}.</p>
     *
     * @return The commentPromptText field of the {@link Tender}.
     */
    public String getCommentPromptText () {
        return commentPromptText;
    }

    /**
     * <p>Setter for the commentPromptText field of the {@link Tender}.</p>
     *
     * @param commentPromptText The commentPromptText field of the {@link Tender}.
     */
    public void setCommentPromptText (String commentPromptText) {
        this.commentPromptText = commentPromptText;
    }

    /**
     * <p>Getter for the quantity field of the {@link Tender}.</p>
     *
     * @return The quantity field of the {@link Tender}.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link Tender}.</p>
     *
     * @param quantity The quantity field of the {@link Tender}.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the refundedQuantity field of the {@link Tender}.</p>
     *
     * @return The refundedQuantity field of the {@link Tender}.
     */
    public double getRefundedQuantity () {
        return refundedQuantity;
    }

    /**
     * <p>Setter for the refundedQuantity field of the {@link Tender}.</p>
     *
     * @param refundedQuantity The refundedQuantity field of the {@link Tender}.
     */
    public void setRefundedQuantity (double refundedQuantity) {
        this.refundedQuantity = refundedQuantity;
    }

    /**
     * <p>Getter for the amount field of the {@link Tender}.</p>
     *
     * @return The amount field of the {@link Tender}.
     */
    public double getAmount () {
        return amount;
    }

    /**
     * <p>Setter for the amount field of the {@link Tender}.</p>
     *
     * @param amount The amount field of the {@link Tender}.
     */
    public void setAmount (double amount) {
        this.amount = amount;
    }

    /**
     * <p>Getter for the refundedAmount field of the {@link Tender}.</p>
     *
     * @return The refundedAmount field of the {@link Tender}.
     */
    public double getRefundedAmount () {
        return refundedAmount;
    }

    /**
     * <p>Setter for the refundedAmount field of the {@link Tender}.</p>
     *
     * @param refundedAmount The refundedAmount field of the {@link Tender}.
     */
    public void setRefundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    /**
     * <p>Getter for the printAccountNumber field of the {@link Tender}.</p>
     *
     * @return The printAccountNumber field of the {@link Tender}.
     */
    public boolean getPrintAccountNumber () {
        return printAccountNumber;
    }

    /**
     * <p>Setter for the printAccountNumber field of the {@link Tender}.</p>
     *
     * @param printAccountNumber The printAccountNumber field of the {@link Tender}.
     */
    public void setPrintAccountNumber (boolean printAccountNumber) {
        this.printAccountNumber = printAccountNumber;
    }

    /**
     * <p>Getter for the accountData field of the {@link Tender}.</p>
     *
     * @return The accountData field of the {@link Tender}.
     */
    public AccountData getAccountData () {
        return accountData;
    }

    /**
     * <p>Setter for the accountData field of the {@link Tender}.</p>
     *
     * @param accountData The accountData field of the {@link Tender}.
     */
    public void setAccountData (AccountData accountData) {
        this.accountData = accountData;
    }

    /**
     * <p>Getter for the creditCardTxnData field of the {@link Tender}.</p>
     *
     * @return The creditCardTxnData field of the {@link Tender}.
     */
    public CreditCardTxnData getCreditCardTxnData () {
        return creditCardTxnData;
    }

    /**
     * <p>Setter for the creditCardTxnData field of the {@link Tender}.</p>
     *
     * @param creditCardTxnData The creditCardTxnData field of the {@link Tender}.
     */
    public void setCreditCardTxnData (CreditCardTxnData creditCardTxnData) {
        this.creditCardTxnData = creditCardTxnData;
    }

    /**
     * <p>Getter for the giftCardTxnData field of the {@link Tender}.</p>
     *
     * @return The giftCardTxnData field of the {@link Tender}.
     */
    public GiftCardTxnData getGiftCardTxnData () {
        return giftCardTxnData;
    }

    /**
     * <p>Setter for the giftCardTxnData field of the {@link Tender}.</p>
     *
     * @param giftCardTxnData The giftCardTxnData field of the {@link Tender}.
     */
    public void setGiftCardTxnData (GiftCardTxnData giftCardTxnData) {
        this.giftCardTxnData = giftCardTxnData;
    }

}