package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-14 16:03:47 -0400 (Thu, 14 May 2020) $: Date of last commit
    $Rev: 11744 $: Revision of last commit
    Notes: Factory to create transaction line items.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Factory to create transaction line items.</p>
 *
 */
public class TransLineItemFactory {

    /**
     * <p>Builds a transaction line item of the given type and sets the data.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param transLineItemType The type of transaction line item to build.
     * @param lineItemRec The {@link HashMap} containing information about the transaction line item.
     */
    public ITransLineItem buildTransLineItemFromHM (String log, DataManager dataManager, int transLineItemType, HashMap lineItemRec) {
        ITransLineItem transLineItem = null;

        try {
            if (transLineItemType > 0) {
                switch (transLineItemType) {
                    case TransLineItemType.COMBO:
                        transLineItem = new Combo();
                        transLineItem.setLineItemData(log, dataManager, lineItemRec);
                        break;
                    case TransLineItemType.DISCOUNT:
                        transLineItem = new Discount();
                        transLineItem.setLineItemData(log, dataManager, lineItemRec);
                        break;
                    case TransLineItemType.LOYALTY_REWARD:
                        transLineItem = new LoyaltyReward();
                        transLineItem.setLineItemData(log, dataManager, lineItemRec);
                        break;
                    case TransLineItemType.PAID_OUT:
                        transLineItem = new PaidOut();
                        transLineItem.setLineItemData(log, dataManager, lineItemRec);
                        break;
                    case TransLineItemType.PRODUCT:
                        transLineItem = new Product();
                        transLineItem.setLineItemData(log, dataManager, lineItemRec);
                        break;
                    case TransLineItemType.RECEIVED_ON_ACCOUNT:
                        transLineItem = new ReceivedOnAccount();
                        transLineItem.setLineItemData(log, dataManager, lineItemRec);
                        break;
                    case TransLineItemType.SURCHARGE:
                        transLineItem = new Surcharge();
                        transLineItem.setLineItemData(log, dataManager, lineItemRec);
                        break;
                    case TransLineItemType.TAX_RATE:
                        transLineItem = new TaxRate();
                        transLineItem.setLineItemData(log, dataManager, lineItemRec);
                        break;
                    case TransLineItemType.TENDER:
                        transLineItem = new Tender();
                        transLineItem.setLineItemData(log, dataManager, lineItemRec);
                        break;
                    default:
                        Logger.logMessage(String.format("Encountered an unknown transaction line item type of %s in TransLineItemFactory.buildTransLineItemFromHM",
                                Objects.toString(transLineItemType, "N/A")), log, Logger.LEVEL.ERROR);
                        break;
                }
            }
            else {
                Logger.logMessage("No valid transaction line item type was provided in TransLineItemFactory.buildTransLineItemFromHM", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build the transaction line item of type %s from the given HashMap in TransLineItemFactory.buildTransLineItemFromHM",
                    Objects.toString(transLineItemType, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return transLineItem;
    }

}