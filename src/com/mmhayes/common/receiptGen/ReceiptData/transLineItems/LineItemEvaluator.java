package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-14 16:03:47 -0400 (Thu, 14 May 2020) $: Date of last commit
    $Rev: 11744 $: Revision of last commit
    Notes: Analyzes the line items in a transaction and modifies them appropriately.
*/

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.LoyaltyRewardData;
import com.mmhayes.common.receiptGen.ReceiptData.TransactionData;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep.IKitchenPrep;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.kitchenPrep.KitchenPrepFactory;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Analyzes the line items in a transaction and modifies them appropriately.</p>
 *
 */
public class LineItemEvaluator {

    // private member variables of a LineItemEvaluator
    private String log;
    private DataManager dataManager;
    private TransactionData transactionData;
    private ArrayList<ITransLineItem> transLineItems;

    private HashMap<Integer, Discount> subtotalDiscounts = new HashMap<>();
    private ArrayList<Discount> ordererdSubtotalDiscounts = new ArrayList<>();
    private ArrayList<Discount> itemDiscounts = new ArrayList<>();
    private HashMap<Integer, Product> products = new HashMap<>(); // use Map for O(1) lookup in LineItemEvaluator.getProductByID
    private ArrayList<Surcharge> itemSurcharges = new ArrayList<>();
    private ArrayList<Surcharge> subtotalSurcharges = new ArrayList<>();
    private HashMap<Integer, TaxRate> taxRates = new HashMap<>();

    /**
     * <p>Constructor for a {@link LineItemEvaluator}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param transactionData The {@link TransactionData} related to the transaction.
     * @param transLineItems An {@link ArrayList} of {@link ITransLineItem} corresponding to the line items in a transaction.
     */
    public LineItemEvaluator (String log, DataManager dataManager, TransactionData transactionData, ArrayList<ITransLineItem> transLineItems) {
        this.log = log;
        this.dataManager = dataManager;
        this.transactionData = transactionData;
        this.transLineItems = transLineItems;
        analyzeLineItems();
    }

    /**
     * <p>Analyzes the lines items in the transactions and makes modifications to them as necessary.</p>
     *
     */
    private void analyzeLineItems () {

        try {
            if (!DataFunctions.isEmptyCollection(transLineItems)) {
                for (ITransLineItem lineItem : transLineItems) {
                    if (lineItem instanceof Combo) {
                        Combo combo = ((Combo) lineItem);
                        setComboRootPrice(combo);
                    }
                    else if (lineItem instanceof Discount) {
                        Discount discount = ((Discount) lineItem);
                        if (discount.getIsSubtotalDiscount()) {
                            subtotalDiscounts.put(discount.getDiscountID(), discount);
                            ordererdSubtotalDiscounts.add(discount);
                        }
                        else {
                            itemDiscounts.add(discount);
                        }
                    }
                    else if (lineItem instanceof Product) {
                        Product product = ((Product) lineItem);
                        setParentProductAndModifiers(product);
                        setLoyaltyRewardsForProduct(product);
                        setTaxDiscountRewardDetailsForProduct(product);
                        products.put(product.getProductID(), product);
                    }
                    else if (lineItem instanceof Surcharge) {
                        Surcharge surcharge = ((Surcharge) lineItem);
                        if (surcharge.isSubtotalSurcharge()) {
                            subtotalSurcharges.add(surcharge);
                        }
                        else {
                            itemSurcharges.add(surcharge);
                        }
                    }
                    else if (lineItem instanceof TaxRate) {
                        TaxRate taxRate = ((TaxRate) lineItem);
                        int taxRateID = taxRate.getTaxRateID();
                        if (taxRates.containsKey(taxRateID)) {
                            TaxRate existingTaxRate = taxRates.get(taxRateID);
                            BigDecimal existingTaxRateAmount = new BigDecimal(existingTaxRate.getExtendedAmount()).setScale(2, RoundingMode.HALF_UP);
                            BigDecimal currentTaxRateAmount = new BigDecimal(taxRate.getExtendedAmount()).setScale(2, RoundingMode.HALF_UP);
                            BigDecimal newTaxRateAmount = existingTaxRateAmount.add(currentTaxRateAmount);
                            existingTaxRate.setAmount(newTaxRateAmount.doubleValue());
                        }
                        else {
                            taxRates.put(taxRateID, taxRate);
                        }
                    }
                }
                if (!DataFunctions.isEmptyMap(products)) {
                    setKitchenPrepsMappedToProducts();
                }
            }
            else {
                Logger.logMessage("No line items found within the transaction! Unable to analyze the transaction's line items in LineItemEvaluator.analyzeLineItems", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("A problem occurred while analyzing the transaction's line items in LineItemEvaluator.analyzeLineItems", log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Calculates and sets the root price of the combo.</p>
     *
     * @param combo The {@link Combo} to calculate the root price for.
     */
    @SuppressWarnings("Convert2streamapi")
    private void setComboRootPrice (Combo combo) {

        try {
            if (combo != null) {
                // calculate the root price of the combo
                if (combo.getRootPrice() == 0.0d) {
                    double rootPrice = 0.0d;
                    BigDecimal comboTransLineItemID = combo.getPaTransLineItemID();
                    for (ITransLineItem lineItem : transLineItems) {
                        if (lineItem instanceof Product) {
                            Product product = ((Product) lineItem);
                            BigDecimal productComboTransLineItemID = product.getPaComboTransLineItemID();
                            if (comboTransLineItemID.equals(productComboTransLineItemID)) {
                                double comboPrice = product.getComboPrice();
                                rootPrice += comboPrice;
                            }
                        }
                    }
                    // round the root price
                    rootPrice = new BigDecimal(rootPrice).setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
                    // set the root price for the combo
                    combo.setRootPrice(rootPrice);
                }
            }
            else {
                Logger.logMessage("The combo passed to LineItemEvaluator.setRootComboPrice can't be null!", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to calculate the root price for the combo %s in LineItemEvaluator.setRootComboPrice",
                    Objects.toString((combo != null ? combo.getName() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Sets the parent {@link Product} or any modifiers for the given {@link Product}.</p>
     *
     * @param product The {@link Product} to get the parent product or any modifiers for.
     */
    private void setParentProductAndModifiers (Product product) {

        try {
            if (product != null) {
                if ((product.getPaTransLineItemModID().compareTo(BigDecimal.ZERO) > 0) && (product.getIsModifier())) {
                    Product parentProduct = getParentProduct(product.getPaTransLineItemID());
                    if (parentProduct != null) {
                        parentProduct.addModifier(product);
                        product.setParentProduct(parentProduct);
                    }
                }
            }
            else {
                Logger.logMessage("The product passed to LineItemEvaluator.setParentProductAndModifiers can't be null!", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("A problem occurred while trying to determine the parent product or any modifiers for the product %s in LineItemEvaluator.setParentProductAndModifiers",
                    Objects.toString((product != null ? product.getName() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Gets the {@link Product} with the given transaction line item ID for the transaction line items.</p>
     *
     * @param paTransLineItemID The transaction line item ID as a {@link BigDecimal} of the {@link Product} to get.
     * @return The {@link Product} with the given transaction line item ID.
     */
    private Product getParentProduct (BigDecimal paTransLineItemID) {
        Product parentProduct = null;

        try {
            if (paTransLineItemID != null) {
                for (ITransLineItem lineItem : transLineItems) {
                    if (lineItem instanceof Product) {
                        Product product = ((Product) lineItem);
                        if (product.getPaTransLineItemID().equals(paTransLineItemID)) {
                            // the parent product has been found
                            parentProduct = product;
                            break;
                        }
                    }
                }
            }
            else {
                Logger.logMessage("The transaction line item ID passed to LineItemEvaluator.getParentProduct can't be null!", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to get the product with a transaction line item ID of %s in LineItemEvaluator.getParentProduct",
                    Objects.toString((paTransLineItemID != null ? paTransLineItemID.toPlainString() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return parentProduct;
    }

    /**
     * <p>Finds and sets any loyalty rewards that are mapped to the given {@link Product}.</p>
     *
     * @param product The {@link Product} to get any mapped loyalty rewards for.
     */
    @SuppressWarnings("unchecked")
    private void setLoyaltyRewardsForProduct (Product product) {

        try {
            if (product != null) {
                // query the database to get loyalty rewards mapped to the product
                if (dataManager != null) {
                    ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(dataManager.parameterizedExecuteQuery("data.ReceiptGen.GetProductMappedRewardsV2", new Object[]{product.getProductID()}, true));
                    if (!DataFunctions.isEmptyCollection(queryRes)) {
                        for (HashMap hm : queryRes) {
                            // add the loyalty reward to the products ArrayList of loyalty rewards
                            product.addLoyaltyReward(LoyaltyRewardData.buildFromHM(log, hm));
                        }
                    }
                }
                else {
                    Logger.logMessage("Encountered an invalid DataManager in LineItemEvaluator.setLoyaltyRewardsForProduct, the DataManager can't be null!", log, Logger.LEVEL.ERROR);
                }
            }
            else {
                Logger.logMessage("The product passed to LineItemEvaluator.setLoyaltyRewardsForProduct can't be null!", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to find and set any loyalty rewards for the product %s in LineItemEvaluator.setLoyaltyRewardsForProduct",
                    Objects.toString((product != null ? product.getName() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Sets the tax, discount and loyalty reward details for the given {@link Product}.</p>
     *
     * @param product The {@link Product} to set the tax, discount and loyalty reward details for.
     */
    @SuppressWarnings("unchecked")
    private void setTaxDiscountRewardDetailsForProduct (Product product) {

        try {
            if (product != null) {
                if ((product.getPaTransLineItemModID().compareTo(BigDecimal.ZERO) <= 0) && (!product.getIsModifier())) {
                    if (dataManager != null) {
                        ArrayList<HashMap> queryRes =
                                DataFunctions.purgeAlOfHm(dataManager.parameterizedExecuteQuery("data.ReceiptGen.GetItemTaxDsctDetails", new Object[]{product.getPaTransLineItemModID().intValue()}, true));
                        for (HashMap hm : queryRes) {
                            int itemTypeID = HashMapDataFns.getIntVal(hm, "ITEMTYPEID");
                            int itemID = HashMapDataFns.getIntVal(hm, "ITEMID");
                            double amount = HashMapDataFns.getDoubleVal(hm, "AMOUNT");
                            String shortName = "";

                            switch (itemTypeID) {
                                case TypeData.ItemType.DISCOUNT:
                                    Discount discount;
                                    if ((!DataFunctions.isEmptyMap(subtotalDiscounts)) && (subtotalDiscounts.containsKey(itemID))) {
                                        discount = subtotalDiscounts.get(itemID);
                                    }
                                    else {
                                        discount = getItemDiscount(itemID, amount);
                                        if ((discount != null) && (product.getItemDiscount() == null)) {
                                            product.setItemDiscount(discount);
                                        }
                                    }
                                    if (discount != null) {
                                        shortName = discount.getShortName();
                                    }
                                    break;
                                case TypeData.ItemType.LOYALTY_REWARD:
                                    LoyaltyRewardData loyaltyReward = LoyaltyRewardData.buildFromID(log, dataManager, itemID);
                                    if (loyaltyReward != null) {
                                        shortName = loyaltyReward.getShortName();
                                        // check if this is a free product reward and if it is set it for the product
                                        if (loyaltyReward.getLoyaltyRewardTypeID() == PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS.toInt()) {
                                            product.addFreeProductReward(loyaltyReward);
                                        }
                                    }
                                    break;
                                case TypeData.ItemType.SURCHARGE:
                                    Surcharge surcharge = getSurcharge(itemID, amount);
                                    if (surcharge != null) {
                                        shortName = surcharge.getShortName();
                                        // if it's an item surcharge set it for the product
                                        if (!surcharge.isSubtotalSurcharge()) {
                                            product.addItemSurcharge(surcharge);
                                        }
                                    }
                                    break;
                                case TypeData.ItemType.TAX:
                                    if ((!DataFunctions.isEmptyMap(taxRates)) && (taxRates.containsKey(itemID))) {
                                        shortName = taxRates.get(itemID).getShortName();
                                    }
                                    break;
                                default:
                                    Logger.logMessage(String.format("Encountered an invalid item type ID of %s in LineItemEvaluatorsetTaxDiscountRewardDetailsForProduct",
                                            Objects.toString(itemTypeID, "N/A")), log, Logger.LEVEL.ERROR);
                                    break;
                            }

                            if (StringFunctions.stringHasContent(shortName)) {
                                product.addToTaxDiscountStr(shortName);
                            }
                        }
                        linkMissingItemSurchargeRecords(product);
                    }
                    else {
                        Logger.logMessage("Encountered an invalid DataManager in LineItemEvaluator.setTaxDiscountRewardDetailsForProduct, the DataManager can't be null!", log, Logger.LEVEL.ERROR);
                    }
                }
            }
            else {
                Logger.logMessage("The product passed to LineItemEvaluator.setTaxDiscountRewardDetailsForProduct can't be null!", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to find and set the tax, discount and loyalty reward details for the product %s in LineItemEvaluator.setTaxDiscountRewardDetailsForProduct",
                    Objects.toString((product != null ? product.getName() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Iterates through the item discount transaction line items and tries to find the item discount with the given discount ID.</p>
     *
     * @param discountID The ID of the item discount to search for.
     * @param amount The amount the item discount should be for.
     * @return The item {@link Discount} with the given discount ID in the transaction line items.
     */
    private Discount getItemDiscount (int discountID, double amount) {
        Discount discount = null;

        try {
            if (!DataFunctions.isEmptyCollection(itemDiscounts)) {
                for (Discount itemDiscount : itemDiscounts) {
                    if ((itemDiscount.getDiscountID() == discountID) && (itemDiscount.getExtendedAmount() == amount)) {
                        discount = itemDiscount;
                        break;
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to find the item discount with an ID of %s in the transaction line items in LineItemEvaluator.getItemDiscount",
                    Objects.toString(discountID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return discount;
    }

    /**
     * <p>Iterates through the surcharge line items and tries to find the surcharge with the given surcharge ID.</p>
     *
     * @param surchargeID The ID of the surcharge to search for.
     * @param amount The amount the surcharge should be for.
     * @return The item {@link Discount} with the given surcharge ID in the transaction line items.
     */
    private Surcharge getSurcharge (int surchargeID, double amount) {
        Surcharge surcharge = null;

        try {
            // combine subtotal ans item surcharges
            ArrayList<Surcharge> allSurcharges = new ArrayList<>();
            if (!DataFunctions.isEmptyCollection(subtotalSurcharges)) {
                allSurcharges.addAll(subtotalSurcharges);
            }
            if (!DataFunctions.isEmptyCollection(itemSurcharges)) {
                allSurcharges.addAll(itemSurcharges);
            }

            // iterate through all the surcharges to find the surcharge with the given surcharge ID
            if (!DataFunctions.isEmptyCollection(allSurcharges)) {
                for (Surcharge aSurcharge : allSurcharges) {
                    if (aSurcharge != null) {
                        if ((surchargeID == aSurcharge.getSurchargeID()) && (amount == aSurcharge.getAmount())) {
                            surcharge = aSurcharge;
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to find the surcharge with an ID of %s in the transaction line items in LineItemEvaluator.getSurcharge",
                    Objects.toString(surchargeID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return surcharge;
    }

    /**
     * <p>Links missing item {@link Surcharge} records to the given {@link Product}.</p>
     *
     * @param product The {@link Product} to link the missing item {@link Surcharge} records to.
     */
    private void linkMissingItemSurchargeRecords (Product product) {

        try {
            if (product != null) {
                if (!DataFunctions.isEmptyCollection(itemSurcharges)) {
                    for (Surcharge itemSurcharge : itemSurcharges) {
                        if ((itemSurcharge != null) && (itemSurcharge.getLinkedPATransLineItemID().compareTo(BigDecimal.ZERO) > 0)
                                && (itemSurcharge.getLinkedPATransLineItemID().equals(product.getPaTransLineItemID()))) {
                            // check if the item surcharge is already mapped to the product
                            boolean productHasSurchargeMapped = false;
                            ArrayList<Surcharge> productItemSurcharges = product.getItemSurcharges();
                            if (!DataFunctions.isEmptyCollection(productItemSurcharges)) {
                                for (Surcharge productItemSurcharge : productItemSurcharges) {
                                    if (itemSurcharge.getSurchargeID() == productItemSurcharge.getSurchargeID()) {
                                        productHasSurchargeMapped = true;
                                        break;
                                    }
                                }
                            }

                            // if the item surcharge isn't mapped to the product yet then add it
                            if (!productHasSurchargeMapped) {
                                Surcharge newItemSurcharge = getSurcharge(itemSurcharge.getSurchargeID(), itemSurcharge.getAmount());
                                if (newItemSurcharge != null) {
                                    product.addItemSurcharge(newItemSurcharge);
                                    if (StringFunctions.stringHasContent(newItemSurcharge.getShortName())) {
                                        product.addToTaxDiscountStr(newItemSurcharge.getShortName());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                Logger.logMessage("The product passed to LineItemEvaluator.linkMissingItemSurchargeRecords can't be null!", log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to link the item surcharge records to the product %s in LineItemEvaluator.linkMissingItemSurchargeRecords",
                    Objects.toString((product != null ? product.getName() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Queries the database to get all KP/KDS/KMS preps mapped to the products within the transaction.</p>
     *
     */
    private void setKitchenPrepsMappedToProducts () {

        try {
            if (!DataFunctions.isEmptyMap(products)) {
                if (dataManager != null) {
                    ArrayList<Integer> productIDs = getProductIDs();
                    // determine the revenue center in which the transaction took place
                    int revenueCenterID = 0;
                    if ((transactionData != null) && (transactionData.getTxnHdrData() != null)) {
                        revenueCenterID = transactionData.getTxnHdrData().getRevenueCenterID();
                    }
                    if ((revenueCenterID > 0) && (!DataFunctions.isEmptyCollection(productIDs))) {
                        // query the database to get all mapped KP/KDS/KMS preps
                        DynamicSQL dynamicSQL = new DynamicSQL("data.ReceiptGen.GetAllKitchenPrepsMappedToProducts").addGenericIDList(1, productIDs).addIDList(2, String.valueOf(revenueCenterID));
                        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(dynamicSQL.serialize(dataManager));
                        if (!DataFunctions.isEmptyCollection(queryRes)) {
                            // create the kitchen prep factory
                            KitchenPrepFactory kitchenPrepFactory = new KitchenPrepFactory();
                            for (HashMap hm : queryRes) {
                                int productID = HashMapDataFns.getIntVal(hm, "PAPLUID");
                                int kitchenPrepType = HashMapDataFns.getIntVal(hm, "KITCHENPREPTYPE");
                                Product product = getProductByID(productID);
                                if (product != null) {
                                    IKitchenPrep kitchenPrep = kitchenPrepFactory.buildKitchenPrepFromHM(log, kitchenPrepType, hm);
                                    if (kitchenPrep != null) {
                                        // add the KP/KDS/KMS kitchen prep item to the product
                                        product.addKichenPrep(kitchenPrep);
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    Logger.logMessage("Encountered an invalid DataManager in LineItemEvaluator.setKitchenPrepsMappedToProducts, the DataManager can't be null!", log, Logger.LEVEL.ERROR);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
        }

    }

    /**
     * <p>Gets the IDs from the list of products in the transaction.</p>
     *
     * @return An {@link ArrayList} of {@link Integer} corresponding to the IDs of the products in the transaction.
     */
    @SuppressWarnings("Convert2streamapi")
    private ArrayList<Integer> getProductIDs () {
        ArrayList<Integer> productIDs = new ArrayList<>();

        if (!DataFunctions.isEmptyMap(products)) {
            for (Product product : products.values()) {
                if (product != null) {
                    productIDs.add(product.getProductID());
                }
            }
        }
        else {
            Logger.logMessage("No products within the transaction, unable to determine the product IDs in LineItemEvaluator.getProductIDs", log, Logger.LEVEL.ERROR);
        }

        return productIDs;
    }

    /**
     * <p>Gets the {@link Product} with the given product ID from the products within the transaction.</p>
     *
     * @param productID The ID of the {@link Product} to find.
     * @return The {@link Product} with the given product ID from the products within the transaction.
     */
    private Product getProductByID (int productID) {
        Product product = null;

        if (!DataFunctions.isEmptyMap(products)) {
            if (products.containsKey(productID)) {
                product = products.get(productID);
            }
        }
        else {
            Logger.logMessage(String.format("No products within the transaction, unable to determine find the product with an ID of %s within the " +
                    "products in the transaction in LineItemEvaluator.getProductByID",
                    Objects.toString(productID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return product;
    }

}