package com.mmhayes.common.receiptGen.ReceiptData.transLineItems;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-15 10:25:09 -0400 (Fri, 15 May 2020) $: Date of last commit
    $Rev: 11752 $: Revision of last commit
    Notes: Represents a paid out line item.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.GiftCardTxnData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a paid out line item.</p>
 *
 */
public class PaidOut implements ITransLineItem {

    // private member variables of a PaidOut
    private int paidOutID = 0;
    private int paidOutTypeID = 0;
    private BigDecimal paTransLineItemID = null;
    private String name = "";
    private String itemComment = "";
    private double quantity = 0.0d;
    private double refundedQuantity = 0.0d;
    private double amount = 0.0d;
    private double refundedAmount = 0.0d;
    private GiftCardTxnData giftCardTxnData = null;

    /**
     * <p>Sets the data for a {@link PaidOut} transaction line item.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param lineItemRec The {@link HashMap} containing information about the transaction line item.
     */
    @Override
    public void setLineItemData (String log, DataManager dataManager, HashMap lineItemRec) {

        if (!DataFunctions.isEmptyMap(lineItemRec)) {
            this.paidOutID = HashMapDataFns.getIntVal(lineItemRec, "PAITEMID");
            this.paidOutTypeID = HashMapDataFns.getIntVal(lineItemRec, "PAPAIDOUTTYPEID");
            String paTransLineItemIDStr = HashMapDataFns.getStringVal(lineItemRec, "PATRANSLINEITEMID");
            if ((StringFunctions.stringHasContent(paTransLineItemIDStr)) && (NumberUtils.isNumber(paTransLineItemIDStr))) {
                this.paTransLineItemID = new BigDecimal(paTransLineItemIDStr);
            }
            this.name = HashMapDataFns.getStringVal(lineItemRec, "ITEMNAME");
            this.itemComment = HashMapDataFns.getStringVal(lineItemRec, "ITEMCOMMENT");
            this.quantity = HashMapDataFns.getDoubleVal(lineItemRec, "QUANTITY");
            this.refundedQuantity = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDQUANTITY");
            this.amount = HashMapDataFns.getDoubleVal(lineItemRec, "AMOUNT");
            this.refundedAmount = HashMapDataFns.getDoubleVal(lineItemRec, "REFUNDEDAMOUNT");
            String gcTxnDataStr = HashMapDataFns.getStringVal(lineItemRec, "CREDITCARDTRANSINFO");
            if (StringFunctions.stringHasContent(gcTxnDataStr)) {
                this.giftCardTxnData = new GiftCardTxnData(gcTxnDataStr, ";");
            }
        }
        else {
            Logger.logMessage("The PaidOut record can't be acquired from an empty HashMap in PaidOut.setLineItemData", log, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Overridden toString() method for a {@link PaidOut}.</p>
     *
     * @return A {@link String} representation of this {@link PaidOut}.
     */
    @Override
    public String toString () {

        return String.format("PAIDOUTID: %s, PAIDOUTTYPEID: %s, PATRANSLINEITEMID: %s, NAME: %s, ITEMCOMMENT: %s, QUANTITY: %s, " +
                "REFUNDEDQUANTITY: %s, AMOUNT: %s, REFUNDEDAMOUNT: %s, GIFTCARDTXNDATA: %s",
                Objects.toString((paidOutID != -1 ? paidOutID : "N/A"), "N/A"),
                Objects.toString((paidOutTypeID != -1 ? paidOutTypeID : "N/A"), "N/A"),
                Objects.toString((((paTransLineItemID != null) && (StringFunctions.stringHasContent(paTransLineItemID.toPlainString()))) ? paTransLineItemID.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(itemComment) ? itemComment : "N/A"), "N/A"),
                Objects.toString((quantity != -1.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQuantity != -1.0d ? refundedQuantity : "N/A"), "N/A"),
                Objects.toString((amount != -1.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount != -1.0d ? refundedAmount : "N/A"), "N/A"),
                Objects.toString((giftCardTxnData != null ? giftCardTxnData.toString() : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link PaidOut}.
     * Two {@link PaidOut} are defined as being equal if they have the same paidOutID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link PaidOut}.
     * @return Whether or not the {@link Object} is equal to this {@link PaidOut}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a PaidOut and check if the obj's paidOutID is equal to this PaidOut's paidOutID
        PaidOut paidOut = ((PaidOut) obj);
        return Objects.equals(paidOut.paidOutID, paidOutID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link PaidOut}.</p>
     *
     * @return The unique hash code for this {@link PaidOut}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(paidOutID);
    }

    /**
     * <p>Getter for the paidOutID field of the {@link PaidOut}.</p>
     *
     * @return The paidOutID field of the {@link PaidOut}.
     */
    public int getPaidOutID () {
        return paidOutID;
    }

    /**
     * <p>Setter for the paidOutID field of the {@link PaidOut}.</p>
     *
     * @param paidOutID The paidOutID field of the {@link PaidOut}.
     */
    public void setPaidOutID (int paidOutID) {
        this.paidOutID = paidOutID;
    }

    /**
     * <p>Getter for the paidOutTypeID field of the {@link PaidOut}.</p>
     *
     * @return The paidOutTypeID field of the {@link PaidOut}.
     */
    public int getPaidOutTypeID () {
        return paidOutTypeID;
    }

    /**
     * <p>Setter for the paidOutTypeID field of the {@link PaidOut}.</p>
     *
     * @param paidOutTypeID The paidOutTypeID field of the {@link PaidOut}.
     */
    public void setPaidOutTypeID (int paidOutTypeID) {
        this.paidOutTypeID = paidOutTypeID;
    }

    /**
     * <p>Getter for the paTransLineItemID field of the {@link PaidOut}.</p>
     *
     * @return The paTransLineItemID field of the {@link PaidOut}.
     */
    public BigDecimal getPaTransLineItemID () {
        return paTransLineItemID;
    }

    /**
     * <p>Setter for the paTransLineItemID field of the {@link PaidOut}.</p>
     *
     * @param paTransLineItemID The paTransLineItemID field of the {@link PaidOut}.
     */
    public void setPaTransLineItemID (BigDecimal paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
    }

    /**
     * <p>Getter for the name field of the {@link PaidOut}.</p>
     *
     * @return The name field of the {@link PaidOut}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link PaidOut}.</p>
     *
     * @param name The name field of the {@link PaidOut}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the itemComment field of the {@link PaidOut}.</p>
     *
     * @return The itemComment field of the {@link PaidOut}.
     */
    public String getItemComment () {
        return itemComment;
    }

    /**
     * <p>Setter for the itemComment field of the {@link PaidOut}.</p>
     *
     * @param itemComment The itemComment field of the {@link PaidOut}.
     */
    public void setItemComment (String itemComment) {
        this.itemComment = itemComment;
    }

    /**
     * <p>Getter for the quantity field of the {@link PaidOut}.</p>
     *
     * @return The quantity field of the {@link PaidOut}.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link PaidOut}.</p>
     *
     * @param quantity The quantity field of the {@link PaidOut}.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the refundedQuantity field of the {@link PaidOut}.</p>
     *
     * @return The refundedQuantity field of the {@link PaidOut}.
     */
    public double getRefundedQuantity () {
        return refundedQuantity;
    }

    /**
     * <p>Setter for the refundedQuantity field of the {@link PaidOut}.</p>
     *
     * @param refundedQuantity The refundedQuantity field of the {@link PaidOut}.
     */
    public void setRefundedQuantity (double refundedQuantity) {
        this.refundedQuantity = refundedQuantity;
    }

    /**
     * <p>Getter for the amount field of the {@link PaidOut}.</p>
     *
     * @return The amount field of the {@link PaidOut}.
     */
    public double getAmount () {
        return amount;
    }

    /**
     * <p>Setter for the amount field of the {@link PaidOut}.</p>
     *
     * @param amount The amount field of the {@link PaidOut}.
     */
    public void setAmount (double amount) {
        this.amount = amount;
    }

    /**
     * <p>Getter for the refundedAmount field of the {@link PaidOut}.</p>
     *
     * @return The refundedAmount field of the {@link PaidOut}.
     */
    public double getRefundedAmount () {
        return refundedAmount;
    }

    /**
     * <p>Setter for the refundedAmount field of the {@link PaidOut}.</p>
     *
     * @param refundedAmount The refundedAmount field of the {@link PaidOut}.
     */
    public void setRefundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    /**
     * <p>Getter for the giftCardTxnData field of the {@link PaidOut}.</p>
     *
     * @return The giftCardTxnData field of the {@link PaidOut}.
     */
    public GiftCardTxnData getGiftCardTxnData () {
        return giftCardTxnData;
    }

    /**
     * <p>Setter for the giftCardTxnData field of the {@link PaidOut}.</p>
     *
     * @param giftCardTxnData The giftCardTxnData field of the {@link PaidOut}.
     */
    public void setGiftCardTxnData (GiftCardTxnData giftCardTxnData) {
        this.giftCardTxnData = giftCardTxnData;
    }

}