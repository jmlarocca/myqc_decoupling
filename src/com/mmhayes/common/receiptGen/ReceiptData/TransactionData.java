package com.mmhayes.common.receiptGen.ReceiptData;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.product.models.ItemDiscountModel;
import com.mmhayes.common.product.models.ItemRewardModel;
import com.mmhayes.common.product.models.ItemSurchargeModel;
import com.mmhayes.common.product.models.ItemTaxModel;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.ITransLineItem;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.LineItemEvaluator;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.Product;
import com.mmhayes.common.receiptGen.ReceiptData.transLineItems.TransLineItemFactory;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TransactionData.*;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;
import org.owasp.esapi.ESAPI;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by nyu on 10/12/2016.
 */
public class TransactionData
{
    private DecimalFormat df = new DecimalFormat("#,###,###,##0.00");
    private final static String COMMON_LOG_FILE = "ConsolidatedReceipt.log";
    protected DataManager dm;

    // transaction v2 variables
    private TransactionHeaderData txnHdrData;
    private ArrayList<ITransLineItem> txnLineItems;
    private LineItemEvaluator lineItemEvaluator;
    
    // Transaction header variables - roughly follows QC_PATransactions table
    private String transactionID;
    private String originalTransactionID;
    private String transDateTime;
    private String transDate;
    private String transDateNoYear;
    private String transTime;
    private String transTimeNoSeconds;
    private int transTypeID;
    private int trainingModeTransTypeID = 0;
    private int terminalID;
    private int posKioskTerminalID;
    private TerminalData terminalData;
    private String cashierName;
    private String deliveryLocation;
    private String transComment;
    private int userID = 0;
    private int revenueCenterID = 0;
    private int printerHostID = 0;
    private int transactionStatus = 0;
    private int orderTypeID = 0; //1-Normal Sale, 2-Delivery, 3-Pickup

    // Transaction detail variables
    private ArrayList transItems = new ArrayList();                  // Can include PLUs, POs, RA's, modifiers, item discounts, rewards
    private ArrayList<Tender> tenders = new ArrayList();
    private HashMap<Integer,Tax> taxes = new HashMap();
    private HashMap<Integer,Discount> subtotalDiscounts = new HashMap();
    private ArrayList<Discount> orderedSubtotalDscts = new ArrayList<Discount>();           // Same list as subtotalDiscounts hashmap, but preserves the order that the subtotal dscts were applied in
    private ArrayList<Discount> itemDiscounts = new ArrayList<Discount>();
    private ArrayList<LoyaltyReward> rewards = new ArrayList<LoyaltyReward>();
    private ArrayList<Surcharge> subtotalSurcharges = new ArrayList<Surcharge>();
    private ArrayList<Surcharge> itemSurcharges = new ArrayList<Surcharge>();

    // Loyalty
    private EmployeeData loyaltyAcct;

    // Suspended transactions
    private String suspTxnNameLabel;
    private String suspTxnName;

    public TransactionData()
    {
    }

    public String getLogFileName(int terminalId)
    {
        if (terminalId <= 0)
        {
            return COMMON_LOG_FILE;
        }
        else
        {
            return "QCPOS_" + terminalId + "_T.log";
        }
    }
    //--------------------------------------------------------------------------
    //  region Transaction Building Methods
    //--------------------------------------------------------------------------
    public void buildHeaderInfo(HashMap transactionInfo)
    {
        if (transactionInfo == null)
        {
            return;
        }

        // Set transaction header info
        transactionID = HashMapDataFns.getStringVal(transactionInfo, "PATRANSACTIONID");
        transTypeID = HashMapDataFns.getIntVal(transactionInfo, "TRANSTYPEID");
        if(transTypeID == -1)
        {
            Logger.logMessage("invalid transactionTypeID for paTransactionID '" + transactionID + "' valid?", getLogFileName(0));
            return;
        }
        originalTransactionID = HashMapDataFns.getStringVal(transactionInfo, "PATRANSIDORIGINAL");

        // set the terminal data
        terminalID = HashMapDataFns.getIntVal(transactionInfo, "TERMINALID");
        posKioskTerminalID = HashMapDataFns.getIntVal(transactionInfo, "POSKIOSKTERMINALID");
        if (posKioskTerminalID > 0) {
            HashMap terminalDataInfo = getTerminalDataInfo(posKioskTerminalID);
            if (!DataFunctions.isEmptyMap(terminalDataInfo)) {
                terminalData = new TerminalData(terminalDataInfo);
            }
        }
        else {
            HashMap terminalDataInfo = getTerminalDataInfo(terminalID);
            if (!DataFunctions.isEmptyMap(terminalDataInfo)) {
                terminalData = new TerminalData(terminalDataInfo);
            }
        }

        cashierName = HashMapDataFns.getStringVal(transactionInfo, "CASHIERNAME");
        transComment = HashMapDataFns.getStringVal(transactionInfo, "TRANSCOMMENT");
        deliveryLocation = HashMapDataFns.getStringVal(transactionInfo, "DELIVERYLOCATION");
        userID = HashMapDataFns.getIntVal(transactionInfo, "USERID");
        orderTypeID = HashMapDataFns.getIntVal(transactionInfo, "PAORDERTYPEID");

        Date transactionDate = HashMapDataFns.getDateVal(transactionInfo, "TRANSACTIONDATE");
        if (transactionDate != null)
        {
            transDate = getFormattedDateTimeStr(transactionDate, "MM/dd/yyyy");
            transDateNoYear = getFormattedDateTimeStr(transactionDate, "M/dd");
            transTime = getFormattedDateTimeStr(transactionDate, "hh:mm:ss a");
            transTimeNoSeconds = getFormattedDateTimeStr(transactionDate, "h:mm a");
            transDateTime = getFormattedDateTimeStr(transactionDate, "yyyy-MM-dd HH:mm:ss a");
        }

        revenueCenterID = HashMapDataFns.getIntVal(transactionInfo, "REVENUECENTERID");
        printerHostID = getPrinterHostIDForRC(revenueCenterID);
        suspTxnName = HashMapDataFns.getStringVal(transactionInfo, "TRANSNAME");
        suspTxnNameLabel = HashMapDataFns.getStringVal(transactionInfo, "PATRANSNAMELABEL");
    }

    /**
     * <p>Builds the line items within the transaction.</p>
     *
     * @param transactionInfo An {@link ArrayList} of {@link HashMap} corresponding to line item records within the transaction.
     */
    @SuppressWarnings("Convert2streamapi")
    public void buildLineItemsV2 (ArrayList<HashMap> transactionInfo) {

        try {
            ArrayList<ITransLineItem> txnLineItems = new ArrayList<>();
            if (!DataFunctions.isEmptyCollection(transactionInfo)) {
                // instantiate a new factory for building line items
                TransLineItemFactory transLineItemFactory = new TransLineItemFactory();
                for (HashMap lineItemRec : transactionInfo) {
                    if (!DataFunctions.isEmptyMap(lineItemRec)) {
                        ITransLineItem transLineItem = null;
                        // determine which type of line item to build
                        int paItemTypeID = HashMapDataFns.getIntVal(lineItemRec, "PAITEMTYPEID");
                        if (paItemTypeID > 0) {
                            transLineItem = transLineItemFactory.buildTransLineItemFromHM(getLogFileName(0), dm, paItemTypeID, lineItemRec);
                        }
                        if (transLineItem != null) {
                            txnLineItems.add(transLineItem);
                            // if the trans line item is a dining option product, then we found the dining option within this
                            // transaction and can set the dining option product in the TransactionHeaderData
                            if ((transLineItem instanceof Product) && (((Product) transLineItem).getIsDiningOption())) {
                                txnHdrData.setDiningOptionName(((Product) transLineItem).getName());
                            }
                        }
                    }
                }
            }
            else {
                Logger.logMessage("No transaction information was passed to TransactionData.buildLineItemsV2", getLogFileName(0), Logger.LEVEL.ERROR);
            }

            // set the transaction line items if we have some
            if (!DataFunctions.isEmptyCollection(txnLineItems)) {
                setTxnLineItems(txnLineItems);
                // analyze the line items and modify them appropriately, set rewards, tax info, etc...
                lineItemEvaluator = new LineItemEvaluator(getLogFileName(0), dm, this, txnLineItems);
            }
        }
        catch (Exception e) {
            Logger.logException(e, getLogFileName(0));
            String paTransactionID = "";
            if ((!DataFunctions.isEmptyCollection(transactionInfo)) && (!DataFunctions.isEmptyMap(transactionInfo.get(0)))
                    && (StringFunctions.stringHasContent(HashMapDataFns.getStringVal(transactionInfo.get(0), "PATRANSACTIONID")))) {
                paTransactionID = HashMapDataFns.getStringVal(transactionInfo.get(0), "PATRANSACTIONID");
            }
            Logger.logMessage(String.format("There was a problem trying to build the line items for the transaction with an ID of %s in TransactionData.buildLineItemsV2",
                    Objects.toString(paTransactionID, "N/A")), getLogFileName(0), Logger.LEVEL.ERROR);
        }

    }

    // Retrieves the transaction details from the server for the specified PATransactionID
    // Takes the retrieved DB data and builds it into a transaction
    public void buildLineItems(int receiptTypeID, ArrayList transactionInfo, ArrayList<String> rcptAcctInfoLines)
    {
        try {
            // Go through the line items and build the transaction
            for (Object objLineItem : transactionInfo)
            {
                HashMap lineItem = (HashMap) objLineItem;

                String currentPATransLineItemID = HashMapDataFns.getStringVal(lineItem, "PATRANSLINEITEMID");
                String currentPATransLineItemModID = HashMapDataFns.getStringVal(lineItem, "PATRANSLINEITEMMODID");
                int itemTypeID = HashMapDataFns.getIntVal(lineItem, "PAITEMTYPEID");

                switch (itemTypeID)
                {
                    case TypeData.ItemType.PLU:
                        Plu product = Plu.createPluFromDB(lineItem, transTypeID);

                        // Get product's mapped rewards
                        ArrayList mappedRewardIDS = getProductMappedRewards(product.getPluID());

                        // Set product's mapped rewards
                        product.setRewardIDs(mappedRewardIDS);

                        // Check if the product line has already been added
                        if( product.checkDuplicateProduct(transItems) )
                        {
                            continue;
                        }

                        // Add product to list of transaction items
                        transItems.add(product);

                        // If this is a modifier, assign the parent
                        if (product.getPaTransLineItemModID() != 0)
                        {
                            product.setParentProduct(getProduct(product.getPATransLineItemID()));
                            product.getParentProduct().addModifier(product);
                        }
                        break;
                    case TypeData.ItemType.TENDER:
                        Tender tndr = Tender.createTenderFromDB(lineItem);

                        // If we need to print anything about QC employees, get those details
                        if (tndr.getEmployeeData() != null && rcptAcctInfoLines.size() > 0)
                        {
                            tndr.getEmployeeData().lookupQCAcctInfo(terminalID, tndr.getAmount(), dm);
                        }

                        tenders.add(tndr);
                        //For the case of dollar donations, the donation is in a transaction of the SALE type and the line item
                        //is tied to item type of TENDER.
                        //Because of this we create the donation item within the tender and get
                        //the transaction item of the donation type out of it.
                        transItems.add(tndr.getDonationItem());
                        break;
                    case TypeData.ItemType.DISCOUNT:
                        Discount dsct = Discount.createDiscountFromDB(lineItem);

                        if (dsct.getIsSubtotalDsct())
                        {
                            // Add discount to list of subtotal discounts
                            subtotalDiscounts.put(new Integer(dsct.getDiscountID()), dsct);
                            orderedSubtotalDscts.add(dsct);
                        }
                        else
                        {
                            // Otherwise, add it to the list of item discounts
                            itemDiscounts.add(dsct);
                        }
                        break;
                    case TypeData.ItemType.TAX:
                        Tax tax = Tax.createTaxFromDB(lineItem);
                        Integer taxID = new Integer(tax.getTaxID());
                        if (taxes.containsKey(taxID))
                        {
                            Tax existingTax = taxes.get(taxID);
                            BigDecimal existingTaxAmt = new BigDecimal(existingTax.getExtendedAmt()).setScale(2, RoundingMode.HALF_UP);
                            BigDecimal taxAmt = new BigDecimal(tax.getExtendedAmt()).setScale(2, RoundingMode.HALF_UP);
                            BigDecimal newTaxAmt = existingTaxAmt.add(taxAmt);
                            existingTax.setAmount(newTaxAmt);
                        }
                        else
                        {
                            taxes.put(taxID, tax);
                        }
                        break;
                    case TypeData.ItemType.PAIDOUT:
                        PaidOut paidOutItem = PaidOut.createPaidOutFromDB(lineItem);

                        // Add paid out item to list of transaction items
                        transItems.add(paidOutItem);
                        break;
                    case TypeData.ItemType.RA:
                        ReceivedOnAcct raItem = ReceivedOnAcct.createRAItemFromDB(lineItem);

                        // Add RA item to list of transaction items
                        transItems.add(raItem);
                        break;
                    case TypeData.ItemType.LOYALTY_REWARD:
                        LoyaltyReward reward = LoyaltyReward.createRewardFromDB(lineItem);

                        rewards.add(reward);
                        break;
                    case TypeData.ItemType.SURCHARGE:
                        Surcharge surcharge = Surcharge.createSurchargeFromDB(lineItem);

                        if (surcharge.isSubtotalSurcharge())
                        {
                            subtotalSurcharges.add(surcharge);
                        }
                        else
                        {
                            itemSurcharges.add(surcharge);
                        }
                        break;
                    case TypeData.ItemType.COMBO:
                        Combo comboItem = Combo.createComboFromDB(lineItem);
                        //Build the rootPrice by adding up all matching product's combo prices
                        if (comboItem.getRootPrice()==0.0)
                        {
                            Double rootComboPrice = new Double(0);
                            for (Object prodLineItem : transactionInfo) {
                                HashMap plulineItem = (HashMap) prodLineItem;
                                int pluItemTypeID = HashMapDataFns.getIntVal(plulineItem, "PAITEMTYPEID");
                                if (pluItemTypeID==TypeData.ItemType.PLU)
                                {
                                    String comboTransLineItemID = HashMapDataFns.getStringVal(lineItem, "PATRANSLINEITEMID");
                                    String pluComboTransLineItemID = HashMapDataFns.getStringVal(plulineItem, "PACOMBOTRANSLINEITEMID");
                                    if (comboTransLineItemID.equals(pluComboTransLineItemID))
                                    {
                                        Double comboPrice = HashMapDataFns.getDoubleVal(plulineItem, "COMBOPRICE");
                                        rootComboPrice += comboPrice;
                                    }
                                }
                            }
                            BigDecimal rootComboPriceRounded = new BigDecimal(rootComboPrice).setScale(4, RoundingMode.HALF_UP);
                            comboItem.setRootPrice(rootComboPriceRounded.doubleValue());
                        }

                        // Add combo item to list of transaction items
                        transItems.add(comboItem);
                        break;
                    default:
                        Logger.logMessage("TransactionData.buildLineItems error - unknown object type: " + itemTypeID, getLogFileName(0), Logger.LEVEL.ERROR);

                }
            }

            // Set the taxes/discounts/rewards that were applied to each product
            for (Object transItem : transItems)
            {
                if (transItem instanceof Plu && ((Plu) transItem).getPaTransLineItemModID() == 0)
                {
                    setTaxDsctRwdDetailsForProduct((Plu)transItem);
                }
            }
        } catch (Exception ex) {
            ReceiptGen.logError("TransactionData.buildLineItems error", ex);
        }
    }

    // This gets called when we have most of the details we need from Flex
    public void buildLineItems(ArrayList<ArrayList> details, ArrayList<String> rcptAcctInfoLines)
    {
        for (ArrayList detail : details)
        {
            int itemTypeID = new Integer(detail.get(0).toString()).intValue();

            switch (itemTypeID)
            {
                case TypeData.ItemType.COMBO:
                    Combo ComboItem = Combo.createComboFromXMLRPC(detail);

                    // Add product to list of transaction items
                    transItems.add(ComboItem);
                    break;
                case TypeData.ItemType.PLU:
                    Plu product = Plu.createPluFromXMLRPC(detail);

                    // Add product to list of transaction items
                    transItems.add(product);
                    break;
                case TypeData.ItemType.TENDER:
                    Tender tndr = Tender.createTenderFromXMLRPC(detail, transTypeID);

                    tenders.add(tndr);
                    break;
                case TypeData.ItemType.DISCOUNT:
                    Discount dsct = Discount.createDiscountFromXMLRPC(detail, transTypeID);

                    if (dsct.getIsSubtotalDsct())
                    {
                        // Add discount to list of subtotal discounts
                        subtotalDiscounts.put(new Integer(dsct.getDiscountID()), dsct);
                        orderedSubtotalDscts.add(dsct);
                    }
                    else
                    {
                        // Otherwise, add it to the list of item discounts
                        itemDiscounts.add(dsct);

                        // Assign it to the previous PARENT Plu
                        for (int x = transItems.size() -1 ; x >= 0; x--)
                        {
                            if (transItems.get(x) instanceof Plu && ((Plu)transItems.get(x)).getIsModifier() == false)
                            {
                                ((Plu)transItems.get(x)).setItemDiscount(dsct);
                                break;
                            }
                        }
                    }
                    break;
                case TypeData.ItemType.TAX:
                    Tax tax = Tax.createTaxFromXMLRPC(detail, transTypeID);

                    taxes.put(new Integer(tax.getTaxID()), tax);
                    break;
                case TypeData.ItemType.PAIDOUT:
                    PaidOut paidOutItem = PaidOut.createPaidOutFromXMLRPC(detail);

                    // Add paid out item to list of transaction items
                    transItems.add(paidOutItem);
                    break;
                case TypeData.ItemType.RA:
                    ReceivedOnAcct raItem = ReceivedOnAcct.createRAItemFromXMLRPC(detail, transTypeID);

                    // Add RA item to list of transaction items
                    transItems.add(raItem);
                    break;
                case TypeData.ItemType.LOYALTY_REWARD:
                    LoyaltyReward reward = LoyaltyReward.createRewardFromXMLRPC(detail, transTypeID);

                    rewards.add(reward);
                    break;
                case TypeData.ItemType.SURCHARGE:
                    Surcharge surcharge = Surcharge.createSurchargeFromXMLRPC(detail, transTypeID);

                    if (surcharge.isSubtotalSurcharge())
                    {
                        subtotalSurcharges.add(surcharge);
                    }
                    else
                    {
                        itemSurcharges.add(surcharge);
                    }
                    break;
            }
        }

        setParentsOnModifierProducts();
    }

    // This gets called when we have a populated TransactionModel
    public void buildLineItems(TransactionModel transactionModel)
    {
        //Product
        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()){
            Plu product = Plu.createPlu(productLineItemModel, transactionModel);
            // Add product to list of transaction items
            transItems.add(product);

            for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()){
                Plu modifierPlu = Plu.createPlu(modifierLineItemModel, transactionModel);
                transItems.add(modifierPlu);
            }
        }

        //Tender
        for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()){
            Tender tndr = Tender.createTender(tenderLineItemModel, transactionModel);
            tenders.add(tndr);
        }

        //Discounts
        for (DiscountLineItemModel discountLineItemModel : transactionModel.getDiscounts()){
            Discount dsct = Discount.createDiscount(discountLineItemModel, transactionModel);

            if (dsct.getIsSubtotalDsct())
            {
                // Add discount to list of subtotal discounts
                subtotalDiscounts.put(new Integer(dsct.getDiscountID()), dsct);
                orderedSubtotalDscts.add(dsct);
            }
            else
            {
                // Otherwise, add it to the list of item discounts
                itemDiscounts.add(dsct);
            }
        }

        //Tax
        for (TaxLineItemModel taxLineItemModel : transactionModel.getTaxes()){
            Tax tax = Tax.createTax(taxLineItemModel, transactionModel);
            Integer taxID = new Integer(tax.getTaxID());
            if (taxes.containsKey(taxID))
            {
                Tax existingTax = taxes.get(taxID);
                BigDecimal existingTaxAmt = new BigDecimal(existingTax.getExtendedAmt()).setScale(2, RoundingMode.HALF_UP);
                BigDecimal taxAmt = new BigDecimal(tax.getExtendedAmt()).setScale(2, RoundingMode.HALF_UP);
                BigDecimal newTaxAmt = existingTaxAmt.add(taxAmt);
                existingTax.setAmount(newTaxAmt);
            }
            else
            {
                taxes.put(taxID, tax);
            }
        }

        //PaidOut
        for (PaidOutLineItemModel paidOutLineItemModel : transactionModel.getPaidOuts()){
            PaidOut paidOutItem = PaidOut.createPaidOut(paidOutLineItemModel);
            // Add paid out item to list of transaction items
            transItems.add(paidOutItem);
        }

        //Roa
        for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : transactionModel.getReceivedOnAccounts()){
            ReceivedOnAcct raItem = ReceivedOnAcct.createRA(receivedOnAccountLineItemModel, transactionModel);
            // Add RA item to list of transaction items
            transItems.add(raItem);
        }

        //Loyalty Reward
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()){
            LoyaltyReward reward = LoyaltyReward.createReward(loyaltyRewardLineItemModel);
            rewards.add(reward);
        }

        //Surcharges
        for (SurchargeLineItemModel surchargeLineItemModel : transactionModel.getSurcharges()){
            Surcharge surcharge = Surcharge.createSurcharge(surchargeLineItemModel, transactionModel);
            if (surcharge.isSubtotalSurcharge())
            {
                subtotalSurcharges.add(surcharge);
            }
            else
            {
                itemSurcharges.add(surcharge);
            }
        }

        //Combos
        for (ComboLineItemModel comboLineItemModel : transactionModel.getCombos()){
            Combo comboItem = Combo.createCombo(comboLineItemModel);

            //Build the rootPrice by adding up all matching product's combo prices
            if (comboItem.getRootPrice()==0.0)
            {
                Double rootComboPrice = new Double(0);
                for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()){
                    if (comboLineItemModel.getId() != null && comboLineItemModel.getId().equals(productLineItemModel.getComboTransLineItemId())){
                        rootComboPrice += productLineItemModel.getComboPrice().doubleValue();
                    }
                }

                BigDecimal rootComboPriceRounded = new BigDecimal(rootComboPrice).setScale(4, RoundingMode.HALF_UP);
                comboItem.setRootPrice(rootComboPriceRounded.doubleValue());
            }

            transItems.add(comboItem);
        }

        // Set the taxes/discounts/rewards that were applied to each product
        for (Object transItem : transItems)
        {
            if (transItem instanceof Plu && ((Plu) transItem).getPaTransLineItemModID() == 0)
            {
                Plu plu = (Plu)transItem;
                for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()){
                    if (productLineItemModel.getId().equals(plu.getPATransLineItemID())){
                        setTaxDsctRwdDetailsForProduct((Plu)transItem, productLineItemModel, transactionModel);
                    }
                }
            }
        }

        setParentsOnModifierProducts();
    }

    public void cleanLineItems(TransactionModel transactionModel){
        //Product
        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()){
            productLineItemModel.getProduct().setName( cleanStringField(productLineItemModel.getProduct().getName()));
            productLineItemModel.getProduct().setIsModifier(false);
            if (productLineItemModel.getPrepOption() != null && productLineItemModel.getPrepOption().getName() != null){
                productLineItemModel.getPrepOption().setName(cleanStringField(productLineItemModel.getPrepOption().getName()));
            }

            for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()){
                modifierLineItemModel.setParentPaTransLineItemId(productLineItemModel.getId());
                modifierLineItemModel.getProduct().setName(cleanStringField(modifierLineItemModel.getProduct().getName()));
                if (modifierLineItemModel.getPrepOption() != null && modifierLineItemModel.getPrepOption().getName() != null){
                    modifierLineItemModel.getPrepOption().setName(cleanStringField(modifierLineItemModel.getPrepOption().getName()));
                }
            }
        }

        //Tender
        for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()){
            tenderLineItemModel.getTender().setName(cleanStringField(tenderLineItemModel.getTender().getName()));
        }

        //Discounts
        for (DiscountLineItemModel discountLineItemModel : transactionModel.getDiscounts()){
           discountLineItemModel.getDiscount().setName(cleanStringField(discountLineItemModel.getDiscount().getName()));
        }

        //Tax
        for (TaxLineItemModel taxLineItemModel : transactionModel.getTaxes()){
            taxLineItemModel.getTax().setName(cleanStringField(taxLineItemModel.getTax().getName()));
        }

        //PaidOut
        for (PaidOutLineItemModel paidOutLineItemModel : transactionModel.getPaidOuts()){
            paidOutLineItemModel.getPaidOut().setName(cleanStringField(paidOutLineItemModel.getPaidOut().getName()));
        }

        //Roa
        for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : transactionModel.getReceivedOnAccounts()){
            receivedOnAccountLineItemModel.getReceivedOnAccount().setName(cleanStringField(receivedOnAccountLineItemModel.getReceivedOnAccount().getName()));
        }

        //Loyalty Reward
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()){
            loyaltyRewardLineItemModel.getReward().setName(cleanStringField(loyaltyRewardLineItemModel.getReward().getName()));
        }

        //Surcharges
        for (SurchargeLineItemModel surchargeLineItemModel : transactionModel.getSurcharges()){
            surchargeLineItemModel.getSurcharge().setName(cleanStringField(surchargeLineItemModel.getSurcharge().getName()));
        }
    }

    private String cleanStringField(String text){
        return ESAPI.encoder().decodeForHTML(text);
    }

    // This only needs to be used with receipts from Flex
    private void setParentsOnModifierProducts()
    {
        for (int x = 0; x < transItems.size(); x++)
        {
            if (transItems.get(x) instanceof Plu)
            {
                Plu product = ((Plu)transItems.get(x));

                if (product.getIsModifier())
                {
                    int paTransLineItemId = product.getPATransLineItemID();
                    Plu parent = getProduct(paTransLineItemId);

                    if (parent != null)
                    {
                        product.setParentProduct(parent);
                    }
                }
            }

        }
    }
    // endregion

    //--------------------------------------------------------------------------
    //  region Methods for Retrieving Info from the Database
    //--------------------------------------------------------------------------

    /**
     * <p>Queries that database to get information on the transaction with the given transaction ID.</p>
     *
     * @param paTransactionID The ID of the transaction to get the information for.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the details of the transaction with the given ID.
     */
    @SuppressWarnings({"unchecked", "Convert2streamapi"})
    public ArrayList<HashMap> getTransactionInfoV2 (int paTransactionID) {
        ArrayList<HashMap> transactionInfo = new ArrayList<>();

        try {
            // make sure we have a valid DataManager
            if (dm == null) {
                Logger.logMessage("No valid data manager found in TransactionData.getTransactionInfoV2", getLogFileName(0), Logger.LEVEL.ERROR);
                return transactionInfo;
            }

            // query the database to get the transaction information
            if (paTransactionID > 0) {
                ArrayList<HashMap> queryRes = dm.parameterizedExecuteQuery("data.kms.getTransactionInformation", new Object[]{paTransactionID}, COMMON_LOG_FILE, true);
                if (!DataFunctions.isEmptyCollection(queryRes)) {
                    for (HashMap hm : queryRes) {
                        if (!DataFunctions.isEmptyMap(hm)) {
                            transactionInfo.add(hm);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, getLogFileName(0));
            Logger.logMessage(String.format("There was a problem trying to query the database to get information regarding the " +
                    "transaction with an ID of %s in TransactionData.getTransactionInfoV2",
                    Objects.toString(paTransactionID, "N/A")), getLogFileName(0), Logger.LEVEL.ERROR);
        }

        return transactionInfo;
    }

    public ArrayList getTransactionInfo(int paTransactionID)
    {
        if (dm == null)
        {
            Logger.logMessage("TransactionData.getTxnAndPrintJobHeaderInfo: DataManager object is null", getLogFileName(0), Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList txnInfo = dm.parameterizedExecuteQuery("data.ReceiptGen.GetTransactionDetails", new Object[]{ new Integer(paTransactionID), new Integer(paTransactionID), new Integer(paTransactionID) }, COMMON_LOG_FILE, true, null);

        if(txnInfo == null || txnInfo.size() < 1)
        {
            Logger.logMessage("Unexpected response for data.ReceiptGen.GetTransactionDetails, is the PATransactionID '" + paTransactionID + "' valid?", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        return txnInfo;
    }

    // Retrieves all ItemTaxDsct records from the DB for the given PATransLineItemID
    private void setTaxDsctRwdDetailsForProduct(Plu product)
    {
        try
        {
            if (dm == null)
            {
                Logger.logMessage("TransactionData.getItemTaxDsctDetails: DataManager object is null", getLogFileName(0), Logger.LEVEL.ERROR);
                return;
            }

            int paTransLineItemID = product.getPATransLineItemID();
            ArrayList dbResults = dm.parameterizedExecuteQuery("data.ReceiptGen.GetItemTaxDsctDetails", new Object[]{ new Integer(paTransLineItemID) }, COMMON_LOG_FILE, true, null);

            // We want to turn this into a hashmap where the key is the PATransLineItemID and
            // the key is an arraylist of tax/discount details
            for (Object result : dbResults)
            {
                HashMap paItemTaxDsctRecHM = (HashMap)result;
                int detailItemTypeID = HashMapDataFns.getIntVal(paItemTaxDsctRecHM, "ITEMTYPEID");
                Integer detailItemID = HashMapDataFns.getIntVal(paItemTaxDsctRecHM, "ITEMID");
                BigDecimal detailAmt = new BigDecimal(HashMapDataFns.getDoubleVal(paItemTaxDsctRecHM, "AMOUNT"));
                String shortName = "";

                switch (detailItemTypeID)
                {
                    case TypeData.ItemType.TAX:
                        shortName = ((Tax)taxes.get(detailItemID)).getShortName();
                        break;
                    case TypeData.ItemType.DISCOUNT:
                        Discount dsct = null;

                        if (subtotalDiscounts.containsKey(detailItemID))
                        {
                            dsct = (Discount)subtotalDiscounts.get(detailItemID);
                        }
                        else
                        {
                            dsct = getItemDiscount(detailItemID, detailAmt);
                            if (dsct != null)
                            {
                                // Attach this item discount to the PLU
                                if (product.getItemDiscount() == null)
                                {
                                    product.setItemDiscount(dsct);
                                }
                            }
                        }

                        shortName = dsct.getShortName();
                        break;
                    case TypeData.ItemType.LOYALTY_REWARD:
                        ItemRewardModel rwdDetail = new ItemRewardModel(paItemTaxDsctRecHM);
                        BigDecimal rwdDetailAmt = rwdDetail.getAmount();
                        LoyaltyReward reward = getLoyaltyReward(rwdDetail.getItemId(), rwdDetailAmt);

                        if (reward != null)
                        {
                            shortName = reward.getShortName();

                            if (reward.getRewardTypeID() == PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS.toInt())
                            {
                                product.addFreeProductRwd(reward);
                            }
                        }
                        break;
                    case TypeData.ItemType.SURCHARGE:
                        ItemSurchargeModel surchargeDetail = new ItemSurchargeModel(paItemTaxDsctRecHM);
                        BigDecimal surchargeDetailAmt = surchargeDetail.getAmount();
                        Surcharge itemSurcharge = getSurcharge(surchargeDetail.getItemId(), surchargeDetailAmt);

                        if (itemSurcharge != null)
                        {
                            shortName = itemSurcharge.getShortName();

                            if (itemSurcharge.getApplicationTypeID() == TypeData.SurchargeApplicationType.ITEM)
                            {
                                product.addItemSurcharge(itemSurcharge);
                            }
                        }
                        break;
                    default:
                        Logger.logMessage("TransactionData.setTaxDsctRwdDetailsForProduct error: UNKNOWN ITEM TYPE ID: " + detailItemTypeID, ReceiptGen.getLogFileName(), Logger.LEVEL.ERROR);
                }

                if (shortName.equals("") == false)
                {
                    product.addToTaxDsctRwdStr(shortName);
                }
            }

            linkMissingItemSurchargeRecords(product);
        }
        catch (Exception e)
        {
            ReceiptGen.logError("TransactionData.setTaxDsctRwdDetailsForProduct error", e);
        }
    }

    // Retrieves all ItemTaxDsct records from the TransactionModel
    private void setTaxDsctRwdDetailsForProduct(Plu product, ProductLineItemModel productLineItemModel, TransactionModel transactionModel)
    {
        try
        {

            for (ItemTaxModel itemTaxModel : productLineItemModel.getTaxes()){
                if (itemTaxModel.getTax().getShortName() != null && !itemTaxModel.getTax().getShortName().isEmpty())
                {
                    product.addToTaxDsctRwdStr(itemTaxModel.getTax().getShortName());
                }
            }

            for (ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()){
                if (itemDiscountModel.getDiscount().getShortName() != null && !itemDiscountModel.getDiscount().getShortName().isEmpty()){
                    product.addToTaxDsctRwdStr(itemDiscountModel.getDiscount().getShortName());
                }

                Discount dsct = null;

                if (subtotalDiscounts.containsKey(itemDiscountModel.getDiscount().getId()))
                {
                    dsct = (Discount)subtotalDiscounts.get(itemDiscountModel.getDiscount().getId());
                }
                else
                {
                    dsct = Discount.createDiscount(itemDiscountModel, transactionModel);

                    if (!dsct.getIsSubtotalDsct()){
                        if (product.getItemDiscount() == null)
                        {
                            product.setItemDiscount(dsct);
                        }
                    }
                }
            }

            for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()){
                if (itemRewardModel.getReward().getShortName() != null && !itemRewardModel.getReward().getShortName().isEmpty()){
                  product.addToTaxDsctRwdStr(itemRewardModel.getReward().getShortName());
                 }

                if (itemRewardModel.getReward().getRewardTypeId().equals(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS.toInt())){

                    LoyaltyReward reward = LoyaltyReward.createReward(itemRewardModel);
                    product.addFreeProductRwd(reward);
                }
            }

            for (ItemSurchargeModel itemSurchargeModel : productLineItemModel.getSurcharges()){
                String shortName = "";

                if (itemSurchargeModel.getSurcharge().getShortName() != null && !itemSurchargeModel.getSurcharge().getShortName().isEmpty()){
                    product.addToTaxDsctRwdStr(itemSurchargeModel.getSurcharge().getShortName());
                }

                if (itemSurchargeModel.getSurcharge().getApplicationTypeId().equals(PosAPIHelper.SurchargeApplicationType.ITEM.toInt())){

                    Surcharge surcharge = Surcharge.createSurcharge(itemSurchargeModel, productLineItemModel, transactionModel);
                    product.addItemSurcharge(surcharge);
                }
            }

            linkMissingItemSurchargeRecords(product);
        }
        catch (Exception e)
        {
            ReceiptGen.logError("TransactionData.setTaxDsctRwdDetailsForProduct error", e);
        }
    }

    public ArrayList getLoyaltyTxnPointInfo(int paTransactionID)
    {
        if (dm == null)
        {
            Logger.logMessage("TransactionData.getLoyaltyTxnPointInfo: DataManager object is null", getLogFileName(0), Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList txnLoyaltyPointInfo = dm.parameterizedExecuteQuery("data.ReceiptGen.GetTxnLoyaltyPointDetail", new Object[]{ new Integer(paTransactionID), new Integer(revenueCenterID) }, COMMON_LOG_FILE, true, null);

        if(txnLoyaltyPointInfo == null)
        {
            Logger.logMessage("Unexpected response for data.ReceiptGen.GetTxnLoyaltyPointDetail, is the PATransactionID '" + paTransactionID + "' valid?", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        return txnLoyaltyPointInfo;
    }

    public ArrayList getLoyaltyAcctInfo(String employeeID)
    {
        if (dm == null)
        {
            Logger.logMessage("TransactionData.getLoyaltyAcctInfo: DataManager object is null", getLogFileName(0), Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList loyaltyAcctInfo = dm.parameterizedExecuteQuery("data.ReceiptGen.GetLoyaltyAcctInfo", new Object[]{ employeeID }, COMMON_LOG_FILE, true, null);

        if(loyaltyAcctInfo == null)
        {
            Logger.logMessage("Unexpected response for data.ReceiptGen.GetLoyaltyAcctInfo, is the employeeID '" + employeeID + "' valid?", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        return loyaltyAcctInfo;
    }

    public ArrayList getProductMappedRewards(int paPluID)
    {
        if (dm == null)
        {
            Logger.logMessage("TransactionData.getProductMappedRewards: DataManager object is null", getLogFileName(0), Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList mappedRwdsInfo = dm.parameterizedExecuteQuery("data.ReceiptGen.GetProductMappedRewards", new Object[]{ new Integer(paPluID)}, COMMON_LOG_FILE, true, null);

        if(mappedRwdsInfo == null || mappedRwdsInfo.size() < 1)
        {
            Logger.logMessage("Unexpected response for data.ReceiptGen.GetProductMappedRewards, is the PAPluID '" + paPluID + "' valid?", COMMON_LOG_FILE, Logger.LEVEL.DEBUG);
            return null;
        }

        return mappedRwdsInfo;
    }
    // endregion

    //--------------------------------------------------------------------------
    //  region Other
    //--------------------------------------------------------------------------
    // Returns the number of items in the transaction
    public int getItemCount()
    {
        int itemCount = 0;

        for (Object item : transItems)
        {
            if (item instanceof Plu || item instanceof ReceivedOnAcct || item instanceof PaidOut)
            {
                if (item instanceof Plu)
                {
                    if (((Plu)item).getIsModifier() == false)
                    {
                        if (((Plu)item).getIsWeightedItem())
                        {
                            itemCount++;
                        }
                        else
                        {
                            Double itemQty = ((Plu)item).getQuantity();

                            if (itemQty < 0 && (transTypeID == TypeData.TranType.VOID || transTypeID == TypeData.TranType.REFUND))
                            {
                                itemQty = itemQty * -1;
                            }

                            itemCount = itemCount + itemQty.intValue();
                        }
                    }
                }
                else
                {
                    itemCount++;
                }
            }
        }

        return itemCount;
    }

    /**
     * <p>Calculates the item count using the given {@link CustomerReceiptData}.</p>
     *
     * @param customerReceiptData The {@link CustomerReceiptData} to use to calculate the item count.
     * @return The item count.
     */
    public static int getItemCount (CustomerReceiptData customerReceiptData) {

        // make sure the customer receipt data is valid
        if (customerReceiptData == null) {
            Logger.logMessage("The CustomerReceiptData passed to TransactionData.getItemCount can't be null, unable to determine the item " +
                    "count, now returning -1.", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return -1;
        }

        int itemCount = 0;
        if (!DataFunctions.isEmptyCollection(customerReceiptData.getProducts())) {
            for (Plu product : customerReceiptData.getProducts()) {
                if ((product != null) && (!product.getIsModifier()) && (product.getIsWeightedItem())) {
                    itemCount++;
                }
                else if ((product != null) && (!product.getIsModifier()) && (!product.getIsWeightedItem())) {
                    double quantity = product.getQuantity();
                    // determine if items were removed
                    if ((quantity < 0.0d) && ((customerReceiptData.getTransTypeID() == TypeData.TranType.VOID) || (customerReceiptData.getTransTypeID() == TypeData.TranType.REFUND))) {
                        quantity *= -1.0d;
                    }
                    itemCount += ((int) Math.round(quantity));
                }
            }
        }
        if (!DataFunctions.isEmptyCollection(customerReceiptData.getPaidOuts())) {
            itemCount += customerReceiptData.getPaidOuts().size();
        }
        if (!DataFunctions.isEmptyCollection(customerReceiptData.getReceivedOnAccts())) {
            itemCount += customerReceiptData.getReceivedOnAccts().size();
        }

        return itemCount;
    }

    // Returns the total gross weight of items in the transaction
    public BigDecimal getGrossWeightTotal()
    {
        BigDecimal grossWeightTotal = new BigDecimal(0);

        for (Object item : transItems)
        {
            if (item instanceof Plu)
            {
                if (((Plu)item).getIsModifier() == false)
                {
                    if (((Plu)item).getIsWeightedItem())
                    {
                        //add net weight for weighted products (do not include tare)
                        BigDecimal itemNetWeight = new BigDecimal(((Plu)item).getNetWeight());

                        grossWeightTotal = grossWeightTotal.add(itemNetWeight);

                    }
                    else
                    {
                        BigDecimal itemGrossWeight = ((Plu)item).getGrossWeight();

                        grossWeightTotal = grossWeightTotal.add(itemGrossWeight);
                    }
                }
            }

        }

        return grossWeightTotal;
    }

    // Returns the total gross weight of items in the transaction
    public BigDecimal getNetWeightTotal()
    {
        BigDecimal netWeightTotal = new BigDecimal(0);

        for (Object item : transItems)
        {
            if (item instanceof Plu)
            {
                if (((Plu)item).getIsModifier() == false)
                {
                    if (((Plu)item).getIsWeightedItem())
                    {
                        //add net weight for weighted products (do not include tare)
                        BigDecimal itemNetWeight = new BigDecimal(((Plu)item).getNetWeight());
                        netWeightTotal = netWeightTotal.add(itemNetWeight);

                    }
                    else
                    {
                        BigDecimal fixedNetWeight = ((Plu)item).getFixedNetWeight();
                        netWeightTotal = netWeightTotal.add(fixedNetWeight);
                    }
                }
            }

        }

        return netWeightTotal;
    }

    String getFormattedDateTimeStr (Date d, String fmt)
    {
        SimpleDateFormat df = new SimpleDateFormat(fmt);
        return df.format(d);
    }

    public Plu getProduct(int paTransLineItemId)
    {
        Plu resultProduct = null;

        for (int x = 0; x < transItems.size(); x++)
        {
            if ( (transItems.get(x) instanceof Plu) && (((Plu) transItems.get(x)).getPATransLineItemID() == paTransLineItemId) )
            {
                resultProduct = (Plu) transItems.get(x);
                break;
            }
        }

        return resultProduct;
    }

    private void linkMissingItemSurchargeRecords(Plu product){
        //If Surcharge.RefundsWithProduct = false, an ItemTaxDsct record is not created.  This links the product line and surcharge line
        if (this.itemSurcharges.size() > 0){
            for (Surcharge txItemSurcharge : this.itemSurcharges){
                if (txItemSurcharge.getLinkedPaTransLineItemID() > 0 && txItemSurcharge.getLinkedPaTransLineItemID() == product.getPATransLineItemID()){
                    boolean surchargeIsOnProduct = false;
                    for (Surcharge prodItemSurcharge : product.getItemSurcharges()){
                        if (prodItemSurcharge.getSurchargeID() == txItemSurcharge.getSurchargeID()){
                            surchargeIsOnProduct = true;
                        }
                    }

                    if (!surchargeIsOnProduct){
                        BigDecimal surchargeAmount = new BigDecimal(String.valueOf(txItemSurcharge.getAmount()));
                        Surcharge itemSurcharge = getSurcharge(txItemSurcharge.getSurchargeID(), surchargeAmount);
                        product.addItemSurcharge(itemSurcharge);
                        if (itemSurcharge.getShortName().equals("") == false)
                        {
                            product.addToTaxDsctRwdStr(itemSurcharge.getShortName());
                        }
                    }
                }
            }
        }
    }

    // Takes a hashmap and returns the Date value of the specified key name
    public Date getDateVal(String dateTimeStr) {

        Date retVal = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            retVal = sdf.parse(dateTimeStr);
            return retVal;
        } catch (ParseException e) {
            Logger.logMessage("Exception parsing DateTime string with format yyyy-MM-dd HH:mm:ss.", COMMON_LOG_FILE);
        }

        sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
        try {
            retVal = sdf.parse(dateTimeStr);
            return retVal;
        } catch (ParseException e) {
            Logger.logMessage("Exception parsing DateTime string with format MM/dd/yyyy HH:mm:ss a.", COMMON_LOG_FILE);
        }

        sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        try {
            retVal = sdf.parse(dateTimeStr);
            return retVal;
        } catch (ParseException e) {
            Logger.logMessage("Exception parsing DateTime string with format MM/dd/yyyy HH:mm:ss.", COMMON_LOG_FILE);
        }

        return retVal;

    }

    public Discount getItemDiscount(int discountID, BigDecimal amount)
    {
        Discount targetDsct = null;

        for (Discount itemDsct : itemDiscounts)
        {
            if (itemDsct.getDiscountID() == discountID)
            {
                BigDecimal dsctAmt = new BigDecimal(itemDsct.getExtendedAmt());

                if (amount.compareTo(dsctAmt) == 0)
                {
                    targetDsct = itemDsct;
                    break;
                }
            }
        }

        return targetDsct;
    }
    // endregion

    //--------------------------------------------------------------------------
    //  region Transaction Total Calculation Methods
    //      - We want to use BigDecimal in instances where we're doing calculation (more accurate rounding)
    //--------------------------------------------------------------------------
    public BigDecimal getSubtotal()
    {
        BigDecimal subtotal = new BigDecimal(0.0).setScale(2, RoundingMode.HALF_UP);

        try
        {
            for (int x = 0; x < transItems.size(); x++)
            {
                if (transItems.get(x) instanceof Combo)
                {
                    subtotal = subtotal.add(((Combo)transItems.get(x)).getTotalItemPrice());
                }
                else if (transItems.get(x) instanceof Plu)
                {
                    Plu product = ((Plu)transItems.get(x));

                    BigDecimal productAmt = new BigDecimal(product.getAmount());
                    double prepCost = 0;
                    if (product.getPrepOption()!=null)
                    {
                        //Add in the cost of the prep option
                        if (product.getPrepOption().getPrepOptionPrice() != 0)
                        {
                            prepCost = product.getAmount() + product.getPrepOption().getPrepOptionPrice();
                        }
                    }
                    productAmt = productAmt.add(new BigDecimal(prepCost));

                    // If product is in a combo, factor in free product rewards and item surcharges
                    if( product.getPaComboTransLineItemID() > 0 || (product.getIsModifier() && product.getParentProduct().getPaComboTransLineItemID() > 0))
                    {
                        if ( (product.getFreeProductRwds().size() > 0) && (product.getParentProduct() == null) )      // Modifiers will never have a free product rwd applied only to itself
                        {
                            for (LoyaltyReward reward : product.getFreeProductRwds())
                            {
                                subtotal = subtotal.add(reward.getTotalItemPrice());
                            }
                        }

                        if ( product.getItemSurcharges().size() > 0 )
                        {
                            for (Surcharge surcharge : product.getItemSurcharges())
                            {
                                subtotal = subtotal.add(surcharge.getTotalItemPrice());
                            }
                        }
                    }
                    else
                    {  // only add product price to subtotal if it is not part of a combo

                        if ( (productAmt.compareTo(BigDecimal.ZERO) == 0) && (product.getFreeProductRwds().size() > 0) && (product.hasModifiers()) )
                        {
                            // If the parent product's price is zero and it has a free product reward, then
                            // the FP rwd must have only applied to priced modifiers
                            // In this case, we need to additionally subtract the free product reward amount from the subtotal
                            for (LoyaltyReward freeProdRwd : product.getFreeProductRwds())
                            {
                                subtotal = subtotal.add(freeProdRwd.getTotalItemPrice());
                            }
                        }
                        else
                        {
                            //HP 4830 - We should only be adding the products total item price to the subtotal if we arent doing the above step of adding in the modifiers to cover a $0 product after rewards
                            subtotal = subtotal.add(product.getTotalItemPrice());
                        }
                    }
                }
                else if (transItems.get(x) instanceof PaidOut)
                {
                    subtotal = subtotal.add(((PaidOut)transItems.get(x)).getTotalItemPrice());
                }
                else if (transItems.get(x) instanceof ReceivedOnAcct)
                {
                    subtotal = subtotal.add(((ReceivedOnAcct)transItems.get(x)).getTotalItemPrice());
                }
                else if (transItems.get(x) instanceof Donation)
                {
                    subtotal = subtotal.add(((Donation)transItems.get(x)).getTotalItemPrice());
                }
                else if (transItems.get(x) instanceof LoyaltyReward)
                {
                    subtotal = subtotal.add(((LoyaltyReward)transItems.get(x)).getTotalItemPrice());
                }
            }
        }
        catch (Exception e)
        {
            ReceiptGen.logError("TransactionData.getSubtotal", e);
        }

        return subtotal;
    }

    public BigDecimal getTotalTax()
    {
        BigDecimal totalTax = new BigDecimal(0.0).setScale(2, RoundingMode.HALF_UP);

        for (HashMap.Entry<Integer,Tax> entry : taxes.entrySet())
        {
            totalTax = totalTax.add(entry.getValue().getTotalItemPrice());
        }

        return totalTax;
    }

    public BigDecimal getSubtotalDsctTotal()
    {
        BigDecimal subtotalDsctTotal = new BigDecimal(0.0).setScale(2, RoundingMode.HALF_UP);

        for (HashMap.Entry<Integer,Discount> entry : subtotalDiscounts.entrySet())
        {
            subtotalDsctTotal = subtotalDsctTotal.add(entry.getValue().getTotalItemPrice());
        }

        return subtotalDsctTotal;
    }

    public BigDecimal getSubtotalSurchargeTotal()
    {
        BigDecimal subtotalSrchgTotal = new BigDecimal(0.0).setScale(2, RoundingMode.HALF_UP);

        for (Surcharge srchg : subtotalSurcharges)
        {
            if (srchg.getApplicationTypeID() == TypeData.SurchargeApplicationType.SUBTOTAL)
            {
                subtotalSrchgTotal = subtotalSrchgTotal.add(srchg.getTotalItemPrice());
            }
        }

        return subtotalSrchgTotal;
    }

    public BigDecimal getTransactionTotal()
    {
        BigDecimal transactionTotal = getSubtotal().add(getTotalTax());
        transactionTotal = transactionTotal.add(getSubtotalDsctTotal());
        transactionTotal = transactionTotal.add(getNonFreeProdRewardTotal());
        transactionTotal = transactionTotal.add(getSubtotalSurchargeTotal());

        return transactionTotal;
    }

    public BigDecimal getTenderTotal()
    {
        BigDecimal tenderTotal = new BigDecimal(0.0).setScale(2, RoundingMode.HALF_UP);

        for (Tender tndr : tenders)
        {
            if (tndr.getAmount() < 0 && transTypeID != TypeData.TranType.REFUND && transTypeID != TypeData.TranType.VOID)
            {
                continue;
            }
            tenderTotal = tenderTotal.add(tndr.getTotalItemPrice());
        }

        return tenderTotal;
    }

    public BigDecimal getChangeAmount()
    {
        BigDecimal transTotal = getTransactionTotal();
        BigDecimal tenderTotal = getTenderTotal();
        BigDecimal changeAmount = getTransactionTotal().subtract(getTenderTotal());

        return changeAmount;
    }

    // Returns the sum of all loyalty rewards that are NOT of type Free Product
    public BigDecimal getNonFreeProdRewardTotal()
    {
        BigDecimal rewardTotal = new BigDecimal(0.0).setScale(2, RoundingMode.HALF_UP);

        for (LoyaltyReward reward : rewards)
        {
            if (reward.isFreeProductReward() == false)
            {
                rewardTotal = rewardTotal.add(reward.getTotalItemPrice());
            }
        }

        return rewardTotal;
    }
    // endregion

    /**
     * <p>Utility method to get information on the terminal that made the transaction.</p>
     *
     * @param terminalID The ID of the terminal that the transaction was made from.
     * @return A {@link HashMap} containing information on the terminal that made the transaction.
     */
    @SuppressWarnings("unchecked")
    private HashMap getTerminalDataInfo (int terminalID) {
        HashMap terminalDataInfo = new HashMap();

        try {
            if (terminalID > 0) {
                ArrayList<HashMap> queryRes = dm.parameterizedExecuteQuery("data.ReceiptGen.GetTransactionTerminalData", new Object[]{terminalID}, true);
                if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
                    terminalDataInfo = queryRes.get(0);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to get the terminal information for the transaction in TransactionData.getTerminalDataInfo", Logger.LEVEL.ERROR);
        }

        return terminalDataInfo;
    }

    /**
     * <p>Determines whether or not this transaction is complete or not.</p>
     *
     * @return Whether or not this transaction is complete or not.
     */
    public boolean isTransactionComplete () {
        return ((txnHdrData != null) && (txnHdrData.getPaTransStatusID() == TypeData.TransactionStatus.COMPLETED));
    }

    /**
     * <p>Determines whether or not this transaction was made remotely.</p>
     *
     * @return Whether or not this transaction was made remotely.
     */
    public boolean isTransactionRemote () {
        return ((txnHdrData != null) && ((txnHdrData.getOnlineOrder()) || (txnHdrData.getRemoteOrderTerminal())));
    }

    /**
     * <p>Tries to find the {@link Product} transaction line item with the given transaction line item ID.</p>
     *
     * @param transLineItemID ID of the the transaction line item to get the product for.
     * @return The {@link Product} transaction line item with the given transaction line item ID.
     */
    @SuppressWarnings("Convert2streamapi")
    public Product getProductFromLineItemID (BigDecimal transLineItemID) {
        Product productMatch = null;

        if ((transLineItemID != null) && (!DataFunctions.isEmptyCollection(txnLineItems))) {
            for (ITransLineItem transLineItem : txnLineItems) {
                if ((transLineItem != null) && (transLineItem instanceof Product)) {
                    // cast the ITransLineItem to a Product
                    Product product = ((Product) transLineItem);
                    // check if the product is a match
                    if (product.getPaTransLineItemID().equals(transLineItemID)) {
                        productMatch = product;
                        break; // match has been found no need to keep processing
                    }
                }
            }
        }

        return productMatch;
    }

    /**
     * <p>Queries the database to get the printer host being used within the given revenue center.</p>
     *
     * @param revenueCenterID The ID of the revenue center to get the printer host within.
     * @return The ID of the printer host being used within the given revenue center.
     */
    private int getPrinterHostIDForRC (int revenueCenterID) {

        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to TransactionData.getPrinterHostIDForRC must be greater than 0!", Logger.LEVEL.ERROR);
            return -1;
        }

        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetPrinterHostInRC").addIDList(1, revenueCenterID);
        Object queryRes = sql.getSingleField(dm);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString()))) {
            return Integer.parseInt(queryRes.toString());
        }

        return -1;
    }

    //--------------------------------------------------------------------------
    //  region Getters and Setters
    //--------------------------------------------------------------------------
    public DataManager getDataManager()
    {
        return dm;
    }

    public void setDataManager(DataManager dm)
    {
        this.dm = dm;
    }

    public String getCashierName()
    {
        return cashierName;
    }

    public void setCashierName(String newCashierName)
    {
        cashierName = newCashierName;
    }

    public String getOrigTransactionID()
    {
        return originalTransactionID;
    }

    public void setOrigTransactionID(String newOrigTransID)
    {
        originalTransactionID = newOrigTransID;
    }

    public String getTransactionID()
    {
        return transactionID;
    }

    public void setTransactionID(String newTransID)
    {
        transactionID = newTransID;
    }

    public String getTransDateTime()
    {
        return transDateTime;
    }

    public String getTransDate()
    {
        return transDate;
    }

    public void setTransDate(String newTransDate)
    {
        transDate = newTransDate;
    }

    public String getTransTime()
    {
        return transTime;
    }

    public void setTransTime(String newTransTime)
    {
        transTime = newTransTime;
    }

    public int getTerminalID()
    {
        return terminalID;
    }

    public void setTerminalID(int terminalID)
    {
        this.terminalID = terminalID;
    }

    public int getTransTypeID()
    {
        return transTypeID;
    }

    public void setTransTypeID(int newTransTypeID)
    {
        this.transTypeID = newTransTypeID;
    }

    public int getTrainingModeTransTypeID()
    {
        return trainingModeTransTypeID;
    }

    public void setTrainingModeTransTypeID(int trainingModeTransTypeID)
    {
        this.trainingModeTransTypeID = trainingModeTransTypeID;
    }

    public int getUserID()
    {
        return userID;
    }

    public void setUserID(int newUserID)
    {
        userID = newUserID;
    }

    public void setRevCenterID(int newRevCenterID)
    {
        revenueCenterID = newRevCenterID;
    }

    public int getPrinterHostID () { return printerHostID; }

    public void setPrinterHostID (int printerHostID) { this.printerHostID = printerHostID; }

    public HashMap<Integer,Discount> getSubtotalDiscounts()
    {
        return subtotalDiscounts;
    }

    public ArrayList<Discount> getOrderedSubtotalDscts()
    {
        return orderedSubtotalDscts;
    }

    public HashMap<Integer,Tax> getTaxes()
    {
        return taxes;
    }

    public ArrayList<Tender> getTenders()
    {
        return tenders;
    }

    public ArrayList getTransItems()
    {
        return transItems;
    }

    public int getRevenueCenterID()
    {
        return revenueCenterID;
    }

    public void setRevenueCenterID(int newRevCenterID)
    {
        revenueCenterID = newRevCenterID;
    }

    public String getTransComment()
    {
        return transComment;
    }

    public String getDeliveryLocation () { return deliveryLocation; }

    public EmployeeData getLoyaltyAcct()
    {
        return loyaltyAcct;
    }

    public void setLoyaltyAcct(EmployeeData newLoyaltyAcct)
    {
        this.loyaltyAcct = newLoyaltyAcct;
    }

    public ArrayList<LoyaltyReward> getRewards()
    {
        return rewards;
    }

    // Searches the transaction's ArrayList of rewards and returns the reward with the matching ID and amount
    // If the reward is not found, return null
    private LoyaltyReward getLoyaltyReward(int rewardID, BigDecimal itemTxDscAmt)
    {
        LoyaltyReward rwd = null;

        // Round "amount" the same way
        itemTxDscAmt = itemTxDscAmt.setScale(2, RoundingMode.HALF_UP);
        itemTxDscAmt = itemTxDscAmt.setScale(2, RoundingMode.UNNECESSARY);

        for (LoyaltyReward reward : rewards)
        {
            BigDecimal rwdAmt = new BigDecimal(reward.getAmount());
            rwdAmt = rwdAmt.setScale(2, RoundingMode.HALF_UP);
            rwdAmt = rwdAmt.setScale(2, RoundingMode.UNNECESSARY);

            //For Free Product rewards, we want to match on the rewardID and the amount because we display the reward value
            if (reward.getRewardTypeID() == PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS.toInt()){
                if (reward.getRewardID() == rewardID) {
                    switch (transTypeID) {
                        case 2: //void
                        case 3: //refund
                            if (itemTxDscAmt.compareTo(rwdAmt.abs()) == 0) {
                                rwd = reward;
                            }
                            break;
                        default:
                            if (itemTxDscAmt.compareTo(rwdAmt) == 0) {
                                rwd = reward;
                            }
                            break;
                    }
                }
            } else { //For transaction credit rewards, only the short name is shown by the product.  We don't need the specific reward amt information thus we don't need to match on the amt
                if (reward.getRewardID() == rewardID) {
                    rwd = reward;
                }
            }

            if (rwd != null){
                break;
            }
        }

        return rwd;
    }

    public String getSuspTxnNameLabel() {
        return suspTxnNameLabel;
    }

    public void setSuspTxnNameLabel(String suspTxnNameLabel) {
        this.suspTxnNameLabel = suspTxnNameLabel;
    }

    public String getSuspTxnName() {
        return suspTxnName;
    }

    public void setSuspTxnName(String suspTxnName) {
        this.suspTxnName = suspTxnName;
    }

    public int getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(int transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public ArrayList<Surcharge> getSubtotalSurcharges()
    {
        return subtotalSurcharges;
    }

    private Surcharge getSurcharge (int surchargeID, BigDecimal amount)
    {
        Surcharge surcharge = null;

        // Round "amount" the same way
        amount = amount.setScale(2, RoundingMode.HALF_UP);
        amount = amount.setScale(2, RoundingMode.UNNECESSARY);

        ArrayList<Surcharge> allSurcharges = new ArrayList<Surcharge>();
        allSurcharges.addAll(subtotalSurcharges);
        allSurcharges.addAll(itemSurcharges);

        for (Surcharge tmpSurcharge : allSurcharges)
        {
            BigDecimal surchargeAmt = new BigDecimal(String.valueOf(tmpSurcharge.getAmount()));
            surchargeAmt = surchargeAmt.setScale(2, RoundingMode.HALF_UP);
            surchargeAmt = surchargeAmt.setScale(2, RoundingMode.UNNECESSARY);

            if ( (tmpSurcharge.getSurchargeID() == surchargeID) && (amount.compareTo(surchargeAmt) == 0) )
            {
                surcharge = tmpSurcharge;
                break;
            }
        }

        return surcharge;
    }

    public int getOrderTypeID() {
        return orderTypeID;
    }

    public void setOrderTypeID(int orderTypeID) {
        this.orderTypeID = orderTypeID;
    }

    public String getTransDateNoYear() {
        return transDateNoYear;
    }

    public void setTransDateNoYear(String transDateNoYear) {
        this.transDateNoYear = transDateNoYear;
    }

    public String getTransTimeNoSeconds() {
        return transTimeNoSeconds;
    }

    public void setTransTimeNoSeconds(String transTimeNoSeconds) {
        this.transTimeNoSeconds = transTimeNoSeconds;
    }

    /**
     * <p>Getter for the terminalData field of the {@link TransactionData}.</p>
     *
     * @return The terminalData field of the {@link TransactionData}.
     */
    public TerminalData getTerminalData () {
        return terminalData;
    }

    /**
     * <p>Setter for the terminalData field of the {@link TransactionData}.</p>
     *
     * @param terminalData The terminalData field of the {@link TransactionData}.
     */
    public void setTerminalData (TerminalData terminalData) {
        this.terminalData = terminalData;
    }

    /**
     * <p>Getter for the txnHdrData field of the {@link TransactionData}.</p>
     *
     * @return The txnHdrData field of the {@link TransactionData}.
     */
    public TransactionHeaderData getTxnHdrData () {
        return txnHdrData;
    }

    /**
     * <p>Setter for the txnHdrData field of the {@link TransactionData}.</p>
     *
     * @param txnHdrData The txnHdrData field of the {@link TransactionData}.
     */
    public void setTxnHdrData (TransactionHeaderData txnHdrData) {
        this.txnHdrData = txnHdrData;
    }

    /**
     * <p>Getter for the txnLineItems field of the {@link TransactionData}.</p>
     *
     * @return The txnLineItems field of the {@link TransactionData}.
     */
    public ArrayList<ITransLineItem> getTxnLineItems () {
        return txnLineItems;
    }

    /**
     * <p>Setter for the txnLineItems field of the {@link TransactionData}.</p>
     *
     * @param txnLineItems The txnLineItems field of the {@link TransactionData}.
     */
    public void setTxnLineItems (ArrayList<ITransLineItem> txnLineItems) {
        this.txnLineItems = txnLineItems;
    }

    /**
     * <p>Getter for the lineItemEvaluator field of the {@link TransactionData}.</p>
     *
     * @return The lineItemEvaluator field of the {@link TransactionData}.
     */
    public LineItemEvaluator getLineItemEvaluator () {
        return lineItemEvaluator;
    }

    /**
     * <p>Setter for the lineItemEvaluator field of the {@link TransactionData}.</p>
     *
     * @param lineItemEvaluator The lineItemEvaluator field of the {@link TransactionData}.
     */
    public void setLineItemEvaluator (LineItemEvaluator lineItemEvaluator) {
        this.lineItemEvaluator = lineItemEvaluator;
    }

    //endregion
}
