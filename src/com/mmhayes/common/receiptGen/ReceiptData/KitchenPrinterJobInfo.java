package com.mmhayes.common.receiptGen.ReceiptData;

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kitchenPrinters.KitchenPrinterCommon;
import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TransactionData.Plu;
import com.mmhayes.common.receiptGen.TransactionData.TransactionItem;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by nyu on 10/13/2016.
 */
public class KitchenPrinterJobInfo
{
    private int paTransactionID = 0;
    private int terminalID = 0;
    private int revenueCenterID = 0;
    private int paTransTypeID = 0;

    private KPJobHeader kpJobHeader;
    private ArrayList<KPJobDetail> kpJobDetails = new ArrayList<KPJobDetail>();     // Holds print queue details for transaction details
    private ArrayList plainTxtDetails = new ArrayList();                            // We need this because objects (classes) can't be passed over XML RPC

    // store the ID of the printer with a name of [ No Printer ]
    private long EXPEDITOR_NO_PRINTER = 0L;

    private DataManager dm;
    private static String logFileName = "KitchenPrinter.log";

    public KitchenPrinterJobInfo (int paTransactionID, int terminalID, int revenueCenterID, int transTypeID, DataManager dm) {
        this.paTransactionID = paTransactionID;
        this.terminalID = terminalID;
        this.revenueCenterID = revenueCenterID;
        this.paTransTypeID = transTypeID;
        this.dm = dm;

        kpJobHeader = buildKPJobHeader(paTransactionID, terminalID, dm);
    }

    /**
     * Return and log the results of the given query.
     *
     * @param sqlPropName {@link String} SQL property name of the query we wish to execute.
     * @param params {@link Object[]} Arguments to be used in the query we wish to execute.
     * @return {@link ArrayList<T>} The results of the query we wished to execute.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "unchecked"})
    private <T> ArrayList<T> getItemDetailList (String sqlPropName, Object[] params) {

        try {
            // try to execute the query with the given sql property name and parameters
            ArrayList<T> details = null;
            if (dm != null) {
                details = dm.parameterizedExecuteQuery(sqlPropName, params, logFileName, true);
            }

            if ((details != null) && (!details.isEmpty())) {
                // log and return the details
                logCollection(details, sqlPropName);
                return details;
            }
            else if ((details != null) && (details.isEmpty())) {
                Logger.logMessage("No results were returned from the query with a sql property name of: " +
                                Objects.toString(sqlPropName, "NULL")+" in KitchenPrinterJobInfo.getItemDetailList",
                        logFileName, Logger.LEVEL.DEBUG);
            }
            else if (details == null) {
                Logger.logMessage("Invalid response returned from the query with a sql property name of: " +
                                Objects.toString(sqlPropName, "NULL")+" in KitchenPrinterJobInfo.getItemDetailList",
                        logFileName, Logger.LEVEL.DEBUG);
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to obtain the results from the query with a sql property " +
                            "name of: "+Objects.toString(sqlPropName, "NULL")+" in KitchenPrinterJobInfo.getItemDetailList",
                    logFileName, Logger.LEVEL.DEBUG);
        }

        return new ArrayList<>();
    }

    /**
     * Logs the items that are stored in the given collection.
     *
     * @param details {@link Collection<?>} Collection we wish to log, the type of collection is irrelevant.
     * @param listName {@link String} The name of the collection we are trying to log.
     */
    private void logCollection (Collection<?> details, String listName) {

        try {
            if ((details != null) && (!details.isEmpty())) {
                Logger.logMessage("Retrieved the following for "+Objects.toString(listName, "NULL") + ":", logFileName,
                        Logger.LEVEL.DEBUG);
                details.forEach((detail) -> {
                    if (detail != null) {
                        Logger.logMessage(detail.toString(), logFileName, Logger.LEVEL.DEBUG);
                    }
                });
                Logger.logMessage("----------", logFileName, Logger.LEVEL.DEBUG);
            }
            else {
                Logger.logMessage("Unexpected response for "+Objects.toString(listName, "NULL") +
                                ", is the PATransactionID '"+Objects.toString(paTransactionID, "NULL")+"' valid?",
                        logFileName, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to log the collection named " +
                            Objects.toString(listName, "NULL")+" in KitchenPrinterJobInfo.logCollection", logFileName,
                    Logger.LEVEL.ERROR);
        }
    }

    /**
     * Creates print jobs details.
     *
     * @param txnData {@link TransactionData} Data from the transaction.
     */
    @SuppressWarnings({"unchecked", "OverlyComplexMethod", "SpellCheckingInspection"})
    public void createJobDetails (TransactionData txnData) {

        try {
            if (dm == null) {
                Logger.logMessage("KitchenPrinterJobInfo.createJobDetails: DataManager object is null", logFileName,
                        Logger.LEVEL.ERROR);
                return;
            }

            // get the printers within the revenue center
            ArrayList<PrinterData> printers = getPrintersInRevenueCenter(txnData.getRevenueCenterID());
            HashMap<String, ArrayList<PrinterData>> sortedPrinters = new HashMap<>();
            if ((printers != null) && (!printers.isEmpty())) {
                sortedPrinters = sortPrintersInRevCntr(printers);
            }
            // log the sorted printers for debugging purposes
            if ((sortedPrinters != null) && (!sortedPrinters.isEmpty())) {
                Logger.logMessage("Now logging sorted printers within the revenue center with an ID of " +
                        Objects.toString(txnData.getRevenueCenterID(), "NULL")+".....", logFileName, Logger.LEVEL.DEBUG);
                sortedPrinters.forEach((k, v) -> {
                    if ((k != null) && (!k.isEmpty())) {
                        Logger.logMessage("PRINTER CONFIG TYPE: "+Objects.toString(k, "NULL"), logFileName, Logger.LEVEL.DEBUG);
                    }
                    if ((v != null) && (!v.isEmpty())) {
                        v.forEach(printer -> {
                            if (printer != null) {
                                Logger.logMessage("    PRINTER: "+Objects.toString(printer.toString(), "NULL"), logFileName, Logger.LEVEL.DEBUG);
                            }
                        });
                    }
                });
                Logger.logMessage("Logging of sorted printers within the revenue center with an ID of " +
                        Objects.toString(txnData.getRevenueCenterID(), "NULL")+" has completed.", logFileName, Logger.LEVEL.DEBUG);
                // build the KPJobDetails
                buildKPJobDetails(txnData, sortedPrinters);
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to create the print job details in " +
                    "KitchenPrinterJobInfo.createJobDetails", logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Gets the printers within the given revenue center.
     *
     * @param revenueCenterID ID of the revenue center in which the transaction took place.
     * @return {@link ArrayList<PrinterData>} The printers within the given revenue center.
     */
    @SuppressWarnings("unchecked")
    private ArrayList<PrinterData> getPrintersInRevenueCenter (int revenueCenterID) {
        ArrayList<PrinterData> printers = new ArrayList<>();

        try {
            if (revenueCenterID > 0) {
                // query the database to find printers within the given revenue center
                ArrayList<HashMap> queryRes = dm.parameterizedExecuteQuery(
                        "data.kitchenPrinter.GetPrintersInRevCntrForTransLineItems", new Object[]{revenueCenterID}, true);
                if ((queryRes != null) && (!queryRes.isEmpty())) {
                    queryRes.forEach(hm -> {
                        if ((hm != null) && (!hm.isEmpty())) {
                            printers.add(new PrinterData(hm));
                        }
                    });
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to get the printers within the revenue center ID: " +
                    Objects.toString(revenueCenterID, "NULL")+" in KitchenPrinterJobInfo.getPrintersInRevenueCenter",
                    Logger.LEVEL.ERROR);
        }

        return printers;
    }

    /**
     * Sort printers in the revenue center based on their TypeData.PrinterConfigType.
     *
     * @param printers {@link ArrayList<PrinterData>} The printers to sort.
     * @return {@link HashMap<String, ArrayList<PrinterData>>} The sorted printers.
     */
    @SuppressWarnings({"SpellCheckingInspection", "OverlyComplexMethod", "TypeMayBeWeakened"})
    private HashMap<String, ArrayList<PrinterData>> sortPrintersInRevCntr (ArrayList<PrinterData> printers) {
        HashMap<String, ArrayList<PrinterData>> sortedPrinters = new HashMap<>();

        try {
            Object queryRes = new DataManager().parameterizedExecuteScalar("data.kitchenPrinter.GetExpPrinterNoPrinter", new Object[]{});
            if ((queryRes != null) && (!queryRes.toString().isEmpty()) && (NumberUtils.isNumber(queryRes.toString()))) {
                EXPEDITOR_NO_PRINTER = ((long) queryRes);
            }
            else {
                EXPEDITOR_NO_PRINTER = 0L;
            }

            if ((printers != null) && (!printers.isEmpty())) {
                ArrayList<PrinterData> printersByPrinterConfigType;
                for (PrinterData printer : printers) {
                    if (printer != null) {
                        // determine the config type of the printer
                        String printerConfigType = "";
                        // check for KP prep
                        if ((printer.getPrinterHardwareTypeID() == 1) && (printer.getPrinterTypeID() == 1)) {
                            printerConfigType = TypeData.PrinterConfigType.KP_PREP;
                        }
                        // check for KDS prep
                        if ((printer.getPrinterHardwareTypeID() == 2) && (printer.getPrinterTypeID() == 1)) {
                            printerConfigType = TypeData.PrinterConfigType.KDS_PREP;
                        }
                        // check for KP expeditor
                        if ((!printer.getRemoteOrderUseExpeditor()) && (printer.getPrinterID() != EXPEDITOR_NO_PRINTER)
                                && (printer.getPrinterID() == printer.getExpeditorPrinterID()) && (printer.getPrinterHardwareTypeID() == 1)
                                && (printer.getPrinterTypeID() == 2)) {
                            printerConfigType = TypeData.PrinterConfigType.KP_EXPEDITOR;
                        }
                        // make sure it's not a KP expeditor serving as the remote order printer
                        else if ((printer.getRemoteOrderUseExpeditor()) && (printer.getRemoteOrderPrinterID() > 0) && (printer.getPrinterID() != EXPEDITOR_NO_PRINTER)
                                && (printer.getPrinterID() == printer.getExpeditorPrinterID()) && (printer.getPrinterHardwareTypeID() == 1)
                                && (printer.getPrinterTypeID() == 2)) {
                            printerConfigType = TypeData.PrinterConfigType.KP_EXPEDITOR;
                        }
                        // check for KDS expeditor
                        if ((!printer.getRemoteOrderUseExpeditor()) && (printer.getPrinterID() != EXPEDITOR_NO_PRINTER)
                                && (printer.getPrinterID() == printer.getExpeditorPrinterID()) && (printer.getPrinterHardwareTypeID() == 2)
                                && (printer.getPrinterTypeID() == 2)) {
                            printerConfigType = TypeData.PrinterConfigType.KDS_EXPEDITOR;
                        }
                        // make sure it's not a KDS expeditor serving as the remote order printer
                        else if ((printer.getRemoteOrderUseExpeditor()) && (printer.getRemoteOrderPrinterID() > 0) && (printer.getPrinterID() != EXPEDITOR_NO_PRINTER)
                                && (printer.getPrinterID() == printer.getExpeditorPrinterID()) && (printer.getPrinterHardwareTypeID() == 2)
                                && (printer.getPrinterTypeID() == 2)) {
                            printerConfigType = TypeData.PrinterConfigType.KDS_EXPEDITOR;
                        }
                        // check for KP remote
                        if ((!printer.getRemoteOrderUseExpeditor()) && (printer.getPrinterID() == printer.getRemoteOrderPrinterID())
                                && (printer.getPrinterHardwareTypeID() == 1) && (printer.getPrinterTypeID() == 4)) {
                            printerConfigType = TypeData.PrinterConfigType.KP_REMOTE;
                        }
                        // make sure it's not a KP expeditor serving as the remote order printer
                        else if ((printer.getRemoteOrderUseExpeditor()) && (printer.getRemoteOrderPrinterID() > 0)
                                && (printer.getPrinterID() == printer.getRemoteOrderPrinterID()) && (printer.getPrinterHardwareTypeID() == 1)
                                && (printer.getPrinterTypeID() == 4)) {
                            printerConfigType = TypeData.PrinterConfigType.KP_REMOTE;
                        }
                        // check for KDS remote
                        if ((!printer.getRemoteOrderUseExpeditor()) && (printer.getPrinterID() == printer.getRemoteOrderPrinterID())
                                && (printer.getPrinterHardwareTypeID() == 2) && (printer.getPrinterTypeID() == 4)) {
                            printerConfigType = TypeData.PrinterConfigType.KDS_REMOTE;
                        }
                        // make sure it's not a KDS expeditor serving as the remote order printer
                        else if ((printer.getRemoteOrderUseExpeditor()) && (printer.getRemoteOrderPrinterID() > 0)
                                && (printer.getPrinterID() == printer.getRemoteOrderPrinterID()) && (printer.getPrinterHardwareTypeID() == 2)
                                && (printer.getPrinterTypeID() == 4)) {
                            printerConfigType = TypeData.PrinterConfigType.KDS_REMOTE;
                        }
                        // check for KP expeditor being used as a remote order printer
                        if ((printer.getRemoteOrderUseExpeditor()) && (printer.getRemoteOrderPrinterID() <= 0)
                                && (printer.getExpeditorPrinterID() > 0) && (printer.getExpeditorPrinterID() != EXPEDITOR_NO_PRINTER)
                                && (printer.getPrinterHardwareTypeID() == 1) && (printer.getPrinterTypeID() == 2)) {
                            printerConfigType = TypeData.PrinterConfigType.KP_EXPEDITOR_AS_REMOTE;
                        }
                        // check for KDS expeditor being used as a remote order printer
                        if ((printer.getRemoteOrderUseExpeditor()) && (printer.getRemoteOrderPrinterID() <= 0)
                                && (printer.getExpeditorPrinterID() > 0) && (printer.getExpeditorPrinterID() != EXPEDITOR_NO_PRINTER)
                                && (printer.getPrinterHardwareTypeID() == 2) && (printer.getPrinterTypeID() == 2)) {
                            printerConfigType = TypeData.PrinterConfigType.KDS_EXPEDITOR_AS_REMOTE;
                        }

                        // use the printerConfigType to sort the printers
                        if (!printerConfigType.isEmpty()) {
                            if (sortedPrinters.containsKey(printerConfigType)) {
                                printersByPrinterConfigType = sortedPrinters.get(printerConfigType);
                            }
                            else {
                                printersByPrinterConfigType = new ArrayList<>();
                            }
                            printersByPrinterConfigType.add(printer);
                            sortedPrinters.put(printerConfigType, printersByPrinterConfigType);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to sort the printers within the revenue center in " +
                    "KitchenPrinterJobInfo.sortPrintersInRevCntr", logFileName, Logger.LEVEL.ERROR);
        }

        return sortedPrinters;
    }

    /**
     * Builds the KPJobDetails for the given transaction.
     *
     * @param txnData {@link TransactionData} The transaction to create KPJobDetails for.
     * @param sortedPrinters {@link HashMap<String, ArrayList<PrinterData>>} The printers within the revenue center in
     *         which the transaction took place.
     */
    @SuppressWarnings({"unchecked", "OverlyComplexMethod"})
    private void buildKPJobDetails (TransactionData txnData, HashMap<String, ArrayList<PrinterData>> sortedPrinters) {

        try {
            if ((txnData != null) && (sortedPrinters != null) && (!sortedPrinters.isEmpty())) {
                // check if the the transaction was made remotely
                boolean isRemote = isTransactionRemote(txnData);

                // get printers based on their printer config type
                ArrayList<PrinterData> kpPrepPrinters = getPrintersFromConfigType(TypeData.PrinterConfigType.KP_PREP, sortedPrinters);
                ArrayList<PrinterData> kdsPrepPrinters =  getPrintersFromConfigType(TypeData.PrinterConfigType.KDS_PREP, sortedPrinters);
                ArrayList<PrinterData> kpExpeditorPrinters = getPrintersFromConfigType(TypeData.PrinterConfigType.KP_EXPEDITOR, sortedPrinters);
                ArrayList<PrinterData> kdsExpeditorPrinters = getPrintersFromConfigType(TypeData.PrinterConfigType.KDS_EXPEDITOR, sortedPrinters);
                ArrayList<PrinterData> kpRemotePrinters = getPrintersFromConfigType(TypeData.PrinterConfigType.KP_REMOTE, sortedPrinters);
                ArrayList<PrinterData> kdsRemotePrinters = getPrintersFromConfigType(TypeData.PrinterConfigType.KDS_REMOTE, sortedPrinters);
                ArrayList<PrinterData> kpExpeditorAsRemotePrinters = getPrintersFromConfigType(TypeData.PrinterConfigType.KP_EXPEDITOR_AS_REMOTE, sortedPrinters);
                ArrayList<PrinterData> kdsExpeditorAsRemotePrinters = getPrintersFromConfigType(TypeData.PrinterConfigType.KDS_EXPEDITOR_AS_REMOTE, sortedPrinters);

                // get the products in the transaction
                ArrayList transItems = txnData.getTransItems();
                if ((transItems != null) && (!transItems.isEmpty())) {
                    // store the IDs of the products in the transaction in the productIDs array
                    int[] productIDs = new int[transItems.size()];
                    for (int i = 0; i < transItems.size(); i++) {
                        if ((transItems.get(i) != null) && (transItems.get(i) instanceof Plu)) {
                            Plu product = ((Plu) transItems.get(i));
                            if (product != null) {
                                productIDs[i] = product.getPluID();
                            }
                        }
                    }

                    // get the printer host ID
                    int printerHostID = -1;
                    if (!DataFunctions.isEmptyMap(sortedPrinters)) {
                        ArrayList<PrinterData> printers = sortedPrinters.entrySet().iterator().next().getValue();
                        if ((!DataFunctions.isEmptyCollection(printers)) && (printers.get(0) != null)) {
                            printerHostID = printers.get(0).getPrinterHostID();
                        }
                    }

                    // get the printers mapped to each printer
                    ArrayList<HashMap> productToPrinterMappings = new ArrayList<>();
                    if (productIDs.length > 0) {
                        productToPrinterMappings = getProductToPrinterMappings(txnData.getRevenueCenterID(), productIDs, printerHostID);
                    }
                    if ((productToPrinterMappings != null) && (!productToPrinterMappings.isEmpty())) {
                        createPrintJobs(transItems, isRemote, productToPrinterMappings,
                                kpPrepPrinters, kdsPrepPrinters, kpExpeditorPrinters, kdsExpeditorPrinters,
                                kpRemotePrinters, kdsRemotePrinters, kpExpeditorAsRemotePrinters, kdsExpeditorAsRemotePrinters);
                    }
                }
                // log the KPJobDetails
                if ((kpJobDetails != null) && (!kpJobDetails.isEmpty())) {
                    Logger.logMessage("********** START PRINT JOBS SENT TO BUILD RECEIPT **********", logFileName, Logger.LEVEL.DEBUG);
                    kpJobDetails.forEach(kpJobDetail -> Logger.logMessage(kpJobDetail.toString(), logFileName, Logger.LEVEL.DEBUG));
                    Logger.logMessage("********** END PRINT JOBS SENT TO BUILD RECEIPT **********", logFileName, Logger.LEVEL.DEBUG);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to build the KPJobDetails for the transaction with an ID " +
                    "of "+Objects.toString(txnData.getTransactionID(), "NULL")+" in " +
                    "KitchenPrinterJobInfo.buildKPJobDetails", logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Determines whether or not the given transaction was made remotely.
     *
     * @param txnData {@link TransactionData} The transaction to check for whether or not it was made remotely.
     * @return Whether or not the given transaction was made remotely.
     */
    private boolean isTransactionRemote (TransactionData txnData) {
        boolean isTransactionRemote = false;

        try {
            Object queryRes = dm.parameterizedExecuteScalar("data.newKitchenPrinter.GetIsTerminalRemote", new Object[]{txnData.getTerminalID()});
            if (queryRes != null) {
                if (queryRes.toString().equalsIgnoreCase("1")) {
                    isTransactionRemote = true;
                }
                else if (queryRes.toString().equalsIgnoreCase("0")) {
                    isTransactionRemote = false;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the transaction " +
                    "with and ID of %s was made remotely in KitchenPrinterJobInfo.isTransactionRemote",
                    Objects.toString(txnData.getTransactionID(), "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return isTransactionRemote;
    }

    /**
     * Gets the printers of the given printer config type from the sorted printers.
     *
     * @param printerConfigType {@link String} The type of printers we want to get from the sorted printers.
     * @param sortedPrinters {@link HashMap<String, ArrayList<PrinterData>>} The printers within the revenue center in
     *         which the transaction took place.
     * @return {@link ArrayList<PrinterData>} Printers of the given printer config type from the sorted printers.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private ArrayList<PrinterData> getPrintersFromConfigType (String printerConfigType,
                                                              HashMap<String, ArrayList<PrinterData>> sortedPrinters) {
        ArrayList<PrinterData> printers = new ArrayList<>();

        try {
            if ((printerConfigType != null) && (!printerConfigType.isEmpty()) && (sortedPrinters != null) && (!sortedPrinters.isEmpty())) {
                if (sortedPrinters.containsKey(printerConfigType)) {
                    printers = sortedPrinters.get(printerConfigType);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to get the printers with a printer config type of " +
                    Objects.toString(printerConfigType, "NULL")+" in KitchenPrinterJobInfo.getPrintersFromConfigType",
                    logFileName, Logger.LEVEL.ERROR);
        }

        return printers;
    }

    /**
     * Gets the printers mapped to the products in the transaction.
     *
     * @param revenueCenterID ID of the revenue center in which the transaction took place.
     * @param productIDs IDs of the products to get the mapped printers for.*
     * @param printerHostID ID of the printer host that will execute the print job.
     * @return {@link ArrayList<HashMap>} Printers mapped to the products in the transaction.
     */
    @SuppressWarnings({"unchecked", "TypeMayBeWeakened"})
    private ArrayList<HashMap> getProductToPrinterMappings (int revenueCenterID, int[] productIDs, int printerHostID) {
        ArrayList<HashMap> productToPrinterMappings = new ArrayList<>();

        try {
            if ((revenueCenterID > 0) && (!DataFunctions.isEmptyIntArr(productIDs))) {
                // query the database to get the printers mapped to each of the products in the transaction
                ArrayList<String> productIDsAL = new ArrayList<>();
                for (int i = 0; i < productIDs.length; i++) {
                    productIDsAL.add(String.valueOf(productIDs[i]));
                }
                DynamicSQL dynamicSQL = new DynamicSQL("data.kitchenPrinter.GetPrintersForProducts")
                        .addIDList(1, StringFunctions.buildStringFromList(productIDsAL, "?"))
                        .addIDList(2, String.valueOf(revenueCenterID))
                        .addIDList(3, String.valueOf(printerHostID));
                ArrayList<HashMap> queryRes = dynamicSQL.serialize(dm);
                if ((queryRes != null) && (!queryRes.isEmpty())) {
                    queryRes.forEach(hm -> {
                        if ((hm != null) && (!hm.isEmpty())) {
                            productToPrinterMappings.add(hm);
                        }
                    });
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to get the printers that have been mapped to the products " +
                    "with IDs of "+Objects.toString(Arrays.toString(productIDs), "NULL")+" in " +
                    "KitchenPrinterJobInfo.getProductToPrinterMappings", logFileName, Logger.LEVEL.ERROR);
        }

        return productToPrinterMappings;
    }

    /**
     * Matches the products from the transaction to the printers they should print on and creates the
     * corresponding print job.
     *
     * @param txnItems {@link ArrayList} Items in the transaction.
     * @param isRemote Whether or not the transaction was made remotely.
     * @param productToPrinterMappings {@link ArrayList<HashMap>} Mappings between products and the printers they should print out on.
     * @param kpPrepPrinters {@link ArrayList<PrinterData>} Configured KP prep printers within the revenue center in which the transaction took place.
     * @param kdsPrepPrinters {@link ArrayList<PrinterData>} Configured KDS prep printers within the revenue center in which the transaction took place.
     * @param kpExpeditorPrinters {@link ArrayList<PrinterData>} Configured KP expeditor printers within the revenue center in which the transaction took place.
     * @param kdsExpeditorPrinters {@link ArrayList<PrinterData>} Configured KDS expeditor printers within the revenue center in which the transaction took place.
     * @param kpRemotePrinters {@link ArrayList<PrinterData>} Configured KP remote order printers within the revenue center in which the transaction took place.
     * @param kdsRemotePrinters {@link ArrayList<PrinterData>} Configured KDS remote order printers within the revenue center in which the transaction took place.
     * @param kpExpeditorAsRemotePrinters {@link ArrayList<PrinterData>} Configured KP expeditor as remote order printers within the revenue center in which the transaction took place.
     * @param kdsExpeditorAsRemotePrinters {@link ArrayList<PrinterData>} Configured KDS expeditor as remote order printers within the revenue center in which the transaction took place.
     */
    @SuppressWarnings({"ConstantConditions", "OverlyComplexMethod"})
    private void createPrintJobs (ArrayList txnItems,
                                  boolean isRemote,
                                  ArrayList<HashMap> productToPrinterMappings,
                                  ArrayList<PrinterData> kpPrepPrinters,
                                  ArrayList<PrinterData> kdsPrepPrinters,
                                  ArrayList<PrinterData> kpExpeditorPrinters,
                                  ArrayList<PrinterData> kdsExpeditorPrinters,
                                  ArrayList<PrinterData> kpRemotePrinters,
                                  ArrayList<PrinterData> kdsRemotePrinters,
                                  ArrayList<PrinterData> kpExpeditorAsRemotePrinters,
                                  ArrayList<PrinterData> kdsExpeditorAsRemotePrinters) {

        try {
            if (!DataFunctions.isEmptyCollection(txnItems)) {
                ArrayList<Plu> diningOptions = getDiningOptions(txnItems, productToPrinterMappings);
                ArrayList<Plu> nonDiningOptions = getNonDiningOptions(txnItems, productToPrinterMappings);
                boolean nonDiningOptionsOnExpeditor = false;
                if (!DataFunctions.isEmptyCollection(nonDiningOptions)) {
                    for (Plu nonDiningOption : nonDiningOptions) {
                        if (nonDiningOption.getPrintsOnExpeditor()) {
                            nonDiningOptionsOnExpeditor = true;
                            break;
                        }
                    }
                }
                if (!DataFunctions.isEmptyCollection(diningOptions)) {
                    for (Plu diningOption : diningOptions) {
                        if (diningOption != null) {
                            int[] mappedPrinters = diningOption.getMappedPrinters();
                            if (!DataFunctions.isEmptyIntArr(mappedPrinters)) {
                                // check if we should add the dining option to prep, expeditor or remote order printer(s)
                                ArrayList<PrinterData> kpPrepPrintersForDiningOption = KPPrinterDeterminator.getKPPrepPrintersForDiningOption(mappedPrinters, nonDiningOptions, kpPrepPrinters);
                                ArrayList<PrinterData> kdsPrepPrintersForDiningOption = KPPrinterDeterminator.getKDSPrepPrintersForDiningOption(mappedPrinters, nonDiningOptions, kdsPrepPrinters);
                                ArrayList<PrinterData> kpExpeditorPrintersForDiningOption = null;
                                ArrayList<PrinterData> kdsExpeditorPrintersForDiningOption = null;
                                ArrayList<PrinterData> kpRemoteOrderPrintersForDiningOption = null;
                                ArrayList<PrinterData> kdsRemoteOrderPrintersForDiningOption = null;
                                ArrayList<PrinterData> kpExpeditorAsRemoteOrderPrintersForDiningOption = null;
                                ArrayList<PrinterData> kdsExpeditorAsRemoteOrderPrintersForDiningOption = null;
                                if ((diningOption.getPrintsOnExpeditor()) && (nonDiningOptionsOnExpeditor) && ((!DataFunctions.isEmptyCollection(kpExpeditorPrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorPrinters)))) {
                                    kpExpeditorPrintersForDiningOption = KPPrinterDeterminator.getKPExpeditorPrintersForDiningOption(kpExpeditorPrinters);
                                    kdsExpeditorPrintersForDiningOption = KPPrinterDeterminator.getKDSExpeditorPrintersForDiningOption(kdsExpeditorPrinters, (!kdsPrepPrintersForDiningOption.isEmpty()));
                                }
                                else if ((diningOption.getPrintsOnExpeditor()) && (nonDiningOptionsOnExpeditor) && ((!isRemote) && ((!DataFunctions.isEmptyCollection(kpExpeditorAsRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters))))) {
                                    kpExpeditorPrintersForDiningOption = KPPrinterDeterminator.getKPExpeditorPrintersForProduct(kpExpeditorAsRemotePrinters);
                                    kdsExpeditorPrintersForDiningOption = KPPrinterDeterminator.getKDSExpeditorPrintersForProduct(kdsExpeditorAsRemotePrinters, (!kdsPrepPrintersForDiningOption.isEmpty()));
                                }
                                else if ((!diningOption.getPrintsOnExpeditor()) && ((!DataFunctions.isEmptyCollection(kdsExpeditorPrinters)) || ((!isRemote) && (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters))))) {
                                    if ((!isRemote) && (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters))) {
                                        diningOption.setHiddenKDSStationID(String.valueOf(kdsExpeditorAsRemotePrinters.get(0).getStationID()));
                                        diningOption.setHideOnKDSExpeditor(true);
                                    }
                                    else {
                                        diningOption.setHiddenKDSStationID(String.valueOf(kdsExpeditorPrinters.get(0).getStationID()));
                                        diningOption.setHideOnKDSExpeditor(true);
                                    }
                                }
                                if ((isRemote) && (!DataFunctions.isEmptyCollection(nonDiningOptions)) && ((!DataFunctions.isEmptyCollection(kpRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsRemotePrinters))
                                        || (!DataFunctions.isEmptyCollection(kpExpeditorAsRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters)))) {
                                    kpRemoteOrderPrintersForDiningOption = KPPrinterDeterminator.getKPRemoteOrderPrintersForDiningOption(kpRemotePrinters);
                                    kdsRemoteOrderPrintersForDiningOption = KPPrinterDeterminator.getKDSRemoteOrderPrintersForDiningOption(kdsRemotePrinters);
                                    kpExpeditorAsRemoteOrderPrintersForDiningOption = KPPrinterDeterminator.getKPExpeditorAsRemoteOrderPrintersForDiningOption(kpExpeditorAsRemotePrinters);
                                    kdsExpeditorAsRemoteOrderPrintersForDiningOption = KPPrinterDeterminator.getKDSExpeditorAsRemoteOrderPrintersForDiningOption(kdsExpeditorAsRemotePrinters, (!kdsPrepPrintersForDiningOption.isEmpty()));
                                }

                                // create the print jobs
                                if (!DataFunctions.isEmptyCollection(kpPrepPrintersForDiningOption)) {
                                    kpPrepPrintersForDiningOption.forEach(kpPrepPrinter -> createPrintJob(diningOption, kpPrepPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsPrepPrintersForDiningOption)) {
                                    kdsPrepPrintersForDiningOption.forEach(kdsPrepPrinter -> createPrintJob(diningOption, kdsPrepPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kpExpeditorPrintersForDiningOption)) {
                                    kpExpeditorPrintersForDiningOption.forEach(kpExpeditorPrinter -> createPrintJob(diningOption, kpExpeditorPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsExpeditorPrintersForDiningOption)) {
                                    kdsExpeditorPrintersForDiningOption.forEach(kdsExpeditorPrinter -> createPrintJob(diningOption, kdsExpeditorPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kpRemoteOrderPrintersForDiningOption)) {
                                    kpRemoteOrderPrintersForDiningOption.forEach(kpRemoteOrderPrinter -> createPrintJob(diningOption, kpRemoteOrderPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsRemoteOrderPrintersForDiningOption)) {
                                    kdsRemoteOrderPrintersForDiningOption.forEach(kdsRemoteOrderPrinter -> createPrintJob(diningOption, kdsRemoteOrderPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kpExpeditorAsRemoteOrderPrintersForDiningOption)) {
                                    kpExpeditorAsRemoteOrderPrintersForDiningOption.forEach(kpExpeditorAsRemoteOrderPrinterForDiningOption -> createPrintJob(diningOption, kpExpeditorAsRemoteOrderPrinterForDiningOption));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemoteOrderPrintersForDiningOption)) {
                                    kdsExpeditorAsRemoteOrderPrintersForDiningOption.forEach(kdsExpeditorAsRemoteOrderPrinterForDiningOption -> createPrintJob(diningOption, kdsExpeditorAsRemoteOrderPrinterForDiningOption));
                                }
                            }
                            else {
                                // check if we should add the dining option to expeditor or remote order printer(s)
                                ArrayList<PrinterData> kpExpeditorPrintersForDiningOption = null;
                                ArrayList<PrinterData> kdsExpeditorPrintersForDiningOption = null;
                                ArrayList<PrinterData> kpRemoteOrderPrintersForDiningOption = null;
                                ArrayList<PrinterData> kdsRemoteOrderPrintersForDiningOption = null;
                                ArrayList<PrinterData> kpExpeditorAsRemoteOrderPrintersForDiningOption = null;
                                ArrayList<PrinterData> kdsExpeditorAsRemoteOrderPrintersForDiningOption = null;
                                if ((diningOption.getPrintsOnExpeditor()) && (nonDiningOptionsOnExpeditor) && ((!DataFunctions.isEmptyCollection(kpExpeditorPrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorPrinters)))) {
                                    kpExpeditorPrintersForDiningOption = KPPrinterDeterminator.getKPExpeditorPrintersForDiningOption(kpExpeditorPrinters);
                                    kdsExpeditorPrintersForDiningOption = KPPrinterDeterminator.getKDSExpeditorPrintersForDiningOption(kdsExpeditorPrinters, false);
                                }
                                else if ((diningOption.getPrintsOnExpeditor()) && (nonDiningOptionsOnExpeditor) && ((!isRemote) && ((!DataFunctions.isEmptyCollection(kpExpeditorAsRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters))))) {
                                    kpExpeditorPrintersForDiningOption = KPPrinterDeterminator.getKPExpeditorPrintersForProduct(kpExpeditorAsRemotePrinters);
                                    kdsExpeditorPrintersForDiningOption = KPPrinterDeterminator.getKDSExpeditorPrintersForProduct(kdsExpeditorAsRemotePrinters, false);
                                }
                                else if ((!diningOption.getPrintsOnExpeditor()) && ((!DataFunctions.isEmptyCollection(kdsExpeditorPrinters)) || ((!isRemote) && (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters))))) {
                                    if ((!isRemote) && (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters))) {
                                        diningOption.setHiddenKDSStationID(String.valueOf(kdsExpeditorAsRemotePrinters.get(0).getStationID()));
                                        diningOption.setHideOnKDSExpeditor(true);
                                    }
                                    else {
                                        diningOption.setHiddenKDSStationID(String.valueOf(kdsExpeditorPrinters.get(0).getStationID()));
                                        diningOption.setHideOnKDSExpeditor(true);
                                    }
                                }
                                if ((isRemote) && (!DataFunctions.isEmptyCollection(nonDiningOptions)) && ((!DataFunctions.isEmptyCollection(kpRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsRemotePrinters))
                                        || (!DataFunctions.isEmptyCollection(kpExpeditorAsRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters)))) {
                                    kpRemoteOrderPrintersForDiningOption = KPPrinterDeterminator.getKPRemoteOrderPrintersForDiningOption(kpRemotePrinters);
                                    kdsRemoteOrderPrintersForDiningOption = KPPrinterDeterminator.getKDSRemoteOrderPrintersForDiningOption(kdsRemotePrinters);
                                    kpExpeditorAsRemoteOrderPrintersForDiningOption = KPPrinterDeterminator.getKPExpeditorAsRemoteOrderPrintersForDiningOption(kpExpeditorAsRemotePrinters);
                                    kdsExpeditorAsRemoteOrderPrintersForDiningOption = KPPrinterDeterminator.getKDSExpeditorAsRemoteOrderPrintersForDiningOption(kdsExpeditorAsRemotePrinters, false);
                                }

                                // create the print jobs
                                if (!DataFunctions.isEmptyCollection(kpExpeditorPrintersForDiningOption)) {
                                    kpExpeditorPrintersForDiningOption.forEach(kpExpeditorPrinter -> createPrintJob(diningOption, kpExpeditorPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsExpeditorPrintersForDiningOption)) {
                                    kdsExpeditorPrintersForDiningOption.forEach(kdsExpeditorPrinter -> createPrintJob(diningOption, kdsExpeditorPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kpRemoteOrderPrintersForDiningOption)) {
                                    kpRemoteOrderPrintersForDiningOption.forEach(kpRemoteOrderPrinter -> createPrintJob(diningOption, kpRemoteOrderPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsRemoteOrderPrintersForDiningOption)) {
                                    kdsRemoteOrderPrintersForDiningOption.forEach(kdsRemoteOrderPrinter -> createPrintJob(diningOption, kdsRemoteOrderPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kpExpeditorAsRemoteOrderPrintersForDiningOption)) {
                                    kpExpeditorAsRemoteOrderPrintersForDiningOption.forEach(kpExpeditorAsRemoteOrderPrinterForDiningOption -> createPrintJob(diningOption, kpExpeditorAsRemoteOrderPrinterForDiningOption));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemoteOrderPrintersForDiningOption)) {
                                    kdsExpeditorAsRemoteOrderPrintersForDiningOption.forEach(kdsExpeditorAsRemoteOrderPrinterForDiningOption -> createPrintJob(diningOption, kdsExpeditorAsRemoteOrderPrinterForDiningOption));
                                }
                            }
                        }
                    }
                }
                // iterate through the items in the transaction
                for (Object txnItem : txnItems) {
                    if ((txnItem != null) && (txnItem instanceof Plu)) {
                        Plu product = ((Plu) txnItem);
                        // set the prints on expeditor, is dining option, and mapped printers properties of the product
                        setAdditionalPropertiesForProduct(product, productToPrinterMappings);
                        int[] mappedPrinters = null;
                        if (!product.isDiningOption()) {
                            // if the product is a modifier without any printers mapped to it then use the parent product's mapped printers
                            if ((product.getIsModifier()) && (DataFunctions.isEmptyIntArr(product.getMappedPrinters()))) {
                                Plu modifierParentProduct = getModifiersParentProduct(product, txnItems);
                                if (modifierParentProduct != null) {
                                    mappedPrinters = modifierParentProduct.getMappedPrinters();
                                }
                            }
                            else {
                                mappedPrinters = product.getMappedPrinters();
                            }

                            if (!DataFunctions.isEmptyIntArr(mappedPrinters)) {
                                // product will go to prep printer(s)
                                ArrayList<PrinterData> kpPrepPrintersForProduct = KPPrinterDeterminator.getKPPrepPrintersForProduct(mappedPrinters, kpPrepPrinters);
                                ArrayList<PrinterData> kdsPrepPrintersForProduct = KPPrinterDeterminator.getKDSPrepPrintersForProduct(mappedPrinters, kdsPrepPrinters);
                                ArrayList<PrinterData> kpExpeditorPrintersForProduct = null;
                                ArrayList<PrinterData> kdsExpeditorPrintersForProduct = null;
                                ArrayList<PrinterData> kpRemoteOrderPrintersForProduct = null;
                                ArrayList<PrinterData> kdsRemoteOrderPrintersForProduct = null;
                                ArrayList<PrinterData> kpExpeditorAsRemoteOrderPrintersForProduct = null;
                                ArrayList<PrinterData> kdsExpeditorAsRemoteOrderPrintersForProduct = null;
                                if ((product.getPrintsOnExpeditor()) && ((!DataFunctions.isEmptyCollection(kpExpeditorPrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorPrinters)))) {
                                    kpExpeditorPrintersForProduct = KPPrinterDeterminator.getKPExpeditorPrintersForProduct(kpExpeditorPrinters);
                                    kdsExpeditorPrintersForProduct = KPPrinterDeterminator.getKDSExpeditorPrintersForProduct(kdsExpeditorPrinters, (!kdsPrepPrintersForProduct.isEmpty()));
                                }
                                else if ((product.getPrintsOnExpeditor()) && ((!isRemote) && ((!DataFunctions.isEmptyCollection(kpExpeditorAsRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters))))) {
                                    kpExpeditorPrintersForProduct = KPPrinterDeterminator.getKPExpeditorPrintersForProduct(kpExpeditorAsRemotePrinters);
                                    kdsExpeditorPrintersForProduct = KPPrinterDeterminator.getKDSExpeditorPrintersForProduct(kdsExpeditorAsRemotePrinters, (!kdsPrepPrintersForProduct.isEmpty()));
                                }
                                else if ((!product.getPrintsOnExpeditor()) && ((!DataFunctions.isEmptyCollection(kdsExpeditorPrinters)) || ((!isRemote) && (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters))))) {
                                    if ((!isRemote) && (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters))) {
                                        product.setHiddenKDSStationID(String.valueOf(kdsExpeditorAsRemotePrinters.get(0).getStationID()));
                                        product.setHideOnKDSExpeditor(true);
                                    }
                                    else {
                                        product.setHiddenKDSStationID(String.valueOf(kdsExpeditorPrinters.get(0).getStationID()));
                                        product.setHideOnKDSExpeditor(true);
                                    }
                                }
                                if ((isRemote) && ((!DataFunctions.isEmptyCollection(kpRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsRemotePrinters))
                                        || (!DataFunctions.isEmptyCollection(kpExpeditorAsRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters)))) {
                                    kpRemoteOrderPrintersForProduct = KPPrinterDeterminator.getKPRemoteOrderPrintersForProduct(kpRemotePrinters);
                                    kdsRemoteOrderPrintersForProduct = KPPrinterDeterminator.getKDSRemoteOrderPrintersForProduct(kdsRemotePrinters);
                                    kpExpeditorAsRemoteOrderPrintersForProduct = KPPrinterDeterminator.getKPExpeditorAsRemoteOrderPrintersForProduct(kpExpeditorAsRemotePrinters);
                                    kdsExpeditorAsRemoteOrderPrintersForProduct = KPPrinterDeterminator.getKDSExpeditorAsRemoteOrderPrintersForProduct(kdsExpeditorAsRemotePrinters, (!kdsPrepPrintersForProduct.isEmpty()));
                                }

                                // create the print jobs
                                if (!DataFunctions.isEmptyCollection(kpPrepPrintersForProduct)) {
                                    kpPrepPrintersForProduct.forEach(kpPrepPrinter -> createPrintJob(product, kpPrepPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsPrepPrintersForProduct)) {
                                    kdsPrepPrintersForProduct.forEach(kdsPrepPrinter -> createPrintJob(product, kdsPrepPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kpExpeditorPrintersForProduct)) {
                                    kpExpeditorPrintersForProduct.forEach(kpExpeditorPrinter -> createPrintJob(product, kpExpeditorPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsExpeditorPrintersForProduct)) {
                                    kdsExpeditorPrintersForProduct.forEach(kdsExpeditorPrinter -> createPrintJob(product, kdsExpeditorPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kpRemoteOrderPrintersForProduct)) {
                                    kpRemoteOrderPrintersForProduct.forEach(kpRemoteOrderPrinter -> createPrintJob(product, kpRemoteOrderPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsRemoteOrderPrintersForProduct)) {
                                    kdsRemoteOrderPrintersForProduct.forEach(kdsRemoteOrderPrinter -> createPrintJob(product, kdsRemoteOrderPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kpExpeditorAsRemoteOrderPrintersForProduct)) {
                                    kpExpeditorAsRemoteOrderPrintersForProduct.forEach(kpExpeditorAsRemoteOrderPrinterForProduct -> createPrintJob(product, kpExpeditorAsRemoteOrderPrinterForProduct));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemoteOrderPrintersForProduct)) {
                                    kdsExpeditorAsRemoteOrderPrintersForProduct.forEach(kdsExpeditorAsRemoteOrderPrinterForProduct -> createPrintJob(product, kdsExpeditorAsRemoteOrderPrinterForProduct));
                                }
                            }
                            else {
                                // product might go to expeditor or remote order printer(s)
                                ArrayList<PrinterData> kpExpeditorPrintersForProduct = null;
                                ArrayList<PrinterData> kdsExpeditorPrintersForProduct = null;
                                ArrayList<PrinterData> kpRemoteOrderPrintersForProduct = null;
                                ArrayList<PrinterData> kdsRemoteOrderPrintersForProduct = null;
                                ArrayList<PrinterData> kpExpeditorAsRemoteOrderPrintersForProduct = null;
                                ArrayList<PrinterData> kdsExpeditorAsRemoteOrderPrintersForProduct = null;
                                if ((product.getPrintsOnExpeditor()) && (((!DataFunctions.isEmptyCollection(kpExpeditorPrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorPrinters)))
                                        || ((!isRemote) && ((!DataFunctions.isEmptyCollection(kpExpeditorAsRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters)))))) {
                                    if ((product.getPrintsOnExpeditor()) && ((!DataFunctions.isEmptyCollection(kpExpeditorPrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorPrinters)))) {
                                        kpExpeditorPrintersForProduct = KPPrinterDeterminator.getKPExpeditorPrintersForProduct(kpExpeditorPrinters);
                                        kdsExpeditorPrintersForProduct = KPPrinterDeterminator.getKDSExpeditorPrintersForProduct(kdsExpeditorPrinters, false);
                                    }
                                    else if ((product.getPrintsOnExpeditor()) && ((!isRemote) && ((!DataFunctions.isEmptyCollection(kpExpeditorAsRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters))))) {
                                        kpExpeditorPrintersForProduct = KPPrinterDeterminator.getKPExpeditorPrintersForProduct(kpExpeditorAsRemotePrinters);
                                        kdsExpeditorPrintersForProduct = KPPrinterDeterminator.getKDSExpeditorPrintersForProduct(kdsExpeditorAsRemotePrinters, false);
                                    }
                                    else if ((!product.getPrintsOnExpeditor()) && ((!DataFunctions.isEmptyCollection(kdsExpeditorPrinters)) || ((!isRemote) && (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters))))) {
                                        if ((!isRemote) && (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters))) {
                                            product.setHiddenKDSStationID(String.valueOf(kdsExpeditorAsRemotePrinters.get(0).getStationID()));
                                            product.setHideOnKDSExpeditor(true);
                                        }
                                        else {
                                            product.setHiddenKDSStationID(String.valueOf(kdsExpeditorPrinters.get(0).getStationID()));
                                            product.setHideOnKDSExpeditor(true);
                                        }
                                    }
                                }
                                if ((isRemote) && ((!DataFunctions.isEmptyCollection(kpRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsRemotePrinters))
                                        || (!DataFunctions.isEmptyCollection(kpExpeditorAsRemotePrinters)) || (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemotePrinters)))) {
                                    kpRemoteOrderPrintersForProduct = KPPrinterDeterminator.getKPRemoteOrderPrintersForProduct(kpRemotePrinters);
                                    kdsRemoteOrderPrintersForProduct = KPPrinterDeterminator.getKDSRemoteOrderPrintersForProduct(kdsRemotePrinters);
                                    kpExpeditorAsRemoteOrderPrintersForProduct = KPPrinterDeterminator.getKPExpeditorAsRemoteOrderPrintersForProduct(kpExpeditorAsRemotePrinters);
                                    kdsExpeditorAsRemoteOrderPrintersForProduct = KPPrinterDeterminator.getKDSExpeditorAsRemoteOrderPrintersForProduct(kdsExpeditorAsRemotePrinters, (false));
                                }

                                // create print jobs
                                if (!DataFunctions.isEmptyCollection(kpExpeditorPrintersForProduct)) {
                                    kpExpeditorPrintersForProduct.forEach(kpExpeditorPrinter -> createPrintJob(product, kpExpeditorPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsExpeditorPrintersForProduct)) {
                                    kdsExpeditorPrintersForProduct.forEach(kdsExpeditorPrinter -> createPrintJob(product, kdsExpeditorPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kpRemoteOrderPrintersForProduct)) {
                                    kpRemoteOrderPrintersForProduct.forEach(kpRemoteOrderPrinter -> createPrintJob(product, kpRemoteOrderPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsRemoteOrderPrintersForProduct)) {
                                    kdsRemoteOrderPrintersForProduct.forEach(kdsRemoteOrderPrinter -> createPrintJob(product, kdsRemoteOrderPrinter));
                                }
                                if (!DataFunctions.isEmptyCollection(kpExpeditorAsRemoteOrderPrintersForProduct)) {
                                    kpExpeditorAsRemoteOrderPrintersForProduct.forEach(kpExpeditorAsRemoteOrderPrinterForProduct -> createPrintJob(product, kpExpeditorAsRemoteOrderPrinterForProduct));
                                }
                                if (!DataFunctions.isEmptyCollection(kdsExpeditorAsRemoteOrderPrintersForProduct)) {
                                    kdsExpeditorAsRemoteOrderPrintersForProduct.forEach(kdsExpeditorAsRemoteOrderPrinterForProduct -> createPrintJob(product, kdsExpeditorAsRemoteOrderPrinterForProduct));
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to build the KPJobDetails for the transaction with an ID " +
                    "of in KitchenPrinterJobInfo.createPrintJobs", logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Gets any dining option products in the items from the transaction.
     *
     * @param txnItems {@link ArrayList} Items in the transaction
     * @param productToPrinterMappings {@link ArrayList<HashMap>} Mappings between products and the printers they should print out on.
     * @return {@link ArrayList<Plu>} Dining option products in the items from the transaction.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "unchecked", "Convert2streamapi"})
    private ArrayList<Plu> getDiningOptions (ArrayList txnItems, ArrayList<HashMap> productToPrinterMappings) {
        ArrayList<Plu> diningOptions = new ArrayList<>();

        try {
            if ((!DataFunctions.isEmptyCollection(txnItems)) && (!DataFunctions.isEmptyCollection(productToPrinterMappings))) {
                // iterate through the items in the transaction
                for (Object txnItem : txnItems) {
                    if ((txnItem != null) && (txnItem instanceof Plu)) {
                        Plu product = ((Plu) txnItem);
                        // set the prints on expeditor, is dining option, and mapped printers properties of the product
                        setAdditionalPropertiesForProduct(product, productToPrinterMappings);
                        if (product.isDiningOption()) {
                            diningOptions.add(product);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to get the dining options in " +
                    "KitchenPrinterJobInfo.getDiningOptions", logFileName, Logger.LEVEL.ERROR);
        }

        return diningOptions;
    }

    /**
     * Gets any non dining option products in the items from the transaction.
     *
     * @param txnItems {@link ArrayList} Items in the transaction
     * @param productToPrinterMappings {@link ArrayList<HashMap>} Mappings between products and the printers they should print out on.
     * @return {@link ArrayList<Plu>} Dining option products in the items from the transaction.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "unchecked", "Convert2streamapi"})
    private ArrayList<Plu> getNonDiningOptions (ArrayList txnItems, ArrayList<HashMap> productToPrinterMappings) {
        ArrayList<Plu> nonNonDiningOptions = new ArrayList<>();

        try {
            if ((!DataFunctions.isEmptyCollection(txnItems)) && (!DataFunctions.isEmptyCollection(productToPrinterMappings))) {
                // iterate through the items in the transaction
                for (Object txnItem : txnItems) {
                    if ((txnItem != null) && (txnItem instanceof Plu)) {
                        Plu product = ((Plu) txnItem);
                        // set the prints on expeditor, is non dining option, and mapped printers properties of the product
                        setAdditionalPropertiesForProduct(product, productToPrinterMappings);
                        if (!product.isDiningOption()) {
                            nonNonDiningOptions.add(product);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to get the non dining options in " +
                    "KitchenPrinterJobInfo.getNonDiningOptions", logFileName, Logger.LEVEL.ERROR);
        }

        return nonNonDiningOptions;
    }

    /**
     * Gets the given modifiers parent product within the transaction.
     *
     * @param modifier {@link Plu} The modifier to get the parent product for.
     * @param txnItems {@link ArrayList} Items in the transaction.
     * @return {@link Plu} The parent product.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private Plu getModifiersParentProduct (Plu modifier, ArrayList txnItems) {
        Plu parent = null;

        try {
            if ((!DataFunctions.isEmptyCollection(txnItems)) && (modifier != null)) {
                for (Object txnItem : txnItems) {
                    if ((txnItem != null) && (txnItem instanceof Plu)) {
                        Plu potentialParentProduct = ((Plu) txnItem);
                        if ((modifier.getPluID() != potentialParentProduct.getPluID())
                                && (modifier.getPATransLineItemID() == potentialParentProduct.getPATransLineItemID())) {
                            parent = potentialParentProduct;
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to find the parent product for the modifier " +
                    Objects.toString(modifier.getName(), "NULL")+" in KitchenPrinterJobInfo.getModifiersParentProduct",
                    logFileName, Logger.LEVEL.ERROR);
        }

        return parent;
    }

    /**
     * Utility method to create a print job with the given product and printer.
     *
     * @param product {@link Plu} Product to create a print job for.
     * @param printer {@link PrinterData} Printer the print job will be executed on.
     */
    private void createPrintJob (Plu product, PrinterData printer) {

        try {
            kpJobDetails.add(new KPJobDetailCreator.KPJobDetailBuilder()
                    .paTransLineItemID(product.getPATransLineItemID())
                    .paTransLineItemModID(product.getPaTransLineItemModID())
                    .papluID(product.getPluID())
                    .productName(product.getNameWithPrepOption())
                    .quantity(product.getQuantity())
                    .printerID(printer.getPrinterID())
                    .printerHostID(printer.getPrinterHostID())
                    .printerTypeID(printer.getPrinterTypeID())
                    .printerName(printer.getName())
                    .isExpeditor(printer.getIsExpeditorPrinter())
                    .isModifier(product.getIsModifier())
                    .noExpeditorTktsUnlessProdAdded(printer.getNoExpeditorTktsUnlessProdAdded())
                    .printerHardwareTypeID(printer.getPrinterHardwareTypeID())
                    .printerPrintsOnlyNew(printer.getPrintOnlyNewItems())
                    .hideStation(product.getHiddenKDSStationID())
                    .build().convertCreatorToKPJobDetail());
        }
        catch (Exception e) {
            Logger.logMessage(String.format("There was a problem trying to create the print job for the product %s " +
                    "to print on the printer %s in KitchenPrinterJobInfo.createPrintJob",
                    Objects.toString(product.getName(), "NULL"),
                    Objects.toString(printer.getName(), "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Sets the printsOnExpeditor and mappedPrinters properties of the given product.
     *
     * @param product {@link Plu} The product to set the additional properties for.
     * @param productToPrinterMappings {@link ArrayList<HashMap>} Printers mapped to the products in the transaction.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    private void setAdditionalPropertiesForProduct (Plu product, ArrayList<HashMap> productToPrinterMappings) {

        try {
            if ((product != null) && (productToPrinterMappings != null) && (!productToPrinterMappings.isEmpty())) {
                int productID = product.getPluID();
                // find the record for this product from the mappings
                HashMap matchedHM = new HashMap();
                for (HashMap productToPrinterMapping : productToPrinterMappings) {
                    if ((productToPrinterMapping != null) && (!productToPrinterMapping.isEmpty()) && (productToPrinterMapping.containsKey("PAPLUID"))
                            && (NumberUtils.isNumber(productToPrinterMapping.get("PAPLUID").toString())) && (Integer.parseInt(productToPrinterMapping.get("PAPLUID").toString()) == productID)) {
                        matchedHM = productToPrinterMapping;
                        break;
                    }
                }
                if (!matchedHM.isEmpty()) {
                    if (matchedHM.containsKey("PRINTSONEXPEDITOR")) {
                        product.setPrintsOnExpeditor(HashMapDataFns.getBooleanVal(matchedHM, "PRINTSONEXPEDITOR"));
                    }
                    if (matchedHM.containsKey("ISDININGOPTION")) {
                        product.setIsDiningOption(HashMapDataFns.getBooleanVal(matchedHM, "ISDININGOPTION"));
                    }
                    // get the printers specifically mapped to this product
                    if ((matchedHM.containsKey("MAPPEDPRINTERS")) && (matchedHM.get("MAPPEDPRINTERS") instanceof String)) {
                        String mappedPrintersStr = matchedHM.get("MAPPEDPRINTERS").toString();
                        if ((mappedPrintersStr != null) && (!mappedPrintersStr.isEmpty()) && (mappedPrintersStr.contains(","))) {
                            String[] mappedPrintersStrArr = mappedPrintersStr.split("\\s*,\\s*");
                            if (mappedPrintersStrArr.length > 0) {
                                // convert to HashSet to remove any duplicates
                                HashSet<String> mappedPrintersSet = new HashSet<>(Arrays.asList(mappedPrintersStrArr));
                                if (!mappedPrintersSet.isEmpty()) {
                                    int[] mappedPrinters = new int[mappedPrintersSet.size()];
                                    int index = 0;
                                    for (String mappedPrinter : mappedPrintersSet) {
                                        if (NumberUtils.isNumber(mappedPrinter)) {
                                            mappedPrinters[index] = Integer.parseInt(mappedPrinter);
                                        }
                                        index++;
                                    }
                                    if (mappedPrinters.length > 0) {
                                        product.setMappedPrinters(mappedPrinters);
                                    }
                                }
                            }
                        }
                        else if ((mappedPrintersStr != null) && (!mappedPrintersStr.isEmpty()) && (!mappedPrintersStr.contains(",")) && (NumberUtils.isNumber(mappedPrintersStr))) {
                            product.setMappedPrinters(new int[]{Integer.parseInt(mappedPrintersStr)});
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to set the printsOnExpeditor and mappedPrinters properties " +
                    "for the product "+Objects.toString(product.getName(), "NULL")+" in " +
                    "KitchenPrinterJobInfo.setAdditionalPropertiesForProduct", logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Queries the database and sets information to build a print job header.
     *
     * @param paTransactionID ID of the transaction to get information for.
     * @param terminalID ID of the terminal the transaction took place on.
     * @param dataManager {@link DataManager} DataManager to be used by the KPJobInfo.
     * @return {@link KPJobHeader} The new KPJobHeader.
     */
    @SuppressWarnings({"unchecked", "OverlyComplexMethod"})
    private KPJobHeader buildKPJobHeader (int paTransactionID, int terminalID, DataManager dataManager) {
        KPJobHeader kpJobHeader = new KPJobHeader(paTransactionID, terminalID, dataManager);

        try {
            if ((dataManager != null) && (paTransactionID > 0)) {
                // query the database to get the information
                ArrayList<HashMap> queryRes = dataManager.parameterizedExecuteQuery(
                        "data.ReceiptGen.GetTrxDetailOptimized", new Object[]{paTransactionID}, true);
                if ((queryRes != null) && (!queryRes.isEmpty()) && (queryRes.get(0) != null) && (!queryRes.get(0).isEmpty())) {
                    HashMap kpHeaderInfo = queryRes.get(0);
                    String transactionDate = HashMapDataFns.getStringVal(kpHeaderInfo, "TRANSACTIONDATE");
                    String estimatedOrderTime = HashMapDataFns.getStringVal(kpHeaderInfo, "ESTIMATEDORDERTIME");
                    String personName = HashMapDataFns.getStringVal(kpHeaderInfo, "PERSONNAME");
                    String phone = HashMapDataFns.getStringVal(kpHeaderInfo, "PHONE");
                    String transName = HashMapDataFns.getStringVal(kpHeaderInfo, "TRANSNAME");
                    String transComment = HashMapDataFns.getStringVal(kpHeaderInfo, "TRANSCOMMENT");
                    String orderNum = HashMapDataFns.getStringVal(kpHeaderInfo, "ORDERNUM");
                    int transTypeID = HashMapDataFns.getIntVal(kpHeaderInfo, "TRANSTYPEID");
                    String paTransNameLabel = HashMapDataFns.getStringVal(kpHeaderInfo, "PATRANSNAMELABEL");
                    boolean remoteOrderTerminal = HashMapDataFns.getBooleanVal(kpHeaderInfo, "REMOTEORDERTERMINAL");
                    boolean onlineOrder = HashMapDataFns.getBooleanVal(kpHeaderInfo, "ONLINEORDER");
                    String queueTime = HashMapDataFns.getStringVal(kpHeaderInfo, "QUEUETIME");
                    String printerQueueEstimatedTime = HashMapDataFns.getStringVal(kpHeaderInfo, "PRINTERQUEUEESTIMATEDTIME");
                    int paOrderTypeID = HashMapDataFns.getIntVal(kpHeaderInfo, "PAORDERTYPEID");
                    String printerQueuePhone = HashMapDataFns.getStringVal(kpHeaderInfo, "PRINTERQUEUEPHONE");
                    String printerQueueComment = HashMapDataFns.getStringVal(kpHeaderInfo, "PRINTERQUEUECOMMENT");
                    String pickUpDeliveryNote = HashMapDataFns.getStringVal(kpHeaderInfo, "PICKUPDELIVERYNOTE");
                    String printerQueueOrderNum = HashMapDataFns.getStringVal(kpHeaderInfo, "PRINTERQUEUEORDERNUM");
                    String address = HashMapDataFns.getStringVal(kpHeaderInfo, "ADDRESS");
                    // resolve some variables
                    if (paOrderTypeID == -1) {
                        // default order type
                        paOrderTypeID = TypeData.OrderType.NORMAL_SALE;
                    }
                    phone = KPJobHeader.sanitizePhoneNumber((!printerQueuePhone.isEmpty() ? printerQueuePhone : phone), true);
                    estimatedOrderTime = (!printerQueueEstimatedTime.isEmpty() ? printerQueueEstimatedTime : estimatedOrderTime);
                    transComment = (!printerQueueComment.isEmpty() ? printerQueueComment : transComment);
                    transComment = transComment.replaceAll("null;", "");
                    orderNum = (!printerQueueOrderNum.isEmpty() ? printerQueueOrderNum : orderNum);
                    boolean remoteOrder = (onlineOrder || remoteOrderTerminal);

                    // build the KPJobHeader
                    kpJobHeader = new KPJobHeaderCreator.KPJobHeaderBuilder(dataManager, paTransactionID, terminalID)
                            .isOnlineOrder(onlineOrder)
                            .isRemoteOrder(remoteOrder)
                            .transactionDate(transactionDate)
                            .orderNumber(orderNum)
                            .transTypeID(transTypeID)
                            .queueDateTime(queueTime)
                            .orderTypeID(paOrderTypeID)
                            .estimatedOrderTime(estimatedOrderTime)
                            .address(address)
                            .personName(personName)
                            .phoneNumber(phone)
                            .transName(transName)
                            .transNameLabel(paTransNameLabel)
                            .transComment(transComment)
                            .pickupDeliveryNote(pickUpDeliveryNote)
                            .build().convertCreatorToKPJobHeader();
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to build the KPJobHeader for the transaction with an ID " +
                    "of "+Objects.toString(paTransactionID, "NULL")+" in KitchenPrinterJobInfo.buildKPJobHeader",
                    logFileName, Logger.LEVEL.ERROR);
        }

        return kpJobHeader;
    }

    /**
     * Adds any inactive KDS expeditor stations within the given revenue center to the given ArrayList of kdsExpeditorPrinters.
     *
     * @param revenueCenterID ID of the revenue center to search for inactive KDS expeditor stations.
     * @param kdsExpeditorPrinters {@link ArrayList<PrinterData>} The ArrayList of KDS expeditor stations to add any
     *         inactive KDS expeditor stations to.
     */
    @SuppressWarnings({"unchecked", "TypeMayBeWeakened"})
    private void addInactiveKDSExpeditorStations (int revenueCenterID, ArrayList<PrinterData> kdsExpeditorPrinters) {

        try {
            ArrayList<HashMap> queryRes = dm.parameterizedExecuteQuery("data.kitchenPrinter.GetInactiveKDSExpInRevCntr", new Object[]{revenueCenterID}, true);
            if ((queryRes != null) && (!queryRes.isEmpty())) {
                queryRes.forEach(inactiveKDSExpeditorHM -> {
                    if ((inactiveKDSExpeditorHM != null) && (!inactiveKDSExpeditorHM.isEmpty())) {
                        PrinterData inactiveKDSExpeditor = new PrinterData(inactiveKDSExpeditorHM);
                        kdsExpeditorPrinters.add(inactiveKDSExpeditor);
                    }
                });
            }
        }
        catch (Exception e) {
            Logger.logMessage(String.format("There was a problem trying to find inactive KDS expeditor stations within " +
                    "the revenue centers with an ID of %s in KitchenPrinterJobInfo.addInactiveKDSExpeditorStations",
                    Objects.toString(revenueCenterID, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Sets the prevOrderNumsForKDS of the KPJobHeader.
     *
     * @param prevOrderNumsForKDS {@link String} Previous order numbers of an open transaction to be displayed on KDS.
     */
    public void setPrevOrderNumsForKDS (String prevOrderNumsForKDS) {

        try {
            kpJobHeader.setPrevOrderNumsForKDS(prevOrderNumsForKDS);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to set the previous order numbers to show on KDS in " +
                    "KitchenPrinterJobInfo.setPrevOrderNumsForKDS", logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Add the transaction line item ID for details that are KDS and not expeditor.
     *
     * @param combinedDetails {@link ArrayList} The details we want to check.
     * @return {@link HashSet<Integer>} Unique transaction line item IDs that are KDS and not expeditor.
     */
    @SuppressWarnings("unchecked")
    private HashSet<Integer> getTransLinesOnKDSPrep (ArrayList combinedDetails) {
        HashSet<Integer> transLinesOnKDSPrep = new HashSet<>();

        try {
            if ((combinedDetails != null) && (!combinedDetails.isEmpty())) {
                combinedDetails.forEach((detailObj) -> {
                    if (detailObj != null) {
                        HashMap detail = (HashMap) detailObj;
                        if (!detail.isEmpty()) {
                            int paTransLineItemID = HashMapDataFns.getIntVal(detail, "PATRANSLINEITEMID");
                            int kdsStation = HashMapDataFns.getIntVal(detail, "STATIONID");
                            boolean isExpeditor = HashMapDataFns.getBooleanVal(detail, "ISEXPEDITER");
                            boolean isRemote = HashMapDataFns.getBooleanVal(detail, "ISREMOTE");
                            boolean isKDS = (kdsStation > 0);

                            if ((isKDS) && (!isExpeditor) && (!isRemote)) {
                                transLinesOnKDSPrep.add(paTransLineItemID);
                            }
                        }
                    }
                });
                // log the transaction line items that are KDS and not expeditor if there are any
                if (!transLinesOnKDSPrep.isEmpty()) {
                    logCollection(transLinesOnKDSPrep, "KDS Prep Filter");
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to obtain the transaction line item IDs that are KDS and " +
                    "not expeditor in KitchenPrinterJobInfo.getTransLinesOnKDSPrep", logFileName, Logger.LEVEL.ERROR);
        }

        return transLinesOnKDSPrep;
    }

    /**
     * Check for and obtain the station ID of a KDS station configured as an expeditor.
     *
     * @param combinedDetails {@link ArrayList} The details we want to check.
     * @return The station ID of the KDS station that is an expeditor.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    private int getExpeditorStationID (ArrayList combinedDetails) {
        int expeditorStationID = 0;

        try {
            if ((combinedDetails != null) && (!combinedDetails.isEmpty())) {
                for (Object detailObj : combinedDetails) {
                    if (detailObj != null) {
                        HashMap detail = (HashMap) detailObj;
                        if (!detail.isEmpty()) {
                            int stationID = HashMapDataFns.getIntVal(detail, "STATIONID");
                            boolean isExpeditor = HashMapDataFns.getBooleanVal(detail, "ISEXPEDITER");
                            boolean isKDS = (stationID > 0);
                            Logger.logMessage("isExpeditor: "+Objects.toString((isExpeditor ? "true" : "false"), "NULL") +
                                    ", stationID: "+Objects.toString(stationID, "NULL")+" in " +
                                    "KitchenPrinterJobInfo.getExpeditorStationID", logFileName, Logger.LEVEL.DEBUG);
                            if ((isKDS) && (isExpeditor)) {
                                expeditorStationID = stationID;
                                break; // no need to keep processing
                            }
                        }
                    }
                }
                // log the station ID of the KDS station that is an expeditor if there is one
                if (expeditorStationID > 0) {
                    Logger.logMessage("expeditorStationID: "+Objects.toString(expeditorStationID, "NULL")+" in " +
                            "KitchenPrinterJobInfo.getExpeditorStationID", logFileName, Logger.LEVEL.DEBUG);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to determine the station ID of the KDS station configured " +
                    "as an expeditor in KitchenPrinterJobInfo.getExpeditorStationID", logFileName, Logger.LEVEL.ERROR);
        }

        return expeditorStationID;
    }

//    /**
//     * Generate kitchen printer print job details that will need to be printed.
//     *
//     * @param combinedDetails {@link ArrayList} Details we need create print job details for.
//     * @param modsWithPrinters {@link ArrayList} Modifiers used with printers that aren't expeditors.
//     * @param modsWithExpPrinters {@link ArrayList} Modifiers used with expeditor printers.
//     * @param productCountPerPrinter {@link HashMap<Integer, Integer>} Count of how many non-dining options are mapped
//     *         to each printer.
//     * @param transLinesOnKDSPrep {@link HashSet<Integer>} Transaction line item IDs on KDS prep printers.
//     * @param expeditorStationID ID of a KDS station configured as an expeditor.
//     */
//    @SuppressWarnings({"TypeMayBeWeakened", "unchecked", "OverlyComplexMethod"})
//    private void generateKPJobDetails (ArrayList combinedDetails, ArrayList modsWithPrinters, ArrayList modsWithExpPrinters,
//                                       HashMap<Integer, Integer> productCountPerPrinter, HashSet<Integer> transLinesOnKDSPrep, int expeditorStationID) {
//
//        try {
//            if ((combinedDetails != null) && (!combinedDetails.isEmpty())) {
//                combinedDetails.forEach((detailObj) -> {
//                    if (detailObj != null) {
//                        HashMap detail = (HashMap) detailObj;
//                        if (!detail.isEmpty()) {
//                            int paTransLineItemID = HashMapDataFns.getIntVal(detail, "PATRANSLINEITEMID");
//                            int paTransLineItemModID = HashMapDataFns.getIntVal(detail, "PATRANSLINEITEMMODID");
//                            int paPluID = HashMapDataFns.getIntVal(detail, "PAPLUID");
//                            double quantity = HashMapDataFns.getDoubleVal(detail, "QUANTITY");
//                            int printerID = HashMapDataFns.getIntVal(detail, "PRINTERID");
//                            String printerName = HashMapDataFns.getStringVal(detail, "PRINTERNAME");
//                            int printerHostID = HashMapDataFns.getIntVal(detail, "PRINTCONTROLLERID");
//                            String productName = HashMapDataFns.getStringVal(detail, "PRODUCTNAME");
//                            boolean isExpeditor = HashMapDataFns.getBooleanVal(detail, "ISEXPEDITER");
//                            boolean isRemote = HashMapDataFns.getBooleanVal(detail, "ISREMOTE");
//                            boolean printsOnExpeditor = HashMapDataFns.getBooleanVal(detail, "PRINTSONEXPEDITOR");
//                            boolean isModifier = HashMapDataFns.getBooleanVal(detail, "ISMODIFIER");
//                            boolean isDiningOption = HashMapDataFns.getBooleanVal(detail, "ISDININGOPTION");
//                            int printerHardwareTypeID = HashMapDataFns.getIntVal(detail, "PRINTERHARDWARETYPEID");
//                            boolean isPrintOnlyNew = HashMapDataFns.getBooleanVal(detail, "PRINTONLYNEWITEMS");
//                            PrintJobStatusType printJobStatusType = PrintJobStatusType.getFromID(HashMapDataFns.getIntVal(
//                                    detail, "PRINTSTATUSID"));
//                            int hideStation = ((!printsOnExpeditor) && (!isExpeditor) && (!isModifier)
//                                    ? expeditorStationID
//                                    : 0);
//                            Logger.logMessage("printsOnExpeditor: " +
//                                    Objects.toString((printsOnExpeditor ? "true" : "false"), "NULL")+", isExpeditor: " +
//                                    Objects.toString((isExpeditor ? "true" : "false"), "NULL")+", isRemote: "+
//                                    Objects.toString((isRemote ? "true" : "false"), "NULL")+", isModifier: " +
//                                    Objects.toString((isModifier ? "true" : "false"), "NULL")+", hideStation: "+
//                                    Objects.toString(hideStation, "NULL")+" in " +
//                                    "KitchenPrinterJobInfo.generateKPJobDetails", logFileName, Logger.LEVEL.DEBUG);
//
//                            // don't add the detail to if it's not assigned to a printer or KDS expeditors
//                            // that are on a KDS prep
//                            if ((printerID != -1)
//                                    && (!((isExpeditor) && (transLinesOnKDSPrep.contains(paTransLineItemID))))) {
//                                KPJobDetail kpJobDetail = new KPJobDetail();
//                                kpJobDetail.setPaTransLineItemID(paTransLineItemID);
//                                kpJobDetail.setPaTransLineItemModID(paTransLineItemModID);
//                                kpJobDetail.setPAPluID(paPluID);
//                                kpJobDetail.setProductName(productName);
//                                kpJobDetail.setQuantity(quantity);
//                                kpJobDetail.setPrinterID(printerID);
//                                kpJobDetail.setPrinterHostID(printerHostID);
//                                kpJobDetail.setPrinterName(printerName);
//                                kpJobDetail.setIsExpeditor(isExpeditor);
//                                kpJobDetail.setIsRemote(isRemote);
//                                kpJobDetail.setIsModifier(isModifier);
//                                kpJobDetail.setPrinterHardwareTypeID(printerHardwareTypeID);
//                                kpJobDetail.setPrinterPrintsOnlyNew(isPrintOnlyNew);
//                                kpJobDetail.setHideStation(hideStation);
//                                if (printJobStatusType != null) {
//                                    kpJobDetail.setPrintStatusID(printJobStatusType.getTypeID());
//                                }
//
//                                // update non-dining option products to be sent to this printer
//                                updateProductCountPerPrinter(productCountPerPrinter, printerID, isDiningOption);
//
//                                // add the detail
//                                kpJobDetails.add(kpJobDetail);
//
//                                if (isModifier) {
//                                    if (isExpeditor) {
//                                        modsWithExpPrinters.add(paTransLineItemModID);
//                                    }
//                                    else {
//                                        modsWithPrinters.add(paTransLineItemModID);
//                                    }
//                                }
//                            }
//                            else {
//                                Logger.logMessage("Skipping the detail with a paTransLineItemID of " +
//                                        Objects.toString(paTransLineItemID, "NULL")+" due to an invalid printer for " +
//                                        "the detail or is a KDS expeditor on a KDS prep in " +
//                                        "KitchenPrinterJobInfo.generateKPJobDetails", logFileName, Logger.LEVEL.DEBUG);
//                            }
//                        }
//                    }
//                });
//            }
//
//            // remove details from printers with no non-dining product options
//            if ((productCountPerPrinter != null) && (!productCountPerPrinter.isEmpty())) {
//                cleanJobDetails(productCountPerPrinter);
//            }
//        }
//        catch (Exception e) {
//            Logger.logException(e, logFileName);
//            Logger.logMessage("There was a problem trying to generate the print job details in " +
//                    "KitchenPrinterJobInfo.generateKPJobDetails", logFileName, Logger.LEVEL.ERROR);
//        }
//
//    }

    /**
     * Keeps track of how many non-dining options there are for each printer.
     *
     * @param productCountPerPrinter {@link HashMap<Integer, Integer>} Count of how many non-dining options are mapped
     *         to each printer.
     * @param printerID ID of the printer we want to update the count of non-dining options for.
     * @param isDiningOption Whether or not the product is a dining option.
     */
    private void updateProductCountPerPrinter (HashMap<Integer, Integer> productCountPerPrinter, int printerID,
                                               boolean isDiningOption) {

        try {
            if (!isDiningOption) {
                if (productCountPerPrinter == null) {
                    productCountPerPrinter = new HashMap<>();
                }
                if (productCountPerPrinter.isEmpty()) {
                    // add the count for this printer to the productCountPerPrinter
                    productCountPerPrinter.put(printerID, 1);
                }
                else {
                    if (productCountPerPrinter.containsKey(printerID)) {
                        // increment the current product count for the printer
                        int productCount = productCountPerPrinter.get(printerID);
                        productCountPerPrinter.put(printerID, productCount + 1);
                    }
                    else {
                        // add the count for this printer to the productCountPerPrinter
                        productCountPerPrinter.put(printerID, 1);
                    }
                }
            }
            else {
                if (productCountPerPrinter == null) {
                    productCountPerPrinter = new HashMap<>();
                }
                if (productCountPerPrinter.isEmpty()) {
                    // add the count for this printer to the productCountPerPrinter
                    productCountPerPrinter.put(printerID, 0);
                }
                else {
                    // don't need to increment the count for the printer if it's in the productCountPerPrinter HashMap
                    if (!productCountPerPrinter.containsKey(printerID)) {
                        // add the count for this printer to the productCountPerPrinter
                        productCountPerPrinter.put(printerID, 0);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to update the product count of non-dining options for the " +
                    "printer with an ID of "+Objects.toString(printerID, "NULL")+" in " +
                    "KitchenPrinterJobInfo.updateProductCountPerPrinter", logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Removes details if there are no non-dining product options for the printer.
     *
     * @param productCountPerPrinter {@link HashMap<Integer, Integer>} Count of how many non-dining options are mapped
     *         to each printer.
     */
    private void cleanJobDetails (Map<Integer, Integer> productCountPerPrinter) {

        try {
            if ((productCountPerPrinter != null) && (!productCountPerPrinter.isEmpty())) {
                productCountPerPrinter.forEach((printerID, productCount) -> {
                    if (productCount == 0) {
                        removeDetailsForPrinterID(printerID);
                    }
                });
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to remove job details for printers without any non-dining " +
                    "product options in KitchenPrinterJobInfo.cleanJobDetails", logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Remove KPJobDetails for the given printer.
     *
     * @param printerID ID of the printer we want to remove KPJobDetails from.
     */
    private void removeDetailsForPrinterID (int printerID) {

        try {
            ArrayList<KPJobDetail> validDetails = new ArrayList<>();

            if (!kpJobDetails.isEmpty()) {
                kpJobDetails.forEach((kpJobDetail) -> {
                    if (kpJobDetail.getPrinterID() != printerID) {
                        validDetails.add(kpJobDetail);
                    }
                });
            }

            kpJobDetails = validDetails;
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to remove job details from the printer with an ID of " +
                            Objects.toString(printerID, "NULL")+" in KitchenPrinterJobInfo.removeDetailsForPrinterID",
                    logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Iterates through the transaction data and create print job details for modifiers not mapped to printers.
     *
     * @param transactionData {@link TransactionData} The transaction data we wish to scan for modifiers that aren't
     *         mapped to printers.
     * @param modsWithExpPrinters {@link ArrayList} Modifiers to be printed on expeditor printers.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "unchecked", "OverlyComplexMethod"})
    private void createJobDetailsForUnmappedMods (TransactionData transactionData, ArrayList modsWithExpPrinters) {

        try {
            if ((transactionData != null) && (transactionData.getTransItems() != null)
                    && (!transactionData.getTransItems().isEmpty())) {
                transactionData.getTransItems().forEach((transItemObj) -> {
                    if ((transItemObj != null) && (transItemObj instanceof Plu)) {
                        Plu transItem = (Plu) transItemObj;
                        int paTransLineItemID = transItem.getPATransLineItemID();
                        int paTransLineItemModID = transItem.getPaTransLineItemModID();
                        boolean isModifier = transItem.getIsModifier();

                        if ((isModifier) && (modsWithExpPrinters != null) && (!modsWithExpPrinters.isEmpty())
                                && (!modsWithExpPrinters.contains(paTransLineItemModID))) {
                            KPJobDetail modDetail = null;
                            // if the modifier isn't mapped to a printer then it will follow it's parent's printer settings
                            if (!kpJobDetails.isEmpty()) {
                                for (int i = 0; i < kpJobDetails.size(); i++) {
                                    KPJobDetail kpJobDetail = kpJobDetails.get(i);
                                    // find the parent and create the job detail
                                    if ((!kpJobDetail.getIsModifier())
                                            && (kpJobDetail.getPaTransLineItemID() == paTransLineItemID)) {
                                        // create the job detail if it's for an expeditor we don't have a print job yet
                                        if ((!kpJobDetail.getIsExpeditor()) || ((kpJobDetail.getIsExpeditor())
                                                && (!modsWithExpPrinters.contains(paTransLineItemModID)))) {
                                            // create job detail for the modifier using some of the parents settings
                                            modDetail = new KPJobDetail();
                                            modDetail.setPaTransLineItemID(paTransLineItemID);
                                            modDetail.setPaTransLineItemModID(paTransLineItemModID);
                                            modDetail.setPAPluID(transItem.getPluID());
                                            modDetail.setProductName(transItem.getName());
                                            modDetail.setQuantity(kpJobDetail.getQuantity());
                                            modDetail.setPrinterID(kpJobDetail.getPrinterID());
                                            modDetail.setPrinterHostID(kpJobDetail.getPrinterHostID());
                                            modDetail.setPrinterName(kpJobDetail.getPrinterName());
                                            modDetail.setIsExpeditor(kpJobDetail.getIsExpeditor());
                                            modDetail.setIsRemote(kpJobDetail.getIsRemote());
                                            modDetail.setIsModifier(true);
                                            modDetail.setPrinterPrintsOnlyNew(kpJobDetail.isPrinterPrintsOnlyNew());
                                        }
                                    }

                                    // add the newly created detail to the kpJobDetails ArrayList
                                    if (modDetail != null) {
                                        KPJobDetail nextKPJobDetail = null;

                                        // if the modifier has no printer assignments but has printsOnExpeditor set to
                                        // true it will get picked up by the
                                        // data.kitchenPrinter.GetExpPrintersForTransLineItems query so we need to avoid
                                        // adding it twice for expeditor printers
                                        if (i + 1 < kpJobDetails.size()) {
                                            nextKPJobDetail = kpJobDetails.get(i + 1);
                                        }

                                        if ((!kpJobDetail.getIsModifier())
                                                && ((nextKPJobDetail == null) || (!nextKPJobDetail.getIsModifier()))
                                                || (((nextKPJobDetail != null) && (nextKPJobDetail.getIsModifier()))
                                                && (nextKPJobDetail.getPaTransLineItemID() != modDetail.getPaTransLineItemID()))) {
                                            kpJobDetails.add(i + 1, modDetail);
                                            modDetail = new KPJobDetail();
                                            modDetail = null;
                                        }
                                        else if (kpJobDetail.getIsModifier()) {
                                            if ((paTransLineItemModID > kpJobDetail.getPaTransLineItemModID())
                                                    && ((nextKPJobDetail == null) || (!nextKPJobDetail.getIsModifier()))) {
                                                kpJobDetails.add(i + 1, modDetail);
                                                modDetail = new KPJobDetail();
                                                modDetail = null;
                                            }
                                            else if (((nextKPJobDetail != null) && (nextKPJobDetail.getIsModifier()))
                                                    && (paTransLineItemModID > kpJobDetail.getPaTransLineItemModID())
                                                    && (paTransLineItemModID < nextKPJobDetail.getPaTransLineItemModID())) {
                                                kpJobDetails.add(i + 1, modDetail);
                                                modDetail = new KPJobDetail();
                                                modDetail = null;
                                            }
                                            else if (paTransLineItemModID < kpJobDetail.getPaTransLineItemModID()) {
                                                kpJobDetails.add(i, modDetail);
                                                modDetail = new KPJobDetail();
                                                modDetail = null;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to create job details for modifiers that aren't mapped to " +
                            "a printer in KitchenPrinterJobInfo.createJobDetailsForUnmappedMods", logFileName,
                    Logger.LEVEL.ERROR);
        }

    }

    public void createPlainTextDetails (ReceiptData receiptData) {

        try {
            if ((kpJobDetails != null) && (!kpJobDetails.isEmpty())) {
                Logger.logMessage("********** START PLAIN TEXT DETAILS **********", logFileName, Logger.LEVEL.DEBUG);

                boolean validPrintersFound = false;
                for (KPJobDetail kpJobDetail : kpJobDetails) {
                    if ((kpJobDetail != null) && (kpJobDetail.getPrinterID() != -1)) {
                        validPrintersFound = true;
                        break;
                    }
                }

                HashMap printerToStationMappings = getPrinterToStationMappings();
                String previousOrderNumsSentToKDS = getPreviousOrderNumsSentToKDS(Integer.parseInt(receiptData.getTxnData().getTransactionID()), receiptData.getOrderNumber());

                if (validPrintersFound) {
                    HashMap<String,String> printQueueHeader = new HashMap<>();
                    printQueueHeader.put("PATransactionID", Integer.toString(kpJobHeader.getPATransactionID()));
                    printQueueHeader.put("TransactionDate", kpJobHeader.getTransactionDate());
                    printQueueHeader.put("TerminalID", Integer.toString(kpJobHeader.getTerminalID()));
                    printQueueHeader.put("OrderTypeID", Integer.toString(kpJobHeader.getOrderTypeID()));
                    printQueueHeader.put("PersonName", kpJobHeader.getPersonName());
                    printQueueHeader.put("PhoneNumber", kpJobHeader.getPhoneNumber());
                    // the trans comment is a combination of the delivery location and the actual transaction comment
                    printQueueHeader.put("TransComment", (StringFunctions.stringHasContent(kpJobHeader.getAddress()) ? kpJobHeader.getAddress()+" - "+kpJobHeader.getTransComment() : kpJobHeader.getTransComment()));
                    printQueueHeader.put("PickupDeliveryNote", kpJobHeader.getPickupDeliveryNote());
                    printQueueHeader.put("IsOnlineOrder", Integer.toString(kpJobHeader.getIsOnlineOrder() ? 1 : 0));
                    printQueueHeader.put("EstimatedOrderTime", kpJobHeader.getEstimatedOrderTime());
                    printQueueHeader.put("OrderNum", kpJobHeader.getOrderNumber());
                    printQueueHeader.put("TransName", kpJobHeader.getTransName());
                    printQueueHeader.put("TransNameLabel", kpJobHeader.getTransNameLabel());
                    printQueueHeader.put("TransTypeID", Integer.toString(paTransTypeID));
                    printQueueHeader.put("PrevOrderNumsForKDS", (StringFunctions.stringHasContent(previousOrderNumsSentToKDS) ? previousOrderNumsSentToKDS : ""));

                    addToDetails(printQueueHeader);

                    for (KPJobDetail detail : kpJobDetails) {
                        // filter out KDS details
                        if (Integer.parseInt(printerToStationMappings.get(detail.getPrinterID()).toString()) <= 0) {
                            // replace escaped apostrophes
                            String detailStr = KitchenPrinterCommon.decode(detail.getLineDetail(), logFileName);
                            detailStr = detailStr.replaceAll("&#x27;", "\'");
                            detail.setLineDetail(new String(Base64.getEncoder().encode(detailStr.getBytes())));
                            HashMap<String,String> printQueueDetail = new HashMap<>();
                            printQueueDetail.put("PrinterID", Integer.toString(detail.getPrinterID()));
                            printQueueDetail.put("PrintStatusID", Integer.toString(detail.getPrintStatusID()));
                            printQueueDetail.put("PrintControllerID", Integer.toString(detail.getPrinterHostID()));
                            printQueueDetail.put("PAPluID", Integer.toString(detail.getPAPluID()));
                            printQueueDetail.put("Quantity", Double.toString(detail.getQuantity()));
                            printQueueDetail.put("IsModifier", Boolean.toString(detail.getIsModifier()));
                            printQueueDetail.put("LineDetail", detail.getLineDetail());
                            printQueueDetail.put("HideStation", detail.getHideStation());
                            addToDetails(printQueueDetail);
                        }
                    }

                    // add in the KDS details
                    ArrayList<KPJobDetail> kdsJobDetails = KitchenDisplaySystemJobInfo.getKDSPrintJobs(receiptData);
                    if (!DataFunctions.isEmptyCollection(kdsJobDetails)) {
                        for (KPJobDetail kdsDetail : kdsJobDetails) {
                            HashMap<String,String> printQueueDetail = new HashMap<>();
                            printQueueDetail.put("PrinterID", Integer.toString(kdsDetail.getPrinterID()));
                            printQueueDetail.put("PrintStatusID", Integer.toString(kdsDetail.getPrintStatusID()));
                            printQueueDetail.put("PrintControllerID", Integer.toString(kdsDetail.getPrinterHostID()));
                            printQueueDetail.put("PAPluID", Integer.toString(kdsDetail.getPAPluID()));
                            printQueueDetail.put("Quantity", Double.toString(kdsDetail.getQuantity()));
                            printQueueDetail.put("IsModifier", Boolean.toString(kdsDetail.getIsModifier()));
                            printQueueDetail.put("LineDetail", kdsDetail.getLineDetail());
                            printQueueDetail.put("HideStation", kdsDetail.getHideStation());
                            addToDetails(printQueueDetail);
                        }
                    }
                }

                Logger.logMessage("********** END PLAIN TEXT DETAILS **********", logFileName, Logger.LEVEL.DEBUG);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to create the plain text details in " +
                    "KitchenPrinterJobInfo.createPlaintextDetails", logFileName, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Queries the database to get the station ID associated with each printer if applicable.
     *
     * @return {@link ArrayList<HashMap>} Station ID associated with each printer if applicable.
     */
    @SuppressWarnings({"unchecked", "Convert2streamapi"})
    private HashMap getPrinterToStationMappings () {
        HashMap printerToStationMappings = new HashMap();

        try {
            ArrayList<HashMap> queryRes = dm.parameterizedExecuteQuery("data.kitchenPrinter.GetPrintersAndStations", new Object[]{}, true);
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                for (Object hmObj : queryRes) {
                    if ((hmObj instanceof HashMap) && (!DataFunctions.isEmptyMap(((HashMap) hmObj)))) {
                        int printerID = HashMapDataFns.getIntVal(((HashMap) hmObj), "PRINTERID");
                        int stationID = HashMapDataFns.getIntVal(((HashMap) hmObj), "STATIONID");
                        printerToStationMappings.put(printerID, stationID);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to get the mappings between printers and station IDs in " +
                    "KitchenPrinterJobInfo.getPrinterToStationMappings", logFileName, Logger.LEVEL.ERROR);
        }

        return printerToStationMappings;
    }

    /**
     * Queries the database to get any order numbers within an open transaction.
     *
     * @param paTransactionID ID of the transaction.
     * @param currentOrderNumber {@link String} The current order number.
     * @return {@link String} Any order numbers within an open transaction.
     */
    @SuppressWarnings({"unchecked", "Convert2streamapi", "TypeMayBeWeakened"})
    private String getPreviousOrderNumsSentToKDS (int paTransactionID, String currentOrderNumber) {
        String previousOrderNumsSentToKDS = "";

        try {
            ArrayList<HashMap> queryRes = dm.parameterizedExecuteQuery("data.newKitchenPrinter.GetOrderNumsSentToKDSForTxnID", new Object[]{paTransactionID}, true);
            ArrayList<String> orderNumbers = new ArrayList<>();
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                for (HashMap hm : queryRes) {
                    if (!DataFunctions.isEmptyMap(hm)) {
                        orderNumbers.add(HashMapDataFns.getStringVal(hm, "ORDERNUM"));
                    }
                }
            }
            if (!DataFunctions.isEmptyCollection(orderNumbers)) {
                // remove the current order number
                if (orderNumbers.contains(currentOrderNumber)) {
                    Iterator<String> ordNumItr = orderNumbers.iterator();
                    while (ordNumItr.hasNext()) {
                        if (ordNumItr.next().equalsIgnoreCase(currentOrderNumber)) {
                            ordNumItr.remove();
                        }
                    }
                }
                // check that there's still previous order numbers
                if (!DataFunctions.isEmptyCollection(orderNumbers)) {
                    // sort the order numbers
                    Collections.sort(orderNumbers);
                    StringBuilder sb = new StringBuilder();
                    sb.append("Prev Order #: ");
                    for (String orderNum : orderNumbers) {
                        sb.append(orderNum);
                        sb.append(",");
                    }
                    // remove the trailing comma
                    sb.setLength(sb.length() - 1);
                    previousOrderNumsSentToKDS = sb.toString();
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to get the previous order numbers sent to KDS " +
                    "for the transaction with an ID of %s in KitchenPrinterJobInfo.getPreviousOrderNumsSentToKDS",
                    Objects.toString(paTransactionID, "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return previousOrderNumsSentToKDS;
    }

    /**
     * Adds the given map to the plain text details.
     *
     * @param map {@link HashMap<String, String>} The map to add to the plain text details.
     */
    @SuppressWarnings("unchecked")
    private void addToDetails (HashMap<String,String> map) {

        try {
            plainTxtDetails.add(map);

            StringBuilder sb = new StringBuilder();
            boolean isFirst = true;
            for (String sKey : map.keySet()) {
                if (isFirst) {
                    isFirst = false;
                }
                else {
                    sb.append(", ");
                }

                sb.append("\"");
                sb.append(sKey);
                sb.append("\"");
                sb.append(":");
                sb.append("\"");
                sb.append(map.get(sKey));
                sb.append("\"");
            }

            Logger.logMessage("Adding to plainTxtDetails: " + sb.toString(), logFileName, Logger.LEVEL.DEBUG);
        }
        catch (Exception e) {
            Logger.logMessage("There was a problem trying to add the map to the plain text details in " +
                    "KitchenPrinterJonInfo.addToDetails", logFileName, Logger.LEVEL.ERROR);
        }

    }

    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public KPJobHeader getKpJobHeader()
    {
        return kpJobHeader;
    }

    public ArrayList<KPJobDetail> getDetails()
    {
        return kpJobDetails;
    }

    public void setDetails (ArrayList<KPJobDetail> newDetails) {
        kpJobDetails = newDetails;
    }

    @SuppressWarnings("unchecked")
    public ArrayList getPlainTextDetails()
    {
        Logger.logMessage("********** START RETRIEVAL OF PLAIN TEXT DETAILS **********", logFileName, Logger.LEVEL.DEBUG);
        if ((plainTxtDetails != null) && (!plainTxtDetails.isEmpty())) {
            plainTxtDetails.forEach(detail -> Logger.logMessage(detail.toString(), logFileName, Logger.LEVEL.DEBUG));
        }
        Logger.logMessage("********** END RETRIEVAL OF PLAIN TEXT DETAILS **********", logFileName, Logger.LEVEL.DEBUG);

        return plainTxtDetails;
    }

    /**
     * Sorts the KPJobDetails with the unprinted KPJobDetails first followed by the remaining KPJobsDetails
     * by descending order number.
     *
     * @param txnData {@link TransactionData} Data pertaining to the transaction.
     * @param orderNumber {@link String} The order number.
     * @return {@link ArrayList<KPJobDetail>} The sorted KPJobDetails.
     */
    @SuppressWarnings({"OverlyComplexMethod"})
    public ArrayList<KPJobDetail> sortDetails (TransactionData txnData, String orderNumber) {
        ArrayList<KPJobDetail> unprintedDetails = new ArrayList<>();
        ArrayList<KPJobDetail> printedDetails = new ArrayList<>();
        HashMap<Integer, ArrayList<KPJobDetail>> kpJobDetailsSortedByPrinter;

        try {
            if ((kpJobDetails != null) && (!kpJobDetails.isEmpty()) && (txnData != null)) {
                // get the unprinted KPJobDetails
                kpJobDetails.forEach(kpJobDetail -> {
                    Plu product = txnData.getProduct(kpJobDetail.getPaTransLineItemID());
                    if ((product != null) && (!product.hasBeenPrinted())) {
                        unprintedDetails.add(kpJobDetail);
                    }
                });
                // sort the unprinted KPJobDetails by descending order number
                sortKPJobDetailsByOrderNum(txnData, orderNumber, unprintedDetails);

                // get the printed KPJobDetails
                kpJobDetails.forEach(kpJobDetail -> {
                    Plu product = txnData.getProduct(kpJobDetail.getPaTransLineItemID());
                    if ((product != null) && (product.hasBeenPrinted())) {
                        printedDetails.add(kpJobDetail);
                    }
                });
                // sort the printed KPJobDetails by descending order number
                sortKPJobDetailsByOrderNum(txnData, orderNumber, printedDetails);

                // combine the unprinted and printed KPJobDetails and sort them based on the printer they should be printed on
                unprintedDetails.addAll(printedDetails);
                kpJobDetailsSortedByPrinter = sortKPJobDetailsByPrinter(unprintedDetails);

                // sort the KPJobDetails by descending order number for each printer
                if ((kpJobDetailsSortedByPrinter != null) && (!kpJobDetailsSortedByPrinter.isEmpty())) {
                    unprintedDetails.clear();
                    kpJobDetailsSortedByPrinter.values().forEach(unprintedDetails::addAll);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to sort the KPJobDetails for the transaction with an ID of " +
                    Objects.toString(txnData.getTransactionID(), "NULL")+" in KitchenPrinterJobInfo.sortDetails",
                    logFileName, Logger.LEVEL.ERROR);
        }

        return unprintedDetails;
    }

    /**
     * Sorts the given KPJobDetails by descending order number with in the transaction.
     *
     * @param txnData {@link TransactionData} Data pertaining to the transaction.
     * @param orderNumber {@link String} The order number.
     * @param kpJobDetails {@link ArrayList<KPJobDetail>} The KPJobDetails to sort.
     * @return {@link ArrayList<KPJobDetail>} The given KPJobDetails sorted by descending order number within the transaction.
     */
    @SuppressWarnings({"OverlyComplexMethod", "ConstantConditions"})
    private ArrayList<KPJobDetail> sortKPJobDetailsByOrderNum (TransactionData txnData,
                                                               String orderNumber,
                                                               ArrayList<KPJobDetail> kpJobDetails) {

        try {
            if ((kpJobDetails != null) && (!kpJobDetails.isEmpty())) {
                if ((txnData == null) || (orderNumber == null) || (orderNumber.isEmpty())) {
                    return kpJobDetails;
                }

                boolean sortingDone = false;
                int index = 0;
                while (!sortingDone) {
                    if (index + 1 >= kpJobDetails.size()) {
                        sortingDone = true;
                        break;
                    }

                    // get the KPJobDetail for this iteration and the next iteration from the KPJobsDetails
                    KPJobDetail currKPJobDetail = kpJobDetails.get(index);
                    Plu currProduct = txnData.getProduct(currKPJobDetail.getPaTransLineItemID());
                    KPJobDetail nextKPJobDetail = kpJobDetails.get(index + 1);
                    Plu nextProduct = txnData.getProduct(nextKPJobDetail.getPaTransLineItemID());

                    if ((currProduct != null) && (nextProduct != null)) {
                        String currKPJobDetailOrderStr = (!currProduct.getOriginalOrderNum().isEmpty() ? currProduct.getOriginalOrderNum() : orderNumber);
                        String nextKPJobDetailOrderStr = (!nextProduct.getOriginalOrderNum().isEmpty() ? nextProduct.getOriginalOrderNum() : orderNumber);

                        currKPJobDetailOrderStr = (currKPJobDetailOrderStr.contains("-") ? currKPJobDetailOrderStr.replaceAll("-", "") : currKPJobDetailOrderStr);
                        nextKPJobDetailOrderStr = (nextKPJobDetailOrderStr.contains("-") ? nextKPJobDetailOrderStr.replaceAll("-", "") : nextKPJobDetailOrderStr);

                        int currKPJobDetailOrderNum;
                        int nextKPJobDetailOrderNum;
                        currKPJobDetailOrderNum = (NumberUtils.isNumber(currKPJobDetailOrderStr) ? Integer.parseInt(currKPJobDetailOrderStr) : 0);
                        nextKPJobDetailOrderNum = (NumberUtils.isNumber(nextKPJobDetailOrderStr) ? Integer.parseInt(nextKPJobDetailOrderStr) : 0);

                        // if the order number of the next KPJobDetail is larger than the current KPJobDetail, than it
                        // should be placed before the current KPJobDetail
                        if (nextKPJobDetailOrderNum > currKPJobDetailOrderNum) {
                            kpJobDetails.remove(index + 1);
                            kpJobDetails.add(index, nextKPJobDetail);
                            index = -1;
                        }
                    }

                    index++;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to sort the KPJobDetails by order number in " +
                    "KitchenPrinterJobInfo.sortKPJobDetailsByOrderNum", logFileName, Logger.LEVEL.ERROR);
        }

        return kpJobDetails;
    }

    /**
     * Sort KPJobDetails by the printer the should be printed on.
     *
     * @param kpJobDetails {@link ArrayList<KPJobDetail>} The KPJobDetails to sort.
     * @return {@link HashMap<Integer, ArrayList<KPJobDetail>>} KPJobDetails sorted by printer.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "JavaDoc"})
    private HashMap<Integer, ArrayList<KPJobDetail>> sortKPJobDetailsByPrinter (ArrayList<KPJobDetail> kpJobDetails) {
        HashMap<Integer, ArrayList<KPJobDetail>> sortedKPJobDetails = new HashMap<>();
        ArrayList<KPJobDetail> kpJobDetailsOnSamePrinter;

        try {
            if ((kpJobDetails != null) && (!kpJobDetails.isEmpty())) {
                for (KPJobDetail kpJobDetail : kpJobDetails) {
                    int printerID = kpJobDetail.getPrinterID();
                    if (sortedKPJobDetails.containsKey(printerID)) {
                        kpJobDetailsOnSamePrinter = sortedKPJobDetails.get(printerID);
                    }
                    else {
                        kpJobDetailsOnSamePrinter = new ArrayList<>();
                    }
                    kpJobDetailsOnSamePrinter.add(kpJobDetail);
                    sortedKPJobDetails.put(printerID, kpJobDetailsOnSamePrinter);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to sort the KPJobsDetails by printer in " +
                    "KitchenPrinterJobInfo.sortKPJobDetailsByPrinterID", logFileName, Logger.LEVEL.ERROR);
        }

        return sortedKPJobDetails;
    }

}
