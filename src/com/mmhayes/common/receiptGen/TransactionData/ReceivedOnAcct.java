package com.mmhayes.common.receiptGen.TransactionData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-21 12:58:42 -0400 (Mon, 21 Sep 2020) $: Date of last commit
    $Rev: 12712 $: Revision of last commit
    Notes: Represents a ReceivedOnAcct transaction line item.
*/

import com.mmhayes.common.datawire.DatawireConstants;
import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.GiftCardTxnData;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.ReceivedOnAccountLineItemModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by nyu on 10/16/2016.
 */
public class ReceivedOnAcct extends TransactionItem
{
    // Identifying attributes
    int itemTypeID = TypeData.ItemType.RA;
    int receivedOnAcctID;

    // For QC RAs
    private int badgeAssignmentID = 0;
    private String badgeNumber = "";
    private String employeeID = "";

    GiftCardTxnData gcTxnData;
    String giftCardTransInfo;
    int receivedOnAcctTypeID;

    /**
     * <p>Empty constructor for a {@link ReceivedOnAcct}.</p>
     *
     */
    public ReceivedOnAcct () {}

    /**
     * <p>Constructor for a {@link ReceivedOnAcct} with it's receivedOnAcctID field set.</p>
     *
     * @param receivedOnAcctID The ID of the of the received on account.
     * @return The {@link ReceivedOnAcct} instance with it's receivedOnAcctID field set.
     */
    public ReceivedOnAcct receivedOnAcctID (int receivedOnAcctID) {
        this.receivedOnAcctID = receivedOnAcctID;
        return this;
    }

    /**
     * <p>Constructor for a {@link ReceivedOnAcct} with it's name field set.</p>
     *
     * @param name The name {@link String} of the received on account.
     * @return The {@link ReceivedOnAcct} instance with it's name field set.
     */
    public ReceivedOnAcct name (String name) {
        this.name = name;
        return this;
    }

    /**
     * <p>Constructor for a {@link ReceivedOnAcct} with it's paTransLineItemID field set.</p>
     *
     * @param paTransLineItemID The transaction line item ID of the received on account line item.
     * @return The {@link ReceivedOnAcct} instance with it's paTransLineItemID field set.
     */
    public ReceivedOnAcct paTransLineItemID (int paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
        return this;
    }

    /**
     * <p>Constructor for a {@link ReceivedOnAcct} with it's quantity field set.</p>
     *
     * @param quantity The quantity of the transaction line item.
     * @return The {@link ReceivedOnAcct} instance with it's quantity field set.
     */
    public ReceivedOnAcct quantity (double quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * <p>Constructor for a {@link ReceivedOnAcct} with it's refundedQty field set.</p>
     *
     * @param refundedQty The refunded quantity of the transaction line item.
     * @return The {@link ReceivedOnAcct} instance with it's refundedQty field set.
     */
    public ReceivedOnAcct refundedQty (double refundedQty) {
        this.refundedQty = refundedQty;
        return this;
    }

    /**
     * <p>Constructor for a {@link ReceivedOnAcct} with it's amount field set.</p>
     *
     * @param amount The amount of the transaction line item.
     * @return The {@link ReceivedOnAcct} instance with it's amount field set.
     */
    public ReceivedOnAcct amount (double amount) {
        this.amount = amount;
        return this;
    }

    /**
     * <p>Constructor for a {@link ReceivedOnAcct} with it's refundedAmount field set.</p>
     *
     * @param refundedAmount The refunded amount of the transaction line item.
     * @return The {@link ReceivedOnAcct} instance with it's refundedAmount field set.
     */
    public ReceivedOnAcct refundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
        return this;
    }

    /**
     * <p>Constructor for a {@link ReceivedOnAcct} with it's employeeID field set.</p>
     *
     * @param employeeID The {@link String} ID of the account.
     * @return The {@link ReceivedOnAcct} instance with it's employeeID field set.
     */
    public ReceivedOnAcct employeeID (String employeeID) {
        this.employeeID = employeeID;
        return this;
    }

    /**
     * <p>Constructor for a {@link ReceivedOnAcct} with it's badgeAssignmentID field set.</p>
     *
     * @param badgeAssignmentID The badge assignment ID.
     * @return The {@link ReceivedOnAcct} instance with it's badgeAssignmentID field set.
     */
    public ReceivedOnAcct badgeAssignmentID (int badgeAssignmentID) {
        this.badgeAssignmentID = badgeAssignmentID;
        return this;
    }

    /**
     * <p>Constructor for a {@link ReceivedOnAcct} with it's receivedOnAcctTypeID field set.</p>
     *
     * @param receivedOnAcctTypeID An ID corresponding to the received on account type.
     * @return The {@link ReceivedOnAcct} instance with it's receivedOnAcctTypeID field set.
     */
    public ReceivedOnAcct receivedOnAcctTypeID (int receivedOnAcctTypeID) {
        this.receivedOnAcctTypeID = receivedOnAcctTypeID;
        return this;
    }

    /**
     * <p>Constructor for a {@link ReceivedOnAcct} with it's giftCardTransInfo field set.</p>
     *
     * @param giftCardTransInfo A {@link String} containing gift card information.
     * @return The {@link ReceivedOnAcct} instance with it's giftCardTransInfo field set.
     */
    public ReceivedOnAcct giftCardTransInfo (String giftCardTransInfo) {
        this.giftCardTransInfo = giftCardTransInfo;
        return this;
    }

    /**
     * <p>Constructor for a {@link ReceivedOnAcct} with it's itemComment field set.</p>
     *
     * @param itemComment A comment {@link String} for the transaction line item.
     * @return The {@link ReceivedOnAcct} instance with it's itemComment field set.
     */
    public ReceivedOnAcct itemComment (String itemComment) {
        this.itemComment = itemComment;
        return this;
    }

    //--------------------------------------------------------------------------
    //  Static methods
    //--------------------------------------------------------------------------
    public static ReceivedOnAcct createRAItemFromDB(HashMap dbData)
    {
        ReceivedOnAcct raItem = new ReceivedOnAcct();

        try {
            raItem.receivedOnAcctID = HashMapDataFns.getIntVal(dbData, "PAITEMID");
            raItem.name = HashMapDataFns.getStringVal(dbData, "ITEMNAME");
            raItem.paTransLineItemID = HashMapDataFns.getIntVal(dbData, "PATRANSLINEITEMID");

            raItem.quantity = HashMapDataFns.getDoubleVal(dbData, "QUANTITY");
            raItem.refundedQty = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDQUANTITY");

            raItem.amount = HashMapDataFns.getDoubleVal(dbData, "AMOUNT");
            raItem.refundedAmount = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDAMOUNT");

            raItem.employeeID = HashMapDataFns.getStringVal(dbData, "EMPLOYEEID");
            raItem.badgeAssignmentID = HashMapDataFns.getIntVal(dbData, "BADGEASSIGNMENTID");

            raItem.receivedOnAcctTypeID = HashMapDataFns.getIntVal(dbData, "PARECEIVEDACCTTYPEID");

            String giftCardTransInfo = HashMapDataFns.getStringVal(dbData, "CREDITCARDTRANSINFO");
            if (!giftCardTransInfo.isEmpty())
            {
                raItem.gcTxnData = new GiftCardTxnData(giftCardTransInfo, ";");
            }
        } catch (Exception e) {
            ReceiptGen.logError("ReceivedOnAcct.createRAItemFromDB error", e);
        }

        return raItem;
    }

    public static ReceivedOnAcct createRAItemFromXMLRPC(ArrayList detail, int transTypeID)
    {
        ReceivedOnAcct raItem = new ReceivedOnAcct();

        try {
            raItem.receivedOnAcctID = new Integer(detail.get(11).toString()).intValue();
            raItem.amount = new Double(detail.get(2).toString()).doubleValue();

            if (transTypeID == TypeData.TranType.REFUND || transTypeID == TypeData.TranType.VOID)
            {
                raItem.quantity = -1;
            }
            else
            {
                raItem.quantity = 1;
            }

            raItem.itemComment = detail.get(4).toString();
            raItem.name = detail.get(5).toString();
        } catch (Exception e) {
            ReceiptGen.logError("ReceivedOnAcct.createRAItemFromXMLRPC error", e);
        }

        return raItem;
    }

    /**
     * <p>Builds a {@link ReceivedOnAcct} from the given {@link XmlRpcStruct}.</p>
     *
     * @param receivedOnAcctStruct The {@link XmlRpcStruct} to use to build the {@link ReceivedOnAcct}.
     * @return The {@link ReceivedOnAcct} instance built form a {@link XmlRpcStruct}.
     */
    public static ReceivedOnAcct buildFromXmlRpcStruct (XmlRpcStruct receivedOnAcctStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(receivedOnAcctStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to ReceivedOnAcct.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new ReceivedOnAcct()
                .receivedOnAcctID(HashMapDataFns.getIntVal(receivedOnAcctStruct, "RECEIVEDONACCTID", true))
                .name(HashMapDataFns.getStringVal(receivedOnAcctStruct, "NAME", true))
                .paTransLineItemID(HashMapDataFns.getIntVal(receivedOnAcctStruct, "PATRANSLINEITEMID", true))
                .quantity(HashMapDataFns.getDoubleVal(receivedOnAcctStruct, "QUANTITY", true))
                .refundedQty(HashMapDataFns.getDoubleVal(receivedOnAcctStruct, "REFUNDEDQTY", true))
                .amount(HashMapDataFns.getDoubleVal(receivedOnAcctStruct, "AMOUNT", true))
                .refundedAmount(HashMapDataFns.getDoubleVal(receivedOnAcctStruct, "REFUNDEDAMOUNT", true))
                .employeeID(HashMapDataFns.getStringVal(receivedOnAcctStruct, "EMPLOYEEID", true))
                .badgeAssignmentID(HashMapDataFns.getIntVal(receivedOnAcctStruct, "BADGEASSIGNMENTID", true))
                .receivedOnAcctTypeID(HashMapDataFns.getIntVal(receivedOnAcctStruct, "RECEIVEDONACCTTYPEID", true))
                .giftCardTransInfo(HashMapDataFns.getStringVal(receivedOnAcctStruct, "GIFTCARDTRANSINFO", true))
                .itemComment(HashMapDataFns.getStringVal(receivedOnAcctStruct, "ITEMCOMMENT", true));
    }

    public static ReceivedOnAcct createRA(ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel, TransactionModel transactionModel)
    {
        ReceivedOnAcct raItem = new ReceivedOnAcct();

        try {
            raItem.receivedOnAcctID = receivedOnAccountLineItemModel.getReceivedOnAccount().getId();
            raItem.amount = receivedOnAccountLineItemModel.getAmount().doubleValue();

            if (transactionModel.getTransactionTypeId() == TypeData.TranType.REFUND || transactionModel.getTransactionTypeId() == TypeData.TranType.VOID)
            {
                raItem.quantity = -1;
            }
            else
            {
                raItem.quantity = 1;
            }

            raItem.itemComment = receivedOnAccountLineItemModel.getItemComment();
            raItem.name = receivedOnAccountLineItemModel.getReceivedOnAccount().getName();

        } catch (Exception e) {
            ReceiptGen.logError("ReceivedOnAcct.createRA(receivedOnAccountLineItemModel) error", e);
        }

        return raItem;
    }

    public String getNameForReceipt()
    {
        String receiptName = "";

        try {
            receiptName = name;
            if (isIntegratedGiftCard() && gcTxnData != null)
            {
                String txnTypeCode = gcTxnData.getDetailValue(DatawireConstants.DatawireDetailPrefix.TRANSACTION_TYPE.getPrefixName());
                String txnTypeReceiptName = DatawireConstants.DatawireTransType.convertTxnCodeToReceiptName(txnTypeCode);
                String cardNumLastFour = getGiftCardCardNumLastFour();

                if (!txnTypeReceiptName.isEmpty())
                {
                    receiptName = receiptName + " " + txnTypeReceiptName;
                }

                if (!cardNumLastFour.isEmpty())
                {
                    receiptName = receiptName + " (" + cardNumLastFour + ")";
                }
            }
        } catch (Exception error) {
            ReceiptGen.logError("ReceivedOnAcct.getNameForReceipt error", error);
        }

        return receiptName;
    }

    public boolean isIntegratedGiftCard()
    {
        boolean isGiftCard = false;

        if (receivedOnAcctTypeID == TypeData.ReceivedOnAcctType.INTEGRATED_GIFT_CARD)
        {
            isGiftCard = true;
        }

        return isGiftCard;
    }

    public String getGiftCardTxnTypeName()
    {
        String txnTypeName = "";

        try
        {
            if (gcTxnData != null)
            {
                String txnCode = gcTxnData.getDetailValue(DatawireConstants.DatawireDetailPrefix.TRANSACTION_TYPE.getPrefixName());
                txnTypeName = txnCode.isEmpty() ? "" : DatawireConstants.DatawireTransType.convertTxnCodeToName(txnCode);
            }
        } catch (Exception error) {
            ReceiptGen.logError("ReceivedOnAcct.getGiftCardTxnTypeName error", error);
        }

        return txnTypeName;
    }

    public String getGiftCardCardNumLastFour()
    {
        String giftCardCardNumLastFour = "";

        try
        {
            if (gcTxnData != null)
            {
                giftCardCardNumLastFour = gcTxnData.getDetailValue(DatawireConstants.DatawireDetailPrefix.CARD_NUM.getPrefixName());
                if (giftCardCardNumLastFour.length() == 16)
                {
                    giftCardCardNumLastFour = giftCardCardNumLastFour.substring(12, 16);
                }
            }
        } catch (Exception error) {
            ReceiptGen.logError("ReceivedOnAcct.getGiftCardCardNumLastFour error", error);
        }

        return giftCardCardNumLastFour;
    }
    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public int getBadgeAssignmentID()
    {
        return badgeAssignmentID;
    }

    public String getBadgeNumber()
    {
        return badgeNumber;
    }

    public String getEmployeeID()
    {
        return employeeID;
    }

    public GiftCardTxnData getGiftCardTransData() {
        return gcTxnData;
    }

    public int getReceivedOnAcctTypeID() {
        return receivedOnAcctTypeID;
    }

    /**
     * <p>Overridden toString() method for a {@link ReceivedOnAcct} instance.</p>
     *
     * @return The {@link ReceivedOnAcct} instance as a {@link String};
     */
    @Override
    public String toString () {

        return String.format("RECEIVEDONACCTID: %s, NAME: %s, PATRANSLINEITEMID: %s, QUANTITY: %s, REFUNDEDQTY: %s, AMOUNT: %s, " +
                "REFUNDEDAMOUNT: %s, EMPLOYEEID: %s, BADGEASSIGNMENTID: %s, RECEIVEDONACCTTYPEID: %s, GIFTCARDTRANSINFO: %s, ITEMCOMMENT: %s",
                Objects.toString((receivedOnAcctID > 0 ? receivedOnAcctID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((paTransLineItemID > 0 ? paTransLineItemID : "N/A"), "N/A"),
                Objects.toString((quantity > 0.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQty > 0.0d ? refundedQty : "N/A"), "N/A"),
                Objects.toString((amount > 0.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount > 0.0d ? refundedAmount : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(employeeID) ? employeeID : "N/A"), "N/A"),
                Objects.toString((badgeAssignmentID > 0 ? badgeAssignmentID : "N/A"), "N/A"),
                Objects.toString((receivedOnAcctTypeID > 0 ? receivedOnAcctTypeID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(giftCardTransInfo) ? giftCardTransInfo : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(itemComment) ? itemComment : "N/A"), "N/A"));
    }

    /**
     * <p>Overridden equals() method for a {@link ReceivedOnAcct} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link ReceivedOnAcct} instance.
     * @return Whether or not the {@link Object} is equal to the {@link ReceivedOnAcct} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a ReceivedOnAcct
        ReceivedOnAcct receivedOnAcct = (ReceivedOnAcct) obj;
        return Objects.equals(receivedOnAcct.receivedOnAcctID, receivedOnAcctID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link ReceivedOnAcct} instance.</p>
     *
     * @return The hash code for a {@link ReceivedOnAcct} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(receivedOnAcctID);
    }
            
}
