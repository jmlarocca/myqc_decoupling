package com.mmhayes.common.receiptGen.TransactionData;

import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.product.models.ItemRewardModel;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.ModifierLineItemModel;
import com.mmhayes.common.transaction.models.ProductLineItemModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by nyu on 10/12/2016.
 */
public class Plu extends TransactionItem
{
    // Identifying attributes
    int itemTypeID = TypeData.ItemType.PLU;
    int pluID;
    String pluCode = "";
    int paTransLineItemModID = 0;

    // Other
    boolean isModifier = false;
    boolean hasModifiers = false;
    String taxDsctStr = "";

    // Modifiers
    Plu parentProduct;
    ArrayList<Plu> modifiers = new ArrayList<Plu>();

    // Weighted item attributes
    boolean isWeightedItem = false;
    boolean isManualWeightedItem = false;
    double netWeight = 0.0;

    Discount itemDiscount;
    // Discounts

    // Loyalty
    ArrayList<LoyaltyReward> freeProductRwds = new ArrayList<LoyaltyReward>();
    ArrayList rewardIDs = new ArrayList();

    // Open txns
    String originalOrderNum = "";
    int transLinePrintStatus = -1;

    // Item surcharges
    ArrayList<Surcharge> itemSurcharges = new ArrayList<Surcharge>();

    // printer info
    private boolean printsOnExpeditor = false;
    private boolean isDiningOption = false;
    private int[] mappedPrinters = null;
    private String hiddenKDSStationID = "";
    private boolean hideOnKDSExpeditor = false;
    private ArrayList<Integer> kdsPrepStationsMapped = new ArrayList<>();
    private int[] mappedKDSPrepStationPrinterIDs = null;

    //Combos
    int paComboTransLineItemID = 0;
    int paComboDetailID = 0;
    double basePrice = 0;
    double comboPrice = 0;

    PrepOption prepOption = null;

    /**
     * <p>Empty constructor for a {@link Plu}.</p>
     *
     */
    public Plu () {}

    /**
     * <p>Constructor for a {@link Plu} with it's quantity field set.</p>
     *
     * @param quantity The quantity of the transaction line item.
     * @return The {@link Plu} instance with it's quantity field set.
     */
    public Plu quantity (double quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's amount field set.</p>
     *
     * @param amount The amount of the transaction line item.
     * @return The {@link Plu} instance with it's amount field set.
     */
    public Plu amount (double amount) {
        this.amount = amount;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's itemComment field set.</p>
     *
     * @param itemComment A comment {@link String} for the transaction line item.
     * @return The {@link Plu} instance with it's itemComment field set.
     */
    public Plu itemComment (String itemComment) {
        this.itemComment = itemComment;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's isManualWeightedItem field set.</p>
     *
     * @param isManualWeightedItem Whether or not the product is weighed manually.
     * @return The {@link Plu} instance with it's isManualWeightedItem field set.
     */
    public Plu isManualWeightedItem (boolean isManualWeightedItem) {
        this.isManualWeightedItem = isManualWeightedItem;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's name field set.</p>
     *
     * @param name The name {@link String} of the product.
     * @return The {@link Plu} instance with it's name field set.
     */
    public Plu name (String name) {
        this.name = name;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's isWeightedItem field set.</p>
     *
     * @param isWeightedItem Whether or not the product is weighed.
     * @return The {@link Plu} instance with it's isWeightedItem field set.
     */
    public Plu isWeightedItem (boolean isWeightedItem) {
        this.isWeightedItem = isWeightedItem;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's netWeight field set.</p>
     *
     * @param netWeight The net weight of the product.
     * @return The {@link Plu} instance with it's netWeight field set.
     */
    public Plu netWeight (double netWeight) {
        this.netWeight = netWeight;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's pluCode field set.</p>
     *
     * @param pluCode The plu code {@link String} for the product.
     * @return The {@link Plu} instance with it's pluCode field set.
     */
    public Plu pluCode (String pluCode) {
        this.pluCode = pluCode;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's taxDsctStr field set.</p>
     *
     * @param taxDsctStr The tax discount {@link String} for the product.
     * @return The {@link Plu} instance with it's taxDsctStr field set.
     */
    public Plu taxDsctStr (String taxDsctStr) {
        this.taxDsctStr = taxDsctStr;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's isModifier field set.</p>
     *
     * @param isModifier Whether or not the product is a modifier.
     * @return The {@link Plu} instance with it's isModifier field set.
     */
    public Plu isModifier (boolean isModifier) {
        this.isModifier = isModifier;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's originalOrderNum field set.</p>
     *
     * @param originalOrderNum Original order number {@link String} this product belongs to within a transaction.
     * @return The {@link Plu} instance with it's originalOrderNum field set.
     */
    public Plu originalOrderNum (String originalOrderNum) {
        this.originalOrderNum = originalOrderNum;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's hasModifiers field set.</p>
     *
     * @param hasModifiers Whether or not this product has modifiers.
     * @return The {@link Plu} instance with it's hasModifiers field set.
     */
    public Plu hasModifiers (boolean hasModifiers) {
        this.hasModifiers = hasModifiers;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's freeProductRwds field set.</p>
     *
     * @param freeProductRwds An {@link ArrayList} of {@link LoyaltyReward}  corresponding to free product rewards associated with the product.
     * @return The {@link Plu} instance with it's freeProductRwds field set.
     */
    public Plu freeProductRwds (ArrayList<LoyaltyReward> freeProductRwds) {
        this.freeProductRwds = freeProductRwds;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's paTransLineItemID field set.</p>
     *
     * @param paTransLineItemID The transaction line item ID for the product line item.
     * @return The {@link Plu} instance with it's paTransLineItemID field set.
     */
    public Plu paTransLineItemID (int paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's paTransLineItemModID field set.</p>
     *
     * @param paTransLineItemModID The transaction line item modifier ID for the product line item.
     * @return The {@link Plu} instance with it's paTransLineItemModID field set.
     */
    public Plu paTransLineItemModID (int paTransLineItemModID) {
        this.paTransLineItemModID = paTransLineItemModID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's rewardIDs field set.</p>
     *
     * @param rewardIDs An {@link ArrayList} of {@link Integer} reward IDs corresponding to the IDs rewards associated with the product.
     * @return The {@link Plu} instance with it's rewardIDs field set.
     */
    public Plu rewardIDs (ArrayList<Integer> rewardIDs) {
        this.rewardIDs = rewardIDs;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's prepOption field set.</p>
     *
     * @param prepOption A {@link PrepOption} containing prep option data for a prep option associated with the product.
     * @return The {@link Plu} instance with it's prepOption field set.
     */
    public Plu prepOption (PrepOption prepOption) {
        this.prepOption = prepOption;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's grossWeight field set.</p>
     *
     * @param grossWeight The gross weight of the product.
     * @return The {@link Plu} instance with it's grossWeight field set.
     */
    public Plu grossWeight (BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's fixedNetWeight field set.</p>
     *
     * @param fixedNetWeight The net weight of the product.
     * @return The {@link Plu} instance with it's fixedNetWeight field set.
     */
    public Plu fixedNetWeight (BigDecimal fixedNetWeight) {
        this.fixedNetWeight = fixedNetWeight;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's paComboTransLineItemID field set.</p>
     *
     * @param paComboTransLineItemID The ID of the combo associated with the product.
     * @return The {@link Plu} instance with it's paComboTransLineItemID field set.
     */
    public Plu paComboTransLineItemID (int paComboTransLineItemID) {
        this.paComboTransLineItemID = paComboTransLineItemID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's paComboDetailID field set.</p>
     *
     * @param paComboDetailID The ID of the combo detail associated with the product.
     * @return The {@link Plu} instance with it's paComboDetailID field set.
     */
    public Plu paComboDetailID (int paComboDetailID) {
        this.paComboDetailID = paComboDetailID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's basePrice field set.</p>
     *
     * @param basePrice The base price of the product.
     * @return The {@link Plu} instance with it's basePrice field set.
     */
    public Plu basePrice (double basePrice) {
        this.basePrice = basePrice;
        return this;
    }

    /**
     * <p>Constructor for a {@link Plu} with it's comboPrice field set.</p>
     *
     * @param comboPrice The price of the product within a combo.
     * @return The {@link Plu} instance with it's comboPrice field set.
     */
    public Plu comboPrice (double comboPrice) {
        this.comboPrice = comboPrice;
        return this;
    }



    //--------------------------------------------------------------------------
    //  Static methods
    //--------------------------------------------------------------------------
    // Creates a Plu object based on information retrieved from the database
    public static Plu createPluFromDB(HashMap dbData, int transTypeID)
    {
        Plu product = new Plu();

        product.pluID = HashMapDataFns.getIntVal(dbData, "PAITEMID");
        product.paTransLineItemID = HashMapDataFns.getIntVal(dbData, "PATRANSLINEITEMID");
        product.paTransLineItemModID = HashMapDataFns.getIntVal(dbData, "PATRANSLINEITEMMODID");
        product.name = HashMapDataFns.getStringVal(dbData, "ITEMNAME");
        product.pluCode = HashMapDataFns.getStringVal(dbData, "PLUCODE");

        product.isModifier = HashMapDataFns.getBooleanVal(dbData, "ISMODIFIER");
        product.isWeightedItem = HashMapDataFns.getBooleanVal(dbData, "SCALEUSED");
        product.isDiningOption = HashMapDataFns.getBooleanVal(dbData, "ISDININGOPTION");
        String manWeightCheck = HashMapDataFns.getStringVal(dbData, "ITEMCOMMENT");
        if ( (manWeightCheck != null) && (manWeightCheck.length() > 0) && (manWeightCheck.equals("MAN WT")) )
        {
            product.isManualWeightedItem = true;
        }
        if (product.isWeightedItem)
        {
            // For voids and refunds, assume a quantity of -1
            product.quantity = 1;

            if (transTypeID == TypeData.TranType.REFUND || transTypeID == TypeData.TranType.VOID)
            {
                product.quantity = -1;
            }

            // Weight is stored in the Quantity field
            product.netWeight = Math.abs(HashMapDataFns.getDoubleVal(dbData, "QUANTITY"));
        }
        else
        {
            product.quantity = HashMapDataFns.getDoubleVal(dbData, "QUANTITY");
        }

        product.grossWeight = new BigDecimal(Math.abs(HashMapDataFns.getDoubleVal(dbData, "GROSSWEIGHT")));
        product.fixedNetWeight = new BigDecimal(Math.abs(HashMapDataFns.getDoubleVal(dbData, "FIXEDNETWEIGHT")));

        product.refundedQty = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDQUANTITY");

        product.amount = HashMapDataFns.getDoubleVal(dbData, "AMOUNT");
        product.refundedAmount = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDAMOUNT");

        product.originalOrderNum = HashMapDataFns.getStringVal(dbData, "ORIGINALORDERNUM");
        product.transLinePrintStatus = HashMapDataFns.getIntVal(dbData, "PRINTSTATUSID");

        product.paComboTransLineItemID = HashMapDataFns.getIntVal(dbData, "PACOMBOTRANSLINEITEMID");
        product.paComboDetailID = HashMapDataFns.getIntVal(dbData, "PACOMBODETAILID");
        product.comboPrice = HashMapDataFns.getDoubleVal(dbData, "COMBOPRICE");
        product.basePrice = HashMapDataFns.getDoubleVal(dbData, "BASEPRICE");

        product.prepOption = PrepOption.createPrepOptionFromDB(dbData);

        if (product.prepOption != null && product.prepOption.prepOptionPrice != 0) {
            product.amount = product.amount - product.prepOption.prepOptionPrice; //store the amount field minus the prepOption Price
        }

        return product;
    }

    // Creates a Plu object based on information received in XML RPC (like Flex)
    public static Plu createPluFromXMLRPC(ArrayList pluData)
    {
        Plu product = new Plu();

        try
        {
            product.quantity = new Double(pluData.get(1).toString()).doubleValue();
            product.amount = new Double(pluData.get(2).toString()).doubleValue();
            product.itemComment = pluData.get(4).toString();
            //Manually weighed items are delineated by having "MAN WT" in their comment field  (NTEP requires we list them on the receipt)
            if ( (product.itemComment != null) && (product.itemComment.length() > 0) && (product.itemComment.equals("MAN WT")) )
            {
                product.isManualWeightedItem = true;
            }
            product.name = pluData.get(5).toString();

            if (pluData.get(6).toString().equals("") == false && pluData.get(6).toString().equals("0") == false)
            {
                product.isWeightedItem = true;
                product.netWeight = new Double(pluData.get(6).toString()).doubleValue();
            }

            product.pluCode = pluData.get(7).toString();
            product.taxDsctStr = pluData.get(8).toString();

            product.isModifier = new Boolean(pluData.get(10).toString()).booleanValue();
            product.originalOrderNum = pluData.get(11).toString();

            //Add modifiers array
            if (pluData.size() > 11)
            {
                product.hasModifiers = new Boolean(pluData.get(12).toString()).booleanValue();

                //Loop through the array of rewards
                if (pluData.size() > 12)
                {
                    ArrayList rewardsDetailArr = (ArrayList) pluData.get(13);

                    for (int x = 0; x < rewardsDetailArr.size(); x++)
                    {
                        ArrayList rewardArr = (ArrayList) rewardsDetailArr.get(x);
                        //Rewards should fip their signs to positive for refunds/voids
                        if (product.quantity<0)
                        {
                            Double rewAmt = new Double(rewardArr.get(6).toString()).doubleValue();

                            rewardArr.set(6, Math.abs(rewAmt));
                        }
                        LoyaltyReward loyReward = new LoyaltyReward().createRewardFromArrayList(rewardArr);
                        product.addFreeProductRwd(loyReward);
                    }
                }
            }

            if (pluData.size() >= 16)
            {
                product.paTransLineItemID =  Integer.parseInt(pluData.get(14).toString());
                product.paTransLineItemModID = Integer.parseInt(pluData.get(15).toString());
                product.rewardIDs = (ArrayList) pluData.get(16);
            }
            if (pluData.size() >= 17)
            {
                if ( (pluData.get(17) != null) && (pluData.get(17).toString().length() > 1)  )
                {
                    ArrayList prepOptionsDetailArr = (ArrayList) pluData.get(17);
                    if (prepOptionsDetailArr.size() > 0)
                    {
                        product.prepOption = PrepOption.createPrepOptionFromXMLRPC(prepOptionsDetailArr);
                    }
                }
            }

            if (pluData.size() >= 18)
            {
                if ( (pluData.get(18) != null) && (pluData.get(18).toString().length() > 0)  )
                {
                    product.grossWeight = new BigDecimal(new Double(pluData.get(18).toString()).doubleValue());
                }
            }
            if (pluData.size() >= 19)
            {
                if ( (pluData.get(19) != null) && (pluData.get(19).toString().length() > 0)  )
                {
                    product.fixedNetWeight = new BigDecimal(new Double(pluData.get(19).toString()).doubleValue());
                }
            }

            if (pluData.size() > 20)
            {
                if (pluData.get(20) != null)
                {
                    ArrayList comboDetailArr = (ArrayList) pluData.get(20);
                    if (!comboDetailArr.isEmpty())
                    {
                        //[0] - tempID (product)
                        //[1] - tempComboID (Combo)
                        //[2] - tempComboDiscountID (Line Item Discount)
                        //[3] - paComboTransLineItemID
                        //[4] - paComboDetailID
                        //[5] - basePrice
                        //[6] - comboPrice
                        product.paComboTransLineItemID = Integer.parseInt(comboDetailArr.get(3).toString());
                        product.paComboDetailID = Integer.parseInt(comboDetailArr.get(4).toString());
                        product.basePrice = Double.parseDouble(comboDetailArr.get(5).toString());
                        product.comboPrice = Double.parseDouble(comboDetailArr.get(6).toString());
                    }
                }
            }

        }
        catch (Exception ex)
        {
            ReceiptGen.logError("Plu.createPluFromXMLRPC error", ex);
        }

        return product;
    }

    /**
     * <p>Builds a {@link Plu} from the given {@link XmlRpcStruct}.</p>
     *
     * @param pluStruct The {@link XmlRpcStruct} to use to build the {@link Plu}.
     * @return The {@link Plu} instance built form a {@link XmlRpcStruct}.
     */
    @SuppressWarnings({"unchecked", "Convert2streamapi", "Duplicates"})
    public static Plu buildFromXmlRpcStruct (XmlRpcStruct pluStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(pluStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to Plu.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<LoyaltyReward> freeProductRwds = new ArrayList<>();
        ArrayList freeProductRwdObjs = ((ArrayList) HashMapDataFns.getValFromMap(pluStruct, "FREEPRODUCTRWDS", true));
        if (!DataFunctions.isEmptyCollection(freeProductRwdObjs)) {
            for (Object obj : freeProductRwdObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    LoyaltyReward loyaltyReward = LoyaltyReward.buildFromXmlRpcStruct(struct);
                    if (loyaltyReward != null) {
                        freeProductRwds.add(loyaltyReward);
                    }
                }
            }
        }

        PrepOption prepOption = new PrepOption();
        XmlRpcStruct prepOptStruct = ((XmlRpcStruct) HashMapDataFns.getValFromMap(pluStruct, "PREPOPTION", true));
        if (!DataFunctions.isEmptyMap(prepOptStruct)) {
            prepOption = PrepOption.buildFromXmlRpcStruct(prepOptStruct);
        }

        return new Plu()
                .quantity(HashMapDataFns.getDoubleVal(pluStruct, "QUANTITY", true))
                .amount(HashMapDataFns.getDoubleVal(pluStruct, "AMOUNT", true))
                .itemComment(HashMapDataFns.getStringVal(pluStruct, "ITEMCOMMENT", true))
                .isManualWeightedItem(HashMapDataFns.getBooleanVal(pluStruct, "ISMANUALWEIGHTEDITEM", true))
                .name(HashMapDataFns.getStringVal(pluStruct, "NAME", true))
                .isWeightedItem(HashMapDataFns.getBooleanVal(pluStruct, "ISWEIGHTEDITEM", true))
                .netWeight(HashMapDataFns.getDoubleVal(pluStruct, "NETWEIGHT", true))
                .pluCode(HashMapDataFns.getStringVal(pluStruct, "PLUCODE", true))
                .taxDsctStr(HashMapDataFns.getStringVal(pluStruct, "TAXDSCTSTR", true))
                .isModifier(HashMapDataFns.getBooleanVal(pluStruct, "ISMODIFIER", true))
                .originalOrderNum(HashMapDataFns.getStringVal(pluStruct, "ORIGINALORDERNUM", true))
                .hasModifiers(HashMapDataFns.getBooleanVal(pluStruct, "HASMODIFIERS", true))
                .freeProductRwds(freeProductRwds)
                .paTransLineItemID(HashMapDataFns.getIntVal(pluStruct, "PATRANSLINEITEMID", true))
                .paTransLineItemModID(HashMapDataFns.getIntVal(pluStruct, "PATRANSLINEITEMMODID", true))
                .rewardIDs(((ArrayList<Integer>) HashMapDataFns.getValFromMap(pluStruct, "REWARDIDS", true)))
                .prepOption(prepOption)
                .grossWeight(new BigDecimal(HashMapDataFns.getStringVal(pluStruct, "GROSSWEIGHT", true)))
                .fixedNetWeight(new BigDecimal(HashMapDataFns.getStringVal(pluStruct, "FIXEDNETWEIGHT", true)))
                .paComboTransLineItemID(HashMapDataFns.getIntVal(pluStruct, "PACOMBOTRANSLINEITEMID", true))
                .paComboDetailID(HashMapDataFns.getIntVal(pluStruct, "PACOMBODETAILID", true))
                .basePrice(HashMapDataFns.getDoubleVal(pluStruct, "BASEPRICE", true))
                .comboPrice(HashMapDataFns.getDoubleVal(pluStruct, "COMBOPRICE", true));
    }

    // Creates a Plu object based on information received in POS API request
    /**
     * Convert a ProductLineItemModel to a Plu
     * @param productLineItemModel
     * @param transactionModel
     * @return
     */
    public static Plu createPlu(ProductLineItemModel productLineItemModel, TransactionModel transactionModel)
    {
        Plu product = new Plu();

        try {
            if (productLineItemModel.getProduct().isModifier()){
                product.paTransLineItemModID = productLineItemModel.getId();
                product.paTransLineItemID = productLineItemModel.getParentPaTransLineItemId();
            } else {
                product.paTransLineItemID =  productLineItemModel.getId();
            }

            product.pluID = productLineItemModel.getProduct().getId();
            product.name = productLineItemModel.getProduct().getName();
            product.pluCode = productLineItemModel.getProduct().getProductCode();
            product.quantity = productLineItemModel.getQuantity().doubleValue();
            product.isModifier = productLineItemModel.getProduct().isModifier();
            if (productLineItemModel.getProduct().isWeighted()) {
                // For voids and refunds, assume a quantity of -1
                product.quantity = 1;

                if (transactionModel.getTransactionTypeId() == TypeData.TranType.REFUND || transactionModel.getTransactionTypeId() == TypeData.TranType.VOID) {
                    product.quantity = -1;
                }

                // Weight is stored in the Quantity field
                product.netWeight = Math.abs(productLineItemModel.getQuantity().doubleValue());
                product.isWeightedItem = true;
            } else {
                product.netWeight = productLineItemModel.getQuantity().doubleValue();
            }

            product.isDiningOption = productLineItemModel.getProduct().isDiningOption();
            product.itemComment = productLineItemModel.getItemComment();
            //Manually weighed items are delineated by having "MAN WT" in their comment field  (NTEP requires we list them on the receipt)
            if ( (product.itemComment != null) && (product.itemComment.length() > 0) && (product.itemComment.equals("MAN WT")) )
            {
                product.isManualWeightedItem = true;
            }

            //TODO: fixedTareWeight
            //TODO: product.grossWeight = ...
            //TODO: product.fixedNetWeight = ..
            product.refundedQty = (productLineItemModel.getRefundedQty() == null) ? 0 : productLineItemModel.getRefundedQty().doubleValue();
            product.amount = productLineItemModel.getAmount().doubleValue();
            product.refundedAmount = (productLineItemModel.getRefundedAmount() == null) ? 0 : productLineItemModel.getRefundedAmount().doubleValue();

            product.originalOrderNum = productLineItemModel.getOriginalOrderNumber();
            product.transLinePrintStatus = (productLineItemModel.getPrintStatusId() == null ? -1 : productLineItemModel.getPrintStatusId());
            product.hasModifiers = (productLineItemModel.getModifiers() != null && !productLineItemModel.getModifiers().isEmpty());

            product.paComboTransLineItemID = (productLineItemModel.getComboTransLineItemId() == null ? 0 : productLineItemModel.getComboTransLineItemId());
            product.paComboDetailID = (productLineItemModel.getComboDetailId() == null ? 0 : productLineItemModel.getComboDetailId());
            product.comboPrice = (productLineItemModel.getComboPrice() == null ? new Double(0) : new Double(productLineItemModel.getComboPrice().toString()));
            product.basePrice = (productLineItemModel.getBasePrice() == null ? new Double(0) : new Double(productLineItemModel.getBasePrice().toString()));

            //store the amount field minus the prepOption Price
            if (productLineItemModel.getPrepOption() != null){
                product.prepOption = PrepOption.createPrepOption(productLineItemModel.getPrepOption());
                if (product.prepOption.prepOptionPrice != 0){
                    product.amount = product.amount - product.prepOption.prepOptionPrice;
                }
            }

        } catch (Exception ex) {
            ReceiptGen.logError("Plu.createPlu error", ex);
        }

        return product;
    }

    //--------------------------------------------------------------------------
    //  Non static methods
    //--------------------------------------------------------------------------

    /**
     * <p>Gets the name of the product and it's associated prep option to be printed on the receipt.</p>
     *
     * @return The name of the product and it's associated prep option as a {@link String} to be printed on the receipt.
     */
    @SuppressWarnings("SimplifiableBooleanExpression")
    public String getNameWithPrepOption () {
        String pluName = "";

        try {
            pluName = getName();
            PrepOption prepOption = getPrepOption();
            if ((prepOption != null) && (StringFunctions.stringHasContent(pluName))) {
                if (((prepOption.getDefaultOption()) && (prepOption.getDisplayDefaultKitchen())) || (!prepOption.getDefaultOption())) {
                    // get the name of the prep option and append it to the product name
                    if (StringFunctions.stringHasContent(prepOption.getOriginalName())) {
                        pluName += " - "+prepOption.getOriginalName();
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to get the associated prep name for the product %s so " +
                    "it may be added to the receipt in Plu.getNameWithPrepOption",
                    Objects.toString(getName(), "NULL")), Logger.LEVEL.ERROR);
        }

        return pluName;
    }

    // Returns the value of the full price of the item with modifiers and prep options included (for display on the right-hand side of the receipt print as the total price)
    //This amount should NOT be used to calculate any values (such as subtotal, total, etc)
    public BigDecimal getTotalItemPriceForDisplay()
    {
        double finalAmount = amount;
        if (getPrepOption()!=null)
        {
            //Add in the cost of the prep option (myQc sends them already added together, POS does not)
            if (getPrepOption().getPrepOptionPrice() > 0)
            {
                finalAmount = getAmount() + getPrepOption().getPrepOptionPrice();
            }
        }
        //Add the cost of modifiers to the total for the product displayed on the receipt
        if (getModifiers().size() > 0)
        {
            for (Plu modPlu : getModifiers()){
                Double modPrice = modPlu.getSingleQtyTotalItemPrice().doubleValue();
                finalAmount += modPrice;
            }
        }

        BigDecimal totalItemPrice = new BigDecimal(quantity * finalAmount + 0.0).setScale(3, RoundingMode.HALF_UP);
        totalItemPrice = totalItemPrice.setScale(2, RoundingMode.HALF_UP);

        if (isWeightedItem)
        {
            totalItemPrice = new BigDecimal(netWeight * amount * quantity).setScale(5, RoundingMode.HALF_UP);
            totalItemPrice = totalItemPrice.setScale(2, RoundingMode.HALF_UP);
        }

        if (itemDiscount != null)
        {
            totalItemPrice = totalItemPrice.add(itemDiscount.getTotalItemPrice());
        }

        if ( (freeProductRwds.size() > 0) && (parentProduct == null) )      // Modifiers will never have a free product rwd applied only to itself
        {
            for (LoyaltyReward reward : freeProductRwds)
            {
                BigDecimal rewardAmt = reward.getTotalItemPrice();
                Boolean hasMods = false;
                if (this.hasModifiers || (modifiers.size() > 0) )
                {
                    hasMods = true;
                }
                //If the reward amount is greater than the plu and there are modifiers
                if ( (rewardAmt.abs().compareTo(totalItemPrice) == 1) && (hasMods) )
                {
                    //Wipe out the product price
                    totalItemPrice = BigDecimal.ZERO;
                }
                else
                {
                    //Else either the free product doesn't cover the entire cost of the product, or there are no modifiers
                    if (totalItemPrice.compareTo(BigDecimal.ZERO) == -1)
                    {
                        totalItemPrice = totalItemPrice.add(rewardAmt.abs());
                    }
                    else
                    {
                        totalItemPrice = totalItemPrice.add(rewardAmt);
                    }
                }
            }
        }

        if (itemSurcharges.size() > 0)
        {
            for (Surcharge itemSurcharge : itemSurcharges)
            {
                totalItemPrice = totalItemPrice.add(itemSurcharge.getTotalItemPrice());
            }
        }

        // We want this here so the price prints with two decimal places
        totalItemPrice = totalItemPrice.setScale(2, RoundingMode.HALF_UP);

        return totalItemPrice;
    }


    // Returns the value of the full price of the item
    public BigDecimal getTotalItemPrice()
    {
        double finalAmount = amount;
        if (getPrepOption()!=null)
        {
            //Add in the cost of the prep option (myQc sends them already added together, POS does not)
            if (getPrepOption().getPrepOptionPrice() != 0)
            {
                finalAmount = getAmount() + getPrepOption().getPrepOptionPrice();
            }
        }

        BigDecimal totalItemPrice = new BigDecimal(quantity * finalAmount + 0.0).setScale(3, RoundingMode.HALF_UP);
        totalItemPrice = totalItemPrice.setScale(2, RoundingMode.HALF_UP);

        if (isWeightedItem)
        {
            totalItemPrice = new BigDecimal(netWeight * amount * quantity).setScale(5, RoundingMode.HALF_UP);
            totalItemPrice = totalItemPrice.setScale(2, RoundingMode.HALF_UP);
        }

        if (itemDiscount != null)
        {
            totalItemPrice = totalItemPrice.add(itemDiscount.getTotalItemPrice());
        }

        //Modifiers will never have a free product rwd applied only to itself, so we only need to calculate free prods on parents or products without mods
        if ( (freeProductRwds.size() > 0) && (parentProduct == null) )
        {
            for (LoyaltyReward reward : freeProductRwds)
            {
                BigDecimal rewardAmt = reward.getTotalItemPrice();

                //Subtract the reward amount from the product.
                //HP 4773 - We used to zero out the cost of products if free product rewards were applied, but this was shortsighted
                //A free product reward may be of a lower value than the cost of the product (if the reward has a max amount)
                //A product may also have priced modifiers that need to be added after a reward has been applied
                //Zeroing it out made the priced mods give the product a cost higher than it should
                //The reward should just be made positive and subtracted from the total. If the product amount becomes negative, the modifiers' cost will bring it back to $0 or positive again

                //If this is a refund/void
                if (totalItemPrice.compareTo(BigDecimal.ZERO) == -1)
                {
                    totalItemPrice = totalItemPrice.add(rewardAmt);
                }
                else
                {
                    totalItemPrice = totalItemPrice.subtract(rewardAmt.abs());
                }
            }
        }

        if (itemSurcharges.size() > 0)
        {
            for (Surcharge itemSurcharge : itemSurcharges)
            {
                totalItemPrice = totalItemPrice.add(itemSurcharge.getTotalItemPrice());
            }
        }

        // We want this here so the price prints with two decimal places
        totalItemPrice = totalItemPrice.setScale(2, RoundingMode.HALF_UP);

        return totalItemPrice;
    }

    // Returns the value of the full price of the item
    public BigDecimal getSingleQtyTotalItemPrice()
    {
        double finalAmount = amount;
        if (getPrepOption()!=null)
        {
            //Add in the cost of the prep option (myQc sends them already added together, POS does not)
            if (getPrepOption().getPrepOptionPrice() > 0)
            {
                finalAmount = getAmount() + getPrepOption().getPrepOptionPrice();
            }
        }

        BigDecimal totalItemPrice = new BigDecimal(finalAmount + 0.0).setScale(3, RoundingMode.HALF_UP);
        totalItemPrice = totalItemPrice.setScale(2, RoundingMode.HALF_UP);

        if (isWeightedItem)
        {
            totalItemPrice = new BigDecimal(netWeight * amount).setScale(5, RoundingMode.HALF_UP);
            totalItemPrice = totalItemPrice.setScale(2, RoundingMode.HALF_UP);
        }

        if (itemDiscount != null)
        {
            totalItemPrice = totalItemPrice.add(itemDiscount.getSingleQtyTotalItemPrice());
        }

        if ( (freeProductRwds.size() > 0) && (parentProduct == null) )      // Modifiers will never have a free product rwd applied only to itself
        {
            for (LoyaltyReward reward : freeProductRwds)
            {
                BigDecimal rewardAmt = reward.getSingleQtyTotalItemPrice();
                Boolean hasMods = false;
                if (this.hasModifiers || (modifiers.size() > 0) )
                {
                    hasMods = true;
                }
                //If the reward amount is greater than the plu and there are modifiers
                if ( (rewardAmt.abs().compareTo(totalItemPrice) == 1) && (hasMods) )
                {
                    //Wipe out the product price
                    totalItemPrice = BigDecimal.ZERO;
                }
                else
                {
                    //Else either the free product doesn't cover the entire cost of the product, or there are no modifiers
                    if (totalItemPrice.compareTo(BigDecimal.ZERO) == -1)
                    {
                        totalItemPrice = totalItemPrice.add(rewardAmt.abs());
                    }
                    else
                    {
                        totalItemPrice = totalItemPrice.add(rewardAmt);
                    }
                }
            }
        }

        if (itemSurcharges.size() > 0)
        {
            for (Surcharge itemSurcharge : itemSurcharges)
            {
                totalItemPrice = totalItemPrice.add(itemSurcharge.getTotalItemPrice());
            }
        }

        // We want this here so the price prints with two decimal places
        totalItemPrice = totalItemPrice.setScale(2, RoundingMode.HALF_UP);

        return totalItemPrice;
    }

    // Builds the string of the tax and discount codes that were applied to this Plu
    public void addToTaxDsctRwdStr(String newShortName)
    {
        if (taxDsctStr.equals(""))
        {
            taxDsctStr = taxDsctStr + "(" + newShortName;
        }
        else
        {
            // Check if we already have this tax or discount code added..
            boolean codeAlreadyExists = false;
            String tempTaxDsctStr = taxDsctStr.substring(1, taxDsctStr.length());
            String[] existingTaxDsctCodes = tempTaxDsctStr.split(",");

            for (String code : existingTaxDsctCodes)
            {
                if (code.equals(newShortName))
                {
                    codeAlreadyExists = true;
                    break;
                }
            }

            // If we don't have it already, add it
            if (!codeAlreadyExists)
            {
                taxDsctStr = taxDsctStr + "," + newShortName;
            }
        }
    }

    // Check if this same product line already exists in transItem, could happen when you put the same product more
    // than once on the same keypad, joining on the keypad to get product's button text
    public boolean checkDuplicateProduct(ArrayList transItems) {

        for (Object transItem : transItems)
        {
            if (transItem instanceof Plu )
            {
                Plu transItemPlu = (Plu)transItem;

                if( transItemPlu.getPATransLineItemID() == getPATransLineItemID() && !transItemPlu.getIsModifier() &&
                        !getIsModifier() && transItemPlu.getPluID() == getPluID() )
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void addModifier(Plu modifier) {
        modifiers.add(modifier);
    }
    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public int getPluID()
    {
        return pluID;
    }

    public int getPATransLineItemModID()
    {
        return paTransLineItemModID;
    }

    public boolean getIsModifier()
    {
        return isModifier;
    }

    public Discount getItemDiscount()
    {
        return itemDiscount;
    }

    public void setItemDiscount(Discount itemDsct)
    {
        itemDiscount = itemDsct;
    }

    public boolean getIsWeightedItem()
    {
        return isWeightedItem;
    }

    public double getNetWeight()
    {
        return netWeight;
    }

    public String getTaxDsctStr()
    {
        return taxDsctStr;
    }

    public String getPluCode()
    {
        return pluCode;
    }

    public double getUnitPrice()
    {
        return amount;
    }

    public int getPaTransLineItemModID()
    {
        return paTransLineItemModID;
    }

    public void addFreeProductRwd (LoyaltyReward freeProductRwd)
    {
        freeProductRwds.add(freeProductRwd);
    }

    public boolean hasReward (int rewardID)
    {
        boolean hasRwd = false;

        for (LoyaltyReward reward : freeProductRwds)
        {
            if (reward.getRewardID() == rewardID)
            {
                hasRwd = true;
                break;
            }
        }

        return hasRwd;
    }

    public void updateFreeProdRwd(LoyaltyReward freeProdRwd)
    {
        amount = amount + freeProdRwd.amount;
        quantity = quantity + freeProdRwd.quantity;

        refundedAmount = refundedAmount + freeProdRwd.refundedAmount;
        refundedQty = refundedQty + freeProdRwd.refundedQty;
    }

    public ArrayList<LoyaltyReward> getFreeProductRwds()
    {
        return freeProductRwds;
    }

    public Plu getParentProduct() {
        return parentProduct;
    }

    public void setParentProduct(Plu parentProduct) {
        this.parentProduct = parentProduct;
    }

    public ArrayList getRewardIDs() {
        return rewardIDs;
    }

    public void setRewardIDs(ArrayList mappedRewardIDs)
    {
        if (mappedRewardIDs != null)
        {
            for (int x = 0; x < mappedRewardIDs.size(); x++)
            {
                HashMap hm = (HashMap) mappedRewardIDs.get(x);
                int loyaltyRewardID = new Integer(HashMapDataFns.getStringVal(hm, "LOYALTYREWARDID")).intValue();
                rewardIDs.add(loyaltyRewardID);
            }
        }
    }

    public ArrayList<Plu> getModifiers() {
        return modifiers;
    }

    public void setModifiers(ArrayList<Plu> modifiers) {
        this.modifiers = modifiers;
    }

    public String getOriginalOrderNum()
    {
        if (originalOrderNum == null)
        {
            originalOrderNum = "";
        }
        return originalOrderNum;
    }

    public void setOriginalOrderNum(String originalOrderNum) {
        this.originalOrderNum = originalOrderNum;
    }

    public int getTransLinePrintStatus() {
        return transLinePrintStatus;
    }

    public void setTransLinePrintStatus(int transLinePrintStatus) {
        this.transLinePrintStatus = transLinePrintStatus;
    }

    public boolean hasBeenPrinted() {
        return transLinePrintStatus == PrintJobStatusType.WAITING.getTypeID();
    }

    public Boolean getIsManualWeight()
    {
        return isManualWeightedItem;
    }

    public Boolean hasModifiers()
    {
        return modifiers.size() > 0 || hasModifiers;
    }

    public void addItemSurcharge (Surcharge itemSurchg)
    {
        itemSurcharges.add(itemSurchg);
    }

    public ArrayList<Surcharge> getItemSurcharges()
    {
        return itemSurcharges;
    }
    public void setPrintsOnExpeditor (boolean printsOnExpeditor) {
        this.printsOnExpeditor = printsOnExpeditor;
    }

    public boolean getPrintsOnExpeditor () {
        return printsOnExpeditor;
    }

    public void setIsDiningOption (boolean isDiningOption) {
        this.isDiningOption = isDiningOption;
    }

    public boolean isDiningOption () {
        return isDiningOption;
    }

    public void setMappedPrinters (int[] mappedPrinters) {
        this.mappedPrinters = mappedPrinters;
    }

    public int[] getMappedPrinters () {
        return mappedPrinters;
    }

    public void setKDSPrepStationsMapped (ArrayList<Integer> kdsPrepStationsMapped) {
        this.kdsPrepStationsMapped = kdsPrepStationsMapped;
    }

    public ArrayList<Integer> getKDSPrepStationsMapped () {
        return kdsPrepStationsMapped;
    }

    public void setHiddenKDSStationID (String hiddenKDSStationID) {
        this.hiddenKDSStationID = hiddenKDSStationID;
    }

    public String getHiddenKDSStationID () {
        return hiddenKDSStationID;
    }

    public void setHideOnKDSExpeditor (boolean hideOnKDSExpeditor) {
        this.hideOnKDSExpeditor = hideOnKDSExpeditor;
    }

    public boolean getHideOnKDSExpeditor () {
        return hideOnKDSExpeditor;
    }

    public int getPaComboTransLineItemID() {
        return paComboTransLineItemID;
    }

    public void setPaComboTransLineItemID(int paComboTransLineItemID) {
        this.paComboTransLineItemID = paComboTransLineItemID;
    }

    public int getPaComboDetailID() {
        return paComboDetailID;
    }

    public void setPaComboDetailID(int paComboDetailID) {
        this.paComboDetailID = paComboDetailID;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public double getComboPrice() {
        return comboPrice;
    }

    public void setComboPrice(double comboPrice) {
        this.comboPrice = comboPrice;
    }

    public PrepOption getPrepOption() {
        return prepOption;
    }

    public void setPrepOption(PrepOption prepOption) {
        this.prepOption = prepOption;
    }

    public int[] getMappedKDSPrepStationPrinterIDs () { return mappedKDSPrepStationPrinterIDs; }

    public void setMappedKDSPrepStationPrinterIDs (int[] mappedKDSPrepStationPrinterIDs) { this.mappedKDSPrepStationPrinterIDs = mappedKDSPrepStationPrinterIDs; }

    /**
     * <p>Overridden toString() method for a {@link Plu} instance.</p>
     *
     * @return The {@link Plu} instance as a {@link String};
     */
    @Override
    public String toString () {

        return String.format("QUANTITY: %s, AMOUNT: %s, ITEMCOMMENT: %s, ISMANUALWEIGHTEDITEM: %s, NAME: %s, ISWEIGHTEDITEM: %s, " +
                "NETWEIGHT: %s, PLUCODE: %s, TAXDSCTSTR: %s, ISMODIFIER: %s, ORIGINALORDERNUM: %s, HASMODIFIERS: %s, FREEPRODUCTRWDS: %s, " +
                "PATRANSLINEITEMID: %s, PATRANSLINEITEMMODID: %s, REWARDIDS: %s, PREPOPTION: %s, GROSSWEIGHT: %s, FIXEDNETWEIGHT: %s, " +
                "PACOMBOTRANSLINEITEMID: %s, PACOMBODETAILID: %s, BASEPRICE: %s, COMBOPRICE: %s",
                Objects.toString((quantity > 0.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((amount > 0.0d ? amount : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(itemComment) ? itemComment : "N/A"), "N/A"),
                Objects.toString(isManualWeightedItem, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString(isWeightedItem, "N/A"),
                Objects.toString((netWeight > 0.0d ? netWeight : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(pluCode) ? pluCode : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(taxDsctStr) ? taxDsctStr : "N/A"), "N/A"),
                Objects.toString(isModifier, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(originalOrderNum) ? originalOrderNum : "N/A"), "N/A"),
                Objects.toString(hasModifiers, "N/A"),
                Objects.toString((!DataFunctions.isEmptyCollection(freeProductRwds) ? StringFunctions.collectionToBracketedString(freeProductRwds) : "N/A"), "N/A"),
                Objects.toString((paTransLineItemID > 0 ? paTransLineItemID : "N/A"), "N/A"),
                Objects.toString((paTransLineItemModID > 0 ? paTransLineItemModID : "N/A"), "N/A"),
                Objects.toString((!DataFunctions.isEmptyCollection(rewardIDs) ? StringFunctions.collectionToBracketedString(rewardIDs) : "N/A"), "N/A"),
                Objects.toString((prepOption != null ? "[" + prepOption.toString() + "]" : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(grossWeight.toPlainString()) ? grossWeight.toPlainString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(fixedNetWeight.toPlainString()) ? fixedNetWeight.toPlainString() : "N/A"), "N/A"),
                Objects.toString((paComboTransLineItemID > 0 ? paComboTransLineItemID : "N/A"), "N/A"),
                Objects.toString((paComboDetailID > 0 ? paComboDetailID : "N/A"), "N/A"),
                Objects.toString((basePrice > 0.0d ? basePrice : "N/A"), "N/A"),
                Objects.toString((comboPrice > 0.0d ? comboPrice : "N/A"), "N/A"));
    }

    /**
     * <p>Overridden equals() method for a {@link Plu} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link Plu} instance.
     * @return Whether or not the {@link Object} is equal to the {@link Plu} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a Plu
        Plu plu = (Plu) obj;
        return Objects.equals(plu.pluID, pluID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link Plu} instance.</p>
     *
     * @return The hash code for a {@link Plu} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(pluID);
    }

}
