package com.mmhayes.common.receiptGen.TransactionData;

/*
    Last Updated (automatically updated by SVN)
    $Author: skaugustine $: Author of last commit
    $Date: 2021-08-18 11:55:10 -0400 (Wed, 18 Aug 2021) $: Date of last commit
    $Rev: 15015 $: Revision of last commit
    Notes: Represents a Tender transaction line item.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.datawire.DatawireConstants;
import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.GiftCardTxnData;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.TenderLineItemModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.*;
import org.owasp.esapi.ESAPI;
import redstone.xmlrpc.XmlRpcStruct;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class Tender extends TransactionItem
{
    private static DataManager dm = new DataManager();
    // Identifying attributes
    int itemTypeID = TypeData.ItemType.TENDER;
    int tenderTypeID;
    String tenderTypeName = "";
    int tenderID;

    // Other
    String commentPromptText = "";
    String itemComment = "";
    Donation donationItem = new Donation();
    // Quickcharge details
    EmployeeData emp;
    private String acctName = null;
    private int acctBadgeAssignmentID = -1;
    private String acctBadgeNumber = null;
    private String acctEmployeeID = null;
    private String acctEmployeeNumber = null;
    private int acctNumSplit = -1;
    private int acctReceiptBalanceTypeID = -1;
    private String acctReceiptBalancePromptTxt = null;
    private double acctGlobalBalance = -1.0d;
    private double acctGlobalAvailable = -1.0d;
    private double acctStoreBalance = -1.0d;
    private double acctStoreAvailable = -1.0d;
    private double acctDailyLimitBal = -1.0d;

    // Credit card info
    String creditCardTransInfo = "";
    String refNo = "";
    String invoiceNo = "";
    String authCode = "";
    String merchID = "";
    String acqRefData = "";
    String processData = "";
    String acctNo = "";
    Boolean printAcctNo = false;
    // Credit card info - EMV specific
    String entryMethod = "";
    String cvm = "";
    String appLbl = "";
    String aid = "";
    String tvr = "";
    String iad = "";
    String tsi = "";
    String arc = "";
    String cardTypeName = "";
    String creditOrDebit = "";
    String manualOrSwipe = "";
    String partialAuth = "";
    String cardholderAgreement = "";
    String cardholderName = "";
    private String paymentGatewayCCDetails = "";            // Added for Simplify.. comma delimited list of EMV details to print

    String integratedGiftCardBalance = "";
    GiftCardTxnData gcTxnData;

    //Voucher information
    String voucherCode = "";


    /**
     * <p>Empty constructor for a a {@link Tender}.</p>
     *
     */
    public Tender () {}

    /**
     * <p>Constructor for a {link Tender} with it's tenderID fields set.</p>
     *
     * @param tenderID The ID of the tender.
     * @return The {link Tender} instance with it's tenderID fields set.
     */
    public Tender tenderID (int tenderID) {
        this.tenderID = tenderID;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's amount fields set.</p>
     *
     * @param amount The amount of the transaction line item.
     * @return The {link Tender} instance with it's amount fields set.
     */
    public Tender amount (double amount) {
        this.amount = amount;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's name fields set.</p>
     *
     * @param name The name {@link String} of the tender.
     * @return The {link Tender} instance with it's name fields set.
     */
    public Tender name (String name) {
        this.name = name;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's tenderTypeName fields set.</p>
     *
     * @param tenderTypeName The name {@link String} of the tender type.
     * @return The {link Tender} instance with it's tenderTypeName fields set.
     */
    public Tender tenderTypeName (String tenderTypeName) {
        this.tenderTypeName = tenderTypeName;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's tenderTypeID fields set.</p>
     *
     * @param tenderTypeID The ID of the tender type.
     * @return The {link Tender} instance with it's tenderTypeID fields set.
     */
    public Tender tenderTypeID (int tenderTypeID) {
        this.tenderTypeID = tenderTypeID;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's itemComment fields set.</p>
     *
     * @param itemComment A comment {@link String} for the transaction line item.
     * @return The {link Tender} instance with it's itemComment fields set.
     */
    public Tender itemComment (String itemComment) {
        this.itemComment = itemComment;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctNo fields set.</p>
     *
     * @param acctNo The account number {@link String} for a QC tender.
     * @return The {link Tender} instance with it's acctNo fields set.
     */
    public Tender acctNo (String acctNo) {
        this.acctNo = acctNo;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's authCode fields set.</p>
     *
     * @param authCode The auth code {@link String}.
     * @return The {link Tender} instance with it's authCode fields set.
     */
    public Tender authCode (String authCode) {
        this.authCode = authCode;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's merchID fields set.</p>
     *
     * @param merchID The merchant link ID {@link String}.
     * @return The {link Tender} instance with it's merchID fields set.
     */
    public Tender merchID (String merchID) {
        this.merchID = merchID;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's invoiceNo fields set.</p>
     *
     * @param invoiceNo The invoice number {@link String}.
     * @return The {link Tender} instance with it's invoiceNo fields set.
     */
    public Tender invoiceNo (String invoiceNo) {
        this.invoiceNo = invoiceNo;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's refNo fields set.</p>
     *
     * @param refNo a reference number {@link String} for the tender.
     * @return The {link Tender} instance with it's refNo fields set.
     */
    public Tender refNo (String refNo) {
        this.refNo = refNo;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's entryMethod fields set.</p>
     *
     * @param entryMethod A {@link String} corresponding to the entry method of the tender.
     * @return The {link Tender} instance with it's entryMethod fields set.
     */
    public Tender entryMethod (String entryMethod) {
        this.entryMethod = entryMethod;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's appLbl fields set.</p>
     *
     * @param appLbl EMV appLbl {@link String}.
     * @return The {link Tender} instance with it's appLbl fields set.
     */
    public Tender appLbl (String appLbl) {
        this.appLbl = appLbl;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's aid fields set.</p>
     *
     * @param aid EMV aid {@link String}.
     * @return The {link Tender} instance with it's aid fields set.
     */
    public Tender aid (String aid) {
        this.aid = aid;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's tvr fields set.</p>
     *
     * @param tvr EMV tvr {@link String}.
     * @return The {link Tender} instance with it's tvr fields set.
     */
    public Tender tvr (String tvr) {
        this.tvr = tvr;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's iad fields set.</p>
     *
     * @param iad EMV iad {@link String}.
     * @return The {link Tender} instance with it's iad fields set.
     */
    public Tender iad (String iad) {
        this.iad = iad;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's tsi fields set.</p>
     *
     * @param tsi EMV tsi {@link String}.
     * @return The {link Tender} instance with it's tsi fields set.
     */
    public Tender tsi (String tsi) {
        this.tsi = tsi;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's arc fields set.</p>
     *
     * @param arc EMV arc {@link String}.
     * @return The {link Tender} instance with it's arc fields set.
     */
    public Tender arc (String arc) {
        this.arc = arc;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's cvm fields set.</p>
     *
     * @param cvm EMV cvm {@link String}.
     * @return The {link Tender} instance with it's cvm fields set.
     */
    public Tender cvm (String cvm) {
        this.cvm = cvm;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's cardholderAgreement fields set.</p>
     *
     * @param cardholderAgreement EMV cardholderAgreement {@link String}.
     * @return The {link Tender} instance with it's cardholderAgreement fields set.
     */
    public Tender cardholderAgreement (String cardholderAgreement) {
        this.cardholderAgreement = cardholderAgreement;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctName fields set.</p>
     *
     * @param acctName QC account name {@link String}.
     * @return The {link Tender} instance with it's acctName fields set.
     */
    public Tender acctName (String acctName) {
        this.acctName = acctName;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctBadgeAssignmentID fields set.</p>
     *
     * @param acctBadgeAssignmentID Badge assignment ID for the QC account.
     * @return The {link Tender} instance with it's acctBadgeAssignmentID fields set.
     */
    public Tender acctBadgeAssignmentID (int acctBadgeAssignmentID) {
        this.acctBadgeAssignmentID = acctBadgeAssignmentID;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctBadgeNumber fields set.</p>
     *
     * @param acctBadgeNumber Badge number {@link String} for the QC account.
     * @return The {link Tender} instance with it's acctBadgeNumber fields set.
     */
    public Tender acctBadgeNumber (String acctBadgeNumber) {
        this.acctBadgeNumber = acctBadgeNumber;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctEmployeeID fields set.</p>
     *
     * @param acctEmployeeID Account number ID {@link String} for the QC account.
     * @return The {link Tender} instance with it's acctEmployeeID fields set.
     */
    public Tender acctEmployeeID (String acctEmployeeID) {
        this.acctEmployeeID = acctEmployeeID;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctEmployeeNumber fields set.</p>
     *
     * @param acctEmployeeNumber Employee number {@link String} for the QC account.
     * @return The {link Tender} instance with it's acctEmployeeNumber fields set.
     */
    public Tender acctEmployeeNumber (String acctEmployeeNumber) {
        this.acctEmployeeNumber = acctEmployeeNumber;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctNumSplit fields set.</p>
     *
     * @param acctNumSplit The number of splits for the QC account.
     * @return The {link Tender} instance with it's acctNumSplit fields set.
     */
    public Tender acctNumSplit (int acctNumSplit) {
        this.acctNumSplit = acctNumSplit;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctReceiptBalanceTypeID fields set.</p>
     *
     * @param acctReceiptBalanceTypeID The ID of the receipt balance type for the QC account.
     * @return The {link Tender} instance with it's acctReceiptBalanceTypeID fields set.
     */
    public Tender acctReceiptBalanceTypeID (int acctReceiptBalanceTypeID) {
        this.acctReceiptBalanceTypeID = acctReceiptBalanceTypeID;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctReceiptBalancePromptTxt fields set.</p>
     *
     * @param acctReceiptBalancePromptTxt The receipt balance prompt text {@link String} for the QC account.
     * @return The {link Tender} instance with it's acctReceiptBalancePromptTxt fields set.
     */
    public Tender acctReceiptBalancePromptTxt (String acctReceiptBalancePromptTxt) {
        this.acctReceiptBalancePromptTxt = acctReceiptBalancePromptTxt;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctGlobalBalance fields set.</p>
     *
     * @param acctGlobalBalance The QC account's global balance.
     * @return The {link Tender} instance with it's acctGlobalBalance fields set.
     */
    public Tender acctGlobalBalance (double acctGlobalBalance) {
        this.acctGlobalBalance = acctGlobalBalance;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctGlobalAvailable fields set.</p>
     *
     * @param acctGlobalAvailable The QC account's global available.
     * @return The {link Tender} instance with it's acctGlobalAvailable fields set.
     */
    public Tender acctGlobalAvailable (double acctGlobalAvailable) {
        this.acctGlobalAvailable = acctGlobalAvailable;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctStoreBalance fields set.</p>
     *
     * @param acctStoreBalance The QC account's store balance.
     * @return The {link Tender} instance with it's acctStoreBalance fields set.
     */
    public Tender acctStoreBalance (double acctStoreBalance) {
        this.acctStoreBalance = acctStoreBalance;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctStoreAvailable fields set.</p>
     *
     * @param acctStoreAvailable The QC account's store available.
     * @return The {link Tender} instance with it's acctStoreAvailable fields set.
     */
    public Tender acctStoreAvailable (double acctStoreAvailable) {
        this.acctStoreAvailable = acctStoreAvailable;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's acctDailyLimitBal fields set.</p>
     *
     * @param acctDailyLimitBal The QC account's daily limit balance.
     * @return The {link Tender} instance with it's acctDailyLimitBal fields set.
     */
    public Tender acctDailyLimitBal (double acctDailyLimitBal) {
        this.acctDailyLimitBal = acctDailyLimitBal;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's cardholderName fields set.</p>
     *
     * @param cardholderName The name {@link String} of the cardholder.
     * @return The {link Tender} instance with it's cardholderName fields set.
     */
    public Tender cardholderName (String cardholderName) {
        this.cardholderName = cardholderName;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's paymentGatewayCCDetails fields set.</p>
     *
     * @param paymentGatewayCCDetails A {@link String} containing payment gateway details.
     * @return The {link Tender} instance with it's paymentGatewayCCDetails fields set.
     */
    public Tender paymentGatewayCCDetails (String paymentGatewayCCDetails) {
        this.paymentGatewayCCDetails = paymentGatewayCCDetails;
        return this;
    }

    /**
     * <p>Constructor for a {link Tender} with it's integratedGiftCardBalance fields set.</p>
     *
     * @param integratedGiftCardBalance The balance {@link String} for an integrated gift card.
     * @return The {link Tender} instance with it's integratedGiftCardBalance fields set.
     */
    public Tender integratedGiftCardBalance (String integratedGiftCardBalance) {
        this.integratedGiftCardBalance = integratedGiftCardBalance;
        return this;
    }

    public String getVoucherCode() { return this.voucherCode; }
    public void setVoucherCode(String val) { this.voucherCode = val; }

    //--------------------------------------------------------------------------
    //  Static methods
    //--------------------------------------------------------------------------
    public static Tender createTenderFromDB(HashMap dbData)
    {
        Tender tndr = new Tender();

        try {
            tndr.tenderID = HashMapDataFns.getIntVal(dbData, "PAITEMID");
            tndr.tenderTypeID = HashMapDataFns.getIntVal(dbData, "PATENDERTYPEID");
            tndr.name = HashMapDataFns.getStringVal(dbData, "ITEMNAME");
            tndr.paTransLineItemID = HashMapDataFns.getIntVal(dbData, "PATRANSLINEITEMID");

            tndr.quantity = HashMapDataFns.getDoubleVal(dbData, "QUANTITY");
            tndr.refundedQty = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDQUANTITY");

            tndr.amount = HashMapDataFns.getDoubleVal(dbData, "AMOUNT");
            tndr.refundedAmount = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDAMOUNT");

            tndr.commentPromptText = HashMapDataFns.getStringVal(dbData, "COMMENTPROMPTTEXT");
            tndr.itemComment = HashMapDataFns.getStringVal(dbData, "ITEMCOMMENT");

            tndr.setVoucherCode(HashMapDataFns.getStringVal(dbData, "VOUCHERCODE"));

            //Code to grab donation name in the case of requested receipt being for a dollar donation transaction
            ArrayList<HashMap> donationName = dm.parameterizedExecuteQuery("data.myqc.getDollarDonationNameForReceipt",
                    new Object[]{HashMapDataFns.getIntVal(dbData, "PATRANSACTIONID")},
                    true
            );
            if(donationName.size() > 0 && donationName.get(0) != null){
                tndr.donationItem = Donation.createDonationFromDB(donationName.get(0));
            }


            // Create employee object if there are QC tender details
            String employeeID = HashMapDataFns.getStringVal(dbData, "EMPLOYEEID");
            if (employeeID.equals("") == false)
            {
                int badgeAssignmentID = 0;
                if (HashMapDataFns.getStringVal(dbData, "BADGEASSIGNMENTID").equals("") == false)
                {
                    badgeAssignmentID = HashMapDataFns.getIntVal(dbData, "BADGEASSIGNMENTID");
                }
                int splits = HashMapDataFns.getIntVal(dbData, "SPLITS");

                tndr.createEmpDataObj();
                tndr.emp.setEmployeeID(employeeID);
                tndr.emp.setBadgeAssignmentID(badgeAssignmentID);
                tndr.emp.setNumSplits(splits);

                tndr.acctName = tndr.emp.getName();
                tndr.acctBadgeAssignmentID = tndr.emp.getBadgeAssignmentID();
                tndr.acctBadgeNumber = tndr.emp.getBadgeNumber();
                tndr.acctEmployeeID = tndr.emp.getEmployeeID();
                tndr.acctEmployeeNumber = tndr.emp.getEmployeeNumber();
                tndr.acctNumSplit = tndr.emp.getNumSplits();
                tndr.acctReceiptBalanceTypeID = tndr.emp.getReceiptBalanceTypeID();
                tndr.acctReceiptBalancePromptTxt = tndr.emp.getReceiptBalancePromptTxt();
                tndr.acctGlobalBalance = tndr.emp.getGlobalBalance();
                tndr.acctGlobalAvailable = tndr.emp.getGlobalAvailable();
                tndr.acctStoreBalance = tndr.emp.getStoreBalance();
                tndr.acctStoreAvailable = tndr.emp.getStoreAvailable();
                tndr.acctDailyLimitBal = tndr.emp.getDailyLimitBal();
            }

            // Set credit card details if creditCardTransInfo has been populated
            String creditCardTransInfo = HashMapDataFns.getStringVal(dbData, "CREDITCARDTRANSINFO");

            if (!creditCardTransInfo.isEmpty())
            {
                if (tndr.tenderTypeID == TypeData.TenderType.INTEGRATED_GIFT_CARD)
                {
                    tndr.gcTxnData = new GiftCardTxnData(creditCardTransInfo, ";");
                }
                else
                {
                    tndr.cardTypeName = tndr.getCreditCardDetail("CardType", creditCardTransInfo);
                    tndr.acctNo = tndr.getCreditCardDetail("AcctNo", creditCardTransInfo);
                    tndr.authCode = tndr.getCreditCardDetail("AuthCode", creditCardTransInfo);
                    tndr.printAcctNo = false;
                    if (HashMapDataFns.getBooleanVal(dbData, "PRINTACCTNO")) {
                        tndr.printAcctNo = true;
                    }

                    String merchIdStr = "";
                    merchIdStr = tndr.getCreditCardDetail("MerchID", creditCardTransInfo);
                    if (!merchIdStr.equals("")) {
                        tndr.merchID = merchIdStr;
                    }

                    tndr.cardholderName = tndr.getCreditCardDetail("NameOnCard", creditCardTransInfo);

                    // Add EMV details if needed
                    if (tndr.getCreditCardDetail("CVM", creditCardTransInfo).equals("") == false) {
                        tndr.entryMethod = tndr.getCreditCardDetail("EntryMethod", creditCardTransInfo);
                        tndr.cvm = tndr.getCreditCardDetail("CVM", creditCardTransInfo);
                        tndr.invoiceNo = tndr.getCreditCardDetail("InvoiceNo", creditCardTransInfo);
                        tndr.refNo = tndr.getCreditCardDetail("RefNo", creditCardTransInfo);
                        tndr.appLbl = tndr.getCreditCardDetail("AppLbl", creditCardTransInfo);
                        tndr.aid = tndr.getCreditCardDetail("AID", creditCardTransInfo);
                        tndr.tvr = tndr.getCreditCardDetail("TVR", creditCardTransInfo);
                        tndr.iad = tndr.getCreditCardDetail("IAD", creditCardTransInfo);
                        tndr.tsi = tndr.getCreditCardDetail("TSI", creditCardTransInfo);
                        tndr.arc = tndr.getCreditCardDetail("ARC", creditCardTransInfo);

                        tndr.cardholderAgreement = tndr.setCardholderAgreement();
                    }
                }
            }
        }
        catch (Exception e)
        {
            ReceiptGen.logError("Tender.createTender error", e);
        }

        return tndr;
    }

    public static Tender createTenderFromXMLRPC(ArrayList tenderData, int transTypeID)
    {
        Tender tndr = new Tender();

        try
        {
            tndr.tenderID = new Integer(tenderData.get(20).toString()).intValue();
            tndr.amount = new Double(tenderData.get(1).toString()).doubleValue();
            tndr.name = tenderData.get(2).toString();
            tndr.tenderTypeName = tenderData.get(3).toString();
            tndr.tenderTypeID = new Integer(tenderData.get(25).toString()).intValue();
            tndr.itemComment = tenderData.get(4).toString();
            tndr.acctNo = tenderData.get(5).toString();
            tndr.authCode = tenderData.get(6).toString();
            tndr.merchID = tenderData.get(7).toString();

            tndr.invoiceNo = tenderData.get(9).toString();
            tndr.refNo = tenderData.get(10).toString();
            tndr.entryMethod = tenderData.get(11).toString();
            tndr.appLbl = tenderData.get(12).toString();
            tndr.aid = tenderData.get(13).toString();
            tndr.tvr = tenderData.get(14).toString();
            tndr.iad = tenderData.get(15).toString();
            tndr.tsi = tenderData.get(16).toString();
            tndr.arc = tenderData.get(17).toString();
            tndr.cvm = tenderData.get(18).toString();
            tndr.cardholderAgreement = tenderData.get(19).toString();

            boolean hasAccountNum = (!tndr.acctNo.toString().equals(""));
            if (hasAccountNum)
            {
                tndr.printAcctNo = true;
            }
            else
            {
                tndr.printAcctNo = false;
            }

            if (transTypeID == TypeData.TranType.REFUND || transTypeID == TypeData.TranType.VOID)
            {
                tndr.quantity = -1;
            }
            else
            {
                tndr.quantity = 1;
            }

            tndr.commentPromptText = tenderData.get(22).toString();

            ArrayList<String> rcptAcctDetails = (ArrayList) tenderData.get(8);

            if (rcptAcctDetails.size() > 0)
            {
                tndr.createEmpDataObj();
                tndr.emp.setReceiptBalanceTypeID(new Integer(tenderData.get(23).toString()).intValue());
                tndr.emp.setReceiptBalancePromptTxt(tenderData.get(24).toString());

                for (String acctDetail : rcptAcctDetails)
                {
                    String[] detailSplit = acctDetail.split(":");

                    if (detailSplit.length < 2)
                    {
                        continue;
                    }

                    if (acctDetail.contains("Name:"))
                    {
                        tndr.emp.setName(detailSplit[1]);
                    }
                    else if (acctDetail.contains("[BADGE]"))
                    {
                        tndr.emp.setBadgeNumber(detailSplit[1]);
                    }
                    else if (acctDetail.contains("Acct No:"))
                    {
                        tndr.emp.setEmployeeNumber(detailSplit[1]);
                    }
                    else if (acctDetail.contains("[TERMINALPROFILEBALANCETYPE]"))
                    {
                        double termProfileBalance = new Double(detailSplit[1].toString()).doubleValue();

                        switch (tndr.emp.getReceiptBalanceTypeID())
                        {
                            case 1:
                                tndr.emp.setStoreBalance(termProfileBalance);
                                break;
                            case 2:
                                tndr.emp.setStoreAvailable(termProfileBalance);
                                break;
                            case 3:
                                tndr.emp.setGlobalBalance(termProfileBalance);
                                break;
                            case 4:
                                tndr.emp.setGlobalAvailable(termProfileBalance);
                                break;
                        }
                    }
                    else if (acctDetail.contains("[GLOBALACCTBAL]"))
                    {
                        tndr.emp.setGlobalAvailable(new Double(detailSplit[1].toString()).doubleValue());
                    }
                    else if (acctDetail.contains("[STOREACCTBAL]"))
                    {
                        tndr.emp.setStoreAvailable(new Double(detailSplit[1].toString()).doubleValue());
                    }
                    else if (acctDetail.contains("Daily Limit Balance:"))
                    {
                        tndr.emp.setDailyLimitBal(new Double(detailSplit[1].toString()).doubleValue());
                    }
                }
                tndr.acctName = tndr.emp.getName();
                tndr.acctBadgeAssignmentID = tndr.emp.getBadgeAssignmentID();
                tndr.acctBadgeNumber = tndr.emp.getBadgeNumber();
                tndr.acctEmployeeID = tndr.emp.getEmployeeID();
                tndr.acctEmployeeNumber = tndr.emp.getEmployeeNumber();
                tndr.acctNumSplit = tndr.emp.getNumSplits();
                tndr.acctReceiptBalanceTypeID = tndr.emp.getReceiptBalanceTypeID();
                tndr.acctReceiptBalancePromptTxt = tndr.emp.getReceiptBalancePromptTxt();
                tndr.acctGlobalBalance = tndr.emp.getGlobalBalance();
                tndr.acctGlobalAvailable = tndr.emp.getGlobalAvailable();
                tndr.acctStoreBalance = tndr.emp.getStoreBalance();
                tndr.acctStoreAvailable = tndr.emp.getStoreAvailable();
                tndr.acctDailyLimitBal = tndr.emp.getDailyLimitBal();
            }

            tndr.cardholderName = tenderData.get(26).toString();
            tndr.paymentGatewayCCDetails = tenderData.get(27).toString();
            tndr.integratedGiftCardBalance = tenderData.get(29).toString();
        }
        catch (Exception ex)
        {
            ReceiptGen.logError("Tender.createTenderFromXMLRPC error", ex);
        }

        return tndr;
    }

    /**
     * <p>Builds a {@link Tender} from the given {@link XmlRpcStruct}.</p>
     *
     * @param tenderStruct The {@link XmlRpcStruct} to use to build the {@link Tender}.
     * @return The {@link Tender} instance built form a {@link XmlRpcStruct}.
     */
    public static Tender buildFromXmlRpcStruct (XmlRpcStruct tenderStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(tenderStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to Tender.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new Tender()
                .tenderID(HashMapDataFns.getIntVal(tenderStruct, "TENDERID", true))
                .amount(HashMapDataFns.getDoubleVal(tenderStruct, "AMOUNT", true))
                .name(HashMapDataFns.getStringVal(tenderStruct, "NAME", true))
                .tenderTypeName(HashMapDataFns.getStringVal(tenderStruct, "TENDERTYPENAME", true))
                .tenderTypeID(HashMapDataFns.getIntVal(tenderStruct, "TENDERTYPEID", true))
                .itemComment(HashMapDataFns.getStringVal(tenderStruct, "ITEMCOMMENT", true))
                .acctNo(HashMapDataFns.getStringVal(tenderStruct, "ACCTNO", true))
                .authCode(HashMapDataFns.getStringVal(tenderStruct, "AUTHCODE", true))
                .merchID(HashMapDataFns.getStringVal(tenderStruct, "MERCHID", true))
                .invoiceNo(HashMapDataFns.getStringVal(tenderStruct, "INVOICENO", true))
                .refNo(HashMapDataFns.getStringVal(tenderStruct, "REFNO", true))
                .entryMethod(HashMapDataFns.getStringVal(tenderStruct, "ENTRYMETHOD", true))
                .appLbl(HashMapDataFns.getStringVal(tenderStruct, "APPLBL", true))
                .aid(HashMapDataFns.getStringVal(tenderStruct, "AID", true))
                .tvr(HashMapDataFns.getStringVal(tenderStruct, "TVR", true))
                .iad(HashMapDataFns.getStringVal(tenderStruct, "IAD", true))
                .tsi(HashMapDataFns.getStringVal(tenderStruct, "TSI", true))
                .arc(HashMapDataFns.getStringVal(tenderStruct, "ARC", true))
                .cvm(HashMapDataFns.getStringVal(tenderStruct, "CVM", true))
                .cardholderAgreement(HashMapDataFns.getStringVal(tenderStruct, "CARDHOLDERAGREEMENT", true))
                .acctName(HashMapDataFns.getStringVal(tenderStruct, "ACCTNAME", true))
                .acctBadgeAssignmentID(HashMapDataFns.getIntVal(tenderStruct, "ACCTBADGEASSIGNMENTID", true))
                .acctBadgeNumber(HashMapDataFns.getStringVal(tenderStruct, "ACCTBADGENUMBER", true))
                .acctEmployeeID(HashMapDataFns.getStringVal(tenderStruct, "ACCTEMPLOYEEID", true))
                .acctEmployeeNumber(HashMapDataFns.getStringVal(tenderStruct, "ACCTEMPLOYEENUMBER", true))
                .acctNumSplit(HashMapDataFns.getIntVal(tenderStruct, "ACCTNUMSPLIT", true))
                .acctReceiptBalanceTypeID(HashMapDataFns.getIntVal(tenderStruct, "ACCTRECEIPTBALANCETYPEID", true))
                .acctReceiptBalancePromptTxt(HashMapDataFns.getStringVal(tenderStruct, "ACCTRECEIPTBALANCEPROMPTTXT", true))
                .acctGlobalBalance(HashMapDataFns.getDoubleVal(tenderStruct, "ACCTGLOBALBALANCE", true))
                .acctGlobalAvailable(HashMapDataFns.getDoubleVal(tenderStruct, "ACCTGLOBALAVAILABLE", true))
                .acctStoreBalance(HashMapDataFns.getDoubleVal(tenderStruct, "ACCTSTOREBALANCE", true))
                .acctStoreAvailable(HashMapDataFns.getDoubleVal(tenderStruct, "ACCTSTOREAVAILABLE", true))
                .acctDailyLimitBal(HashMapDataFns.getDoubleVal(tenderStruct, "ACCTDAILYLIMITBAL", true))
                .cardholderName(HashMapDataFns.getStringVal(tenderStruct, "CARDHOLDERNAME", true))
                .paymentGatewayCCDetails(HashMapDataFns.getStringVal(tenderStruct, "PAYMENTGATEWAYCCDETAILS", true))
                .integratedGiftCardBalance(HashMapDataFns.getStringVal(tenderStruct, "INTEGRATEDGIFTCARDBALANCE", true));
    }

    public static Tender createTender(TenderLineItemModel tenderLineItemModel, TransactionModel transactionModel)
    {
        Tender tndr = new Tender();

        try {

            tndr.tenderID = tenderLineItemModel.getTender().getId();
            tndr.tenderTypeID = tenderLineItemModel.getTender().getTenderTypeId();
            tndr.name = tenderLineItemModel.getTender().getName();
            tndr.paTransLineItemID = tenderLineItemModel.getId();
            tndr.quantity = tenderLineItemModel.getQuantity().doubleValue();
            if (tenderLineItemModel.getRefundedQty() != null){
                tndr.refundedQty = tenderLineItemModel.getRefundedQty().doubleValue();
            } else {
                tndr.refundedQty = 0;
            }
            tndr.amount = tenderLineItemModel.getAmount().doubleValue();
            if (tenderLineItemModel.getRefundedAmount() != null) {
                tndr.refundedAmount = tenderLineItemModel.getRefundedAmount().doubleValue();
            } else {
                tndr.refundedAmount = 0;
            }
            tndr.commentPromptText = tenderLineItemModel.getTender().getBadgeNumPromptText();
            tndr.itemComment = tenderLineItemModel.getItemComment();
            // Create employee object if there are QC tender details
            if (tenderLineItemModel.getEmployeeId() != null && !tenderLineItemModel.getEmployeeId().toString().isEmpty())
            {
                int badgeAssignmentID = 0;
                if (tenderLineItemModel.getBadgeAssignmentId() != null && !tenderLineItemModel.getBadgeAssignmentId().toString().isEmpty() ){
                    badgeAssignmentID = tenderLineItemModel.getBadgeAssignmentId();
                }
                int splits = 1;
                if (tenderLineItemModel.getAccount() != null && tenderLineItemModel.getAccount().getEligiblePayments() != null && !tenderLineItemModel.getAccount().getEligiblePayments().equals(0)){
                    splits = tenderLineItemModel.getAccount().getEligiblePayments();
                }

                tndr.createEmpDataObj();
                tndr.emp.setEmployeeID(tenderLineItemModel.getEmployeeId().toString());
                tndr.emp.setBadgeAssignmentID(badgeAssignmentID);
                tndr.emp.setNumSplits(splits);

                if (tenderLineItemModel.getAccount() != null) {
                    tndr.emp.setName(ESAPI.encoder().decodeForHTML(tenderLineItemModel.getAccount().getName()));
                    tndr.emp.setBadgeNumber(tenderLineItemModel.getAccount().getBadge());
                    tndr.emp.setEmployeeNumber(tenderLineItemModel.getAccount().getNumber());
                }
                tndr.acctName = tndr.emp.getName();
                tndr.acctBadgeAssignmentID = tndr.emp.getBadgeAssignmentID();
                tndr.acctBadgeNumber = tndr.emp.getBadgeNumber();
                tndr.acctEmployeeID = tndr.emp.getEmployeeID();
                tndr.acctEmployeeNumber = tndr.emp.getEmployeeNumber();
                tndr.acctNumSplit = tndr.emp.getNumSplits();
                tndr.acctReceiptBalanceTypeID = tndr.emp.getReceiptBalanceTypeID();
                tndr.acctReceiptBalancePromptTxt = tndr.emp.getReceiptBalancePromptTxt();
                tndr.acctGlobalBalance = tndr.emp.getGlobalBalance();
                tndr.acctGlobalAvailable = tndr.emp.getGlobalAvailable();
                tndr.acctStoreBalance = tndr.emp.getStoreBalance();
                tndr.acctStoreAvailable = tndr.emp.getStoreAvailable();
                tndr.acctDailyLimitBal = tndr.emp.getDailyLimitBal();
            }

            // Set credit card details if creditCardTransInfo has been populated
            String creditCardTransInfo = tenderLineItemModel.getCreditCardTransInfo();

            if (creditCardTransInfo.equals("") == false)
            {
                tndr.cardTypeName = tndr.getCreditCardDetail("CardType", creditCardTransInfo);
                tndr.acctNo = tndr.getCreditCardDetail("AcctNo", creditCardTransInfo);
                tndr.authCode = tndr.getCreditCardDetail("AuthCode", creditCardTransInfo);

                if (tndr.cardTypeName != null && !tndr.cardTypeName.isEmpty()){
                    tndr.name = tndr.cardTypeName + " " + tndr.name;
                }
                if (tenderLineItemModel.getTender().isPrintAccountLastFour()) {
                    tndr.printAcctNo = true;
                }

                String merchIdStr = "";
                merchIdStr = tndr.getCreditCardDetail("MerchID", creditCardTransInfo);
                if (!merchIdStr.equals(""))
                {
                    tndr.merchID = merchIdStr;
                }

                tndr.cardholderName = tndr.getCreditCardDetail("NameOnCard", creditCardTransInfo);

                // Add EMV details if needed
                if (tndr.getCreditCardDetail("CVM", creditCardTransInfo).equals("") == false)
                {
                    tndr.entryMethod = tndr.getCreditCardDetail("EntryMethod", creditCardTransInfo);
                    tndr.cvm = tndr.getCreditCardDetail("CVM", creditCardTransInfo);
                    tndr.invoiceNo = tndr.getCreditCardDetail("InvoiceNo", creditCardTransInfo);
                    tndr.refNo = tndr.getCreditCardDetail("RefNo", creditCardTransInfo);
                    tndr.appLbl = tndr.getCreditCardDetail("AppLbl", creditCardTransInfo);
                    tndr.aid = tndr.getCreditCardDetail("AID", creditCardTransInfo);
                    tndr.tvr = tndr.getCreditCardDetail("TVR", creditCardTransInfo);
                    tndr.iad = tndr.getCreditCardDetail("IAD", creditCardTransInfo);
                    tndr.tsi = tndr.getCreditCardDetail("TSI", creditCardTransInfo);
                    tndr.arc = tndr.getCreditCardDetail("ARC", creditCardTransInfo);

                    tndr.cardholderAgreement = tndr.setCardholderAgreement();
                }
            }
        }
        catch (Exception e)
        {
            ReceiptGen.logError("Tender.createTender(tenderLineItemModel) error", e);
        }

        return tndr;
    }

    public boolean isIntegratedGiftCard()
    {
        boolean isGiftCard = false;

        if (tenderTypeID == TypeData.TenderType.INTEGRATED_GIFT_CARD)
        {
            isGiftCard = true;
        }

        return isGiftCard;
    }

    public String getNameForReceipt()
    {
        String receiptName = "";

        try {
            receiptName = name;
            if (isIntegratedGiftCard())
            {
                String cardNumLastFour = getGiftCardCardNumLastFour();
                if (!cardNumLastFour.isEmpty())
                {
                    receiptName = receiptName + " (" + cardNumLastFour + ")";
                }
            }

            //Add the voucher code if one exists
            if (!voucherCode.equals("")) { receiptName += " (" + voucherCode + ")"; }

        } catch (Exception error) {
            ReceiptGen.logError("Tender.getNameForReceipt error", error);
        }

        return receiptName;
    }

    public String getGiftCardCardNumLastFour()
    {
        String giftCardCardNumLastFour = "";

        try
        {
            if (gcTxnData != null)
            {
                giftCardCardNumLastFour = gcTxnData.getDetailValue(DatawireConstants.DatawireDetailPrefix.CARD_NUM.getPrefixName());
                if (giftCardCardNumLastFour.length() == 16)
                {
                    giftCardCardNumLastFour = giftCardCardNumLastFour.substring(12, 16);
                }
            }
        } catch (Exception error) {
            ReceiptGen.logError("Tender.getGiftCardCardNumLastFour error", error);
        }

        return giftCardCardNumLastFour;
    }

    public void createEmpDataObj()
    {
        emp = new EmployeeData();
    }

    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public int getTenderID()
    {
        return tenderID;
    }

    public int getTenderTypeID()
    {
        return tenderTypeID;
    }

    public String getTenderComment()
    {
        return itemComment;
    }

    public String getCommentPromptText()
    {
        return commentPromptText;
    }

    public EmployeeData getEmployeeData()
    {
        return emp;
    }

    // Takes creditCardTransInfo from QC_PATransLineItem and returns the value of the
    // specified detail name (creditCardTransInfo is semi-colon delimited)
    public String getCreditCardDetail(String detailName, String creditCardTransInfo)
    {
        String detail = "";
        String[] ccDetails = creditCardTransInfo.split(";");

        for (int x = 0; x < ccDetails.length; x++)
        {
            String[] fullDetail = ccDetails[x].split("=");

            if (fullDetail[0].toString().equals(detailName) == true)
            {
                detail = fullDetail[1].toString();
            }
        }

        return detail;
    }

    public String getCardholderAgreement()
    {
        return cardholderAgreement;
    }

    public String setCardholderAgreement()
    {
        String cardholderAgreement = "";

        if (entryMethod.equals("") == false)
        {
            if (cvm.equals("PIN VERIFIED") && entryMethod.equals("CHIP"))
            {
                cardholderAgreement = "BY ENTERING A VERIFIED PIN, CARDHOLDER AGREES TO PAY ISSUER SUCH TOTAL IN ACCORDANCE WITH ISSUER'S AGREEMENT WITH CARDHOLDER";
            }
            else
            {
                cardholderAgreement = "I AGREE TO PAY THE ABOVE TOTAL AMOUNT ACCORDING TO CARD ISSUER AGREEMENT (MERCHANT AGREEMENT IF CREDIT VOUCHER)";
            }
        }

        return cardholderAgreement;
    }

    public String getCVM()
    {
        return cvm;
    }

    public String getAcctNo()
    {
        return acctNo;
    }

    public String getAuthCode()
    {

        return authCode;
    }

    public String getMerchID()
    {
        return merchID;
    }

    public String getInvoiceNo()
    {
        return invoiceNo;
    }

    public String getRefNo()
    {
        return refNo;
    }

    public String getEntryMethod()
    {
        return entryMethod;
    }

    public Boolean getPrintAccountNo()
    {
        return printAcctNo;
    }
    public String getAppLbl()
    {
        return appLbl;
    }

    public String getAID()
    {
        return aid;
    }

    public String getTVR()
    {
        return tvr;
    }

    public String getIAD()
    {
        return iad;
    }

    public String getTSI()
    {
        return tsi;
    }

    public String getARC()
    {
        return arc;
    }

    public String getCardholderName()
    {
        return cardholderName;
    }

    public String getPaymentGatewayCCDetails()
    {
        return paymentGatewayCCDetails;
    }

    public Donation getDonationItem(){
        return donationItem;
    }

    public String getIntegratedGiftCardBalance()
    {
        return integratedGiftCardBalance;
    }

    public void setIntegratedGiftCardBalance(String integratedGiftCardBalance)
    {
        this.integratedGiftCardBalance = integratedGiftCardBalance;
    }

    /**
     * <p>Overridden toString() method for a {@link Tender} instance.</p>
     *
     * @return The {@link Tender} instance as a {@link String};
     */
    @Override
    public String toString () {

        return String.format("TENDERID: %s, AMOUNT: %s, NAME: %s, TENDERTYPENAME: %s, TENDERTYPEID: %s, ITEMCOMMENT: %s, ACCTNO: %s, AUTHCODE: %s, " +
                "MERCHID: %s, INVOICENO: %s, REFNO: %s, ENTRYMETHOD: %s, APPLBL: %s, AID: %s, TVR: %s, IAD: %s, TSI: %s, ARC: %s, CVM: %s, CARDHOLDERAGREEMENT: %s, " +
                "ACCTNAME: %s, ACCTBADGEASSIGNMENTID: %s, ACCTBADGENUMBER: %s, ACCTEMPLOYEEID: %s, ACCTEMPLOYEENUMBER: %s, ACCTNUMSPLIT: %s, ACCTRECEIPTBALANCETYPEID: %s, " +
                "ACCTRECEIPTBALANCEPROMPTTXT: %s, ACCTGLOBALBALANCE: %s, ACCTGLOBALAVAILABLE: %s, ACCTSTOREBALANCE: %s, ACCTSTOREAVAILABLE: %s, ACCTDAILYLIMITBAL: %s, " +
                "CARDHOLDERNAME: %s, PAYMENTGATEWAYCCDETAILS: %s, INTEGRATEDGIFTCARDBALANCE: %s",
                Objects.toString((tenderID > 0 ? tenderID : "N/A"), "N/A"),
                Objects.toString((amount > 0.0d ? amount : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(tenderTypeName) ? tenderTypeName : "N/A"), "N/A"),
                Objects.toString((tenderTypeID > 0 ? tenderTypeID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(itemComment) ? itemComment : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(acctNo) ? acctNo : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(authCode) ? authCode : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(merchID) ? merchID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(invoiceNo) ? invoiceNo : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(refNo) ? refNo : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(entryMethod) ? entryMethod : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(appLbl) ? appLbl : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(aid) ? aid : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(tvr) ? tvr : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(iad) ? iad : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(tsi) ? tsi : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(arc) ? arc : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(cvm) ? cvm : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(cardholderAgreement) ? cardholderAgreement : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(acctName) ? acctName : "N/A"), "N/A"),
                Objects.toString((acctBadgeAssignmentID > 0 ? acctBadgeAssignmentID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(acctBadgeNumber) ? acctBadgeNumber : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(acctEmployeeID) ? acctEmployeeID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(acctEmployeeNumber) ? acctEmployeeNumber : "N/A"), "N/A"),
                Objects.toString((acctNumSplit > 0 ? acctNumSplit : "N/A"), "N/A"),
                Objects.toString((acctReceiptBalanceTypeID > 0 ? acctReceiptBalanceTypeID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(acctReceiptBalancePromptTxt) ? acctReceiptBalancePromptTxt : "N/A"), "N/A"),
                Objects.toString((acctGlobalBalance > 0.0d ? acctGlobalBalance : "N/A"), "N/A"),
                Objects.toString((acctGlobalAvailable > 0.0d ? acctGlobalAvailable : "N/A"), "N/A"),
                Objects.toString((acctStoreBalance > 0.0d ? acctStoreBalance : "N/A"), "N/A"),
                Objects.toString((acctStoreAvailable > 0.0d ? acctStoreAvailable : "N/A"), "N/A"),
                Objects.toString((acctDailyLimitBal > 0.0d ? acctDailyLimitBal : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(cardholderName) ? cardholderName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(paymentGatewayCCDetails) ? paymentGatewayCCDetails : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(integratedGiftCardBalance) ? integratedGiftCardBalance : "N/A"), "N/A"));
    }

    /**
     * <p>Overridden equals() method for a {@link Tender} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link Tender} instance.
     * @return Whether or not the {@link Object} is equal to the {@link Tender} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a Tender
        Tender tender = (Tender) obj;
        return Objects.equals(tender.tenderID, tenderID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link Tender} instance.</p>
     *
     * @return The hash code for a {@link Tender} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(tenderID);
    }
}
