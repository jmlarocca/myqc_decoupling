package com.mmhayes.common.receiptGen.TransactionData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-24 08:49:40 -0400 (Thu, 24 Sep 2020) $: Date of last commit
    $Rev: 12739 $: Revision of last commit
    Notes: Represents a LoyaltyReward transaction line item.
*/

import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.product.models.ItemRewardModel;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.LoyaltyRewardLineItemModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by nyu on 10/17/2016.
 */
public class LoyaltyReward extends TransactionItem
{
    int itemTypeID = TypeData.ItemType.LOYALTY_REWARD;
    int rewardID;
    String shortName = "";

    int rewardTypeID = 0;
    int pointsToRedeem = 0;

    boolean applyToAllPLUs = false;

    /**
     * <p>Empty constructor for a {@link LoyaltyReward}.</p>
     *
     */
    public LoyaltyReward () {}

    /**
     * <p>Constructor for a {@link LoyaltyReward} with it's rewardID field set.</p>
     *
     * @param rewardID The ID of the loyalty reward.
     * @return The {@link LoyaltyReward} instance with it's rewardID field set.
     */
    public LoyaltyReward rewardID (int rewardID) {
        this.rewardID = rewardID;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyReward} with it's name field set.</p>
     *
     * @param name The name {@link String} of the loyalty reward.
     * @return The {@link LoyaltyReward} instance with it's name field set.
     */
    public LoyaltyReward name (String name) {
        this.name = name;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyReward} with it's shortName field set.</p>
     *
     * @param shortName The short name {@link String} of the loyalty reward.
     * @return The {@link LoyaltyReward} instance with it's shortName field set.
     */
    public LoyaltyReward shortName (String shortName) {
        this.shortName = shortName;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyReward} with it's pointsToRedeem field set.</p>
     *
     * @param pointsToRedeem The number of points to redeem for the loyalty program.
     * @return The {@link LoyaltyReward} instance with it's pointsToRedeem field set.
     */
    public LoyaltyReward pointsToRedeem (int pointsToRedeem) {
        this.pointsToRedeem = pointsToRedeem;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyReward} with it's rewardTypeID field set.</p>
     *
     * @param rewardTypeID The ID corresponding to the loyalty reward type.
     * @return The {@link LoyaltyReward} instance with it's rewardTypeID field set.
     */
    public LoyaltyReward rewardTypeID (int rewardTypeID) {
        this.rewardTypeID = rewardTypeID;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyReward} with it's amount field set.</p>
     *
     * @param amount The amount of the transaction line item.
     * @return The {@link LoyaltyReward} instance with it's amount field set.
     */
    public LoyaltyReward amount (double amount) {
        this.amount = amount;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyReward} with it's quantity field set.</p>
     *
     * @param quantity The quantity of the transaction line item.
     * @return The {@link LoyaltyReward} instance with it's quantity field set.
     */
    public LoyaltyReward quantity (double quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyReward} with it's applyToAllPLUs field set.</p>
     *
     * @param applyToAllPLUs Whether or not the loyalty reward should be applied to all products within the transaction.
     * @return The {@link LoyaltyReward} instance with it's applyToAllPLUs field set.
     */
    public LoyaltyReward applyToAllPLUs (boolean applyToAllPLUs) {
        this.applyToAllPLUs = applyToAllPLUs;
        return this;
    }

    public static LoyaltyReward createRewardFromXMLRPC(ArrayList rewardData, int transTypeID)
    {
        LoyaltyReward reward = new LoyaltyReward();

        try
        {
            reward.rewardID = new Integer(rewardData.get(2).toString()).intValue();
            reward.name = rewardData.get(3).toString();
            reward.shortName = rewardData.get(4).toString();
            reward.pointsToRedeem = new Integer(rewardData.get(5).toString()).intValue();
            reward.rewardTypeID = new Integer(rewardData.get(1).toString()).intValue();
            reward.amount = new BigDecimal(rewardData.get(6).toString()).doubleValue();
            reward.quantity = new BigDecimal(rewardData.get(7).toString()).intValue();
        }
        catch (Exception e)
        {
            ReceiptGen.logError("LoyaltyReward.createRewardFromXMLRPC error", e);
        }

        return reward;
    }

    /**
     * <p>Builds a {@link LoyaltyReward} from the given {@link XmlRpcStruct}.</p>
     *
     * @param loyaltyRewardStruct The {@link XmlRpcStruct} to use to build the {@link LoyaltyReward}.
     * @return The {@link LoyaltyReward} instance built form a {@link XmlRpcStruct}.
     */
    public static LoyaltyReward buildFromXmlRpcStruct (XmlRpcStruct loyaltyRewardStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(loyaltyRewardStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to LoyaltyReward.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new LoyaltyReward()
                .rewardID(HashMapDataFns.getIntVal(loyaltyRewardStruct, "REWARDID", true))
                .name(HashMapDataFns.getStringVal(loyaltyRewardStruct, "NAME", true))
                .shortName(HashMapDataFns.getStringVal(loyaltyRewardStruct, "SHORTNAME", true))
                .pointsToRedeem(HashMapDataFns.getIntVal(loyaltyRewardStruct, "POINTSTOREDEEM", true))
                .rewardTypeID(HashMapDataFns.getIntVal(loyaltyRewardStruct, "REWARDTYPEID", true))
                .amount(HashMapDataFns.getDoubleVal(loyaltyRewardStruct, "AMOUNT", true))
                .quantity(HashMapDataFns.getDoubleVal(loyaltyRewardStruct, "QUANTITY", true))
                .applyToAllPLUs(HashMapDataFns.getBooleanVal(loyaltyRewardStruct, "APPLYTOALLPLUS", true));
    }

    public static LoyaltyReward createRewardFromArrayList(ArrayList rewardData)
    {
        LoyaltyReward reward = new LoyaltyReward();

        try
        {
            // [0] = Item type ID
            //reward.xx  = rewardData.get(0);
            // [1] = Reward type ID
            reward.rewardTypeID = ((Integer)rewardData.get(1));
            // [2] = Reward ID
            reward.rewardID = ((Integer)rewardData.get(2));
            // [3] = Reward name
            reward.name = rewardData.get(3).toString();
            // [4] = Short name
            reward.shortName = rewardData.get(4).toString();
            // [5] = Number of points used to redeem this reward
            //reward.xx = rewardData.get(5).toString();
            // [6] = Reward dollar amount
            reward.amount = new Double(rewardData.get(6).toString()).doubleValue();
            // [7] = Reward quantity (this is more for Free Prod rewards)
            reward.quantity = ((Integer)rewardData.get(7));
        }
        catch (Exception e)
        {
            ReceiptGen.logError("LoyaltyReward.createRewardFromXMLRPC error", e);
        }

        return reward;
    }

    public static LoyaltyReward createRewardFromDB(HashMap dbData)
    {
        LoyaltyReward reward = new LoyaltyReward();

        try
        {
            reward.rewardID = HashMapDataFns.getIntVal(dbData, "PAITEMID");
            reward.name = HashMapDataFns.getStringVal(dbData, "ITEMNAME");
            reward.shortName = HashMapDataFns.getStringVal(dbData, "SHORTNAME");
            reward.paTransLineItemID = HashMapDataFns.getIntVal(dbData, "PATRANSLINEITEMID");

            reward.quantity = HashMapDataFns.getDoubleVal(dbData, "QUANTITY");
            reward.refundedQty = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDQUANTITY");

            reward.amount = HashMapDataFns.getDoubleVal(dbData, "AMOUNT");
            reward.refundedAmount = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDAMOUNT");

            reward.rewardTypeID = HashMapDataFns.getIntVal(dbData, "LOYALTYREWARDTYPEID");
            reward.applyToAllPLUs = HashMapDataFns.getBooleanVal(dbData, "APPLYTOALLPLUS");
        }
        catch (Exception e)
        {
            ReceiptGen.logError("ReceivedOnAcct.createRAItemFromDB error", e);
        }

        return reward;
    }

    public static LoyaltyReward createReward(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel)
    {
        LoyaltyReward reward = new LoyaltyReward();

        try
        {
            reward.rewardID = loyaltyRewardLineItemModel.getReward().getId();
            reward.name = loyaltyRewardLineItemModel.getReward().getName();
            reward.shortName = loyaltyRewardLineItemModel.getReward().getShortName();
            reward.paTransLineItemID = loyaltyRewardLineItemModel.getId();
            reward.quantity = loyaltyRewardLineItemModel.getQuantity().doubleValue();
            if (loyaltyRewardLineItemModel.getRefundedQty() != null){
                reward.refundedQty = loyaltyRewardLineItemModel.getRefundedQty().doubleValue();
            } else {
                reward.refundedQty = 0;
            }
            reward.amount = loyaltyRewardLineItemModel.getAmount().doubleValue();
            if (loyaltyRewardLineItemModel.getRefundedAmount() != null) {
                reward.refundedAmount = loyaltyRewardLineItemModel.getRefundedAmount().doubleValue();
            } else {
                reward.refundedAmount = 0;
            }
            reward.rewardTypeID = loyaltyRewardLineItemModel.getReward().getRewardTypeId();
            reward.applyToAllPLUs = loyaltyRewardLineItemModel.getReward().isApplyToAllPlus();
        }
        catch (Exception e)
        {
            ReceiptGen.logError("ReceivedOnAcct.createReward(loyaltyRewardLineItemModel) error", e);
        }

        return reward;
    }

    /**
     * TODO: Need to confirm that this method should take in an ItemRewardModel?
     * @param itemRewardModel
     * @return
     */
    public static LoyaltyReward createReward(ItemRewardModel itemRewardModel)
    {
        LoyaltyReward reward = new LoyaltyReward();

        try
        {
            reward.rewardID = itemRewardModel.getReward().getId();
            reward.name = itemRewardModel.getReward().getName();
            reward.shortName = itemRewardModel.getReward().getShortName();
            reward.paTransLineItemID = itemRewardModel.getTransLineItemId();

            //TODO: This field doesn't exist in the POS API
            reward.quantity = new Double(1);

            //TODO: confirm this
            reward.refundedQty = new Double(0);
            reward.amount = new Double(itemRewardModel.getAmount().toString());
            if (itemRewardModel.getRefundedAmount() != null) {
                reward.refundedAmount = new Double(itemRewardModel.getRefundedAmount().toString());
            } else {
                reward.refundedAmount = 0;
            }
            reward.rewardTypeID = itemRewardModel.getReward().getRewardTypeId();
            reward.applyToAllPLUs = itemRewardModel.getReward().isApplyToAllPlus();
        }
        catch (Exception e)
        {
            ReceiptGen.logError("ReceivedOnAcct.createReward(itemRewardModel) error", e);
        }

        return reward;
    }

    public boolean isFreeProductReward()
    {
        boolean result = false;

        if (rewardTypeID == TypeData.LoyaltyRewardType.FREE_PRODUCT)
        {
            result = true;
        }

        return result;
    }

    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public String getRewardName()
    {
        return name;
    }

    public String getShortName()
    {
        return shortName;
    }

    public int getRewardTypeID()
    {
        return rewardTypeID;
    }

    public int getRewardID()
    {
        return rewardID;
    }

    public boolean getApplyToAllPLUs() {
        return applyToAllPLUs;
    }

    public void setApplyToAllPLUs(boolean applyToAllPLUs) {
        this.applyToAllPLUs = applyToAllPLUs;
    }

    /**
     * <p>Getter for the pointsToRedeem field of the {@link LoyaltyReward}.</p>
     *
     * @return The pointsToRedeem field of the {@link LoyaltyReward}.
     */
    public int getPointsToRedeem () {
        return pointsToRedeem;
    }

    /**
     * <p>Overridden toString() method for a {@link LoyaltyReward} instance.</p>
     *
     * @return The {@link LoyaltyReward} instance as a {@link String};
     */
    @Override
    public String toString () {

        return String.format("REWARDID: %s, NAME: %s, SHORTNAME: %s, POINTSTOREDEEM: %s, REWARDTYPEID: %s, AMOUNT: %s, QUANTITY: %s, APPLYTOALLPLUS: %s",
                Objects.toString((rewardID > 0 ? rewardID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((pointsToRedeem > 0 ? pointsToRedeem : "N/A"), "N/A"),
                Objects.toString((rewardTypeID > 0 ? rewardTypeID : "N/A"), "N/A"),
                Objects.toString((amount > 0.0d ? amount : "N/A"), "N/A"),
                Objects.toString((quantity > 0.0d ? quantity : "N/A"), "N/A"),
                Objects.toString(applyToAllPLUs, "N/A"));
    }

    /**
     * <p>Overridden equals() method for a {@link LoyaltyReward} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link LoyaltyReward} instance.
     * @return Whether or not the {@link Object} is equal to the {@link LoyaltyReward} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a LoyaltyReward
        LoyaltyReward loyaltyReward = (LoyaltyReward) obj;
        return Objects.equals(loyaltyReward.rewardID, rewardID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link LoyaltyReward} instance.</p>
     *
     * @return The hash code for a {@link LoyaltyReward} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(rewardID);
    }
}
