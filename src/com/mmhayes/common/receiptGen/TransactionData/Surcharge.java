package com.mmhayes.common.receiptGen.TransactionData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-21 11:55:42 -0400 (Mon, 21 Sep 2020) $: Date of last commit
    $Rev: 12711 $: Revision of last commit
    Notes: Represents a Surcharge transaction line item.
*/

import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.product.models.ItemSurchargeModel;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.ProductLineItemModel;
import com.mmhayes.common.transaction.models.SurchargeLineItemModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by nyu on 4/26/2019.
 */
public class Surcharge extends TransactionItem
{
    // Identifying attributes
    int itemTypeID = TypeData.ItemType.SURCHARGE;
    int surchargeID;
    int applicationTypeID;
    String shortName = "";

    /**
     * <p>Empty constructor for a {@link Surcharge}.</p>
     *
     */
    public Surcharge () {}

    /**
     * <p>Constructor for a {@link Surcharge} with it's surchargeID field set.</p>
     *
     * @param surchargeID The ID of the surcharge.
     * @return The {@link Surcharge} instance with it's surchargeID field set.
     */
    public Surcharge surchargeID (int surchargeID) {
        this.surchargeID = surchargeID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Surcharge} with it's paTransLineItemID field set.</p>
     *
     * @param paTransLineItemID Transaction line item ID of the surcharge line item.
     * @return The {@link Surcharge} instance with it's paTransLineItemID field set.
     */
    public Surcharge paTransLineItemID (int paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Surcharge} with it's name field set.</p>
     *
     * @param name The name {@link String} of the surcharge.
     * @return The {@link Surcharge} instance with it's name field set.
     */
    public Surcharge name (String name) {
        this.name = name;
        return this;
    }

    /**
     * <p>Constructor for a {@link Surcharge} with it's shortName field set.</p>
     *
     * @param shortName The short name {@link String} of the surcharge.
     * @return The {@link Surcharge} instance with it's shortName field set.
     */
    public Surcharge shortName (String shortName) {
        this.shortName = shortName;
        return this;
    }

    /**
     * <p>Constructor for a {@link Surcharge} with it's applicationTypeID field set.</p>
     *
     * @param applicationTypeID ID corresponding to the application type.
     * @return The {@link Surcharge} instance with it's applicationTypeID field set.
     */
    public Surcharge applicationTypeID (int applicationTypeID) {
        this.applicationTypeID = applicationTypeID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Surcharge} with it's quantity field set.</p>
     *
     * @param quantity The quantity of the transaction line item.
     * @return The {@link Surcharge} instance with it's quantity field set.
     */
    public Surcharge quantity (double quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * <p>Constructor for a {@link Surcharge} with it's refundedQty field set.</p>
     *
     * @param refundedQty The refunded quantity of the transaction line item.
     * @return The {@link Surcharge} instance with it's refundedQty field set.
     */
    public Surcharge refundedQty (double refundedQty) {
        this.refundedQty = refundedQty;
        return this;
    }

    /**
     * <p>Constructor for a {@link Surcharge} with it's amount field set.</p>
     *
     * @param amount The amount of the transaction line item.
     * @return The {@link Surcharge} instance with it's amount field set.
     */
    public Surcharge amount (double amount) {
        this.amount = amount;
        return this;
    }

    /**
     * <p>Constructor for a {@link Surcharge} with it's refundedAmount field set.</p>
     *
     * @param refundedAmount The refunded amount of the transaction line item.
     * @return The {@link Surcharge} instance with it's refundedAmount field set.
     */
    public Surcharge refundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
        return this;
    }

    /**
     * <p>Constructor for a {@link Surcharge} with it's linkedPaTransLineItemID field set.</p>
     *
     * @param linkedPaTransLineItemID The paTransLineItemID linked to this transaction line item.
     * @return The {@link Surcharge} instance with it's linkedPaTransLineItemID field set.
     */
    public Surcharge linkedPaTransLineItemID (int linkedPaTransLineItemID) {
        this.linkedPaTransLineItemID = linkedPaTransLineItemID;
        return this;
    }

    //--------------------------------------------------------------------------
    //  Static methods
    //--------------------------------------------------------------------------
    // Creates a Surcharge object based on information retrieved from the database
    public static Surcharge createSurchargeFromDB(HashMap dbData)
    {
        Surcharge surcharge = new Surcharge();

        surcharge.surchargeID = HashMapDataFns.getIntVal(dbData, "PAITEMID");
        surcharge.paTransLineItemID = HashMapDataFns.getIntVal(dbData, "PATRANSLINEITEMID");
        surcharge.name = HashMapDataFns.getStringVal(dbData, "ITEMNAME");
        surcharge.shortName = HashMapDataFns.getStringVal(dbData, "SHORTNAME");
        surcharge.applicationTypeID = HashMapDataFns.getIntVal(dbData, "PASURCHARGEAPPLICATIONTYPEID");

        surcharge.quantity = HashMapDataFns.getDoubleVal(dbData, "QUANTITY");
        surcharge.refundedQty = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDQUANTITY");

        surcharge.amount = HashMapDataFns.getDoubleVal(dbData, "AMOUNT");
        surcharge.refundedAmount = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDAMOUNT");
        surcharge.linkedPaTransLineItemID = HashMapDataFns.getIntVal(dbData, "LINKEDPATRANSLINEITEMID");

        return surcharge;
    }

    //TODO- Verify the field locations in surchargeData when QCPOS implements surcharges
    public static Surcharge createSurchargeFromXMLRPC(ArrayList surchargeData, int transTypeID)
    {
        Surcharge surcharge = new Surcharge();

        /*try {
            surcharge.surchargeID = new Integer(surchargeData.get(4).toString()).intValue();
            surcharge.name = surchargeData.get(1).toString();
            surcharge.amount = new Double(surchargeData.get(2).toString()).doubleValue();
            surcharge.shortName = surchargeData.get(3).toString();

            if (transTypeID == TypeData.TranType.REFUND || transTypeID == TypeData.TranType.VOID)
            {
                surcharge.quantity = -1;
            }
            else
            {
                surcharge.quantity = 1;
            }
        } catch (Exception e) {
            ReceiptGen.logError("Surcharge.createTaxFromXMLRPC error", e);
        }*/

        return surcharge;
    }

    /**
     * <p>Builds a {@link Surcharge} from the given {@link XmlRpcStruct}.</p>
     *
     * @param surchargeStruct The {@link XmlRpcStruct} to use to build the {@link Surcharge}.
     * @return The {@link Surcharge} instance built form a {@link XmlRpcStruct}.
     */
    public static Surcharge buildFromXmlRpcStruct (XmlRpcStruct surchargeStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(surchargeStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to Surcharge.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new Surcharge()
                .surchargeID(HashMapDataFns.getIntVal(surchargeStruct, "SURCHARGEID", true))
                .paTransLineItemID(HashMapDataFns.getIntVal(surchargeStruct, "PATRANSLINEITEMID", true))
                .name(HashMapDataFns.getStringVal(surchargeStruct, "NAME", true))
                .shortName(HashMapDataFns.getStringVal(surchargeStruct, "SHORTNAME", true))
                .applicationTypeID(HashMapDataFns.getIntVal(surchargeStruct, "APPLICATIONTYPEID", true))
                .quantity(HashMapDataFns.getDoubleVal(surchargeStruct, "QUANTITY", true))
                .refundedQty(HashMapDataFns.getDoubleVal(surchargeStruct, "REFUNDEDQTY", true))
                .amount(HashMapDataFns.getDoubleVal(surchargeStruct, "AMOUNT", true))
                .refundedAmount(HashMapDataFns.getDoubleVal(surchargeStruct, "REFUNDEDAMOUNT", true))
                .linkedPaTransLineItemID(HashMapDataFns.getIntVal(surchargeStruct, "LINKEDPATRANSLINEITEMID", true));
    }

    public static Surcharge createSurcharge(SurchargeLineItemModel surchargeLineItemModel, TransactionModel transactionModel)
    {
        Surcharge surcharge = new Surcharge();

        try {
            surcharge.surchargeID = surchargeLineItemModel.getSurcharge().getId();
            surcharge.name = surchargeLineItemModel.getSurcharge().getName();
            surcharge.amount = new Double(surchargeLineItemModel.getAmount().toString());
            surcharge.shortName = surchargeLineItemModel.getSurcharge().getShortName();
            surcharge.applicationTypeID = surchargeLineItemModel.getSurcharge().getApplicationTypeId();
            surcharge.linkedPaTransLineItemID = (surchargeLineItemModel.getLinkedTransLineItemId() == null ? 0 : surchargeLineItemModel.getLinkedTransLineItemId());
            surcharge.quantity = surchargeLineItemModel.getQuantity().intValue();
        } catch (Exception e) {
            ReceiptGen.logError("Surcharge.createSurcharge error", e);
        }

        return surcharge;
    }

    public static Surcharge createSurcharge(ItemSurchargeModel itemSurchargeModel, ProductLineItemModel productLineItemModel, TransactionModel transactionModel)
    {
        Surcharge surcharge = new Surcharge();

        try {
            surcharge.surchargeID = itemSurchargeModel.getSurcharge().getId();
            surcharge.name = itemSurchargeModel.getSurcharge().getName();
            surcharge.amount = new Double(itemSurchargeModel.getAmount().toString());
            surcharge.shortName = itemSurchargeModel.getSurcharge().getShortName();
            surcharge.applicationTypeID = itemSurchargeModel.getSurcharge().getApplicationTypeId();
            surcharge.quantity = productLineItemModel.getQuantity().intValue(); //set the Item Surcharge quantity to the parent product's quantity
        } catch (Exception e) {
            ReceiptGen.logError("Surcharge.createSurcharge error", e);
        }

        return surcharge;
    }

    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public int getSurchargeID()
    {
        return surchargeID;
    }

    public void setSurchargeID(int surchargeID)
    {
        this.surchargeID = surchargeID;
    }

    public int getApplicationTypeID()
    {
        return applicationTypeID;
    }

    public String getShortName()
    {
        return shortName;
    }

    public boolean isSubtotalSurcharge()
    {
        if (applicationTypeID == TypeData.SurchargeApplicationType.SUBTOTAL)
        {
            return true;
        }
        return false;
    }

    public int getItemTypeID() {
        return itemTypeID;
    }

    public void setItemTypeID(int itemTypeID) {
        this.itemTypeID = itemTypeID;
    }

    public void setApplicationTypeID(int applicationTypeID) {
        this.applicationTypeID = applicationTypeID;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * <p>Overridden toString() method for a {@link Surcharge} instance.</p>
     *
     * @return The {@link Surcharge} instance as a {@link String};
     */
    @Override
    public String toString () {

        return String.format("SURCHARGEID: %s, PATRANSLINEITEMID: %s, NAME: %s, SHORTNAME: %s, APPLICATIONTYPEID: %s, QUANTITY: %s, REFUNDEDQTY: %s, " +
                "AMOUNT: %s, REFUNDEDAMOUNT: %s, LINKEDPATRANSLINEITEMID: %s",
                Objects.toString((surchargeID > 0 ? surchargeID : "N/A"), "N/A"),
                Objects.toString((paTransLineItemID > 0 ? paTransLineItemID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((applicationTypeID > 0 ? applicationTypeID : "N/A"), "N/A"),
                Objects.toString((quantity > 0.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQty > 0.0d ? refundedQty : "N/A"), "N/A"),
                Objects.toString((amount > 0.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount > 0.0d ? refundedAmount : "N/A"), "N/A"),
                Objects.toString((linkedPaTransLineItemID > 0 ? linkedPaTransLineItemID : "N/A"), "N/A"));
    }

    /**
     * <p>Overridden equals() method for a {@link Surcharge} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link Surcharge} instance.
     * @return Whether or not the {@link Object} is equal to the {@link Surcharge} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a Surcharge
        Surcharge surcharge = (Surcharge) obj;
        return Objects.equals(surcharge.surchargeID, surchargeID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link Surcharge} instance.</p>
     *
     * @return The hash code for a {@link Surcharge} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(surchargeID);
    }

}
