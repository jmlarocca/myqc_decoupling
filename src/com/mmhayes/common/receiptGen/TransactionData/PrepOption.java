package com.mmhayes.common.receiptGen.TransactionData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-21 16:43:52 -0400 (Mon, 21 Sep 2020) $: Date of last commit
    $Rev: 12716 $: Revision of last commit
    Notes: Represents a prep option for a product.
*/

import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.product.models.PrepOptionModel;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a prep option for a product.</p>
 *
 */
public class PrepOption {
    // Identifying attributes
    int prepOptionID;
    int prepOptionSetID;
    double prepOptionPrice = 0.0d;

    String name = "";
    String originalName = "";

    boolean displayDefaultReceipts = false;
    boolean displayDefaultKitchen = false;
    boolean defaultOption = false;

    /**
     * <p>Empty constructor for a {@link PrepOption}.</p>
     *
     */
    public PrepOption () {}

    /**
     * <p>Constructor for a {@link PrepOption} with it's prepOptionID field set.</p>
     *
     * @param prepOptionID The ID of the prep option.
     * @return The {@link PrepOption} instance with it's prepOptionID field set.
     */
    public PrepOption prepOptionID (int prepOptionID) {
        this.prepOptionID = prepOptionID;
        return this;
    }

    /**
     * <p>Constructor for a {@link PrepOption} with it's prepOptionSetID field set.</p>
     *
     * @param prepOptionSetID The ID of the set to which the prep option belongs.
     * @return The {@link PrepOption} instance with it's prepOptionSetID field set.
     */
    public PrepOption prepOptionSetID (int prepOptionSetID) {
        this.prepOptionSetID = prepOptionSetID;
        return this;
    }

    /**
     * <p>Constructor for a {@link PrepOption} with it's prepOptionPrice field set.</p>
     *
     * @param prepOptionPrice The price of the prep option.
     * @return The {@link PrepOption} instance with it's prepOptionPrice field set.
     */
    public PrepOption prepOptionPrice (double prepOptionPrice) {
        this.prepOptionPrice = prepOptionPrice;
        return this;
    }

    /**
     * <p>Constructor for a {@link PrepOption} with it's name field set.</p>
     *
     * @param name The name {@link String} of the prep option.
     * @return The {@link PrepOption} instance with it's name field set.
     */
    public PrepOption name (String name) {
        this.name = name;
        return this;
    }

    /**
     * <p>Constructor for a {@link PrepOption} with it's originalName field set.</p>
     *
     * @param originalName The original name {@link String} of the prep option.
     * @return The {@link PrepOption} instance with it's originalName field set.
     */
    public PrepOption originalName (String originalName) {
        this.originalName = originalName;
        return this;
    }

    /**
     * <p>Constructor for a {@link PrepOption} with it's displayDefaultReceipts field set.</p>
     *
     * @param displayDefaultReceipts Whether or not to display the default prep option on receipts.
     * @return The {@link PrepOption} instance with it's displayDefaultReceipts field set.
     */
    public PrepOption displayDefaultReceipts (boolean displayDefaultReceipts) {
        this.displayDefaultReceipts = displayDefaultReceipts;
        return this;
    }

    /**
     * <p>Constructor for a {@link PrepOption} with it's displayDefaultKitchen field set.</p>
     *
     * @param displayDefaultKitchen Whether or not to display the default prep option on kitchen receipts.
     * @return The {@link PrepOption} instance with it's displayDefaultKitchen field set.
     */
    public PrepOption displayDefaultKitchen (boolean displayDefaultKitchen) {
        this.displayDefaultKitchen = displayDefaultKitchen;
        return this;
    }

    /**
     * <p>Constructor for a {@link PrepOption} with it's defaultOption field set.</p>
     *
     * @param defaultOption Wether or not the prep option is the default.
     * @return The {@link PrepOption} instance with it's defaultOption field set.
     */
    public PrepOption defaultOption (boolean defaultOption) {
        this.defaultOption = defaultOption;
        return this;
    }

    /**
     * <p>Constructor for a {@link PrepOption}.</p>
     *
     * @param prepOptHM The {@link HashMap} containing information to build the {@link PrepOption}.
     */
    public PrepOption (HashMap prepOptHM) {

        if (!DataFunctions.isEmptyMap(prepOptHM)) {
            this.prepOptionID = HashMapDataFns.getIntVal(prepOptHM, "PAPREPOPTIONID");
            this.prepOptionSetID = HashMapDataFns.getIntVal(prepOptHM, "PAPREPOPTIONSETID");
            this.prepOptionPrice = HashMapDataFns.getDoubleVal(prepOptHM, "PAPREPOPTIONPRICE");
            this.name = HashMapDataFns.getStringVal(prepOptHM, "PREPNAME");
            this.displayDefaultReceipts = HashMapDataFns.getBooleanVal(prepOptHM, "DISPLAYDEFAULTRECEIPTS");
            this.displayDefaultKitchen = HashMapDataFns.getBooleanVal(prepOptHM, "DISPLAYDEFAULTKITCHEN");
            this.defaultOption = HashMapDataFns.getBooleanVal(prepOptHM, "DEFAULTOPTION");
        }
        else {
            throw new NullPointerException("Unable to build a PrepOption object with an empty HashMap in the PrepOption constructor!");
        }

    }

    //--------------------------------------------------------------------------
    //  Static methods
    //--------------------------------------------------------------------------
    public static PrepOption createPrepOptionFromDB(HashMap dbData)
    {
        PrepOption prepOption = new PrepOption();

        prepOption.prepOptionID = HashMapDataFns.getIntVal(dbData, "PAPREPOPTIONID");
        prepOption.prepOptionSetID = HashMapDataFns.getIntVal(dbData, "PAPREPOPTIONSETID");
        prepOption.prepOptionPrice = HashMapDataFns.getDoubleVal(dbData, "PAPREPOPTIONPRICE");
        prepOption.name = HashMapDataFns.getStringVal(dbData, "PREPNAME");
        prepOption.originalName = HashMapDataFns.getStringVal(dbData, "PREPORIGINALNAME");
        prepOption.displayDefaultReceipts = HashMapDataFns.getBooleanVal(dbData, "DISPLAYDEFAULTRECEIPTS");
        prepOption.displayDefaultKitchen = HashMapDataFns.getBooleanVal(dbData, "DISPLAYDEFAULTKITCHEN");
        prepOption.defaultOption = HashMapDataFns.getBooleanVal(dbData, "DEFAULTOPTION");

        if( prepOption.getPrepOptionID() == -1 || prepOption.getPrepOptionSetID() == -1 ) {
            return null;
        }

        return prepOption;
    }

    public static PrepOption createPrepOptionFromXMLRPC(ArrayList detail)
    {
        PrepOption prepOption = new PrepOption();

        try
        {

            prepOption.prepOptionID = new Integer(detail.get(0).toString()).intValue();
            prepOption.prepOptionSetID = new Integer(detail.get(1).toString()).intValue();
            prepOption.prepOptionPrice = new Double(detail.get(2).toString()).doubleValue();
            prepOption.name = detail.get(3).toString();
            prepOption.displayDefaultReceipts = new Boolean(detail.get(4).toString()).booleanValue();
            prepOption.defaultOption = new Boolean(detail.get(5).toString()).booleanValue();
            prepOption.displayDefaultKitchen = new Boolean(detail.get(6).toString()).booleanValue();
        }
        catch (Exception e)
        {
            ReceiptGen.logError("PrepOption.createPrepOptionFromXMLRPC error", e);
        }

        return prepOption;
    }

    /**
     * <p>Builds a {@link PrepOption} from the given {@link XmlRpcStruct}.</p>
     *
     * @param prepOptionStruct The {@link XmlRpcStruct} to use to build the {@link PrepOption}.
     * @return The {@link PrepOption} instance built form a {@link XmlRpcStruct}.
     */
    public static PrepOption buildFromXmlRpcStruct (XmlRpcStruct prepOptionStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(prepOptionStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to PrepOption.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new PrepOption()
                .prepOptionID(HashMapDataFns.getIntVal(prepOptionStruct, "PREPOPTIONID", true))
                .prepOptionSetID(HashMapDataFns.getIntVal(prepOptionStruct, "PREPOPTIONSETID", true))
                .prepOptionPrice(HashMapDataFns.getDoubleVal(prepOptionStruct, "PREPOPTIONPRICE", true))
                .name(HashMapDataFns.getStringVal(prepOptionStruct, "NAME", true))
                .originalName(HashMapDataFns.getStringVal(prepOptionStruct, "ORIGINALNAME", true))
                .displayDefaultReceipts(HashMapDataFns.getBooleanVal(prepOptionStruct, "DISPLAYDEFAULTRECEIPTS", true))
                .displayDefaultKitchen(HashMapDataFns.getBooleanVal(prepOptionStruct, "DISPLAYDEFAULTKITCHEN", true))
                .defaultOption(HashMapDataFns.getBooleanVal(prepOptionStruct, "DEFAULTOPTION", true));
    }

    public static PrepOption createPrepOption(PrepOptionModel prepOptionModel)
    {
        PrepOption prepOption = new PrepOption();

        try {
            prepOption.prepOptionID = prepOptionModel.getId();
            prepOption.prepOptionSetID = prepOptionModel.getPrepOptionSetId();
            prepOption.prepOptionPrice = (prepOptionModel.getPrice() == null ? new Double(0) : new Double(prepOptionModel.getPrice().toString()));
            prepOption.name = prepOptionModel.getName();
            prepOption.displayDefaultReceipts = prepOptionModel.isDisplayDefaultReceipts();
            prepOption.defaultOption = prepOptionModel.isDefaultOption();

        } catch (Exception e) {
            ReceiptGen.logError("PrepOption.createPrepOption error", e);
        }

        return prepOption;
    }

    /**
     * <p>Overridden toString() method for a {@link PrepOption}.</p>
     *
     * @return A {@link String} representation of this {@link PrepOption}.
     */
    @Override
    public String toString () {

        return String.format("PREPOPTIONID: %s, PREPOPTIONSETID: %s, PREPOPTIONPRICE: %s, NAME: %s, ORIGINALNAME: %s, DISPLAYDEFAULTRECEIPTS: %s, " +
                "DISPLAYDEFAULTKITCHEN: %s, DEFAULTOPTION: %s",
                Objects.toString((prepOptionID != -1 ? prepOptionID : "N/A"), "N/A"),
                Objects.toString((prepOptionSetID != -1 ? prepOptionSetID : "N/A"), "N/A"),
                Objects.toString((prepOptionPrice != -1.0d ? prepOptionPrice : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(originalName) ? originalName : "N/A"), "N/A"),
                Objects.toString(displayDefaultReceipts, "N/A"),
                Objects.toString(displayDefaultKitchen, "N/A"),
                Objects.toString(defaultOption, "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link PrepOption}.
     * Two {@link PrepOption} are defined as being equal if they have the same prepOptionID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link PrepOption}.
     * @return Whether or not the {@link Object} is equal to this {@link PrepOption}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a PrepOption and check if the obj's prepOptionID is equal to this PrepOption's prepOptionID
        PrepOption prepOption = ((PrepOption) obj);
        return Objects.equals(prepOption.prepOptionID, prepOptionID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link PrepOption}.</p>
     *
     * @return The unique hash code for this {@link PrepOption}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(prepOptionID);
    }

    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    /**
     * <p>Getter for the prepOptionID field of the {@link PrepOption}.</p>
     *
     * @return The prepOptionID field of the {@link PrepOption}.
     */
    public int getPrepOptionID () {
        return prepOptionID;
    }

    /**
     * <p>Setter for the prepOptionID field of the {@link PrepOption}.</p>
     *
     * @param prepOptionID The prepOptionID field of the {@link PrepOption}.
     */
    public void setPrepOptionID (int prepOptionID) {
        this.prepOptionID = prepOptionID;
    }

    /**
     * <p>Getter for the prepOptionSetID field of the {@link PrepOption}.</p>
     *
     * @return The prepOptionSetID field of the {@link PrepOption}.
     */
    public int getPrepOptionSetID () {
        return prepOptionSetID;
    }

    /**
     * <p>Setter for the prepOptionSetID field of the {@link PrepOption}.</p>
     *
     * @param prepOptionSetID The prepOptionSetID field of the {@link PrepOption}.
     */
    public void setPrepOptionSetID (int prepOptionSetID) {
        this.prepOptionSetID = prepOptionSetID;
    }

    /**
     * <p>Getter for the prepOptionPrice field of the {@link PrepOption}.</p>
     *
     * @return The prepOptionPrice field of the {@link PrepOption}.
     */
    public double getPrepOptionPrice () {
        return prepOptionPrice;
    }

    /**
     * <p>Setter for the prepOptionPrice field of the {@link PrepOption}.</p>
     *
     * @param prepOptionPrice The prepOptionPrice field of the {@link PrepOption}.
     */
    public void setPrepOptionPrice (double prepOptionPrice) {
        this.prepOptionPrice = prepOptionPrice;
    }

    /**
     * <p>Getter for the name field of the {@link PrepOption}.</p>
     *
     * @return The name field of the {@link PrepOption}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link PrepOption}.</p>
     *
     * @param name The name field of the {@link PrepOption}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the originalName field of the {@link PrepOption}.</p>
     *
     * @return The originalName field of the {@link PrepOption}.
     */
    public String getOriginalName () {
        return originalName;
    }

    /**
     * <p>Setter for the originalName field of the {@link PrepOption}.</p>
     *
     * @param originalName The originalName field of the {@link PrepOption}.
     */
    public void setOriginalName (String originalName) {
        this.originalName = originalName;
    }

    /**
     * <p>Getter for the displayDefaultReceipts field of the {@link PrepOption}.</p>
     *
     * @return The displayDefaultReceipts field of the {@link PrepOption}.
     */
    public boolean getDisplayDefaultReceipts () {
        return displayDefaultReceipts;
    }

    /**
     * <p>Setter for the displayDefaultReceipts field of the {@link PrepOption}.</p>
     *
     * @param displayDefaultReceipts The displayDefaultReceipts field of the {@link PrepOption}.
     */
    public void setDisplayDefaultReceipts (boolean displayDefaultReceipts) {
        this.displayDefaultReceipts = displayDefaultReceipts;
    }

    /**
     * <p>Getter for the displayDefaultKitchen field of the {@link PrepOption}.</p>
     *
     * @return The displayDefaultKitchen field of the {@link PrepOption}.
     */
    public boolean getDisplayDefaultKitchen () {
        return displayDefaultKitchen;
    }

    /**
     * <p>Setter for the displayDefaultKitchen field of the {@link PrepOption}.</p>
     *
     * @param displayDefaultKitchen The displayDefaultKitchen field of the {@link PrepOption}.
     */
    public void setDisplayDefaultKitchen (boolean displayDefaultKitchen) {
        this.displayDefaultKitchen = displayDefaultKitchen;
    }

    /**
     * <p>Getter for the defaultOption field of the {@link PrepOption}.</p>
     *
     * @return The defaultOption field of the {@link PrepOption}.
     */
    public boolean getDefaultOption () {
        return defaultOption;
    }

    /**
     * <p>Setter for the defaultOption field of the {@link PrepOption}.</p>
     *
     * @param defaultOption The defaultOption field of the {@link PrepOption}.
     */
    public void setDefaultOption (boolean defaultOption) {
        this.defaultOption = defaultOption;
    }

    public boolean isDefaultOption(){
        return this.defaultOption;
    }
    public boolean isDisplayDefaultKitchen(){
        return this.displayDefaultKitchen;
    }

}
