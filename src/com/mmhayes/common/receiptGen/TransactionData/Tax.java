package com.mmhayes.common.receiptGen.TransactionData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-21 15:16:11 -0400 (Mon, 21 Sep 2020) $: Date of last commit
    $Rev: 12714 $: Revision of last commit
    Notes: Represents a Tax transaction line item.
*/

import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.TaxLineItemModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by nyu on 10/13/2016.
 */
public class Tax extends TransactionItem
{
    // Identifying attributes
    int itemTypeID = TypeData.ItemType.TAX;
    int taxID;
    String shortName = "";

    /**
     * <p>Empty constructor for a {@link Tax}.</p>
     *
     */
    public Tax () {}

    /**
     * <p>Constructor for a {@link Tax} with it's taxID field set.</p>
     *
     * @param taxID The ID of the tax.
     * @return The {@link Tax} instance with it's taxID field set.
     */
    public Tax taxID (int taxID) {
        this.taxID = taxID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Tax} with it's name field set.</p>
     *
     * @param name The name {@link String} of the tax.
     * @return The {@link Tax} instance with it's name field set.
     */
    public Tax name (String name) {
        this.name = name;
        return this;
    }

    /**
     * <p>Constructor for a {@link Tax} with it's shortName field set.</p>
     *
     * @param shortName The short name {@link String} of the tax.
     * @return The {@link Tax} instance with it's shortName field set.
     */
    public Tax shortName (String shortName) {
        this.shortName = shortName;
        return this;
    }

    /**
     * <p>Constructor for a {@link Tax} with it's paTransLineItemID field set.</p>
     *
     * @param paTransLineItemID Transaction line item ID of the tax line item.
     * @return The {@link Tax} instance with it's paTransLineItemID field set.
     */
    public Tax paTransLineItemID (int paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Tax} with it's quantity field set.</p>
     *
     * @param quantity The quantity of the transaction line item.
     * @return The {@link Tax} instance with it's quantity field set.
     */
    public Tax quantity (double quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * <p>Constructor for a {@link Tax} with it's refundedQty field set.</p>
     *
     * @param refundedQty The refunded quantity of the transaction line item.
     * @return The {@link Tax} instance with it's refundedQty field set.
     */
    public Tax refundedQty (double refundedQty) {
        this.refundedQty = refundedQty;
        return this;
    }

    /**
     * <p>Constructor for a {@link Tax} with it's amount field set.</p>
     *
     * @param amount The amount of the transaction line item.
     * @return The {@link Tax} instance with it's amount field set.
     */
    public Tax amount (double amount) {
        this.amount = amount;
        return this;
    }

    /**
     * <p>Constructor for a {@link Tax} with it's refundedAmount field set.</p>
     *
     * @param refundedAmount The refunded amount of the transaction line item.
     * @return The {@link Tax} instance with it's refundedAmount field set.
     */
    public Tax refundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
        return this;
    }

    //--------------------------------------------------------------------------
    //  Static methods
    //--------------------------------------------------------------------------
    // Creates a Tax object based on information retrieved from the database
    public static Tax createTaxFromDB(HashMap dbData)
    {
        Tax tax = new Tax();

        tax.taxID = HashMapDataFns.getIntVal(dbData, "PAITEMID");
        tax.name = HashMapDataFns.getStringVal(dbData, "ITEMNAME");
        tax.shortName = HashMapDataFns.getStringVal(dbData, "SHORTNAME");
        tax.paTransLineItemID = HashMapDataFns.getIntVal(dbData, "PATRANSLINEITEMID");

        tax.quantity = HashMapDataFns.getDoubleVal(dbData, "QUANTITY");
        tax.refundedQty = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDQUANTITY");

        tax.amount = HashMapDataFns.getDoubleVal(dbData, "AMOUNT");
        tax.refundedAmount = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDAMOUNT");

        return tax;
    }

    public static Tax createTaxFromXMLRPC(ArrayList taxData, int transTypeID)
    {
        Tax tax = new Tax();

        try {
            tax.taxID = new Integer(taxData.get(4).toString()).intValue();
            tax.name = taxData.get(1).toString();
            tax.amount = new Double(taxData.get(2).toString()).doubleValue();
            tax.shortName = taxData.get(3).toString();

            if (transTypeID == TypeData.TranType.REFUND || transTypeID == TypeData.TranType.VOID)
            {
                tax.quantity = -1;
            }
            else
            {
                tax.quantity = 1;
            }
        } catch (Exception e) {
            ReceiptGen.logError("Tax.createTax error", e);
        }

        return tax;
    }

    /**
     * <p>Builds a {@link Tax} from the given {@link XmlRpcStruct}.</p>
     *
     * @param taxStruct The {@link XmlRpcStruct} to use to build the {@link Tax}.
     * @return The {@link Tax} instance built form a {@link XmlRpcStruct}.
     */
    public static Tax buildFromXmlRpcStruct (XmlRpcStruct taxStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(taxStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to Tax.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new Tax()
                .taxID(HashMapDataFns.getIntVal(taxStruct, "TAXID", true))
                .name(HashMapDataFns.getStringVal(taxStruct, "NAME", true))
                .shortName(HashMapDataFns.getStringVal(taxStruct, "SHORTNAME", true))
                .paTransLineItemID(HashMapDataFns.getIntVal(taxStruct, "PATRANSLINEITEMID", true))
                .quantity(HashMapDataFns.getDoubleVal(taxStruct, "QUANTITY", true))
                .refundedQty(HashMapDataFns.getDoubleVal(taxStruct, "REFUNDEDQTY", true))
                .amount(HashMapDataFns.getDoubleVal(taxStruct, "AMOUNT", true))
                .refundedAmount(HashMapDataFns.getDoubleVal(taxStruct, "REFUNDEDAMOUNT", true));
    }

    public static Tax createTax(TaxLineItemModel taxLineItemModel, TransactionModel transactionModel)
    {
        Tax tax = new Tax();

        try {
            tax.taxID = taxLineItemModel.getTax().getId();
            tax.name = taxLineItemModel.getTax().getName();
            //TODO: not sure if this is supposed to be the extended amount?
            tax.amount = new Double(taxLineItemModel.getExtendedAmount().toString());
            tax.shortName = taxLineItemModel.getTax().getShortName();

            if (transactionModel.getTransactionTypeId() == TypeData.TranType.REFUND || transactionModel.getTransactionTypeId() == TypeData.TranType.VOID)
            {
                tax.quantity = -1;
            }
            else
            {
                tax.quantity = 1;
            }
        } catch (Exception e) {
            ReceiptGen.logError("Tax.createTax error", e);
        }

        return tax;
    }

    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public int getTaxID()
    {
        return taxID;
    }

    public String getShortName()
    {
        return shortName;
    }

    /**
     * <p>Overridden toString() method for a {@link Tax} instance.</p>
     *
     * @return The {@link Tax} instance as a {@link String};
     */
    @Override
    public String toString () {

        return String.format("TAXID: %s, NAME: %s, SHORTNAME: %s, PATRANSLINEITEMID: %s, QUANTITY: %s, REFUNDEDQTY: %s, AMOUNT: %s, REFUNDEDAMOUNT: %s",
                Objects.toString((taxID > 0 ? taxID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((paTransLineItemID > 0 ? paTransLineItemID : "N/A"), "N/A"),
                Objects.toString((quantity > 0.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQty > 0.0d ? refundedQty : "N/A"), "N/A"),
                Objects.toString((amount > 0.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount > 0.0d ? refundedAmount : "N/A"), "N/A"));
    }

    /**
     * <p>Overridden equals() method for a {@link Tax} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link Tax} instance.
     * @return Whether or not the {@link Object} is equal to the {@link Tax} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a Tax
        Tax tax = (Tax) obj;
        return Objects.equals(tax.taxID, taxID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link Tax} instance.</p>
     *
     * @return The hash code for a {@link Tax} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(taxID);
    }
}
