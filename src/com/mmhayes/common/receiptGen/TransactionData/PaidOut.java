package com.mmhayes.common.receiptGen.TransactionData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-21 12:58:42 -0400 (Mon, 21 Sep 2020) $: Date of last commit
    $Rev: 12712 $: Revision of last commit
    Notes: Represents a PaidOut transaction line item.
*/

import com.mmhayes.common.datawire.DatawireConstants;
import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.GiftCardTxnData;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.PaidOutLineItemModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by nyu on 10/16/2016.
 */
public class PaidOut extends TransactionItem
{
    // Identifying attributes
    int itemTypeID = TypeData.ItemType.PAIDOUT;
    int paidOutID;
    GiftCardTxnData gcTxnData;
    int paidOutTypeID;
    String giftCardTransInfo;

    /**
     * <p>Empty constructor for a {@link PaidOut}.</p>
     *
     */
    public PaidOut () {}

    /**
     * <p>Constructor for a {PaidOut} with it's paidOutID field set.</p>
     *
     * @param paidOutID The ID of the paid out.
     * @return The {PaidOut} instance with it's paidOutID field set.
     */
    public PaidOut paidOutID (int paidOutID) {
        this.paidOutID = paidOutID;
        return this;
    }

    /**
     * <p>Constructor for a {PaidOut} with it's name field set.</p>
     *
     * @param name The name {@link String} of the paid out.
     * @return The {PaidOut} instance with it's name field set.
     */
    public PaidOut name (String name) {
        this.name = name;
        return this;
    }

    /**
     * <p>Constructor for a {PaidOut} with it's paTransLineItemID field set.</p>
     *
     * @param paTransLineItemID The transaction line item ID of the paid out line item.
     * @return The {PaidOut} instance with it's paTransLineItemID field set.
     */
    public PaidOut paTransLineItemID (int paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
        return this;
    }

    /**
     * <p>Constructor for a {PaidOut} with it's quantity field set.</p>
     *
     * @param quantity The quantity of the transaction line item.
     * @return The {PaidOut} instance with it's quantity field set.
     */
    public PaidOut quantity (double quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * <p>Constructor for a {PaidOut} with it's refundedQty field set.</p>
     *
     * @param refundedQty The refunded quantity of the transaction line item.
     * @return The {PaidOut} instance with it's refundedQty field set.
     */
    public PaidOut refundedQty (double refundedQty) {
        this.refundedQty = refundedQty;
        return this;
    }

    /**
     * <p>Constructor for a {PaidOut} with it's amount field set.</p>
     *
     * @param amount The amount of the transaction line item.
     * @return The {PaidOut} instance with it's amount field set.
     */
    public PaidOut amount (double amount) {
        this.amount = amount;
        return this;
    }

    /**
     * <p>Constructor for a {PaidOut} with it's refundedAmount field set.</p>
     *
     * @param refundedAmount The refunded amount of the transaction line item.
     * @return The {PaidOut} instance with it's refundedAmount field set.
     */
    public PaidOut refundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
        return this;
    }

    /**
     * <p>Constructor for a {PaidOut} with it's itemComment field set.</p>
     *
     * @param itemComment A comment {@link String} for the transaction line item.
     * @return The {PaidOut} instance with it's itemComment field set.
     */
    public PaidOut itemComment (String itemComment) {
        this.itemComment = itemComment;
        return this;
    }

    /**
     * <p>Constructor for a {PaidOut} with it's paidOutTypeID field set.</p>
     *
     * @param paidOutTypeID An ID corresponding to the type of paid out.
     * @return The {PaidOut} instance with it's paidOutTypeID field set.
     */
    public PaidOut paidOutTypeID (int paidOutTypeID) {
        this.paidOutTypeID = paidOutTypeID;
        return this;
    }

    /**
     * <p>Constructor for a {PaidOut} with it's giftCardTransInfo field set.</p>
     *
     * @param giftCardTransInfo A {@link String} containing gift card information.
     * @return The {PaidOut} instance with it's giftCardTransInfo field set.
     */
    public PaidOut giftCardTransInfo (String giftCardTransInfo) {
        this.giftCardTransInfo = giftCardTransInfo;
        return this;
    }

    //--------------------------------------------------------------------------
    //  Static methods
    //--------------------------------------------------------------------------
    public static PaidOut createPaidOutFromDB(HashMap dbData)
    {
        PaidOut paidOutItem = new PaidOut();

        try {
            paidOutItem.paidOutID = HashMapDataFns.getIntVal(dbData, "PAITEMID");
            paidOutItem.name = HashMapDataFns.getStringVal(dbData, "ITEMNAME");
            paidOutItem.paTransLineItemID = HashMapDataFns.getIntVal(dbData, "PATRANSLINEITEMID");

            paidOutItem.quantity = HashMapDataFns.getDoubleVal(dbData, "QUANTITY");
            paidOutItem.refundedQty = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDQUANTITY");

            paidOutItem.amount = HashMapDataFns.getDoubleVal(dbData, "AMOUNT");
            paidOutItem.refundedAmount = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDAMOUNT");

            paidOutItem.itemComment = HashMapDataFns.getStringVal(dbData, "ITEMCOMMENT");
            paidOutItem.paidOutTypeID = HashMapDataFns.getIntVal(dbData, "PAPAIDOUTTYPEID");

            String giftCardTransInfo = HashMapDataFns.getStringVal(dbData, "CREDITCARDTRANSINFO");
            if (!giftCardTransInfo.isEmpty())
            {
                paidOutItem.gcTxnData = new GiftCardTxnData(giftCardTransInfo, ";");
            }
        } catch (Exception ex) {
            ReceiptGen.logError("PaidOut.createPaidOutFromDB error", ex);
        }

        return paidOutItem;
    }

    public static PaidOut createPaidOutFromXMLRPC(ArrayList detail)
    {
        PaidOut paidOutItem = new PaidOut();

        try
        {
            paidOutItem.paidOutID = new Integer(detail.get(4).toString()).intValue();
            paidOutItem.quantity = new Double(detail.get(1).toString()).doubleValue();
            paidOutItem.amount = new Double(detail.get(2).toString()).doubleValue();
            paidOutItem.name = detail.get(3).toString();
            paidOutItem.itemComment = detail.get(5).toString();
        }
        catch (Exception e)
        {
            ReceiptGen.logError("PaidOut.createPaidOutFromXMLRPC error", e);
        }

        return paidOutItem;
    }

    /**
     * <p>Builds a {@link PaidOut} from the given {@link XmlRpcStruct}.</p>
     *
     * @param paidOutStruct The {@link XmlRpcStruct} to use to build the {@link PaidOut}.
     * @return The {@link PaidOut} instance built form a {@link XmlRpcStruct}.
     */
    public static PaidOut buildFromXmlRpcStruct (XmlRpcStruct paidOutStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(paidOutStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to PaidOut.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new PaidOut()
                .paidOutID(HashMapDataFns.getIntVal(paidOutStruct, "PAIDOUTID", true))
                .name(HashMapDataFns.getStringVal(paidOutStruct, "NAME", true))
                .paTransLineItemID(HashMapDataFns.getIntVal(paidOutStruct, "PATRANSLINEITEMID", true))
                .quantity(HashMapDataFns.getDoubleVal(paidOutStruct, "QUANTITY", true))
                .refundedQty(HashMapDataFns.getDoubleVal(paidOutStruct, "REFUNDEDQTY", true))
                .amount(HashMapDataFns.getDoubleVal(paidOutStruct, "AMOUNT", true))
                .refundedAmount(HashMapDataFns.getDoubleVal(paidOutStruct, "REFUNDEDAMOUNT", true))
                .itemComment(HashMapDataFns.getStringVal(paidOutStruct, "ITEMCOMMENT", true))
                .paidOutTypeID(HashMapDataFns.getIntVal(paidOutStruct, "PAIDOUTTYPEID", true))
                .giftCardTransInfo(HashMapDataFns.getStringVal(paidOutStruct, "GIFTCARDTRANSINFO", true));
    }

    public static PaidOut createPaidOut(PaidOutLineItemModel paidOutLineItemModel)
    {
        PaidOut paidOutItem = new PaidOut();

        try {
            paidOutItem.paidOutID = paidOutLineItemModel.getPaidOut().getId();
            paidOutItem.quantity = new Double(paidOutLineItemModel.getQuantity().toString());
            paidOutItem.amount = new Double(paidOutLineItemModel.getAmount().toString());
            paidOutItem.name = paidOutLineItemModel.getPaidOut().getName();
        } catch (Exception e) {
            ReceiptGen.logError("PaidOut.createPaidOut(paidOutLineItemModel) error", e);
        }

        return paidOutItem;
    }

    public String getNameForReceipt()
    {
        String receiptName = "";

        try {
            receiptName = name;
            if (isIntegratedGiftCard())
            {
                String txnTypeCode = gcTxnData.getDetailValue(DatawireConstants.DatawireDetailPrefix.TRANSACTION_TYPE.getPrefixName());
                String txnTypeReceiptName = DatawireConstants.DatawireTransType.convertTxnCodeToReceiptName(txnTypeCode);
                String cardNumLastFour = getGiftCardCardNumLastFour();

                if (!txnTypeReceiptName.isEmpty())
                {
                    receiptName = receiptName + " " + txnTypeReceiptName;
                }

                if (!cardNumLastFour.isEmpty())
                {
                    receiptName = receiptName + " (" + cardNumLastFour + ")";
                }
            }
        } catch (Exception error) {
            ReceiptGen.logError("PaidOut.getNameForReceipt error", error);
        }

        return receiptName;
    }

    public boolean isIntegratedGiftCard()
    {
        boolean isGiftCard = false;

        if (paidOutTypeID == TypeData.PaidOutType.INTEGRATED_GIFT_CARD)
        {
            isGiftCard = true;
        }

        return isGiftCard;
    }

    public String getGiftCardTxnTypeName()
    {
        String txnTypeName = "";

        try
        {
            if (gcTxnData != null)
            {
                String txnCode = gcTxnData.getDetailValue(DatawireConstants.DatawireDetailPrefix.TRANSACTION_TYPE.getPrefixName());
                txnTypeName = txnCode.isEmpty() ? "" : DatawireConstants.DatawireTransType.convertTxnCodeToName(txnCode);
            }
        } catch (Exception error) {
            ReceiptGen.logError("PaidOut.getGiftCardTxnTypeName error", error);
        }

        return txnTypeName;
    }

    public String getGiftCardCardNumLastFour()
    {
        String giftCardCardNumLastFour = "";

        try
        {
            if (gcTxnData != null)
            {
                giftCardCardNumLastFour = gcTxnData.getDetailValue(DatawireConstants.DatawireDetailPrefix.CARD_NUM.getPrefixName());
                if (giftCardCardNumLastFour.length() == 16)
                {
                    giftCardCardNumLastFour = giftCardCardNumLastFour.substring(12, 16);
                }
            }
        } catch (Exception error) {
            ReceiptGen.logError("PaidOut.getGiftCardCardNumLastFour error", error);
        }

        return giftCardCardNumLastFour;
    }

    /**
     * <p>Overridden toString() method for a {@link PaidOut} instance.</p>
     *
     * @return The {@link PaidOut} instance as a {@link String};
     */
    @Override
    public String toString () {

        return String.format("PAIDOUTID: %s, NAME: %s, PATRANSLINEITEMID: %s, QUANTITY: %s, REFUNDEDQTY: %s, AMOUNT: %s, " +
                "REFUNDEDAMOUNT: %s, ITEMCOMMENT: %s, PAIDOUTTYPEID: %s, GIFTCARDTRANSINFO: %s",
                Objects.toString((paidOutID > 0 ? paidOutID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((paTransLineItemID > 0 ? paTransLineItemID : "N/A"), "N/A"),
                Objects.toString((quantity > 0.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQty > 0.0d ? refundedQty : "N/A"), "N/A"),
                Objects.toString((amount > 0.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount > 0.0d ? refundedAmount : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(itemComment) ? itemComment : "N/A"), "N/A"),
                Objects.toString((paidOutTypeID > 0 ? paidOutTypeID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(giftCardTransInfo) ? giftCardTransInfo : "N/A"), "N/A"));
    }

    /**
     * <p>Overridden equals() method for a {@link PaidOut} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link PaidOut} instance.
     * @return Whether or not the {@link Object} is equal to the {@link PaidOut} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a PaidOut
        PaidOut paidOut = (PaidOut) obj;
        return Objects.equals(paidOut.paidOutID, paidOutID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link PaidOut} instance.</p>
     *
     * @return The hash code for a {@link PaidOut} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(paidOutID);
    }
}
