package com.mmhayes.common.receiptGen.TransactionData;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by nyu on 10/14/2016.
 */
public abstract class TransactionItem
{
    int itemTypeID;
    int paTransLineItemID;
    int linkedPaTransLineItemID;

    String name = "";

    // Quantity
    double quantity;
    double refundedQty;

    // Amount
    double amount;
    double refundedAmount;
    double rootPrice;

    // Other
    String itemComment = "";
    BigDecimal grossWeight = null;
    BigDecimal fixedNetWeight = null;

    public int getPATransLineItemID()
    {
        return paTransLineItemID;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String newName)
    {
        name = newName;
    }

    public double getQuantity()
    {
        return quantity;
    }

    public void setGrossWeight(BigDecimal weight)
    {
        grossWeight = weight;
    }

    public BigDecimal getGrossWeight()
    {
        return grossWeight;
    }

    public void setFixedNetWeight(BigDecimal weight)
    {
        fixedNetWeight = weight;
    }

    public BigDecimal getFixedNetWeight()
    {
        return fixedNetWeight;
    }


    public double getRefundedQty()
    {
        return refundedQty;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(BigDecimal newAmount)
    {
        amount = newAmount.doubleValue();
    }

    public double getRootPrice()
    {
        return rootPrice;
    }

    public void setRootPrice(Double rootPrice)
    {
        this.rootPrice = rootPrice;
    }

    public int getLinkedPaTransLineItemID() {
        return linkedPaTransLineItemID;
    }

    public void setLinkedPaTransLineItemID(int linkedPaTransLineItemID) {
        this.linkedPaTransLineItemID = linkedPaTransLineItemID;
    }

    public double getExtendedAmt()
    {
        BigDecimal extendedAmount = new BigDecimal(0);

        BigDecimal rwdQty = new BigDecimal(quantity);
        BigDecimal rwdAmt = new BigDecimal(amount);
        extendedAmount = rwdQty.multiply(rwdAmt);

        return extendedAmount.doubleValue();
    }

    public double getRefundedAmount()
    {
        return refundedAmount;
    }

    public String getItemComment()
    {
        return itemComment;
    }

    public BigDecimal getTotalItemPrice()
    {
        BigDecimal totalItemPrice = new BigDecimal(quantity * amount).setScale(4, RoundingMode.HALF_UP);

        totalItemPrice = totalItemPrice.setScale(2, RoundingMode.HALF_UP);

        return totalItemPrice;
    }

    public BigDecimal getSingleQtyTotalItemPrice()
    {
        BigDecimal totalItemPrice = new BigDecimal(amount).setScale(4, RoundingMode.HALF_UP);

        totalItemPrice = totalItemPrice.setScale(2, RoundingMode.HALF_UP);

        return totalItemPrice;
    }

    public BigDecimal getTaxItemPrice()
    {
        BigDecimal taxItemPrice = new BigDecimal(quantity * amount).setScale(2, RoundingMode.CEILING);

        return taxItemPrice;
    }
}
