package com.mmhayes.common.receiptGen.TransactionData;

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.datawire.DatawireConstants;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.GiftCardTxnData;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.TenderLineItemModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.MMHResultSet;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
public class Donation extends TransactionItem{

    int DonationID;
    int DonationTypeID;

    public Donation()
    {
    }

    public static Donation createDonationFromDB(HashMap dbData){
        Donation dntn = new Donation();
        try {
            dntn.DonationID = HashMapDataFns.getIntVal(dbData, "PAITEMID");
            dntn.name= HashMapDataFns.getStringVal(dbData, "DOLLARDONATIONNAME");
            dntn.paTransLineItemID = HashMapDataFns.getIntVal(dbData, "PATRANSLINEITEMID");
            dntn.quantity = HashMapDataFns.getDoubleVal(dbData, "QUANTITY");
            dntn.refundedQty = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDQUANTITY");
            dntn.amount = HashMapDataFns.getDoubleVal(dbData, "AMOUNT");
            dntn.refundedAmount = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDAMOUNT");
        } catch (Exception e) {
            ReceiptGen.logError("Donation.createDonationFromDB error", e);
        }

        return dntn;
    }

    public String getNameForReceipt()
    {
        String receiptName = "DONATION: ";

        try {
            receiptName = receiptName + name;
        } catch (Exception error) {
            ReceiptGen.logError("Donation.getNameForReceipt error", error);
        }

        return receiptName;
    }
}
