package com.mmhayes.common.receiptGen.TransactionData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-21 11:55:42 -0400 (Mon, 21 Sep 2020) $: Date of last commit
    $Rev: 12711 $: Revision of last commit
    Notes: Represents a Combo transaction line item.
*/

import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.ComboLineItemModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by gematuszyk on 11/12/2019.
 */
public class Combo extends TransactionItem
{
    // Identifying attributes
    int itemTypeID = TypeData.ItemType.COMBO;
    int comboID;
    Double rootPrice = 0.0;
    String originalOrderNum = "";

    /**
     * <p>Empty constructor for a {@link Combo}.</p>
     *
     */
    public Combo () {}
    
    /**
     * <p>Constructor for a {@link Combo} with it's comboID field set.</p>
     * 
     * @param comboID The ID for of the combo.
     * @return The {@link Combo} instance with it's comboID field set.
     */
    public Combo comboID (int comboID) {
        this.comboID = comboID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Combo} with it's name field set.</p>
     * 
     * @param name The name {@link String} of the combo.
     * @return The {@link Combo} instance with it's name field set.
     */
    public Combo name (String name) {
        this.name = name;
        return this;
    }

    /**
     * <p>Constructor for a {@link Combo} with it's paTransLineItemID field set.</p>
     * 
     * @param paTransLineItemID The transaction line item ID of the combo transaction line item.
     * @return The {@link Combo} instance with it's paTransLineItemID field set.
     */
    public Combo paTransLineItemID (int paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Combo} with it's quantity field set.</p>
     * 
     * @param quantity The quantity of the transaction line item.
     * @return The {@link Combo} instance with it's quantity field set.
     */
    public Combo quantity (double quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * <p>Constructor for a {@link Combo} with it's refundedQty field set.</p>
     * 
     * @param refundedQty The refunded amount of the transaction line item.
     * @return The {@link Combo} instance with it's refundedQty field set.
     */
    public Combo refundedQty (double refundedQty) {
        this.refundedQty = refundedQty;
        return this;
    }

    /**
     * <p>Constructor for a {@link Combo} with it's amount field set.</p>
     * 
     * @param amount The amount of the transaction line item.
     * @return The {@link Combo} instance with it's amount field set.
     */
    public Combo amount (double amount) {
        this.amount = amount;
        return this;
    }

    /**
     * <p>Constructor for a {@link Combo} with it's refundedAmount field set.</p>
     * 
     * @param refundedAmount The refunded amount of the transaction line item.
     * @return The {@link Combo} instance with it's refundedAmount field set.
     */
    public Combo refundedAmount (double refundedAmount) {
        this.refundedAmount = refundedAmount;
        return this;
    }

    /**
     * <p>Constructor for a {@link Combo} with it's itemComment field set.</p>
     * 
     * @param itemComment A comment {@link String} associated with the transaction line item.
     * @return The {@link Combo} instance with it's itemComment field set.
     */
    public Combo itemComment (String itemComment) {
        this.itemComment = itemComment;
        return this;
    }

    /**
     * <p>Constructor for a {@link Combo} with it's originalOrderNum field set.</p>
     * 
     * @param originalOrderNum The original order number {@link String} of the transaction line item.
     * @return The {@link Combo} instance with it's originalOrderNum field set.
     */
    public Combo originalOrderNum (String originalOrderNum) {
        this.originalOrderNum = originalOrderNum;
        return this;
    }
    
    //--------------------------------------------------------------------------
    //  Static methods
    //--------------------------------------------------------------------------
    public static Combo createComboFromDB(HashMap dbData)
    {
        Combo comboItem = new Combo();

        try {
            comboItem.comboID = HashMapDataFns.getIntVal(dbData, "PAITEMID");
            comboItem.name = HashMapDataFns.getStringVal(dbData, "ITEMNAME");
            comboItem.paTransLineItemID = HashMapDataFns.getIntVal(dbData, "PATRANSLINEITEMID");

            comboItem.quantity = HashMapDataFns.getDoubleVal(dbData, "QUANTITY");
            comboItem.refundedQty = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDQUANTITY");

            comboItem.amount = Math.abs(HashMapDataFns.getDoubleVal(dbData, "AMOUNT"));
            comboItem.refundedAmount = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDAMOUNT");

            comboItem.itemComment = HashMapDataFns.getStringVal(dbData, "ITEMCOMMENT");
            comboItem.originalOrderNum = HashMapDataFns.getStringVal(dbData, "ORIGINALORDERNUM");
        } catch (Exception ex) {
            ReceiptGen.logError("Combo.createComboFromDB error", ex);
        }

        return comboItem;
    }

    public static Combo createComboFromXMLRPC(ArrayList detail)
    {
        Combo comboItem = new Combo();

        try
        {
            comboItem.comboID = new Integer(detail.get(4).toString()).intValue();
            comboItem.quantity = new Double(detail.get(1).toString()).doubleValue();
            comboItem.amount = new Double(detail.get(2).toString()).doubleValue();
            comboItem.name = detail.get(3).toString();
            comboItem.paTransLineItemID = Integer.parseInt(detail.get(7).toString());
            comboItem.setRootPrice(new Double(detail.get(8).toString()).doubleValue());
            comboItem.originalOrderNum = detail.get(9).toString();
        }
        catch (Exception err)
        {
            ReceiptGen.logError("Combo.createComboFromXMLRPC error", err);
        }

        return comboItem;
    }

    public static Combo createCombo(ComboLineItemModel comboLineItemModel)
    {
        Combo comboItem = new Combo();

        try {
            comboItem.comboID = comboLineItemModel.getCombo().getId();
            comboItem.name = comboLineItemModel.getCombo().getName();
            comboItem.paTransLineItemID = comboLineItemModel.getId();

            comboItem.quantity = new Double(comboLineItemModel.getQuantity().toString());
            comboItem.refundedQty = (comboLineItemModel.getRefundedQty() == null ? new Double(0) : new Double(comboLineItemModel.getRefundedQty().toString()));

            comboItem.amount = new Double(comboLineItemModel.getAmount().toString());
            comboItem.refundedAmount = comboItem.getRefundedAmount();
            comboItem.itemComment = comboLineItemModel.getItemComment();
            comboItem.originalOrderNum = comboLineItemModel.getOriginalOrderNumber();
        } catch (Exception e) {
            ReceiptGen.logError("Combo.createCombo(comboLineItemModel) error", e);
        }

        return comboItem;
    }

    /**
     * <p>Builds a {@link Combo} from the given {@link XmlRpcStruct}.</p>
     *
     * @param comboStruct The {@link XmlRpcStruct} to use to build the {@link Combo}.
     * @return The {@link Combo} instance built form a {@link XmlRpcStruct}.
     */
    public static Combo buildFromXmlRpcStruct (XmlRpcStruct comboStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(comboStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to Combo.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new Combo()
                .comboID(HashMapDataFns.getIntVal(comboStruct, "COMBOID", true))
                .name(HashMapDataFns.getStringVal(comboStruct, "NAME", true))
                .paTransLineItemID(HashMapDataFns.getIntVal(comboStruct, "PATRANSLINEITEMID", true))
                .quantity(HashMapDataFns.getDoubleVal(comboStruct, "QUANTITY", true))
                .refundedQty(HashMapDataFns.getDoubleVal(comboStruct, "REFUNDEDQTY", true))
                .amount(HashMapDataFns.getDoubleVal(comboStruct, "AMOUNT", true))
                .refundedAmount(HashMapDataFns.getDoubleVal(comboStruct, "REFUNDEDAMOUNT", true))
                .itemComment(HashMapDataFns.getStringVal(comboStruct, "ITEMCOMMENT", true))
                .originalOrderNum(HashMapDataFns.getStringVal(comboStruct, "ORIGINALORDERNUM", true));
    }

    public BigDecimal getTotalItemPrice()
    {
        BigDecimal totalItemPrice = new BigDecimal(quantity * amount).setScale(4, RoundingMode.HALF_UP);

        totalItemPrice = totalItemPrice.setScale(2, RoundingMode.HALF_UP);
        //Make sure voids/refunds display the price as a negative
        if (quantity<0)
        {
            totalItemPrice = totalItemPrice.abs().multiply(new BigDecimal(-1));
        }

        return totalItemPrice;
    }

    public int getComboID() {
        return comboID;
    }

    public void setComboID(int comboID) {
        this.comboID = comboID;
    }

    public String getOriginalOrderNum()
    {
        return originalOrderNum;
    }

    public void setOriginalOrderNum(String originalOrderNum)
    {
        this.originalOrderNum = originalOrderNum;
    }

    /**
     * <p>Overridden toString() method for a {@link Combo} instance.</p>
     *
     * @return The {@link Combo} instance as a {@link String};
     */
    @Override
    public String toString () {

        return String.format("COMBOID: %s, NAME: %s, PATRANSLINEITEMID: %s, QUANTITY: %s, REFUNDEDQTY: %s, AMOUNT: %s, REFUNDEDAMOUNT: %s, ITEMCOMMENT: %s, ORIGINALORDERNUM: %s",
                Objects.toString((comboID > 0 ? comboID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((paTransLineItemID > 0 ? paTransLineItemID : "N/A"), "N/A"),
                Objects.toString((quantity > 0.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((refundedQty > 0.0d ? refundedQty : "N/A"), "N/A"),
                Objects.toString((amount > 0.0d ? amount : "N/A"), "N/A"),
                Objects.toString((refundedAmount > 0.0d ? refundedAmount : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(itemComment) ? itemComment : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(originalOrderNum) ? originalOrderNum : "N/A"), "N/A"));
    }

    /**
     * <p>Overridden equals() method for a {@link Combo} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link Combo} instance.
     * @return Whether or not the {@link Object} is equal to the {@link Combo} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a Combo
        Combo combo = (Combo) obj;
        return Objects.equals(combo.comboID, comboID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link Combo} instance.</p>
     *
     * @return The hash code for a {@link Combo} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(comboID);
    }
}
