package com.mmhayes.common.receiptGen.TransactionData;

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.MMHResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nyu on 10/16/2016.
 */
public class EmployeeData
{
    private String name = "";
    private int badgeAssignmentID = 0;
    private String badgeNumber = "";
    private String employeeID = "";
    private String employeeNumber = "";
    private int numSplits;
    private int receiptBalanceTypeID = 1;
    private String receiptBalancePromptTxt = "";
    private double globalBalance = 0.0;
    private double globalAvailable = 0.0;
    private double storeBalance = 0.0;
    private double storeAvailable = 0.0;
    private double dailyLimitBal = 0.0;

    public EmployeeData()
    {
    }

    // Looks up and sets information about this employee so that we can print receipt acct info lines later
    public void lookupQCAcctInfo(int terminalID, double tenderAmount, DataManager dm)
    {
        try {
            String badgeAssignID = "";
            if (badgeAssignmentID != 0)
            {
                badgeAssignID = Integer.toString(badgeAssignmentID);
            }

            ArrayList empDBData = personBadgeLookupWorker(terminalID, tenderAmount, employeeID, Integer.toString(badgeAssignmentID), dm);

            if (empDBData != null && empDBData.size() > 0)
            {
                HashMap empDetails = (HashMap)empDBData.get(0);

                // Get/set balance info
                globalBalance = HashMapDataFns.getDoubleVal(empDetails, "GLOBALBALANCE");
                globalAvailable = HashMapDataFns.getDoubleVal(empDetails, "GLOBALLIMIT") - globalBalance;
                storeBalance = HashMapDataFns.getDoubleVal(empDetails, "STOREBALANCE");
                storeAvailable = HashMapDataFns.getDoubleVal(empDetails, "STORELIMIT") - storeBalance;

                // Other
                name = HashMapDataFns.getStringVal(empDetails, "NAME");
                employeeNumber = HashMapDataFns.getStringVal(empDetails, "EMPLOYEENUMBER");

                // Badge number
                if (badgeAssignmentID != 0)
                {
                    badgeNumber = HashMapDataFns.getStringVal(empDetails, "BADGEALIASNUM");
                }
                else
                {
                    badgeNumber = HashMapDataFns.getStringVal(empDetails, "BADGE");
                }

                // Receipt balance type ID
                receiptBalanceTypeID = HashMapDataFns.getIntVal(empDetails, "RECEIPTBALANCETYPEID");
                receiptBalancePromptTxt = HashMapDataFns.getStringVal(empDetails, "RECEIPTBALANCEPROMPT");
            }
        } catch (Exception e) {
            ReceiptGen.logError("Tender.lookupQCAcctInfo error", e);
        }
    }

    private ArrayList personBadgeLookupWorker(int terminalId, double amount, String employeeID, String badgeAssignmentID, DataManager dm) throws SQLException
    {
        ArrayList al = new ArrayList();
        int iEmpID = 0;
        int iBadgeID = 0;
        int iBadgeAssignmentID = 0;
        long lBadgeNum = 0;
        String badgeNum = "";

        if (badgeNum.equals("") == false)
        {
            // If we have the badge number, get the employee ID, badge ID, and badge assignment ID associated with this badge number
            // We have the badge number but not empID/badgeID for sale transactions
            String sql = dm.getSQLProperty("data.posanywhere.PersonBadgeLookup", null, ReceiptGen.getLogFileName());
            sql = dm.expandSqlParam(1, sql, badgeNum, false, ReceiptGen.getLogFileName());
            MMHResultSet rsEmpInfo = dm.runSql(sql);
            if (rsEmpInfo.resultSet.next())
            {
                iEmpID = rsEmpInfo.resultSet.getInt(1);
                if (rsEmpInfo.resultSet.getMetaData().getColumnCount() > 1)
                {
                    iBadgeID = rsEmpInfo.resultSet.getInt(2);
                    iBadgeAssignmentID = rsEmpInfo.resultSet.getInt(3);
                }
            }
            else
            {
                return new ArrayList();
            }
            rsEmpInfo.close();
            rsEmpInfo = null;
        }
        else
        {
            iEmpID = Integer.parseInt(employeeID);

            if (badgeAssignmentID.equals("") == false)
            {
                iBadgeAssignmentID = Integer.parseInt(badgeAssignmentID);
            }
        }

        // If we have the badge assignment ID but no badge number or badgeID, get the badge number (alias)
        // We have the badge assignment ID but no badge num for recalled transactions (reprints, voids, refunds)
        if (badgeAssignmentID.equals("") == false && (badgeNum.equals("") || iBadgeID == 0))
        {
            // Get the badge number associated with this badge ID
            MMHResultSet badgeAliasInfo = dm.parameterizedExecuteQuery("data.posanywhere.BadgeNumberLookup", new Object[]{new Integer(badgeAssignmentID)}, ReceiptGen.getLogFileName());
            if (badgeAliasInfo.resultSet.next())
            {
                iBadgeID = badgeAliasInfo.resultSet.getInt(1);
                lBadgeNum = badgeAliasInfo.resultSet.getLong(2);
            }
        }
        else
        {
            if (badgeNum.equals("") == false)
            {
                lBadgeNum = Long.parseLong(badgeNum);
            }
        }

        al = dm.parameterizedExecuteQuery("data.posanywhere.PersonTerminalLookupBP",
                new Object[]{new Integer(terminalId), new Double(amount), new Integer(iEmpID)}, ReceiptGen.getLogFileName(), true);

        if ((al != null) && (al.size() > 0))
        {
            HashMap hm = (HashMap)al.get(0);

            // Add BadgeID to hashmap
            hm.put("BADGEID", iBadgeID);

            // Add BadgeAssignmentID to hashmap
            hm.put("BADGEASSIGNMENTID", iBadgeAssignmentID);

            // Add BadgeNumber (alias) to hashmap
            hm.put("BADGEALIASNUM", new Long(lBadgeNum).toString());

            if (Integer.parseInt("0" + hm.get("NUMPAYMENTS").toString()) > 1)
            {
                double available = 0;
                double globalAvailable = Double.parseDouble(hm.get("GLOBALLIMIT").toString() + "0") - Double.parseDouble(hm.get("GLOBALBALANCE").toString() + "0");
                double storeAvailable = Double.parseDouble(hm.get("STORELIMIT").toString() + "0") - Double.parseDouble(hm.get("STOREBALANCE").toString() + "0");
                double transactionLimit = Double.parseDouble(hm.get("SINGLECHARGELIMIT").toString() + "0");
                if (globalAvailable < storeAvailable)
                {
                    if (globalAvailable < transactionLimit)
                    {
                        available = globalAvailable;
                    }
                    else
                    {
                        available = transactionLimit;
                    }
                }
                else
                {
                    if (storeAvailable < transactionLimit)
                    {
                        available = storeAvailable;
                    }
                    else
                    {
                        available = transactionLimit;
                    }
                }
                if (available < amount)
                {
                    al = dm.serializeSqlWithColNames("data.posanywhere.PersonTerminalLookup",
                            new Object[]{new Integer(terminalId), new Double(available), new Integer(iEmpID)},null,ReceiptGen.getLogFileName());

                    // Add BadgeID to hashmap
                    ((HashMap) al.get(0)).put("BADGEID", iBadgeID);

                    // Add BadgeAssignmentID to hashmap
                    ((HashMap) al.get(0)).put("BADGEASSGINMENTID", iBadgeAssignmentID);

                    // Add BadgeNumber (alias) to hashmap
                    ((HashMap) al.get(0)).put("BADGEALIASNUM", new Long(lBadgeNum).toString());

                    return al;
                }
                else
                {
                    return al;
                }
            }
            else
            {
                return al;
            }
        }
        else
        {
            return al;
        }
    }

    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public int getBadgeAssignmentID()
    {
        return badgeAssignmentID;
    }

    public void setBadgeAssignmentID(int badgeAssignID)
    {
        badgeAssignmentID = badgeAssignID;
    }

    public String getBadgeNumber()
    {
        return badgeNumber;
    }

    public String getModifiedBadgeNumber(int numDigitsToDisplay)
    {
        String modBadgeNum = badgeNumber;

        if (numDigitsToDisplay > modBadgeNum.length())
        {
            numDigitsToDisplay = modBadgeNum.length();
        }

        if (numDigitsToDisplay != modBadgeNum.length())
        {
            for (int x = 0; x < modBadgeNum.length(); x++)
            {
                if (x == 0)
                {
                    modBadgeNum = "X" + modBadgeNum.substring(x + 1, modBadgeNum.length());
                }
                else if ((x == numDigitsToDisplay - 1) && (numDigitsToDisplay == modBadgeNum.length()))
                {
                    modBadgeNum = modBadgeNum.substring(0, modBadgeNum.length() - 1) + "X";
                }
                else
                {
                    modBadgeNum = modBadgeNum.substring(0, x) + "X" + modBadgeNum.substring(x + 1, modBadgeNum.length());
                }

                if ((modBadgeNum.length() - (x + 1)) == numDigitsToDisplay)
                {
                    break;
                }
            }
        }

        return modBadgeNum;
    }

    public void setBadgeNumber(String newBadgeNum)
    {
        badgeNumber = newBadgeNum;
    }

    public String getEmployeeID()
    {
        return employeeID;
    }

    public void setEmployeeID(String empID)
    {
        employeeID = empID;
    }

    public String getEmployeeNumber()
    {
        return employeeNumber;
    }

    public void setEmployeeNumber(String newEmpNumber)
    {
        employeeNumber = newEmpNumber;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String newName)
    {
        name = newName;
    }

    public int getNumSplits()
    {
        return numSplits;
    }

    public void setNumSplits(int numSplits)
    {
        this.numSplits = numSplits;
    }

    public void setDailyLimitBal(double newDailyLimitBal)
    {
        dailyLimitBal = newDailyLimitBal;
    }

    public double getDailyLimitBal()
    {
        return dailyLimitBal;
    }

    public double getGlobalAvailable()
    {
        return globalAvailable;
    }

    public void setGlobalAvailable(double newGlobalAvail)
    {
        globalAvailable = newGlobalAvail;
    }

    public double getGlobalBalance()
    {
        return globalBalance;
    }

    public void setGlobalBalance(double newGlobalBal)
    {
        globalBalance = newGlobalBal;
    }

    public double getStoreAvailable()
    {
        return storeAvailable;
    }

    public void setStoreAvailable(double newStoreAvail)
    {
        storeAvailable = newStoreAvail;
    }

    public double getStoreBalance()
    {
        return storeBalance;
    }

    public void setStoreBalance(double newStoreBal)
    {
        storeBalance = newStoreBal;
    }

    public int getReceiptBalanceTypeID()
    {
        return receiptBalanceTypeID;
    }

    public void setReceiptBalanceTypeID(int newRcptBalTypeID)
    {
        receiptBalanceTypeID = newRcptBalTypeID;
    }

    public String getReceiptBalancePromptTxt()
    {
        return receiptBalancePromptTxt;
    }

    public void setReceiptBalancePromptTxt(String newRcptBalPromptText)
    {
        receiptBalancePromptTxt = newRcptBalPromptText;
    }
}
