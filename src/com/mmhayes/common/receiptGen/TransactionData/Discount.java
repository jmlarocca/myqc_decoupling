package com.mmhayes.common.receiptGen.TransactionData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-21 11:55:42 -0400 (Mon, 21 Sep 2020) $: Date of last commit
    $Rev: 12711 $: Revision of last commit
    Notes: Represents a Discount transaction line item.
*/

import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.product.models.ItemDiscountModel;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.DiscountLineItemModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by nyu on 10/13/2016.
 */
public class Discount extends TransactionItem
{
    // Identifying attributes
    int itemTypeID = TypeData.ItemType.DISCOUNT;
    int discountID;
    String shortName = "";

    // Other
    boolean isSubtotalDiscount = false;
    int discountTypeID = 0;

    // These attributes would only come from QCPOS
    double rateCouponAmt = 0.0;
    double openDsctPercentageAmt = 0.0;

    /**
     * <p>Empty constructor for a {@link Discount}.</p>
     *
     */
    public Discount () {}

    /**
     * <p>Constructor for a {@link Discount} with it's discountID field set.</p>
     *
     * @param discountID The ID of the discount.
     * @return The {@link Discount} instance with it's discountID field set.
     */
    public Discount discountID (int discountID) {
        this.discountID = discountID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Discount} with it's name field set.</p>
     *
     * @param name The name {@link String} of the discount.
     * @return The {@link Discount} instance with it's name field set.
     */
    public Discount name (String name) {
        this.name = name;
        return this;
    }

    /**
     * <p>Constructor for a {@link Discount} with it's amount field set.</p>
     *
     * @param amount The amount of the transaction line item.
     * @return The {@link Discount} instance with it's amount field set.
     */
    public Discount amount (double amount) {
        this.amount = amount;
        return this;
    }

    /**
     * <p>Constructor for a {@link Discount} with it's quantity field set.</p>
     *
     * @param quantity The quantity of the transaction line item.
     * @return The {@link Discount} instance with it's quantity field set.
     */
    public Discount quantity (double quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * <p>Constructor for a {@link Discount} with it's paTransLineItemID field set.</p>
     *
     * @param paTransLineItemID The transaction line item ID of the discount transaction line item.
     * @return The {@link Discount} instance with it's paTransLineItemID field set.
     */
    public Discount paTransLineItemID (int  paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Discount} with it's isSubtotalDiscount field set.</p>
     *
     * @param isSubtotalDiscount Whether or not the discount is a subtotal discount.
     * @return The {@link Discount} instance with it's isSubtotalDiscount field set.
     */
    public Discount isSubtotalDiscount (boolean isSubtotalDiscount) {
        this.isSubtotalDiscount = isSubtotalDiscount;
        return this;
    }

    /**
     * <p>Constructor for a {@link Discount} with it's discountTypeID field set.</p>
     *
     * @param discountTypeID The ID corresponding to the discount's type.
     * @return The {@link Discount} instance with it's discountTypeID field set.
     */
    public Discount discountTypeID (int  discountTypeID) {
        this.discountTypeID = discountTypeID;
        return this;
    }

    /**
     * <p>Constructor for a {@link Discount} with it's shortName field set.</p>
     *
     * @param shortName The short name {@link String} for the discount.
     * @return The {@link Discount} instance with it's shortName field set.
     */
    public Discount shortName (String shortName) {
        this.shortName = shortName;
        return this;
    }

    /**
     * <p>Constructor for a {@link Discount} with it's rateCouponAmt field set.</p>
     *
     * @param rateCouponAmt The rate coupon amount.
     * @return The {@link Discount} instance with it's rateCouponAmt field set.
     */
    public Discount rateCouponAmt (double rateCouponAmt) {
        this.rateCouponAmt = rateCouponAmt;
        return this;
    }

    /**
     * <p>Constructor for a {@link Discount} with it's openDsctPercentageAmt field set.</p>
     *
     * @param openDsctPercentageAmt The open discount percentage amount.
     * @return The {@link Discount} instance with it's openDsctPercentageAmt field set.
     */
    public Discount openDsctPercentageAmt (double openDsctPercentageAmt) {
        this.openDsctPercentageAmt = openDsctPercentageAmt;
        return this;
    }

    //--------------------------------------------------------------------------
    //  Static methods
    //--------------------------------------------------------------------------
    public static Discount createDiscountFromDB(HashMap dbData)
    {
        Discount dsct = new Discount();

        dsct.discountID = HashMapDataFns.getIntVal(dbData, "PAITEMID");
        dsct.name = HashMapDataFns.getStringVal(dbData, "ITEMNAME");
        dsct.shortName = HashMapDataFns.getStringVal(dbData, "SHORTNAME");
        dsct.paTransLineItemID = HashMapDataFns.getIntVal(dbData, "PATRANSLINEITEMID");

        dsct.quantity = HashMapDataFns.getDoubleVal(dbData, "QUANTITY");
        dsct.refundedQty = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDQUANTITY");

        dsct.amount = HashMapDataFns.getDoubleVal(dbData, "AMOUNT");
        dsct.refundedAmount = HashMapDataFns.getDoubleVal(dbData, "REFUNDEDAMOUNT");

        dsct.isSubtotalDiscount = HashMapDataFns.getBooleanVal(dbData, "SUBTOTALDISCOUNT");
        dsct.discountTypeID = HashMapDataFns.getIntVal(dbData, "DISCOUNTTYPEID");

        return dsct;
    }

    public static Discount createDiscountFromXMLRPC(ArrayList dsctData, int transTypeID)
    {
        Discount dsct = new Discount();

        try {
            dsct.discountID = new Integer(dsctData.get(9).toString()).intValue();
            dsct.name = dsctData.get(1).toString();
            dsct.amount = new Double(dsctData.get(2).toString()).doubleValue();
            dsct.quantity = new Double(dsctData.get(4).toString()).doubleValue();
            dsct.isSubtotalDiscount = new Boolean(dsctData.get(5).toString()).booleanValue();
            dsct.discountTypeID = new Integer(dsctData.get(6).toString()).intValue();
            dsct.shortName = dsctData.get(8).toString();

            // These are QCPOS specific
            dsct.rateCouponAmt = new Double(dsctData.get(3).toString()).doubleValue();
            dsct.openDsctPercentageAmt = new Double(dsctData.get(7).toString()).doubleValue();

            // Amounts should always be negative
            if (dsct.amount > 0)
            {
                dsct.amount = dsct.amount * -1;
            }

            if (transTypeID == TypeData.TranType.REFUND || transTypeID == TypeData.TranType.VOID)
            {
                // Quantities should be negative for voids and refunds
                if (dsct.quantity > 0)
                {
                    dsct.quantity = dsct.quantity * -1;
                }
            }
        } catch (Exception e) {
            ReceiptGen.logError("Discount.createDiscountFromXMLRPC error", e);
        }

        return dsct;
    }

    /**
     * <p>Builds a {@link Discount} from the given {@link XmlRpcStruct}.</p>
     *
     * @param discountStruct The {@link XmlRpcStruct} to use to build the {@link Discount}.
     * @return The {@link Discount} instance built form a {@link XmlRpcStruct}.
     */
    public static Discount buildFromXmlRpcStruct (XmlRpcStruct discountStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(discountStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to Discount.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new Discount()
                .discountID(HashMapDataFns.getIntVal(discountStruct, "DISCOUNTID", true))
                .name(HashMapDataFns.getStringVal(discountStruct, "NAME", true))
                .amount(HashMapDataFns.getDoubleVal(discountStruct, "AMOUNT", true))
                .quantity(HashMapDataFns.getDoubleVal(discountStruct, "QUANTITY", true))
                .paTransLineItemID(HashMapDataFns.getIntVal(discountStruct, "PATRANSLINEITEMID", true))
                .isSubtotalDiscount(HashMapDataFns.getBooleanVal(discountStruct, "ISSUBTOTALDISCOUNT", true))
                .discountTypeID(HashMapDataFns.getIntVal(discountStruct, "DISCOUNTTYPEID", true))
                .shortName(HashMapDataFns.getStringVal(discountStruct, "SHORTNAME", true))
                .rateCouponAmt(HashMapDataFns.getDoubleVal(discountStruct, "RATECOUPONAMT", true))
                .openDsctPercentageAmt(HashMapDataFns.getDoubleVal(discountStruct, "OPENDSCTPERCENTAGEAMT", true));
    }

    public static Discount createDiscount(DiscountLineItemModel discountLineItemModel, TransactionModel transactionModel)
    {
        Discount dsct = new Discount();

        try {
            dsct.discountID = discountLineItemModel.getDiscount().getId();
            dsct.name = discountLineItemModel.getDiscount().getName();
            dsct.amount = discountLineItemModel.getAmount().doubleValue();
            dsct.quantity = discountLineItemModel.getQuantity().doubleValue();
            dsct.isSubtotalDiscount = discountLineItemModel.getDiscount().isSubTotalDiscount();
            dsct.discountTypeID = discountLineItemModel.getDiscount().getDiscountTypeId();
            dsct.shortName = discountLineItemModel.getDiscount().getShortName();
            // These are QCPOS specific
            dsct.rateCouponAmt = discountLineItemModel.getDiscount().getAmount().doubleValue();

            // Amounts should always be negative
            if (dsct.amount > 0)
            {
                dsct.amount = dsct.amount * -1;
            }

            if (transactionModel.getTransactionTypeId() == TypeData.TranType.REFUND || transactionModel.getTransactionTypeId() == TypeData.TranType.VOID)
            {
                // Quantities should be negative for voids and refunds
                if (dsct.quantity > 0)
                {
                    dsct.quantity = dsct.quantity * -1;
                }
            }
        } catch (Exception e) {
            ReceiptGen.logError("Discount.createDiscount error", e);
        }

        return dsct;
    }

    public static Discount createDiscount(ItemDiscountModel itemDiscountModel, TransactionModel transactionModel)
    {
        Discount dsct = new Discount();

        try {
            dsct.discountID = itemDiscountModel.getDiscount().getId();
            dsct.name = itemDiscountModel.getDiscount().getName();
            dsct.amount = itemDiscountModel.getAmount().doubleValue();
            dsct.quantity = 1; //TODO: do i need to change this
            dsct.isSubtotalDiscount = itemDiscountModel.getDiscount().isSubTotalDiscount();
            dsct.discountTypeID = itemDiscountModel.getDiscount().getDiscountTypeId();
            dsct.shortName = itemDiscountModel.getDiscount().getShortName();
            // These are QCPOS specific
            dsct.rateCouponAmt = itemDiscountModel.getDiscount().getAmount().doubleValue();

            // Amounts should always be negative
            if (dsct.amount > 0)
            {
                dsct.amount = dsct.amount * -1;
            }

            if (transactionModel.getTransactionTypeId() == TypeData.TranType.REFUND || transactionModel.getTransactionTypeId() == TypeData.TranType.VOID)
            {
                // Quantities should be negative for voids and refunds
                if (dsct.quantity > 0)
                {
                    dsct.quantity = dsct.quantity * -1;
                }
            }
        } catch (Exception e) {
            ReceiptGen.logError("Discount.createDiscount error", e);
        }

        return dsct;
    }

    //--------------------------------------------------------------------------
    //  Getters and Setters
    //--------------------------------------------------------------------------
    public int getDiscountID()
    {
        return discountID;
    }

    public int getDiscountTypeID()
    {
        return discountTypeID;
    }

    public boolean getIsSubtotalDsct()
    {
        return isSubtotalDiscount;
    }

    public String getShortName()
    {
        return shortName;
    }

    /**
     * <p>Overridden toString() method for a {@link Discount} instance.</p>
     *
     * @return The {@link Discount} instance as a {@link String};
     */
    @Override
    public String toString () {

        return String.format("DISCOUNTID: %s, NAME: %s, AMOUNT: %s, QUANTITY: %s, PATRANSLINEITEMID: %s, ISSUBTOTALDISCOUNT: %s, " +
                "DISCOUNTTYPEID: %s, SHORTNAME: %s, RATECOUPONAMT: %s, OPENDSCTPERCENTAGEAMT: %s",
                Objects.toString((discountID > 0 ? discountID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((amount > 0.0d ? amount : "N/A"), "N/A"),
                Objects.toString((quantity > 0.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((paTransLineItemID > 0 ? paTransLineItemID : "N/A"), "N/A"),
                Objects.toString(isSubtotalDiscount, "N/A"),
                Objects.toString((discountTypeID > 0 ? discountTypeID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((rateCouponAmt > 0.0d ? rateCouponAmt : "N/A"), "N/A"),
                Objects.toString((openDsctPercentageAmt > 0.0d ? openDsctPercentageAmt : "N/A"), "N/A"));
    }

    /**
     * <p>Overridden equals() method for a {@link Discount} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link Discount} instance.
     * @return Whether or not the {@link Object} is equal to the {@link Discount} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a Discount
        Discount discount = (Discount) obj;
        return Objects.equals(discount.discountID, discountID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link Discount} instance.</p>
     *
     * @return The hash code for a {@link Discount} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(discountID);
    }
}
