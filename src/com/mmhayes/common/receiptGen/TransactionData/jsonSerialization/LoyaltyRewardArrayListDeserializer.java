package com.mmhayes.common.receiptGen.TransactionData.jsonSerialization;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-24 15:03:29 -0400 (Thu, 24 Sep 2020) $: Date of last commit
    $Rev: 12743 $: Revision of last commit
    Notes: Converts JSON into an ArrayList of LoyaltyReward.
*/

import com.google.gson.*;
import com.mmhayes.common.receiptGen.TransactionData.LoyaltyReward;
import com.mmhayes.common.utils.Logger;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

public class LoyaltyRewardArrayListDeserializer implements JsonDeserializer<ArrayList<LoyaltyReward>> {
    
    /**
     * <p>Overridden serialize method for converting a {@link JsonElement} into an {@link ArrayList} of {@link LoyaltyReward}.</p>
     *
     * @param jsonElement The {@link JsonElement} to convert into an {@link ArrayList} of {@link LoyaltyReward}.
     * @param type The {@link Type} of the expected return value.
     * @param jsonDeserializationContext Context for deserialization that is passed to a custom deserializer during invocation
     * of it's JsonDeserializer.deserialize(JsonElement, Type, JsonDeserializationContext) method.
     * @return The {@link ArrayList} of {@link LoyaltyReward} created from the {@link JsonElement}.
     * @throws JsonParseException
     */
    @SuppressWarnings("WhileLoopReplaceableByForEach")
    @Override
    public ArrayList<LoyaltyReward> deserialize (JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

        if (jsonElement == null) {
            Logger.logMessage("The JsonElement passed to LoyaltyRewardArrayListDeserializer.deserialize can't be null!", Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<LoyaltyReward> loyaltyRewards = new ArrayList<>();
        JsonArray loyaltyRewardsJSONArr = jsonElement.getAsJsonArray();
        Iterator<JsonElement> loyaltyRewardsJSONItr = loyaltyRewardsJSONArr.iterator();
        while (loyaltyRewardsJSONItr.hasNext()) {
            JsonObject loyaltyRewardJSON = loyaltyRewardsJSONItr.next().getAsJsonObject();
            if (loyaltyRewardJSON != null) {

                // loyalty reward properties needed to build a LoyaltyReward
                int rewardID = -1;
                String name = null;
                String shortName = null;
                int pointsToRedeem = -1;
                int rewardTypeID = -1;
                double amount = -1.0d;
                double quantity = -1.0d;
                boolean applyToAllPLUs = false;

                for (Map.Entry<String, JsonElement> loyaltyRewardJSONEntry : loyaltyRewardJSON.entrySet()) {
                    switch (loyaltyRewardJSONEntry.getKey()) {
                        case "rewardID":
                            rewardID = loyaltyRewardJSONEntry.getValue().getAsInt();
                            break;
                        case "name":
                            name = loyaltyRewardJSONEntry.getValue().getAsString();
                            break;
                        case "shortName":
                            shortName = loyaltyRewardJSONEntry.getValue().getAsString();
                            break;
                        case "pointsToRedeem":
                            pointsToRedeem = loyaltyRewardJSONEntry.getValue().getAsInt();
                            break;
                        case "rewardTypeID":
                            rewardTypeID = loyaltyRewardJSONEntry.getValue().getAsInt();
                            break;
                        case "amount":
                            amount = loyaltyRewardJSONEntry.getValue().getAsDouble();
                            break;
                        case "quantity":
                            quantity = loyaltyRewardJSONEntry.getValue().getAsDouble();
                            break;
                        case "applyToAllPLUs":
                            applyToAllPLUs = loyaltyRewardJSONEntry.getValue().getAsBoolean();
                            break;
                        default:
                            Logger.logMessage(String.format("Encountered an invalid key of %s while parsing the loyalty reward JSON in LoyaltyRewardArrayListDeserializer.deserialize!",
                                    Objects.toString(loyaltyRewardJSONEntry.getKey(), "N/A")), Logger.LEVEL.ERROR);
                            break;
                    }
                }

                // build and add the loyalty reward to the ArrayList of LoyaltyReward
                loyaltyRewards.add(new LoyaltyReward()
                        .rewardID(rewardID)
                        .name(name)
                        .shortName(shortName)
                        .pointsToRedeem(pointsToRedeem)
                        .rewardTypeID(rewardTypeID)
                        .amount(amount)
                        .quantity(quantity)
                        .applyToAllPLUs(applyToAllPLUs));
            }
        }

        return loyaltyRewards;
    }

}