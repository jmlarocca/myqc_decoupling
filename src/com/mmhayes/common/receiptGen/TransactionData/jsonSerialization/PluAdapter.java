package com.mmhayes.common.receiptGen.TransactionData.jsonSerialization;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-24 15:03:29 -0400 (Thu, 24 Sep 2020) $: Date of last commit
    $Rev: 12743 $: Revision of last commit
    Notes: Converts a Plu into JSON.
*/

import com.google.gson.*;
import com.mmhayes.common.receiptGen.TransactionData.LoyaltyReward;
import com.mmhayes.common.receiptGen.TransactionData.Plu;
import com.mmhayes.common.receiptGen.TransactionData.PrepOption;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;

import java.lang.reflect.Type;

public class PluAdapter implements JsonSerializer<Plu> {

    /**
     * <p>Overridden serialize method for converting a {@link Plu} to JSON.</p>
     *
     * @param plu The {@link Plu} to convert into JSON.
     * @param type The fully genericized version of the source {@link Type}.
     * @param jsonSerializationContext Context for serialization that is passed to a custom serializer during invocation of
     * it's JsonSerializer.serialize(Object, Type, JsonSerializationContext) method.
     * @return A {@link JsonElement} corresponding to the converted {@link Plu}.
     */
    @Override
    public JsonElement serialize (Plu plu, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject pluJSON = new JsonObject();

        if (plu == null) {
            Logger.logMessage("The product passed to PluAdapter.serialize can't be null!", Logger.LEVEL.ERROR);
            return null;
        }

        Gson gson = new Gson();

        pluJSON.addProperty("quantity", plu.getQuantity());
        pluJSON.addProperty("amount", plu.getAmount());
        pluJSON.addProperty("itemComment", plu.getItemComment());
        pluJSON.addProperty("isManualWeightedItem", plu.getIsManualWeight());
        pluJSON.addProperty("name", plu.getName());
        pluJSON.addProperty("isWeightedItem", plu.getIsWeightedItem());
        pluJSON.addProperty("netWeight", plu.getNetWeight());
        pluJSON.addProperty("pluCode", plu.getPluCode());
        pluJSON.addProperty("taxDsctStr", plu.getTaxDsctStr());
        pluJSON.addProperty("isModifier", plu.getIsModifier());
        pluJSON.addProperty("originalOrderNum", plu.getOriginalOrderNum());
        pluJSON.addProperty("hasModifiers", plu.hasModifiers());
        JsonArray freeProductRwdsJSON = new JsonArray();
        if (!DataFunctions.isEmptyCollection(plu.getFreeProductRwds())) {
            gson = new GsonBuilder().registerTypeAdapter(LoyaltyReward.class, new LoyaltyRewardAdapter()).create();
            for (LoyaltyReward loyaltyReward : plu.getFreeProductRwds()) {
                JsonObject freeProductRwdJSON = gson.toJsonTree(loyaltyReward, LoyaltyReward.class).getAsJsonObject();
                freeProductRwdsJSON.add(freeProductRwdJSON);
            }
        }
        pluJSON.add("freeProductRwds", freeProductRwdsJSON);
        pluJSON.addProperty("paTransLineItemID", plu.getPATransLineItemID());
        pluJSON.addProperty("paTransLineItemModID", plu.getPaTransLineItemModID());
        // serialize the ArrayList<Integer> of reward IDs
        pluJSON.add("rewardIDs", gson.toJsonTree(plu.getRewardIDs()).getAsJsonArray());
        // serialize the PrepOption
        JsonArray prepOptionsJSON = new JsonArray();
        if (plu.getPrepOption() != null) {
            gson = new GsonBuilder().registerTypeAdapter(PrepOption.class, new PrepOptionAdapter()).create();
            JsonObject prepOptionJSON = gson.toJsonTree(plu.getPrepOption(), PrepOption.class).getAsJsonObject();
            prepOptionsJSON.add(prepOptionJSON);
        }
        pluJSON.add("prepOption", prepOptionsJSON);
        pluJSON.addProperty("grossWeight", (plu.getGrossWeight() != null ? plu.getGrossWeight().toPlainString() : ""));
        pluJSON.addProperty("fixedNetWeight", (plu.getFixedNetWeight() != null ? plu.getFixedNetWeight().toPlainString() : ""));
        pluJSON.addProperty("paComboTransLineItemID", plu.getPaComboTransLineItemID());
        pluJSON.addProperty("paComboDetailID", plu.getPaComboDetailID());
        pluJSON.addProperty("basePrice", plu.getBasePrice());
        pluJSON.addProperty("comboPrice", plu.getComboPrice());

        return pluJSON;
    }

}