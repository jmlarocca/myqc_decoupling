package com.mmhayes.common.receiptGen.TransactionData.jsonSerialization;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-24 15:03:29 -0400 (Thu, 24 Sep 2020) $: Date of last commit
    $Rev: 12743 $: Revision of last commit
    Notes: Converts JSON into an ArrayList of Plu.
*/

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.receiptGen.TransactionData.LoyaltyReward;
import com.mmhayes.common.receiptGen.TransactionData.Plu;
import com.mmhayes.common.receiptGen.TransactionData.PrepOption;
import com.mmhayes.common.utils.Logger;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

public class PluArrayListDeserializer implements JsonDeserializer<ArrayList<Plu>> {

    /**
     * <p>Overridden serialize method for converting a {@link JsonElement} into an {@link ArrayList} of {@link Plu}.</p>
     *
     * @param jsonElement The {@link JsonElement} to convert into an {@link ArrayList} of {@link Plu}.
     * @param type The {@link Type} of the expected return value.
     * @param jsonDeserializationContext Context for deserialization that is passed to a custom deserializer during invocation
     * of it's JsonDeserializer.deserialize(JsonElement, Type, JsonDeserializationContext) method.
     * @return The {@link ArrayList} of {@link Plu} created from the {@link JsonElement}.
     * @throws JsonParseException
     */
    @SuppressWarnings({"ConstantConditions", "WhileLoopReplaceableByForEach"})
    @Override
    public ArrayList<Plu> deserialize (JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

        if (jsonElement == null) {
            Logger.logMessage("The JsonElement passed to PluArrayListDeserializer.deserialize can't be null!", Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<Plu> products = new ArrayList<>();
        Gson gson = new Gson();
        JsonArray productsJSONArr = jsonElement.getAsJsonArray();
        Iterator<JsonElement> productsJSONItr = productsJSONArr.iterator();
        while (productsJSONItr.hasNext()) {
            JsonObject productJSON = productsJSONItr.next().getAsJsonObject();
            if (productJSON != null) {
                Logger.logMessage("PLU JSON => "+productJSON.toString(), CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.IMPORTANT);

                // product properties needed to build a Plu
                double quantity = -1.0d;
                double amount = -1.0d;
                String itemComment = null;
                boolean isManualWeightedItem = false;
                String name = null;
                boolean isWeightedItem = false;
                double netWeight = -1.0d;
                String pluCode = null;
                String taxDsctStr = null;
                boolean isModifier = false;
                String originalOrderNum = null;
                boolean hasModifiers = false;
                ArrayList<LoyaltyReward> freeProductRwds = null;
                int paTransLineItemID = -1;
                int paTransLineItemModID = -1;
                ArrayList<Integer> rewardIDs = null;
                PrepOption prepOption = null;
                BigDecimal grossWeight = null;
                BigDecimal fixedNetWeight = null;
                int paComboTransLineItemID = -1;
                int paComboDetailID = -1;
                double basePrice = -1.0d;
                double comboPrice = -1.0d;

                // iterate through the key value pairs within the product's JSON
                for (Map.Entry<String, JsonElement> productJSONEntry : productJSON.entrySet()) {
                    switch (productJSONEntry.getKey()) {
                        case "quantity":
                            quantity = productJSONEntry.getValue().getAsDouble();
                            break;
                        case "amount":
                            amount = productJSONEntry.getValue().getAsDouble();
                            break;
                        case "itemComment":
                            itemComment = productJSONEntry.getValue().getAsString();
                            break;
                        case "isManualWeightedItem":
                            isManualWeightedItem = productJSONEntry.getValue().getAsBoolean();
                            break;
                        case "name":
                            name = productJSONEntry.getValue().getAsString();
                            break;
                        case "isWeightedItem":
                            isWeightedItem = productJSONEntry.getValue().getAsBoolean();
                            break;
                        case "netWeight":
                            netWeight = productJSONEntry.getValue().getAsDouble();
                            break;
                        case "pluCode":
                            pluCode = productJSONEntry.getValue().getAsString();
                            break;
                        case "taxDsctStr":
                            taxDsctStr = productJSONEntry.getValue().getAsString();
                            break;
                        case "isModifier":
                            isModifier = productJSONEntry.getValue().getAsBoolean();
                            break;
                        case "originalOrderNum":
                            originalOrderNum = productJSONEntry.getValue().getAsString();
                            break;
                        case "hasModifiers":
                            hasModifiers = productJSONEntry.getValue().getAsBoolean();
                            break;
                        case "freeProductRwds":
                            Type freeProductRwdsListType = new TypeToken<ArrayList<LoyaltyReward>>(){}.getType();
                            gson = new GsonBuilder().registerTypeAdapter(freeProductRwdsListType, new LoyaltyRewardArrayListDeserializer()).create();
                            freeProductRwds = gson.fromJson(productJSONEntry.getValue(), freeProductRwdsListType);
                            break;
                        case "paTransLineItemID":
                            paTransLineItemID = productJSONEntry.getValue().getAsInt();
                            break;
                        case "paTransLineItemModID":
                            paTransLineItemModID = productJSONEntry.getValue().getAsInt();
                            break;
                        case "rewardIDs":
                            Type rewardIDsType = new TypeToken<ArrayList<Integer>>(){}.getType();
                            rewardIDs = gson.fromJson(productJSONEntry.getValue(), rewardIDsType);
                            break;
                        case "prepOption":
                            JsonArray prepOptionJSONArr = productJSONEntry.getValue().getAsJsonArray();
                            Iterator<JsonElement> prepOptionJSONItr = prepOptionJSONArr.iterator();
                            while (prepOptionJSONItr.hasNext()) {
                                JsonObject prepOptionJSON = prepOptionJSONItr.next().getAsJsonObject();
                                if (prepOptionJSON != null) {

                                    // prep option properties needed to build a PrepOption
                                    int prepOptionID = -1;
                                    int prepOptionSetID = -1;
                                    double prepOptionPrice = -1.0d;
                                    String prepName = null;
                                    String originalName = null;
                                    boolean displayDefaultReceipts = false;
                                    boolean displayDefaultKitchen = false;
                                    boolean defaultOption = false;

                                    for (Map.Entry<String, JsonElement> prepOptionJSONEntry : prepOptionJSON.entrySet()) {
                                        switch (prepOptionJSONEntry.getKey()) {
                                            case "prepOptionID":
                                                prepOptionID = prepOptionJSONEntry.getValue().getAsInt();
                                                break;
                                            case "prepOptionSetID":
                                                prepOptionSetID = prepOptionJSONEntry.getValue().getAsInt();
                                                break;
                                            case "prepOptionPrice":
                                                prepOptionPrice = prepOptionJSONEntry.getValue().getAsDouble();
                                                break;
                                            case "name":
                                                prepName = prepOptionJSONEntry.getValue().getAsString();
                                                break;
                                            case "originalName":
                                                originalName = prepOptionJSONEntry.getValue().getAsString();
                                                break;
                                            case "displayDefaultReceipts":
                                                displayDefaultReceipts = prepOptionJSONEntry.getValue().getAsBoolean();
                                                break;
                                            case "displayDefaultKitchen":
                                                displayDefaultKitchen = prepOptionJSONEntry.getValue().getAsBoolean();
                                                break;
                                            case "defaultOption":
                                                defaultOption = prepOptionJSONEntry.getValue().getAsBoolean();
                                                break;
                                            default:
                                                Logger.logMessage(String.format("Encountered an invalid key of %s while parsing the prep option JSON in PluArrayListDeserializer.deserialize!",
                                                        Objects.toString(prepOptionJSONEntry.getKey(), "N/A")), Logger.LEVEL.ERROR);
                                                break;
                                        }
                                    }

                                    // build the PrepOption
                                    prepOption = new PrepOption()
                                            .prepOptionID(prepOptionID)
                                            .prepOptionSetID(prepOptionSetID)
                                            .prepOptionPrice(prepOptionPrice)
                                            .name(prepName)
                                            .originalName(originalName)
                                            .displayDefaultReceipts(displayDefaultReceipts)
                                            .displayDefaultKitchen(displayDefaultKitchen)
                                            .defaultOption(defaultOption);
                                }
                            }
                            break;
                        case "grossWeight":
                            grossWeight = productJSONEntry.getValue().getAsBigDecimal();
                            break;
                        case "fixedNetWeight":
                            fixedNetWeight = productJSONEntry.getValue().getAsBigDecimal();
                            break;
                        case "paComboTransLineItemID":
                            paComboTransLineItemID = productJSONEntry.getValue().getAsInt();
                            break;
                        case "paComboDetailID":
                            paComboDetailID = productJSONEntry.getValue().getAsInt();
                            break;
                        case "basePrice":
                            basePrice = productJSONEntry.getValue().getAsDouble();
                            break;
                        case "comboPrice":
                            comboPrice = productJSONEntry.getValue().getAsDouble();
                            break;
                        default:
                            Logger.logMessage(String.format("Encountered an invalid key of %s while parsing the product JSON in PluArrayListDeserializer.deserialize!",
                                    Objects.toString(productJSONEntry.getKey(), "N/A")), Logger.LEVEL.ERROR);
                            break;
                    }
                }

                // build the Plu
                products.add(new Plu()
                        .quantity(quantity)
                        .amount(amount)
                        .itemComment(itemComment)
                        .isManualWeightedItem(isManualWeightedItem)
                        .name(name)
                        .isWeightedItem(isWeightedItem)
                        .netWeight(netWeight)
                        .pluCode(pluCode)
                        .taxDsctStr(taxDsctStr)
                        .isModifier(isModifier)
                        .originalOrderNum(originalOrderNum)
                        .hasModifiers(hasModifiers)
                        .freeProductRwds(freeProductRwds)
                        .paTransLineItemID(paTransLineItemID)
                        .paTransLineItemModID(paTransLineItemModID)
                        .rewardIDs(rewardIDs)
                        .prepOption(prepOption)
                        .grossWeight(grossWeight)
                        .fixedNetWeight(fixedNetWeight)
                        .paComboTransLineItemID(paComboTransLineItemID)
                        .paComboDetailID(paComboDetailID)
                        .basePrice(basePrice)
                        .comboPrice(comboPrice));
            }
        }

        return products;
    }

}