package com.mmhayes.common.receiptGen.TransactionData.jsonSerialization;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-24 15:03:29 -0400 (Thu, 24 Sep 2020) $: Date of last commit
    $Rev: 12743 $: Revision of last commit
    Notes: Converts a PrepOption into JSON.
*/

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.mmhayes.common.receiptGen.TransactionData.PrepOption;
import com.mmhayes.common.utils.Logger;

import java.lang.reflect.Type;

public class PrepOptionAdapter implements JsonSerializer<PrepOption> {

    /**
     * <p>Overridden serialize method for converting a {@link PrepOption} to JSON.</p>
     *
     * @param prepOption The {@link PrepOption} to convert into JSON.
     * @param type The fully genericized version of the source {@link Type}.
     * @param jsonSerializationContext Context for serialization that is passed to a custom serializer during invocation of
     * it's JsonSerializer.serialize(Object, Type, JsonSerializationContext) method.
     * @return A {@link JsonElement} corresponding to the converted {@link PrepOption}.
     */
    @Override
    public JsonElement serialize (PrepOption prepOption, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject prepOptionJSON = new JsonObject();

        if (prepOption == null) {
            Logger.logMessage("The prep option passed to PrepOptionAdapter.serialize can't be null!", Logger.LEVEL.ERROR);
            return null;
        }

        prepOptionJSON.addProperty("prepOptionID", prepOption.getPrepOptionID());
        prepOptionJSON.addProperty("prepOptionSetID", prepOption.getPrepOptionSetID());
        prepOptionJSON.addProperty("prepOptionPrice", prepOption.getPrepOptionPrice());
        prepOptionJSON.addProperty("name", prepOption.getName());
        prepOptionJSON.addProperty("originalName", prepOption.getOriginalName());
        prepOptionJSON.addProperty("displayDefaultReceipts", prepOption.getDisplayDefaultReceipts());
        prepOptionJSON.addProperty("displayDefaultKitchen", prepOption.getDisplayDefaultKitchen());
        prepOptionJSON.addProperty("defaultOption", prepOption.getDefaultOption());

        return prepOptionJSON;
    }

}