package com.mmhayes.common.receiptGen.TransactionData.jsonSerialization;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-24 15:03:29 -0400 (Thu, 24 Sep 2020) $: Date of last commit
    $Rev: 12743 $: Revision of last commit
    Notes: Converts a LoyaltyReward into JSON.
*/

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.mmhayes.common.receiptGen.TransactionData.LoyaltyReward;
import com.mmhayes.common.utils.Logger;

import java.lang.reflect.Type;

public class LoyaltyRewardAdapter implements JsonSerializer<LoyaltyReward> {

    /**
     * <p>Overridden serialize method for converting a {@link LoyaltyReward} to JSON.</p>
     *
     * @param loyaltyReward The {@link LoyaltyReward} to convert into JSON.
     * @param type The fully genericized version of the source {@link Type}.
     * @param jsonSerializationContext Context for serialization that is passed to a custom serializer during invocation of
     * it's JsonSerializer.serialize(Object, Type, JsonSerializationContext) method.
     * @return A {@link JsonElement} corresponding to the converted {@link LoyaltyReward}.
     */
    @Override
    public JsonElement serialize (LoyaltyReward loyaltyReward, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject loyaltyRewardJSON = new JsonObject();

        if (loyaltyReward == null) {
            Logger.logMessage("The loyalty reward passed to LoyaltyRewardAdapter.serialize can't be null!", Logger.LEVEL.ERROR);
            return null;
        }

        loyaltyRewardJSON.addProperty("rewardID", loyaltyReward.getRewardID());
        loyaltyRewardJSON.addProperty("name", loyaltyReward.getName());
        loyaltyRewardJSON.addProperty("shortName", loyaltyReward.getShortName());
        loyaltyRewardJSON.addProperty("pointsToRedeem", loyaltyReward.getPointsToRedeem());
        loyaltyRewardJSON.addProperty("rewardTypeID", loyaltyReward.getRewardTypeID());
        loyaltyRewardJSON.addProperty("amount", loyaltyReward.getAmount());
        loyaltyRewardJSON.addProperty("quantity", loyaltyReward.getQuantity());
        loyaltyRewardJSON.addProperty("applyToAllPLUs", loyaltyReward.getApplyToAllPLUs());

        return loyaltyRewardJSON;
    }
    
}