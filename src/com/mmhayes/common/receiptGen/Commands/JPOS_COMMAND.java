package com.mmhayes.common.receiptGen.Commands;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-11-10 11:36:52 -0500 (Tue, 10 Nov 2020) $: Date of last commit
    $Rev: 13079 $: Revision of last commit
    Notes: Contains commands for JPOS.
*/

import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * <p>Contains commands for JPOS.</p>
 *
 */
public class JPOS_COMMAND {

    public static final char ESC = '\u001b';
    public static final String ALIGN_CENTER = "|cA";
    public static final String ALIGN_RIGHT = "|rA";
    public static final String NORMAL_FONT = "|1C";
    public static final String LARGE_FONT = "|4C";
    public static final String CUT = "|100P";

    /**
     * <p>Removes and JPOS commands from the given {@link String}.</p>
     *
     * @param s The {@link String} to remove JPOS commands from.
     * @return The given {@link String} with all JPOS commands removed from the given {@link String}.
     */
    public static String removeCommands (String s) {

        if (!StringFunctions.stringHasContent(s)) {
            return "";
        }

        Field[] fields = JPOS_COMMAND.class.getDeclaredFields();
        String[] commands = new String[fields.length - 1];
        for (int i = 1; i < fields.length; i++) {
            try {
                commands[i - 1] = fields[0].get(null).toString() + fields[i].get(null).toString();
            }
            catch (Exception e) {}
        }

        String[] replacements = new String[commands.length];
        Arrays.fill(replacements, "");

        return StringUtils.replaceEach(s, commands, replacements);
    }

}