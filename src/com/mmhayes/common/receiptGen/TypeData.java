package com.mmhayes.common.receiptGen;


/*
 $Author: nyu $: Author of last commit
 $Date: 2021-08-17 15:07:53 -0400 (Tue, 17 Aug 2021) $: Date of last commit
 $Rev: 14939 $: Revision of last commit
 Notes: Shared constants
*/

import com.mmhayes.common.utils.Logger;

public class TypeData {

    public static class TranType
    {
        //////////////////////////////////////////////
        // Transaction types
        public final static int SALE            = 1;
        public final static int VOID            = 2;
        public final static int REFUND          = 3;
        public final static int TRAINING        = 4;
        public final static int CANCEL          = 5;
        public final static int ENDSHIFT        = 6;
        public final static int NOSALE          = 7;
        public final static int BROWSERCLOSE    = 8;
        public final static int LOGIN           = 9;
        public final static int LOGOUT          = 10;
        public final static int RA              = 11;
        public final static int PO              = 12;
        public final static int BATCHCLOSE      = 13;
        public final static int OPEN            = 15;
    }

    public static class ItemType
    {
        //////////////////////////////////////////////////////
        // Item types
        public final static int KEYPAD             = 1;
        public final static int PLU                = 2;
        public final static int TENDER             = 3;
        public final static int SUBDEPT            = 4;
        public final static int DEPT               = 5;
        public final static int TAX                = 6;
        public final static int DISCOUNT           = 7;
        public final static int PAIDOUT            = 10;
        public final static int RA                 = 11;
        public final static int TAXDELETE          = 12;
        public final static int LOYALTY_REWARD     = 30;
        public final static int SURCHARGE          = 32;
        public final static int COMBO              = 36;
    }

    public static class ReceiptType
    {
        // Represents different types of receipts
        public final static int PRINTEDRECEIPT      = 1;
        public final static int GIFTRECEIPT         = 2;
        public final static int EMAILEDRECEIPT      = 3;
        public final static int POPUPRECEIPT        = 4;
        public final static int KITCHENPTRRECEIPT   = 5;
    }

    public static class TenderType
    {
        public final static int CASH                = 1;
        public final static int CREDITCARD          = 2;
        public final static int CHECK               = 3;
        public final static int QUICKCHARGE         = 4;
        public final static int CHARGE              = 5;
        public final static int INTEGRATED_GIFT_CARD = 6;
        public final static int VOUCHER = 7;
    }

    // From QC_PAOrderType
    public static class OrderType
    {
        public static final int NORMAL_SALE  = 1;
        public static final int DELIVERY     = 2;
        public static final int PICKUP       = 3;
    }

    // From QC_PrinterType
    public static class PrinterType
    {
        public static final int KITCHEN_PRINTER             = 1;
        public static final int EXPEDITOR_PRINTER           = 2;
        public static final int LABEL_PRINTER               = 3;
        public static final int REMOTE_ORDER_PRINTER        = 4;
    }

    // From QC_LoyaltyRewardType
    public static class LoyaltyRewardType
    {
        public static final int ACCOUNT_CREDIT              = 1;
        public static final int TRANSACTION_CREDIT          = 2;
        public static final int TRANSACTION_PERCENT_OFF     = 3;
        public static final int FREE_PRODUCT                = 4;
    }

    public static class MerchantLinkReport
    {
        public static final int EMV_CONFIG_RPT      = 1;
        public static final int CHIP_TXN_RPT        = 2;
        public static final int GENERIC_RPT         = 3;
    }

    public static class TransactionStatus
    {
        public static final int SUSPENDED           = 1;
        public static final int EXPIRED             = 2;
        public static final int EDIT_COMPLETED      = 3;
        public static final int EDIT_IN_PROGRESS    = 4;
        public static final int COMPLETED           = 5;
    }

    public static class SurchargeApplicationType
    {
        public static final int SUBTOTAL            = 1;
        public static final int ITEM                = 2;
    }

    public static String getTransactionTypeName(int transTypeID)
    {
        String transTypeName = "";

        switch (transTypeID)
        {
            case TranType.SALE:
                transTypeName = "SALE";
                break;
            case TranType.REFUND:
                transTypeName = "REFUND";
                break;
            case TranType.VOID:
                transTypeName = "VOID";
                break;
            case TranType.TRAINING:
                transTypeName = "TRAINING";
                break;
            case TranType.CANCEL:
                transTypeName = "CANCEL";
                break;
            case TranType.ENDSHIFT:
                transTypeName = "CASHIER END SHIFT";
                break;
            case TranType.NOSALE:
                transTypeName = "NO SALE";
                break;
            case TranType.BROWSERCLOSE:
                transTypeName = "APP CLOSE";
                break;
            case TranType.LOGIN:
                transTypeName = "LOG IN";
                break;
            case TranType.LOGOUT:
                transTypeName = "LOG OUT";
                break;
            case TranType.RA:
                transTypeName = "RECEIVED ON ACCOUNT";
                break;
            case TranType.PO:
                transTypeName = "PAID OUT";
                break;
            case TranType.OPEN:
                transTypeName = "OPEN";
                break;
            default:
                Logger.logMessage("TypeData.getTransactionTypeName error, unknown trans type=" + Integer.toString(transTypeID), "ConsolidatedReceipt.log", Logger.LEVEL.ERROR);
                break;
        }

        return transTypeName;
    }

    public static class PaymentGateway
    {
        public static final int STRIPE              = 1;
        public static final int DATACAP_NETEPAY     = 2;
        public static final int DATACAP_NETEPAY_EMV = 3;
        public static final int MERCHANT_LINK       = 4;
        public static final int DATACAP_NETEPAY_EMV_PCI_P2PE = 5;
        public static final int FREEDOMPAY          = 6;
        public static final int SIMPLIFY_FUSEBOX    = 7;
    }

    public static class PrinterHardwareType
    {
        public static final int PHYSICAL_PRINTER    = 1;
        public static final int KDS                 = 2;
    }

    public static class PrinterConfigType {
        public static final String KP_PREP = "KP PREP";
        public static final String KDS_PREP = "KDS PREP";
        public static final String KP_EXPEDITOR = "KP EXPEDITOR";
        public static final String KDS_EXPEDITOR = "KDS EXPEDITOR";
        public static final String KP_REMOTE = "KP REMOTE";
        public static final String KDS_REMOTE = "KDS REMOTE";
        public static final String KP_EXPEDITOR_AS_REMOTE = "KP EXPEDITOR AS REMOTE";
        public static final String KDS_EXPEDITOR_AS_REMOTE = "KDS EXPEDITOR AS REMOTE";
    }

    public static class TerminalType {
        public static final int ADVANCED_PAYMENTS   = 1;
        public static final int KRONONS_TERMINAL    = 2;
        public static final int PC_ENTRY            = 3;
        public static final int POS_TERMINAL        = 4;
        public static final int MM_HAYES_POS        = 5;
        public static final int QC_KIOSK            = 6;
        public static final int ONLINE_ORDERING     = 7;
        public static final int MY_QC_FUNDING       = 8;
        public static final int KIOSK_ORDERING      = 9;
    }

    public static class ProductKPReceiptGroupType {
        public static final String PRINTED_PLU = "PRINTED PLU";
        public static final String UNPRINTED_PLU = "UNPRINTED PLU";
        public static final String UNPRINTED_EXPEDITOR_PLU = "UNPRINTED EXPEDITOR PLU";
        public static final String PREV_ORDER_PLU = "PREV ORDER PLU";
        public static final String CURR_ORDER_PLU = "CURR ORDER PLU";
    }

    public static class TerminalModelType {
        public static final int POS_ANYWHERE = 102;
        public static final int QC_POS = 103;
        public static final int ONLINE_ORDERING = 105;
        public static final int QC_POS_KIOSK = 107;
    }

    public static class ReceivedOnAcctType {
        public static final int QC                      = 1;
        public static final int NON_QC                  = 2;
        public static final int INTEGRATED_GIFT_CARD    = 3;
    }

    public static class PaidOutType {
        public static final int NON_INTEGRATED          = 1;
        public static final int INTEGRATED_GIFT_CARD    = 2;
    }

    public static class KDS {

        public static class TransType {
            public static final int ADD_NEW_ORDER = 1;
            public static final int DELETE_ORDER = 2;
            public static final int MODIFY_ORDER = 3;
            public static final int TRANSFER_ORDER = 4;
            public static final int GET_ORDER_STATUS = 5;
            public static final int REPLACE_PREVIOUS_ORDER = 6;
        }

        public static class OrderStatus {
            public static final int UNPAID = 0;
            public static final int PAID = 1;
            public static final int IN_PROCESS = 2;
        }

        public static class OrderType {
            public static final String RUSH = "RUSH";
            public static final String FIRE = "FIRE";
        }

        public static class Action {
            public static final int MORE_CONDIMENT = 1;
            public static final int LESS_CONDIMENT = -1;
        }

        public static class TcpResponseCode {
            public static final int NO_ERR = 0;
            public static final int BAD_XML_ERR = 1;
            public static final int NO_ACK_IN_10_SEC_ERR = 2;
            public static final int XML_PARAM_ERR = 3;
            public static final int XML_TRAN_TYPE_ERR = 4;
        }

        public static class RemoteFolderResponseCode {
            public static final int UNKNOWN = -1;
            public static final int NO_ERR = 0;
            public static final int BAD_XML_ERR = 1;
            public static final int NO_ACK_IN_10_SEC_ERR = 2;
            public static final int XML_PARAM_ERR = 3;
            public static final int XML_TRAN_TYPE_ERR = 4;
        }

        public static class KDSRemoteFolderStatus {
            public static final int NO_ACK_FOLDER_EXISTS = 1;
            public static final int ACK_FOLDER_NOT_EMPTY = 2;
            public static final int ACK_FOLDER_EMPTY = 2;
        }

    }


    public static class ProductPrintDisplayStatus {
        public static final String PRINTED = "PRINTED";
        public static final String UNPRINTED = "UNPRINTED";
        public static final String UNPRINTED_EXPEDITOR = "UNPRINTED_EXPEDITOR";
        public static final String PREVIOUS_ORDER = "PREVIOUS_ORDER";
        public static final String CURRENT_ORDER = "CURRENT_ORDER";
    }

    public static class KitchenReceiptType {
        public static final String KP = "KP";
        public static final String KDS = "KDS";
        public static final String KMS = "KMS";
    }
    
    public static class SimplePrintStatus {
        public static final String PRINTED = "PRINTED";
        public static final String UNPRINTED = "UNPRINTED";
    }

    public static class SimpleDisplayStatus {
        public static final String DISPLAYED = "DISPLAYED";
        public static final String UNDISPLAYED = "UNDISPLAYED";
    }

    public static class ReceiptContentType {
        public static final String LINE = "LINE";
        public static final String CENTER_LINE = "CENTER_LINE";
        public static final String CENTER_LARGE_LINE = "CENTER_LARGE_LINE";
        public static final String ONE_COL_LEFT = "ONE_COL_LEFT";
        public static final String TWO_COL = "TWO_COL";
        public static final String BLANK_LINE = "BLANK_LINE";
        public static final String CUT_PAPER = "CUT_PAPER";
    }

}
