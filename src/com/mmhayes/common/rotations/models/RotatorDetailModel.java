package com.mmhayes.common.rotations.models;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;

import java.util.HashMap;

//myqc dependencies
//other dependencies

/* Rotator Detail Model
Last Updated (automatically updated by SVN)
$Author: gematuszyk $: Author of last commit
$Date: 2019-04-24 12:52:04 -0400 (Wed, 24 Apr 2019) $: Date of last commit
$Rev: 38220 $: Revision of last commit

Notes: Model for Rotation Details */
public class RotatorDetailModel {
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer ID = null; //the ID of the rotation
    private Integer paKeypadID = null; // the keypadID of the rotation detail
    private Integer sortOrder = null; //the order in the rotation

    public RotatorDetailModel() {

    }

    public RotatorDetailModel(HashMap modelDetailsHM) throws Exception {
        setModelProperties(modelDetailsHM);
    }

    private RotatorDetailModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setPAKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));
        setSortOrder(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SORTORDER")));

        return this;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Integer getPAKeypadID() {
        return paKeypadID;
    }

    public void setPAKeypadID(Integer paKeypadID) {
        this.paKeypadID = paKeypadID;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }
}
