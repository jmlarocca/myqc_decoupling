package com.mmhayes.common.rotations.models;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.rotations.collections.RotatorDetailCollection;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* Rotator Model
Last Updated (automatically updated by SVN)
$Author: gematuszyk $: Author of last commit
$Date: 2019-04-24 12:52:04 -0400 (Wed, 24 Apr 2019) $: Date of last commit
$Rev: 38220 $: Revision of last commit

Notes: Model for Rotations */
public class RotatorModel {
    private static DataManager dm = new DataManager();
    private commonMMHFunctions commFunc = new commonMMHFunctions();
    private Integer ID = null; //the ID of the rotation
    private Integer controlledPAKeypadID = null; //the keypad ID of the controller keypad in the rotation
    private Integer paRotationTypeID = null; //type of rotation - One Time, Daily, Weekly, Monthly
    private Integer dayPartID = null;  //day part ID

    private String name = "";
    private boolean allowDupeKeypads = false; //determine if duplicate keypads are allowed
    private LocalDateTime referenceDTM = null; //the time when the rotation is set to begin
    List<RotatorDetailModel> rotationDetails = new ArrayList<>(); //list of keypadIDs and their SortOrder's

    //caching related properties
    private LocalDateTime lastUpdateDTM = null; //when the store was last updated
    private LocalDateTime lastUpdateCheck = null; //when the store was last checked to see if it has been updated

    public RotatorModel() {
    }

    public RotatorModel(HashMap modelDetailsHM) throws Exception {
       setModelProperties(modelDetailsHM);
    }

    public RotatorModel(Integer keypadID) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> rotationList = dm.parameterizedExecuteQuery("data.rotation.getRotationByKeypadID",
                new Object[]{
                        keypadID
                },
                true
        );

        if(rotationList != null && rotationList.size() > 0) {
            setModelProperties(rotationList.get(0)); //only set the first rotation found
        }
    }

    private RotatorModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setControlledPAKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("CONTROLLEDPAKEYPADID")));
        setPARotationTypeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAROTATIONTYPEID")));
        setDayPartID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DAYPARTID")));

        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setAllowDupeKeypads(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWDUPEKEYPADS")));

        //specify in and out formats
        String dateTimeInFormat = "yyyy-MM-dd HH:mm:ss.S";
        String dateTimeOutFormat = "M/dd/yyyy h:mm:ss a"; //old way was - M/dd/yyyy H:mm:ss
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeOutFormat);

        //format lastUpdateDTM
        String dateTimeStr = commFunc.formatDateTime(modelDetailHM.get("LASTUPDATEDTM").toString(),dateTimeInFormat,dateTimeOutFormat);
        setLastUpdateDTM( LocalDateTime.parse(dateTimeStr, dateTimeFormatter) );

        //format referenceDTM
        String referenceDTM = commFunc.formatDateTime(modelDetailHM.get("REFERENCEDTM").toString(),dateTimeInFormat,dateTimeOutFormat);
        setReferenceDTM( LocalDateTime.parse(referenceDTM, dateTimeFormatter) );

        setLastUpdateCheck(LocalDateTime.now());

        if (getID() != null) {

            RotatorDetailCollection rotatorDetailCollection = new RotatorDetailCollection(getID());
            setRotationDetails(rotatorDetailCollection.getCollection());
        }

        return this;
    }


    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public LocalDateTime getLastUpdateDTM() {
        return lastUpdateDTM;
    }

    public void setLastUpdateDTM(LocalDateTime lastUpdateDTM) {
        this.lastUpdateDTM = lastUpdateDTM;
    }

    public LocalDateTime getLastUpdateCheck() {
        return lastUpdateCheck;
    }

    public void setLastUpdateCheck(LocalDateTime lastUpdateCheck) {
        this.lastUpdateCheck = lastUpdateCheck;
    }

    public Integer getControlledPAKeypadID() {
        return controlledPAKeypadID;
    }

    public void setControlledPAKeypadID(Integer controlledPAKeypadID) {
        this.controlledPAKeypadID = controlledPAKeypadID;
    }

    public Integer getPARotationTypeID() {
        return paRotationTypeID;
    }

    public void setPARotationTypeID(Integer paRotationTypeID) {
        this.paRotationTypeID = paRotationTypeID;
    }

    public Integer getDayPartID() {
        return dayPartID;
    }

    public void setDayPartID(Integer dayPartID) {
        this.dayPartID = dayPartID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAllowDupeKeypads() {
        return allowDupeKeypads;
    }

    public void setAllowDupeKeypads(boolean allowDupeKeypads) {
        this.allowDupeKeypads = allowDupeKeypads;
    }

    public LocalDateTime getReferenceDTM() {
        return referenceDTM;
    }

    public void setReferenceDTM(LocalDateTime referenceDTM) {
        this.referenceDTM = referenceDTM;
    }

    public List<RotatorDetailModel> getRotationDetails() {
        return rotationDetails;
    }

    public void setRotationDetails(List<RotatorDetailModel> rotationDetails) {
        this.rotationDetails = rotationDetails;
    }

}
