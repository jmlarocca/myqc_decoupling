package com.mmhayes.common.rotations.collections;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.rotations.models.RotatorModel;
import com.mmhayes.common.terminal.models.TerminalModel;

//other dependencies
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-04-12 16:32:05 -0400 (Fri, 12 Apr 2019) $: Date of last commit
 $Rev: 8715 $: Revision of last commit
*/
public class RotatorCollection {
    private static final long REFRESH_MODEL_CACHE_SECONDS = 10;
    private List<RotatorModel> collection = new ArrayList<RotatorModel>();
    private static DataManager dm = new DataManager();

    //caching related properties
    private LocalDateTime lastUpdateDTM = null; //when the store was last updated
    private LocalDateTime lastUpdateCheck = null; //when the store was last checked to see if it has been updated

    //default constructor - used by the API
    public RotatorCollection() {

    }

    /**
     * Give me all the Rotations in one query.  This query fetches the Rotation Details for each actuve Rotation in the Terminal's Revenue Center.
     * I did it this way in order to fetch all this from the database in one query
     * In one example using this cache cut down the queries to the database from 40 to 15     *
     * @param terminalID : pass in the Terminal Model so the rotations can be filtered on Revenue Center
     * @return a list of all the Loyalty Programs
     * @throws Exception
     */
    public static RotatorCollection getAllRotationsForCache(Integer terminalID) throws Exception {
        RotatorCollection rotatorCollection = new RotatorCollection();

        //get all models in an array list
        ArrayList<HashMap> rotatorList = dm.parameterizedExecuteQuery("data.rotation.getAllRotationsForCache",
                new Object[]{terminalID},
                PosAPIHelper.getLogFileName(terminalID),
                true
        );

        for(HashMap rotatorHM : rotatorList) {
            if(CommonAPI.convertModelDetailToInteger(rotatorHM.get("ID")) != null) {
                RotatorModel rotatorModel = new RotatorModel(rotatorHM);
                rotatorCollection.getCollection().add(rotatorModel);
            }
        }
        return rotatorCollection;
    }

    //get all rotations by the store ID for MyQC
    public static RotatorCollection getAllRotationsForCache(Integer storeID, boolean usingStoreID) throws Exception {
        RotatorCollection rotatorCollection = new RotatorCollection();

        LocalDateTime lastUpdateDTM = null;

        //get all models in an array list
        ArrayList<HashMap> rotatorList = dm.parameterizedExecuteQuery("data.rotation.getAllRotationsForCacheByStore",
                new Object[]{storeID},
                true
        );

        if(rotatorList != null && rotatorList.size() > 0) {
            for(HashMap rotatorHM : rotatorList) {
                if(CommonAPI.convertModelDetailToInteger(rotatorHM.get("ID")) != null) {
                    RotatorModel rotatorModel = new RotatorModel(rotatorHM);
                    rotatorCollection.getCollection().add(rotatorModel);

                    if(lastUpdateDTM == null || rotatorModel.getLastUpdateDTM().isAfter(lastUpdateDTM) ) {
                        lastUpdateDTM = rotatorModel.getLastUpdateDTM();
                    }
                }
            }
        }

        //set the lastUpdateDTM as the most recent lastUpdateDTM from all the rotations in the collection
        rotatorCollection.setLastUpdateDTM(lastUpdateDTM);
        rotatorCollection.setLastUpdateCheck(LocalDateTime.now());

        return rotatorCollection;
    }

    //check if the cache needs to be updated if there were changes made to a rotation in the store's revenue center
    public boolean checkForUpdates(Integer terminalId) throws Exception {
        boolean needsUpdate = true;

        Object result = dm.parameterizedExecuteScalar("data.rotation.checkRotationsForUpdates",
                new Object[]{
                        terminalId,
                        getLastUpdateDTM().toLocalDate().toString()+" "+getLastUpdateDTM().plusSeconds(1).toLocalTime().toString()
                }
        );

        //keypads needs to be updated if there is no lastUpdateDTM or if the result is 1
        needsUpdate = ( result == null || result.toString().equals("1") );

        return needsUpdate;
    }

    //used by myqc - check if the cache needs to be updated if there were changes made to a rotation in the store's revenue center
    public boolean checkForUpdatesByStoreID(Integer storeID) throws Exception {
        boolean needsUpdate = false;
        LocalDateTime now = LocalDateTime.now();

        //if it's been more than 5 minutes since the last check for updates
        if ( now.isAfter(getLastUpdateCheck().plusSeconds(REFRESH_MODEL_CACHE_SECONDS)) && getLastUpdateDTM() != null ) {

            Object result = dm.parameterizedExecuteScalar("data.rotation.checkRotationsForUpdatesByStoreID",
                    new Object[]{
                            storeID,
                            getLastUpdateDTM().toLocalDate().toString()+" "+getLastUpdateDTM().plusSeconds(1).toLocalTime().toString()
                    }
            );

            //keypads needs to be updated if there is no lastUpdateDTM or if the result is 1
            needsUpdate = ( result == null || result.toString().equals("1") );

        }

        return needsUpdate;
    }

    public List<RotatorModel> getCollection() {
        return collection;
    }

    public void setCollection(List<RotatorModel> collection) {
        this.collection = collection;
    }

    public LocalDateTime getLastUpdateDTM() {
        return lastUpdateDTM;
    }

    public void setLastUpdateDTM(LocalDateTime lastUpdateDTM) {
        this.lastUpdateDTM = lastUpdateDTM;
    }

    public LocalDateTime getLastUpdateCheck() {
        return lastUpdateCheck;
    }

    public void setLastUpdateCheck(LocalDateTime lastUpdateCheck) {
        this.lastUpdateCheck = lastUpdateCheck;
    }
}
