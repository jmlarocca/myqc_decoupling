package com.mmhayes.common.rotations.collections;

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.rotations.models.RotatorDetailModel;
import com.mmhayes.common.utils.Logger;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* Rotator Detail Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-04-12 16:32:05 -0400 (Fri, 12 Apr 2019) $: Date of last commit
 $Rev: 8715 $: Revision of last commit

Notes: Model for Rotation Details */
public class RotatorDetailCollection {
    private List<RotatorDetailModel> collection = new ArrayList<RotatorDetailModel>();
    private static DataManager dm = new DataManager();

    public RotatorDetailCollection() {

    }

    //constructor - takes hashmap that get sets to this models properties
    public RotatorDetailCollection(Integer PARotationID) throws Exception {
        try {

            //get the most recent lastUpdateDTM for all the keypads in the store
            ArrayList<HashMap> rotationDetailList = dm.parameterizedExecuteQuery("data.rotation.getAllRotationDetails",
                    new Object[]{
                            PARotationID
                    },
                    true
            );

            if (rotationDetailList != null && rotationDetailList.size() > 0) {
                for (HashMap rotationDetailHM : rotationDetailList) {
                    if (rotationDetailHM.get("ID") != null && !rotationDetailHM.get("ID").toString().isEmpty()) {
                        //create a new model and add to the collection
                        getCollection().add(new RotatorDetailModel(rotationDetailHM));
                    }
                }
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    public List<RotatorDetailModel> getCollection() {
        return collection;
    }

    public void setCollection(List<RotatorDetailModel> collection) {
        this.collection = collection;
    }
}
