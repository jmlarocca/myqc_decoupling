package com.mmhayes.common.rotations;

import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.rotations.models.RotatorModel;
import com.mmhayes.common.utils.Logger;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2018-08-24 11:24:09 -0400 (Fri, 24 Aug 2018) $: Date of last commit
 $Rev: 32607 $: Revision of last commit

 Notes: Rotation Handler for rotating keypads
*/

public class RotationHandler {

    public RotationHandler() {

    }

    //used by MyQC - takes in the terminalID, keypadID and currentTime, returns the keypadID from the rotation if there is one
    public static Integer checkForRotatingKeypad(Integer terminalId, Integer keypadID, LocalDateTime currentTime, boolean usingStoreID) throws Exception {

        //get the Rotator Model from the cache whose controllerKeypadID matches the passed in keypadID
        List<RotatorModel> validRotations = PosAPIModelCache.getRotationFromCache(terminalId, keypadID, usingStoreID);

        if(validRotations.size() == 0) {
            return keypadID;
        }

        RotatorModel rotatorModel = determineRotatingModel(validRotations, currentTime);

        //determine which rotating keypad detail should be returned
        Integer rotatingKeypadID = determineRotatingKeypad(rotatorModel, currentTime);

        if(rotatingKeypadID == null) {
            return keypadID;
        }

        return rotatingKeypadID;
    }

    //used by Digital Signage - takes in the keypadID, checks if a rotation's controller keypad is set to that keypad, returns rotating keypadID
    public static Integer checkForRotatingKeypad(Integer keypadID, LocalDateTime currentTime) throws Exception {

        RotatorModel rotatorModel = new RotatorModel(keypadID);

        if(rotatorModel.getControlledPAKeypadID() == null) {
            return keypadID;
        }

        if(currentTime == null) {
            currentTime = LocalDateTime.now();
        }

        Integer rotatingKeypadID = determineRotatingKeypad(rotatorModel, currentTime);

        if(rotatingKeypadID == null) {
            return keypadID;
        }

        return rotatingKeypadID;
    }

    public static RotatorModel determineRotatingModel(List<RotatorModel> rotatorModels, LocalDateTime currentDateTime) {
        if(rotatorModels.size() == 1) {
            return rotatorModels.get(0);
        }

        // Sort the rotation models so they're in order by the reference time
        Collections.sort(rotatorModels, new Comparator<RotatorModel>() {
            @Override
            public int compare(RotatorModel rotatorModel, RotatorModel rotatorModel2) {

                LocalDateTime referenceDateTime1 = rotatorModel.getReferenceDTM();
                LocalTime referenceTime1 = referenceDateTime1.toLocalTime();

                LocalDateTime referenceDateTime2 = rotatorModel2.getReferenceDTM();
                LocalTime referenceTime2 = referenceDateTime2.toLocalTime();

                return referenceTime1.compareTo(referenceTime2);
            }
        });

        RotatorModel validRotatorModel = rotatorModels.get(0);

        for(RotatorModel rotatorModel : rotatorModels) {

            LocalDateTime referenceDateTime = rotatorModel.getReferenceDTM();
            LocalTime referenceTime = referenceDateTime.toLocalTime();

            LocalTime currentTime = currentDateTime.toLocalTime();

            if(currentTime.isAfter(referenceTime)) {
                validRotatorModel = rotatorModel;
            }
        }

        return validRotatorModel;
    }

    //determines which keypad should be returned based on today's date and the rotation type
    public static Integer determineRotatingKeypad(RotatorModel rotatorModel, LocalDateTime currentTime) {
        Integer rotationSize = rotatorModel.getRotationDetails().size();

        //if there are no rotation details, return null
        if(rotationSize == 0) {
            return null;
        }

        Integer rotationType = rotatorModel.getPARotationTypeID();
        LocalDateTime referenceTime = rotatorModel.getReferenceDTM();

        long days = referenceTime.until(currentTime, ChronoUnit.DAYS);
        long rotationIndex = 0;

        switch (rotationType) {
            case 1:  //ONE TIME

                if(currentTime.isAfter(referenceTime)) {
                    rotationIndex = 0;
                } else {
                    return null;
                }

                break;

            case 2: //DAILY

                rotationIndex = days % rotationSize;

                break;

            case 3: //WEEKLY

                long weeks = days / 7;

                rotationIndex = weeks % rotationSize;

                break;

            case 4: //MONTHLY

                long months = referenceTime.until(currentTime, ChronoUnit.MONTHS);

                rotationIndex = months % rotationSize;

                break;

            default:
                Logger.logMessage("Invalid Rotation Type set on controlling keypad ID: "+ rotatorModel.getControlledPAKeypadID(), Logger.LEVEL.ERROR);
                break;
        }

        int rotation = (int) rotationIndex;

        //if the index is invalid then return null
        if(rotation >= rotationSize) {
            return null;
        }

        //return the keypad at the rotation index
        return rotatorModel.getRotationDetails().get(rotation).getPAKeypadID();
    }

}
