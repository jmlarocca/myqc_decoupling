package com.mmhayes.common.login;

import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.OtmModel;
import com.mmhayes.common.utils.Logger;

import javax.servlet.http.HttpServletRequest;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-08-19 12:20:38 -0400 (Wed, 19 Aug 2020) $: Date of last commit
 $Rev: 12389 $: Revision of last commit
*/
//This class is used to Authenticate both QCPOS and OTM requests
public class RequestAuthWrapper {

    //private OtmLoginModel otmLoginModel = null;
    private OtmModel otmModel = null;
    private TerminalModel terminalModel = null;

    private String logFileName = "";

    public RequestAuthWrapper() {

    }

    public RequestAuthWrapper(HttpServletRequest request) throws Exception {

        try {

            LoginModel loginModel = CommonAuthResource.determineTerminalFromAuthHeader(request, true);

            if (loginModel.getTerminalId() != null){
                TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true, true);  //create Terminal Model, grab terminal information out of the loginModel
                this.setTerminalModel(terminalModel);
                this.setLogFileName(terminalModel.getTerminalLogFileName());
            }

            if (loginModel.getOtmId() != null){
                OtmModel otmModel = OtmModel.createOtmModel(CommonAuthResource.determineOtmIdFromAuthHeader(request));
                this.setOtmModel(otmModel);
                this.setLogFileName(PosAPIHelper.getLogFileName(this.getOtmModel(), "OTM"));
            }

        } catch (Exception e) {
            Logger.logMessage("Error during Authentication in RequestAuthWrapper.", Logger.LEVEL.ERROR);
            throw e;
        }
    }

    public void logTransactionTime() {

        if (this.getTerminalModel() != null) {
            this.getTerminalModel().logTransactionTime();

        } else if (this.getOtmModel() != null) {
            this.getOtmModel().logTransactionTime();
        }
    }

    public OtmModel getOtmModel() {
        return otmModel;
    }

    public void setOtmModel(OtmModel otmModel) {
        this.otmModel = otmModel;
    }

    public TerminalModel getTerminalModel() {
        return terminalModel;
    }

    public void setTerminalModel(TerminalModel terminalModel) {
        this.terminalModel = terminalModel;
    }

    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }

    public void setCustomLogFile(String customText){

        String logFileName = this.getLogFileName();
        if (this.getOtmModel() != null){
            logFileName = OtmModel.getLogFileName(this.getOtmModel().getId(), "SimplifyFusebox"); //override the log file name to SimplifyFuseBox
            this.setLogFileName(logFileName);
        }
    }
}
