package com.mmhayes.common.login;

//mmhayes dependencies

//other dependencies

import com.mmhayes.common.api.CommonAPI;

import java.util.HashMap;

/* Branding
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-05-25 17:59:02 -0400 (Thu, 25 May 2017) $: Date of last commit
 $Rev: 4042 $: Revision of last commit

 Notes: Holds branding settings
*/
public class BrandingModel {
    private String MyQCPrimaryColor = null;
    private String MyQCFontColorOnPrimary = null;
    private String MyQCSecondaryColor = null;
    private String MyQCFontColorOnSecondary = null;
    private String MyQCTertiaryColor = null;
    private String MyQCFontColorOnTertiary = null;
    private String MyQCAccentColorOne = null;
    private String MyQCAccentColorTwo = null;
    private String MyQCFilenameProgramLogo = null;
    private String MyQCTokenizedProgramLogo = null;
    private String MyQCProgramName = null;

    //constructor
    public BrandingModel(HashMap modelDetailHM) throws Exception {
        setModelProperties( modelDetailHM );
    }

    public void setModelProperties(HashMap modelDetailHM) throws Exception {
        setMyQCPrimaryColor( CommonAPI.convertModelDetailToString( modelDetailHM.get( "MYQCPRIMARYCOLOR" ) ) );
        setMyQCFontColorOnPrimary( CommonAPI.convertModelDetailToString( modelDetailHM.get( "MYQCFONTCOLORONPRIMARY" ) ) );
        setMyQCSecondaryColor( CommonAPI.convertModelDetailToString( modelDetailHM.get( "MYQCSECONDARYCOLOR" ) ) );
        setMyQCFontColorOnSecondary( CommonAPI.convertModelDetailToString( modelDetailHM.get( "MYQCFONTCOLORONSECONDARY" ) ) );
        setMyQCTertiaryColor( CommonAPI.convertModelDetailToString( modelDetailHM.get( "MYQCTERTIARYCOLOR" ) ) );
        setMyQCFontColorOnTertiary( CommonAPI.convertModelDetailToString( modelDetailHM.get( "MYQCFONTCOLORONTERTIARY" ) ) );
        setMyQCAccentColorOne( CommonAPI.convertModelDetailToString( modelDetailHM.get( "MYQCACCENTCOLORONE" ) ) );
        setMyQCAccentColorTwo( CommonAPI.convertModelDetailToString( modelDetailHM.get( "MYQCACCENTCOLORTWO" ) ) );
        setMyQCFilenameProgramLogo( CommonAPI.convertModelDetailToString( modelDetailHM.get( "MYQCFILENAMEPROGRAMLOGO" ) ) );
        setMyQCTokenizedProgramLogo( CommonAPI.convertModelDetailToString( modelDetailHM.get( "MYQCTOKENIZEDPROGRAMLOGO" ) ) );
        setMyQCProgramName( CommonAPI.convertModelDetailToString( modelDetailHM.get( "MYQCPROGRAMNAME" ) ) );
    }

    public String getMyQCPrimaryColor() {
        return MyQCPrimaryColor;
    }

    public void setMyQCPrimaryColor(String myQCPrimaryColor) {
        MyQCPrimaryColor = myQCPrimaryColor;
    }

    public String getMyQCFontColorOnPrimary() {
        return MyQCFontColorOnPrimary;
    }

    public void setMyQCFontColorOnPrimary(String myQCFontColorOnPrimary) {
        MyQCFontColorOnPrimary = myQCFontColorOnPrimary;
    }

    public String getMyQCSecondaryColor() {
        return MyQCSecondaryColor;
    }

    public void setMyQCSecondaryColor(String myQCSecondaryColor) {
        MyQCSecondaryColor = myQCSecondaryColor;
    }

    public String getMyQCFontColorOnSecondary() {
        return MyQCFontColorOnSecondary;
    }

    public void setMyQCFontColorOnSecondary(String myQCFontColorOnSecondary) {
        MyQCFontColorOnSecondary = myQCFontColorOnSecondary;
    }

    public String getMyQCTertiaryColor() {
        return MyQCTertiaryColor;
    }

    public void setMyQCTertiaryColor(String myQCTertiaryColor) {
        MyQCTertiaryColor = myQCTertiaryColor;
    }

    public String getMyQCFontColorOnTertiary() {
        return MyQCFontColorOnTertiary;
    }

    public void setMyQCFontColorOnTertiary(String myQCFontColorOnTertiary) {
        MyQCFontColorOnTertiary = myQCFontColorOnTertiary;
    }

    public String getMyQCAccentColorOne() {
        return MyQCAccentColorOne;
    }

    public void setMyQCAccentColorOne(String myQCAccentColorOne) {
        MyQCAccentColorOne = myQCAccentColorOne;
    }

    public String getMyQCAccentColorTwo() {
        return MyQCAccentColorTwo;
    }

    public void setMyQCAccentColorTwo(String myQCAccentColorTwo) {
        MyQCAccentColorTwo = myQCAccentColorTwo;
    }

    public String getMyQCFilenameProgramLogo() {
        return MyQCFilenameProgramLogo;
    }

    public void setMyQCFilenameProgramLogo(String myQCFilenameProgramLogo) {
        MyQCFilenameProgramLogo = myQCFilenameProgramLogo;
    }

    public String getMyQCTokenizedProgramLogo() {
        return MyQCTokenizedProgramLogo;
    }

    public void setMyQCTokenizedProgramLogo(String myQCTokenizedProgramLogo) {
        MyQCTokenizedProgramLogo = myQCTokenizedProgramLogo;
    }

    public String getMyQCProgramName() {
        return MyQCProgramName;
    }

    public void setMyQCProgramName(String myQCProgramName) {
        MyQCProgramName = myQCProgramName;
    }
}