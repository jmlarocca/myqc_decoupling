package com.mmhayes.common.login;

//mmhayes dependencies

//other dependencies

import com.mmhayes.common.api.CommonAPI;

import java.util.HashMap;

/* Account Creation
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-05-25 17:59:02 -0400 (Thu, 25 May 2017) $: Date of last commit
 $Rev: 4042 $: Revision of last commit

 Notes: Holds account creation settings
*/
public class AccountCreationModel {
    private Boolean MyQCAllowAcctCreation = null;
    private Boolean MyQCAllowPersonAcctCreation = null;
    private Boolean MyQCAcctEmailForUsername = null;
    private Boolean MyQCAcctPromptForEmail = null;
    private Boolean MyQCAcctPromptForName = null;
    private Boolean MyQCAcctPromptForEmpNumber = null;
    private Boolean MyQCAcctPromptForBadgeNumber = null;
    private Integer MyQCAcctAutoGenerateLength = null;

    // account group info
    private Integer AcctGroupID = null;
    private String AcctGroupName = null;
    private Integer AcctTypeID = null;
    private Integer AccessCodeID = null;
    private String AccessCodeName = null;

    // license info
    private Boolean GuestLicenseAvailable;
    private Boolean EmployeeLicenseAvailable;

    //constructor
    public AccountCreationModel(HashMap modelDetailHM) throws Exception {
        setModelProperties( modelDetailHM );
    }

    public void setModelProperties(HashMap modelDetailHM) throws Exception {
        setMyQCAllowAcctCreation( CommonAPI.convertModelDetailToBoolean( modelDetailHM.get("MYQCALLOWACCTCREATION")) );
        setMyQCAllowPersonAcctCreation( CommonAPI.convertModelDetailToBoolean( modelDetailHM.get("MYQCALLOWPERSONACCTCREATION")) );
        setMyQCAcctEmailForUsername( CommonAPI.convertModelDetailToBoolean( modelDetailHM.get("MYQCACCTEMAILFORUSERNAME")) );
        setMyQCAcctPromptForEmail( CommonAPI.convertModelDetailToBoolean( modelDetailHM.get("MYQCACCTPROMPTFOREMAIL")) );
        setMyQCAcctPromptForName( CommonAPI.convertModelDetailToBoolean( modelDetailHM.get("MYQCACCTPROMPTFORNAME")) );
        setMyQCAcctPromptForEmpNumber( CommonAPI.convertModelDetailToBoolean( modelDetailHM.get("MYQCACCTPROMPTFOREMPNUMBER")) );
        setMyQCAcctPromptForBadgeNumber( CommonAPI.convertModelDetailToBoolean( modelDetailHM.get("MYQCACCTPROMPTFORBADGENUMBER")) );
        setMyQCAcctAutoGenerateLength( CommonAPI.convertModelDetailToInteger( modelDetailHM.get("MYQCACCTAUTOGENERATELENGTH")) );
    }

    public Boolean getMyQCAllowAcctCreation() {
        return MyQCAllowAcctCreation;
    }

    public void setMyQCAllowAcctCreation(Boolean myQCAllowAcctCreation) {
        MyQCAllowAcctCreation = myQCAllowAcctCreation;
    }

    public Boolean getMyQCAllowPersonAcctCreation() {
        return MyQCAllowPersonAcctCreation;
    }

    public void setMyQCAllowPersonAcctCreation(Boolean myQCAllowPersonAcctCreation) {
        MyQCAllowPersonAcctCreation = myQCAllowPersonAcctCreation;
    }

    public Boolean getMyQCAcctEmailForUsername() {
        return MyQCAcctEmailForUsername;
    }

    public void setMyQCAcctEmailForUsername(Boolean myQCAcctEmailForUsername) {
        MyQCAcctEmailForUsername = myQCAcctEmailForUsername;
    }

    public Boolean getMyQCAcctPromptForEmail() {
        return MyQCAcctPromptForEmail;
    }

    public void setMyQCAcctPromptForEmail(Boolean myQCAcctPromptForEmail) {
        MyQCAcctPromptForEmail = myQCAcctPromptForEmail;
    }

    public Boolean getMyQCAcctPromptForName() {
        return MyQCAcctPromptForName;
    }

    public void setMyQCAcctPromptForName(Boolean myQCAcctPromptForName) {
        MyQCAcctPromptForName = myQCAcctPromptForName;
    }

    public Boolean getMyQCAcctPromptForEmpNumber() {
        return MyQCAcctPromptForEmpNumber;
    }

    public void setMyQCAcctPromptForEmpNumber(Boolean myQCAcctPromptForEmpNumber) {
        MyQCAcctPromptForEmpNumber = myQCAcctPromptForEmpNumber;
    }

    public Boolean getMyQCAcctPromptForBadgeNumber() {
        return MyQCAcctPromptForBadgeNumber;
    }

    public void setMyQCAcctPromptForBadgeNumber(Boolean myQCAcctPromptForBadgeNumber) {
        MyQCAcctPromptForBadgeNumber = myQCAcctPromptForBadgeNumber;
    }

    public Integer getMyQCAcctAutoGenerateLength() {
        return MyQCAcctAutoGenerateLength;
    }

    public void setMyQCAcctAutoGenerateLength(Integer myQCAcctAutoGenerateLength) {
        MyQCAcctAutoGenerateLength = myQCAcctAutoGenerateLength;
    }

    public Integer getAcctGroupID() {
        return AcctGroupID;
    }

    public void setAcctGroupID(Integer acctGroupID) {
        AcctGroupID = acctGroupID;
    }

    public String getAcctGroupName() {
        return AcctGroupName;
    }

    public void setAcctGroupName(String acctGroupName) {
        AcctGroupName = acctGroupName;
    }

    public Integer getAcctTypeID() {
        return AcctTypeID;
    }

    public void setAcctTypeID(Integer acctTypeID) {
        AcctTypeID = acctTypeID;
    }

    public Integer getAccessCodeID() {
        return AccessCodeID;
    }

    public void setAccessCodeID(Integer accessCodeID) {
        AccessCodeID = accessCodeID;
    }

    public String getAccessCodeName() {
        return AccessCodeName;
    }

    public void setAccessCodeName(String accessCodeName) {
        AccessCodeName = accessCodeName;
    }

    public void setGuestLicenseAvailable(Boolean guestLicenseAvailable) {
        this.GuestLicenseAvailable = guestLicenseAvailable;
    }

    public Boolean getGuestLicenseAvailable() {
        return this.GuestLicenseAvailable;
    }

    public Boolean getEmployeeLicenseAvailable() {
        return EmployeeLicenseAvailable;
    }

    public void setEmployeeLicenseAvailable(Boolean employeeLicenseAvailable) {
        EmployeeLicenseAvailable = employeeLicenseAvailable;
    }
}