package com.mmhayes.common.login;

//MMHayes Dependencies

import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.transaction.models.OtmModel;

//Api Dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//Other Dependencies
import java.time.Instant;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
*/

public class OtmLoginModel {
    private String apiKey = "";
    private String apiKeyCreateDate = "";
    private String apiKeyExpirationDate = "";
    private Boolean extendSession = false;
    private Instant extendedSessionExpirationDate = null;
    private OtmModel otm = new OtmModel();

    public OtmLoginModel() {

    }

    public void validateLogin() throws Exception {

        if (this.getOtm() != null) {

            if (this.getOtm().getMacAddress() == null || this.getOtm().getMacAddress().isEmpty()) {
                throw new InvalidAuthException("Login Failed - Invalid Mac Address");
            }

            this.getOtm().decryptMacAddress();

            if (this.getOtm().getName() == null || this.getOtm().getName().isEmpty()) {
                throw new InvalidAuthException("Login Failed - Invalid Host Name");
            }
        }
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getApiKeyCreateDate() {
        return apiKeyCreateDate;
    }

    public void setApiKeyCreateDate(String apiKeyCreateDate) {
        this.apiKeyCreateDate = apiKeyCreateDate;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getApiKeyExpirationDate() {
        return apiKeyExpirationDate;
    }

    public void setApiKeyExpirationDate(String apiKeyExpirationDate) {
        this.apiKeyExpirationDate = apiKeyExpirationDate;
    }

    public Boolean getExtendSession() {
        return extendSession;
    }

    public void setExtendSession(Boolean extendSession) {
        this.extendSession = extendSession;
    }

    @JsonIgnore
    public Instant getExtendedSessionExpirationDate() {
        return extendedSessionExpirationDate;
    }

    public void setExtendedSessionExpirationDate(Instant extendedSessionExpirationDate) {
        this.extendedSessionExpirationDate = extendedSessionExpirationDate;
    }

    public OtmModel getOtm() {
        return otm;
    }

    public void setOtmModel(OtmModel otm) {
        this.otm = otm;
    }
 }
