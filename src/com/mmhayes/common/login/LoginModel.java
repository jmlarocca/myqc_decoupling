package com.mmhayes.common.login;

//MMHayes Dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.MMHProperties;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

import javax.servlet.http.HttpServletRequest;

//Other Dependencies
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-07-22 13:41:30 -0400 (Thu, 22 Jul 2021) $: Date of last commit
 $Rev: 14466 $: Revision of last commit
*/

@ApiModel(value = "LoginModel", description = "LoginModel is used for sending Login requests to the API.")
@JsonPropertyOrder({"loginName", "loginPassword", "apiKey", "apiKeyCreateDate", "apiKeyExpirationDate", "clientIdentifier", "message", "extendSession"})

public class LoginModel {

    private Integer userId = null;
    private Integer terminalId = null;
    private Integer employeeId = null;
    private Integer otmId = null;
    private PosAPIHelper.HTTP_VERBS httpVerb = null;

    //new fields added for Maegan Integration
    private String loginName = "";
    private String loginPassword = "";
    private String apiKey = "";
    private String apiKeyCreateDate = "";
    private String apiKeyExpirationDate = "";
    private String clientIdentifier = "";
    private String message = "";

    private Boolean extendSession = false;
    private Instant extendedSessionExpirationDate = null;

    public LoginModel() {

    }

    public static LoginModel createLoginModel(HttpServletRequest request, TransactionModel transactionModel) throws Exception {
        LoginModel loginModel = CommonAuthResource.determineTerminalIdFromAuthHeader(request);

        //First check the TransactionModel.clientIdentifier (QC_Terminals.Pass) for the Terminal ID, then check the DSKey for the Terminal ID.
        if (loginModel != null) {
            loginModel = LoginModel.checkTransactionForTerminalIdentifier(loginModel, transactionModel);
        }

        return loginModel;
    }

    public static LoginModel createLoginModel(HttpServletRequest request) throws Exception {
        LoginModel loginModel = CommonAuthResource.determineTerminalIdFromAuthHeader(request);

        return loginModel;
    }

    public static LoginModel createLoginModel(HttpServletRequest request, boolean forKiosk) throws Exception {
        if ( CommonAuthResource.isKOA( request ) ) {
            return CommonAuthResource.determineUserIdFromAuthHeader(request);
        } else {
            return CommonAuthResource.determineTerminalIdFromAuthHeader(request);
        }
    }

    /**
     * Check the transactionModel for a clientIdentifier (QC_Terminals.Pass).  If this exists check the QC_Terminals table and get the Terminal ID
     * If this does not exists, use the Terminal ID from the DSKey
     *
     * @param loginModel
     * @param transactionModel
     * @throws Exception
     */
    public static LoginModel checkTransactionForTerminalIdentifier(LoginModel loginModel, TransactionModel transactionModel) throws Exception {

        if (transactionModel != null) {
            if (transactionModel.getClientIdentifier() != null && !transactionModel.getClientIdentifier().isEmpty()) {

                TerminalModel terminalModel = TerminalModel.getOneTerminalModelByTerminalIdentifier(transactionModel.getClientIdentifier());
                loginModel.terminalId = (terminalModel != null) ? terminalModel.getId() : null;
            }
        }

        return loginModel;
    }

    public void checkForExtendedSessionTime() {
        if (this.getExtendSession()) {
            Integer tempExtendedSessionMinutes = 129600; //90 * 24 * 60 - Default to 90 days
            Instant defaultDate = Instant.now().plus(tempExtendedSessionMinutes, ChronoUnit.MINUTES);
            this.setExtendedSessionExpirationDate(defaultDate);

            //check if POS is in offline mode
            if (MMHProperties.getAppSetting("site.security.extendedsessiontimeoutminutes").length() > 0) {
                tempExtendedSessionMinutes = Integer.parseInt(MMHProperties.getAppSetting("site.security.extendedsessiontimeoutminutes").toString());
                Instant overriddenDate = Instant.now().plus(tempExtendedSessionMinutes, ChronoUnit.MINUTES);
                this.setExtendedSessionExpirationDate(overriddenDate);
            }
        }
    }

    public void checkAndExtendDsKeyForSSOLogins(){
        if (this.getEmployeeId() != null && this.getUserId() == null) {

            DataManager dm = new DataManager();
            ArrayList<HashMap> terminalListHM = dm.parameterizedExecuteQuery("data.posapi30.hasAccessToThirdPartyTerminalThatSupportsSSO",
                    new Object[]{this.getEmployeeId()},
                    true
            );

            if (terminalListHM != null && !terminalListHM.isEmpty()){
                commonMMHFunctions commFunc = new commonMMHFunctions();
                Integer userId = commFunc.determineMyQCUserID();

                this.setExtendSession(true);
                this.checkForExtendedSessionTime();
                CommonAPI.updateDSKey(this.getApiKey(), null, userId, this.getExtendedSessionExpirationDate());
            }
        }
    }

    @JsonIgnore
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @JsonIgnore
    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    @JsonIgnore
    public PosAPIHelper.HTTP_VERBS getHttpVerb() {
        return httpVerb;
    }

    public void setHttpVerb(PosAPIHelper.HTTP_VERBS httpVerb) {
        this.httpVerb = httpVerb;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getApiKeyExpirationDate() {
        return apiKeyExpirationDate;
    }

    public void setApiKeyExpirationDate(String apiKeyExpirationDate) {
        this.apiKeyExpirationDate = apiKeyExpirationDate;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getApiKeyCreateDate() {
        return apiKeyCreateDate;
    }

    public void setApiKeyCreateDate(String apiKeyCreateDate) {
        this.apiKeyCreateDate = apiKeyCreateDate;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getClientIdentifier() {
        return clientIdentifier;
    }

    public void setClientIdentifier(String clientIdentifier) {
        this.clientIdentifier = clientIdentifier;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonIgnore
    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Boolean getExtendSession() {
        return extendSession;
    }

    public void setExtendSession(Boolean extendSession) {
        this.extendSession = extendSession;
    }

    @JsonIgnore
    public Instant getExtendedSessionExpirationDate() {
        return extendedSessionExpirationDate;
    }

    public void setExtendedSessionExpirationDate(Instant extendedSessionExpirationDate) {
        this.extendedSessionExpirationDate = extendedSessionExpirationDate;
    }

    @JsonIgnore
    public Integer getOtmId() {
        return otmId;
    }

    public void setOtmId(Integer otmId) {
        this.otmId = otmId;
    }

}
