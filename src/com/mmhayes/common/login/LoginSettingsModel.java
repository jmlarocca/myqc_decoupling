package com.mmhayes.common.login;

//mmhayes dependencies

//other dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.NutritionCategory;

import java.util.ArrayList;
import java.util.HashMap;

/* Login Settings
 Last Updated (automatically updated by SVN)
 $Author: ecdyer $: Author of last commit
 $Date: 2021-07-06 14:57:43 -0400 (Tue, 06 Jul 2021) $: Date of last commit
 $Rev: 14288 $: Revision of last commit

 Notes: For paginating collections of models
*/
public class LoginSettingsModel {
    private String APIVersion = null;
    private String errorDetails = null;
    private String passStrength = "strict"; //should return loose or strict, default to strict
    private String accountAlias = "Account"; //default to using Account
    private String personAccountAlias = "Person Account"; //default to your Person Account
    private String onGround = "true"; //from app.property file, defaults to true
    private String qcBaseURL = ""; //for multi-server installs, images need to be retrieved using the qcBaseURL
    private String selectStoreHeader = "Select Store"; //configurable text for Select Store header
    private String currencyType = "$"; //configurable text for currency type, defaults to "$"

    private Integer globalAuthType = 1; //authTypeID, defaults to 1 (Quickcharge)
    private Integer loginFAQSetID = null; // FAQSetID for the Login FAQs
    private Integer keypadPositionID = 1; // Keypad Position for all menus in Online Ordering
    private Integer empNumDigitsDisplayed = 0;

    private Boolean importAllInactive = false; //defaults to false
    private Boolean showKeepLogged = false; //defaults to false
    private Boolean enableFingerprint = false; //defaults to false
    private Boolean showQCAuthLoginLinkMYQC = false; //defaults to false
    private Boolean showTour = false; //defaults to false
    private Boolean allowHelpfulHints = true; //defaults to true
    private Boolean showCurrency = true; //defaults to true
    private Boolean currencyBeforePrice = true; //defaults to true
    private Boolean decimalInPrice = true; //defaults to true

    private BrandingModel brandingDetails = null;

    private AccountCreationModel accountCreationModel = null;
    private ArrayList<Object> accountCreation = new ArrayList<Object>();

    private String wellnessTokenizedFilename = ""; //custom wellness healthy indicator icon
    private String vegetarianTokenizedFilename = ""; //custom vegetarian healthy indicator icon
    private String veganTokenizedFilename = ""; //custom vegan healthy indicator icon
    private String glutenFreeTokenizedFilename = ""; //custom gluten free healthy indicator icon
    private String wellnessLabel = "Healthy"; //custom wellness label
    private String vegetarianLabel = "Vegetarian"; //custom vegetarian label
    private String veganLabel = "Vegan"; //custom vegan label
    private String glutenFreeLabel = "Gluten Free"; //custom gluten free label

    private NutritionCategory nutritionCategory1 = null;
    private NutritionCategory nutritionCategory2 = null;
    private NutritionCategory nutritionCategory3 = null;
    private NutritionCategory nutritionCategory4 = null;
    private NutritionCategory nutritionCategory5 = null;

    private static DataManager dm = new DataManager();

    //constructor
    public LoginSettingsModel() {

    }

    public void setModelProperties(HashMap modelDetailHM) throws Exception {
        setOnGround(CommonAPI.getOnGround());

        setPassStrength(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASSWORDSTRENGTHID"), 2));
        setGlobalAuthType(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("GLOBALAUTHTYPE"), 1));
        setLoginFAQSetID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("FAQSETID"), null));
        setKeypadPositionID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADPOSITIONID"), 1));
        setEmpNumDigitsDisplayed(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("EMPNUMDIGITSDISPLAYED"), 1));

        setAPIVersion(CommonAPI.convertModelDetailToString(modelDetailHM.get("APIVERSION")));
        setAccountAlias(CommonAPI.convertModelDetailToString(modelDetailHM.get("ACCOUNTALIAS")));
        setPersonAccountAlias(CommonAPI.convertModelDetailToString(modelDetailHM.get("PERSONACCOUNTALIAS")));
        setQCBaseURL(CommonAPI.convertModelDetailToString(modelDetailHM.get("QCBASEURL")));
        setSelectStoreHeader(CommonAPI.convertModelDetailToString(modelDetailHM.get("MYQCSELECTSTOREHEADER")));
        setCurrencyType(CommonAPI.convertModelDetailToString(modelDetailHM.get("MYQCCURRENCYTYPE")));

        setShowKeepLogged(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWKEEPLOGGED"), false));
        setImportAllInactive(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("IMPORTALLINACTIVE"), false));
        setShowQCAuthLoginLinkMYQC(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWQCAUTHLOGINLINKMYQC"), false));
        setEnableFingerprint(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("MYQCALLOWFINGERPRINTONDEVICE"), false));
        setShowTour(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("MYQCSHOWTOUR"), true));
        setAllowHelpfulHints(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("MYQCALLOWHELPFULHINTS"), true));
        setShowCurrency(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("MYQCSHOWCURRENCY"), true));
        setCurrencyBeforePrice(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("MYQCCURRENCYBEFOREPRICE"), true));
        setDecimalInPrice(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("MYQCDECIMALINPRICE"), true));

        setWellnessTokenizedFilename(CommonAPI.convertModelDetailToString(modelDetailHM.get("WELLNESSTOKENIZEDFILENAME")));
        setVegetarianTokenizedFilename(CommonAPI.convertModelDetailToString(modelDetailHM.get("VEGETARIANTOKENIZEDFILENAME")));
        setVeganTokenizedFilename(CommonAPI.convertModelDetailToString(modelDetailHM.get("VEGANTOKENIZEDFILENAME")));
        setGlutenFreeTokenizedFilename(CommonAPI.convertModelDetailToString(modelDetailHM.get("GLUTENFREETOKENIZEDFILENAME")));
        setWellnessLabel(CommonAPI.convertModelDetailToString(modelDetailHM.get("WELLNESSLABEL")));
        setVegetarianLabel(CommonAPI.convertModelDetailToString(modelDetailHM.get("VEGETARIANLABEL")));
        setVeganLabel(CommonAPI.convertModelDetailToString(modelDetailHM.get("VEGANLABEL")));
        setGlutenFreeLabel(CommonAPI.convertModelDetailToString(modelDetailHM.get("GLUTENFREELABEL")));

        BrandingModel brandingModel = new BrandingModel( modelDetailHM );
        setBrandingDetails( brandingModel );

        AccountCreationModel accountCreationModel = new AccountCreationModel( modelDetailHM );
        setAccountCreationModel( accountCreationModel );

        setNutritionCategories();
    }

    public void setKOAModelProperties(HashMap modelDetailHM) throws Exception {
        setOnGround(CommonAPI.getOnGround());

        setGlobalAuthType(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("GLOBALAUTHTYPE"), 1));

        setAPIVersion(CommonAPI.convertModelDetailToString(modelDetailHM.get("APIVERSION")));
        setQCBaseURL(CommonAPI.convertModelDetailToString(modelDetailHM.get("QCBASEURL")));

        setShowKeepLogged(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWKEEPLOGGED"), false));
        setShowQCAuthLoginLinkMYQC(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWQCAUTHLOGINLINKMYQC"), false));

        BrandingModel brandingModel = new BrandingModel( modelDetailHM );
        setBrandingDetails( brandingModel );

        setWellnessTokenizedFilename(CommonAPI.convertModelDetailToString(modelDetailHM.get("WELLNESSTOKENIZEDFILENAME")));
        setVegetarianTokenizedFilename(CommonAPI.convertModelDetailToString(modelDetailHM.get("VEGETARIANTOKENIZEDFILENAME")));
        setVeganTokenizedFilename(CommonAPI.convertModelDetailToString(modelDetailHM.get("VEGANTOKENIZEDFILENAME")));
        setGlutenFreeTokenizedFilename(CommonAPI.convertModelDetailToString(modelDetailHM.get("GLUTENFREETOKENIZEDFILENAME")));

        setNutritionCategories();
    }

    public String getPassStrength() {
        return passStrength;
    }

    public void setPassStrength(Integer passStrengthID) {
        String passStrength;

        switch ( passStrengthID ) {
            case 1:
                passStrength = "loose";
                break;
            case 2:
                passStrength = "strict";
                break;
            default:
                passStrength = CommonAPI.getPassStrengthFromPropFile();
        }

        this.passStrength = passStrength;
    }

    public void setNutritionCategories() {
        ArrayList<HashMap> nutriCategories = dm.parameterizedExecuteQuery("data.ordering.getNutritionCategoryNames", new Object[]{}, true);
        ArrayList<NutritionCategory> nutritionCategories = new ArrayList<NutritionCategory>();

        for (HashMap hm : nutriCategories)
        {
            String shortName = HashMapDataFns.getStringVal(hm, "SHORTNAME");
            String measurementLbl = HashMapDataFns.getStringVal(hm, "MEASUREMENTLBL");
            String name = HashMapDataFns.getStringVal(hm, "NAME");

            NutritionCategory nutriCat = new NutritionCategory(name, shortName, measurementLbl);
            nutritionCategories.add(nutriCat);

            if(getNutritionCategory1() == null) {
                setNutritionCategory1(nutriCat);

            } else if(getNutritionCategory2() == null) {
                setNutritionCategory2(nutriCat);

            } else if(getNutritionCategory3() == null) {
                setNutritionCategory3(nutriCat);

            } else if(getNutritionCategory4() == null) {
                setNutritionCategory4(nutriCat);

            } else if(getNutritionCategory5() == null) {
                setNutritionCategory5(nutriCat);
            }
        }
    }

    public String getAPIVersion() {
        return APIVersion;
    }

    public void setAPIVersion(String APIVersion) {
        this.APIVersion = APIVersion;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }

    public Integer getGlobalAuthType() {
        return globalAuthType;
    }

    public void setGlobalAuthType(Integer globalAuthType) {
        this.globalAuthType = globalAuthType;
    }

    public String getOnGround() {
        return onGround;
    }

    public void setOnGround(Boolean onGround) {
        if ( onGround ) {
            this.onGround = "true";
        } else {
            this.onGround = "false";
        }
    }

    public Boolean getImportAllInactive() {
        return importAllInactive;
    }

    public void setImportAllInactive(Boolean importAllInactive) {
        this.importAllInactive = importAllInactive;
    }

    public Boolean getShowKeepLogged() {
        return showKeepLogged;
    }

    public void setShowKeepLogged(Boolean showKeepLogged) {
        this.showKeepLogged = showKeepLogged;
    }

    public Boolean getEnableFingerprint() {
        return enableFingerprint;
    }

    public void setEnableFingerprint(Boolean enableFingerprint) {
        this.enableFingerprint = enableFingerprint;
    }

    public Boolean getShowQCAuthLoginLinkMYQC() {
        return showQCAuthLoginLinkMYQC;
    }

    public void setShowQCAuthLoginLinkMYQC(Boolean showQCAuthLoginLinkMYQC) {
        this.showQCAuthLoginLinkMYQC = showQCAuthLoginLinkMYQC;
    }

    public BrandingModel getBrandingDetails() {
        return brandingDetails;
    }

    public void setBrandingDetails(BrandingModel brandingDetails) {
        this.brandingDetails = brandingDetails;
    }

    public ArrayList<Object> getAccountCreation() {
        return accountCreation;
    }

    public void setAccountCreation(ArrayList<Object> accountCreation) {
        this.accountCreation = accountCreation;
    }

    @JsonIgnore
    public AccountCreationModel getAccountCreationModel() {
        return accountCreationModel;
    }

    public void setAccountCreationModel(AccountCreationModel accountCreationModel) throws Exception {
        this.accountCreationModel = accountCreationModel;

        ArrayList<Object> accountCreation = new ArrayList<Object>();
        accountCreation.add( accountCreationModel );

        //if Employee Account Creation is turned on, need to get the Account Groups
//        if ( accountCreationModel.getMyQCAllowAcctCreation() ) {
            ArrayList<HashMap> accountGroups = CommonAPI.getAccountCreationAccountGroups();
            for ( HashMap accountGroup : accountGroups ) {
                accountCreation.add(accountGroup);
            }
//        }

        setAccountCreation( accountCreation );
    }

    public String getAccountAlias() {
        return accountAlias;
    }

    public void setAccountAlias(String accountAlias) {
        this.accountAlias = accountAlias;
    }

    public String getPersonAccountAlias() {
        return personAccountAlias;
    }

    public void setPersonAccountAlias(String personAccountAlias) {
        this.personAccountAlias = personAccountAlias;
    }

    public Boolean getShowTour() {
        return showTour;
    }

    public void setShowTour(Boolean showTour) {
        this.showTour = showTour;
    }

    public String getWellnessTokenizedFilename() {
        return wellnessTokenizedFilename;
    }

    public void setWellnessTokenizedFilename(String wellnessTokenizedFilename) {
        this.wellnessTokenizedFilename = wellnessTokenizedFilename;
    }

    public String getVegetarianTokenizedFilename() {
        return vegetarianTokenizedFilename;
    }

    public void setVegetarianTokenizedFilename(String vegetarianTokenizedFilename) {
        this.vegetarianTokenizedFilename = vegetarianTokenizedFilename;
    }

    public String getVeganTokenizedFilename() {
        return veganTokenizedFilename;
    }

    public void setVeganTokenizedFilename(String veganTokenizedFilename) {
        this.veganTokenizedFilename = veganTokenizedFilename;
    }

    public String getGlutenFreeTokenizedFilename() {
        return glutenFreeTokenizedFilename;
    }

    public void setGlutenFreeTokenizedFilename(String glutenFreeTokenizedFilename) {
        this.glutenFreeTokenizedFilename = glutenFreeTokenizedFilename;
    }

    public Boolean getAllowHelpfulHints() {
        return allowHelpfulHints;
    }

    public void setAllowHelpfulHints(Boolean allowHelpfulHints) {
        this.allowHelpfulHints = allowHelpfulHints;
    }

    public String getQCBaseURL() {
        return qcBaseURL;
    }

    public void setQCBaseURL(String qcBaseURL) {
        this.qcBaseURL = qcBaseURL;
    }

    public Integer getLoginFAQSetID() {
        return loginFAQSetID;
    }

    public void setLoginFAQSetID(Integer loginFAQSetID) {
        this.loginFAQSetID = loginFAQSetID;
    }

    public String getSelectStoreHeader() {
        return selectStoreHeader;
    }

    public void setSelectStoreHeader(String selectStoreHeader) {
        this.selectStoreHeader = selectStoreHeader;
    }

    public String getWellnessLabel() {
        return wellnessLabel;
    }

    public void setWellnessLabel(String wellnessLabel) {
        this.wellnessLabel = wellnessLabel;
    }

    public String getVegetarianLabel() {
        return vegetarianLabel;
    }

    public void setVegetarianLabel(String vegetarianLabel) {
        this.vegetarianLabel = vegetarianLabel;
    }

    public String getVeganLabel() {
        return veganLabel;
    }

    public void setVeganLabel(String veganLabel) {
        this.veganLabel = veganLabel;
    }

    public String getGlutenFreeLabel() {
        return glutenFreeLabel;
    }

    public void setGlutenFreeLabel(String glutenFreeLabel) {
        this.glutenFreeLabel = glutenFreeLabel;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public Boolean getShowCurrency() {
        return showCurrency;
    }

    public void setShowCurrency(Boolean showCurrency) {
        this.showCurrency = showCurrency;
    }

    public Boolean getCurrencyBeforePrice() {
        return currencyBeforePrice;
    }

    public void setCurrencyBeforePrice(Boolean currencyBeforePrice) {
        this.currencyBeforePrice = currencyBeforePrice;
    }

    public Boolean getDecimalInPrice() {
        return decimalInPrice;
    }

    public void setDecimalInPrice(Boolean decimalInPrice) {
        this.decimalInPrice = decimalInPrice;
    }

    public Integer getKeypadPositionID() {
        return keypadPositionID;
    }

    public void setKeypadPositionID(Integer keypadPositionID) {
        this.keypadPositionID = keypadPositionID;
    }

    public Integer getEmpNumDigitsDisplayed() {
        return empNumDigitsDisplayed;
    }

    public void setEmpNumDigitsDisplayed(Integer empNumDigitsDisplayed) {
        this.empNumDigitsDisplayed = empNumDigitsDisplayed;
    }

    public NutritionCategory getNutritionCategory1() {
        return nutritionCategory1;
    }

    public void setNutritionCategory1(NutritionCategory nutritionCategory1) {
        this.nutritionCategory1 = nutritionCategory1;
    }

    public NutritionCategory getNutritionCategory2() {
        return nutritionCategory2;
    }

    public void setNutritionCategory2(NutritionCategory nutritionCategory2) {
        this.nutritionCategory2 = nutritionCategory2;
    }

    public NutritionCategory getNutritionCategory3() {
        return nutritionCategory3;
    }

    public void setNutritionCategory3(NutritionCategory nutritionCategory3) {
        this.nutritionCategory3 = nutritionCategory3;
    }

    public NutritionCategory getNutritionCategory4() {
        return nutritionCategory4;
    }

    public void setNutritionCategory4(NutritionCategory nutritionCategory4) {
        this.nutritionCategory4 = nutritionCategory4;
    }

    public NutritionCategory getNutritionCategory5() {
        return nutritionCategory5;
    }

    public void setNutritionCategory5(NutritionCategory nutritionCategory5) {
        this.nutritionCategory5 = nutritionCategory5;
    }

}
