package com.mmhayes.common.voucher.collections;

//MMHayes Dependencies
import com.mmhayes.common.voucher.models.DiscountInfoModel;

//API Dependencies
import java.util.ArrayList;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-07-08 13:58:07 -0400 (Thu, 08 Jul 2021) $: Date of last commit
 $Rev: 14303 $: Revision of last commit
*/

public class DiscountInfoCollection {

    private List<DiscountInfoModel> collection = new ArrayList<>();

    public DiscountInfoModel getOneByQcDiscountId(Integer qcDiscountId){
        if (getCollection() != null && !getCollection().isEmpty()){
            for (DiscountInfoModel discountInfoModel : getCollection()){
                if (discountInfoModel.getQcDiscountId().equals(qcDiscountId)){
                    return discountInfoModel;
                }
            }
        }

        return null;
    }

    public Boolean containsQcDiscountId(Integer qcDiscountId) {
        if (getCollection() != null && !getCollection().isEmpty()) {
            for (DiscountInfoModel discountInfoModel : getCollection()) {
                if (discountInfoModel.getQcDiscountId().equals(qcDiscountId)) {
                    return true;
                }
            }
        }

        return false;
    }

    public List<DiscountInfoModel> getCollection() {
        return collection;
    }

    public void setCollection(List<DiscountInfoModel> collection) {
        this.collection = collection;
    }
}
