package com.mmhayes.common.voucher.models;

//API dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import io.swagger.annotations.ApiModelProperty;

//Other dependencies
import java.math.BigDecimal;
import java.util.HashMap;

@JsonPropertyOrder({"id", "name"})
public class VoucherModel {
    private Integer id = null;
    private String name = "";
    private BigDecimal amount = BigDecimal.ZERO;
    private BigDecimal fullValue = BigDecimal.ZERO;
    private Integer tenderID = -1;

    //basic constructor
    public VoucherModel() { }

    //Constructor - takes in a HashMap
    public VoucherModel(HashMap VoucherHM) { setModelProperties(VoucherHM); }

    //map Voucher fields
    public void setModelProperties(HashMap modelDetailHM) {
        if (modelDetailHM.containsKey("ID"))     { this.setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID"))); }
        if (modelDetailHM.containsKey("NAME"))   { this.setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME"))); }
        if (modelDetailHM.containsKey("AMOUNT")) { this.setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT"))); }
        if (modelDetailHM.containsKey("FULLVALUE"))  { this.setFullValue(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("FULLVALUE"))); }
        if (modelDetailHM.containsKey("PATENDERID")) { this.setTenderID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATENDERID"))); }
    }

    @ApiModelProperty(value = "Voucher ID.", dataType = "Integer", notes = "Relates to the QC_PAVoucher.PAVoucherID table and field." )
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getId() {
        return id;
    }
    public void setId(Integer val) { this.id = val; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() { return name; }
    public void setName(String val) { this.name = val; }

    @JsonIgnore
    public BigDecimal getAmount() { return amount; }
    public void setAmount(BigDecimal val) { this.amount = val; }

    @ApiModelProperty(value = "Voucher Full Value.", dataType = "java.math.BigDecimal", notes = "Represents the full value of the voucher when used as a tender.")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getFullValue() { return fullValue; }
    public void setFullValue(BigDecimal val) { this.fullValue = val; }

    @JsonIgnore
    public Integer getTenderID() { return tenderID; }
    public void setTenderID(Integer val) { this.tenderID = val; }
}