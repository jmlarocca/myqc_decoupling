package com.mmhayes.common.voucher.models;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.TransactionBuilder;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.product.models.ItemDiscountModel;
import com.mmhayes.common.product.models.ItemTaxModel;
import com.mmhayes.common.receiptGen.TransactionData.Discount;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.Logger;

//other dependencies
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.HashMap;

public class VoucherCalculation {
    String logFileName = "";
    Integer iApplyVoucherCount = 0;

    TransactionBuilder transactionBuilder = null;
    TransactionModel transactionModel = null;
    TransactionCalculation transactionCalculation = null;
    private HashMap<Integer, Integer> discountTempIds = new HashMap<>();

    public VoucherCalculation(){

    }

    public VoucherCalculation(TransactionBuilder transactionBuilder, TransactionCalculation transactionCalculation){
        this.setTransactionBuilder(transactionBuilder);
        this.setTransactionCalculation(transactionCalculation);
        this.setTransactionModel(transactionBuilder.getTransactionModel());
        this.setLogFileName(PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()));
    }

    public void apply() throws Exception {
        Logger.logMessage("Entered VoucherCalculation.apply()", getLogFileName(), Logger.LEVEL.TRACE);

        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case SALE:
                if (!this.getTransactionModel().isInquiry()) {
                    return;
                }
                break;
            default:
                return;
        }

        BigDecimal voucherTotal = this.getTransactionModel().getTotalVoucherBalance();
        if (voucherTotal.compareTo(BigDecimal.ZERO) == 0) {
            Logger.logMessage("No vouchers found on transaction.  Nothing will be done.", getLogFileName());
            return;
        }

        if (this.getTotalQcDiscountAmount().compareTo(BigDecimal.ZERO) != 0 && shouldRecalculateQcDiscountAmount()){
            BigDecimal transTotalNoQcDiscount = this.getTransactionModel().getTotalTransactionAmount().add(this.getTotalQcDiscountAmount());
            BigDecimal transTotalWithQcDiscount = this.getTransactionModel().getTotalTransactionAmount();

            //Scenario 1
            //voucher covers it all, recalculate with no QC Discount
            if (voucherTotal.compareTo(transTotalNoQcDiscount) >= 0) {
                removeQcDiscountAndRecalculate();
                transTotalNoQcDiscount = this.getTransactionModel().getTotalTransactionAmount().add(this.getTotalQcDiscountAmount());
                updateTenderLineAmounts(transTotalNoQcDiscount);
                Logger.logMessage("Voucher amount greater than transaction balance.  Removed QC Discount.", getLogFileName());
                return;
            }

            //Scenario 2
            //Voucher is the less the transaction total, apply the QC Discount
            if (voucherTotal.compareTo(transTotalWithQcDiscount) <= 0) {
                //Apply the QC Discount
                return;
            }

            //Scenario 3
            if (voucherTotal.compareTo(transTotalWithQcDiscount) > 0
                    && voucherTotal.compareTo(transTotalNoQcDiscount) < 0) {
                recalculateQcDiscount();
                Logger.logMessage("QC Discount adjusted to maximize Voucher amount.", getLogFileName());

                return;
            }
        } else {
            BigDecimal transTotal = this.getTransactionModel().getTotalTransactionAmount();
            //No QC Discounts, or no QC Discounts that need to be recalculated.
            //voucher covers it all, check if we need to update the voucher amount
            if (voucherTotal.compareTo(transTotal) >= 0) {
                checkAndRemoveVouchers(transTotal);
                updateTenderLineAmounts(transTotal);
                return;
            }
        }
    }

    /**
     * Scenario 1 - voucher covers entire transaction total
     */
    public void removeQcDiscountAndRecalculate() throws Exception {
        if (getTotalQcDiscountAmount().compareTo(BigDecimal.ZERO) != 0) {
            this.getTransactionModel().setAutoApplyQcDiscounts(false);
            this.recalculateTransaction();
        }
    }

    /**
     * Scenario 3 - voucher covers entire transaction with QC Discount
     * voucher also is less than entire transaction with no QC Discount
     * We need to decrease the QC Discount amount
     */
    public void recalculateQcDiscount() throws Exception {
        Logger.logMessage("Entered VoucherCalculation.recalculateQcDiscount. ", getLogFileName());

        Integer iCount = 0;
        Boolean needsRecalculation = true;
        Boolean shouldIncreaseQcDiscountAmt = false;

        this.collectTempIds();
        this.getTransactionModel().setVoucherCalcInProgress(true);

        while (needsRecalculation) {
            Boolean firstPass = (iCount == 0);

            setNewQcDiscountAmount(firstPass, shouldIncreaseQcDiscountAmt);
            recalculateTransaction();

            BigDecimal voucherTotal = this.getTransactionModel().getTotalVoucherBalance().setScale(2, RoundingMode.HALF_UP);
            BigDecimal transTotal = this.getTransactionModel().getTotalTransactionAmount().setScale(2, RoundingMode.HALF_UP);
            if (voucherTotal.compareTo(transTotal) == 0) {
                //QC Discount is at the optimum amount to maximize the voucher
                needsRecalculation = false;
            } else if (voucherTotal.compareTo(transTotal) == 1) {
                shouldIncreaseQcDiscountAmt = false;
            } else if (voucherTotal.compareTo(transTotal) == -1){
                shouldIncreaseQcDiscountAmt = true;
            }

            iCount++;

            Logger.logMessage("Recalculating QC Discount for vouchers application.  Count: " + iCount.toString(), getLogFileName());

            if (iCount > 30) {
                Logger.logMessage("VoucherCalculation.recalculateQcDiscount: Error recalculating QC Discount for vouchers application.  Maximum number of attempts was hit.  Count: " + iCount.toString(), getLogFileName(), Logger.LEVEL.ERROR);
                throw new MissingDataException("Error recalculating Voucher amount", getLogFileName());
            }
        }

        this.setTempIds();
    }

    public BigDecimal getTotalQcDiscountAmount(){
        BigDecimal qcDiscountTotal = this.getTransactionModel().getTotalQcDiscountAmount();

        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
            if (discountLineItemModel.getQcDiscount() != null) {
                qcDiscountTotal = qcDiscountTotal.add(discountLineItemModel.getExtendedAmount());

                for (ItemTaxModel itemTaxModel : discountLineItemModel.getTaxes()){
                    qcDiscountTotal = qcDiscountTotal.add(itemTaxModel.getAmount()); //include the tax that the discount removed
                }
            }
        }

        if (qcDiscountTotal.compareTo(BigDecimal.ZERO) < 0) {
            qcDiscountTotal = qcDiscountTotal.negate();
        }

        return qcDiscountTotal;
    }

    public Boolean shouldRecalculateQcDiscountAmount(){
        Boolean shouldRecalculate = false;

        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
            if (discountLineItemModel.getQcDiscount() != null) {
                if (discountLineItemModel.getDiscount().getDiscountTypeId() == PosAPIHelper.DiscountType.COUPON.toInt()
                    && !discountLineItemModel.getQcDiscount().isSingleUse()){
                    shouldRecalculate = true;
                }
            }
        }

        return shouldRecalculate;
    }

    private void removeQcDiscounts(){
        //Remove off current QC Auto applied discount
        Integer discountId = 0;
        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()){
            if (discountLineItemModel.getQcDiscount() != null){

                if (!discountLineItemModel.getQcDiscount().isSingleUse()
                    && discountLineItemModel.getDiscount().getDiscountTypeId() == PosAPIHelper.DiscountType.COUPON.toInt()){

                    discountId = discountLineItemModel.getDiscount().getId();

                    final Integer removeDiscountId = discountId;
                    for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()){
                        productLineItemModel.getDiscounts().removeIf(d -> d.getDiscount().getId().equals(removeDiscountId));
                    }
                }
            }
        }

        //We've found that we don't need the qc discount, remove it
        //Remove discount line item
        this.getTransactionModel().getDiscounts().removeIf(d -> d.getQcDiscount() != null);
    }

    private void recalculateTransaction() throws Exception {
        this.removeQcDiscounts();
        this.setTransactionCalculation(this.getTransactionBuilder().calculateTransaction());
    }

    private void checkAndRemoveVouchers(BigDecimal transTotal){
        transTotal = transTotal.setScale(2, RoundingMode.HALF_UP);

        if (transTotal.compareTo(BigDecimal.ZERO) <= 0) {
            //Trans total <= 0, remove any vouchers
            Logger.logMessage("Transaction total <= 0, vouchers will be removed", getLogFileName(), Logger.LEVEL.TRACE);
            this.getTransactionModel().removeZeroDollarVoucherTenders();
            return;
        }
    }

    public void updateTenderLineAmounts(BigDecimal transTotal) {
        transTotal = transTotal.setScale(2, RoundingMode.HALF_UP);

        if (this.getTransactionModel().getTotalVoucherCount() == 1) {
            //update this tender amount
            for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
                if (tenderLineItemModel.getVoucherCode() != null) {
                    this.setVoucherTenderAmount(tenderLineItemModel, transTotal);
                }
            }
        } else {
            //TODO - Multiple vouchers on a transactions are not supported at this time.
            //find the highest tender line
            /*Integer voucherCount = this.getTransactionModel().getTotalVoucherCount();
            this.getTransactionModel().getTenders().sort(Comparator.comparing(a -> a.getAmount()));

            BigDecimal appliedVouchers = BigDecimal.ZERO;
            for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
                if (tenderLineItemModel.getVoucherCode() != null) {
                    if (transTotal.compareTo(appliedVouchers) == 0) {
                        this.getTransactionModel().getInvalidTenders().add(tenderLineItemModel);
                        //tenderLineItemModel.setValid(false);
                        continue;
                    }

                    //if (tenderLineItemModel.getAmount().compareTo(transactionTtlNoQcDiscounts) < 0){
                    if (tenderLineItemModel.getAmount().compareTo(transTotal) >= 0) {

                        tenderLineItemModel.setAmount(transTotal);
                        tenderLineItemModel.refreshExtendedAmount();
                        appliedVouchers = appliedVouchers.add(transTotal);
                        //tenderLineItemModel.setValid(true);

                        //TODO - figure out how to change tender amounts if there are multiple.
                    }
                }
            }*/

            //this.getTransactionModel().getTenders().removeIf(t -> t.getVoucherCode() != null && !t.isValid());
        }
    }

    private Integer findQcDiscountId() {
        Integer qcDiscountId = -1;

        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
            if (discountLineItemModel.getQcDiscount() != null) {
                qcDiscountId = discountLineItemModel.getQcDiscount().getId();
                break;
            }
        }

        return qcDiscountId;
    }

    /**
     * Set the overriden value of the QC Discount amount in order to maximize the voucher amount.
     * Keep doing this until you get down to $.01
     * This should work if the QC Discount needs to be decremented $.01 or 10.00
     * @return
     */
    private void setNewQcDiscountAmount(Boolean firstPass, Boolean shouldIncreaseQcDiscountAmt) {
        BigDecimal totalVoucherBalance = this.getTransactionModel().getTotalVoucherBalance();
        BigDecimal totalTransAmt = this.getTransactionModel().getTotalTransactionAmount();

        //Figure out how much over the discount amount is
        BigDecimal transDifference = totalVoucherBalance.subtract(totalTransAmt);

        Integer qcDiscountId = findQcDiscountId();
        BigDecimal adjAmount = BigDecimal.ZERO;
        DiscountInfoModel discountInfoModel = this.getTransactionModel().getDiscountInfoCollection().getOneByQcDiscountId(qcDiscountId);
        if (discountInfoModel != null){
            adjAmount = discountInfoModel.getQcDiscountAmt();
        }

        BigDecimal adjDiscountDifference = transDifference;
        if (!firstPass){

            if (shouldIncreaseQcDiscountAmt){
                adjDiscountDifference = new BigDecimal(".01");
            } else {
                adjDiscountDifference = new BigDecimal("-.01");
            }

        } else {
            if (adjDiscountDifference.compareTo(BigDecimal.ZERO) == 1){
                adjDiscountDifference = adjDiscountDifference.negate();
            }
        }

        adjAmount = adjAmount.add(adjDiscountDifference);
        discountInfoModel.setQcDiscountAmt(adjAmount);
        //this new value will override the QC Discount amount in TransactionCalculation
    }

    /***
     * On a transaction coming in, get the tempIds that were set by the client.
     * The tempIds are set to link discount line items to item discount models on the products.
     */
    private void collectTempIds(){
        if (!this.getTransactionModel().getDiscounts().isEmpty()) {
            for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
                if (discountLineItemModel.getTempId() != null) {
                    discountTempIds.put(discountLineItemModel.getDiscount().getId(), discountLineItemModel.getTempId());
                }
            }
        }
    }

    /***
     * After the discount was recalculated, reset the tempId to the original value
     * The tempIds can be used by the client to link discount line items to item discount models on the products.
     */
    private void setTempIds(){
        if (!this.getTransactionModel().getDiscounts().isEmpty()) {

            try {

                for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
                    if (discountLineItemModel.getTempId() != null) {

                        Integer discountId = discountLineItemModel.getDiscount().getId();
                        Integer originalTempId = discountTempIds.get(discountId);
                        Integer invalidTempId = discountLineItemModel.getTempId();

                        discountLineItemModel.setTempId(originalTempId);

                        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                            for (ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()) {
                                if (itemDiscountModel.getTempLinkedLineItemId() != null) {
                                    if (itemDiscountModel.getTempLinkedLineItemId().compareTo(invalidTempId) == 0) {
                                        itemDiscountModel.setTempLinkedLineItemId(originalTempId);
                                    }
                                }
                            }
                        }
                    }
                }

            } catch (Exception ex) {
                Logger.logMessage("VoucherCalculation.setTempIds.  Error setting tempIds on Item Discount Models.", getLogFileName());
            }
        }
    }

    private void setVoucherTenderAmount(TenderLineItemModel tenderLineItemModel, BigDecimal transTotal){
        if (tenderLineItemModel.getVoucherCode().getVoucher().getAmount().compareTo(transTotal) < 1){
            //if transTotal is greater than the max voucher amount, set amount to the max voucher amount
            tenderLineItemModel.setAmount(tenderLineItemModel.getVoucherCode().getVoucher().getAmount());
        } else {
            tenderLineItemModel.setAmount(transTotal);
            Logger.logMessage("Voucher amount greater than transaction balance.  Voucher amount updated to $" + transTotal.toString(), getLogFileName());
        }
        tenderLineItemModel.refreshExtendedAmount();
    }

    public TransactionBuilder getTransactionBuilder() {
        return transactionBuilder;
    }

    public void setTransactionBuilder(TransactionBuilder transactionBuilder) {
        this.transactionBuilder = transactionBuilder;
    }

    public TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    public TransactionCalculation getTransactionCalculation() {
        return transactionCalculation;
    }

    public void setTransactionCalculation(TransactionCalculation transactionCalculation) {
        this.transactionCalculation = transactionCalculation;
    }

    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }
}
