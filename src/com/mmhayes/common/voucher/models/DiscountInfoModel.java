package com.mmhayes.common.voucher.models;

//MMHayes Dependencies
import com.mmhayes.common.transaction.models.QcDiscountModel;

//Other Dependencies
import java.math.BigDecimal;

/*
 Last Updated (automatically updated by SVN)
 $Author: nyu $: Author of last commit
 $Date: 2021-08-17 14:59:44 -0400 (Tue, 17 Aug 2021) $: Date of last commit
 $Rev: 14936 $: Revision of last commit
*/

public class DiscountInfoModel {

    private Integer paDiscountId = null;
    private Integer qcDiscountId = null;
    private Integer transactionItemNum = null;
    private BigDecimal qcDiscountAmt = null;

    public DiscountInfoModel() {

    }

    public DiscountInfoModel(QcDiscountModel qcDiscountModel, Integer transactionItemNum){
        this.setQcDiscountId(qcDiscountModel.getId());
        this.setQcDiscountAmt(qcDiscountModel.getAmount());
        this.setTransactionItemNum(transactionItemNum);
    }

    public Integer getPaDiscountId() {
        return paDiscountId;
    }

    public void setPaDiscountId(Integer paDiscountId) {
        this.paDiscountId = paDiscountId;
    }

    public Integer getQcDiscountId() {
        return qcDiscountId;
    }

    public void setQcDiscountId(Integer qcDiscountId) {
        this.qcDiscountId = qcDiscountId;
    }

    public Integer getTransactionItemNum() {
        return transactionItemNum;
    }

    public void setTransactionItemNum(Integer transactionItemNum) {
        this.transactionItemNum = transactionItemNum;
    }

    public BigDecimal getQcDiscountAmt() {
        return qcDiscountAmt;
    }

    public void setQcDiscountAmt(BigDecimal qcDiscountAmt) {
        this.qcDiscountAmt = qcDiscountAmt;
    }
}
