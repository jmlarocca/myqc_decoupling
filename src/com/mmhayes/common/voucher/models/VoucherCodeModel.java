package com.mmhayes.common.voucher.models;

//API dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidVoucherException;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import io.swagger.annotations.ApiModelProperty;

//Other dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

@JsonPropertyOrder({"id", "voucher", "code", "statusId", "expirationDate", "notes"})
public class VoucherCodeModel {
    private Integer id = null;
    private VoucherModel voucher = null;
    private String code = "";
    private Integer statusID = null;
    private String expirationDate = "";
    private String notes = "";

    //basic constructor
    public VoucherCodeModel() { }

    //Constructor - takes in a HashMap
    public VoucherCodeModel(HashMap VoucherHM) { setModelProperties(VoucherHM); }

    //map common qc pos transaction line item fields
    public void setModelProperties(HashMap modelDetailHM) {
        if (modelDetailHM.containsKey("ID")) { this.setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID"))); }

        Integer voucherTenderID = 0;
        String voucherID = "", voucherName = "";
        BigDecimal voucherAmount = BigDecimal.ZERO;
        BigDecimal voucherFullValue = BigDecimal.ZERO;
        if (modelDetailHM.containsKey("VOUCHER_ID"))         { voucherID = CommonAPI.convertModelDetailToString(modelDetailHM.get("VOUCHER_ID")); }
        if (modelDetailHM.containsKey("VOUCHER_NAME"))       { voucherName = CommonAPI.convertModelDetailToString(modelDetailHM.get("VOUCHER_NAME")); }
        if (modelDetailHM.containsKey("VOUCHER_AMOUNT"))     { voucherAmount = CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("VOUCHER_AMOUNT")); }
        if (modelDetailHM.containsKey("VOUCHER_FULL_VALUE")) { voucherFullValue = CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("VOUCHER_FULL_VALUE")); }
        if (modelDetailHM.containsKey("VOUCHER_TENDER_ID"))  { voucherTenderID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("VOUCHER_TENDER_ID")); }
        this.setVoucher(convertModelDetailToVoucherModel(voucherID, voucherName, voucherAmount, voucherFullValue, voucherTenderID));

        if (modelDetailHM.containsKey("PAVOUCHERCODEID"))       { this.setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAVOUCHERCODEID"))); }
        if (modelDetailHM.containsKey("CODE"))                  { this.setCode(CommonAPI.convertModelDetailToString(modelDetailHM.get("CODE"))); }
        if (modelDetailHM.containsKey("PAVOUCHERCODESTATUSID")) { this.setStatusId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAVOUCHERCODESTATUSID"))); }
        if (modelDetailHM.containsKey("EXPIRATIONDATE"))        { this.setExpirationDate(CommonAPI.convertModelDetailToString(modelDetailHM.get("EXPIRATIONDATE"))); }
        if (modelDetailHM.containsKey("NOTES"))                 { this.setNotes(CommonAPI.convertModelDetailToString(modelDetailHM.get("NOTES"))); }
    }

    //ID - getter/setter
    @ApiModelProperty(value = "Voucher ID.", dataType = "Integer", notes = "Relates to the QC_PAVoucher.PAVoucherID table and field." )
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getId() {
        return id;
    }
    public void setId(Integer val) { this.id = val; }

    //VoucherModel - getter / setter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public VoucherModel getVoucher() { return voucher; }
    public void setVoucher(VoucherModel val) { this.voucher = val; }

    //Code - getter / setter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getCode() { return code; }
    public void setCode(String val) { this.code = val; }

    //Status - getter / setter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getStatusId() {
        return statusID;
    }
    public void setStatusId(Integer val) { this.statusID = val; }

    //Expiration Date - getter / setter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getExpirationDate() { return expirationDate; }
    public void setExpirationDate(String val) { this.expirationDate = val; }

    //Notes - getter / setter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getNotes() { return notes; }
    public void setNotes(String val) { this.notes = val; }

    //Convert a String ID and a String Voucher Name to a new VoucherModel
    @JsonIgnore
    private VoucherModel convertModelDetailToVoucherModel(String voucherID, String voucherName, BigDecimal voucherAmount, BigDecimal voucherFullValue, Integer tenderID) {
        VoucherModel retVal;
        HashMap<String, String> voucherProperties = new HashMap<String, String>(){{
            put("ID", voucherID);
            put("NAME", voucherName);
            put("AMOUNT", voucherAmount.toString());
            put("FULLVALUE", voucherFullValue.toString());
            put("PATENDERID", tenderID.toString());
        }};

        //create the return model
        retVal = new VoucherModel(voucherProperties);
        return retVal;
    }

    //Recreate this Voucher Code Model from it's Code
    @JsonIgnore
    public void createVoucherFromCode(TerminalModel terminal) throws Exception {
        //get voucher details from SQL based on code
        DataManager dm = new DataManager();
        ArrayList<HashMap> voucherData = dm.parameterizedExecuteQuery(
            "data.posapi30.getVoucherDataByCode",
            new Object[]{ this.code },
            PosAPIHelper.getLogFileName(terminal.getId()),
            true
        );

        //set the appropriate values if the voucher was found, otherwise, throw an exception
        if (!voucherData.isEmpty()) { setModelProperties(voucherData.get(0)); }
        else { throw new InvalidVoucherException("Voucher Code not found."); }
    }

    //validate a voucher code within a given transaction
    @JsonIgnore
    public void validateVoucherCode(TransactionModel txnModel) throws Exception {
        //refunds, voids, and cancels should ignore the check for expiration and redemption status
        //cancels should also ignore the active status of a voucher
        ArrayList<HashMap> voucherValidation = new DataManager().parameterizedExecuteQuery(
            "data.posapi30.validateVoucherCode",
            new Object[] {
                this.code,                                                                      //voucher code being checked
                txnModel.getTimeStamp(),                                                        //date to check against voucher expiration
                txnModel.getTerminalId(),                                                       //terminal requesting the voucher
                (txnModel.getApiActionTypeEnum() == PosAPIHelper.ApiActionType.REFUND) ? 1 : 0, //whether the request is a refund
                (txnModel.getApiActionTypeEnum() == PosAPIHelper.ApiActionType.VOID) ? 1 : 0,   //whether the request is a void
                (txnModel.getApiActionTypeEnum() == PosAPIHelper.ApiActionType.CANCEL) ? 1 : 0  //whether the request is a cancel
            },
            PosAPIHelper.getLogFileName(txnModel.getTerminalId()),
            true
        );

        //If the returned result "HAS_ERROR" then create the exception based on the returned issue
        if (voucherValidation.get(0).get("HAS_ERROR").toString().equals("1")) {
            String resultText = voucherValidation.get(0).get("RESULT").toString().toLowerCase();

            throw new InvalidVoucherException(resultText.substring(0, 1).toUpperCase() + resultText.substring(1));
        }
        //otherwise, let the program progress, and we will handle the validation result in the calling frame
    }
}