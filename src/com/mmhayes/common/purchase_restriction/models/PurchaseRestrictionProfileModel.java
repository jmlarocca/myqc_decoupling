package com.mmhayes.common.purchase_restriction.models;

 /* Last Updated (automatically updated by SVN)
         $Author: gematuszyk $: Author of last commit
         $Date: 2019-03-13 17:18:12 -0400 (Wed, 13 Mar 2019) $: Date of last commit
         $Rev: 37153 $: Revision of last commit
*/

//API Dependencies
import com.mmhayes.common.api.CommonAPI;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;

public class PurchaseRestrictionProfileModel {

    private Integer id = null;
    String name = "";
    String description = "";
    ArrayList<PurchaseRestrictionProfileDetailModel> purchaseRestrictionProfileDetailModels = new ArrayList();

    public PurchaseRestrictionProfileModel(){

    }

    public PurchaseRestrictionProfileModel(HashMap purchaseRestrictionProfileHM) throws Exception {
        setModelProperties(purchaseRestrictionProfileHM);
    }

    public PurchaseRestrictionProfileModel setModelProperties(HashMap modelDetailHM) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("RESTRICTIONPROFILEID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PROFILENAME")));
        setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));
        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public ArrayList<PurchaseRestrictionProfileDetailModel> getPurchaseRestrictionProfileDetailModels() {
        return purchaseRestrictionProfileDetailModels;
    }

    public void setPurchaseRestrictionProfileDetailModels(ArrayList<PurchaseRestrictionProfileDetailModel> purchaseRestrictionProfileDetailModels) {
        this.purchaseRestrictionProfileDetailModels = purchaseRestrictionProfileDetailModels;
    }
}
