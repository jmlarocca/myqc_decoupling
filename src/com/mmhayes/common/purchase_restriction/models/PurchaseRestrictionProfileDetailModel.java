package com.mmhayes.common.purchase_restriction.models;

//mmhayes dependencies

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;

import java.util.HashMap;

//javax.ws.rs dependencies
//myqc dependencies
//other dependencies

/* Express Order Item Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-03-13 17:18:12 -0400 (Wed, 13 Mar 2019) $: Date of last commit
 $Rev: 37153 $: Revision of last commit

 Notes: Model for express order items (a.k.a products and products with modifiers)
*/
public class PurchaseRestrictionProfileDetailModel {
    private Integer id = null;
    private Integer PAPluID = null;
    private Integer PADepartmentID = null;
    private Integer PASubDeptID = null;
    private Integer purchaseRestrictionType = null;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String name = "";

    static DataManager dm = new DataManager();

    //dummy constructor - needed to consume ANY JSON object via REST (Jersey/Jackson style)
    public PurchaseRestrictionProfileDetailModel() {

    }

    public static PurchaseRestrictionProfileDetailModel createPurchaseRestrictionModel(HashMap modelDetailHM) throws Exception {
        return new PurchaseRestrictionProfileDetailModel(modelDetailHM);
    }


    //constructor - takes hashmap that get sets to this models properties
    public PurchaseRestrictionProfileDetailModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of the favorite order model's for the My Quick Picks page
    public PurchaseRestrictionProfileDetailModel setModelProperties(HashMap modelDetailHM) throws Exception{
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setPAPluID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPLUID")));
        setPADepartmentID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADEPARTMENTID")));
        setPASubDeptID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASUBDEPTID")));
        setPurchaseRestrictionType(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PURCHASERESTRICTIONTYPE")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("DETAILNAME")));

        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    public Integer getPAPluID() {
        return PAPluID;
    }

    public void setPAPluID(Integer PAPluID) {
        this.PAPluID = PAPluID;
    }

    public Integer getPADepartmentID() {
        return PADepartmentID;
    }

    public void setPADepartmentID(Integer PADepartmentID) {
        this.PADepartmentID = PADepartmentID;
    }

    public Integer getPASubDeptID() {
        return PASubDeptID;
    }

    public void setPASubDeptID(Integer PASubDeptID) {
        this.PASubDeptID = PASubDeptID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPurchaseRestrictionType() {
        return purchaseRestrictionType;
    }

    public void setPurchaseRestrictionType(Integer purchaseRestrictionType) {
        this.purchaseRestrictionType = purchaseRestrictionType;
    }

}

