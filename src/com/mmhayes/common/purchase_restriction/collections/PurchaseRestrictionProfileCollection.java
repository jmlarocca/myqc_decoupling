package com.mmhayes.common.purchase_restriction.collections;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.purchase_restriction.models.PurchaseRestrictionProfileDetailModel;
import com.mmhayes.common.purchase_restriction.models.PurchaseRestrictionProfileModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//myqc dependencies
//other dependencies

/* Purchase Restriction Profile Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-08-22 13:48:29 -0400 (Thu, 22 Aug 2019) $: Date of last commit
 $Rev: 41067 $: Revision of last commit

 Notes: Collection of Purchase Restriction Profile Detail Models
*/
public class PurchaseRestrictionProfileCollection {
    List<PurchaseRestrictionProfileDetailModel> collection = new ArrayList<PurchaseRestrictionProfileDetailModel>();
    private static DataManager dm = new DataManager();

    public PurchaseRestrictionProfileCollection() {

    }

    //constructor - populates the collection
    public PurchaseRestrictionProfileCollection(Integer purchaseRestrictionProfileID) throws Exception {

        //populate this collection with models
        populateCollection(purchaseRestrictionProfileID);
    }

    //populates this collection with models
    public PurchaseRestrictionProfileCollection populateCollection(Integer purchaseRestrictionProfileID) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> purchaseRestrictionProfileList = dm.parameterizedExecuteQuery("data.posapi30.getPurchaseRestrictionProfileDetails",
                new Object[]{
                        purchaseRestrictionProfileID
                },
                true
        );

        //populate this collection from an array list of hashmaps
        if (purchaseRestrictionProfileList != null && purchaseRestrictionProfileList.size() > 0) {
            for(HashMap modelDetailHM : purchaseRestrictionProfileList) {
                if (modelDetailHM.get("ID") != null && !modelDetailHM.get("ID").toString().isEmpty()) {
                    getCollection().add(new PurchaseRestrictionProfileDetailModel(modelDetailHM));
                } else {
                    Logger.logMessage("ERROR: Could not determine purchase restriction profile detail ID in PurchaseRestrictionProfileCollection.populateCollection", Logger.LEVEL.ERROR);
                }
            }
        }

        return this;
    }

    public static PurchaseRestrictionProfileCollection getPurchaseRestrictionCollectionByEmployeeAndTerminalID(Integer employeeID, Integer terminalID) throws Exception {

        try {
            //get all models in an array list
            ArrayList<HashMap> purchaseRestrictionProfileList = dm.parameterizedExecuteQuery("data.posapi30.getPurchaseRestrictionProfileIDByEmployeeAndTerminalID",
                    new Object[]{employeeID, terminalID},
                    PosAPIHelper.getLogFileName(terminalID),
                    true
            );

            //should only be one result
            if (purchaseRestrictionProfileList != null && purchaseRestrictionProfileList.size() == 1) {
                HashMap resultHM = purchaseRestrictionProfileList.get(0);

                if (resultHM.containsKey("RESTRICTIONPROFILEID") && resultHM.get("RESTRICTIONPROFILEID") != null) {
                    Integer purchaseRestrictionProfileID = CommonAPI.convertModelDetailToInteger(resultHM.get("RESTRICTIONPROFILEID"));
                    return new PurchaseRestrictionProfileCollection(purchaseRestrictionProfileID);

                } else {
                    Logger.logMessage("Could not find RestrictionProfileID in PurchaseRestrictionProfileCollection.getPurchaseRestrictionCollectionByEmployeeAndTerminalID", Logger.LEVEL.DEBUG);
                }
            } else {
                Logger.logMessage("Could not retrieve RestrictionProfileID in PurchaseRestrictionProfileCollection.getPurchaseRestrictionCollectionByEmployeeAndTerminalID", Logger.LEVEL.DEBUG);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        return  new PurchaseRestrictionProfileCollection();

    }

    public static ArrayList<PurchaseRestrictionProfileModel> getPurchaseRestrictionsByRevenueCenterID(TerminalModel terminalModel) throws Exception {

        ArrayList<PurchaseRestrictionProfileModel> purchaseRestrictionProfileModels = new ArrayList();

        try {
            //get all models in an array list
            ArrayList<HashMap> purchaseRestrictionProfileListHM = dm.parameterizedExecuteQuery("data.posapi30.getPurchaseRestrictionProfilesByRevenueCenterID",
                    new Object[]{ terminalModel.getId()},
                    PosAPIHelper.getLogFileName(terminalModel.getId()),
                    true
            );

            //should only be one result
            if (purchaseRestrictionProfileListHM != null && !purchaseRestrictionProfileListHM.isEmpty()) {

                Integer tempId = -1;
                PurchaseRestrictionProfileModel purchaseRestrictionProfileModel = null;
                for (HashMap purchaseRestrictionProfileHM : purchaseRestrictionProfileListHM) {

                    Integer restrictionProfileId = CommonAPI.convertModelDetailToInteger(purchaseRestrictionProfileHM.get("RESTRICTIONPROFILEID"));

                    if (!tempId.equals(restrictionProfileId)){
                        purchaseRestrictionProfileModel = new PurchaseRestrictionProfileModel(purchaseRestrictionProfileHM);
                        purchaseRestrictionProfileModels.add(purchaseRestrictionProfileModel);
                        tempId = restrictionProfileId;
                    }

                    PurchaseRestrictionProfileDetailModel purchaseRestrictionProfileDetailModel = new PurchaseRestrictionProfileDetailModel(purchaseRestrictionProfileHM);
                    purchaseRestrictionProfileModel.getPurchaseRestrictionProfileDetailModels().add(purchaseRestrictionProfileDetailModel);
                }

            } else {
                Logger.logMessage("Could not retrieve RestrictionProfileID in PurchaseRestrictionProfileCollection.getPurchaseRestrictionsByRevenueCenterID", Logger.LEVEL.DEBUG);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        return purchaseRestrictionProfileModels;

    }

    //getter
    public List<PurchaseRestrictionProfileDetailModel> getCollection() {
        return this.collection;
    }

}
