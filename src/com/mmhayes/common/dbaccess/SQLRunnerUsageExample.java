package com.mmhayes.common.dbaccess;

//mmhayes dependencies
import com.mmhayes.common.utils.*;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-02-21 17:01:50 -0500 (Tue, 21 Feb 2017) $: Date of last commit
 $Rev: 3582 $: Revision of last commit

 Notes: A collection of usage examples for the SQLRunner - really serves as documentation
*/
public class SQLRunnerUsageExample {

    public SQLRunnerUsageExample() {

        //SQL RUNNER USAGE EXAMPLES
        try {

            //initialize sql runner and params arrayList

            SQLRunner SQLRunner = new SQLRunner();

            //EXAMPLE - Running INSERT statements
            Long lastInsertedID = SQLRunner.runInsert(new QueryInfo("data.sqlRunner.testInsert"));

            //BOOLEAN EXAMPLE - returns whether or not the query was successful
            Boolean wasInsertSuccessful = SQLRunner.runInsert_success(new QueryInfo("data.sqlRunner.testInsert"));

            //EXAMPLE - Running UPDATE statements (with one parameter)
            ArrayList<Object> updateParams = new ArrayList<Object>();
            updateParams.add(0, 1);

            //returns the count of how many records were updated
            Integer updatedRecordCount = SQLRunner.runUpdate(new QueryInfo("data.sqlRunner.testUpdate", updateParams));

            //INLINE PARAMETER AND BOOLEAN EXAMPLE - returns whether or not the query was successful -
            Boolean wasUpdateSuccessful = SQLRunner.runUpdate_success(new QueryInfo("data.sqlRunner.testUpdate", new ArrayList<Object>(){{
                add(1);
            }}));

            //EXAMPLE - Running an SELECT statement (with three parameters)
            ArrayList<Object> selectParams = new ArrayList<Object>();
            selectParams.add(0, 1);
            selectParams.add(1, 22);
            selectParams.add(2, 333);

            //returns array list of hashmaps of objects
            ArrayList<HashMap<String, Object>> queryResult = SQLRunner.runSelect(new QueryInfo("data.sqlRunner.test", selectParams));

            //returns a count of the records selected
            Integer returnedRecords = SQLRunner.runSelect_count(new QueryInfo("data.sqlRunner.test", selectParams));

            //INLINE PARAMETER AND BOOLEAN EXAMPLE - returns whether or not the query was successful
            Boolean wasSelectSuccessful = SQLRunner.runSelect_success(new QueryInfo("data.sqlRunner.test", new ArrayList<Object>(){{
                add(0, 1);
                add(1, 22);
                add(2, 333);
            }}));


            //EXAMPLE - Running a SELECT statement, no parameters, w/ custom HOOK to change all names to "ALL_THE_SAME_NAME"
            QueryInfo hookedQueryInfo = new QueryInfo("data.sqlRunner.hookTest");
            class changeAllNamesToTheSameName implements IQueryInfoHook {
                public void execute(QueryInfo queryInfo) throws Exception {
                    ArrayList<HashMap<String, Object>> results = queryInfo.getResults();
                    if (results != null) {
                        for (HashMap<String, Object> result : results) {
                            result.put("NAME", "ALL_THE_SAME_NAME");
                        }
                    }
                }
                public void executeBatch(ArrayList<QueryInfo> queryInfo) throws Exception {};
            }
            hookedQueryInfo.setPostProcessingHook(new changeAllNamesToTheSameName());
            ArrayList<HashMap<String, Object>> testHookedResult =  SQLRunner.runSelect(hookedQueryInfo);

            //EXAMPLE - Populating a query batch.... (will rollback if any exceptions occur)
            //    - Setup Dummy Data - Dummy Products, Dummy Printer, Dummy User (for example purpose only)
            //    - Add label printing header query to query batch, set hook to give printerID to children
            //    - Iterate through dummy products and label printing detail queries to query batch
            //    - Run the query batch

            //EXAMPLE - Query Batch - Setup Products, Printer, User
            ArrayList<HashMap> productList = new ArrayList<HashMap>();

            //create dummy first product
            HashMap firstProduct = new HashMap();
            firstProduct.put("PRODUCTID", 1);
            firstProduct.put("LABELSPERPRODUCT", 2);
            productList.add(firstProduct);

            //create dummy second product
            HashMap secondProduct = new HashMap();
            secondProduct.put("PRODUCTID", 2);
            secondProduct.put("LABELSPERPRODUCT", 3);
            productList.add(secondProduct);

            //create dummy third product
            HashMap thirdProduct = new HashMap();
            thirdProduct.put("PRODUCTID", 3);
            thirdProduct.put("LABELSPERPRODUCT", 1);
            productList.add(thirdProduct);

            //create dummy default printerID and userID
            Integer printerID = 9;
            Integer userID = 1;

            //EXAMPLE - Add label printing header query to query batch, set hook to give printerID to children
            ArrayList<QueryInfo> queryBatch = new ArrayList<QueryInfo>();

            //create label printer header query
            ArrayList<Object> labelPrintJobValues = new ArrayList<Object>();
            labelPrintJobValues.add(0, printerID);
            labelPrintJobValues.add(1, userID);
            QueryInfo labelPrintJobHeaderQuery = new QueryInfo("data.sqlRunner.createLabelPrintJob", labelPrintJobValues);
            labelPrintJobHeaderQuery.setType(QueryInfo.QUERY_INFO_TYPES.NONQUERY.getType());
            SQLRunner.addHookForHeaderIDAsLastChildParameter(labelPrintJobHeaderQuery, "data.sqlRunner.createLabelPrintJob", "data.sqlRunner.createLabelPrintJobDetail");
            queryBatch.add(labelPrintJobHeaderQuery);

            //EXAMPLE - Iterate through dummy products and label printing detail queries to query batch
            for (HashMap productHM : productList) {
                ArrayList<Object> labelPrintJobDetailValues = new ArrayList<Object>();
                labelPrintJobDetailValues.add(0, productHM.get("PRODUCTID").toString());
                labelPrintJobDetailValues.add(1, productHM.get("LABELSPERPRODUCT").toString());
                QueryInfo labelPrintJobDetailQuery = new QueryInfo("data.sqlRunner.createLabelPrintJobDetail", labelPrintJobDetailValues);
                labelPrintJobDetailQuery.setType(QueryInfo.QUERY_INFO_TYPES.NONQUERY.getType());
                queryBatch.add(labelPrintJobDetailQuery);
            }

            //EXAMPLE - Run the query batch
            SQLRunner.runQueryBatch(queryBatch);

            //EXAMPLE - Running an DELETE statement - Delete the records created in the query batch
            ArrayList<Object> deleteParams = new ArrayList<Object>();
            deleteParams.add(0, labelPrintJobHeaderQuery.getLastInsertedID());

            //returns the count of how many records were deleted
            Integer deletedRecordCount = SQLRunner.runUpdate(new QueryInfo("data.sqlRunner.testDelete_detail", deleteParams));

            //INLINE PARAMETER AND BOOLEAN EXAMPLE - returns whether or not the query was successful
            Boolean wasDeleteSuccessful = SQLRunner.runUpdate_success(new QueryInfo("data.sqlRunner.testDelete_header", new ArrayList<Object>(){{
                add(labelPrintJobHeaderQuery.getLastInsertedID());
            }}));

            Logger.logMessage("TEST - Successfully ran through SQLRunner examples!", Logger.LEVEL.TRACE);

        } catch (Exception ex) {
            Logger.logException(ex);
        }

    }
}
