package com.mmhayes.common.dbaccess;

//mmhayes dependencies
import com.mmhayes.common.utils.*;

//other dependencies
import java.util.*;

/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-02-21 17:01:50 -0500 (Tue, 21 Feb 2017) $: Date of last commit
 $Rev: 3582 $: Revision of last commit

 Notes: For running SQL statements. Contains the query specific information that the caller provides to run SQL as well as information pertaining to the results of the SQL
*/
public class QueryInfo {

    public static enum QUERY_INFO_TYPES { //specifies the different types of queries that can be ran
        QUERY(1), //SELECT statements
        NONQUERY(2); //UPDATE, DELETE, INSERT statements
        private final int type;
        private QUERY_INFO_TYPES(int type) {
            this.type = type;
        }
        public int getType() {
            return type;
        }
    }
    public int type = 1; //what type is this query?
    public String sqlPropName = null; //what's property name in the SQL String properties file?
    public String logFileName = null; //what file should the SQL log too?
    public ArrayList<Object> params = null; //the parameters for the SQL statement - Object should be VALID java objects that will be converted to JDBC objects
    public int timeoutInSecs = 0; //how long before the query times out? - defaults to "never" (0)
    public IQueryInfoHook postProcessingHook = null; //method that will be executed after the query has been processed
    public String rawSQLString = null; //the raw SQL statement
    public Long lastInsertedID = null; //for insert statements - the auto incremented ID of the insert
    public ArrayList<HashMap<String, Object>> results = null; //representation of the SQL result set
    public int affectedRows = 0; //how many rows were affected by running this query?
    public boolean successful = false; //did the query run successfully?

    public QueryInfo() throws Exception {

    }

    public QueryInfo(String sqlPropName) throws Exception {
        setSqlPropName(sqlPropName);
        initialize();
    }

    public QueryInfo(String sqlPropName, ArrayList<Object> params) throws Exception {
        setSqlPropName(sqlPropName);
        setParams(params);
        initialize();
    }

    public QueryInfo(String sqlPropName, ArrayList<Object> params, String logFileName) throws Exception {
        setSqlPropName(sqlPropName);
        setParams(params);
        setLogFileName(logFileName);
        initialize();
    }

    public QueryInfo(String sqlPropName, ArrayList<Object> params, String logFileName, int timeoutInSecs) throws Exception {
        setSqlPropName(sqlPropName);
        setParams(params);
        setLogFileName(logFileName);
        setTimeoutInSecs(timeoutInSecs);
        initialize();
    }

    public QueryInfo(String sqlPropName, ArrayList<Object> params, String logFileName, int timeoutInSecs, IQueryInfoHook postProcessingHook) throws Exception {
        setSqlPropName(sqlPropName);
        setParams(params);
        setLogFileName(logFileName);
        setTimeoutInSecs(timeoutInSecs);
        setPostProcessingHook(postProcessingHook);
        initialize();
    }

    //method ran during the construction process of this object
    private void initialize() throws Exception {

        //if there is no log file specified then set the default on the logger object
        if (getLogFileName() == null) {
            setLogFileName(Logger.getOutputFile());
        }

        //if there is a sql property name, grab it from the properties file
        if (getSqlPropName() != null) {
            setRawSQLString(MMHProperties.getSqlStringForSQLRunner(getSqlPropName(), getLogFileName()));
        }

    }

    //convert curly brace parameters (ex. {1}) to question marks and resort params array list appropriately
    public void convertBraceParameters() throws Exception {

        //if this sqlString contains at least one ? then don't both with the conversion at all or if the parameters are null
        if ((getRawSQLString().indexOf("?", 0) > 0) || (getParams() == null)) {
            return;
        }

        //initialize variables
        String rawSQLString = getRawSQLString(); //the raw sql string
        ArrayList<Integer> paramPositions = new ArrayList<Integer>(); //for storing the position of parameters in the raw sql string
        ArrayList<Object> newParams = new ArrayList<Object>(); //for storing the new correctly sorted parameters

        //iterate through every parameter
        for (int i = 0; i < getParams().size(); i++) {

            //this identifier will be used to find this arguments braces in the sqlString ("{1}" or "{5}" for example)
            String braceIdentifier = "{" + Integer.toString(i + 1) + "}";

            //keep going until this braceIdentifier does not exist in the sqlString
            while ((rawSQLString.indexOf(braceIdentifier, 0) > 0)) {

                //get the position of this parameter in the SQL string
                int thisParamPosition = rawSQLString.indexOf(braceIdentifier);

                //add this param positions list and then immediately sort the list
                paramPositions.add(thisParamPosition);
                Collections.sort(paramPositions);

                //add this parameter at the index of wherever it's position is in the raw sql string
                newParams.add(paramPositions.indexOf(thisParamPosition), getParams().get(i));

                //replace this brace with a question mark
                rawSQLString = rawSQLString.substring(0, thisParamPosition) + "?" + rawSQLString.substring(thisParamPosition + braceIdentifier.length(), rawSQLString.length());
            }
        }

        //set the correctly sorted parameters and rawSQLString - now ready for proper parameterization
        setParams(newParams);
        setRawSQLString(rawSQLString);
        Logger.logMessage("INFO: (SQLRunner) - Successfully converted brace parameters. About to return SQL: '"+getRawSQLString()+"'", getLogFileName(), Logger.LEVEL.TRACE);
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSqlPropName() {
        return sqlPropName;
    }

    public void setSqlPropName(String sqlPropName) {
        this.sqlPropName = sqlPropName;
    }

    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }

    public ArrayList<Object> getParams() {
        return params;
    }

    public void setParams(ArrayList<Object> params) {
        this.params = params;
    }

    public int getTimeoutInSecs() {
        return timeoutInSecs;
    }

    public void setTimeoutInSecs(int timeoutInSecs) {
        this.timeoutInSecs = timeoutInSecs;
    }

    public IQueryInfoHook getPostProcessingHook() {
        return postProcessingHook;
    }

    public void setPostProcessingHook(IQueryInfoHook postProcessingHook) {
        this.postProcessingHook = postProcessingHook;
    }

    public String getRawSQLString() {
        return rawSQLString;
    }

    public void setRawSQLString(String rawSQLString) {
        this.rawSQLString = rawSQLString;
    }

    public Long getLastInsertedID() {
        return lastInsertedID;
    }

    public void setLastInsertedID(Long lastInsertedID) {
        this.lastInsertedID = lastInsertedID;
    }

    public ArrayList<HashMap<String, Object>> getResults() {
        return results;
    }

    public void setResults(ArrayList<HashMap<String, Object>> results) {
        this.results = results;
    }

    public int getAffectedRows() {
        return affectedRows;
    }

    public void setAffectedRows(int affectedRows) {
        this.affectedRows = affectedRows;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

}
