package com.mmhayes.common.dbaccess;

//mmhayes dependencies
import com.mmhayes.common.api.errorhandling.exceptions.SQLRunnerException;
import com.mmhayes.common.utils.*;

//connection pool dependencies
import com.zaxxer.hikari.*;

//other dependencies
import java.sql.*;
import java.util.*;
import java.math.BigDecimal;

/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-02-21 17:01:50 -0500 (Tue, 21 Feb 2017) $: Date of last commit
 $Rev: 3582 $: Revision of last commit

 Notes: For running SQL statements. Acting as a replacement for the DataManager and Connection Pool Classes
*/
public class SQLRunner {

    private static HikariDataSource dataSource = null;

    public SQLRunner() throws Exception {
        try {

            //set properties for the connection pool
            HikariConfig hikariConfig = new HikariConfig();
            hikariConfig.setIdleTimeout(300000); //idle connections timeout in - 5 minutes
            hikariConfig.setMaxLifetime(1800000); //lifetime for a single connection - 30 minutes - in use connection will never expire
            hikariConfig.setMaximumPoolSize(15); //maximum amount of connections the pool is allowed to reach
            hikariConfig.addDataSourceProperty("cachePrepStmts", "true"); //allow caching prepared statements
            hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250"); //default max size
            hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048"); //default max sql limit

            //set database connection information for the connection pool
            hikariConfig.setJdbcUrl(MMHProperties.getAppSetting("database.connection.url"));
            hikariConfig.setUsername(MMHProperties.getAppSetting("database.connection.username"));
            hikariConfig.setPassword(StringFunctions.decodePassword(MMHProperties.getAppSetting("database.connection.password")));

            //create connection pool as a data source
            setDataSource(new HikariDataSource(hikariConfig));
            Logger.logMessage("INFO: (SQLRunner) - Created connection pool in SQLRunner()", Logger.LEVEL.TRACE);
        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }
    }

    //for running parameterized SQL select statements - returns Array List of HashMaps
    public ArrayList<HashMap<String, Object>> runSelect(QueryInfo queryInfo) throws Exception {
        Logger.logMessage("INFO: (SQLRunner) - SQL Start! About to run SQL select statement...", queryInfo.getLogFileName(), Logger.LEVEL.TRACE);
        queryInfo.setType(QueryInfo.QUERY_INFO_TYPES.QUERY.getType());
        execute(queryInfo);
        return queryInfo.getResults();
    };

    //for running parameterized SQL select statements - returns the count of how many records were selected
    public Integer runSelect_count(QueryInfo queryInfo) throws Exception {
        Logger.logMessage("INFO: (SQLRunner) - SQL Start! About to run SQL select statement...", queryInfo.getLogFileName(), Logger.LEVEL.TRACE);
        queryInfo.setType(QueryInfo.QUERY_INFO_TYPES.QUERY.getType());
        execute(queryInfo);
        return queryInfo.getResults().size();
    }

    //for running parameterized SQL select statements - returns Boolean for if the query returned at least one result
    public Boolean runSelect_success(QueryInfo queryInfo) throws Exception {
        Logger.logMessage("INFO: (SQLRunner) - SQL Start! About to run SQL select statement...", queryInfo.getLogFileName(), Logger.LEVEL.TRACE);
        queryInfo.setType(QueryInfo.QUERY_INFO_TYPES.QUERY.getType());
        execute(queryInfo);
        return queryInfo.isSuccessful();
    }

    //for running parameterized SQL insert statements - returns the ID of the inserted record
    public Long runInsert(QueryInfo queryInfo) throws Exception {
        Logger.logMessage("INFO: (SQLRunner) - SQL Start! About to run SQL insert statement...", queryInfo.getLogFileName(), Logger.LEVEL.TRACE);
        queryInfo.setType(QueryInfo.QUERY_INFO_TYPES.NONQUERY.getType());
        execute(queryInfo);
        return queryInfo.getLastInsertedID();
    }

    //for running parameterized SQL insert statements - returns if record was inserted successfully or not
    public Boolean runInsert_success(QueryInfo queryInfo) throws Exception {
        Logger.logMessage("INFO: (SQLRunner) - SQL Start! About to run SQL insert statement...", queryInfo.getLogFileName(), Logger.LEVEL.TRACE);
        queryInfo.setType(QueryInfo.QUERY_INFO_TYPES.NONQUERY.getType());
        execute(queryInfo);
        return queryInfo.isSuccessful();
    }

    //for running parameterized SQL update statements - returns the count of records that were updated
    public Integer runUpdate(QueryInfo queryInfo) throws Exception {
        Logger.logMessage("INFO: (SQLRunner) - SQL Start! About to run SQL update statement...", queryInfo.getLogFileName(), Logger.LEVEL.TRACE);
        queryInfo.setType(QueryInfo.QUERY_INFO_TYPES.NONQUERY.getType());
        execute(queryInfo);
        return queryInfo.getAffectedRows();
    }

    //for running parameterized SQL update statements - returns whether the update statement was successful or not
    public Boolean runUpdate_success(QueryInfo queryInfo) throws Exception {
        Logger.logMessage("INFO: (SQLRunner) - SQL Start! About to run SQL update statement...", queryInfo.getLogFileName(), Logger.LEVEL.TRACE);
        queryInfo.setType(QueryInfo.QUERY_INFO_TYPES.NONQUERY.getType());
        execute(queryInfo);
        return queryInfo.isSuccessful();
    }

    //for running parameterized SQL update statements - returns the count of records that were deleted
    public Integer runDelete(QueryInfo queryInfo) throws Exception {
        Logger.logMessage("INFO: (SQLRunner) - SQL Start! About to run SQL delete statement...", queryInfo.getLogFileName(), Logger.LEVEL.TRACE);
        queryInfo.setType(QueryInfo.QUERY_INFO_TYPES.NONQUERY.getType());
        execute(queryInfo);
        return queryInfo.getAffectedRows();
    }

    //for running parameterized SQL update statements - returns whether the update statement was successful or not
    public Boolean runDelete_success(QueryInfo queryInfo) throws Exception {
        Logger.logMessage("INFO: (SQLRunner) - SQL Start! About to run SQL delete statement...", queryInfo.getLogFileName(), Logger.LEVEL.TRACE);
        queryInfo.setType(QueryInfo.QUERY_INFO_TYPES.NONQUERY.getType());
        execute(queryInfo);
        return queryInfo.isSuccessful();
    }

    //for running MANY queries at once
    public void runQueryBatch(ArrayList<QueryInfo> queryInfoBatch) throws Exception {
        Logger.logMessage("INFO: (SQLRunner) - SQL Start! About to run SQL batch containing " +queryInfoBatch.size()+ " queries!", Logger.LEVEL.TRACE);
        executeBatch(queryInfoBatch);
    }

    //adds the headerID as the last parameter for all children in a query batch
    public void addHookForHeaderIDAsLastChildParameter(QueryInfo queryInfo, String insertParentSQLProp, String insertChldSQLProp) throws Exception {

        //create hook for adding the inserted ID as the last parameter SQL
        class addHeaderIDAsLastChildParameterHook implements IQueryInfoHook {
            public void execute(QueryInfo queryInfo) throws Exception {}
            public void executeBatch(ArrayList<QueryInfo> queryBatch) throws Exception {

                //iterate through queryBatch to determine the insertParentID
                Long insertedParentID = null;
                for (QueryInfo parentQueryInfo : queryBatch) {
                    if (parentQueryInfo.getSqlPropName().equals(insertParentSQLProp)) {
                        insertedParentID = parentQueryInfo.getLastInsertedID();
                    }
                }

                //now that the insertedParentID has been determined, iterate through the batch again and set on all children
                for (QueryInfo childQueryInfo : queryBatch) {
                    if (childQueryInfo.getSqlPropName().equals(insertChldSQLProp)) {
                        if (insertedParentID != null) {
                            childQueryInfo.getParams().add(childQueryInfo.getParams().size(), insertedParentID);
                        } else {
                            Logger.logMessage("ERROR: Cannot find insertedParentID in addHeaderIDAsLastChildParameter()", Logger.LEVEL.ERROR);
                        }
                    }
                }

            };
        }
        queryInfo.setPostProcessingHook(new addHeaderIDAsLastChildParameterHook());
    }

    //calls appropriate "execute" method based on the query info type
    private void execute(QueryInfo queryInfo) throws Exception {

        //grab a connection from the data source, all resources will be cleaned up
        try (Connection connection = getDataSource().getConnection()) {

            //determine type of query and call appropriate "execute" method
            if (queryInfo.getType() == QueryInfo.QUERY_INFO_TYPES.QUERY.getType()) {
                executeQuery(queryInfo, connection);
            } else if (queryInfo.getType() == QueryInfo.QUERY_INFO_TYPES.NONQUERY.getType()) {
                executeUpdate(queryInfo, connection);
            } else { //could not find a valid query info type...
                connection.rollback();
                queryInfo.setSuccessful(false);
                throw new SQLRunnerException();
            }

        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }
    }

    //calls appropriate "execute" method based on the query info type for MANY query info objects
    private void executeBatch(ArrayList<QueryInfo> queryInfoBatch) throws Exception {

        //grab a connection from the data source, all resources will be cleaned up
        try (Connection connection = getDataSource().getConnection()) {

            //tell the connection to wait to commit until all queries have been ran
            connection.setAutoCommit(false);

            //run each query in the batch
            for (QueryInfo queryInfo : queryInfoBatch) {

                if (queryInfo.getType() == QueryInfo.QUERY_INFO_TYPES.QUERY.getType()) {
                    executeQuery(queryInfo, connection);
                } else if (queryInfo.getType() == QueryInfo.QUERY_INFO_TYPES.NONQUERY.getType()) {
                    executeUpdate(queryInfo, connection);
                } else { //could not find a valid query info type...
                    connection.rollback();
                    throw new SQLRunnerException();
                }

                //if there are post processing hooks - execute them after each query is ran!
                if (queryInfo.getPostProcessingHook() != null) {
                    executeBatchHook(queryInfo.getPostProcessingHook(), queryInfoBatch);
                }

            }

            //all queries have been successfully ran, we can now commit
            connection.commit();

        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }
    }

    //generic execute query - returns Array List of Hashmaps of Objects (result set is cleaned up in method and is not returned)
    private void executeQuery(QueryInfo queryInfo, Connection connection) throws Exception {

        //process prepared statement and result set here, all resources will be cleaned up
        try (PreparedStatement preparedStatement = createPreparedStatement(connection, queryInfo, true);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            //process the resultSet here, all resources will be cleaned up
            queryInfo.setResults(processResultSet(resultSet));

            //if there are post processing hooks - execute them!
            if (queryInfo.getPostProcessingHook() != null) {
                executeHook(queryInfo.getPostProcessingHook(), queryInfo);
            }

            //set that the query ran successfully
            queryInfo.setSuccessful(true);

            //log that the SQL was successfully how many rows were records
            Logger.logMessage("INFO: (SQLRunner) - Successfully ran executeQuery and processed result set. SQL: "+queryInfo.getRawSQLString(), queryInfo.getLogFileName(), Logger.LEVEL.TRACE);
            Logger.logMessage("INFO: (SQLRunner) - SQL Complete! DB Records Returned: "+queryInfo.getResults().size(), queryInfo.getLogFileName(), Logger.LEVEL.TRACE);

        } catch (Exception ex) {
            Logger.logException(ex);
            connection.rollback(); //rollback connection if ANY exception occurred
            queryInfo.setSuccessful(false);
            throw ex;
        }
    }

    //generic execute update - returns either (1) the row count for SQL Data Manipulation Language (DML) statements or (2) 0 for SQL statements that return nothing
    private void executeUpdate(QueryInfo queryInfo, Connection connection) throws Exception {

        //process prepared statement here, all resources will be cleaned up
        try (PreparedStatement preparedStatement = createPreparedStatement(connection, queryInfo, false)) {

            //execute prepared statement here, all resources will be cleaned up
            queryInfo.setAffectedRows(preparedStatement.executeUpdate());

            //processes the result, all resources will be cleaned up
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {

                //get the last inserted ID and set it on queryInfo (for INSERT statements)
                resultSet.next();
                queryInfo.setLastInsertedID(resultSet.getLong(1));

                //process the resultSet here, all resources will be cleaned up
                queryInfo.setResults(processResultSet(resultSet));

            } catch (Exception ex) {
                Logger.logException(ex);
                throw ex;
            }

            //if there are post processing hooks - execute them!
            if (queryInfo.getPostProcessingHook() != null) {
                executeHook(queryInfo.getPostProcessingHook(), queryInfo);
            }

            //set that the query ran successfully
            queryInfo.setSuccessful(true);

            //log that the SQL was successfully how many records were affected
            Logger.logMessage("INFO: (SQLRunner) - Successfully ran executeUpdate. SQL: "+queryInfo.getRawSQLString(), queryInfo.getLogFileName(), Logger.LEVEL.TRACE);
            Logger.logMessage("INFO: (SQLRunner) - SQL Complete! DB Records Affected: "+queryInfo.getAffectedRows(), queryInfo.getLogFileName(), Logger.LEVEL.TRACE);

        } catch (Exception ex) {
            Logger.logException(ex);
            connection.rollback(); //rollback connection if ANY exception occurred
            queryInfo.setSuccessful(false);
            throw ex;
        }
    }

    //converts SQL result sets to ArrayList of HashMaps
    private ArrayList<HashMap<String, Object>> processResultSet(ResultSet resultSet) throws Exception {

        if (resultSet == null) { //ensure result set is valid
            throw new SQLRunnerException();
        }

        //initialize variables
        ArrayList<HashMap<String, Object>> processedResultSet = new ArrayList<HashMap<String, Object>>();
        ResultSetMetaData rsmd = resultSet.getMetaData();
        int resultSetColumnCount = rsmd.getColumnCount();

        if (resultSetColumnCount > 0) { //ensure there are columns in the result set

            while (resultSet.next()) { //iterate through all the rows in the result set

                //hashmap that represents a single row in the result set
                HashMap<String, Object> processedRow = new HashMap<String, Object>();

                for (int i = 1; i <= resultSetColumnCount; i++) { //iterate through columns in result set
                    processedRow = mapJDBCTypesToJavaTypes(processedRow, rsmd, resultSet, i);
                }
                
                //add the processed row to the processed result set
                processedResultSet.add(processedRow);
            }
        }
        return processedResultSet;
    }

    //sets params list in QueryInfo on the Raw SQL String - returns a PreparedStatement that is ready for execution
    private PreparedStatement createPreparedStatement(Connection connection, QueryInfo queryInfo, Boolean logExecuteQuery) throws Exception {
        Logger.logMessage("INFO: (SQLRunner) - Preparing parameters for SQL statement...", queryInfo.getLogFileName(), Logger.LEVEL.DEBUG);

        //convert curly brace parameters (ex. {1}) to question marks and resort params array list appropriately
        queryInfo.convertBraceParameters();

        //prepare the raw SQL String
        PreparedStatement preparedStatement = connection.prepareStatement(queryInfo.getRawSQLString(), Statement.RETURN_GENERATED_KEYS);

        //iterate through parameters and set approriately
        ArrayList<Object> params = queryInfo.getParams();
        if (params != null) {
            String paramListForLogging = "";
            for (int i = 0; i < params.size(); i++) {

                //set parameter on preparedStatement
                preparedStatement = mapJavaTypesToJDBCTypes(preparedStatement, params, i);

                //build a list of parameters to log (the logic here is just to make log "pretty")
                paramListForLogging += "Parameter " + (i+1 <= 9 ? "#"+(i+1)+" " : "#"+(i+1)) + " = { " + params.get(i) + " }" + (i+2 <= params.size() ? "\n" : "");

            }
            Logger.logMessage("INFO: (SQLRunner) - Successfully prepared the following parameters: \n" + paramListForLogging, queryInfo.getLogFileName(), Logger.LEVEL.DEBUG);
        }
        if (logExecuteQuery) {
            Logger.logMessage("INFO: (SQLRunner) - About to run executeQuery on prepared statement for SQL String: "+queryInfo.getRawSQLString(), queryInfo.getLogFileName(), Logger.LEVEL.DEBUG);
        }
        return preparedStatement;
    }

    //maps Java Data types to JDBC data types for a single parameter in a query
    private PreparedStatement mapJavaTypesToJDBCTypes(PreparedStatement preparedStatement, ArrayList<Object> params, Integer parameterIndex) throws Exception {

        /* Map Java Data Types to JDBC Data Types
            String 	VARCHAR or LONGVARCHAR
            java.math.BigDecimal 	NUMERIC
            boolean 	BIT
            byte 	TINYINT
            short 	SMALLINT
            int 	INTEGER
            long 	BIGINT
            float 	REAL
            double 	DOUBLE
            byte[] 	VARBINARY or LONGVARBINARY
            java.sql.Date 	DATE
            java.sql.Time 	TIME
            java.sql.Timestamp 	TIMESTAMP
        */
        Integer preparedStatementIndex = parameterIndex+1;
        if (params.get(parameterIndex) instanceof String) {
            String parameterValue = params.get(parameterIndex).toString();
            preparedStatement.setString(preparedStatementIndex, parameterValue);
        } else if (params.get(parameterIndex) instanceof java.math.BigDecimal) {
            BigDecimal parameterValue = (BigDecimal)(params.get(parameterIndex));
            preparedStatement.setBigDecimal(preparedStatementIndex, parameterValue);
        } else if (params.get(parameterIndex) instanceof Boolean) {
            Boolean parameterValue = (Boolean)(params.get(parameterIndex));
            preparedStatement.setBoolean(preparedStatementIndex, parameterValue);
        } else if (params.get(parameterIndex) instanceof Integer) {
            Integer parameterValue = (Integer)(params.get(parameterIndex));
            preparedStatement.setInt(preparedStatementIndex, parameterValue);
        } else if (params.get(parameterIndex) instanceof Long) {
            Long parameterValue = (Long)(params.get(parameterIndex));
            preparedStatement.setLong(preparedStatementIndex, parameterValue);
        } else if (params.get(parameterIndex) instanceof Float) {
            Float parameterValue = (Float)(params.get(parameterIndex));
            preparedStatement.setFloat(preparedStatementIndex, parameterValue);
        } else if (params.get(parameterIndex) instanceof Double) {
            Double parameterValue = (Double)(params.get(parameterIndex));
            preparedStatement.setDouble(preparedStatementIndex, parameterValue);
        } else if (params.get(parameterIndex) instanceof byte[]) {
            byte[] parameterValue = (byte[])(params.get(parameterIndex));
            preparedStatement.setBytes(preparedStatementIndex, parameterValue);
        } else if (params.get(parameterIndex) instanceof java.sql.Date) {
            java.sql.Date parameterValue = (java.sql.Date)(params.get(parameterIndex));
            preparedStatement.setDate(preparedStatementIndex, parameterValue);
        } else if (params.get(parameterIndex) instanceof Time) {
            Time parameterValue = (Time)(params.get(parameterIndex));
            preparedStatement.setTime(preparedStatementIndex, parameterValue);
        } else if (params.get(parameterIndex) instanceof Timestamp) {
            Timestamp parameterValue = (Timestamp)(params.get(parameterIndex));
            preparedStatement.setTimestamp(preparedStatementIndex, parameterValue);
        }

        return preparedStatement;
    }

    //maps JDBC data types to Java Data types for a single column in a result set
    private HashMap<String, Object> mapJDBCTypesToJavaTypes(HashMap<String, Object> processedRow, ResultSetMetaData rsmd, ResultSet resultSet, Integer resultSetIndex) throws Exception {

        //get column meta data
        int columnDataType = rsmd.getColumnType(resultSetIndex);
        //String columnDataTypeName = rsmd.getColumnTypeName(resultSetIndex);
        String columnName = rsmd.getColumnName(resultSetIndex).toUpperCase();

        /* Map JDBC data types to Java Data Types
            JDBC type 	Java type
            CHAR 	String
            VARCHAR 	String
            LONGVARCHAR 	String
            NUMERIC 	java.math.BigDecimal
            DECIMAL 	java.math.BigDecimal
            BIT 	boolean
            TINYINT 	byte
            SMALLINT 	short
            INTEGER 	int
            BIGINT 	long
            REAL 	float
            FLOAT 	double
            DOUBLE 	double
            BINARY 	byte[]
            VARBINARY 	byte[]
            LONGVARBINARY 	byte[]
            DATE 	java.sql.Date
            TIME 	java.sql.Time
            TIMESTAMP 	java.sql.Timestamp
        */
        if (columnDataType == Types.CHAR || columnDataType == Types.VARCHAR || columnDataType == Types.LONGVARCHAR) {
            String columnValue = resultSet.getString(resultSetIndex);
            processedRow.put(columnName, columnValue);
        } else if (columnDataType == Types.NUMERIC || columnDataType == Types.DECIMAL) {
            BigDecimal columnValue = resultSet.getBigDecimal(resultSetIndex);
            processedRow.put(columnName, columnValue);
        } else if (columnDataType == Types.BIT) {
            Boolean columnValue = resultSet.getBoolean(resultSetIndex);
            processedRow.put(columnName, columnValue);
        } else if (columnDataType == Types.TINYINT) {
            Byte columnValue = resultSet.getByte(resultSetIndex);
            processedRow.put(columnName, columnValue);
        } else if (columnDataType == Types.SMALLINT) {
            Short columnValue = resultSet.getShort(resultSetIndex);
            processedRow.put(columnName, columnValue);
        } else if (columnDataType == Types.INTEGER) {
            Integer columnValue = resultSet.getInt(resultSetIndex);
            processedRow.put(columnName, columnValue);
        } else if (columnDataType == Types.BIGINT) {
            Long columnValue = resultSet.getLong(resultSetIndex);
            processedRow.put(columnName, columnValue);
        } else if (columnDataType == Types.REAL) {
            Float columnValue = resultSet.getFloat(resultSetIndex);
            processedRow.put(columnName, columnValue);
        } else if (columnDataType == Types.DOUBLE) {
            Double columnValue = resultSet.getDouble(resultSetIndex);
            processedRow.put(columnName, columnValue);
        } else if (columnDataType == Types.BINARY || columnDataType == Types.VARBINARY || columnDataType == Types.LONGVARBINARY) {
            byte[] columnValue = resultSet.getBytes(resultSetIndex);
            processedRow.put(columnName, columnValue);
        } else if (columnDataType == Types.DATE) {
            java.util.Date columnValue = resultSet.getDate(resultSetIndex);
            processedRow.put(columnName, columnValue);
        } else if (columnDataType == Types.TIME) {
            Time columnValue = resultSet.getTime(resultSetIndex);
            processedRow.put(columnName, columnValue);
        } else if (columnDataType == Types.TIMESTAMP) {
            Timestamp columnValue = resultSet.getTimestamp(resultSetIndex);
            processedRow.put(columnName, columnValue);
        }
        return processedRow;
    }

    //executes any given method that implements the IQueryInfoHook interface - allows for post-processing of query info
    private void executeHook(IQueryInfoHook sqlRunnerHook, QueryInfo queryInfo) throws Exception {
        sqlRunnerHook.execute(queryInfo);
    }

    //executes any given method that implements the IQueryInfoHook interface - allows for post-processing of query info
    private void executeBatchHook(IQueryInfoHook sqlRunnerHook, ArrayList<QueryInfo> queryInfoBatch) throws Exception {
        sqlRunnerHook.executeBatch(queryInfoBatch);
    }

    private static HikariDataSource getDataSource() {
        return dataSource;
    }

    private static void setDataSource(HikariDataSource dataSource) {
        SQLRunner.dataSource = dataSource;
    }

}
