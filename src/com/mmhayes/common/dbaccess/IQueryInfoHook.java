package com.mmhayes.common.dbaccess;

//mmhayes dependencies
import com.mmhayes.common.utils.*;

//other dependencies
import java.util.*;

/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-02-21 17:01:50 -0500 (Tue, 21 Feb 2017) $: Date of last commit
 $Rev: 3582 $: Revision of last commit

 Notes: Interface for SQL Runner Hooks which allow methods to be called after SQL has been ran
*/
public interface IQueryInfoHook
{
    public void execute(QueryInfo queryInfo) throws Exception;
    public void executeBatch(ArrayList<QueryInfo> queryInfoBatch) throws Exception;
}
