package com.mmhayes.common.kitchenPrinters;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-11-03 10:09:01 -0500 (Tue, 03 Nov 2020) $: Date of last commit
    $Rev: 13030 $: Revision of last commit
    Notes: Defines how to convert a LocalPrintJob into JSON.
*/

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.mmhayes.common.utils.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>Defines how to convert a LocalPrintJob into JSON.</p>
 *
 */
public class LocalPrintJobAdapter extends TypeAdapter<LocalPrintJob> {

// <editor-fold desc="Convert a LocalPrintJob to JSON.">
    /**
     * <p>Writes a JSON {@link String} for the given {@link LocalPrintJob}.</p>
     *
     * @param jsonWriter The {@link JsonWriter} to use to write the JSON {@link String}.
     * @param localPrintJob The {@link LocalPrintJob} to convert to JSON.
     * @throws IOException
     */
    @SuppressWarnings("WhileLoopReplaceableByForEach")
    @Override
    public void write (JsonWriter jsonWriter, LocalPrintJob localPrintJob) throws IOException {

        try {
            jsonWriter.beginObject();
            jsonWriter.name("paPrinterQueueID").value(localPrintJob.getPAPrinterQueueID());
            jsonWriter.name("terminalID").value(localPrintJob.getTerminalID());
            jsonWriter.name("revenueCenterID").value(localPrintJob.getRevenueCenterID());
            jsonWriter.name("printerID").value(localPrintJob.getPrinterID());
            jsonWriter.name("kmsStationID").value(localPrintJob.getKMSStationID());
            jsonWriter.name("isKDS").value(localPrintJob.getIsKDS());
            jsonWriter.name("isOnlineOrder").value(localPrintJob.getIsOnlineOrder());
            jsonWriter.name("details");
            jsonWriter.beginArray();
            for (HashMap detail : localPrintJob.getDetails()) {
                jsonWriter.beginObject();
                for (Object detailEntryObj : detail.entrySet()) {
                    Map.Entry detailEntry = ((Map.Entry) detailEntryObj);
                    String key = (detailEntry.getKey() != null ? detailEntry.getKey().toString() : "");
                    if (detailEntry.getValue() instanceof Number) {
                        jsonWriter.name(key).value(((Number) detailEntry.getValue()));
                    }
                    else if (detailEntry.getValue() instanceof Boolean) {
                        jsonWriter.name(key).value(((Boolean) detailEntry.getValue()));
                    }
                    else if (detailEntry.getValue() instanceof Character) {
                        jsonWriter.name(key).value(((Character) detailEntry.getValue()));
                    }
                    else if (detailEntry.getValue() instanceof String) {
                        jsonWriter.name(key).value(detailEntry.getValue().toString());
                    }
                    else {
                        jsonWriter.name(key).value("");
                    }
                }
                jsonWriter.endObject();
            }
            jsonWriter.endArray();
            jsonWriter.endObject();
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("A problem occurred while trying to write out the LocalPrintJob as JSON!", Logger.LEVEL.ERROR);
        }

    }
// </editor-fold>

// <editor-fold desc="Convert JSON to a LocalPrintJob">
    /**
     * <p>Reads a {@link LocalPrintJob} from a JSON {@link String}.</p>
     *
     * @param jsonReader The {@link JsonReader} to use to read in the {@link LocalPrintJob}.
     * @return The {@link LocalPrintJob} read from the JSON {@link String}.
     * @throws IOException
     */
    @Override
    public LocalPrintJob read (JsonReader jsonReader) throws IOException {
        LocalPrintJob localPrintJob = new LocalPrintJob();

        try {
            jsonReader.beginObject();
            String field = null;
            while (jsonReader.hasNext()) {
                JsonToken jsonToken = jsonReader.peek();
                if (jsonToken.equals(JsonToken.NAME)) {
                    field = jsonReader.nextName();
                }
                if ("paPrinterQueueID".equalsIgnoreCase(field)) {
                    localPrintJob.setPAPrinterQueueID(jsonReader.nextInt());
                }

                if ("terminalID".equalsIgnoreCase(field)) {
                    localPrintJob.setTerminalID(jsonReader.nextInt());
                }

                if ("revenueCenterID".equalsIgnoreCase(field)) {
                    localPrintJob.setRevenueCenterID(jsonReader.nextInt());
                }

                if ("printerID".equalsIgnoreCase(field)) {
                    localPrintJob.setPrinterID(jsonReader.nextInt());
                }

                if ("kmsStationID".equalsIgnoreCase(field)) {
                    localPrintJob.setKMSStationID(jsonReader.nextInt());
                }

                if ("isKDS".equalsIgnoreCase(field)) {
                    localPrintJob.setIsKDS(jsonReader.nextBoolean());
                }

                if ("isOnlineOrder".equalsIgnoreCase(field)) {
                    localPrintJob.setIsOnlineOrder(jsonReader.nextBoolean());
                }

                if ("details".equalsIgnoreCase(field)) {
                    ArrayList<HashMap> details = readPrintJobDetails(jsonReader);
                    localPrintJob.setDetails(details);
                }

            }
            jsonReader.endObject();
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("A problem occurred while trying to parse the LocalPrintJob from JSON!", Logger.LEVEL.ERROR);
        }

        return localPrintJob;
    }

    /**
     * <p>Reads the print job details for the {@link LocalPrintJob} from the JSON {@link String}.</p>
     *
     * @param jsonReader The {@link JsonReader} to use to read the print job details from the JSON {@link String} for the {@link LocalPrintJob}.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print job details for the {@link LocalPrintJob} read from the JSON {@link String}
     * @throws IOException
     */
    private ArrayList<HashMap> readPrintJobDetails (JsonReader jsonReader) throws IOException {

        ArrayList<HashMap> details = new ArrayList<>();

        jsonReader.beginArray();
        while (jsonReader.hasNext()) {
            HashMap detailHM = readDetailHM(jsonReader);
            details.add(detailHM);
        }
        jsonReader.endArray();

        return details;
    }

    /**
     * <p>Reads a print job detail for the {@link LocalPrintJob} from the JSON {@link String}.</p>
     *
     * @param jsonReader The {@link JsonReader} to use to read the print job detail from the JSON {@link String} for the {@link LocalPrintJob}.
     * @return A {@link HashMap} corresponding to a print job detail for the {@link LocalPrintJob} read from the JSON {@link String}.
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    private HashMap readDetailHM (JsonReader jsonReader) throws IOException {

        HashMap detailHM = new HashMap();

        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String key = jsonReader.nextName();
            if (key.equalsIgnoreCase("PICKUPDELIVERYNOTE")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("ORDERNUM")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PRINTCONTROLLERID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PRINTERHARDWARETYPEID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("QUANTITY")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("KMSORDERSTATUSID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("QUEUETIME")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PHONE")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("TRANSNAMELABEL")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PERSONNAME")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("TRANSCOMMENT")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("TRANSTYPEID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("TRANSACTIONDATE")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("TRANSNAME")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PAPLUID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("HIDESTATION")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PRINTERID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("ESTIMATEDORDERTIME")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PAORDERTYPEID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("LINKEDFROMIDS")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PAPRINTERQUEUEDETAILID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("ISMODIFIER")) {
                detailHM.put(key, jsonReader.nextBoolean());
            }
            else if (key.equalsIgnoreCase("PATRANSLINEITEMID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("ISKDS")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PATRANSACTIONID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PREVKDSORDERNUMS")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("ONLINEORDER")) {
                detailHM.put(key, jsonReader.nextBoolean());
            }
            else if (key.equalsIgnoreCase("STATIONID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PAKDSUSESMSNOTIFICATIONS")) {
                detailHM.put(key, jsonReader.nextBoolean());
            }
            else if (key.equalsIgnoreCase("PAPRINTERQUEUEID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PRINTEDDTM")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("TERMINALID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("ONLINETRANS")) {
                detailHM.put(key, jsonReader.nextBoolean());
            }
            else if (key.equalsIgnoreCase("LINEDETAIL")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("KMSSTATIONID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("HDRPRINTSTATUSID")) {
                detailHM.put(key, jsonReader.nextString());
            }
            else if (key.equalsIgnoreCase("PRINTSTATUSID")) {
                detailHM.put(key, jsonReader.nextString());
            }
        }
        jsonReader.endObject();

        return detailHM;
    }
// </editor-fold>

}