package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-07-20 11:48:01 -0400 (Mon, 20 Jul 2020) $: Date of last commit
    $Rev: 50368 $: Revision of last commit
    Notes: An object representing a detail in a QCPrintJob.
*/

import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.HashMap;
import java.util.Objects;

/**
 * Class representing a detail within a QCPrintJob.
 *
 */
public class QCPrintJobDetail {

    // variables within the QCPrintJobDetail scope
    private transient QCPrintJobDetail parentPrintJobDetail;
    private int paPrinterQueueDetailID;
    private transient int paTransLineItemID;
    private String lineDetail;
    private transient int paPluID;
    private transient int printerID;
    private transient int stationID;
    private transient double quantity;
    private transient boolean printsOnExpeditor;
    private transient String hideStation;
    private transient boolean isModifier;
    private int KMSStationID;
    private transient int KMSOrderStatusID;
    private transient String linkedFromIDs;

    /**
     * <p>Empty constructor for a {@link QCPrintJobDetail}.</p>
     *
     */
    public QCPrintJobDetail () {}

    /**
     * Constructor for a QCPrintJobDetail.
     *
     * @param printDetail {@link HashMap} HashMap containing the information needed to create an instance of a
     *         QCPrintJobDetail.
     */
    public QCPrintJobDetail (HashMap printDetail) {

        try {
            this.paPrinterQueueDetailID = HashMapDataFns.getIntVal(printDetail, "PAPRINTERQUEUEDETAILID");
            this.lineDetail = HashMapDataFns.getStringVal(printDetail, "LINEDETAIL");
            this.paPluID = HashMapDataFns.getIntVal(printDetail, "PAPLUID");
            this.printerID = HashMapDataFns.getIntVal(printDetail, "PRINTERID");
            this.stationID = HashMapDataFns.getIntVal(printDetail, "STATIONID");
            this.quantity = HashMapDataFns.getDoubleVal(printDetail, "QUANTITY");
            this.printsOnExpeditor = HashMapDataFns.getBooleanVal(printDetail, "PRINTSONEXPEDITOR");
            this.hideStation = HashMapDataFns.getStringVal(printDetail, "HIDESTATION");
            this.isModifier = HashMapDataFns.getBooleanVal(printDetail, "ISMODIFIER");
            this.KMSStationID = HashMapDataFns.getIntVal(printDetail, "KMSSTATIONID");
            this.KMSOrderStatusID = HashMapDataFns.getIntVal(printDetail, "KMSORDERSTATUSID");
            this.paTransLineItemID = HashMapDataFns.getIntVal(printDetail, "PATRANSLINEITEMID");
            this.linkedFromIDs = HashMapDataFns.getStringVal(printDetail, "LINKEDFROMIDS");
        }
        catch (Exception e) {
            Logger.logException(e, QCPrintJob.getLogFile());
            Logger.logMessage("There was a problem trying to construct an instance of a QCPrintJobDetail in the " +
                    "QCPrintJobDetail's constructor", QCPrintJob.getLogFile(), Logger.LEVEL.ERROR);
        }
    }

    /**
     * Decodes the base 64 encoded line detail and returns the result.
     *
     * @return {@link String} The decoded base 64 line detail.
     */
    public String getDecodedLineDetail () {
        String decodedLineDetail = "";

        try {
            if ((lineDetail != null) && (!lineDetail .isEmpty())) {
                decodedLineDetail = KitchenPrinterCommon.decode(lineDetail, QCPrintJob.getLogFile());
            }
        }
        catch (Exception e) {
            Logger.logException(e, QCPrintJob.getLogFile());
            Logger.logMessage("Unable to decode the QCPrintJobDetail's line detail of " +
                    Objects.toString(lineDetail, "NULL")+" in QCPrintJobDetail.getDecodedLineDetail",
                    QCPrintJob.getLogFile(), Logger.LEVEL.ERROR);
        }

        return decodedLineDetail;
    }

    /**
     * <p>Converts the {@link QCPrintJobDetail} instance to a {@link String}.</p>
     *
     * @return The {@link QCPrintJobDetail} instance as a {@link String}.
     */
    @Override
    public String toString () {

        return String.format("PAPRINTERQUEUEDETAILID: %s, LINEDETAIL: %s, PAPLUID: %s, PRINTERID: %s, STATIONID: %s, QUANTITY: %s, PRINTSONEXPEDITOR: %s, HIDESTATION: %s, LINKEDFROMIDS: %s",
                Objects.toString((paPrinterQueueDetailID > 0 ? paPrinterQueueDetailID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(lineDetail) ? lineDetail : "N/A"), "N/A"),
                Objects.toString((paPluID > 0 ? paPluID : "N/A"), "N/A"),
                Objects.toString((printerID > 0 ? printerID : "N/A"), "N/A"),
                Objects.toString((stationID > 0 ? stationID : "N/A"), "N/A"),
                Objects.toString((quantity > 0.0d ? quantity : "N/A"), "N/A"),
                Objects.toString(printsOnExpeditor, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(hideStation) ? hideStation : "N/A"), "N/A"),
                Objects.toString(linkedFromIDs, "N/A"));


    }

    /**
     * <p>Getter for the parentPrintJobDetail field of the {@link QCPrintJobDetail}.</p>
     *
     * @return The parentPrintJobDetail field of the {@link QCPrintJobDetail}.
     */
    public QCPrintJobDetail getParentPrintJobDetail () {
        return parentPrintJobDetail;
    }

    /**
     * <p>Setter for the parentPrintJobDetail field of the {@link QCPrintJobDetail}.</p>
     *
     * @param parentPrintJobDetail The parentPrintJobDetail field of the {@link QCPrintJobDetail}.
     */
    public void setParentPrintJobDetail (QCPrintJobDetail parentPrintJobDetail) {
        this.parentPrintJobDetail = parentPrintJobDetail;
    }

    /**
     * Setter for the QCPrintJobDetail's paPrinterQueueDetailID field.
     *
     * @param paPrinterQueueDetailID PAPrinterQueueDetailID in the QC_PAPrinterQueueDetail table.
     */
    public void setPaPrinterQueueDetailID (int paPrinterQueueDetailID) {
        this.paPrinterQueueDetailID = paPrinterQueueDetailID;
    }

    /**
     * Getter for the QCPrintJobDetail's paPrinterQueueDetailID field.
     *
     * @return PAPrinterQueueDetailID in the QC_PAPrinterQueueDetail table.
     */
    public int getPaPrinterQueueDetailID () {
        return paPrinterQueueDetailID;
    }

    /**
     * Setter for the QCPrintJobDetail's lineDetail field.
     *
     * @param lineDetail {@link String} Base 64 encoded line to be printed on the receipt.
     */
    public void setLineDetail (String lineDetail) {
        this.lineDetail = lineDetail;
    }

    /**
     * Getter for the QCPrintJobDetail's lineDetail field.
     *
     * @return {@link String} Base 64 encoded line to be printed on the receipt.
     */
    public String getLineDetail () {
        return lineDetail;
    }

    /**
     * <p>Setter for the paPluID field of the {@link QCPrintJobDetail}.</p>
     *
     * @param paPluID The paPluID field of the {@link QCPrintJobDetail}.
     */
    public void setPAPluID (int paPluID) {
        this.paPluID = paPluID;
    }

    /**
     * <p>Getter for the paPluID field of the {@link QCPrintJobDetail}.</p>
     *
     * @return The paPluID field of the {@link QCPrintJobDetail}.
     */
    public int getPAPluID () {
        return paPluID;
    }

    /**
     * Setter for the QCPrintJobDetail's printerID field.
     *
     * @param printerID ID of the printer the print job will print to.
     */
    public void setPrinterID (int printerID) {
        this.printerID = printerID;
    }

    /**
     * Getter for the QCPrintJobDetail's printerID field.
     *
     * @return ID of the printer the print job will print to.
     */
    public int getPrinterID () {
        return printerID;
    }

    /**
     * Setter for the QCPrintJobDetail's stationID field.
     *
     * @param stationID ID of the KDS station the print job will display on.
     */
    public void setStationID (int stationID) {
        this.stationID = stationID;
    }

    /**
     * Getter for the QCPrintJobDetail's stationID field.
     *
     * @return Amount of the product in the print job.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * Setter for the QCPrintJobDetail's quantity field.
     *
     * @param quantity Amount of the product in the print job.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * Getter for the QCPrintJobDetail's quantity field.
     *
     * @return ID of the KDS station the print job will display on.
     */
    public int getStationID () {
        return stationID;
    }

    /**
     * Setter for the QCPrintJobDetail's printsOnExpeditor field.
     *
     * @param printsOnExpeditor Whether or not the print job should print on an expeditor printer.
     */
    public void setPrintsOnExpeditor (boolean printsOnExpeditor) {
        this.printsOnExpeditor = printsOnExpeditor;
    }

    /**
     * Getter for the QCPrintJobDetail's printsOnExpeditor field.
     *
     * @return Whether or not the print job should print on an expeditor printer.
     */
    public boolean getPrintsOnExpeditor () {
        return printsOnExpeditor;
    }

    /**
     * Setter for the QCPrintJobDetail's hideStation field.
     *
     * @param hideStation {@link String} Station ID the KDS station to hide the product on.
     */
    public void setHideStation (String hideStation) {
        this.hideStation = hideStation;
    }

    /**
     * Getter for the QCPrintJobDetail's hideStation field.
     *
     * @return {@link String} Station ID the KDS station to hide the product on.
     */
    public String getHideStation () {
        return hideStation;
    }

    public int getKMSStationID() {
        return KMSStationID;
    }

    public void setKMSStationID(int KMSStationID) {
        this.KMSStationID = KMSStationID;
    }

    public boolean isModifier() {
        return isModifier;
    }

    public void setModifier(boolean modifier) {
        isModifier = modifier;
    }

    public int getPaTransLineItemID() {
        return paTransLineItemID;
    }

    public void setPaTransLineItemID(int paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
    }

    public int getKMSOrderStatusID() {
        return KMSOrderStatusID;
    }

    public void setKMSOrderStatusID(int KMSOrderStatusID) {
        this.KMSOrderStatusID = KMSOrderStatusID;
    }

    public String getLinkedFromIDs() {
        return linkedFromIDs;
    }

    public void setLinkedFromIDs(String linkedFromIDs) {
        this.linkedFromIDs = linkedFromIDs;
    }


    public QCPrintJobDetail copy(){
        QCPrintJobDetail copy = new QCPrintJobDetail();
        copy.parentPrintJobDetail=this.parentPrintJobDetail;     
        copy.paPrinterQueueDetailID=this.paPrinterQueueDetailID;   
        copy.paTransLineItemID=this.paTransLineItemID;        
        copy.lineDetail=this.lineDetail;               
        copy.paPluID=this.paPluID;                  
        copy.printerID=this.printerID;                
        copy.stationID=this.stationID;                
        copy.quantity=this.quantity;                 
        copy.printsOnExpeditor=this.printsOnExpeditor;        
        copy.hideStation=this.hideStation;              
        copy.isModifier=this.isModifier;               
        copy.KMSStationID=this.KMSStationID;             
        copy.KMSOrderStatusID=this.KMSOrderStatusID;         
        copy.linkedFromIDs=this.linkedFromIDs;            
        return copy;
    }
}
