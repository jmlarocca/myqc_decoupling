package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2019-11-04 14:56:36 -0500 (Mon, 04 Nov 2019) $: Date of last commit
    $Rev: 43238 $: Revision of last commit
    Notes: An object representing a print job.
*/

import com.google.gson.annotations.SerializedName;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.KitchenDisplaySystemJobInfo;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class to represent a print job.
 *
 */
public class QCPrintJob {

    // log file for hte QCPrintJob
    private static final String QCPJ_LOG = "QCPrintJob.log";

    // variables within the QCPrintJob scope
    private int paPrinterQueueID;
    private int paTransactionID;
    private int terminalID;
    private int orderTypeID;
    private String pickupDeliveryNote;
    private String estimatedOrderTime;
    private String personName;
    private String phone;
    private String transComment;
    @SerializedName("printJobDetails")
    private ArrayList<QCPrintJobDetail> details = new ArrayList<>();
    private String kdsDirectory;
    private boolean useTextMessages;
    private String orderNumber;
    private String transName;
    private String transNameLabel;
    private int transTypeID;
    private String prevOrderNumsForKDS;
    private boolean onlineTrans;

    /**
     * Constructor for a creating a print job to poison the PrinterQueueHandler's print queue.
     *
     */
    public QCPrintJob () {}

    /**
     * Constructor for a QCPrintJob.
     *
     * @param printJobArr {@link ArrayList<HashMap>} Information for this print job stored in an ArrayList.
     * @param kdsDirectoriesByTerminalID {@link HashMap<Long, String>} KDS directories based on each terminal.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public QCPrintJob (ArrayList<HashMap> printJobArr, HashMap<Long, String> kdsDirectoriesByTerminalID) {

        try {
            if ((printJobArr != null) && (!printJobArr.isEmpty())) {
                HashMap printJob = printJobArr.get(0);

                // set fields for the QCPrintJob
                paPrinterQueueID = HashMapDataFns.getIntVal(printJob, "PAPRINTERQUEUEID");
                paTransactionID = HashMapDataFns.getIntVal(printJob, "PATRANSACTIONID");
                terminalID = HashMapDataFns.getIntVal(printJob, "TERMINALID");
                orderTypeID = HashMapDataFns.getIntVal(printJob, "PAORDERTYPEID");
                pickupDeliveryNote = HashMapDataFns.getStringVal(printJob, "PICKUPDELIVERYNOTE");
                estimatedOrderTime = HashMapDataFns.getStringVal(printJob, "ESTIMATEDORDERTIME");
                personName = HashMapDataFns.getStringVal(printJob, "PERSONNAME");
                phone = HashMapDataFns.getStringVal(printJob, "PHONE");
                transComment = HashMapDataFns.getStringVal(printJob, "TRANSCOMMENT");
                useTextMessages = HashMapDataFns.getBooleanVal(printJob, "PAKDSUSESMSNOTIFICATIONS");
                orderNumber = HashMapDataFns.getStringVal(printJob, "ORDERNUM");
                transName = HashMapDataFns.getStringVal(printJob, "TRANSNAME");
                transNameLabel = HashMapDataFns.getStringVal(printJob, "TRANSNAMELABEL");
                transTypeID = HashMapDataFns.getIntVal(printJob, "TRANSTYPEID");
                prevOrderNumsForKDS = HashMapDataFns.getStringVal(printJob, "PREVKDSORDERNUMS");
                onlineTrans = HashMapDataFns.getBooleanVal(printJob, "ONLINETRANS");
                // create the details for this QCPrintJob
                printJobArr.forEach((printJobDetail) -> details.add(new QCPrintJobDetail(printJobDetail)));

                // set any parent print job details
                if (!DataFunctions.isEmptyCollection(details)) {
                    setParentPrintJobDetails(details);
                }

                // set the KDS directory for this QCPrintJob
                if (kdsDirectoriesByTerminalID != null) {
                    setKdsDirectory(kdsDirectoriesByTerminalID.get((long) getTerminalID()));
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, QCPJ_LOG);
            Logger.logMessage("There was a problem trying to construct an instance of a QCPrintJob in the " +
                    "QCPrintJob's constructor", QCPJ_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Iterates through the print job details and sets parent details for modifiers that have a parent product.</p>
     *
     * @param details An {@link ArrayList} of {@link QCPrintJobDetail} corresponding to the print job details within the print job.
     */
    @SuppressWarnings("Convert2streamapi")
    private void setParentPrintJobDetails (ArrayList<QCPrintJobDetail> details) {

        // make sure the details are valid
        if (DataFunctions.isEmptyCollection(details)) {
            Logger.logMessage("The print job details passed to QCPrintJob.setParentPrintJobDetails can't be null or empty!", Logger.LEVEL.ERROR);
            return;
        }

        HashMap<QCPrintJobDetail, Boolean> detailModifierLookup = getDetailModifierLookup(details);
        if (!DataFunctions.isEmptyMap(detailModifierLookup)) {
            for (QCPrintJobDetail qcPrintJobDetail : details) {
                if ((detailModifierLookup.containsKey(qcPrintJobDetail)) && (detailModifierLookup.get(qcPrintJobDetail))) {
                    QCPrintJobDetail parentDetail = getParentDetailFromTransLineItemID(qcPrintJobDetail.getPaTransLineItemID(), details);
                    qcPrintJobDetail.setParentPrintJobDetail(parentDetail);
                }
            }
        }
        else {
            Logger.logMessage("Unable to determine whether or not the details within the print job are modifiers or not, the detailModifierLookup is invalid!", Logger.LEVEL.ERROR);
        }
    }

    /**
     * <p>Iterates through the print job details and determines whether or not the print job detail is a modifier with a parent product.</p>
     *
     * @param details An {@link ArrayList} of {@link QCPrintJobDetail} corresponding to the print job details within the print job.
     * @return A {@link HashMap} whose {@link QCPrintJobDetail} key is for a print job detail and whose {@link Boolean} value is whether or
     * not the print job detail is a modifier with a parent product.
     */
    private HashMap<QCPrintJobDetail, Boolean> getDetailModifierLookup (ArrayList<QCPrintJobDetail> details) {

        // make sure the details are valid
        if (DataFunctions.isEmptyCollection(details)) {
            Logger.logMessage("The print job details passed to QCPrintJob.getDetailModifierLookup can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        HashMap<QCPrintJobDetail, Boolean> detailModifierLookup = new HashMap<>();
        for (QCPrintJobDetail qcPrintJobDetail : details) {
            if (qcPrintJobDetail.isModifier()) {
                // if the trans line item ID for the modifier is unique then it doesn't have a parent
                if (getDetailsTransLineItemIDCount(qcPrintJobDetail.getPaTransLineItemID(), details) > 1) {
                    detailModifierLookup.put(qcPrintJobDetail, true);
                }
                else {
                    detailModifierLookup.put(qcPrintJobDetail, false);
                }
            }
            else {
                // the detail isn't a modifier
                detailModifierLookup.put(qcPrintJobDetail, false);
            }
        }

        return detailModifierLookup;
    }

    /**
     * <p>Counts the number of print job details that have the same trans line item ID.</p>
     *
     * @param paTransLineItemID The trans line item ID to check.
     * @param details An {@link ArrayList} of {@link QCPrintJobDetail} corresponding to the print job details within the print job.
     * @return The number of print job details that have the same trans line item ID.
     */
    private int getDetailsTransLineItemIDCount (int paTransLineItemID, ArrayList<QCPrintJobDetail> details) {

        // make sure the trans line item ID is valid
        if (paTransLineItemID <= 0) {
            Logger.logMessage("The trans line item ID passed to QCPrintJob.getDetailsTransLineItemIDCount must be greater than 0!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure the details are valid
        if (DataFunctions.isEmptyCollection(details)) {
            Logger.logMessage("The print job details passed to QCPrintJob.getDetailsTransLineItemIDCount can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        int count = 0;
        for (QCPrintJobDetail qcPrintJobDetail : details) {
            if (qcPrintJobDetail.getPaTransLineItemID() == paTransLineItemID) {
                count++;
            }
        }

        return count;
    }

    /**
     * <p>Finds the parent print job detail with the given trans line item ID.</p>
     *
     * @param paTransLineItemID The trans line item ID of the parent detail.
     * @param details An {@link ArrayList} of {@link QCPrintJobDetail} corresponding to the print job details within the print job.
     * @return The parent {@link QCPrintJobDetail} with the given trans line item ID.
     */
    private QCPrintJobDetail getParentDetailFromTransLineItemID (int paTransLineItemID, ArrayList<QCPrintJobDetail> details) {

        // make sure the trans line item ID is valid
        if (paTransLineItemID <= 0) {
            Logger.logMessage("The trans line item ID passed to QCPrintJob.getDetailsTransLineItemIDCount must be greater than 0!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the details are valid
        if (DataFunctions.isEmptyCollection(details)) {
            Logger.logMessage("The print job details passed to QCPrintJob.getDetailsTransLineItemIDCount can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        QCPrintJobDetail parent = null;
        for (QCPrintJobDetail qcPrintJobDetail : details) {
            if ((!qcPrintJobDetail.isModifier()) && (qcPrintJobDetail.getPaTransLineItemID() == paTransLineItemID)) {
                parent = qcPrintJobDetail;
                break;
            }
        }

        return parent;
    }

    /**
     * Overridden toString method for a QCPrintJob.
     *
     * @return {@link String} String representation of a QCPrintJob.
     */
    @SuppressWarnings("SpellCheckingInspection")
    @Override
    public String toString () {

        String printQueueDetailsStr = "";
        if (!DataFunctions.isEmptyCollection(details)) {
            printQueueDetailsStr += "[";
            for (QCPrintJobDetail qcPrintJobDetail : details) {
                if (qcPrintJobDetail.equals(details.get(details.size() - 1))) {
                    // the last detail no comma needed
                    printQueueDetailsStr += "[" + qcPrintJobDetail.toString() + "]";
                }
                else {
                    printQueueDetailsStr += "[" + qcPrintJobDetail.toString() + "], ";
                }
            }
            printQueueDetailsStr += "]";
        }

        return String.format("PAPRINTERQUEUEID: %s, PATRANSACTIONID: %s, TERMINALID: %s, ORDERTYPEID: %s, " +
                "PICKUPDELIVERYNOTE: %s, PERSONNAME: %s, PHONE: %s, TRANSCOMMENT: %s, KDSDIRECTORY: %s, USETEXTMESSAGES: %s, " +
                "ORDERNUMBER: %s, TRANSNAME: %s, TRANSNAMELABEL: %s, TRANSTYPEID: %s, PREVORDERNUMSFORKDS: %s, PRINTQUEUEDETAILS: %s",
                Objects.toString(paPrinterQueueID, "NULL"),
                Objects.toString(paTransactionID, "NULL"),
                Objects.toString(terminalID, "NULL"),
                Objects.toString(orderTypeID, "NULL"),
                Objects.toString(pickupDeliveryNote, "NULL"),
                Objects.toString(personName, "NULL"),
                Objects.toString(phone, "NULL"),
                Objects.toString(transComment, "NULL"),
                Objects.toString(kdsDirectory, "NULL"),
                Objects.toString(useTextMessages, "NULL"),
                Objects.toString(orderNumber, "NULL"),
                Objects.toString(transName, "NULL"),
                Objects.toString(transNameLabel, "NULL"),
                Objects.toString(transTypeID, "NULL"),
                Objects.toString(prevOrderNumsForKDS, "NULL"),
                Objects.toString(printQueueDetailsStr, "NULL"));
    }

    /**
     * Create the job tracker ID for this print job by concatenating the paPrinterQueueID, printerID, and terminalID
     * fields of this QCPrintJob.
     *
     * @return {@link String} The jobTrackerID.
     */
    public String getJobTrackerID () {
        return "KP_"+Objects.toString(paPrinterQueueID, "NULL")+"_" +
                Objects.toString(details.get(0).getPrinterID(), "NULL")+"_"+Objects.toString(terminalID, "NULL");
    }

    /**
     * Create the job tracker ID for this print job by concatenating the paPrinterQueueID, printerID, and terminalID
     * fields of this QCPrintJob.
     *
     * @param paPrinterQueueID PAPrinterQueueID in the QC_PAPrinterQueue table.
     * @param printerID ID of the printer that will execute the print job.
     * @param terminalID ID of the terminal the transaction took place on.
     * @return {@link String} The jobTrackerID.
     */
    public static String getJobTrackerID (int paPrinterQueueID, int printerID, int terminalID) {
        return "KP_"+Objects.toString(paPrinterQueueID, "NULL")+"_" +Objects.toString(printerID, "NULL")+"_" +
                Objects.toString(terminalID, "NULL");
    }

    /**
     * Getter for the QCPrintJob's QCPJ_LOG field.
     *
     * @return {@link String} The log file for the QCPrintJob.
     */
    public static String getLogFile () {
        return QCPJ_LOG;
    }

    /**
     * <p>Gets all the printers that print queue details within the print job will print on.</p>
     *
     * @return A {@link HashMap} whose key is a printer ID {@link Integer} and whose value is a {@link Boolean} whether or not the printer is KDS.
     */
    @SuppressWarnings("Convert2streamapi")
    public HashMap<Integer, Boolean> getPrintersInPrintJob () {
        HashMap<Integer, Boolean> printers = new HashMap<>();

        if (DataFunctions.isEmptyCollection(details)) {
            Logger.logMessage(String.format("No print queue details found for the print job with a job tracker ID of %s in QCPrintJob.getPrintersInPrintJob",
                    Objects.toString(getJobTrackerID(), "N/A")), getLogFile(), Logger.LEVEL.ERROR);
            return null;
        }

        for (QCPrintJobDetail qcPrintJobDetail : details) {
            if (qcPrintJobDetail != null) {
                boolean isKDS = (qcPrintJobDetail.getStationID() > 0);
                printers.put(qcPrintJobDetail.getPrinterID(), isKDS);
            }
        }

        return printers;
    }

    /**
     * <p>Gets the IDs of physical kitchen printers that will print print queue details within the print job.</p>
     *
     * @return An {@link ArrayList} of {@link Integer} corresponding to the IDs of physical kitchen printers that will print print queue details within the print job.
     */
    @SuppressWarnings("Convert2streamapi")
    public ArrayList<Integer> getKPPrintersInPrintJob () {
        ArrayList<Integer> kpPrinters = new ArrayList<>();

        HashMap<Integer, Boolean> allPrintersInPrintJob = getPrintersInPrintJob();
        if (!DataFunctions.isEmptyMap(allPrintersInPrintJob)) {
            for (Map.Entry<Integer, Boolean> printJobPrinterEntry : allPrintersInPrintJob.entrySet()) {
                if (!printJobPrinterEntry.getValue()) {
                    kpPrinters.add(printJobPrinterEntry.getKey());
                }
            }
        }

        return kpPrinters;
    }

    /**
     * <p>Gets the IDs of KDS stations that will display print queue details within the print job.</p>
     *
     * @return An {@link ArrayList} of {@link Integer} corresponding to the IDs of KDS stations that will display print queue details within the print job.
     */
    @SuppressWarnings("Convert2streamapi")
    public ArrayList<Integer> getKDSPrintersInPrintJob () {
        ArrayList<Integer> kdsStations = new ArrayList<>();

        HashMap<Integer, Boolean> allPrintersInPrintJob = getPrintersInPrintJob();
        if (!DataFunctions.isEmptyMap(allPrintersInPrintJob)) {
            for (Map.Entry<Integer, Boolean> printJobPrinterEntry : allPrintersInPrintJob.entrySet()) {
                if (printJobPrinterEntry.getValue()) {
                    kdsStations.add(printJobPrinterEntry.getKey());
                }
            }
        }

        return kdsStations;
    }

    /**
     * Setter for the QCPrintJob's paPrinterQueueID field.
     *
     * @param paPrinterQueueID PAPrinterQueueID in the QC_PAPrinterQueue table.
     */
    public void setPaPrinterQueueID (int paPrinterQueueID) {
        this.paPrinterQueueID = paPrinterQueueID;
    }

    /**
     * Getter for the QCPrintJob's paPrinterQueueID field.
     *
     * @return PAPrinterQueueID in the QC_PAPrinterQueue table.
     */
    public int getPaPrinterQueueID () {
        return paPrinterQueueID;
    }

    /**
     * Setter for the QCPrintJob's paTransactionID field.
     *
     * @param paTransactionID ID of the transaction for this print job.
     */
    public void setPaTransactionID (int paTransactionID) {
        this.paTransactionID = paTransactionID;
    }

    /**
     * Getter for the QCPrintJob's paTransactionID field.
     *
     * @return ID of the transaction for this print job.
     */
    public int getPaTransactionID () {
        return paTransactionID;
    }

    /**
     * Setter for the QCPrintJob's terminalID field.
     *
     * @param terminalID ID of the terminal the transaction took place on.
     */
    public void setTerminalID (int terminalID) {
        this.terminalID = terminalID;
    }

    /**
     * Getter for the QCPrintJob's terminalID field.
     *
     * @return ID of the terminal the transaction took place on.
     */
    public int getTerminalID () {
        return terminalID;
    }

    /**
     * Setter for the QCPrintJob's orderTypeID field.
     *
     * @param orderTypeID Type of order that was placed.
     */
    public void setOrderTypeID (int orderTypeID) {
        this.orderTypeID = orderTypeID;
    }

    /**
     * Getter for the QCPrintJob's orderTypeID field.
     *
     * @return Type of order that was placed.
     */
    public int getOrderTypeID () {
        return orderTypeID;
    }

    /**
     * Setter for the QCPrintJob's pickupDeliveryNote field.
     *
     * @param pickupDeliveryNote {@link String} Note related to pickup or delivery of the order.
     */
    public void setPickupDeliveryNote (String pickupDeliveryNote) {
        this.pickupDeliveryNote = pickupDeliveryNote;
    }

    /**
     * Getter for the QCPrintJob's pickupDeliveryNote field.
     *
     * @return {@link String} Note related to pickup or delivery of the order.
     */
    public String getPickupDeliveryNote () {
        return pickupDeliveryNote;
    }

    /**
     * Setter for the QCPrintJob's personName field.
     *
     * @param personName {@link String} Name of the person that made the order.
     */
    public void setPersonName (String personName) {
        this.personName = personName;
    }

    /**
     * Getter for the QCPrintJob's personName field.
     *
     * @return {@link String} Name of the person that made the order.
     */
    public String getPersonName () {
        return personName;
    }

    /**
     * Setter for the QCPrintJob's phone field.
     *
     * @param phone {@link String} Relevant phone number for the transaction.
     */
    public void setPhone (String phone) {
        this.phone = phone;
    }

    /**
     * Getter for the QCPrintJob's phone field.
     *
     * @return {@link String} Relevant phone number for the transaction.
     */
    public String getPhone () {
        return phone;
    }

    /**
     * Setter for the QCPrintJob's transComment field.
     *
     * @param transComment {@link String} Comment related to the transaction.
     */
    public void setTransComment (String transComment) {
        this.transComment = transComment;
    }

    /**
     * Getter for the QCPrintJob's transComment field.
     *
     * @return {@link String} Comment related to the transaction.
     */
    public String getTransComment () {
        return transComment;
    }

    /**
     * Setter for the QCPrintJob's details field.
     *
     * @param details {@link ArrayList<QCPrintJobDetail>} Details for this print job.
     */
    public void setPrintJobDetails (ArrayList<QCPrintJobDetail> details) {
        this.details = details;
    }

    /**
     * Getter for the QCPrintJob's details field.
     *
     * @return {@link ArrayList<QCPrintJobDetail>} Details for this print job.
     */
    public ArrayList<QCPrintJobDetail> getPrintJobDetails () {
        return details;
    }

    /**
     * Setter for the QCPrintJob's kdsDirectory field.
     *
     * @param kdsDirectory {@link String} Directory where KDS print jobs should be placed.
     */
    public void setKdsDirectory (String kdsDirectory) {
        this.kdsDirectory = kdsDirectory;
    }

    /**
     * Getter for the QCPrintJob's kdsDirectory field.
     *
     * @return {@link String} Directory where KDS print jobs should be placed.
     */
    public String getKdsDirectory () {
        return kdsDirectory;
    }

    /**
     * Setter for the QCPrintJob's useTextMessages field.
     *
     * @param useTextMessages Whether or not the status of the order needs to be texted the person who placed the order.
     */
    public void setUseTextMessages (boolean useTextMessages) {
        this.useTextMessages = useTextMessages;
    }

    /**
     * Getter for the QCPrintJob's useTextMessages field.
     *
     * @return Whether or not the status of the order needs to be texted the person who placed the order.
     */
    public boolean getUseTextMessages () {
        return useTextMessages;
    }

    /**
     * Setter for the QCPrintJob's orderNumber field.
     *
     * @param orderNumber {@link String} Number of the order that was placed.
     */
    public void setOrderNumber (String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * Getter for the QCPrintJob's orderNumber field.
     *
     * @return {@link String} Number of the order that was placed.
     */
    public String getOrderNumber () {
        return orderNumber;
    }

    /**
     * Setter for the QCPrintJob's transName field.
     *
     * @param transName {@link String} Name of the transaction that took place.
     */
    public void setTransName (String transName) {
        this.transName = transName;
    }

    /**
     * Getter for the QCPrintJob's transName field.
     *
     * @return {@link String} Name of the transaction that took place.
     */
    public String getTransName () {
        return transName;
    }

    /**
     * Setter for the QCPrintJob's transNameLabel field.
     *
     * @param transNameLabel {@link String} Label for the name of the transaction that took place.
     */
    public void setTransNameLabel (String transNameLabel) {
        this.transNameLabel = transNameLabel;
    }

    /**
     * Getter for the QCPrintJob's transNameLabel field.
     *
     * @return {@link String} Label for the name of the transaction that took place.
     */
    public String getTransNameLabel () {
        return transNameLabel;
    }

    /**
     * Setter for the QCPrintJob's transTypeID field.
     *
     * @param transTypeID Type of transaction that took place.
     */
    public void setTransTypeID (int transTypeID) {
        this.transTypeID = transTypeID;
    }

    /**
     * Getter for the QCPrintJob's transTypeID field.
     *
     * @return Type of transaction that took place.
     */
    public int getTransTypeID () {
        return transTypeID;
    }

    /**
     * Setter for the QCPrintJob's prevOrderNumsForKDS field.
     *
     * @param prevOrderNumsForKDS {@link String} Previous order numbers of an open transaction to be displayed on KDS.
     */
    public void setPrevOrderNumsForKDS (String prevOrderNumsForKDS) {
        this.prevOrderNumsForKDS = prevOrderNumsForKDS;
    }

    /**
     * Getter for the QCPrintJob's prevOrderNumsForKDS field.
     *
     * @return {@link String} Previous order numbers of an open transaction to be displayed on KDS.
     */
    public String getPrevOrderNumsForKDS () {
        return prevOrderNumsForKDS;
    }

    public boolean isOnlineTrans() {
        return onlineTrans;
    }

    public void setOnlineTrans(boolean onlineTrans) {
        this.onlineTrans = onlineTrans;
    }

    /**
     * <p>Gets a deep copy of all the print job details in this print job.</p>
     *
     * @return An {@link ArrayList} of {@link QCPrintJobDetail} containing a deep copy of all the print job details in this {@link QCPrintJob}.
     */
    public ArrayList<QCPrintJobDetail> getCopyOfDetails () {
        ArrayList<QCPrintJobDetail> detailsCopy = new ArrayList<>();

        if (!DataFunctions.isEmptyCollection(details)) {
            detailsCopy.addAll(details.stream().map(QCPrintJobDetail::copy).collect(Collectors.toList()));
        }

        return detailsCopy;
    }

}

