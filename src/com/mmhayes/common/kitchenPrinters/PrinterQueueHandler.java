package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2019-12-13 16:21:17 -0500 (Fri, 13 Dec 2019) $: Date of last commit
    $Rev: 44598 $: Revision of last commit
    Notes: Maintains jobs to be printed in a queue and prints them.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kitchenPrinters.KDS.KDSUtil;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.kms.KMSChangeUpdater;
import com.mmhayes.common.kms.KMSEventLogQueueHandler;
import com.mmhayes.common.kms.KMSManager;
import com.mmhayes.common.kms.KMSOrderStatusUpdateQueueHandler;
import com.mmhayes.common.kms.receiptGen.KitchenReceiptType;
import com.mmhayes.common.printing.PrintStatus;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.xmlapi.XmlRpcUtil;
import org.apache.commons.lang.math.NumberUtils;
import redstone.xmlrpc.XmlRpcArray;
import redstone.xmlrpc.XmlRpcStruct;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Singleton class responsible for printing print jobs that are in it's queue.
 *
 */
public class PrinterQueueHandler implements Runnable {

    // the only instance of PrinterQueueHandler for this WAR
    private static volatile PrinterQueueHandler printerQueueHandlerInstance = null;

    // log file specific to the PrinterQueueHandler
    private static final String PQH_LOG = "KP_PrinterQueueHandler.log";

    // variables within the PrinterQueueHandler scope
    private volatile boolean isShuttingDown = false;
    private volatile boolean isTerminated = false;
    private static final String POISONOUS_PRINT_JOB_PERSON_NAME = "POISONOUS_PERSON";
    private static final int PRINT_QUEUE_CAPACITY = 1024;
    private ArrayBlockingQueue<QCPrintJob> printQueue = new ArrayBlockingQueue<>(PRINT_QUEUE_CAPACITY);
    private ConcurrentHashMap<Integer, QCKitchenPrinter> printers = new ConcurrentHashMap<>();
    private PrintJobStatusHandler printJobStatusHandler = null;

    // variables needed for PrinterQueueHandler instantiation
    private ArrayList<HashMap> printerList;
    private KDSUtil kdsUtil;

    //variables for KMS
    private static boolean usingKMS = false;

    /**
     * Private constructor for a PrinterQueueHandler.
     *
     * @param printerList {@link ArrayList<HashMap>} ArrayList of printers to be used by the PrinterQueueHandler to
     *         execute print jobs in it's queue.
     */
    private PrinterQueueHandler (ArrayList<HashMap> printerList) throws Exception {
        this.printerList = printerList;
        this.printJobStatusHandler = PrintJobStatusHandler.getInstance();
        this.kdsUtil = KDSUtil.init();
    }

    /**
     * Get the only instance of PrinterQueueHandler for this WAR.
     *
     * @return {@link PrinterQueueHandler} The PrinterQueueHandler instance for this WAR.
     */
    public static PrinterQueueHandler getInstance () {

        try {
            if (printerQueueHandlerInstance == null) {
                throw new RuntimeException("The PrinterQueueHandler must be instantiated before trying to obtain its " +
                        "instance.");
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to get the only instance of PrinterQueueHandler in " +
                    "PrinterQueueHandler.getInstance", PQH_LOG, Logger.LEVEL.ERROR);
        }

        return printerQueueHandlerInstance;
    }

    /**
     * Instantiate and return the only instance of PrinterQueueHandler for this WAR.
     *
     * @param printerList {@link ArrayList<HashMap>} ArrayList of printers to be used by the PrinterQueueHandler to
     *         execute print jobs in it's queue.
     * @return {@link PrinterQueueHandler} The PrinterQueueHandler instance for this WAR.
     */
    public static synchronized PrinterQueueHandler init (ArrayList<HashMap> printerList) {

        try {
            if (printerQueueHandlerInstance != null) {
                throw new RuntimeException("There PrinterQueueHandler has already been instantiated and may not be " +
                        "instantiated again.");
            }
            else {
                // create the instance
                printerQueueHandlerInstance = new PrinterQueueHandler(printerList);
                usingKMS = Boolean.valueOf(new DataManager().getSingleField("data.kms.GetIsUsingKMS", new Object[]{PeripheralsDataManager.getTerminalMacAddress()}).toString());
                Logger.logMessage("The instance of PrinterQueueHandler has been instantiated", PQH_LOG, Logger.LEVEL.TRACE);

                if(usingKMS){
                    KMSChangeUpdater.init();
                    KMSOrderStatusUpdateQueueHandler.init();
                    KMSEventLogQueueHandler.init(null);//This will open up the KMSManager
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to instantiate and return the only instance of " +
                    "PrinterQueueHandler in PrinterQueueHandler.init", PQH_LOG, Logger.LEVEL.ERROR);
        }

        return printerQueueHandlerInstance;
    }

    /**
     * Method that will be called by a Thread created using the PrinterQueueHandler Runnable.
     *
     */
    @Override
    public void run () {

        try {
            Thread.currentThread().setName("KitchenPrinter-PrinterQueueHandler");

            // empty the print queue of any possible print jobs that are still lingering in the print queue
            emptyPrintQueue();
            // remove any printers that might be lingering in the printers Map
            printers.clear();
            // populate the printers map
            createMapOfPrinters();
            // try to establish a connection to all the printers
            openConnectionToPrinters();

            while ((!isShuttingDown) && (!Thread.currentThread().isInterrupted())) {
                try {
                    Logger.logMessage("About to take print job from print queue", PQH_LOG, Logger.LEVEL.DEBUG);
                    QCPrintJob qcPrintJob = printQueue.take();
                    Logger.logMessage("took from print Queue - printQueueID:" + qcPrintJob.getPaPrinterQueueID(), PQH_LOG, Logger.LEVEL.TRACE);
                    // check if the print queue was poisoned (i.e. should stop blocking) based on whether or not the person
                    // name for the print job was set to the poisonous name
                    if (qcPrintJob.getPersonName().equalsIgnoreCase(POISONOUS_PRINT_JOB_PERSON_NAME)) {
                        Logger.logMessage("The PrinterQueueHandler's print queue has been poisoned and the Thread created " +
                                "using the PrinterQueueHandler Runnable will be terminated", PQH_LOG, Logger.LEVEL.TRACE);
                        break;
                    }
                    else {
                        // sort the print job details by receipt type (KP/KDS/KMS)
                        HashMap<String, ArrayList<QCPrintJobDetail>> sortedPrintJobDetails = sortPrintJobDetailsByType(qcPrintJob.getPrintJobDetails());

                        if (DataFunctions.isEmptyMap(sortedPrintJobDetails)) {
                            throw new Exception(String.format("A problem occurred while trying to sort the print job details for " +
                                    "the print job with a job tracker ID of %s by their receipt types in PrinterQueueHandler.run!",
                                    Objects.toString(qcPrintJob.getJobTrackerID(), "N/A")));
                        }

                        ArrayList<QCPrintJobDetail> kpDetails;
                        kpDetails = (sortedPrintJobDetails.containsKey(KitchenReceiptType.KP) ? sortedPrintJobDetails.get(KitchenReceiptType.KP) : new ArrayList<>());
                        ArrayList<QCPrintJobDetail> kdsDetails;
                        kdsDetails = (sortedPrintJobDetails.containsKey(KitchenReceiptType.KDS) ? sortedPrintJobDetails.get(KitchenReceiptType.KDS) : new ArrayList<>());
                        ArrayList<QCPrintJobDetail> kmsDetails;
                        kmsDetails = (sortedPrintJobDetails.containsKey(KitchenReceiptType.KMS) ? sortedPrintJobDetails.get(KitchenReceiptType.KMS) : new ArrayList<>());

                        if (!DataFunctions.isEmptyCollection(kpDetails)) {
                            // send the print job details to KP
                            qcPrintJob.setPrintJobDetails(kpDetails);
                            QCKitchenPrinter qcKitchenPrinter = getPrinter(kpDetails.get(0).getPrinterID());
                            if (qcKitchenPrinter != null) {
                                Logger.logMessage(String.format("Now sending the print queue detail IDs %s to KP.",
                                        Objects.toString(StringFunctions.buildStringFromListOfObjProp(kpDetails, QCPrintJobDetail.class, "getPaPrinterQueueDetailID", null), "N/A")), PQH_LOG, Logger.LEVEL.TRACE);
                                doPhysicalPrint(qcPrintJob, qcKitchenPrinter);
                            }
                        }

                        if (!DataFunctions.isEmptyCollection(kdsDetails)) {
                            // send the print job details to KDS
                            qcPrintJob.setPrintJobDetails(kdsDetails);
                            Logger.logMessage(String.format("Now sending the print queue detail IDs %s to KDS.",
                                    Objects.toString(StringFunctions.buildStringFromListOfObjProp(kdsDetails, QCPrintJobDetail.class, "getPaPrinterQueueDetailID", null), "N/A")), PQH_LOG, Logger.LEVEL.TRACE);
                            doKDSPrint(qcPrintJob);
                        }

                        if (!DataFunctions.isEmptyCollection(kmsDetails)) {
                            // send the print job details to KMS
                            qcPrintJob.setPrintJobDetails(kmsDetails);
                            Logger.logMessage(String.format("PrintQueueHandler.run Now sending the print queue detail IDs %s to KMS.",
                                    Objects.toString(StringFunctions.buildStringFromListOfObjProp(kmsDetails, QCPrintJobDetail.class, "getPaPrinterQueueDetailID", null), "N/A")), "KMS.log", Logger.LEVEL.TRACE);
                            sendToKMS(qcPrintJob);
                        }
                    }
                }
                catch (Exception e) {
                    Logger.logException(e, PQH_LOG);
                    Logger.logMessage("There was a problem in the while loop of PrinterQueueHandler.run", PQH_LOG, Logger.LEVEL.ERROR);
                }
                Thread.sleep(100L);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem in PrinterQueueHandler.run", PQH_LOG, Logger.LEVEL.ERROR);
        }
        finally {
            isTerminated = true;
            Logger.logMessage("KitchenPrinter-PrinterQueueHandler is exiting", PQH_LOG, Logger.LEVEL.TRACE);
        }

    }

    /**
     * In case there are any events that are in the print queue. Remove them from the print queue and give them an
     * errored print job status.
     *
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private void emptyPrintQueue () {

        try {

            Logger.logMessage("Emptying print queue", PQH_LOG, Logger.LEVEL.DEBUG);

            while (!printQueue.isEmpty()) {
                QCPrintJob qcPrintJob = printQueue.take();

                if ((qcPrintJob != null) &&
                        (qcPrintJob.getPrintJobDetails() != null && qcPrintJob.getPrintJobDetails().size() > 0) &&
                        (StringFunctions.stringHasContent(qcPrintJob.getJobTrackerID()))) {
                    // get the IDs of printers within the print job
                    ArrayList<Integer> printersInPrintJob = new ArrayList<>();
                    ArrayList<Integer> kpPrinters = qcPrintJob.getKPPrintersInPrintJob();
                    if (!DataFunctions.isEmptyCollection(kpPrinters)) {
                        printersInPrintJob.addAll(kpPrinters);
                    }
                    ArrayList<Integer> kdsStations = qcPrintJob.getKDSPrintersInPrintJob();
                    if (!DataFunctions.isEmptyCollection(kdsStations)) {
                        printersInPrintJob.addAll(kdsStations);
                    }
                    // update the print status for all print queue details in the print job as errored
                    if (!DataFunctions.isEmptyCollection(printersInPrintJob)) {
                        PrintStatus printStatus = new PrintStatus()
                                .addPrintStatusType(PrintStatusType.ERROR)
                                .addUpdatedDTM(LocalDateTime.now());
                        for (int printerID : printersInPrintJob) {
                            printJobStatusHandler.updatePrintStatusMapForPrinter(printerID, printStatus, qcPrintJob);
                        }
                    }
                }
            }
            Logger.logMessage("Print queue emptied", PQH_LOG, Logger.LEVEL.DEBUG);
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to remove any lingering print jobs from the " +
                    "PrinterQueueHandler's print queue in PrinterQueueHandler.emptyPrintQueue", PQH_LOG,
                    Logger.LEVEL.ERROR);
        }

    }

    /**
     * Convert the ArrayList<HashMap> of printer information form the QC_Printer table into the
     * ConcurrentHashMap<Integer, QCKitchenPrinter> of kitchen printers used by the PrinterQueueHandler to print any
     * print jobs in the print queue.
     *
     */
    private void createMapOfPrinters () {

        try {
            if ((printerList != null) && (!printerList.isEmpty())) {
                printerList.forEach((printerHM) -> {
                    if (printerHM.containsKey("PRINTERID")) {
                        printers.put(Integer.parseInt(printerHM.get("PRINTERID").toString()),
                                new QCKitchenPrinter(printerHM));
                    }
                });
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to populate the PrinterQueueHandler's map of printers in " +
                    "PrinterQueueHandler.createMapOfPrinters", PQH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Adds the given QCKitchenPrinter to the PrinterQueueHandler's map of printers.
     *
     * @param qcKitchenPrinter {@link QCKitchenPrinter} The printer we want to add to the map of printers.
     */
    public void addPrinterToMap (QCKitchenPrinter qcKitchenPrinter) {

        try {
            printers.put(Integer.parseInt(qcKitchenPrinter.getPrinterID()), qcKitchenPrinter);
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to add the printer " +
                    Objects.toString(qcKitchenPrinter.getLogicalName(), "NULL")+" to the PrinterQueueHandler's map of " +
                    "printers in PrinterQueueHandler.addPrinterToMap", PQH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Removes the entry from the printers.
     *
     * @param printerID ID of the printer to remove from the printers.
     */
    public void removePrinterFromMap (int printerID) {

        try {
            if ((!DataFunctions.isEmptyMap(printers)) && (printers.containsKey(printerID))) {
                // close connection to the printer
                if (printers.get(printerID).closePrinter()) {
                    printers.remove(printerID);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage(String.format("There was a problem trying to remove the printer with an ID of %s from to the " +
                    "PrinterQueueHandler's map of printers in PrinterQueueHandler.removePrinterFromMap",
                    Objects.toString(printerID, "NULL")), PQH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Try to establish a connection to all the printers in the PrinterQueueHandler's printer map.
     *
     */
    public void openConnectionToPrinters () {
        Logger.logMessage("Connecting to open printers", PQH_LOG, Logger.LEVEL.DEBUG);
        try {
            long delay = getDelayForKPStartup();
            Logger.logMessage(String.format("kitchen printer startup delay set to %s seconds", Objects.toString((delay / 1000L), "NULL")), PQH_LOG, Logger.LEVEL.DEBUG);
            Thread.sleep(delay);

            if ((printers != null) && (!printers.isEmpty())) {
                printers.values().forEach(QCKitchenPrinter::openPrinter);
            }
            if (!DataFunctions.isEmptyMap(printers)) {
                String printersString = StringFunctions.csvListCreateStringListFromJavaList(printers.values().stream().map(QCKitchenPrinter::getName).collect(Collectors.toList()));
                Logger.logMessage(String.format("The machine with a hostname of %s should have connections to the following printers %s",
                        Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                        Objects.toString(printersString, "NULL")), PQH_LOG, Logger.LEVEL.DEBUG);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to establish connections to all the printers in the " +
                    "PrinterQueueHandler's map of printers in PrinterQueueHandler.openConnectionToPrinters", PQH_LOG,
                    Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Updates the printers on this terminal so that they coincide with the backoffice.</p>
     *
     * @param printerStructs The {@link XmlRpcArray} of {@link XmlRpcStruct} containing the {@link QCKitchenPrinter} currently configured in the backoffice.
     * @return Whether or not the printers could be updated properly.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "Convert2streamapi", "OverlyComplexMethod"})
    public boolean updatePrinters (XmlRpcArray printerStructs) {
        boolean success = false;

        try {
            ArrayList<QCKitchenPrinter> prevPrinters = new ArrayList<>();
            if (!DataFunctions.isEmptyMap(printers)) {
                prevPrinters = new ArrayList<>(printers.values());
            }
            ArrayList<QCKitchenPrinter> currPrinters = new ArrayList<>();
            if (!DataFunctions.isEmptyCollection(convertPrinterXmlRpcStructsToPrinters(printerStructs))) {
                currPrinters = convertPrinterXmlRpcStructsToPrinters(printerStructs);
            }

            if ((!DataFunctions.isEmptyCollection(prevPrinters)) && (!DataFunctions.isEmptyCollection(currPrinters))) {
                // determine which printers have been added
                ArrayList<QCKitchenPrinter> addedPrinters = new ArrayList<>();
                for (QCKitchenPrinter currPrinter : currPrinters) {
                    if (!prevPrinters.contains(currPrinter)) {
                        addedPrinters.add(currPrinter);
                    }
                }
                if (!DataFunctions.isEmptyCollection(addedPrinters)) {
                    // add the printers
                    for (QCKitchenPrinter printer : addedPrinters) {
                        printers.put(Integer.parseInt(printer.getPrinterID()), printer);
                    }
                }

                // determine which printers have been removed
                ArrayList<QCKitchenPrinter> removedPrinters = new ArrayList<>();
                for (QCKitchenPrinter prevPrinter : prevPrinters) {
                    if (!currPrinters.contains(prevPrinter)) {
                        removedPrinters.add(prevPrinter);
                    }
                }
                if (!DataFunctions.isEmptyCollection(removedPrinters)) {
                    // remove the printers
                    for (QCKitchenPrinter printer : removedPrinters) {
                        printers.remove(Integer.parseInt(printer.getPrinterID()));
                    }
                }
            }
            else if ((DataFunctions.isEmptyCollection(prevPrinters)) && (!DataFunctions.isEmptyCollection(currPrinters))) {
                // all printers have been added
                for (QCKitchenPrinter printer : currPrinters) {
                    printers.put(Integer.parseInt(printer.getPrinterID()), printer);
                }
            }
            else if ((!DataFunctions.isEmptyCollection(prevPrinters)) && (DataFunctions.isEmptyCollection(currPrinters))) {
                // all printers have been removed
                printers = new ConcurrentHashMap<>();
            }

            success = true;
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to update the printers in PrinterQueueHandler.updatePrinters",
                    PQH_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Utility method to convert a {@link XmlRpcArray} of {@link XmlRpcStruct} into an {@link ArrayList} of {@link QCKitchenPrinter}.</p>
     *
     * @param printerStructs The {@link XmlRpcArray} of {@link XmlRpcStruct} to convert.
     * @return The {@link XmlRpcArray} of {@link XmlRpcStruct} as an {@link ArrayList} of {@link QCKitchenPrinter}.
     */
    private ArrayList<QCKitchenPrinter> convertPrinterXmlRpcStructsToPrinters (XmlRpcArray printerStructs) {
        ArrayList<QCKitchenPrinter> printers = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(printerStructs)) {
                for (int i = 0; i < printerStructs.size(); i++) {
                    XmlRpcStruct printerStruct = printerStructs.getStruct(i);
                    if (!DataFunctions.isEmptyMap(printerStruct)) {
                        printers.add(new QCKitchenPrinter(printerStruct));
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to convert the XmlRpcArray of QCKitchenPrinter XmlRpcStructs " +
                    "into an ArrayList of QCKitchenPrinters in PrinterQueueHandler.convertPrinterXmlRpcStructsToPrinters",
                    PQH_LOG, Logger.LEVEL.ERROR);
        }

        return printers;
    }

    /**
     * <p>Gets the number of milliseconds the kitchen printers should wait before starting up.</p>
     *
     * @return The number of milliseconds the kitchen printers should wait before starting up.
     */
    private long getDelayForKPStartup () {
        // default delay of 3 seconds
        long delay = 3000L;

        try {
            if ((StringFunctions.stringHasContent(MMHProperties.getAppSetting("site.kitchenPrinter.kpStartupDelayInSeconds")))
                    && (NumberUtils.isNumber(MMHProperties.getAppSetting("site.kitchenPrinter.kpStartupDelayInSeconds")))) {
                delay = Long.parseLong(MMHProperties.getAppSetting("site.kitchenPrinter.kpStartupDelayInSeconds")) * 1000L;
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to get the startup delay for kitchen printers in " +
                    "PrinterQueueHandler.getDelayForKPStartup", PQH_LOG, Logger.LEVEL.ERROR);
        }

        return delay;
    }

    /**
     *
     * @param printerID ID of the printer we want check in the PrinterQueueHandler's map of printers.
     * @return Return true or false based on whether or not the given printer ID is found within the
     *         PrinterQueueHandler's map of printers.
     */
    public boolean isPrinterDefined (int printerID) {
        boolean isDefined = false;

        try {
            if (printers.containsKey(printerID)) {
                isDefined = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to determine whether or not the printer with printer ID " +
                    printerID+" was in the PrinterQueueHandler's map of printers in " +
                    "PrinterQueueHandler.isPrinterDefined", PQH_LOG, Logger.LEVEL.ERROR);
        }

        return isDefined;
    }

    /**
     * Get the QCKitchenPrinter from the PrinterQueueHandler's map of printers at the given printerID.
     *
     * @param printerID ID of the printer we wish to retrieve from the PrinterQueueHandler's map of printers.
     * @return {@link QCKitchenPrinter} The QCKitchenPrinter with the given printerID in the PrinterQueueHandler's map
     *         of printers.
     */
    public QCKitchenPrinter getPrinter (int printerID) {
        QCKitchenPrinter qcKitchenPrinter = null;

        try {
            if (printers.containsKey(printerID)) {
                qcKitchenPrinter = printers.get(printerID);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to get the printer with printer ID "+printerID+" from " +
                    "the PrinterQueueHandler's map of printers in PrinterQueueHandler.getPrinter", PQH_LOG,
                    Logger.LEVEL.ERROR);
        }

        return qcKitchenPrinter;
    }

    /**
     * Try to add a print job to the PrinterQueueHandler's print queue within the specified amount of time.
     *
     * @param qcPrintJob {@link QCPrintJob} The print job we are trying to add to the PrinterQueueHandler's print queue.
     * @return Whether or not the print job was added to the queue.
     */
    public boolean offerToPrintQueue (QCPrintJob qcPrintJob) {
        Logger.logMessage("offerToPrintQueue - printQueueID:" + qcPrintJob.getPaPrinterQueueID(), PQH_LOG, Logger.LEVEL.TRACE);
        boolean addToPrintQueueResult = false;
        try {
            addToPrintQueueResult = printQueue.offer(qcPrintJob, 10, TimeUnit.MILLISECONDS);
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to add a print job to the print queue within 10 " +
                    "milliseconds in PrinterQueueHandler.offerToPrintQueue", PQH_LOG, Logger.LEVEL.ERROR);
        }

        return addToPrintQueueResult;
    }

    /**
     * Print a given print job on the KDS system.
     *
     * @param qcPrintJob {@link QCPrintJob} Print job we are trying to print on the KDS system.
     */
    private void doKDSPrint (QCPrintJob qcPrintJob) {

        try {
            if (qcPrintJob != null) {
                // don't send a print job to KDS if it has failed
                if (!isKDSPrintJobFailed(qcPrintJob)) {
                    // send the print job to the KDS system
                    kdsUtil.createOrderFile(qcPrintJob);
                }
            }
            else {
                Logger.logMessage("The print job passed to PrinterQueueHandler.doKDSPrint can't be null!", PQH_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            if (qcPrintJob != null) {
                Logger.logMessage("There was a problem trying to send print job with a job tracker ID of " +
                        Objects.toString(qcPrintJob.getJobTrackerID(), "NULL")+" to the KDS system in " +
                        "PrinterQueueHandler.doKDSPrint", PQH_LOG, Logger.LEVEL.ERROR);
            }
            // update the status of the print job to indicate that it couldn't be sent to the KDS system
            ArrayList<Integer> kdsStations = null;
            if (qcPrintJob != null) {
                kdsStations = qcPrintJob.getKDSPrintersInPrintJob();
            }
            if (!DataFunctions.isEmptyCollection(kdsStations)) {
                for (int kdsPrinterID : kdsStations) {
                    PrintStatus printStatus = new PrintStatus()
                            .addPrintStatusType(PrintStatusType.ERROR)
                            .addUpdatedDTM(LocalDateTime.now());
                    printJobStatusHandler.updatePrintStatusMapForPrinter(kdsPrinterID, printStatus, qcPrintJob);
                }
            }
        }

    }

    /**
     * <p>Checks whether or not the KDS print job has failed.</p>
     *
     * @param qcPrintJob The KDS {@link QCPrintJob} to check the print status of.
     * @return Whether or not the KDS print job failed.
     */
    @SuppressWarnings("OverlyComplexMethod")
    private boolean isKDSPrintJobFailed (QCPrintJob qcPrintJob) {
        boolean isKDSPrintJobFailed = false;

        try {
            if (qcPrintJob == null) {
                Logger.logMessage("The print job passed to PrinterQueueHandler.isKDSPrintJobFailed can't be null, unable to determine whether or not the KDS print job has failed!", PQH_LOG, Logger.LEVEL.ERROR);
                return false;
            }

            // check if it's an online order
            boolean isOOTerminal = KitchenPrinterCommon.isOOTerminal(PQH_LOG, KitchenPrinterTerminal.getInstance().getDataManager(), qcPrintJob.getTerminalID());
            Logger.logMessage(String.format("The terminal with an ID of %s, %s found to be an online ordering terminal in PrinterQueueHandler.isKDSPrintJobFailed",
                    Objects.toString(qcPrintJob.getTerminalID(), "N/A"),
                    Objects.toString((isOOTerminal ? "was" : "wasn't"), "N/A")), PQH_LOG, Logger.LEVEL.TRACE);

            // get the MAC address and hostname for the given terminal ID
            String[] terminalInfo = KitchenPrinterCommon.getMacAndHostnameFromTerminalID(PQH_LOG, KitchenPrinterTerminal.getInstance().getDataManager(), qcPrintJob.getTerminalID());
            String macAddress = "";
            String hostname = "";
            if ((terminalInfo != null) && (terminalInfo.length == 3)) {
                Logger.logMessage(String.format("The MAC address is: %s and the hostname is: %s for the terminal with an ID of %s in PrinterQueueHandler.isKDSPrintJobFailed.",
                        Objects.toString(terminalInfo[1], "N/A"),
                        Objects.toString(terminalInfo[2], "N/A"),
                        Objects.toString(terminalInfo[0], "N/A")), PQH_LOG, Logger.LEVEL.TRACE);
                macAddress = terminalInfo[1];
                hostname = terminalInfo[2];
            }
            else {
                Logger.logMessage(String.format("Unable to determine the MAC address and hostname for the terminal with an ID of %s in PrinterQueueHandler.isKDSPrintJobFailed!",
                        Objects.toString(qcPrintJob.getTerminalID(), "N/A")), PQH_LOG, Logger.LEVEL.TRACE);
            }

            if ((isOOTerminal) || ((macAddress.equalsIgnoreCase(PeripheralsDataManager.getTerminalMacAddress())) && (hostname.equalsIgnoreCase(PeripheralsDataManager.getTerminalMacAddress())))) {
                // check if the print job on this terminal failed
                int printerQueueID = qcPrintJob.getPaPrinterQueueID();
                int printQueueHeaderPrintStatusID = 0;
                Object queryRes = KitchenPrinterTerminal.getInstance().getDataManager().parameterizedExecuteScalar("data.kitchenPrinter.GetPrintStatusOfPrintQueueHeader", new Object[]{printerQueueID});
                if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString()))) {
                    printQueueHeaderPrintStatusID = Integer.parseInt(queryRes.toString());
                }

                if ((printQueueHeaderPrintStatusID == PrintStatusType.PRINTED)
                        || (printQueueHeaderPrintStatusID == PrintStatusType.ERROR)
                        || (printQueueHeaderPrintStatusID == PrintStatusType.FAILEDLOCALPRINT)
                        || (printQueueHeaderPrintStatusID == PrintStatusType.DECLINEDLOCALREPRINT)) {
                    isKDSPrintJobFailed = true;
                }
            }
            else {
                // make XML RPC call to check if the print job failed
                String url = XmlRpcUtil.getInstance().buildURL(macAddress, hostname);
                Object[] args = new Object[]{hostname, qcPrintJob.getPaPrinterQueueID()};
                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname,
                        KitchenPrinterDataManagerMethod.IS_KDS_PRINT_JOB_FAILED.getMethodName(), args);
                isKDSPrintJobFailed = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.IS_KDS_PRINT_JOB_FAILED.getMethodName());
            }

        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to determine whether or not the KDS print job has failed in PrinterQueueHandler.isKDSPrintJobFailed", PQH_LOG, Logger.LEVEL.ERROR);
        }

        return isKDSPrintJobFailed;
    }

    /**
     * Send a given print job to the KMS system.
     *
     * @param qcPrintJob {@link QCPrintJob} Print job we are trying to print on the KDS system.
     */
    private void sendToKMS(QCPrintJob qcPrintJob) {
        Logger.logMessage("PrinterQueueHandler.sendToKMS - printQueueID:" + qcPrintJob.getPaPrinterQueueID(), "KMS.log", Logger.LEVEL.TRACE);
        try {
            if (qcPrintJob != null) {
                // send the print job to the KDS system
                // KDSOrderFileWriter.createOrderFile(qcPrintJob);
//                new KDSUtil().createOrderFile(qcPrintJob);
                PrintStatus s = new PrintStatus()
                        .addUpdatedDTM(LocalDateTime.now());
                ArrayList<Integer> failedJobIDs = new ArrayList<>();
                KMSManager.getInstance().receivePrintJob(qcPrintJob, failedJobIDs);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to send print job with a job tracker ID of " +
                    Objects.toString(qcPrintJob.getJobTrackerID(), "NULL")+" to the KMS system in " +
                    "PrinterQueueHandler.doKMSPrint", PQH_LOG, Logger.LEVEL.ERROR);
            // update the status of the print job to indicate that it couldn't be sent to the KDS system
            PrintStatus s = new PrintStatus()
                    .addPrintStatusType(PrintStatusType.ERROR)
                    .addUpdatedDTM(LocalDateTime.now());
            printJobStatusHandler.updatePrintStatusMapForPrintJob(s, qcPrintJob);
        }

    }


    /**
     * Print a given print job on the given physical printer.
     *
     * @param qcPrintJob {@link QCPrintJob} The print job we wish to print on a physical printer.
     * @param qcKitchenPrinter {@link QCKitchenPrinter} The physical printer we want to use to print the print job.
     */
    @SuppressWarnings({"ConstantConditions", "SpellCheckingInspection"})
    private void doPhysicalPrint (QCPrintJob qcPrintJob, QCKitchenPrinter qcKitchenPrinter) {

        try {
            if ((qcPrintJob != null) && (qcKitchenPrinter != null) && (isPrinterDefined(Integer.parseInt(qcKitchenPrinter.getPrinterID())))) {
                // check whether or not we can establish a connection to the physical printer
                if (isPrinterConnectionAvailable(qcKitchenPrinter)) {
                    // give the printer the print job
                    PrintStatus resultsOfPhysicalPrint = qcKitchenPrinter.printQCPrintJob(qcPrintJob, printJobStatusHandler);

                    // put the results of physical print job into the print job status tracker
                    int printerID = (NumberUtils.isNumber(qcKitchenPrinter.getPrinterID()) ? Integer.parseInt(qcKitchenPrinter.getPrinterID()) : 0);
                    printJobStatusHandler.updatePrintStatusMapForPrinter(printerID, resultsOfPhysicalPrint, qcPrintJob);

                    // if the print failed on the kitchen printer clear the output buffer so it doesn't ever get printed as not being errored, it will get printed locally
                    if (resultsOfPhysicalPrint.getPrintStatusType() == PrintStatusType.ERROR) {
                        try {
                            Logger.logMessage(String.format("The print job failed to print on the printer %s in PrinterQueueHandler.doPhysicalPrint, now clearing the printer's output buffer.",
                                    Objects.toString(qcKitchenPrinter.getName(), "N/A")), PQH_LOG, Logger.LEVEL.IMPORTANT);
                            qcKitchenPrinter.getPOSPrinter().clearOutput();
                            Logger.logMessage(String.format("Successfully cleared the output buffer of the printer %s in PrinterQueueHandler.doPhysicalPrint.",
                                    Objects.toString(qcKitchenPrinter.getName(), "N/A")), PQH_LOG, Logger.LEVEL.IMPORTANT);
                        }
                        catch (Exception e) {
                            Logger.logException(e, PQH_LOG);
                            Logger.logMessage(String.format("Unable to clear the output buffer of the printer %s in PrinterQueueHandler.doPhysicalPrint.",
                                    Objects.toString(qcKitchenPrinter.getName(), "N/A")), PQH_LOG, Logger.LEVEL.ERROR);
                        }
                    }
                }
                else {
                    // we can't connect to the printer, mark the print job as errored
                    int printerID = (NumberUtils.isNumber(qcKitchenPrinter.getPrinterID()) ? Integer.parseInt(qcKitchenPrinter.getPrinterID()) : 0);
                    PrintStatus printStatus = new PrintStatus()
                            .addPrintStatusType(PrintStatusType.ERROR)
                            .addUpdatedDTM(LocalDateTime.now());
                    printJobStatusHandler.updatePrintStatusMapForPrinter(printerID, printStatus, qcPrintJob);
                }
            }
            else {
                if (qcPrintJob != null) {
                    // indicate that the physical print job couldn't be printed
                    int printerID = (NumberUtils.isNumber(qcKitchenPrinter.getPrinterID()) ? Integer.parseInt(qcKitchenPrinter.getPrinterID()) : 0);
                    PrintStatus printStatus = new PrintStatus()
                            .addPrintStatusType(PrintStatusType.ERROR)
                            .addUpdatedDTM(LocalDateTime.now());
                    printJobStatusHandler.updatePrintStatusMapForPrinter(printerID, printStatus, qcPrintJob);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to execute the physical print job for the print job with " +
                    "a job tracker ID of "+Objects.toString((qcPrintJob != null ? qcPrintJob.getJobTrackerID() : "N/A"), "NULL")+" on the printer " +
                    Objects.toString((qcKitchenPrinter != null ? qcKitchenPrinter.getLogicalName() : "N/A"), "NULL")+" in " +
                    "PrinterQueueHandler.doPhysicalPrint", PQH_LOG, Logger.LEVEL.ERROR);
            // indicate that the physical print job couldn't be printed
            int printerID = (NumberUtils.isNumber(qcKitchenPrinter.getPrinterID()) ? Integer.parseInt(qcKitchenPrinter.getPrinterID()) : 0);
            PrintStatus printStatus = new PrintStatus()
                    .addPrintStatusType(PrintStatusType.ERROR)
                    .addUpdatedDTM(LocalDateTime.now());
            printJobStatusHandler.updatePrintStatusMapForPrinter(printerID, printStatus, qcPrintJob);
        }

    }

    /**
     * <p>Utility method to check whether or not the printer host running on this terminal can get a connection to the given {@link QCKitchenPrinter}.</p>
     *
     * @param printer The {@link QCKitchenPrinter} to connect to.
     * @return Whether or not the printer host running on this terminal can get a connection to the given {@link QCKitchenPrinter}.
     */
    @SuppressWarnings("OverlyComplexMethod")
    public boolean isPrinterConnectionAvailable (QCKitchenPrinter printer) {
        boolean canConnect = false;

        try {
            if (printer != null) {
                if (printer.getIsPrinterOpen()) {
                    Logger.logMessage(String.format("The printer host running on the terminal %s already has a connection " +
                            "to the printer %s in PrinterQueueHandler.isPrinterConnectionAvailable",
                            Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                            Objects.toString((StringFunctions.stringHasContent(printer.getName()) ? printer.getName() : "N/A"), "NULL")), PQH_LOG, Logger.LEVEL.TRACE);
                    // connection is available so return
                    return true;
                }
                else {
                    Logger.logMessage(String.format("The printer host running on the terminal %s was unable to establish a connection " +
                            "to the printer %s, checking if another printer host is still holding a connection to the printer in " +
                            "PrinterQueueHandler.isPrinterConnectionAvailable",
                            Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                            Objects.toString((StringFunctions.stringHasContent(printer.getName()) ? printer.getName() : "N/A"), "NULL")), PQH_LOG, Logger.LEVEL.TRACE);
                    // check if another printer host has the connection
                    String macAddressOfTerminalWithPrinterConnection = "";
                    if (StringFunctions.stringHasContent(getAnotherPrinterHostHoldingPrinterConnection(printer))) {
                        macAddressOfTerminalWithPrinterConnection = getAnotherPrinterHostHoldingPrinterConnection(printer);
                    }
                    else {
                        Logger.logMessage(String.format("No other printer host was found to be holding a connection to the " +
                                "printer %s in PrinterQueueHandler.isPrinterConnectionAvailable",
                                Objects.toString((StringFunctions.stringHasContent(printer.getName()) ? printer.getName() : "N/A"), "NULL")), PQH_LOG, Logger.LEVEL.ERROR);
                    }
                    if (StringFunctions.stringHasContent(macAddressOfTerminalWithPrinterConnection)) {
                        Logger.logMessage(String.format("The printer host running on the terminal %s was found to have a " +
                                "connection to the printer %s, sending a request to the terminal %s to release it's connection " +
                                "to the printer %s in PrinterQueueHandler.isPrinterConnectionAvailable",
                                Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddressOfTerminalWithPrinterConnection), "NULL"),
                                Objects.toString(printer.getName(), "NULL"),
                                Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddressOfTerminalWithPrinterConnection), "NULL"),
                                Objects.toString(printer.getName(), "NULL")), PQH_LOG, Logger.LEVEL.TRACE);
                        if (isPrinterConnectionReleased(printer, macAddressOfTerminalWithPrinterConnection)) {
                            Logger.logMessage(String.format("The printer host running on the terminal %s has successfully " +
                                    "released it's connection to the printer %s, the printer host running on the terminal %s " +
                                    "will now attempt to gain a connection to the printer %s in PrinterQueueHandler.isPrinterConnectionAvailable",
                                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddressOfTerminalWithPrinterConnection), "NULL"),
                                    Objects.toString(printer.getName(), "NULL"),
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                    Objects.toString(printer.getName(), "NULL")), PQH_LOG, Logger.LEVEL.TRACE);
                            // try to connect to the printer
                            canConnect = printer.openPrinter();
                            if (!canConnect) {
                                Logger.logMessage(String.format("The printer host running on the terminal %s was still unable to establish a connection " +
                                        "to the printer %s in PrinterQueueHandler.isPrinterConnectionAvailable",
                                        Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                        Objects.toString((StringFunctions.stringHasContent(printer.getName()) ? printer.getName() : "N/A"), "NULL")), PQH_LOG, Logger.LEVEL.ERROR);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage(String.format("There was a problem trying to get a connection to the printer %s in PrinterQueueHandler.isPrinterConnectionAvailable",
                    Objects.toString(printer.getName(), "NULL")));
        }

        return canConnect;
    }

    /**
     * <p>Utility method to check whether or not another printer host is holding a connection to the desired {@link QCKitchenPrinter}.</p>
     *
     * @param printer The {@link QCKitchenPrinter} to check.
     * @return The MAC address {@link String} of the terminal running the printer host that has the connection to the desired {@link QCKitchenPrinter}.
     */
    public String getAnotherPrinterHostHoldingPrinterConnection (QCKitchenPrinter printer) {
        String macAddress = "";

        try {
            // get the printer hosts
            CopyOnWriteArraySet<QCPrinterHost> printerHosts = new CopyOnWriteArraySet<>();
            if ((BackofficeChangeUpdater.getInstance() != null) && (!DataFunctions.isEmptyCollection(BackofficeChangeUpdater.getInstance().getPrinterHostsFromBackofficeUpdaterThread()))) {
                printerHosts = BackofficeChangeUpdater.getInstance().getPrinterHostsFromBackofficeUpdaterThread();
            }
            if (!DataFunctions.isEmptyCollection(printerHosts)) {
                // iterate though the printer hosts and check if any of them have a connection to the printer
                for (QCPrinterHost printerHost : printerHosts) {
                    // no need to check this terminal, it's already known there isn't a connection
                    if (!PeripheralsDataManager.getTerminalMacAddress().equalsIgnoreCase(printerHost.getMacAddress())) {
                        String url = XmlRpcUtil.getInstance().buildURL(printerHost.getMacAddress(), printerHost.getHostname());
                        Object[] args = new Object[]{PeripheralsDataManager.getPHHostname(), Integer.parseInt(printer.getPrinterID())};
                        Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, printerHost.getMacAddress(), printerHost.getHostname(),
                                KitchenPrinterDataManagerMethod.CHECK_FOR_PRINTER_HOST_WITH_PRINTER_CONNECTION.getMethodName(), args);
                        if (XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.CHECK_FOR_PRINTER_HOST_WITH_PRINTER_CONNECTION.getMethodName())) {
                            // we found another printer host holding the connection
                            macAddress = printerHost.getMacAddress();
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not another printer host has " +
                    "a connection to the printer %s in PrinterQueueHandler.getAnotherPrinterHostHoldingPrinterConnection",
                    Objects.toString((printer != null ? printer.getName() : "N/A"), "NULL")), PQH_LOG, Logger.LEVEL.ERROR);
        }

        return macAddress;
    }

    /**
     * <p>Utility method to force a printer host to release it's connection to the given {@link QCKitchenPrinter}.</p>
     *
     * @param printer The {@link QCKitchenPrinter} to release printer host's connection for.
     * @param macAddress The MAC address {@link String} of the terminal running the printer host that currently has a
     * connection to the desired printer.
     * @return Whether or not the printer host released it's connection to the given {@link QCKitchenPrinter}.
     */
    public boolean isPrinterConnectionReleased (QCKitchenPrinter printer, String macAddress) {
        boolean connectionReleased = false;

        try {
            if ((printer != null) && (StringFunctions.stringHasContent(macAddress)) && (!PeripheralsDataManager.getTerminalMacAddress().equalsIgnoreCase(macAddress))) {
                // make XML RPC call
                String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                Object[] args = new Object[]{PeripheralsDataManager.getPHHostname(), Integer.parseInt(printer.getPrinterID())};
                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                        KitchenPrinterDataManagerMethod.CHECK_PRINTER_HOST_CONNECTION_TO_PRINTER_RELEASED.getMethodName(), args);
                connectionReleased = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.CHECK_PRINTER_HOST_CONNECTION_TO_PRINTER_RELEASED.getMethodName());
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the printer host running on the terminal %s " +
                    "to release it's connection to the printer %s in PrinterQueueHandler.isPrinterConnectionReleased",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL"),
                    Objects.toString(printer.getName(), "NULL")), PQH_LOG, Logger.LEVEL.ERROR);
        }

        return connectionReleased;
    }

    /**
     * <p>Sorts the print job details based on their kitchen receipt type.</p>
     *
     * @param printJobDetails An {@link ArrayList} of {@link QCPrintJobDetail} corresponding to the print job details
     * @return A {@link HashMap} whose {@link String} key is the receipt type of the print job detail and whose
     * value is an {@link ArrayList} of {@link QCPrintJobDetail} corresponding to print job details of the same receipt type.
     */
    private HashMap<String, ArrayList<QCPrintJobDetail>> sortPrintJobDetailsByType (ArrayList<QCPrintJobDetail> printJobDetails) {

        if (DataFunctions.isEmptyCollection(printJobDetails)) {
            Logger.logMessage("The print job details passed to PrinterQueueHandler.sortPrintJobDetailsByType can't be null or empty!", PQH_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        HashMap<String, ArrayList<QCPrintJobDetail>> sortedDetails = new HashMap<>();
        ArrayList<QCPrintJobDetail> detailsOfReceiptType;
        for (QCPrintJobDetail qcPrintJobDetail : printJobDetails) {
            if ((qcPrintJobDetail.getPrinterID() > 0) && (qcPrintJobDetail.getStationID() <= 0)) {
                if (sortedDetails.containsKey(KitchenReceiptType.KP)) {
                    detailsOfReceiptType = sortedDetails.get(KitchenReceiptType.KP);
                }
                else {
                    detailsOfReceiptType = new ArrayList<>();
                }
                detailsOfReceiptType.add(qcPrintJobDetail);
                sortedDetails.put(KitchenReceiptType.KP, detailsOfReceiptType);
            }
            else if ((qcPrintJobDetail.getPrinterID() > 0) && (qcPrintJobDetail.getStationID() > 0)) {
                if (sortedDetails.containsKey(KitchenReceiptType.KDS)) {
                    detailsOfReceiptType = sortedDetails.get(KitchenReceiptType.KDS);
                }
                else {
                    detailsOfReceiptType = new ArrayList<>();
                }
                detailsOfReceiptType.add(qcPrintJobDetail);
                sortedDetails.put(KitchenReceiptType.KDS, detailsOfReceiptType);
            }
            else if ((usingKMS) && (qcPrintJobDetail.getKMSStationID() > 0)) {
                if (sortedDetails.containsKey(KitchenReceiptType.KMS)) {
                    detailsOfReceiptType = sortedDetails.get(KitchenReceiptType.KMS);
                }
                else {
                    detailsOfReceiptType = new ArrayList<>();
                }
                detailsOfReceiptType.add(qcPrintJobDetail);
                sortedDetails.put(KitchenReceiptType.KMS, detailsOfReceiptType);
            }
        }

        return sortedDetails;
    }

    /**
     * Getter for the PrinterQueueHandler's map of printers.
     *
     * @return {@link ConcurrentHashMap<Integer, QCKitchenPrinter>} PrinterQueueHandler's map of printers.
     */
    public ConcurrentHashMap<Integer, QCKitchenPrinter> getPrinters () {
        return printers;
    }

    /**
     * Determines whether or not the printerQueueHandlerThread is running.
     *
     * @return Whether or not the printerQueueHandlerThread is running.
     */
    public boolean isActive () {
        boolean isActive = false;

        try {
            if ((!isShuttingDown) && (!isTerminated)) {
                isActive = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to determine whether or not the printerQueueHandlerThread " +
                    "is running in PrinterQueueHandlerThread.isActive", PQH_LOG, Logger.LEVEL.ERROR);
        }

        return isActive;
    }

    /**
     * Creates and starts a Thread using the PrinterQueueHandler Runnable.
     *
     */
    public void startPrinterQueueHandlerThread () {

        try {
            // reset isShuttingDown and isTerminated
            isShuttingDown = false;
            isTerminated = false;

            Thread printerQueueHandlerThread = new Thread(this);
            printerQueueHandlerThread.start();
            Logger.logMessage("The PrinterQueueHandler Thread has started successfully in " +
                    "PrinterQueueHandler.startPrinterQueueHandlerThread", PQH_LOG, Logger.LEVEL.TRACE);

            if(usingKMS){
                KMSChangeUpdater.getInstance().startKMSChangeUpdaterThread();
                KMSOrderStatusUpdateQueueHandler.getInstance().startStatusUpdateQueueHandlerThread();
                KMSEventLogQueueHandler.getInstance().startEventLogQueueHandlerThread();//This will open up the KMSManager
            }
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to start the PrinterQueueHandler Thread in " +
                    "PrinterQueueHandler.startPrinterQueueHandlerThread", PQH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Poisons the PrinterQueueHandler's print queue and stops the PrinterQueueHandler Thread.
     *
     */
    public void stopPrinterQueueHandlerThread () {

        try {
            Logger.logMessage("Stopping printer queue handler thread in " +
                            "PrinterQueueHandler.stopPrinterQueueHandlerThread", PQH_LOG, Logger.LEVEL.DEBUG);

            // poison the print queue
            QCPrintJob qcPrintJob = new QCPrintJob();
            qcPrintJob.setPersonName(POISONOUS_PRINT_JOB_PERSON_NAME);
            printQueue.offer(qcPrintJob);

            // run through Thread shutdown routine
            isShuttingDown = true;

            // close connections to the printers
            if ((printers != null) && (!printers.isEmpty())) {
                printers.values().stream().filter(printer -> printer != null).forEach(QCKitchenPrinter::closePrinter);
                printers.clear();
            }

            int waitCount = 0;
            while (!isTerminated) {
                waitCount++;
                // wait a second
                Thread.sleep(1000L);
                if (waitCount >= 3) {
                    Logger.logMessage("The PrinterQueueHandler Thread didn't stop within the specified wait time, it " +
                            "is being interrupted now in PrinterQueueHandler.stopPrinterQueueHandlerThread", PQH_LOG,
                            Logger.LEVEL.TRACE);
                    // interrupt the PrinterQueueHandler Thread
                    Thread.getAllStackTraces().keySet().forEach((thread) -> {
                        if (thread.getName().equalsIgnoreCase("KitchenPrinter-PrinterQueueHandler")) {
                            thread.interrupt();
                        }
                    });
                }
            }

            //shut down the EventLogQueueHandler and in turn the KMSManager
            try {
                Logger.logMessage("Shutting down KMSEventLogQueueHandler", PQH_LOG, Logger.LEVEL.DEBUG);
                KMSEventLogQueueHandler.getInstance().stopEventLogQueueHandlerThread();
            }
            catch (Exception e) {
                Logger.logException(e, PQH_LOG);
                Logger.logMessage("There was a problem trying to shutdown the KMSEventLogQueueHandler in " +
                        "PrinterQueueHandler.stopPrinterQueueHandlerThread", PQH_LOG, Logger.LEVEL.ERROR);
            }
            try {
                Logger.logMessage("Shutting down KMSOrderStatusUpdateQueueHandler", PQH_LOG, Logger.LEVEL.DEBUG);
                KMSOrderStatusUpdateQueueHandler.getInstance().stopStatusUpdateQueueHandlerThread();
            }
            catch (Exception e) {
                Logger.logException(e, PQH_LOG);
                Logger.logMessage("There was a problem trying to shutdown the KMSOrderStatusUpdateQueueHandler in " +
                        "PrinterQueueHandler.stopPrinterQueueHandlerThread", PQH_LOG, Logger.LEVEL.ERROR);
            }
            try {
                Logger.logMessage("Shutting down KMSChangeUpdater", PQH_LOG, Logger.LEVEL.DEBUG);
                KMSChangeUpdater.getInstance().stopKMSChangeUpdaterThread();
            }
            catch (Exception e) {
                Logger.logException(e, PQH_LOG);
                Logger.logMessage("There was a problem trying to shutdown the KMSChangeUpdater in " +
                        "PrinterQueueHandler.stopPrinterQueueHandlerThread", PQH_LOG, Logger.LEVEL.ERROR);
            }
            Logger.logMessage("The PrinterQueueHandlerThread has been stopped", PQH_LOG, Logger.LEVEL.IMPORTANT);
        }
        catch (Exception e) {
            Logger.logException(e, PQH_LOG);
            Logger.logMessage("There was a problem trying to stop the PrinterQueueHandler Thread in " +
                    "PrinterQueueHandler.stopPrinterQueueHandlerThread", PQH_LOG, Logger.LEVEL.ERROR);
        }

    }
}
