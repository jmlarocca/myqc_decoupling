package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2019-12-11 09:14:00 -0500 (Wed, 11 Dec 2019) $: Date of last commit
    $Rev: 44352 $: Revision of last commit
    Notes: Send print data to printer host and print expired or errored print jobs locally.
*/

import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.xmlapi.XmlRpcUtil;
import org.apache.commons.lang.math.NumberUtils;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Singleton class responsible for sending print jobs to the printer host so they can be printed and printing
 * any errored or expired print jobs locally.
 *
 */
public class TerminalThread implements Runnable {

    // the only instance of a TerminalThread for this WAR
    private static volatile TerminalThread terminalThreadInstance = null;

    // string constants
    // encrypted "******************"
    private static final String ENCRYPTED_ASTERISKS = "G3xjQRt8NEMqKioqKioqKioqKioqKioqKio=";
    // encrypted "Error or Timeout"
    private static final String ENCRYPTED_ERROR_OR_TIMEOUT = "G3xjQRt8NENFcnJvciBvciBUaW1lb3V0";
    // encrypted "Redirected due to"
    private static final String ENCRYPTED_REDIRECTED_DUE_TO = "G3xjQRt8NENSZWRpcmVjdGVkIGR1ZSB0bw==";
    // encrypted JPOS CENTER command, we can use this for an empty line on the receipt
    private static final String ENCRYPTED_EMPTY_LINE = "G3xjQSA=";
    private static final String QUEUE_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";


    // log file specific to the TerminalThread
    private static final String TT_LOG = "KP_TerminalThread.log";

    // variables within the TerminalThread scope
    private volatile boolean isShuttingDown = false;
    private volatile boolean isTerminated = false;
    private Timer successDaysToLiveTimer;

    // variables needed for TerminalThread instantiation
    private KitchenPrinterTerminal kitchenPrinterTerminal;

    /**
     * Private constructor for a TerminalThread.
     *
     * @param kitchenPrinterTerminal {@link KitchenPrinterTerminal} The terminal this TerminalThread is running on.
     */
    private TerminalThread (KitchenPrinterTerminal kitchenPrinterTerminal) {
        this.kitchenPrinterTerminal = kitchenPrinterTerminal;
    }

    /**
     * Get the only instance of TerminalThread for this WAR.
     *
     * @return {@link TerminalThread} The instance of TerminalThread for this WAR.
     */
    public static TerminalThread getInstance () {

        try {
            if (terminalThreadInstance == null) {
                throw new RuntimeException("The TerminalThread must be instantiated before trying to return its instance.");
            }
        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage("There was a problem trying to get the only instance of TerminalThread for this WAR in " +
                    "TerminalThread.getInstance", TT_LOG, Logger.LEVEL.ERROR);
        }

        return terminalThreadInstance;
    }

    /**
     * Instantiate and return the only instance of TerminalThread for this WAR.
     *
     * @param kitchenPrinterTerminal {@link KitchenPrinterTerminal} The terminal this TerminalThread is running on.
     * @return {@link TerminalThread} The instance of TerminalThread for this WAR.
     */
    public static synchronized TerminalThread init (KitchenPrinterTerminal kitchenPrinterTerminal) {

        try {
            if (terminalThreadInstance != null) {
                throw new RuntimeException("The TerminalThread has already been instantiated and may not be " +
                        "instantiated again.");
            }

            // create the instance
            terminalThreadInstance = new TerminalThread(kitchenPrinterTerminal);
            Logger.logMessage("The instance of TermialThread has been instantiated", TT_LOG, Logger.LEVEL.ERROR);
        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage("There was a problem trying to instantiate and return the only instance of " +
                    "TerminalThread for this WAR in TerminalThread.init", TT_LOG, Logger.LEVEL.ERROR);
        }

        return terminalThreadInstance;
    }

    /**
     * Method that will be called by a Thread created using the TerminalThread Runnable.
     *
     */
    @SuppressWarnings({"MagicNumber", "unchecked"})
    @Override
    public void run () {
        // start the timer
        successDaysToLiveTimer = new Timer();
        // based on the app property "site.kitchenPrinter.successDaysToLive" remove any print jobs from the
        // QC_PAPrinterQueue table that are older than the property
        successDaysToLiveTimer.schedule(new TimerTask() {
            @Override
            public void run () {
                try {
                    String daysToLive = MMHProperties.getAppSetting("site.kitchenPrinter.successDaysToLive");
                    if ((daysToLive != null) && (!daysToLive.isEmpty())) {
                        deleteOldSuccessfulPrintJobs(daysToLive);
                    }
                }
                catch (Exception e) {
                    Logger.logException(e, TT_LOG);
                    Logger.logMessage("There was a problem in the timer task to remove old successful print jobs in " +
                            "TerminalThread.run", TT_LOG, Logger.LEVEL.ERROR);
                }
            }
        }, 0, TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));

        try {
            // wait 30 seconds for the web service to accept connections
            Thread.sleep(30000L);

            Thread.currentThread().setName("KitchenPrinter-TerminalThread");

            while ((!isShuttingDown) && (!Thread.currentThread().isInterrupted())) {
                try {
                    ArrayList printData;

                    // get any print jobs with a print job status of waiting, sent, or error from the local database
                    printData = kitchenPrinterTerminal.getDataManager().parameterizedExecuteQuery(
                            "data.newKitchenPrinter.SelectUnprintedDetail", new Object[]{},
                            TT_LOG, true);

                    if ((printData != null) && (!printData.isEmpty())) {
                        printData = KitchenPrinterCommon.reorganizePrintData(printData, TT_LOG);
                    }
                    else {
                        Thread.sleep(kitchenPrinterTerminal.dbPollingIntervalInSeconds() * 1000L);
                        continue;
                    }

                    // print any expired or errored print jobs locally and remove them from the local database
                    printData = printExpiredOrErroredPrintJobsLocally(printData);

                    if ((printData != null) && (!printData.isEmpty())) {
                        // send print jobs ot the active printer host to be printed
                        sendPrintDataToPrinterHost(printData);
                    }
                }
                catch (Exception e) {
                    Logger.logException(e, TT_LOG);
                    Logger.logMessage("There was a problem within the while loop in TerminalThread.run", TT_LOG, Logger.LEVEL.ERROR);
                }
                Thread.sleep(kitchenPrinterTerminal.dbPollingIntervalInSeconds() * 1000L);
            }
        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage("There was a problem in TerminalThread.run", TT_LOG, Logger.LEVEL.ERROR);
        }
        finally {
            isTerminated = true;
            Logger.logMessage("KitchenPrinter-TerminalThread is exiting", TT_LOG, Logger.LEVEL.TRACE);
        }

    }

    /**
     * Remove any old print jobs that were successful and are older than the number of daysToLive.
     *
     * @param daysToLive {@link String} The number of days to keep the print job around for.
     */
    private void deleteOldSuccessfulPrintJobs (String daysToLive) {

        try {
            int queryRes = kitchenPrinterTerminal.getDataManager().parameterizedExecuteNonQuery(
                    "data.kitchenPrinter.deleteOldSuccessJobDetails", new Object[]{Integer.parseInt(daysToLive)}, TT_LOG);
            if (queryRes < 0) {
                Logger.logMessage("The query to remove print jobs that were successful from the QC_PAPrinterQueueDetail " +
                        "table and are older than "+ Objects.toString(daysToLive, "NULL")+" days failed in " +
                        "TerminalThread.deleteOldSuccessfulPrintJobs", TT_LOG, Logger.LEVEL.ERROR);
            }
            queryRes = kitchenPrinterTerminal.getDataManager().parameterizedExecuteNonQuery(
                    "data.kitchenPrinter.deleteOldSuccessJobs", new Object[]{Integer.parseInt(daysToLive)}, TT_LOG);
            if (queryRes < 0) {
                Logger.logMessage("The query to remove print jobs that were successful from the QC_PAPrinterQueue table " +
                        "and are older than "+Objects.toString(daysToLive, "NULL")+" days failed in " +
                        "TerminalThread.deleteOldSuccessfulPrintJobs", TT_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage("There was a problem trying to remove print jobs that were successful and are older " +
                    "than "+ Objects.toString(daysToLive, "NULL")+" days in TerminalThread.deleteOldSuccessfulPrintJobs",
                    TT_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Iterates through the ArrayList of print jobs and if a print job is found to be errored or expired it will be
     * printed locally otherwise the print job will be returned.
     *
     * @param printJobs {@link ArrayList<ArrayList>} Print jobs to check for errored or expired print jobs.
     * @return {@link ArrayList} The print jobs that aren't errored or expired.
     */
    @SuppressWarnings({"SpellCheckingInspection", "OverlyComplexMethod", "UseOfObsoleteDateTimeApi"})
    private ArrayList printExpiredOrErroredPrintJobsLocally (ArrayList<ArrayList> printJobs) {
        ArrayList<ArrayList> printJobsToPrint = new ArrayList<>();
        ArrayList<ArrayList> printJobsToPrintLocally = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(printJobs)) {
                printJobs.forEach((printJob) -> {
                    HashMap printLineDetail = (HashMap) printJob.get(0);
                    String printJobStatusID = HashMapDataFns.getStringVal(printLineDetail, "PRINTSTATUSID");

                    // if the print job is errored add a redirected header and add it to the list
                    // of print jobs to be printed locally
                    if (Integer.parseInt(printJobStatusID) == PrintStatusType.ERROR) {
                        Logger.logMessage(String.format("Adding the print job with a transaction ID of %s to the list " +
                                "of print jobs which have been errored and will be printed locally in TerminalThread.printExpiredOrErroredPrintJobsLocally",
                                Objects.toString( HashMapDataFns.getStringVal(printLineDetail, "PATRANSACTIONID"), "NULL")), TT_LOG, Logger.LEVEL.TRACE);
                        if (!isPrintJobForOMS(printJob)) {
                            addRedirectedHeaderToEntireOrder(printJob, printLineDetail);
                        }
                        else {
                            addRedirectedHeaderPerSuborder(printJob, printLineDetail);
                        }
                        printJobsToPrintLocally.add(printJob);
                    }
                    else {
                        // check if the print job has expired and should be printed locally
                        boolean isKDS = HashMapDataFns.getBooleanVal(printLineDetail, "ISKDS");
                        boolean isOnlineOrder = HashMapDataFns.getBooleanVal(printLineDetail, "ONLINEORDER");
                        int terminalID = HashMapDataFns.getIntVal(printLineDetail, "TERMINALID");
                        String paTransactionID = HashMapDataFns.getStringVal(printLineDetail, "PATRANSACTIONID");
                        String queueTimeStr = HashMapDataFns.getStringVal(printLineDetail, "QUEUETIME");
                        String estimatedOrderTimeStr = HashMapDataFns.getStringVal(printLineDetail, "ESTIMATEDORDERTIME");
                        String txnDateStr = HashMapDataFns.getStringVal(printLineDetail, "TRANSACTIONDATE");

                        if (isPrintJobExpired(paTransactionID, queueTimeStr, estimatedOrderTimeStr, txnDateStr, terminalID, isKDS, isOnlineOrder)) {
                            Logger.logMessage(String.format("Adding the print job with a transaction ID of %s to the list " +
                                    "of print jobs which have expired and will be printed locally in TerminalThread.printExpiredOrErroredPrintJobsLocally",
                                    Objects.toString(paTransactionID, "NULL")), TT_LOG, Logger.LEVEL.TRACE);
                            if (!isPrintJobForOMS(printJob)) {
                                addRedirectedHeaderToEntireOrder(printJob, printLineDetail);
                            }
                            else {
                                addRedirectedHeaderPerSuborder(printJob, printLineDetail);
                            }
                            printJobsToPrintLocally.add(printJob);
                        }
                        else {
                            Logger.logMessage(String.format("Adding the print job with a transaction ID of %s to the list " +
                                    "of print jobs that will be sent to the printer host in TerminalThread.printExpiredOrErroredPrintJobsLocally",
                                    Objects.toString(paTransactionID, "NULL")), TT_LOG, Logger.LEVEL.TRACE);
                            // print job should be sent to the printer host
                            printJobsToPrint.add(printJob);
                        }
                    }
                });
            }

            // print the print jobs that should be printed locally
            if (!printJobsToPrintLocally.isEmpty()) {
                // get all the PAPrinterQueueIDs of the print jobs to be printed locally
                List<String> paPrinterQueueIDs = printJobsToPrintLocally.stream().map((printJob) ->
                        ((HashMap) printJob.get(0)).get("PAPRINTERQUEUEID").toString()).collect(Collectors.toList());
                Logger.logMessage("The following print jobs with PAPrinterQueueIDs " +
                        Objects.toString(String.join(", ", paPrinterQueueIDs), "NULL")+" will be printed locally due " +
                        "to an error or expiration", TT_LOG, Logger.LEVEL.TRACE);

                kitchenPrinterTerminal.getPeripheralsDataManager().handleKitchenPrintJobLocally(printJobsToPrintLocally);
            }
        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage("There was a problem trying to determine which print jobs are errored or expired and " +
                    "should be printed locally in TerminalThread.printExpiredOrErroredPrintJobsLocally", TT_LOG,
                    Logger.LEVEL.ERROR);
        }

        return printJobsToPrint;
    }

    /**
     * <p>Determines whether or not the print job has expired.</p>
     *
     * @param paTransactionID The ID {@link String} of the transaction the print job is for.
     * @param queueTimeStr The time {@link String} the order was added to the print queue.
     * @param estimatedOrderTimeStr The time {@link String} it is estimated that the order will be ready.
     * @param txnDateStr A {@link String} containing when the order should be picked up from the kitchen.
     * @param terminalID The ID of the terminal that made the transaction.
     * @param isKDS Whether or not the print job details are intended for KDS.
     * @param isOnlineOrder Whether or not the transaction was an online order.
     * @return Whether or not the print job has expired.
     */
    @SuppressWarnings("OverlyComplexMethod")
    private boolean isPrintJobExpired (String paTransactionID, String queueTimeStr, String estimatedOrderTimeStr, String txnDateStr, int terminalID, boolean isKDS, boolean isOnlineOrder) {
        boolean hasPrintJobExpired = false;



        try {
            String dateTimePatternForEstOrdTime = getDateTimeFormatPattern(estimatedOrderTimeStr);
            String dateTimePatternForTxnTime = getDateTimeFormatPattern(txnDateStr);



            LocalDateTime estimatedOrderTime = null;
            if (StringFunctions.stringHasContent(estimatedOrderTimeStr)) {
                if (StringFunctions.stringHasContent(dateTimePatternForEstOrdTime)) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimePatternForEstOrdTime);
                    estimatedOrderTime = LocalDateTime.parse(estimatedOrderTimeStr, formatter);
                }
                else {
                    Logger.logMessage(String.format("Unable to determine the date time format pattern that should be used to parse a LocalDateTime of the estimated order time from the String %s!",
                            Objects.toString(estimatedOrderTimeStr, "N/A")), TT_LOG, Logger.LEVEL.ERROR);
                }
            }
            if (estimatedOrderTime == null) {
                Logger.logMessage(String.format("Unable to parse a valid estimated order time from the estimated order time String of %s!",
                        Objects.toString(estimatedOrderTimeStr, "N/A")), TT_LOG, Logger.LEVEL.ERROR);
            }



            LocalDateTime txnDate = null;
            if (StringFunctions.stringHasContent(txnDateStr)) {
                if (StringFunctions.stringHasContent(dateTimePatternForTxnTime)) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimePatternForTxnTime);
                    txnDate = LocalDateTime.parse(txnDateStr, formatter);
                }
                else {
                    Logger.logMessage(String.format("Unable to determine the date time format pattern that should be used to parse a LocalDateTime of the transaction date from the String %s!",
                            Objects.toString(estimatedOrderTimeStr, "N/A")), TT_LOG, Logger.LEVEL.ERROR);
                }
            }
            if (txnDate == null) {
                Logger.logMessage(String.format("Unable to parse a valid transaction date from the transaction date String of %s!",
                        Objects.toString(txnDateStr, "N/A")), TT_LOG, Logger.LEVEL.ERROR);
            }



            LocalDateTime expirationTime = null;
            if (estimatedOrderTime != null) {
                expirationTime = estimatedOrderTime.plusMinutes(kitchenPrinterTerminal.getPrintJobExpirationInMinutes());
            }
            else {
                if (txnDate != null) {
                    expirationTime = txnDate.plusMinutes(kitchenPrinterTerminal.getPrintJobExpirationInMinutes());
                }
            }



            if (expirationTime == null) {
                Logger.logMessage(String.format("Unable to determine whether or not the print job for the transaction with an ID of %s is expired, " +
                        "given the queue time %s, estimated order time of %s, and transaction date of %s, marking it as expired so it will print locally!",
                        Objects.toString(paTransactionID, "N/A"),
                        Objects.toString(queueTimeStr, "N/A"),
                        Objects.toString(estimatedOrderTimeStr, "N/A"),
                        Objects.toString(txnDateStr, "N/A")), TT_LOG, Logger.LEVEL.ERROR);
                return true;
            }



            // check if the current time is after the print job should've expired
            if (LocalDateTime.now().isAfter(expirationTime)) {
                hasPrintJobExpired = true;
            }



            Logger.logMessage(String.format("The print job with a transaction ID of %s %s expired, given the params: queue time = %s, " +
                    "estimated order time = %s, and expiration time = %s in TerminalThread.isPrintJobExpired",
                    Objects.toString(paTransactionID, "N/A"),
                    Objects.toString((hasPrintJobExpired ? "has" : "hasn't"), "N/A"),
                    Objects.toString(queueTimeStr, "N/A"),
                    Objects.toString(estimatedOrderTimeStr, "N/A"),
                    Objects.toString(expirationTime.toString(), "N/A")), TT_LOG, Logger.LEVEL.TRACE);



        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the print job for the transaction with " +
                    "an ID of %s had expired given the queue time of %s and estimated order time of %s in TerminalThread.isPrintJobExpired",
                    Objects.toString(paTransactionID, "NULL"),
                    Objects.toString(queueTimeStr, "NULL"),
                    Objects.toString(estimatedOrderTimeStr, "NULL")), TT_LOG, Logger.LEVEL.ERROR);
        }



        return hasPrintJobExpired;
    }



    /**
     * <p>Determines the pattern that should be used by a {@link DateTimeFormatter} to parse the time {@link String}.</p>
     *
     * @param timeStr The time {@link String} that will need to be parsed.
     * @return The pattern {@link String} that should be used by a {@link DateTimeFormatter} to parse the time {@link String}.
     */
    private String getDateTimeFormatPattern (String timeStr) {



        // make sure the time String is valid
        if (!StringFunctions.stringHasContent(timeStr)) {
            Logger.logMessage("The time String passed to TerminalThread.getDateTimeFormatPattern can't be null or empty, unable " +
                    "to determine the date time format pattern to use, now returning null!", TT_LOG, Logger.LEVEL.ERROR);
            return null;
        }



        // possible date time patterns
        String[] dateTimeFormatPatterns = new String[]{"yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss.S", "yyyy-MM-dd HH:mm:ss.SSS"};



        // try to use each pattern to determine the correct one
        String patternToUse = "";
        for (String pattern : dateTimeFormatPatterns) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
            try {
                LocalDateTime.parse(timeStr, dateTimeFormatter);
                patternToUse = pattern;
                break;
            }
            catch (Exception e) {
                // continue on to the next pattern
            }
        }



        return patternToUse;
    }

    /**
     * <p>Queries the database to get the prep time in minutes for the terminal.</p>
     *
     * @param terminalID ID of the terminal to get the prep time for.
     * @return The prep time in minutes for the given terminal.
     */
    private int getTerminalPrepTime (int terminalID) {
        int prepTime = -1;

        if (terminalID <= 0) {
            Logger.logMessage("The terminal ID passed to PeripheralsDataManager.getTerminalPrepTime must be greater than 0!", TT_LOG, Logger.LEVEL.ERROR);
            return -1;
        }

        // query the database
        DynamicSQL sql =
                new DynamicSQL("data.kitchenPrinter.GetTerminalPrepTime")
                        .addIDList(1, terminalID);
        Object queryRes = sql.getSingleField(kitchenPrinterTerminal.getDataManager());
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString()))) {
            prepTime = Integer.parseInt(queryRes.toString());
        }

        return prepTime;
    }

    /**
     * <p>Checks whether or not the given print job is destined for OMS or not.</p>
     *
     * @param printJob An {@link ArrayList} containing the print job details.
     * @return Whether or not the given print job is destined for OMS or not.
     */
    private boolean isPrintJobForOMS (ArrayList printJob) {

        if (!DataFunctions.isEmptyCollection(printJob)) {
            HashMap firstPrintJobDetail = DataFunctions.convertObjectToType(printJob.get(0), HashMap.class);
            if (!DataFunctions.isEmptyMap(firstPrintJobDetail)) {
                return (HashMapDataFns.getIntVal(firstPrintJobDetail, "KMSSTATIONID") > 0);
            }
        }

        return false;
    }

    /**
     * <p>Adds a header for the order in the print job to indicate that it was redirected.</p>
     *
     * @param printJob {@link ArrayList} The print job we want to add the redirected header to.
     * @param printLineDetail {@link HashMap} The existing lines of text to print to for the print job.
     */
    @SuppressWarnings("unchecked")
    private void addRedirectedHeaderToEntireOrder (ArrayList printJob, HashMap printLineDetail) {

        if (DataFunctions.isEmptyCollection(printJob)) {
            Logger.logMessage("The print job passed to TerminalThread.addRedirectedHeaderToEntireOrder can't be null or empty!", TT_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyMap(printLineDetail)) {
            Logger.logMessage("The print line detail passed to TerminalThread.addRedirectedHeaderToEntireOrder can't be null or empty!", TT_LOG, Logger.LEVEL.ERROR);
            return;
        }

        // add in the redirected header
        String[] redirectedHeader = new String[]{ENCRYPTED_EMPTY_LINE, ENCRYPTED_ASTERISKS, ENCRYPTED_REDIRECTED_DUE_TO, ENCRYPTED_ERROR_OR_TIMEOUT, ENCRYPTED_ASTERISKS, ENCRYPTED_EMPTY_LINE};
        for (int i = redirectedHeader.length -1; i >= 0; i--) {
            HashMap printLineDetailClone = DataFunctions.convertObjectToType(printLineDetail.clone(), HashMap.class);
            printLineDetailClone.put("LINEDETAIL", redirectedHeader[i]);
            printJob.add(0, printLineDetailClone);
        }

    }

    /**
     * add a header per suborder in the print job to indicate that it was redirected
     *
     * @param printJob {@link ArrayList} The print job we want to add the redirected header to.
     * @param printLineDetail {@link HashMap} The existing lines of text to print to for the print job.
     */
    @SuppressWarnings("unchecked")
    private void addRedirectedHeaderPerSuborder (ArrayList printJob, HashMap printLineDetail) {

        // keep track of the indicies at which there is a different OMS station within the print job details
        int currentKmsStationID = 0;
        int previousKmsStationID = 0;
        HashMap<Integer, Integer> stationStartIndicies = new HashMap<>();
        for (int i = 0; i < printJob.size(); i++) {
            HashMap printJobDetail = DataFunctions.convertObjectToType(printJob.get(i), HashMap.class);
            if ((!DataFunctions.isEmptyMap(printJobDetail)) && (HashMapDataFns.getIntVal(printJobDetail, "KMSSTATIONID") > 0)) {
                currentKmsStationID = HashMapDataFns.getIntVal(printJobDetail, "KMSSTATIONID");
                if ((previousKmsStationID == 0) || (previousKmsStationID != currentKmsStationID)) {
                    stationStartIndicies.put(currentKmsStationID, i);
                }
            }
            previousKmsStationID = currentKmsStationID;
        }

        // add in the redirected header for each OMS station on which the print job errored
        String[] redirectedHeader = new String[]{ENCRYPTED_EMPTY_LINE, ENCRYPTED_ASTERISKS, ENCRYPTED_REDIRECTED_DUE_TO, ENCRYPTED_ERROR_OR_TIMEOUT, ENCRYPTED_ASTERISKS, ENCRYPTED_EMPTY_LINE};
        if (!DataFunctions.isEmptyMap(stationStartIndicies)) {
            int shift = 0; // keep track of how much starting indices have been shifted due to adding the redirected header
            for (Map.Entry<Integer, Integer> e : stationStartIndicies.entrySet()) {
                int kmsStationID = e.getKey();
                int insertionIndex = e.getValue() + shift;
                for (int i = redirectedHeader.length - 1; i >= 0; i--) {
                    HashMap printLineDetailClone = DataFunctions.convertObjectToType(printLineDetail.clone(), HashMap.class);
                    printLineDetailClone.put("KMSSTATIONID", kmsStationID);
                    printLineDetailClone.put("LINEDETAIL", redirectedHeader[i]);
                    printJob.add(insertionIndex, printLineDetailClone);
                }
                shift += redirectedHeader.length;
            }
        }

    }

    /**
     * Set the time of the Calendar instance from the given queueTime String.
     *
     * @param queueTime {@link String} The time to set for the Calendar instance.
     * @return {@link Calendar} The Calendar instance.
     */
    @SuppressWarnings("UseOfObsoleteDateTimeApi")
    private Calendar parseQueueTime (String queueTime) {
        Calendar calendar = Calendar.getInstance();

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(QUEUE_DATETIME_FORMAT);
            calendar.setTime(simpleDateFormat.parse(queueTime));
        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage("There was a problem trying to set the Calendar time from the String " +
                    Objects.toString(queueTime, "NULL")+" in TerminalThread.parseQueueTime", TT_LOG, Logger.LEVEL.ERROR);
        }

        return calendar;
    }

    /**
     * Send print jobs to the printer host so they can be printed.
     *
     * @param printData {@link ArrayList} The print jobs we want to send to the printer host to be printed.
     */
    @SuppressWarnings({"OverlyComplexMethod", "TypeMayBeWeakened", "UseOfObsoleteDateTimeApi", "Convert2streamapi"})
    private void sendPrintDataToPrinterHost (ArrayList printData) {

        try {
            ArrayList queuedPrintData = null;

            // get the MAC address of the in-service printer host on the server
            String inServicePrinterHost = "";
            int terminalID = HashMapDataFns.getIntVal(((HashMap) ((ArrayList) printData.get(0)).get(0)), "TERMINALID");
            if (terminalID > 0) {
                inServicePrinterHost = getInServicePrinterHost(terminalID);
            }

            Logger.logMessage(String.format("The terminal %s reports that the in-service printer host is running on the " +
                    "terminal %s at %s and print jobs should be sent there in TerminalThread.sendDataToPrinterHost",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(inServicePrinterHost), "NULL"),
                    Objects.toString(LocalDateTime.now(), "NULL")), TT_LOG, Logger.LEVEL.TRACE);

            if (!inServicePrinterHost.isEmpty()) {
                if (inServicePrinterHost.equalsIgnoreCase(PeripheralsDataManager.getTerminalMacAddress())) {
                    // make the call to send print jobs to the active printer host locally
                    queuedPrintData = kitchenPrinterTerminal.getPeripheralsDataManager().kpPrint(printData);
                }
                else {
                    // make XML RPC to send the print jobs to the active printer host
                    String url = XmlRpcUtil.getInstance().buildURL(inServicePrinterHost, PeripheralsDataManager.getTerminalHostname(inServicePrinterHost));
                    String macAddress = PeripheralsDataManager.getTerminalMacAddress();
                    String hostname = PeripheralsDataManager.getPHHostname();
                    String methodName = "PeripheralsDataManager.kpPrint";
                    Object[] methodParams = new Object[]{printData};
                    Logger.logMessage(String.format("Making XMLRPC Request %s to the " +
                            "printer host at %s",
                            Objects.toString(methodName, "NULL"),
                            Objects.toString(url, "NULL")), TT_LOG, Logger.LEVEL.TRACE);
                    Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName,
                            methodParams);

                    // get the print jobs that were added to the PrinterQueueHandler's print queue to be printed
                    queuedPrintData = XmlRpcUtil.getInstance().parseArrayListXmlRpcResponse(xmlRpcResponse, methodName);
                }
            }

            // update the print status of the queued print data in the database
            for(Object a: queuedPrintData){
                ArrayList<ArrayList<HashMap>> singleJobHolder = new ArrayList<>();
                singleJobHolder.add((ArrayList<HashMap>) a);
                PrintJobStatusHandler.getInstance().updatePrintQueueStatusesInDB(singleJobHolder);
            }

        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage("There was a problem sending the print jobs to the printer host so they could be " +
                    "printed in TerminalThread.sendPrintDataToPrinterHost", TT_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Gets the MAC address {@link String} of the in-service printer host.</p>
     *
     * @param terminalID ID of the terminal to get the in-service printer host for.
     * @return The MAC address {@link String} of the in-service printer host.
     */
    @SuppressWarnings({"unchecked", "Convert2streamapi", "TypeMayBeWeakened", "OverlyComplexMethod"})
    public String getInServicePrinterHost (int terminalID) {
        String inServicePrinterHost = "";

        try {
            Logger.logMessage("About to get the in service printer host from the server in TerminalThread.getInServicePrinterHost", TT_LOG, Logger.LEVEL.DEBUG);
            String inServicePHOnServer = getInServicePrinterHostFromServer(terminalID);
            if (StringFunctions.stringHasContent(inServicePHOnServer)) {
                Logger.logMessage("Got the in service printer host from the server: " + inServicePHOnServer, TT_LOG, Logger.LEVEL.DEBUG);

                return inServicePHOnServer;
            }
            else {

                Logger.logMessage(String.format("The terminal %s is unable to determine the in-service printer host from the server in TerminalThread.getInServicePrinterHost",
                        Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), TT_LOG, Logger.LEVEL.DEBUG);

                // check if this terminal is a satellite terminal
                if (KitchenPrinterCommon.isTerminalSatellite(TT_LOG)) {
                    // get any printer hosts this satellite terminal could potentially send print jobs to
                    ArrayList<String> potentialPHMacAddresses = new ArrayList<>();
                    ArrayList<HashMap> queryRes =
                            KitchenPrinterTerminal.getInstance().getDataManager().parameterizedExecuteQuery("data.newKitchenPrinter.GetPotentialPrinterHostsForTerminal", new Object[]{terminalID}, TT_LOG, true);
                    if (!DataFunctions.isEmptyCollection(queryRes)) {
                        for (HashMap hm : queryRes) {
                            if ((!DataFunctions.isEmptyMap(hm)) && (StringFunctions.stringHasContent(HashMapDataFns.getStringVal(hm, "MACADDRESS")))) {
                                String potentialPHMac = HashMapDataFns.getStringVal(hm, "MACADDRESS");

                                Logger.logMessage("TerminalThread.getInServicePrinterHost: adding potential mac address " + potentialPHMac, TT_LOG, Logger.LEVEL.DEBUG);
                                potentialPHMacAddresses.add(HashMapDataFns.getStringVal(hm, "MACADDRESS"));
                            }
                        }
                    }

                    Logger.logMessage("Number of potential printer hosts: " + potentialPHMacAddresses.size(), TT_LOG, Logger.LEVEL.DEBUG);

                    if (!DataFunctions.isEmptyCollection(potentialPHMacAddresses)) {
                        boolean inServicePrinterHostFound = false;
                        int numberOfPotentialPrinterHosts = potentialPHMacAddresses.size();

                        while ((!inServicePrinterHostFound) && (numberOfPotentialPrinterHosts > 0)) {
                            int index = numberOfPotentialPrinterHosts - 1;
                            String macAddress = potentialPHMacAddresses.get(index);

                            String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                            Object[] args = new Object[]{PeripheralsDataManager.getPHHostname()};

                            Logger.logMessage("TerminalThread.getInServicePrinterHost: calling " + KitchenPrinterDataManagerMethod.GET_IN_SERVICE_PRINTER_HOST_ON_TERMINAL.getMethodName()
                                    + " using the XML RPC URL " + url + " and the mac address " + macAddress, TT_LOG, Logger.LEVEL.DEBUG);

                            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                                    KitchenPrinterDataManagerMethod.GET_IN_SERVICE_PRINTER_HOST_ON_TERMINAL.getMethodName(), args);
                            if (StringFunctions.stringHasContent(XmlRpcUtil.getInstance().parseStringXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.GET_IN_SERVICE_PRINTER_HOST_ON_TERMINAL.getMethodName()))) {
                                inServicePrinterHost = XmlRpcUtil.getInstance().parseStringXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.GET_IN_SERVICE_PRINTER_HOST_ON_TERMINAL.getMethodName());
                                inServicePrinterHostFound = true;
                            }
                            else {
                                numberOfPotentialPrinterHosts--;
                            }
                        }

                        if (StringFunctions.stringHasContent(inServicePrinterHost)) {
                            Logger.logMessage(String.format("The terminal %s is a satellite terminal and reports that the " +
                                    "in-service printer host is %s in TerminalThread.getInServicePrinterHost",
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                    Objects.toString(PeripheralsDataManager.getTerminalHostname(inServicePrinterHost), "NULL")), TT_LOG, Logger.LEVEL.DEBUG);
                        }
                        else {
                            Logger.logMessage(String.format("The terminal %s is a satellite terminal and can't determine the " +
                                    "in-service printer host in TerminalThread.getInServicePrinterHost",
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), TT_LOG, Logger.LEVEL.ERROR);
                        }
                    } else {
                        Logger.logMessage("TerminalThread.getInServicePrinterHost: no potential printer hosts have been found!", TT_LOG, Logger.LEVEL.ERROR);
                    }
                }
                else {
                    // get the MAC address of the terminal running the in-service printer host stored on this terminal
                    inServicePrinterHost = KitchenPrinterTerminal.getInstance().getActivePHMacAddress();
                    if (StringFunctions.stringHasContent(inServicePrinterHost)) {
                        Logger.logMessage(String.format("The terminal %s isn't a satellite terminal and reports that the " +
                                "in-service printer host is %s in TerminalThread.getInServicePrinterHost",
                                Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                Objects.toString(PeripheralsDataManager.getTerminalHostname(inServicePrinterHost), "NULL")), TT_LOG, Logger.LEVEL.DEBUG);
                    }
                    else {
                        Logger.logMessage(String.format("The terminal %s isn't a satellite terminal and can't determine the " +
                                "in-service printer host in TerminalThread.getInServicePrinterHost",
                                Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), TT_LOG, Logger.LEVEL.ERROR);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the in-service printer host for the " +
                    "terminal %s in TerminalThread.getInServicePrinterHost",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), TT_LOG, Logger.LEVEL.ERROR);
        }

        return inServicePrinterHost;
    }

    /**
     * <p>Makes an XML RPC call to the server to get the MAC address {@link String} of the in-service printer host.</p>
     *
     * @param terminalID ID of the terminal to get the in-service printer host for.
     * @return The MAC address {@link String} of the in-service printer host from the server.
     */
    private String getInServicePrinterHostFromServer (int terminalID) {
        String inServicePrinterHost = "";

        Logger.logMessage("Getting the in service printer host from the server", TT_LOG, Logger.LEVEL.DEBUG);

        try {
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            Object[] args = new Object[]{PeripheralsDataManager.getPHHostname(), terminalID};
            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, KitchenPrinterDataManagerMethod.GET_IN_SERVICE_PRINTER_HOST.getMethodName(), args);

            if (StringFunctions.stringHasContent(XmlRpcUtil.getInstance().parseStringXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.GET_IN_SERVICE_PRINTER_HOST.getMethodName()))) {
                inServicePrinterHost = XmlRpcUtil.getInstance().parseStringXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.GET_IN_SERVICE_PRINTER_HOST.getMethodName());
            }
        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the in-service printer host on the server for the " +
                    "terminal %s in TerminalThread.getInServicePrinterHostFromServer",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), TT_LOG, Logger.LEVEL.ERROR);
        }

        Logger.logMessage("In service printer host found to be: " + inServicePrinterHost, TT_LOG, Logger.LEVEL.DEBUG);

        return inServicePrinterHost;
    }

    /**
     * Update the time the print job was printed in the local database's QC_PAPrinterQueueDetail table.
     *
     * @param printedDTM {@link Date} Updated time the print job was printed.
     * @param paPrinterQueueID {@link String} PAPrinterQueueID of the print job to update in the local database.
     * @param printerID {@link String} PrinterID of the print job to update in the local database.
     */
    @SuppressWarnings("UseOfObsoleteDateTimeApi")
    private void updatePrintedDTM (Date printedDTM, String paPrinterQueueID, String printerID) {

        try {
            int queryRes = kitchenPrinterTerminal.getDataManager().parameterizedExecuteNonQuery(
                    "data.newKitchenPrinter.SetPrintedDTM",
                    new Object[]{printedDTM, paPrinterQueueID, printerID}, TT_LOG);
            if (queryRes < 0) {
                Logger.logMessage("Unable to update the time the print job with PAPrinterQueueID " +
                        Objects.toString(paPrinterQueueID, "NULL")+" was printed in the local database's " +
                        "QC_PAPrinterQueueDetail table because the query to do so failed", TT_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage("There was a problem trying to update the status of the print job with PAPrinterQueueID " +
                    Objects.toString(paPrinterQueueID, "NULL")+" to "+printedDTM.toString()+" in the local database's " +
                    "QC_PAPrinterQueueDetail table in TerminalThread.updatePrintedDTM");
        }

    }

    /**
     * Update the status of print jobs in the local database at the given PAPrinterQueueDetailIDs.
     *
     * @param idsToUpdate {@link String} PAPrinterQueueDetailID of records to update in the QC_PAPrinterQueue and
     *         QC_PAPrinterQueueDetail tables.
     * @param newStatus {@link PrintJobStatusType} Type of the status we want to change the print jobs to.
     */
    private void updatePrintJobStatus (String idsToUpdate, PrintJobStatusType newStatus) {

        try {
            String[] queries = new String[]{"data.newKitchenPrinter.SetPAPrinterQueueStatusByPrinterQueueDetailID",
                    "data.newKitchenPrinter.SetPAPrinterQueueDetailStatus"};
            for (String query : queries) {
                if (!idsToUpdate.isEmpty()) {
                    int queryRes = kitchenPrinterTerminal.getDataManager().serializeUpdate(query,
                            new Object[]{newStatus.getTypeID(), idsToUpdate}, null, TT_LOG, 0, false);
                    if (queryRes < 0) {
                        Logger.logMessage("There was a problem trying to update the printer queue records in the local " +
                                "database with the query "+Objects.toString(query, "NULL")+" in " +
                                "TerminalThread.updatePrintJobStatus", TT_LOG, Logger.LEVEL.ERROR);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage("There was a problem trying to update the status for the PAPrinterQueueDetailIDs "
                    +Objects.toString(idsToUpdate, "NULL")+" to "+newStatus.name()+" in " +
                    "TerminalThread.updatePrintJobStatus", TT_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Creates and starts a Thread using the TerminalThread Runnable.
     *
     */
    public void startTerminalThread () {

        try {
            Thread TerminalThread = new Thread(this);
            TerminalThread.start();
            Logger.logMessage("The TerminalThread has started successfully in TerminalThread,startTerminalThread",
                    TT_LOG, Logger.LEVEL.TRACE);
        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage("There was a problem trying to start the TerminalThread in " +
                    "TerminalThread.startTerminalThread", TT_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Stops the TerminalThread.
     *
     */
    public void stopTerminalThread () {

        try {
            // run through Thread shutdown routine
            isShuttingDown = true;

            int waitCount = 0;
            while (!isTerminated) {
                waitCount++;
                // wait a second
                Thread.sleep(1000L);
                if (waitCount >= 3) {
                    Logger.logMessage("The TerminalThread didn't stop within the specified wait time, it " +
                            "is being interrupted now in TerminalThread.stopTerminalThread", TT_LOG,
                            Logger.LEVEL.TRACE);
                    // interrupt the PrinterQueueHandler Thread
                    Thread.getAllStackTraces().keySet().forEach((thread) -> {
                        if (thread.getName().equalsIgnoreCase("KitchenPrinter-TerminalThread")) {
                            thread.interrupt();
                        }
                    });
                }
            }

            Logger.logMessage("The TerminalThread has been stopped", TT_LOG, Logger.LEVEL.IMPORTANT);
        }
        catch (Exception e) {
            Logger.logException(e, TT_LOG);
            Logger.logMessage("There was a problem trying to stop the TerminalThread in " +
                    "TerminalThread.stopTerminalThread", TT_LOG, Logger.LEVEL.ERROR);
        }

    }

}

