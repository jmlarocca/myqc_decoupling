package com.mmhayes.common.kitchenPrinters;

/*
 $Author: ecdyer $: Author of last commit
 $Date: 2019-11-15 10:01:40 -0500 (Fri, 15 Nov 2019) $: Date of last commit
 $Rev: 43721 $: Revision of last commit
 Notes: Handles terminal handoff and tracking of terminal print jobs to the active printer host.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.xmlapi.XmlRpcUtil;
import org.apache.commons.lang.math.NumberUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Singleton class responsible to starting the terminal or server thread and tracking of print jobs to the active
 * printer host.
 *
 */
public class KitchenPrinterTerminal {

    // the only instance of a KitchenPrinterTerminal for this WAR
    private static volatile KitchenPrinterTerminal kitchenPrinterTerminalInstance = null;

    // log file specific to the KitchenPrinterTerminal
    private static final String KPT_LOG = "KP_KitchenPrinterTerminal.log";

    // variables within the KitchenPrinterTerminal scope
    private ServerThread serverThread;
    private TerminalThread terminalThread;
    private PrinterHost printerHost;
    private volatile String activePHMacAddress;
    private volatile LocalDateTime timePHBecameActive;
    private ConcurrentHashMap<String, PrinterHostStatus> printerHostStatusTracker = new ConcurrentHashMap<>();

    // check what has been instantiated
    private boolean hasServerThreadStarted = false;
    private boolean hasTerminalThreadStarted = false;
    private boolean hasPrinterHostStarted = false;

    // variables needed for KitchenPrinterTerminal instantiation
    private DataManager dataManager;
    private PeripheralsDataManager peripheralsDataManager;

    /**
     * Private constructor for a KitchenPrinterTerminal.
     *
     * @param dataManager {@link DataManager} DataManager to be used by this terminal.
     * @param peripheralsDataManager {@link PeripheralsDataManager} PeripheralsDataManager to be used by this terminal.
     */
    private KitchenPrinterTerminal (DataManager dataManager, PeripheralsDataManager peripheralsDataManager) {
        this.dataManager = dataManager;
        this.peripheralsDataManager = peripheralsDataManager;

        try {
            Logger.logMessage(String.format("The terminal %s %s a satellite terminal.",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString((isTerminalSatellite() ? "is" : "isn't"), "NULL")), KPT_LOG, Logger.LEVEL.IMPORTANT);

            // if the terminal is configured as the server in the app.properties file and there are online ordering
            // terminals enabled in the database then the ServerThread should be started, otherwise the TerminalThread
            // should be started
            if (isThisTheServer()) {
                if (shouldServerCheckForOnlineOrders()) {
                    // start the ServerThread
                    serverThread = ServerThread.init(this);
                    serverThread.startServerThread();
                    hasServerThreadStarted = true;
                }
            }
            else {
                // start the TerminalThread
                terminalThread = TerminalThread.init(this);
                terminalThread.startTerminalThread();
                hasTerminalThreadStarted = true;
            }

            // if the terminal has a printer host configured to run it then create it
            if (doesThisTerminalHaveAPrinterHost()) {
                printerHost = PrinterHost.init(this, PeripheralsDataManager.getPHPrinters());
                hasPrinterHostStarted = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage("A problem occurred in the KitchenPrinterTerminal constructor", KPT_LOG, Logger.LEVEL.ERROR);
        }
    }

    /**
     * Get the only instance of KitchenPrinterTerminal for this WAR.
     *
     * @return {@link KitchenPrinterTerminal} The instance of KitchenPrinterTerminal for this WAR.
     */
    public static KitchenPrinterTerminal getInstance () {

        if (kitchenPrinterTerminalInstance == null) {
            throw new RuntimeException("The KitchenPrinterTerminal must be instantiated before trying to obtain its " +
                    "instance.");
        }

        return kitchenPrinterTerminalInstance;
    }

    /**
     * Instantiate and return the only instance of KitchenPrinterTerminal for this WAR.
     *
     * @param dataManager {@link DataManager} DataManager to be used by this terminal.
     * @param peripheralsDataManager {@link PeripheralsDataManager} PeripheralsDataManager to be used by this terminal.
     * @return {@link KitchenPrinterTerminal} The instance of KitchenPrinterTerminal for this WAR.
     */
    public static synchronized KitchenPrinterTerminal init (DataManager dataManager,
                                                            PeripheralsDataManager peripheralsDataManager) {

        try {
            if (kitchenPrinterTerminalInstance != null) {
                throw new RuntimeException("The KitchenPrinterTerminal has already been instantiated and may not be " +
                        "instantiated again.");
            }

            // create the instance
            kitchenPrinterTerminalInstance = new KitchenPrinterTerminal(dataManager, peripheralsDataManager);
            Logger.logMessage("The instance of KitchenPrinterTerminal has been instantiated", KPT_LOG, Logger.LEVEL.TRACE);
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage("There was a problem trying to instantiate and return the only instance of " +
                    "KitchenPrinterTerminal for this WAR in KitchenPrinterTerminal.init", KPT_LOG, Logger.LEVEL.ERROR);
        }

        return kitchenPrinterTerminalInstance;
    }

    /**
     * Check if this terminal was configured as the server in the app.properties file.
     *
     * @return Whether or not this terminal is the server.
     */
    public boolean isThisTheServer () {
        boolean isServer = false;

        try {
            isServer = (MMHProperties.getAppSetting("site.application.offline").compareToIgnoreCase("yes") != 0);

            if (!isServer) {
                Logger.logMessage("The ServerThread will not start on the terminal because the app property " +
                        "site.application.offline is either missing or set to yes", KPT_LOG, Logger.LEVEL.TRACE);
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage("There was a problem trying to determine whether or not the terminal has been designated " +
                    "as the server in the app.properties file in KitchenPrinterTerminal.isThisTheServer", KPT_LOG,
                    Logger.LEVEL.ERROR);
        }

        return isServer;
    }

    /**
     * Checks if there are any online ordering terminals that are enabled in the database and if there are the server
     * should check for online orders.
     *
     * @return Whether or not the server should check for online orders.
     */
    private boolean shouldServerCheckForOnlineOrders () {
        boolean serverShouldCheckForOnlineOrders = false;

        try {
            if (dataManager != null) {
                // query the database to check if there are any online ordering terminals enabled
                Object terminalID = dataManager.parameterizedExecuteScalar(
                        "data.kitchenPrinter.checkForOnlineOrderingTerminals", new Object[]{}, KPT_LOG);

                if (terminalID != null) {
                    serverShouldCheckForOnlineOrders = true;
                }
                else {
                    Logger.logMessage("The ServerThread will not start on the terminal because there were no " +
                            "online ordering terminals that were enabled", KPT_LOG, Logger.LEVEL.TRACE);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage("There was a problem trying to determine whether or no the terminal should check for " +
                    "online orders in KitchenPrinterTerminal.shouldServerCheckForOnlineOrders", KPT_LOG,
                    Logger.LEVEL.ERROR);
        }

        return serverShouldCheckForOnlineOrders;
    }

    /**
     * Check whether a printer host has been configured to run on the terminal.
     *
     * @return Whether or not this terminal should have a printer host running on it.
     */
    private boolean doesThisTerminalHaveAPrinterHost () {
        boolean terminalHasPrinterHost = false;

        try {
            if (dataManager != null) {
                ArrayList printerHosts = dataManager.parameterizedExecuteQuery(
                        "data.newKitchenPrinter.GetPrintControllerIdByMacAddress",
                        new Object[]{PeripheralsDataManager.getTerminalMacAddress()}, KPT_LOG, true);
                if ((printerHosts != null) && (!printerHosts.isEmpty())) {
                    terminalHasPrinterHost = true;
                    Logger.logMessage("The terminal with hostname " +
                            Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL") + " has a printer host that is configured to run on it", KPT_LOG, Logger.LEVEL.TRACE);
                }
                else {
                    Logger.logMessage("The terminal with hostname " +
                            Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")+" doesn't have a " +
                            "printer host that is configured to run on it", KPT_LOG, Logger.LEVEL.TRACE);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage("There was a problem trying to determine whether or not the terminal should have a " +
                    "printer host running on it in KitchenPrinterTerminal.doesThisTerminalHaveAPrinterHost", KPT_LOG,
                    Logger.LEVEL.ERROR);
        }

        return terminalHasPrinterHost;
    }

    /**
     * Called by PeripheralsDataManager.kpPrint which was called through XML RPC to send print jobs to the
     * PrinterQueueHandler's print queue.
     *
     * @param printData {@link ArrayList} The print jobs we want to send to the PrinterQueueHandler's print queue.
     * @return {@link ArrayList} The print jobs that have been successfully added to the PrinterQueueHandler's print queue.
     */
    public ArrayList queuePrintData (ArrayList printData) {
        Logger.logMessage("KitchenPrinterTerminal.queuePrintData called.", "KMS.log", Logger.LEVEL.TRACE);
        try {
            if (printerHost != null) {
                return printerHost.queuePrintData(printData);
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage("There was a problem trying to send print jobs to the PrinterQueueHandler's print queue " +
                    "in KitchenPrinterTerminal.queuePrintData", KPT_LOG, Logger.LEVEL.ERROR);
        }

        return new ArrayList();
    }

    /**
     * Gets the transaction details for an online order from the server so they can be inserted into the local database.
     *
     * @param paTransactionID ID of the online order's transaction.
     * @return {@link ArrayList} The transaction from the online order on the server.
     */
    public ArrayList getKitchenPrinterDataForOrderFromServer (int paTransactionID) {
        ArrayList kpTransactionData = new ArrayList();

        try {
            // make XML RPC call to get the transaction data for an online order on the server
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            String methodName = "PeripheralsDataManager.kpGetTranData";
            Object[] methodParams = new Object[]{paTransactionID};

            Logger.logMessage("KitchenPrinterTerminal.getKitchenPrinterDataForOrderFromServer: url=" + url + ", macAddress=" + macAddress
                    + ", hostname=" + hostname + ", methodName=" + methodName + ", paTransactionID= " + paTransactionID, KPT_LOG, Logger.LEVEL.DEBUG);

            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName,
                    methodParams);

            if ((XmlRpcUtil.getInstance().parseArrayListXmlRpcResponse(xmlRpcResponse, methodName) != null)
                    && (!XmlRpcUtil.getInstance().parseArrayListXmlRpcResponse(xmlRpcResponse, methodName).isEmpty())) {
                kpTransactionData = XmlRpcUtil.getInstance().parseArrayListXmlRpcResponse(xmlRpcResponse, methodName);
            } else {
                Logger.logMessage("KitchenPrinterTerminal.getKitchenPrinterDataForOrderFromServer ERROR: unable to parse XML RPC response for PATransactionID " + paTransactionID, KPT_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage("There was a problem trying to get the transaction data on the server for the online " +
                    "order with a PATransactionID of "+paTransactionID+" in " +
                    "KitchenPrinterTerminal.getKitchenPrinterDataForOrderFromServer", KPT_LOG, Logger.LEVEL.ERROR);
        }

        return kpTransactionData;
    }

    /**
     * Get the print job expiration time in minutes from the app.properties file, if the property can't be found or is
     * invalid a default of 30 minutes is used.
     *
     * @return The number of minutes before the print job expires.
     */
    public int getPrintJobExpirationInMinutes () {
        int printJobExpirationInMinutes = 30;

        try {
            if (NumberUtils.isNumber(MMHProperties.getAppSetting("site.kitchenPrinter.printJobExpirationInMinutes"))) {
                printJobExpirationInMinutes =
                        Integer.parseInt(MMHProperties.getAppSetting("site.kitchenPrinter.printJobExpirationInMinutes"));
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage("There was a problem trying to determine the print job expiration time in minutes from " +
                    "the app.properties file in KitchenPrinterTerminal.getPrintJobExpirationInMinutes", KPT_LOG,
                    Logger.LEVEL.ERROR);
        }

        return printJobExpirationInMinutes;
    }


    /**
     * Get how frequently in seconds we should poll the database for print jobs from the app.properties file, if the
     * property can't be found or is invalid a default of 5 seconds is used.
     *
     * @return The number of minutes before the print job expires.
     */
    public int dbPollingIntervalInSeconds () {
        int dbPollingIntervalInSeconds = 5;

        try {
            if (NumberUtils.isNumber(MMHProperties.getAppSetting("site.kitchenPrinter.pollingRateInSeconds"))) {
                dbPollingIntervalInSeconds =
                        Integer.parseInt(MMHProperties.getAppSetting("site.kitchenPrinter.pollingRateInSeconds"));
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage("There was a problem trying to determine how frequently to poll the database for print " +
                    "jobs from the app.properties file in KitchenPrinterTerminal.dbPollingIntervalInSeconds",
                    KPT_LOG, Logger.LEVEL.ERROR);
        }

        return dbPollingIntervalInSeconds;
    }

    /**
     * <p>Makes XML RPC call to the server to get the in-service printer host.</p>
     *
     * @return The MAC address {@link String} of the in-service printer host.
     */
    public String getInServicePH () {
        String inSvcPHMac = "";

        try {
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            String methodName = "PeripheralsDataManager.getInServicePrinterHost";
            Object[] methodParams = new Object[]{macAddress};
            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName, methodParams);

            if (StringFunctions.stringHasContent(XmlRpcUtil.getInstance().parseStringXmlRpcResponse(xmlRpcResponse, methodName))) {
                inSvcPHMac = XmlRpcUtil.getInstance().parseStringXmlRpcResponse(xmlRpcResponse, methodName);
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage(String.format("The terminal with a hostname of %s was unable to get the in-service " +
                    "printer host from the server in KitchenPrinterTerminal.getInServicePH",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), KPT_LOG, Logger.LEVEL.ERROR);
        }

        return inSvcPHMac;
    }

    /**
     * <p>Utility method to check whether or not this terminal is a satellite terminal.</p>
     *
     * @return Whether or not this terminal is a satellite terminal.
     */
    public boolean isTerminalSatellite () {
        boolean isTerminalSatellite = false;

        try {
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            Logger.logMessage("Checking isTerminalSatellite with MAC Address of " + macAddress, KPT_LOG, Logger.LEVEL.DEBUG);
            if (StringFunctions.stringHasContent(macAddress)) {
                Object queryRes = dataManager.parameterizedExecuteScalar("data.newKitchenPrinter.GetIsTerminalSatellite", new Object[]{macAddress});
                if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString())) && (Integer.parseInt(queryRes.toString()) <= 0)) {
                    isTerminalSatellite = true;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the terminal %s " +
                    "is a satellite terminal in KitchenPrinterTerminal.isTerminalSatellite",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), KPT_LOG, Logger.LEVEL.ERROR);
        }

        return isTerminalSatellite;
    }

    /**
     * Setter for the KitchenPrinterTerminal's activePHMacAddress field.
     *
     * @param macAddress {@link String} The MAC address of the active printer host, as seen by this WAR.
     */
    public void setActivePHMacAddress (String macAddress) {
        this.activePHMacAddress = macAddress;
    }

    /**
     * Getter for the KitchenPrinterTerminal's activePHMacAddress field.
     *
     * @return {@link String} The MAC address of the active printer host, as seen by this WAR.
     */
    public String getActivePHMacAddress () {
        return activePHMacAddress;
    }

    /**
     * Setter for the KitchenPrinterTerminal's timePHBecameActive field.
     *
     * @param timePHBecameActive {@link Date} The time the active printer host became active, as seen by this WAR.
     */
    @SuppressWarnings("UseOfObsoleteDateTimeApi")
    public void setTimePHBecameActive (LocalDateTime timePHBecameActive) {
        this.timePHBecameActive = timePHBecameActive;
    }

    /**
     * Getter for the KitchenPrinterTerminal's timePHBecameActive field.
     *
     * @return {@link Date} The time the active printer host became active, as seen by this WAR.
     */
    @SuppressWarnings("UseOfObsoleteDateTimeApi")
    public LocalDateTime getTimePHBecameActive () {
        return timePHBecameActive;
    }

    /**
     * Getter for the KitchenPrinterTerminal's dataManager field.
     *
     * @return {@link DataManager} The KitchenPrinterTerminal's DataManager.
     */
    public DataManager getDataManager () {
        return dataManager;
    }

    /**
     * Getter for the KitchenPrinterTerminal's peripheralsDataManager field.
     *
     * @return {@link PeripheralsDataManager} The KitchenPrinterTerminal's PeripheralsDataManager.
     */
    public PeripheralsDataManager getPeripheralsDataManager () {
        return peripheralsDataManager;
    }

    /**
     * Getter for the KitchenPrinterTerminal's hasServerThreadStarted field.
     *
     * @return {@link PeripheralsDataManager} The KitchenPrinterTerminal's hasServerThreadStarted field.
     */
    public boolean getHasServerThreadStarted () {
        return hasServerThreadStarted;
    }

    /**
     * Getter for the KitchenPrinterTerminal's hasTerminalThreadStarted field.
     *
     * @return {@link PeripheralsDataManager} The KitchenPrinterTerminal's hasTerminalThreadStarted field.
     */
    public boolean getHasTerminalThreadStarted () {
        return hasTerminalThreadStarted;
    }

    /**
     * Getter for the KitchenPrinterTerminal's hasPrinterHostStarted field.
     *
     * @return {@link PeripheralsDataManager} The KitchenPrinterTerminal's hasPrinterHostStarted field.
     */
    public boolean getHasPrinterHostStarted () {
        return hasPrinterHostStarted;
    }

    /**
     * Adds an entry to the printer host status tracker with the given MAC address and printer host status.
     *
     * @param macAddress {@link String} MAC address of the printer host we are adding to the printer host status tracker.
     * @param printerHostStatus {@link PrinterHostStatus} Status of the printer host we are adding to the printer host
     *         status tracker.
     */
    public void addToPrinterHostStatusTracker (String macAddress, PrinterHostStatus printerHostStatus) {

        try {
            printerHostStatusTracker.put(macAddress, printerHostStatus);
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage("There was a problem trying to add the printer host status of " +
                    Objects.toString(printerHostStatus.getPrinterHostStatus(), "NULL")+" to the " +
                    "KitchenPrinterTerminal's printer host status tracker for the printer host running on the machine " +
                    "with a hostname of "+Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL") +
                    " in KitchenPrinterTerminal.addToPrinterHostStatusTracker", KPT_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Adds an existing mapping of a printer host MAC address and that printer host's status to the printer host status
     * tracker.
     *
     * @param printerHostStatusMap {@link ConcurrentHashMap<String, PrinterHostStatus>} The existing mapping we are
     *         adding to the printer host status tracker.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public void addToPrinterHostStatusTracker (ConcurrentHashMap<String, PrinterHostStatus> printerHostStatusMap) {

        try {
            printerHostStatusTracker.putAll(printerHostStatusMap);
        }
        catch (Exception e) {
            Logger.logException(e, KPT_LOG);
            Logger.logMessage("There was a problem trying to add the map of printer host status information to the " +
                    "printer host status tracker in KitchenPrinterTerminal.addToPrinterHostStatusTracker", KPT_LOG,
                    Logger.LEVEL.ERROR);
        }

    }

    /**
     * Getter for the KitchenPrinterTerminal's printerHostStatusTracker field.
     *
     * @return {@link ConcurrentHashMap<String, PrinterHostStatus>} The KitchenPrinterTerminal's printerHostStatusTracker.
     */
    public ConcurrentHashMap<String, PrinterHostStatus> getPrinterHostStatusTracker () {
        return printerHostStatusTracker;
    }

    /**
     * Getter for the KitchenPrinterTerminal's KPT_LOG field.
     *
     * @return {@link String} The name of the log file for the KitchenPrinterTerminal.
     */
    public String getKptLog () {
        return KPT_LOG;
    }
}