package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: nyu $: Author of last commit
    $Date: 2019-08-16 17:08:55 -0400 (Fri, 16 Aug 2019) $: Date of last commit
    $Rev: 40901 $: Revision of last commit
    Notes: Object to maintain the status of a print job.
*/

import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.utils.Logger;

import java.util.Calendar;

/**
 * Class to maintain the print job status type and any messages related to a print job.
 *
 */
@Deprecated
public class PrintJobStatus {

    // variables within the PrintJobStatus scope
    private PrintJobStatusType printJobStatusType;
    private Calendar completedTime;
    private String statusMsg;

    /**
     * Constructor for a PrintJobStatus.
     *
     * @param printJobStatusType {@link PrintJobStatusType} The type of print job status the print job has.
     * @param statusMsg {@link String} A message related to the print job.
     */
    public PrintJobStatus (PrintJobStatusType printJobStatusType, String statusMsg) {
        this.printJobStatusType = printJobStatusType;
        this.completedTime = Calendar.getInstance();
        this.statusMsg = statusMsg;
    }

    /**
     * Update a the printJobStatusType, completedTime, and statusMsg fields of a PrintJobStatus.
     *
     * @param printJobStatusType {@link PrintJobStatusType} The new printJobStatusType for the PrintJobStatus.
     * @param completedTime {@link Calendar} The new completedTime for the PrintJobStatus.
     * @param statusMsg {@link String} The new statusMsg for the PrintJobStatus.
     */
    public void update (PrintJobStatusType printJobStatusType, Calendar completedTime, String statusMsg) {

        try {
            setPrintJobStatusType(printJobStatusType);
            setCompletedTime(completedTime);
            setStatusMsg(statusMsg);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to update the PrintJobStatus in PrintJobStatus.update",
                    Logger.LEVEL.ERROR);
        }

    }

    /**
     * Get the ID of the PrintJobStatusType for the print job.
     *
     * @return The ID of the PrintJobStatusType for the print job.
     */
    public int getStatusID () {
        int printJobStatusTypeID = 0;

        try {
            if (printJobStatusType != null) {
                printJobStatusTypeID = printJobStatusType.getTypeID();
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to get the print job status type ID for the print job in " +
                    "PrintJobStatus.getStatusID", Logger.LEVEL.ERROR);
        }

        return printJobStatusTypeID;
    }

    /**
     * Setter for the PrintJobStatus' printJobStatusType field.
     *
     * @param printJobStatusType {@link PrintJobStatusType} The type of print job status the print job has.
     */
    public void setPrintJobStatusType (PrintJobStatusType printJobStatusType) {
        this.printJobStatusType = printJobStatusType;
    }

    /**
     * Getter for the PrintJobStatus' printJobStatusType field.
     *
     * @return {@link PrintJobStatusType} The type of print job status the print job has.
     */
    public PrintJobStatusType getPrintJobStatusType () {
        return printJobStatusType;
    }

    /**
     * Setter for the PrintJobStatus' completedTime field.
     *
     * @param completedTime {@link Calendar} The time the print job has been completed.
     */
    public void setCompletedTime (Calendar completedTime) {
        this.completedTime = completedTime;
    }

    /**
     * Getter for the PrintJobStatus' completedTime field.
     *
     * @return {@link Calendar} The time the print job has been completed.
     */
    public Calendar getCompletedTime () {
        return completedTime;
    }

    /**
     * Setter for the PrintJobStatus' statusMsg field.
     *
     * @param statusMsg {@link String} A message related to the print job.
     */
    public void setStatusMsg (String statusMsg) {
        this.statusMsg = statusMsg;
    }

    /**
     * Getter for the PrintJobStatus' statusMsg field.
     *
     * @return {@link String} A message related to the print job.
     */
    public String getStatusMsg () {
        return statusMsg;
    }
}


