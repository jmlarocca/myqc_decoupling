package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-10-26 12:07:11 -0400 (Mon, 26 Oct 2020) $: Date of last commit
    $Rev: 12972 $: Revision of last commit
    Notes: Checks whether or not a NCR or SNBC printer is still printing a sale receipt.
*/

import com.mmhayes.common.kitchenapi.JPosReceiptPrinter;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import jpos.POSPrinter;

import java.util.Objects;
import java.util.concurrent.Callable;

public class NCR_SNBC_PrinterCheck implements Callable<Boolean> {

    // private member variables of a NCR_SNBC_PrinterCheck
    private JPosReceiptPrinter printer = null;
    private POSPrinter posPrinter = null;
    private String log = null;
    private final long ONE_SECOND = 1000L;

    /**
     * <p>Constructor for a {@link NCR_SNBC_PrinterCheck}.</p>
     *
     * @param printer The {@link JPosReceiptPrinter} representation of the NCR or SNBC printer.
     * @param posPrinter The {@link POSPrinter} representation of the NCR or SNBC printer.
     * @param log The path {@link String} of the log file to log information related to this method to.
     */
    public NCR_SNBC_PrinterCheck (JPosReceiptPrinter printer, POSPrinter posPrinter, String log) {

        // validate the log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to the NCR_SNBC_PrinterCheck constructor can't be null or empty!", Logger.LEVEL.ERROR);
            return;
        }

        // validate the JPOS printer
        if (printer == null) {
            Logger.logMessage("The JPosReceiptPrinter passed to the NCR_SNBC_PrinterCheck constructor can't be null!", log, Logger.LEVEL.ERROR);
            return;
        }

        // validate the POS printer
        if (posPrinter == null) {
            Logger.logMessage("The POSPrinter passed to the NCR_SNBC_PrinterCheck constructor can't be null!", log, Logger.LEVEL.ERROR);
            return;
        }

        this.printer = printer;
        this.posPrinter = posPrinter;
        this.log = log;
    }

    /**
     * <p>Overridden call method for a {@link NCR_SNBC_PrinterCheck}.</p>
     *
     * @return Whether or not a NCR or SNBC printer is still printing a sale receipt.
     * @throws Exception
     */
    @Override
    public Boolean call () throws Exception {

        // validate the log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file can't be null or empty in NCR_SNBC_PrinterCheck.call, pass a valid log file to the NCR_SNBC_PrinterCheck constructor, now returning false!", Logger.LEVEL.ERROR);
            return false;
        }

        // validate the JPOS printer
        if (printer == null) {
            Logger.logMessage("The JPosReceiptPrinter can't be null in NCR_SNBC_PrinterCheck.call, pass a valid JPosReceiptPrinter " +
                    "to the NCR_SNBC_PrinterCheck constructor, now returning false!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // validate the POS printer
        if (posPrinter == null) {
            Logger.logMessage("The POSPrinter can't be null in NCR_SNBC_PrinterCheck.call, pass a valid POSPrinter to the " +
                    "NCR_SNBC_PrinterCheck constructor, now returning false!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // make an attempt once every 10 seconds to see if the sale receipt is done printing
        int secondsWaited = 0;
        int attempts = 10;
        for (int i = 0; i < attempts; i++) {
            int printerState = posPrinter.getState();
            // check if the sale receipt finished printing
            if (printer.getPrinterStateMsg(printerState).contains("Device is Idle")) {
                return true;
            }
            else {
                // wait a second before checking if the sale receipt has finished printing
                Thread.sleep(ONE_SECOND);
                secondsWaited++;
                Logger.logMessage(String.format("%s second%s passed waiting for the NCR or SNBC printer to finish printing sale receipts.",
                        Objects.toString(secondsWaited, "N/A"),
                        Objects.toString(secondsWaited == 1 ? " has" : "s have", "N/A")), log, Logger.LEVEL.TRACE);
            }
        }

        // we've waited 10 seconds and the sale receipt hasn't finished printing, throw an Exception
        throw new Exception("More than 10 seconds have passed and the NCR or SNBC printer hasn't indicated that it has completed printing a sale receipt!");
    }

}