package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-03-23 12:30:35 -0400 (Mon, 23 Mar 2020) $: Date of last commit
    $Rev: 11156 $: Revision of last commit
    Notes: Custom XML RPC serializer that will enable methods to return a {@link QCKitchenPrinter} through a XML RPC call.
*/

import com.mmhayes.common.utils.Logger;
import redstone.xmlrpc.XmlRpcCustomSerializer;
import redstone.xmlrpc.XmlRpcException;
import redstone.xmlrpc.XmlRpcMessages;
import redstone.xmlrpc.XmlRpcSerializer;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.Writer;

/**
 * <p>Custom XML RPC serializer that will enable methods to return a {@link QCKitchenPrinter} through a XML RPC call.</p>
 *
 */
public class QCKitchenPrinterSerializer implements XmlRpcCustomSerializer {

    /**
     * <p>Overridden getSupportedClass method for a {@link QCKitchenPrinterSerializer}.</p>
     *
     * @return The supported {@link Class} that the serializer knows how to handle.
     */
    @Override
    public Class getSupportedClass () {
        return QCKitchenPrinter.class;
    }

    /**
     * <p>Overridden serialize method for a {@link QCKitchenPrinterSerializer}.</p>
     *
     * @param value The {@link Object} to serialize.
     * @param writer The {@link Writer} to place the serialized data.
     * @param xmlRpcSerializer The built in {@link XmlRpcSerializer} used by the "server".
     * @throws XmlRpcException
     * @throws IOException
     */
    @Override
    public void serialize (Object value, Writer writer, XmlRpcSerializer xmlRpcSerializer) throws XmlRpcException, IOException {
        writer.write("<struct>");

        try {
            BeanInfo qcKitchenPrinterInfo = Introspector.getBeanInfo(value.getClass(), Object.class);
            PropertyDescriptor[] descriptors = qcKitchenPrinterInfo.getPropertyDescriptors();

            if (descriptors.length > 0) {
                for (PropertyDescriptor descriptor : descriptors) {
                    Object propertyValue = descriptor.getReadMethod().invoke(value, (Object[]) null);
                    if (propertyValue != null) {
                        writer.write("<member>");
                        writer.write("<name>");
                        writer.write(descriptor.getDisplayName());
                        writer.write("</name>");
                        xmlRpcSerializer.serialize(propertyValue, writer);
                        writer.write("</member>");
                    }
                }
            }
            else {
                Logger.logMessage("No property descriptors found for the QCKitchenPrinter in QCKitchenPrinterSerializer.serialize", Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            throw new XmlRpcException(XmlRpcMessages.getString("QCKitchenPrinterSerializer.SerializationError"), e);
        }

        writer.write("</struct>");
    }

}