package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2019-11-05 15:48:11 -0500 (Tue, 05 Nov 2019) $: Date of last commit
    $Rev: 43280 $: Revision of last commit
    Notes: Object representation of a record in the QC_Printer table.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.printing.PrintStatus;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.Formatter.KitchenPrinterReceiptFormatter;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.Renderer.CommonRenderer;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.peripherals.printing.MMHPosPrinter;
import com.mmhayes.common.utils.StringFunctions;
import jpos.POSPrinter;
import jpos.POSPrinterConst;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.math.NumberUtils;
import redstone.xmlrpc.XmlRpcStruct;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to represent a either a physical kitchen printer or KDS system.
 *
 */
public class QCKitchenPrinter {

    // log file specific to this instance of a QCKitchenPrinter
    private static String KP_LOG = "QCKitchenPrinter_";

    // variables within the QCKitchen printer scope
    private String printerID;
    private String logicalName;
    private String description;
    private boolean isExpeditorPrinter;
    private String ownershipGroupID;
    private String printerHostID;
    private boolean active;
    private String name;
    private int printerTypeID;
    private int expireJobMins;
    private boolean printsDigitalOrdersOnly;
    private int stationID;
    private int printerHardwareTypeID;
    private boolean printOnlyNewItems;

    private POSPrinter printer;
    private boolean isPrinterOpen = false;

    private static final String JPOS_ALIGN_CENTER = "\u001b|cA";
    private static final Pattern ITEM_PATTERN = Pattern.compile("^\\s*(?<qty>\\d+)X\\s+(?<name>.*)");
    private static final String ASTERISK_DIVIDER = "******************************";
    private static final int NUM_OF_NEWLINES = 12;

    /**
     * Constructor for an instance of a QCKitchenPrinter. It's based on a record in the QC_Printer table.
     *
     * @param printerInfo {@link HashMap} HashMap of information about the specific printer obtained from the
     *         QC_Printer table in the database.
     */
    public QCKitchenPrinter (HashMap printerInfo) {
        printerID = HashMapDataFns.getStringVal(printerInfo, "PRINTERID");
        logicalName = HashMapDataFns.getStringVal(printerInfo, "LOGICALNAME");
        description = HashMapDataFns.getStringVal(printerInfo, "DESCRIPTION");
        isExpeditorPrinter = HashMapDataFns.getBooleanVal(printerInfo, "ISEXPEDITORPRINTER");
        ownershipGroupID = HashMapDataFns.getStringVal(printerInfo, "OWNERSHIPGROUPID");
        printerHostID = HashMapDataFns.getStringVal(printerInfo, "PRINTCONTROLLERID");
        active = HashMapDataFns.getBooleanVal(printerInfo, "ACTIVE");
        name = HashMapDataFns.getStringVal(printerInfo, "NAME");
        printerTypeID = HashMapDataFns.getIntVal(printerInfo, "PRINTERTYPEID");
        expireJobMins = HashMapDataFns.getIntVal(printerInfo, "EXPIREJOBMINS");
        printsDigitalOrdersOnly = HashMapDataFns.getBooleanVal(printerInfo, "PRINTSDIGITALORDERSONLY");
        stationID = HashMapDataFns.getIntVal(printerInfo, "STATIONID");
        printerHardwareTypeID = HashMapDataFns.getIntVal(printerInfo, "PRINTERHARDWARETYPEID");
        printOnlyNewItems = HashMapDataFns.getBooleanVal(printerInfo, "PRINTONLYNEWITEMS");

        printer = new MMHPosPrinter(getLogFileName());
    }

    /**
     * <p>Constructor for a {@link QCKitchenPrinter} from a {@link XmlRpcStruct}.</p>
     *
     * @param struct The {@link XmlRpcStruct} that contains the information for this {@link QCKitchenPrinter} instance.
     */
    public QCKitchenPrinter (XmlRpcStruct struct) {
        if (struct != null) {
            printerID = struct.getString("printerID");
            logicalName = struct.getString("logicalName");
            description = struct.getString("description");
            isExpeditorPrinter = struct.getBoolean("isExpeditorPrinter");
            ownershipGroupID = struct.getString("ownershipGroupID");
            printerHostID = struct.getString("printerHostID");
            active = struct.getBoolean("active");
            name = struct.getString("name");
            printerTypeID = struct.getInteger("printerTypeID");
            expireJobMins = struct.getInteger("expireJobMins");
            printsDigitalOrdersOnly = struct.getBoolean("printsDigitalOrdersOnly");
            stationID = struct.getInteger("stationID");
            printerHardwareTypeID = struct.getInteger("printerHardwareTypeID");
            printOnlyNewItems = struct.getBoolean("printOnlyNewItems");

            printer = new MMHPosPrinter(getLogFileName());
        }
    }

    /**
     * <p>Utility method to convert a {@link QCKitchenPrinter} to a {@link XmlRpcStruct}.</p>
     *
     * @return The {@link QCKitchenPrinter} as a {@link XmlRpcStruct}.
     */
    @SuppressWarnings("unchecked")
    public XmlRpcStruct getXmlRpcStruct () {
        XmlRpcStruct printerStruct = new XmlRpcStruct();

        try {
            printerStruct.put("printerID", printerID);
            printerStruct.put("logicalName", logicalName);
            printerStruct.put("description", description);
            printerStruct.put("isExpeditorPrinter", isExpeditorPrinter);
            printerStruct.put("ownershipGroupID", ownershipGroupID);
            printerStruct.put("printerHostID", printerHostID);
            printerStruct.put("active", active);
            printerStruct.put("name", name);
            printerStruct.put("printerTypeID", printerTypeID);
            printerStruct.put("expireJobMins", expireJobMins);
            printerStruct.put("printsDigitalOrdersOnly", printsDigitalOrdersOnly);
            printerStruct.put("stationID", stationID);
            printerStruct.put("printerHardwareTypeID", printerHardwareTypeID);
            printerStruct.put("printOnlyNewItems", printOnlyNewItems);
        }
        catch (Exception e) {
            Logger.logMessage(String.format("There was a problem trying to convert the printer %s into a " +
                    "XmlRpcStruct in QCKitchenPrinter.getXmlRpcStruct",
                    Objects.toString(name, "NULL")), KP_LOG, Logger.LEVEL.ERROR);
        }

        return printerStruct;
    }

    /**
     * Iterate through the details of the given print job and print them line by line.
     *
     * @param qcPrintJob {@link QCPrintJob} The print job we are tyring to print.
     * @param printJobStatusHandler The {@link PrintJobStatusHandler} maintaining the print status of print queue details within the print job.
     * @return {@link PrintStatus} The status of the print job, i.e. whether or not it printed successfully.
     */
    public PrintStatus printQCPrintJob (QCPrintJob qcPrintJob, PrintJobStatusHandler printJobStatusHandler) {
        PrintStatus printJobStatus = new PrintStatus()
                .addPrintStatusType(PrintStatusType.ERROR)
                .addUpdatedDTM(LocalDateTime.now());

        try {
            // check if we should turn on transaction printing
            if (!DataFunctions.isEmptyCollection(qcPrintJob.getPrintJobDetails())) {

                printer.transactionPrint(POSPrinterConst.PTR_S_RECEIPT, POSPrinterConst.PTR_TP_TRANSACTION);

                for (QCPrintJobDetail printJobDetail : qcPrintJob.getPrintJobDetails()) {
                    // get and decode the line to print
                    String printLine = printJobDetail.getLineDetail();
                    printLine = KitchenPrinterCommon.decode(printLine, getLogFileName());
                    printLine = KitchenPrinterCommon.correctAposAndQues(printLine);

                    if (StringFunctions.stringHasContent(printLine)) {
                        try {
                            if (printLine.compareTo(KitchenPrinterReceiptFormatter.JPOS_CUT) == 0) {
                                // cut command received
                                Logger.logMessage("The printer "+Objects.toString(logicalName, "NULL")+" has received " +
                                        "a cut command, printing "+NUM_OF_NEWLINES+" new lines and cutting the paper",
                                        getLogFileName(), Logger.LEVEL.TRACE);

                                // print new lines
                                for (int i = 0; i < NUM_OF_NEWLINES; i++) {
                                    printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
                                }

                                // cut the paper
                                printer.cutPaper(1);

                                // turn of transaction printing
                                Logger.logMessage("Turning off transaction printing for the printer " +
                                        Objects.toString(logicalName, "NULL"), getLogFileName(), Logger.LEVEL.TRACE);
                                printer.transactionPrint(POSPrinterConst.PTR_S_RECEIPT, POSPrinterConst.PTR_TP_NORMAL);

                                // update the print jobs status to done
                                PrintStatus printStatus = new PrintStatus()
                                        .addPrintStatusType(PrintStatusType.DONE)
                                        .addUpdatedDTM(LocalDateTime.now());
                                int printerIDInt = (NumberUtils.isNumber(printerID) ? Integer.parseInt(printerID) : 0);
                                printJobStatusHandler.updatePrintStatusMapForPrinter(printerIDInt, printStatus, qcPrintJob);
                                printJobStatus = printStatus;
                            }
                            else if (printLine.contains(KitchenPrinterReceiptFormatter.BARCODE)) {
                                // command to print barcode received
                                Logger.logMessage("Adding a barcode", getLogFileName(), Logger.LEVEL.DEBUG);
                                printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
                                printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
                                String barcodeToPrint = "QCPOS"+qcPrintJob.getPaTransactionID();

                                // print the barcode
                                try {
                                    CommonRenderer.addBarcode(barcodeToPrint, printer, getLogFileName());
                                }
                                catch (Exception e3) {
                                    Logger.logException(e3, getLogFileName());
                                    Logger.logMessage("There was a problem trying to print a barcode on the printer " +
                                            Objects.toString(logicalName, "NULL")+" in QCKitchenPrinter.printQCPrintJob",
                                            getLogFileName(), Logger.LEVEL.ERROR);
                                }
                                printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
                            }
                            else {
                                // print line normally
                                Logger.logMessage("Printing: "+printLine, getLogFileName(), Logger.LEVEL.TRACE);
                                printer.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n"+printLine);
                            }
                        }
                        catch (Exception e2) {
                            Logger.logException(e2, getLogFileName());
                            Logger.logMessage("There was a problem trying to print the line "+printLine+" on the " +
                                    "printer "+Objects.toString(logicalName, "NULL")+" in " +
                                    "QCKitchenPrinter.printQCPrintJob", getLogFileName(), Logger.LEVEL.ERROR);
                            // update the print job status with the error
                            PrintStatus printStatus = new PrintStatus()
                                    .addPrintStatusType(PrintStatusType.ERROR)
                                    .addUpdatedDTM(LocalDateTime.now());
                            int printerIDInt = (NumberUtils.isNumber(printerID) ? Integer.parseInt(printerID) : 0);
                            printJobStatusHandler.updatePrintStatusMapForPrinter(printerIDInt, printStatus, qcPrintJob);
                            printJobStatus = printStatus;
                        }
                    }
                    else {
                        // update the print job status with the error
                        PrintStatus printStatus = new PrintStatus()
                                .addPrintStatusType(PrintStatusType.ERROR)
                                .addUpdatedDTM(LocalDateTime.now());
                        int printerIDInt = (NumberUtils.isNumber(printerID) ? Integer.parseInt(printerID) : 0);
                        printJobStatusHandler.updatePrintStatusMapForPrinter(printerIDInt, printStatus, qcPrintJob);
                        printJobStatus = printStatus;
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, getLogFileName());
            Logger.logMessage("The printer "+Objects.toString(logicalName, "NULL")+" failed to print the print job " +
                    Objects.toString(qcPrintJob.getJobTrackerID(), "NULL")+" in QCKitchenPrinter.printQCPrintJob",
                    getLogFileName(), Logger.LEVEL.ERROR);
            // update the print job status with the error
            PrintStatus printStatus = new PrintStatus()
                    .addPrintStatusType(PrintStatusType.ERROR)
                    .addUpdatedDTM(LocalDateTime.now());
            int printerIDInt = (NumberUtils.isNumber(printerID) ? Integer.parseInt(printerID) : 0);
            printJobStatusHandler.updatePrintStatusMapForPrinter(printerIDInt, printStatus, qcPrintJob);
            printJobStatus = printStatus;
        }

        return printJobStatus;
    }

    /**
     * Opens a connection to the printer so that print jobs may be printed by the printer.
     *
     * @return Whether or not a connection to the printer could be established.
     */
    public synchronized boolean openPrinter () {
        boolean openResult = false;

        // if the printer is a KDS printer set the openResult to true
        if (printerHardwareTypeID == 2) {
            Logger.logMessage("The KDS Station "+Objects.toString(logicalName, "NULL")+" is open for input",
                    getLogFileName(), Logger.LEVEL.TRACE);
            isPrinterOpen = true;
            return true;
        }

        try {
            // close connection to the printer if it's open for some reason
            if (isPrinterOpen) {
                closePrinter();
            }

            // if the printer uses ESC/POS we need to build the logical name to open the printer
            String printerOpenName;
            if (isEscPosPrinter()) {
                printerOpenName = "ESC/POS,"+getEscPosCOMPort();
            }
            else {
                printerOpenName = logicalName;
            }

            // open the printer
            Logger.logMessage("Opening the printer: "+Objects.toString(logicalName, "NULL"), getLogFileName(),
                    Logger.LEVEL.TRACE);
            printer.open(printerOpenName);

            // claim the printer
            Logger.logMessage("Claiming the printer: "+Objects.toString(logicalName, "NULL"), getLogFileName(),
                    Logger.LEVEL.TRACE);
            printer.claim(120000);

            // enable the printer
            Logger.logMessage("Enabling the printer: "+Objects.toString(logicalName, "NULL"), getLogFileName(),
                    Logger.LEVEL.TRACE);
            printer.setDeviceEnabled(true);

            // perform a test print
            printer.printNormal(2, " \n");
            Logger.logMessage("The printer: "+Objects.toString(logicalName, "NULL")+" is open for input",
                    getLogFileName(), Logger.LEVEL.TRACE);

            isPrinterOpen = true;
            openResult = true;
        }
        catch (Exception e) {
            Logger.logException(e, getLogFileName());
            Logger.logMessage("Unable to establish a connection to the printer " +
                    Objects.toString(logicalName, "NULL")+" in QCKitchenPrinter.openPrinter", getLogFileName(),
                    Logger.LEVEL.ERROR);
        }

        return openResult;
    }

    /**
     * Close the connection to this printer if it is open.
     *
     * @return Whether or not the connection to this printer could be closed.
     */
    public synchronized boolean closePrinter () {
        boolean closeResult = false;

        // if the printer is a KDS printer set the closeResult to true
        if (printerHardwareTypeID == 2) {
            Logger.logMessage("The KDS Station "+Objects.toString(logicalName, "NULL")+" has been closed successfully",
                    getLogFileName(), Logger.LEVEL.TRACE);
            isPrinterOpen = false;
            return true;
        }

        try {
            if (isPrinterOpen) {
                // disable the printer
                Logger.logMessage("Disabling the printer: "+Objects.toString(logicalName,"NULL"), getLogFileName(),
                        Logger.LEVEL.TRACE);
                printer.setDeviceEnabled(false);

                // release the printer
                Logger.logMessage("Releasing the printer: "+Objects.toString(logicalName,"NULL"), getLogFileName(),
                        Logger.LEVEL.TRACE);
                printer.release();

                // close the printer
                Logger.logMessage("Closing the printer: "+Objects.toString(logicalName,"NULL"), getLogFileName(),
                        Logger.LEVEL.TRACE);
                printer.close();

                Logger.logMessage("The printer: "+Objects.toString(logicalName, "NULL")+" has been closed " +
                        "successfully", getLogFileName(), Logger.LEVEL.TRACE);

                isPrinterOpen = false;
                closeResult = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, getLogFileName());
            Logger.logMessage("Unable to close the connection to the printer "+Objects.toString(logicalName, "NULL") +
                    " in QCKitchenPrinter.closePrinter", getLogFileName(), Logger.LEVEL.ERROR);
        }

        return closeResult;
    }

    /**
     * <p>Queries the database to determine whether or not this printer uses ESC/POS.</p>
     *
     * @return Whether or not this printer uses ESC/POS.
     */
    private boolean isEscPosPrinter () {
        boolean isEscPosPrinter = false;

        try {
            Object queryRes = new DataManager().parameterizedExecuteScalar("data.newKitchenPrinter.GetIsEscPrinter", new Object[]{printerID}, getLogFileName());
            if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (queryRes.toString().equalsIgnoreCase("1"))) {
                isEscPosPrinter = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, getLogFileName());
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the printer %s uses ESC/POS in QCKitchenPrinter.isEscPosPrinter",
                    Objects.toString(name, "NULL")), getLogFileName(), Logger.LEVEL.ERROR);
        }

        return isEscPosPrinter;
    }

    /**
     * <p>Gets the COM port {@link String} used by this printer if it uses ESC/POS.</p>
     *
     * @return The COM port {@link String} used by this printer if it uses ESC/POS.
     */
    private String getEscPosCOMPort () {
        String comPort = "";

        try {
            Object queryRes = new DataManager().parameterizedExecuteScalar("data.newKitchenPrinter.GetEscPrinterCOMPort", new Object[]{printerID}, getLogFileName());
            if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString()))) {
                comPort = queryRes.toString();
            }
        }
        catch (Exception e) {
            Logger.logException(e, getLogFileName());
            Logger.logMessage(String.format("There was a problem trying to determine the COM port being used by the printer %s which uses ESC/POS in QCKitchenPrinter.getEscPosCOMPort",
                    Objects.toString(name, "NULL")), getLogFileName(), Logger.LEVEL.ERROR);
        }

        return comPort;
    }

    /**
     * Get the name of the log file for this specific instance of QCKitchenPrinter.
     *
     * @return {@link String} The name of the log file for this specific instance of QCKitchenPrinter.
     */
    private String getLogFileName () {
        return (KP_LOG+printerID+".log");
    }

    /**
     * Setter for the QCKitchenPrinters printerID field.
     *
     * @param printerID {@link String} The ID of the printer.
     */
    public void setPrinterID (String printerID) {
        this.printerID = printerID;
    }

    /**
     * Getter for the QCKitchenPrinters printerID field.
     *
     * @return {@link String} The ID of the printer.
     */
    public String getPrinterID () {
        return printerID;
    }

    /**
     * Setter for the QCKitchenPrinters logicalName field.
     *
     * @param logicalName {@link String} The logical name of the printer.
     */
    public void setLogicalName (String logicalName) {
        this.logicalName = logicalName;
    }

    /**
     * Getter for the QCKitchenPrinters logicalName field.
     *
     * @return {@link String} The logical name of the printer.
     */
    public String getLogicalName () {
        return logicalName;
    }

    /**
     * Setter for the QCKitchenPrinters description field.
     *
     * @param description {@link String} The description for the printer.
     */
    public void setDescription (String description) {
        this.description = description;
    }

    /**
     * Getter for the QCKitchenPrinters description field.
     *
     * @return {@link String} The description for the printer.
     */
    public String getDescription () {
        return description;
    }

    /**
     * Setter for the QCKitchenPrinters isExpeditorPrinter field.
     *
     * @param isExpeditorPrinter Whether or not this printer is an expeditor printer.
     */
    public void setIsExpeditorPrinter (boolean isExpeditorPrinter) {
        this.isExpeditorPrinter = isExpeditorPrinter;
    }

    /**
     * Getter for the QCKitchenPrinters isExpeditorPrinter field.
     *
     * @return Whether or not this printer is an expeditor printer.
     */
    public boolean getIsExpeditorPrinter () {
        return isExpeditorPrinter;
    }

    /**
     * Setter for the QCKitchenPrinters ownershipGroupID field.
     *
     * @param ownershipGroupID {@link String} The ID of the ownership group the printer belongs to.
     */
    public void setOwnershipGroupID (String ownershipGroupID) {
        this.ownershipGroupID = ownershipGroupID;
    }

    /**
     * Getter for the QCKitchenPrinters ownershipGroupID field.
     *
     * @return {@link String} The ID of the ownership group the printer belongs to.
     */
    public String getOwnershipGroupID () {
        return ownershipGroupID;
    }

    /**
     * Setter for the QCKitchenPrinters printerHostID field.
     *
     * @param printerHostID {@link String} The ID of the printer host the printer will receive print jobs from.
     */
    public void setPrinterHostID (String printerHostID) {
        this.printerHostID = printerHostID;
    }

    /**
     * Getter for the QCKitchenPrinters printerHostID field.
     *
     * @return {@link String} The ID of the printer host the printer will receive print jobs from.
     */
    public String getPrinterHostID () {
        return printerHostID;
    }

    /**
     * Setter for the QCKitchenPrinters active field.
     *
     * @param active Whether or not this printer is active.
     */
    public void setActive (boolean active) {
        this.active = active;
    }

    /**
     * Getter for the QCKitchenPrinters active field.
     *
     * @return Whether or not this printer is active.
     */
    public boolean getActive () {
        return active;
    }

    /**
     * Setter for the QCKitchenPrinters name field.
     *
     * @param name {@link String} The name of the printer.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * Getter for the QCKitchenPrinters name field.
     *
     * @return {@link String} The name of the printer.
     */
    public String getName () {
        return name;
    }

    /**
     * Setter for the QCKitchenPrinters printerTypeID field.
     *
     * @param printerTypeID The ID for the type of this printer.
     */
    public void setPrinterTypeID (int printerTypeID) {
        this.printerTypeID = printerTypeID;
    }

    /**
     * Getter for the QCKitchenPrinters printerTypeID field.
     *
     * @return The ID for the type of this printer.
     */
    public int getPrinterTypeID () {
        return printerTypeID;
    }

    /**
     * Setter for the QCKitchenPrinters expireJobMins field.
     *
     * @param expireJobMins The number of minutes before the print job is considered expired.
     */
    public void setExpireJobMins (int expireJobMins) {
        this.expireJobMins = expireJobMins;
    }

    /**
     * Getter for the QCKitchenPrinters expireJobMins field.
     *
     * @return The number of minutes before the print job is considered expired.
     */
    public int getExpireJobMins () {
        return expireJobMins;
    }

    /**
     * Setter for the QCKitchenPrinters printsDigitalOrdersOnly field.
     *
     * @param printsDigitalOrdersOnly Whether or not this printer only prints remote orders.
     */
    public void setPrintsDigitalOrdersOnly (boolean printsDigitalOrdersOnly) {
        this.printsDigitalOrdersOnly = printsDigitalOrdersOnly;
    }

    /**
     * Getter for the QCKitchenPrinters printsDigitalOrdersOnly field.
     *
     * @return Whether or not this printer only prints remote orders.
     */
    public boolean getPrintsDigitalOrdersOnly () {
        return printsDigitalOrdersOnly;
    }

    /**
     * Setter for the QCKitchenPrinters stationID field.
     *
     * @param stationID The station ID if this printer is KDS.
     */
    public void setStationID (int stationID) {
        this.stationID = stationID;
    }

    /**
     * Getter for the QCKitchenPrinters stationID field.
     *
     * @return The station ID if this printer is KDS.
     */
    public int getStationID () {
        return stationID;
    }

    /**
     * Setter for the QCKitchenPrinters printerHardwareTypeID field.
     *
     * @param printerHardwareTypeID The ID for the type of hardware this printer is.
     */
    public void setPrinterHardwareTypeID (int printerHardwareTypeID) {
        this.printerHardwareTypeID = printerHardwareTypeID;
    }

    /**
     * Getter for the QCKitchenPrinters printerHardwareTypeID field.
     *
     * @return The ID for the type of hardware this printer is.
     */
    public int getPrinterHardwareTypeID () {
        return printerHardwareTypeID;
    }

    /**
     * Setter for the QCKitchenPrinters printOnlyNewItems field.
     *
     * @param printOnlyNewItems Whether or not this printer only prints new items.
     */
    public void setPrintOnlyNewItems (boolean printOnlyNewItems) {
        this.printOnlyNewItems = printOnlyNewItems;
    }

    /**
     * Getter for the QCKitchenPrinters printOnlyNewItems field.
     *
     * @return Whether or not this printer only prints new items.
     */
    public boolean getPrintOnlyNewItems () {
        return printOnlyNewItems;
    }

    /**
     * Setter for the QCKitchenPrinters isPrinterOpen field.
     *
     * @param isPrinterOpen Whether or not this printer already has an open connection.
     */
    public void setIsPrinterOpen (boolean isPrinterOpen) {
        this.isPrinterOpen = isPrinterOpen;
    }

    /**
     * Getter for the QCKitchenPrinters isPrinterOpen field.
     *
     * @return Whether or not this printer already has an open connection.
     */
    public boolean getIsPrinterOpen () {
        return isPrinterOpen;
    }

    /**
     * <p>Getter for the printer field of the {@link QCKitchenPrinter}.</p>
     *
     * @return The printer field of the {@link QCKitchenPrinter}.
     */
    public POSPrinter getPOSPrinter() {
        return printer;
    }

    /**
     * Overridden toString method for a QCKitchenPrinter.
     *
     * @return {@link String} QCKitchenPrinter as a String.
     */
    @Override
    public String toString () {
        return String.format("PRINTERID: %s, LOGICALNAME: %s, DESCRIPTION: %s, ISEXPEDITORPRINTER: %s, " +
                "OWNERSHIPGROUPID: %s, PRINTERHOSTID: %s, ACTIVE: %s, NAME: %s, PRINTERTYPEID: %s, EXPIREJOBMINS: %s, " +
                "PRINTSDIGITALORDERSONLY: %s, STATIONID: %s, PRINTERHARDWARETYPEID: %s, PRINTONLYNEWITEMS: %s",
                Objects.toString(printerID, "NULL"),
                Objects.toString(logicalName, "NULL"),
                Objects.toString(description, "NULL"),
                Objects.toString(isExpeditorPrinter, "NULL"),
                Objects.toString(ownershipGroupID, "NULL"),
                Objects.toString(printerHostID, "NULL"),
                Objects.toString(active, "NULL"),
                Objects.toString(name, "NULL"),
                Objects.toString(printerTypeID, "NULL"),
                Objects.toString(expireJobMins, "NULL"),
                Objects.toString(printsDigitalOrdersOnly, "NULL"),
                Objects.toString(stationID, "NULL"),
                Objects.toString(printerHardwareTypeID, "NULL"),
                Objects.toString(printOnlyNewItems, "NULL"));
    }

    /**
     * Overridden equals method for a QCKitchenPrinter.
     *
     * @param o {@link Object} Instance of the QCKitchenPrinter we are comparing against.
     * @return Whether or not the QCKitchenPrinters are equal.
     */
    @Override
    public boolean equals (Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof QCKitchenPrinter)) {
            return false;
        }

        // check if the instances printerID is equal to this QCKitchenPrinter's printerID
        QCKitchenPrinter qcKitchenPrinter = (QCKitchenPrinter) o;
        return Objects.equals(qcKitchenPrinter.getPrinterID(), printerID);
    }

    /**
     * Overridden hashCode method for the QCKitchenPrinter.
     *
     * @return The unique hash code.
     */
    @Override
    public int hashCode () {
        return Objects.hash(printerID);
    }

}
