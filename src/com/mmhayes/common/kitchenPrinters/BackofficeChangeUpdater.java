package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-04-26 10:59:25 -0400 (Mon, 26 Apr 2021) $: Date of last commit
    $Rev: 13847 $: Revision of last commit
    Notes: Checks for changes in configuration to printer hosts and printers made in the backoffice.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.xmlapi.XmlRpcUtil;
import redstone.xmlrpc.XmlRpcArray;
import redstone.xmlrpc.XmlRpcStruct;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * <p>Checks for changes in configuration to printer hosts and printers made in the backoffice.</p>
 *
 */
public class BackofficeChangeUpdater implements Runnable, Serializable {

    // the only instance of the BackofficeChangeUpdater for this terminal
    private static volatile BackofficeChangeUpdater backofficeChangeUpdater = null;

    // log file for the BackofficeChangeUpdater
    private static final String BCU_LOG = "KP_BackofficeUpdate.log";

    // private member variables of a BackofficeChangeUpdater
    private DataManager dataManager;
    private int checkInterval;
    private volatile boolean isStartup = true;
    private LocalDateTime lastBackofficeUpdateCheck;
    private final long POLLING_RATE = 60000L;
    private CopyOnWriteArraySet<QCPrinterHost> printerHosts;
    private CopyOnWriteArraySet<QCKitchenPrinter> printers;
    private ScheduledExecutorService printerHostHealthCheck;
    private PrinterHostHealthCheck printerHostHealthCheckInstance;
    private final long PRINTER_HOST_HEALTH_CHECK_INITAL_DELAY = 10L;
    private volatile boolean isPHHealthCheckRunningOnTerminal = false;

    // thread control variables
    private volatile boolean isShuttingDown = false;
    private volatile boolean isTerminated = false;

    /**
     * <p>Private constructor for a {@link BackofficeChangeUpdater}.</p>
     *
     * @param dataManager The {@link DataManager} to be used by the {@link BackofficeChangeUpdater}.
     * @param checkInterval How frequently to check the backoffice for updates to printer hosts and printers.
     */
    private BackofficeChangeUpdater (DataManager dataManager, int checkInterval) {
        // prevent instantiating a BackofficeChangeUpdater through reflection
        if (backofficeChangeUpdater != null) {
            throw new RuntimeException("Use the BackofficeChangeUpdater.getInstanceMethod to get the only instance of a BackofficeChangeUpdater on this terminal.");
        }

        this.dataManager = dataManager;
        this.checkInterval = checkInterval;

        Logger.logMessage(String.format("The BackofficeChangeUpdater has been instantiated and it's check interval has been set to %s minutes",
                Objects.toString(checkInterval, "NULL")), BCU_LOG, Logger.LEVEL.TRACE);
    }

    /**
     * <p>Getter for the isPHHealthCheckRunningOnTerminal field of the {@link BackofficeChangeUpdater}.</p>
     *
     * @return The isPHHealthCheckRunningOnTerminal field of the {@link BackofficeChangeUpdater}.
     */
    public boolean getIsPHHealthCheckRunningOnTerminal () {
        return isPHHealthCheckRunningOnTerminal;
    }

    /**
     * <p>Getter for the printerHosts field of the {@link BackofficeChangeUpdater}.</p>
     *
     * @return The printerHosts field of the {@link BackofficeChangeUpdater}.
     */
    public CopyOnWriteArraySet<QCPrinterHost> getPrinterHostsFromBackofficeUpdaterThread () {
        return printerHosts;
    }

    /**
     * <p>Getter for the printers field of the {@link BackofficeChangeUpdater}.</p>
     *
     * @return The printers field of the {@link BackofficeChangeUpdater}.
     */
    public CopyOnWriteArraySet<QCKitchenPrinter> getPrintersFromBackofficeUpdaterThread () {
        return printers;
    }

    /**
     * <p>Getter for the printerHostHealthCheckInstance field of the {@link BackofficeChangeUpdater}.</p>
     *
     * @return The printerHostHealthCheckInstance field of the {@link BackofficeChangeUpdater}.
     */
    public PrinterHostHealthCheck getPrinterHostHealthCheckInstance () {
        return printerHostHealthCheckInstance;
    }

    /**
     * <p>Setter for the printerHostHealthCheckInstance field of the {@link BackofficeChangeUpdater}.</p>
     *
     * @param printerHostHealthCheckInstance The printerHostHealthCheckInstance field of the {@link BackofficeChangeUpdater}.
     */
    public void setPrinterHostHealthCheckInstance (PrinterHostHealthCheck printerHostHealthCheckInstance) {
        this.printerHostHealthCheckInstance = printerHostHealthCheckInstance;
    }

    /**
     * <p>Gets the only instance of a {@link BackofficeChangeUpdater} for this terminal.</p>
     *
     * @return The {@link BackofficeChangeUpdater} instance.
     */
    public static BackofficeChangeUpdater getInstance () {

        try {
            if (backofficeChangeUpdater == null) {
                throw new RuntimeException("The BackofficeChangeUpdater must be instantiated before trying to obtain it's instance.");
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage("There was a problem trying to get the only instance of a BackofficeChangeUpdater for this " +
                    "terminal in BackofficeChangeUpdater.getInstance", BCU_LOG, Logger.LEVEL.ERROR);
        }

        return backofficeChangeUpdater;
    }

    /**
     * <p>Instantiates the only BackofficeChangeUpdater instance on this terminal.</p>
     *
     * @param dataManager The {@link DataManager} to be used by the {@link BackofficeChangeUpdater}.
     * @param checkInterval How frequently to check the backoffice for updates to printer hosts and printers.
     * @return The {@link BackofficeChangeUpdater} instance.
     */
    public static synchronized BackofficeChangeUpdater init (DataManager dataManager, int checkInterval) {

        try {
            if (backofficeChangeUpdater != null) {
                throw new RuntimeException("The BackofficeChangeUpdater has already been instantiated and may not be instantiated again.");
            }
            else {
                backofficeChangeUpdater = new BackofficeChangeUpdater(dataManager, checkInterval);
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage("There was a problem trying to instantiate and return the BackofficeChangeUpdater " +
                    "instance for this terminal in BackofficeChangeUpdater.init", BCU_LOG, Logger.LEVEL.ERROR);
        }

        return backofficeChangeUpdater;
    }

    /**
     * <p>Prevents instantiating a new {@link BackofficeChangeUpdater} though serialization.</p>
     *
     * @return The {@link BackofficeChangeUpdater} instance.
     */
    protected BackofficeChangeUpdater readResolve () {
        return getInstance();
    }

    /**
     * <p>Overridden run method for a {@link BackofficeChangeUpdater}. This method will be called by a {@link Thread}
     * created using the {@link BackofficeChangeUpdater} {@link Runnable}.</p>
     *
     */
    @Override
    public void run () {
        try {
            while ((!isShuttingDown) && (!Thread.currentThread().isInterrupted())) {
                if (isStartup) {
                    // perform the backoffice check
                    backofficeCheck(true);
                    lastBackofficeUpdateCheck = LocalDateTime.now();
                    isStartup = false;
                }

                // check if we should be looking for updates every polling rate
                Thread.sleep(POLLING_RATE);

                // log the printer hosts and printers
                Logger.logMessage("START LOGGING CONFIGURED PRINTER HOSTS AND PRINTERS", BCU_LOG, Logger.LEVEL.TRACE);
                if (!DataFunctions.isEmptyCollection(printerHosts)) {
                    printerHosts.forEach(printerHost -> {
                        Logger.logMessage("    PRINTER HOST -> "+printerHost.toString(), BCU_LOG, Logger.LEVEL.TRACE);
                    });
                }
                if (!DataFunctions.isEmptyCollection(printers)) {
                    printers.forEach(printer -> {
                        Logger.logMessage("    PRINTER -> "+printer.toString(), BCU_LOG, Logger.LEVEL.TRACE);
                    });
                }
                Logger.logMessage("END LOGGING CONFIGURED PRINTER HOSTS AND PRINTERS", BCU_LOG, Logger.LEVEL.TRACE);

                if (shouldCheckForBackofficeUpdates()) {

                    // perform the backoffice check
                    backofficeCheck(false);

                    // update the last time backoffice updates were checked for
                    lastBackofficeUpdateCheck = LocalDateTime.now();
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage("There was a problem in BackOfficeChangeUpdater.run", BCU_LOG, Logger.LEVEL.ERROR);
        }
        finally {
            isTerminated = true;
            Logger.logMessage("The Thread named KitchenPrinter-BackOfficeChangeUpdater is exiting", BCU_LOG, Logger.LEVEL.IMPORTANT);
        }
    }

    /**
     * <p>Determines whether or not the BackofficeChangeUpdater should check for changes made in the backoffice to printer hosts and printers.</p>
     *
     * @return Whether or not the BackofficeChangeUpdater should check for changes made in the backoffice to printer hosts and printers.
     */
    private boolean shouldCheckForBackofficeUpdates () {
        boolean shouldCheck = false;

        try {
            // get what time it is now
            LocalDateTime currentTime = LocalDateTime.now();
            // get how many minutes have passed since the last check
            long minutes = lastBackofficeUpdateCheck.until(currentTime, ChronoUnit.MINUTES);
            if (((int) minutes) == 1) {
                Logger.logMessage(minutes+" minute has passed since the last backoffice updates check", BCU_LOG, Logger.LEVEL.DEBUG);
            }
            else {
                Logger.logMessage(minutes+" minutes have passed since the last backoffice updates check", BCU_LOG, Logger.LEVEL.DEBUG);
            }

            if (minutes >= checkInterval) {
                shouldCheck = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage("There was a problem trying to determine whether or not to check for changes to printer hosts and printers in " +
                    "the backoffice in BackofficeChangeUpdater.shouldCheckForBackofficeUpdates", BCU_LOG, Logger.LEVEL.ERROR);
        }

        return shouldCheck;
    }

    /**
     * <p>Creates and starts a {@link Thread} using the {@link BackofficeChangeUpdater} {@link Runnable}.</p>
     *
     */
    public void startBackOfficeChangeUpdaterThread () {

        try {
            // make sure isShuttingDown and isTerminated are false
            isShuttingDown = false;
            isTerminated = false;

            Thread backOfficeChangeUpdaterThread = new Thread(this);
            backOfficeChangeUpdaterThread.setName("KitchenPrinter-BackOfficeChangeUpdater");
            backOfficeChangeUpdaterThread.start();
            Logger.logMessage("The thread to check for backoffice changes to printer hosts and printers has started " +
                    "successfully in BackOfficeChangeUpdater.startBackOfficeChangeUpdaterThread", BCU_LOG, Logger.LEVEL.IMPORTANT);
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage("There was a problem trying to start the Thread created using the BackOfficeChangeUpdater " +
                    "Runnable in BackOfficeChangeUpdater.startBackOfficeChangeUpdaterThread", BCU_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Stops the {@link Thread} created from the {@link BackofficeChangeUpdater} {@link Runnable}.</p>
     *
     */
    public void stopBackOfficeChangeUpdaterThread () {

        try {
            // run through the Thread shutdown routine
            isShuttingDown = true;

            if (isPHHealthCheckRunningOnTerminal) {
                stopPrinterHostHealthCheck();
            }

            int waitCount = 0;
            while (!isTerminated) {
                waitCount++;
                // wait a second
                Thread.sleep(1000L);
                if (waitCount >= 3) {
                    Logger.logMessage("The thread to check for backoffice changes to printer hosts and printers didn't stop within the specified wait time, it " +
                            "is being interrupted now in BackOfficeChangeUpdater.stopBackOfficeChangeUpdaterThread", BCU_LOG, Logger.LEVEL.TRACE);
                    // interrupt the Thread created using the BackOfficeChangeUpdater Runnable
                    Thread.getAllStackTraces().keySet().forEach((thread) -> {
                        if (thread.getName().equalsIgnoreCase("KitchenPrinter-BackOfficeChangeUpdater")) {
                            thread.interrupt();
                        }
                    });
                }
            }

            Logger.logMessage("The thread to check for backoffice changes to printer hosts and printers has stopped " +
                    "successfully in BackOfficeChangeUpdater.stopBackOfficeChangeUpdaterThread", BCU_LOG, Logger.LEVEL.IMPORTANT);
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage("There was a problem trying to stop the thread created using the BackOfficeChangeUpdater " +
                    "in BackOfficeChangeUpdater.stopBackOfficeChangeUpdaterThread", BCU_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Checks for any changes made the backoffice to the configuration of printer hosts and printers.</p>
     *
     * @param isStartup Whether or not the backoffice update change updater Thread is starting up.
     */
    private void backofficeCheck (boolean isStartup) {

        try {
            if (isStartup) {
                // make sure there are no printer hosts on startup
                printerHosts = new CopyOnWriteArraySet<>();
                if (!DataFunctions.isEmptyCollection(getPrinterHosts())) {
                    printerHosts = new CopyOnWriteArraySet<>(getPrinterHosts());
                }

                // make sure there are no printers on startup
                printers = new CopyOnWriteArraySet<>();
                if (!DataFunctions.isEmptyCollection(getPrinters())) {
                    printers = new CopyOnWriteArraySet<>(getPrinters());
                }

                // start the printer host health check on all terminal's that should be running the printer host health check
                if (!DataFunctions.isEmptyCollection(printerHosts)) {
                    for (QCPrinterHost printerHost : printerHosts) {
                        if ((printerHost != null) && (startPrinterHostHealthCheckOnTerminal(printerHost.getMacAddress(), printerHost.getHealthCheckFrequency()))) {
                            Logger.logMessage(String.format("The terminal %s was able to successfully start the printer " +
                                    "host health check on the terminal %s in BackofficeChangeUpdater.backofficeCheck",
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                    Objects.toString(printerHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.DEBUG);
                        }
                        else if ((printerHost != null) && (!startPrinterHostHealthCheckOnTerminal(printerHost.getMacAddress(), printerHost.getHealthCheckFrequency()))) {
                            Logger.logMessage(String.format("The terminal %s was unable to successfully start the printer " +
                                    "host health check on the terminal %s in BackofficeChangeUpdater.backofficeCheck",
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                    Objects.toString(printerHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
                        }
                    }
                }
            }
            else {
                // handle backoffice updates to printer hosts
                handlePrinterHostChanges(printerHosts);

                // handle backoffice updates to printers
                handlePrinterChanges();
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to perform the backoffice check on " +
                    "the terminal %s in BackofficeChangeUpdater.backofficeCheck",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Finds the printer hosts this terminal could potentially send a print job to.</p>
     *
     * @return The {@link ArrayList} of {@link QCPrinterHost} containing the printer hosts this terminal could potentially send a print job to.
     */
    @SuppressWarnings("unchecked")
    private ArrayList<QCPrinterHost> getPrinterHosts () {
        ArrayList<QCPrinterHost> printerHosts = new ArrayList<>();

        try {
            ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(dataManager.parameterizedExecuteQuery(
                    "data.newKitchenPrinter.GetPrinterHostsForTerminal", new Object[]{PeripheralsDataManager.getTerminalMacAddress()}, BCU_LOG, true));
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                printerHosts.addAll(queryRes.stream().map(QCPrinterHost::new).collect(Collectors.toList()));
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to obtain the printer hosts that the terminal %s " +
                    "could potentially send print jobs to in BackofficeChangeUpdater.getPrinterHosts",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

        return printerHosts;
    }

    /**
     * <p>Finds the printers this terminal could potentially execute a print job on.</p>
     *
     * @return The {@link ArrayList} of {@link QCKitchenPrinter} containing the printers this terminal could potentially execute a print job on.
     */
    @SuppressWarnings("unchecked")
    private ArrayList<QCKitchenPrinter> getPrinters () {
        ArrayList<QCKitchenPrinter> printers = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(printerHosts)) {
                int[] printerHostIDs = new int[printerHosts.size()];
                int index = 0;
                for (QCPrinterHost printerHost : printerHosts) {
                    printerHostIDs[index] = printerHost.getPrinterHostID();
                    index++;
                }
                if (!DataFunctions.isEmptyIntArr(printerHostIDs)) {
                    ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(dataManager.parameterizedExecuteQuery(
                            "data.newKitchenPrinter.GetPrintersForTerminal", new Object[]{DataFunctions.convertIntArrToStrForSQL(printerHostIDs)}, BCU_LOG, true));
                    if (!DataFunctions.isEmptyCollection(queryRes)) {
                        printers.addAll(queryRes.stream().map(QCKitchenPrinter::new).collect(Collectors.toList()));
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to obtain the printers that the terminal %s " +
                    "could potentially execute print jobs on in BackofficeChangeUpdater.getPrinters",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

        return printers;
    }

    /**
     * <p>Finds changes made in the backoffice to {@link QCPrinterHost} since the last time the backoffice check was run.</p>
     *
     * @param prevPrinterHosts The {@link CopyOnWriteArraySet} of {@link QCPrinterHost} that were configured in the backoffice the last time the backoffice check ran.
     */
    @SuppressWarnings({"Convert2streamapi", "OverlyComplexMethod", "TypeMayBeWeakened"})
    private void handlePrinterHostChanges (CopyOnWriteArraySet<QCPrinterHost> prevPrinterHosts) {

        try {
            // get the printers hosts that are currently configured in the backoffice
            CopyOnWriteArraySet<QCPrinterHost> currPrinterHosts = null;
            if (!DataFunctions.isEmptyCollection(getPrinterHosts())) {
                currPrinterHosts = new CopyOnWriteArraySet<>(getPrinterHosts());
            }

            if ((!DataFunctions.isEmptyCollection(prevPrinterHosts)) && (!DataFunctions.isEmptyCollection(currPrinterHosts))) {
                // check for new printer hosts
                CopyOnWriteArraySet<QCPrinterHost> addedPrinterHosts = new CopyOnWriteArraySet<>();
                for (QCPrinterHost currPrinterHost : currPrinterHosts) {
                    if (!prevPrinterHosts.contains(currPrinterHost)) {
                        addedPrinterHosts.add(currPrinterHost);
                    }
                }
                if (!DataFunctions.isEmptyCollection(addedPrinterHosts)) {
                    for (QCPrinterHost addedPrinterHost : addedPrinterHosts) {
                        if (addedPrinterHost != null) {
                            addPrinterHostOnTerminal(addedPrinterHost.getMacAddress(), addedPrinterHost.getPrinterHostID());
                            Logger.logMessage(String.format("The printer host running on the terminal %s has been added since " +
                                    "the last time the check for backoffice updates was run in BackofficeChangeUpdater.handlePrinterHostChanges",
                                    Objects.toString(addedPrinterHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.TRACE);
                            // start the printer host health check on the newly configured printer host
                            if (startPrinterHostHealthCheckOnTerminal(addedPrinterHost.getMacAddress(), addedPrinterHost.getHealthCheckFrequency())) {
                                Logger.logMessage(String.format("The terminal %s was able to successfully start the printer " +
                                        "host health check on the terminal %s in BackofficeChangeUpdater.handlePrinterHostChanges",
                                        Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                        Objects.toString(addedPrinterHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.DEBUG);
                            }
                            else if (!startPrinterHostHealthCheckOnTerminal(addedPrinterHost.getMacAddress(), addedPrinterHost.getHealthCheckFrequency())) {
                                Logger.logMessage(String.format("The terminal %s was unable to successfully start the printer " +
                                        "host health check on the terminal %s in BackofficeChangeUpdater.handlePrinterHostChanges",
                                        Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                        Objects.toString(addedPrinterHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
                            }
                        }
                    }
                    printerHosts.addAll(addedPrinterHosts);
                }

                // check for removed printer hosts
                CopyOnWriteArraySet<QCPrinterHost> removedPrinterHosts = new CopyOnWriteArraySet<>();
                for (QCPrinterHost prevPrinterHost : prevPrinterHosts) {
                    if (!currPrinterHosts.contains(prevPrinterHost)) {
                        removedPrinterHosts.add(prevPrinterHost);
                    }
                }
                if (!DataFunctions.isEmptyCollection(removedPrinterHosts)) {
                    for (QCPrinterHost removedPrinterHost : removedPrinterHosts) {
                        if (removedPrinterHost != null) {
                            // stop the printer host if it's running
                            if (printerHostHealthCheckInstance.isPrinterHostInService(removedPrinterHost.getMacAddress())) {
                                printerHostHealthCheckInstance.stopPrinterHost(removedPrinterHost.getMacAddress());
                            }
                            removePrinterHostOnTerminal(removedPrinterHost.getMacAddress(), removedPrinterHost.getPrinterHostID());
                            Logger.logMessage(String.format("The printer host running on the terminal %s has been inactivated since " +
                                    "the last time the check for backoffice updates was run in BackofficeChangeUpdater.handlePrinterHostChanges",
                                    Objects.toString(removedPrinterHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.TRACE);
                            // stop the printer host health check on the previously configured printer host
                            if (stopPrinterHostHealthCheckOnTerminal(removedPrinterHost.getMacAddress())) {
                                Logger.logMessage(String.format("The terminal %s was able to successfully stop the printer " +
                                        "host health check on the terminal %s in BackofficeChangeUpdater.handlePrinterHostChanges",
                                        Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                        Objects.toString(removedPrinterHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.DEBUG);
                            }
                            else if (!stopPrinterHostHealthCheckOnTerminal(removedPrinterHost.getMacAddress())) {
                                Logger.logMessage(String.format("The terminal %s was unable to successfully stop the printer " +
                                        "host health check on the terminal %s in BackofficeChangeUpdater.handlePrinterHostChanges",
                                        Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                        Objects.toString(removedPrinterHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
                            }
                        }
                    }
                    printerHosts.removeAll(removedPrinterHosts);
                }
            }
            else if ((DataFunctions.isEmptyCollection(prevPrinterHosts)) && (!DataFunctions.isEmptyCollection(currPrinterHosts))) {
                // all printer hosts were added
                for (QCPrinterHost printerHost : currPrinterHosts) {
                    if (printerHost != null) {
                        addPrinterHostOnTerminal(printerHost.getMacAddress(), printerHost.getPrinterHostID());
                        Logger.logMessage(String.format("The printer host running on the terminal %s has been added since " +
                                "the last time the check for backoffice updates was run in BackofficeChangeUpdater.handlePrinterHostChanges",
                                Objects.toString(printerHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.TRACE);
                        // start the printer host health check on the newly configured printer host
                        if (startPrinterHostHealthCheckOnTerminal(printerHost.getMacAddress(), printerHost.getHealthCheckFrequency())) {
                            Logger.logMessage(String.format("The terminal %s was able to successfully start the printer " +
                                    "host health check on the terminal %s in BackofficeChangeUpdater.handlePrinterHostChanges",
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                    Objects.toString(printerHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.DEBUG);
                        }
                        else if (!startPrinterHostHealthCheckOnTerminal(printerHost.getMacAddress(), printerHost.getHealthCheckFrequency())) {
                            Logger.logMessage(String.format("The terminal %s was unable to successfully start the printer " +
                                    "host health check on the terminal %s in BackofficeChangeUpdater.handlePrinterHostChanges",
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                    Objects.toString(printerHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
                        }
                    }
                }
                printerHosts = currPrinterHosts;
            }
            else if ((!DataFunctions.isEmptyCollection(prevPrinterHosts)) && (DataFunctions.isEmptyCollection(currPrinterHosts))) {
                // all printer hosts were removed
                for (QCPrinterHost printerHost : prevPrinterHosts) {
                    if (printerHost != null) {
                        // stop the printer host if it's running
                        if (printerHostHealthCheckInstance.isPrinterHostInService(printerHost.getMacAddress())) {
                            printerHostHealthCheckInstance.stopPrinterHost(printerHost.getMacAddress());
                        }
                        removePrinterHostOnTerminal(printerHost.getMacAddress(), printerHost.getPrinterHostID());
                        Logger.logMessage(String.format("The printer host running on the terminal %s has been inactivated since " +
                                "the last time the check for backoffice updates was run in BackofficeChangeUpdater.handlePrinterHostChanges",
                                Objects.toString(printerHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.TRACE);
                        // stop the printer host health check on the previously configured printer host
                        if (stopPrinterHostHealthCheckOnTerminal(printerHost.getMacAddress())) {
                            Logger.logMessage(String.format("The terminal %s was able to successfully stop the printer " +
                                    "host health check on the terminal %s in BackofficeChangeUpdater.handlePrinterHostChanges",
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                    Objects.toString(printerHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.DEBUG);
                        }
                        else if (!stopPrinterHostHealthCheckOnTerminal(printerHost.getMacAddress())) {
                            Logger.logMessage(String.format("The terminal %s was unable to successfully stop the printer " +
                                    "host health check on the terminal %s in BackofficeChangeUpdater.handlePrinterHostChanges",
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                    Objects.toString(printerHost.getHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
                        }
                    }
                }
                printerHosts = new CopyOnWriteArraySet<>();
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage("There was a problem trying to process changes made in the backoffice to printer hosts " +
                    "in BackofficeChangeUpdater.handlePrinterHostChanges", BCU_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Finds changes made in the backoffice to {@link QCKitchenPrinter} since the last time the backoffice check was run.
     *
     * NOTE: Only terminals with printer hosts currently configured to on them will have their printers updated.</p>
     */
    @SuppressWarnings({"unchecked", "Convert2streamapi", "OverlyComplexMethod", "TypeMayBeWeakened"})
    private void handlePrinterChanges () {

        try {
            // get the printers that are currently configured in the backoffice
            if (!DataFunctions.isEmptyCollection(getPrinters())) {
                printers = new CopyOnWriteArraySet<>(getPrinters());
            }

            if (!DataFunctions.isEmptyCollection(printerHosts)) {
                for (QCPrinterHost printerHost : printerHosts) {
                    if (PeripheralsDataManager.getTerminalMacAddress().equalsIgnoreCase(printerHost.getMacAddress())) {
                        PrinterQueueHandler.getInstance().updatePrinters(convertPrintersToXmlRpcArray());
                    }
                    else {
                        // make XML RPC call
                        String url = XmlRpcUtil.getInstance().buildURL(printerHost.getMacAddress(), printerHost.getHostname());
                        Object[] args = new Object[]{PeripheralsDataManager.getPHHostname(), convertPrintersToXmlRpcArray()};
                        Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, printerHost.getMacAddress(), printerHost.getHostname(),
                                KitchenPrinterDataManagerMethod.UPDATE_PRINTERS.getMethodName(), args);
                        if (!XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.UPDATE_PRINTERS.getMethodName())) {
                            Logger.logMessage(String.format("There was a problem trying to update the printers on the " +
                                    "terminal %s from the terminal %s, an invalid response was received from the XML RPC call " +
                                    "to %s in BackofficeChangeUpdater.handlePrinterChanges",
                                    Objects.toString(printerHost.getHostname(), "NULL"),
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                    Objects.toString(KitchenPrinterDataManagerMethod.UPDATE_PRINTERS.getMethodName(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage("There was a problem trying to process changes made in the backoffice to printers " +
                    "in BackofficeChangeUpdater.handlePrinterChanges", BCU_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Starts the printer host health check on this terminal.</p>
     *
     * @param healthCheckFrequency How frequently the printer host health check should run.
     */
    public synchronized void startPrinterHostHealthCheck (int healthCheckFrequency) {

        try {
            if (!isPHHealthCheckRunningOnTerminal) {
                // create the Executor
                printerHostHealthCheck = Executors.newSingleThreadScheduledExecutor();

                // start the Executor
                printerHostHealthCheckInstance = new PrinterHostHealthCheck();
                printerHostHealthCheck.scheduleAtFixedRate(printerHostHealthCheckInstance, PRINTER_HOST_HEALTH_CHECK_INITAL_DELAY, healthCheckFrequency, TimeUnit.SECONDS);

                isPHHealthCheckRunningOnTerminal = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to start the printer host health check on the " +
                    "terminal %s in BackofficeChangeUpdater.startPrinterHostHealthCheck",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Stops the printer host health check on this terminal.</p>
     *
     */
    public synchronized void stopPrinterHostHealthCheck () {

        try {
            if (isPHHealthCheckRunningOnTerminal) {
                printerHostHealthCheck.shutdown();
                while (!printerHostHealthCheck.isTerminated()) {
                    Logger.logMessage("Waiting for the printer host health check to terminate operations in " +
                            "BackofficeChangeUpdater.stopPrinterHostHealthCheck", BCU_LOG, Logger.LEVEL.IMPORTANT);
                    printerHostHealthCheck.awaitTermination(1L, TimeUnit.SECONDS);
                }

                isPHHealthCheckRunningOnTerminal = false;
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to stop the printer host health check on the " +
                    "terminal %s in BackofficeChangeUpdater.stopPrinterHostHealthCheck",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Starts the printer host health check on the terminal with the given MAC address.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to start the printer host health check on.
     * @param healthCheckFrequency How frequently the printer host health check should run on the given terminal.
     * @return Whether or not the printer host health check could be started successfully on the given terminal.
     */
    public boolean startPrinterHostHealthCheckOnTerminal (String macAddress, int healthCheckFrequency) {
        boolean success = false;

        try {
            // check if the MAC address is for this terminal, if so we don't need to use XML RPC
            if (macAddress.equalsIgnoreCase(PeripheralsDataManager.getTerminalMacAddress())) {
                Logger.logMessage(String.format("The printer host health check started properly on the " +
                        "terminal %s in BackofficeChangeUpdater.startPrinterHostHealthCheckOnTerminal",
                        Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), BCU_LOG, Logger.LEVEL.DEBUG);
                startPrinterHostHealthCheck(healthCheckFrequency);
                success = true;
            }
            else {
                // make XML RPC call to the desired terminal
                String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                Object[] args = new Object[]{PeripheralsDataManager.getPHHostname(), healthCheckFrequency};

                Logger.logMessage("BackOfficeChangeUpdater.startPrinterHostHealthCheckOnTerminal: calling " + KitchenPrinterDataManagerMethod.START_PRINTER_HOST_HEALTH_CHECK.getMethodName()
                        + " using the XML RPC URL " + url + " and the mac address " + macAddress, BCU_LOG, Logger.LEVEL.DEBUG);

                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress,
                        PeripheralsDataManager.getTerminalHostname(macAddress), KitchenPrinterDataManagerMethod.START_PRINTER_HOST_HEALTH_CHECK.getMethodName(), args);
                success = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.START_PRINTER_HOST_HEALTH_CHECK.getMethodName(), PeripheralsDataManager.getPHHostname(), url);
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("A problem occurred while the terminal %s tried to start the printer host " +
                    "health check on the terminal %s in BackofficeChangeUpdater.startPrinterHostHealthCheckOnTerminal",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Stops the printer host health check on the terminal with the given MAC address.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to stop the printer host health check on.
     * @return Whether or not the printer host health check could be stopped successfully on the given terminal.
     */
    public boolean stopPrinterHostHealthCheckOnTerminal (String macAddress) {
        boolean success = false;

        try {
            // check if the MAC address is for this terminal, if so we don't need to use XML RPC
            if (macAddress.equalsIgnoreCase(PeripheralsDataManager.getTerminalMacAddress())) {
                Logger.logMessage(String.format("The printer host health check shut down properly on the " +
                        "terminal %s in BackofficeChangeUpdater.stopPrinterHostHealthCheckOnTerminal",
                        Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), BCU_LOG, Logger.LEVEL.DEBUG);
                stopPrinterHostHealthCheck();
                success = true;
            }
            else {
                // make XML RPC call to the desired terminal
                String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                Object[] args = new Object[]{PeripheralsDataManager.getPHHostname()};
                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress,
                        PeripheralsDataManager.getTerminalHostname(macAddress), KitchenPrinterDataManagerMethod.STOP_PRINTER_HOST_HEALTH_CHECK.getMethodName(), args);
                success = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.STOP_PRINTER_HOST_HEALTH_CHECK.getMethodName());
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("A problem occurred while the terminal %s tried to stop the printer host " +
                    "health check on the terminal %s in BackofficeChangeUpdater.stopPrinterHostHealthCheckOnTerminal",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Adds the given {@link QCPrinterHost} to the {@link BackofficeChangeUpdater} {@link ArrayList} of
     * {@link QCPrinterHost} on this terminal if it hasn't been added already.</p>
     *
     * @param printerHostID ID of the {@link QCPrinterHost} to add.
     * @return Whether or not the {@link QCPrinterHost} could be added successfully.
     */
    public synchronized boolean addPrinterHost (int printerHostID) {
        boolean success = false;
        QCPrinterHost ph = null;

        try {
            // check if the printer host is on this terminal already
            ph = new QCPrinterHost(PeripheralsDataManager.getPHRecFromLocalDB(printerHostID));
            if ((printerHosts != null) && (!printerHosts.contains(ph))) {
                printerHosts.add(ph);
                success = true;
            }
            else if (printerHosts == null) {
                printerHosts = new CopyOnWriteArraySet<>();
                printerHosts.add(ph);
                success = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to add the printer host running on the " +
                    "terminal %s to the list of printer hosts on the terminal %s in BackofficeChangeUpdater.addPrinterHost",
                    Objects.toString((ph != null ? ph.getHostname() : ""), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Removes the given {@link QCPrinterHost} from the {@link BackofficeChangeUpdater} {@link ArrayList} of
     * {@link QCPrinterHost} on this terminal if it hasn't been removed already.</p>
     *
     * @param printerHostID ID of the {@link QCPrinterHost} to remove.
     * @return Whether or not the {@link QCPrinterHost} could be removed successfully.
     */
    public synchronized boolean removePrinterHost (int printerHostID) {
        boolean success = false;
        QCPrinterHost ph = null;

        try {
            // check if the printer host has been removed from this terminal already
            ph = new QCPrinterHost(PeripheralsDataManager.getPHRecFromLocalDB(printerHostID));
            if ((printerHosts != null) && (printerHosts.contains(ph))) {
                printerHosts.remove(ph);
            }
            else if (printerHosts == null) {
                success = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to remove the printer host running on the " +
                    "terminal %s from the list of printer hosts on the terminal %s in BackofficeChangeUpdater.removePrinterHost",
                    Objects.toString((ph != null ? ph.getHostname() : ""), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Adds the {@link QCPrinterHost} with the given printer host ID to the {@link ArrayList} of {@link QCPrinterHost} for
     * the {@link BackofficeChangeUpdater} on the terminal with the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to add the printer host to.
     * @param printerHostID ID of the {@link QCPrinterHost} to add.
     * @return Whether or not the {@link QCPrinterHost} with the given printer host ID could be added to the
     * {@link ArrayList} of {@link QCPrinterHost} for the {@link BackofficeChangeUpdater} on the terminal with the
     * given MAC address {@link String}.
     */
    public boolean addPrinterHostOnTerminal (String macAddress, int printerHostID) {
        boolean success = false;

        try {
            // check if the MAC address is for this terminal
            if ((StringFunctions.stringHasContent(macAddress)) && (macAddress.equalsIgnoreCase(PeripheralsDataManager.getTerminalMacAddress()))) {
                success = addPrinterHost(printerHostID);
            }
            else {
                // make XML RPC call to the desired terminal
                String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                Object[] args = new Object[]{PeripheralsDataManager.getTerminalHostname(macAddress), printerHostID};
                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                        KitchenPrinterDataManagerMethod.ADD_PRINTER_HOST_TO_BACKOFFICE_UPDATER.getMethodName(), args);
                success = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.ADD_PRINTER_HOST_TO_BACKOFFICE_UPDATER.getMethodName());
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to add the printer host running on the " +
                    "terminal %s to the list of printer hosts on the terminal %s in BackofficeChangeUpdater.addPrinterHostOnTerminal",
                    Objects.toString(new QCPrinterHost(PeripheralsDataManager.getPHRecFromLocalDB(printerHostID)).getHostname(), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Removes the {@link QCPrinterHost} with the given printer host ID from the {@link ArrayList} of {@link QCPrinterHost} for
     * the {@link BackofficeChangeUpdater} on the terminal with the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to remove the printer host from.
     * @param printerHostID ID of the {@link QCPrinterHost} to remove.
     * @return Whether or not the {@link QCPrinterHost} with the given printer host ID could be removed from the
     * {@link ArrayList} of {@link QCPrinterHost} for the {@link BackofficeChangeUpdater} on the terminal with the
     * given MAC address {@link String}.
     */
    public boolean removePrinterHostOnTerminal (String macAddress, int printerHostID) {
        boolean success = false;

        try {
            // check if the MAC address is for this terminal
            if ((StringFunctions.stringHasContent(macAddress)) && (macAddress.equalsIgnoreCase(PeripheralsDataManager.getTerminalMacAddress()))) {
                success = removePrinterHost(printerHostID);
            }
            else {
                // make XML RPC call to the desired terminal
                String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                Object[] args = new Object[]{PeripheralsDataManager.getTerminalHostname(macAddress), printerHostID};
                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                        KitchenPrinterDataManagerMethod.REMOVE_PRINTER_HOST_FROM_BACKOFFICE_UPDATER.getMethodName(), args);
                success = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.REMOVE_PRINTER_HOST_FROM_BACKOFFICE_UPDATER.getMethodName());
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to remove the printer host running on the " +
                    "terminal %s from the list of printer hosts on the terminal %s in BackofficeChangeUpdater.removePrinterHostOnTerminal",
                    Objects.toString(new QCPrinterHost(PeripheralsDataManager.getPHRecFromLocalDB(printerHostID)).getHostname(), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Adds the {@link QCKitchenPrinter} with the given printer ID to the {@link BackofficeChangeUpdater} {@link ArrayList} of
     * {@link QCKitchenPrinter} on this terminal if it hasn't been removed already.</p>
     *
     * @param printerID ID of the {@link QCKitchenPrinter} to add.
     * @return Whether or not the {@link QCKitchenPrinter} could be added successfully.
     */
    public synchronized boolean addPrinter (int printerID) {
        boolean success = false;
        QCKitchenPrinter ptr = null;

        try {
            ptr = new QCKitchenPrinter(PeripheralsDataManager.getPtrRecFromLocalDB(printerID));
            if ((printers != null) && (!printers.contains(ptr))) {
                printers.add(ptr);
                success = true;
            }
            else if (printers == null) {
                printers = new CopyOnWriteArraySet<>();
                printers.add(ptr);
                success = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to add the printer %s to the list of " +
                    "printers on the terminal %s in BackofficeChangeUpdater.addPrinter",
                    Objects.toString((ptr != null ? ptr.getName() : ""), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Removes the {@link QCKitchenPrinter} with the given printer ID from the {@link BackofficeChangeUpdater} {@link ArrayList} of
     * {@link QCKitchenPrinter} on this terminal if it hasn't been removed already.</p>
     *
     * @param printerID ID of the {@link QCKitchenPrinter} to remove.
     * @return Whether or not the {@link QCKitchenPrinter} could be removed successfully.
     */
    public synchronized boolean removePrinter (int printerID) {
        boolean success = false;
        QCKitchenPrinter ptr = null;

        try {
            ptr = new QCKitchenPrinter(PeripheralsDataManager.getPtrRecFromLocalDB(printerID));
            if ((printers != null) && (printers.contains(ptr))) {
                printers.remove(ptr);
                success = true;
            }
            else if (printers == null) {
                success = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to remove the printer %s from the list of " +
                    "printers on the terminal %s in BackofficeChangeUpdater.removePrinter",
                    Objects.toString((ptr != null ? ptr.getName() : ""), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Adds the {@link QCKitchenPrinter} with the given printer ID to the {@link ArrayList} of {@link QCKitchenPrinter} for
     * the {@link BackofficeChangeUpdater} on the terminal with the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to add the printer to.
     * @param printerID ID of the {@link QCKitchenPrinter} to add.
     * @return Whether or not the {@link QCKitchenPrinter} with the given printer ID could be added to the
     * {@link ArrayList} of {@link QCKitchenPrinter} for the {@link BackofficeChangeUpdater} on the terminal with the
     * given MAC address {@link String}.
     */
    public boolean addPrinterOnTerminal (String macAddress, int printerID) {
        boolean success = false;

        try {
            // check if the MAC address is for this terminal
            if ((StringFunctions.stringHasContent(macAddress)) && (macAddress.equalsIgnoreCase(PeripheralsDataManager.getTerminalMacAddress()))) {
                success = addPrinter(printerID);
            }
            else if ((StringFunctions.stringHasContent(macAddress)) && (!macAddress.equalsIgnoreCase(PeripheralsDataManager.getTerminalMacAddress()))) {
                // make XML RPC call to the desired terminal
                String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                Object[] args = new Object[]{PeripheralsDataManager.getTerminalHostname(macAddress), printerID};
                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                        KitchenPrinterDataManagerMethod.ADD_PRINTER_TO_BACKOFFICE_UPDATER.getMethodName(), args);
                success = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.ADD_PRINTER_TO_BACKOFFICE_UPDATER.getMethodName());
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to add the printer %s to the list of " +
                    "printers on the terminal %s in BackofficeChangeUpdater.addPrinterOnTerminal",
                    Objects.toString(new QCKitchenPrinter(PeripheralsDataManager.getPtrRecFromLocalDB(printerID)).getName(), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Removes the {@link QCKitchenPrinter} with the given printer ID from the {@link ArrayList} of {@link QCKitchenPrinter} for
     * the {@link BackofficeChangeUpdater} on the terminal with the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to remove the printer from.
     * @param printerID ID of the {@link QCKitchenPrinter} to remove.
     * @return Whether or not the {@link QCKitchenPrinter} with the given printer ID could be removed from the
     * {@link ArrayList} of {@link QCKitchenPrinter} for the {@link BackofficeChangeUpdater} on the terminal with the
     * given MAC address {@link String}.
     */
    public boolean removePrinterOnTerminal (String macAddress, int printerID) {
        boolean success = false;

        try {
            // check if the MAC address is for this terminal
            if ((StringFunctions.stringHasContent(macAddress)) && (macAddress.equalsIgnoreCase(PeripheralsDataManager.getTerminalMacAddress()))) {
                success = removePrinter(printerID);
            }
            else if ((StringFunctions.stringHasContent(macAddress)) && (!macAddress.equalsIgnoreCase(PeripheralsDataManager.getTerminalMacAddress()))) {
                // make XML RPC call to the desired terminal
                String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                Object[] args = new Object[]{PeripheralsDataManager.getTerminalHostname(macAddress), printerID};
                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                        KitchenPrinterDataManagerMethod.REMOVE_PRINTER_FROM_BACKOFFICE_UPDATER.getMethodName(), args);
                success = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.REMOVE_PRINTER_FROM_BACKOFFICE_UPDATER.getMethodName());
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage(String.format("There was a problem trying to remove the printer %s from the list of " +
                    "printers on the terminal %s in BackofficeChangeUpdater.removePrinterOnTerminal",
                    Objects.toString(new QCKitchenPrinter(PeripheralsDataManager.getPtrRecFromLocalDB(printerID)).getName(), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), BCU_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Utility method to convert the {@link CopyOnWriteArraySet} of {@link QCKitchenPrinter} to a {@link XmlRpcArray} of {@link XmlRpcStruct}.</p>
     *
     * @return The {@link CopyOnWriteArraySet} of {@link QCKitchenPrinter} as a {@link XmlRpcArray} of {@link XmlRpcStruct}.
     */
    @SuppressWarnings({"Convert2streamapi", "unchecked"})
    public XmlRpcArray convertPrintersToXmlRpcArray () {
        XmlRpcArray printersXmlRpcArray = new XmlRpcArray();

        try {
            if (!DataFunctions.isEmptyCollection(printers)) {
                for (QCKitchenPrinter printer : printers) {
                    if ((printer != null) && (!DataFunctions.isEmptyMap(printer.getXmlRpcStruct()))) {
                        printersXmlRpcArray.add(printer.getXmlRpcStruct());
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, BCU_LOG);
            Logger.logMessage("There was a problem trying to convert the printers into a XmlRpcArray in " +
                    "BackofficeChangeUpdater.convertPrintersToXmlRpcArray", BCU_LOG, Logger.LEVEL.ERROR);
        }

        return printersXmlRpcArray;
    }

}