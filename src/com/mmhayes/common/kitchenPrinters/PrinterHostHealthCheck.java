package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-04-26 10:59:25 -0400 (Mon, 26 Apr 2021) $: Date of last commit
    $Rev: 13847 $: Revision of last commit
    Notes: Polls all configured printer hosts and ensures there is always an in-service printer host.
*/

import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.xmlapi.XmlRpcUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * <p>Polls all configured printer hosts and ensures there is always an in-service printer host.</p>
 *
 */
public class PrinterHostHealthCheck implements Runnable {

    // log file specific to the PrinterHostHealthCheck
    private static final String PHHC_LOG = "KP_PrinterHostHealthCheck.log";

    // public member variables of a PrinterHostHealthCheck
    public static final long FIVE_SECONDS = 5000L;

    /**
     * <p>Overridden run method for a {@link PrinterHostHealthCheck}.</p>
     *
     */
    @Override
    public void run () {
        performPrinterHostHealthCheck();
    }

    /**
     * <p>Updates the printer host status tracker for the this terminal with the latest status of each printer host.</p>
     *
     */
    @SuppressWarnings("Convert2streamapi")
    public void updatePrinterHostStatusTracker () {
        String currentMacAddress = "";

        try {
            // remove any printer hosts that shouldn't be in the printer host status tracker
            removePrinterHostsFromTracker();

            // get the status of each printer host and update the printer host status tracker
            CopyOnWriteArraySet<QCPrinterHost> printerHosts = getPrinterHosts();
            if (!DataFunctions.isEmptyCollection(printerHosts)) {
                for (QCPrinterHost printerHost : printerHosts) {
                    currentMacAddress = printerHost.getMacAddress();
                    if (!isTerminalRunningOnGivenMac(printerHost.getMacAddress())) {
                        // make XML RPC call
                        String url = XmlRpcUtil.getInstance().buildURL(printerHost.getMacAddress(), PeripheralsDataManager.getTerminalHostname(printerHost.getMacAddress()));
                        Object[] args = new Object[]{PeripheralsDataManager.getPHHostname()};
                        Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, printerHost.getMacAddress(), PeripheralsDataManager.getTerminalHostname(printerHost.getMacAddress()),
                                KitchenPrinterDataManagerMethod.GET_PRINTER_HOST_STATUS.getMethodName(), args);
                        String parsedXmlRpcResponse = XmlRpcUtil.getInstance().parseStringXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.GET_PRINTER_HOST_STATUS.getMethodName());
                        if ((StringFunctions.stringHasContent(parsedXmlRpcResponse)) && (PrinterHostStatus.getPHStatusFromString(parsedXmlRpcResponse) != null)) {
                            // update the printer host status tracker with the response received from the XML PRC call
                            KitchenPrinterTerminal.getInstance().addToPrinterHostStatusTracker(printerHost.getMacAddress(), PrinterHostStatus.getPHStatusFromString(parsedXmlRpcResponse));
                        }
                        else {
                            // a problem occurred use an unresponsive status
                            KitchenPrinterTerminal.getInstance().addToPrinterHostStatusTracker(printerHost.getMacAddress(), PrinterHostStatus.UNRESPONSIVE);
                        }
                    }
                }
            }

            // log the printer host status tracker
            logPrinterHostsStatusTracker();
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to update the printer host status tracker with " +
                    "the status of every printer host on the terminal %s in PrinterHostHealthCheck.updatePrinterHostStat usTracker",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
            // a problem occurred use an unresponsive status
            KitchenPrinterTerminal.getInstance().addToPrinterHostStatusTracker(currentMacAddress, PrinterHostStatus.UNRESPONSIVE);
        }

    }

    /**
     * <p>Utility method to remove irrelevant printer host entries from the printer host status tracker on this terminal.</p>
     *
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public void removePrinterHostsFromTracker () {

        try {
            if ((!DataFunctions.isEmptyCollection(getPrinterHosts())) && (!DataFunctions.isEmptyMap(KitchenPrinterTerminal.getInstance().getPrinterHostStatusTracker()))) {
                // keep track of which entries to remove from the printer host status tracker
                ArrayList<String> printerHostEntriesToRemoveFromTracker = new ArrayList<>();
                for (String macAddress : KitchenPrinterTerminal.getInstance().getPrinterHostStatusTracker().keySet()) {
                    boolean printerHostsHaveMacAddress = false;
                    for (QCPrinterHost printerHost : getPrinterHosts()) {
                        if (printerHost.getMacAddress().equalsIgnoreCase(macAddress)) {
                            printerHostsHaveMacAddress = true;
                            break;
                        }
                    }

                    // if the MAC address from the printer host status tracker isn't in the printer hosts then it should be removed from the printer host status tracker
                    if (!printerHostsHaveMacAddress) {
                        printerHostEntriesToRemoveFromTracker.add(macAddress);
                    }
                }

                // remove entries that should be removed from the printer host status tracker
                if (!DataFunctions.isEmptyCollection(printerHostEntriesToRemoveFromTracker)) {
                    for (String macAddress : printerHostEntriesToRemoveFromTracker) {
                        KitchenPrinterTerminal.getInstance().getPrinterHostStatusTracker().remove(macAddress);
                        Logger.logMessage(String.format("The printer host running on the terminal %s has been removed " +
                                "from the printer host status tracker on the terminal %s in PrinterHostHealthCheck.removePrinterHostsFromTracker",
                                Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL"),
                                Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.TRACE);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to remove irrelevant printer host entries from " +
                    "the printer host status tracker on the terminal %s in PrinterHostHealthCheck.removePrinterHostsFromTracker",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Utility method to log the printer host status tracker.</p>
     *
     */
    public void logPrinterHostsStatusTracker () {

        try {
            HashMap<PrinterHostStatus, ArrayList<String>> reorganizedPrinterHosts = reorganizePrinterHostsByStatus();
            if (!DataFunctions.isEmptyMap(reorganizedPrinterHosts)) {
                Logger.logMessage("***** START LOGGING PRINTER HOST STATUS TRACKER *****", PHHC_LOG, Logger.LEVEL.DEBUG);
                reorganizedPrinterHosts.forEach((printerHostStatus, commonStatusPrinterHosts) -> {
                    String commonStatusPrinterHostsString = "";
                    if (!DataFunctions.isEmptyCollection(commonStatusPrinterHosts)) {
                        for (String macAddress : commonStatusPrinterHosts) {
                            commonStatusPrinterHostsString += PeripheralsDataManager.getTerminalHostname(macAddress)+",";
                        }
                    }
                    if (StringFunctions.stringHasContent(commonStatusPrinterHostsString)) {
                        // remove the last comma from the commonStatusPrinterHostsString
                        commonStatusPrinterHostsString = commonStatusPrinterHostsString.substring(0, commonStatusPrinterHostsString.length() -1);
                        Logger.logMessage(String.format("PRINTER HOST STATUS OF %s: PRINTER HOSTS -> %s",
                                Objects.toString(printerHostStatus.getPrinterHostStatus(), "NULL"),
                                Objects.toString(commonStatusPrinterHostsString, "NULL")), PHHC_LOG, Logger.LEVEL.DEBUG);
                    }
                });
                Logger.logMessage("***** END LOGGING PRINTER HOST STATUS TRACKER *****", PHHC_LOG, Logger.LEVEL.DEBUG);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage("There was a problem trying to log the printer host status tracker in " +
                    "PrinterHostHealthCheck.logPrinterHostStatusTracker", PHHC_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Utility method to get the printer host that was in-service the last time the printer host health check ran.</p>
     *
     * @return A {@link HashMap} containing the MAC address {@link String} of the terminal running the in-service
     * printer host and a {@link LocalDateTime} of when the in-service printer host became the in-service printer host.
     */
    @SuppressWarnings("OverlyComplexMethod")
    public HashMap<String, LocalDateTime> getLastInServicePrinterHost () {
        HashMap<String, LocalDateTime> lastInServicePrinterHost = new HashMap<>();

        try {
            HashMap<String, LocalDateTime> allInServicePrinterHosts = getAllInServicePrinterHosts();
            // determine which of these reported in-service printer hosts was the printer host that was in-service the last time the printer host health check ran
            if ((!DataFunctions.isEmptyMap(allInServicePrinterHosts)) && (allInServicePrinterHosts.size() > 1)) {
                String macAddressOfTerminalWithOldestBecameInServiceTime = "";
                LocalDateTime oldestBecameInServiceTime = null;
                for (Map.Entry<String, LocalDateTime> allInServicePrinterHostsEntry : allInServicePrinterHosts.entrySet()) {
                    if ((StringFunctions.stringHasContent(allInServicePrinterHostsEntry.getKey())) && (allInServicePrinterHostsEntry.getValue() != null) && (oldestBecameInServiceTime == null)) {
                        macAddressOfTerminalWithOldestBecameInServiceTime = allInServicePrinterHostsEntry.getKey();
                        oldestBecameInServiceTime = allInServicePrinterHostsEntry.getValue();
                    }
                    else if ((StringFunctions.stringHasContent(allInServicePrinterHostsEntry.getKey())) && (allInServicePrinterHostsEntry.getValue() != null)
                            && (oldestBecameInServiceTime != null) && (allInServicePrinterHostsEntry.getValue().isBefore(oldestBecameInServiceTime))) {
                        macAddressOfTerminalWithOldestBecameInServiceTime = allInServicePrinterHostsEntry.getKey();
                        oldestBecameInServiceTime = allInServicePrinterHostsEntry.getValue();
                    }
                }

                // return the printer host that was in-service the last time the printer host health check ran
                if ((StringFunctions.stringHasContent(macAddressOfTerminalWithOldestBecameInServiceTime)) && (oldestBecameInServiceTime != null)) {
                    lastInServicePrinterHost.put(macAddressOfTerminalWithOldestBecameInServiceTime, oldestBecameInServiceTime);
                }
            }
            else if ((!DataFunctions.isEmptyMap(allInServicePrinterHosts)) && (allInServicePrinterHosts.size() == 1)) {
                lastInServicePrinterHost = allInServicePrinterHosts;
            }

            if (!DataFunctions.isEmptyMap(lastInServicePrinterHost)) {
                String inServiceMacAddress = lastInServicePrinterHost.entrySet().iterator().next().getKey();
                LocalDateTime inServiceTime = lastInServicePrinterHost.entrySet().iterator().next().getValue();
                Logger.logMessage(String.format("The terminal %s reports at %s that the printer host that was in-service the last time the printer host health check ran was the " +
                        "printer host running on the terminal %s which became the in-service printer host at %s in PrinterHostHealthCheck.getLastInServicePrinterHost",
                        Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                        Objects.toString(LocalDateTime.now(), "NULL"),
                        Objects.toString(PeripheralsDataManager.getTerminalHostname(inServiceMacAddress), "NULL"),
                        Objects.toString(inServiceTime, "NULL")), PHHC_LOG, Logger.LEVEL.TRACE);
            }
            else {
                Logger.logMessage(String.format("The terminal %s reports at %s that there wasn't an in-service printer host the last " +
                        "time the printer host health check ran in PrinterHostHealthCheck.getLastInServicePrinterHost",
                        Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                        Objects.toString(LocalDateTime.now(), "NULL")), PHHC_LOG, Logger.LEVEL.TRACE);
            }

        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage("There was a problem trying to determine the printer host that was in-service the last time " +
                    "the printer host health check ran in PrinterHostHealthCheck.getLastInServicePrinterHost", PHHC_LOG, Logger.LEVEL.ERROR);
        }

        return lastInServicePrinterHost;
    }

    /**
     * <p>Performs logic to ensure that there is always an in-service printer host.</p>
     *
     */
    @SuppressWarnings("OverlyComplexMethod")
    public void performPrinterHostHealthCheck () {

        try {
            // update the printer host status tracker on this terminal
            updatePrinterHostStatusTracker();

            // get the printer host that was in-service the last time the printer host health check was run
            HashMap<String, LocalDateTime> lastInServicePrinterHost = getLastInServicePrinterHost();

            if (DataFunctions.isEmptyMap(lastInServicePrinterHost)) {
                processNoPreviousInServicePrinterHost();
            }
            else {
                processPreviousInServicePrinterHost(lastInServicePrinterHost);
            }

            String lastInSvcPHMac = "";
            if ((!DataFunctions.isEmptyMap(lastInServicePrinterHost)) && (StringFunctions.stringHasContent(lastInServicePrinterHost.entrySet().iterator().next().getKey()))) {
                lastInSvcPHMac = lastInServicePrinterHost.entrySet().iterator().next().getKey();
            }
            else {
                Logger.logMessage("No previous in-service printer host was found in PrinterHostHealthCheck.performPrinterHostHealthCheck, the application is either " +
                        "starting up or an error occurred to prevent the determination of the previous in-service printer host.", PHHC_LOG, Logger.LEVEL.TRACE);
            }

            String currInSvcPHMac = "";
            if ((KitchenPrinterTerminal.getInstance() != null) && (StringFunctions.stringHasContent(KitchenPrinterTerminal.getInstance().getActivePHMacAddress()))) {
                currInSvcPHMac = KitchenPrinterTerminal.getInstance().getActivePHMacAddress();
            }
            else {
                Logger.logMessage("No current in-service printer host was found in PrinterHostHealthCheck.performPrinterHostHealthCheck, an error occurred to " +
                        "prevent the determination of the current in-service printer host.", PHHC_LOG, Logger.LEVEL.ERROR);
            }

            // check whether or not the in service printer host has changed and we should update it on the server
            if ((StringFunctions.stringHasContent(lastInSvcPHMac)) && (StringFunctions.stringHasContent(currInSvcPHMac)) && (!lastInSvcPHMac.equalsIgnoreCase(currInSvcPHMac))) {
                // update the in-service printer host on the server
                updateInServicePrinterHost(KitchenPrinterTerminal.getInstance().getActivePHMacAddress());
            }
            else if ((!StringFunctions.stringHasContent(lastInSvcPHMac)) && (StringFunctions.stringHasContent(currInSvcPHMac))) {
                // update the in-service printer host on the server
                updateInServicePrinterHost(KitchenPrinterTerminal.getInstance().getActivePHMacAddress());
            }

        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("A problem occurred while trying to perform the printer host health check " +
                    "on the terminal %s in PrinterHostHealthCheck.performPrinterHostHealthCheck",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Called by the printer host health check to determine which printer host should be in-service when there wasn't
     * a printer host that was in service the last time that the printer host health check ran.</p>
     *
     */
    public void processNoPreviousInServicePrinterHost () {

        try {
            Logger.logMessage(String.format("There was no printer host that was in-service the last time the printer " +
                    "host health check ran on the terminal %s",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.TRACE);

            // get the MAC address of the printer host configured as the primary in the backoffice
            String primaryMacAddress = KitchenPrinterCommon.getPrimaryPrinterHost(PeripheralsDataManager.getTerminalMacAddress(), PHHC_LOG);
            if (StringFunctions.stringHasContent(primaryMacAddress)) {
                // check if the primary printer host is in-service
                if (isPrinterHostInService(primaryMacAddress)) {
                    // stop any other printer hosts that report being in-service
                    stopInServicePrinterHostsThatShouldntBe(primaryMacAddress);
                    // set the printer host configured as the primary to be the in-service printer host on all terminals
                    setInServicePrinterHostOnAllPrinterHostTerminals(primaryMacAddress);
                }
                else {
                    // try to make this printer host the in-service printer host on all terminals
                    stopInServicePrinterHostsThatShouldntBe(PeripheralsDataManager.getTerminalMacAddress());
                    setInServicePrinterHostOnAllPrinterHostTerminals(PeripheralsDataManager.getTerminalMacAddress());
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to process having no in-service printer host " +
                    "the last time the printer host health check ran on the terminal %s in " +
                    "PrinterHostHealthCheck.processNoPreviousInServicePrinterHost",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Called by the printer host health check to determine which printer host should be in-service when there was a
     * printer host that was in service the last time that the printer host health check ran.</p>
     *
     * @param inServicePrinterHost The {@link HashMap} containing the MAC address {@link String} of and the
     * {@link LocalDateTime} that the last in-service printer host became the in-service printer host.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public void processPreviousInServicePrinterHost (HashMap<String, LocalDateTime> inServicePrinterHost) {

        try {
            if (!DataFunctions.isEmptyMap(inServicePrinterHost)) {
                String macAddressOfLastInServicePrinterHost = inServicePrinterHost.entrySet().iterator().next().getKey();
                LocalDateTime timeLastInServicePrinterHostBecameInService = inServicePrinterHost.entrySet().iterator().next().getValue();
                if ((StringFunctions.stringHasContent(macAddressOfLastInServicePrinterHost)) && (timeLastInServicePrinterHostBecameInService != null)) {

                    Logger.logMessage(String.format("The printer host running on the terminal %s was the printer host that " +
                            "was in-service the last time the printer host health check ran on the terminal %s",
                            Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddressOfLastInServicePrinterHost), "NULL"),
                            Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);

                    // check if the last reported in-service printer host is still in-service
                    if (isPrinterHostInService(macAddressOfLastInServicePrinterHost)) {
                        // stop any other printer hosts that report being in-service
                        stopInServicePrinterHostsThatShouldntBe(macAddressOfLastInServicePrinterHost);
                        setInServicePrinterHostOnAllPrinterHostTerminals(macAddressOfLastInServicePrinterHost, timeLastInServicePrinterHostBecameInService);
                    }
                    else {
                        boolean ableToRestart = restartInServicePrinterHost(macAddressOfLastInServicePrinterHost);
                        if (!ableToRestart) {
                            // try to make this printer host the in-service printer host on all terminals
                            stopInServicePrinterHostsThatShouldntBe(PeripheralsDataManager.getTerminalMacAddress());
                            setInServicePrinterHostOnAllPrinterHostTerminals(PeripheralsDataManager.getTerminalMacAddress());
                        }
                        else {
                            // stop any other printer hosts that may have tried to take over while restarting the printer host
                            stopInServicePrinterHostsThatShouldntBe(macAddressOfLastInServicePrinterHost);
                        }
                    }
                }
                else {
                    // try to make this printer host the in-service printer host on all terminals
                    stopInServicePrinterHostsThatShouldntBe(PeripheralsDataManager.getTerminalMacAddress());
                    setInServicePrinterHostOnAllPrinterHostTerminals(PeripheralsDataManager.getTerminalMacAddress());
                }
            }
            else {
                // try to make this printer host the in-service printer host on all terminals
                stopInServicePrinterHostsThatShouldntBe(PeripheralsDataManager.getTerminalMacAddress());
                setInServicePrinterHostOnAllPrinterHostTerminals(PeripheralsDataManager.getTerminalMacAddress());
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to process having the in-service printer host " +
                    "running on the terminal %s the last time the printer host health check ran on the terminal %s in " +
                    "PrinterHostHealthCheck.processPreviousInServicePrinterHost",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(inServicePrinterHost.entrySet().iterator().next().getKey()), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Updates the in-service printer host on the server.</p>
     *
     * @param macAddress The MAC address {@link String} of the in-service printer host.
     */
    public void updateInServicePrinterHost (String macAddress) {

        try {
            if (StringFunctions.stringHasContent(macAddress)) {

                Logger.logMessage(String.format("The terminal %s is attempting to set the in-service " +
                        "printer host on the server as the printer host running on the terminal %s at %s in PrinterHostHealthCheck.updateInServicePrinterHost",
                        Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                        Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL"),
                        Objects.toString(LocalDateTime.now(), "NULL")), PHHC_LOG, Logger.LEVEL.DEBUG);

                String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
                Object[] args = new Object[]{PeripheralsDataManager.getPHHostname(), macAddress};
                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, PeripheralsDataManager.getTerminalMacAddress(), PeripheralsDataManager.getPHHostname(),
                        KitchenPrinterDataManagerMethod.UPDATE_IN_SERVICE_PRINTER_HOST_ON_SERVER.getMethodName(), args);
                if (!XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.UPDATE_IN_SERVICE_PRINTER_HOST_ON_SERVER.getMethodName())) {
                    Logger.logMessage(String.format("Unable to set the in-service printer host as the printer host running " +
                            "on the terminal %s due to an invalid response received from the XML RPC call in PrinterHostHealthCheck.updateInServicePrinterHost",
                            Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
                }
                else {
                    Logger.logMessage(String.format("The terminal %s was able to successfully set the in-service printer host " +
                            "on the server as the printer host running on the terminal %s at %s in PrinterHostHealthCheck.updateInServicePrinterHost",
                            Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                            Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL"),
                            Objects.toString(LocalDateTime.now(), "NULL")), PHHC_LOG, Logger.LEVEL.DEBUG);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to set the in-service printer host on the server " +
                    "as the printer host running on the terminal %s in PrinterHostHealthCheck.updateInServicePrinterHost",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Utility method to consolidate all the printer hosts that report being in-service.</p>
     *
     * @return A {@link HashMap} containing the MAC address {@link String} of the terminal running the in-service
     * printer host and a {@link LocalDateTime} of when the in-service printer host became the in-service printer host
     * for all printer hosts that report being in-service.
     */
    public HashMap<String, LocalDateTime> getAllInServicePrinterHosts () {
        HashMap<String, LocalDateTime> allInServicePrinterHosts = new HashMap<>();

        try {
            CopyOnWriteArraySet<QCPrinterHost> printerHosts = getPrinterHosts();
            if (!DataFunctions.isEmptyCollection(printerHosts)) {
                for (QCPrinterHost printerHost : printerHosts) {
                    // get the in-service printer host in the terminal
                    HashMap<String, LocalDateTime> inServicePrinterHost = getInServicePrinterHostOnTerminal(printerHost.getMacAddress());
                    if (!DataFunctions.isEmptyMap(inServicePrinterHost)) {
                        String inServiceMacAddress = inServicePrinterHost.entrySet().iterator().next().getKey();
                        LocalDateTime inServiceTime = inServicePrinterHost.entrySet().iterator().next().getValue();
                        if ((StringFunctions.stringHasContent(inServiceMacAddress)) && (inServiceTime != null)) {
                            if (allInServicePrinterHosts.containsKey(inServiceMacAddress)) {
                                // use the oldest reported became in-service time
                                LocalDateTime currentOldestBecameInServiceTime = allInServicePrinterHosts.get(inServiceMacAddress);
                                if (inServiceTime.isBefore(currentOldestBecameInServiceTime)) {
                                    allInServicePrinterHosts.put(inServiceMacAddress, inServiceTime);
                                }
                            }
                            else {
                                allInServicePrinterHosts.put(inServiceMacAddress, inServiceTime);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage("There was a problem trying to determine all the printer hosts that reported being " +
                    "in-service in PrinterHostHealthCheck.getAllInServicePrinterHosts", PHHC_LOG, Logger.LEVEL.ERROR);
        }

        return allInServicePrinterHosts;
    }

    public boolean isTerminalRunningOnGivenMac(String macAddress) {
        Logger.logMessage("Terminal mac address is: " + PeripheralsDataManager.getTerminalMacAddress(), PHHC_LOG, Logger.LEVEL.DEBUG);
        return (PeripheralsDataManager.getTerminalMacAddress().equalsIgnoreCase(macAddress));
    }

    /**
     * <p>Utility method to determine whether or not the in service printer host is running on the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the printer host to check is running on.
     * @return Whether or not the printer host with the given MAC address {@link String} is the in service printer host.
     */
    public boolean isInServicePrinterHostRunningOnGivenMac(String macAddress) {
        Logger.logMessage("In service printer host mac address is: " + PeripheralsDataManager.getInServicePHMacAddress(), PHHC_LOG, Logger.LEVEL.DEBUG);
        return (PeripheralsDataManager.getInServicePHMacAddress().equalsIgnoreCase(macAddress));
    }

    /**
     * <p>Utility method to get the printer hosts that this terminal could potentially send a print job to from the backoffice change updater thread.</p>
     *
     * @return A {@link CopyOnWriteArraySet} of {@link QCPrinterHost} that this terminal could potentially send print jobs to.
     */
    public CopyOnWriteArraySet<QCPrinterHost> getPrinterHosts () {
        return BackofficeChangeUpdater.getInstance().getPrinterHostsFromBackofficeUpdaterThread();
    }

    /**
     * <p>Utility method to reorganize the printer hosts in the printer host status tracker by their status.</p>
     *
     * @return The printer hosts in the printer host status tracker reorganized by their status.
     */
    @SuppressWarnings("UnusedAssignment")
    public HashMap<PrinterHostStatus, ArrayList<String>> reorganizePrinterHostsByStatus () {
        HashMap<PrinterHostStatus, ArrayList<String>> reorganizedPrinterHosts = new HashMap<>();
        ArrayList<String> commonStatusPrinterHosts = null;

        try {
            ConcurrentHashMap<String, PrinterHostStatus> printerHostStatusTracker = KitchenPrinterTerminal.getInstance().getPrinterHostStatusTracker();
            if (!DataFunctions.isEmptyMap(printerHostStatusTracker)) {
                for (Map.Entry<String, PrinterHostStatus> printerHostStatusTrackerEntry : printerHostStatusTracker.entrySet()) {
                    String macAddress = printerHostStatusTrackerEntry.getKey();
                    PrinterHostStatus printerHostStatus = printerHostStatusTrackerEntry.getValue();
                    if (reorganizedPrinterHosts.containsKey(printerHostStatus)) {
                        commonStatusPrinterHosts = reorganizedPrinterHosts.get(printerHostStatus);
                    }
                    else {
                        commonStatusPrinterHosts = new ArrayList<>();
                    }
                    commonStatusPrinterHosts.add(macAddress);
                    reorganizedPrinterHosts.put(printerHostStatus, commonStatusPrinterHosts);
                }
            }
            else {
                Logger.logMessage("Unable to reorganize printer hosts in the printer host status tracker by their " +
                        "status, no printers were found within the printer host status tracker", PHHC_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to reorganize the printer hosts in the printer " +
                    "host status tracker by their status on the terminal %s in PrinterHostHealthCheck.reorganizePrinterHostsByStatus",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

        return reorganizedPrinterHosts;
    }

    /**
     * <p>Utility method to start the printer host on the terminal with the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to start the printer host on.
     */
    public void startPrinterHost (String macAddress) {

        try {
            if (StringFunctions.stringHasContent(macAddress)) {
                // make sure the MAC address of the terminal running the printer host is valid
                boolean isValidPrinterHostMacAddress = false;
                if (!DataFunctions.isEmptyCollection(BackofficeChangeUpdater.getInstance().getPrinterHostsFromBackofficeUpdaterThread())) {
                    for (QCPrinterHost printerHost : BackofficeChangeUpdater.getInstance().getPrinterHostsFromBackofficeUpdaterThread()) {
                        if (printerHost.getMacAddress().equalsIgnoreCase(macAddress)) {
                            isValidPrinterHostMacAddress = true;
                            break;
                        }
                    }
                }

                if (isValidPrinterHostMacAddress) {
                    if (isTerminalRunningOnGivenMac(macAddress)) {
                        PrinterHostThread.getInstance().startPrinterHost();

                        // wait a little for the printer host to start up
                        Thread.sleep(FIVE_SECONDS);

                        // check whether or not the printer host could be started properly
                        if (PrinterHost.getInstance().isPrinterHostActive()) {
                            Logger.logMessage(String.format("The terminal %s was able to successfully start the printer " +
                                    "host running on itself in PrinterHostHealthCheck.startPrinterHost",
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.TRACE);
                        }
                        else {
                            Logger.logMessage(String.format("The terminal %s was unable to successfully start the printer " +
                                    "host running on itself in PrinterHostHealthCheck.startPrinterHost",
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
                        }
                    }
                    else {
                        // make XML RPC call
                        String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                        Object[] args = new Object[]{PeripheralsDataManager.getPHHostname()};
                        Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                                KitchenPrinterDataManagerMethod.START_PRINTER_HOST.getMethodName(), args);
                        boolean parsedXmlRpcResponse = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.START_PRINTER_HOST.getMethodName());

                        // check whether or not the printer host could be started properly
                        if (parsedXmlRpcResponse) {
                            Logger.logMessage(String.format("The terminal %s was able to successfully start the printer " +
                                    "host running on the terminal %s in PrinterHostHealthCheck.startPrinterHost",
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.TRACE);
                        }
                        else {
                            Logger.logMessage(String.format("The terminal %s was unable to successfully start the printer " +
                                    "host running on the terminal %s in PrinterHostHealthCheck.startPrinterHost",
                                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to start the printer host running on the " +
                    "terminal %s from the terminal %s in PrinterHostHealthCheck.startPrinterHost",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Utility method to stop the printer host on the terminal with the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to stop the printer host on.
     */
    public void stopPrinterHost (String macAddress) {

        try {
            if (StringFunctions.stringHasContent(macAddress)) {
                if (isTerminalRunningOnGivenMac(macAddress)) {
                    PrinterHostThread.getInstance().stopPrinterHost();

                    // wait a little for the printer host to stop
                    Thread.sleep(FIVE_SECONDS);

                    // check whether or not the printer host could be stopped properly
                    if (!PrinterHost.getInstance().isPrinterHostActive()) {
                        Logger.logMessage(String.format("The terminal %s was able to successfully stop the printer " +
                                "host running on itself in PrinterHostHealthCheck.stopPrinterHost",
                                Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.TRACE);
                    }
                    else {
                        Logger.logMessage(String.format("The terminal %s was unable to successfully stop the printer " +
                                "host running on itself in PrinterHostHealthCheck.stopPrinterHost",
                                Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
                    }
                }
                else {
                    // make XML RPC call
                    String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                    Object[] args = new Object[]{PeripheralsDataManager.getPHHostname()};
                    Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                            KitchenPrinterDataManagerMethod.STOP_PRINTER_HOST.getMethodName(), args);
                    boolean parsedXmlRpcResponse = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.STOP_PRINTER_HOST.getMethodName());

                    // check whether or not the printer host could be stopped properly
                    if (parsedXmlRpcResponse) {
                        Logger.logMessage(String.format("The terminal %s was able to successfully stop the printer " +
                                "host running on the terminal %s in PrinterHostHealthCheck.stopPrinterHost",
                                Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.TRACE);
                    }
                    else {
                        Logger.logMessage(String.format("The terminal %s was unable to successfully stop the printer " +
                                "host running on the terminal %s in PrinterHostHealthCheck.stopPrinterHost",
                                Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                                Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to stop the printer host running on the " +
                    "terminal %s from the terminal %s in PrinterHostHealthCheck.stopPrinterHost",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Utility method to check whether or not the printer host with the given MAC address {@link String} is in-service.</p>
     *
     * @param macAddress MAC address {@link String} of the terminal running the printer host to check.
     * @return Whether or not the printer host running on the terminal with the given MAC address {@link String} is in-service.
     */
    public boolean isPrinterHostInService (String macAddress) {
        boolean inService = false;

        try {
            if (StringFunctions.stringHasContent(macAddress)) {
                if (isTerminalRunningOnGivenMac(macAddress)) {
                    inService = PrinterHost.getInstance().isPrinterHostActive();
                }
                else {
                    // make XML RPC call
                    String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                    Object[] args = new Object[]{PeripheralsDataManager.getPHHostname()};
                    Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                            KitchenPrinterDataManagerMethod.IS_PRINTER_HOST_IN_SERVICE.getMethodName(), args);
                    inService = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.IS_PRINTER_HOST_IN_SERVICE.getMethodName());
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the printer host " +
                    "running on the terminal %s is in-service in PrinterHostHealthCheck.isPrinterHostInService",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

        return inService;
    }

    /**
     * <p>Utility method to get the MAC address {@link String} of the terminal running the in-service printer host as perceived the terminal with the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to get the MAC address {@link String} of the terminal running the in-service printer host from.
     * @return The MAC address {@link String} of the terminal running the in-service printer host as perceived the terminal with the given MAC address {@link String}.
     */
    public String getMacAddressOfInServicePrinterHost (String macAddress) {
        String macAddressOfInServicePrinterHost = "";

        try {
            if (StringFunctions.stringHasContent(macAddress)) {
                if (isTerminalRunningOnGivenMac(macAddress)) {
                    macAddressOfInServicePrinterHost = KitchenPrinterTerminal.getInstance().getActivePHMacAddress();
                }
                else {
                    // make XML RPC call
                    String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                    Object[] args = new Object[]{PeripheralsDataManager.getPHHostname()};
                    Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                            KitchenPrinterDataManagerMethod.GET_MAC_ADDRESS_OF_IN_SERVICE_PRINTER_HOST.getMethodName(), args);
                    String parsedXmlRpcResponse = XmlRpcUtil.getInstance().parseStringXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.GET_MAC_ADDRESS_OF_IN_SERVICE_PRINTER_HOST.getMethodName());
                    if (StringFunctions.stringHasContent(parsedXmlRpcResponse)) {
                        macAddressOfInServicePrinterHost = parsedXmlRpcResponse;
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the MAC address of the terminal running " +
                    "the in-service printer host from the terminal %s is in-service in PrinterHostHealthCheck.getMacAddressOfInServicePrinterHost",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

        return macAddressOfInServicePrinterHost;
    }

    /**
     * <p>Utility method to get the {@link LocalDateTime} that the in-service printer host became in-service on the terminal with the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to get the time the printer host became in-service on.
     * @return The {@link LocalDateTime} that the in-service printer host became in-service on the terminal with the given MAC address {@link String}.
     */
    public LocalDateTime getTimeInServicePrinterHostBecameInService (String macAddress) {
        LocalDateTime timeInServicePrinterHostBecameInService = null;

        try {
            if (StringFunctions.stringHasContent(macAddress)) {
                if (isTerminalRunningOnGivenMac(macAddress)) {
                    timeInServicePrinterHostBecameInService = KitchenPrinterTerminal.getInstance().getTimePHBecameActive();
                }
                else {
                    String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                    Object[] args = new Object[]{PeripheralsDataManager.getPHHostname()};
                    Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                            KitchenPrinterDataManagerMethod.GET_TIME_IN_SERVICE_PRINTER_HOST_BECAME_IN_SERVICE.getMethodName(), args);
                    String parsedXmlRpcResponse = XmlRpcUtil.getInstance().parseStringXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.GET_TIME_IN_SERVICE_PRINTER_HOST_BECAME_IN_SERVICE.getMethodName());
                    if ((StringFunctions.stringHasContent(parsedXmlRpcResponse)) && (DataFunctions.isValidLocalDateTimeString(parsedXmlRpcResponse))) {
                        timeInServicePrinterHostBecameInService = LocalDateTime.parse(parsedXmlRpcResponse);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the time the perceived in-service " +
                    "printer host on the terminal %s began operating as the in-service printer host in " +
                    "PrinterHostHealthCheck.getTimeInServicePrinterHostBecameInService",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

        return timeInServicePrinterHostBecameInService;
    }

    /**
     * <p>Utility method to get the in-service printer host as perceived by the terminal with the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to get the in-service printer host on.
     * @return The in-service printer host as perceived by the terminal with the given MAC address {@link String}.
     */
    public HashMap<String, LocalDateTime> getInServicePrinterHostOnTerminal (String macAddress) {
        HashMap<String, LocalDateTime> inServicePrinterHost = new HashMap<>();

        try {
            if (StringFunctions.stringHasContent(macAddress)) {
                String macAddressOfInServicePrinterHost = getMacAddressOfInServicePrinterHost(macAddress);
                LocalDateTime timeInServicePrinterHostBecameInService = getTimeInServicePrinterHostBecameInService(macAddress);
                inServicePrinterHost.put(macAddressOfInServicePrinterHost, timeInServicePrinterHostBecameInService);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to get the in-service printer host on the " +
                    "terminal %s from the terminal %s in PrinterHostHealthCheck.getInServicePrinterHostOnTerminal",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

        return inServicePrinterHost;
    }

    /**
     * <p>Utility method to set the MAC address {@link String} of the in-service printer host running on the terminal with the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to set the MAC address {@link String} of the terminal running the in-service printer host on.
     * @param macAddressOfInServicePrinterHost The MAC address {@link String} of the in-service printer host.
     * @return Whether or not the MAC address {@link String} of the in-service printer host running on the terminal with the given MAC address {@link String} could be set properly.
     */
    public boolean setMacAddressOfInServicePrinterHost (String macAddress,
                                                        String macAddressOfInServicePrinterHost) {
        boolean success = false;

        try {

            Logger.logMessage("Mac address of printer host to update: " + macAddress, PHHC_LOG, Logger.LEVEL.DEBUG);
            Logger.logMessage("Mac address of new in service PH: " + macAddressOfInServicePrinterHost, PHHC_LOG, Logger.LEVEL.DEBUG);

            if ((StringFunctions.stringHasContent(macAddress)) && (StringFunctions.stringHasContent(macAddressOfInServicePrinterHost))) {
                if (isTerminalRunningOnGivenMac(macAddress)) {
                    Logger.logMessage("Terminal is running on the given mac to update, updating the local in service printer host mac address", PHHC_LOG, Logger.LEVEL.DEBUG);
                    KitchenPrinterTerminal.getInstance().setActivePHMacAddress(macAddressOfInServicePrinterHost);
                    success = true;
                }
                else {
                    Logger.logMessage("Terminal is not running on the given mac to update, making an xml rpc call to set the active printer host mac address", PHHC_LOG, Logger.LEVEL.DEBUG);
                    // make XML RPC call
                    String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                    Object[] args = new Object[]{PeripheralsDataManager.getPHHostname(), macAddressOfInServicePrinterHost};
                    Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                            KitchenPrinterDataManagerMethod.SET_MAC_ADDRESS_OF_IN_SERVICE_PRINTER_HOST.getMethodName(), args);
                    success = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.SET_MAC_ADDRESS_OF_IN_SERVICE_PRINTER_HOST.getMethodName());
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to set the MAC address of the terminal running " +
                    "the in-service printer host on the terminal %s in PrinterHostHealthCheck.setMacAddressOfInServicePrinterHost",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Utility method to set the time {@link String} the in-service printer host became the in-service printer host on the terminal with the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to set the time {@link String} the terminal running the in-service became the in-service printer host.
     * @param timeInServicePrinterHostBecameInService The time {@link String} the in-service printer host became the in-service printer host.
     * @return Whether or not the time {@link String} the in-service printer host became the in-service printer host on the terminal with the given MAC address {@link String}. could be set properly.
     */
    public boolean setTimeInServicePrinterHostBecameInService (String macAddress,
                                                               String timeInServicePrinterHostBecameInService) {
        boolean success = false;

        try {
            if ((StringFunctions.stringHasContent(macAddress))
                    && (StringFunctions.stringHasContent(timeInServicePrinterHostBecameInService))
                    && (DataFunctions.isValidLocalDateTimeString(timeInServicePrinterHostBecameInService))) {
                if (isTerminalRunningOnGivenMac(macAddress)) {
                    KitchenPrinterTerminal.getInstance().setTimePHBecameActive(LocalDateTime.parse(timeInServicePrinterHostBecameInService));
                    success = true;
                }
                else {
                    // make XML RPC call
                    String url = XmlRpcUtil.getInstance().buildURL(macAddress, PeripheralsDataManager.getTerminalHostname(macAddress));
                    Object[] args = new Object[]{PeripheralsDataManager.getPHHostname(), timeInServicePrinterHostBecameInService};
                    Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, PeripheralsDataManager.getTerminalHostname(macAddress),
                            KitchenPrinterDataManagerMethod.SET_TIME_IN_SERVICE_PRINTER_HOST_BECAME_IN_SERVICE.getMethodName(), args);
                    success = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, KitchenPrinterDataManagerMethod.SET_TIME_IN_SERVICE_PRINTER_HOST_BECAME_IN_SERVICE.getMethodName());
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to set the time the in-service printer host " +
                    "became in service on the terminal %s in PrinterHostHealthCheck.setTimeInServicePrinterHostBecameInService",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Utility method to set the in-service printer host on the terminal with the given MAC address {@link String}.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to set the in-service printer host on.
     * @param inServicePrinterHost The {@link HashMap} of the MAC address {@link String} and {@link LocalDateTime} the in-service printer host became the in-service printer host.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public void setInServicePrinterHostOnTerminal (String macAddress, HashMap<String, LocalDateTime> inServicePrinterHost) {

        try {
            if ((StringFunctions.stringHasContent(macAddress)) && (!DataFunctions.isEmptyMap(inServicePrinterHost))) {
                String inServicePrinterHostMacAddress = inServicePrinterHost.entrySet().iterator().next().getKey();
                LocalDateTime timeInServicePrinterHostBecameInService = inServicePrinterHost.entrySet().iterator().next().getValue();
                if ((StringFunctions.stringHasContent(inServicePrinterHostMacAddress)) && (timeInServicePrinterHostBecameInService != null)) {
                    if (!setMacAddressOfInServicePrinterHost(macAddress, inServicePrinterHostMacAddress)) {
                        Logger.logMessage(String.format("Unable to set the MAC address of the terminal running the in-service " +
                                "printer host for the terminal %s in PrinterHostHealthCheck.setInServicePrinterHostOnTerminal",
                                Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
                    }

                    if (!setTimeInServicePrinterHostBecameInService(macAddress, timeInServicePrinterHostBecameInService.toString())) {
                        Logger.logMessage(String.format("Unable to set the time the in-service printer host became the in-service " +
                                "printer host for the terminal %s in PrinterHostHealthCheck.setInServicePrinterHostOnTerminal",
                                Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);

                    }
                }
                else {
                    if (!StringFunctions.stringHasContent(inServicePrinterHostMacAddress)) {
                        Logger.logMessage("Invalid MAC address for a terminal that could be running the in-service printer host in " +
                                "PrinterHostHealthCheck.setInServicePrinterHostOnTerminal", PHHC_LOG, Logger.LEVEL.ERROR);
                    }
                    if (timeInServicePrinterHostBecameInService == null) {
                        Logger.logMessage("Invalid time for when a printer host could have become in-service in " +
                                "PrinterHostHealthCheck.setInServicePrinterHostOnTerminal", PHHC_LOG, Logger.LEVEL.ERROR);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to set the time the in-service printer host as " +
                    "%s on the terminal %s in PrinterHostHealthCheck.setInServicePrinterHostOnTerminal",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(inServicePrinterHost.entrySet().iterator().next().getKey()), "NULL"),
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Utility method to set the in-service printer host on all terminals that contain a printer host.</p>
     *
     * @param macAddress The MAC address {@link String} of the in-service printer host.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public void setInServicePrinterHostOnAllPrinterHostTerminals (String macAddress) {

        try {
            if (StringFunctions.stringHasContent(macAddress)) {
                // make sure the in-service printer host is in-service
                if (!isPrinterHostInService(macAddress)) {
                    startPrinterHost(macAddress);
                }

                KitchenPrinterTerminal.getInstance().setActivePHMacAddress(macAddress);

                // create the in-service printer host HashMap
                HashMap<String, LocalDateTime> inServicePrinterHost = new HashMap<>();
                inServicePrinterHost.put(macAddress, LocalDateTime.now());

                // set the on-service printer host on each terminal containing a printer host
                CopyOnWriteArraySet<QCPrinterHost> printerHosts = getPrinterHosts();
                if (!DataFunctions.isEmptyCollection(printerHosts)) {
                    for (QCPrinterHost printerHost : printerHosts) {
                        setInServicePrinterHostOnTerminal(printerHost.getMacAddress(), inServicePrinterHost);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to set the in-service printer host running on " +
                    "the terminal %s on all terminals configured to run a printer host in PrinterHostHealthCheck.setInServicePrinterHostOnAllPrinterHostTerminals",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Utility method to set the in-service printer host on all terminals that contain a printer host.</p>
     *
     * @param macAddress The MAC address {@link String} of the in-service printer host.
     * @param inServiceTime The {@link LocalDateTime} that the in-service printer host became the in-service printer host.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public void setInServicePrinterHostOnAllPrinterHostTerminals (String macAddress, LocalDateTime inServiceTime) {

        try {
            if ((StringFunctions.stringHasContent(macAddress)) && (inServiceTime != null)) {
                // make sure the in-service printer host is in-service
                if (!isPrinterHostInService(macAddress)) {
                    startPrinterHost(macAddress);
                }

                KitchenPrinterTerminal.getInstance().setActivePHMacAddress(macAddress);

                // create the in-service printer host HashMap
                HashMap<String, LocalDateTime> inServicePrinterHost = new HashMap<>();
                inServicePrinterHost.put(macAddress, inServiceTime);

                // set the on-service printer host on each terminal containing a printer host
                CopyOnWriteArraySet<QCPrinterHost> printerHosts = getPrinterHosts();
                if (!DataFunctions.isEmptyCollection(printerHosts)) {
                    for (QCPrinterHost printerHost : printerHosts) {
                        setInServicePrinterHostOnTerminal(printerHost.getMacAddress(), inServicePrinterHost);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to set the in-service printer host running on " +
                    "the terminal %s which became the in-service printer host at %s on all terminals configured to run a " +
                    "printer host in PrinterHostHealthCheck.setInServicePrinterHostOnAllPrinterHostTerminals",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL"),
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(inServiceTime.toString()), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Utility method to stop all printer hosts that have a printer host status of ACTIVE or BECOMING_ACTIVE and shouldn't.</p>
     *
     * @param macAddress The MAC address {@link String} of the in-service printer host.
     */
    @SuppressWarnings({"Convert2streamapi", "TypeMayBeWeakened", "SpellCheckingInspection", "OverlyComplexMethod"})
    public void stopInServicePrinterHostsThatShouldntBe (String macAddress) {

        try {
            if ((StringFunctions.stringHasContent(macAddress)) && (!DataFunctions.isEmptyMap(reorganizePrinterHostsByStatus()))) {
                // keep track of which printer hosts need to be stopped
                ArrayList<String> macAddressesOfPrinterHostsToStop = new ArrayList<>();
                for (Map.Entry<PrinterHostStatus, ArrayList<String>> reorganizedPrinterHostsEntry : reorganizePrinterHostsByStatus().entrySet()) {
                    if ((reorganizedPrinterHostsEntry.getKey().equals(PrinterHostStatus.ACTIVE)) || (reorganizedPrinterHostsEntry.getKey().equals(PrinterHostStatus.BECOMING_ACTIVE))) {
                        if (!DataFunctions.isEmptyCollection(reorganizedPrinterHostsEntry.getValue())) {
                            for (String phMacAddress : reorganizedPrinterHostsEntry.getValue()) {
                                // don't stop the in-service printer host
                                if (!phMacAddress.equalsIgnoreCase(macAddress)) {
                                    macAddressesOfPrinterHostsToStop.add(phMacAddress);
                                }
                            }
                        }
                    }
                }

                // stop printer hosts that should be stopped
                if (!DataFunctions.isEmptyCollection(macAddressesOfPrinterHostsToStop)) {
                    Logger.logMessage("Stopping printer hosts in PrinterHostHealthCheck.stopInServicePrinterHostsThatShouldntBe", PHHC_LOG, Logger.LEVEL.TRACE);
                    for (String phMacAddress : macAddressesOfPrinterHostsToStop) {
                        stopPrinterHost(phMacAddress);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to stop printer hosts with a printer host status of " +
                    "ACTIVE or BECOMING_ACTIVE and shouldn't on the terminal %s in PrinterHostHealthCheck.stopInServicePrinterHostsThatShouldntBe",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Utility method to try to restart the printer host that was in-service the last time the printer host health check ran.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal running the printer host that was in-service the last time the printer host health check ran.
     * @return Whether or not the printer host that was in-service that last time the printer host health check was run could be restarted.
     */
    public boolean restartInServicePrinterHost (String macAddress) {
        boolean ableToRestart = false;

        try {
            int restartAttempts = 1;
            while ((!ableToRestart) && (restartAttempts <= 3)) {

                // wait a little bit in-between each attempt
                Thread.sleep(FIVE_SECONDS);

                Logger.logMessage(String.format("Attempt number %s to restart the printer host that was running on the " +
                        "terminal %s as the in-service printer host in PrinterHostHealthCheck.restartInServicePrinterHost",
                        Objects.toString(restartAttempts, "NULL"),
                        Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.DEBUG);

                // try to start the printer host
                startPrinterHost(macAddress);

                // check if the printer host could be restarted
                if (isPrinterHostInService(macAddress)) {
                    Logger.logMessage(String.format("The printer host running on the terminal %s was able to resume being " +
                            "the in-service printer host in PrinterHostHealthCheck.restartInServicePrinterHost",
                            Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")));
                    ableToRestart = true;
                }

                restartAttempts++;
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHHC_LOG);
            Logger.logMessage(String.format("There was a problem trying to restart the printer host running on the " +
                    "terminal %s in PrinterHostHealthCheck.restartInServicePrinterHost",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), PHHC_LOG, Logger.LEVEL.ERROR);
        }

        return ableToRestart;
    }

}