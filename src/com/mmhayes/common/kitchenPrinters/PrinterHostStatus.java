package com.mmhayes.common.kitchenPrinters;

/*
 Last Updated (automatically updated by SVN)
 $Author: jkflanagan $: Author of last commit
 $Date: 2019-04-19 14:49:01 -0400 (Fri, 19 Apr 2019) $: Date of last commit
 $Rev: 38067 $: Revision of last commit
 Notes: Enum to maintain the status of a printer host.
*/

import java.util.HashMap;

/**
 * Enum to maintain the status of a printer host.
 *
 */
public enum PrinterHostStatus {

    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE"),
    BECOMING_ACTIVE("BECOMING_ACTIVE"),
    SHUTTING_DOWN("SHUTTING_DOWN"),
    UNRESPONSIVE("UNRESPONSIVE");

    // field within the PrinterHostStatus enum
    private String phStatus;

    /**
     * Constructor for a PrinterHostStatus.
     *
     * @param phStatus {@link String} The status of the printer host.
     */
    PrinterHostStatus (String phStatus) {
        this.phStatus = phStatus;
    }

    /**
     * Getter for the phStatus field.
     *
     * @return {@link String} The status of the printer host.
     */
    public String getPrinterHostStatus () {
        return phStatus;
    }

    // START REVERSE LOOKUP ------------------------------------------------------------------------------------------//
    // a reverse lookup is used to get the element in the enum by its String value -----------------------------------//

    // create the lookup HashMap
    private static final HashMap<String, PrinterHostStatus> lookupHM = new HashMap<>();

    // populate the lookup HashMap
    static {
        for (PrinterHostStatus phStatus : PrinterHostStatus.values()) {
            lookupHM.put(phStatus.getPrinterHostStatus(), phStatus);
        }
    }

    /**
     * Get the PrinterHost status from the lookup HashMap using the given String.
     *
     * @param phStatus {@link String} The status of the printer host.
     * @return {@link PrinterHostStatus} The PrinterHostStatus element corresponding to the given phStatus String.
     */
    public static PrinterHostStatus getPHStatusFromString (String phStatus) {
        return lookupHM.get(phStatus);
    }
    // END REVERSE LOOKUP --------------------------------------------------------------------------------------------//

}
