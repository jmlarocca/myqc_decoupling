package com.mmhayes.common.kitchenPrinters.KDS;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-14 09:14:23 -0400 (Mon, 14 Sep 2020) $: Date of last commit
    $Rev: 12625 $: Revision of last commit
    Notes: Removes order XML log files that are more than 30 days old.
*/

import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>Removes order XML log files that are more than 30 days old.</p>
 *
 */
public class OrderXMLCleaner implements Runnable, Serializable {

    // log file for the OrderXMLCleaner
    private static final String ORDER_XML_CLEANER_LOG = "KDSOrderXMLCleaner.log";

    // the only instance of a OrderXMLCleaner
    private static volatile OrderXMLCleaner orderXMLCleaner = null;

    /**
     * <p>Private constructor for a {@link OrderXMLCleaner}.</p>
     *
     */
    private OrderXMLCleaner () {Logger.logMessage("ORDER XML CLEANER....", ORDER_XML_CLEANER_LOG, Logger.LEVEL.IMPORTANT);}

    /**
     * <p>Gets the {@link OrderXMLCleaner} instance.</p>
     *
     * @return The {@link OrderXMLCleaner} instance.
     */
    public static OrderXMLCleaner getInstance () throws Exception {

        // check that there is an OrderXMLCleaner instance to return
        if (orderXMLCleaner == null) {
            throw new Exception("No OrderXMLCleaner instance has been created. Instantiate a new OrderXMLCleaner through the init() method.");
        }

        return orderXMLCleaner;
    }

    /**
     * <p>Instantiates and return the {@link OrderXMLCleaner}.</p>
     *
     * @return The {@link OrderXMLCleaner} instance.
     */
    public static synchronized OrderXMLCleaner init () throws Exception {

        if (orderXMLCleaner != null) {
            throw new Exception("An OrderXMLCleaner instance already exists. Use the getInstance() method to obtain the instance.");
        }
        orderXMLCleaner = new OrderXMLCleaner();

        return orderXMLCleaner;
    }

    /**
     * <p>Prevents creating a new {@link OrderXMLCleaner} instance through serialization.</p>
     *
     * @return The {@link OrderXMLCleaner} instance.
     */
    protected OrderXMLCleaner readResolve () throws Exception {
        return getInstance();
    }

    /**
     * <p>Overridden run() method for a {@link OrderXMLCleaner}.</p>
     *
     */
    @SuppressWarnings("Duplicates")
    @Override
    public void run () {
        HashMap<File, BasicFileAttributes> attributesForFiles = getAttributesForFiles(getDirectory());
        if (!DataFunctions.isEmptyMap(attributesForFiles)) {
            for (Map.Entry<File, BasicFileAttributes> entry : attributesForFiles.entrySet()) {
                File orderFile = entry.getKey();
                LocalDateTime creationTime = DataFunctions.convertFileTimeToLocalDateTime(entry.getValue().creationTime());

                // check if the file is older than 30 days
                boolean isFileOlderThan30Days = isFileExpired(orderFile, creationTime);

                Logger.logMessage(String.format("The file %s %s older than 30 days %s.",
                        Objects.toString((orderFile != null ? orderFile.getName() : "N/A"), "N/A"),
                        Objects.toString((isFileOlderThan30Days ? "is" : "isn't"), "N/A"),
                        Objects.toString((isFileOlderThan30Days ? "and will be deleted" : "and will not be deleted"), "N/A")), ORDER_XML_CLEANER_LOG, Logger.LEVEL.TRACE);

                if (isFileOlderThan30Days) {
                    if (commonMMHFunctions.deleteFile(orderFile)) {
                        Logger.logMessage(String.format("The KDS order XML file %s which is older than 30 days has been deleted in OrderXMLCleaner.run!",
                                Objects.toString((orderFile != null ? orderFile.getName() : "N/A"), "N/A")), ORDER_XML_CLEANER_LOG, Logger.LEVEL.TRACE);
                    }
                    else {
                        Logger.logMessage(String.format("Unable to delete the KDS order XML file %s which is older than 30 days in OrderXMLCleaner.run!",
                                Objects.toString((orderFile != null ? orderFile.getName() : "N/A"), "N/A")), ORDER_XML_CLEANER_LOG, Logger.LEVEL.ERROR);
                    }
                }
            }
        }
    }

    /**
     * <p>Gets the directory that houses the KDS order XML files.</p>
     *
     * @return The directory {@link File} that houses the KDS order XML files.
     */
    private File getDirectory () {

        File directory = commonMMHFunctions.getDirectory("kds", "order_xml_logs");
        if ((directory != null) && (directory.isDirectory())) {
            return directory;
        }

        return null;
    }

    /**
     * <p>Iterates through the files in the KDS order XML directory and gets the file attributes for each file.</p>
     *
     * @param dir The directory {@link File} that houses the KDS order XML files.
     * @return A {@link HashMap} whose key is a KDS order XML {@link File} and whose value is the {@link BasicFileAttributes} for the KDS order XML {@link File}.
     */
    @SuppressWarnings("Duplicates")
    private HashMap<File, BasicFileAttributes> getAttributesForFiles (File dir) {

        if (dir == null) {
            Logger.logMessage("The directory passed to OrderXMLCleaner.getAttributesForFiles can't be null!", ORDER_XML_CLEANER_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        HashMap<File, BasicFileAttributes> attributesForFiles = new HashMap<>();
        File[] orderFiles = dir.listFiles();
        if (!DataFunctions.isEmptyGenericArr(orderFiles)) {
            for (File orderFile : orderFiles) {
                if (orderFile != null) {
                    Path orderFilePath = orderFile.toPath();
                    try {
                        BasicFileAttributes attributes = Files.readAttributes(orderFilePath, BasicFileAttributes.class);
                        if (attributes != null) {
                            attributesForFiles.put(orderFile, attributes);
                        }
                    }
                    catch (IOException e) {
                        Logger.logException(e, ORDER_XML_CLEANER_LOG);
                        Logger.logMessage(String.format("A problem occurred while trying get the BasicFileAttributes for the File %s in OrderXMLCleaner.getAttributesForFiles.",
                                Objects.toString(orderFile.getName(), "N/A")));
                    }
                }
            }
        }
        else {
            Logger.logMessage("No files were found within the KDS order XML directory.", ORDER_XML_CLEANER_LOG, Logger.LEVEL.TRACE);
        }

        return attributesForFiles;
    }

    /**
     * <p>Checks whether or not the KDS order XML file is more than 30 days old.</p>
     *
     * @param orderFile The KDS order XML {@link File} to check for being expired.
     * @param creationTime The {@link LocalDateTime} the the given KDS order XML {@link File} was created.
     * @return Whether or not the KDS order XML file is more than 30 days old.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private boolean isFileExpired (File orderFile, LocalDateTime creationTime) {

        if (orderFile == null) {
            Logger.logMessage("The KDS order XML file passed to OrderXMLCleaner.isFileExpired can't be null!", ORDER_XML_CLEANER_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        if (creationTime == null) {
            Logger.logMessage("The time the KDS order XML file was created in OrderXMLCleaner.isFileExpired can't be null!", ORDER_XML_CLEANER_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        LocalDateTime now = LocalDateTime.now();
        long days = ChronoUnit.DAYS.between(creationTime, now);

        final long DAYS_BEFORE_EXPIRATION = 30L;
        if (days >= DAYS_BEFORE_EXPIRATION) {
            return true;
        }

        return false;
    }

}