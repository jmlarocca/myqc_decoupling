package com.mmhayes.common.kitchenPrinters.KDS;

/*
    $Author: jmkimber $: Author of last commit
    $Date: 2021-04-26 10:59:25 -0400 (Mon, 26 Apr 2021) $: Date of last commit
    $Rev: 13847 $: Revision of last commit
    Notes: Monitors the KDS XML files that will be sent to KDS to ensure that none of them are too old.
*/

import com.google.gson.Gson;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kitchenPrinters.KitchenPrinterDataManagerMethod;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.xmlapi.XmlRpcManager;
import com.mmhayes.common.xmlapi.XmlRpcUtil;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class KDSShareFolderMonitor implements Runnable, Serializable {

    // log file fore the KDSShareFolderMonitor
    public static final String KDS_SHARE_MONITOR_LOG = "KDSShareFolderMonitor.log";

    // the only instance of a KDSShareFolderMonitor
    private static volatile KDSShareFolderMonitor kdsShareFolderMonitor = null;

    // private member variables of the KDSShareFolderMonitor
    private DataManager dataManager = null;
    private String shareFolderPath = "";
    private int xmlTimeout = 0;
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(MMHTimeFormatString.YR_MO_DY_HR_MIN_SEC);

    /**
     * <p>Private constructor for a {@link KDSShareFolderMonitor}.</p>
     *
     * @param dataManager The {@link DataManager} to be used by the {@link KDSShareFolderMonitor} to access the database.
     * @param shareFolderPath The file path {@link String} at which the KDS share folder may be found.
     * @param xmlTimeout The maximum number of minutes a KDS XML file should remain within the share folder before sending out an alert.
     */
    private KDSShareFolderMonitor (DataManager dataManager, String shareFolderPath, int xmlTimeout) {
        Logger.logMessage(String.format("Started monitoring the KDS share folder at %s.",
                Objects.toString(LocalDateTime.now().format(dateTimeFormatter), "N/A")), KDS_SHARE_MONITOR_LOG, Logger.LEVEL.IMPORTANT);
        this.dataManager = dataManager;
        this.shareFolderPath = shareFolderPath;
        this.xmlTimeout = xmlTimeout;
    }

    /**
     * <p>Gets the {@link KDSShareFolderMonitor} instance.</p>
     *
     * @return The {@link KDSShareFolderMonitor} instance.
     */
    public static KDSShareFolderMonitor getInstance () throws Exception {

        // check that there is an KDSShareFolderMonitor instance to return
        if (kdsShareFolderMonitor == null) {
            throw new Exception("No KDSShareFolderMonitor instance has been created. Instantiate a new KDSShareFolderMonitor through the init() method.");
        }

        return kdsShareFolderMonitor;
    }

    /**
     * <p>Instantiates and return the {@link KDSShareFolderMonitor}.</p>
     *
     * @param dataManager The {@link DataManager} to be used by the {@link KDSShareFolderMonitor} to access the database.
     * @param shareFolderPath The file path {@link String} at which the KDS share folder may be found.
     * @param xmlTimeout The maximum number of minutes a KDS XML file should remain within the share folder before sending out an alert.
     * @return The {@link KDSShareFolderMonitor} instance.
     */
    public static synchronized KDSShareFolderMonitor init (DataManager dataManager, String shareFolderPath, int xmlTimeout) throws Exception {

        if (kdsShareFolderMonitor != null) {
            throw new Exception("An KDSShareFolderMonitor instance already exists. Use the getInstance() method to obtain the instance.");
        }

        // validate the DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KDSShareFolderMonitor.init can't be null, unable to start the KDS share " +
                    "folder monitor, now returning null!", KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // validate the share folder path
        if (!StringFunctions.stringHasContent(shareFolderPath)) {
            Logger.logMessage("The KDS share folder path passed to KDSShareFolderMonitor.init can't be null or empty, unable to " +
                    "start the KDS share folder monitor, now returning null!", KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // validate the KDS XML file timeout
        if (xmlTimeout <= 0) {
            Logger.logMessage("The KDS order XML file timeout passed to KDSShareFolderMonitor.init must be greater than 0, unable to " +
                    "start the KDS share folder monitor, now returning null!", KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        kdsShareFolderMonitor = new KDSShareFolderMonitor(dataManager, shareFolderPath, xmlTimeout);

        return kdsShareFolderMonitor;
    }

    /**
     * <p>Prevents creating a new {@link KDSShareFolderMonitor} instance through serialization.</p>
     *
     * @return The {@link KDSShareFolderMonitor} instance.
     */
    protected KDSShareFolderMonitor readResolve () throws Exception {
        return getInstance();
    }

    /**
     * <p>Overridden run method for a {@link KDSShareFolderMonitor}.</p>
     *
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    @Override
    public void run () {
        Logger.logMessage(String.format("Call to monitor the KDS share folder made at %s.",
                Objects.toString(LocalDateTime.now().format(dateTimeFormatter), "N/A")), KDS_SHARE_MONITOR_LOG, Logger.LEVEL.TRACE);

        // get attributes for files within the KDS file share
        File kdsFileShare = getKDSFileShare();
        HashMap<File, BasicFileAttributes> attributesForFileShareFiles = getAttributesForFiles(kdsFileShare);
        ArrayList<HashMap<String, String>> expiredXMLPaths = new ArrayList<>();
        if (!DataFunctions.isEmptyMap(attributesForFileShareFiles)) {
            for (Map.Entry<File, BasicFileAttributes> fileAndAttrsEntry : attributesForFileShareFiles.entrySet()) {
                File xml = fileAndAttrsEntry.getKey();
                LocalDateTime creationTime = DataFunctions.convertFileTimeToLocalDateTime(fileAndAttrsEntry.getValue().creationTime());

                // check if the XML file is too old
                if (isFileExpired(xml, creationTime)) {
                    Logger.logMessage(String.format("The file %s is more than %s old and should have been sent to KDS already! The file was " +
                            "created at %s and it is currently %s. Now sending an email in KDSShareFolderMonitor.run!",
                            Objects.toString((xml != null ? xml.getName() : "N/A"), "N/A"),
                            Objects.toString((xmlTimeout == 1 ? xmlTimeout + " minute" : xmlTimeout + " minutes"), "N/A"),
                            Objects.toString(creationTime.format(dateTimeFormatter), "N/A"),
                            Objects.toString(LocalDateTime.now().format(dateTimeFormatter), "N/A")), KDS_SHARE_MONITOR_LOG, Logger.LEVEL.TRACE);
                    String xmlPath = (xml != null ? xml.getAbsolutePath() : "");
                    if (StringFunctions.stringHasContent(xmlPath)) {
                        HashMap<String, String> expiredXMLHM = new HashMap<>();
                        expiredXMLHM.put("PATH", xmlPath);
                        expiredXMLHM.put("CREATEDTIME", creationTime.format(dateTimeFormatter));
                        expiredXMLHM.put("NOWTIME", LocalDateTime.now().format(dateTimeFormatter));
                        expiredXMLPaths.add(expiredXMLHM);
                    }
                }
                else {
                    Logger.logMessage(String.format("The file %s has not yet expired and may still be picked up by KDS. The file was created at %s and it is currently %s.",
                            Objects.toString((xml != null ? xml.getName() : "N/A"), "N/A"),
                            Objects.toString(creationTime.format(dateTimeFormatter), "N/A"),
                            Objects.toString(LocalDateTime.now().format(dateTimeFormatter), "N/A")), KDS_SHARE_MONITOR_LOG, Logger.LEVEL.TRACE);
                }
            }
        }
        if (!DataFunctions.isEmptyCollection(expiredXMLPaths)) {
            // determine the company name from the database
            String companyName = "";
            DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetCompanyName").setLogFileName(KDS_SHARE_MONITOR_LOG);
            Object queryRes = sql.getSingleField(dataManager);
            if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString()))) {
                companyName = queryRes.toString();
            }

            // make XML RPC call to the server to send the expired XML email
            String serverURL = "";
            if (StringFunctions.stringHasContent(MMHProperties.getAppSetting("site.application.serverXmlRpc.path"))) {
                serverURL = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            }
            if ((!StringFunctions.stringHasContent(serverURL)) && (StringFunctions.stringHasContent(MMHProperties.getAppSetting("site.POSAnySync.QCConnect.XmlRpc.path")))) {
                serverURL = MMHProperties.getAppSetting("site.POSAnySync.QCConnect.XmlRpc.path");
            }
            XmlRpcManager xmlRpcManager = null;
            try {
                xmlRpcManager = XmlRpcManager.getInstance();
            }
            catch (Exception e) {
                Logger.logException(e, KDS_SHARE_MONITOR_LOG);
                Logger.logMessage("A problem occurred in KDSShareFolderMonitor.run while trying to get the XmlRpcManager instance.", KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            }

            if (xmlRpcManager != null) {
                String macAddress = PeripheralsDataManager.getTerminalMacAddress();
                String hostname = PeripheralsDataManager.getPHHostname();
                String method = KitchenPrinterDataManagerMethod.SEND_EXPIRED_KDS_XML_EMAIL.getMethodName();
                String expiredKDSXMLJSON = new Gson().toJson(expiredXMLPaths);
                Object[] args = new Object[]{macAddress, hostname, companyName, expiredKDSXMLJSON};
                Object xmlRpcResponse = xmlRpcManager.xmlRpcInvokeInstanceOnServer(KDS_SHARE_MONITOR_LOG, method, args);
                if (XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, method, hostname, serverURL)) {
                    Logger.logMessage(String.format("Successfully made a request to the server to send an email alert for KDS XML order " +
                            "files which have been in the file share for too long in KDSShareFolderMonitor.run, the following " +
                            "KDS XML order files %s, were included in that email.",
                            Objects.toString(StringFunctions.buildStringFromList(expiredXMLPaths, ","), "N/A")), KDS_SHARE_MONITOR_LOG, Logger.LEVEL.TRACE);
                }
                else {
                    Logger.logMessage(String.format("Failed to make a request to the server to send an email alert for KDS XML order " +
                            "files which have been in the file share for too long in KDSShareFolderMonitor.run, the following " +
                            "KDS XML order files %s, should have been included in that email.",
                            Objects.toString(StringFunctions.buildStringFromList(expiredXMLPaths, ","), "N/A")), KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
                }
            }
        }
    }

    /**
     * <p>Gets the KDS XML file share as a {@link File} object.</p>
     *
     * @return The KDS XML file share as a {@link File} object.
     */
    private File getKDSFileShare () {

        if (StringFunctions.stringHasContent(shareFolderPath)) {
            File kdsFileShare = new File(shareFolderPath);
            if (kdsFileShare.isDirectory()) {
                return kdsFileShare;
            }
        }

        return null;
    }

    /**
     * <p>Iterates through the files in the KDS XML file share and gets the file attributes for each file.</p>
     *
     * @param dir The directory {@link File} that houses the KDS order XML files.
     * @return A {@link HashMap} whose key is a KDS order XML {@link File} and whose value is the {@link BasicFileAttributes} for the KDS order XML {@link File}.
     */
    private HashMap<File, BasicFileAttributes> getAttributesForFiles (File dir) {

        if (dir == null) {
            Logger.logMessage("The directory passed to KDSShareFolderMonitor.getAttributesForFiles can't be null!", KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        HashMap<File, BasicFileAttributes> attributesForFiles = new HashMap<>();
        // exclude directories in listFiles
        File[] files = dir.listFiles(pathname -> !pathname.isDirectory());
        if (!DataFunctions.isEmptyGenericArr(files)) {
            for (File file : files) {
                if (file != null) {
                    Path filePath = file.toPath();
                    try {
                        BasicFileAttributes attributes = Files.readAttributes(filePath, BasicFileAttributes.class);
                        if (attributes != null) {
                            attributesForFiles.put(file, attributes);
                        }
                    }
                    catch (IOException e) {
                        Logger.logException(e, KDS_SHARE_MONITOR_LOG);
                        Logger.logMessage(String.format("A problem occurred while trying get the BasicFileAttributes for the File %s in KDSShareFolderMonitor.getAttributesForFiles.",
                                Objects.toString(file.getName(), "N/A")));
                    }
                }
            }
        }
        else {
            Logger.logMessage("No files were found within the KDS share folder.", KDS_SHARE_MONITOR_LOG, Logger.LEVEL.TRACE);
        }

        return attributesForFiles;
    }

    /**
     * <p>Checks whether or not a file in the KDS file share is older than the timeout.</p>
     *
     * @param file The KDS order XML {@link File} to check for being expired.
     * @param createdTime The {@link LocalDateTime} the the given KDS order XML {@link File} was created.
     * @return Whether or not a file in the KDS share folder is older than the timeout.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private boolean isFileExpired (File file, LocalDateTime createdTime) {

        if (file == null) {
            Logger.logMessage("The KDS order XML file passed to KDSShareFolderMonitor.isFileExpired can't be null!", KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        if (createdTime == null) {
            Logger.logMessage("The time the KDS order XML file was created in KDSShareFolderMonitor.isFileExpired can't be null!", KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // get the time it is now
        LocalDateTime now = LocalDateTime.now();

        // get how many minutes have passed since the file was created
        long minutesDiff = ChronoUnit.MINUTES.between(createdTime, now);

        return (minutesDiff >= xmlTimeout);
    }

}