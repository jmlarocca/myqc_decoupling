package com.mmhayes.common.kitchenPrinters.KDS;

/*
 * $Author: jkflanagan $: Author of last commit
 * $Date: 2020-08-13 17:09:36 -0400 (Thu, 13 Aug 2020) $: Date of last commit
 * $Rev: 12347 $: Revision of last commit
 * Notes: Contains common functionality methods for KDS.
 */

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Contains common functionality methods for KDS.</p>
 *
 */
public class KDSCommon {

    /**
     * <p>Converts the given XML {@link String} into a byte array that can be interpreted by KDS.</p>
     *
     * @param xmlStr The XML {@link String} to convert.
     * @param log The file path {@link String} of the file that any information related to this class should be logged to.
     * @return The converted XML {@link String} as a byte array that can be interpreted by KDS.
     */
    public static byte[] buildSendBuffer (String xmlStr, String log) {

        if (!StringFunctions.stringHasContent(xmlStr)) {
            Logger.logMessage("The XML String passed to KDSCommon.buildSendBuffer can't be null or empty!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // get the bytes in our payload
        byte[] xmlStrBytes = xmlStr.getBytes(StandardCharsets.UTF_8);

        if (xmlStrBytes.length <= 0) {
            Logger.logMessage("No payload to send to KDS has been found in KDSCommon.buildSendBuffer.", log, Logger.LEVEL.ERROR);
            return null;
        }

        // constants
        final int XML_STR_BYTES_LEN = xmlStrBytes.length;
        final byte STX = 0x02; // start transmission
        final byte COMMAND = 0x16; // command
        final byte DL_0 = (byte)((XML_STR_BYTES_LEN & 0xff000000L) >> 24); // data length
        final byte DL_1 = (byte)((XML_STR_BYTES_LEN & 0xff0000L) >> 16); // data length
        final byte DL_2 = (byte)((XML_STR_BYTES_LEN & 0xff00L) >> 8); // data length
        final byte DL_3 = (byte)(XML_STR_BYTES_LEN & 0xffL); // data length
        final byte ETX = 0x03; // end transmission
        final int SEND_BUFFER_LEN = XML_STR_BYTES_LEN + 7; // add 7 to account for STX, COMMAND, DATA LENGTH, ETX

        // create the buffer to send to KDS
        byte[] sendBuffer = new byte[SEND_BUFFER_LEN];
        sendBuffer[0] = STX;
        sendBuffer[1] = COMMAND;
        sendBuffer[2] = DL_0;
        sendBuffer[3] = DL_1;
        sendBuffer[4] = DL_2;
        sendBuffer[5] = DL_3;
        System.arraycopy(xmlStrBytes, 0, sendBuffer, 6, XML_STR_BYTES_LEN); // add the payload to the buffer
        sendBuffer[6 + XML_STR_BYTES_LEN] = ETX;

        Logger.logMessage(String.format("Built the buffer in KDSCommon.buildSendBuffer which contains %s to send to KDS.",
                Objects.toString(DataFunctions.convertByteArrToStr(sendBuffer, StandardCharsets.UTF_8, false), "N/A")), log, Logger.LEVEL.TRACE);

        Logger.logMessage(String.format("Built the buffer in KDSCommon.buildSendBuffer which contains the bytes %s to send to KDS.",
                Objects.toString(DataFunctions.convertByteArrToStr(sendBuffer, StandardCharsets.UTF_8, true), "N/A")), log, Logger.LEVEL.LUDICROUS);

        return sendBuffer;
    }

    /**
     * <p>Replaces escaped markup with their corresponding characters.</p>
     *
     * @param s The {@link String} to correct the markup for.
     * @return The given {@link String} with it's markup fixed.
     */
    public static String fixMarkup (String s) {

        if (!StringFunctions.stringHasContent(s)) {
            Logger.logMessage("The String passed to KDSCommon.fixMarkup can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // list of 'words' to replace with their corresponding markup
        String[] charsToReplace = new String[]{"&lt;", "&gt;", "&amp;", "&apos;", "&quot;", "&#37;"};
        // replacement markup whose indices align with charsToReplace
        String[] replacementChars = new String[]{"<", ">", "&", "\'", "\"", "%"};

        return StringUtils.replaceEach(s, charsToReplace, replacementChars);
    }

    /**
     * <p>Replaces escaped markup with their corresponding characters.</p>
     *
     * @param s The {@link String} to correct the markup for.
     * @param log The file path {@link String} of the file that any information related to this class should be logged to.
     * @return The given {@link String} with it's markup fixed.
     */
    public static String fixMarkup (String s, String log) {

        if (!StringFunctions.stringHasContent(s)) {
            Logger.logMessage("The String passed to KDSCommon.fixMarkup can't be null or empty!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // list of 'words' to replace with their corresponding markup
        String[] charsToReplace = new String[]{"&lt;", "&gt;", "&amp;", "&apos;", "&quot;", "&#37;"};
        // replacement markup whose indices align with charsToReplace
        String[] replacementChars = new String[]{"<", ">", "&", "\'", "\"", "%"};

        return StringUtils.replaceEach(s, charsToReplace, replacementChars);
    }

    /**
     * <p>Gets the KDS station ID associated with the given printer ID.</p>
     *
     * @param printerID The printer ID to get the associated KDS station ID for.
     * @param log The file path {@link String} of the file that any information related to this class should be logged to.
     * @return The KDS station ID associated with the given printer ID.
     */
    public static int getKDSStationIDFromPrinterID (int printerID, String log) {

        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KDSCommon.getKDSStationIDFromPrinterID can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        DynamicSQL sql = new DynamicSQL("data.kms.GetKDSStationIDFromPrinterID").addIDList(1, printerID).setLogFileName(log);
        return sql.getSingleIntField(new DataManager());
    }

    /**
     * <p>Gets the KDS station ID associated with the given printer ID.</p>
     *
     * @param printerID The printer ID to get the associated KDS station ID for.
     * @param log The file path {@link String} of the file that any information related to this class should be logged to.
     * @param lookup A {@link HashMap} whose {@link Integer} key is for a printer ID and whose {@link Integer} value is for the corresponding KDS station ID.
     * @return The KDS station ID associated with the given printer ID.
     */
    public static int getKDSStationIDFromPrinterID (String log, int printerID, HashMap<Integer, Integer> lookup) {

        // make sure the log is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KDSCommon.getKDSStationIDFromPrinterID can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure the printer ID is valid
        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KDSCommon.getKDSStationIDFromPrinterID can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure the lookup Map is valid
        if (DataFunctions.isEmptyMap(lookup)) {
            Logger.logMessage("The printer to KDS station ID lookup Map passed to KDSCommon.getKDSStationIDFromPrinterID can't be null or empty!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        if (lookup.containsKey(printerID)) {
            return lookup.get(printerID);
        }

        return -1;
    }

    /**
     * <p>Gets the printer ID associated with the given KDS station ID.</p>
     *
     * @param kdsStationID The KDS station ID to get the associated printer ID for.
     * @param log The file path {@link String} of the file that any information related to this class should be logged to.
     * @return The printer ID associated with the given KDS station ID.
     */
    public static int getPrinterIDFromKDSStationID (int kdsStationID, String log) {

        if (kdsStationID <= 0) {
            Logger.logMessage("The KDS station ID passed to KDSCommon.getPrinterIDFromKDSStationID can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        DynamicSQL sql = new DynamicSQL("data.kms.GetPrinterIDFromKDSStationID").addIDList(1, kdsStationID).setLogFileName(log);
        return sql.getSingleIntField(new DataManager());
    }

    /**
     * <p>Gets the KDS station ID associated with the given printer ID.</p>
     *
     * @param kdsStationID The KDS station ID to get the associated printer ID for.
     * @param log The file path {@link String} of the file that any information related to this class should be logged to.
     * @param lookup A {@link HashMap} whose {@link Integer} key is for a KDS station ID and whose {@link Integer} value is for the corresponding printer ID.
     * @return The printer ID associated with the given KDS station ID.
     */
    public static int getPrinterIDFromKDSStationID (String log, int kdsStationID, HashMap<Integer, Integer> lookup) {

        // make sure the log is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KDSCommon.getPrinterIDFromKDSStationID can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure the KDS station ID is valid
        if (kdsStationID <= 0) {
            Logger.logMessage("The KDS station ID passed to KDSCommon.getPrinterIDFromKDSStationID can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure the lookup Map is valid
        if (DataFunctions.isEmptyMap(lookup)) {
            Logger.logMessage("The KDS station to printer ID lookup Map passed to KDSCommon.getPrinterIDFromKDSStationID can't be null or empty!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        if (lookup.containsKey(kdsStationID)) {
            return lookup.get(kdsStationID);
        }

        return -1;
    }

}