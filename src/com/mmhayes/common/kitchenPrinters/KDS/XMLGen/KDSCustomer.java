package com.mmhayes.common.kitchenPrinters.KDS.XMLGen;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-02 10:48:52 -0400 (Wed, 02 Sep 2020) $: Date of last commit
    $Rev: 12536 $: Revision of last commit
    Notes: Represents the customer who placed the order to be sent to KDS.
*/

import com.mmhayes.common.utils.StringFunctions;

import java.util.Objects;

public class KDSCustomer {

    // private member variables of a KDSCustomer
    private int id;
    private String phone;

    /**
     * <p>Constructor for a {@link KDSCustomer}.</p>
     *
     */
    public KDSCustomer () {}

    /**
     * <p>The {@link String} representation of a {@link KDSCustomer}.</p>
     *
     * @return The {@link KDSCustomer} instance as a {@link String}.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, PHONE: %s",
                Objects.toString((id > 0 ? id : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(phone) ? phone : "N/A"), "N/A"));

    }

    /**
     * <p>Getter for the id field of the {@link KDSCustomer}.</p>
     *
     * @return The id field of the {@link KDSCustomer}.
     */
    public int getID () {
        return id;
    }

    /**
     * <p>Setter for the id field of the {@link KDSCustomer}.</p>
     *
     * @param id The id field of the {@link KDSCustomer}.
     */
    public void setID (int id) {
        this.id = id;
    }

    /**
     * <p>Getter for the phone field of the {@link KDSCustomer}.</p>
     *
     * @return The phone field of the {@link KDSCustomer}.
     */
    public String getPhone () {
        return phone;
    }

    /**
     * <p>Setter for the phone field of the {@link KDSCustomer}.</p>
     *
     * @param phone The phone field of the {@link KDSCustomer}.
     */
    public void setPhone (String phone) {
        this.phone = phone;
    }

}