package com.mmhayes.common.kitchenPrinters.KDS.XMLGen;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-07-20 12:20:09 -0400 (Mon, 20 Jul 2020) $: Date of last commit
    $Rev: 12195 $: Revision of last commit
    Notes: Represents a pre modifier within an item or condiment to be sent to KDS.
*/

import com.mmhayes.common.utils.StringFunctions;

import java.util.Objects;

public class KDSPreModifier {

    // private member variables of a KDSPreModifier
    private int count;
    private String s0;
    private String s1;

    /**
     * <p>Constructor for a {@link KDSPreModifier}.</p>
     *
     */
    public KDSPreModifier () {}

    /**
     * <p>The {@link String} representation of a {@link KDSPreModifier}.</p>
     *
     * @return The {@link KDSPreModifier} instance as a {@link String}.
     */
    @Override
    public String toString () {

        return String.format("COUNT: %s, S0: %s, S1: %s",
                Objects.toString((count > 0 ? count : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(s0) ? s0 : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(s1) ? s1 : "N/A"), "N/A"));

    }

    /**
     * <p>Getter for the count field of the {@link KDSPreModifier}.</p>
     *
     * @return The count field of the {@link KDSPreModifier}.
     */
    public int getCount () {
        return count;
    }

    /**
     * <p>Setter for the count field of the {@link KDSPreModifier}.</p>
     *
     * @param count The count field of the {@link KDSPreModifier}.
     */
    public void setCount (int count) {
        this.count = count;
    }

    /**
     * <p>Getter for the s0 field of the {@link KDSPreModifier}.</p>
     *
     * @return The s0 field of the {@link KDSPreModifier}.
     */
    public String getS0 () {
        return s0;
    }

    /**
     * <p>Setter for the s0 field of the {@link KDSPreModifier}.</p>
     *
     * @param s0 The s0 field of the {@link KDSPreModifier}.
     */
    public void setS0 (String s0) {
        this.s0 = s0;
    }

    /**
     * <p>Getter for the s1 field of the {@link KDSPreModifier}.</p>
     *
     * @return The s1 field of the {@link KDSPreModifier}.
     */
    public String getS1 () {
        return s1;
    }

    /**
     * <p>Setter for the s1 field of the {@link KDSPreModifier}.</p>
     *
     * @param s1 The s1 field of the {@link KDSPreModifier}.
     */
    public void setS1 (String s1) {
        this.s1 = s1;
    }

}