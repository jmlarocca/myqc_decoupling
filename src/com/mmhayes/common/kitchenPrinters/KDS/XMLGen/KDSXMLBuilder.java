package com.mmhayes.common.kitchenPrinters.KDS.XMLGen;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-07-08 07:57:26 -0400 (Thu, 08 Jul 2021) $: Date of last commit
    $Rev: 14295 $: Revision of last commit
    Notes: Builds the XML to send to KDS.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kitchenPrinters.KitchenPrinterCommon;
import com.mmhayes.common.kitchenPrinters.QCPrintJob;
import com.mmhayes.common.kitchenPrinters.QCPrintJobDetail;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.KitchenDisplaySystemJobInfo;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.w3c.dom.Document;

import javax.xml.transform.OutputKeys;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("OverlyComplexClass")
public class KDSXMLBuilder {

    private static final String KDS_XML_GEN_LOG = "KDSXMLGen.log";
    private static final int COUNT = 2;

    /**
     * <p>Converts the given {@link QCPrintJob} into a {@link KDSTransaction}.</p>
     *
     * @param qcPrintJob The {@link QCPrintJob} to convert into a {@link KDSTransaction}.
     * @param dataManager The {@link DataManager} instance to use to query the database.
     * @return The given {@link QCPrintJob} converted into a {@link KDSTransaction}.
     */
    @SuppressWarnings({"OverlyComplexMethod", "Duplicates"})
    public static KDSTransaction convertQCPrintJobToKDSTransaction (QCPrintJob qcPrintJob, DataManager dataManager) {

        // make sure we have a print job to convert
        if (qcPrintJob == null) {
            Logger.logMessage("The QCPrintJob passed to KDSXMLBuilder.convertQCPrintJobToKDSTransaction can't be null!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure there are items within the print job
        ArrayList<QCPrintJobDetail> qcPrintJobDetails = qcPrintJob.getPrintJobDetails();
        if (DataFunctions.isEmptyCollection(qcPrintJobDetails)) {
            Logger.logMessage(String.format("No products or modifiers have been found within the print job with a print job tracker ID of %s in KDSXMLBuilder.convertQCPrintJobToKDSTransaction!",
                    Objects.toString(qcPrintJob.getJobTrackerID(), "N/A")), KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we can get a KDS station ID from a given printer ID
        HashMap<Integer, Integer> kdsPrinterToStationIDMappings = getKDSPrinterToStationIDMappings(dataManager);
        if (DataFunctions.isEmptyMap(kdsPrinterToStationIDMappings)) {
            Logger.logMessage("Unable to determine the KDS station ID given a printer ID in KDSXMLBuilder.convertQCPrintJobToKDSTransaction!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        Logger.logMessage("Now logging the QCPrintJob that will be converted into a KDSTransaction.", KDS_XML_GEN_LOG, Logger.LEVEL.TRACE);
        Logger.logMessage(qcPrintJob.toString(), KDS_XML_GEN_LOG, Logger.LEVEL.TRACE);

        // the KDS transaction to return
        KDSTransaction kdsTransaction = new KDSTransaction();

        // the KDS order within the KDS transaction
        KDSOrder kdsOrder = new KDSOrder();

        kdsOrder.setID(qcPrintJob.getOrderNumber());
        kdsOrder.setPOSTerminal(qcPrintJob.getTerminalID());
        kdsOrder.setTransType(TypeData.KDS.TransType.ADD_NEW_ORDER);
        kdsOrder.setOrderStatus(qcPrintJob.getTransTypeID());
        if ((StringFunctions.stringHasContent(qcPrintJob.getPickupDeliveryNote()))
                && (StringFunctions.stringHasContent(getPickupDeliveryNoteForKDS(qcPrintJob.getOrderTypeID(), qcPrintJob.getPickupDeliveryNote())))) {
            kdsOrder.setUserInfo(getPickupDeliveryNoteForKDS(qcPrintJob.getOrderTypeID(), qcPrintJob.getPickupDeliveryNote()));
        }
        if (StringFunctions.stringHasContent(qcPrintJob.getPrevOrderNumsForKDS())) {
            kdsOrder.setServerName(qcPrintJob.getPrevOrderNumsForKDS());
        }
        String userInfo = "";
        String personName = (StringFunctions.stringHasContent(qcPrintJob.getPersonName()) ? qcPrintJob.getPersonName() : "");
        personName = KitchenPrinterCommon.correctAposAndQues(personName);
        String phone = (StringFunctions.stringHasContent(qcPrintJob.getPhone()) ? qcPrintJob.getPhone() : "");
        // create the KDSCustomer
        if ((StringFunctions.stringHasContent(phone)) && (qcPrintJob.getUseTextMessages())) {
            KDSCustomer kdsCustomer = new KDSCustomer();
            kdsCustomer.setID(1);
            kdsCustomer.setPhone(phone);
            kdsOrder.setKdsCustomer(kdsCustomer);
        }
        String transName = (StringFunctions.stringHasContent(qcPrintJob.getTransNameLabel()) ? qcPrintJob.getTransName() : "");
        String transNameLabel = (StringFunctions.stringHasContent(qcPrintJob.getTransNameLabel()) ? qcPrintJob.getTransNameLabel() : "");
        String transComment = (StringFunctions.stringHasContent(qcPrintJob.getTransComment()) ? qcPrintJob.getTransComment() : "");
        transComment = KitchenPrinterCommon.correctAposAndQues(transComment);
        if ((StringFunctions.stringHasContent(personName)) && (!StringFunctions.stringHasContent(transName))) {
            if (StringFunctions.stringHasContent(phone)) {
                userInfo += "Customer: " + personName + " (" + phone + ")";
            }
            else {
                userInfo += "Customer: " + personName;
            }
        }
        else if ((!StringFunctions.stringHasContent(personName)) && (StringFunctions.stringHasContent(transName))) {
            if (StringFunctions.stringHasContent(transNameLabel)) {
                userInfo += transNameLabel + ": " + transName;
            }
            else {
                userInfo += transName;
            }
        }
        KDSOrderMessage orderMessage = new KDSOrderMessage();
        orderMessage.setCount(COUNT);
        orderMessage.setS0(userInfo);
        orderMessage.setS1(transComment);
        kdsOrder.setOrderMessage(orderMessage);
        kdsOrder.setGuestTable((StringFunctions.stringHasContent(personName) ? personName : ""));
        kdsOrder.setDestination((StringFunctions.stringHasContent(transComment) ? transComment : ""));

        // get the print job details that will be sent only to the KDS expeditor station
        ArrayList<QCPrintJobDetail> kdsExpeditorOnlyDetails = getKDSExpeditorOnlyDetails(dataManager, qcPrintJob.getTerminalID(), qcPrintJobDetails);

        int itemCount = 0;
        int modifierCount = 0;
        if (!DataFunctions.isEmptyCollection(kdsExpeditorOnlyDetails)) {
            Logger.logMessage("Found KDS expeditor station specific print job details in KDSXMLBuilder.convertQCPrintJobToKDSTransaction.", KDS_XML_GEN_LOG, Logger.LEVEL.TRACE);

            // remove the KDS expeditor only details
            qcPrintJobDetails.removeAll(kdsExpeditorOnlyDetails);

            // create KDSItem or KDSCondiment for each KDS expeditor specific detail, create parent KDSItems first then KDSCondiments
            for (QCPrintJobDetail qcPrintJobDetail : kdsExpeditorOnlyDetails) {
                if (qcPrintJobDetail.getParentPrintJobDetail() == null) {
                    itemCount++;
                    modifierCount = 0;
                    KDSItem kdsItem = createKDSItem(itemCount, qcPrintJobDetail, kdsPrinterToStationIDMappings);
                    kdsOrder.getItems().add(kdsItem);
                }
            }
            for (QCPrintJobDetail qcPrintJobDetail : kdsExpeditorOnlyDetails) {
                if (qcPrintJobDetail.getParentPrintJobDetail() != null) {
                    modifierCount++;
                    KDSCondiment kdsCondiment = createKDSCondiment(modifierCount, qcPrintJobDetail);

                    // find the parent KDSItem
                    KDSItem parentItem = getCondimentsParentItem(kdsOrder, qcPrintJobDetail);

                    // add the KDSCondiment to the KDSItem
                    if (parentItem != null) {
                        parentItem.getCondiments().add(kdsCondiment);
                    }
                }
            }
        }
        else {
            Logger.logMessage("There aren't any KDS expeditor station specific print job details in KDSXMLBuilder.convertQCPrintJobToKDSTransaction.", KDS_XML_GEN_LOG, Logger.LEVEL.TRACE);
        }

        // create KDSItem or KDSCondiment for non KDS expeditor details being sent to KDS
        if (!DataFunctions.isEmptyCollection(qcPrintJobDetails)) {

            // create KDSItem or KDSCondiment for each KDS expeditor specific detail, create parent KDSItems first then KDSCondiments
            for (QCPrintJobDetail qcPrintJobDetail : qcPrintJobDetails) {
                if (qcPrintJobDetail.getParentPrintJobDetail() == null) {
                    itemCount++;
                    modifierCount = 0;
                    KDSItem kdsItem = createKDSItem(itemCount, qcPrintJobDetail, kdsPrinterToStationIDMappings);
                    kdsOrder.getItems().add(kdsItem);
                }
            }
            for (QCPrintJobDetail qcPrintJobDetail : qcPrintJobDetails) {
                if (qcPrintJobDetail.getParentPrintJobDetail() != null) {
                    modifierCount++;
                    KDSCondiment kdsCondiment = createKDSCondiment(modifierCount, qcPrintJobDetail);

                    // find the parent KDSItem
                    KDSItem parentItem = getCondimentsParentItem(kdsOrder, qcPrintJobDetail);

                    // add the KDSCondiment to the KDSItem
                    if (parentItem != null) {
                        parentItem.getCondiments().add(kdsCondiment);
                    }
                }
            }
        }

        // add any KDS expeditor only details back in so their print status can be updated
        if (!DataFunctions.isEmptyCollection(kdsExpeditorOnlyDetails)) {
            qcPrintJob.getPrintJobDetails().addAll(kdsExpeditorOnlyDetails);
        }

        kdsTransaction.setPATransactionID(qcPrintJob.getPaTransactionID());
        kdsTransaction.setOrder(kdsOrder);

        Logger.logMessage("Now logging the KDSTransaction that will be sent to KDS.", KDS_XML_GEN_LOG, Logger.LEVEL.TRACE);
        Logger.logMessage(kdsTransaction.toString(), KDS_XML_GEN_LOG, Logger.LEVEL.TRACE);

        return kdsTransaction;
    }

    /**
     * <p>Queries the database to determine which KDS station ID corresponds with a given KDS printer ID.</p>
     *
     * @param dataManager The {@link DataManager} instance to use to query the database.
     * @return A {@link HashMap} whose {@link Integer} key is a printer ID and whose {@link Integer} value is the corresponding KDS station ID.
     */
    private static HashMap<Integer, Integer> getKDSPrinterToStationIDMappings (DataManager dataManager) {

        // make sure we have a valid DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KDSXMLBuilder.getKDSPrinterToStationIDMappings can't be null!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        HashMap<Integer, Integer> kdsPrinterToStationIDMappings = new HashMap<>();
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetKDSPrinterIDToStationIDMappings");
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                int printerID = HashMapDataFns.getIntVal(hm, "PRINTERID");
                int kdsStationID = HashMapDataFns.getIntVal(hm, "STATIONID");
                kdsPrinterToStationIDMappings.put(printerID, kdsStationID);
            }
        }

        return kdsPrinterToStationIDMappings;
    }

    /**
     * <p>Builds a compact version of the pickup/delivery note for display on KDS.</p>
     *
     * @param paOrderTypeID ID corresponding to an order type of pickup or delivery.
     * @param pickupDeliveryNote The pickup/delivery note {@link String} to create a shorter version for.
     * @return The compact version of the pickup/delivery note {@link String} for display on KDS.
     */
    private static String getPickupDeliveryNoteForKDS (int paOrderTypeID, String pickupDeliveryNote) {

        if (!((paOrderTypeID == TypeData.OrderType.PICKUP) || (paOrderTypeID == TypeData.OrderType.DELIVERY))) {
            Logger.logMessage("The order type passed to KDSXMLBuilder.getPickupDeliveryNoteForKDS must be for pickup or delivery!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if (!StringFunctions.stringHasContent(pickupDeliveryNote)) {
            Logger.logMessage("The pickup or delivery note passed to KDSXMLBuilder.getPickupDeliveryNoteForKDS can't be null or empty!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if (pickupDeliveryNote.contains("\\n")) {
            pickupDeliveryNote = pickupDeliveryNote.substring(0, pickupDeliveryNote.indexOf("\\n"));
        }

        if (StringFunctions.stringHasContent(getTimeInPickupDeliveryNote(pickupDeliveryNote))) {
            String pickupDelivery = (paOrderTypeID == TypeData.OrderType.PICKUP ? "PU" : "DEL");
            String timeStr = getTimeInPickupDeliveryNote(pickupDeliveryNote);
            return pickupDelivery + " @ " + timeStr;
        }

        return pickupDeliveryNote;
    }

    /**
     * <p>Extracts the pickup or delivery time from the given {@link String}.</p>
     *
     * @param pickupDeliveryNote The pickup/delivery note {@link String} to find the time within.
     * @return The pickup or delivery time extracted from the given {@link String}.
     */
    private static String getTimeInPickupDeliveryNote (String pickupDeliveryNote) {

        if (!StringFunctions.stringHasContent(pickupDeliveryNote)) {
            Logger.logMessage("The pickup or delivery note passed to KDSXMLBuilder.getTimeInPickupDeliveryNote can't be null or empty!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        Pattern timePattern = Pattern.compile("\\d+:\\d+");
        Matcher matcher = timePattern.matcher(pickupDeliveryNote);
        String matchedTime = "";
        if (matcher.find()) {
            matchedTime = matcher.group(0);
        }
        if (StringFunctions.stringHasContent(matchedTime)) {
            String timeStr = pickupDeliveryNote.substring(pickupDeliveryNote.indexOf(matchedTime));
            timeStr = timeStr.replaceAll("\\.", "");
            return timeStr;
        }
        else {
            Logger.logMessage("Unable to extract the time form the String %s in KDSXMLBuilder.getTimeInPickupDeliveryNote!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }
    }

    /**
     * <p>Iterates through the print job details to be sent to KDS and gets the print job details that will only be sent to the KDS expeditor.</p>
     *
     * @param dataManager The {@link DataManager} instance to use to query the database.
     * @param terminalID The ID of the terminal the transaction took place on.
     * @param qcPrintJobDetails An {@link ArrayList} of {@link QCPrintJobDetail} corresponding to the print job details to be sent to KDS.
     * @return An {@link ArrayList} of {@link QCPrintJobDetail} corresponding to the print job details to the print job details that will only be sent to the KDS expeditor.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private static ArrayList<QCPrintJobDetail> getKDSExpeditorOnlyDetails (DataManager dataManager, int terminalID, ArrayList<QCPrintJobDetail> qcPrintJobDetails) {

        // make sure we have a valid DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KDSXMLBuilder.getKDSExpeditorOnlyDetails can't be null!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid terminal ID
        if (terminalID <= 0) {
            Logger.logMessage("The terminal ID passed to KDSXMLBuilder.getKDSExpeditorOnlyDetails must be greater than 0!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database to get the ID of the KDS expeditor station
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetKDSExpStnPtrIDFromTermID").addIDList(1, terminalID);
        Object queryRes = sql.getSingleField(dataManager);
        int kdsExpeditorPrinterID = -1;
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString()))) {
            kdsExpeditorPrinterID = Integer.parseInt(queryRes.toString());
        }

        if (kdsExpeditorPrinterID > 0) {
            Logger.logMessage(String.format("Found the KDS expeditor station with a printer ID of %s within the same revenue " +
                            "center as the terminal with an ID of %s in KDSXMLBuilder.getKDSExpeditorOnlyDetails.",
                    Objects.toString(kdsExpeditorPrinterID, "N/A"),
                    Objects.toString(terminalID, "N/A")), KDS_XML_GEN_LOG, Logger.LEVEL.TRACE);
            // get all details being sent to the KDS expeditor station
            ArrayList<QCPrintJobDetail> kdsExpeditorOnlyDetails = new ArrayList<>();
            for (QCPrintJobDetail qcPrintJobDetail : qcPrintJobDetails) {
                if (qcPrintJobDetail.getPrinterID() == kdsExpeditorPrinterID) {
                    kdsExpeditorOnlyDetails.add(qcPrintJobDetail);
                }
            }
            return kdsExpeditorOnlyDetails;
        }
        else {
            Logger.logMessage(String.format("No KDS expeditor station was found within the same revenue center as the terminal with an ID of %s in KDSXMLBuilder.getKDSExpeditorOnlyDetails.",
                    Objects.toString(terminalID, "N/A")), KDS_XML_GEN_LOG, Logger.LEVEL.TRACE);
        }

        return null;
    }

    /**
     * <p>Creates a {@link KDSItem} from a given {@link QCPrintJobDetail}.</p>
     *
     * @param itemCount The number of items within the order.
     * @param qcPrintJobDetail The {@link QCPrintJobDetail} to turn into a {@link KDSItem}.
     * @param kdsPrinterToStationIDMappings A {@link HashMap} whose key is the {@link Integer} printer ID and
     * whose {@link Integer} value is the corresponding KDS station ID.
     * @return The {@link KDSItem} built from the {@link QCPrintJobDetail}.
     */
    private static KDSItem createKDSItem (int itemCount, QCPrintJobDetail qcPrintJobDetail, HashMap<Integer, Integer> kdsPrinterToStationIDMappings) {

        // make sure the item count is valid
        if (itemCount <= 0) {
            Logger.logMessage("The item count passed to KDSXMLBuilder.createKDSItem must be greater than 0!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the QCPrintJobDetail is valid
        if (qcPrintJobDetail == null) {
            Logger.logMessage("The print job detail passed to KDSXMLBuilder.createKDSItem can't be null!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the printer to KDS station ID mappings are valid
        if (DataFunctions.isEmptyMap(kdsPrinterToStationIDMappings)) {
            Logger.logMessage("The printer to KDS station ID mappings passed to KDSXMLBuilder.createKDSItem can't be null or empty!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if ((StringFunctions.stringHasContent(qcPrintJobDetail.getDecodedLineDetail())) && (qcPrintJobDetail.getDecodedLineDetail().contains("&#x27;"))) {
            String lineDetail = qcPrintJobDetail.getDecodedLineDetail().replaceAll("&#x27;", "\'");
            qcPrintJobDetail.setLineDetail(java.util.Base64.getEncoder().encodeToString(lineDetail.getBytes()));
        }

        KDSItem kdsItem = new KDSItem();
        kdsItem.setID(qcPrintJobDetail.getPAPluID() + "_" + itemCount);
        kdsItem.setTransType(TypeData.KDS.TransType.ADD_NEW_ORDER);
        kdsItem.setName(removeQuantityFromKDSItemName(qcPrintJobDetail.getDecodedLineDetail()));
        kdsItem.setQuantity(((int) Math.round(qcPrintJobDetail.getQuantity())));
        kdsItem.setKDSStation(kdsPrinterToStationIDMappings.get(qcPrintJobDetail.getPrinterID()));
        kdsItem.setHiddenStations(qcPrintJobDetail.getHideStation());
        kdsItem.setTransLineItemID(qcPrintJobDetail.getPaTransLineItemID());
        kdsItem.setPrinterID(qcPrintJobDetail.getPrinterID());

        return kdsItem;
    }

    /**
     * <p>Removes quantities from any KDS item names.</p>
     *
     * @param kdsItemName The item name {@link String} potentially containing quantities to remove.
     * @return The item name {@link String} with quantities removed.
     */
    private static String removeQuantityFromKDSItemName (String kdsItemName) {

        if (StringFunctions.stringHasContent(kdsItemName)) {
            Pattern pattern = Pattern.compile("(\\s+)?((\\d+)X)");
            Matcher matcher = pattern.matcher(kdsItemName);

            if (matcher.find()) {
                kdsItemName = kdsItemName.trim();
                kdsItemName = kdsItemName.replaceFirst("((\\d+)X)", "");
                return removeQuantityFromKDSItemName(kdsItemName);
            }
            else {
                return kdsItemName.trim();
            }

        }

        return kdsItemName;
    }

    /**
     * <p>Creates a {@link KDSCondiment} from a given {@link QCPrintJobDetail}.</p>
     *
     * @param modifierCount The number of modifiers within the parent product.
     * @param qcPrintJobDetail The {@link QCPrintJobDetail} to turn into a {@link KDSCondiment}.
     * @return The {@link KDSCondiment} built from the {@link QCPrintJobDetail}.
     */
    private static KDSCondiment createKDSCondiment (int modifierCount, QCPrintJobDetail qcPrintJobDetail) {

        // make sure the modifier count is valid
        if (modifierCount <= 0) {
            Logger.logMessage("The modifier count passed to KDSXMLBuilder.createKDSCondiment must be greater than 0!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the QCPrintJobDetail is valid
        if (qcPrintJobDetail == null) {
            Logger.logMessage("The print job detail passed to KDSXMLBuilder.createKDSCondiment can't be null!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if ((StringFunctions.stringHasContent(qcPrintJobDetail.getDecodedLineDetail())) && (qcPrintJobDetail.getDecodedLineDetail().contains("&#x27;"))) {
            String lineDetail = qcPrintJobDetail.getDecodedLineDetail().replaceAll("&#x27;", "\'");
            qcPrintJobDetail.setLineDetail(java.util.Base64.getEncoder().encodeToString(lineDetail.getBytes()));
        }

        KDSCondiment kdsCondiment = new KDSCondiment();
        kdsCondiment.setID(qcPrintJobDetail.getPAPluID() + "_" + modifierCount);
        kdsCondiment.setTransType(TypeData.KDS.TransType.ADD_NEW_ORDER);
        kdsCondiment.setName(qcPrintJobDetail.getDecodedLineDetail());

        return kdsCondiment;
    }

    /**
     * <p>Gets the parent {@link KDSItem} for a {@link KDSCondiment}.</p>
     *
     * @param kdsOrder The {@link KDSOrder} that contains the item that contains the condiment.
     * @param qcPrintJobDetail The {@link QCPrintJobDetail} corresponding to the {@link KDSCondiment}.
     * @return The parent {@link KDSItem} for the {@link KDSCondiment}.
     */
    private static KDSItem getCondimentsParentItem (KDSOrder kdsOrder, QCPrintJobDetail qcPrintJobDetail) {

        // make sure the KDSOrder is valid
        if (kdsOrder == null) {
            Logger.logMessage("The KDSOrder passed to KDSXMLBuilder.getCondimentsParentItem can't be null!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the QCPrintJobDetail is valid
        if (qcPrintJobDetail == null) {
            Logger.logMessage("The print job detail passed to KDSXMLBuilder.getCondimentsParentItem can't be null!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        KDSItem parentItem = null;
        if (!DataFunctions.isEmptyCollection(kdsOrder.getItems())) {
            for (KDSItem kdsItem : kdsOrder.getItems()) {
                if ((qcPrintJobDetail.getPaTransLineItemID() == kdsItem.getTransLineItemID()) && (qcPrintJobDetail.getPrinterID() == kdsItem.getPrinterID())) {
                    parentItem = kdsItem;
                    break;
                }
            }
        }

        return parentItem;
    }

    /**
     * <p>Converts a KDS transaction into XML that may be parsed by KDS.</p>
     *
     * @param kdsTransaction The {@link KDSTransaction} to convert to XML.
     * @return The {@link KDSTransaction} as a XML {@link String} that can be parsed by KDS.
     */
    @SuppressWarnings({"SpellCheckingInspection", "OverlyComplexMethod"})
    private static String getKDSXML (KDSTransaction kdsTransaction) {

        // make sure we have a valid KDS transaction
        if (kdsTransaction == null) {
            Logger.logMessage("The KDSTransaction passed to KDSXMLBuilder.getKDSXML can't be null!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the KDS transaction contains a valid KDS order
        KDSOrder kdsOrder = kdsTransaction.getOrder();
        if (kdsOrder == null) {
            Logger.logMessage("The KDSOrder within the KDSTransaction passed to KDSXMLBuilder.getKDSXML can't be null!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        StringBuilder stringBuilder = new StringBuilder();

        String newLine = System.getProperty("line.separator");

        // define a XML file
        stringBuilder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        stringBuilder.append(newLine);

        // start of the transaction
        stringBuilder.append("<Transaction>");
        stringBuilder.append(newLine);

        // start of the order
        stringBuilder.append("    <Order>");
        stringBuilder.append(newLine);

        // order ID
        stringBuilder.append("        <ID>");
        stringBuilder.append(convertXMLReservedChars(kdsOrder.getID()));
        stringBuilder.append("</ID>");
        stringBuilder.append(newLine);

        // POS terminal
        stringBuilder.append("        <PosTerminal>");
        stringBuilder.append(kdsOrder.getPOSTerminal());
        stringBuilder.append("</PosTerminal>");
        stringBuilder.append(newLine);

        // transaction type
        stringBuilder.append("        <TransType>");
        stringBuilder.append(kdsOrder.getTransType());
        stringBuilder.append("</TransType>");
        stringBuilder.append(newLine);

        // order status
        stringBuilder.append("        <OrderStatus>");
        stringBuilder.append(kdsOrder.getOrderStatus());
        stringBuilder.append("</OrderStatus>");
        stringBuilder.append(newLine);

        // order type
        if (StringFunctions.stringHasContent(kdsOrder.getOrderType())) {
            stringBuilder.append("        <OrderType>");
            stringBuilder.append(kdsOrder.getOrderType());
            stringBuilder.append("</OrderType>");
            stringBuilder.append(newLine);
        }

        // server name
        if (StringFunctions.stringHasContent(kdsOrder.getServerName())) {
            stringBuilder.append("        <ServerName>");
            stringBuilder.append(convertXMLReservedChars(kdsOrder.getServerName()));
            stringBuilder.append("</ServerName>");
            stringBuilder.append(newLine);
        }

        // destination
        if (StringFunctions.stringHasContent(kdsOrder.getDestination())) {
            stringBuilder.append("        <Destination>");
            stringBuilder.append(convertXMLReservedChars(kdsOrder.getDestination()));
            stringBuilder.append("</Destination>");
            stringBuilder.append(newLine);
        }

        // guest table
        if (StringFunctions.stringHasContent(kdsOrder.getGuestTable())) {
            stringBuilder.append("        <GuestTable>");
            stringBuilder.append(convertXMLReservedChars(kdsOrder.getGuestTable()));
            stringBuilder.append("</GuestTable>");
            stringBuilder.append(newLine);
        }

        // user information
        if (StringFunctions.stringHasContent(kdsOrder.getUserInfo())) {
            stringBuilder.append("        <UserInfo>");
            stringBuilder.append(convertXMLReservedChars(kdsOrder.getUserInfo()));
            stringBuilder.append("</UserInfo>");
            stringBuilder.append(newLine);
        }

        // customer
        if (kdsOrder.getKdsCustomer() != null) {
            // start of customer
            stringBuilder.append("        <Customer>");
            stringBuilder.append(newLine);

            // an ID for the customer
            stringBuilder.append("            <ID>");
            stringBuilder.append(kdsOrder.getKdsCustomer().getID());
            stringBuilder.append("</ID>");
            stringBuilder.append(newLine);

            // customer's phone number
            stringBuilder.append("            <Phone>");
            stringBuilder.append(kdsOrder.getKdsCustomer().getPhone());
            stringBuilder.append("</Phone>");
            stringBuilder.append(newLine);

            // end of customer
            stringBuilder.append("        </Customer>");
            stringBuilder.append(newLine);
        }

        // order message
        if ((kdsOrder.getOrderMessage() != null)
                && ((kdsOrder.getOrderMessage().getCount() > 0)
                || (StringFunctions.stringHasContent(kdsOrder.getOrderMessage().getS0()))
                || (StringFunctions.stringHasContent(kdsOrder.getOrderMessage().getS1())))) {
            // start of order message
            stringBuilder.append("        <OrderMessages>");
            stringBuilder.append(newLine);

            // order message count
            if (kdsOrder.getOrderMessage().getCount() > 0) {
                stringBuilder.append("            <Count>");
                stringBuilder.append(kdsOrder.getOrderMessage().getCount());
                stringBuilder.append("</Count>");
                stringBuilder.append(newLine);
            }

            // order message 0
            if (StringFunctions.stringHasContent(kdsOrder.getOrderMessage().getS0())) {
                stringBuilder.append("            <S0>");
                stringBuilder.append(convertXMLReservedChars(kdsOrder.getOrderMessage().getS0()));
                stringBuilder.append("</S0>");
                stringBuilder.append(newLine);
            }

            // order message 1
            if (StringFunctions.stringHasContent(kdsOrder.getOrderMessage().getS1())) {
                stringBuilder.append("            <S1>");
                stringBuilder.append(convertXMLReservedChars(kdsOrder.getOrderMessage().getS1()));
                stringBuilder.append("</S1>");
                stringBuilder.append(newLine);
            }

            // end of order message
            stringBuilder.append("        </OrderMessages>");
            stringBuilder.append(newLine);
        }

        // make sure there are items within the order
        if (DataFunctions.isEmptyCollection(kdsOrder.getItems())) {
            Logger.logMessage(String.format("No KDSItems were found within the KDSOrder in KDSXMLBuilder.getKDSXML, unable " +
                            "to build the XML file to sent to KDS for the transaction with an ID of %s!",
                    Objects.toString(kdsTransaction.getPATransactionID(), "N/A")), KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // get the items within the order
        ArrayList<KDSItem> items = kdsOrder.getItems();

        // add the items to the XML
        for (KDSItem item : items) {
            // start of item
            stringBuilder.append("        <Item>");
            stringBuilder.append(newLine);

            // ID of the item
            stringBuilder.append("            <ID>");
            stringBuilder.append(item.getID());
            stringBuilder.append("</ID>");
            stringBuilder.append(newLine);

            // transaction type of the item
            stringBuilder.append("            <TransType>");
            stringBuilder.append(item.getTransType());
            stringBuilder.append("</TransType>");
            stringBuilder.append(newLine);

            // name of the item
            if (StringFunctions.stringHasContent(item.getName())) {
                stringBuilder.append("            <Name>");
                stringBuilder.append(convertXMLReservedChars(item.getName()));
                stringBuilder.append("</Name>");
                stringBuilder.append(newLine);
            }

            // quantity of the item
            stringBuilder.append("            <Quantity>");
            stringBuilder.append(item.getQuantity());
            stringBuilder.append("</Quantity>");
            stringBuilder.append(newLine);

            // the ID of the KDS station to display the item on
            stringBuilder.append("            <KDSStation>");
            stringBuilder.append(item.getKDSStation());
            stringBuilder.append("</KDSStation>");
            stringBuilder.append(newLine);

            // pre modifier for the item
            if ((item.getPreModifier() != null)
                    && ((item.getPreModifier().getCount() > 0)
                    || (StringFunctions.stringHasContent(item.getPreModifier().getS0()))
                    || (StringFunctions.stringHasContent(item.getPreModifier().getS1())))) {
                // start of pre modifier
                stringBuilder.append("            <PreModifier>");
                stringBuilder.append(newLine);

                // pre modifier count
                if (item.getPreModifier().getCount() > 0) {
                    stringBuilder.append("                <Count>");
                    stringBuilder.append(item.getPreModifier().getCount());
                    stringBuilder.append("</Count>");
                    stringBuilder.append(newLine);
                }

                // pre modifier 0
                if (StringFunctions.stringHasContent(item.getPreModifier().getS0())) {
                    stringBuilder.append("                <S0>");
                    stringBuilder.append(convertXMLReservedChars(item.getPreModifier().getS0()));
                    stringBuilder.append("</S0>");
                    stringBuilder.append(newLine);
                }

                // pre modifier 1
                if (StringFunctions.stringHasContent(item.getPreModifier().getS1())) {
                    stringBuilder.append("                <S1>");
                    stringBuilder.append(convertXMLReservedChars(item.getPreModifier().getS1()));
                    stringBuilder.append("</S1>");
                    stringBuilder.append(newLine);
                }

                // end of pre modifier
                stringBuilder.append("            </PreModifier>");
                stringBuilder.append(newLine);
            }

            // the ID of the KDS station to hide the item on
            if (StringFunctions.stringHasContent(item.getHiddenStations())) {
                stringBuilder.append("            <HideStation>");
                stringBuilder.append(convertXMLReservedChars(item.getHiddenStations()));
                stringBuilder.append("</HideStation>");
                stringBuilder.append(newLine);
            }

            // get any condiments for the item
            ArrayList<KDSCondiment> condiments = item.getCondiments();
            if (!DataFunctions.isEmptyCollection(condiments)) {
                for (KDSCondiment condiment : condiments) {
                    // start of condiment
                    stringBuilder.append("            <Condiment>");
                    stringBuilder.append(newLine);

                    // ID of the condiment
                    stringBuilder.append("                <ID>");
                    stringBuilder.append(condiment.getID());
                    stringBuilder.append("</ID>");
                    stringBuilder.append(newLine);

                    // pre modifier for the condiment
                    if ((condiment.getPreModifier() != null)
                            && ((condiment.getPreModifier().getCount() > 0)
                            || (StringFunctions.stringHasContent(condiment.getPreModifier().getS0()))
                            || (StringFunctions.stringHasContent(condiment.getPreModifier().getS1())))) {
                        // start of pre modifier
                        stringBuilder.append("                <PreModifier>");
                        stringBuilder.append(newLine);

                        // pre modifier count
                        if (condiment.getPreModifier().getCount() > 0) {
                            stringBuilder.append("                    <Count>");
                            stringBuilder.append(condiment.getPreModifier().getCount());
                            stringBuilder.append("</Count>");
                            stringBuilder.append(newLine);
                        }

                        // pre modifier 0
                        if (StringFunctions.stringHasContent(condiment.getPreModifier().getS0())) {
                            stringBuilder.append("                    <S0>");
                            stringBuilder.append(convertXMLReservedChars(condiment.getPreModifier().getS0()));
                            stringBuilder.append("</S0>");
                            stringBuilder.append(newLine);
                        }

                        // pre modifier 1
                        if (StringFunctions.stringHasContent(condiment.getPreModifier().getS1())) {
                            stringBuilder.append("                    <S1>");
                            stringBuilder.append(convertXMLReservedChars(condiment.getPreModifier().getS1()));
                            stringBuilder.append("</S1>");
                            stringBuilder.append(newLine);
                        }

                        // end of pre modifier
                        stringBuilder.append("                </PreModifier>");
                        stringBuilder.append(newLine);
                    }

                    // transaction type of the condiment
                    stringBuilder.append("                <TransType>");
                    stringBuilder.append(condiment.getTransType());
                    stringBuilder.append("</TransType>");
                    stringBuilder.append(newLine);

                    // name of the condiment
                    if (StringFunctions.stringHasContent(condiment.getName())) {
                        stringBuilder.append("                <Name>");
                        stringBuilder.append(convertXMLReservedChars(condiment.getName()));
                        stringBuilder.append("</Name>");
                        stringBuilder.append(newLine);
                    }

                    // action for the condiment
                    if (condiment.getAction() != 0) {
                        stringBuilder.append("                <Action>");
                        stringBuilder.append(condiment.getAction());
                        stringBuilder.append("</Action>");
                        stringBuilder.append(newLine);
                    }

                    // end of condiment
                    stringBuilder.append("            </Condiment>");
                    stringBuilder.append(newLine);
                }
            }

            // end of item
            stringBuilder.append("        </Item>");
            stringBuilder.append(newLine);
        }

        // end of the order
        stringBuilder.append("    </Order>");
        stringBuilder.append(newLine);

        // end of the transaction
        stringBuilder.append("</Transaction>");

        return stringBuilder.toString();
    }

    /**
     * <p>Replaces any reserved XML characters.</p>
     *
     * @param strToConvert The {@link String} to remove any reserved XML characters from.
     * @return The given {@link String} with any reserved XML characters replaced.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public static String convertXMLReservedChars (String strToConvert) {
        if (!StringFunctions.stringHasContent(strToConvert)) {
            Logger.logMessage("The String passed to KDSXMLBuilder.convertXMLReservedChars can't be null or empty!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        final String[] reservedChars = new String[]{"<", ">", "&", "\'", "\"", "%"};
        final String[] replacementChars = new String[]{"&lt;", "&gt;", "&amp;", "&apos;", "&quot;", "&#37;"};

        return StringUtils.replaceEach(strToConvert, reservedChars, replacementChars);
    }

    /**
     * <p>Converts the {@link KDSTransaction} into a XML {@link File}.</p>
     *
     * @param kdsTransaction The {@link KDSTransaction} to convert into a XML {@link File}.
     * @param backupFile The {@link File} the XML content should be added to.
     * @return The {@link KDSTransaction} as a XML {@link File}.
     */
    public static File getKDSXMLFile (KDSTransaction kdsTransaction, File backupFile) {

        // make sure we have a valid KDS transaction
        if (kdsTransaction == null) {
            Logger.logMessage("The KDSTransaction passed to KDSXMLBuilder.getKDSXMLFile can't be null!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid backup file
        if (backupFile == null) {
            Logger.logMessage("The backup File passed to KDSXMLBuilder.getKDSXMLFile can't be null!", KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // get the KDS Transaction as a XML String to build the KDS XML File
        String kdsXMLStr = getKDSXML(kdsTransaction);
        if (!StringFunctions.stringHasContent(kdsXMLStr)) {
            Logger.logMessage(String.format("Can't convert the KDSTransaction with a transaction ID of %s into a XML String in KDSXMLBuilder.getKDSXMLFile!",
                    Objects.toString(kdsTransaction.getPATransactionID(), "N/A")), KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        Logger.logMessage("Now logging the KDS XML String that will be used to build the KDS XML File to send to KDS.", KDS_XML_GEN_LOG, Logger.LEVEL.DEBUG);
        Logger.logMessage(kdsXMLStr, KDS_XML_GEN_LOG, Logger.LEVEL.DEBUG);

        // convert the KDS XML String to a Document
        Document document = DataFunctions.convertStrToDoc(kdsXMLStr);

        if (document == null) {
            Logger.logMessage(String.format("A problem occurred while trying to convert the KDS XML String into a Document in KDSXMLBuilder.getKDSXMLFile for the KDSTransaction with an ID of %s!",
                    Objects.toString(kdsTransaction.getPATransactionID(), "N/A")), KDS_XML_GEN_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // set output properties for the transformer that will transform the Document into a XML File
        HashMap<String, String> transOutProps = new HashMap<>();
        transOutProps.put(OutputKeys.OMIT_XML_DECLARATION, "no");
        transOutProps.put(OutputKeys.METHOD, "xml");
        transOutProps.put(OutputKeys.INDENT, "yes");
        transOutProps.put(OutputKeys.ENCODING, "UTF-8");

        // transform the Document into a XML file
        return DataFunctions.transformDocToXML(document, backupFile, transOutProps);
    }

}