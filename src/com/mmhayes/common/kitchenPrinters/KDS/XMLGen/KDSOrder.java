package com.mmhayes.common.kitchenPrinters.KDS.XMLGen;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-02 10:48:52 -0400 (Wed, 02 Sep 2020) $: Date of last commit
    $Rev: 12536 $: Revision of last commit
    Notes: Represents an order within a transaction to be sent to KDS.
*/

import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;
import java.util.Objects;

public class KDSOrder {

    // private member variables of a KDSOrder
    private String id;
    private int posTerminal;
    private int transType;
    private int orderStatus;
    private String orderType;
    private String serverName;
    private String destination;
    private String guestTable;
    private String userInfo;
    private KDSCustomer kdsCustomer;
    private KDSOrderMessage orderMessage;
    private ArrayList<KDSItem> items = new ArrayList<>();

    /**
     * <p>Constructor for a {@link KDSOrder}.</p>
     *
     */
    public KDSOrder () {}

    /**
     * <p>The {@link String} representation of a {@link KDSOrder}.</p>
     *
     * @return The {@link KDSOrder} instance as a {@link String}.
     */
    @SuppressWarnings("OverlyComplexMethod")
    @Override
    public String toString () {

        String itemsStr = "";
        if (!DataFunctions.isEmptyCollection(items)) {
            itemsStr += "[";
            for (KDSItem item : items) {
                if (item.equals(items.get(items.size() - 1))) {
                    itemsStr += "[" + item.toString() + "]";
                }
                else {
                    itemsStr += "[" + item.toString() + "], ";
                }
            }
            itemsStr += "]";
        }

        return String.format("ID: %s, POSTERMINAL: %s, TRANSTYPE: %s, ORDERSTATUS: %s, ORDERTYPE: %s, SERVERNAME: %s, DESTINATION: %s, GUESTTABLE: %s, USERINFO: %s, CUSTOMER: %s, ORDERMESSAGE: %s, ITEMS: %s",
                Objects.toString((StringFunctions.stringHasContent(id) ? id : "N/A"), "N/A"),
                Objects.toString((posTerminal > 0 ? posTerminal : "N/A"), "N/A"),
                Objects.toString((transType > 0 ? transType : "N/A"), "N/A"),
                Objects.toString((orderStatus > 0 ? orderStatus : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(orderType) ? orderType : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(serverName) ? serverName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(destination) ? destination : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(guestTable) ? guestTable : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(userInfo) ? userInfo : "N/A"), "N/A"),
                Objects.toString((kdsCustomer != null ? kdsCustomer.toString() : "N/A"), "N/A"),
                Objects.toString((orderMessage != null ? orderMessage.toString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(itemsStr) ? itemsStr : "N/A"), "N/A"));

    }

    /**
     * <p>Getter for the id field of the {@link KDSOrder}.</p>
     *
     * @return The id field of the {@link KDSOrder}.
     */
    public String getID () {
        return id;
    }

    /**
     * <p>Setter for the id field of the {@link KDSOrder}.</p>
     *
     * @param id The id field of the {@link KDSOrder}.
     */
    public void setID (String id) {
        this.id = id;
    }

    /**
     * <p>Getter for the posTerminal field of the {@link KDSOrder}.</p>
     *
     * @return The posTerminal field of the {@link KDSOrder}.
     */
    public int getPOSTerminal () {
        return posTerminal;
    }

    /**
     * <p>Setter for the posTerminal field of the {@link KDSOrder}.</p>
     *
     * @param posTerminal The posTerminal field of the {@link KDSOrder}.
     */
    public void setPOSTerminal (int posTerminal) {
        this.posTerminal = posTerminal;
    }

    /**
     * <p>Getter for the transType field of the {@link KDSOrder}.</p>
     *
     * @return The transType field of the {@link KDSOrder}.
     */
    public int getTransType () {
        return transType;
    }

    /**
     * <p>Setter for the transType field of the {@link KDSOrder}.</p>
     *
     * @param transType The transType field of the {@link KDSOrder}.
     */
    public void setTransType (int transType) {
        this.transType = transType;
    }

    /**
     * <p>Getter for the orderStatus field of the {@link KDSOrder}.</p>
     *
     * @return The orderStatus field of the {@link KDSOrder}.
     */
    public int getOrderStatus () {
        return orderStatus;
    }

    /**
     * <p>Setter for the orderStatus field of the {@link KDSOrder}.</p>
     *
     * @param orderStatus The orderStatus field of the {@link KDSOrder}.
     */
    public void setOrderStatus (int orderStatus) {
        if (orderStatus == TypeData.TranType.OPEN) {
            // unpaid
            this.orderStatus = TypeData.KDS.OrderStatus.UNPAID;
        }
        else {
            // paid
            this.orderStatus = TypeData.KDS.OrderStatus.PAID;
        }
    }

    /**
     * <p>Getter for the orderType field of the {@link KDSOrder}.</p>
     *
     * @return The orderType field of the {@link KDSOrder}.
     */
    public String getOrderType () {
        return orderType;
    }

    /**
     * <p>Setter for the orderType field of the {@link KDSOrder}.</p>
     *
     * @param orderType The orderType field of the {@link KDSOrder}.
     */
    public void setOrderType (String orderType) {
        this.orderType = orderType;
    }

    /**
     * <p>Getter for the serverName field of the {@link KDSOrder}.</p>
     *
     * @return The serverName field of the {@link KDSOrder}.
     */
    public String getServerName () {
        return serverName;
    }

    /**
     * <p>Setter for the serverName field of the {@link KDSOrder}.</p>
     *
     * @param serverName The serverName field of the {@link KDSOrder}.
     */
    public void setServerName (String serverName) {
        this.serverName = serverName;
    }

    /**
     * <p>Getter for the destination field of the {@link KDSOrder}.</p>
     *
     * @return The destination field of the {@link KDSOrder}.
     */
    public String getDestination () {
        return destination;
    }

    /**
     * <p>Setter for the destination field of the {@link KDSOrder}.</p>
     *
     * @param destination The destination field of the {@link KDSOrder}.
     */
    public void setDestination (String destination) {
        this.destination = destination;
    }

    /**
     * <p>Getter for the guestTable field of the {@link KDSOrder}.</p>
     *
     * @return The guestTable field of the {@link KDSOrder}.
     */
    public String getGuestTable () {
        return guestTable;
    }

    /**
     * <p>Setter for the guestTable field of the {@link KDSOrder}.</p>
     *
     * @param guestTable The guestTable field of the {@link KDSOrder}.
     */
    public void setGuestTable (String guestTable) {
        this.guestTable = guestTable;
    }

    /**
     * <p>Getter for the userInfo field of the {@link KDSOrder}.</p>
     *
     * @return The userInfo field of the {@link KDSOrder}.
     */
    public String getUserInfo () {
        return userInfo;
    }

    /**
     * <p>Setter for the userInfo field of the {@link KDSOrder}.</p>
     *
     * @param userInfo The userInfo field of the {@link KDSOrder}.
     */
    public void setUserInfo (String userInfo) {
        this.userInfo = userInfo;
    }

    /**
     * <p>Getter for the kdsCustomer field of the {@link KDSOrder}.</p>
     *
     * @return The kdsCustomer field of the {@link KDSOrder}.
     */
    public KDSCustomer getKdsCustomer () {
        return kdsCustomer;
    }

    /**
     * <p>Setter for the kdsCustomer field of the {@link KDSOrder}.</p>
     *
     * @param kdsCustomer The kdsCustomer field of the {@link KDSOrder}.
     */
    public void setKdsCustomer (KDSCustomer kdsCustomer) {
        this.kdsCustomer = kdsCustomer;
    }

    /**
     * <p>Getter for the orderMessage field of the {@link KDSOrder}.</p>
     *
     * @return The orderMessage field of the {@link KDSOrder}.
     */
    public KDSOrderMessage getOrderMessage () {
        return orderMessage;
    }

    /**
     * <p>Setter for the orderMessage field of the {@link KDSOrder}.</p>
     *
     * @param orderMessage The orderMessage field of the {@link KDSOrder}.
     */
    public void setOrderMessage (KDSOrderMessage orderMessage) {
        this.orderMessage = orderMessage;
    }

    /**
     * <p>Getter for the items field of the {@link KDSOrder}.</p>
     *
     * @return The items field of the {@link KDSOrder}.
     */
    public ArrayList<KDSItem> getItems () {
        return items;
    }

    /**
     * <p>Setter for the items field of the {@link KDSOrder}.</p>
     *
     * @param items The items field of the {@link KDSOrder}.
     */
    public void setItems (ArrayList<KDSItem> items) {
        this.items = items;
    }

}