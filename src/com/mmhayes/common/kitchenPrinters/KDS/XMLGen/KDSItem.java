package com.mmhayes.common.kitchenPrinters.KDS.XMLGen;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-03 15:58:08 -0400 (Thu, 03 Sep 2020) $: Date of last commit
    $Rev: 12563 $: Revision of last commit
    Notes: Represents an item within an order to be sent to KDS.
*/

import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;
import java.util.Objects;

public class KDSItem {

    // private member variables for a KDSItem
    private String id;
    private int transType;
    private String name;
    private int quantity;
    private int kdsStation;
    private KDSPreModifier preModifier;
    private String hiddenStations;
    private int transLineItemID;
    private int printerID;
    private ArrayList<KDSCondiment> condiments = new ArrayList<>();

    /**
     * <p>Constructor for a {@link KDSItem}.</p>
     *
     */
    public KDSItem () {}

    /**
     * <p>The {@link String} representation of a {@link KDSItem}.</p>
     *
     * @return The {@link KDSItem} instance as a {@link String}.
     */
    @SuppressWarnings("OverlyComplexMethod")
    @Override
    public String toString () {

        String condimentsStr = "";
        if (!DataFunctions.isEmptyCollection(condiments)) {
            condimentsStr += "[";
            for (KDSCondiment condiment : condiments) {
                if (condiment.equals(condiments.get(condiments.size() - 1))) {
                    condimentsStr += "[" + condiment.toString() + "]";
                }
                else {
                    condimentsStr += "[" + condiment.toString() + "], ";
                }
            }
            condimentsStr += "]";
        }

        return String.format("ID: %s, TRANSTYPE: %s, NAME: %s, QUANTITY: %s, KDSSTATION: %s, PREMODIFIER: %s, HIDDENSTATIONS: %s, TRANSLINEITEMID: %s, PRINTERID: %s, CONDIMENTS: %s",
                Objects.toString((StringFunctions.stringHasContent(id) ? id : "N/A"), "N/A"),
                Objects.toString((transType > 0 ? transType : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((quantity > 0 ? quantity : "N/A"), "N/A"),
                Objects.toString((kdsStation > 0 ? kdsStation : "N/A"), "N/A"),
                Objects.toString((preModifier != null ? preModifier.toString() : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(hiddenStations) ? hiddenStations : "N/A"), "N/A"),
                Objects.toString((transLineItemID > 0 ? transLineItemID : "N/A"), "N/A"),
                Objects.toString((printerID > 0 ? printerID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(condimentsStr) ? condimentsStr : "N/A"), "N/A"));

    }

    /**
     * <p>Getter for the id field of the {@link KDSItem}.</p>
     *
     * @return The id field of the {@link KDSItem}.
     */
    public String getID () {
        return id;
    }

    /**
     * <p>Setter for the id field of the {@link KDSItem}.</p>
     *
     * @param id The id field of the {@link KDSItem}.
     */
    public void setID (String id) {
        this.id = id;
    }

    /**
     * <p>Getter for the transType field of the {@link KDSItem}.</p>
     *
     * @return The transType field of the {@link KDSItem}.
     */
    public int getTransType () {
        return transType;
    }

    /**
     * <p>Setter for the transType field of the {@link KDSItem}.</p>
     *
     * @param transType The transType field of the {@link KDSItem}.
     */
    public void setTransType (int transType) {
        this.transType = transType;
    }

    /**
     * <p>Getter for the name field of the {@link KDSItem}.</p>
     *
     * @return The name field of the {@link KDSItem}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link KDSItem}.</p>
     *
     * @param name The name field of the {@link KDSItem}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the quantity field of the {@link KDSItem}.</p>
     *
     * @return The quantity field of the {@link KDSItem}.
     */
    public int getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link KDSItem}.</p>
     *
     * @param quantity The quantity field of the {@link KDSItem}.
     */
    public void setQuantity (int quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the kdsStation field of the {@link KDSItem}.</p>
     *
     * @return The kdsStation field of the {@link KDSItem}.
     */
    public int getKDSStation () {
        return kdsStation;
    }

    /**
     * <p>Setter for the kdsStation field of the {@link KDSItem}.</p>
     *
     * @param kdsStation The kdsStation field of the {@link KDSItem}.
     */
    public void setKDSStation (int kdsStation) {
        this.kdsStation = kdsStation;
    }

    /**
     * <p>Getter for the preModifier field of the {@link KDSItem}.</p>
     *
     * @return The preModifier field of the {@link KDSItem}.
     */
    public KDSPreModifier getPreModifier () {
        return preModifier;
    }

    /**
     * <p>Setter for the preModifier field of the {@link KDSItem}.</p>
     *
     * @param preModifier The preModifier field of the {@link KDSItem}.
     */
    public void setPreModifier (KDSPreModifier preModifier) {
        this.preModifier = preModifier;
    }

    /**
     * <p>Getter for the transLineItemID field of the {@link KDSItem}.</p>
     *
     * @return The transLineItemID field of the {@link KDSItem}.
     */
    public int getTransLineItemID () {
        return transLineItemID;
    }

    /**
     * <p>Setter for the transLineItemID field of the {@link KDSItem}.</p>
     *
     * @param transLineItemID The transLineItemID field of the {@link KDSItem}.
     */
    public void setTransLineItemID (int transLineItemID) {
        this.transLineItemID = transLineItemID;
    }

    /**
     * <p>Getter for the hiddenStations field of the {@link KDSItem}.</p>
     *
     * @return The hiddenStations field of the {@link KDSItem}.
     */
    public String getHiddenStations () {
        return hiddenStations;
    }

    /**
     * <p>Setter for the hiddenStations field of the {@link KDSItem}.</p>
     *
     * @param hiddenStations The hiddenStations field of the {@link KDSItem}.
     */
    public void setHiddenStations (String hiddenStations) {
        this.hiddenStations = hiddenStations;
    }

    /**
     * <p>Getter for the printerID field of the {@link KDSItem}.</p>
     *
     * @return The printerID field of the {@link KDSItem}.
     */
    public int getPrinterID () {
        return printerID;
    }

    /**
     * <p>Setter for the printerID field of the {@link KDSItem}.</p>
     *
     * @param printerID The printerID field of the {@link KDSItem}.
     */
    public void setPrinterID (int printerID) {
        this.printerID = printerID;
    }

    /**
     * <p>Getter for the condiments field of the {@link KDSItem}.</p>
     *
     * @return The condiments field of the {@link KDSItem}.
     */
    public ArrayList<KDSCondiment> getCondiments () {
        return condiments;
    }

    /**
     * <p>Setter for the condiments field of the {@link KDSItem}.</p>
     *
     * @param condiments The condiments field of the {@link KDSItem}.
     */
    public void setCondiments (ArrayList<KDSCondiment> condiments) {
        this.condiments = condiments;
    }

}