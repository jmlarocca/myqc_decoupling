package com.mmhayes.common.kitchenPrinters.KDS.XMLGen;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-07-20 12:20:09 -0400 (Mon, 20 Jul 2020) $: Date of last commit
    $Rev: 12195 $: Revision of last commit
    Notes: Represents a condiment within an item to be sent to KDS.
*/

import com.mmhayes.common.utils.StringFunctions;

import java.util.Objects;

public class KDSCondiment {

    // private member variables of a KDSCondiment
    private String id;
    private KDSPreModifier preModifier;
    private int transType;
    private String name;
    private int action;

    /**
     * <p>Constructor for a {@link KDSCondiment}.</p>
     *
     */
    public KDSCondiment () {}

    /**
     * <p>The {@link String} representation of a {@link KDSCondiment}.</p>
     *
     * @return The {@link KDSCondiment} instance as a {@link String}.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, PREMODIFIER: %s, TRANSTYPE: %s, NAME: %s, ACTION: %s",
                Objects.toString((StringFunctions.stringHasContent(id) ? id : "N/A"), "N/A"),
                Objects.toString((preModifier != null ? preModifier.toString() : "N/A"), "N/A"),
                Objects.toString((transType > 0 ? transType : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((action != 0 ? action : "N/A"), "N/A"));

    }

    /**
     * <p>Getter for the id field of the {@link KDSCondiment}.</p>
     *
     * @return The id field of the {@link KDSCondiment}.
     */
    public String getID () {
        return id;
    }

    /**
     * <p>Setter for the id field of the {@link KDSCondiment}.</p>
     *
     * @param id The id field of the {@link KDSCondiment}.
     */
    public void setID (String id) {
        this.id = id;
    }

    /**
     * <p>Getter for the preModifier field of the {@link KDSCondiment}.</p>
     *
     * @return The preModifier field of the {@link KDSCondiment}.
     */
    public KDSPreModifier getPreModifier () {
        return preModifier;
    }

    /**
     * <p>Setter for the preModifier field of the {@link KDSCondiment}.</p>
     *
     * @param preModifier The preModifier field of the {@link KDSCondiment}.
     */
    public void setPreModifier (KDSPreModifier preModifier) {
        this.preModifier = preModifier;
    }

    /**
     * <p>Getter for the transType field of the {@link KDSCondiment}.</p>
     *
     * @return The transType field of the {@link KDSCondiment}.
     */
    public int getTransType () {
        return transType;
    }

    /**
     * <p>Setter for the transType field of the {@link KDSCondiment}.</p>
     *
     * @param transType The transType field of the {@link KDSCondiment}.
     */
    public void setTransType (int transType) {
        this.transType = transType;
    }

    /**
     * <p>Getter for the name field of the {@link KDSCondiment}.</p>
     *
     * @return The name field of the {@link KDSCondiment}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link KDSCondiment}.</p>
     *
     * @param name The name field of the {@link KDSCondiment}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the action field of the {@link KDSCondiment}.</p>
     *
     * @return The action field of the {@link KDSCondiment}.
     */
    public int getAction () {
        return action;
    }

    /**
     * <p>Setter for the action field of the {@link KDSCondiment}.</p>
     *
     * @param action The action field of the {@link KDSCondiment}.
     */
    public void setAction (int action) {
        this.action = action;
    }

}