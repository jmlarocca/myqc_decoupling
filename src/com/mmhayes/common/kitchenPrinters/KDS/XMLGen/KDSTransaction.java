package com.mmhayes.common.kitchenPrinters.KDS.XMLGen;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-07-20 12:20:09 -0400 (Mon, 20 Jul 2020) $: Date of last commit
    $Rev: 12195 $: Revision of last commit
    Notes: Represents a transaction to be sent to KDS.
*/

import java.util.Objects;

public class KDSTransaction {

    // private member variables of a KDSTransaction
    private int paTransactionID;
    private KDSOrder order;

    /**
     * <p>Constructor for a {@link KDSTransaction}.</p>
     *
     */
    public KDSTransaction () {}

    /**
     * <p>The {@link String} representation of a {@link KDSCondiment}.</p>
     *
     * @return The {@link KDSCondiment} instance as a {@link String}.
     */
    @Override
    public String toString () {

        return String.format("PATRANSACTIONID: %s, ORDER: %s",
                Objects.toString((paTransactionID > 0 ? paTransactionID : "N/A"), "N/A"),
                Objects.toString((order != null ? order.toString() : "N/A"), "N/A"));

    }

    /**
     * <p>Getter for the paTransactionID field of the {@link KDSTransaction}.</p>
     *
     * @return The paTransactionID field of the {@link KDSTransaction}.
     */
    public int getPATransactionID () {
        return paTransactionID;
    }

    /**
     * <p>Setter for the paTransactionID field of the {@link KDSTransaction}.</p>
     *
     * @param paTransactionID The paTransactionID field of the {@link KDSTransaction}.
     */
    public void setPATransactionID (int paTransactionID) {
        this.paTransactionID = paTransactionID;
    }

    /**
     * <p>Getter for the order field of the {@link KDSTransaction}.</p>
     *
     * @return The order field of the {@link KDSTransaction}.
     */
    public KDSOrder getOrder () {
        return order;
    }

    /**
     * <p>Setter for the order field of the {@link KDSTransaction}.</p>
     *
     * @param order The order field of the {@link KDSTransaction}.
     */
    public void setOrder (KDSOrder order) {
        this.order = order;
    }

}