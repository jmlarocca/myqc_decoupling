package com.mmhayes.common.kitchenPrinters.KDS;

/*
 * $Author: jkflanagan $: Author of last commit
 * $Date: 2020-07-28 10:22:37 -0400 (Tue, 28 Jul 2020) $: Date of last commit
 * $Rev: 12261 $: Revision of last commit
 * Notes: TCP Client for sending XML to Bematech Router
 */

import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class KDSTcpClient {

    // private member variables of a KDSTcpClient
    private String log = "";
    private int paTransactionID = -1;
    private String ipAddress = "";
    private int portNumber = -1;
    private Socket socket = null;
    private final int SOCKET_TIMEOUT_IN_MS = 15000; // 15 seconds

    /**
     * <p>Constructor for a {@link KDSTcpClient}.</p>
     *
     * @param ipAddress The IP address {@link String} of the KDS router.
     * @param portNumber The port number for TCP/IP communication between the POS and KDS.
     * @param paTransactionID ID of the transaction the KDS print job is for.
     * @param log The file path {@link String} of the file that any information related to this class should be logged to.
     */
    public KDSTcpClient (String ipAddress, int portNumber, int paTransactionID, String log) {
        this.log = log;
        this.paTransactionID = paTransactionID;
        this.ipAddress = ipAddress;
        this.portNumber = portNumber;
        // try to create the socket
        if ((StringFunctions.stringHasContent(ipAddress)) && (portNumber > 0)) {
            try {
                Logger.logMessage(String.format("Opening a Socket connection to IP address: %s on port: %s.",
                        Objects.toString(ipAddress, "N/A"),
                        Objects.toString(portNumber, "N/A")), log, Logger.LEVEL.IMPORTANT);
                socket = new Socket(ipAddress, portNumber);
            }
            catch (Exception e) {
                Logger.logException(e, log);
                Logger.logMessage(String.format("Unable to create the Socket with an IP address of %s and a port number of %s in the KDSTcpClient constructor!",
                        Objects.toString(ipAddress, "N/A"),
                        Objects.toString(portNumber, "N/A")), log, Logger.LEVEL.ERROR);
            }
        }
    }

    /**
     * <p>Attempts to send the given XML file to the KDS router.</p>
     *
     * @param xmlFile The XML {@link File} to send to KDS.
     * @return The {@link KDSTcpResponse} from received from trying to send the XML {@link File} to KDS.
     */
    @SuppressWarnings({"MagicNumber", "OverlyComplexMethod"})
    public KDSTcpResponse sendXML (File xmlFile) {
        if (xmlFile == null) {
            return new KDSTcpResponse().addResponseReceived(false).addErrorMsg("The XML file passed to KDSTcpClient.sendXML can't be null!");
        }

        if (socket == null) {
            return new KDSTcpResponse().addResponseReceived(false).addErrorMsg("The socket being used for TCP/IP communication in KDSTcpClient.sendXML can't be null!");
        }

        // try to read the contents of the XML file into a String
        String xmlStr = DataFunctions.convertFileToString(xmlFile, StandardCharsets.UTF_8);
        if (!StringFunctions.stringHasContent(xmlStr)) {
            return new KDSTcpResponse().addResponseReceived(false).addErrorMsg("No contents have been read from the XML file in KDSTcpClient.sendXML!");
        }

        // create the buffer of data to send to KDS
        byte[] sendBuffer = KDSCommon.buildSendBuffer(xmlStr, log);
        if (DataFunctions.isEmptyByteArr(sendBuffer)) {
            return new KDSTcpResponse().addResponseReceived(false).addErrorMsg(String.format("No data in the buffer in " +
                    "KDSTcpClient.sendXML, nothing to send to KDS for the transaction with an ID of %s!",
                    Objects.toString(paTransactionID, "N/A")));
        }

        DataInputStream dataInputStream = null;
        DataOutputStream dataOutputStream = null;

        // try to get the data input and output streams attached to the socket
        try {
            dataInputStream = new DataInputStream(socket.getInputStream());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            // set a timeout for the socket
            socket.setSoTimeout(SOCKET_TIMEOUT_IN_MS);
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("A problem occurred while trying to get the data input and output streams attached " +
                    "to the socket with IP address: %s and listening on port: %s in KDSTcpClient.sendXML!",
                    Objects.toString(ipAddress, "N/A"),
                    Objects.toString(portNumber, "N/A")), log, Logger.LEVEL.ERROR);
        }

        // try to send the data to KDS and get back a response
        try {
            if (dataOutputStream != null) {
                dataOutputStream.write(sendBuffer);
                dataOutputStream.flush();
                Logger.logMessage(String.format("The data buffer has been successfully sent to KDS in KDSTcpClient.sendXML for the transaction with an ID of %s.",
                        Objects.toString(paTransactionID, "N/A")), log, Logger.LEVEL.TRACE);
            }
            else {
                return new KDSTcpResponse().addResponseReceived(false).addErrorMsg(String.format("The data output stream associated " +
                        "with the socket with IP address: %s listening on port: %s can't be null in KDSTcpClient.sendXML!",
                        Objects.toString(ipAddress, "N/A"),
                        Objects.toString(portNumber, "N/A")));
            }
            // sleep to give KDS a chance to respond
            Thread.sleep(100);
        }
        catch (Exception e) {
            Logger.logException(e, log);
            return new KDSTcpResponse().addResponseReceived(false).addErrorMsg("A problem occurred while trying to send the data buffer to KDS in KDSTcpClient.sendXML!");
        }

        // get the response from KDS
        String kdsResponse = "";
        byte[] receiveBuffer = new byte[sendBuffer.length * 3];
        try {
            if (dataInputStream.read(receiveBuffer, 0, receiveBuffer.length) != -1) {
                kdsResponse += new String(receiveBuffer, StandardCharsets.UTF_8).trim();
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            return new KDSTcpResponse().addResponseReceived(false).addErrorMsg(String.format("A problem occurred while trying " +
                    "to obtain a response from KDS in KDSTcpClient.sendXML, for the transaction with an ID of %s.",
                    Objects.toString(paTransactionID, "N/A")));
        }

        if (StringFunctions.stringHasContent(kdsResponse)) {
            return new KDSTcpResponse().addResponseReceived(true).addResponseStr(kdsResponse);
        }

        return new KDSTcpResponse().addResponseReceived(false).addErrorMsg("The response received from KDS in KDSTcpClient.sendXML can't be null or empty!");
    }

    /**
     * <p>Getter for the paTransactionID field of the {@link KDSTcpClient}.</p>
     *
     * @return The paTransactionID field of the {@link KDSTcpClient}.
     */
    public int getPATransactionID () {
        return paTransactionID;
    }

    /**
     * <p>Setter for the paTransactionID field of the {@link KDSTcpClient}.</p>
     *
     * @param paTransactionID The paTransactionID field of the {@link KDSTcpClient}.
     */
    public void setPATransactionID (int paTransactionID) {
        this.paTransactionID = paTransactionID;
    }

    /**
     * <p>Getter for the ipAddress field of the {@link KDSTcpClient}.</p>
     *
     * @return The ipAddress field of the {@link KDSTcpClient}.
     */
    public String getIPAddress () {
        return ipAddress;
    }

    /**
     * <p>Setter for the ipAddress field of the {@link KDSTcpClient}.</p>
     *
     * @param ipAddress The ipAddress field of the {@link KDSTcpClient}.
     */
    public void setIPAddress (String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * <p>Getter for the portNumber field of the {@link KDSTcpClient}.</p>
     *
     * @return The portNumber field of the {@link KDSTcpClient}.
     */
    public int getPortNumber () {
        return portNumber;
    }

    /**
     * <p>Setter for the portNumber field of the {@link KDSTcpClient}.</p>
     *
     * @param portNumber The portNumber field of the {@link KDSTcpClient}.
     */
    public void setPortNumber (int portNumber) {
        this.portNumber = portNumber;
    }

    /**
     * <p>Getter for the socket field of the {@link KDSTcpClient}.</p>
     *
     * @return The socket field of the {@link KDSTcpClient}.
     */
    public Socket getSocket () {
        return socket;
    }

    /**
     * <p>Setter for the socket field of the {@link KDSTcpClient}.</p>
     *
     * @param socket The socket field of the {@link KDSTcpClient}.
     */
    public void setSocket (Socket socket) {
        this.socket = socket;
    }

}
