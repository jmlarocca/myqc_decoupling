package com.mmhayes.common.kitchenPrinters.KDS;

/*
 * $Author: jkflanagan $: Author of last commit
 * $Date: 2020-07-27 10:50:49 -0400 (Mon, 27 Jul 2020) $: Date of last commit
 * $Rev: 12251 $: Revision of last commit
 * Notes: Contains information related to the response received from the Bematech router after the KDS XML has been sent via TCP.
 */

import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.util.Objects;

/**
 * <p>Contains information related to the response received from the Bematech router after the KDS XML has been sent via TCP.</p>
 *
 */
public class KDSTcpResponse {

    // private member variables of a KDSTcpResponse
    private boolean responseReceived = false;
    private String errorMsg = "";
    private String responseStr = "";
    private int errorCode = -1;

    /**
     * <p>Constructor for a {@link KDSTcpResponse}.</p>
     *
     */
    public KDSTcpResponse () {}

    /**
     * <p>Constructor for a {@link KDSTcpResponse} with it's responseReceived field set.</p>
     *
     * @param responseReceived Whether or not a response was received from KDS when the XML was sent there through TCP.
     * @return The {@link KDSTcpResponse} instance with it's responseReceived field set.
     */
    public KDSTcpResponse addResponseReceived (boolean responseReceived) {
        this.responseReceived = responseReceived;
        return this;
    }

    /**
     * <p>Constructor for a {@link KDSTcpResponse} with it's errorMsg field set.</p>
     *
     * @param errorMsg A message {@link String} related to any error that might have occurred while trying to send the XML file to KDS through TCP.
     * @return The {@link KDSTcpResponse} instance with it's errorMsg field set.
     */
    public KDSTcpResponse addErrorMsg (String errorMsg) {
        this.errorMsg = errorMsg;
        return this;
    }

    /**
     * <p>Constructor for a {@link KDSTcpResponse} with it's responseStr field set.</p>
     *
     * @param responseStr The response {@link String} that was received from KDS when the XML was sent there through TCP.
     * @return The {@link KDSTcpResponse} instance with it's responseStr field set.
     */
    public KDSTcpResponse addResponseStr (String responseStr) {
        this.responseStr = responseStr;
        this.errorCode = getErrorCodeFromResponse(responseStr);
        return this;
    }

    /**
     * <p>Finds the error code within the response received from KDS when the XML was sent there through TCP.</p>
     *
     * @param responseStr The response {@link String} received from KDS when the XML was sent there through TCP.
     * @return The error code within the response received from KDS when the XML was sent there through TCP.
     */
    private int getErrorCodeFromResponse (String responseStr) {

        if (!StringFunctions.stringHasContent(responseStr)) {
            Logger.logMessage("The response passed to KDSTcpClient.getErrorCodeFromResponse can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        String ackText = "Acknowledgement Error=";
        int indexOfAckText = responseStr.indexOf(ackText);
        if (indexOfAckText > 0) {
            // get the error code
            int indexOfErrorCode = indexOfAckText + ackText.length() + 1;
            String errorCode = responseStr.substring(indexOfErrorCode, indexOfErrorCode + 1);

            if (NumberUtils.isNumber(errorCode)) {
                return Integer.parseInt(errorCode);
            }
            else {
                Logger.logMessage("The error code found at the error code index in KDSTcpClient.getErrorCodeFromResponse is invalid!", Logger.LEVEL.ERROR);
                return -1;
            }
        }
        else {
            Logger.logMessage("Unable to find the error code within the response in KDSTcpClient.getErrorCodeFromResponse!", Logger.LEVEL.ERROR);
            return -1;
        }
    }

    /**
     * <p>The {@link String} representation of a {@link KDSTcpResponse} instance.</p>
     *
     * @return A {@link KDSTcpResponse} instance as a {@link String}.
     */
    @Override
    public String toString () {

        return String.format("RESPONSERECEIVED: %s, ERRORMSG: %s, RESPONSESTR: %s, ERRORCODE: %s",
                Objects.toString(responseReceived, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(errorMsg) ? errorMsg : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(responseStr) ? KDSCommon.fixMarkup(responseStr) : "N/A"), "N/A"),
                Objects.toString((errorCode >= 0 ? errorCode : "N/A"), "N/A"));

    }

    /**
     * <p>Getter for the responseReceived field of the {@link KDSTcpResponse}.</p>
     *
     * @return The responseReceived field of the {@link KDSTcpResponse}.
     */
    public boolean getResponseReceived () {
        return responseReceived;
    }

    /**
     * <p>Setter for the responseReceived field of the {@link KDSTcpResponse}.</p>
     *
     * @param responseReceived The responseReceived field of the {@link KDSTcpResponse}.
     */
    public void setResponseReceived (boolean responseReceived) {
        this.responseReceived = responseReceived;
    }

    /**
     * <p>Getter for the errorMsg field of the {@link KDSTcpResponse}.</p>
     *
     * @return The errorMsg field of the {@link KDSTcpResponse}.
     */
    public String getErrorMsg () {
        return errorMsg;
    }

    /**
     * <p>Setter for the errorMsg field of the {@link KDSTcpResponse}.</p>
     *
     * @param errorMsg The errorMsg field of the {@link KDSTcpResponse}.
     */
    public void setErrorMsg (String errorMsg) {
        this.errorMsg = errorMsg;
    }

    /**
     * <p>Getter for the responseStr field of the {@link KDSTcpResponse}.</p>
     *
     * @return The responseStr field of the {@link KDSTcpResponse}.
     */
    public String getResponseStr () {
        return responseStr;
    }

    /**
     * <p>Setter for the responseStr field of the {@link KDSTcpResponse}.</p>
     *
     * @param responseStr The responseStr field of the {@link KDSTcpResponse}.
     */
    public void setResponseStr (String responseStr) {
        this.responseStr = responseStr;
    }

    /**
     * <p>Getter for the errorCode field of the {@link KDSTcpResponse}.</p>
     *
     * @return The errorCode field of the {@link KDSTcpResponse}.
     */
    public int getErrorCode () {
        return errorCode;
    }

    /**
     * <p>Setter for the errorCode field of the {@link KDSTcpResponse}.</p>
     *
     * @param errorCode The errorCode field of the {@link KDSTcpResponse}.
     */
    public void setErrorCode (int errorCode) {
        this.errorCode = errorCode;
    }

}