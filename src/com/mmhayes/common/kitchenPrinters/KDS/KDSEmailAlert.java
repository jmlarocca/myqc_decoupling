package com.mmhayes.common.kitchenPrinters.KDS;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-10-15 10:14:49 -0400 (Thu, 15 Oct 2020) $: Date of last commit
    $Rev: 12855 $: Revision of last commit
    Notes: Contains methods for creating an email for KDS related problems.
*/

        import com.mmhayes.common.dataaccess.DataManager;
        import com.mmhayes.common.dataaccess.DynamicSQL;
        import com.mmhayes.common.dataaccess.commonMMHFunctions;
        import com.mmhayes.common.receiptGen.HashMapDataFns;
        import com.mmhayes.common.utils.DataFunctions;
        import com.mmhayes.common.utils.Logger;
        import com.mmhayes.common.utils.MMHTimeFormatString;
        import com.mmhayes.common.utils.StringFunctions;
        import org.apache.commons.lang.math.NumberUtils;

        import java.time.LocalDateTime;
        import java.time.format.DateTimeFormatter;
        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.Objects;

public class KDSEmailAlert {

    /**
     * <p>Queries the database to determine the revenue center the terminal is within.</p>
     *
     * @param trmMacAddress The MAC address {@link String} of the terminal to check.
     * @param trmHostname The hostname {@link String} of the terminal to check.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return The ID of the revenue center the terminal with the given MAC address and hostname is located within.
     */
    public static int getTerminalsRevenueCenter (String trmMacAddress, String trmHostname, DataManager dataManager, String log) {

        // validate the log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KDSEmailAlert.getTerminalsRevenueCenter, unable to get the revenue center ID for the terminal!", Logger.LEVEL.ERROR);
            return -1;
        }

        // validate the DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KDSEmailAlert.getTerminalsRevenueCenter can't be null, unable to get the revenue center ID for the terminal!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // validate the terminal's MAC address
        if (!StringFunctions.stringHasContent(trmMacAddress)) {
            Logger.logMessage("The terminal MAC address passed to KDSEmailAlert.getTerminalsRevenueCenter can't be null or empty, unable to get the revenue center ID for the terminal!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // validate the terminal's hostname
        if (!StringFunctions.stringHasContent(trmHostname)) {
            Logger.logMessage("The terminal hostname passed to KDSEmailAlert.getTerminalsRevenueCenter can't be null or empty, unable to get the revenue center ID for the terminal!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetTerminalsRC").addIDList(1, trmMacAddress).addIDList(2, trmHostname).setLogFileName(log);
        Object queryRes = sql.getSingleField(dataManager);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString()))) {
            return Integer.parseInt(queryRes.toString());
        }

        return -1;
    }

    /**
     * <p>Gets the email addresses within the revenue center that should get emails.</p>
     *
     * @param revenueCenterID The ID of the revenue center in which the transaction took place.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return An {@link ArrayList} of {@link String} corresponding to email addresses within the revenue center.
     */
    @SuppressWarnings("ManualArrayToCollectionCopy")
    public static ArrayList<String> getEmailListForRC (int revenueCenterID, DataManager dataManager, String log) {

        // validate the log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KDSEmailAlert.getEmailListForRC, unable to get the email list for the revenue center!", Logger.LEVEL.ERROR);
            return null;
        }

        // validate the DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KDSEmailAlert.getEmailListForRC can't be null, unable to get the email list for the revenue center!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // validate revenue center ID
        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KDSEmailAlert.getEmailListForRC must be greater than 0, unable to get the email list for the revenue center!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        ArrayList<String> emails = new ArrayList<>();
        DynamicSQL sql = new DynamicSQL("data.newKitchenPrinter.GetKPContactEmailByRevenueCenterID").addIDList(1, revenueCenterID);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1)) {
            String emailAddressesStr = HashMapDataFns.getStringVal(queryRes.get(0), "KPCONTACTEMAIL");
            if (StringFunctions.stringHasContent(emailAddressesStr)) {
                String[] emailAddressesArr = emailAddressesStr.split("\\s*,\\s*", -1);
                if (!DataFunctions.isEmptyGenericArr(emailAddressesArr)) {
                    for (String email : emailAddressesArr) {
                        emails.add(email);
                    }
                }
            }
        }

        return emails;
    }

    /**
     * <p>Queries the database to get the back office email configuration.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return A {@link HashMap} containing the back office email configuration information.
     */
    public static HashMap getEmailConfig (DataManager dataManager, String log) {

        // validate the log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KDSEmailAlert.getEmailConfig, unable to get the back office email configuration!", Logger.LEVEL.ERROR);
            return null;
        }

        // validate the DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KDSEmailAlert.getEmailConfig can't be null, unable to get the back office email configuration!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.ReceiptGen.EmailCfg").setLogFileName(log);

        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1)) {
            return queryRes.get(0);
        }

        return null;
    }

    /**
     * <p>Builds an email subject line for when KDS is unable to retrieve orders from the file share within a certain amount of time.</p>
     *
     * @param printerHostName The hostname {@link String} of the terminal which houses the primary printer host where the file share for KDS is located.
     * @param companyName The name {@link String} of the company where the error is occurring.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return An email subject line {@link String} for when KDS is unable to retrieve orders from the file share within a certain amount of time.
     */
    public static String buildKDSFileShareErrorSubjectLine (String printerHostName, String companyName, String log) {

        // validate the log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KDSEmailAlert.buildKDSFileShareErrorSubjectLine, unable to build the email subject line!", Logger.LEVEL.ERROR);
            return null;
        }

        // validate the printer host name
        if (!StringFunctions.stringHasContent(printerHostName)) {
            Logger.logMessage("The printer host name passed to KDSEmailAlert.buildKDSFileShareErrorSubjectLine can't be null or empty, unable to build the email subject line!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // validate the company name
        if (!StringFunctions.stringHasContent(companyName)) {
            Logger.logMessage("The company name passed to KDSEmailAlert.buildKDSFileShareErrorSubjectLine can't be null or empty, unable to build the email subject line!", log, Logger.LEVEL.ERROR);
            return null;
        }

        return String.format("KDS failed to retrieve orders from the printer host %s at %s!",
                Objects.toString(printerHostName, "N/A"),
                Objects.toString(companyName, "N/A"));
    }

    /**
     * <p>Builds the content of an email in which there are KDS order files which remained in the file share too long and haven't been picked up by KDS yet.</p>
     *
     * @param subject The email subject {@link String} line.
     * @param expiredKDSXMLFilePaths An {@link ArrayList} of {@link HashMap} with a {@link String} key and value corresponding to file paths of KDS XML order files which have been in the file share too long.
     * @return The html content {@link String} of an email in which there are KDS order files which remained in the file share too long and haven't been picked up by KDS yet.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public static String buildKDSFileShareErrorEmailBody (String subject, ArrayList<HashMap<String, String>> expiredKDSXMLFilePaths) {

        if (!StringFunctions.stringHasContent(subject)) {
            Logger.logMessage("The email subject line passed to KDSEmailAlert.buildKDSFileShareErrorEmailBody is null or empty!", KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            subject = "[EMAIL SUBJECT LINE INDETERMINABLE]";
        }

        // make sure there are files that failed to be sent to KDS in a timely manner
        if (DataFunctions.isEmptyCollection(expiredKDSXMLFilePaths)) {
            Logger.logMessage("The ArrayList of KDS file paths that failed to be sent to KDS in a timely manner and passed " +
                    "to KDSEmailAlert.buildKDSFileShareErrorEmailBody, unable to build the content for the email body, now returning null!", KDSShareFolderMonitor.KDS_SHARE_MONITOR_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // sort the expired KDS XML order files by descending creation date
        ArrayList<HashMap<String, String>> sortedExpiredKDSXMLFilePaths = new ArrayList<>();
        for (HashMap<String, String> expiredKDSXMLFilePath : expiredKDSXMLFilePaths) {
            if (DataFunctions.isEmptyCollection(sortedExpiredKDSXMLFilePaths)) {
                sortedExpiredKDSXMLFilePaths.add(expiredKDSXMLFilePath);
            }
            else {
                int insertionIndex = 0;
                for (HashMap<String, String> sortedEpiredKDSXMLFilePath : sortedExpiredKDSXMLFilePaths) {
                    LocalDateTime sortedTime = LocalDateTime.parse(HashMapDataFns.getStringVal(sortedEpiredKDSXMLFilePath, "CREATEDTIME"), DateTimeFormatter.ofPattern(MMHTimeFormatString.YR_MO_DY_HR_MIN_SEC));
                    LocalDateTime unsortedTime = LocalDateTime.parse(HashMapDataFns.getStringVal(expiredKDSXMLFilePath, "CREATEDTIME"), DateTimeFormatter.ofPattern(MMHTimeFormatString.YR_MO_DY_HR_MIN_SEC));
                    if (unsortedTime.isBefore(sortedTime)) {
                        insertionIndex++;
                    }
                }
                sortedExpiredKDSXMLFilePaths.add(insertionIndex, expiredKDSXMLFilePath);
            }
        }

        StringBuilder stringBuilder = new StringBuilder();

        // store the email's main content in a table
        stringBuilder.append("<style>");
        stringBuilder.append("table, th, td {");
        stringBuilder.append("border: 2px solid #000000;");
        stringBuilder.append("border-collapse: collapse;");
        stringBuilder.append("}");
        stringBuilder.append("th {");
        stringBuilder.append("text-align: left;");
        stringBuilder.append("}");
        stringBuilder.append("</style>");
        stringBuilder.append("<h3>");
        stringBuilder.append(subject);
        stringBuilder.append("</h3>");
        stringBuilder.append("<table style=\"width:100%;font-family:sans-serif;\">");
        stringBuilder.append("<tr>");
        stringBuilder.append("<th>");
        stringBuilder.append("File");
        stringBuilder.append("</th>");
        stringBuilder.append("<th>");
        stringBuilder.append("File Creation Time");
        stringBuilder.append("</th>");
        stringBuilder.append("<th>");
        stringBuilder.append("Current Time");
        stringBuilder.append("</th>");
        stringBuilder.append("</tr>");
        for (HashMap<String, String> expiredKDSXMLFileHM : sortedExpiredKDSXMLFilePaths) {
            stringBuilder.append("<tr>");
            stringBuilder.append("<td>");
            stringBuilder.append(HashMapDataFns.getStringVal(expiredKDSXMLFileHM, "PATH"));
            stringBuilder.append("</td>");
            stringBuilder.append("<td>");
            stringBuilder.append(HashMapDataFns.getStringVal(expiredKDSXMLFileHM, "CREATEDTIME"));
            stringBuilder.append("</td>");
            stringBuilder.append("<td>");
            stringBuilder.append(HashMapDataFns.getStringVal(expiredKDSXMLFileHM, "NOWTIME"));
            stringBuilder.append("</td>");
            stringBuilder.append("</tr>");
        }
        stringBuilder.append("</table>");

        // get the path of the MM Hayes logo image
        commonMMHFunctions commonMMHFunc = new commonMMHFunctions();
        String qcBaseURL = commonMMHFunc.getQCBaseURL();
        String instanceName = commonMMHFunctions.getInstanceName();
        String mmhLogoImgPath = "";
        if ((StringFunctions.stringHasContent(qcBaseURL)) && (StringFunctions.stringHasContent(instanceName))) {
            mmhLogoImgPath = qcBaseURL + "/" + instanceName + "/assets/MMHLogo.png";
        }

        // add the M.M. Hayes footer
        stringBuilder.append("<p style=\"color: black;\"><strong>M.M. Hayes Co., Inc</strong></p>");
        stringBuilder.append("<p style=\"color: black;\">16 Sage Estate | Albany, NY 12204</p>");
        stringBuilder.append("<p style=\"color: black;\">Office: (518) 459-5545</p><br>");
        stringBuilder.append("<img alt=\"MM Hayes Co., Inc.\" src=\"");
        stringBuilder.append(mmhLogoImgPath);
        stringBuilder.append("\" border=\"0\" style=\"outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;border:none;display:block;\"/>");
        stringBuilder.append("<p style=\"color: #EF4135;\"><strong>Join MM Hayes on:</strong>");
        stringBuilder.append("<a href=\"http://www.mmhayes.com/\" target=\"_blank\" style=\"color: blue;\"><strong>mmhayes.com</strong></a>");
        stringBuilder.append("<strong>|</strong>");
        stringBuilder.append("<a href=\"https://twitter.com/mmhayesco\" target=\"_blank\" style=\"color: blue;\"><strong>Twitter</strong></a>");
        stringBuilder.append("<strong>|</strong>");
        stringBuilder.append("<a href=\"http://www.linkedin.com/company/mm-hayes-company-inc-\" target=\"_blank\" style=\"color: blue;\"><strong>LinkedIn</strong></a>");
        stringBuilder.append("<strong>|</strong>");
        stringBuilder.append("<a href=\"http://www.youtube.com/themmhayescompany\" target=\"_blank\" style=\"color: blue;\"><strong>YouTube</strong></a>");
        stringBuilder.append("</p>");
        stringBuilder.append("<p>To assist MM Hayes in serving you better, please place all service requests through the Help Desk. ");
        stringBuilder.append("The Help Desk can be reached by calling (518) 459-5545 or via email at ");
        stringBuilder.append("<a href=\"mailto:helpdesk@mmhayes.com\" target=\"_blank\" title=\"mailto:helpdesk@mmhayes.com\" style=\"color: blue;\">helpdesk@mmhayes.com</a>.</p>");

        return stringBuilder.toString();
    }

}