package com.mmhayes.common.kitchenPrinters.KDS;

/*
    $Author: jmkimber $: Author of last commit
    $Date: 2021-04-26 10:59:25 -0400 (Mon, 26 Apr 2021) $: Date of last commit
    $Rev: 13847 $: Revision of last commit
    Notes: Utils for KDS
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.kitchenPrinters.*;
import com.mmhayes.common.kitchenPrinters.KDS.XMLGen.KDSXMLBuilder;
import com.mmhayes.common.kitchenPrinters.KDS.XMLGen.KDSTransaction;
import com.mmhayes.common.printing.PrintStatus;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Objects;

/**
 * Contains methods for converting a QCPrintJob into a KDSTransaction that will be displayed on the KDS system.
 *
 */
public class KDSUtil implements Serializable {

    // the only instance of a KDSUitl that will exist
    private static volatile KDSUtil kdsUtil = null;

    // log file specific to the KDSUtil
    private static final String KDS_UTIL_LOG = "KDS_Util.log";

    /**
     * <p>Private constructor for a {@link KDSUtil}.</p>
     *
     * @throws Exception
     */
    private KDSUtil () throws Exception {
        // prevent creating a KDSUtil through reflection
        if (kdsUtil != null) {
            throw new Exception("A KDSUtil instance already exists! Please use getInstance() to obtain the instance.");
        }
    }

    /**
     * <p>Returns the {@link KDSUtil} instance.</p>
     *
     * @return The {@link KDSUtil} instance.
     */
    public static KDSUtil getInstance () throws Exception {
        // make sure there is an instance to return
        if (kdsUtil == null) {
            throw new Exception("No KDSUtil instance exists! You can create one by using the init() method.");
        }

        return kdsUtil;
    }

    /**
     * <p>Instantiates and returns the only instance of a {@link KDSUtil}.</p>
     *
     * @return The {@link KDSUtil} instance.
     */
    public static synchronized KDSUtil init () throws Exception {
        // don't create a new KDSUtil if one already exists
        if (kdsUtil != null) {
            throw new Exception("A KDSUtil instance already exists! Please use getInstance() to obtain the instance.");
        }
        kdsUtil = new KDSUtil();

        return kdsUtil;
    }

    /**
     * <p>Implementation of readResolve() to prevent creating a {@link KDSUtil} through serialization.</p>
     *
     * @return The only instance of a {@link KDSUtil}.
     * @throws Exception
     */
    protected KDSUtil readResolve () throws Exception {
        return getInstance();
    }

    /**
     * Converts a QCPrintJob to a KDSTransaction and sends the transaction to the KDS system to be displayed.
     *
     */
    public void createOrderFile (QCPrintJob qcPrintJob) {

        try {
            KDSTransaction kdsTransaction = KDSXMLBuilder.convertQCPrintJobToKDSTransaction(qcPrintJob, new DataManager());
            sendKDSTxnToKDS(kdsTransaction, qcPrintJob);
        }
        catch (Exception e) {
            Logger.logMessage("There was a problem trying to convert the QCPrintJob with a tracker ID of " +
                    Objects.toString(qcPrintJob.getJobTrackerID(), "NULL")+" to a KDSTransaction that can be " +
                    "displayed by the KDS system in KDSUtil.createOrderFile", KDS_UTIL_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Determines how the print job should be sent to KDS (remote folder or TCP/IP) and sends it that way.</p>
     *
     * @param kdsTransaction The {@link KDSTransaction} to be sent to KDS.
     * @param qcPrintJob The {@link QCPrintJob} used to build the {@link KDSTransaction}.
     */
    @SuppressWarnings("OverlyComplexMethod")
    public void sendKDSTxnToKDS (KDSTransaction kdsTransaction, QCPrintJob qcPrintJob) {

        try {
            if (kdsTransaction == null) {
                Logger.logMessage("The KDSTransaction passed to KDSUtil.sendKDSTxnToKDS can't be null!", KDS_UTIL_LOG, Logger.LEVEL.ERROR);
                return;
            }

            if (qcPrintJob == null) {
                Logger.logMessage("The QCPrintJob passed to KDSUtil.sendKDSTxnToKDS can't be null!", KDS_UTIL_LOG, Logger.LEVEL.ERROR);
                return;
            }

            // marshal the KDS transaction into a XML file
            File kdsTxnFile = marshalKDSTxn(kdsTransaction, qcPrintJob);
            if (kdsTxnFile == null) {
                Logger.logMessage(String.format("Unable to marshal the KDS transaction with a print job tracker ID of %s into a XML file in KDSUtil.sendKDSTxnToKDS",
                        Objects.toString(qcPrintJob.getJobTrackerID(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.ERROR);
                return;
            }

            // store how the KDS transaction should be sent to KDS
            String kdsDataSourceID = "1"; // default to remote folder
            // get router information in the database
            HashMap routerInfo = new commonMMHFunctions().getKDSRouterInfo(qcPrintJob.getTerminalID(), KDS_UTIL_LOG);
            if (!DataFunctions.isEmptyMap(routerInfo)) {
                kdsDataSourceID = HashMapDataFns.getStringVal(routerInfo, "KDSDATASOURCEID");
            }

            if (kdsDataSourceID.equalsIgnoreCase("1")) {
                if (!sendThroughRemoteFolder(qcPrintJob, kdsTxnFile)) {
                    // add the print job back into the print queue
                    PrinterQueueHandler.getInstance().offerToPrintQueue(qcPrintJob);
                }
            }
            else if (kdsDataSourceID.equalsIgnoreCase("2")) {
                if (!sendThroughTCPIP(qcPrintJob, kdsTxnFile, routerInfo)) {
                    // add the print job back into the print queue
                    PrinterQueueHandler.getInstance().offerToPrintQueue(qcPrintJob);
                }
            }
            else {
                Logger.logMessage(String.format("Encountered an unknown KDS data source type of %s in KDSUtil.sendKDSTxnToKDS",
                        Objects.toString(kdsDataSourceID, "N/A")), KDS_UTIL_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, KDS_UTIL_LOG);
            String trackingID = "";
            if (qcPrintJob != null) {
                trackingID = qcPrintJob.getJobTrackerID();
            }
            Logger.logMessage(String.format("There was a problem trying to send the print job with a print job tracker ID of %s to KDS in KDSUtil.sendKDSTxnToKDS",
                    Objects.toString(trackingID, "N/A")));
        }

    }

    /**
     * <p>Converts the {@link KDSTransaction} into XML and inserts it into a file which will be used by the KDS router.</p>
     *
     * @param kdsTransaction The {@link KDSTransaction} to be sent to KDS.
     * @param qcPrintJob The {@link QCPrintJob} used to build the {@link KDSTransaction}.
     * @return The {@link File} the given {@link KDSTransaction} was marshalled into.
     */
    private File marshalKDSTxn (KDSTransaction kdsTransaction, QCPrintJob qcPrintJob) {
        File kdsTxnFile = null;

        try {
            if (kdsTransaction == null) {
                Logger.logMessage("The KDSTransaction passed to KDSUtil.marshalKDSTxn can't be null!", KDS_UTIL_LOG, Logger.LEVEL.ERROR);
                return null;
            }

            if (qcPrintJob == null) {
                Logger.logMessage("The QCPrintJob passed to KDSUtil.marshalKDSTxn can't be null!", KDS_UTIL_LOG, Logger.LEVEL.ERROR);
                return null;
            }

            Logger.logMessage(String.format("KDS directory name: %s", Objects.toString(qcPrintJob.getKdsDirectory(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.DEBUG);

            File directoryLog = commonMMHFunctions.getDirectory("kds", "order_xml_logs");
            String baseFilename = "KDS-"+Objects.toString(qcPrintJob.getTerminalID(), "N/A")+"-"+Objects.toString(qcPrintJob.getPaTransactionID(), "N/A");
            File backupFile = new File(directoryLog, baseFilename+".xml"+".log");
            Logger.logMessage("Outputting KDS log file to: "+Objects.toString(backupFile.getAbsolutePath(), "N/A"), KDS_UTIL_LOG, Logger.LEVEL.DEBUG);

            backupFile = KDSXMLBuilder.getKDSXMLFile(kdsTransaction, backupFile);

            kdsTxnFile = backupFile;
        }
        catch (Exception e) {
            Logger.logException(e, KDS_UTIL_LOG);
            String trackingID = "";
            if (qcPrintJob != null) {
                trackingID = qcPrintJob.getJobTrackerID();
            }
            Logger.logMessage(String.format("There was a problem trying to marshal the print job with a print job tracker ID of %s in KDSUtil.marshalKDSTxn",
                    Objects.toString(trackingID, "N/A")), KDS_UTIL_LOG, Logger.LEVEL.ERROR);
        }

        return kdsTxnFile;
    }

    /**
     * <p>Sends the print job to KDS through a remote folder.</p>
     *
     * @param qcPrintJob The {@link QCPrintJob} to send to KDS.
     * @param kdsTxnFile The {@link File} that contains the {@link QCPrintJob} for KDS.
     * @return Whether or not the XML file made it to KDS.
     */
    private boolean sendThroughRemoteFolder (QCPrintJob qcPrintJob, File kdsTxnFile) {
        boolean sentToKDS = false;

        try {
            Logger.logMessage("KDS data source: remote folder", KDS_UTIL_LOG, Logger.LEVEL.TRACE);

            if (qcPrintJob == null) {
                Logger.logMessage("The QCPrintJob passed to KDSUtil.sendThroughRemoteFolder can't be null!", KDS_UTIL_LOG, Logger.LEVEL.ERROR);
                return false;
            }

            if (kdsTxnFile == null) {
                Logger.logMessage("The KDS transaction XML file passed tp KDSUtil.sendThroughRemoteFolder can't be null!", KDS_UTIL_LOG, Logger.LEVEL.ERROR);
                return false;
            }

            if (StringFunctions.stringHasContent(qcPrintJob.getKdsDirectory())) {
                File kdsDirectory = commonMMHFunctions.getDirectory("kds", qcPrintJob.getKdsDirectory());
                String baseFilename = "KDS-"+Objects.toString(qcPrintJob.getTerminalID(), "N/A")+"-"+Objects.toString(qcPrintJob.getPaTransactionID(), "N/A");
                File mainFile = new File(kdsDirectory, baseFilename+".xml");
                Logger.logMessage("Outputting KDS main file to: "+Objects.toString(mainFile.getAbsolutePath(), "N/A"), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                commonMMHFunctions.copyFile(kdsTxnFile, mainFile);
                // indicate that the print job has made it to KDS
                PrintStatus printStatus = new PrintStatus()
                        .addPrintStatusType(PrintStatusType.DONE)
                        .addUpdatedDTM(LocalDateTime.now());
                PrintJobStatusHandler.getInstance().updatePrintStatusMapForPrintJob(printStatus, qcPrintJob);
                sentToKDS = true;
            } else {
                Logger.logMessage("KDSUtil.sendThroughRemoteFolder: KDS directory is empty for print job PATransactionID " + qcPrintJob.getPaTransactionID(), KDS_UTIL_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, KDS_UTIL_LOG);
            String trackingID = "";
            if (qcPrintJob != null) {
                trackingID = qcPrintJob.getJobTrackerID();
            }
            Logger.logMessage(String.format("There was a problem trying to send the print job with a print job tracker ID of %s through a remote folder in KDSUtil.sendThroughRemoteFolder",
                    Objects.toString(trackingID, "N/A")), KDS_UTIL_LOG, Logger.LEVEL.ERROR);
        }

        return sentToKDS;
    }

    /**
     * <p>Sends the print job to KDS through TCP/IP.</p>
     *
     * @param qcPrintJob The {@link QCPrintJob} to send to KDS.
     * @param kdsTxnFile The {@link File} that contains the {@link QCPrintJob} for KDS.
     * @param routerInfo A {@link HashMap} containing information about the router for TCP/IP.
     * @return Whether or not the XML file made it to KDS.
     */
    @SuppressWarnings({"OverlyComplexMethod", "ConstantConditions"})
    private boolean sendThroughTCPIP (QCPrintJob qcPrintJob, File kdsTxnFile, HashMap routerInfo) {
        boolean sentToKDS = false;

        try {
            Logger.logMessage("KDS data source: TCP/IP", KDS_UTIL_LOG, Logger.LEVEL.TRACE);

            if (qcPrintJob == null) {
                Logger.logMessage("The QCPrintJob passed to KDSUtil.sendThroughRemoteFolder can't be null!", KDS_UTIL_LOG, Logger.LEVEL.ERROR);
                return false;
            }

            if (kdsTxnFile == null) {
                Logger.logMessage("The KDS transaction XML file passed to KDSUtil.sendThroughRemoteFolder can't be null!", KDS_UTIL_LOG, Logger.LEVEL.ERROR);
                return false;
            }

            if (DataFunctions.isEmptyMap(routerInfo)) {
                Logger.logMessage("The HashMap containing KDS router information passed KDSUtil.sendThroughRemoteFolder can't be null or empty!", KDS_UTIL_LOG, Logger.LEVEL.ERROR);
                return false;
            }

            int paTransactionID = qcPrintJob.getPaTransactionID();

            // get KDS router information
            String primaryRouterAddress = HashMapDataFns.getStringVal(routerInfo, "KDSROUTERADDRESSPRIMARY");
            int primaryRouterPort = HashMapDataFns.getIntVal(routerInfo, "KDSROUTERPORTNUMBERPRIMARY");
            String backupRouterAddress = HashMapDataFns.getStringVal(routerInfo, "KDSROUTERADDRESSBACKUP");
            int backupRouterPort = HashMapDataFns.getIntVal(routerInfo, "KDSROUTERPORTNUMBERBACKUP");

            // try to send the XML to the primary router
            KDSTcpClient primaryRouterClient = new KDSTcpClient(primaryRouterAddress, primaryRouterPort, paTransactionID, KDS_UTIL_LOG);
            KDSTcpResponse primaryRouterResponse = primaryRouterClient.sendXML(kdsTxnFile);
            Logger.logMessage(String.format("Primary router response - %s", Objects.toString(primaryRouterResponse.toString(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
            boolean xmlSentToPrimaryRouter;
            if (primaryRouterResponse.getResponseReceived()) {
                xmlSentToPrimaryRouter = true;
                if (primaryRouterResponse.getErrorCode() == TypeData.KDS.TcpResponseCode.NO_ERR) {
                    Logger.logMessage(String.format("The transaction with an ID of %s has successfully been sent to the primary " +
                            "KDS router with an IP address of %s listening on port %s without any errors in KDSUtil.sendThroughTCPIP!",
                            Objects.toString(paTransactionID, "N/A"),
                            Objects.toString(primaryRouterAddress, "N/A"),
                            Objects.toString(primaryRouterPort, "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                    // indicate that the print job has made it to KDS
                    PrintStatus printStatus = new PrintStatus()
                            .addPrintStatusType(PrintStatusType.DONE)
                            .addUpdatedDTM(LocalDateTime.now());
                    PrintJobStatusHandler.getInstance().updatePrintStatusMapForPrintJob(printStatus, qcPrintJob);
                    sentToKDS = true;
                }
                else {
                    switch (primaryRouterResponse.getErrorCode()) {
                        case TypeData.KDS.TcpResponseCode.BAD_XML_ERR:
                            Logger.logMessage(String.format("The transaction with an ID of %s has been sent to the primary KDS router " +
                                    "with an IP address of %s listening on port %s but received a the error code of %s which indicates " +
                                    "that the XML sent to KDS was malformed in the response in KDSUtil.sendThroughTCPIP!",
                                    Objects.toString(paTransactionID, "N/A"),
                                    Objects.toString(primaryRouterAddress, "N/A"),
                                    Objects.toString(primaryRouterPort, "N/A"),
                                    Objects.toString(primaryRouterResponse.getErrorCode(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                            break;
                        case TypeData.KDS.TcpResponseCode.NO_ACK_IN_10_SEC_ERR:
                            Logger.logMessage(String.format("The transaction with an ID of %s has been sent to the primary KDS router " +
                                    "with an IP address of %s listening on port %s but received a the error code of %s which indicates " +
                                    "that no response was received from KDS within 10 seconds in the response in KDSUtil.sendThroughTCPIP!",
                                    Objects.toString(paTransactionID, "N/A"),
                                    Objects.toString(primaryRouterAddress, "N/A"),
                                    Objects.toString(primaryRouterPort, "N/A"),
                                    Objects.toString(primaryRouterResponse.getErrorCode(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                            break;
                        case TypeData.KDS.TcpResponseCode.XML_PARAM_ERR:
                            Logger.logMessage(String.format("The transaction with an ID of %s has been sent to the primary KDS router " +
                                    "with an IP address of %s listening on port %s but received a the error code of %s which indicates " +
                                    "an error within the parameters in the XML in the response in KDSUtil.sendThroughTCPIP!",
                                    Objects.toString(paTransactionID, "N/A"),
                                    Objects.toString(primaryRouterAddress, "N/A"),
                                    Objects.toString(primaryRouterPort, "N/A"),
                                    Objects.toString(primaryRouterResponse.getErrorCode(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                            break;
                        case TypeData.KDS.TcpResponseCode.XML_TRAN_TYPE_ERR:
                            Logger.logMessage(String.format("The transaction with an ID of %s has been sent to the primary KDS router " +
                                    "with an IP address of %s listening on port %s but received a the error code of %s which indicates " +
                                    "an error in the TransType within the XML in the response in KDSUtil.sendThroughTCPIP!",
                                    Objects.toString(paTransactionID, "N/A"),
                                    Objects.toString(primaryRouterAddress, "N/A"),
                                    Objects.toString(primaryRouterPort, "N/A"),
                                    Objects.toString(primaryRouterResponse.getErrorCode(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                            break;
                        default:
                            Logger.logMessage(String.format("The transaction with an ID of %s has been sent to the primary KDS router " +
                                    "with an IP address of %s listening on port %s but received an unknown error code of %s in " +
                                    "the response in KDSUtil.sendThroughTCPIP!",
                                    Objects.toString(paTransactionID, "N/A"),
                                    Objects.toString(primaryRouterAddress, "N/A"),
                                    Objects.toString(primaryRouterPort, "N/A"),
                                    Objects.toString(primaryRouterResponse.getErrorCode(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                            break;
                    }
                    // indicate that the print job has made it to KDS with an error
                    PrintStatus printStatus = new PrintStatus()
                            .addPrintStatusType(PrintStatusType.ERROR)
                            .addUpdatedDTM(LocalDateTime.now());
                    PrintJobStatusHandler.getInstance().updatePrintStatusMapForPrintJob(printStatus, qcPrintJob);
                    sentToKDS = true;
                }
            }
            else {
                // get the reason why the XML was unable to be sent to the primary router
                Logger.logMessage(String.format("The XML for the transaction with an ID of %s wasn't sent to the primary " +
                        "router with an IP address of %s listening on port %s for the following reason, \"%s\" in KDSUtil.sendThroughTCPIP!",
                        Objects.toString(paTransactionID, "N/A"),
                        Objects.toString(primaryRouterAddress, "N/A"),
                        Objects.toString(primaryRouterPort, "N/A"),
                        Objects.toString(primaryRouterResponse.getErrorMsg(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                xmlSentToPrimaryRouter = false;
            }

            boolean isBackupRouterConfigured = ((StringFunctions.stringHasContent(backupRouterAddress)) && (backupRouterPort > 0));
            if ((!xmlSentToPrimaryRouter) && (isBackupRouterConfigured)) {
                // try to send the XML to the backup router
                KDSTcpClient backupRouterClient = new KDSTcpClient(backupRouterAddress, backupRouterPort, paTransactionID, KDS_UTIL_LOG);
                KDSTcpResponse backupRouterResponse = backupRouterClient.sendXML(kdsTxnFile);
                if (backupRouterResponse.getResponseReceived()) {
                    if (backupRouterResponse.getErrorCode() == TypeData.KDS.TcpResponseCode.NO_ERR) {
                        Logger.logMessage(String.format("The transaction with an ID of %s has successfully been sent to the backup " +
                                "KDS router with an IP address of %s listening on port %s without any errors in KDSUtil.sendThroughTCPIP!",
                                Objects.toString(paTransactionID, "N/A"),
                                Objects.toString(backupRouterAddress, "N/A"),
                                Objects.toString(backupRouterPort, "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                        // indicate that the print job has made it to KDS
                        PrintStatus printStatus = new PrintStatus()
                                .addPrintStatusType(PrintStatusType.DONE)
                                .addUpdatedDTM(LocalDateTime.now());
                        PrintJobStatusHandler.getInstance().updatePrintStatusMapForPrintJob(printStatus, qcPrintJob);
                        sentToKDS = true;
                    }
                    else {
                        switch (backupRouterResponse.getErrorCode()) {
                            case TypeData.KDS.TcpResponseCode.BAD_XML_ERR:
                                Logger.logMessage(String.format("The transaction with an ID of %s has been sent to the backup KDS router " +
                                        "with an IP address of %s listening on port %s but received a the error code of %s which indicates " +
                                        "that the XML sent to KDS was malformed in the response in KDSUtil.sendThroughTCPIP!",
                                        Objects.toString(paTransactionID, "N/A"),
                                        Objects.toString(backupRouterAddress, "N/A"),
                                        Objects.toString(backupRouterPort, "N/A"),
                                        Objects.toString(backupRouterResponse.getErrorCode(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                                break;
                            case TypeData.KDS.TcpResponseCode.NO_ACK_IN_10_SEC_ERR:
                                Logger.logMessage(String.format("The transaction with an ID of %s has been sent to the backup KDS router " +
                                        "with an IP address of %s listening on port %s but received a the error code of %s which indicates " +
                                        "that no response was received from KDS within 10 seconds in the response in KDSUtil.sendThroughTCPIP!",
                                        Objects.toString(paTransactionID, "N/A"),
                                        Objects.toString(backupRouterAddress, "N/A"),
                                        Objects.toString(backupRouterPort, "N/A"),
                                        Objects.toString(backupRouterResponse.getErrorCode(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                                break;
                            case TypeData.KDS.TcpResponseCode.XML_PARAM_ERR:
                                Logger.logMessage(String.format("The transaction with an ID of %s has been sent to the backup KDS router " +
                                        "with an IP address of %s listening on port %s but received a the error code of %s which indicates " +
                                        "an error within the parameters in the XML in the response in KDSUtil.sendThroughTCPIP!",
                                        Objects.toString(paTransactionID, "N/A"),
                                        Objects.toString(backupRouterAddress, "N/A"),
                                        Objects.toString(backupRouterPort, "N/A"),
                                        Objects.toString(backupRouterResponse.getErrorCode(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                                break;
                            case TypeData.KDS.TcpResponseCode.XML_TRAN_TYPE_ERR:
                                Logger.logMessage(String.format("The transaction with an ID of %s has been sent to the backup KDS router " +
                                        "with an IP address of %s listening on port %s but received a the error code of %s which indicates " +
                                        "an error in the TransType within the XML in the response in KDSUtil.sendThroughTCPIP!",
                                        Objects.toString(paTransactionID, "N/A"),
                                        Objects.toString(backupRouterAddress, "N/A"),
                                        Objects.toString(backupRouterPort, "N/A"),
                                        Objects.toString(backupRouterResponse.getErrorCode(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                                break;
                            default:
                                Logger.logMessage(String.format("The transaction with an ID of %s has been sent to the backup KDS router " +
                                        "with an IP address of %s listening on port %s but received an unknown error code of %s in " +
                                        "the response in KDSUtil.sendThroughTCPIP!",
                                        Objects.toString(paTransactionID, "N/A"),
                                        Objects.toString(backupRouterAddress, "N/A"),
                                        Objects.toString(backupRouterPort, "N/A"),
                                        Objects.toString(backupRouterResponse.getErrorCode(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                                break;
                        }
                        // indicate that the print job has made it to KDS with an error
                        PrintStatus printStatus = new PrintStatus()
                                .addPrintStatusType(PrintStatusType.ERROR)
                                .addUpdatedDTM(LocalDateTime.now());
                        PrintJobStatusHandler.getInstance().updatePrintStatusMapForPrintJob(printStatus, qcPrintJob);
                        sentToKDS = true;
                    }
                }
                else {
                    // get the reason why the XML was unable to be sent to the backup router
                    Logger.logMessage(String.format("The XML for the transaction with an ID of %s wasn't sent to the backup " +
                            "router with an IP address of %s listening on port %s for the following reason, \"%s\" in KDSUtil.sendThroughTCPIP!",
                            Objects.toString(paTransactionID, "N/A"),
                            Objects.toString(backupRouterAddress, "N/A"),
                            Objects.toString(backupRouterPort, "N/A"),
                            Objects.toString(backupRouterResponse.getErrorMsg(), "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
                }
            }
            else if ((!xmlSentToPrimaryRouter) && (!isBackupRouterConfigured)) {
                Logger.logMessage(String.format("No backup router is configured to send the XML for the transaction with an ID of %s to in KDSUtil.sendThroughTCPIP!",
                        Objects.toString(paTransactionID, "N/A")), KDS_UTIL_LOG, Logger.LEVEL.TRACE);
            }
        }
        catch (Exception e) {
            Logger.logException(e, KDS_UTIL_LOG);
            String trackingID = "";
            if (qcPrintJob != null) {
                trackingID = qcPrintJob.getJobTrackerID();
            }
            Logger.logMessage(String.format("There was a problem trying to send the print job with a print job tracker ID of %s through TCP/IP in KDSUtil.sendThroughTCPIP",
                    Objects.toString(trackingID, "N/A")), KDS_UTIL_LOG, Logger.LEVEL.ERROR);
        }

        return sentToKDS;
    }

}
