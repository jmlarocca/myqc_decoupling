package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-03-22 11:11:07 -0400 (Mon, 22 Mar 2021) $: Date of last commit
    $Rev: 13670 $: Revision of last commit
    Notes: Maintains the print status of each print queue detail within a print job.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kms.types.KMSOrderStatus;
import com.mmhayes.common.printing.PrintStatus;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHTimeFormatString;
import com.mmhayes.common.utils.StringFunctions;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>Maintains the print status of each print queue detail within a print job.</p>
 *
 */
public class PrintJobStatusHandler implements Serializable {

    // the only instance of a PrintJobStatusHandler that will exist
    private static volatile PrintJobStatusHandler printJobStatusHandler = null;

    // log file for the PrintJobStatusHandler
    private static final String PJSH_LOG = "KP_PrintJobStatusHandler.log";

    // private member variables of a PrintJobStatusHandler
    private DataManager dataManager;
    private ConcurrentHashMap<Integer, PrintStatus> printStatusMap = new ConcurrentHashMap<>();

    /**
     * <p>Getter for the PJSH_LOG field of the {@link PrintJobStatusHandler}.</p>
     *
     * @return The PJSH_LOG field of the {@link PrintJobStatusHandler}.
     */
    public static String getLog() {
        return PJSH_LOG;
    }

    /**
     * <p>Getter for the printStatusMap field of the {@link PrintJobStatusHandler}.</p>
     *
     * @return The printStatusMap field of the {@link PrintJobStatusHandler}.
     */
    public ConcurrentHashMap<Integer, PrintStatus> getPrintStatusMap () {
        return printStatusMap;
    }

    /**
     * <p>Private constructor for a {@link PrintJobStatusHandler}.</p>
     *
     * @param dataManager The {@link DataManager} to be used by the {@link PrintJobStatusHandler} to access the database.
     */
    private PrintJobStatusHandler (DataManager dataManager) {
        this.dataManager = dataManager;
    }

    /**
     * <p>Gets the only instance of a {@link PrintJobStatusHandler}.</p>
     *
     * @return The only instance of a {@link PrintJobStatusHandler}.
     * @throws Exception
     */
    public static PrintJobStatusHandler getInstance () throws Exception {
        // check that we have an instance to return
        if (printJobStatusHandler == null) {
            throw new Exception("No PrintJobStatusHandler instance has been instantiated. Instantiate the PrintJobStatusHandler instance with the init() method.");
        }
        return printJobStatusHandler;
    }

    /**
     * <p>Instantiates and returns the only instance of a {@link PrintJobStatusHandler}.</p>
     *
     * @param dataManager The {@link DataManager} to be used by the {@link PrintJobStatusHandler} to access the database.
     * @return The only instance of a {@link PrintJobStatusHandler}.
     * @throws Exception
     */
    public static synchronized PrintJobStatusHandler init (DataManager dataManager) throws Exception {
        // check if a PrintJobStatusHandler instance already exists
        if (printJobStatusHandler != null) {
            throw new Exception("An instance of PrintJobStatusHandler already exists.");
        }
        else {
            printJobStatusHandler = new PrintJobStatusHandler(dataManager);
        }
        return printJobStatusHandler;
    }

    /**
     * <p>Implementation of readResolve() to prevent creating a {@link PrintJobStatusHandler} through serialization.</p>
     *
     * @return The only instance of a {@link PrintJobStatusHandler}.
     * @throws Exception
     */
    protected PrintJobStatusHandler readResolve () throws Exception {
        return getInstance();
    }

    /**
     * <p>Finds the {@link PrintStatus} associated with the given print queue detail ID.</p>
     *
     * @param printerQueueDetailID ID of the print queue detail to get the {@link PrintStatus} for.
     * @return The {@link PrintStatus} associated with the given print queue detail ID.
     */
    public PrintStatus getPrintStatusOfPrintQueueDetail (int printerQueueDetailID) {

        if (DataFunctions.isEmptyMap(printStatusMap)) {
            Logger.logMessage(String.format("No print queue details were found within the print status map in PrintJobStatusHandler.getPrintStatusOfPrintQueueDetail, " +
                    "unable to determine the print status of the print queue detail with an ID of %s!",
                    Objects.toString(printerQueueDetailID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if (printerQueueDetailID <= 0) {
            Logger.logMessage(String.format("Encountered an invalid print queue detail ID of %s in PrintJobStatusHandler.getPrintStatusOfPrintQueueDetail, " +
                    "unable to update the status of the print queue detail!",
                    Objects.toString(printerQueueDetailID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // try to get the status
        if (printStatusMap.containsKey(printerQueueDetailID)) {
            return printStatusMap.get(printerQueueDetailID);
        }
        else {
            Logger.logMessage(String.format("The print queue detail with an ID of %s couldn't be found within the print status map in PrintJobStatusHandler.getPrintStatusOfPrintQueueDetail",
                    Objects.toString(printerQueueDetailID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return null;
        }

    }

    /**
     * <p>Updates the print status map for the given print queue detail.</p>
     *
     * @param printerQueueDetailID The ID of the print queue detail to update.
     * @param printStatus The {@link PrintStatus} to update the print queue detail to.
     */
    public void updatePrintStatusMap (int printerQueueDetailID, PrintStatus printStatus) {
        if (printerQueueDetailID <= 0) {
            Logger.logMessage(String.format("Encountered an invalid print queue detail ID of %s in PrintJobStatusHandler.updatePrintStatusMap, " +
                    "unable to update the status of the print queue detail!",
                    Objects.toString(printerQueueDetailID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (printStatus == null) {
            Logger.logMessage("The print status passed to PrintJobStatusHandler.updatePrintStatusMap can't be null, unable to update the status of the print queue detail!", PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (printStatusMap == null) {
            printStatusMap = new ConcurrentHashMap<>();
        }

        // update the map
        printStatusMap.put(printerQueueDetailID, printStatus);
        Logger.logMessage(String.format("The print queue detail with an ID of %s has had it's print status updated to %s in PrintJobStatusHandler.updatePrintStatusMap",
                Objects.toString(printerQueueDetailID, "N/A"),
                Objects.toString(PrintStatusType.getFromIntValue(printStatus.getPrintStatusType()), "N/A")), PJSH_LOG, Logger.LEVEL.DEBUG);
    }

    /**
     * <p>Removes the print queue detail with the given print queue ID form the print status map.</p>
     *
     * @param printerQueueDetailID The ID of the print queue detail to remove from the print status map.
     */
    public void removePrintQueueDetailFromPrintStatusMap (int printerQueueDetailID) {

        if (DataFunctions.isEmptyMap(printStatusMap)) {
            Logger.logMessage(String.format("No print queue details were found within the print status map in PrintJobStatusHandler.removePrintQueueDetailFromPrintStatusMap, " +
                    "unable to remove the print queue detail with an ID of %s from the print status map!",
                    Objects.toString(printerQueueDetailID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (printerQueueDetailID <= 0) {
            Logger.logMessage(String.format("Encountered an invalid print queue detail ID of %s in PrintJobStatusHandler.removePrintQueueDetailFromPrintStatusMap, " +
                    "unable to remove the print queue detail from the print status map!",
                    Objects.toString(printerQueueDetailID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        // try to remove the print queue detail from the print status map
        if (printStatusMap.containsKey(printerQueueDetailID)) {
            printStatusMap.remove(printerQueueDetailID);
        }
        else {
            Logger.logMessage(String.format("The print queue detail with an ID of %s couldn't be found within the print status map in PrintJobStatusHandler.removePrintQueueDetailFromPrintStatusMap",
                    Objects.toString(printerQueueDetailID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Sorts the print queue details based on which printer or station they should print on.</p>
     *
     * @param printQueueDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details within a print job.
     * @return A {@link HashMap} whose key is the printer ID {@link Integer} and whose value is an {@link ArrayList} of
     * {@link HashMap} corresponding to the print queue details that should print on the printer.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "UnusedAssignment"})
    public HashMap<Integer, ArrayList<HashMap>> sortPrintQueueDetailsByPrinter (ArrayList<HashMap> printQueueDetails) {
        HashMap<Integer, ArrayList<HashMap>> sortedDetails = new HashMap<>();
        ArrayList<HashMap> detailsForPrinter = null;

        if (DataFunctions.isEmptyCollection(printQueueDetails)) {
            Logger.logMessage("No print queue details have been found in PrintJobStatusHandler.sortPrintQueueDetailsByPrinter, " +
                    "unable to sort the print queue details by printer!", PJSH_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // do the sorting
        for (HashMap printQueueDetail : printQueueDetails) {
            if (!DataFunctions.isEmptyMap(printQueueDetail)) {
                int printerID = HashMapDataFns.getIntVal(printQueueDetail, "PRINTERID", true);
                if (sortedDetails.containsKey(printerID)) {
                    detailsForPrinter = sortedDetails.get(printerID);
                }
                else {
                    detailsForPrinter = new ArrayList<>();
                }
                detailsForPrinter.add(printQueueDetail);
                sortedDetails.put(printerID, detailsForPrinter);
            }
        }

        return sortedDetails;
    }

    /**
     * <p>Updates the print status map for print queue details that are on the given printer within the print job.</p>
     *
     * @param printerID The ID of the printer to update the print status of the print queue details for.
     * @param printStatus The {@link PrintStatus} to update the printer queue details to.
     * @param printQueueDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details within a print job.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public void updatePrintStatusMapForPrinter (int printerID, PrintStatus printStatus, ArrayList<HashMap> printQueueDetails) {

        if (printerID <= 0) {
            Logger.logMessage(String.format("Encountered an invalid printer with an ID of %s in PrintJobStatusHandler.updatePrintStatusMapForPrinter, the ID of the printer should be greater than 0!",
                    Objects.toString(printerID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (printStatus == null) {
            Logger.logMessage(String.format("The print status passed to PrintJobStatusHandler.updatePrintStatusMapForPrinter can't be null, " +
                    "unable to update the print statuses for print queue details on the printer with an ID of %s!",
                    Objects.toString(printerID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(printQueueDetails)) {
            Logger.logMessage(String.format("No print queue details have been found in PrintJobStatusHandler.updatePrintStatusMapForPrinter, " +
                    "unable to update the print statuses for print queue details on the printer with an ID of %s!",
                    Objects.toString(printerID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        // get the print queue details that should go to the printer
        ArrayList<Integer> printQueueDetailIDs = new ArrayList<>();
        HashMap<Integer, ArrayList<HashMap>> printQueueDetailsByPrinter = sortPrintQueueDetailsByPrinter(printQueueDetails);
        if ((!DataFunctions.isEmptyMap(printQueueDetailsByPrinter)) && (printQueueDetailsByPrinter.containsKey(printerID))) {
            ArrayList<HashMap> printQueueDetailsForPrinter = printQueueDetailsByPrinter.get(printerID);
            if (!DataFunctions.isEmptyCollection(printQueueDetailsForPrinter)) {
                printQueueDetailIDs = getPrinterQueueDetailIDs(printQueueDetailsForPrinter);
            }
        }

        // update the print status map for each print queue detail that should go to the printer
        if (!DataFunctions.isEmptyCollection(printQueueDetailIDs)) {
            for (int printQueueDetailID : printQueueDetailIDs) {
                updatePrintStatusMap(printQueueDetailID, printStatus);
            }
        }

    }

    /**
     * <p>Updates the print status map for print queue details that are on the given printer within the print job.</p>
     *
     * @param printerID The ID of the printer to update the print status of the print queue details for.
     * @param printStatus The {@link PrintStatus} to update the printer queue details to.
     * @param qcPrintJob The {@link QCPrintJob} that contains the print queue details.
     */
    @SuppressWarnings({"Convert2streamapi", "TypeMayBeWeakened"})
    public void updatePrintStatusMapForPrinter (int printerID, PrintStatus printStatus, QCPrintJob qcPrintJob) {

        if (printerID <= 0) {
            Logger.logMessage(String.format("Encountered an invalid printer with an ID of %s in PrintJobStatusHandler.updatePrintStatusMapForPrinter, the ID of the printer should be greater than 0!",
                    Objects.toString(printerID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (printStatus == null) {
            Logger.logMessage(String.format("The print status passed to PrintJobStatusHandler.updatePrintStatusMapForPrinter can't be null, " +
                    "unable to update the print statuses for print queue details on the printer with an ID of %s!",
                    Objects.toString(printerID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (qcPrintJob == null) {
            Logger.logMessage(String.format("The print job passed to PrintJobStatusHandler.updatePrintStatusMapForPrinter can't be null, " +
                    "unable to update the print statuses for print queue details on the printer with an ID of %s!",
                    Objects.toString(printerID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(qcPrintJob.getPrintJobDetails())) {
            Logger.logMessage(String.format("No print queue details have been found in PrintJobStatusHandler.updatePrintStatusMapForPrinter, " +
                    "unable to update the print statuses for print queue details on the printer with an ID of %s!",
                    Objects.toString(printerID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        // get the print queue details that should go to the printer
        ArrayList<Integer> printQueueDetailIDs = new ArrayList<>();
        for (QCPrintJobDetail qcPrintJobDetail : qcPrintJob.getPrintJobDetails()) {
            if ((qcPrintJobDetail != null) && (qcPrintJobDetail.getPrinterID() == printerID)) {
                printQueueDetailIDs.add(qcPrintJobDetail.getPaPrinterQueueDetailID());
            }
        }

        // update the print status map for each print queue detail that should go to the printer
        if (!DataFunctions.isEmptyCollection(printQueueDetailIDs)) {
            for (int printQueueDetailID : printQueueDetailIDs) {
                updatePrintStatusMap(printQueueDetailID, printStatus);
            }
        }

    }

    /**
     * <p>Updates the print status map for print queue details that are within the print job for a specific station.</p>
     *
     * @param printStatus The {@link PrintStatus} to update the printer queue details to.
     * @param qcPrintJob The {@link QCPrintJob} that contains the print queue details.
     * @param stationId The KMS Station Id to update the print job details for
     */
    public void updatePrintStatusMapForPrintJobByStationId (PrintStatus printStatus, QCPrintJob qcPrintJob, int stationId) {

        if (printStatus == null) {
            Logger.logMessage("The print status passed to PrintJobStatusHandler.updatePrintStatusMapForPrintJobByStationId can't be null, unable to update the print status map!", PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (qcPrintJob == null) {
            Logger.logMessage("The print job passed to PrintJobStatusHandler.updatePrintStatusMapForPrintJobByStationId can't be null, unable to update the print status map!", PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        QCPrintJob partialPrintJob = new QCPrintJob();
        ArrayList<QCPrintJobDetail> relevantPrintJobDetails = new ArrayList<>();

        for (QCPrintJobDetail detail : qcPrintJob.getPrintJobDetails()) {
            if (detail.getKMSStationID() == stationId) {
                relevantPrintJobDetails.add(detail);
            }
        }

        partialPrintJob.setPrintJobDetails(relevantPrintJobDetails);

        updatePrintStatusMapForPrintJob(printStatus, partialPrintJob);
    }

    /**
     * <p>Updates the print status map for print queue details that are within the print job.</p>
     *
     * @param printStatus The {@link PrintStatus} to update the printer queue details to.
     * @param qcPrintJob The {@link QCPrintJob} that contains the print queue details.
     */
    @SuppressWarnings({"Convert2streamapi", "TypeMayBeWeakened"})
    public void updatePrintStatusMapForPrintJob (PrintStatus printStatus, QCPrintJob qcPrintJob) {

        if (printStatus == null) {
            Logger.logMessage("The print status passed to PrintJobStatusHandler.updatePrintStatusMapForPrintJob can't be null, unable to update the print status map!", PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (qcPrintJob == null) {
            Logger.logMessage("The print job passed to PrintJobStatusHandler.updatePrintStatusMapForPrintJob can't be null, unable to update the print status map!", PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(qcPrintJob.getPrintJobDetails())) {
            Logger.logMessage("No print queue details have been found in PrintJobStatusHandler.updatePrintStatusMapForPrintJob, unable to update the print status map!", PJSH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        // get the print queue details that should have their print status updated
        ArrayList<Integer> printQueueDetailIDs = new ArrayList<>();
        for (QCPrintJobDetail qcPrintJobDetail : qcPrintJob.getPrintJobDetails()) {
            if (qcPrintJobDetail != null) {
                printQueueDetailIDs.add(qcPrintJobDetail.getPaPrinterQueueDetailID());
            }
        }

        // update the print status map for each print queue detail that should go to the printer
        if (!DataFunctions.isEmptyCollection(printQueueDetailIDs)) {
            for (int printQueueDetailID : printQueueDetailIDs) {
                updatePrintStatusMap(printQueueDetailID, printStatus);
            }
        }

    }

    /**
     * <p>Sorts the print queue details within the given print data based on the print queue record they belong to.</p>
     *
     * @param queuedPrintData The {@link ArrayList} of print data to sort.
     * @return A {@link HashMap} whose key is the {@link Integer} ID of the print queue record and whose value is
     * an {@link ArrayList} of {@link HashMap} corresponding to the print queue detail records within the print queue record.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "Convert2streamapi"})
    private HashMap<Integer, ArrayList<HashMap>> sortPrintQueueDetailsByPrintQueueID (ArrayList queuedPrintData) {

        if (DataFunctions.isEmptyCollection(queuedPrintData)) {
            Logger.logMessage("The queued print data passed to PrintJobStatusHandler.sortPrintQueueDetailsByPrintQueueID is " +
                    "null or empty, unable to sort the print queue details by print queue ID!", PJSH_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // do the sorting
        HashMap<Integer, ArrayList<HashMap>> sortedDetails = new HashMap<>();
        ArrayList<HashMap> commonPrintQueueDetails;
        for (Object printQueueObj : queuedPrintData) {
            if ((printQueueObj != null) && (printQueueObj instanceof ArrayList)) {
                ArrayList printQueueRec = ((ArrayList) printQueueObj);
                if (!DataFunctions.isEmptyCollection(printQueueRec)) {
                    for (Object printQueueDetailObj : printQueueRec) {
                        if ((printQueueDetailObj != null) && (printQueueDetailObj instanceof HashMap)) {
                            HashMap printQueueDetailRec = ((HashMap) printQueueDetailObj);
                            int paPrinterQueueID = HashMapDataFns.getIntVal(printQueueDetailRec, "PAPRINTERQUEUEID");
                            if (sortedDetails.containsKey(paPrinterQueueID)) {
                                commonPrintQueueDetails = sortedDetails.get(paPrinterQueueID);
                            }
                            else {
                                commonPrintQueueDetails = new ArrayList<>();
                            }
                            commonPrintQueueDetails.add(printQueueDetailRec);
                            sortedDetails.put(paPrinterQueueID, commonPrintQueueDetails);
                        }
                    }
                }
            }
        }

        return sortedDetails;
    }

    /**
     * <p>Extracts the print queue details from the given print job.</p>
     *
     * @param printJob The {@link ArrayList} print job to get the print queue details within.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print queue details within the print job.
     */
    @SuppressWarnings({"Convert2streamapi", "TypeMayBeWeakened"})
    private ArrayList<HashMap> getPrintQueueDetailsFromPrintJob (ArrayList printJob) {

        if (DataFunctions.isEmptyCollection(printJob)) {
            Logger.logMessage("The print job passed to PrintJobStatusHandler.getPrintQueueDetailsFromPrintJob is null or empty, unable to get the print queue details!", PJSH_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<HashMap> printQueueDetails = new ArrayList<>();
        for (Object printQueueDetailsObj : printJob) {
            if ((printQueueDetailsObj != null) && (printQueueDetailsObj instanceof  ArrayList)) {
                ArrayList somePrintQueueDetails = ((ArrayList) printQueueDetailsObj);
                if (!DataFunctions.isEmptyCollection(somePrintQueueDetails)) {
                    printQueueDetails.addAll(DataFunctions.convertToCollectionOfHM(somePrintQueueDetails));
                }
            }
        }

        return printQueueDetails;
    }

    /**
     * <p>Iterates through the print queue details and checks whether or not they all have the same print status.</p>
     *
     * @param printQueueDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details within the print job.
     * @return Whether or not all print queue details have the same print status.
     */
    @SuppressWarnings({"Convert2streamapi", "TypeMayBeWeakened"})
    private boolean doPrintQueueDetailsHaveSamePrintStatus (ArrayList<HashMap> printQueueDetails) {

        if (DataFunctions.isEmptyCollection(printQueueDetails)) {
            Logger.logMessage("No print queue details found in PrintJobStatusHandler.doPrintQueueDetailsHaveSamePrintStatus, unable " +
                    "to determine if all the print queue details have the same print status!", PJSH_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        boolean firstItr = true;
        int firstPrintStatus = 0;
        for (HashMap printQueueDetail : printQueueDetails) {
            if (!DataFunctions.isEmptyMap(printQueueDetail)) {
                // set the first print status found that all subsequent print statuses will be compared against
                if (firstItr) {
                    firstPrintStatus = HashMapDataFns.getIntVal(printQueueDetail, "PRINTSTATUSID", true);
                    firstItr = false;
                }
                else {
                    if (firstPrintStatus != HashMapDataFns.getIntVal(printQueueDetail, "PRINTSTATUSID", true)) {
                        return false; // we found a print status that doesn't match the first
                    }
                }
            }
        }

        return true;
    }

    /**
     * <p>Sorts the print queue details by print status ID.</p>
     *
     * @param printQueueDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details to sort.
     * @return A {@link HashMap} whose key is the {@link Integer} print status ID and whose value is an {@link ArrayList}
     * of {@link HashMap} corresponding to the print queue details that share the same print status.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "UnusedAssignment"})
    public HashMap<Integer, ArrayList<HashMap>> sortPrintQueueDetailsByPrintStatus (ArrayList<HashMap> printQueueDetails) {

        if (DataFunctions.isEmptyCollection(printQueueDetails)) {
            Logger.logMessage("No print queue details found in PrintJobStatusHandler.sortPrintQueueDetailsByPrintStatus, unable " +
                    "to sort the print queue details based on print status!", PJSH_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // do the sorting
        HashMap<Integer, ArrayList<HashMap>> sortedDetails = new HashMap<>();
        ArrayList<HashMap> commonStatusDetails = null;
        for (HashMap printQueueDetail : printQueueDetails) {
            if (!DataFunctions.isEmptyMap(printQueueDetail)) {
                int printStatusID = HashMapDataFns.getIntVal(printQueueDetail, "PRINTSTATUSID", true);
                if (sortedDetails.containsKey(printStatusID)) {
                    commonStatusDetails = sortedDetails.get(printStatusID);
                }
                else {
                    commonStatusDetails = new ArrayList<>();
                }
                commonStatusDetails.add(printQueueDetail);
                sortedDetails.put(printStatusID, commonStatusDetails);
            }
        }

        return sortedDetails;
    }

    /**
     * <p>Sorts the order queue details by order status ID.</p>
     *
     * @param printQueueDetails An {@link ArrayList} of {@link HashMap} corresponding to the order queue details to sort.
     * @return A {@link HashMap} whose key is the {@link Integer} order status ID and whose value is an {@link ArrayList}
     * of {@link HashMap} corresponding to the order queue details that share the same order status.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "UnusedAssignment"})
    public HashMap<Integer, ArrayList<HashMap>> sortorderQueueDetailsByKMSOrderStatus (ArrayList<HashMap> printQueueDetails) {

        if (DataFunctions.isEmptyCollection(printQueueDetails)) {
            Logger.logMessage("No order queue details found in orderJobStatusHandler.sortorderQueueDetailsByorderStatus, unable " +
                    "to sort the order queue details based on order status!", PJSH_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // do the sorting
        HashMap<Integer, ArrayList<HashMap>> sortedDetails = new HashMap<>();
        ArrayList<HashMap> commonStatusDetails = null;
        for (HashMap printQueueDetail : printQueueDetails) {
            if (!DataFunctions.isEmptyMap(printQueueDetail)) {
                int orderStatusID = HashMapDataFns.getIntVal(printQueueDetail, "KMSORDERSTATUSID", true);
                if(orderStatusID > 0){
                    if (sortedDetails.containsKey(orderStatusID)) {
                        commonStatusDetails = sortedDetails.get(orderStatusID);
                    }
                    else {
                        commonStatusDetails = new ArrayList<>();
                    }
                    commonStatusDetails.add(printQueueDetail);
                    sortedDetails.put(orderStatusID, commonStatusDetails);
                }
            }
        }

        return sortedDetails;
    }

    /**
     * <p>Iterates through the print queue details and gets the IDs of the print queue details.</p>
     *
     * @param printQueueDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details to get the IDs of.
     * @return An {@link ArrayList} of {@link Integer} corresponding to the IDs of the given print queue details.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "Convert2streamapi"})
    public ArrayList<Integer> getPrinterQueueDetailIDs (ArrayList<HashMap> printQueueDetails) {

        if (DataFunctions.isEmptyCollection(printQueueDetails)) {
            Logger.logMessage("No print queue details found in PrintJobStatusHandler.getPrinterQueueDetailIDs, unable " +
                    "to get the print queue detail IDs!", PJSH_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<Integer> printQueueDetailIDs = new ArrayList<>();
        for (HashMap printQueueDetail : printQueueDetails) {
            if (!DataFunctions.isEmptyMap(printQueueDetail)) {
                printQueueDetailIDs.add(HashMapDataFns.getIntVal(printQueueDetail, "PAPRINTERQUEUEDETAILID", true));
            }
        }

        return printQueueDetailIDs;
    }

    /**
     * <p>Updates the print status of the given print queue record.</p>
     *
     * @param dataManager The {@link DataManager} to use to update the print queue record in the database.
     * @param printQueueID The ID of the print queue record to update the status for.
     * @param printStatus The {@link PrintStatus} to update the print queue record to.
     */
    public void updatePrintStatusOfPrintQueueHeader (DataManager dataManager, int printQueueID, PrintStatus printStatus) {

        try {
            // make sure we have a valid DataManager
            if (dataManager == null) {
                Logger.logMessage("The DataManager passed to PrintJobStatusHandler.updatePrintStatusOfPrintQueueHeader can't be null, " +
                        "unable to update the print status of the print queue record!", PJSH_LOG, Logger.LEVEL.ERROR);
                return;
            }

            Logger.logMessage(String.format("Updating the print status of the print queue record with an ID of %s to %s in PrintJobStatusHandler.updatePrintStatusOfPrintQueueHeader",
                    Objects.toString(printQueueID, "N/A"),
                    Objects.toString(printStatus != null ? PrintStatusType.getFromIntValue(printStatus.getPrintStatusType()) : "N/A", "N/A")), PJSH_LOG, Logger.LEVEL.TRACE);

            // try to update the status in the database
            DynamicSQL dynamicSQL = new DynamicSQL("data.kitchenPrinter.UpdatePrintQueueHeaderPrintStatus")
                    .addIDList(1, printQueueID)
                    .addIDList(2, (printStatus != null ? printStatus.getPrintStatusType() : PrintStatusType.ERROR));
            if (dynamicSQL.runUpdate(dataManager) <= 0) {
                Logger.logMessage(String.format("A problem occurred while trying to update the print status of the print " +
                        "queue record with an ID of %s to a print status of %s within the database in PrintJobStatusHandler.updatePrintStatusOfPrintQueueHeader",
                        Objects.toString(printQueueID, "N/A"),
                        Objects.toString(printStatus != null ? PrintStatusType.getFromIntValue(printStatus.getPrintStatusType()) : "N/A", "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PJSH_LOG);
            Logger.logMessage(String.format("There was a problem trying to update the print status to %s for the print queue record with an " +
                    "ID of %s in PrintJobStatusHandler.updatePrintStatusOfPrintQueueHeader",
                    Objects.toString(printStatus != null ? PrintStatusType.getFromIntValue(printStatus.getPrintStatusType()) : "N/A", "N/A"),
                    Objects.toString(printQueueID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Updates the print status of the given print queue detail record IDs.</p>
     *
     * @param dataManager The {@link DataManager} to use to update the print queue detail records in the database.
     * @param printQueueDetailIDs An {@link ArrayList} of {@link Integer} corresponding to the IDs of print queue detail records to update the status for.
     * @param printStatus The {@link PrintStatus} to update the print queue detail records to.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    public void updatePrintStatusOfPrintQueueDetails (DataManager dataManager, ArrayList<Integer> printQueueDetailIDs, PrintStatus printStatus) {

        try {
            // make sure we have a valid DataManager
            if (dataManager == null) {
                Logger.logMessage("The DataManager passed to PrintJobStatusHandler.updatePrintStatusOfPrintQueueDetails can't be null, " +
                        "unable to update the print status of the print queue detail records!", PJSH_LOG, Logger.LEVEL.ERROR);
                return;
            }

            Logger.logMessage(String.format("Updating the print status of the print queue detail records with IDs of %s to %s in PrintJobStatusHandler.updatePrintStatusOfPrintQueueDetails",
                    Objects.toString(StringFunctions.buildStringFromList(printQueueDetailIDs, ","), "N/A"),
                    Objects.toString(printStatus != null ? PrintStatusType.getFromIntValue(printStatus.getPrintStatusType()) : "N/A", "N/A")), PJSH_LOG, Logger.LEVEL.TRACE);

            // convert the print queue detail IDs into an ArrayList of String
            ArrayList<String> printQueueDetailIDStrs = new ArrayList<>();
            if (!DataFunctions.isEmptyCollection(printQueueDetailIDs)) {
                Collection<String> printQueueDetailIDStrsCol = DataFunctions.convertIntCollectionToStrCollection(printQueueDetailIDs);
                if (!DataFunctions.isEmptyCollection(printQueueDetailIDStrsCol)) {
                    printQueueDetailIDStrs = new ArrayList<>(printQueueDetailIDStrsCol);
                }
            }

            if (!DataFunctions.isEmptyCollection(printQueueDetailIDStrs)) {
                LocalDateTime updatedDTM = (printStatus != null ? printStatus.getUpdatedDTM() : LocalDateTime.now());
                DynamicSQL dynamicSQL = new DynamicSQL("data.kitchenPrinter.UpdatePrintQueueDetailsPrintStatus")
                        .addLargeIDList(1, printQueueDetailIDStrs)
                        .addIDList(2, (printStatus != null ? printStatus.getPrintStatusType() : PrintStatusType.ERROR))
                        .addIDList(3, updatedDTM, DateTimeFormatter.ofPattern(MMHTimeFormatString.YR_MO_DY_HR_MIN_SEC_MS));
                // try to update the status in the database
                if (dynamicSQL.runUpdate(dataManager) <= 0) {
                    Logger.logMessage(String.format("A problem occurred while trying to update the print status of the print " +
                            "queue detail records IDs of %s to a print status of %s within the database in PrintJobStatusHandler.updatePrintStatusOfPrintQueueDetails",
                            Objects.toString(StringFunctions.buildStringFromList(printQueueDetailIDs, ","), "N/A"),
                            Objects.toString(printStatus != null ? PrintStatusType.getFromIntValue(printStatus.getPrintStatusType()) : "N/A", "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PJSH_LOG);
            Logger.logMessage(String.format("There was a problem trying to update the print status to %s for the print queue detail records with IDs " +
                    "of %s in PrintJobStatusHandler.updatePrintStatusOfPrintQueueDetails",
                    Objects.toString(printStatus != null ? PrintStatusType.getFromIntValue(printStatus.getPrintStatusType()) : "N/A", "N/A"),
                    Objects.toString(!DataFunctions.isEmptyCollection(printQueueDetailIDs) ? StringFunctions.buildStringFromList(printQueueDetailIDs, ",") : "N/A", "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Updates the print status of the given print queue record.</p>
     *
     * @param dataManager The {@link DataManager} to use to update the print queue record in the database.
     * @param printQueueID The ID of the print queue record to update the status for.
     * @param orderStatus The {@link KMSOrderStatus} to update the print queue record to.
     */
    public void updateOrderStatusOfPrintQueueHeader (DataManager dataManager, int printQueueID, KMSOrderStatus orderStatus) {

        try {
            // make sure we have a valid DataManager
            if (dataManager == null) {
                Logger.logMessage("The DataManager passed to PrintJobStatusHandler.updatePrintStatusOfPrintQueueHeader can't be null, " +
                        "unable to update the print status of the print queue record!", PJSH_LOG, Logger.LEVEL.ERROR);
                return;
            }

            Logger.logMessage(String.format("Updating the order status of the order queue record with an ID of %s to %s in PrintJobStatusHandler.updateOrderStatusOfPrintQueueHeader",
                    Objects.toString(printQueueID, "N/A"),
                    Objects.toString(orderStatus != null ? PrintStatusType.getFromIntValue(orderStatus.getKMSOrderStatusType()) : "N/A", "N/A")), PJSH_LOG, Logger.LEVEL.TRACE);

            // try to update the status in the database
            DynamicSQL dynamicSQL = new DynamicSQL("data.kitchenPrinter.UpdatePrintQueueHeaderOrderStatus")
                    .addIDList(1, printQueueID)
                    .addIDList(2, (orderStatus != null ? orderStatus.getKMSOrderStatusType() : KMSOrderStatus.ERROR));
            if (dynamicSQL.runUpdate(dataManager) <= 0) {
                Logger.logMessage(String.format("A problem occurred while trying to update the order status of the print " +
                        "queue record with an ID of %s to a order status of %s within the database in PrintJobStatusHandler.updateOrderStatusOfPrintQueueHeader",
                        Objects.toString(printQueueID, "N/A"),
                        Objects.toString(orderStatus != null ? PrintStatusType.getFromIntValue(orderStatus.getKMSOrderStatusType()) : "N/A", "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PJSH_LOG);
            Logger.logMessage(String.format("There was a problem trying to update the order status to %s for the print queue record with an " +
                    "ID of %s in PrintJobStatusHandler.updatePrintStatusOfPrintQueueHeader",
                    Objects.toString(orderStatus != null ? PrintStatusType.getFromIntValue(orderStatus.getKMSOrderStatusType()) : "N/A", "N/A"),
                    Objects.toString(printQueueID, "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Updates the order status of the given order queue detail record IDs.</p>
     *
     * @param dataManager The {@link DataManager} to use to update the order queue detail records in the database.
     * @param orderQueueDetailIDs An {@link ArrayList} of {@link Integer} corresponding to the IDs of order queue detail records to update the status for.
     * @param orderStatus The {@link PrintStatus} to update the order queue detail records to.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    public void updateKMSOrderStatusOfPrintQueueDetails (DataManager dataManager, ArrayList<Integer> orderQueueDetailIDs, KMSOrderStatus orderStatus) {

        try {
            // make sure we have a valid DataManager
            if (dataManager == null) {
                Logger.logMessage("The DataManager passed to PrintJobStatusHandler.updatePrintStatusOfPrintQueueDetails can't be null, " +
                        "unable to update the order status of the order queue detail records!", PJSH_LOG, Logger.LEVEL.ERROR);
                return;
            }

            Logger.logMessage(String.format("Updating the order status of the order queue detail records with IDs of %s to %s in PrintJobStatusHandler.updateKMSOrderStatusOfPrintQueueDetails",
                    Objects.toString(StringFunctions.buildStringFromList(orderQueueDetailIDs, ","), "N/A"),
                    Objects.toString(orderStatus != null ? PrintStatusType.getFromIntValue(orderStatus.getKMSOrderStatusType()) : "N/A", "N/A")), PJSH_LOG, Logger.LEVEL.TRACE);

            // convert the order queue detail IDs into an ArrayList of String
            ArrayList<String> orderQueueDetailIDStrs = new ArrayList<>();
            if (!DataFunctions.isEmptyCollection(orderQueueDetailIDs)) {
                Collection<String> orderQueueDetailIDStrsCol = DataFunctions.convertIntCollectionToStrCollection(orderQueueDetailIDs);
                if (!DataFunctions.isEmptyCollection(orderQueueDetailIDStrsCol)) {
                    orderQueueDetailIDStrs = new ArrayList<>(orderQueueDetailIDStrsCol);
                }
            }

            if (!DataFunctions.isEmptyCollection(orderQueueDetailIDStrs)) {
                LocalDateTime updatedDTM = (orderStatus != null ? orderStatus.getUpdatedDTM() : LocalDateTime.now());
                DynamicSQL dynamicSQL = new DynamicSQL("data.kitchenPrinter.UpdatePrintQueueDetailsOrderStatus")
                        .addIDList(1, orderQueueDetailIDStrs)
                        .addIDList(2, (orderStatus != null ? orderStatus.getKMSOrderStatusType() : PrintStatusType.ERROR))
                        .addIDList(3, updatedDTM, DateTimeFormatter.ofPattern(MMHTimeFormatString.YR_MO_DY_HR_MIN_SEC_MS));
                // try to update the status in the database
                if (dynamicSQL.runUpdate(dataManager) <= 0) {
                    Logger.logMessage(String.format("A problem occurred while trying to update the order status of the order " +
                            "queue detail records IDs of %s to a order status of %s within the database in PrintJobStatusHandler.updateKMSOrderStatusOfPrintQueueDetails",
                            Objects.toString(StringFunctions.buildStringFromList(orderQueueDetailIDs, ","), "N/A"),
                            Objects.toString(orderStatus != null ? PrintStatusType.getFromIntValue(orderStatus.getKMSOrderStatusType()) : "N/A", "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PJSH_LOG);
            Logger.logMessage(String.format("There was a problem trying to update the order status to %s for the order queue detail records with IDs " +
                    "of %s in PrintJobStatusHandler.updatePrintStatusOfPrintQueueDetails",
                    Objects.toString(orderStatus != null ? PrintStatusType.getFromIntValue(orderStatus.getKMSOrderStatusType()) : "N/A", "N/A"),
                    Objects.toString(!DataFunctions.isEmptyCollection(orderQueueDetailIDs) ? StringFunctions.buildStringFromList(orderQueueDetailIDs, ",") : "N/A", "N/A")), PJSH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Updates the print status of the print queue record and print queue detail records in the database.</p>
     *
     * @param queuedPrintData The {@link ArrayList} of all print queue detail records.
     */
    @SuppressWarnings({"Convert2streamapi", "OverlyComplexMethod", "TypeMayBeWeakened"})
    public void updatePrintQueueStatusesInDB (ArrayList queuedPrintData) {

        try {
            if (DataFunctions.isEmptyCollection(queuedPrintData)) {
                Logger.logMessage("The queued print data passed to PrintJobStatusHandler.updatePrintQueueStatusesInDB can't be null or empty, " +
                        "unable to update the print status of the print queue header and details in the database!", PJSH_LOG, Logger.LEVEL.ERROR);
                return;
            }

            DataManager dataManager = KitchenPrinterTerminal.getInstance().getDataManager();

            // get a list of all the print queue details in the print job
            HashMap<Integer, ArrayList<HashMap>> printQueueRecsHM = sortPrintQueueDetailsByPrintQueueID(queuedPrintData);
            if (!DataFunctions.isEmptyMap(printQueueRecsHM)) {
                for (Map.Entry<Integer, ArrayList<HashMap>> printQueueRecEntry : printQueueRecsHM.entrySet()) {
                    int printQueueID = printQueueRecEntry.getKey();
                    ArrayList<HashMap> printQueueDetails = printQueueRecEntry.getValue();
                    if (!DataFunctions.isEmptyCollection(printQueueDetails)) {

                        int jobsKMSOrderStatusID = HashMapDataFns.getIntVal(printQueueDetails.get(0), "ORDERSKMSORDERSTATUSID", true);
                        if(jobsKMSOrderStatusID > 0){
                            KMSOrderStatus orderStatus = new KMSOrderStatus()
                                    .addKMSOrderStatusType(jobsKMSOrderStatusID)
                                    .addUpdatedDTM(LocalDateTime.now());
                            // do the update in the database
                            updateOrderStatusOfPrintQueueHeader(dataManager, printQueueID, orderStatus);
                        }

                        // update the print queue header in the database
                        if (doPrintQueueDetailsHaveSamePrintStatus(printQueueDetails)) {
                            int printStatusID = HashMapDataFns.getIntVal(printQueueDetails.get(0), "PRINTSTATUSID", true);
                            // build the PrintStatus
                            PrintStatus printStatus = new PrintStatus()
                                    .addPrintStatusType(printStatusID)
                                    .addUpdatedDTM(LocalDateTime.now());
                            // do the update in the database
                            updatePrintStatusOfPrintQueueHeader(dataManager, printQueueID, printStatus);
                        }

                        //the way this works currently is it groups the details by their statusID and updates them all at once, same for KMSORDERSTATUSID?
                        HashMap<Integer, ArrayList<HashMap>> sortedKMSPrintQueueDetails = sortorderQueueDetailsByKMSOrderStatus(printQueueDetails);
                        if (!DataFunctions.isEmptyMap(sortedKMSPrintQueueDetails)) {
                            for (Map.Entry<Integer, ArrayList<HashMap>> sortedPrintQueueDetailsEntry : sortedKMSPrintQueueDetails.entrySet()) {
                                int KMSOrderStatusID = sortedPrintQueueDetailsEntry.getKey();
                                ArrayList<Integer> printQueueDetailIDs = getPrinterQueueDetailIDs(sortedPrintQueueDetailsEntry.getValue());
                                // build the print status
                                KMSOrderStatus orderStatus = new KMSOrderStatus()
                                        .addKMSOrderStatusType(KMSOrderStatusID)
                                        .addUpdatedDTM(LocalDateTime.now());
                                // do the update in the database
                                if (!DataFunctions.isEmptyCollection(printQueueDetailIDs)) {
                                    updateKMSOrderStatusOfPrintQueueDetails(dataManager, printQueueDetailIDs, orderStatus);
                                }
                            }
                        }

                        // update the print queue details in the database
                        HashMap<Integer, ArrayList<HashMap>> sortedPrintQueueDetails = sortPrintQueueDetailsByPrintStatus(printQueueDetails);
                        if (!DataFunctions.isEmptyMap(sortedPrintQueueDetails)) {
                            for (Map.Entry<Integer, ArrayList<HashMap>> sortedPrintQueueDetailsEntry : sortedPrintQueueDetails.entrySet()) {
                                int printStatusID = sortedPrintQueueDetailsEntry.getKey();
                                ArrayList<Integer> printQueueDetailIDs = getPrinterQueueDetailIDs(sortedPrintQueueDetailsEntry.getValue());
                                // build the print status
                                PrintStatus printStatus = new PrintStatus()
                                        .addPrintStatusType(printStatusID)
                                        .addUpdatedDTM(LocalDateTime.now());
                                // do the update in the database
                                if (!DataFunctions.isEmptyCollection(printQueueDetailIDs)) {
                                    updatePrintStatusOfPrintQueueDetails(dataManager, printQueueDetailIDs, printStatus);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PJSH_LOG);
            Logger.logMessage("There was a problem trying update the print job statuses of the print queue and print queue " +
                    "details in PrintJobStatusHandler.updatePrintQueueStatusesInDB", PJSH_LOG, Logger.LEVEL.ERROR);
        }

    }

}