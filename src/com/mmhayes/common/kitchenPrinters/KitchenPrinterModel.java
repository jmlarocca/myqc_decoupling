package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-28 07:51:11 -0400 (Wed, 28 Apr 2021) $: Date of last commit
    $Rev: 13861 $: Revision of last commit
    Notes: Represents a model of kitchen printer.
*/

public enum KitchenPrinterModel {

    TM_88V(1, "TM-88V", 40, 20),
    TM_88VI(2, "TM-88VI", 40, 20),
    TM_U220(3, "TM-U220", 32, 16),
    TM_L90(4, "TM-L90", 42, 21);

    private int printerModelID;
    private String name;
    private int maxReceiptCharsPerLineNormal;
    private int maxReceiptCharsPerLineLarge;

    /**
     * <p>Constructor for a KitchenPrinterModel.</p>
     *
     * @param printerModelID ID of the printer model.
     * @param name The name {@link String} of the printer model with the associated ID.
     */
    KitchenPrinterModel (final int printerModelID, final String name, final int maxReceiptCharsPerLineNormal, final int maxReceiptCharsPerLineLarge) {
        this.printerModelID = printerModelID;
        this.name = name;
        this.maxReceiptCharsPerLineNormal = maxReceiptCharsPerLineNormal;
        this.maxReceiptCharsPerLineLarge = maxReceiptCharsPerLineLarge;
    }

    /**
     * <p>Getter for the printerModelID field of the {@link KitchenPrinterModel}.</p>
     *
     * @return The printerModelID field of the {@link KitchenPrinterModel}.
     */
    public int getPrinterModelID () {
        return printerModelID;
    }

    /**
     * <p>Getter for the name field of the {@link KitchenPrinterModel}.</p>
     *
     * @return The name field of the {@link KitchenPrinterModel}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Getter for the maxReceiptCharsPerLineNormal field of the {@link KitchenPrinterModel}.</p>
     *
     * @return The maxReceiptCharsPerLineNormal field of the {@link KitchenPrinterModel}.
     */
    public int getMaxReceiptCharsPerLineNormal() {
        return maxReceiptCharsPerLineNormal;
    }

    /**
     * <p>Getter for the maxReceiptCharsPerLineLarge field of the {@link KitchenPrinterModel}.</p>
     *
     * @return The maxReceiptCharsPerLineLarge field of the {@link KitchenPrinterModel}.
     */
    public int getMaxReceiptCharsPerLineLarge() {
        return maxReceiptCharsPerLineLarge;
    }

    /**
     * <p>Reverse lookup to obtain a {@link KitchenPrinterModel}.</p>
     *
     * @param printerModelID The ID of the printer madel to get.
     * @return The {@link KitchenPrinterModel} with the given printer model ID.
     */
    public static KitchenPrinterModel get (int printerModelID) {

        for (KitchenPrinterModel kitchenPrinterModel : values()) {
            if (kitchenPrinterModel.printerModelID == printerModelID) {
                return kitchenPrinterModel;
            }
        }

        return null;
    }

}