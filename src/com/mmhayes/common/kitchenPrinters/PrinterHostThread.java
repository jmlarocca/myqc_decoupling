package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2019-12-10 16:46:53 -0500 (Tue, 10 Dec 2019) $: Date of last commit
    $Rev: 44337 $: Revision of last commit
    Notes: Poll server for online orders and send print jobs to the local database.
*/

import com.mmhayes.common.conn_pool.JDCConnection;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.kmsapi.KMSDataManager;
import com.mmhayes.common.printing.PrintStatus;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.xmlapi.XmlRpcUtil;
import org.apache.commons.lang.math.NumberUtils;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Singleton class responsible for polling the server for online orders and adding those print jobs to the local
 * database.
 *
 */
public class PrinterHostThread implements Runnable {

    // the only instance of a PrinterHostThread for this WAR
    private static volatile PrinterHostThread printerHostThreadInstance = null;

    // log file specific to the PrinterHostThread
    private static final String PHT_LOG = "KP_PrinterHostThread.log";

    // variables within the PrinterHostThread scope
    private volatile boolean isShuttingDown = false;
    private volatile boolean isTerminated = false;
    private PrinterQueueHandler printQueueHandler;
    private PrintJobStatusHandler printJobStatusHandler = null;

    // variables needed for PrinterHostThread instantiation
    private KitchenPrinterTerminal kitchenPrinterTerminal;
    private KMSDataManager kmsdm;
    private ArrayList<HashMap> printerList;

    /**
     * Private constructor for a PrinterHostThread.
     *
     * @param kitchenPrinterTerminal {@link KitchenPrinterTerminal} KitchenPrinterTerminal to be used by the
     *         PrinterHostThread.
     * @param printerList {@link ArrayList<HashMap>} Records from the QC_Printer table that represent printers
     *         configured to run with the printer host.
     */
    private PrinterHostThread (KitchenPrinterTerminal kitchenPrinterTerminal, ArrayList<HashMap> printerList) throws Exception {
        this.kitchenPrinterTerminal = kitchenPrinterTerminal;
        this.printerList = printerList;
        this.printJobStatusHandler = PrintJobStatusHandler.getInstance();
        this.kmsdm = new KMSDataManager();
        try {
            printQueueHandler = PrinterQueueHandler.init(printerList);
            printQueueHandler.startPrinterQueueHandlerThread();
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem trying to instantiate the PrinterQueueHandler instance and start " +
                    "the PrinterQueueHandler Thread in the PrinterHostThread constructor", PHT_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Get the only instance of PrinterHostThread for this WAR.
     *
     * @return {@link PrinterHostThread} The instance of PrinterHostThread for this WAR.
     */
    public static PrinterHostThread getInstance () {

        try {
            if (printerHostThreadInstance == null) {
                throw new RuntimeException("The PrinterHostThread must be instantiated before trying to obtain its " +
                        "instance.");
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem trying to get the only instance of PrinterHostThread for this WAR " +
                    "in PrinterHostThread.getInstance", PHT_LOG, Logger.LEVEL.ERROR);
        }

        return printerHostThreadInstance;
    }

    /**
     * Instantiate and return the only instance of PrinterHostThread for this WAR.
     *
     * @param kitchenPrinterTerminal {@link KitchenPrinterTerminal} KitchenPrinterTerminal to be used by the
     *         PrinterHostThread.
     * @param printerList {@link ArrayList<HashMap>} Records from the QC_Printer table that represent printers
     *         configured to run with the printer host.
     * @return {@link PrinterHostThread} The instance of PrinterHostThread for this WAR.
     */
    public static synchronized PrinterHostThread init (KitchenPrinterTerminal kitchenPrinterTerminal,
                                                       ArrayList<HashMap> printerList) {

        try {
            if (printerHostThreadInstance != null) {
                throw new RuntimeException("The PrinterHostThread has already been instantiated and may not be " +
                        "instantiated again.");
            }

            // create the instance
            printerHostThreadInstance = new PrinterHostThread(kitchenPrinterTerminal, printerList);
            Logger.logMessage("The instance of PrinterHostThread has been instantiated", PHT_LOG, Logger.LEVEL.TRACE);
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem trying to instantiate and return the only instance of " +
                    "PrinterHostThread for this WAR in PrinterHostThread.init", PHT_LOG, Logger.LEVEL.ERROR);
        }

        return printerHostThreadInstance;
    }

    /**
     * Method that will be called by a Thread created using the PrinterHostThread Runnable.
     *
     */
    @SuppressWarnings({"MagicNumber", "Convert2streamapi", "TypeMayBeWeakened"})
    @Override
    public void run () {

        try {
            Thread.currentThread().setName("KitchenPrinter-PrinterHostThread");

            while ((!isShuttingDown) && (!Thread.currentThread().isInterrupted())) {
                try {
                    // sleep for 10 seconds
                    Thread.sleep(10000L);

                    // should we check for online orders
                    if (shouldCheckForOnlineOrders()) {
                        checkServerForOnlineOrderIDs();
                    }
                    else {
                        phServerCheckIn();
                    }

                    removeExpiredPrintQueueDetailsFromPrintStatusMap();
                }
                catch (Exception e) {
                    Logger.logException(e, PHT_LOG);
                    Logger.logMessage("There was a problem in the while loop of PrinterHostThread.run", PHT_LOG, Logger.LEVEL.ERROR);
                }
                Thread.sleep(100L);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem in PrinterHostThread.run", PHT_LOG, Logger.LEVEL.ERROR);
        }
        finally {
            isTerminated = true;
            Logger.logMessage("KitchenPrinter-PrinterHostThread is exiting", PHT_LOG, Logger.LEVEL.TRACE);
        }

    }

    /**
     * Determine whether or not the PrinterHostThread Runnable should be polling the server for online orders.
     *
     * @return Whether or not the PrinterHostThread should poll the server for online orders.
     */
    private boolean shouldCheckForOnlineOrders () {
        boolean shouldPollServer = false;

        try {
            Object result = kitchenPrinterTerminal.getDataManager().parameterizedExecuteScalar(
                    "data.kitchenPrinter.checkForOnlineOrderingTermsInRevCenters",
                    new Object[]{PeripheralsDataManager.getPHRevCenters()}, PHT_LOG);
            if (result != null) {
                shouldPollServer = true;
                Logger.logMessage("The PrinterHostThread Runnable will poll the server for online orders", PHT_LOG,
                        Logger.LEVEL.DEBUG);
            }
            else {
                Logger.logMessage("The PrinterHostThread Runnable will not poll the server for online orders", PHT_LOG,
                        Logger.LEVEL.DEBUG);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem trying to determine whether or not the PrinterHostThread Runnable " +
                    "should be polling the server for online orders in PrinterHostThread.shouldCheckForOnlineOrders",
                    PHT_LOG, Logger.LEVEL.ERROR);
        }

        return shouldPollServer;
    }

    /**
     * Poll the server for any online orders that have been placed and create any print jobs that will need to be
     * printed for the online orders.
     *
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    private void checkServerForOnlineOrders () {

        try {
            // update the LastOnlineOrderCheck time on the server, for the printer host configured as the primary in the
            // back office, if the active printer host was configured as a backup then still have it update the
            // LastOnlineOrderCheck for what was configured as the primary
//            if (StringFunctions.stringHasContent(kitchenPrinterTerminal.getActivePHMacAddress())) {
//                String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
//                String macAddress = PeripheralsDataManager.getPHMacAddress();
//                String hostname = PeripheralsDataManager.getPHHostname();
//                String methodName = "PeripheralsDataManager.updatePHLastOnlineOrderCheck";
//                Object[] methodParams = new Object[]{kitchenPrinterTerminal.getActivePHMacAddress()};
//                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName,
//                        methodParams);
//
//                // get any transactions from the XML RPC response so they can be printed
//                if (XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, methodName)) {
//                    Logger.logMessage("The last online order check time was successfully updated at " +
//                            Objects.toString(new Date().toString(), "NULL"), PHT_LOG, Logger.LEVEL.DEBUG);
//                }
//
//            }

            // call PeripheralsDataManager.kpCheck on the server through XML RPC
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            String methodName = KitchenPrinterDataManagerMethod.KP_CHECK.getMethodName(); //"PeripheralsDataManager.kpCheck";
            Object[] methodParams = new Object[]{PeripheralsDataManager.getPHHostname(), PeripheralsDataManager.getPHRevCenters(), "KitchenPrinter.log"};
            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName,
                    methodParams);

            // get any transactions from the XML RPC response so they can be printed
            ArrayList printData = XmlRpcUtil.getInstance().parseArrayListXmlRpcResponse(xmlRpcResponse, methodName);

            Logger.logMessage(String.format("The printer host running on the terminal %s has received %s online orders from " +
                    "the server at %s in PrinterHostThread.checkServerForOnlineOrders",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString((!DataFunctions.isEmptyCollection(printData) ? printData.size() : "no"), "NULL"),
                    Objects.toString(LocalDateTime.now(), "NULL")), PHT_LOG, Logger.LEVEL.TRACE);


            if (DataFunctions.isEmptyCollection(printData)) {
                Logger.logMessage("No print data retrieved from the server in PrinterHostThread.checkServerForOnlineOrders, the print data is either null or empty!", PHT_LOG, Logger.LEVEL.ERROR);
                return;
            }

            if ((printData != null) && (!printData.isEmpty())) {
                JDCConnection conn = null;
                try {
                    conn = kitchenPrinterTerminal.getDataManager().pool.getConnection(false, PHT_LOG);
                }
                catch (SQLException qe) {
                    Logger.logMessage("PrinterHostThread.checkServerForOnlineOrders no connection to DB", PHT_LOG, Logger.LEVEL.ERROR);
                    Logger.logException(qe, PHT_LOG);
                }

                try {
                    conn.setAutoCommit(false);

                    for (Object transaction : printData) {
                        // each transaction will get a new record inserted into the local database
                        int printerQueueID = -1;
                        ArrayList transLineItemsObj = (ArrayList) transaction;
                        if (DataFunctions.isEmptyCollection(transLineItemsObj)) {
                            Logger.logMessage("No print line details were found within the transaction in PrinterHostThread.checkServerForOnlineOrders!", PHT_LOG, Logger.LEVEL.ERROR);
                            continue;
                        }

                        Logger.logMessage("BEGIN TRANSACTION IN PRINT DATA", PHT_LOG, Logger.LEVEL.TRACE);
                        for (Object printLine : (ArrayList) transaction) {
                            HashMap printLineHM = (HashMap) printLine;
                            Logger.logMessage("LINE ITEM IN PRINT DATA => "+Collections.singletonList(printLineHM), PHT_LOG, Logger.LEVEL.TRACE);
                        }

                        for (Object printLine : (ArrayList) transaction) {
                            HashMap printLineHM = (HashMap) printLine;

                            Logger.logMessage("CURRENT LINE ITEM IN PRINT DATA => "+Collections.singletonList(printLineHM), PHT_LOG, Logger.LEVEL.TRACE);

                            // check if is this print line a detail
                            if (((ArrayList) transaction).indexOf(printLine) == 0) {
                                String estimatedOrderTime = null;
                                if(printLineHM.containsKey("ESTIMATEDORDERTIME") && printLineHM.get("ESTIMATEDORDERTIME") != null && !printLineHM.get("ESTIMATEDORDERTIME").toString().isEmpty())
                                    estimatedOrderTime = printLineHM.get("ESTIMATEDORDERTIME").toString();
                                // insert the header in the local database, the ID of the newly inserted QC_PAPrinterQueue
                                // table record is returned from the query (into printerQueueID) and used to add the details
                                // to the QC_PAPrinterQueueDetail table, in other words PAY ATTENTION TO SCOPE!!!
                                printerQueueID = kitchenPrinterTerminal.getDataManager().parameterizedExecuteNonQuery(
                                        "data.newKitchenPrinter.InsertPAPrinterQueueHeader", new Object[]{
                                                printLineHM.get("PATRANSACTIONID"), printLineHM.get("TRANSACTIONDATE"),
                                                printLineHM.get("TERMINALID"), printLineHM.get("ORDERTYPEID"),
                                                printLineHM.get("PERSONNAME"), printLineHM.get("PHONENUMBER"),
                                                printLineHM.get("TRANSCOMMENT"), printLineHM.get("PICKUPDELIVERYNOTE"),
                                                printLineHM.get("ISONLINEORDER"), estimatedOrderTime, printLineHM.get("ORDERNUM"),
                                                printLineHM.get("TRANSNAME"), printLineHM.get("TRANSNAMELABEL"),
                                                printLineHM.get("TRANSTYPEID"), printLineHM.get("PREVORDERNUMSFORKDS")}, PHT_LOG, conn);
                                if (printerQueueID < 0) {
                                    Logger.logMessage("Unable to insert the header into the QC_PAPrinterQueue table of " +
                                            "the local database in PrinterHostThread.checkServerForOnlineOrders", PHT_LOG,
                                            Logger.LEVEL.ERROR);
                                }
                                else {
                                    Logger.logMessage(String.format("Added print queue ID %s into the database in PrinterHostThread.checkServerForOnlineOrders",
                                            Objects.toString(printerQueueID, "N/A")), PHT_LOG, Logger.LEVEL.DEBUG);
                                }
                            }
                            else {
                                // insert the detail into the local database
                                int queryRes = kitchenPrinterTerminal.getDataManager().parameterizedExecuteNonQuery(
                                        "data.newKitchenPrinter.InsertIntoPAPrinterQueueDetail", new Object[]{
                                                printerQueueID, printLineHM.get("PrinterID"), printLineHM.get("PrintStatusID"),
                                                printLineHM.get("PrintControllerID"), printLineHM.get("PAPluID"),
                                                printLineHM.get("Quantity"), printLineHM.get("IsModifier"),
                                                printLineHM.get("LineDetail"), printLineHM.get("HideStation")}, PHT_LOG, conn);
                                if (queryRes < 0) {
                                    Logger.logMessage("Unable to insert the detail into the QC_PAPrinterQueueDetail " +
                                            "table of the local database in PrinterHostThread.checkServerForOnlineOrders",
                                            PHT_LOG, Logger.LEVEL.ERROR);
                                }
                                else {
                                    Logger.logMessage(String.format("Added print queue detail ID %s into the database in PrinterHostThread.checkServerForOnlineOrders",
                                            Objects.toString(queryRes, "N/A")), PHT_LOG, Logger.LEVEL.DEBUG);
                                }
                            }
                        }
                    }

                    conn.commit();
                }
                catch (Exception ex) {
                    Logger.logException(ex, PHT_LOG);

                    try {
                        conn.rollback();
                    }
                    catch (SQLException sqlEx) {
                        Logger.logException(sqlEx, PHT_LOG);
                    }
                }
                finally {
                    try {
                        conn.setAutoCommit(true);
                        kitchenPrinterTerminal.getDataManager().pool.returnConnection(conn, PHT_LOG);
                    }
                    catch (SQLException sqlEx) {
                        Logger.logException(sqlEx, PHT_LOG);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem trying to get any print jobs from online orders that have been " +
                    "placed in PrinterHostThread.checkServerForOnlineOrders", PHT_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Poll the server for any online orders that have been placed and create any print jobs that will need to be
     * printed for the online orders.
     *
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    private void checkServerForOnlineOrderIDs () {

        try {
            // update the LastOnlineOrderCheck time on the server, for the printer host configured as the primary in the
            // back office, if the active printer host was configured as a backup then still have it update the
            // LastOnlineOrderCheck for what was configured as the primary
//            if (StringFunctions.stringHasContent(kitchenPrinterTerminal.getActivePHMacAddress())) {
//                String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
//                String macAddress = PeripheralsDataManager.getPHMacAddress();
//                String hostname = PeripheralsDataManager.getPHHostname();
//                String methodName = "PeripheralsDataManager.updatePHLastOnlineOrderCheck";
//                Object[] methodParams = new Object[]{kitchenPrinterTerminal.getActivePHMacAddress()};
//                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName,
//                        methodParams);
//
//                // get any transactions from the XML RPC response so they can be printed
//                if (XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, methodName)) {
//                    Logger.logMessage("The last online order check time was successfully updated at " +
//                            Objects.toString(new Date().toString(), "NULL"), PHT_LOG, Logger.LEVEL.DEBUG);
//                }
//
//            }


            phServerCheckIn();

            // call PeripheralsDataManager.kpCheck on the server through XML RPC
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            String methodName = KitchenPrinterDataManagerMethod.KP_CHECKIDS.getMethodName(); //"PeripheralsDataManager.kpCheck";
            Object[] methodParams = new Object[]{PeripheralsDataManager.getPHHostname(), PeripheralsDataManager.getPHRevCenters(), "KitchenPrinter.log"};
            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName,
                    methodParams);

            // get any transactions from the XML RPC response so they can be printed
            ArrayList printDataIDs = XmlRpcUtil.getInstance().parseArrayListXmlRpcResponse(xmlRpcResponse, methodName);

            Logger.logMessage(String.format("The printer host running on the terminal %s has received %s online order IDs from " +
                    "the server at %s in PrinterHostThread.checkServerForOnlineOrderIDs",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL"),
                    Objects.toString((!DataFunctions.isEmptyCollection(printDataIDs) ? printDataIDs.size() : "no"), "NULL"),
                    Objects.toString(LocalDateTime.now(), "NULL")), PHT_LOG, Logger.LEVEL.TRACE);


            if (DataFunctions.isEmptyCollection(printDataIDs)) {
                Logger.logMessage("Invalid print data retrieved from the server in PrinterHostThread.checkServerForOnlineOrders, the print data is either null or empty!", PHT_LOG, Logger.LEVEL.ERROR);
                return;
            }
//

            if ((printDataIDs != null) && (!printDataIDs.isEmpty())) {
                for (Object transaction : printDataIDs) {
                    //TODO store a reference to a KMSDM so we dont have to keep making new ones
                    kmsdm.printOrderUsingKMSInOnlineMode(Integer.parseInt(transaction.toString()));
                }

            }
//                JDCConnection conn = null;
//                try {
//                    conn = kitchenPrinterTerminal.getDataManager().pool.getConnection(false, PHT_LOG);
//                }
//                catch (SQLException qe) {
//                    Logger.logMessage("PrinterHostThread.checkServerForOnlineOrders no connection to DB", PHT_LOG, Logger.LEVEL.ERROR);
//                    Logger.logException(qe, PHT_LOG);
//                }
//
//                try {
//                    conn.setAutoCommit(false);
//
//                    for (Object transaction : printData) {
//                        // each transaction will get a new record inserted into the local database
//                        int printerQueueID = -1;
//                        ArrayList transLineItemsObj = (ArrayList) transaction;
//                        if (DataFunctions.isEmptyCollection(transLineItemsObj)) {
//                            Logger.logMessage("No print line details were found within the transaction int PrinterHostThread.checkServerForOnlineOrders!", PHT_LOG, Logger.LEVEL.ERROR);
//                            return;
//                        }
//
//                        Logger.logMessage("BEGIN TRANSACTION IN PRINT DATA", PHT_LOG, Logger.LEVEL.TRACE);
//                        for (Object printLine : (ArrayList) transaction) {
//                            HashMap printLineHM = (HashMap) printLine;
//                            Logger.logMessage("LINE ITEM IN PRINT DATA => "+Collections.singletonList(printLineHM), PHT_LOG, Logger.LEVEL.TRACE);
//                        }
//
//                        for (Object printLine : (ArrayList) transaction) {
//                            HashMap printLineHM = (HashMap) printLine;
//
//                            Logger.logMessage("LINE ITEM IN PRINT DATA => "+Collections.singletonList(printLineHM), PHT_LOG, Logger.LEVEL.TRACE);
//
//                            // check if is this print line a detail
//                            if (((ArrayList) transaction).indexOf(printLine) == 0) {
//                                String estimatedOrderTime = !printLineHM.get("ESTIMATEDORDERTIME").toString().isEmpty()
//                                        ? printLineHM.get("ESTIMATEDORDERTIME").toString()
//                                        : null;
//                                // insert the header in the local database, the ID of the newly inserted QC_PAPrinterQueue
//                                // table record is returned from the query (into printerQueueID) and used to add the details
//                                // to the QC_PAPrinterQueueDetail table, in other words PAY ATTENTION TO SCOPE!!!
//                                printerQueueID = kitchenPrinterTerminal.getDataManager().parameterizedExecuteNonQuery(
//                                        "data.newKitchenPrinter.InsertPAPrinterQueueHeader", new Object[]{
//                                        printLineHM.get("PATRANSACTIONID"), printLineHM.get("TRANSACTIONDATE"),
//                                        printLineHM.get("TERMINALID"), printLineHM.get("ORDERTYPEID"),
//                                        printLineHM.get("PERSONNAME"), printLineHM.get("PHONENUMBER"),
//                                        printLineHM.get("TRANSCOMMENT"), printLineHM.get("PICKUPDELIVERYNOTE"),
//                                        printLineHM.get("ISONLINEORDER"), estimatedOrderTime, printLineHM.get("ORDERNUM"),
//                                        printLineHM.get("TRANSNAME"), printLineHM.get("TRANSNAMELABEL"),
//                                        printLineHM.get("TRANSTYPEID"), printLineHM.get("PREVORDERNUMSFORKDS")}, PHT_LOG, conn);
//                                if (printerQueueID < 0) {
//                                    Logger.logMessage("Unable to insert the header into the QC_PAPrinterQueue table of " +
//                                            "the local database in PrinterHostThread.checkServerForOnlineOrders", PHT_LOG,
//                                            Logger.LEVEL.ERROR);
//                                }
//                                else {
//                                    Logger.logMessage(String.format("Added print queue ID %s into the database in PrinterHostThread.checkServerForOnlineOrders",
//                                            Objects.toString(printerQueueID, "N/A")), PHT_LOG, Logger.LEVEL.DEBUG);
//                                }
//                            }
//                            else {
//                                // insert the detail into the local database
//                                int queryRes = kitchenPrinterTerminal.getDataManager().parameterizedExecuteNonQuery(
//                                        "data.newKitchenPrinter.InsertIntoPAPrinterQueueDetail", new Object[]{
//                                        printerQueueID, printLineHM.get("PrinterID"), printLineHM.get("PrintStatusID"),
//                                        printLineHM.get("PrintControllerID"), printLineHM.get("PAPluID"),
//                                        printLineHM.get("Quantity"), printLineHM.get("IsModifier"),
//                                        printLineHM.get("LineDetail"), printLineHM.get("HideStation")}, PHT_LOG, conn);
//                                if (queryRes < 0) {
//                                    Logger.logMessage("Unable to insert the detail into the QC_PAPrinterQueueDetail " +
//                                            "table of the local database in PrinterHostThread.checkServerForOnlineOrders",
//                                            PHT_LOG, Logger.LEVEL.ERROR);
//                                }
//                                else {
//                                    Logger.logMessage(String.format("Added print queue detail ID %s into the database in PrinterHostThread.checkServerForOnlineOrders",
//                                            Objects.toString(queryRes, "N/A")), PHT_LOG, Logger.LEVEL.DEBUG);
//                                }
//                            }
//                        }
//                    }
//
//                    conn.commit();
//                }
//                catch (Exception ex) {
//                    Logger.logException(ex, PHT_LOG);
//
//                    try {
//                        conn.rollback();
//                    }
//                    catch (SQLException sqlEx) {
//                        Logger.logException(sqlEx, PHT_LOG);
//                    }
//                }
//                finally {
//                    try {
//                        conn.setAutoCommit(true);
//                        kitchenPrinterTerminal.getDataManager().pool.returnConnection(conn, PHT_LOG);
//                    }
//                    catch (SQLException sqlEx) {
//                        Logger.logException(sqlEx, PHT_LOG);
//                    }
//                }
//            }
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem trying to get any print jobs from online orders that have been " +
                    "placed in PrinterHostThread.checkServerForOnlineOrderIDs", PHT_LOG, Logger.LEVEL.ERROR);
        }
    }

    /**
     * <p>Makes an XML RPC call to the server to indicate that the printer host is still running and has check in.</p>
     *
     */
    private void phServerCheckIn () {

        try {
            Logger.logMessage("About to do a printer host server check in", PHT_LOG, Logger.LEVEL.DEBUG);

            String serverURL= MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            String methodName = "PeripheralsDataManager.updatePHLastOnlineOrderCheck";
            String activePHMacAddress = kitchenPrinterTerminal.getActivePHMacAddress();

            // only attempt a server check in if the active printer host mac address has been set -
            // it may not have been when first starting up
            if (StringFunctions.stringHasContent(activePHMacAddress)) {

                Logger.logMessage("Active PH Mac address found to be: " + activePHMacAddress, PHT_LOG, Logger.LEVEL.DEBUG);

                Object[] methodParams = new Object[]{activePHMacAddress};
                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(serverURL, macAddress, hostname, methodName,
                        methodParams);

                if (XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, methodName)) {
                    Logger.logMessage("The last online order check time was successfully updated at " +
                            Objects.toString(new Date().toString(), "NULL"), PHT_LOG, Logger.LEVEL.DEBUG);
                }
            }
            else {
                Logger.logMessage("Active PH Mac address not found, no update being made", PHT_LOG, Logger.LEVEL.DEBUG);
            }

        }
        catch (Exception ex) {
            Logger.logMessage("Error checking in in PrinterHostThread.phServerCheckIn", PHT_LOG, Logger.LEVEL.ERROR);
            Logger.logException(ex, PHT_LOG);
        }
    }

    /**
     * <p>Removes print queue details from the print status map that have expired.</p>
     *
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private void removeExpiredPrintQueueDetailsFromPrintStatusMap () {

        // remove old completed print jobs from the print job status map that have expired
        ConcurrentHashMap<Integer, PrintStatus> printStatusMap = printJobStatusHandler.getPrintStatusMap();
        if (!DataFunctions.isEmptyMap(printStatusMap)) {
            // determine which print queue details should be removed from the print status map
            ArrayList<Integer> printQueueDetailsToRemove = new ArrayList<>();
            for (Map.Entry<Integer, PrintStatus> printStatusMapEntry : printStatusMap.entrySet()) {
                int printerQueueDetailID = printStatusMapEntry.getKey();
                PrintStatus printStatus = printStatusMapEntry.getValue();
                LocalDateTime completedTime = null;
                if (printStatus != null) {
                    completedTime = printStatus.getUpdatedDTM();
                }
                if (completedTime != null) {
                    LocalDateTime currentTime = LocalDateTime.now();
                    // check if it has expired
                    if (completedTime.plusMinutes(KitchenPrinterTerminal.getInstance().getPrintJobExpirationInMinutes()).isBefore(currentTime)) {
                        printQueueDetailsToRemove.add(printerQueueDetailID);
                    }
                }
            }

            // remove print queue details that should be removed from the print status map
            if (!DataFunctions.isEmptyCollection(printQueueDetailsToRemove)) {
                for (int printQueueDetailToRemove : printQueueDetailsToRemove) {
                    Logger.logMessage(String.format("Removing the print queue detail with an ID of %s from the print status map in PrinterHostThread.removeExpiredPrintQueueDetailsFromPrintStatusMap",
                            Objects.toString(printQueueDetailsToRemove, "N/A")), PHT_LOG, Logger.LEVEL.DEBUG);
                    printJobStatusHandler.removePrintQueueDetailFromPrintStatusMap(printQueueDetailToRemove);
                }
            }
        }

    }

    /**
     * Get the KDS directories within the revenue centers associated with this printer host.
     *
     * @return {@link HashMap<Long, String>} The directories within the given revenue centers.
     */
    @SuppressWarnings("unchecked")
    public HashMap<Long, String> getKDSDirectories () {
        HashMap<Long, String> kdsDirectories = new HashMap<>();

        try {
            // get the revenue centers associated with this printer host
            String revCenterIDs = PeripheralsDataManager.getPHRevCenters();
            int[] revCenterIDsIntArr = null;
            if (StringFunctions.stringHasContent(revCenterIDs)) {
                String[] revCenterIDsStrArr = revCenterIDs.split("\\s*,\\s*", -1);
                if (revCenterIDsStrArr.length > 0) {
                    revCenterIDsIntArr = new int[revCenterIDsStrArr.length];
                    for (int i = 0; i < revCenterIDsStrArr.length; i++) {
                        revCenterIDsIntArr[i] = (NumberUtils.isNumber(revCenterIDsStrArr[i]) ? Integer.parseInt(revCenterIDsStrArr[i]) : 0);
                    }
                }
            }
            if ((revCenterIDsIntArr != null) && (revCenterIDsIntArr.length > 0)) {
                ArrayList<HashMap> directories = kitchenPrinterTerminal.getDataManager().parameterizedExecuteQuery(
                        "data.newKitchenPrinter.GetKDSFolderNames", new Object[]{DataFunctions.convertIntArrToStrForSQL(revCenterIDsIntArr)}, PHT_LOG, true);
                if ((directories != null) && (!directories.isEmpty())) {
                    directories.forEach((directory) -> {
                        Long terminalID = (Long) directory.get("TERMINALID");
                        String kdsDirectory = directory.get("KDSFOLDERNAME").toString();
                        if ((terminalID != null) && (terminalID > 0)
                                && (kdsDirectory != null) && (!kdsDirectory.isEmpty())) {
                            kdsDirectories.put(terminalID, kdsDirectory);
                        }
                    });
                }
            }

            if (kdsDirectories.isEmpty()) {
                Logger.logMessage("No KDS directories have been found within the revenue centers associated with this " +
                        "printer host in PrintControllerThread.getKDSDirectories", PHT_LOG, Logger.LEVEL.DEBUG);
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem trying to get the KDS directories within the revenue centers " +
                    "associated with the printer host in PrinterHostThread.getKDSDirectories", PHT_LOG,
                    Logger.LEVEL.ERROR);
        }

        return kdsDirectories;
    }

    /**
     * Determines whether or not the printerHostThread is running.
     *
     * @return Whether or not the printerHostThread is running.
     */
    public boolean isActive () {
        boolean isActive = false;

        try {
            if ((!isShuttingDown) && (!isTerminated)) {
                isActive = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem trying to determine whether or not the printerHostThread " +
                    "is running in PrinterHostThread.isActive", PHT_LOG, Logger.LEVEL.ERROR);
        }

        return isActive;
    }

    /**
     * Creates and starts a Thread using the PrinterHostThread Runnable.
     *
     */
    public void startPrinterHostThread () {

        try {
            // reset isShuttingDown and isTerminated
            System.out.println("Starting Printer Host Thread");
            isShuttingDown = false;
            isTerminated = false;

            // set status for this printer host to becoming active
            kitchenPrinterTerminal.addToPrinterHostStatusTracker(PeripheralsDataManager.getTerminalMacAddress(),
                    PrinterHostStatus.BECOMING_ACTIVE);

            Thread printerHostThread = new Thread(this);
            printerHostThread.start();
            Logger.logMessage("The PrinterHostThread has started successfully in " +
                    "PrinterHostThread.startPrinterHostThread", PHT_LOG, Logger.LEVEL.TRACE);

            // set status for this printer host to active
            kitchenPrinterTerminal.addToPrinterHostStatusTracker(PeripheralsDataManager.getTerminalMacAddress(),
                    PrinterHostStatus.ACTIVE);
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem trying to start the PrinterHostThread in " +
                    "PrinterHostThread.startPrinterHostThread", PHT_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Stops the PrinterHostThread.
     *
     */
    @SuppressWarnings("MagicNumber")
    public void stopPrinterHostThread () {

        try {
            System.out.println("Shutting down the Printer Host Thread");

            // run through Thread shutdown routine
            isShuttingDown = true;

            // set status for this printer host to shutting down
            kitchenPrinterTerminal.addToPrinterHostStatusTracker(PeripheralsDataManager.getTerminalMacAddress(),
                    PrinterHostStatus.SHUTTING_DOWN);



            int waitCount = 0;
            while (!isTerminated) {
                waitCount++;
                System.out.println("Shutting down the Printer Host Thread wait: " + waitCount);
                // wait a second
                Thread.sleep(1000L);
                if (waitCount >= 3) {
                    System.out.println("Shutting down the Printer Host Thread wait time exceeded");
                    Logger.logMessage("The PrinterHostThread didn't stop within the specified wait time, it " +
                            "is being interrupted now in PrinterHostThread.stopPrinterHostThread", PHT_LOG,
                            Logger.LEVEL.TRACE);
                    // interrupt the PrinterQueueHandler Thread
                    Thread.getAllStackTraces().keySet().forEach((thread) -> {
                        if (thread.getName().equalsIgnoreCase("KitchenPrinter-PrinterHostThread")) {
                            System.out.println("Printer Host Thread interrupted");
                            thread.interrupt();
                        }
                    });
                }
            }

            Logger.logMessage("The PrinterHostThread has been stopped", PHT_LOG, Logger.LEVEL.IMPORTANT);

            // set status for this printer host to inactive
            kitchenPrinterTerminal.addToPrinterHostStatusTracker(PeripheralsDataManager.getTerminalMacAddress(),
                    PrinterHostStatus.INACTIVE);
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem trying to stop the PrinterHostThread in " +
                    "PrinterHostThread.stopPrinterHostThread", PHT_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Called from the PeripheralsDataManager to start the printerHostThread and printerQueueHandlerThread running on
     * this WAR.
     *
     */
    public void startPrinterHost () {

        try {
            // start the printerHostThread
            startPrinterHostThread();

            // start the printerQueueHandlerThread
            printQueueHandler.startPrinterQueueHandlerThread();
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem trying to start the printerHostThread for the printer host with " +
                    "hostname "+Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")+" in " +
                    "PrinterHostThread.startPrinterHost", PHT_LOG);
        }

    }

    /**
     * Called from the PeripheralsDataManager to stop the printerHostThread and printerQueueHandlerThread running on
     * this WAR.
     *
     */
    public void stopPrinterHost () {

        try {
            Logger.logMessage("Stopping printer host.", PHT_LOG, Logger.LEVEL.DEBUG);
            // stop the printerQueueHandlerThread
            Logger.logMessage("Stopping printer queue handler.", PHT_LOG, Logger.LEVEL.DEBUG);
            printQueueHandler.stopPrinterQueueHandlerThread();

            // start the printerHostThread
            System.out.println("Stopping printer host thread.");
            stopPrinterHostThread();
        }
        catch (Exception e) {
            Logger.logException(e, PHT_LOG);
            Logger.logMessage("There was a problem trying to stop the printerHostThread for the printer host with " +
                    "hostname "+Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")+" in " +
                    "PrinterHostThread.startPrinterHost", PHT_LOG);
        }

    }
}
