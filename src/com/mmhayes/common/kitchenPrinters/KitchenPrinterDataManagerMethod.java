package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-07-08 14:31:34 -0400 (Thu, 08 Jul 2021) $: Date of last commit
    $Rev: 14306 $: Revision of last commit
    Notes: Enum to hold the names of methods available in the KitchenPrinterDataManager.
*/

import com.mmhayes.common.kitchenapi.KitchenPrinterDataManager;

/**
 * <p>Enum to hold the names of methods available in the {@link KitchenPrinterDataManager}.</p>
 *
 */
public enum KitchenPrinterDataManagerMethod {

    START_PRINTER_HOST_HEALTH_CHECK("KitchenPrinterDataManager.startPrinterHostHealthCheck"),
    STOP_PRINTER_HOST_HEALTH_CHECK("KitchenPrinterDataManager.stopPrinterHostHealthCheck"),
    ADD_PRINTER_HOST_TO_BACKOFFICE_UPDATER("KitchenPrinterDataManager.addPrinterHostToBackofficeUpdaterPrinterHosts"),
    REMOVE_PRINTER_HOST_FROM_BACKOFFICE_UPDATER("KitchenPrinterDataManager.removePrinterHostFromBackofficeUpdaterPrinterHosts"),
    ADD_PRINTER_TO_BACKOFFICE_UPDATER("KitchenPrinterDataManager.addPrinterToBackofficeUpdaterPrinters"),
    REMOVE_PRINTER_FROM_BACKOFFICE_UPDATER("KitchenPrinterDataManager.removePrinterFromBackofficeUpdaterPrinters"),
    START_PRINTER_HOST("KitchenPrinterDataManager.startPrinterHost"),
    STOP_PRINTER_HOST("KitchenPrinterDataManager.stopPrinterHost"),
    IS_PRINTER_HOST_IN_SERVICE("KitchenPrinterDataManager.isPrinterHostInService"),
    GET_MAC_ADDRESS_OF_IN_SERVICE_PRINTER_HOST("KitchenPrinterDataManager.getMacAddressOfInServicePrinterHost"),
    GET_TIME_IN_SERVICE_PRINTER_HOST_BECAME_IN_SERVICE("KitchenPrinterDataManager.getTimeInServicePrinterHostBecameInService"),
    SET_MAC_ADDRESS_OF_IN_SERVICE_PRINTER_HOST("KitchenPrinterDataManager.setMacAddressOfInServicePrinterHost"),
    SET_TIME_IN_SERVICE_PRINTER_HOST_BECAME_IN_SERVICE("KitchenPrinterDataManager.setTimeInServicePrinterHostBecameInService"),
    GET_PRINTER_HOST_STATUS("KitchenPrinterDataManager.getPrinterHostStatus"),
    UPDATE_IN_SERVICE_PRINTER_HOST_ON_SERVER("KitchenPrinterDataManager.updateInServicePrinterHostOnServer"),
    UPDATE_PRINTERS("KitchenPrinterDataManager.updatePrinters"),
    CHECK_FOR_PRINTER_HOST_WITH_PRINTER_CONNECTION("KitchenPrinterDataManager.isPrinterHostHoldingPrinterConnectionOnTerminal"),
    CHECK_PRINTER_HOST_CONNECTION_TO_PRINTER_RELEASED("KitchenPrinterDataManager.isPrinterHostConnectionToPrinterReleasedOnTerminal"),
    KP_CHECK("KitchenPrinterDataManager.kpCheck"),
    KP_CHECKIDS("KitchenPrinterDataManager.kpCheckIDs"),
    GET_IN_SERVICE_PRINTER_HOST("KitchenPrinterDataManager.getInServicePrinterHostOnServer"),
    GET_IN_SERVICE_PRINTER_HOST_ON_TERMINAL("KitchenPrinterDataManager.getInServicePrinterHostOnTerminal"),
    IS_KDS_PRINT_JOB_FAILED("KitchenPrinterDataManager.isKDSPrintJobFailed"),
    SEND_EXPIRED_KDS_XML_EMAIL("KitchenPrinterDataManager.sendEmailForExpiredKDSXML"),
    GET_CASHIER_NAME("KitchenPrinterDataManager.getCashierName"),
    UPDATE_PRINT_JOB_STATUS("KitchenPrinterDataManager.updatePrintJobStatus");

    // private member variables of a KitchenPrinterDataManagerMethod
    private String methodName;

    /**
     * <p>Create a {@link KitchenPrinterDataManagerMethod} and set it's methodName.</p>
     *
     * @param methodName The name {@link String} of the method to invoke in the {@link KitchenPrinterDataManager}
     */
    KitchenPrinterDataManagerMethod (String methodName) {
        this.methodName = methodName;
    }

    /**
     * <p>Getter for the methodName field of the {@link KitchenPrinterDataManagerMethod}.</p>
     *
     * @return The methodName field of the {@link KitchenPrinterDataManagerMethod}.
     */
    public String getMethodName () {
        return methodName;
    }

}