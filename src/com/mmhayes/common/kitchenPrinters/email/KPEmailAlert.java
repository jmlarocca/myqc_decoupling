package com.mmhayes.common.kitchenPrinters.email;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-07-28 09:57:23 -0400 (Tue, 28 Jul 2020) $: Date of last commit
    $Rev: 12259 $: Revision of last commit
    Notes: Methods for sending an email to alert people that a print job failed to print.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.KitchenDisplaySystemJobInfo;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.*;
import org.apache.commons.lang.math.NumberUtils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Objects;

/**
 * <p>Methods for sending an email to alert people that a print job failed to print.</p>
 *
 */
public class KPEmailAlert {

    /**
     * <p>Gets the email addresses within the revenue center that should get emails.</p>
     *
     * @param revenueCenterID The ID of the revenue center in which the transaction took place.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return An {@link ArrayList} of {@link String} corresponding to email addresses within the revenue center.
     */
    private static ArrayList<String> getEmailListForRC (int revenueCenterID, DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.getEmailListForRC, unable to get the email list for the revenue center!", Logger.LEVEL.ERROR);
            return null;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.getEmailListForRC can't be null, unable to get the email list for the revenue center!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KPEmailAlert.getEmailListForRC must be greater than 0, unable to get the email list for the revenue center!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        ArrayList<String> emails = new ArrayList<>();
        DynamicSQL sql = new DynamicSQL("data.newKitchenPrinter.GetKPContactEmailByRevenueCenterID").addIDList(1, revenueCenterID);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1)) {
            String emailAddressesStr = HashMapDataFns.getStringVal(queryRes.get(0), "KPCONTACTEMAIL");
            if (StringFunctions.stringHasContent(emailAddressesStr)) {
                String[] emailAddressesArr = emailAddressesStr.split("\\s*,\\s*", -1);
                if (!DataFunctions.isEmptyGenericArr(emailAddressesArr)) {
                    for (String email : emailAddressesArr) {
                        emails.add(email);
                    }
                }
            }
        }

        return emails;
    }

    /**
     * <p>Queries the database to get the back office email configuration for the revenue center.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return A {@link HashMap} containing the back office email configuration information.
     */
    private static HashMap getEmailConfig (DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.getEmailConfig, unable to get the back office email configuration!", Logger.LEVEL.ERROR);
            return null;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.getEmailConfig can't be null, unable to get the back office email configuration!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.ReceiptGen.EmailCfg");

        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1)) {
            return queryRes.get(0);
        }

        return null;
    }

    /**
     * <p>Queries the database to get the information necessary to build the email subject line.</p>
     *
     * @param paPrinterQueueID The ID of the printer queue record that failed.
     * @param printerID The ID of the printer that failed to print.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return A {@link HashMap} containing the information necessary to build the email subject line.
     */
    private static HashMap getEmailSubjectInfo (int paPrinterQueueID, int printerID, DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.getEmailSubjectInfo, unable to get the email subject information!", Logger.LEVEL.ERROR);
            return null;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.getEmailSubjectInfo can't be null, unable to get the email subject information!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (paPrinterQueueID <= 0) {
            Logger.logMessage("The printer queue ID passed to KPEmailAlert.getEmailSubjectInfo must be greater than 0, unable to get the email subject information!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KPEmailAlert.getEmailSubjectInfo must be greater than 0, unable to get the email subject information!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetEmailAlertSubjectInfo").addIDList(1, paPrinterQueueID).addIDList(2, printerID);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
            return queryRes.get(0);
        }

        return null;
    }

    /**
     * <p>Builds the subject line for the email alert.</p>
     *
     * @param paPrinterQueueID The ID of the printer queue record that failed.
     * @param printerID The ID of the printer that failed to print.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return The subject line {@link String} of the email alert.
     */
    private static String buildEmailSubject (int paPrinterQueueID, int printerID, DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.buildEmailSubject, returning a default email subject line!", Logger.LEVEL.ERROR);
            return "Order Failed to Print";
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.buildEmailSubject can't be null, returning a default email subject line!", log, Logger.LEVEL.ERROR);
            return "Order Failed to Print";
        }

        if (paPrinterQueueID <= 0) {
            Logger.logMessage("The printer queue ID passed to KPEmailAlert.buildEmailSubject must be greater than 0, returning a default email subject line!", log, Logger.LEVEL.ERROR);
            return "Order Failed to Print";
        }

        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KPEmailAlert.buildEmailSubject must be greater than 0, returning a default email subject line!", log, Logger.LEVEL.ERROR);
            return "Order Failed to Print";
        }

        HashMap emailSubjectInfo = getEmailSubjectInfo(paPrinterQueueID, printerID, dataManager, log);
        if (DataFunctions.isEmptyMap(emailSubjectInfo)) {
            Logger.logMessage("Failed to get the subject line information for the email alert in KPEmailAlert.buildEmailSubject, returning a default email subject line!", log, Logger.LEVEL.ERROR);
            return "Order Failed to Print";
        }

        // extract the subject line information and use it to build the subject line for the email
        String orderNumber = HashMapDataFns.getStringVal(emailSubjectInfo, "ORDERNUM");
        String printerName = HashMapDataFns.getStringVal(emailSubjectInfo, "NAME");
        boolean isKDS = (HashMapDataFns.getIntVal(emailSubjectInfo, "PRINTERHARDWARETYPEID") == 2);

        // get the name of the company
        String companyName = "";
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetCompanyName");
        Object queryRes = sql.getSingleField(dataManager);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString()))) {
            companyName = queryRes.toString();
        }

        return String.format("Failed to %s the Order: %s on the %s: %s%s",
                Objects.toString((isKDS ? "Display" : "Print"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(orderNumber) ? orderNumber : "N/A"), "N/A"),
                Objects.toString((isKDS ? "KDS Station" : "Printer"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(printerName) ? printerName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(companyName) ? " at " + companyName : ""), "N/A"));
    }

    /**
     * <p>Queries the database to get the printer queue record of the failed print job.</p>
     *
     * @param paPrinterQueueID The ID of the printer queue record that failed.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return A {@link HashMap} containing the printer queue record of the failed print job.
     */
    private static HashMap getFailedPrintJobHeaderInfo (int paPrinterQueueID, DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.getFailedPrintJobHeaderInfo, unable to get the printer queue record for the failed print job!", Logger.LEVEL.ERROR);
            return null;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.getFailedPrintJobHeaderInfo can't be null, unable to get the printer queue record for the failed print job!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (paPrinterQueueID <= 0) {
            Logger.logMessage("The printer queue ID passed to KPEmailAlert.getFailedPrintJobHeaderInfo must be greater than 0, unable to get the printer queue record for the failed print job!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetFailedPrintJobHeaderInfo").addIDList(1, paPrinterQueueID);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
            return queryRes.get(0);
        }

        return null;
    }

    /**
     * <p>Builds the message that will be in the email's body.</p>
     *
     * @param paPrinterQueueID The ID of the printer queue record that failed.
     * @param failedPrintJobDetails An {@link ArrayList} of {@link HashMap} corresponding to the print job details that failed to print.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return The message {@link String} that will be in the email's body.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private static String buildEmailMessage (int paPrinterQueueID, ArrayList<HashMap> failedPrintJobDetails, DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.buildEmailMessage, unable to build the email message!", Logger.LEVEL.ERROR);
            return null;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.buildEmailMessage can't be null, unable to build the email message!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (paPrinterQueueID <= 0) {
            Logger.logMessage("The printer queue ID passed to KPEmailAlert.buildEmailMessage must be greater than 0, unable to build the email message!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyCollection(failedPrintJobDetails)) {
            Logger.logMessage("No failed print job details have been passed to KPEmailAlert.buildEmailMessage, unable to build the email message!", log, Logger.LEVEL.ERROR);
            return null;
        }

        HashMap failedPrintJobHeaderInfo = getFailedPrintJobHeaderInfo(paPrinterQueueID, dataManager, log);
        if (DataFunctions.isEmptyMap(failedPrintJobHeaderInfo)) {
            Logger.logMessage("Failed to get printer queue record for the failed print job in KPEmailAlert.buildEmailMessage, unable to build the email message!", log, Logger.LEVEL.ERROR);
            return null;
        }

        KPEmailAlertMessageBuilder emailMessageBuilder = new KPEmailAlertMessageBuilder();

        int paOrderTypeID = HashMapDataFns.getIntVal(failedPrintJobHeaderInfo, "PAORDERTYPEID");
        emailMessageBuilder = emailMessageBuilder.addPAOrderTypeID(paOrderTypeID);

        int printerID = HashMapDataFns.getIntVal(failedPrintJobDetails.get(0), "PRINTERID");
        boolean isKDS = (getPrinterHardwareTypeFromID(printerID, dataManager, log) == TypeData.PrinterHardwareType.KDS);
        emailMessageBuilder = emailMessageBuilder.addIsKDS(isKDS);

        // get the transaction date and add it to the receipt
        LocalDateTime transactionDate = new Timestamp(HashMapDataFns.getDateVal(failedPrintJobHeaderInfo, "TRANSACTIONDATE").getTime()).toLocalDateTime();
        emailMessageBuilder = emailMessageBuilder.addTransactionDate(transactionDate);

        // get the transaction ID and add it to the receipt
        int paTransactionID = HashMapDataFns.getIntVal(failedPrintJobHeaderInfo, "PATRANSACTIONID");
        emailMessageBuilder = emailMessageBuilder.addPATransactionID(paTransactionID);

        // get the terminal ID and add it to the receipt
        int terminalID = HashMapDataFns.getIntVal(failedPrintJobHeaderInfo, "TERMINALID");
        emailMessageBuilder = emailMessageBuilder.addTerminalID(terminalID);

        // get the order number and add it to the receipt
        String orderNUmber = HashMapDataFns.getStringVal(failedPrintJobHeaderInfo, "ORDERNUM");
        emailMessageBuilder = emailMessageBuilder.addOrderNumber(orderNUmber);

        // get the printer name and add it to the receipt
        String printerName = getPrinterNameFromID(printerID, dataManager, log);
        if (StringFunctions.stringHasContent(printerName)) {
            emailMessageBuilder = emailMessageBuilder.addPrinterName(printerName);
        }

        // get the customer name and add it to the receipt
        String customer = HashMapDataFns.getStringVal(failedPrintJobHeaderInfo, "PERSONNAME");
        emailMessageBuilder = emailMessageBuilder.addCustomer(customer);

        // get the comment and add it to the receipt
        String comment = HashMapDataFns.getStringVal(failedPrintJobHeaderInfo, "TRANSCOMMENT");
        emailMessageBuilder = emailMessageBuilder.addComment(comment);

        // get the pickup/delivery note and add it to the receipt
        String pickupDeliveryNote = HashMapDataFns.getStringVal(failedPrintJobHeaderInfo, "PICKUPDELIVERYNOTE");
        emailMessageBuilder = emailMessageBuilder.addPickupDeliveryNote(pickupDeliveryNote);

        // get all the products and their quantity that failed to print on the printer, use a LinkedHashMap to maintain order
        LinkedHashMap<String, Integer> productsAndQuantities = getTransLineItemsInFailedPrintJob(failedPrintJobDetails, dataManager, log);
        emailMessageBuilder = emailMessageBuilder.addTransLineItems(productsAndQuantities);

        // sum the total number of products that failed to print on the printer
        int totalNumberOfProducts = calculateTotalNumberOfProducts(productsAndQuantities, dataManager, log);
        emailMessageBuilder = emailMessageBuilder.addTotalNumberOfProducts(totalNumberOfProducts);

        return emailMessageBuilder.toString();
    }

    /**
     * <p>Queries the database to get the name of the printer with the given ID.</p>
     *
     * @param printerID The ID of the printer to get the name for.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return The name {@link String} of the printer with the given ID.
     */
    private static String getPrinterNameFromID (int printerID, DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.getPrinterNameFromID, unable to get the printer name!", Logger.LEVEL.ERROR);
            return null;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.getPrinterNameFromID can't be null, unable to get the printer name!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KPEmailAlert.getPrinterNameFromID must be greater than 0, unable to get the printer name!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetPrinterNameFromID").addIDList(1, printerID);
        Object queryRes = sql.getSingleField(dataManager);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString()))) {
            return queryRes.toString();
        }

        return null;
    }

    /**
     * <p>Queries the database to get the hardware type of the printer with the given ID.</p>
     *
     * @param printerID The ID of the printer to get the hardware type for.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return The hardware type ID of the printer with the given ID.
     */
    private static int getPrinterHardwareTypeFromID (int printerID, DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.getPrinterHardwareTypeFromID, unable to get the printer hardware type!", Logger.LEVEL.ERROR);
            return -1;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.getPrinterHardwareTypeFromID can't be null, unable to get the printer hardware type!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KPEmailAlert.getPrinterHardwareTypeFromID must be greater than 0, unable to get the printer hardware type!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetPrinterHardwareTypeFromID").addIDList(1, printerID);
        Object queryRes = sql.getSingleField(dataManager);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString()))) {
            return Integer.parseInt(queryRes.toString());
        }

        return -1;
    }

    /**
     * <p>Gets the products and their quantities that failed to print on the printer.</p>
     *
     * @param failedPrintJobDetails An {@link ArrayList} of {@link HashMap} corresponding to the print job details that failed to print.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return A {@link LinkedHashMap} whose key is the {@link String} product name and whose value of the {@link Integer} quantity
     * of the product within the order that failed to print on the printer.
     */
    private static LinkedHashMap<String, Integer> getTransLineItemsInFailedPrintJob (ArrayList<HashMap> failedPrintJobDetails, DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.getTransLineItemsInFailedPrintJob, unable to determine the transaction line items that failed to print on the printer!", Logger.LEVEL.ERROR);
            return null;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.getTransLineItemsInFailedPrintJob can't be null, unable to determine the transaction line items that failed to print on the printer!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyCollection(failedPrintJobDetails)) {
            Logger.logMessage("No products were passed to KPEmailAlert.getTransLineItemsInFailedPrintJob, unable to determine the transaction line items that failed to print on the printer!", log, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<MMHTree<HashMap>> organizedTransLineItems = organizeTransLineItemsInFailedPrintJob(failedPrintJobDetails, dataManager, log);
        if (DataFunctions.isEmptyCollection(organizedTransLineItems)) {
            Logger.logMessage("No transaction line items were found within the failed print job in KPEmailAlert.getTransLineItemsInFailedPrintJob after organization!", log, Logger.LEVEL.ERROR);
        }
        else {
            // get the names of the products and modifiers within the transaction line items
            getTransLineItemNames(organizedTransLineItems, dataManager, log);

            Logger.logMessage("Logging the organized transaction line items in the failed print job.", log, Logger.LEVEL.TRACE);
            for (MMHTree<HashMap> transLineItemTree : organizedTransLineItems) {
                transLineItemTree.logTree(transLineItemTree, "--", log, Logger.LEVEL.TRACE);
            }

            LinkedHashMap<String, Integer> productsAndQuantities = new LinkedHashMap<>();
            for (MMHTree<HashMap> transLineItemTree : organizedTransLineItems) {
                productsAndQuantities.put(HashMapDataFns.getStringVal(transLineItemTree.getData(), "PRODUCTNAME"), ((int) Math.round(HashMapDataFns.getDoubleVal(transLineItemTree.getData(), "QUANTITY"))));
                if (!DataFunctions.isEmptyCollection(transLineItemTree.getChildren())) {
                    for (MMHTree<HashMap> modifierTree : transLineItemTree.getChildren()) {
                        productsAndQuantities.put(HashMapDataFns.getStringVal(modifierTree.getData(), "PRODUCTNAME"), -1);
                    }
                }
            }
            return productsAndQuantities;
        }

        return null;
    }

    /**
     * <p>Iterates through the failed print job details and organizes them so that the dining option will appear first and any modifiers will follow the parent product.</p>
     *
     * @param failedPrintJobDetails An {@link ArrayList} of {@link HashMap} corresponding to the print job details that failed to print.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return An {@link ArrayList} of a {@link MMHTree} of {@link HashMap} corresponding to the organized print job details that failed to be printed.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "Convert2streamapi", "OverlyComplexMethod"})
    private static ArrayList<MMHTree<HashMap>> organizeTransLineItemsInFailedPrintJob (ArrayList<HashMap> failedPrintJobDetails, DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.organizeTransLineItemsInFailedPrintJob, unable to organize the transaction line items that failed to print!", Logger.LEVEL.ERROR);
            return null;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.organizeTransLineItemsInFailedPrintJob can't be null, unable to organize the transaction line items that failed to print!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyCollection(failedPrintJobDetails)) {
            Logger.logMessage("No products were passed to KPEmailAlert.organizeTransLineItemsInFailedPrintJob, unable to organize the transaction line items that failed to print!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // determine which lines from the failed print job are trans line items that need to be organized
        ArrayList<HashMap> printJobDetailsToOrg = new ArrayList<>();
        for (HashMap printJobDetail : failedPrintJobDetails) {
            if ((!DataFunctions.isEmptyMap(printJobDetail)) && (printJobDetail.containsKey("PARENTPAPLUID")) && (printJobDetail.containsKey("ISDININGOPTION"))) {
                printJobDetailsToOrg.add(printJobDetail);
            }
        }

        // organize the print job details for trans line items
        if (!DataFunctions.isEmptyCollection(printJobDetailsToOrg)) {
            ArrayList<MMHTree<HashMap>> organizedTransLineItems = new ArrayList<>();

            // search for a dining option
            HashMap diningOptionHM = null;
            for (HashMap printJobDetail : printJobDetailsToOrg) {
                if (HashMapDataFns.getBooleanVal(printJobDetail, "ISDININGOPTION")) {
                    diningOptionHM = printJobDetail;
                    break;
                }
            }

            // organize the remaining print job details
            if (!DataFunctions.isEmptyCollection(printJobDetailsToOrg)) {
                // set all the products
                for (HashMap printJobDetail : printJobDetailsToOrg) {
                    if ((!HashMapDataFns.getBooleanVal(printJobDetail, "ISDININGOPTION")) && (HashMapDataFns.getIntVal(printJobDetail, "PARENTPAPLUID") < 0)) {
                        organizedTransLineItems.add(new MMHTree<>(printJobDetail));
                    }
                }
                // add modifiers to the products
                for (HashMap printJobDetail : printJobDetailsToOrg) {
                    if ((!HashMapDataFns.getBooleanVal(printJobDetail, "ISDININGOPTION")) && (HashMapDataFns.getIntVal(printJobDetail, "PARENTPAPLUID") > 0)) {
                        // get the parent record
                        MMHTree<HashMap> parentProductTree = null;
                        if (!DataFunctions.isEmptyCollection(organizedTransLineItems)) {
                            for (MMHTree<HashMap> potentialParentProductTree : organizedTransLineItems) {
                                if (HashMapDataFns.getIntVal(printJobDetail, "PARENTPAPLUID").intValue() == HashMapDataFns.getIntVal(potentialParentProductTree.getData(), "PAPLUID").intValue()) {
                                    parentProductTree = potentialParentProductTree;
                                    break;
                                }
                            }
                        }
                        // append the modifier to the parent product
                        if (parentProductTree != null) {
                            parentProductTree.addChild(new MMHTree<>(printJobDetail));
                        }
                    }
                }
            }

            // if there is a dining option list it first
            if (!DataFunctions.isEmptyMap(diningOptionHM)) {
                organizedTransLineItems.add(0, new MMHTree<>(diningOptionHM));
            }

            return organizedTransLineItems;
        }

        return null;
    }

    @SuppressWarnings({"Convert2streamapi", "TypeMayBeWeakened", "unchecked"})
    private static void getTransLineItemNames (ArrayList<MMHTree<HashMap>> transLineItems, DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.getTransLineItemNames, unable to get the names of transaction line items that failed to print!", Logger.LEVEL.ERROR);
            return;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.getTransLineItemNames can't be null, unable to get the names of transaction line items that failed to print!", log, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(transLineItems)) {
            Logger.logMessage("No transaction line items were passed to KPEmailAlert.getTransLineItemNames, unable to get the names of transaction line items that failed to print!", log, Logger.LEVEL.ERROR);
            return;
        }

        // get the product IDs from the transaction line items
        ArrayList<Integer> productIDs = new ArrayList<>();
        for (MMHTree<HashMap> productTree : transLineItems) {
            productIDs.add(HashMapDataFns.getIntVal(productTree.getData(), "PAPLUID"));
            if (!DataFunctions.isEmptyCollection(productTree.getChildren())) {
                for (MMHTree<HashMap> modifierTree : productTree.getChildren()) {
                    productIDs.add(HashMapDataFns.getIntVal(modifierTree.getData(), "PAPLUID"));
                }
            }
        }

        // query the database
        if (!DataFunctions.isEmptyCollection(productIDs)) {
            DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetProductNamesFromIDs").addIDList(1, productIDs);
            ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                for (HashMap productIDAndName : queryRes) {
                    int productID = HashMapDataFns.getIntVal(productIDAndName, "PAPLUID");
                    String name = HashMapDataFns.getStringVal(productIDAndName, "NAME");
                    for (MMHTree<HashMap> productTree : transLineItems) {
                        // check if the product ID is for a modifier
                        if (!DataFunctions.isEmptyCollection(productTree.getChildren())) {
                            for (MMHTree<HashMap> modifierTree : productTree.getChildren()) {
                                if (productID == HashMapDataFns.getIntVal(modifierTree.getData(), "PAPLUID")) {
                                    modifierTree.getData().put("PRODUCTNAME", KitchenDisplaySystemJobInfo.MODIFIER_INDICATOR + name);
                                }
                            }
                        }
                        // check if the product ID is for the product
                        if (productID == HashMapDataFns.getIntVal(productTree.getData(), "PAPLUID")) {
                            productTree.getData().put("PRODUCTNAME", name);
                        }
                    }
                }
            }
        }

    }

    /**
     * <p>Calculates the total number of products that failed to print on the printer.</p>
     *
     * @param productsAndQuantities A {@link LinkedHashMap} whose key is the {@link String} product name and whose value of the {@link Integer} quantity
     * of the product within the order that failed to print on the printer.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return The total number of products that failed to print on the printer.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private static int calculateTotalNumberOfProducts (LinkedHashMap<String, Integer> productsAndQuantities, DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.calculateTotalNumberOfProducts, unable to calculate the total number of products!", Logger.LEVEL.ERROR);
            return -1;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.calculateTotalNumberOfProducts can't be null, unable to calculate the total number of products!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        if (DataFunctions.isEmptyMap(productsAndQuantities)) {
            Logger.logMessage("No products were passed to KPEmailAlert.calculateTotalNumberOfProducts, unable to calculate the total number of products!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        int total = 0;
        for (Integer productTotal : productsAndQuantities.values()) {
            if ((productTotal != null) && (productTotal > 0)) {
                total += productTotal;
            }
        }

        return total;
    }

    /**
     * <p>Sends an email to people whose should be alerted when a print job fails within the revenue center.</p>
     *
     * @param failedPrintJobDetails An {@link ArrayList} of {@link HashMap} corresponding to the print job details that failed to print.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @return Whether or not the email could be sent successfully.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    public static boolean sendEmail (ArrayList<HashMap> failedPrintJobDetails, DataManager dataManager, String log) {

        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("No log file found in KPEmailAlert.sendEmail, unable to send the email!", Logger.LEVEL.ERROR);
            return false;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KPEmailAlert.sendEmail can't be null, unable to send the email!", log, Logger.LEVEL.ERROR);
            return false;
        }

        if (DataFunctions.isEmptyCollection(failedPrintJobDetails)) {
            Logger.logMessage("No failed print job details were passed to KPEmailAlert.sendEmail, unable to send the email!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // remove any null or empty HashMaps
        failedPrintJobDetails = DataFunctions.purgeAlOfHm(failedPrintJobDetails);

        if (DataFunctions.isEmptyCollection(failedPrintJobDetails)) {
            Logger.logMessage("No failed print job details were passed to KPEmailAlert.sendEmail after removing null or empty print job details, unable to send the email!", log, Logger.LEVEL.ERROR);
            return false;
        }

        int revenueCenterID = HashMapDataFns.getIntVal(failedPrintJobDetails.get(0), "REVENUECENTERID");

        if (revenueCenterID <= 0) {
            Logger.logMessage("Encountered an invalid revenue center ID in KPEmailAlert.sendEmail, unable to send the email!", log, Logger.LEVEL.ERROR);
            return false;
        }

        int paPrinterQueueID = HashMapDataFns.getIntVal(failedPrintJobDetails.get(0), "PAPRINTERQUEUEID");

        if (paPrinterQueueID <= 0) {
            Logger.logMessage("Encountered an invalid printer queue ID in KPEmailAlert.sendEmail, unable to send the email!", log, Logger.LEVEL.ERROR);
            return false;
        }

        int printerID = HashMapDataFns.getIntVal(failedPrintJobDetails.get(0), "PRINTERID");

        if (printerID <= 0) {
            Logger.logMessage("Encountered an invalid printer ID in KPEmailAlert.sendEmail, unable to send the email!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // get the back office email configuration information
        String smtpServer;
        String smtpUser;
        String smtpPassword;
        int smtpPort;
        String smtpFromAddress;
        HashMap emailConfig = getEmailConfig(dataManager, log);
        if (!DataFunctions.isEmptyMap(emailConfig)) {
            smtpServer = HashMapDataFns.getStringVal(emailConfig, "SMTPSERVER");
            smtpUser = HashMapDataFns.getStringVal(emailConfig, "SMTPUSER");
            smtpPassword = HashMapDataFns.getStringVal(emailConfig, "SMTPPASSWORD");
            smtpPort = HashMapDataFns.getIntVal(emailConfig, "SMTPPORT");
            smtpFromAddress = HashMapDataFns.getStringVal(emailConfig, "SMTPFROMADDRESS");
        }
        else {
            Logger.logMessage("Unable to obtain the back office email configuration in KPEmailAlert.sendEmail, unable to send the email!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // build the subject line for the email
        String subject = buildEmailSubject(paPrinterQueueID, printerID, dataManager, log);

        // don't send an email if there isn't any content
        String message = buildEmailMessage(paPrinterQueueID, failedPrintJobDetails, dataManager, log);
        if (!StringFunctions.stringHasContent(message)) {
            Logger.logMessage("No message content for the email body in KPEmailAlert.sendEmail, unable to send the email!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // get the people to email within the revenue center
        ArrayList<String> emailAddresses = getEmailListForRC(revenueCenterID, dataManager, log);
        if (!DataFunctions.isEmptyCollection(emailAddresses)) {
            // iterate through each email address an email the receipt
            ArrayList<Boolean> sendResults = new ArrayList<>();
            for (String emailAddress : emailAddresses) {
                if (StringFunctions.stringHasContent(smtpPassword)) {
                    smtpPassword = StringFunctions.decodePassword(smtpPassword);
                }
                if (!StringFunctions.stringHasContent(smtpServer)) {
                    Logger.logMessage("The SMTP server in KPEmailAlert.sendEmail must be defined, unable to send the email!", log, Logger.LEVEL.ERROR);
                    sendResults.add(false);
                }
                else {
                    if (StringFunctions.stringHasContent(smtpUser)) {
                        // send an authenticated email
                        Logger.logMessage(String.format("Sending an authenticated email receipt to %s using the mail server %s and port %s in KPEmailAlert.sendEmail.",
                                Objects.toString(emailAddress, "N/A"),
                                Objects.toString(smtpServer, "N/A"),
                                Objects.toString(smtpPort, "N/A")), log, Logger.LEVEL.TRACE);
                        sendResults.add(MMHEmail.sendAuthEmail(smtpPort, smtpServer, smtpUser, smtpPassword, emailAddress, smtpFromAddress, subject, message, "text/html"));
                    }
                    else {
                        // send an unauthenticated email
                        Logger.logMessage(String.format("Sending an unauthenticated email receipt to %s using the mail server %s and port %s in KPEmailAlert.sendEmail.",
                                Objects.toString(emailAddress, "N/A"),
                                Objects.toString(smtpServer, "N/A"),
                                Objects.toString(smtpPort, "N/A")), log, Logger.LEVEL.TRACE);
                        sendResults.add(MMHEmail.sendEmail(smtpPort, smtpServer, emailAddress, smtpFromAddress, subject, message, "text/html"));
                    }
                }
            }
            // if any email that should have been sent fails, indicate an error occurred
            if (!DataFunctions.isEmptyCollection(sendResults)) {
                boolean totalSendResult = true;
                for (boolean sendResult : sendResults) {
                    if (!sendResult) {
                        totalSendResult = false;
                        break;
                    }
                }
                return totalSendResult;
            }
        }
        else {
            Logger.logMessage("No email addresses found within the revenue center in KPEmailAlert.sendEmail, unable to send the email!", log, Logger.LEVEL.ERROR);
            return false;
        }

        return false;
    }


}