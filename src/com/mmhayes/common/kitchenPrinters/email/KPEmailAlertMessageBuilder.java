package com.mmhayes.common.kitchenPrinters.email;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-07-28 09:57:23 -0400 (Tue, 28 Jul 2020) $: Date of last commit
    $Rev: 12259 $: Revision of last commit
    Notes: Builds the message body of an email that will alert people that a print job failed to print.
*/

import com.mmhayes.common.receiptGen.ReceiptData.KitchenDisplaySystemJobInfo;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.StringFunctions;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>Builds the message body of an email that will alert people that a print job failed to print.</p>
 *
 */
public class KPEmailAlertMessageBuilder {

    // path to the M.M. Hayes logo image
    private static final Path MMH_LOGO_ABS_PATH = Paths.get(System.getProperty("user.dir") + "/webapps/pos/assets/MMHLogo.png");
    private static final Path MMH_LOGO_BASE_PATH = Paths.get(System.getProperty("user.dir"));
    private static final Path MMH_LOGO_REL_PATH = MMH_LOGO_BASE_PATH.relativize(MMH_LOGO_ABS_PATH);
    public static final String MMH_LOGO_IMG = MMH_LOGO_REL_PATH.toUri().getPath();

    // private member variables of a KPEmailAlertMessageBuilder
    private int paOrderTypeID = -1;
    private boolean isKDS = false;
    private LocalDateTime transactionDate = null;
    private int paTransactionID = -1;
    private int terminalID = -1;
    private String orderNumber = "";
    private String printerName = "";
    private String customer = "";
    private String comment = "";
    private String pickupDeliveryNote = "";
    private HashMap<String, Integer> transLineItems = null;
    private int totalNumberOfProducts = -1;

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder}.</p>
     *
     */
    public KPEmailAlertMessageBuilder () {}

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder} that appends it's paOrderTypeID field.</p>
     *
     * @param paOrderTypeID The ID of the type of order the transaction is.
     * @return The {@link KPEmailAlertMessageBuilder} it's paOrderTypeID field appended.
     */
    public KPEmailAlertMessageBuilder addPAOrderTypeID (int paOrderTypeID) {
        this.paOrderTypeID = paOrderTypeID;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder} that appends it's isKDS field.</p>
     *
     * @param isKDS Whether or not the print job that failed was for KDS.
     * @return The {@link KPEmailAlertMessageBuilder} it's isKDS field appended.
     */
    public KPEmailAlertMessageBuilder addIsKDS (boolean isKDS) {
        this.isKDS = isKDS;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder} that appends it's transactionDate field.</p>
     *
     * @param transactionDate The {@link LocalDateTime} that the transaction took place.
     * @return The {@link KPEmailAlertMessageBuilder} it's transactionDate field appended.
     */
    public KPEmailAlertMessageBuilder addTransactionDate (LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder} that appends it's paTransactionID field.</p>
     *
     * @param paTransactionID The ID of the transaction.
     * @return The {@link KPEmailAlertMessageBuilder} it's paTransactionID field appended.
     */
    public KPEmailAlertMessageBuilder addPATransactionID (int paTransactionID) {
        this.paTransactionID = paTransactionID;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder} that appends it's terminalID field.</p>
     *
     * @param terminalID The ID of the terminal the transaction took place on.
     * @return The {@link KPEmailAlertMessageBuilder} it's terminalID field appended.
     */
    public KPEmailAlertMessageBuilder addTerminalID (int terminalID) {
        this.terminalID = terminalID;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder} that appends it's orderNumber field.</p>
     *
     * @param orderNumber The order number {@link String} of the order that failed to print.
     * @return The {@link KPEmailAlertMessageBuilder} it's orderNumber field appended.
     */
    public KPEmailAlertMessageBuilder addOrderNumber (String orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder} that appends it's printerName field.</p>
     *
     * @param printerName The name {@link String} of the printer that failed to execute the print job.
     * @return The {@link KPEmailAlertMessageBuilder} it's printerName field appended.
     */
    public KPEmailAlertMessageBuilder addPrinterName (String printerName) {
        this.printerName = printerName;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder} that appends it's customer field.</p>
     *
     * @param customer The name {@link String} of the customer that placed the order.
     * @return The {@link KPEmailAlertMessageBuilder} it's customer field appended.
     */
    public KPEmailAlertMessageBuilder addCustomer (String customer) {
        this.customer = customer;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder} that appends it's comment field.</p>
     *
     * @param comment A comment {@link String} that may have been left by the customer.
     * @return The {@link KPEmailAlertMessageBuilder} it's comment field appended.
     */
    public KPEmailAlertMessageBuilder addComment (String comment) {
        this.comment = comment;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder} that appends it's pickupDeliveryNote field.</p>
     *
     * @param pickupDeliveryNote A note {@link String} related to the pickup or delivery of the order.
     * @return The {@link KPEmailAlertMessageBuilder} it's pickupDeliveryNote field appended.
     */
    public KPEmailAlertMessageBuilder addPickupDeliveryNote (String pickupDeliveryNote) {
        this.pickupDeliveryNote = pickupDeliveryNote;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder} that appends it's transLineItems field.</p>
     *
     * @param transLineItems A {@link HashMap} whose key is the {@link String} product name and whose value of
     * the {@link Integer} quantity of the product within the order that failed to print on the printer.
     * @return The {@link KPEmailAlertMessageBuilder} it's transLineItems field appended.
     */
    public KPEmailAlertMessageBuilder addTransLineItems (HashMap<String, Integer> transLineItems) {
        this.transLineItems = transLineItems;
        return this;
    }

    /**
     * <p>Constructor for a {@link KPEmailAlertMessageBuilder} that appends it's totalNumberOfProducts field.</p>
     *
     * @param totalNumberOfProducts The total number of products within the order that failed to print on the printer.
     * @return The {@link KPEmailAlertMessageBuilder} it's totalNumberOfProducts field appended.
     */
    public KPEmailAlertMessageBuilder addTotalNumberOfProducts (int totalNumberOfProducts) {
        this.totalNumberOfProducts = totalNumberOfProducts;
        return this;
    }

    /**
     * <p>Converts the {@link KPEmailAlertMessageBuilder} into a {@link String}.</p>
     *
     * @return The converted {@link KPEmailAlertMessageBuilder} as a {@link String}.
     */
    @SuppressWarnings("OverlyComplexMethod")
    @Override
    public String toString () {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("<h1>");
        stringBuilder.append(String.format("FAILED TO %s ORDER!", Objects.toString((isKDS ? "DISPLAY" : "PRINT"), "N/A")));
        stringBuilder.append("</h1>");
        stringBuilder.append("<br>");
        stringBuilder.append("<hr>");
        stringBuilder.append("<br>");
        stringBuilder.append("<table style='border: 2px solid #231F20; border-collapse: collapse; width: 100%; font-family: sans-serif;'>");
        stringBuilder.append("<thead style='border: 2px solid #231F20; background-color: #EF4135; color: #FFFFFF;'>");


        // transaction date and time row
        if (transactionDate != null) {
            // add the transaction date
            DateTimeFormatter txnDateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy"); // example ==> 06/15/2020
            String transactionDateStr = txnDateFormatter.format(transactionDate);
            stringBuilder.append("<tr>");
            stringBuilder.append("<th colspan='2'>");
            stringBuilder.append("<div style='float: left;'>");
            stringBuilder.append("Transaction Date: ");
            stringBuilder.append(transactionDateStr);
            stringBuilder.append("</div>");
            // add the transaction time
            DateTimeFormatter txnTimeFormatter = DateTimeFormatter.ofPattern("hh:mm:ss a"); // example ==> 08:08:58 AM
            String transactionTimeStr = txnTimeFormatter.format(transactionDate);
            stringBuilder.append("<div style='float: right;'>");
            stringBuilder.append("Transaction Time: ");
            stringBuilder.append(transactionTimeStr);
            stringBuilder.append("</div>");
            stringBuilder.append("</th>");
            stringBuilder.append("</tr>");
        }

        // transaction ID and terminal ID row
        if ((paTransactionID > 0) && (terminalID > 0)) {
            // add the transaction ID
            stringBuilder.append("<tr>");
            stringBuilder.append("<th colspan='2'>");
            stringBuilder.append("<div style='float: left;'>");
            stringBuilder.append("Transaction ID: ");
            stringBuilder.append(paTransactionID);
            stringBuilder.append("</div>");
            // add the terminal ID
            stringBuilder.append("<div style='float: right;'>");
            stringBuilder.append("Terminal ID: ");
            stringBuilder.append(terminalID);
            stringBuilder.append("</div>");
            stringBuilder.append("</th>");
            stringBuilder.append("</tr>");
        }

        // order number and printer name row
        if (StringFunctions.stringHasContent(orderNumber)) {
            // add the order number
            stringBuilder.append("<tr>");
            stringBuilder.append("<th colspan='2'>");
            stringBuilder.append("<div style='float: left;'>");
            stringBuilder.append("Order #: ");
            stringBuilder.append(orderNumber);
            stringBuilder.append("</div>");
            // add the printer name
            stringBuilder.append("<div style='float: right;'>");
            stringBuilder.append(String.format("%s Name: ", Objects.toString((isKDS ? "KDS Station" : "Printer"), "N/A")));
            stringBuilder.append(printerName);
            stringBuilder.append("</div>");
            stringBuilder.append("</th>");
            stringBuilder.append("</tr>");
        }

        // customer and comment row
        if ((StringFunctions.stringHasContent(customer)) && (StringFunctions.stringHasContent(comment))) {
            // add the customer
            stringBuilder.append("<tr>");
            stringBuilder.append("<th colspan='2'>");
            stringBuilder.append("<div style='float: left;'>");
            stringBuilder.append("Customer: ");
            stringBuilder.append(customer);
            stringBuilder.append("</div>");
            // add the comment
            stringBuilder.append("<div style='float: right;'>");
            stringBuilder.append("Comment: ");
            stringBuilder.append(comment);
            stringBuilder.append("</div>");
            stringBuilder.append("</th>");
            stringBuilder.append("</tr>");
        }
        else if ((StringFunctions.stringHasContent(customer)) && (!StringFunctions.stringHasContent(comment))) {
            // add the customer only
            stringBuilder.append("<tr>");
            stringBuilder.append("<th colspan='2'>");
            stringBuilder.append("<div style='float: left;'>");
            stringBuilder.append("Customer: ");
            stringBuilder.append(customer);
            stringBuilder.append("</div>");
            stringBuilder.append("</th>");
            stringBuilder.append("</tr>");
        }
        else if ((!StringFunctions.stringHasContent(customer)) && (StringFunctions.stringHasContent(comment))) {
            // add the comment only
            stringBuilder.append("<tr>");
            stringBuilder.append("<th colspan='2'>");
            stringBuilder.append("<div style='float: left;'>");
            stringBuilder.append("Comment: ");
            stringBuilder.append(comment);
            stringBuilder.append("</div>");
            stringBuilder.append("</th>");
            stringBuilder.append("</tr>");
        }

        // pickup/delivery note row
        if (StringFunctions.stringHasContent(pickupDeliveryNote)) {
            // add the pickup/delivery note
            stringBuilder.append("<tr>");
            stringBuilder.append("<th colspan='2'>");
            stringBuilder.append("<div style='float: left;'>");
            stringBuilder.append(String.format("%s Note: ", Objects.toString((paOrderTypeID == TypeData.OrderType.PICKUP ? "Pickup" : "Delivery"), "N/A")));
            stringBuilder.append(pickupDeliveryNote);
            stringBuilder.append("</div>");
            stringBuilder.append("</th>");
            stringBuilder.append("</tr>");
        }
        stringBuilder.append("</thead>");

        stringBuilder.append("<tbody style='background-color: #F59089; color: #FFFFFF;'>");
        if (!DataFunctions.isEmptyMap(transLineItems)) {
            stringBuilder.append("<tr>");
            stringBuilder.append("<td style='width: 5%;border: 2px solid #231F20; background-color: #F2645A; text-align: center;'>");
            stringBuilder.append("<b>");
            stringBuilder.append("Quantity");
            stringBuilder.append("</b>");
            stringBuilder.append("</td>");
            stringBuilder.append("<td style='width: 95%; border: 2px solid #231F20; background-color: #F2645A; text-align: center;'>");
            stringBuilder.append("<b>");
            stringBuilder.append("Product");
            stringBuilder.append("</b>");
            stringBuilder.append("</td>");
            stringBuilder.append("</tr>");
            for (Map.Entry<String, Integer> productAndQuantityEntry : transLineItems.entrySet()) {
                String quantity = "";
                if (productAndQuantityEntry.getValue() > 0) {
                    quantity = String.valueOf(productAndQuantityEntry.getValue());
                }
                String product;
                if (productAndQuantityEntry.getKey().startsWith(KitchenDisplaySystemJobInfo.MODIFIER_INDICATOR)) {
                    product = productAndQuantityEntry.getKey().replaceAll(KitchenDisplaySystemJobInfo.MODIFIER_INDICATOR, "    ");
                }
                else {
                    product = productAndQuantityEntry.getKey();
                }
                // add a new row for each quantity and product
                stringBuilder.append("<tr>");
                stringBuilder.append("<td style='width: 5%; border: 1px solid #231F20; text-align: center;'>");
                stringBuilder.append(quantity);
                stringBuilder.append("</td>");
                stringBuilder.append("<td style='width: 95%; border: 1px solid #231F20;'>");
                stringBuilder.append(product);
                stringBuilder.append("</td>");
                stringBuilder.append("</tr>");
            }
        }
        stringBuilder.append("</tbody>");

        stringBuilder.append("<tfoot style='border: 2px solid #231F20; background-color: #EF4135; color: #FFFFFF;'>");
        if (totalNumberOfProducts > 0) {
            stringBuilder.append("<tr>");
            stringBuilder.append("<th colspan='2'>");
            stringBuilder.append("<div style='float: left;'>");
            stringBuilder.append("Total Number Of Items: ");
            stringBuilder.append(totalNumberOfProducts);
            stringBuilder.append("</div>");
            stringBuilder.append("</th>");
            stringBuilder.append("</tr>");
        }
        stringBuilder.append("</tfoot>");
        stringBuilder.append("</table>");

        // add a divider between the order and the email's footer
        stringBuilder.append("<br>");
        stringBuilder.append("<hr>");
        stringBuilder.append("<br>");

        // add the M.M. Hayes footer
        stringBuilder.append("<p style=\"color: black;\"><strong>M.M. Hayes Co., Inc</strong></p>");
        stringBuilder.append("<p style=\"color: black;\">16 Sage Estate | Albany, NY 12204</p>");
        stringBuilder.append("<p style=\"color: black;\">Office: (518) 459-5545</p><br>");
        stringBuilder.append("<img src=\"");
        stringBuilder.append(MMH_LOGO_IMG);
        stringBuilder.append("\">");
        stringBuilder.append("<p style=\"color: #EF4135;\"><strong>Join MM Hayes on:</strong>");
        stringBuilder.append("<a href=\"http://www.mmhayes.com/\" target=\"_blank\" style=\"color: blue;\"><strong>mmhayes.com</strong></a>");
        stringBuilder.append("<strong>|</strong>");
        stringBuilder.append("<a href=\"https://twitter.com/mmhayesco\" target=\"_blank\" style=\"color: blue;\"><strong>Twitter</strong></a>");
        stringBuilder.append("<strong>|</strong>");
        stringBuilder.append("<a href=\"http://www.linkedin.com/company/mm-hayes-company-inc-\" target=\"_blank\" style=\"color: blue;\"><strong>LinkedIn</strong></a>");
        stringBuilder.append("<strong>|</strong>");
        stringBuilder.append("<a href=\"http://www.youtube.com/themmhayescompany\" target=\"_blank\" style=\"color: blue;\"><strong>YouTube</strong></a>");
        stringBuilder.append("</p>");
        stringBuilder.append("<p>To assist MM Hayes in serving you better, please place all service requests through the Help Desk. ");
        stringBuilder.append("The Help Desk can be reached by calling (518) 459-5545 or via email at ");
        stringBuilder.append("<a href=\"mailto:helpdesk@mmhayes.com\" target=\"_blank\" title=\"mailto:helpdesk@mmhayes.com\" style=\"color: blue;\">helpdesk@mmhayes.com</a>.</p>");

        return stringBuilder.toString();
    }

}