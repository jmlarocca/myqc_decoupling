package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-08-20 16:25:56 -0400 (Fri, 20 Aug 2021) $: Date of last commit
    $Rev: 15135 $: Revision of last commit
    Notes: Houses common methods to be used by kitchen printer code.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.*;

/**
 * Class to house common methods for kitchen printer code.
 *
 */
public class KitchenPrinterCommon {

    /**
     * Decode the given base 64 encoded String.
     *
     * @param input {@link String} Base 64 encoded String we wish to decode.
     * @param logFileName {@link String} Name of the log file to be used for error reporting.
     * @return {@link String} String after being decoded from it base 64 encoded version.
     */
    public static String decode (String input, String logFileName) {
        String decodedInput = "";

        try {
            byte[] decodedBytes = Base64.getDecoder().decode(input);
            decodedInput = new String(decodedBytes);
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to decode the base 64 encoded input String in " +
                    "KitchenPrinterCommon.decode", logFileName, Logger.LEVEL.ERROR);
        }

        return decodedInput;
    }

    public static String encode (String input, String logFileName) {
        String encodedInput = "";

        try {
            encodedInput = new String( Base64.getEncoder().encode(input.getBytes()));
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to encode the base 64 encoded input String in " +
                    "KitchenPrinterCommon.encode", logFileName, Logger.LEVEL.ERROR);
        }

        return encodedInput;
    }

    /**
     * Reorganize the printData into an ArrayList of ArrayLists where each ArrayList is a print job and contains
     * a collection of HashMaps (the line details to be printed).
     *
     * @param printData {@link ArrayList} The print data to reorganize.
     * @param logFileName {@link String} The log file for any error reporting.
     * @return {@link ArrayList<ArrayList>} The newly organized print data.
     */
    @SuppressWarnings({"ConstantConditions", "unchecked"})
    public static ArrayList<ArrayList> reorganizePrintData (ArrayList printData, String logFileName) {
        ArrayList<ArrayList> organizedPrintData = new ArrayList<>();

        try {
            String lastPhysicalPAPrinterQueueID = "";
            String lastPhysicalPrinterID = "";
            ArrayList lastPhysicalPrintJob = null;
            ArrayList kdsPrintJob = null;

            if ((printData != null) && (!printData.isEmpty())) {
                for (Object printLineDetailObj : printData) {
                    HashMap printLineDetail = (HashMap) printLineDetailObj;
                    ArrayList printJob;
                    String paPrinterQueueID = HashMapDataFns.getStringVal(printLineDetail, "PAPRINTERQUEUEID");
                    String printerID = HashMapDataFns.getStringVal(printLineDetail, "PRINTERID");
                    boolean isKDS = HashMapDataFns.getBooleanVal(printLineDetail, "ISKDS");

                    if ((lastPhysicalPrintJob == null)
                            || (!lastPhysicalPAPrinterQueueID.equalsIgnoreCase(paPrinterQueueID))
                            || ( isKDS == false && (!lastPhysicalPAPrinterQueueID.equalsIgnoreCase(paPrinterQueueID) || !lastPhysicalPrinterID.equalsIgnoreCase(printerID)))) {
                        printJob = new ArrayList();
                        lastPhysicalPrintJob = printJob;
                        organizedPrintData.add(printJob);

                        lastPhysicalPAPrinterQueueID = paPrinterQueueID;
                        lastPhysicalPrinterID = printerID;
                    }
                    else {
                        printJob = lastPhysicalPrintJob;
                    }

                    // add the print line detail to the print job
                    printJob.add(printLineDetail);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to reorganize the printData in " +
                    "KitchenPrinterCommon.reorganizePrintData", logFileName, Logger.LEVEL.ERROR);
        }

        return organizedPrintData;
    }

    /**
     * <p>Utility method to find the MAC address {@link String} of the terminal running the printer host configured as the primary printer host in the backoffice.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to get the associated primary printer host terminal for.
     * @param log The path {@link String} of the file to log any errors to.
     * @return The MAC address {@link String} of the terminal running the printer host configured as the primary printer host in the backoffice.
     */
    public static String getPrimaryPrinterHost (String macAddress, String log) {
        String primaryMacAddress = "";

        try {
            if (StringFunctions.stringHasContent(macAddress)) {
                Object queryRes = new DataManager().parameterizedExecuteScalar("data.newKitchenPrinter.GetPrimaryPrinterHostMacAddress", new Object[]{macAddress}, log);
                if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString()))) {
                    primaryMacAddress = queryRes.toString();
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to get the MAC address of the terminal running the " +
                    "primary printer host associated with the terminal %s in KitchenPrinterCommon.getPrimaryPrinterHost",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), log, Logger.LEVEL.ERROR);
        }

        return primaryMacAddress;
    }

    /**
     * <p>Utility method to check whether or not the terminal that called this method is a satellite terminal.</p>
     *
     * @param log The path {@link String} of the file to log any errors to.
     * @return Whether or not the terminal that called this method is a satellite terminal.
     */
    public static boolean isTerminalSatellite (String log) {
        boolean isSatellite = false;

        try {
            Object queryRes = new DataManager().parameterizedExecuteScalar("data.newKitchenPrinter.GetIsSatelliteTerminal", new Object[]{PeripheralsDataManager.getTerminalMacAddress()}, log);
            if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (queryRes.toString().equalsIgnoreCase("1"))) {
                isSatellite = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not the terminal %s is a satellite " +
                    "terminal in KitchenPrinterCommon.isTerminalSatellite",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), log, Logger.LEVEL.ERROR);
        }

        return isSatellite;
    }

    /**
     * <p>Gets the name of the WAR as a {@link String} that is running on this terminal.</p>
     *
     * @param log The path {@link String} of the file to log any errors to.
     * @return The name of the WAR as a {@link String} that is running on this terminal.
     */
    public static String getWarName (String log) {
        String war = "";

        try {
            Object queryRes = new DataManager().parameterizedExecuteScalar("data.newKitchenPrinter.GetWarName", new Object[]{PeripheralsDataManager.getTerminalMacAddress()}, log);
            if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString()))) {
                war = queryRes.toString();
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to find the name of the WAR being used by the " +
                    "terminal %s in KitchenPrinterCommon.getWarName",
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), log, Logger.LEVEL.ERROR);
        }

        return war;
    }

    /**
     * <p>Gets the name of the WAR as a {@link String} that is running on the given terminal.</p>
     *
     * @param macAddress The MAC address {@link String} of the terminal to get the WAR name for.
     * @param log The path {@link String} of the file to log any errors to.
     * @return The name of the WAR as a {@link String} that is running on the given terminal.
     */
    public static String getWarName (String macAddress, String log) {
        String war = "";

        try {
            Object queryRes = new DataManager().parameterizedExecuteScalar("data.newKitchenPrinter.GetWarName", new Object[]{macAddress}, log);
            if (StringFunctions.stringHasContent(queryRes.toString())) {
                war = queryRes.toString();
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to find the name of the WAR being used by the " +
                    "terminal %s in KitchenPrinterCommon.getWarName",
                    Objects.toString(PeripheralsDataManager.getTerminalHostname(macAddress), "NULL")), log, Logger.LEVEL.ERROR);
        }

        return war;
    }

    /**
     * <p>Corrects and apostrophes or question marks which were escaped as html.</p>
     *
     * @param s The {@link String} to correct.
     * @return The corrected {@link String}.
     */
    public static String correctAposAndQues (String s) {

        if (!StringFunctions.stringHasContent(s)) {
            Logger.logMessage("The String passed to KitchenPrinterCommon.correctAposAndQues can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        if (s.contains("&#x27;")) {
            s = s.replaceAll("&#x27;", "\'");
        }
        if (s.contains("&amp;&#x23;x27&#x3b;")) {
            s = s.replaceAll("&amp;&#x23;x27&#x3b;", "\'");
        }
        if (s.contains("&#x3f;")) {
            s = s.replaceAll("&#x3f;", "?");
        }
        if (s.contains("&amp;&#x23;x3f&#x3b;")) {
            s = s.replaceAll("&amp;&#x23;x3f&#x3b;", "?");
        }

        return s;
    }

    /**
     * <p>Checks whether or not the terminal with the given ID is an online ordering terminal.</p>
     *
     * @param log The path {@link String} of the file to log any errors to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param terminalID The ID of the terminal the transaction was made on.
     * @return Whether or not the terminal with the given ID is an online ordering terminal.
     */
    public static boolean isOOTerminal (String log, DataManager dataManager, int terminalID) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KitchenPrinterCommon.isOOTerminal can't be false or empty!", Logger.LEVEL.ERROR);
            return false;
        }

        // make sure we have a valid DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenPrinterCommon.isOOTerminal can't be false!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure we have a valid terminal ID
        if (terminalID <= 0) {
            Logger.logMessage("The terminal ID passed to KitchenPrinterCommon.isOOTerminal must be greater than 0!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetIsOOTerminalFromTerminalID").addIDList(1, terminalID).setLogFileName(log);
        Object queryRes = sql.getSingleField(dataManager);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && ((queryRes.toString().equalsIgnoreCase("true")) || (queryRes.toString().equalsIgnoreCase("1")))) {
            return true;
        }

        return false;
    }

    /**
     * <p>Gets the terminal ID, MAC address and the hostname of the terminal with the given ID.</p>
     *
     * @param log The path {@link String} of the file to log any errors to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param terminalID The ID of the terminal the transaction was made on.
     * @return A {@link String} array with 3 elements, the first is the terminal ID, the second is the MAC address, and the third is the hostname.
     */
    public static String[] getMacAndHostnameFromTerminalID (String log, DataManager dataManager, int terminalID) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KitchenPrinterCommon.getMacAndHostnameFromTerminalID can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenPrinterCommon.getMacAndHostnameFromTerminalID can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid terminal ID
        if (terminalID <= 0) {
            Logger.logMessage("The terminal ID passed to KitchenPrinterCommon.getMacAndHostnameFromTerminalID must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetMacAndHostnameFromTerminalID").addIDList(1, terminalID).setLogFileName(log);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
            String id = HashMapDataFns.getStringVal(queryRes.get(0), "TERMINALID");
            String macAddress = HashMapDataFns.getStringVal(queryRes.get(0), "MACADDRESS");
            String hostname = HashMapDataFns.getStringVal(queryRes.get(0), "PASS");
            if ((StringFunctions.stringHasContent(id)) && (StringFunctions.stringHasContent(macAddress)) && (StringFunctions.stringHasContent(hostname))) {
                return new String[]{id, macAddress, hostname};
            }
        }

        return null;
    }

    /**
     * <p>Gets the terminal ID for the terminal with the given MAC address and hostname.</p>
     *
     * @param log The path {@link String} of the file to log any errors to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param macAddress The MAC address {@link String} to get the terminal ID for.
     * @param hostname The hostname {@link String} to get the terminal ID for.
     * @return The terminal ID for the terminal with the given MAC address and hostname.
     */
    public static int getTerminalIDFromMacAndHostname (String log, DataManager dataManager, String macAddress, String hostname) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KitchenPrinterCommon.getTerminalIDFromMacAndHostname can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenPrinterCommon.getTerminalIDFromMacAndHostname can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid MAC address
        if (!StringFunctions.stringHasContent(macAddress)) {
            Logger.logMessage("The MAC address passed to KitchenPrinterCommon.getTerminalIDFromMacAndHostname can't be null or empty!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid hostname
        if (!StringFunctions.stringHasContent(hostname)) {
            Logger.logMessage("The hostname passed to KitchenPrinterCommon.getTerminalIDFromMacAndHostname can't be null or empty!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetTerminalIDFromMacAndHostname").addIDList(1, macAddress).addIDList(2, hostname).setLogFileName(log);

         return sql.getSingleIntField(dataManager);
    }

    /**
     * <p>Queries the database to get a lookup between a terminal and the revenue center the terminal is in.</p>
     *
     * @param log The path {@link String} of the file to log any information related to this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @return A {@link HashMap} whose {@link Integer} key is the ID of a terminal and whose {@link Integer} value is the ID of the revenue center the terminal is in.
     */
    public static HashMap<Integer, Integer> getTrmToRCLookup (String log, DataManager dataManager) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KitchenPrinterCommon.getTrmToRCLookup can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenPrinterCommon.getTrmToRCLookup can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetTerminalRevCenterIDLookup").setLogFileName(log);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        HashMap<Integer, Integer> lookup = new HashMap<>();
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                int terminalID = HashMapDataFns.getIntVal(hm, "TERMINALID");
                int revenueCenterID = HashMapDataFns.getIntVal(hm, "REVENUECENTERID");
                lookup.put(terminalID, revenueCenterID);
            }
        }

        return lookup;
    }

    /**
     * <p>Queries the database to get a lookup for whether or not a printer ID is for KDS.</p>
     *
     * @param log The path {@link String} of the file to log any information related to this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @return A {@link HashMap} whose {@link Integer} key is the ID of a printer and whose {@link Boolean} value is whether or not the printer is KDS.
     */
    public static HashMap<Integer, Boolean> getIsPtrIDForKDS (String log, DataManager dataManager) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KitchenPrinterCommon.getIsPtrIDForKDS can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenPrinterCommon.getIsPtrIDForKDS can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetIsPrinterIDIsKDSLookup").setLogFileName(log);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        HashMap<Integer, Boolean> lookup = new HashMap<>();
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                int printerID = HashMapDataFns.getIntVal(hm, "PRINTERID");
                boolean isKDS = HashMapDataFns.getBooleanVal(hm, "ISKDS");
                lookup.put(printerID, isKDS);
            }
        }

        return lookup;
    }

    /**
     * <p>Gets a list of all the dining options that have been configured.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @return An {@link ArrayList} of {@link Long} containing all the dining options that have been configured.
     */
    public static ArrayList<HashMap> getAllDiningOptions (DataManager dataManager) {

        // make sure we have a valid DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenPrinterCommon.getTerminalIDOfInServicePHForOOTerminal can't be null!", Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetAllDiningOptions");
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        ArrayList<HashMap> diningOptions = new ArrayList<>();
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            diningOptions = queryRes;
        }

        return diningOptions;
    }

    /**
     * <p>Gets the ID of the terminal running the in-service printer host that the online ordering terminal should be sending order to.</p>
     *
     * @param log The path {@link String} of the file to log any information related to this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param ooTerminalID The ID of the online ordering terminal to check.
     * @return The ID of the terminal running the in-service printer host that the online ordering terminal should be sending order to.
     */
    public static int getTerminalIDOfInServicePHForOOTerminal (String log, DataManager dataManager, int ooTerminalID) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KitchenPrinterCommon.getTerminalIDOfInServicePHForOOTerminal can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenPrinterCommon.getTerminalIDOfInServicePHForOOTerminal can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure the ID of the online ordering terminal is valid
        if (ooTerminalID <= 0) {
            Logger.logMessage("The online ordering terminal ID passed to KitchenPrinterCommon.getTerminalIDOfInServicePHForOOTerminal must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetInSvcPHTrmIDForOOTrm").addIDList(1, ooTerminalID).setLogFileName(log);

        return sql.getSingleIntField(dataManager);
    }

    /**
     * <p>Gets a lookup between the printer ID and it's printer model.</p>
     *
     * @param log The path {@link String} of the file to log any information related to this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @return A {@link HashMap} whose {@link Integer} key is a printer ID and whose {@link Integer} value is a printer model ID.
     */
    public static HashMap<Integer, Integer> getPrinterIDToModelIDLookup (String log, DataManager dataManager) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KitchenPrinterCommon.getPrinterIDToModelIDLookup can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenPrinterCommon.getPrinterIDToModelIDLookup can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(DataFunctions.getGenericALFromObjList(dataManager.parameterizedExecuteQuery("data.kms.GetPrinterIDToModelIDLookup", new Object[]{}, log, true), HashMap.class));

        HashMap<Integer, Integer> lookup = new HashMap<>();
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                int printerID = HashMapDataFns.getIntVal(hm, "PRINTERID");
                int printerModelID = HashMapDataFns.getIntVal(hm, "PRINTERMODELID");
                if ((printerID > 0) && (printerModelID > 0)) {
                    lookup.put(printerID, printerModelID);
                }
            }
        }

        return lookup;
    }

}
