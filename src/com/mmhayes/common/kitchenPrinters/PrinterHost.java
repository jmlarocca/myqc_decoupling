package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2019-05-24 11:14:20 -0400 (Fri, 24 May 2019) $: Date of last commit
    $Rev: 38862 $: Revision of last commit
    Notes: Create print jobs and send them to the PrinterQueueHandler.
*/

import com.mmhayes.common.kms.KMSManager;
import com.mmhayes.common.kms.types.KMSOrderStatus;
import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.printing.PrintStatus;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.kms.PeripheralsDataManager;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Singleton class responsible for creating and sending print jobs to the PrinterQueueHandler to be printed.
 *
 */
public class PrinterHost {

    // the only instance of a PrinterHost for this WAR
    private static volatile PrinterHost printerHostInstance = null;

    // log file specific to the PrinterHost
    private static final String PH_LOG = "KP_PrinterHost.log";

    // variables within the PrinterHost scope
    private PrinterHostThread printerHostThread;
    private PrintJobStatusHandler printJobStatusHandler = null;

    // variables needed for PrinterHost instantiation
    private KitchenPrinterTerminal kitchenPrinterTerminal;

    /**
     * Private constructor for a PrinterHost.
     *
     * @param kitchenPrinterTerminal {@link KitchenPrinterTerminal} The kitchen printer terminal this printer host is
     *         running on.
     * @param printerList {@link ArrayList<HashMap>} Records from the QC_Printer table associated with the printer host.
     */
    private PrinterHost (KitchenPrinterTerminal kitchenPrinterTerminal, ArrayList<HashMap> printerList) throws Exception {
        this.kitchenPrinterTerminal = kitchenPrinterTerminal;
        this.printJobStatusHandler = PrintJobStatusHandler.getInstance();

        try {
            printerHostThread = PrinterHostThread.init(kitchenPrinterTerminal, printerList);
            printerHostThread.startPrinterHostThread();
        }
        catch (Exception e) {
            Logger.logException(e, PH_LOG);
            Logger.logMessage("There was a problem trying to instantiate the PrinterHostThread instance and start the " +
                    "PrinterHostThread in the PrinterHost constructor", PH_LOG, Logger.LEVEL.ERROR);
        }
    }

    /**
     * Get the only instance of PrinterHost for this WAR.
     *
     * @return {@link PrinterHost} The instance of PrinterHost for this WAR.
     */
    public static PrinterHost getInstance () {

        try {
            if (printerHostInstance == null) {
                throw new RuntimeException("The PrinterHost must be instantiated before trying to obtain its instance.");
            }
        }
        catch (Exception e) {
            Logger.logException(e, PH_LOG);
            Logger.logMessage("There was a problem trying to get the only instance of PrinterHost for this WAR in " +
                    "PrinterHost.getInstance", PH_LOG, Logger.LEVEL.ERROR);
        }

        return printerHostInstance;
    }

    /**
     * Instantiate and return the only instance of PrinterHost for this WAR.
     *
     * @param kitchenPrinterTerminal {@link KitchenPrinterTerminal} The kitchen printer terminal this printer host is
     *         running on.
     * @param printerList {@link ArrayList<HashMap>} Records from the QC_Printer table associated with the printer host.
     * @return {@link PrinterHost} The instance of PrinterHost for this WAR.
     */
    public static synchronized PrinterHost init (KitchenPrinterTerminal kitchenPrinterTerminal,
                                                 ArrayList<HashMap> printerList) {

        try {
            if (printerHostInstance != null) {
                throw new RuntimeException("The PrinterHost has already been instantiated and may not be instantiated " +
                        "again.");
            }

            // create the instance
            printerHostInstance = new PrinterHost(kitchenPrinterTerminal, printerList);
            Logger.logMessage("The instance of PrinterHost has been instantiated", PH_LOG, Logger.LEVEL.TRACE);
        }
        catch (Exception e) {
            Logger.logException(e, PH_LOG);
            Logger.logMessage("There was a problem trying to instantiate and return the only instance of PrinterHost " +
                    "for this WAR in PrinterHost.init", PH_LOG, Logger.LEVEL.ERROR);
        }

        return printerHostInstance;
    }

    /**
     * Called from the KitchenPrinterTerminal to add print jobs to the PrinterQueueHandler's print queue so they can
     * be printed.
     *
     * @param printJobs {@link ArrayList} The print jobs that we would like to print.
     * @return {@link ArrayList} The print jobs that were passed to the method as an argument.
     */
    @SuppressWarnings({"UnnecessaryLocalVariable", "unchecked", "TypeMayBeWeakened"})
    public synchronized ArrayList queuePrintData (ArrayList printJobs) {
        Logger.logMessage("PrinterHost.queuePrintData called.", "KMS.log", Logger.LEVEL.TRACE);
        ArrayList returnPrintJobs = printJobs;

        try {
            if (!DataFunctions.isEmptyCollection(printJobs)) {
                for (Object printJobObj : printJobs) {
                    ArrayList printQueueDetailObjs = ((ArrayList) printJobObj);
                    ArrayList<HashMap> printQueueDetails = new ArrayList<>();
                    if (!DataFunctions.isEmptyCollection(printQueueDetailObjs)) {
                        for (Object printQueueDetailObj : printQueueDetailObjs) {
                            printQueueDetails.add(((HashMap) printQueueDetailObj));
                        }
                    }
                    if (!DataFunctions.isEmptyCollection(printQueueDetails)) {
                        if (printerHostThread == null) {
                            // if the printer host thread isn't running than nothing can be printed
                            PrintStatus printStatus = new PrintStatus()
                                    .addPrintStatusType(PrintStatusType.ERROR)
                                    .addUpdatedDTM(LocalDateTime.now());
                            ArrayList<Integer> printQueueDetailIDs = printJobStatusHandler.getPrinterQueueDetailIDs(printQueueDetails);
                            // update the status of all the print queue details as errored
                            if (!DataFunctions.isEmptyCollection(printQueueDetailIDs)) {
                                for (int printQueueDetailID : printQueueDetailIDs) {
                                    printJobStatusHandler.updatePrintStatusMap(printQueueDetailID, printStatus);
                                }
                            }
                        }
                        else {
                            // sort print queue details by status
                            HashMap<Integer, ArrayList<HashMap>> printQueueDetailsByPrintStatus = printJobStatusHandler.sortPrintQueueDetailsByPrintStatus(printQueueDetails);
                            if (!DataFunctions.isEmptyMap(printQueueDetailsByPrintStatus)) {
                                if (printQueueDetailsByPrintStatus.containsKey(PrintStatusType.WAITING)) {
                                    ArrayList<HashMap> waitingPrintQueueDetails = printQueueDetailsByPrintStatus.get(PrintStatusType.WAITING);
                                    queueWaitingPrintQueueDetails(waitingPrintQueueDetails);
                                }
                                if (printQueueDetailsByPrintStatus.containsKey(PrintStatusType.SENT)) {
                                    ArrayList<HashMap> sentPrintQueueDetails = printQueueDetailsByPrintStatus.get(PrintStatusType.SENT);
                                    processSentPrintQueueDetails(sentPrintQueueDetails);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, PH_LOG);
            Logger.logMessage("There was a problem trying to add the print job to the PrinterQueueHandler's print " +
                    "queue in PrinterHost.queuePrintData", PH_LOG, Logger.LEVEL.ERROR);
        }

        return returnPrintJobs;
    }

    /**
     * <p>Tries to find the print queue detail with the given ID,</p>
     *
     * @param printQueueDetailID ID of the print queue detail to find.
     * @param printQueueDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details to search through.
     * @return The {@link HashMap} corresponding to the print queue detail with the given ID.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private HashMap getPrintQueueDetailByID (int printQueueDetailID, ArrayList<HashMap> printQueueDetails) {

        if (printQueueDetailID <= 0) {
            Logger.logMessage(String.format("Invalid print queue detail ID of %s passed to PrinterHost.queueWaitingPrintQueueDetails, the print queue detail ID must be greater than 0!",
                    Objects.toString(printQueueDetailID, "N/A")), PH_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyCollection(printQueueDetails)) {
            Logger.logMessage("The print queue details passed to PrinterHost.queueWaitingPrintQueueDetails can't be null or empty!", PH_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        HashMap printQueueDetail = null;
        for (HashMap hm : printQueueDetails) {
            if (!DataFunctions.isEmptyMap(hm)) {
                if (printQueueDetailID == HashMapDataFns.getIntVal(hm, "PAPRINTERQUEUEDETAILID")) {
                    printQueueDetail = hm;
                    break; // match found, no need to keep processing
                }
            }
        }

        return printQueueDetail;
    }

    /**
     * <p>Tries to add print queue details with a print status of waiting to the print queue.</p>
     *
     * @param waitingPrintQueueDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details that have a status of waiting.
     */
    @SuppressWarnings("OverlyComplexMethod")
    private void queueWaitingPrintQueueDetails (ArrayList<HashMap> waitingPrintQueueDetails) {

        if (DataFunctions.isEmptyCollection(waitingPrintQueueDetails)) {
            Logger.logMessage("No print queue details that have been waiting have been passed to PrinterHost.queueWaitingPrintQueueDetails, unable to add them to the print queue!", PH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        // sort the waiting print job details by printer
        HashMap<Integer, ArrayList<HashMap>> printQueueDetailsByPrinter = printJobStatusHandler.sortPrintQueueDetailsByPrinter(waitingPrintQueueDetails);
        if (!DataFunctions.isEmptyMap(printQueueDetailsByPrinter)) {
            // create print jobs for each printer, combine all KDS details into a single print job
            ArrayList<HashMap> kdsPrintQueueDetails = new ArrayList<>();
            ArrayList<HashMap> kmsPrintQueueDetails = new ArrayList<>();
            for (Map.Entry<Integer, ArrayList<HashMap>> printQueueDetailsByPrinterEntry : printQueueDetailsByPrinter.entrySet()) {
                int printerID = printQueueDetailsByPrinterEntry.getKey();
                ArrayList<HashMap> printQueueDetails = printQueueDetailsByPrinterEntry.getValue();
                String printQueueDetailIDsStr = "";
                if ((!DataFunctions.isEmptyCollection(printJobStatusHandler.getPrinterQueueDetailIDs(printQueueDetails)))
                        && (StringFunctions.stringHasContent(StringFunctions.buildStringFromList(printJobStatusHandler.getPrinterQueueDetailIDs(printQueueDetails), ",")))) {
                    printQueueDetailIDsStr = StringFunctions.buildStringFromList(printJobStatusHandler.getPrinterQueueDetailIDs(printQueueDetails), ",");
                }
                boolean isKDS = false;
                boolean isKMS = false;
                if ((!DataFunctions.isEmptyCollection(printQueueDetails)) && (!DataFunctions.isEmptyMap(printQueueDetails.get(0)))) {
                    isKDS = (HashMapDataFns.getIntVal(printQueueDetails.get(0), "STATIONID") > 0);
                }
                if ((!DataFunctions.isEmptyCollection(printQueueDetails)) && (!DataFunctions.isEmptyMap(printQueueDetails.get(0)))) {
                    isKMS = (HashMapDataFns.getIntVal(printQueueDetails.get(0), "KMSSTATIONID") > 0);
                }

                if (isKDS) {
                    kdsPrintQueueDetails.addAll(printQueueDetails);
                }
                else if(isKMS){
                    kmsPrintQueueDetails.addAll(printQueueDetails);
                }
                else {
                    // make sure the kitchen printer is valid and defined    //TODO: need to add a check in here for KMSStation, and if so still offer it to the queue
                    QCKitchenPrinter printer = null;
                    if (!PrinterQueueHandler.getInstance().isPrinterDefined(printerID)) {
                        createPrinter(printerID);
                    }
                    if (PrinterQueueHandler.getInstance().isPrinterDefined(printerID)) {
                        printer = PrinterQueueHandler.getInstance().getPrinter(printerID);
                    }

                    if (printer != null) {
                        // create a print job for the kitchen printer and add it to the queue
                        QCPrintJob kpPrintJob = new QCPrintJob(printQueueDetails, printerHostThread.getKDSDirectories());
                        if (PrinterQueueHandler.getInstance().offerToPrintQueue(kpPrintJob)) {
                            Logger.logMessage(String.format("The print queue details with IDs of %s have successfully been added to the print queue in PrinterHost.queueWaitingPrintQueueDetails",
                                    Objects.toString(StringFunctions.stringHasContent(printQueueDetailIDsStr) ? printQueueDetailIDsStr : "N/A", "N/A")), PH_LOG, Logger.LEVEL.TRACE);
                            // indicate that the print queue details have been added to the queue
                            PrintStatus printStatus = new PrintStatus()
                                    .addPrintStatusType(PrintStatusType.SENT)
                                    .addUpdatedDTM(LocalDateTime.now());
                            printJobStatusHandler.updatePrintStatusMapForPrinter(printerID, printStatus, kpPrintJob);
                            updatePrintStatusOfPrintQueueDetails(printQueueDetails, printStatus.getPrintStatusType());
                        }
                        else {
                            Logger.logMessage(String.format("Unable to add the print queue details with IDs of %s to the print queue in PrinterHost.queueWaitingPrintQueueDetails",
                                    Objects.toString(StringFunctions.stringHasContent(printQueueDetailIDsStr) ? printQueueDetailIDsStr : "N/A", "N/A")), PH_LOG, Logger.LEVEL.ERROR);
                            // indicate that the print queue details failed to be added to the queue
                            PrintStatus printStatus = new PrintStatus()
                                    .addPrintStatusType(PrintStatusType.ERROR)
                                    .addUpdatedDTM(LocalDateTime.now());
                            printJobStatusHandler.updatePrintStatusMapForPrinter(printerID, printStatus, kpPrintJob);
                            updatePrintStatusOfPrintQueueDetails(printQueueDetails, printStatus.getPrintStatusType());
                        }
                    }
                    else {
                        Logger.logMessage(String.format("Unable to add the print queue details with IDs of %s to the print " +
                                "queue due to an invalid printer which has an ID of %s in PrinterHost.queueWaitingPrintQueueDetails",
                                Objects.toString(StringFunctions.stringHasContent(printQueueDetailIDsStr) ? printQueueDetailIDsStr : "N/A", "N/A"),
                                Objects.toString(printerID, "N/A")), PH_LOG, Logger.LEVEL.ERROR);
                        // indicate that the print queue details failed to be added to the queue because of an invalid printer
                        PrintStatus printStatus = new PrintStatus()
                                .addPrintStatusType(PrintStatusType.ERROR)
                                .addUpdatedDTM(LocalDateTime.now());
                        printJobStatusHandler.updatePrintStatusMapForPrinter(printerID, printStatus, printQueueDetails);
                        updatePrintStatusOfPrintQueueDetails(printQueueDetails, printStatus.getPrintStatusType());
                    }

                }
            }

            // create a KDS print job and add it to the queue
            if (!DataFunctions.isEmptyCollection(kdsPrintQueueDetails)) {
                String printQueueDetailIDsStr = "";
                if ((!DataFunctions.isEmptyCollection(printJobStatusHandler.getPrinterQueueDetailIDs(kdsPrintQueueDetails)))
                        && (StringFunctions.stringHasContent(StringFunctions.buildStringFromList(printJobStatusHandler.getPrinterQueueDetailIDs(kdsPrintQueueDetails), ",")))) {
                    printQueueDetailIDsStr = StringFunctions.buildStringFromList(printJobStatusHandler.getPrinterQueueDetailIDs(kdsPrintQueueDetails), ",");
                }
                QCPrintJob kdsPrintJob = new QCPrintJob(kdsPrintQueueDetails, printerHostThread.getKDSDirectories());
                if (PrinterQueueHandler.getInstance().offerToPrintQueue(kdsPrintJob)) {
                    Logger.logMessage(String.format("The print queue details with IDs of %s have successfully been added to the print queue in PrinterHost.queueWaitingPrintQueueDetails",
                            Objects.toString(StringFunctions.stringHasContent(printQueueDetailIDsStr) ? printQueueDetailIDsStr : "N/A", "N/A")), PH_LOG, Logger.LEVEL.TRACE);
                    // indicate that the print queue details have been added to the queue
                    PrintStatus printStatus = new PrintStatus()
                            .addPrintStatusType(PrintStatusType.SENT)
                            .addUpdatedDTM(LocalDateTime.now());
                    ArrayList<Integer> kdsPrintQueueDetailIDs = printJobStatusHandler.getPrinterQueueDetailIDs(kdsPrintQueueDetails);
                    if (!DataFunctions.isEmptyCollection(kdsPrintQueueDetailIDs)) {
                        for (int kdsPrintQueueDetailID : kdsPrintQueueDetailIDs) {
                            printJobStatusHandler.updatePrintStatusMap(kdsPrintQueueDetailID, printStatus);
                            HashMap kdsPrintQueueDetail = getPrintQueueDetailByID(kdsPrintQueueDetailID, kdsPrintQueueDetails);
                            if (!DataFunctions.isEmptyMap(kdsPrintQueueDetail)) {
                                updatePrintStatus(kdsPrintQueueDetail, printStatus.getPrintStatusType());
                            }
                        }
                    }
                }
                else {
                    Logger.logMessage(String.format("Unable to add the print queue details with IDs of %s to the print queue in PrinterHost.queueWaitingPrintQueueDetails",
                            Objects.toString(StringFunctions.stringHasContent(printQueueDetailIDsStr) ? printQueueDetailIDsStr : "N/A", "N/A")), PH_LOG, Logger.LEVEL.ERROR);
                    // indicate that the print queue details failed to be added to the queue
                    PrintStatus printStatus = new PrintStatus()
                            .addPrintStatusType(PrintStatusType.ERROR)
                            .addUpdatedDTM(LocalDateTime.now());
                    ArrayList<Integer> kdsPrintQueueDetailIDs = printJobStatusHandler.getPrinterQueueDetailIDs(kdsPrintQueueDetails);
                    if (!DataFunctions.isEmptyCollection(kdsPrintQueueDetailIDs)) {
                        for (int kdsPrintQueueDetailID : kdsPrintQueueDetailIDs) {
                            printJobStatusHandler.updatePrintStatusMap(kdsPrintQueueDetailID, printStatus);
                            HashMap kdsPrintQueueDetail = getPrintQueueDetailByID(kdsPrintQueueDetailID, kdsPrintQueueDetails);
                            if (!DataFunctions.isEmptyMap(kdsPrintQueueDetail)) {
                                updatePrintStatus(kdsPrintQueueDetail, printStatus.getPrintStatusType());
                            }
                        }
                    }
                }
            }
            // create a KMS print job and add it to the queue
            if (!DataFunctions.isEmptyCollection(kmsPrintQueueDetails)) {
                Logger.logMessage("PrinterHost.queueWaitingPrintQueueDetails found kmsPrintQueueDetails", "KMS.log", Logger.LEVEL.TRACE);
                String printQueueDetailIDsStr = "";
                if ((!DataFunctions.isEmptyCollection(printJobStatusHandler.getPrinterQueueDetailIDs(kmsPrintQueueDetails)))
                        && (StringFunctions.stringHasContent(StringFunctions.buildStringFromList(printJobStatusHandler.getPrinterQueueDetailIDs(kmsPrintQueueDetails), ",")))) {
                    printQueueDetailIDsStr = StringFunctions.buildStringFromList(printJobStatusHandler.getPrinterQueueDetailIDs(kmsPrintQueueDetails), ",");
                }
                QCPrintJob kmsPrintJob = new QCPrintJob(kmsPrintQueueDetails, printerHostThread.getKDSDirectories());
                Logger.logMessage("PrinterHost.queueWaitingPrintQueueDetails kmsPrintJob created: " + kmsPrintJob.getPaPrinterQueueID() + " offering to print queue", "KMS.log", Logger.LEVEL.TRACE);
                if (PrinterQueueHandler.getInstance().offerToPrintQueue(kmsPrintJob)) {
                    Logger.logMessage(String.format("PrinterHost.queueWaitingPrintQueueDetails The kms print queue details with IDs of %s have successfully been added to the print queue in PrinterHost.queueWaitingPrintQueueDetails",
                            Objects.toString(StringFunctions.stringHasContent(printQueueDetailIDsStr) ? printQueueDetailIDsStr : "N/A", "N/A")), "KMS.log", Logger.LEVEL.TRACE);
                    // indicate that the print queue details have been added to the queue
                    PrintStatus printStatus = new PrintStatus()
                            .addPrintStatusType(PrintStatusType.SENT)
                            .addUpdatedDTM(LocalDateTime.now());
                    ArrayList<Integer> kmsPrintQueueDetailIDs = printJobStatusHandler.getPrinterQueueDetailIDs(kmsPrintQueueDetails);
                    if (!DataFunctions.isEmptyCollection(kmsPrintQueueDetailIDs)) {
                        for (int kmsPrintQueueDetailID : kmsPrintQueueDetailIDs) {
                            printJobStatusHandler.updatePrintStatusMap(kmsPrintQueueDetailID, printStatus);
                            HashMap kmsPrintQueueDetail = getPrintQueueDetailByID(kmsPrintQueueDetailID, kmsPrintQueueDetails);
                            if (!DataFunctions.isEmptyMap(kmsPrintQueueDetail)) {
                                updatePrintStatus(kmsPrintQueueDetail, printStatus.getPrintStatusType());
                            }
                        }
                    }
                }
                else {
                    Logger.logMessage(String.format("PrinterHost.queueWaitingPrintQueueDetails Unable to add the kms print queue details with IDs of %s to the print queue in PrinterHost.queueWaitingPrintQueueDetails",
                            Objects.toString(StringFunctions.stringHasContent(printQueueDetailIDsStr) ? printQueueDetailIDsStr : "N/A", "N/A")), "KMS.log", Logger.LEVEL.ERROR);
                    // indicate that the print queue details failed to be added to the queue
                    PrintStatus printStatus = new PrintStatus()
                            .addPrintStatusType(PrintStatusType.ERROR)
                            .addUpdatedDTM(LocalDateTime.now());
                    ArrayList<Integer> kmsPrintQueueDetailIDs = printJobStatusHandler.getPrinterQueueDetailIDs(kmsPrintQueueDetails);
                    if (!DataFunctions.isEmptyCollection(kmsPrintQueueDetailIDs)) {
                        for (int kmsPrintQueueDetailID : kmsPrintQueueDetailIDs) {
                            printJobStatusHandler.updatePrintStatusMap(kmsPrintQueueDetailID, printStatus);
                            HashMap kmsPrintQueueDetail = getPrintQueueDetailByID(kmsPrintQueueDetailID, kmsPrintQueueDetails);
                            if (!DataFunctions.isEmptyMap(kmsPrintQueueDetail)) {
                                updatePrintStatus(kmsPrintQueueDetail, printStatus.getPrintStatusType());
                            }
                        }
                    }
                }
            }
        }

    }

    /**
     * <p>Indicates that a print queue detail has been processed and removes it from the print status map.</p>
     *
     * @param sentPrintQueueDetails An {@link ArrayList} of {@link HashMap} corresponding to print queue details with a print status of sent.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private void processSentPrintQueueDetails (ArrayList<HashMap> sentPrintQueueDetails) {

        if (DataFunctions.isEmptyCollection(sentPrintQueueDetails)) {
            Logger.logMessage("No print queue details that have been sent have been passed to PrinterHost.processSentPrintQueueDetails!", PH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        ArrayList<Integer> printQueueDetailIDs = printJobStatusHandler.getPrinterQueueDetailIDs(sentPrintQueueDetails);
        ConcurrentHashMap<Integer, PrintStatus> printStatusMap = printJobStatusHandler.getPrintStatusMap();

        //Update print job details with the most UTD order status
        HashMap<Integer, Integer> orderDetailsStatusMap = KMSManager.getInstance().getPrintJobDetailIDsToOrderProductStatusMap();
        //add the print job's order status to the first detail: ORDERSKMSORDERSTATUSID
        int printJobID = HashMapDataFns.getIntVal(sentPrintQueueDetails.get(0), "PAPRINTERQUEUEID");
        if(printJobID > 0){
            int orderStatus = KMSManager.getInstance().getOrderStatusOfPrintJob(printJobID);
            if(orderStatus > 0){
                if(orderStatus == KMSOrderStatus.CANCELED || orderStatus == KMSOrderStatus.ERROR){//Errored or Canceled status -> Error Print Status
                    Logger.logMessage("Updating print status to ERROR for printQueueID "+printJobID+" PrinterHost.processSentPrintQueueDetails!", "ErrorPrinting.log", Logger.LEVEL.TRACE);
                    sentPrintQueueDetails.get(0).put("PRINTSTATUSID", PrintStatusType.ERROR);
                    KMSManager.getInstance().removeFinishedOrderByPrintJobID(printJobID);
                }else if(orderStatus == KMSOrderStatus.FINALIZED || orderStatus == KMSOrderStatus.REPLACED){//Finalized -> done print status
                    Logger.logMessage("Updating print status to DONE for printQueueID "+printJobID+" PrinterHost.processSentPrintQueueDetails!", "ErrorPrinting.log", Logger.LEVEL.TRACE);
                    sentPrintQueueDetails.get(0).put("PRINTSTATUSID", PrintStatusType.DONE);
                    KMSManager.getInstance().removeFinishedOrderByPrintJobID(printJobID);
                }
                sentPrintQueueDetails.get(0).put("ORDERSKMSORDERSTATUSID", orderStatus);
                //update the status of every detail
                for(int i = 1; i < sentPrintQueueDetails.size(); i++){
                    int printJobDetailID = HashMapDataFns.getIntVal(sentPrintQueueDetails.get(i), "PAPRINTERQUEUEDETAILID");
                    int detailStatus = KMSManager.getInstance().getProductStatusOfPrintJobDetail(printJobDetailID);
                    sentPrintQueueDetails.get(i).put("KMSORDERSTATUSID", detailStatus);
                }
            }else{
                Logger.logMessage("The printer host has been asked for the status of an apparently in progress order that we could not map to. Attempting remap...", Logger.LEVEL.TRACE);
                //attempt remapping of order to job
                QCPrintJob lostPrintJob = new QCPrintJob(sentPrintQueueDetails, printerHostThread.getKDSDirectories());
                if(KMSManager.getInstance().setOrderMapPrintJob(lostPrintJob)){
                    //we have mapped this print job to an order, retry to get the status
                    Logger.logMessage("Remap Successful.", Logger.LEVEL.TRACE);
                    orderStatus = KMSManager.getInstance().getOrderStatusOfPrintJob(printJobID);
                    if(orderStatus > 0){
                        if(orderStatus == KMSOrderStatus.CANCELED || orderStatus == KMSOrderStatus.ERROR){//Errored or Canceled status -> Error Print Status
                            Logger.logMessage("Updating print status to ERROR for printQueueID "+printJobID+" PrinterHost.processSentPrintQueueDetails!", "ErrorPrinting.log", Logger.LEVEL.TRACE);
                            sentPrintQueueDetails.get(0).put("PRINTSTATUSID", PrintStatusType.ERROR);
                            KMSManager.getInstance().removeFinishedOrderByPrintJobID(printJobID);
                        }else if(orderStatus == KMSOrderStatus.FINALIZED || orderStatus == KMSOrderStatus.REPLACED){//Finalized -> done print status
                            Logger.logMessage("Updating print status to DONE for printQueueID "+printJobID+" PrinterHost.processSentPrintQueueDetails!", "ErrorPrinting.log", Logger.LEVEL.TRACE);
                            sentPrintQueueDetails.get(0).put("PRINTSTATUSID", PrintStatusType.DONE);
                            KMSManager.getInstance().removeFinishedOrderByPrintJobID(printJobID);
                        }
                        sentPrintQueueDetails.get(0).put("ORDERSKMSORDERSTATUSID", orderStatus);
                    }else{
                        Logger.logMessage("No status could be found for the job " + printJobID + " in PrinterHost.processSentPrintQueueDetails", Logger.LEVEL.ERROR);
                    }
                }else{
                    //so this was previously sent, but it couldnt be mapped to an existing order...meaning we are likely waiting for the station to reupload it's state...
                    Logger.logMessage("The print job could not be remapped. We are likely waiting for the station to upload order state to the new instance of KMSManager.");
                }
            }
        }

        //process each of the order details.
        if ((!DataFunctions.isEmptyCollection(printQueueDetailIDs)) && (!DataFunctions.isEmptyMap(orderDetailsStatusMap))) {
            for (int printQueueDetailID : printQueueDetailIDs) {
                Integer KMSProductDetailOrderStatusID = null;
                if (orderDetailsStatusMap.containsKey(printQueueDetailID)) {//we have details about this, if not its not a KMSTransaction
                    KMSProductDetailOrderStatusID = orderDetailsStatusMap.get(printQueueDetailID);
                }
                if (KMSProductDetailOrderStatusID != null) {
                    HashMap printQueueDetail = getPrintQueueDetailByID(printQueueDetailID, sentPrintQueueDetails);
                    if (!DataFunctions.isEmptyMap(printQueueDetail)) {
                        updateKMSStatus(printQueueDetail, KMSProductDetailOrderStatusID);
                    }
                    Logger.logMessage(String.format("Processed the KMSOrderStatus of a print queue detail with an ID of %s in PrinterHost.processSentPrintQueueDetails",
                            Objects.toString(printQueueDetailID, "N/A")), PH_LOG, Logger.LEVEL.TRACE);
                }
            }
        }


        if ((!DataFunctions.isEmptyCollection(printQueueDetailIDs)) && (!DataFunctions.isEmptyMap(printStatusMap))) {
            for (int printQueueDetailID : printQueueDetailIDs) {
                // try to find the print queue detail within the print status map to get the print status
                PrintStatus printStatus = null;
                if (printStatusMap.containsKey(printQueueDetailID)) {
                    printStatus = printStatusMap.get(printQueueDetailID);
                }
                if (printStatus != null) {
                    HashMap printQueueDetail = getPrintQueueDetailByID(printQueueDetailID, sentPrintQueueDetails);
                    if (!DataFunctions.isEmptyMap(printQueueDetail)) {
                        updatePrintStatus(printQueueDetail, printStatus.getPrintStatusType());
                    }

                    Logger.logMessage(String.format("Processed the print queue detail with an ID of %s in PrinterHost.processSentPrintQueueDetails",
                            Objects.toString(printQueueDetailID, "N/A")), PH_LOG, Logger.LEVEL.TRACE);

                    printStatusMap.remove(printQueueDetailID);
                }
            }
        }

    }

    /**
     * <p>Updates the print statuses of the given print queue details.</p>
     *
     * @param printQueueDetail The print queue detail {@link HashMap} to update.
     * @param printStatusID The new print status to update the print queue detail to.
     */
    @SuppressWarnings({"unchecked", "TypeMayBeWeakened"})
    private void updatePrintStatus (HashMap printQueueDetail, int printStatusID) {

        if (DataFunctions.isEmptyMap(printQueueDetail)) {
            Logger.logMessage("The print queue detail passed to PrinterHost.updatePrintStatus can't be null or empty!", PH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        printQueueDetail.put("PRINTSTATUSID", printStatusID);

    }

    /**
     * <p>Updates the print statuses of the given print queue details.</p>
     *
     * @param printQueueDetail The print queue detail {@link HashMap} to update.
     * @param KMSStatusID The new print status to update the print queue detail to.
     */
    @SuppressWarnings({"unchecked", "TypeMayBeWeakened"})
    private void updateKMSStatus (HashMap printQueueDetail, int KMSStatusID) {

        if (DataFunctions.isEmptyMap(printQueueDetail)) {
            Logger.logMessage("The print queue detail passed to PrinterHost.updateKMSStatus can't be null or empty!", PH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        printQueueDetail.put("KMSORDERSTATUSID", KMSStatusID);

    }

    /**
     * <p>Updates the print statuses of the given print queue details.</p>
     *
     * @param printQueueDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details to update the print status for.
     * @param printStatusID The new print status to update the print queue details to.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private void updatePrintStatusOfPrintQueueDetails (ArrayList<HashMap> printQueueDetails, int printStatusID) {

        if (DataFunctions.isEmptyCollection(printQueueDetails)) {
            Logger.logMessage("The print queue details passed to PrinterHost.updatePrintStatusOfPrintQueueDetails can't be null or empty!", PH_LOG, Logger.LEVEL.ERROR);
            return;
        }

        for (HashMap printQueueDetail : printQueueDetails) {
            updatePrintStatus(printQueueDetail, printStatusID);
        }

    }

    /**
     * Get information (a HashMap) for the printer based on the printer ID from the QC_Printer table and use the
     * printer information to create a new QCKitchenPrinter and add it to the PrinterQueueHandler's map of printers.
     *
     * @param printerID ID of the printer we'd like to add to the PrinterQueueHandler's map of printers.
     */
    @SuppressWarnings("unchecked")
    private void createPrinter (int printerID) {

        try {
            ArrayList<HashMap> printerInfo = kitchenPrinterTerminal.getDataManager().parameterizedExecuteQuery(
                    "data.kitchenPrinter.getPrinterDefinition", new Object[]{printerID}, PH_LOG, true);
            if ((printerInfo != null) && (!printerInfo.isEmpty())) {
                PrinterQueueHandler.getInstance().addPrinterToMap(new QCKitchenPrinter(printerInfo.get(0)));
            }
        }
        catch (Exception e) {
            Logger.logException(e, PH_LOG);
            Logger.logMessage("There was a problem trying to create and add the printer with ID "+printerID+" to the " +
                    "PrinterQueueHandler's map of printers in PrinterHost.createPrinter", PH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Determines whether or not the printer host is active (the printerHostThread and printerQueueHandlerThread are
     * running).
     *
     * @return Whether or not the printer host running in this WAR is active.
     */
    public boolean isPrinterHostActive () {
        boolean isPrinterHostActive = false;

        try {
            if ((PrinterHostThread.getInstance().isActive()) && (PrinterQueueHandler.getInstance().isActive())) {
                isPrinterHostActive = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, PH_LOG);
            Logger.logMessage("There was a problem trying to determine whether or not the printer host " +
                    "running on a machine with a hostname of "+Objects.toString(PeripheralsDataManager.getPHHostname(),
                    "NULL")+" is active in PrinterHost.isPrinterHostActive", PH_LOG, Logger.LEVEL.ERROR);
        }

        return isPrinterHostActive;
    }
}
