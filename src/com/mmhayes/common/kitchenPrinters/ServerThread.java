package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: nyu $: Author of last commit
    $Date: 2019-08-16 17:08:55 -0400 (Fri, 16 Aug 2019) $: Date of last commit
    $Rev: 40901 $: Revision of last commit
    Notes: Checks if there is online orders that need to be printed.
*/

import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Singleton class checking for checking if there are online orders that need to be printed.
 *
 */
public class ServerThread implements Runnable {

    // the only instance of a ServerThread for this WAR
    private static volatile ServerThread serverThreadInstance = null;

    // log file specific to the ServerThread
    private static final String ST_LOG = "KP_ServerThread.log";

    // variables within the ServerThread scope
    private volatile boolean isShuttingDown = false;
    private volatile boolean isTerminated = false;
    private Timer successDaysToLiveTimer;

    // variables needed for ServerThread instantiation
    private KitchenPrinterTerminal kitchenPrinterTerminal;

    /**
     * Private constructor for a ServerThread.
     *
     * @param kitchenPrinterTerminal {@link KitchenPrinterTerminal} The terminal this ServerThread is running on.
     */
    private ServerThread (KitchenPrinterTerminal kitchenPrinterTerminal) {
        this.kitchenPrinterTerminal = kitchenPrinterTerminal;
    }

    /**
     * Get the only instance of ServerThread for this WAR.
     *
     * @return {@link ServerThread} The instance of ServerThread for this WAR.
     */
    public static ServerThread getInstance () {

        try {
            if (serverThreadInstance == null) {
                throw new RuntimeException("The ServerThread must be instantiated before trying to obtain its instance.");
            }
        }
        catch (Exception e) {
            Logger.logException(e, ST_LOG);
            Logger.logMessage("There was a problem trying to get the only instance of ServerThread for this WAR in " +
                    "ServerThread.getInstance", ST_LOG, Logger.LEVEL.ERROR);
        }

        return serverThreadInstance;
    }

    /**
     * Instantiate and return the only instance of ServerThread for this WAR.
     *
     * @param kitchenPrinterTerminal {@link KitchenPrinterTerminal} The terminal this ServerThread is running on.
     * @return {@link ServerThread} The instance of ServerThread for this WAR.
     */
    public static synchronized ServerThread init (KitchenPrinterTerminal kitchenPrinterTerminal) {

        try {
            if (serverThreadInstance != null) {
                throw new RuntimeException("The ServerThread has already been instantiated and may not be instantiated " +
                        "again.");
            }

            // create the instance
            serverThreadInstance = new ServerThread(kitchenPrinterTerminal);
            Logger.logMessage("The instance of ServerThread has been instantiated", ST_LOG, Logger.LEVEL.TRACE);
        }
        catch (Exception e) {
            Logger.logException(e, ST_LOG);
            Logger.logMessage("There was a problem trying to instantiate and return the only instance of ServerThread " +
                    "for this WAR in ServerThread.init", ST_LOG, Logger.LEVEL.ERROR);
        }

        return serverThreadInstance;
    }

    /**
     * Method that will be called by a Thread created using the ServerThread Runnable.
     *
     */
    @Override
    public void run () {
        // start the timer
        successDaysToLiveTimer = new Timer();
        // based on the app property "site.kitchenPrinter.successDaysToLive" remove any print jobs from the
        // QC_PAPrinterQueue table that are older than the property
        successDaysToLiveTimer.schedule(new TimerTask() {
            @Override
            public void run () {
                try {
                    String daysToLive = MMHProperties.getAppSetting("site.kitchenPrinter.successDaysToLive");
                    if ((daysToLive != null) && (!daysToLive.isEmpty())) {
                        deleteOldSuccessfulPrintJobs(daysToLive);
                    }
                }
                catch (Exception e) {
                    Logger.logException(e, ST_LOG);
                    Logger.logMessage("There was a problem in the timer task to remove old successful print jobs in " +
                            "ServerThread.run", ST_LOG, Logger.LEVEL.ERROR);
                }
            }
        }, 0, TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));

        try {
            // wait 5 seconds for the web service to accept connections
            Thread.sleep(5000L);

            Thread.currentThread().setName("KitchenPrinter-ServerThread");

            while ((!isShuttingDown) && (!Thread.currentThread().isInterrupted())) {
                try {
                    ArrayList printData;

                    // get any print jobs with a print job status of queued or waiting from the local database
                    printData = kitchenPrinterTerminal.getDataManager().parameterizedExecuteQuery(
                            "data.newKitchenPrinter.SelectUnprintedOnlineOrders", new Object[]{},
                            ST_LOG, true);

                    // if there are print jobs that have a print job status of waiting check for expired or errored print
                    // jobs and send an email for those print jobs
                    if ((printData != null) && (!printData.isEmpty())) {
                        kitchenPrinterTerminal.getPeripheralsDataManager().checkForExpiredOrErroredJobsAndSendEmail(printData);
                    } else {
                        Thread.sleep(kitchenPrinterTerminal.dbPollingIntervalInSeconds() * 1000L);
                        continue;
                    }
                }
                catch (Exception e) {
                    Logger.logException(e, ST_LOG);
                    Logger.logMessage("There was a problem in the while loop of ServerThread.run", ST_LOG, Logger.LEVEL.ERROR);
                }
                Thread.sleep(kitchenPrinterTerminal.dbPollingIntervalInSeconds() * 1000L);
            }

        }
        catch (Exception e) {
            Logger.logException(e, ST_LOG);
            Logger.logMessage("There was a problem in ServerThread.run", ST_LOG, Logger.LEVEL.ERROR);
        }
        finally {
            isTerminated = true;
            Logger.logMessage("KitchenPrinter-ServerThread is exiting", ST_LOG, Logger.LEVEL.TRACE);
        }
    }

    /**
     * Remove any old print jobs that were successful and are older than the number of daysToLive.
     *
     * @param daysToLive {@link String} The number of days to keep the print job around for.
     */
    private void deleteOldSuccessfulPrintJobs (String daysToLive) {

        try {
            int queryRes = kitchenPrinterTerminal.getDataManager().parameterizedExecuteNonQuery(
                    "data.kitchenPrinter.deleteOldSuccessJobDetails", new Object[]{Integer.parseInt(daysToLive)}, ST_LOG);
            if (queryRes < 0) {
                Logger.logMessage("The query to remove print jobs that were successful from the QC_PAPrinterQueueDetail " +
                        "table and are older than "+Objects.toString(daysToLive, "NULL")+" days failed in " +
                        "ServerThread.deleteOldSuccessfulPrintJobs", ST_LOG, Logger.LEVEL.ERROR);
            }
            queryRes = kitchenPrinterTerminal.getDataManager().parameterizedExecuteNonQuery(
                    "data.kitchenPrinter.deleteOldSuccessJobs", new Object[]{Integer.parseInt(daysToLive)}, ST_LOG);
            if (queryRes < 0) {
                Logger.logMessage("The query to remove print jobs that were successful from the QC_PAPrinterQueue table " +
                        "and are older than "+Objects.toString(daysToLive, "NULL")+" days failed in " +
                        "ServerThread.deleteOldSuccessfulPrintJobs", ST_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, ST_LOG);
            Logger.logMessage("There was a problem trying to remove print jobs that were successful and are older " +
                    "than "+ Objects.toString(daysToLive, "NULL")+" days in ServerThread.deleteOldSuccessfulPrintJobs",
                    ST_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Creates and starts a Thread using the ServerThread Runnable.
     *
     */
    public void startServerThread () {

        try {
            Thread serverThread = new Thread(this);
            serverThread.start();
            Logger.logMessage("The ServerThread has started successfully in ServerThread,startServerThread", ST_LOG,
                    Logger.LEVEL.TRACE);
        }
        catch (Exception e) {
            Logger.logException(e, ST_LOG);
            Logger.logMessage("There was a problem trying to start the ServerThread in ServerThread.startServerThread",
                    ST_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Stops the ServerThread.
     *
     */
    public void stopServerThread () {

        try {
            // run through Thread shutdown routine
            isShuttingDown = true;

            int waitCount = 0;
            while (!isTerminated) {
                waitCount++;
                // wait a second
                Thread.sleep(1000L);
                if (waitCount >= 3) {
                    Logger.logMessage("The ServerThread didn't stop within the specified wait time, it " +
                            "is being interrupted now in ServerThread.stopServerThread", ST_LOG,
                            Logger.LEVEL.TRACE);
                    // interrupt the PrinterQueueHandler Thread
                    Thread.getAllStackTraces().keySet().forEach((thread) -> {
                        if (thread.getName().equalsIgnoreCase("KitchenPrinter-ServerThread")) {
                            thread.interrupt();
                        }
                    });
                }
            }

            Logger.logMessage("The ServerThread has been stopped", ST_LOG, Logger.LEVEL.IMPORTANT);
        }
        catch (Exception e) {
            Logger.logException(e, ST_LOG);
            Logger.logMessage("There was a problem trying to stop the ServerThread in " +
                    "ServerThread.stopServerThread", ST_LOG, Logger.LEVEL.ERROR);
        }

    }

}
