package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-10-30 13:50:13 -0400 (Fri, 30 Oct 2020) $: Date of last commit
    $Rev: 13005 $: Revision of last commit
    Notes: Represents a kitchen print job that will be printed locally, emailed, or both.
*/

import com.mmhayes.common.kms.KMSCommon;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class LocalPrintJob {

    // private member variables of a LocalPrintJob
    private int paPrinterQueueID = -1;
    private int terminalID = -1;
    private int revenueCenterID = -1;
    private int printerID = -1;
    private int kmsStationID = -1;
    private boolean isKDS = false;
    private boolean isOnlineOrder = false;
    private ArrayList<HashMap> details = new ArrayList<>();

    /**
     * <p>Empty constructor for a {@link LocalPrintJob}.</p>
     *
     */
    public LocalPrintJob () {}

    /**
     * <p>Constructor for a {@link LocalPrintJob} with it's paPrinterQueueID field set.</p>
     *
     * @param paPrinterQueueID The ID of the print queue header record for the print job.
     * @return The {@link LocalPrintJob} instance with it's paPrinterQueueID field set.
     */
    public LocalPrintJob paPrinterQueueID (int paPrinterQueueID) {
        this.paPrinterQueueID = paPrinterQueueID;
        return this;
    }

    /**
     * <p>Constructor for a {@link LocalPrintJob} with it's terminalID field set.</p>
     *
     * @param terminalID ID of the terminal on which the transaction took place to make the print job.
     * @return The {@link LocalPrintJob} instance with it's terminalID field set.
     */
    public LocalPrintJob terminalID (int terminalID) {
        this.terminalID = terminalID;
        return this;
    }

    /**
     * <p>Constructor for a {@link LocalPrintJob} with it's revenueCenterID field set.</p>
     *
     * @param revenueCenterID ID of the revenue center in which the transaction to make the print job was made.
     * @return The {@link LocalPrintJob} instance with it's revenueCenterID field set.
     */
    public LocalPrintJob revenueCenterID (int revenueCenterID) {
        this.revenueCenterID = revenueCenterID;
        return this;
    }

    /**
     * <p>Constructor for a {@link LocalPrintJob} with it's printerID field set.</p>
     *
     * @param printerID The ID of the kitchen printer or KDS station that should have originally displayed the print job.
     * @return The {@link LocalPrintJob} instance with it's printerID field set.
     */
    public LocalPrintJob printerID (int printerID) {
        this.printerID = printerID;
        return this;
    }

    /**
     * <p>Constructor for a {@link LocalPrintJob} with it's kmsStationID field set.</p>
     *
     * @param kmsStationID The ID of the KMS station that should have originally displayed the print job.
     * @return The {@link LocalPrintJob} instance with it's kmsStationID field set.
     */
    public LocalPrintJob kmsStationID (int kmsStationID) {
        this.kmsStationID = kmsStationID;
        return this;
    }

    /**
     * <p>Constructor for a {@link LocalPrintJob} with it's isKDS field set.</p>
     *
     * @param isKDS Whether or not the print job should've been sent to KDS.
     * @return The {@link LocalPrintJob} instance with it's isKDS field set.
     */
    public LocalPrintJob isKDS (boolean isKDS) {
        this.isKDS = isKDS;
        return this;
    }

    /**
     * <p>Constructor for a {@link LocalPrintJob} with it's isOnlineOrder field set.</p>
     *
     * @param isOnlineOrder Whether or not the print job was generated for an online order.
     * @return The {@link LocalPrintJob} instance with it's isOnlineOrder field set.
     */
    public LocalPrintJob isOnlineOrder (boolean isOnlineOrder) {
        this.isOnlineOrder = isOnlineOrder;
        return this;
    }



    /**
     * <p>Constructor for a {@link LocalPrintJob} with it's details field set.</p>
     *
     * @param details An {@link ArrayList} of {@link HashMap} corresponding to the details to add to the receipt.
     * @return The {@link LocalPrintJob} instance with it's details field set.
     */
    public LocalPrintJob details (ArrayList<HashMap> details) {
        this.details = details;
        return this;
    }

    /**
     * <p>Adds a detail to this {@link LocalPrintJob} details.</p>
     *
     * @param key A generic key to add to the detail {@link HashMap}.
     * @param value A generic value to add to the detail {@link HashMap} at the specified key.
     * @param detailHM The {@link HashMap} detail to add.
     */
    @SuppressWarnings("unchecked")
    public <K, V> void addDetail (K key, V value, HashMap detailHM) {

        // make sure the detail make be added
        if (details == null) {
            details = new ArrayList<>();
        }

        // validate the key
        if ((key == null) || ((key instanceof String) && (!StringFunctions.stringHasContent(key.toString())))) {
            Logger.logMessage("The key passed to LocalPrintJob.addDetail is invalid, unable to add the detail to the LocalPrintJob's details, now returning from the method.", Logger.LEVEL.ERROR);
            return;
        }

        // validate the detail HashMap
        if (DataFunctions.isEmptyMap(detailHM)) {
            Logger.logMessage("The detail HashMap passed to LocalPrintJob.addDetail can't be null or empty, unable to add the " +
                    "detail to the LocalPrintJob's details, now returning from the method.", Logger.LEVEL.ERROR);
            return;
        }

        detailHM.put(key, KMSCommon.base64Encode(value.toString()));
        details.add(detailHM);
    }

    /**
     * <p>Adds a detail to this {@link LocalPrintJob} details.</p>
     *
     * @param index The index at which to insert the detail into the details.
     * @param key A generic key to add to the detail {@link HashMap}.
     * @param value A generic value to add to the detail {@link HashMap} at the specified key.
     * @param detailHM The {@link HashMap} detail to add.
     */
    @SuppressWarnings("unchecked")
    public <K, V> void addDetail (int index, K key, V value, HashMap detailHM) {

        // make sure the detail make be added
        if (details == null) {
            details = new ArrayList<>();
        }

        // validate the index
        if ((index < 0) || ((details.size() == 0) && (index > 0)) || ((!DataFunctions.isEmptyCollection(details)) && (index > details.size()))) {
            Logger.logMessage(String.format("The index of %s passed to LocalPrintJob.addDetail is invalid, unable to add the detail to the LocalPrintJob's details, now returning from the method.",
                    Objects.toString(index, "N/A")), Logger.LEVEL.ERROR);
            return;
        }

        // validate the key
        if ((key == null) || ((key instanceof String) && (!StringFunctions.stringHasContent(key.toString())))) {
            Logger.logMessage("The key passed to LocalPrintJob.addDetail is invalid, unable to add the detail to the LocalPrintJob's details, now returning from the method.", Logger.LEVEL.ERROR);
            return;
        }

        // validate the detail HashMap
        if (DataFunctions.isEmptyMap(detailHM)) {
            Logger.logMessage("The detail HashMap passed to LocalPrintJob.addDetail can't be null or empty, unable to add the " +
                    "detail to the LocalPrintJob's details, now returning from the method.", Logger.LEVEL.ERROR);
            return;
        }

        detailHM.put(key, KMSCommon.base64Encode(value.toString()));
        details.add(index, detailHM);
    }

    /**
     * <p>Overridden toString method for a {@link LocalPrintJob}.</p>
     *
     * @return The {@link String} representation of a {@link LocalPrintJob};
     */
    @Override
    public String toString () {

        // create a String of the details
        String detailsStr = "";
        if (!DataFunctions.isEmptyCollection(details)) {
            detailsStr += "[";
            for (HashMap detail : details) {
                if (!DataFunctions.isEmptyMap(detail)) {
                    if (detail.equals(details.get(details.size() - 1))) {
                        detailsStr += HashMapDataFns.getPrettyMapStr(detail);
                    }
                    else {
                        detailsStr += HashMapDataFns.getPrettyMapStr(detail) + ", ";
                    }
                }
            }
            detailsStr += "]";
        }

        return String.format("PAPRINTERQUEUEID: %s, TERMINALID: %s, REVENUECENTERID: %s, PRINTERID: %s, KMSSTATIONID: %s, ISKDS: %s, ISONLINEORDER: %s, DETAILS: %s",
                Objects.toString((paPrinterQueueID > 0 ? paPrinterQueueID : "N/A"), "N/A"),
                Objects.toString((terminalID > 0 ? terminalID : "N/A"), "N/A"),
                Objects.toString((revenueCenterID > 0 ? revenueCenterID : "N/A"), "N/A"),
                Objects.toString((printerID > 0 ? printerID : "N/A"), "N/A"),
                Objects.toString((kmsStationID > 0 ? kmsStationID : "N/A"), "N/A"),
                Objects.toString(isKDS, "N/A"),
                Objects.toString(isOnlineOrder, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(detailsStr) ? detailsStr : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals method for a {@link LocalPrintJob}.</p>
     *
     * @param obj The {@link Object} to compare against this {@link LocalPrintJob} instance.
     * @return Whether or not the given {@link Object} is equal to this {@link LocalPrintJob} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a LocalPrintJob and check if the obj's paPrinterQueueID and printer or KMS stations ID is equal to this LocalPrintJobs's
        LocalPrintJob localPrintJob = ((LocalPrintJob) obj);
        if (printerID > 0) {
            return ((Objects.equals(localPrintJob.paPrinterQueueID, paPrinterQueueID)) && (Objects.equals(localPrintJob.printerID, printerID)));
        }
        else {
            return ((Objects.equals(localPrintJob.paPrinterQueueID, paPrinterQueueID)) && (Objects.equals(localPrintJob.kmsStationID, kmsStationID)));
        }
    }

    /**
     * <p>Overridden hashCode method for a {@link LocalPrintJob}.</p>
     *
     * @return The unique hash code for this {@link LocalPrintJob}.
     */
    @Override
    public int hashCode () {
        if (printerID > 0) {
            return Objects.hash(paPrinterQueueID, printerID);
        }
        else {
            return Objects.hash(paPrinterQueueID, kmsStationID);
        }
    }

    /**
     * <p>Getter for the paPrinterQueueID field of the {@link LocalPrintJob}.</p>
     *
     * @return The paPrinterQueueID field of the {@link LocalPrintJob}.
     */
    public int getPAPrinterQueueID () {
        return paPrinterQueueID;
    }

    /**
     * <p>Setter for the paPrinterQueueID field of the {@link LocalPrintJob}.</p>
     *
     * @param paPrinterQueueID The paPrinterQueueID field of the {@link LocalPrintJob}.
     */
    public void setPAPrinterQueueID (int paPrinterQueueID) {
        this.paPrinterQueueID = paPrinterQueueID;
    }

    /**
     * <p>Getter for the terminalID field of the {@link LocalPrintJob}.</p>
     *
     * @return The terminalID field of the {@link LocalPrintJob}.
     */
    public int getTerminalID () {
        return terminalID;
    }

    /**
     * <p>Setter for the terminalID field of the {@link LocalPrintJob}.</p>
     *
     * @param terminalID The terminalID field of the {@link LocalPrintJob}.
     */
    public void setTerminalID (int terminalID) {
        this.terminalID = terminalID;
    }

    /**
     * <p>Getter for the revenueCenterID field of the {@link LocalPrintJob}.</p>
     *
     * @return The revenueCenterID field of the {@link LocalPrintJob}.
     */
    public int getRevenueCenterID () {
        return revenueCenterID;
    }

    /**
     * <p>Setter for the revenueCenterID field of the {@link LocalPrintJob}.</p>
     *
     * @param revenueCenterID The revenueCenterID field of the {@link LocalPrintJob}.
     */
    public void setRevenueCenterID (int revenueCenterID) {
        this.revenueCenterID = revenueCenterID;
    }

    /**
     * <p>Getter for the printerID field of the {@link LocalPrintJob}.</p>
     *
     * @return The printerID field of the {@link LocalPrintJob}.
     */
    public int getPrinterID () {
        return printerID;
    }

    /**
     * <p>Setter for the printerID field of the {@link LocalPrintJob}.</p>
     *
     * @param printerID The printerID field of the {@link LocalPrintJob}.
     */
    public void setPrinterID (int printerID) {
        this.printerID = printerID;
    }

    /**
     * <p>Getter for the kmsStationID field of the {@link LocalPrintJob}.</p>
     *
     * @return The kmsStationID field of the {@link LocalPrintJob}.
     */
    public int getKMSStationID () {
        return kmsStationID;
    }

    /**
     * <p>Setter for the kmsStationID field of the {@link LocalPrintJob}.</p>
     *
     * @param kmsStationID The kmsStationID field of the {@link LocalPrintJob}.
     */
    public void setKMSStationID (int kmsStationID) {
        this.kmsStationID = kmsStationID;
    }

    /**
     * <p>Getter for the isKDS field of the {@link LocalPrintJob}.</p>
     *
     * @return The isKDS field of the {@link LocalPrintJob}.
     */
    public boolean getIsKDS () {
        return isKDS;
    }

    /**
     * <p>Setter for the isKDS field of the {@link LocalPrintJob}.</p>
     *
     * @param isKDS The isKDS field of the {@link LocalPrintJob}.
     */
    public void setIsKDS (boolean isKDS) {
        this.isKDS = isKDS;
    }

    /**
     * <p>Getter for the isOnlineOrder field of the {@link LocalPrintJob}.</p>
     *
     * @return The isOnlineOrder field of the {@link LocalPrintJob}.
     */
    public boolean getIsOnlineOrder () {
        return isOnlineOrder;
    }

    /**
     * <p>Setter for the isOnlineOrder field of the {@link LocalPrintJob}.</p>
     *
     * @param isOnlineOrder The isOnlineOrder field of the {@link LocalPrintJob}.
     */
    public void setIsOnlineOrder (boolean isOnlineOrder) {
        this.isOnlineOrder = isOnlineOrder;
    }

    /**
     * <p>Getter for the details field of the {@link LocalPrintJob}.</p>
     *
     * @return The details field of the {@link LocalPrintJob}.
     */
    public ArrayList<HashMap> getDetails () {
        return details;
    }

    /**
     * <p>Setter for the details field of the {@link LocalPrintJob}.</p>
     *
     * @param details The details field of the {@link LocalPrintJob}.
     */
    public void setDetails (ArrayList<HashMap> details) {
        this.details = details;
    }

}