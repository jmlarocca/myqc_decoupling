package com.mmhayes.common.kitchenPrinters;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2019-04-11 16:17:56 -0400 (Thu, 11 Apr 2019) $: Date of last commit
    $Rev: 37836 $: Revision of last commit
    Notes: Representation of a printer host in the QC WEB back office.
*/

import com.mmhayes.common.receiptGen.HashMapDataFns;

import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

/**
 * Representation of a printer host in the QC WEB back office. It aligns with a record in the QC_PrintController table.
 *
 */
public class QCPrinterHost {

    // QCPrinterHost fields
    private int printerHostID;
    private String hostname;
    private String description;
    private boolean active;
    private int ownershipGroupID;
    private int pollFrequencySecs;
    private String macAddress;
    private Date lastOnlineOrderCheck;
    private boolean isPrimary;
    private int healthCheckFrequency;
    private int primaryPrinterHostID;

    /**
     * Constructor for the QCPrinterHost. It converts the fields for a record in the QC_PrintController table to their
     * corresponding field in the QCPrinterHost object.
     *
     * @param printerHostRecord {@link HashMap} HashMap that corresponds to a row in the QC_PrintController table.
     */
    public QCPrinterHost (HashMap printerHostRecord) {
        printerHostID = HashMapDataFns.getIntVal(printerHostRecord, "PRINTCONTROLLERID");
        hostname = HashMapDataFns.getStringVal(printerHostRecord, "HOSTNAME");
        description = HashMapDataFns.getStringVal(printerHostRecord, "DESCRIPTION");
        active = HashMapDataFns.getBooleanVal(printerHostRecord, "ACTIVE");
        ownershipGroupID = HashMapDataFns.getIntVal(printerHostRecord, "OWNERSHIPGROUPID");
        pollFrequencySecs = HashMapDataFns.getIntVal(printerHostRecord, "POLLFREQUENCYSECS");
        macAddress = HashMapDataFns.getStringVal(printerHostRecord, "MACADDRESS");
        lastOnlineOrderCheck = HashMapDataFns.getDateVal(printerHostRecord, "LASTONLINEORDERCHECK");
        isPrimary = HashMapDataFns.getBooleanVal(printerHostRecord, "ISPRIMARY");
        healthCheckFrequency = HashMapDataFns.getIntVal(printerHostRecord, "HEALTHCHECKFREQUENCYSECONDS");
        primaryPrinterHostID = HashMapDataFns.getIntVal(printerHostRecord, "PRIMARYPRINTCONTROLLERID");
    }

    /**
     * Setter for the QCPrinterHost's printerHostID field.
     *
     * @param printerHostID ID of the printer host.
     */
    public void setPrinterHostID (int printerHostID) {
        this.printerHostID = printerHostID;
    }

    /**
     * Getter for the QCPrinterHost's printerHostID field.
     *
     * @return ID of the printer host.
     */
    public int getPrinterHostID () {
        return printerHostID;
    }

    /**
     * Setter for the QCPrinterHost's hostname field.
     *
     * @param hostname {@link String} Hostname of the printer host.
     */
    public void setHostname (String hostname) {
        this.hostname = hostname;
    }

    /**
     * Getter for the QCPrinterHost's hostname field.
     *
     * @return {@link String} Hostname of the printer host.
     */
    public String getHostname () {
        return hostname;
    }

    /**
     * Setter for the QCPrinterHost's description field.
     *
     * @param description {@link String} Description of the printer host.
     */
    public void setDescription (String description) {
        this.description = description;
    }

    /**
     * Getter for the QCPrinterHost's description field.
     *
     * @return {@link String} Description of the printer host.
     */
    public String getDescription () {
        return description;
    }

    /**
     * Setter for the QCPrinterHost's active field.
     *
     * @param active Whether or no the printer host is active.
     */
    public void setActive (boolean active) {
        this.active = active;
    }

    /**
     * Getter for the QCPrinterHost's active field.
     *
     * @return Whether or no the printer host is active.
     */
    public boolean isActive () {
        return active;
    }

    /**
     * Setter for the QCPrinterHost's ownershipGroupID field.
     *
     * @param ownershipGroupID Ownership group ID of the printer host.
     */
    public void setOwnershipGroupID (int ownershipGroupID) {
        this.ownershipGroupID = ownershipGroupID;
    }

    /**
     * Getter for the QCPrinterHost's ownershipGroupID field.
     *
     * @return Ownership group ID of the printer host.
     */
    public int getOwnershipGroupID () {
        return ownershipGroupID;
    }

    /**
     * Setter for the QCPrinterHost's pollFrequencySecs field.
     *
     * @param pollFrequencySecs Polling frequency in seconds for the printer host.
     */
    public void setPollFrequencySecs (int pollFrequencySecs) {
        this.pollFrequencySecs = pollFrequencySecs;
    }

    /**
     * Getter for the QCPrinterHost's pollFrequencySecs field.
     *
     * @return Polling frequency in seconds for the printer host.
     */
    public int getPollFrequencySecs () {
        return pollFrequencySecs;
    }

    /**
     * Setter for the QCPrinterHost's macAddress field.
     *
     * @param macAddress {@link String} MAC address of the printer host.
     */
    public void setMacAddress (String macAddress) {
        this.macAddress = macAddress;
    }

    /**
     * Getter for the QCPrinterHost's macAddress field.
     *
     * @return {@link String} MAC address of the printer host.
     */
    public String getMacAddress () {
        return macAddress;
    }

    /**
     * Setter for the QCPrinterHost's lastOnlineOrderCheck field.
     *
     * @param lastOnlineOrderCheck {@link Date} Last time this printer host checked for online orders.
     */
    public void setLastOnlineOrderCheck (Date lastOnlineOrderCheck) {
        this.lastOnlineOrderCheck = lastOnlineOrderCheck;
    }

    /**
     * Setter for the QCPrinterHost's lastOnlineOrderCheck field.
     *
     * @return {@link Date} Last time this printer host checked for online orders.
     */
    public Date getLastOnlineOrderCheck () {
        return lastOnlineOrderCheck;
    }

    /**
     * Setter for the QCPrinterHost's isPrimary field.
     *
     * @param primary Whether or not this printer host should start up as the primary printer host.
     */
    public void setPrimary (boolean primary) {
        isPrimary = primary;
    }

    /**
     * Getter for the QCPrinterHost's isPrimary field.
     *
     * @return Whether or not this printer host should start up as the primary printer host.
     */
    public boolean isPrimary () {
        return isPrimary;
    }

    /**
     * Setter for the QCPrinterHost's healthCheckFrequency field.
     *
     * @param healthCheckFrequency How frequently this print controller should check the health of other printer hosts
     *         in seconds.
     */
    public void setHealthCheckFrequency (int healthCheckFrequency) {
        this.healthCheckFrequency = healthCheckFrequency;
    }

    /**
     * Getter for the QCPrinterHost's healthCheckFrequency field.
     *
     * @return How frequently this print controller should check the health of other printer hosts in seconds.
     */
    public int getHealthCheckFrequency () {
        return healthCheckFrequency;
    }

    /**
     * Setter for the QCPrinterHost's primaryPrinterHostID field.
     *
     * @param primaryPrinterHostID If this printer host isn't configured as the primary, then it's the ID of the
     *         printer host that is configured as the primary.
     */
    public void setPrimaryPrinterHostID (int primaryPrinterHostID) {
        this.primaryPrinterHostID = primaryPrinterHostID;
    }

    /**
     * Getter for the QCPrinterHost's primaryPrinterHostID field.
     *
     * @return If this printer host isn't configured as the primary, then it's the ID of the
     *         printer host that is configured as the primary.
     */
    public int getPrimaryPrinterHostID () {
        return primaryPrinterHostID;
    }

    /**
     * Overridden toString method for a QCPrinterHost.
     *
     */
    @Override
    public String toString () {
        return String.format("PRINTERHOSTID: %s, HOSTNAME: %s, DESCRIPTION: %s, ACTIVE: %s, OWNERSHIPGROUPID: %s, " +
                "POLLFREQUENCYSECS: %s, MACADDRESS: %s, LASTONLINEORDERCHECK: %s, ISPRIMARY: %s, " +
                "HEALTHCHECKFREQUENCY: %s, PRIMARYPRINTERHOSTID: %s",
                Objects.toString(printerHostID, "NULL"),
                Objects.toString(hostname, "NULL"),
                Objects.toString(description, "NULL"),
                Objects.toString(active, "NULL"),
                Objects.toString(ownershipGroupID, "NULL"),
                Objects.toString(pollFrequencySecs, "NULL"),
                Objects.toString(macAddress, "NULL"),
                Objects.toString(lastOnlineOrderCheck, "NULL"),
                Objects.toString(isPrimary, "NULL"),
                Objects.toString(healthCheckFrequency, "NULL"),
                Objects.toString(primaryPrinterHostID, "NULL"));
    }

    /**
     * Override the QCPrinterHost's equals method to define two QCPrinterHosts as being equal if they have the same
     * printer host ID.
     *
     * @param o {@link Object} Instance of QCPrinterHost we are comparing against.
     * @return Whether or not the QCPrinterHosts are equal.
     */
    @Override
    public boolean equals (Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof QCPrinterHost)) {
            return false;
        }

        // check if the instances printerHostID is equal to this QCPrinterHost's printerHostID
        QCPrinterHost qcPrinterHost = (QCPrinterHost) o;
        return (qcPrinterHost.getPrinterHostID() == printerHostID);

    }

    /**
     * Since we are overriding the QCPrinterHost's equals method we must also override the QCPrinterHost's hashCode
     * method. We are trying to create a unique hashcode the easiest way to do this is to use two prime numbers. I'm
     * using 17 and 31 in this case.
     *
     * @return A unique hashcode value.
     */
    @Override
    public int hashCode () {

        return ((31 * 17) + printerHostID);

    }

}
