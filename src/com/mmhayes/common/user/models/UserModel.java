package com.mmhayes.common.user.models;

//MMHayes Dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;

//API Dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2018-01-22 11:51:45 -0500 (Mon, 22 Jan 2018) $: Date of last commit
 $Rev: 10596 $: Revision of last commit
*/

public class UserModel {
    private Integer id = null;
    private String name = "";
    private String payrollGroupIds = "";
    private Boolean active = false;
    private Boolean locked = false;
    private Boolean canCreate = false;
    private Boolean canViewEdit = false;
    private Boolean canSuspendTransaction = false;

    public UserModel() {

    }

    public static UserModel getOneUserModelById(Integer userId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> userList = dm.parameterizedExecuteQuery("data.posapi30.getOneUserById",
                new Object[]{userId},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(userList, "User Not Found", terminalModel.getId());

        UserModel userModel = new UserModel();
        userModel.setModelProperties(userList.get(0));
        return userModel;
    }

    public static UserModel getOneUserModelByBadge(String badge, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> userList = dm.parameterizedExecuteQuery("data.posapi30.getOneUserByBadge",
                new Object[]{badge},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(userList, "User Not Found", terminalModel.getId());

        UserModel userModel = new UserModel();
        userModel.setModelProperties(userList.get(0));
        return userModel;
    }

    //setter for all of this model's properties
    public UserModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setActive(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ACTIVE"), false));
        setLocked(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("LOCKED"), false));
        setPayrollGroupIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYROLLGROUPIDS")));
        setCanCreate(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("CANCREATE"), false));
        setCanViewEdit(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("CANVIEWEDIT"), false));
        setCanSuspendTransaction(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("CANSUSPENDTRANSACTION"), false));

        return this;
    }

    public Boolean canUserIdAccessAccountGroup(TerminalModel terminalModel, AccountModel accountModel) {
        Boolean canUserAccessAccountGroup = false;

        if (accountModel == null || accountModel.getPayrollGroupingId() == null || accountModel.getPayrollGroupingId().toString().isEmpty()) {
            canUserAccessAccountGroup = false;
        }

        if (this.getPayrollGroupIds() == null || this.getPayrollGroupIds().toString().isEmpty()) {
            return false;
        }

        if (this.getPayrollGroupIds().equals("-1")) {
            return true;
        }

        if (this.getPayrollGroupIds().contains(accountModel.getPayrollGroupingId().toString())) {
            return true;
        }

        return canUserAccessAccountGroup;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPayrollGroupIds() {
        return payrollGroupIds;
    }

    public void setPayrollGroupIds(String payrollGroupIds) {
        this.payrollGroupIds = payrollGroupIds;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Boolean getCanCreate() {
        return canCreate;
    }

    public void setCanCreate(Boolean canCreate) {
        this.canCreate = canCreate;
    }

    public Boolean getCanViewEdit() {
        return canViewEdit;
    }

    public void setCanViewEdit(Boolean canViewEdit) {
        this.canViewEdit = canViewEdit;
    }

    public Boolean canSuspendTransaction() {
        return canSuspendTransaction;
    }

    public void setCanSuspendTransaction(Boolean canSuspendTransaction) {
        this.canSuspendTransaction = canSuspendTransaction;
    }
}
