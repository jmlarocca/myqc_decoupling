package com.mmhayes.common.giftcards;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class GiftCardSetupModel {
    private Integer id;
    private String name;
    private GiftCardProcessorModel giftCardProcessor;

    //private fields not sent or recieved in JSON
    @JsonIgnore
    private String firstDataMerchantID;
    @JsonIgnore
    private String firstDataServiceURLs;
    @JsonIgnore
    private String firstDAtaServiceID;
    @JsonIgnore
    private String firstDataApplicationID;
    @JsonIgnore private static DataManager dm = new DataManager();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GiftCardProcessorModel getGiftCardProcessor() {
        return giftCardProcessor;
    }

    public void setGiftCardProcessor(GiftCardProcessorModel giftCardProcessor) {
        this.giftCardProcessor = giftCardProcessor;
    }

    public String getFirstDataMerchantID() {
        return firstDataMerchantID;
    }

    public void setFirstDataMerchantID(String firstDataMerchantID) {
        this.firstDataMerchantID = firstDataMerchantID;
    }

    public String getFirstDataServiceURLs() {
        return firstDataServiceURLs;
    }

    public void setFirstDataServiceURLs(String firstDataServiceURLs) {
        this.firstDataServiceURLs = firstDataServiceURLs;
    }

    public String getFirstDAtaServiceID() {
        return firstDAtaServiceID;
    }

    public void setFirstDAtaServiceID(String firstDAtaServiceID) {
        this.firstDAtaServiceID = firstDAtaServiceID;
    }

    public String getFirstDataApplicationID() {
        return firstDataApplicationID;
    }

    public void setFirstDataApplicationID(String firstDataApplicationID) {
        this.firstDataApplicationID = firstDataApplicationID;
    }


    public GiftCardSetupModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setFirstDataApplicationID(CommonAPI.convertModelDetailToString(modelDetailHM.get("FIRSTDATAAPPLICATIONID")));
        setFirstDataMerchantID(CommonAPI.convertModelDetailToString(modelDetailHM.get("FIRSTDATAMERCHANTID")));
        setFirstDAtaServiceID(CommonAPI.convertModelDetailToString(modelDetailHM.get("FIRSTDATASERVICEID")));
        setFirstDataServiceURLs(CommonAPI.convertModelDetailToString(modelDetailHM.get("FIRSTDATASERVICEURLS")));

        //TODO: do we need to get this?
        int giftCardProcessorID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("GIFTCARDPROCESSORID"));


        return this;
    }

    //populates this model by querying the database
    private GiftCardSetupModel populateModel() throws Exception {

        //get all models in an array list
        ArrayList<HashMap> details = dm.parameterizedExecuteQuery("data.common.populateGiftCardSetupModel",
                new Object[]{
                        this.id
                },
                true
        );

        //automatic funding list should only contain one account which we will use to populate this model
        if (details != null && details.size() == 1) {
            HashMap modelDetailHM = details.get(0);
            setModelProperties(modelDetailHM);
        } else {
            throw new MissingDataException();
        }

        return this;
    }
}
