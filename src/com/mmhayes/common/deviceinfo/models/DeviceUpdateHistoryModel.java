package com.mmhayes.common.deviceinfo.models;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-04-09 16:17:38 -0400 (Thu, 09 Apr 2020) $: Date of last commit
 $Rev: 11307 $: Revision of last commit
*/
public class DeviceUpdateHistoryModel {
    private Integer id = null;
    private String lastChecked = "";
    private String lastInstalled = "";

    public DeviceUpdateHistoryModel(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getLastChecked() {
        return lastChecked;
    }

    public void setLastChecked(String lastChecked) {
        this.lastChecked = lastChecked;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getLastInstalled() {
        return lastInstalled;
    }

    public void setLastInstalled(String lastInstalled) {
        this.lastInstalled = lastInstalled;
    }
}
