package com.mmhayes.common.deviceinfo.models;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-04-16 15:08:44 -0400 (Thu, 16 Apr 2020) $: Date of last commit
 $Rev: 11383 $: Revision of last commit
*/
public class DeviceAntiVirusHistoryModel {
    private Integer id = null;
    private String productName = "";
    private String statusCode = "";
    private Integer antiVirusStatusId = null;

    public DeviceAntiVirusHistoryModel(){

    }

    //region Getters and Setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @JsonIgnore
    public Integer getAntiVirusStatusId() {
        return antiVirusStatusId;
    }

    public void setAntiVirusStatusId(Integer antiVirusStatusId) {
        this.antiVirusStatusId = antiVirusStatusId;
    }

    //endregion
}
