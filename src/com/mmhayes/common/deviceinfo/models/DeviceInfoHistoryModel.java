package com.mmhayes.common.deviceinfo.models;

//MMHayes Dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.conn_pool.JDCConnection;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.transaction.models.OtmModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

//Other Dependencies
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-03-12 10:57:46 -0500 (Fri, 12 Mar 2021) $: Date of last commit
 $Rev: 13636 $: Revision of last commit
*/
public class DeviceInfoHistoryModel {
    private Integer id = null;

    private String historyDateTime = "";
    private String osName = "";
    private String osVersion = "";
    private OtmModel otm = null;
    private DeviceAntiVirusHistoryModel deviceAntiVirusHistory = null;
    private DeviceUpdateHistoryModel deviceUpdateHistory = null;

    private static DataManager dm = new DataManager();

    public DeviceInfoHistoryModel() {

    }

    public void validate() throws Exception {
        if (this.getOtm() == null) {
            throw new MissingDataException("Invalid OTM information.", PosAPIHelper.getLogFileName(otm));
        }
    }

    public void save() throws Exception {
        DataManager dm = new DataManager();
        String logFileName = PosAPIHelper.getSyncServiceLogFileName(this.getOtm());

        int saveStatus = 0;
        Exception mainEx = null;

        JDCConnection conn = null;
        try {
            conn = dm.pool.getConnection(false, logFileName);
        } catch (SQLException qe) {
            Logger.logMessage("DeviceInfoHistory.save no connection to DB.", logFileName);
            Logger.logMessage(qe.getMessage(), logFileName);
            throw new MissingDataException("Could not establish a connection to the database.", logFileName);
        }

        try {
            conn.setAutoCommit(false); //make sure the save is atomic
            this.generateFormattedDate();
            this.insertDeviceInfoHistory(conn, logFileName);
            this.insertDeviceInfoUpdateHistory(conn, logFileName);
            this.checkAntiVirusStatus(conn, logFileName);
            this.insertDeviceInfoAntiVirusHistory(conn, logFileName);
            this.updateOtm(conn, logFileName);

            saveStatus = 1;
            conn.commit();
            Logger.logMessage("DeviceInfoHistory Saved Successfully", logFileName, Logger.LEVEL.DEBUG);
        } catch (Exception ex) {
            Logger.logMessage("DeviceInfoHistory Failed", logFileName, Logger.LEVEL.ERROR);
            mainEx = ex;

            try {
                conn.rollback();
                Logger.logMessage("Rollback Successfully", logFileName, Logger.LEVEL.ERROR);
            } catch (SQLException e) {
                Logger.logException("DeviceInfoHistoryModel.save. SQL exception", logFileName, e);
            }
            saveStatus = -1;
            Logger.logException(ex, logFileName);
        } finally {
            try {
                conn.setAutoCommit(true);
                dm.pool.returnConnection(conn, logFileName);
                Logger.logMessage("Database Transaction Committed Successfully", logFileName, Logger.LEVEL.DEBUG);
            } catch (SQLException ex) {
                Logger.logException(ex, logFileName);
            }
        }

        //Now the the "finally" is done and the database should be cleaned up
        //if the save did not occur properly, return the error to the API Client
        if (saveStatus < 1) {
            if (mainEx != null) {
                throw mainEx;
            } else {
                throw new MissingDataException("Error Saving DeviceInfoHistoryModel.  Check logs for more details.", logFileName);
            }
        }

        this.getOtm().logTransactionTime();
    }

    /**
     * Insert the Device Info History Model
     *
     * @return a Terminal Health Info Model with the inserted database record set as the ID
     */
    public DeviceInfoHistoryModel insertDeviceInfoHistory(JDCConnection conn, String logFileName) throws Exception {

        //insert history record into OtmHealth table
        Integer insertedOtmHealthId = dm.parameterizedExecuteNonQuery("data.posapi30.insertDeviceInfoHistory",
                new Object[]{
                        getOtm().getId(),
                        getHistoryDateTime(),
                        getOsName(),
                        getOsVersion()
                },
                logFileName, conn
        );

        //record success in response
        if (insertedOtmHealthId > 0) {
            setId(insertedOtmHealthId);

        } else {
            Logger.logMessage("Could not insert Device Info History record in DeviceInfoHistoryModel.save.", PosAPIHelper.getLogFileName(getOtm()), Logger.LEVEL.ERROR);
            throw new MissingDataException("Could not insert Device Info History record.", logFileName);
        }

        return this;
    }

    private void checkAntiVirusStatus(JDCConnection conn, String logFileName) throws Exception {
        Integer antiVirusStatusId = null;

        //get Anti-Virus id from db
        ArrayList<HashMap> antiVirusStatusList = dm.parameterizedExecuteQuery("data.posapi30.getOneAntiVirusStatus",
                new Object[]{
                        getDeviceAntiVirusHistory().getProductName(),
                        getDeviceAntiVirusHistory().getStatusCode()
                },
                logFileName,
                true
        );

        //populate this collection from an array list of hashmaps
        if (antiVirusStatusList != null && !antiVirusStatusList.isEmpty()){
            antiVirusStatusId = CommonAPI.convertModelDetailToInteger(antiVirusStatusList.get(0).get("ANTIVIRUSSTATUSID"));
        }

        if (antiVirusStatusId == null || antiVirusStatusId < 1) {
            antiVirusStatusId = dm.parameterizedExecuteNonQuery("data.posapi30.insertAntiVirusStatus",
                    new Object[]{
                            getDeviceAntiVirusHistory().getProductName(),
                            getDeviceAntiVirusHistory().getStatusCode()
                    },
                    logFileName, conn
            );
        }

        getDeviceAntiVirusHistory().setAntiVirusStatusId(antiVirusStatusId);
    }

    private void insertDeviceInfoAntiVirusHistory(JDCConnection conn, String logFileName) throws Exception {
        Integer insertedDeviceAntiVirusHistoryId = dm.parameterizedExecuteNonQuery("data.posapi30.insertDeviceAntiVirusHistory",
                new Object[]{
                        this.getId(),
                        getDeviceAntiVirusHistory().getAntiVirusStatusId()
                },
                logFileName, conn
        );

        //record success in response
        if (insertedDeviceAntiVirusHistoryId > 0) {
            getDeviceAntiVirusHistory().setId(insertedDeviceAntiVirusHistoryId);

        } else {
            Logger.logMessage("Could not insert Device Anti-Virus record in DeviceInfoHistoryModel.save.", PosAPIHelper.getSyncServiceLogFileName(getOtm()), Logger.LEVEL.ERROR);
            throw new MissingDataException("Could not insert Device Anti-Virus record.", logFileName);
        }
    }

    private void insertDeviceInfoUpdateHistory(JDCConnection conn, String logFileName) throws Exception {
        Integer insertedDeviceUpdateHistoryId = dm.parameterizedExecuteNonQuery("data.posapi30.insertDeviceUpdateHistory",
                new Object[]{
                        this.getId(),
                        getDeviceUpdateHistory().getLastChecked(),
                        getDeviceUpdateHistory().getLastInstalled()
                },
                logFileName, conn
        );

        //record success in response
        if (insertedDeviceUpdateHistoryId > 0) {
            getDeviceUpdateHistory().setId(insertedDeviceUpdateHistoryId);

        } else {
            Logger.logMessage("Could not insert Device Update record in DeviceInfoHistoryModel.save.", PosAPIHelper.getLogFileName(getOtm()), Logger.LEVEL.ERROR);
            throw new MissingDataException("Could not insert Device Update record.", logFileName);
        }
    }

    private void updateOtm(JDCConnection conn, String logFileName) throws Exception {
        Integer updatedRecords = dm.parameterizedExecuteNonQuery("data.posapi30.updateOtmDeviceInfo", new Object[]{
                this.getId(),
                this.getOtm().getId()
        }, logFileName, conn);

        if (updatedRecords < 0) {
            throw new MissingDataException("Information could not be updated in QC_OTM", logFileName);
        }
    }

    private void generateFormattedDate() {
        Date dateNow = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        this.setHistoryDateTime(simpleDateFormat.format(dateNow));
    }

    //region Getters/Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getHistoryDateTime() {
        return historyDateTime;
    }

    public void setHistoryDateTime(String historyDateTime) {
        this.historyDateTime = historyDateTime;
    }

    public OtmModel getOtm() {
        return otm;
    }

    public void setOtm(OtmModel otm) {
        this.otm = otm;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public DeviceAntiVirusHistoryModel getDeviceAntiVirusHistory() {
        return deviceAntiVirusHistory;
    }

    public void setDeviceAntiVirusHistory(DeviceAntiVirusHistoryModel deviceAntiVirusHistory) {
        this.deviceAntiVirusHistory = deviceAntiVirusHistory;
    }

    public DeviceUpdateHistoryModel getDeviceUpdateHistory() {
        return deviceUpdateHistory;
    }

    public void setDeviceUpdateHistory(DeviceUpdateHistoryModel deviceUpdateHistory) {
        this.deviceUpdateHistory = deviceUpdateHistory;
    }

    //endregion
}
