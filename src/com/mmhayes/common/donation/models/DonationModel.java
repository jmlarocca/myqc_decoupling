package com.mmhayes.common.donation.models;

// API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.loyalty.models.LoyaltyProgramModel;

// Other dependencies
import java.util.ArrayList;
import java.util.HashMap;

public class DonationModel {

    private Integer id = null;
    private String name;
    private String description;
    private LoyaltyProgramModel loyaltyProgram  = null;
    private Integer terminalId = null;
    private Integer donationTypeId = null;

    public DonationModel() {

    }

    public DonationModel(HashMap donationModelHM) throws Exception {
        setModelProperties(donationModelHM, null);
    }

    public DonationModel(HashMap donationModelHM, Integer terminalId) throws Exception {
        setModelProperties(donationModelHM, terminalId);
    }

    public DonationModel setModelProperties(HashMap modelDetailHM, Integer terminalId) throws Exception {

        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));
        setTerminalId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALID")));
        setDonationTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DONATIONTYPEID")));

        if (terminalId == null || !PosAPIModelCache.useTerminalCache) {
            if (this.getLoyaltyProgram() == null) {
                if (CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")) != null) {
                    setLoyaltyProgram(LoyaltyProgramModel.getOneLoyaltyProgram(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")), getTerminalId()));
                }
            }
        }
        else if (PosAPIModelCache.useTerminalCache) {
            if (this.getDonationTypeId() != null && !this.getDonationTypeId().equals(PosAPIHelper.DonationType.MONETARY.toInt())) { //don't validate Loyalty Program for monetary donations
                if (this.getLoyaltyProgram() == null) {
                    if (CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")) != null) {
                        LoyaltyProgramModel validatedLoyaltyProgram = null;
                        validatedLoyaltyProgram = (LoyaltyProgramModel) PosAPIModelCache.getOneObject(getTerminalId(), CommonAPI.PosModelType.LOYALTY_PROGRAM, CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LOYALTYPROGRAMID")));
                        if (validatedLoyaltyProgram != null) {
                            setLoyaltyProgram(validatedLoyaltyProgram);
                        } else {
                            throw new MissingDataException("Invalid Loyalty Program", getTerminalId());
                        }
                    }
                }
            }
        }

        return this;
    }

    // Get one Donation Model by the donation ID
    public static DonationModel getOneDonationModelById(Integer donationId, Integer terminalId) throws Exception {
        DonationModel donationModel = new DonationModel();

        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> donationHM = dm.parameterizedExecuteQuery("data.posapi30.getOneDonationById",
                new Object[]{donationId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        //donation list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(donationHM, "Donation Not Found", terminalId);
        return new DonationModel(donationHM.get(0), terminalId);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LoyaltyProgramModel getLoyaltyProgram() {
        return loyaltyProgram;
    }

    public void setLoyaltyProgram(LoyaltyProgramModel loyaltyProgram) {
        this.loyaltyProgram = loyaltyProgram;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public Integer getDonationTypeId() {
        return donationTypeId;
    }

    public void setDonationTypeId(Integer donationTypeId) {
        this.donationTypeId = donationTypeId;
    }
}
