package com.mmhayes.common.donation.models;

//MMHayes dependencies
import com.mmhayes.common.loyalty.models.LoyaltyPointModel;
import com.mmhayes.common.transaction.models.TransactionLineItemModel;
import com.mmhayes.common.transaction.models.TransactionModel;

// API dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

// Other dependencies
import java.util.HashMap;

public class DonationLineItemModel extends TransactionLineItemModel {

    private DonationModel donation = null;
    private Integer points = null;
    private LoyaltyPointModel loyaltyPoint = null;

    public DonationLineItemModel() {

    }

    public DonationLineItemModel(HashMap donationLineItemModelHM) throws Exception {
        setModelProperties(donationLineItemModelHM, null);
    }

    public DonationLineItemModel setModelProperties(HashMap modelDetailHM, Integer terminalId) throws Exception {

        setPoints(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POINTS")));

        return this;
    }

    public void createLoyaltyPoint(TransactionModel transactionModel) {

        if (this.getLoyaltyPoint() == null) {
            this.setLoyaltyPoint(new LoyaltyPointModel());
        }

        this.loyaltyPoint.setLoyaltyProgram(donation.getLoyaltyProgram());
        this.loyaltyPoint.setEmployeeId(transactionModel.getLoyaltyAccount().getId());
        this.loyaltyPoint.setPoints(points);
    }

    @JsonIgnore
    //We need the ItemTypeId on the DonationLineItemModel
    public
    @Override
    Integer getItemTypeId() {
        if (itemTypeId != null) {
            return itemTypeId;
        } else {
            return PosAPIHelper.ObjectType.DONATION.toInt();
        }
    }

    public DonationModel getDonation() {
        return donation;
    }

    public void setDonation(DonationModel donation) {
        this.donation = donation;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public LoyaltyPointModel getLoyaltyPoint() {
        return loyaltyPoint;
    }

    public void setLoyaltyPoint(LoyaltyPointModel loyaltyPoint) {
        this.loyaltyPoint = loyaltyPoint;
    }
}
