package com.mmhayes.common.donation.collections;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.donation.models.DonationModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: prsmith $: Author of last commit
 $Date: 2020-12-04 17:32:06 -0500 (Fri, 04 Dec 2020) $: Date of last commit
 $Rev: 13224 $: Revision of last commit
*/
public class DonationCollection {

    private List<DonationModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    //default constructor - used by the API
    public DonationCollection() {
    }

    /*
    * Get all Loyalty Donations By Program Id, look at revenue center mappings
    * Used by My QC - 06/02/2020 gem
    */
    public static DonationCollection getAllDonationsByEmployeeId(Integer loyaltyProgramId, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        DonationCollection donationCollection = new DonationCollection();

        //get all models in an array list
        ArrayList<HashMap> donationList = dm.parameterizedExecuteQuery("data.posapi30.getAllDonationsByEmployeeId",
                new Object[]{loyaltyProgramId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap donationHM : donationList) {
            DonationModel donationModel = new DonationModel(donationHM);
            donationCollection.getCollection().add(donationModel);  //create a new model and add to the collection
        }

        return donationCollection;
    }

    public static DonationCollection getAllMonetaryDonationsByEmployeeId(Integer employeeID, Integer terminalId) throws Exception {
        DataManager dm = new DataManager();
        DonationCollection donationCollection = new DonationCollection();

        //get all models in an array list
        ArrayList<HashMap> donationList = dm.parameterizedExecuteQuery("data.posapi30.getAllMonetaryDonationsByEmployeeId", //change this query, make a new one
                new Object[]{employeeID},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        for (HashMap donationHM : donationList) {
            DonationModel donationModel = new DonationModel(donationHM);
            donationCollection.getCollection().add(donationModel);  //create a new model and add to the collection
        }

        return donationCollection;
    }

    public List<DonationModel> getCollection() {
        return collection;
    }

    public void setCollection(List<DonationModel> collection) {
        this.collection = collection;
    }
}
