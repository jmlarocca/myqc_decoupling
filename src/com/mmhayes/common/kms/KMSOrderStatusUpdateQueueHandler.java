
package com.mmhayes.common.kms;

import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.kms.models.KMSStatusModel;
import com.mmhayes.common.kms.models.KMSStatusUpdateModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.xmlapi.XmlRpcUtil;
import redstone.xmlrpc.XmlRpcStruct;
import org.apache.commons.lang.math.NumberUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class KMSOrderStatusUpdateQueueHandler implements Runnable {

    // the only instance of KMSOrderStatusUpdateQueueHandler for this WAR
    private static volatile KMSOrderStatusUpdateQueueHandler orderStatusQueueHandlerInstance = null;

    // log file specific to the KMSOrderStatusUpdateQueueHandler
    private static final String ELQH_LOG = "KMSOrderStatusUpdateQueueHandler.log";

    // variables within the KMSOrderStatusUpdateQueueHandler scope
    private volatile boolean isShuttingDown = false;
    private volatile boolean isTerminated = false;
    //hold a list of all the status updates that havent been sent off to the server yet
//    private ConcurrentLinkedQueue<KMSStatusUpdateModel> statusQueue = new ConcurrentLinkedQueue<>();
    //a mapping of transaction to a update list sorted by time
    private ConcurrentHashMap<String, PriorityQueue<KMSStatusUpdateModel>> updateQueue = new ConcurrentHashMap<>();
    private Comparator<KMSStatusUpdateModel> updateChronologicalComparator = new Comparator<KMSStatusUpdateModel>() {
        @Override
        public int compare(KMSStatusUpdateModel o1, KMSStatusUpdateModel o2) {
            //greater than 1 means o1 has higher priority
            if(o1 != null && o2 != null){
                if(o1.updateTime < o2.updateTime){
                    return 1;//o1 is older, so it should go first
                }else if(o1.updateTime == o2.updateTime){
                    return 0;
                }else{
                    return -1;
                }
            }
            return 0;  //To change body of implemented methods use File | Settings | File Templates.
        }
    };

    private KMSOrderStatusUpdateQueueHandler(ArrayList<XmlRpcStruct> updates){
        //break apart by order
        synchronized (updateQueue){
            for(XmlRpcStruct hm : updates){
                KMSStatusUpdateModel m = new KMSStatusUpdateModel(hm);
                if(updateQueue.containsKey(m.status.PATransactionID)){
                    updateQueue.get(m.status.PATransactionID).add(m);
                }else{
                    PriorityQueue<KMSStatusUpdateModel> updateList = new PriorityQueue<>(10, updateChronologicalComparator);
                    updateList.add(m);
                    updateQueue.put(m.status.PATransactionID.toString(), updateList);
                }
            }
        }

        //OLD WAY
//        if(updates != null && updates.size() > 0){
//            for(KMSStatusUpdateModel update : updates){
//                statusQueue.offer(update);
//            }
//        }


        //startStatusUpdateQueueHandlerThread();
    }
    /**
     * Get the only instance of KMSOrderStatusUpdateQueueHandler for this WAR.
     *
     * @return {@link KMSOrderStatusUpdateQueueHandler} The KMSOrderStatusUpdateQueueHandler instance for this WAR.
     */
    public static KMSOrderStatusUpdateQueueHandler getInstance () {

        try {
            if (orderStatusQueueHandlerInstance == null) {
                throw new RuntimeException("The KMSOrderStatusUpdateQueueHandler must be instantiated before trying to obtain its " +
                        "instance.");
            }
        }
        catch (Exception e) {
            Logger.logException(e, ELQH_LOG);
            Logger.logMessage("There was a problem trying to get the only instance of KMSOrderStatusUpdateQueueHandler in " +
                    "KMSOrderStatusUpdateQueueHandler.getInstance", ELQH_LOG, Logger.LEVEL.ERROR);
        }

        return orderStatusQueueHandlerInstance;
    }

    /**
     * Instantiate and return the only instance of KMSOrderStatusUpdateQueueHandler for this WAR.
     * @return {@link KMSOrderStatusUpdateQueueHandler} The KMSOrderStatusUpdateQueueHandler instance for this WAR.
     */
    public static synchronized KMSOrderStatusUpdateQueueHandler init () {

        try {
            //get the server's unsaved status update list.
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            String methodName = "KMSDataManager.getUnsavedOrderUpdates";
            Object[] methodParams = new Object[]{};
            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName,
                    methodParams);
            //TODO: is this an array of XMLRPCStructs?
            ArrayList serverList = XmlRpcUtil.getInstance().parseArrayListXmlRpcResponse(xmlRpcResponse, methodName);

            if (orderStatusQueueHandlerInstance != null) {
                throw new RuntimeException("There KMSOrderStatusUpdateQueueHandler has already been instantiated and may not be " +
                        "instantiated again.");
            }
            else {
                // create the instance
                orderStatusQueueHandlerInstance = new KMSOrderStatusUpdateQueueHandler(serverList);
                Logger.logMessage("The instance of KMSOrderStatusUpdateQueueHandler has been instantiated", ELQH_LOG, Logger.LEVEL.TRACE);
            }
        }
        catch (Exception e) {
            Logger.logException(e, ELQH_LOG);
            Logger.logMessage("There was a problem trying to instantiate and return the only instance of " +
                    "KMSOrderStatusUpdateQueueHandler in KMSOrderStatusUpdateQueueHandler.init", ELQH_LOG, Logger.LEVEL.ERROR);
        }

        return orderStatusQueueHandlerInstance;
    }

    /**
     * Method that will be called by a Thread created using the KMSOrderStatusUpdateQueueHandler Runnable.
     *
     */
    @Override
    public void run () {

        try {
            Logger.logMessage("Running KMSOrderStatusUpdateQueueHandler", ELQH_LOG, Logger.LEVEL.DEBUG);
            Thread.currentThread().setName("KitchenPrinter-KMSOrderStatusUpdateQueueHandler");
            //loop over the hashmap keyset, and check first if all statuses
            // are sent to the server and attempts to send those that have not been, THEN sends one off at a time to be saved to the DB
            // and then removes that from the list if success, otherwise it will update the location and stop saving for that transaction.
            //one failed attempt to send/communicate with the server will put this to sleep for a while.
            boolean shouldSleep = false;
            int sleepCounter = 0;
            int secondsToSleep = 15;

            HashMap<Integer, String> updateStatuses = new HashMap<>();
            updateStatuses.put(0, "NOT_SENT");
            updateStatuses.put(1, "SENT_TO_SERVER");
            updateStatuses.put(2, "SAVED_IN_SERVER_DB");

            while ((!isShuttingDown) && (!Thread.currentThread().isInterrupted())) {
                if(shouldSleep){
                    sleepCounter++;
                    if(sleepCounter > secondsToSleep){
                        sleepCounter = 0;
                        shouldSleep = false;
                    }else{
                        Thread.sleep(1000L);
                    }
                }else{
                    ArrayList<String> emptyTransactions = new ArrayList<>();
                    for(String key: updateQueue.keySet()){
                        Logger.logMessage("--- ATTEMPTING STATUS UPDATES FOR TRANSACTION " + key + " --- (outer loop)", ELQH_LOG, Logger.LEVEL.DEBUG);

                        //Notify of any unsent statuses
                        KMSStatusUpdateModel[] updates;
                        synchronized (updateQueue){
                            PriorityQueue<KMSStatusUpdateModel> queue = updateQueue.get(key);
                            updates = new KMSStatusUpdateModel[queue.size()];
                            Arrays.sort(queue.toArray(updates),updateChronologicalComparator);
                        }
                        for (int i = 0; i < updates.length; i++) {

                            KMSStatusUpdateModel update = updates[i];

                            Logger.logMessage("Iterating through the updates for Transaction... (first inner loop)" + update.status.PATransactionID, Logger.LEVEL.DEBUG);


                            Logger.logMessage("Current trans line item is " + update.status.PATransLineItemID + " (first inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);
                            Logger.logMessage("Current order status is " + updateStatuses.get(update.currentUpdateLocation) + " (first inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);

                            if(update.currentUpdateLocation == KMSStatusUpdateModel.NOT_SENT){

                                //attempt to send it
                                String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
                                String macAddress = PeripheralsDataManager.getTerminalMacAddress();
                                String hostname = PeripheralsDataManager.getPHHostname();
                                String methodName = "KMSDataManager.notifyOfOrderUpdate";
                                Object[] methodParams = new Object[]{update.serialize()};

                                Logger.logMessage("Attempting to notifyOfOrderUpdate" + " (first inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);

                                Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName,
                                        methodParams);

                                Logger.logMessage("Checking the response received from the server for notifyOfOrderUpdate (first inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);
                                if(xmlRpcResponse != null){
                                    Logger.logMessage("Response for notifyOfOrderUpdate was not null, checking response data (first inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);
                                    // get any transactions from the XML RPC response so they can be printed
                                    boolean sent = false;
                                    if(xmlRpcResponse instanceof Boolean)
                                        sent = (Boolean) xmlRpcResponse;
                                    else
                                        sent = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, methodName);

                                    if(sent){
                                        Logger.logMessage("Sent status update to the server. Transaction " +
                                                update.status.PATransactionID + ", Line " + update.status.PATransLineItemID +
                                                ", Status " + update.status.KMSOrderStatusID, ELQH_LOG, Logger.LEVEL.DEBUG);
                                        update.currentUpdateLocation = KMSStatusUpdateModel.SENT_TO_SERVER;
                                    }
                                    else {
                                        Logger.logMessage("Tried but failed to send status update to the server. Transaction " +
                                                update.status.PATransactionID + ", Line " + update.status.PATransLineItemID +
                                                ", Status " + update.status.KMSOrderStatusID, ELQH_LOG, Logger.LEVEL.DEBUG);
                                    }
                                }else{
                                    //it was not sent.
                                    Logger.logMessage("Failed to send status update to the server. Transaction " +
                                            update.status.PATransactionID + ", Line " + update.status.PATransLineItemID +
                                            ", Status " + update.status.KMSOrderStatusID, ELQH_LOG, Logger.LEVEL.DEBUG);
                                    shouldSleep = true;
                                }
                            }
                        }


                        //check if we can save each update for this terminal, if not, wait till next time.
                        boolean saveMore = true;
                        while(saveMore && (!Thread.currentThread().isInterrupted())){
                            Logger.logMessage("Going through to... save more", Logger.LEVEL.DEBUG);
                            if(updateQueue.get(key).size() > 0){
                                Logger.logMessage("Update queue is not empty - getting the next update (second inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);

                                KMSStatusUpdateModel update = updateQueue.get(key).peek();

                                Logger.logMessage("Current trans line item is " + update.status.PATransLineItemID + " (second inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);
                                Logger.logMessage("Current order status is " + updateStatuses.get(update.currentUpdateLocation) + " (second inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);

                                if(update.currentUpdateLocation == KMSStatusUpdateModel.SENT_TO_SERVER){

                                    //attempt to send it
                                    String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
                                    String macAddress = PeripheralsDataManager.getTerminalMacAddress();
                                    String hostname = PeripheralsDataManager.getPHHostname();
                                    String methodName = "KMSDataManager.updateOrderStatus";
                                    Object[] methodParams = new Object[]{update.serialize()};

                                    Logger.logMessage("Attempting to update order status (second inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);
                                    Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName,
                                            methodParams);

                                    Logger.logMessage("Checking the response received from the server for updateOrderStatus (second inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);
                                    if(xmlRpcResponse != null){
                                        Logger.logMessage("Response for updateOrderStatus was not null, checking response data (second inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);
                                        // get any transactions from the XML RPC response so they can be printed
                                        boolean saved = false;
                                        if(xmlRpcResponse instanceof  Boolean)
                                            saved = (Boolean)xmlRpcResponse;
                                        else
                                            saved = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, methodName);
                                        if(saved){
                                            Logger.logMessage("Update was saved in the server - updating the model to SAVED_IN_SERVER_DB (second inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);
                                            update.currentUpdateLocation = KMSStatusUpdateModel.SAVED_IN_SERVER_DB;
                                            updateQueue.get(key).poll();//remove the head update

                                            Logger.logMessage("About to send order status update message (second inner loop)", ELQH_LOG, Logger.LEVEL.DEBUG);
                                            SendOrderStatusUpdateMessage(update);

                                            Logger.logMessage("Saved status update to the server DB. Transaction " +
                                                    update.status.PATransactionID + ", Line " + update.status.PATransLineItemID +
                                                    ", Status " + update.status.KMSOrderStatusID, ELQH_LOG, Logger.LEVEL.DEBUG);
                                        }else{
                                            Logger.logMessage("Update was NOT saved in the server DB. Transaction " +
                                                    update.status.PATransactionID + ", Line " + update.status.PATransLineItemID +
                                                    ", Status " + update.status.KMSOrderStatusID, ELQH_LOG, Logger.LEVEL.DEBUG);

                                            //expire the update if its older than 24h (same as in KMSDataManager.updateOrderStatus
                                            if(System.currentTimeMillis() - 86400000L > update.updateTime){
                                                updateQueue.get(key).poll();//remove the head update
                                                Logger.logMessage("Old status update could not be saved in the server DB. Transaction " +
                                                        update.status.PATransactionID + ", Line " + update.status.PATransLineItemID + ", Status " +
                                                        update.status.KMSOrderStatusID, ELQH_LOG, Logger.LEVEL.DEBUG);

                                            }else{
                                                //we cant save this transaction yet, so wait.
                                                shouldSleep = true;
                                                saveMore = false;
                                            }
                                        }
                                    }else{
                                        //it was not sent.
                                        Logger.logMessage("Failed to save status update in the server DB. Transaction " +
                                                update.status.PATransactionID + ", Line " + update.status.PATransLineItemID +
                                                ", Status " + update.status.KMSOrderStatusID, ELQH_LOG, Logger.LEVEL.DEBUG);
                                        shouldSleep = true;
                                        saveMore = false;//the server may be offline. wait.
                                    }
                                }else if(update.currentUpdateLocation == KMSStatusUpdateModel.SAVED_IN_SERVER_DB){
                                    //this should never happen, but just in case, so we dont get an infinite loop:
                                    updateQueue.get(key).poll();//remove the head update

                                }else{
                                    saveMore = false;//this update has not been sent to the server, dont continue.
                                }
                            }else{
                                emptyTransactions.add(key);
                                saveMore = false;//this transaction has no more updates to save.
                            }
                        }
                    }
                    synchronized (updateQueue){
                        for(String empty : emptyTransactions){
                            if(updateQueue.get(empty).size() == 0){
                                updateQueue.remove(empty);
                            }
                        }
                    }
                    Thread.sleep(1000L);//chill for a second between runs
                }
            }

        }
        catch (Exception e) {
            Logger.logException(e, ELQH_LOG);
            Logger.logMessage("There was a problem in KMSOrderStatusUpdateQueueHandler.run", ELQH_LOG, Logger.LEVEL.ERROR);
        }
        finally {
            isTerminated = true;
            Logger.logMessage("KitchenPrinter-KMSOrderStatusUpdateQueueHandler is exiting", ELQH_LOG, Logger.LEVEL.TRACE);
        }

    }

    /**
     * Send a Twillio message to the phone number associated with the
     * @param update
     */
    public void SendOrderStatusUpdateMessage(KMSStatusUpdateModel update){
        Logger.logMessage("Trying to send order status update message in SendOrderStatusUpdateMessage", ELQH_LOG, Logger.LEVEL.DEBUG);
        if(update.status.KMSOrderStatusID == 6){//Finalized
            //Send XMLRPC to server to get relevant transaction Info
            //TODO: if the transactionID has a "T" (for offlineMode) in it we might need use a different lookup function
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            String methodName = "KMSDataManager.updateOrderStatus";
            Object[] methodParams = new Object[]{update.serialize()};

            Logger.logMessage("Trying to determine the transaction data with updateOrderStatus", ELQH_LOG, Logger.LEVEL.DEBUG);
            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName,
                    methodParams);

            Logger.logMessage("Transaction data determined from updateOrderStatus, determining whether it is null", ELQH_LOG, Logger.LEVEL.DEBUG);
            if(xmlRpcResponse != null){
                Logger.logMessage("Transaction data from updateOrderStatus not null, will try to send an sms", ELQH_LOG, Logger.LEVEL.DEBUG);

                ArrayList transactionData;
                if(xmlRpcResponse instanceof ArrayList){
                    transactionData =  (ArrayList) xmlRpcResponse;
                }else{
                    transactionData = XmlRpcUtil.getInstance().parseArrayListXmlRpcResponse(xmlRpcResponse, "KMSDataManager.getTransactionData");
                }

                String transactionId = update.status.PATransactionID;
                Logger.logMessage("Transaction id is " + transactionId, ELQH_LOG, Logger.LEVEL.DEBUG);

                if (NumberUtils.isNumber(transactionId) && Integer.parseInt(transactionId) > 0) {
                    String textDetailsMethod = "KMSDataManager.getOrderUpdateMessages";
                    Object[] textDetailsParams = new Object[]{transactionId, update.status.KMSOrderStatusID};

                    Logger.logMessage("Trying to getOrderUpdateMessages", Logger.LEVEL.DEBUG);
                    Object messageDetailsObj = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, textDetailsMethod, textDetailsParams);

                    ArrayList<HashMap> messageDetailsHms;
                    Logger.logMessage("Checking if result from getOrderUpdateMessages was null or an arraylist", ELQH_LOG, Logger.LEVEL.DEBUG);
                    if (messageDetailsObj != null && messageDetailsObj instanceof ArrayList) {

                        Logger.logMessage("Found non null message details from getOrderUpdateMessages, typecasting and iterating", ELQH_LOG, Logger.LEVEL.DEBUG);

                        messageDetailsHms = (ArrayList<HashMap>) messageDetailsObj;

                        Logger.logMessage("There are " + messageDetailsHms.size() + " sets of message details", ELQH_LOG, Logger.LEVEL.DEBUG);
                        Logger.logMessage("About to iterate through each set of message details", ELQH_LOG, Logger.LEVEL.DEBUG);
                        for (HashMap messageDetailsHm : messageDetailsHms) {

                            Logger.logMessage("Found a message details hm to send a message for...", ELQH_LOG, Logger.LEVEL.DEBUG);

                            Integer thirdPartyApiId = Integer.parseInt(messageDetailsHm.get("TEXTAPIID").toString());
                            Logger.logMessage("ThirdPartyApiId is " + thirdPartyApiId, ELQH_LOG, Logger.LEVEL.DEBUG);

                            String sendToNumber = messageDetailsHm.get("SENDTOPHONENUMBER").toString();
                            Logger.logMessage("SendToNumber is " + sendToNumber, ELQH_LOG, Logger.LEVEL.DEBUG);

                            String messageContent = messageDetailsHm.get("MESSAGE").toString();
                            Logger.logMessage("MessageContent is " + messageContent, ELQH_LOG, Logger.LEVEL.DEBUG);

                            if (thirdPartyApiId != null &&
                                    sendToNumber != null && !sendToNumber.isEmpty() &&
                                    messageContent != null && !messageContent.isEmpty()) {

                                Logger.logMessage("All relevant sms details found, will attempt to send text", ELQH_LOG, Logger.LEVEL.DEBUG);

                                // TODO Convert to rest endpoint and rest api call
                                String sendTextMethod = "KMSDataManager.sendOrderUpdateMessage";
                                Object[] sendTextParams = new Object[]{thirdPartyApiId, sendToNumber, messageContent};

                                Object sendTextResultObj = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, sendTextMethod, sendTextParams);

                                if (sendTextResultObj != null && sendTextResultObj instanceof Boolean) {

                                    Logger.logMessage("Got result trying to send text, typecasting and checking", ELQH_LOG, Logger.LEVEL.DEBUG);

                                    Boolean sendTextResult = (Boolean) sendTextResultObj;

                                    if (sendTextResult) {
                                        Logger.logMessage("API call to send text successfully handled (does not mean text was sent)", ELQH_LOG, Logger.LEVEL.DEBUG);
                                    }
                                    else {
                                        Logger.logMessage("API call to send text not successfully handled", ELQH_LOG, Logger.LEVEL.DEBUG);
                                    }
                                }
                            }
                            else {

                                if (thirdPartyApiId == null) {
                                    Logger.logMessage("Third party api id not found, not attempting to send text", ELQH_LOG, Logger.LEVEL.WARNING);
                                }

                                if (sendToNumber == null || sendToNumber.isEmpty()) {
                                    Logger.logMessage("Send to phone number not found, not attempting to send text", ELQH_LOG, Logger.LEVEL.WARNING);
                                }

                                if (messageContent == null || messageContent.isEmpty()) {
                                    Logger.logMessage("Message content not found, not attempting to send text", ELQH_LOG, Logger.LEVEL.WARNING);
                                }
                            }
                        }
                    }

                    Logger.logMessage("All relevant sms messages sent", ELQH_LOG, Logger.LEVEL.DEBUG);
                }
            }
        }
    }

    /**
     * Creates and starts a Thread using the KMSOrderStatusUpdateQueueHandler Runnable.
     *
     */
    public void startStatusUpdateQueueHandlerThread () {

        try {
            // reset isShuttingDown and isTerminated
            isShuttingDown = false;
            isTerminated = false;

            Thread orderStatusQueueHandlerThread = new Thread(this);
            orderStatusQueueHandlerThread.start();


            Logger.logMessage("The KMSOrderStatusUpdateQueueHandler Thread has started successfully in " +
                    "KMSOrderStatusUpdateQueueHandler.startStatusUpdateQueueHandlerThread", ELQH_LOG, Logger.LEVEL.TRACE);
        }
        catch (Exception e) {
            Logger.logException(e, ELQH_LOG);
            Logger.logMessage("There was a problem trying to start the KMSOrderStatusUpdateQueueHandler Thread in " +
                    "KMSOrderStatusUpdateQueueHandler.startStatusUpdateQueueHandlerThread", ELQH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Poisons the KMSOrderStatusUpdateQueueHandler's print queue and stops the KMSOrderStatusUpdateQueueHandler Thread.
     *
     */
    public void stopStatusUpdateQueueHandlerThread () {
        try {
            System.out.println( System.currentTimeMillis() + " stopStatusUpdateQueueHandlerThread called");
            // run through Thread shutdown routine
            isShuttingDown = true;

            int waitCount = 0;
            while (!isTerminated) {
                waitCount++;
                // wait a second
                Thread.sleep(1000L);
                if (waitCount >= 3) {
                    Logger.logMessage("The KMSOrderStatusUpdateQueueHandler Thread didn't stop within the specified wait time, it " +
                                    "is being interrupted now in KMSOrderStatusUpdateQueueHandler.stopStatusUpdateQueueHandlerThread", ELQH_LOG,
                            Logger.LEVEL.TRACE);
                    // interrupt the KMSOrderStatusUpdateQueueHandler Thread
                    Thread.getAllStackTraces().keySet().forEach((thread) -> {
                        if (thread.getName().equalsIgnoreCase("KitchenPrinter-KMSOrderStatusUpdateQueueHandler")) {
                            System.out.println( System.currentTimeMillis() + " sending interrupt to KMSOrderStatusUpdateQueueHandler");
                            thread.interrupt();
                        }
                    });
                }
            }

            Logger.logMessage("The KMSOrderStatusUpdateQueueHandler has been stopped", ELQH_LOG, Logger.LEVEL.IMPORTANT);
        }
        catch (Exception e) {
            Logger.logException(e, ELQH_LOG);
            Logger.logMessage("There was a problem trying to stop the KMSOrderStatusUpdateQueueHandler Thread in " +
                    "KMSOrderStatusUpdateQueueHandler.stopStatusUpdateQueueHandlerThread", ELQH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Try to add a event to the KMSOrderStatusUpdateQueueHandler's print queue within the specified amount of time.
     *
     * @param status {@link KMSStatusModel} The event we are trying to add to the KMSOrderStatusUpdateQueueHandler's print queue.
     * @return Whether or not the event was added to the queue.
     */
    public boolean offerToStatusUpdateQueue (KMSStatusModel status) {
        boolean statusAddedToQueue = false;

        try {
            KMSStatusUpdateModel update = new KMSStatusUpdateModel();
            update.status = status;
            update.updateTime = System.currentTimeMillis();
            update.currentUpdateLocation = KMSStatusUpdateModel.NOT_SENT;
            synchronized (updateQueue){
                if(updateQueue.containsKey(update.status.PATransactionID)){
                    updateQueue.get(update.status.PATransactionID).add(update);
                }else{
                    PriorityQueue<KMSStatusUpdateModel> updateList = new PriorityQueue<>(10, updateChronologicalComparator);
                    updateList.add(update);
                    updateQueue.put(update.status.PATransactionID.toString(), updateList);
                }
                statusAddedToQueue = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, ELQH_LOG);
            Logger.logMessage("There was a problem trying to add a Event to the Log queue within 10 " +
                    "milliseconds in KMSOrderStatusUpdateQueueHandler.offerToPrintQueue", ELQH_LOG, Logger.LEVEL.ERROR);
        }

        return statusAddedToQueue;
    }
}
