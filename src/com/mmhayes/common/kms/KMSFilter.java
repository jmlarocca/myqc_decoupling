package com.mmhayes.common.kms;

import org.apache.tomcat.websocket.server.WsFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class KMSFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if(KMSManager.isAcceptingNewConnections()){
            //DO NOT ACCEPT NEW CONNECTIONS
            chain.doFilter(request,response);
        }else{
            //abort the connection instead of upgrading it
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            httpServletResponse.sendError(403, "KMS Manager is not accepting connections on this WAR. Try requesting an up to date printer host.");
            return;
        }
    }

    @Override
    public void destroy() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
