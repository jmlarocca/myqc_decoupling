package com.mmhayes.common.kms;

import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.kms.models.KMSEventLogLine;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.xmlapi.XmlRpcUtil;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class KMSEventLogQueueHandler implements Runnable {

    // the only instance of KMSEventLogQueueHandler for this WAR
    private static volatile KMSEventLogQueueHandler eventLogQueueHandlerInstance = null;

    // log file specific to the KMSEventLogQueueHandler
    private static final String ELQH_LOG = "KMSEventLogQueueHandler.log";

    // variables within the KMSEventLogQueueHandler scope
    private volatile boolean isShuttingDown = false;
    private volatile boolean isTerminated = false;
    private static final String POISONOUS_LOG_NAME = "TERMINATE_THREAD_VEVSTUlOQVRFX1RIUkVBRA==";
    //hold a list of all the events that havent been sent off to the server yet
    private static final int LOG_QUEUE_CAPACITY = 1024;
    private ArrayBlockingQueue<KMSEventLogLine> logQueue = new ArrayBlockingQueue<>(LOG_QUEUE_CAPACITY);

    private KMSEventLogQueueHandler(ArrayList<KMSEventLogLine> logLines){
        if(logLines != null && logLines.size() > 0){
            for(KMSEventLogLine line : logLines){
                logQueue.offer(line);
            }
        }
        //startEventLogQueueHandlerThread();
    }
    /**
     * Get the only instance of KMSEventLogQueueHandler for this WAR.
     *
     * @return {@link KMSEventLogQueueHandler} The KMSEventLogQueueHandler instance for this WAR.
     */
    public static KMSEventLogQueueHandler getInstance () {

        try {
            if (eventLogQueueHandlerInstance == null) {
                throw new RuntimeException("The KMSEventLogQueueHandler must be instantiated before trying to obtain its " +
                        "instance.");
            }
        }
        catch (Exception e) {
            Logger.logException(e, ELQH_LOG);
            Logger.logMessage("There was a problem trying to get the only instance of KMSEventLogQueueHandler in " +
                    "KMSEventLogQueueHandler.getInstance", ELQH_LOG, Logger.LEVEL.ERROR);
        }

        return eventLogQueueHandlerInstance;
    }

    /**
     * Instantiate and return the only instance of KMSEventLogQueueHandler for this WAR.
     *
     * @param logList {@link java.util.ArrayList < KMSEventLogLine >} ArrayList of events to be used by the KMSEventLogQueueHandler to
     *         send to the server
     * @return {@link KMSEventLogQueueHandler} The KMSEventLogQueueHandler instance for this WAR.
     */
    public static synchronized KMSEventLogQueueHandler init (ArrayList<KMSEventLogLine> logList) {

        try {
            if (eventLogQueueHandlerInstance != null) {
                throw new RuntimeException("There KMSEventLogQueueHandler has already been instantiated and may not be " +
                        "instantiated again.");
            }
            else {
                // create the instance
                eventLogQueueHandlerInstance = new KMSEventLogQueueHandler(logList);
                Logger.logMessage("The instance of KMSEventLogQueueHandler has been instantiated", ELQH_LOG, Logger.LEVEL.TRACE);
            }
        }
        catch (Exception e) {
            Logger.logException(e, ELQH_LOG);
            Logger.logMessage("There was a problem trying to instantiate and return the only instance of " +
                    "KMSEventLogQueueHandler in KMSEventLogQueueHandler.init", ELQH_LOG, Logger.LEVEL.ERROR);
        }

        return eventLogQueueHandlerInstance;
    }

    /**
     * Method that will be called by a Thread created using the KMSEventLogQueueHandler Runnable.
     *
     */
    @Override
    public void run () {

        try {
            Thread.currentThread().setName("KitchenPrinter-KMSEventLogQueueHandler");


            while ((!isShuttingDown) && (!Thread.currentThread().isInterrupted())) {
                KMSEventLogLine event = logQueue.take();

                // check if the print queue was poisoned (i.e. should stop blocking) based on whether or not the person
                // name for the event was set to the poisonous name
                if (event.getNOTE().equalsIgnoreCase(POISONOUS_LOG_NAME)) {
                    Logger.logMessage("The KMSEventLogQueueHandler's print queue has been poisoned and the Thread created " +
                            "using the KMSEventLogQueueHandler Runnable will be terminated", ELQH_LOG, Logger.LEVEL.TRACE);
                    break;
                }
                else {
                    //log the event on the server
                    String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
                    String macAddress = PeripheralsDataManager.getTerminalMacAddress();
                    String hostname = PeripheralsDataManager.getPHHostname();
                    String methodName = "KMSDataManager.logEvent";
                    Object[] methodParams = new Object[]{
//                            event.getEventDTM(),
//                            event.getKMSStationID(),
//                            event.getKMSEventTypeID(),
//                            event.getPATransactionID()+"",
//                            event.getPATransLineItemID()+"",
//                            event.getNOTE(),
//                            event.getKMSOrderStatusID()+""
                            event
                    };
                    Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName,
                            methodParams);

                    // get any transactions from the XML RPC response so they can be printed
                    boolean saved = false;
                    if(xmlRpcResponse instanceof Boolean)
                        saved = (Boolean) xmlRpcResponse;
                    else
                        saved = XmlRpcUtil.getInstance().parseBooleanXmlRpcResponse(xmlRpcResponse, methodName);
                    if(!saved){
                        Logger.logMessage("Failed to save the event on the server...", ELQH_LOG, Logger.LEVEL.ERROR);
                        Logger.logMessage(event.log(), ELQH_LOG, Logger.LEVEL.ERROR);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, ELQH_LOG);
            Logger.logMessage("There was a problem in KMSEventLogQueueHandler.run", ELQH_LOG, Logger.LEVEL.ERROR);
        }
        finally {
            isTerminated = true;
            Logger.logMessage("KitchenPrinter-KMSEventLogQueueHandler is exiting", ELQH_LOG, Logger.LEVEL.TRACE);
        }

    }

    /**
     * Creates and starts a Thread using the KMSEventLogQueueHandler Runnable.
     *
     */
    public void startEventLogQueueHandlerThread () {

        try {
            // reset isShuttingDown and isTerminated
            isShuttingDown = false;
            isTerminated = false;

            Thread eventLogQueueHandlerThread = new Thread(this);
            eventLogQueueHandlerThread.start();

            KMSManager.startUp();

            Logger.logMessage("The KMSEventLogQueueHandler Thread has started successfully in " +
                    "KMSEventLogQueueHandler.startPrinterQueueHandlerThread", ELQH_LOG, Logger.LEVEL.TRACE);
        }
        catch (Exception e) {
            Logger.logException(e, ELQH_LOG);
            Logger.logMessage("There was a problem trying to start the KMSEventLogQueueHandler Thread in " +
                    "KMSEventLogQueueHandler.startPrinterQueueHandlerThread", ELQH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Poisons the KMSEventLogQueueHandler's print queue and stops the KMSEventLogQueueHandler Thread.
     *
     */
    public void stopEventLogQueueHandlerThread () {

        try {
            System.out.println( System.currentTimeMillis() + " stopEventLogQueueHandlerThread called");
            System.out.println("Shutting down KMSManager");
            KMSManager.shutdown();
            // poison the print queue
            KMSEventLogLine terminatingEvent = new KMSEventLogLine();
            terminatingEvent.setNOTE(POISONOUS_LOG_NAME);
            logQueue.offer(terminatingEvent);

            // run through Thread shutdown routine
            isShuttingDown = true;

            int waitCount = 0;
            while (!isTerminated) {
                waitCount++;
                // wait a second
                Thread.sleep(1000L);
                if (waitCount >= 3) {
                    Logger.logMessage("The KMSEventLogQueueHandler Thread didn't stop within the specified wait time, it " +
                            "is being interrupted now in KMSEventLogQueueHandler.stopEventLogQueueHandlerThread", ELQH_LOG,
                            Logger.LEVEL.TRACE);
                    // interrupt the KMSEventLogQueueHandler Thread
                    Thread.getAllStackTraces().keySet().forEach((thread) -> {
                        if (thread.getName().equalsIgnoreCase("KitchenPrinter-KMSEventLogQueueHandler")) {
                            System.out.println( System.currentTimeMillis() + " sending interrupt to KMSEventLogQueueHandler");
                            thread.interrupt();
                        }
                    });
                }
            }

            Logger.logMessage("The KMSEventLogQueueHandler has been stopped", ELQH_LOG, Logger.LEVEL.IMPORTANT);
        }
        catch (Exception e) {
            Logger.logException(e, ELQH_LOG);
            Logger.logMessage("There was a problem trying to stop the KMSEventLogQueueHandler Thread in " +
                    "KMSEventLogQueueHandler.stopEventLogQueueHandlerThread", ELQH_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Try to add a event to the KMSEventLogQueueHandler's print queue within the specified amount of time.
     *
     * @param event {@link KMSEventLogLine} The event we are trying to add to the KMSEventLogQueueHandler's print queue.
     * @return Whether or not the event was added to the queue.
     */
    public boolean offerToEventLogQueue (KMSEventLogLine event) {
        boolean addToEventQueueResult = false;

        try {
            addToEventQueueResult = logQueue.offer(event, 10, TimeUnit.MILLISECONDS);
        }
        catch (Exception e) {
            Logger.logException(e, ELQH_LOG);
            Logger.logMessage("There was a problem trying to add a Event to the Log queue within 10 " +
                    "milliseconds in KMSEventLogQueueHandler.offerToPrintQueue", ELQH_LOG, Logger.LEVEL.ERROR);
        }

        return addToEventQueueResult;
    }
}
