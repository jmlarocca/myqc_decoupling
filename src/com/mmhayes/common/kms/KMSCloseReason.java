package com.mmhayes.common.kms;

import javax.websocket.CloseReason;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public enum KMSCloseReason {
        PRINTER_HOST_SHUTTING_DOWN(4000, "The printer host running on this terminal is in the process of shutting down."),
        FAILED_VALIDATION(4001, "The supplied token is invalid."),
        MISSED_HEARTBEATS(4002, "Too many heartbeats missed.");

// private member variables of a KitchenPrinterDataManagerMethod
private String reason;
private int code;

/**
 * <p>Create a {@link KMSCloseReason} and set it's methodName.</p>
 *
 * @param reason The reason {@link String} that the connection is being closed.
 */
KMSCloseReason (int closeCode, String reason) {
        if(closeCode < 4000 || closeCode > 4999)
            throw new RuntimeException("The acceptable range of application close codes is between 4000 and 4999");
        this.code = closeCode;
        this.reason = reason;
}

private String getReason(){
    if(this.reason == null)
        this.reason = "Unknown Reason";
    return this.reason;
}

/**
 * <p>Getter for the reason {@link CloseReason}.</p>
 *
 * @return a {@link CloseReason} for this reason.
 */
public CloseReason getCloseReason () {
    CloseReason reason = new CloseReason(new CloseReason.CloseCode() {
        @Override
        public int getCode() {
            return code;  //To change body of implemented methods use File | Settings | File Templates.
        }
    }, getReason());
    return reason;
}
}
