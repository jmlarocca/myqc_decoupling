package com.mmhayes.common.kms.types;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-04-24 15:27:41 -0400 (Fri, 24 Apr 2020) $: Date of last commit
    $Rev: 11478 $: Revision of last commit
    Notes: Contains the various modes of how to interact with a KMS station.
*/

/**
 * <p>Contains the various modes of how to interact with a KMS station.</p>
 *
 */
public class KMSStationMode {

    public static final int TOUCH_MODE = 1;
    public static final int BUMP_MODE = 2;
    public static final int TOUCH_AND_BUMP_MODE = 3;

}