package com.mmhayes.common.kms.types;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-04-24 14:59:03 -0400 (Fri, 24 Apr 2020) $: Date of last commit
    $Rev: 11474 $: Revision of last commit
    Notes: Contains the different types of KMS stations that may be configured.
*/

/**
 * <p>Contains the different types of KMS stations that may be configured.</p>
 *
 */
public class KMSStationType {

    public static final int PREP_STATION = 1;
    public static final int EXPEDITOR_STATION = 2;
    public static final int REMOTE_ORDER_STATION = 3;
    public static final int MIRROR_STATION = 4;
    public static final int CUSTOMER_DISPLAY_STATION = 5;

}