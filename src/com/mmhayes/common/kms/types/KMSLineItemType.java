package com.mmhayes.common.kms.types;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-06 10:05:33 -0400 (Wed, 06 May 2020) $: Date of last commit
    $Rev: 11639 $: Revision of last commit
    Notes: Represents the type of data represented by a line item within a transaction.
*/

/**
 * <p>Represents the type of data represented by a line item within a transaction.</p>
 *
 */
public class KMSLineItemType {

    public static final int PRODUCT = 2;
    public static final int TENDER = 3;
    public static final int TAX_RATE = 6;
    public static final int DISCOUNT = 7;
    public static final int PAID_OUT = 10;
    public static final int RECEIVED_ON_ACCT = 11;
    public static final int LOYALTY_REWARD = 30;
    public static final int SURCHARGE = 32;
    public static final int COMBO = 36;

}