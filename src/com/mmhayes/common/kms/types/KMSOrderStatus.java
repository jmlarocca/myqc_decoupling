package com.mmhayes.common.kms.types;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-07-08 14:24:53 -0400 (Thu, 08 Jul 2021) $: Date of last commit
    $Rev: 14305 $: Revision of last commit
    Notes: Status of the order.
*/

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * <p>Status of the order.</p>
 *
 */
public class KMSOrderStatus {

    public static final int RECEIVED = 1;
    public static final int IN_PROGRESS = 2;
    public static final int COMPLETED = 3;
    public static final int CANCELED = 4;
    public static final int SUSPENDED = 5;
    public static final int FINALIZED = 6;
    public static final int WAITING = 7;
    public static final int SENT = 8;
    public static final int ERROR = 9;
    public static final int REPLACED = 10;

    // private member variables of a KMSOrderStatus
    private int KMSOrderStatusType = 0;
    private LocalDateTime updatedDTM = null;

    /**
     * <p>Constructor for a {@link KMSOrderStatus}.</p>
     *
     */
    public KMSOrderStatus () {}

    /**
     * <p>Sets the KMSOrderStatusType field for {@link KMSOrderStatus} and returns the {@link KMSOrderStatus} instance.</p>
     *
     * @param KMSOrderStatusType The print status.
     * @return The {@link KMSOrderStatus} instance with it's KMSOrderStatusType field set.
     */
    public KMSOrderStatus addKMSOrderStatusType (int KMSOrderStatusType) {
        this.KMSOrderStatusType = KMSOrderStatusType;
        return this;
    }

    /**
     * <p>Sets the updatedDTM field for {@link KMSOrderStatus} and returns the {@link KMSOrderStatus} instance.</p>
     *
     * @param updatedDTM The {@link LocalDateTime} the print job was updated.
     * @return The {@link KMSOrderStatus} instance with it's updatedDTM field set.
     */
    public KMSOrderStatus addUpdatedDTM (LocalDateTime updatedDTM) {
        this.updatedDTM = updatedDTM;
        return this;
    }

    /**
     * <p>Getter for the KMSOrderStatusType field of the {@link KMSOrderStatus}.</p>
     *
     * @return The KMSOrderStatusType field of the {@link KMSOrderStatus}.
     */
    public int getKMSOrderStatusType () {
        return KMSOrderStatusType;
    }

    /**
     * <p>Setter for the KMSOrderStatusType field of the {@link KMSOrderStatus}.</p>
     *
     * @param KMSOrderStatusType The KMSOrderStatusType field of the {@link KMSOrderStatus}.
     */
    public void setKMSOrderStatusType (int KMSOrderStatusType) {
        this.KMSOrderStatusType = KMSOrderStatusType;
    }

    /**
     * <p>Getter for the updatedDTM field of the {@link KMSOrderStatus}.</p>
     *
     * @return The updatedDTM field of the {@link KMSOrderStatus}.
     */
    public LocalDateTime getUpdatedDTM () {
        return updatedDTM;
    }

    /**
     * <p>Setter for the updatedDTM field of the {@link KMSOrderStatus}.</p>
     *
     * @param updatedDTM The updatedDTM field of the {@link KMSOrderStatus}.
     */
    public void setUpdatedDTM (LocalDateTime updatedDTM) {
        this.updatedDTM = updatedDTM;
    }

    /**
     * <p>Compares two KMSOrderStatus integers to see which Order Status ID is further along in "the chain".</p>
     *
     * @param statusIdOne The updatedDTM field of the {@link KMSOrderStatus}.
     * @param statusIdTwo The updatedDTM field of the {@link KMSOrderStatus}.
     * @return 1 if statusIdOne is further in "the chain" than statusIdTwo, 0 if they are the same place in
     *              in the chain, and -1 if statusIdTwo is further in "the chain" than statusIdOne.
     */
    public static int compare(int statusIdOne, int statusIdTwo) {
        // create an arraylist that has all the statuses in order based on where
        // they are in "the chain"
        ArrayList<Integer> statusIdsInOrder = new ArrayList<>();
        statusIdsInOrder.add(KMSOrderStatus.WAITING);
        statusIdsInOrder.add(KMSOrderStatus.SENT);
        statusIdsInOrder.add(KMSOrderStatus.RECEIVED);
        statusIdsInOrder.add(KMSOrderStatus.IN_PROGRESS);
        statusIdsInOrder.add(KMSOrderStatus.COMPLETED);
        statusIdsInOrder.add(KMSOrderStatus.FINALIZED);
        statusIdsInOrder.add(KMSOrderStatus.SUSPENDED);
        statusIdsInOrder.add(KMSOrderStatus.REPLACED);
        statusIdsInOrder.add(KMSOrderStatus.CANCELED);
        statusIdsInOrder.add(KMSOrderStatus.ERROR);

        Integer indexOfFirstStatus = statusIdsInOrder.indexOf(statusIdOne);
        Integer indexOfSecondStatus = statusIdsInOrder.indexOf(statusIdTwo);

        // based on the first status id, check if the second id is further along in "the chain" than the first id
        // if it is, return 1
        // if it is the same status, return 0
        // otherwise, return -1
        // Integer.compareTo() handles the comparison since the indices are in order of precedence
        return indexOfFirstStatus.compareTo(indexOfSecondStatus);
    }

}