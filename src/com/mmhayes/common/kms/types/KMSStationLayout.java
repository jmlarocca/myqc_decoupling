package com.mmhayes.common.kms.types;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-04-24 15:27:41 -0400 (Fri, 24 Apr 2020) $: Date of last commit
    $Rev: 11478 $: Revision of last commit
    Notes: Contains the various layouts for a KMS station.
*/

/**
 * Contains the various layouts for a KMS station.
 *
 */
public class KMSStationLayout {

    public static final int COLUMNS = 1;
    public static final int MERGED = 2;
    public static final int STRUCTURED = 3;
    public static final int FILL = 4;

}