package com.mmhayes.common.kms.types;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-04-24 15:27:41 -0400 (Fri, 24 Apr 2020) $: Date of last commit
    $Rev: 11478 $: Revision of last commit
    Notes: Represents various events that may occur within KMS.
*/

/**
 * <p>Represents various events that may occur within KMS.</p>
 *
 */
public class KMSEventType {

    public static final int ORDER_RECEIVED = 1;
    public static final int BUMPED = 2;
    public static final int UNBUMPED = 3;
    public static final int SUSPENDED = 4;
    public static final int CANCELED = 5;
    public static final int TRANSFER = 6;
    public static final int MANUAL_BACKUP = 7;
    public static final int AUTOMATIC_BACKUP = 8;
    public static final int STATION_ONLINE = 9;
    public static final int STATION_OFFLINE = 10;

}