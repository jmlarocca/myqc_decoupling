package com.mmhayes.common.kms.types;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-04-24 15:27:41 -0400 (Fri, 24 Apr 2020) $: Date of last commit
    $Rev: 11478 $: Revision of last commit
    Notes: Represents the priority of the order.
*/

/**
 * <p>Represents the priority of the order.</p>
 *
 */
public class KMSOrderPriority {

    public static final int NORMAL = 1;
    public static final int RUSH = 2;

}