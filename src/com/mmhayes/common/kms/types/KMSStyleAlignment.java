package com.mmhayes.common.kms.types;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-04-24 15:27:41 -0400 (Fri, 24 Apr 2020) $: Date of last commit
    $Rev: 11478 $: Revision of last commit
    Notes: Represents general alignment values for KMS.
*/

/**
 * <p>Represents general alignment values for KMS.</p>
 *
 */
public class KMSStyleAlignment {

    public static final int TOP = 1;
    public static final int RIGHT = 2;
    public static final int BOTTOM = 3;
    public static final int LEFT = 4;

}