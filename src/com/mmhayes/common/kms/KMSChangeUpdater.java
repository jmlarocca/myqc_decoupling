package com.mmhayes.common.kms;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-04-26 10:59:25 -0400 (Mon, 26 Apr 2021) $: Date of last commit
    $Rev: 13847 $: Revision of last commit
    Notes: Polls the backoffice for changes to the KMS configuration.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.kms.models.KMSStationModel;
import com.mmhayes.common.kms.models.KMSUpdateConfigModel;
import com.mmhayes.common.kmsapi.KMSDataManager;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.websockets.MMHEvent;
import com.mmhayes.common.xmlapi.XmlRpcManager;
import com.mmhayes.common.xmlapi.XmlRpcUtil;
import org.apache.commons.lang.math.NumberUtils;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * <p>Polls the backoffice for changes to the KMS configuration.</p>
 *
 */
public class KMSChangeUpdater implements Runnable, Serializable {

    // the only instance of a KMSChangeUpdater that will exist
    private static volatile KMSChangeUpdater kmsChangeUpdater = null;

    // log file for the KMSChangeUpdater
    private static final String KMS_CHANGE_UPDATER_LOG = "KMSChangeUpdater.log";

    // time constants
    private final long ONE_SECOND = 1000L;
    private final long ONE_MINUTE = 60000L;

    // private member variables of a KMSChangeUpdater
    private DataManager dataManager;
    private int pollingRate;
    private LocalDateTime lastCheck;
    private ConcurrentHashMap<String, String> nextStationUpdateTimes;

    // thread control variables
    private volatile boolean isStartingUp = true;
    private volatile boolean isShuttingDown = false;
    private AtomicBoolean shuttingDown;
    private volatile boolean isTerminated = false;

    /**
     * <p>Private constructor for a {@link KMSChangeUpdater}.</p>
     *
     * @param dataManager The {@link DataManager} to be used by the {@link KMSChangeUpdater}.
     * @param pollingRate How frequently the {@link KMSChangeUpdater} should check for backoffice changes to the KMS configuration in minutes.
     * @throws Exception
     */
    private KMSChangeUpdater (DataManager dataManager, int pollingRate) throws Exception {

        // prevent instantiating a new KMSChangeUpdater through reflection
        if (kmsChangeUpdater != null) {
            throw new Exception("A KMSChangeUpdater has already been instantiated. Use to getInstance() method to obtain this instance.");
        }

        this.dataManager = dataManager;
        this.pollingRate = pollingRate;
        nextStationUpdateTimes = new ConcurrentHashMap<>();
        shuttingDown = new AtomicBoolean(false);
        //startKMSChangeUpdaterThread();
    }

    /**
     * <p>Gets the only instance of a {@link KMSChangeUpdater}.</p>
     *
     * @return The only instance of a {@link KMSChangeUpdater}.
     * @throws Exception
     */
    public static KMSChangeUpdater getInstance () throws Exception {

        // check if we have a KMSChangeUpdater instance to return
        if (kmsChangeUpdater == null) {
            throw new Exception("No KMSChangeUpdater instance has been instantiated. Instantiate the KMSChangeUpdater instance with the init() method.");
        }

        return kmsChangeUpdater;
    }

    /**
     * <p>Instantiates and returns the only instance of a {@link KMSChangeUpdater}.</p>
     *
     * @return The only instance of a {@link KMSChangeUpdater}.
     * @throws Exception
     */
    public static synchronized KMSChangeUpdater init () throws Exception {

        // check if the KMSChangeUpdater instance already exists
        if (kmsChangeUpdater != null) {
            throw new Exception("An instance of KMSChangeUpdater already exists.");
        }
        else {
            DataManager dm = new DataManager();
            int pollingRate = 1;
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, "KMSDataManager.getKMSChangePollingRate",new Object[]{});
            if(xmlRpcResponse != null){
                try{
                    pollingRate = Integer.parseInt(XmlRpcUtil.getInstance().parseStringXmlRpcResponse(xmlRpcResponse, "KMSDataManager.getKMSChangePollingRate"));
                }catch(NumberFormatException e){
                    Logger.logMessage("Failed to parse KMSChangePollFrequencyMinutes in KMSChangeUpdater.init. Defaulting to 15");
                }
            }else{
                    pollingRate = getKMSChangePollingFrequencyInMinutes(dm);
                    Logger.logMessage(String.format("The WAR running on the machine with a hostname of %s is using KMS and will poll for backoffice changes every %s minutes",
                            Objects.toString(PeripheralsDataManager.getPHHostname(), "N/A"),
                            Objects.toString(pollingRate, "N/A")), Logger.LEVEL.IMPORTANT);
            }
            kmsChangeUpdater = new KMSChangeUpdater(dm, pollingRate);
        }

        return kmsChangeUpdater;
    }

    /**
     * <p>Implementation of readResolve() to prevent creating a {@link KMSChangeUpdater} through serialization.</p>
     *
     * @return The only instance of a {@link KMSChangeUpdater}.
     * @throws Exception
     */
    protected KMSChangeUpdater readResolve () throws Exception {
        return getInstance();
    }

    /**
     * <p>Overridden implementation of the {@link Runnable} run() method for a {@link KMSChangeUpdater}.</p>
     *
     */
    @Override
    public void run () {

        try {
            while ((!shuttingDown.get()) && (!Thread.currentThread().isInterrupted())) {
                if (isStartingUp) {//TODO: remove the whole isStartingUp, its not needed
//TEST_STARTUP();//TODO: REMOVE TEST CODE
                    isStartingUp = false;
                }
                lastCheck = LocalDateTime.now();
                updateKMSManagerConnectionMappings();
                updateStationConfigReload();
//TEST_RUN();//TODO: REMOVE TEST CODE
                if(!shouldCheckForUpdates())//this will always fail, just keeping it for the logging?
                    Thread.sleep(ONE_MINUTE * pollingRate);//context switching is expensive, might as well wait the whole time all at once?
            }
        }
        catch (Exception e) {
            Logger.logException(e, KMS_CHANGE_UPDATER_LOG);
            Logger.logMessage("An exception occurred in KMSChangeUpdater.run", KMS_CHANGE_UPDATER_LOG, Logger.LEVEL.ERROR);
        }
        finally {
            isTerminated = true;
            Logger.logMessage("The Thread created using the KMSChangeUpdater Runnable has been stopped in KMSChangeUpdater.run", KMS_CHANGE_UPDATER_LOG, Logger.LEVEL.IMPORTANT);
        }

    }

    private void TEST_STARTUP(){
        try{
            // TODO DELETE THIS TEST CODE
            new KMSDataManager().printOrderUsingKMSInOfflineMode(7690419);
        }catch (Exception e){
            Logger.logException(e);
        }
    }
    private void TEST_RUN() throws Exception {
        try{
        // TODO DELETE THIS TEST CODE
        Thread.sleep(10000L);
        Object resp = XmlRpcManager.getInstance().xmlRpcInvokeOnServer(KMS_CHANGE_UPDATER_LOG, "KMSOrderManager.testCall", new Object[]{});
        Logger.logMessage(XmlRpcManager.getInstance().getBooleanResponse(KMS_CHANGE_UPDATER_LOG, resp) ? "RESPONSE ==> TRUE" : "RESPONSE ==> FALSE", KMS_CHANGE_UPDATER_LOG, Logger.LEVEL.IMPORTANT);
//                XmlRpcClient client = new XmlRpcClient("http://localhost:80/kms/QuickCharge/XmlRpcManager", true);
//                Object resp = client.invoke("KMSOrderManager.testCall", new Object[]{});
        }catch (Exception e){
            Logger.logException(e);
        }
    }

    public void forceUpdate(){
        updateKMSManagerConnectionMappings();
        updateStationConfigReload();
    }
    private void updateStationConfigReload(){
        Logger.logMessage("KMSChangeUpdater.updateStationConfigReload called");
        HashMap<String, Object> mappingConfig = KMSManager.getMappingConfig();

        ArrayList<HashMap> updateTimes = getResultSetFromServerDataBase("KMSDataManager.getConfigUpdates", new Object[]{});
        for(HashMap hm: updateTimes){
            if(hm.containsKey("KMSSTATIONID")){
                String stationID = hm.get("KMSSTATIONID").toString();
                String newTime = "null";
                if(hm.get("NEXTAUTOREFRESHDTM") != null && hm.get("NEXTAUTOREFRESHDTM").toString().length() > 0)
                    newTime = hm.get("NEXTAUTOREFRESHDTM").toString();
                Logger.logMessage("KMSChangeUpdater.updateStationConfigReload new update time for station "+ stationID +" set to: " + newTime, "KMS.log", Logger.LEVEL.DEBUG);
                if(nextStationUpdateTimes.containsKey(stationID)){
                    String knownTime = nextStationUpdateTimes.get(stationID);
                    if(knownTime != null && knownTime.compareTo(newTime) == 0){
                        //the value hasnt changed.
                    }else{
                        //there was a change in the time
                        //the update time is new, so add it to the map, regardless of if it is null
                        nextStationUpdateTimes.put(stationID,newTime);
                        //and send it off to the station
                        KMSManagerSocketHandler.sendMessageDirectlyToStation(stationID, MMHEvent.CONFIGURATION_UPDATE, new KMSUpdateConfigModel(newTime, mappingConfig), null);
                    }
                }else{
                    //we didnt have a record of the next update time, so
                    nextStationUpdateTimes.put(stationID,newTime);
                    //and send it off to the station
                    KMSManagerSocketHandler.sendMessageDirectlyToStation(stationID, MMHEvent.CONFIGURATION_UPDATE, new KMSUpdateConfigModel(newTime, mappingConfig), null);
                }
            }
        }
    }

    /**
     * <p>Determines whether or not to check for backoffice changes to the KMS configuration.</p>
     *
     * @return Whether or not to check for backoffice changes to the KMS configuration.
     */
    private boolean shouldCheckForUpdates () {
        boolean shouldCheck = false;

        try {
            // get the current time
            LocalDateTime currentTime = LocalDateTime.now();
            // determine how many minutes have passed since the last check
            long minutes = lastCheck.until(currentTime, ChronoUnit.MINUTES);

            Logger.logMessage(String.format("%s minute%s %s passed since the last KMS configuration check",
                    Objects.toString(minutes, "N/A"),
                    Objects.toString(((int) minutes == 1 ? "" : "s"), "N/A"),
                    Objects.toString(((int) minutes == 1 ? "has" : "have"), "N/A")), KMS_CHANGE_UPDATER_LOG, Logger.LEVEL.DEBUG);

            // if enough time has passed then check for KMS configuration updates in the backoffice
            Logger.logMessage("MINUTES = "+minutes+", POLLING RATE = "+pollingRate, KMS_CHANGE_UPDATER_LOG, Logger.LEVEL.IMPORTANT);
            if (minutes >= pollingRate) {
                shouldCheck = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, KMS_CHANGE_UPDATER_LOG);
            Logger.logMessage("There was a problem trying to determine whether or not to check for backoffice updates to the " +
                    "KMS configuration in KMSChangeUpdater.shouldCheckForUpdates", KMS_CHANGE_UPDATER_LOG, Logger.LEVEL.ERROR);
        }

        return shouldCheck;
    }

    /**
     * <p>Queries the database to retrieve all active KMS stations.</p>
     *
     * @return A {@link CopyOnWriteArraySet} of {@link KMSStationModel} corresponding to the active KMS stations.
     */
    @SuppressWarnings({"unchecked", "Convert2streamapi"})
    private CopyOnWriteArraySet<KMSStationModel> getKmsStations () {
        CopyOnWriteArraySet<KMSStationModel> kmsStations = new CopyOnWriteArraySet<>();

        try {
            ArrayList<HashMap> queryRes = dataManager.parameterizedExecuteQuery("data.kms.getAllActiveKMSStations", new Object[]{}, true);
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                for (HashMap hm : queryRes) {
                    kmsStations.add(KMSStationModel.buildFromHM(dataManager, KMS_CHANGE_UPDATER_LOG, hm));
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, KMS_CHANGE_UPDATER_LOG);
            Logger.logMessage("There was a problem trying to get the KMS stations in KMSChangeUpdater.getKmsStations", KMS_CHANGE_UPDATER_LOG, Logger.LEVEL.ERROR);
        }

        return kmsStations;
    }

    /**
     * <p>Creates and starts a {@link Thread} using the {@link KMSChangeUpdater} {@link Runnable}.</p>
     *
     */
    public void startKMSChangeUpdaterThread () {

        try {
            // make sure isShuttingDown and isTerminated are false before creating a Thread using the KMSChangeUpdater Runnable
            isShuttingDown = false;
            isTerminated = false;
            shuttingDown.set(false);
            // create the Thread
            Thread kmsChangeUpdaterThread = new Thread(this);
            kmsChangeUpdaterThread.setName("KMS-KMSChangeUpdater");
            kmsChangeUpdaterThread.start();
            Logger.logMessage("The Thread to check for backoffice changes made to the KMS configuration has started successfully " +
                    "in KMSChangeUpdater.startKMSChangeUpdaterThread", KMS_CHANGE_UPDATER_LOG, Logger.LEVEL.IMPORTANT);
        }
        catch (Exception e) {
            Logger.logException(e, KMS_CHANGE_UPDATER_LOG);
            Logger.logMessage("Unable to start the Thread to check for backoffice changes made to the KMS configuration " +
                    "in KMSChangeUpdater.startKMSChangeUpdaterThread", KMS_CHANGE_UPDATER_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * <p>Stops the {@link Thread} created from the {@link KMSChangeUpdater} {@link Runnable}.</p>
     *
     */
    public void stopKMSChangeUpdaterThread () {

        try {
            System.out.println( System.currentTimeMillis() + " stopKMSChangeUpdaterThread called");
            // run through the Thread shutdown routine
            isShuttingDown = true;
            shuttingDown.set(true);
            // wait 3 seconds for the Thread to terminate, if it doesn't then interrupt and stop it
            int secondsPassed = 0;
            while (!isTerminated) {
                Thread.sleep(ONE_SECOND);
                secondsPassed++;
                if (secondsPassed >= 3) {
                    Logger.logMessage("The Thread created using the KMSChangeUpdater has not terminated within 3 seconds, " +
                            "it is now being interrupted in KMSChangeUpdater.stopKMSChangeUpdaterThread", KMS_CHANGE_UPDATER_LOG, Logger.LEVEL.TRACE);
                    Thread.getAllStackTraces().keySet().forEach((thread) -> {
                        if (thread.getName().equalsIgnoreCase("KMS-KMSChangeUpdater")) {
                            System.out.println( System.currentTimeMillis() + " sending interrupt to KMSChangeUpdater");
                            thread.interrupt();
                        }
                    });
                }
            }
            dataManager = null;
            nextStationUpdateTimes = null;
        }
        catch (Exception e) {
            Logger.logException(e, KMS_CHANGE_UPDATER_LOG);
            Logger.logMessage("A problem occurred when trying to stop the Thread created using the KMSChangeUpdater " +
                    "Runnable in KMSChangeUpdater.stopKMSChangeUpdaterThread", KMS_CHANGE_UPDATER_LOG, Logger.LEVEL.ERROR);
        }

    }

    /**
     * Triggers a reload of the KMSManger's connection configuration based on the server's DB
     */
    private void updateKMSManagerConnectionMappings(){
//        getResultSetFromServerDataBase("PeripheralsDataManager.getPHMacAddress", new Object[]{});
        ArrayList<HashMap> prepExpMappings = getResultSetFromServerDataBase("KMSDataManager.getPrepExpMappings", new Object[]{});
        ArrayList<HashMap> displays = getResultSetFromServerDataBase("KMSDataManager.getDisplayStations", new Object[]{});
        ArrayList<HashMap> backupAndMirrors = getResultSetFromServerDataBase("KMSDataManager.getBackupAndMirrors", new Object[]{});
        ArrayList<HashMap> websocketConfig = getResultSetFromServerDataBase("KMSDataManager.getWebsocketConfig", new Object[]{PeripheralsDataManager.getTerminalMacAddress()});
        KMSManager.updateConfiguration(prepExpMappings, displays, backupAndMirrors, websocketConfig);
    }

    //KMSManager Config update methods
    private ArrayList getResultSetFromServerDataBase(String methodName, Object[] methodParams){
        Logger.logMessage("RPC to server for " + methodName, Logger.LEVEL.DEBUG);
        String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
        String macAddress = PeripheralsDataManager.getTerminalMacAddress();
        String hostname = PeripheralsDataManager.getPHHostname();
        Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName,methodParams);

        // get any transactions from the XML RPC response so they can be printed
        if(xmlRpcResponse instanceof ArrayList){
            return (ArrayList) xmlRpcResponse;
        }else{
            ArrayList result = XmlRpcUtil.getInstance().parseArrayListXmlRpcResponse(xmlRpcResponse, methodName);
            return result;
        }
    }

    /**
     * <p>Determines how frequently the {@link KMSChangeUpdater} should check for backoffice changes to the KMS configuration in minutes.</p>
     *
     * @param dataManager The {@link DataManager} to use for any queries.
     * @return How frequently the {@link KMSChangeUpdater} should check for backoffice changes to the KMS configuration in minutes.
     */
    private static int getKMSChangePollingFrequencyInMinutes (DataManager dataManager) {
        int frequency = 10;

        try {
            Object queryRes = dataManager.parameterizedExecuteScalar("data.kms.GetKMSChangePollFrequencyInMinutes", new Object[]{});
            if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString()))) {
                frequency = Integer.parseInt(queryRes.toString());
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to determine how frequently the KMSChangeUpdater should poll for backoffice changes " +
                    "to the KMS configuration in MMHServletListener.getKMSChangePollingFrequencyInMinutes", Logger.LEVEL.ERROR);
        }

        return frequency;
    }

}