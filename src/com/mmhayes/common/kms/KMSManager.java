package com.mmhayes.common.kms;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-07-08 14:24:53 -0400 (Thu, 08 Jul 2021) $: Date of last commit
    $Rev: 14305 $: Revision of last commit
    Notes: Serializes objects to JSON and sends/receives messages from the KMS stations.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kitchenPrinters.*;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.kms.collections.StationCollection;
import com.mmhayes.common.kms.models.*;
import com.mmhayes.common.kms.orders.models.KMSModifierModel;
import com.mmhayes.common.kms.orders.models.KMSOrderModel;
import com.mmhayes.common.kms.orders.models.KMSOrphanOrderModel;
import com.mmhayes.common.kms.orders.models.KMSProductModel;
import com.mmhayes.common.kms.types.KMSOrderStatus;
import com.mmhayes.common.printing.PrintStatus;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.Formatter.KitchenPrinterReceiptFormatter;
import com.mmhayes.common.receiptGen.ReceiptData.KDSJobInfo;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHPair;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.utils.stringMatchingUtils.RabinKarp;
import com.mmhayes.common.websockets.MMHEvent;
import com.mmhayes.common.websockets.MMHSocket;
import com.mmhayes.common.websockets.MMHSocketPinger;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

import java.util.concurrent.*;

/**
 * Serializes objects to JSON and sends/receives messages from the KMS stations.
 *
 */
public class KMSManager {

    //use a custom set of connections that is NOT the global list of connections. By default, the global list is used.
    //For the KMS Manager we want to send events to the next station in the list and the display stations. For this, we need a few maps...
    //this would be a list of station IDs that should receive messages from the current station.
    //If a station ID in this list sends a message, these are the stations that the message should be sent to
    protected static final ConcurrentHashMap<String, CopyOnWriteArraySet<String>> stationIDsToListenerIDs= new ConcurrentHashMap<>();
    protected static final ConcurrentHashMap<String, CopyOnWriteArraySet<String>> displayToPrepIDs= new ConcurrentHashMap<>();
    //if a station ID is in this list, the mapped value should always be used.
    protected static final ConcurrentHashMap<String,String> stationIDToManualBackupStationID = new ConcurrentHashMap<>();
    //If a station ID is in this list, and the station is offline, then this ID should beused.
    protected static final ConcurrentHashMap<String,String> stationIDToAutomaticBackupStationID = new ConcurrentHashMap<>();
    //If a station ID is in this list is about to receive a message, it should also send the message to the mapped station
    protected static final ConcurrentHashMap<String,String> stationIDToMirrorStationID = new ConcurrentHashMap<>(); //a mapping from a stationID to the one it is mirroring
    protected static final ConcurrentHashMap<String,CopyOnWriteArraySet<String>> stationIDToMirrorPool = new ConcurrentHashMap<>();

    protected static final CopyOnWriteArraySet<String> expeditorIDs = new CopyOnWriteArraySet<>();
    protected static final CopyOnWriteArraySet<String> remoteOrderIDs = new CopyOnWriteArraySet<>();
    protected static final CopyOnWriteArraySet<String> displayIDs = new CopyOnWriteArraySet<>();

    private static ScheduledExecutorService PingPongExecutor;// = Executors.newSingleThreadScheduledExecutor();



    // log file to be used by the KitchenPrinterDataManager
    private static final String KMS_LOG = "KMS.log";
    private static final ConcurrentHashMap<String, Integer> eventIDs  = new ConcurrentHashMap();
    private static final ConcurrentHashMap<Integer, String> statusNames  = new ConcurrentHashMap();
    //hold the most UTD status of orders locally PATransactionID -> Order
    ConcurrentHashMap<String, KMSOrderModel> orderStatusMap = new ConcurrentHashMap<>();
    LinkedList<String> finishedOrders = new LinkedList<>();
    protected static final CopyOnWriteArraySet<KMSOrphanOrderModel> pendingOrphanOrders = new CopyOnWriteArraySet<>();

    //Make KMSManager a singleton
    private static KMSManager instance = null;
    private static boolean acceptingNewConnections = false;

    private KMSManager(){
        instance = this;
    }
    public static KMSManager getInstance(){
         if(instance != null){
             return instance;
         }else{
             return new KMSManager();
         }
    }

    /**
     * updates the configuration list with station -> backup, station -> mirror, station -> display, and expeditor -> prep / prep -> expeditor
     */
    public static void updateConfigurationFromLocalDB(){

        DataManager dm = new DataManager();
        ArrayList<HashMap> prepExpMappings = dm.parameterizedExecuteQuery("data.KMSManager.getPrepExpMappings", new Object[]{},true);
        ArrayList<HashMap> displays = dm.parameterizedExecuteQuery("data.KMSManager.getDisplayStations", new Object[]{},true);
        ArrayList<HashMap> backupAndMirrors = dm.parameterizedExecuteQuery("data.KMSManager.getBackupAndMirrors", new Object[]{},true);
        ArrayList<HashMap> websocketConfig = dm.parameterizedExecuteQuery("KMSDataManager.getWebsocketConfig", new Object[]{PeripheralsDataManager.getTerminalMacAddress()}, true);
        updateConfiguration(prepExpMappings, displays, backupAndMirrors, websocketConfig);
    }

    public static void updateConfiguration(ArrayList<HashMap> prepExpMappings, ArrayList<HashMap> displays, ArrayList<HashMap> backupAndMirrors, ArrayList<HashMap>  websocketConfig){
        stationIDsToListenerIDs.clear();
        stationIDToAutomaticBackupStationID.clear();
        stationIDToMirrorStationID.clear();
        stationIDToMirrorPool.clear();
        expeditorIDs.clear();
        remoteOrderIDs.clear();
        displayIDs.clear();
        displayToPrepIDs.clear();

        //Based on query: "data.KMSManager.getPrepExpMappings" run on the server
        for(HashMap mapping: prepExpMappings){
            if(mapping.get("PREPSTATIONID") != null){
                if(mapping.get("EXPEDITORSTATIONID") != null){
                    if(stationIDsToListenerIDs.get(mapping.get("PREPSTATIONID").toString()) == null)
                        stationIDsToListenerIDs.put(mapping.get("PREPSTATIONID").toString(), new CopyOnWriteArraySet<String>());
                    stationIDsToListenerIDs.get(mapping.get("PREPSTATIONID").toString()).add(mapping.get("EXPEDITORSTATIONID").toString());

                    if(stationIDsToListenerIDs.get(mapping.get("EXPEDITORSTATIONID").toString()) == null)
                        stationIDsToListenerIDs.put(mapping.get("EXPEDITORSTATIONID").toString(), new CopyOnWriteArraySet<String>());
                    stationIDsToListenerIDs.get(mapping.get("EXPEDITORSTATIONID").toString()).add(mapping.get("PREPSTATIONID").toString());
                    expeditorIDs.add(mapping.get("EXPEDITORSTATIONID").toString());
                }
                if(mapping.get("REMOTEORDERSTATION") != null){
                    if(stationIDsToListenerIDs.get(mapping.get("PREPSTATIONID").toString()) == null)
                        stationIDsToListenerIDs.put(mapping.get("PREPSTATIONID").toString(), new CopyOnWriteArraySet<String>());
                    stationIDsToListenerIDs.get(mapping.get("PREPSTATIONID").toString()).add(mapping.get("REMOTEORDERSTATION").toString());

                    if(stationIDsToListenerIDs.get(mapping.get("REMOTEORDERSTATION").toString()) == null)
                        stationIDsToListenerIDs.put(mapping.get("REMOTEORDERSTATION").toString(), new CopyOnWriteArraySet<String>());
                    stationIDsToListenerIDs.get(mapping.get("REMOTEORDERSTATION").toString()).add(mapping.get("PREPSTATIONID").toString());
                    remoteOrderIDs.add(mapping.get("REMOTEORDERSTATION").toString());
                }
            }
        }

        //RO stations and Exp stations need to listen to eachother
        for(String remoteStationID : remoteOrderIDs){
            for(String expStationID: expeditorIDs){
                if(stationIDsToListenerIDs.get(remoteStationID) == null)
                    stationIDsToListenerIDs.put(remoteStationID, new CopyOnWriteArraySet<String>());
                stationIDsToListenerIDs.get(remoteStationID).add(expStationID);

                if(stationIDsToListenerIDs.get(expStationID) == null)
                    stationIDsToListenerIDs.put(expStationID, new CopyOnWriteArraySet<String>());
                stationIDsToListenerIDs.get(expStationID).add(remoteStationID);
            }
        }

        //Based on query: "data.KMSManager.getDisplayStations"
        for(HashMap display: displays){
            if(display.get("KMSSTATIONID") != null){
                if(display.get("DISPLAYKMSSTATIONID") != null && display.get("DISPLAYKMSSTATIONID").toString().length() > 0){
                    if(stationIDsToListenerIDs.get(display.get("KMSSTATIONID").toString()) == null)
                        stationIDsToListenerIDs.put(display.get("KMSSTATIONID").toString(), new CopyOnWriteArraySet<String>());
                    stationIDsToListenerIDs.get(display.get("KMSSTATIONID").toString()).add(display.get("DISPLAYKMSSTATIONID").toString());
                    displayIDs.add(display.get("DISPLAYKMSSTATIONID").toString());
                    if(displayToPrepIDs.get(display.get("DISPLAYKMSSTATIONID").toString()) == null)
                        displayToPrepIDs.put(display.get("DISPLAYKMSSTATIONID").toString(), new CopyOnWriteArraySet<String>());
                    if(!displayToPrepIDs.get(display.get("DISPLAYKMSSTATIONID").toString()).contains(display.get("KMSSTATIONID").toString()))
                        displayToPrepIDs.get(display.get("DISPLAYKMSSTATIONID").toString()).add(display.get("KMSSTATIONID").toString());

//                    //displays should listen to expeditors too  //commented out because its a mapping you have to setup in the backoffice...
//                    for(String expID : expeditorIDs){
//                        stationIDsToListenerIDs.get(expID).add(display.get("DISPLAYKMSSTATIONID").toString());
//                    }
                }
            }
        }

        //Based on query: "data.KMSManager.getBackupAndMirrors"
        for(HashMap mapping: backupAndMirrors){
            if(mapping.get("KMSSTATIONID") != null){
                if(mapping.get("BACKUPKMSSTATIONID") != null && mapping.get("BACKUPKMSSTATIONID").toString().length() > 0){
                    stationIDToAutomaticBackupStationID.put(mapping.get("KMSSTATIONID").toString(), mapping.get("BACKUPKMSSTATIONID").toString());
                }
                if(mapping.get("MIRRORKMSSTATIONID") != null && mapping.get("MIRRORKMSSTATIONID").toString().length() > 0){
                    String mirror = mapping.get("KMSSTATIONID").toString();
                    String mirrorED = mapping.get("MIRRORKMSSTATIONID").toString();
                    stationIDToMirrorStationID.put(mirror, mirrorED);
                    if(stationIDToMirrorPool.containsKey(mirrorED)){
                        CopyOnWriteArraySet<String> pool = stationIDToMirrorPool.get(mirrorED);
                        pool.add(mirror);
                        stationIDToMirrorPool.put(mirror, pool);
                    }else{
                        CopyOnWriteArraySet<String> pool = new CopyOnWriteArraySet<>();
                        pool.add(mirror);
                        pool.add(mirrorED);
                        stationIDToMirrorPool.put(mirror, pool);
                        stationIDToMirrorPool.put(mirrorED, pool);

                    }
                }
            }
        }

        if(instance.acceptingNewConnections && !PingPongExecutor.isShutdown() && websocketConfig != null && websocketConfig.size() > 0){
            ShutdownPingerTask();
            MMHSocket.PING_PONG_FREQUENCY = Long.parseLong(websocketConfig.get(0).getOrDefault("WEBSOCKETPINGFREQUENCYMS", "1000").toString());
            MMHSocket.MISSED_PING_PONGS_FOR_TIMEOUT = Long.parseLong(websocketConfig.get(0).getOrDefault("WEBSOCKETMISSEDPONGMAX", "3").toString());
            StartUpPingerTask();
        }
    }

    public static void startUp(){
        getInstance();
        if(instance != null){
            //cashe eventIDs
            if(eventIDs != null) eventIDs.clear();
            ArrayList<HashMap> ids = new DataManager().parameterizedExecuteQuery("data.KMSEvent.getEventIDs", new Object[]{}, true);
            for(HashMap id: ids){
                eventIDs.put(id.get("NAME").toString().toLowerCase().replaceAll(" ", "-"), Integer.parseInt(id.get("KMSEVENTTYPEID").toString()));
            }

            instance.acceptingNewConnections = true;
            Logger.logMessage("KMSManager.startUp() has run and will now accept new websocket connections ", KMS_LOG, Logger.LEVEL.IMPORTANT);

            StartUpPingerTask();

        }else
            Logger.logMessage("Failed to create instance of KMSManager in KMSManager.startUp()", KMS_LOG, Logger.LEVEL.ERROR);

    }
    public static void shutdown(){
        System.out.println( System.currentTimeMillis() + " KMSManager.shutdown called");
        if(instance != null){
            instance.acceptingNewConnections = false;

            ShutdownPingerTask();
        }
        KMSManagerSocketHandler.closeAllConnections(KMSCloseReason.PRINTER_HOST_SHUTTING_DOWN);
        Logger.logMessage("KMSManager.shutdown() has terminated existing connections and will now block new websocket connections ", KMS_LOG, Logger.LEVEL.IMPORTANT);

    }

    public static void StartUpPingerTask(){

        try {
            if(PingPongExecutor != null && !PingPongExecutor.isShutdown()){
                Logger.logMessage("Attempted to startup another PingPongExecutor while the current one was not shutdown. Shutting down now", KMS_LOG, Logger.LEVEL.ERROR);
                ShutdownPingerTask();
            }
            String logMessage = "Starting Ping-Pong Task frequency:" + MMHSocket.PING_PONG_FREQUENCY + "ms";
            Logger.logMessage(logMessage, KMS_LOG, Logger.LEVEL.TRACE);
            PingPongExecutor = Executors.newSingleThreadScheduledExecutor();
            PingPongExecutor.scheduleWithFixedDelay(
                    new MMHSocketPinger(),
                    0,
                    MMHSocket.PING_PONG_FREQUENCY,
                    TimeUnit.MILLISECONDS
            );
        } catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("Error creating Ping-Pong executor...", KMS_LOG, Logger.LEVEL.ERROR);
        }
    }

    public static void ShutdownPingerTask(){
        PingPongExecutor.shutdownNow();
        while(!PingPongExecutor.isTerminated()) {
            Logger.logMessage("Waiting for Ping Pong termination", KMS_LOG, Logger.LEVEL.IMPORTANT);
            try {
                PingPongExecutor.awaitTermination(1, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Logger.logMessage("Error in ShutdownPingerTask method.", KMS_LOG, Logger.LEVEL.IMPORTANT);
            }
        }
        PingPongExecutor = null;
    }

    public static boolean isAcceptingNewConnections(){
        return acceptingNewConnections;
    }

    /**
     * Get a mapping from job detail IDs to KMS product order status
     * @return
     */
    public HashMap<Integer, Integer> getPrintJobDetailIDsToOrderProductStatusMap(){
        HashMap<Integer, Integer> products = new HashMap<>();
        for(String transactionID: orderStatusMap.keySet()){
            KMSOrderModel order = orderStatusMap.get(transactionID);
            //if(order.originalPrintJob.getPaPrinterQueueID() == printJobID){
                for(KMSProductModel product: order.getProducts()){
                    products.put(product.printJobDetailId, product.getOrderStatusId());
                    for(KMSModifierModel mod: product.getModifiers()){
                        products.put(mod.printJobDetailId, product.getOrderStatusId());
                    }
                }
            //}
        }
        return products;
    }

    /**
     * <p>Re-maps an order to a print job if the order isn't mapped to an existing print job.</p>
     *
     * @param qcPrintJob The {@link QCPrintJob} to re-map.
     * @return Whether or not the print job could be re-mapped.
     */
    @SuppressWarnings("Convert2streamapi")
    public boolean setOrderMapPrintJob (QCPrintJob qcPrintJob) {

        // make sure the order status Map has content
        if (DataFunctions.isEmptyMap(orderStatusMap)) {
            Logger.logMessage("The order status Map within KMSManager.setOrderMapPrintJob can't be null or empty!", KMS_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure there is a print job present
        if (qcPrintJob == null) {
            Logger.logMessage("The QCPrintJob passed to KMSManager.setOrderMapPrintJob can't be null!", KMS_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the print job has details
        ArrayList<QCPrintJobDetail> qcPrintJobDetails = qcPrintJob.getPrintJobDetails();
        if (DataFunctions.isEmptyCollection(qcPrintJobDetails)) {
            Logger.logMessage(String.format("The print job with a print job tracker ID of %s that was passed to KMSManager.setOrderMapPrintJob has no details!",
                    Objects.toString(qcPrintJob.getJobTrackerID(), "N/A")), KMS_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // check that the order status Map contains the transaction ID
        String transactionID = String.valueOf(qcPrintJob.getPaTransactionID());
        if (!orderStatusMap.containsKey(transactionID)) {
            Logger.logMessage(String.format("The order status Map in KMSManager.setOrderMapPrintJob doesn't contain the transaction " +
                    "ID %s, unable to map the print job with a print job tracker ID of %s!",
                    Objects.toString(transactionID, "N/A"),
                    Objects.toString(qcPrintJob.getJobTrackerID(), "N/A")), KMS_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // set the original print job for the KMSOrderModel if it hasn't been set yet
        KMSOrderModel kmsOrderModel = orderStatusMap.get(transactionID);
        if (kmsOrderModel.originalPrintJob == null) {
            kmsOrderModel.originalPrintJob = qcPrintJob;
        }

        // make sure the KMSOrderModel contains products
        ArrayList<KMSProductModel> kmsProductModels = kmsOrderModel.getProducts();
        if (DataFunctions.isEmptyCollection(kmsProductModels)) {
            Logger.logMessage(String.format("The KMSOrderModel with an ID of %s within KMSManager.setOrderMapPrintJob doesn't contain any products!",
                    Objects.toString(kmsOrderModel.getId(), "N/A")), KMS_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // re-map the detail mapping for the products
        for (KMSProductModel kmsProductModel : kmsProductModels) {
            if (kmsProductModel.printJobDetailId == 0) {
                for (QCPrintJobDetail qcPrintJobDetail : qcPrintJobDetails) {
                    if ((qcPrintJobDetail.getPAPluID() == kmsProductModel.getProductId())
                            && (qcPrintJobDetail.getQuantity() == kmsProductModel.getQuantity())
                            && (qcPrintJobDetail.isModifier() == kmsProductModel.isModifier())) {
                        kmsProductModel.printJobDetailId = qcPrintJobDetail.getPaPrinterQueueDetailID();
                    }
                }
            }
            // check modifiers attached to the product
            ArrayList<KMSModifierModel> kmsModifierModels = kmsProductModel.getModifiers();
            if (!DataFunctions.isEmptyCollection(kmsModifierModels)) {
                for (KMSModifierModel kmsModifierModel : kmsModifierModels) {
                    if (kmsModifierModel.printJobDetailId == 0) {
                        for (QCPrintJobDetail qcPrintJobDetail : qcPrintJobDetails) {
                            if ((qcPrintJobDetail.getPAPluID() == kmsModifierModel.getProductId())
                                    && (kmsProductModel.getProductId() == kmsModifierModel.getParentProductId())) {
                                kmsModifierModel.printJobDetailId = qcPrintJobDetail.getPaPrinterQueueDetailID();
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    public int getProductStatusOfPrintJobDetail(int printJobDetailID){
        //TODO: need to pass in transaction ID to setup mapping if it doesnt exit(in the case of the order being reuploaded from the station, after a printerhost shutdown
        for(String transactionID: orderStatusMap.keySet()){
            KMSOrderModel order = orderStatusMap.get(transactionID);
            for(KMSProductModel productModel : order.getProducts()){
                if(productModel.printJobDetailId == printJobDetailID)
                    return productModel.getOrderStatusId();
            }
        }
        return 0;
    }

    public int getOrderStatusOfPrintJob(int printJobID){
        //TODO: need to pass in transaction ID to setup mapping if it doesnt exit(in the case of the order being reuploaded from the station, after a printerhost shutdown
        for(String transactionID: orderStatusMap.keySet()){
            KMSOrderModel order = orderStatusMap.get(transactionID);
            if(order != null && order.originalPrintJob != null && order.originalPrintJob.getPaPrinterQueueID() == printJobID){
                return order.getOrderStatusId();
            }
        }
        return 0;
    }

    public void uploadOrdersFromStation(String originatingStationID, KMSOrderModel[] orders, String stationReferenceID){
        Logger.logMessage("KMSManager.uploadOrdersFromStation", KMS_LOG, Logger.LEVEL.TRACE);
        //for every kms order model, check and see if it and all of the details are in the order status map.
            //if not, add it
        //if a station uploads a known order and that station is not the handling station: ignore it
        if(orders != null){
            for(int i = 0; i < orders.length; i++){
                KMSOrderModel order = orders[i];
                if(!orderStatusMap.containsKey(order.getId()+"")){
                    if(!finishedOrders.contains(order.getId())){
                        for(KMSProductModel product: order.getProducts()){
                            product.handlingKMSStationId = originatingStationID;
                            //TODO: there is a edge case race condition here... the order in which stations come back online to an ignorant host would matter....
                        }
                        orderStatusMap.put(order.getId()+"", order);
                    }else{
                        //this order was finished
                        KMSManagerSocketHandler.sendErrorMessage(
                                originatingStationID,
                                MMHEvent.ORDER_UPLOAD,
                                originatingStationID + "",
                                "Order was previously finished.",
                                stationReferenceID);
                    }
                }else{
                    //now check the details (different prep stations could have subsets of the entire order)
                    // knownOrder -> the order that the printer host already knew about
                    KMSOrderModel knownOrder = orderStatusMap.get(order.getId()+"");
                    // uploadedProduct -> the product that has been sent by a station that has just come back online
                    for (KMSProductModel uploadedProduct : order.getProducts()) {
                        boolean known = false;

                        // knownProduct -> the product as the printer host is aware of it
                        for (KMSProductModel knownProduct : knownOrder.getProducts()) {
                            // if the knownProduct is the uploadedProduct, then update the knownProduct to be
                            // in line with the newly known status from the uploadedProduct
                            if (knownProduct.getTransLineId().equalsIgnoreCase(uploadedProduct.getTransLineId())) {
                                known = true;

                                // only update the product status if the uploaded status is further along in "the chain"
                                // than the status already assigned to the product
                                if (KMSOrderStatus.compare(uploadedProduct.getOrderStatusId(), knownProduct.getOrderStatusId()) >= 1) {

                                    // only allow the an update to finalized if the order has been uploaded from the expeditor printer
                                    // or there are no expeditor products / no expeditor printer
                                    if (uploadedProduct.getOrderStatusId() != KMSOrderStatus.FINALIZED ||
                                            (uploadedProduct.getOrderStatusId() == KMSOrderStatus.FINALIZED &&
                                                ((!hasExpeditorProducts(knownOrder) && !hasExpeditorProducts(order)) ||
                                                    getExpeditorOrBackup() == null || getExpeditorOrBackup().equals(originatingStationID)))) {
                                        knownProduct.setOrderStatusId(uploadedProduct.getOrderStatusId());
                                    }
                                    else if (uploadedProduct.getOrderStatusId() == KMSOrderStatus.FINALIZED) {
                                        knownProduct.setOrderStatusId(KMSOrderStatus.COMPLETED);
                                    }

                                }

                                if (knownProduct.handlingKMSStationId == null) {
                                    knownProduct.handlingKMSStationId = originatingStationID;
                                }
                            }
                        }

                        if (!known) {
//                            if (uploadedProduct.handlingKMSStationId == null) {
//                                uploadedProduct.handlingKMSStationId = originatingStationID;
//                            }
                            uploadedProduct.handlingKMSStationId = originatingStationID;
                            knownOrder.getProducts().add(uploadedProduct);
                        }
                    }

                    // only update the order status if the uploaded status is further along in "the chain"
                    // than the status already assigned to the order
                    if (KMSOrderStatus.compare(order.getOrderStatusId(), knownOrder.getOrderStatusId()) >= 1) {
                        // only allow the an update to finalized if the order has been uploaded from the expeditor printer
                        // or there are no expeditor products / no expeditor printer
                        if (order.getOrderStatusId() != KMSOrderStatus.FINALIZED ||
                                (order.getOrderStatusId() == KMSOrderStatus.FINALIZED &&
                                        ((!hasExpeditorProducts(knownOrder) && !hasExpeditorProducts(order)) ||
                                                getExpeditorOrBackup() == null || getExpeditorOrBackup().equals(originatingStationID)))) {
                            knownOrder.setOrderStatusId(order.getOrderStatusId());
                        }
                        // if we are denying the update to finalized, just update the order to completed
                        else if (order.getOrderStatusId() == KMSOrderStatus.FINALIZED) {
                            knownOrder.setOrderStatusId(KMSOrderStatus.COMPLETED);
                        }
                    }

                }
                KMSManagerSocketHandler.sendConfirmationMessage(
                        originatingStationID,
                        MMHEvent.ORDER_UPLOAD,
                        order.getId()+"",
                        "Order "+order.getId()+" Uploaded",
                        stationReferenceID);
            }
        }else{
            KMSManagerSocketHandler.sendErrorMessage(
                    originatingStationID,
                    MMHEvent.ORDER_UPLOAD,
                    originatingStationID + "",
                    "Missing Required fields.",
                    stationReferenceID);
        }
    }

    //Handling messages from client functions
    public void backupManually(String backupStationID, String stationBeingBackedUpID, String stationReferenceID){
        //Add the manual backup
        Logger.logMessage("KMSManager.backupManually", KMS_LOG, Logger.LEVEL.TRACE);
        if(stationBeingBackedUpID != null && stationBeingBackedUpID.trim().length() > 0 && Integer.parseInt(stationBeingBackedUpID.trim()) > 0){
            String note;
            boolean validBackup = true;
            //TODO: validate that the station can do this
            if(validBackup){
//              if a manual backup for ED exists{
                if(stationIDToManualBackupStationID.containsKey(stationBeingBackedUpID)){
//                  if the station that raised the MB event is the backING up station{
                    if(stationIDToManualBackupStationID.get(stationBeingBackedUpID) != null &&  stationIDToManualBackupStationID.get(stationBeingBackedUpID).equals(backupStationID)){
//                        end the manual backup.
                        stationIDToManualBackupStationID.remove(stationBeingBackedUpID);
                        note = "Removed manual backup of " + stationBeingBackedUpID + " by " + backupStationID;
//                        Send ING a confirmation
                        KMSManagerSocketHandler.sendMessageDirectlyToStationAndMirrors(backupStationID,MMHEvent.CONFIRMATION,
                                KMSManagerSocketHandler.buildInformationMessage(
                                        backupStationID,MMHEvent.KMSEventType.MANUAL_BACKUP,stationBeingBackedUpID,note), stationReferenceID);
//                        Send ED a MB
                        KMSManagerSocketHandler.sendMessageDirectlyToStationAndMirrors(stationBeingBackedUpID,MMHEvent.KMSEventType.MANUAL_BACKUP,
                                KMSManagerSocketHandler.buildInformationMessage(
                                        backupStationID,MMHEvent.KMSEventType.MANUAL_BACKUP,stationBeingBackedUpID,note), stationReferenceID);
//                  } else if the station that raised the MB event is the backED up station{
                    }else if(backupStationID.compareTo(stationBeingBackedUpID) == 0){
//                        end the manual backup,
                        note = "Removed manual backup of " + stationBeingBackedUpID + " by " + stationIDToManualBackupStationID.get(stationBeingBackedUpID);
                        String backINGUpStation = stationIDToManualBackupStationID.get(stationBeingBackedUpID);
                        stationIDToManualBackupStationID.remove(stationBeingBackedUpID);
//                        Send ED a confirmation,
                        KMSManagerSocketHandler.sendMessageDirectlyToStationAndMirrors(stationBeingBackedUpID,MMHEvent.CONFIRMATION,
                                KMSManagerSocketHandler.buildInformationMessage(
                                        backupStationID,MMHEvent.KMSEventType.MANUAL_BACKUP,stationBeingBackedUpID,note), stationReferenceID);
//                        Send ING a MB
                        KMSManagerSocketHandler.sendMessageDirectlyToStationAndMirrors(backINGUpStation,MMHEvent.KMSEventType.MANUAL_BACKUP,
                                KMSManagerSocketHandler.buildInformationMessage(
                                        backupStationID,MMHEvent.KMSEventType.MANUAL_BACKUP,stationBeingBackedUpID,note), stationReferenceID);
//                  } otherwise {
                    }else{
//                        replace the backING up station
                        note = "Station " + backupStationID + " manually backing up station " + stationBeingBackedUpID + ", replacing " + stationIDToManualBackupStationID.get(stationBeingBackedUpID);
                        String previousBackup = stationIDToManualBackupStationID.get(stationBeingBackedUpID);
                        stationIDToManualBackupStationID.put(stationBeingBackedUpID, backupStationID);
//                        Send previous ING a MB,
                        KMSManagerSocketHandler.sendMessageDirectlyToStationAndMirrors(previousBackup,MMHEvent.KMSEventType.MANUAL_BACKUP,
                                KMSManagerSocketHandler.buildInformationMessage(
                                        backupStationID,MMHEvent.KMSEventType.MANUAL_BACKUP,stationBeingBackedUpID,note), stationReferenceID);
//                        send ED an MB,
                        KMSManagerSocketHandler.sendMessageDirectlyToStationAndMirrors(stationBeingBackedUpID,MMHEvent.KMSEventType.MANUAL_BACKUP,
                                KMSManagerSocketHandler.buildInformationMessage(
                                        backupStationID,MMHEvent.KMSEventType.MANUAL_BACKUP,stationBeingBackedUpID,note), stationReferenceID);
//                        send the replacement ING a confirmation
                        KMSManagerSocketHandler.sendMessageDirectlyToStationAndMirrors(backupStationID,MMHEvent.CONFIRMATION,
                                KMSManagerSocketHandler.buildInformationMessage(
                                        backupStationID,MMHEvent.KMSEventType.MANUAL_BACKUP,stationBeingBackedUpID,note), stationReferenceID);
//                  }
                    }
//              }otherwise{
                }else{
//                    create the manual backup.
                    note = "Station " + backupStationID + " manually backing up station " + stationBeingBackedUpID;
                    stationIDToManualBackupStationID.put(stationBeingBackedUpID, backupStationID);
//                      Send ED a MB,
                    KMSManagerSocketHandler.sendMessageDirectlyToStationAndMirrors(stationBeingBackedUpID,MMHEvent.KMSEventType.MANUAL_BACKUP,
                            KMSManagerSocketHandler.buildInformationMessage(
                                    backupStationID,MMHEvent.KMSEventType.MANUAL_BACKUP,stationBeingBackedUpID,note), stationReferenceID);
//                      Send ING a confirmation.
                    KMSManagerSocketHandler.sendMessageDirectlyToStationAndMirrors(backupStationID,MMHEvent.CONFIRMATION,
                            KMSManagerSocketHandler.buildInformationMessage(
                                    backupStationID,MMHEvent.KMSEventType.MANUAL_BACKUP,stationBeingBackedUpID,note), stationReferenceID);
//                }
                }
            }else{
                //this station can not backup the selected station.
                KMSManagerSocketHandler.sendErrorMessage(
                        backupStationID,
                        MMHEvent.KMSEventType.MANUAL_BACKUP,
                        stationBeingBackedUpID + "",
                        "Backup failed validation.",
                        stationReferenceID);
            }
        }else{
            KMSManagerSocketHandler.sendErrorMessage(
                    backupStationID,
                    MMHEvent.KMSEventType.MANUAL_BACKUP,
                    stationBeingBackedUpID + "",
                    "Missing Required fields.",
                    stationReferenceID);
        }

    }

    private boolean isAManualBackup(String stationID){
        for(String backedUp: stationIDToManualBackupStationID.keySet()){
            if(stationIDToAutomaticBackupStationID.get(backedUp).equals(stationID))
                return true;
        }
        return false;
    }

    /**
     * A station is updating the status of a product in the order
     * @param originatingStationID
     * @param status
     * @param note
     */
    public void updateOrderProductStatus(String originatingStationID, KMSStatusModel status, String note, String stationReferenceID){
        Logger.logMessage("KMSManager.updateOrderProductStatus", KMS_LOG, Logger.LEVEL.TRACE);
        String event = getEvent(status.KMSOrderStatusID, status.PreviousKMSOrderStatusID);

        //get the orderID and update the local memory copy of the order
        if(status != null && status.PATransactionID != null && status.PATransLineItemID != null && status.KMSOrderStatusID != null){
            if(orderStatusMap.containsKey(status.PATransactionID.toString())){
                ArrayList<KMSProductModel> products = orderStatusMap.get(status.PATransactionID.toString()).getProducts();// .setOrderStatusId(status.KMSOrderStatusID);
                for(KMSProductModel product : products){
                    if(product.getTransLineId().compareTo(status.PATransLineItemID) == 0){//Note:
                        product.setOrderStatusId(status.KMSOrderStatusID);
                        //update the print status depending on the status of the detail
                        try {
                            int jobID = product.printJobDetailId;
                            if(PrintJobStatusHandler.getInstance().getPrintStatusMap().containsKey(jobID)){
                                if(status.KMSOrderStatusID == KMSOrderStatus.ERROR || status.KMSOrderStatusID == KMSOrderStatus.CANCELED){//Errored or Canceled status -> Error Print Status
                                    PrintJobStatusHandler.getInstance().getPrintStatusMap().put(jobID,
                                            new PrintStatus()
                                                    .addPrintStatusType(PrintStatusType.ERROR)
                                                    .addUpdatedDTM(LocalDateTime.now()));
                                }else if(status.KMSOrderStatusID == KMSOrderStatus.FINALIZED){//Finalized -> done print status
                                    PrintJobStatusHandler.getInstance().getPrintStatusMap().put(jobID,
                                            new PrintStatus()
                                                    .addPrintStatusType(PrintStatusType.DONE)
                                                    .addUpdatedDTM(LocalDateTime.now()));
                                }
                            }
                        } catch (Exception e) {
                            Logger.logException(e, KMS_LOG);
                        }
                    }
                }
                //send the order status to the server
                KMSOrderStatusUpdateQueueHandler.getInstance().offerToStatusUpdateQueue(status);
                //queue the event to be logged on the server
                KMSManager.logEvent(Integer.parseInt(originatingStationID), status.PATransactionID, status.PATransLineItemID, event, note, status.KMSOrderStatusID);
                //forward the event to any listening stations/backups and their mirrors
                KMSManagerSocketHandler.sendMessageToListeningOnlineStationsButNotOriginating(originatingStationID, MMHEvent.ORDER_ITEM_STATUS_UPDATE, status, stationReferenceID);

                KMSManagerSocketHandler.sendConfirmationMessage(originatingStationID,MMHEvent.ORDER_ITEM_STATUS_UPDATE,status.PATransLineItemID+"","Product Status Updated", stationReferenceID);
            }else{
                Logger.logMessage("Station attempted to update an order that this host is unaware of", KMS_LOG, Logger.LEVEL.WARNING);
                KMSManagerSocketHandler.sendErrorMessage(
                        originatingStationID,
                        MMHEvent.ORDER_ITEM_STATUS_UPDATE,
                        status.PATransactionID + "",
                        "The specified order is not known to the host.",
                        stationReferenceID);
            }

        }else{
            KMSManagerSocketHandler.sendErrorMessage(
                    originatingStationID,
                    MMHEvent.ORDER_ITEM_STATUS_UPDATE,
                    status.PATransactionID + "",
                    "Missing Required fields.",
                    stationReferenceID);
        }
    }

    /**
     * A station is updating an order
     * @param originatingStationID
     * @param status
     * @param note
     */
    public void updateOrderStatusOLD(String originatingStationID, KMSStatusModel status, String note, String stationReferenceID){
        Logger.logMessage("KMSManager.updateOrderStatus", KMS_LOG, Logger.LEVEL.TRACE);

        //update the in memory copy of the status
        if(status != null && status.PATransactionID != null && status.KMSOrderStatusID != null && status.PreviousKMSOrderStatusID != null){
            String event = getEvent(status.KMSOrderStatusID, status.PreviousKMSOrderStatusID);
            if(orderStatusMap.containsKey(status.PATransactionID.toString())){
                //TODO: this isnt as cut and dry as originally planned... a status update coming from a station may or may not have the whole order, and may attempt to override the state of the whole order
                boolean allowed = false;//should the new status be applied to the whole order?
                //if(forewardStatusToListeners(status.KMSOrderStatusID, orderStatusMap.get(status.PATransactionID.toString()))){
                if(status.KMSOrderStatusID == KMSOrderStatus.FINALIZED && (
                    (connectedExpCount() > 0 && hasExpeditorProducts(orderStatusMap.get(status.PATransactionID.toString())))  ||
                    (connectedROCount() > 0 && hasRemoteOrderProducts(orderStatusMap.get(status.PATransactionID.toString())))
                )){
                    if(expeditorIDs.contains(originatingStationID) || remoteOrderIDs.contains(originatingStationID))
                        allowed = true;
                }else{
                    //allow from any station
                    allowed = true;
                }
                //}
                if(allowed){
                    orderStatusMap.get(status.PATransactionID.toString()).setOrderStatusId(status.KMSOrderStatusID);
                    //send off the status to the server
                    KMSOrderStatusUpdateQueueHandler.getInstance().offerToStatusUpdateQueue(status);
                    //queue the event to be logged
                    KMSManager.logEvent(Integer.parseInt(originatingStationID), status.PATransactionID, status.PATransLineItemID, event, note, status.KMSOrderStatusID);
                    //forward the event to any listening stations/backups and their mirrors
                    KMSManagerSocketHandler.sendMessageToListeningOnlineStations(originatingStationID, MMHEvent.ORDER_STATUS_UPDATE, status, stationReferenceID);
                    //and send the message to anything that would normally be listening to the manual backup
                    for(String backedUp: stationIDToManualBackupStationID.keySet()){
                        if(stationIDToAutomaticBackupStationID.get(backedUp).equals(originatingStationID)){
                            //the backedUp staiton is being manually backed up by this station, so send the event too
                            KMSManagerSocketHandler.sendMessageToListeningOnlineStations(backedUp, MMHEvent.ORDER_STATUS_UPDATE, status, stationReferenceID);
                        }
                    }
                    //KMSManagerSocketHandler.broadcastMessage(originatingStationID, MMHEvent.ORDER_STATUS_UPDATE, status, MMHEvent.Status.OKAY, stationReferenceID);
                    KMSManagerSocketHandler.sendConfirmationMessage(originatingStationID,MMHEvent.ORDER_STATUS_UPDATE,status.PATransactionID+"","Status Updated", stationReferenceID);
                }else{
                    //not allowed should still be sent to the mirrors... JUST THE MIRRORS
                    KMSManagerSocketHandler.sendMessageDirectlyToMirrorsOfStation(originatingStationID, MMHEvent.ORDER_STATUS_UPDATE, status, stationReferenceID);
                }
            }else{
                Logger.logMessage("Station attempted to update an order that this host is unaware of", KMS_LOG, Logger.LEVEL.WARNING);
                KMSManagerSocketHandler.sendErrorMessage(
                        originatingStationID,
                        MMHEvent.ORDER_STATUS_UPDATE,
                        status.PATransactionID + "",
                        "The specified order is not known to the host.",
                        stationReferenceID);
            }
        }else{
            KMSManagerSocketHandler.sendErrorMessage(
                    originatingStationID,
                    MMHEvent.ORDER_STATUS_UPDATE,
                    status.PATransactionID + "",
                    "Missing Required fields.",
                    stationReferenceID);
        }
    }

    public void updateOrderStatus(String originatingStationID, KMSStatusModel status, String note, String stationReferenceID){
        Logger.logMessage("KMSManager.updateOrderStatus", "Order_Subset.log", Logger.LEVEL.TRACE);

        //update the in memory copy of the status
        if(status != null && status.PATransactionID != null && status.KMSOrderStatusID != null && status.PreviousKMSOrderStatusID != null){
            String event = getEvent(status.KMSOrderStatusID, status.PreviousKMSOrderStatusID);
            if(orderStatusMap.containsKey(status.PATransactionID)){
                //TODO: this isnt as cut and dry as originally planned... a status update coming from a station may or may not have the whole order, and may attempt to override the state of the whole order
                boolean updatedOrderStatus = false;//should the new status be applied to the whole order?
                //if(forewardStatusToListeners(status.KMSOrderStatusID, orderStatusMap.get(status.PATransactionID.toString()))){
                updatedOrderStatus = attemptUpdateOrderStatus(orderStatusMap.get(status.PATransactionID), status.KMSOrderStatusID, originatingStationID);
                //}
                if(updatedOrderStatus){
                    orderStatusMap.get(status.PATransactionID).setOrderStatusId(status.KMSOrderStatusID);
                    //send off the status to the server
                    KMSOrderStatusUpdateQueueHandler.getInstance().offerToStatusUpdateQueue(status);
                    //queue the event to be logged
                    KMSManager.logEvent(Integer.parseInt(originatingStationID), status.PATransactionID, status.PATransLineItemID, event, note, status.KMSOrderStatusID);

                    if (status.KMSOrderStatusID == KMSOrderStatus.SUSPENDED || status.PreviousKMSOrderStatusID == KMSOrderStatus.SUSPENDED) {
                        KMSManagerSocketHandler.broadcastMessage(originatingStationID, MMHEvent.ORDER_STATUS_UPDATE, status, MMHEvent.Status.OKAY, stationReferenceID);
                    }
                    else {
                        //forward the event to any listening stations/backups and their mirrors
                        KMSManagerSocketHandler.sendMessageToListeningOnlineStationsButNotOriginating(originatingStationID, MMHEvent.ORDER_STATUS_UPDATE, status, stationReferenceID);
                        //and send the message to anything that would normally be listening to the manual backup
                        for(String backedUp: stationIDToManualBackupStationID.keySet()){
                            if(stationIDToAutomaticBackupStationID.get(backedUp).equals(originatingStationID)){
                                //the backedUp staiton is being manually backed up by this station, so send the event too
                                KMSManagerSocketHandler.sendMessageToListeningOnlineStationsButNotOriginating(backedUp, MMHEvent.ORDER_STATUS_UPDATE, status, stationReferenceID);
                            }
                        }
                    }

                    //KMSManagerSocketHandler.broadcastMessage(originatingStationID, MMHEvent.ORDER_STATUS_UPDATE, status, MMHEvent.Status.OKAY, stationReferenceID);
                    KMSManagerSocketHandler.sendConfirmationMessage(originatingStationID,MMHEvent.ORDER_STATUS_UPDATE,status.PATransactionID+"","Status Updated", stationReferenceID);
                }else{
                    //not allowed should still be sent to the mirrors... JUST THE MIRRORS
                    KMSManagerSocketHandler.sendMessageDirectlyToMirrorsOfStation(originatingStationID, MMHEvent.ORDER_STATUS_UPDATE, status, stationReferenceID);
                    KMSManagerSocketHandler.sendErrorMessage(originatingStationID,MMHEvent.ORDER_STATUS_UPDATE,status.PATransactionID+"","Status Update Not allowed", stationReferenceID);
                }
            }else{
                Logger.logMessage("Station attempted to update an order that this host is unaware of", KMS_LOG, Logger.LEVEL.WARNING);
                Logger.logMessage("Station " +originatingStationID+ " attempted to update order "+ status.KMSOrderStatusID +" that this host is unaware of", "Order_Subset.log", Logger.LEVEL.WARNING);
                KMSManagerSocketHandler.sendErrorMessage(
                        originatingStationID,
                        MMHEvent.ORDER_STATUS_UPDATE,
                        status.PATransactionID + "",
                        "The specified order is not known to the host.",
                        stationReferenceID);
            }
        }else{
            Logger.logMessage("Station attempted to update an order without all required fields", KMS_LOG, Logger.LEVEL.WARNING);
            Logger.logMessage("Station " +originatingStationID+ " attempted to update order "+ status.KMSOrderStatusID +" without specifying the required fields", "Order_Subset.log", Logger.LEVEL.WARNING);

            KMSManagerSocketHandler.sendErrorMessage(
                    originatingStationID,
                    MMHEvent.ORDER_STATUS_UPDATE,
                    status.PATransactionID + "",
                    "Missing Required fields.",
                    stationReferenceID);
        }
    }

    /**
     * This function is called when a station updates it's order status.
     * This needs to read the state of the entire order's products
     * then figure out the current status of the whole order
     * then send that whole status to Exp and Disp stations, and then parse out the current status of each sub station order
     * then send each individual status off (including another order status back to the station that prompted this)
     * then log the event and send off the order status update to the server
     * @param originatingStationID
     * @param status
     * @param note
     * @param stationReferenceID
     */
    public void updateOrderStatusPerProduct(String originatingStationID, KMSStatusModel status, String note, String stationReferenceID){
        Logger.logMessage("KMSManager.updateOrderStatusBySubOrder", KMS_LOG, Logger.LEVEL.TRACE);
        Integer productsStatus;
        Integer currentOrderStatus;
        Integer incomingStatus;
        boolean restrictFinalizedToExp = (connectedExpCount() > 0 || connectedROCount() > 0);
        boolean originatingFromExpOrRO = (expeditorIDs.contains(originatingStationID) || remoteOrderIDs.contains(originatingStationID));
        boolean entireOrderStatus = false; //if the incoming status should be used for all other sub orders, or if the status should be recalculated per subset //certain statuses should be propagated to everything working on an order, regardless of where they came from
        boolean updatePrevented = false; //if the update is prevented, it will not be sent out to any other stations


        //update the in memory copy of the status
        if(status != null && status.PATransactionID != null && status.KMSOrderStatusID != null && status.PreviousKMSOrderStatusID != null){
            String event = getEvent(status.KMSOrderStatusID, status.PreviousKMSOrderStatusID);
            if(orderStatusMap.containsKey(status.PATransactionID.toString())){
                //Assume: that item status updates have come in for all products within the station that has sent this message?
                //        This needs to read the state of the entire order's products
                productsStatus = orderStatusMap.get(status.PATransactionID.toString()).getOrderStatusBasedOnProductStatuses();
//        * then figure out the current status of the whole order
                currentOrderStatus = orderStatusMap.get(status.PATransactionID.toString()).getOrderStatusId();
                incomingStatus =  status.KMSOrderStatusID;
//        * then send that whole status to Exp and Disp stations, and then parse out the current status of each sub station order
                //we should do different things based on what status is getting set?
                switch(incomingStatus){
                    case KMSOrderStatus.RECEIVED :
                        //either a station just got the order, or has unbumped it from inprogress
                        if(productsStatus == KMSOrderStatus.IN_PROGRESS || productsStatus == KMSOrderStatus.COMPLETED){
                            //its an unbump, but other stations are working on it
                            updatePrevented = true;
                        }else{
                            //other wise its a station is probably receiving it.
                            entireOrderStatus = true;
                        }
                    case KMSOrderStatus.IN_PROGRESS :
                        //if the order is currently suspended, the incoming status should never be propagated
                        //you can always set an order back to in progress(unless its finalized) and it should get sent to the expeditor/disp
                        if(currentOrderStatus == KMSOrderStatus.SUSPENDED)
                            entireOrderStatus = false;
                        break;
                    case KMSOrderStatus.COMPLETED :
                        //just because one station completed an order, it doesnt mean the whole order is completed
                        entireOrderStatus = false;
                        if(productsStatus == KMSOrderStatus.COMPLETED){
                            //complete the whole order
                            entireOrderStatus = true;
                        }else{
                            //just complete this part of the order
                            updatePrevented = true;
                        }
                        break;
                    case KMSOrderStatus.FINALIZED :
                        //either an expeditor is finalizing it or a prep station is

                        if(restrictFinalizedToExp){//an open connection to an exp/ro exists
                            if(originatingFromExpOrRO){
                                //finalize the order
                                //this is not  really true
                                entireOrderStatus = true;
                            }else{
                                //block the order from being finalized on other stations
                                updatePrevented = true;
                            }
                        }
                        break;

                    case KMSOrderStatus.CANCELED :
                        //if one station cancels an order it should be canceled on all other stations
                    case KMSOrderStatus.SUSPENDED :
                        //if one station suspends the order, it should be suspended on all others.
                    case KMSOrderStatus.ERROR :
                        //if one part of the order errors, the whole thing is errored
                        entireOrderStatus = true;
                        break;

                    case KMSOrderStatus.WAITING :
                        //nothing really puts the order back into a waiting state
                    case KMSOrderStatus.SENT :
                        //no updates should come in that put an order into the send state
                    case KMSOrderStatus.REPLACED :
                        entireOrderStatus = false;
                        break;
                }

                if(entireOrderStatus && updatePrevented){
                    Logger.logMessage("Status update: entireOrderStatus && updatePrevented", "KMS.log", Logger.LEVEL.TRACE);
                    Logger.logMessage("This order update should be impossible... you cant have an update that should apply to the entire order and be prevented", "KMS.log", Logger.LEVEL.ERROR);
                }else if(entireOrderStatus && !updatePrevented){
                    Logger.logMessage("Status update: entireOrderStatus && !updatePrevented", "KMS.log", Logger.LEVEL.TRACE);
                    //update the whole order with this status and send it off
                    orderStatusMap.get(status.PATransactionID.toString()).setOrderStatusId(incomingStatus);
                    //send off the status to the server
                    KMSOrderStatusUpdateQueueHandler.getInstance().offerToStatusUpdateQueue(status);
                    //queue the event to be logged
                    KMSManager.logEvent(Integer.parseInt(originatingStationID), status.PATransactionID, status.PATransLineItemID, event, note, status.KMSOrderStatusID);
                    KMSManagerSocketHandler.broadcastMessage(originatingStationID, MMHEvent.ORDER_STATUS_UPDATE, status, MMHEvent.Status.OKAY, stationReferenceID);
//                    KMSManagerSocketHandler.sendConfirmationMessage(originatingStationID,MMHEvent.ORDER_STATUS_UPDATE,status.PATransactionID+"","Status Updated", stationReferenceID);
                }else if(!entireOrderStatus && updatePrevented){
                    Logger.logMessage("Status update: !entireOrderStatus && updatePrevented", "KMS.log", Logger.LEVEL.TRACE);
                    //dont send it to any other station and dont update the whole order
                    //do nothing
                }else if(!entireOrderStatus && !updatePrevented){
                    Logger.logMessage("Status update: !entireOrderStatus && !updatePrevented", "KMS.log", Logger.LEVEL.TRACE);
                    //TOOD: recalculate each stations' status by the it's products' status and send that off individually?
                    //forward the event to any listening stations/backups and their mirrors
                    KMSManagerSocketHandler.sendMessageToListeningOnlineStations(originatingStationID, MMHEvent.ORDER_STATUS_UPDATE, status, stationReferenceID);
                    //and send the message to anything that would normally be listening to the manual backup
                    for(String backedUp: stationIDToManualBackupStationID.keySet()){
                        if(stationIDToAutomaticBackupStationID.get(backedUp).equals(originatingStationID)){
                            //the backedUp staiton is being manually backed up by this station, so send the event too
                            KMSManagerSocketHandler.sendMessageToListeningOnlineStations(backedUp, MMHEvent.ORDER_STATUS_UPDATE, status, stationReferenceID);
                        }
                    }
                }
            }else{
                Logger.logMessage("Station attempted to update an order that this host is unaware of", KMS_LOG, Logger.LEVEL.WARNING);
                KMSManagerSocketHandler.sendErrorMessage(
                        originatingStationID,
                        MMHEvent.ORDER_STATUS_UPDATE,
                        status.PATransactionID + "",
                        "The specified order is not known to the host.",
                        stationReferenceID);
            }
        }else{
            KMSManagerSocketHandler.sendErrorMessage(
                    originatingStationID,
                    MMHEvent.ORDER_STATUS_UPDATE,
                    status.PATransactionID + "",
                    "Missing Required fields.",
                    stationReferenceID);
        }
    }

    private int connectedExpCount(){
        int c = 0;
        for(String exp: expeditorIDs){
            if(KMSManagerSocketHandler.isOnline(exp))
                c++;
        }
        return c;
    }

    private int connectedROCount(){
        int c = 0;
        for(String ro: remoteOrderIDs){
            if(KMSManagerSocketHandler.isOnline(ro))
                c++;
        }
        return c;
    }

    /**
     * Checks the status of every individual product instead of the order's state to determine what the true order status is
     * @return
     */
    private boolean forewardStatusToListeners(int newIncomingOrderState, KMSOrderModel order){
        Logger.logMessage("Attempting to update order " + order.getId() + " to status " + newIncomingOrderState, KMS_LOG, Logger.LEVEL.TRACE);
        boolean hasRECEIVED = false;
        boolean hasIN_PROGRESS = false;
        boolean hasCOMPLETED = false;
        boolean hasCANCELED = false;
        boolean hasSUSPENDED = false;
        boolean hasFINALIZED = false;
        boolean hasWAITING = false;
        boolean hasSENT = false;
        boolean hasERROR = false;
        boolean hasREPLACED = false;

        for(KMSProductModel product: order.getProducts()){
            switch(product.getOrderStatusId()){
                case KMSOrderStatus.RECEIVED :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is RECEIVED", KMS_LOG, Logger.LEVEL.TRACE);
                    hasRECEIVED = true;
                    break;
                case KMSOrderStatus.IN_PROGRESS :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is IN_PROGRESS", KMS_LOG, Logger.LEVEL.TRACE);
                    hasIN_PROGRESS = true;
                    break;
                case KMSOrderStatus.COMPLETED :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is COMPLETED", KMS_LOG, Logger.LEVEL.TRACE);
                    hasCOMPLETED = true;
                    break;
                case KMSOrderStatus.CANCELED :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is CANCELED", KMS_LOG, Logger.LEVEL.TRACE);
                    hasCANCELED = true;
                    break;
                case KMSOrderStatus.SUSPENDED :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is SUSPENDED", KMS_LOG, Logger.LEVEL.TRACE);
                    hasSUSPENDED = true;
                    break;
                case KMSOrderStatus.FINALIZED :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is FINALIZED", KMS_LOG, Logger.LEVEL.TRACE);
                    hasFINALIZED = true;
                    break;
                case KMSOrderStatus.WAITING :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is WAITING", KMS_LOG, Logger.LEVEL.TRACE);
                    hasWAITING = true;
                    break;
                case KMSOrderStatus.SENT :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is SENT", KMS_LOG, Logger.LEVEL.TRACE);
                    hasSENT = true;
                    break;
                case KMSOrderStatus.ERROR :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is ERROR", KMS_LOG, Logger.LEVEL.TRACE);
                    hasERROR = true;
                    break;
                case KMSOrderStatus.REPLACED :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is REPLACED", KMS_LOG, Logger.LEVEL.TRACE);
                    hasREPLACED = true;
                    break;
            }
        }

        //log what the order has
        String msg = "";
        if(hasRECEIVED)    msg += " RECEIVED";
        if(hasIN_PROGRESS) msg += " IN_PROGRESS";
        if(hasCOMPLETED)   msg += " COMPLETED";
        if(hasCANCELED)    msg += " CANCELED";
        if(hasSUSPENDED)   msg += " SUSPENDED";
        if(hasFINALIZED)   msg += " FINALIZED";
        if(hasWAITING)     msg += " WAITING";
        if(hasSENT)        msg += " SENT";
        if(hasERROR)       msg += " ERROR";
        if(hasREPLACED)    msg += " REPLACED";
        Logger.logMessage("Determined order has products of status: " + msg, KMS_LOG, Logger.LEVEL.TRACE);

        //find all the cases where we would not want to allow that state to be sent to the Exp/Display stations
        switch(newIncomingOrderState){
            case KMSOrderStatus.WAITING :
            case KMSOrderStatus.SENT :
                if(hasIN_PROGRESS){
                    Logger.logMessage("Order Status Update limited. Can not set to SENT while hasIN_PROGRESS", KMS_LOG, Logger.LEVEL.TRACE);
                    return false;
                }
                //we wouldnt want these to be sent if we have oreders of any other state, but this should never even happen
                break;
            case KMSOrderStatus.RECEIVED :
                //for an order to be received, it must be at least received by every station so nothing can be waiting/sent
                if(hasWAITING || hasSENT){
                    Logger.logMessage("Order Status Update limited. Can not set to RECEIVED while hasWAITING or hasSENT", KMS_LOG, Logger.LEVEL.TRACE);
                    return false;
                }
                break;
            case KMSOrderStatus.IN_PROGRESS :
                //we should always allow an order to go back into in progress
                break;
            case KMSOrderStatus.COMPLETED :
                //
                if(hasWAITING || hasSENT || hasRECEIVED || hasIN_PROGRESS){
                    Logger.logMessage("Order Status Update limited. Can not set to COMPLETED while hasWAITING or hasSENT or hasRECEIVED or hasIN_PROGRESS", KMS_LOG, Logger.LEVEL.TRACE);
                    return false;
                }
                break;
            case KMSOrderStatus.FINALIZED :
                if(hasWAITING || hasSENT || hasRECEIVED || hasIN_PROGRESS){
                    Logger.logMessage("Order Status Update limited. Can not set to FINALIZED while hasWAITING or hasSENT or hasRECEIVED or hasIN_PROGRESS", KMS_LOG, Logger.LEVEL.TRACE);
                    return false;
                }
                break;
            //you cant cancel or suspend individual products
//            case KMSOrderStatus.CANCELED :
//                break;
//            case KMSOrderStatus.SUSPENDED :
//                break;
            case KMSOrderStatus.ERROR :
                break;
            case KMSOrderStatus.REPLACED :
                break;
        }
        return true;//allow the order to go into the new status
    }

    private boolean hasExpeditorProducts(KMSOrderModel order){
        ArrayList<KMSProductModel> products = order.getProducts();
        for(KMSProductModel product : products){
            if(expeditorIDs.contains(product.handlingKMSStationId))
                return true;
        }
        return false;
    }
    private boolean hasRemoteOrderProducts(KMSOrderModel order){
        ArrayList<KMSProductModel> products = order.getProducts();
        for(KMSProductModel product : products){
            if(remoteOrderIDs.contains(product.handlingKMSStationId))
                return true;
        }
        return false;
    }

    private boolean attemptUpdateOrderStatus(KMSOrderModel order, int newStatus, String fromStation){
        /*rules:    S_C S_S S_E always update the state
        *           if E/R exists S_F -/-> ALL
        *           if all P have S_C, S_C -/-> E/D/R
        * */
        Logger.logMessage("attempting to update the order " + order.getOrderNumber() + " to status " + newStatus + " from station " + fromStation, "Order_Subset.log", Logger.LEVEL.DEBUG);
        Logger.logMessage("" +
                "   connectedExpCount() > 0 = " + (connectedExpCount() > 0) +
                "   connectedROCount() > 0 = " + (connectedROCount() > 0) +
                "   hasExpeditorProducts(order) = " + hasExpeditorProducts(order) +
                "   hasRemoteOrderProducts(order) = " + hasRemoteOrderProducts(order) +
                "   expeditorIDs.contains(fromStation) =" + expeditorIDs.contains(fromStation) +
                "   remoteOrderIDs.contains(fromStation) =" + remoteOrderIDs.contains(fromStation), "Order_Subset.log", Logger.LEVEL.DEBUG);
        int initialStatus = order.getOrderStatusId();
        if (newStatus == KMSOrderStatus.FINALIZED && ((
                (connectedExpCount() > 0 && hasExpeditorProducts(order) && expeditorIDs.contains(fromStation)) ||
                (connectedROCount() > 0 && hasRemoteOrderProducts(order) && remoteOrderIDs.contains(fromStation)) ||
                ((connectedExpCount() == 0 && connectedROCount() == 0) || !hasExpeditorProducts(order)) && order.getOrderStatusBasedOnProductStatuses() == KMSOrderStatus.COMPLETED))){
                Logger.logMessage("Setting entire order status to FINALIZED", KMS_LOG, Logger.LEVEL.TRACE);
                order.setOrderStatusId(newStatus);
        }
        if(newStatus == KMSOrderStatus.COMPLETED){
            //only complete the order if it its the last prep station to complete it, or if its coming from the expeditor
            if(expeditorIDs.contains(fromStation) || remoteOrderIDs.contains(fromStation)){
                Logger.logMessage("Setting entire order status to COMPLETED", KMS_LOG, Logger.LEVEL.TRACE);
                order.setOrderStatusId(newStatus);
            }else if(order.getOrderStatusBasedOnProductStatuses() == KMSOrderStatus.COMPLETED){//coming from a prep, but the whole order's products are completed
                Logger.logMessage("Setting entire order status to COMPLETED", KMS_LOG, Logger.LEVEL.TRACE);
                order.setOrderStatusId(newStatus);
            }
        }
        if(newStatus == KMSOrderStatus.IN_PROGRESS){
            Logger.logMessage("Setting entire order status to IN_PROGRESS", KMS_LOG, Logger.LEVEL.TRACE);
            order.setOrderStatusId(newStatus);
        }
        if(newStatus == KMSOrderStatus.RECEIVED) { // && order.getOrderStatusBasedOnProductStatuses() == KMSOrderStatus.RECEIVED){ // TODO
            Logger.logMessage("Setting entire order status to RECEIVED", KMS_LOG, Logger.LEVEL.TRACE);
            order.setOrderStatusId(newStatus);
        }
        if(newStatus == KMSOrderStatus.CANCELED || newStatus == KMSOrderStatus.SUSPENDED || newStatus == KMSOrderStatus.ERROR){
            Logger.logMessage("Setting entire order status to " + newStatus);
            order.setOrderStatusId(newStatus);
        }
        //was the order status actually updated?
        Logger.logMessage("Result of attempt: " + (initialStatus != order.getOrderStatusId()), "Order_Subset.log", Logger.LEVEL.DEBUG);

        return initialStatus != order.getOrderStatusId();
    }

    /**
     * Get the event type to log based on the previous state and the next state of an order.
     * @param KMSOrderStatusID
     * @param PreviousKMSOrderStatusID
     * @return
     */
    private String getEvent(int KMSOrderStatusID, int PreviousKMSOrderStatusID){
        //Legal Order State transition matrix
        switch (PreviousKMSOrderStatusID){
            case 7://From Waiting
                switch (KMSOrderStatusID){
//                    case 7://To Waiting
                    case 8://To Sent
                        return "";//Valid transition, but no event logged, this code is not executed
//                    case 1://To Order Received
//                    case 2://To In Progress
//                    case 3://To Completed
//                    case 6://To Finalized
//                    case 4://To Canceled
//                    case 5://To Suspended
                    case 9://To Error
                        return MMHEvent.KMSEventType.ERROR;
                    case 10://To Replaced
                        return MMHEvent.KMSEventType.CANCELED;
                }
                break;
            case 8://From Sent
                switch (KMSOrderStatusID){
//                    case 7://To Waiting
//                    case 8://To Sent
                    case 1://To Order Received
                        return MMHEvent.KMSEventType.ORDER_RECEIVED;
//                    case 2://To In Progress
//                    case 3://To Completed
//                    case 6://To Finalized
                    case 10://To Replaced
                    case 4://To Canceled
                        return MMHEvent.KMSEventType.CANCELED;
                    case 5://To Suspended
                        return MMHEvent.KMSEventType.SUSPENDED;
                    case 9://To Error
                        return MMHEvent.KMSEventType.ERROR;
                }
                break;
            case 1://From Order Received
                switch (KMSOrderStatusID){
//                    case 7://To Waiting
//                    case 8://To Sent
//                    case 1://To Order Received
                    case 2://To In Progress
                        return MMHEvent.KMSEventType.BUMPED;
//                    case 3://To Completed
//                    case 6://To Finalized
                    case 10://To Replaced
                    case 4://To Canceled
                        return MMHEvent.KMSEventType.CANCELED;
                    case 5://To Suspended
                        return MMHEvent.KMSEventType.SUSPENDED;
                    case 9://To Error
                        return MMHEvent.KMSEventType.ERROR;
                }
            case 2://From In Progress
                switch (KMSOrderStatusID){
//                    case 7://To Waiting
//                    case 8://To Sent
                    case 1://To Order Received
                        return MMHEvent.KMSEventType.UNBUMPED;
//                    case 2://To In Progress
                    case 3://To Completed
                    case 6://To Finalized
                        return MMHEvent.KMSEventType.BUMPED;
                    case 10://To Replaced
                    case 4://To Canceled
                        return MMHEvent.KMSEventType.CANCELED;
                    case 5://To Suspended
                        return MMHEvent.KMSEventType.SUSPENDED;
                    case 9://To Error
                        return MMHEvent.KMSEventType.ERROR;
                }
            case 3://From Completed
                switch (KMSOrderStatusID){
//                    case 7://To Waiting
//                    case 8://To Sent
//                    case 1://To Order Received
                    case 2://To In Progress
                        return MMHEvent.KMSEventType.UNBUMPED;
//                    case 3://To Completed
                    case 6://To Finalized
                        return MMHEvent.KMSEventType.BUMPED;
                    case 10://To Replaced
                    case 4://To Canceled
                        return MMHEvent.KMSEventType.CANCELED;
                    case 5://To Suspended
                        return MMHEvent.KMSEventType.SUSPENDED;
                    case 9://To Error
                        return MMHEvent.KMSEventType.ERROR;
                }
            case 6://From Finalized
                switch (KMSOrderStatusID){
//                    case 7://To Waiting
//                    case 8://To Sent
//                    case 1://To Order Received
//                    case 2://To In Progress
//                    case 3://To Completed
//                    case 6://To Finalized
//                    case 4://To Canceled
//                    case 5://To Suspended
                    case 9://To Error
                        return MMHEvent.KMSEventType.ERROR;
                }
            case 4://From Canceled
                switch (KMSOrderStatusID){
                    case 7://To Waiting
                    case 8://To Sent
                    case 1://To Order Received
                    case 2://To In Progress
                    case 3://To Completed
                        return MMHEvent.KMSEventType.UNBUMPED;
//                    case 6://To Finalized
//                    case 4://To Canceled
                    case 10://To Replaced
                        return MMHEvent.KMSEventType.CANCELED;
//                    case 5://To Suspended
                    case 9://To Error
                        return MMHEvent.KMSEventType.ERROR;
                }
            case 5://From Suspended
                switch (KMSOrderStatusID){
                    case 7://To Waiting
                    case 8://To Sent
                    case 1://To Order Received
                    case 2://To In Progress
                    case 3://To Completed
                        return MMHEvent.KMSEventType.UNBUMPED;
//                    case 6://To Finalized
//                    case 4://To Canceled
                    case 10://To Replaced
                        return MMHEvent.KMSEventType.CANCELED;
//                    case 5://To Suspended
                    case 9://To Error
                        return MMHEvent.KMSEventType.ERROR;
                }

            case 9://From Error
                //TODO: is it possible to recover from an error state? what event would that be logged as?
                switch (KMSOrderStatusID){
//                    case 7://To Waiting
//                    case 8://To Sent
//                    case 1://To Order Received
//                    case 2://To In Progress
//                    case 3://To Completed
//                    case 6://To Finalized
//                    case 4://To Canceled
//                    case 5://To Suspended
//                    case 9://To Error
                }
            case 10://From Replaced
                //it should not be possible to transfer from the replaced state to any other state.
                switch (KMSOrderStatusID){
//                    case 7://To Waiting
//                    case 8://To Sent
//                    case 1://To Order Received
//                    case 2://To In Progress
//                    case 3://To Completed
//                    case 6://To Finalized
//                    case 4://To Canceled
//                    case 5://To Suspended
//                    case 9://To Error
                }

        }
        Logger.logMessage("Unhandled Order State Transition found. From " + PreviousKMSOrderStatusID + " to " +KMSOrderStatusID, KMS_LOG, Logger.LEVEL.ERROR);
        return MMHEvent.KMSEventType.ERROR;
    }

    public void transferALL(String originatingStationID, KMSTransferModel transfer, String stationReferenceID){
        Logger.logMessage("KMSManager.transferALL", KMS_LOG, Logger.LEVEL.TRACE);
        ArrayList<String> ordersWithDetails = new ArrayList<>();
        for(String orderID: orderStatusMap.keySet()){

            KMSOrderModel order = orderStatusMap.get(orderID);
            if(order.getProducts() != null && order.getProducts().size() > 0){
                for(KMSProductModel prod : order.getProducts()){
                    //this needs to only send the products that were meant for the original station
                    if((prod.handlingKMSStationId +"").compareTo(transfer.fromKMSStationID+"")==0){
                        ordersWithDetails.add(orderID);
                        break;
                    }
                }
            }
        }
        for(String orderID: ordersWithDetails){
            transfer.PATransactionID = orderID;
            transfer(originatingStationID, transfer, stationReferenceID);
        }
    }

    /**
     * This ocures when a station is transferring an order to another station.
     * @param originatingStationID
     * @param transfer
     */
    public void transfer(String originatingStationID, KMSTransferModel transfer, String stationReferenceID){
        Logger.logMessage("KMSManager.transfer ref: " + stationReferenceID, KMS_LOG, Logger.LEVEL.TRACE);
        //TODO:send event to appropriate stationIDs... the station being transfered to and mirrors would need the event, but are there others?
        if(transfer.fromKMSStationID != null && transfer.toKMSStationID != null && transfer.PATransactionID != null){
            if(orderStatusMap.containsKey(transfer.PATransactionID+"")){
                KMSOrderModel order = orderStatusMap.get(transfer.PATransactionID+"");
                String onlineStationToSendTo = KMSManagerSocketHandler.getOnlineStationID(transfer.toKMSStationID+"");

                ArrayList<String> stationIDsToTransfer = new ArrayList<>();
                stationIDsToTransfer.add(transfer.fromKMSStationID + "");//ONLY itself should be added to this list, see below:
//======================================================================================================================
// I dont think we can do any of this due to stations uploading their status when they come back online,
//      they could attempt to upload an order that was already finished, or get a conflicting state. I walked through the logic of doing it
//      either way mentioned below (transfer at backup time, or allow transfer after already backing up)
//      and there is a logical paradox if we allow orders to be transferred by other stations (ie the backups)

//--------------dont need to do this block because when we manually back up a station, all of the other stations orders
//                      will get transferred to the backup. If that were not the case, we would need to do this.
                //get the list of stationIDs this station is manually backing up
//                for(String key: stationIDToManualBackupStationID.keySet()){
//                    if(stationIDToManualBackupStationID.get(key).compareTo(originatingStationID) == 0)
//                        stationIDsToTransfer.add(key);
//                }
//--------------
//                //wont need to do this if when a station goes offline we transfer all of the orders to the backup... WE CAN NOT DO THIS
//                //get all of the offline stations that this station is backing up
//                for(String key: stationIDToAutomaticBackupStationID.keySet()){
//                    if(stationIDToAutomaticBackupStationID.get(key).compareTo(originatingStationID) == 0 && !KMSManagerSocketHandler.isOnline(stationIDToAutomaticBackupStationID.get(key)))
//                        stationIDsToTransfer.add(key);
//                }
//======================================================================================================================

                if(onlineStationToSendTo != null){//we found the online station to send to
                    KMSOrderModel orderSubset = new KMSOrderModel();
                    ArrayList<KMSProductModel> stationSpecificProducts = orderSubset.getProducts();
                    if(order.getProducts() != null && order.getProducts().size() > 0){
                        for(KMSProductModel prod : order.getProducts()){
                            Logger.logMessage("Transfer product check. " + prod.getName() + " is handled by " + prod.handlingKMSStationId, KMS_LOG, Logger.LEVEL.TRACE);
                            //this needs to only send the products that were meant for the original station
                            boolean isBackingUp = false;
                            for(String id: stationIDsToTransfer){
                                if((prod.handlingKMSStationId +"").compareTo(id+"")==0){
                                    Logger.logMessage("Transfer product check. " + prod.getName() + " found to be handled by the station we are transferring from.", KMS_LOG, Logger.LEVEL.TRACE);
                                    isBackingUp = true;
                                    break;
                                }
                            }
                            if(isBackingUp){
                                prod.handlingKMSStationId = transfer.toKMSStationID+"";
                                Logger.logMessage("Transfer product check. " + prod.getName() + " transfered to being handled by " + prod.handlingKMSStationId, KMS_LOG, Logger.LEVEL.TRACE);
                                stationSpecificProducts.add(prod);
                            }
                        }
                    }

                    //copy order fields to the the order subset
                    orderSubset.setOrderStatusId(order.getOrderStatusId());
                    orderSubset.setOrderPriorityId(order.getOrderPriorityId());
                    orderSubset.setId(order.getId());
                    orderSubset.setComment(order.getComment());
                    orderSubset.setOrderNumber(order.getOrderNumber());
                    orderSubset.setPersonName(order.getPersonName());
                    orderSubset.setReceivedTime(order.getReceivedTime());
                    orderSubset.setPreviousOrderNumbers(transfer.previousOrderNumbers);

                    String sentTo = KMSManagerSocketHandler.sendMessageToOnlineStationAndMirror(transfer.toKMSStationID.toString(), MMHEvent.KMSEventType.TRANSFER, orderSubset, stationReferenceID);
                    KMSManager.logEvent(Integer.parseInt(originatingStationID), transfer.PATransactionID, null, MMHEvent.KMSEventType.TRANSFER,
                            "Transfer order from "+ transfer.fromKMSStationID +" to station " + sentTo, orderSubset.getOrderStatusId());
                    //the from station should receive a confirmation that it's order has been transferred
                    Logger.logMessage("DEBUG LINE: Transfer Reference in function handler: " + stationReferenceID, KMS_LOG, Logger.LEVEL.DEBUG);
                    KMSManagerSocketHandler.sendConfirmationMessage(transfer.fromKMSStationID+"",MMHEvent.KMSEventType.TRANSFER,transfer.PATransactionID+"","Transferred by "+ originatingStationID, stationReferenceID);
                }else{
                    KMSManagerSocketHandler.sendErrorMessage(
                            originatingStationID,
                            MMHEvent.ORDER_STATUS_UPDATE,
                            transfer.fromKMSStationID +" "+ transfer.toKMSStationID +" "+ transfer.PATransactionID,
                            "Could not find an online connection nor backup for specified station.", stationReferenceID);
                }
            }else{
                KMSManagerSocketHandler.sendErrorMessage(
                        originatingStationID,
                        MMHEvent.ORDER_STATUS_UPDATE,
                        transfer.fromKMSStationID +" "+ transfer.toKMSStationID +" "+ transfer.PATransactionID,
                        "The specified order is not known to the host.", stationReferenceID);
            }
        }else{
            KMSManagerSocketHandler.sendErrorMessage(
                    originatingStationID,
                    MMHEvent.KMSEventType.TRANSFER,
                    transfer.fromKMSStationID +" "+ transfer.toKMSStationID +" "+ transfer.PATransactionID,
                    "Missing Required fields.", stationReferenceID);
        }
    }

//    /**
//     * This occures when the order is changed (not by a station, but by an external update)
//     * @param originatingStationID
//     * @param order
//     */
//    public void itemUpdate(String originatingStationID, KMSOrderModel order){
//        //TODO: this will change the order as we and the station know it
//        //TODO: grab these values from the model
//        Long transactionID = 0L;
//        Long lineItemID = 0L;
//        Integer orderStatus = 0;
//        //TODO:send event to appropriate stationIDs
//        //TODO: update data in the DB?
//        //TODO: logging a ITEM_UPDATE wont work, its not a real event type yet A.O.5/6/2020
//        KMSManager.logEvent(Integer.parseInt(originatingStationID), transactionID, lineItemID, MMHEvent.KMSEventType.ITEM_UPDATE,null, orderStatus);
//        //forward the event to any listening stations/backups and their mirrors
//        KMSManagerSocketHandler.sendMessageToListeningOnlineStations(originatingStationID, MMHEvent.KMSEventType.ITEM_UPDATE, order);
//    }
    public void printOrder(String originatingStationID, KMSPrintModel printModel, String stationReferenceID){
        Logger.logMessage("KMSManager.printOrder", KMS_LOG, Logger.LEVEL.TRACE);
        if(printModel == null || printModel.PATransactionID == null || printModel.PrinterID <= 0){
            KMSManagerSocketHandler.sendErrorMessage(
                    originatingStationID,
                    MMHEvent.PRINT_ORDER,
                    "PrinterID: "+ printModel.PrinterID +", PATransactionID: "+ printModel.PATransactionID,
                    "Missing Required fields.", stationReferenceID);
        } else if(!orderStatusMap.containsKey(printModel.PATransactionID) && finishedOrders.contains(printModel.PATransactionID)){
            KMSManagerSocketHandler.sendErrorMessage(
                    originatingStationID,
                    MMHEvent.PRINT_ORDER,
                    "PrinterID: "+ printModel.PrinterID +", PATransactionID: "+ printModel.PATransactionID,
                    "Order has been previously finished and data removed.", stationReferenceID);
        } else if(!orderStatusMap.containsKey(printModel.PATransactionID)){
            KMSManagerSocketHandler.sendErrorMessage(
                    originatingStationID,
                    MMHEvent.PRINT_ORDER,
                    "PrinterID: "+ printModel.PrinterID +", PATransactionID: "+ printModel.PATransactionID,
                    "Unknown Order", stationReferenceID);
        } else if(orderStatusMap.get(printModel.PATransactionID) == null || orderStatusMap.get(printModel.PATransactionID).originalPrintJob == null){
            KMSManagerSocketHandler.sendErrorMessage(
                    originatingStationID,
                    MMHEvent.PRINT_ORDER,
                    "PrinterID: "+ printModel.PrinterID +", PATransactionID: "+ printModel.PATransactionID,
                    "There is no original print job for this order", stationReferenceID);
            //TODO: this is a very real case that should be handled...
        }else {
            KMSOrderModel order = orderStatusMap.get(printModel.PATransactionID);
            QCPrintJob original = order.originalPrintJob;
            QCPrintJob newJob = new QCPrintJob();
            //copy the print job's info
            newJob.setKdsDirectory(original.getKdsDirectory());//TODO: should this be modified?
            newJob.setPersonName(original.getPersonName());
            newJob.setOnlineTrans(original.isOnlineTrans());
            newJob.setOrderNumber(original.getOrderNumber());
            newJob.setOrderTypeID(original.getOrderTypeID());
            newJob.setPaPrinterQueueID(original.getPaPrinterQueueID());
            newJob.setPaTransactionID(original.getPaTransactionID());
            newJob.setPhone(original.getPhone());
            newJob.setPrevOrderNumsForKDS(original.getPrevOrderNumsForKDS());
            newJob.setTerminalID(original.getTerminalID());
            newJob.setTransComment(original.getTransComment());
            newJob.setTransName(original.getTransName());
            newJob.setTransNameLabel(original.getTransNameLabel());
            newJob.setTransTypeID(original.getTransTypeID());
            newJob.setUseTextMessages(original.getUseTextMessages());

            //slide through the QCPRintJob details to find the subset of the lines that belong to a single station
            int stationToMatch = Integer.parseInt(originatingStationID);
            int stationStart = 0;//always assume the start is 0
            int stationEnd = -1;//and that the receipt is not in there
            int currentStart = 0;
            int currendEnd = 0;
            boolean foundStation = false;
            for(int i = 0; i < original.getPrintJobDetails().size(); i++){
                //if we hit a --CUT--, the receipt is over,
                if(original.getPrintJobDetails().get(i).getDecodedLineDetail().equals(KitchenPrinterReceiptFormatter.JPOS_CUT)){
                    //set the current end flag
                    currendEnd = i;
                    //check if we matched
                    //if so, set the indexes, we're done
                    if(foundStation != false){
                        stationEnd = currendEnd;
                        stationStart = currentStart;
                        break;
                    }
                    //otherwise set the current start to the next line
                    if(i < original.getPrintJobDetails().size() -1){
                        currentStart = i+1;
                    }
                }
                //if the detail's station == origin, set the match flag
                if(original.getPrintJobDetails().get(i).getKMSStationID() == stationToMatch)
                    foundStation = true;
            }
            //grab the subset of details
            for( int i = stationStart; i <= stationEnd; i++ ){
                QCPrintJobDetail d = original.getPrintJobDetails().get(i);
                QCPrintJobDetail newDetail = new QCPrintJobDetail();
                //alterations to the original data
                newDetail.setPrinterID(printModel.PrinterID);
                newDetail.setStationID(0);
                newDetail.setKMSStationID(0);

                //copy the original data
//                //Replace KMS prep option delimiter with KP delim
//                String lineDetail = d.getDecodedLineDetail().replace("__PREP__", " - ");
//                byte[] decodedBytes = Base64.getEncoder().encode(lineDetail.getBytes());
//                String encodedLine = new String(decodedBytes);
//
//                newDetail.setLineDetail(encodedLine);
                newDetail.setLineDetail(d.getLineDetail());//TODO: this might need to be modified from just the value KMS value?
                newDetail.setPaPrinterQueueDetailID(d.getPaPrinterQueueDetailID());
                newDetail.setPAPluID(d.getPAPluID());
                newDetail.setQuantity(d.getQuantity());
                newDetail.setPrintsOnExpeditor(d.getPrintsOnExpeditor());
                newDetail.setHideStation(d.getHideStation());
                newDetail.setModifier(d.isModifier());
                newDetail.setKMSOrderStatusID(d.getKMSOrderStatusID());//TODO: this is probably an outdated value
                newDetail.setPaTransLineItemID(d.getPaTransLineItemID());
                newDetail.setParentPrintJobDetail(d.getParentPrintJobDetail());

                newJob.getPrintJobDetails().add(newDetail);
            }

            Logger.logMessage("Offering new job to PrinterQueueHandler.offerToPrintQueue:" + newJob.toString(), KMS_LOG, Logger.LEVEL.DEBUG);

            if(PrinterQueueHandler.getInstance().offerToPrintQueue(newJob)){
                KMSManagerSocketHandler.sendConfirmationMessage(
                        originatingStationID,
                        MMHEvent.PRINT_ORDER,
                        "PrinterID: " + printModel.PrinterID + ", PATransactionID: " + printModel.PATransactionID,
                        "Successfully added order to print queue", stationReferenceID);
            }else{
                KMSManagerSocketHandler.sendErrorMessage(
                        originatingStationID,
                        MMHEvent.PRINT_ORDER,
                        "PrinterID: "+ printModel.PrinterID +", PATransactionID: "+ printModel.PATransactionID,
                        "Failed to add the order to the print queue", stationReferenceID);
            }
        }
    }

    public void query(String originatingStationID, KMSQueryModel query, String stationReferenceID){
        Logger.logMessage("KMSManager.query", KMS_LOG, Logger.LEVEL.TRACE);
        populateQueryModel(originatingStationID, query, stationReferenceID);
        KMSManagerSocketHandler.sendMessageDirectlyToStation(originatingStationID, MMHEvent.INQUIRY, query, stationReferenceID);
    }

    /**
     * Takes in the query model and pupulates the requested fields with data
     * @param originatingStationID
     * @param query
     * @param stationReferenceID
     */
    public void populateQueryModel(String originatingStationID, KMSQueryModel query, String stationReferenceID){
        if(query.fields != null && query.fields.length > 0){
            query.results = new HashMap<>();
            for(int i = 0; i < query.fields.length; i++){
                String[] args = query.fields[i].split(" ");
                switch (args[0]) {
                    case "onlineStations":
                        ArrayList onlineStations = KMSManagerSocketHandler.getOnlineStationIDs();
                        query.results.put("onlineStations", onlineStations.toArray());
                        break;
                    case "mirrorSets":
                        HashMap m = new HashMap();
                        for (String key : stationIDToMirrorPool.keySet()) {
                            m.put(key, stationIDToMirrorPool.get(key).toArray());
                        }
                        query.results.put("mirrorSets", m);
                        break;
                    case "listeners":
                        HashMap l = new HashMap();
                        for (String key : stationIDsToListenerIDs.keySet()) {
                            l.put(key, stationIDsToListenerIDs.get(key).toArray());
                        }
                        query.results.put("listeners", l);
                        break;
                    case "orderStatus"://TODO: this likely exceeds the websocket message limit of 64kb (or 30k-ish characters)
                        query.results.put("orderStatus", orderStatusMap);
                        break;
                    case "finishedOrders":
                        query.results.put("finishedOrders", finishedOrders);
                        break;
                    case "automaticBackups":
                        HashMap a = new HashMap();
                        for (String key : stationIDToAutomaticBackupStationID.keySet()) {
                            a.put(key, stationIDToAutomaticBackupStationID.get(key));
                        }
                        query.results.put("automaticBackups", a);
                        break;
                    case "manualBackups":
                        HashMap mb = new HashMap();
                        for (String key : stationIDToManualBackupStationID.keySet()) {
                            mb.put(key, stationIDToManualBackupStationID.get(key));
                        }
                        query.results.put("manualBackups", mb);
                        break;
                    case "displayMappings":
                        HashMap dm = new HashMap();
                        for (String key : displayToPrepIDs.keySet()) {
                            dm.put(key, displayToPrepIDs.get(key).toArray());
                        }
                        query.results.put("displayMappings", dm);
                        break;
                    case "ordersBeingHandled":
                        //get mappings of what orders details are being handled by what stations and what transactions have what lines
                        boolean isDisplay = false;
                        isDisplay = displayIDs.contains(originatingStationID);
                        String mirroring = null;
                        if(stationIDToMirrorStationID.containsKey(originatingStationID)){
                            mirroring = stationIDToMirrorStationID.get(originatingStationID);
                        }

                        HashMap<String, HashMap<String, Object>> transactionIDsToLineIDs = new HashMap<>();
                        for (String order : orderStatusMap.keySet()) {
                            KMSOrderModel orderModel = orderStatusMap.get(order);
                            if(orderModel.getOrderStatusId() == KMSOrderStatus.CANCELED || orderModel.getOrderStatusId() == KMSOrderStatus.FINALIZED) continue;
                            for(KMSProductModel prod: orderModel.getProducts()){
                                if(prod.handlingKMSStationId != null){
                                    //if the originatingStationID is the handler or is in the same mirror pool, add it
                                    if (originatingStationID.compareTo(prod.handlingKMSStationId) == 0
                                            || isDisplay
                                            || (mirroring != null && mirroring.compareTo(prod.handlingKMSStationId) == 0)) {

                                        // check that this product is either not completed OR is only being sent to
                                        // the relevant stations that should be showing it if it is completed
                                        if (prod.getOrderStatusId() != KMSOrderStatus.COMPLETED ||
                                                (displayIDs.contains(originatingStationID) || (mirroring != null && displayIDs.contains(mirroring))) ||
                                                (prod.getOrderStatusId() == KMSOrderStatus.COMPLETED &&
                                                        ((!hasExpeditorProducts(orderModel)) ||
                                                                expeditorIDs.contains(originatingStationID) || (mirroring != null && expeditorIDs.contains(mirroring))))) {
                                            if(!transactionIDsToLineIDs.containsKey(orderModel.getId())) {
                                                HashMap<String, Object> transactionIdsToLineId = new HashMap<>();
                                                transactionIdsToLineId.put("orderStatusId", orderModel.getOrderStatusId());
                                                // transLines maps to a HashMap that is transLineId : orderStatusId of that transLine
                                                transactionIdsToLineId.put("transLines", new HashMap<String, Integer>());
                                                transactionIDsToLineIDs.put(orderModel.getId(), transactionIdsToLineId);
                                            }

                                            ((HashMap<String, Integer>) transactionIDsToLineIDs.get(orderModel.getId()).get("transLines")).put(prod.getTransLineId(), prod.getOrderStatusId());
                                        }
                                    }
                                }
                            }
                        }
                        query.results.put("ordersBeingHandled", transactionIDsToLineIDs);
                        break;
                    case "handlerMappings":
                        HashMap<String, ArrayList<String>> handlerMappings = new HashMap<>();
                        for (String order : orderStatusMap.keySet()) {
                            KMSOrderModel orderModel = orderStatusMap.get(order);
                            for(KMSProductModel prod: orderModel.getProducts()){
                                //if(prod.handlingKMSStationId != null){
                                    //if the originatingStationID is the handler or is in the same mirror pool, add it
                                    //(originatingStationID.compareTo(prod.handlingKMSStationId) == 0
                                     //       || isDisplay
                                     //       || (mirroring != null && mirroring.compareTo(prod.handlingKMSStationId) == 0)){
                                        //|| (stationIDToMirrorPool.contains(prod.handlingKMSStationId) && stationIDToMirrorPool.get(prod.handlingKMSStationId).contains(originatingStationID))){
                                String handler = prod.handlingKMSStationId;
                                if(handler == null)
                                    handler = "orphaned";
                                if(!handlerMappings.containsKey(handler)){
                                    handlerMappings.put(handler, new ArrayList<String>());
                                }
                                handlerMappings.get(handler).add(prod.getTransLineId());
                                    //}
                                //}
                            }
                        }
                        query.results.put("handlerMappings", handlerMappings);
                        break;
                    case "order":
                        if(args.length > 1 && orderStatusMap.containsKey(args[1])){
                            Logger.logMessage("orderStatusMap contains " + args[1], "KMS.log", Logger.LEVEL.DEBUG);

                            boolean isDisplayStation = false;
                            isDisplayStation = displayIDs.contains(originatingStationID);
                            String mirroringStation = null;
                            if(stationIDToMirrorStationID.containsKey(originatingStationID)){
                                mirroringStation = stationIDToMirrorStationID.get(originatingStationID);
                            }

                            //grab the order subset specifically for the requesting station
                            KMSOrderModel order = orderStatusMap.get(args[1]);
                            KMSOrderModel duplicateOrder = new KMSOrderModel();
                            duplicateOrder.originalPrintJob = order.originalPrintJob;
                            duplicateOrder.setId(order.getId());
                            duplicateOrder.setOrderNumber(order.getOrderNumber());
                            duplicateOrder.setPreviousOrderNumbers(order.getPreviousOrderNumbers());
                            duplicateOrder.setComment(order.getComment());
                            duplicateOrder.setOrderPriorityId(order.getOrderPriorityId());
                            duplicateOrder.setPersonName(order.getPersonName());
                            duplicateOrder.setReceivedTime(order.getReceivedTime());
                            for(KMSProductModel p : order.getProducts()){
                                if(p.handlingKMSStationId != null){
                                    if(originatingStationID.compareTo(p.handlingKMSStationId) == 0
                                            || isDisplayStation
                                            || (mirroringStation != null && mirroringStation.compareTo(p.handlingKMSStationId) == 0)){
                                        duplicateOrder.getProducts().add(p);
                                    }
                                }else{
                                    Logger.logMessage("Product with no handling station found in order" + order.getId() + " Product: " + p.getName(), "KMS.log", Logger.LEVEL.ERROR);
                                }
                            }

                            duplicateOrder.setOrderStatusId(order.getOrderStatusId());

                            query.results.put("order", duplicateOrder);
                        }else{
                            Logger.logMessage("orderStatusMap does not contains queried order", "KMS.log", Logger.LEVEL.ERROR);
                        }

                        break;
                    case "details":
                        HashMap<String, ArrayList<String>> details = new HashMap<>();
                        for (String order : orderStatusMap.keySet()) {
                            ArrayList<String> lineDetails = new ArrayList<String>();
                            KMSOrderModel orderModel = orderStatusMap.get(order);

                            if(orderModel.originalPrintJob != null){
                                for(QCPrintJobDetail d : orderModel.originalPrintJob.getPrintJobDetails()){
                                    lineDetails.add(d.getDecodedLineDetail());
                                }
                            }else{
                                lineDetails.add("data unavailable");
                            }
                            details.put(String.format("%-12s",orderModel.getId()) + String.format("%5s",orderModel.getOrderNumber()), lineDetails);
                        }
                        query.results.put("details", details);

                        break;
                    default:
                        break;
                }
            }
        }
    }

    public static HashMap<String, Object> getMappingConfig(){
        HashMap<String, Object> mappings = new HashMap<>();
        HashMap a = new HashMap();
        for(String key : stationIDToAutomaticBackupStationID.keySet()){
            a.put(key, stationIDToAutomaticBackupStationID.get(key));
        }
        mappings.put("automaticBackups", a);
        HashMap m = new HashMap();
        for(String key : stationIDToMirrorPool.keySet()){
            m.put(key, stationIDToMirrorPool.get(key).toArray());
        }
        mappings.put("mirrorSets", m);
        HashMap mb = new HashMap();
        for(String key : stationIDToManualBackupStationID.keySet()){
            mb.put(key, stationIDToManualBackupStationID.get(key));
        }
        mappings.put("manualBackups", mb);
        return mappings;
    }

    /**
     * Add a new pending orphan order to the list of pending orphaned orders
     * (pending meaning they are currently waiting to be accepted by a station)
     *
     * @param orphan the new pending orphan order
     */
    public void addPendingOrphanOrder(KMSOrphanOrderModel orphan) {
        if (orphan != null) {
            pendingOrphanOrders.add(orphan);
        }
    }

    /**
     * Get the entire list of pending orphan orders
     * (pending meaning they are currently waiting to be accepted by a station)
     *
     * @return the list of pending orphan orders
     */
    public static Set<KMSOrphanOrderModel> getPendingOrphanOrders() {
        return pendingOrphanOrders;
    }

    /**
     * Get the list of pending orphan orders that were supposed to be handled by a given station
     * (pending meaning they are currently waiting to be accepted by a station)
     *
     * @return the list of pending orphan orders
     */
    public static Set<KMSOrphanOrderModel> getPendingOrphanOrdersByStation(String stationId) {
        HashSet<KMSOrphanOrderModel> pendingOrphanOrdersForStation = new HashSet<>();

        Set<KMSOrphanOrderModel> pendingOrphanOrders = getPendingOrphanOrders();
        for (KMSOrphanOrderModel orphan : pendingOrphanOrders) {
            if (Integer.toString(orphan.getOriginalHandlingStationID()).equals(stationId)) {
                pendingOrphanOrdersForStation.add(orphan);
            }
        }

        return pendingOrphanOrdersForStation;
    }

    /**
     * Removes a pending orphan order from the list of pending orphan orders based on the KMS info model
     * (this implies the orphan order has now been accepted by a station)
     *
     * @param infoModel the info model for the pending orphan order to remove
     */
    public void removePendingOrphanOrder(KMSInfoModel infoModel) {
        if (pendingOrphanOrders != null) {
            pendingOrphanOrders.removeIf(orphan -> orphan.getId().equals(infoModel.reference) && orphan.getOriginalHandlingStationID() == infoModel.originalHandlingKMSStationId);
        }
    }

    /**
     * Called when an order could not be sent to any station. This will send out a call to all stations asking them to receive the order
     * @param orphan
     * @return
     */
    public boolean createOrphanOrder(KMSOrphanOrderModel orphan){
        Logger.logMessage("createOrphanOrder for " + orphan.getId(), KMS_LOG, Logger.LEVEL.TRACE);
        ArrayList<String> onlineStations = KMSManagerSocketHandler.getOnlineStationIDs();
        if (onlineStations != null && onlineStations.size() > 0) {

            // add the new orphan to the list of pending orphans
            addPendingOrphanOrder(orphan);

            for (String stationID: onlineStations) {
                KMSManagerSocketHandler.sendMessageDirectlyToStation(stationID, MMHEvent.ORPHANED_ORDER, orphan, null);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * When a station takes on an orphan order as it's own, this gets called to assign that order to the station,
     * and notify other stations that it was adopted
     */
    public void receivedOrphanOrder(String originatingStationID, KMSInfoModel confirmationFields, String stationReferenceID){
        Logger.logMessage("KMSManager.receivedOrphanOrder from " + originatingStationID + " ref " + stationReferenceID, KMS_LOG, Logger.LEVEL.TRACE);
        ArrayList<String> onlineStations = KMSManagerSocketHandler.getOnlineStationIDs();
        if (orderStatusMap.containsKey(confirmationFields.reference)) {
            KMSOrderModel order = orderStatusMap.get(confirmationFields.reference);

            if(order.getOrderStatusId() == KMSOrderStatus.SENT){
                //order.setOrderStatusId(1);
                //KMSManager.logEvent(Integer.parseInt(originatingStationID), Long.parseLong(confirmationFields.reference), null, MMHEvent.ORDER_STATUS_UPDATE, "Order Adopted", order.getOrderStatusId());
                KMSStatusModel status = new KMSStatusModel();
                status.KMSOrderStatusID = KMSOrderStatus.RECEIVED;
                status.PreviousKMSOrderStatusID = KMSOrderStatus.SENT;
                status.PATransactionID = (confirmationFields.reference);
                updateOrderStatus(originatingStationID, status, "Order Adopted", stationReferenceID);
            }

            // remove the orphan from the list of pending orphans
            removePendingOrphanOrder(confirmationFields);

            // dont require that the order still be in sent status - if someone accepts the orphan, changed the print
            // job details to reflect the fact the orphan's new handling station
            updatePrintJobDetailsToAcceptedStation(order, originatingStationID, confirmationFields.originalHandlingKMSStationId);

            // update the print job status in the database
            PrintStatus s = new PrintStatus().addUpdatedDTM(LocalDateTime.now());
            s.addPrintStatusType(PrintStatusType.DONE);

            try {
                PrintJobStatusHandler.getInstance().updatePrintStatusMapForPrintJobByStationId(s, order.originalPrintJob, Integer.parseInt(originatingStationID));
            }
            catch (Exception ex) {
                Logger.logMessage("KMSManager.receivedOrphanOrder - exception thrown when trying to get the PrintJobStatusHandler instance", KMS_LOG, Logger.LEVEL.ERROR);
                Logger.logException(ex, KMS_LOG);
            }

            for(KMSProductModel p : order.getProductsByHandlingStationId(confirmationFields.originalHandlingKMSStationId)){
                p.handlingKMSStationId = originatingStationID;
                // TODO updateOrderProductStatus();

//                if(p.handlingKMSStationId == null){
//                //TODO: there is a bug here. What happens if 2 different subsets of the
//                //TODO: same order couldnt be received? this will mark both subsets as belonging to this station, when they may be at 2 different stations...
//                    p.handlingKMSStationId = originatingStationID;
//                }
            }

            if(onlineStations != null && onlineStations.size() > 0){
                confirmationFields.operation = MMHEvent.ORPHANED_ORDER;
                confirmationFields.message = "Orphaned order has been adopted.";
                for(String stationID: onlineStations){
                    KMSManagerSocketHandler.sendMessageDirectlyToStation(stationID, MMHEvent.CONFIRMATION, confirmationFields, stationReferenceID);
                }
            }
        }else{
            KMSManagerSocketHandler.sendErrorMessage(
                    originatingStationID,
                    MMHEvent.ORPHANED_ORDER,
                    confirmationFields.reference,
                    "Unknown Reference.", stationReferenceID);
        }
    }

    /**
     * Take a print job, turn it into an order and send it off to the appropriate stations
     * @param printJob
     * @return
     */
    public boolean receivePrintJob(QCPrintJob printJob, ArrayList<Integer> failedDetails){
        Logger.logMessage("KMSManager.receivePrintJob" + printJob.toString(), KMS_LOG, Logger.LEVEL.TRACE);
        boolean successful = true;
        boolean completeFailure = true;
        //details of the order will be broken up and sent off to stations that match the stationID
        //because the KMSManager is running on the PrinterHost Terminal,
        //      we know that the stations we have connections to are all within this RC,
        //      and because there can only be one Expediter per RC,
        //      we can grab the first
        KMSOrderModel entireOrder;// = new KMSOrderModel();
        HashMap<Integer, KMSOrderModel> splitOrders = new HashMap<>();

        if(printJob.getPrevOrderNumsForKDS() != null && printJob.getPrevOrderNumsForKDS().length() > 0){
            updatePrintJobDetailsToExistingOrderStatus(printJob);
        }
        //this will update the print job's status to
        entireOrder = convertPrintJobToOrder(printJob, splitOrders);

        //update to sent
        if(entireOrder.getOrderStatusId() == KMSOrderStatus.WAITING)//Waiting
            entireOrder.setOrderStatusId(KMSOrderStatus.SENT);//update the status to sent.

        //add the order to the status map.
        orderStatusMap.put(entireOrder.getId(), entireOrder);

        //iterate through the hashmap and send each order to the online prep station. Print where each part is going
        for (Integer i: splitOrders.keySet()){
            KMSOrderModel forStation = splitOrders.get(i);

            if (forStation == null) {
                continue;
            }

            if(forStation.getOrderStatusId() == KMSOrderStatus.WAITING || forStation.getOrderStatusId() == -1)//Waiting
                forStation.setOrderStatusId(KMSOrderStatus.SENT);//update the status to sent.
            for(KMSProductModel product : forStation.getProducts()){
                //update the status of the entire order's details that are in forStation to sent
                if(product.getOrderStatusId() == KMSOrderStatus.WAITING || product.getOrderStatusId() == -1){//if waiting
                    product.setOrderStatusId(KMSOrderStatus.SENT);//update to sent (this is the same object thats in entire order
                }
            }
            /*Logic used (Defect 4180):
             * Use forStation's (forStation = the current suborder) previous order numbers to search the orderStatusMap for the pre-existing order.
             * After finding that previous order, check its products for the handlingKMSStationID -> then match those to the forStation's products,
             * then,
             * If the order has a different station already handling it, then we should check if the order in this case is an updated version of the same order.
             * If it is an updated version, then we should first try sending to the handling station. If this fails, THEN we should continue on and create the new orphaned order
             */
            boolean openOrderSuccess = false; //default to false so if it's not an open order or that fails we proceed to regular order sending/orphaning

            //if the order is meant for the expeditor it follows different logic and is "orphaned" differently, thus it should just proceed as normal
            boolean isForDisplayOrExpeditor = displayIDs.contains(i.toString()) || expeditorIDs.contains(i.toString());

            if(!isForDisplayOrExpeditor) {
                String forStationPrevOrderID = null;
                if ((StringFunctions.stringHasContent(forStation.getPreviousOrderNumbers())) && (forStation.getPreviousOrderNumbers().split(",").length > 0)
                        && (StringFunctions.stringHasContent(forStation.getPreviousOrderNumbers().split(",")[0]))) {
                    forStationPrevOrderID = forStation.getPreviousOrderNumbers().split(",")[0]; //grab the most recent prevOrder
                }
                /*Case where order is modified after creation (open order)*/
                if (StringFunctions.stringHasContent(forStationPrevOrderID)) {
                    String splitOrderHandlingStation = "";
                    for (String transID : orderStatusMap.keySet()) {
                        KMSOrderModel orderInMap = orderStatusMap.get(transID);
                        /*if the mapped order has a match to the previous order # of current order check products info within that mapped order*/
                        if ((orderInMap != null) && (String.valueOf(orderInMap.getOrderNumber()).equalsIgnoreCase(forStationPrevOrderID))) {
                            for (KMSProductModel forStationProd : forStation.getProducts()) {
                                for (KMSProductModel product : orderInMap.getProducts()) {
                                    if (product.handlingKMSStationId != null) {
                                        if (forStationProd.getProductId() == product.getProductId()) {
                                            Logger.logMessage("KMSManager.receivePrintJob - Matched order "+ forStation.getOrderNumber()+ " to orderStatusMap order ID at Product ID " + product.getProductId() + " - handling at: " + product.handlingKMSStationId, KMS_LOG, Logger.LEVEL.DEBUG);
                                            splitOrderHandlingStation = product.handlingKMSStationId;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (StringFunctions.stringHasContent(splitOrderHandlingStation)) {
                        Logger.logMessage("KMSManager.receivePrintJob - Order " + forStation.getOrderNumber() + " with previous order #'s {" + forStation.getPreviousOrderNumbers() + "} - Has HandlingStation: " + splitOrderHandlingStation, KMS_LOG, Logger.LEVEL.DEBUG);
                        updatePrintJobDetailsToAcceptedStation(forStation, splitOrderHandlingStation, i);
                        openOrderSuccess = sendNewOrderToStation(splitOrderHandlingStation, forStation);
                    }
                }
            }
            boolean success = true;
            boolean sentOrphanedOrder = false;
            /*End Defect 4180 Logic*/

            if(!openOrderSuccess) {
                success = sendNewOrderToStation(i.toString(), forStation);
                if (!success) {
                    //We want to find a station to adopt the order.
                    Logger.logMessage("KMSManager.receivePrintJob - failed to send order subset to " + i.toString() + ", creating orphan.", KMS_LOG, Logger.LEVEL.TRACE);

                    String originalHandlingStationName = StationCollection.getStationName(i);
                    KMSOrphanOrderModel orphanOrder = new KMSOrphanOrderModel(i, originalHandlingStationName, forStation);

                    //should orphaned Order event not be sent when failing to send to an expeditor?
                    if (!expeditorIDs.contains(i.toString()) && createOrphanOrder(orphanOrder)) { //not every station is offline, at least 1 will get it
                        success = true;
                        sentOrphanedOrder = true;
                    }
                }
            }
            completeFailure = completeFailure && !success;
            successful = (successful && success) || (successful && openOrderSuccess);

            PrintStatus s = new PrintStatus()
                    .addUpdatedDTM(LocalDateTime.now());
            if (sentOrphanedOrder) {
                s.addPrintStatusType(PrintStatusType.SENT);
            }
            else if (success) {
                s.addPrintStatusType(PrintStatusType.DONE);
            }
            else {
                s.addPrintStatusType(PrintStatusType.ERROR);
            }

            try {
                PrintJobStatusHandler.getInstance().updatePrintStatusMapForPrintJobByStationId(s, printJob, i);
            }
            catch (Exception ex) {
                Logger.logMessage("KMSManager.receivePrintJob - exception thrown when trying to get the PrintJobStatusHandler instance", KMS_LOG, Logger.LEVEL.ERROR);
                Logger.logException(ex, KMS_LOG);
            }
        }
        //send the whole order to the display stations
        //only send to display stations that are mapped to the prep stations that an order is sent to
        for(String displayID: displayIDs){
            CopyOnWriteArraySet<String> mappedPreps = displayToPrepIDs.get(displayID);
            //send the order to the display if at least 1 prep station being sent to is mapped to the station
            for (Integer i: splitOrders.keySet()){
                if(mappedPreps.contains(i.toString())){
                    if(KMSManagerSocketHandler.messageCanBeSent(displayID)){
                        Logger.logMessage("KMSManager.receivePrintJob - sending entire order to display " + displayID + " because order is being sent to" + i + " and the display is mapped to " + mappedPreps.toArray().toString(), KMS_LOG, Logger.LEVEL.TRACE);
                        String sentTo = KMSManagerSocketHandler.sendMessageToOnlineStationAndMirror(displayID, MMHEvent.KMSEventType.ORDER_RECEIVED, entireOrder, null);
                    }
                    break;
                }
            }

        }

        if(completeFailure)
            entireOrder.setOrderStatusId(KMSOrderStatus.ERROR);

        return successful;
    }

// <editor-fold desc="update to existing order status">
    /**
     * <p>Updates the order status of the print job details within a print job to reflect any changes that may have
     * been made to previous versions of a product within the print job. For example, this is to prevent sending a completed product
     * to the stations as a new product in new versions of the order.</p>
     *
     * @param currentPrintJob The {@link QCPrintJob} to update the order statuses of the details for.
     */
    private void updatePrintJobDetailsToExistingOrderStatus (QCPrintJob currentPrintJob) {

        if (currentPrintJob == null) {
            Logger.logMessage("The QCPrintJob passed to KMSManager.updatePrintJobDetailsToExistingOrderStatus can't be null, " +
                    "unable to update the details of the print job.", KMS_LOG, Logger.LEVEL.ERROR);
            return;
        }

        Logger.logMessage(String.format("Attempting to update the print job details for the print job [%s] to account for any changes to previous versions.",
                Objects.toString(currentPrintJob.toString(), "N/A")), KMS_LOG, Logger.LEVEL.TRACE);

        String[] prevOrderNums = StringFunctions.stringHasContent(currentPrintJob.getPrevOrderNumsForKDS()) ? currentPrintJob.getPrevOrderNumsForKDS().split("\\s*,\\s*") : null;
        if (!DataFunctions.isEmptyGenericArr(prevOrderNums)) {
            for (String prevOrderNum : prevOrderNums) {
                // get the products from a previous version of the order
                ArrayList<KMSProductModel> productsFromExistingOrder = getProductsFromPreviousVersionOfOrder(prevOrderNum);
                if (DataFunctions.isEmptyCollection(productsFromExistingOrder)) {
                    Logger.logMessage(String.format("No previous versions of products found within the order %s in KMSManager.updatePrintJobDetailsToExistingOrderStatus, " +
                            "unable to update the details of the current print job.",
                            Objects.toString(prevOrderNum, "N/A")), KMS_LOG, Logger.LEVEL.TRACE);
                    return;
                }
                // for each product in the new version of the order, check through the existing order for matching linked from IDs, and take on their order status
                ArrayList<QCPrintJobDetail> currentPrintJobDetails = currentPrintJob.getPrintJobDetails();
                if (DataFunctions.isEmptyCollection(currentPrintJobDetails)) {
                    Logger.logMessage(String.format("No details were found within the current print job %s in KMSManager.updatePrintJobDetailsToExistingOrderStatus, " +
                            "unable to update the details of the current print job.",
                            Objects.toString(currentPrintJob.toString(), "N/A")), KMS_LOG, Logger.LEVEL.TRACE);
                    return;
                }
                updateProductsInCurrentOrder(productsFromExistingOrder, currentPrintJobDetails);
            }
        }

    }

    /**
     * <p>Gets the products that were in a previous version of the the order represented by the current print job.</p>
     *
     * @param orderNum The previous order number {@link String} to get the products within.
     * @return An {@link ArrayList} of {@link KMSProductModel} corresponding to previous versions of products within the current print job.
     */
    private ArrayList<KMSProductModel> getProductsFromPreviousVersionOfOrder (String orderNum) {

        if (!StringFunctions.stringHasContent(orderNum)) {
            Logger.logMessage("The order number passed to KMSManager.getProductsFromPreviousVersionOfOrder can't be null, unable " +
                    "to determine which products were within a previous version of the order, now returning null!", KMS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyMap(orderStatusMap)) {
            Logger.logMessage("No orders were found within the order status map in KMSManager.getProductsFromPreviousVersionOfOrder, unable " +
                    "to determine which products were within a previous version of the order, now returning null!", KMS_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        // try to find a previous version of the order within the order status map
        KMSOrderModel previousVersionOfOrder = null;
        for (String paTransactionID : orderStatusMap.keySet()) {
            String orderID = orderStatusMap.get(paTransactionID) != null ? String.valueOf(orderStatusMap.get(paTransactionID).getOrderNumber()) : null;
            if ((StringFunctions.stringHasContent(orderID)) && (orderID.trim().equalsIgnoreCase(orderNum.trim()))) {
                Logger.logMessage(String.format("KMSManager.updatePrintJobDetailsToExistingOrderStatus - found an old order version of order number: %s",
                        Objects.toString(orderNum, "N/A")), KMS_LOG, Logger.LEVEL.TRACE);
                previousVersionOfOrder = orderStatusMap.get(paTransactionID);
                break;
            }
        }

        // return the products from the previous version of the order
        if (previousVersionOfOrder != null) {
            return previousVersionOfOrder.getProducts();
        }

        return null;
    }

    /**
     * <p>Iterates through the print job details within the current print job and updates the order status of each print job detail
     * with the order status contained within the previous version of the product represented by the print job detail.</p>
     *
     * @param productsFromExistingOrder An {@link ArrayList} of {@link KMSProductModel} corresponding to previous versions of products within the current print job.
     * @param currentPrintJobDetails An {@link ArrayList} of {@link QCPrintJobDetail} corresponding to the print job details within the current print job.
     */
    private void updateProductsInCurrentOrder (ArrayList<KMSProductModel> productsFromExistingOrder, ArrayList<QCPrintJobDetail> currentPrintJobDetails) {

        if (DataFunctions.isEmptyCollection(productsFromExistingOrder)) {
            Logger.logMessage("The products from a previous version of the current order passed to KMSManager.getOldVersionsOfProductsInCurrentOrder can't be null or empty, unable to " +
                    "update products in the current order to align with previous versions of the product.", KMS_LOG, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(currentPrintJobDetails)) {
            Logger.logMessage("The print job details passed to KMSManager.getOldVersionsOfProductsInCurrentOrder can't be null or empty, unable to " +
                    "update products in the current order to align with previous versions of the product.", KMS_LOG, Logger.LEVEL.ERROR);
            return;
        }

        for (QCPrintJobDetail qcPrintJobDetail : currentPrintJobDetails) {
            if ((qcPrintJobDetail != null)
                    && (StringFunctions.stringHasContent(qcPrintJobDetail.getDecodedLineDetail()))
                    && (qcPrintJobDetail.getDecodedLineDetail().startsWith(KDSJobInfo.PRODUCT_INDICATOR))
                    && (StringFunctions.stringHasContent(qcPrintJobDetail.getLinkedFromIDs()))) {
                String[] prevTransLineIDsOfDetail = qcPrintJobDetail.getLinkedFromIDs().split("\\s*,\\s*");
                // match up the old trans line ID to the existing version of the product (the last version of the product is the last previous trans line ID listed)
                String prevTransLineIDOfDetail = prevTransLineIDsOfDetail[prevTransLineIDsOfDetail.length - 1];
                for (KMSProductModel existingProduct : productsFromExistingOrder) {
                    if ((StringFunctions.stringHasContent(prevTransLineIDOfDetail))
                            && (StringFunctions.stringHasContent(existingProduct.getTransLineId()))
                            && (prevTransLineIDOfDetail.trim().equalsIgnoreCase(existingProduct.getTransLineId()))) {
                        qcPrintJobDetail.setKMSOrderStatusID(existingProduct.getOrderStatusId());
                        Logger.logMessage(String.format("KMSManager.updatePrintJobDetailsToExistingOrderStatus - updated product with a trans line ID of %s status to a KMSOrderStatus of %s",
                                Objects.toString(existingProduct.getTransLineId(), "N/A"),
                                Objects.toString(existingProduct.getOrderStatusId(), "N/A")), KMS_LOG, Logger.LEVEL.TRACE);
                        break;
                    }
                }
            }
        }
    }
// </editor-fold>

    /**
     * Iterate's through the passed order and updates its print job with the KMS Station ID of the station currently handling it
     * @param order
     * @param orderHandlingStation
     */
    private void updatePrintJobDetailsToAcceptedStation(KMSOrderModel order, String orderHandlingStation, int originalHandlingStation){
        QCPrintJob adjustedPrintJob = order.originalPrintJob;
        ArrayList<QCPrintJobDetail> adjustedDetails;
        adjustedDetails = adjustedPrintJob.getPrintJobDetails();
        try{
            int handlingStation = Integer.parseInt(orderHandlingStation);

            for (int i = 0; i<adjustedDetails.size();i++) {
                if (adjustedDetails.get(i).getKMSStationID() == originalHandlingStation) {
                    adjustedDetails.get(i).setKMSStationID(handlingStation);
                }
            }
        } catch (Exception e) {
            Logger.logMessage("Error in KMSManager.updatePrintJobDetailsToAcceptedStation() : message -" + e, KMS_LOG, Logger.LEVEL.DEBUG);
            return;
        }
        Logger.logMessage("Order " + order.getOrderNumber() + " Had its print job KMS Station ID updated to " + orderHandlingStation, KMS_LOG, Logger.LEVEL.DEBUG);
        adjustedPrintJob.setPrintJobDetails(adjustedDetails);
        Logger.logMessage(adjustedPrintJob.toString() , KMS_LOG, Logger.LEVEL.LUDICROUS );
        order.originalPrintJob = adjustedPrintJob;
    }

    /**
     * This function populates a KMSOrderModel from a QCPrintJob
     * @param qcPrintJob input QCPrintJob
     * @param stationToOrderMappings output hashmap of orders where the products contained within are split based on what station they are for.
     * @return a KMSOrderModel (a single order with all the products (not split by destined station)
     */
    private KMSOrderModel convertPrintJobToOrder(QCPrintJob qcPrintJob, HashMap<Integer, KMSOrderModel> stationToOrderMappings){
        Logger.logMessage("KMSManager.convertPrintJobToOrder", KMS_LOG, Logger.LEVEL.TRACE);

        KMSOrderModel order = new KMSOrderModel();
        try {
            if(stationToOrderMappings == null){
                Logger.logMessage("stationToOrderMappings to populate is null. KMSManager.convertPrintJobToOrder can not split order by station.", KMS_LOG, Logger.LEVEL.DEBUG);
                order = new KMSOrderModel();
            }
            // make sure the print job exists
            if (qcPrintJob == null) {
                return order;
            }

            //is the transaction in online or offline mode
            if(qcPrintJob.isOnlineTrans()){
                order.setId(qcPrintJob.getPaTransactionID() + "");
            }else{
                order.setId(qcPrintJob.getPaTransactionID() + "T" + qcPrintJob.getTerminalID());
            }

            // set order information
            order.setReceivedTime(new Timestamp(System.currentTimeMillis()).toString());
            if(qcPrintJob.getOrderNumber() != null && !qcPrintJob.getOrderNumber().isEmpty()){
                String num = qcPrintJob.getOrderNumber();
                order.setOrderNumber(num);
            }
            order.setComment(qcPrintJob.getTransComment());
            order.setOrderStatusId(KMSOrderStatus.WAITING);

            Object priority =  new DataManager().parameterizedExecuteScalar("data.KMSManager.getOrderPriority", new Object[]{qcPrintJob.getTerminalID()});
            if(priority != null && priority.toString().length() > 0)
                order.setOrderPriorityId(Integer.parseInt(priority.toString()));
            else
                order.setOrderPriorityId(1);// 1 is normal 2 is rush. defined on the terminal

            String personName = "";
            if (StringFunctions.stringHasContent(qcPrintJob.getTransName())) {
                personName = qcPrintJob.getTransName();
            }
            else if (StringFunctions.stringHasContent(qcPrintJob.getPersonName())) {
                personName = qcPrintJob.getPersonName();
            }
            if (StringFunctions.stringHasContent(personName)) {
                order.setPersonName(personName);
            }

            //TODO: we arent using textmsgs, right?
//            if (qcPrintJob.getUseTextMessages()) {
//                kdsOrder.getCustomer().setPhone(qcPrintJob.getPhone());
//            }

            order.setPreviousOrderNumbers(qcPrintJob.getPrevOrderNumsForKDS());
            Logger.logMessage("Set Previous Order Numbers to " + qcPrintJob.getPrevOrderNumsForKDS(), "KMS.log", Logger.LEVEL.DEBUG);

//            HashMap<Integer, Integer> printerToStationMappings = new HashMap<>();
//            ArrayList<HashMap> queryRes = new DataManager().parameterizedExecuteQuery("data.kitchenPrinter.GetPrintersAndStations", new Object[]{}, true);
//            if (!DataFunctions.isEmptyCollection(queryRes)) {
//                for (Object hmObj : queryRes) {
//                    if ((hmObj instanceof HashMap) && (!DataFunctions.isEmptyMap(((HashMap) hmObj)))) {
//                        int printerID = HashMapDataFns.getIntVal(((HashMap) hmObj), "PRINTERID");
//                        int stationID = HashMapDataFns.getIntVal(((HashMap) hmObj), "STATIONID");
//                        printerToStationMappings.put(printerID, stationID);
//                    }
//                }
//            }

            ArrayList<QCPrintJobDetail> details = qcPrintJob.getPrintJobDetails();
            if ((!DataFunctions.isEmptyCollection(details))){//TODO: do we want this? ->   && (!DataFunctions.isEmptyMap(printerToStationMappings))) {
                int productCount = 0;

                //make a duplicate list of concatinated details
                ArrayList<QCPrintJobDetail> mergedDetails = new ArrayList<>();
                for (QCPrintJobDetail detail : details) {
                    mergedDetails.add(detail.copy());
                }
                QCPrintJobDetail lastKMSItem = null;
                for(int i = mergedDetails.size()-1; i >= 0; i--){
                    QCPrintJobDetail detail = mergedDetails.get(i);
                    Logger.logMessage("Checking Detail: " + detail.getPaTransLineItemID(), "KMS.log", Logger.LEVEL.DEBUG);
                    if(detail.isModifier()){
                        if(lastKMSItem != null && lastKMSItem.isModifier() && lastKMSItem.getPaTransLineItemID() == detail.getPaTransLineItemID()){
                            String currentDecode = KitchenPrinterCommon.decode(detail.getLineDetail(), "KMS.log");
                            String lastDecode = KitchenPrinterCommon.decode(lastKMSItem.getLineDetail(), "KMS.log");
                            String combined = currentDecode + lastDecode;
                            Logger.logMessage("Setting name to " + combined, "KMS.log", Logger.LEVEL.DEBUG);
                            detail.setLineDetail(KitchenPrinterCommon.encode(combined, "KMS.log"));
                            Logger.logMessage("Removed Detail: " + lastKMSItem.toString(), "KMS.log", Logger.LEVEL.DEBUG);
                            mergedDetails.remove(lastKMSItem);
                        }
                    }else{
                        if(lastKMSItem != null && !lastKMSItem.isModifier() && lastKMSItem.getPaTransLineItemID() == detail.getPaTransLineItemID()){
                            String currentDecode = KitchenPrinterCommon.decode(detail.getLineDetail(), "KMS.log");
                            String lastDecode = KitchenPrinterCommon.decode(lastKMSItem.getLineDetail(), "KMS.log");
                            String combined = currentDecode + lastDecode;
                            Logger.logMessage("Setting name to " + combined, "KMS.log", Logger.LEVEL.DEBUG);
                            detail.setLineDetail(KitchenPrinterCommon.encode(combined, "KMS.log"));
                            Logger.logMessage("Removed Detail: " + lastKMSItem.toString(), "KMS.log", Logger.LEVEL.DEBUG);
                            mergedDetails.remove(lastKMSItem);
                        }
                    }
                    lastKMSItem = detail;
                }


                KMSProductModel currKMSItem = null;
                for (QCPrintJobDetail detail : mergedDetails) {
                    int modifierCount = 0;
                    if (detail.getPAPluID() > 0 && !detail.isModifier()){// detail.getDecodedLineDetail().startsWith(KDSJobInfo.PRODUCT_INDICATOR)) {
                        currKMSItem = new KMSProductModel();
                        String productName = detail.getDecodedLineDetail().replace(KDSJobInfo.PRODUCT_INDICATOR, "");
                        if(productName.contains("Dining Option: ")){
                            productName = productName.replace("Dining Option: ", "");
                            currKMSItem.setDiningOption(true);
                        }

                        //break off the quantity from the name
                        productName = removeProductQuantity(productName);

                        String[] parts = productName.split(" - ");
                        if(parts.length > 1){
                            String name = "";
                            String prepOption = parts[parts.length-1];
                            Logger.logMessage("Found prep option: " + prepOption, KMS_LOG, Logger.LEVEL.DEBUG);
                            parts[parts.length-1] = "";
                            for(int i = 0; i < parts.length-1; i++){
                                name += parts[i];
                                if(i < parts.length-2){
                                    name += " - ";
                                }
                            }
                            productName = name;
                            currKMSItem.setPrepOption(prepOption);
                        }
                        productCount++;
                        //TODO: show on expeditor field?
                        if(detail.getPaTransLineItemID() > -1){
                            if(qcPrintJob.isOnlineTrans()){
                                currKMSItem.setTransLineId(detail.getPaTransLineItemID() + "");
                            }else{
                                currKMSItem.setTransLineId(detail.getPaTransLineItemID() + "T" + qcPrintJob.getTerminalID());
                            }

                        }else{
                            currKMSItem.setTransLineId(productCount + "");
                        }
                        currKMSItem.setName(productName);
                        currKMSItem.setQuantity((detail.getQuantity()));

                        //check if the product ID is a weighted product, and if so set the quantity to 1;
                        if(isWeighted(detail.getPAPluID()))
                            currKMSItem.setQuantity((1));

                        if(detail.getKMSOrderStatusID() == 0)
                            currKMSItem.setOrderStatusId(KMSOrderStatus.WAITING);
                        else
                            currKMSItem.setOrderStatusId(detail.getKMSOrderStatusID());
                        currKMSItem.setProductId(detail.getPAPluID());
                        currKMSItem.setModifier(detail.isModifier());
                        currKMSItem.setLinkedFromIDs(detail.getLinkedFromIDs());
                        currKMSItem.printJobDetailId = detail.getPaPrinterQueueDetailID();
                        //TODO: the following fields dont have a field in the KDSItem  yet
//                        currKMSItem.setPrepOption();
//                        currKMSItem.setDiningOption();

                        order.getProducts().add(currKMSItem);
                        //split the products within the order based on what station they are going to.
                        Integer stationMappedTo = detail.getKMSStationID();
//                        if(stationMappedTo != null && stationMappedTo != 0)//not really needed for transfer functionality
//                            currKMSItem.handlingKMSStationId = stationMappedTo + "";
                        //if a mapping for this order exits already,
                        if(stationToOrderMappings != null && stationToOrderMappings.containsKey(stationMappedTo) && stationToOrderMappings.get(stationMappedTo) != null){

                            stationToOrderMappings.get(stationMappedTo).getProducts().add(currKMSItem);
                        }else{
                            //we need to create a duplicate order, without any previously mapped products
                            KMSOrderModel duplicateOrder = new KMSOrderModel();
                            duplicateOrder.originalPrintJob = qcPrintJob;
                            duplicateOrder.setId(order.getId());
                            duplicateOrder.setOrderNumber(order.getOrderNumber());
                            duplicateOrder.setPreviousOrderNumbers(order.getPreviousOrderNumbers());
                            duplicateOrder.setComment(order.getComment());
                            duplicateOrder.setOrderStatusId(order.getOrderStatusId());
                            duplicateOrder.setOrderPriorityId(order.getOrderPriorityId());
                            duplicateOrder.setPersonName(order.getPersonName());
                            duplicateOrder.getProducts().add(currKMSItem);
                            duplicateOrder.setReceivedTime(order.getReceivedTime());
                            if(stationToOrderMappings != null){//we have defined one in the params
                                stationToOrderMappings.put(stationMappedTo, duplicateOrder);
                            }
                        }
                    }
                    else if ((detail.getPAPluID() > 0 && detail.isModifier())){//  detail.getDecodedLineDetail().startsWith(KDSJobInfo.MODIFIER_INDICATOR)) && (currKMSItem != null)) {
                        String modifierName = detail.getDecodedLineDetail().replace(KDSJobInfo.MODIFIER_INDICATOR, "");
                        modifierCount++;
                        KMSModifierModel modifier = new KMSModifierModel();

                        String[] parts = modifierName.split(" - ");
                        if(parts.length > 1){
                            String name = "";
                            String prepOption = parts[parts.length-1];
                            Logger.logMessage("Found prep option: " + prepOption, KMS_LOG, Logger.LEVEL.DEBUG);
                            parts[parts.length-1] = "";
                            for(int i = 0; i < parts.length-1; i++){
                                name += parts[i];
                                if(i < parts.length-2){
                                    name += " - ";
                                }
                            }
                            modifierName = name;
                            modifier.setPrepOption(prepOption);
                        }

                        if(detail.getPaTransLineItemID() > -1){
                            if(qcPrintJob.isOnlineTrans()){
                                modifier.setTransLineId(detail.getPaTransLineItemID() + "");
                            }else{
                                modifier.setTransLineId(detail.getPaTransLineItemID() + "T" + qcPrintJob.getTerminalID());
                            }

                        }else{
                            modifier.setTransLineId(modifierCount + "");
                        }
                        modifier.setProductId(detail.getPAPluID());
                        modifier.setParentProductId(currKMSItem.getProductId());
                        modifier.setName(modifierName);
                        modifier.setLinkedFromIDs(detail.getLinkedFromIDs());
                        modifier.printJobDetailId = detail.getPaPrinterQueueDetailID();
                        currKMSItem.getModifiers().add(modifier);
                    }else{
                        //this line is not a product or a mod
                        Logger.logMessage("Not making KMS Order detail for:  " + detail.toString(), KMS_LOG, Logger.LEVEL.TRACE);
                        //TODO: test if dining options go into here

                    }

                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, KMS_LOG);
            Logger.logMessage("There was a problem trying to convert the QCPrintJob with a print job tracker id of " +
                    Objects.toString(qcPrintJob.getJobTrackerID(), "NULL")+" into a KDSTransaction in " +
                    "KDSUtil.convertQCPrintJobToKDSTransaction", KMS_LOG, Logger.LEVEL.ERROR);
        }
        order.originalPrintJob = qcPrintJob;
        return order;
    }

    /**
     * Removes an order from the status map
     * Used when a satellite asks for the status of a finished order.
     * @param printJobID
     */
    public boolean removeFinishedOrderByPrintJobID(int printJobID){
        String orderID = null;

        for(String key: orderStatusMap.keySet()){
            KMSOrderModel order = orderStatusMap.get(key);
            if(order.originalPrintJob != null && order.originalPrintJob.getPaPrinterQueueID() == printJobID){
                orderID = key;
                break;
            }
        }

        if(orderID != null && orderStatusMap.containsKey(orderID)){
            orderStatusMap.remove(orderID);
            finishedOrders.addFirst(orderID);
            if(finishedOrders.size() > 1000)
                finishedOrders.removeLast();

            return true;
        }
        return false;
    }

    private String getExpeditor(){
        if(expeditorIDs != null && expeditorIDs.size() > 0){
            for(String expID: expeditorIDs){
                if(KMSManagerSocketHandler.isOnline(expID))
                    return expID;
            }
        }
        return null;
    }
    private String getExpeditorOrBackup(){
        if(expeditorIDs != null && expeditorIDs.size() > 0){
            for(String expID: expeditorIDs){
                if(KMSManagerSocketHandler.messageCanBeSent(expID))
                    return expID;
            }
        }
        return null;
    }

    public static String removeProductQuantity(String productName){
        String[] quantityParts = productName.split("X ");
        if(quantityParts.length > 1){
            String rejoinedName = "";
            int i;
            for(i = 1; i < quantityParts.length-1;i++){
                rejoinedName += quantityParts[i] + "X ";
            }
            rejoinedName += quantityParts[i];
            productName = rejoinedName;
        }
        return productName;
    }

    //Handling sending messages to client functions

    /**
     *
     * @param stationID
     * @param order
     * @return
     */
    public boolean sendNewOrderToStation (String stationID, KMSOrderModel order) {
        boolean success = false;

        try {
            String sentTo = KMSManagerSocketHandler.sendMessageToOnlineStationAndMirror(stationID, MMHEvent.KMSEventType.ORDER_RECEIVED, order, null);

            for(KMSProductModel product: order.getProducts()){
                product.handlingKMSStationId = stationID;
            }

            success = (sentTo != null);
            if(!success){
                //there was no online station to receive the order...
                Logger.logMessage("Failed to send new order "+ order.getId() +" to online station " + stationID);
                //remove the stationID from the products
//                for(KMSProductModel product: order.getProducts()){
//                    product.handlingKMSStationId = null;
//                }
            }else{
                String lineItemID = null;
                Integer orderStatus = order.getOrderStatusId();
                //TODO: this wont log it was sent to mirrors
                KMSManager.logEvent(Integer.parseInt(sentTo), order.getId(), lineItemID, MMHEvent.KMSEventType.ORDER_RECEIVED, "Sent order to station", orderStatus);
            }
        }
        catch (Exception e) {
            Logger.logException(e, KMS_LOG);
        }
        return success;
    }

    /**
     * Send an event to the server for the KMSEventLog table. Typically events are logged when a client station sends a message to the manager,
     * But for the Order Received event it is logged when a client confirms receipt of the order previously sent with that Order ID
     * @param stationID
     * @param PATransactionID
     * @param PATransLineItemID
     * @param event
     * @param note
     * @param KMSOrderStatusID
     */
    protected static void logEvent(Integer stationID, String PATransactionID, String PATransLineItemID, String event, String note, Integer KMSOrderStatusID){
        Integer eventID = getEventID(event);
        if(eventID != null){
            String time = new Timestamp(System.currentTimeMillis()).toString();
            KMSEventLogQueueHandler.getInstance().offerToEventLogQueue(new KMSEventLogLine(time, stationID, PATransactionID, PATransLineItemID, eventID, note, KMSOrderStatusID));
        }
    }

    public static Integer getEventID(String event){
        if(eventIDs.size() == 0){
            ArrayList<HashMap> ids =  new DataManager().parameterizedExecuteQuery("data.KMSEvent.getEventIDs", new Object[]{}, true);
            for(HashMap id: ids){
                String eventName = id.get("NAME").toString();
                eventName = eventName.toLowerCase().replaceAll(" ", "-");
                eventIDs.put(eventName, Integer.parseInt(id.get("KMSEVENTTYPEID").toString()));
            }
        }

        return eventIDs.get(event);

    }
    public static String getStatusName(String statusID){
        if(statusNames.size() == 0){
            ArrayList<HashMap> ids =  new DataManager().parameterizedExecuteQuery("data.KMSEvent.getStatusNames", new Object[]{}, true);
            for(HashMap id: ids){
                statusNames.put(Integer.parseInt(id.get("KMSORDERSTATUSID").toString()), id.get("NAME").toString());
            }
        }

        return statusNames.get(statusID);

    }

    public static boolean isWeighted(int PAPLUID){
        Object usesScale =  new DataManager().parameterizedExecuteScalar("data.KMSManager.getProductUsesScale", new Object[]{PAPLUID});
        return (usesScale != null && usesScale.toString().length() > 0 && Integer.parseInt(usesScale.toString()) > 0 );
    }

    public HashMap TESTgetStatus(){
        HashMap hm = new HashMap();
        hm.put("orderStatusMapSize", orderStatusMap.size());
        HashMap<String, KMSOrderModel> orderStatus = new HashMap();
        for(String order: orderStatusMap.keySet()){
            orderStatus.put(order, orderStatusMap.get(order));
        }
        hm.put("orderStatusMap", orderStatus);
        return hm;
    }


    //#region UNIT TEST HELPER METHODS
    public int test_orderCount(){
        return orderStatusMap.size();
    }
    //#endregion
}