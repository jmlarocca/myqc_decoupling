package com.mmhayes.common.kms;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-10-30 13:50:13 -0400 (Fri, 30 Oct 2020) $: Date of last commit
    $Rev: 13005 $: Revision of last commit
    Notes: Houses common methods to be used by KMS code.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;

/**
 * <p>Houses common methods to be used by KMS code.</p>
 *
 */
public class KMSCommon {

    /**
     * <p>Gets a lookup between a KMS station ID and the name of the KMS station that has that ID.</p>
     *
     * @param log The path {@link String} of the file to log any information related to this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @return A {@link HashMap} whose {@link Integer} key is the ID of a KMS station and whose {@link String} value is that name of the KMS station.
     */
    @SuppressWarnings("Convert2streamapi")
    public static HashMap<Integer, String> getKMSStationNameLookup (String log, DataManager dataManager) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KMSCommon.getKMSStationNameLookup can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KMSCommon.getKMSStationNameLookup can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetKMSStationNameLookup").setLogFileName(log);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        HashMap<Integer, String> lookup = new HashMap<>();
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                if (!DataFunctions.isEmptyMap(hm)) {
                    int kmsStationID = HashMapDataFns.getIntVal(hm, "KMSSTATIONID");
                    String name = HashMapDataFns.getStringVal(hm, "NAME");
                    if ((kmsStationID > 0) && (StringFunctions.stringHasContent(name))) {
                        lookup.put(kmsStationID, name);
                    }
                }
            }
        }

        return lookup;
    }

    /**
     * <p>Encodes a {@link String} to base 64.</p>
     *
     * @param s The {@link String} to encode.
     * @return The encoded base 64 {@link String}.
     */
    public static String base64Encode (String s) {

        if (!StringFunctions.stringHasContent(s)) {
            Logger.logMessage("The String passed to KMSCommon.base64Encode can't be null or empty, unable to encode the String, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        return Base64.getEncoder().encodeToString(s.getBytes());
    }

    /**
     * <p>Dencodes a {@link String} to base 64.</p>
     *
     * @param s The {@link String} to dencode.
     * @return The dencoded base 64 {@link String}.
     */
    public static String base64Decode (String s) {

        if (!StringFunctions.stringHasContent(s)) {
            return "";
        }

        return new String(Base64.getDecoder().decode(s));
    }

}