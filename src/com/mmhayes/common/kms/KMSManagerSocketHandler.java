package com.mmhayes.common.kms;

/*
    Last Updated (automatically updated by SVN)
    $Author: crking $: Author of last commit
    $Date: 2020-05-01 17:19:39 -0400 (Fri, 01 May 2020) $: Date of last commit
    $Rev: 11593 $: Revision of last commit
    Notes: Serializes objects to JSON and sends/receives messages from the KMS stations.
*/

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kitchenPrinters.QCPrintJob;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.kms.models.*;
import com.mmhayes.common.kms.orders.models.KMSOrderModel;
import com.mmhayes.common.kms.orders.models.KMSOrphanOrderModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.websockets.MMHEvent;
import com.mmhayes.common.websockets.MMHSocket;
import com.mmhayes.common.websockets.MMHSocketConfig;
import com.mmhayes.common.websockets.MMHSocketMessage;

import java.io.IOException;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import com.mmhayes.common.xmlapi.XmlRpcUtil;

import javax.websocket.CloseReason;
import javax.websocket.OnMessage;
import javax.websocket.server.ServerEndpoint;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Serializes objects to JSON and sends/receives messages from the KMS stations.
 *
 */
@ServerEndpoint(value = "/websocket/station", configurator = MMHSocketConfig.class)
public class KMSManagerSocketHandler extends MMHSocket {
    private static DataManager dm = new DataManager();
    private static final String KMS_LOG = "KMS.log";
    //use a custom set of connections that is NOT the global list of connections. By default, the global list is used.
    //For the KMS Manager we want to send events to the next station in the list and the display stations. For this, we need a few maps...
    //A mapping from the station ID to the current connection
    private static final ConcurrentHashMap<String, MMHSocket> stationIDToConnections = new ConcurrentHashMap<>();

    //model types
    private static final Type orderType = new TypeToken<MMHSocketMessage<KMSOrderModel>>(){}.getType();
    private static final Type ordersType = new TypeToken<MMHSocketMessage<KMSOrderModel[]>>(){}.getType();
    private static final Type statusType = new TypeToken<MMHSocketMessage<KMSStatusModel>>(){}.getType();
    private static final Type stationType = new TypeToken<MMHSocketMessage<KMSStationModel>>(){}.getType();
    private static final Type transferType = new TypeToken<MMHSocketMessage<KMSTransferModel>>(){}.getType();
    private static final Type queryType = new TypeToken<MMHSocketMessage<KMSQueryModel>>(){}.getType();
    private static final Type infoType = new TypeToken<MMHSocketMessage<KMSInfoModel>>(){}.getType();
    private static final Type printType = new TypeToken<MMHSocketMessage<KMSPrintModel>>(){}.getType();
    private static final Type logMessageType = new TypeToken<MMHSocketMessage<JsonObject>>(){}.getType();
    private static final Type triggerConfigUpdateType = new TypeToken<MMHSocketMessage<JsonObject>>(){}.getType();

    public String incomingMessageUUID;
    private StringBuilder partialMessages = new StringBuilder();
    /**
     * Sends the order to one specific station (or the backup for that station)
     * @param model
     * @return true if the order has been sent to the station or an online backup
     */
//region SEND MESSAGE FUNCTIONS

    public static int broadcastMessage(String  originatingStationID, String event, Object model, String status, String reference){
        MMHSocketMessage<Object> message = new MMHSocketMessage();
        message.status = status;
        message.event = event;
        message.model = model;
        message.reference = reference;
//        String onlineID = getOnlineStationID(stationID);

        int numSent = 0;
        for(String stationID : stationIDToConnections.keySet()){
            if(stationIDToConnections.containsKey(stationID) && !stationID.equals(originatingStationID)){
                try{
                    if(forwardTo(stationIDToConnections.get(stationID), message))
                        numSent++;

                }catch (Exception e){Logger.logException(e);}
            }
        }
        return numSent;
    }

    /**
     * Send a message to a single station. Note, does not check that the station is online nor does it lookup any backups
     * @param stationID
     * @param event
     * @param model
     * @return
     */
    public static boolean sendMessageDirectlyToStation(String stationID, String event, Object model, String status, String reference){
        MMHSocketMessage<Object> message = new MMHSocketMessage();
        message.status = status;
        message.event = event;
        message.model = model;
        message.reference = reference;
//        String onlineID = getOnlineStationID(stationID);
        if(stationID != null && stationIDToConnections.containsKey(stationID))
            return forwardTo(stationIDToConnections.get(stationID), message);
        else{
            Logger.logMessage("Can not send message to station " + Objects.toString(stationID, "NULL") + ". Connection doesn't exist", Logger.LEVEL.ERROR);
            return false;
        }
    }
    public static boolean sendMessageDirectlyToStation(String stationID, String event, Object model, String reference){
        return sendMessageDirectlyToStation(stationID, event, model, MMHEvent.Status.OKAY, reference);
    }

    public static boolean sendMessageDirectlyToMirrorsOfStation(String stationID, String event, Object model, String reference){
        return sendMessageToMirrors(stationID, event, model, MMHEvent.Status.OKAY, reference, false) > 0; //send to a whole group of mirrors
    }

    public static boolean sendMessageDirectlyToStationAndMirrors(String stationID, String event, Object model, String reference){
        return sendMessageDirectlyToStationAndMirrors(stationID, event, model, MMHEvent.Status.OKAY, reference);
    }

    public static boolean sendMessageDirectlyToStationAndMirrors(String stationID, String event, Object model, String status, String reference){
        boolean sentToStation = false;
        int sentToMirrors = sendMessageToMirrors(stationID, event, model, status, reference, true);
        if(sentToMirrors == 0)
            sentToStation = sendMessageDirectlyToStation(stationID, event, model, status, reference);//no mirrors were sent to, so attempt sending it to the station instead of the group
        return sentToStation || (sentToMirrors > 0);
    }

    /**
     * Checks listening stations and sends messages to the first online station and online mirror for each listener
     * @param stationID
     * @param event
     * @param model
     * @return the stationID the message was sent to. NULL if it was unable to be sent
     */
    public static String sendMessageToOnlineStationAndMirror(String stationID, String event, Object model, String reference){
        return sendMessageToOnlineStationAndMirror(stationID, event, model, MMHEvent.Status.OKAY, reference);
    }
    public static String sendMessageToOnlineStationAndMirror(String stationID, String event, Object model, String status, String reference){
        //TODO: the mirror SHOULD BE mirrors for the online station actually getting the order
//        String sentTo = null;
//        //send to the first online station
//        String onlineID = getOnlineStationID(stationID);
//        if(sendMessageDirectlyToStation(onlineID, event, model)){
//            sentTo = onlineID;
//        }
//        boolean success = (sentTo != null);
//        if(success){//if we were successful, attempt to send to all mirrors
//            KMSManagerSocketHandler.sendMessageToMirrors(sentTo, event, model, status, true);
//        }


        //we want to send the message to
        String sentTo = null;
        String onlineID = getOnlineStationID(stationID);//look through the backup mappings
        Logger.logMessage("Online Station of "+stationID+" found to be "+onlineID, "KMS.log", Logger.LEVEL.TRACE);
        if(onlineID != null){
            if(KMSManagerSocketHandler.sendMessageToMirrors(onlineID, event, model, status, reference, true) > 0){
                //the message was sent to at least 1 mirror
                sentTo = onlineID;
            }else{
                //there may not be a mirror group for this station, so attempt to send it independently
                Logger.logMessage("Sending to mirrors failed. Sending directly to " + onlineID + " instead", "KMS.log", Logger.LEVEL.TRACE);
                if(sendMessageDirectlyToStation(onlineID, event, model, reference)){
                    sentTo = onlineID;
                }
            }
        }
        return sentTo;
    }

    public static boolean sendMessageToListeningOnlineStationsButNotOriginating(String stationID, String event, Object model, String reference){

        //Mirrors, by definition should be listening to each other
        sendMessageToMirrors(stationID, event, model, MMHEvent.Status.OKAY, reference, false);

        CopyOnWriteArraySet<String> listeners =  KMSManager.stationIDsToListenerIDs.get(stationID);
        if(listeners == null)
            return true;//there are no stations listening to this
        ArrayList<String> failed = new ArrayList<>();
        for(String s: listeners){
            Logger.logMessage("Sending messages for the listener "+s+" of "+stationID);
            if(sendMessageToOnlineListenersAndMirrors(s,stationID,event,model,MMHEvent.Status.OKAY, reference) == null){
                failed.add(s);
            }
        }

        if(failed.size() != 0){
            //TODO: how to handle this?
        }
        //return true if there were no errors
        return failed.size() == 0;
    }
    public static String sendMessageToOnlineListenersAndMirrors(String listenerID, String originatingStationID, String event, Object model, String status, String reference){
        //we want to send the message to
        String sentTo = null;
        String onlineID = getOnlineStationID(listenerID);//look through the backup mappings
        Logger.logMessage("Online Station of "+listenerID+" found to be "+onlineID, "KMS.log", Logger.LEVEL.TRACE);
        if(onlineID != null){
            //send to mirrors
            if(KMSManager.stationIDToMirrorPool.containsKey(listenerID)){
                //the station has mirror(s)
                CopyOnWriteArraySet<String> mirrors = KMSManager.stationIDToMirrorPool.get(listenerID);
                int successful = 0;
                for(String mirrorID: mirrors){
                    if(mirrorID != originatingStationID && mirrorID != null && mirrorID.length() > 0){
                        //mirrors dont have backups, so we should be able to send this directly
                        if(sendMessageDirectlyToStation(mirrorID, event, model, status, reference)){
                            Logger.logMessage("Successfully sent message to a mirror "+mirrorID+" for station " + listenerID + "with message from " + originatingStationID, "KMS.log", Logger.LEVEL.TRACE);
                            successful++;
                        }else{
                            Logger.logMessage("Failed to send message to a mirror "+mirrorID+" for station " + listenerID + "with message from " + originatingStationID, "KMS.log", Logger.LEVEL.ERROR);
                        }
                    }
                }
                if(successful > 0)
                    sentTo = onlineID;
            }else{
                //there may not be a mirror group for this station, so attempt to send it independently
                if(onlineID != originatingStationID){
                    Logger.logMessage("Sending to mirrors failed. Sending directly to " + onlineID + " instead", "KMS.log", Logger.LEVEL.TRACE);
                    if(sendMessageDirectlyToStation(onlineID, event, model, reference)){
                        sentTo = onlineID;
                    }
                }
            }
        }
        return sentTo;
    }

    public static int sendMessageToMirrors(String sentTo, String event, Object model, String status, String reference, boolean sendToReferenceStation){
        //TODO: the mirror SHOULD BE mirrors for the online station actually getting the order
        int successful = 0;
        if(KMSManager.stationIDToMirrorPool.containsKey(sentTo)){
            //the station has mirror(s)
            CopyOnWriteArraySet<String> mirrors = KMSManager.stationIDToMirrorPool.get(sentTo);
            for(String mirrorID: mirrors){
                if( (sendToReferenceStation || mirrorID.compareTo(sentTo) != 0) && mirrorID != null && mirrorID.length() > 0){
                    //mirrors dont have backups, so we should be able to send this directly
                    if(sendMessageDirectlyToStation(mirrorID, event, model, status, reference)){
                        Logger.logMessage("Successfully sent message to a mirror "+mirrorID+" for station " + sentTo, "KMS.log", Logger.LEVEL.TRACE);
                        successful++;
                    }else{
                        Logger.logMessage("Failed to send message to a mirror "+mirrorID+" for station " + sentTo, "KMS.log", Logger.LEVEL.ERROR);
                    }
                }
            }
        }
        return successful;
    }

    public static boolean sendMessageToListeningOnlineStations(String stationID, String event, Object model, String reference){

        //Mirrors, by definition should be listening to each other
        sendMessageToMirrors(stationID, event, model, MMHEvent.Status.OKAY, reference, false);

        CopyOnWriteArraySet<String> listeners =  KMSManager.stationIDsToListenerIDs.get(stationID);
        if(listeners == null)
            return true;//there are no stations listening to this
        ArrayList<String> failed = new ArrayList<>();
        for(String s: listeners){
            Logger.logMessage("Sending messages for the listener "+s+" of "+stationID);
            if(sendMessageToOnlineStationAndMirror(s,event,model,reference) == null){
                failed.add(s);
            }
        }

        if(failed.size() != 0){
            //TODO: how to handle this?
        }
        //return true if there were no errors
        return failed.size() == 0;
    }
//endregion SEND MESSAGE FUNCTIONS

    public static boolean isOnline(String stationID){
        return (stationIDToConnections!= null && stationIDToConnections.get(stationID) != null && stationIDToConnections.get(stationID).getSession().isOpen());
    }
    public static boolean messageCanBeSent(String stationID){
        stationID = getOnlineStationID(stationID);
        if(stationID != null && stationIDToConnections.get(stationID) != null){
            return stationIDToConnections.get(stationID).getSession().isOpen();
        }

        return false;
    }


    /**get the list of active connections.
     * Accounts for
     * @param requestingConnection a message has been sent from this station
     * @return
     */
    @Override
    protected Set<MMHSocket> getConnectionSet(MMHSocket requestingConnection){
        //TODO: will this list ever depend on the message that is going to be sent?
        String stationID = null;
        CopyOnWriteArraySet<MMHSocket> connections = new CopyOnWriteArraySet<>();
        for(String id: stationIDToConnections.keySet()){
            if(stationIDToConnections.containsKey(id) && stationIDToConnections.get(id) != null && stationIDToConnections.get(id).getSession().isOpen())
                connections.add(stationIDToConnections.get(id));
        }
//        if(requestingConnection != null){
//            if(requestingConnection.getSession() != null && requestingConnection.getSession().getUserProperties() != null){
//                Object station = requestingConnection.getSession().getUserProperties().get("STATION");
//                if(station != null) stationID = station.toString();
//            }
//        }
//        if(stationID != null && KMSManager.stationIDsToListenerIDs.get(stationID) != null){//we have a station and other stations to send events to
//            Object[] listeners = KMSManager.stationIDsToListenerIDs.get(stationID).toArray();
//            for(int i = listeners.length-1; i >= 0; i--){
//                String searchingFor = listeners[i].toString();
//                listeners[i] = getOnlineStationID(searchingFor);
//                //listeners[i] should now contain a valid connection. If it does not, that means we could not find any backups....
//                if(listeners[i] != null){
//                    //if there is a mirror for the station that would be receiving the message, add that too
//                    if(KMSManager.stationIDToMirrorPool.containsKey(listeners[i])){
//                        //there are mirror stations for what ever this station is.
//                        //we need to send the message to all of them.
//                        for(String mirrorID: KMSManager.stationIDToMirrorPool.get(listeners[i])){
//                            if(mirrorID != stationID && mirrorID != listeners[i])
//                                connections.add(stationIDToConnections.get(mirrorID));
//                        }
//                    }
////                    if(KMSManager.stationIDToMirrorStationID.containsKey(listeners[i])){
////                        //there is a mirror, check that it's online
////                        if(isOnline(KMSManager.stationIDToMirrorStationID.get(listeners[i]))){
////                            //add the mirror to the list
////                            connections.add(stationIDToConnections.get(KMSManager.stationIDToMirrorStationID.get(listeners[i])));
////                        }
////                    }
////
//                    connections.add(stationIDToConnections.get(listeners[i]));
//                }else{
//                    Logger.logMessage("Failed to find a online connection to station or backups of " + searchingFor, Logger.LEVEL.ERROR);
//                    //TODO: no connection could be found.... what now?
//                }
//            }
//        }
        return connections;
    }

    /**
     * Checks through the backups for the online station
     * @param stationID
     * @return
     */
    public static String getOnlineStationID(String stationID){
        String onlineID = null;
//        if(KMSManager.stationIDToManualBackupStationID.containsKey(stationID)){
//            //a manual backup exists for this station, use it
//            //TODO: are manual stations required to be online, it would make sense?
//            if(stationIDToConnections.containsKey(KMSManager.stationIDToManualBackupStationID.get(stationID))
//                    && stationIDToConnections.get(KMSManager.stationIDToManualBackupStationID.get(stationID)).getSession().isOpen()){
//                return KMSManager.stationIDToManualBackupStationID.get(stationID);
//
//            }else{
//                //the manual backup is offline, remove it
//                KMSManager.stationIDToManualBackupStationID.remove(stationID);
//            }
//
//        }//no manual backup has been specified
//
//        //check that there is a connection for this ID, if not online attempt the automatic backups otherwise the station is online, so just add it
//        if(stationIDToConnections.containsKey(stationID) && stationIDToConnections.get(stationID).getSession().isOpen() ){
//            onlineID = stationID;
//        }
//        String currentID = stationID;
//        int depth = 0; int maxDepth = 50;
//        while(onlineID == null && currentID != null && depth < maxDepth){
//            depth++;
//            if(!stationIDToConnections.containsKey(currentID) || !stationIDToConnections.get(onlineID).getSession().isOpen() ){
//            //there is no connection to that station, so use the backup
//                //TODO: check the mirrors
//
//                //there are no mirrors, or theyre offline, so use the backup
//                if(KMSManager.stationIDToAutomaticBackupStationID.containsKey(currentID)){
//                    //check the next backup
//                    currentID = KMSManager.stationIDToAutomaticBackupStationID.get(currentID);
//                }else{
//                    //we have reached the end of backups...
//                    currentID = null;
//                }
//            }else{
//                //we found our online backup.
//                onlineID = currentID;
//            }
//        }
        long startTime = System.currentTimeMillis();
        onlineID =  getOnlineStationIdRecursively(stationID);
        long endTime = System.currentTimeMillis();
        Logger.logMessage("Find online search took: " + (endTime - startTime) + "ms.", Logger.LEVEL.DEBUG);
        return onlineID;

    }

    public static String getOnlineStationIdRecursively(String stationID){
        //Manual backups have the highest priority
        if(KMSManager.stationIDToManualBackupStationID.containsKey(stationID)){
            //a manual backup exists for this station, use it
            //TODO: are manual stations required to be online, it would make sense?
            if(stationIDToConnections.containsKey(KMSManager.stationIDToManualBackupStationID.get(stationID))
                    && stationIDToConnections.get(KMSManager.stationIDToManualBackupStationID.get(stationID)).getSession().isOpen()){
                return KMSManager.stationIDToManualBackupStationID.get(stationID);

            }else{
//                //the manual backup is offline, go deeper
//                KMSManager.stationIDToManualBackupStationID.remove(stationID);
                String online = getOnlineStationIdRecursively(KMSManager.stationIDToManualBackupStationID.get(stationID));
                if(online != null)
                    return online;
            }

        }//no manual backup has been specified, or nothing has been found down that branch
        //check the base case
        if(stationIDToConnections.containsKey(stationID) && stationIDToConnections.get(stationID).getSession().isOpen() ){
            return stationID;
        }
        //stationID IS NOT ONLINE
        //then check mirrors
        if(KMSManager.stationIDToMirrorPool.containsKey(stationID)){
            //there are mirrors, check if any of them are online, and if so use that
            for(String mirrorID: KMSManager.stationIDToMirrorPool.get(stationID)){
                if(mirrorID != stationID && stationIDToConnections.containsKey(mirrorID) && stationIDToConnections.get(mirrorID).getSession().isOpen()){//we wont check the reference station
                    return mirrorID;//mirrors can not have backups, so no need to go deeper
                }
            }
        }

        //no online mirrors for this station, or none of them are online
        //then check the automatic backups
        if(KMSManager.stationIDToAutomaticBackupStationID.containsKey(stationID)){
            //there is an automatic backup, go deeper to see if its manually backed up
            return getOnlineStationIdRecursively(KMSManager.stationIDToAutomaticBackupStationID.get(stationID));
        }

        return null;
    }


    @Override
    protected void addToConnectionSet(MMHSocket currentConnection){
        if(currentConnection.getSession().getUserProperties().get("STATION") != null){
            stationIDToConnections.put(currentConnection.getSession().getUserProperties().get("STATION").toString(), currentConnection);
        }
        super.addToConnectionSet(currentConnection);
    }

    @Override
    protected void removeFromConnectionSet(MMHSocket currentConnection){
        if(currentConnection.getSession().getUserProperties().get("STATION") != null){
            String stationID = currentConnection.getSession().getUserProperties().get("STATION").toString();
            stationIDToConnections.remove(stationID);
        }
        super.removeFromConnectionSet(currentConnection);
    }

    //This is how we are actually checking that the tokens are correct
    @Override
    protected String validateToken(String token) {
        Logger.logMessage("KMSManagerSocketHandler.validateToken of " + token, KMS_LOG, Logger.LEVEL.TRACE);
        String ID = null;
        try {

            //XMLRPC to the server to check that the DSKey is valid
            //KMSDataManager.validateStationDSKey
            Logger.logMessage("RPC to server for " + "KMSDataManager.validateStationDSKey", Logger.LEVEL.DEBUG);
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, "KMSDataManager.validateStationDSKey",new Object[]{token});

            if(xmlRpcResponse != null)
                ID = xmlRpcResponse.toString();
            else
                Logger.logMessage("KMSManagerSocketHandler.validateToken RPC call to KMSDataManager.validateStationDSKey's response was null" + ID, KMS_LOG, Logger.LEVEL.TRACE);
            Logger.logMessage("Determined connection is Station ID = " + ID, Logger.LEVEL.DEBUG);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }
        Logger.logMessage("KMSManagerSocketHandler.validateToken evaluated to " + ID, KMS_LOG, Logger.LEVEL.TRACE);
        return ID;
    }


    //This method has to be defined in the class with @ServerEndpoint, so that any path params can be passed appropriately.
    @OnMessage
    public void incomingMessage(String message, boolean last) {
        //handle pingpongs
        lastCommTime = System.currentTimeMillis();
        if(message.compareTo("p") == 0){
            try {
                Logger.logMessage("PONG for station's WS", "KMS_PPWC.log", Logger.LEVEL.LUDICROUS);
                session.getBasicRemote().sendText(message);
                return;
            } catch (IOException e) {
                Logger.logException(e, KMS_LOG);
            }
        }
        if(last){
            message = partialMessages.append(message).toString();
            partialMessages.setLength(0);
            if(validateIncomingMessage(message)){
                LogWebsocketMessage(false,false, getStationID(), message);
                handleIncomingMessage(message);
            }
        }else{
            partialMessages.append(message);
        }
    }

    //This function is called after a connection has been authenticated
    @Override
    public void handleConnectionApproved(String stationID) {
        //The the information about this stationID
        ArrayList<HashMap> station = dm.parameterizedExecuteQuery("data.station.getStationData",new Object[]{stationID},true);
        if(station == null || station.size() == 0){
            this.end();//close the connection, we dont have the station data
        }else{
            //cash some station data
            session.getUserProperties().put("STATION", stationID);
            session.getUserProperties().put("NAME", station.get(0).get("NAME"));
            //set the connection to never timeout
            session.setMaxIdleTimeout(60000);
        }
        String authedStationID = session.getUserProperties().get("STATION").toString();
        String name = session.getUserProperties().get("NAME").toString();
        KMSManager.logEvent(Integer.parseInt(stationID),null, null, MMHEvent.KMSEventType.STATION_ONLINE, name + " has connected", null);
        broadcast(this, MMHEvent.Status.OKAY, MMHEvent.KMSEventType.STATION_ONLINE, buildInformationMessage(authedStationID,null, null, name + " has connected"), true);
        Logger.logMessage("KMSManagerSocketHandler.handleConnectionApproved: " + stationID, KMS_LOG, Logger.LEVEL.TRACE);
        sendToClient(MMHEvent.Status.OKAY, MMHEvent.CONFIRMATION,"Connection has been authenticated as " + "Station: " + session.getUserProperties().get("STATION") + " " + session.getUserProperties().get("NAME"));
        sendToClient(MMHEvent.Status.OKAY, MMHEvent.KMSEventType.STATION_ONLINE, buildInformationMessage(authedStationID,null, null, name + " has connected"));

        // send a message to say that any of the unaccepted orphans that would have been sent to this station
        // are now being handled by this station
        Set<KMSOrphanOrderModel> orphanOrdersForStation = KMSManager.getPendingOrphanOrdersByStation(stationID);
        for (KMSOrphanOrderModel orphan : orphanOrdersForStation) {
            KMSInfoModel orphanInfoModel = new KMSInfoModel();
            orphanInfoModel.originatingKMSStationId = stationID;
            orphanInfoModel.originalHandlingKMSStationId = Integer.parseInt(stationID);
            orphanInfoModel.operation = MMHEvent.ORPHANED_ORDER;
            orphanInfoModel.reference = orphan.getId();
            orphanInfoModel.message = "Orphaned order has been adopted.";

            KMSManager.getInstance().receivedOrphanOrder(stationID, orphanInfoModel, "");
        }
    }

    @Override
    public void handleConnectionClosing() {
        //When our client has disconnected, we want to let everyone else know
        String status = MMHEvent.Status.OKAY;
        String event = MMHEvent.KMSEventType.STATION_OFFLINE;
        String stationID = session.getUserProperties().getOrDefault("STATION", "UNKNOWN").toString();
        String name = session.getUserProperties().getOrDefault("NAME", "UNKNOWN").toString();
        Logger.logMessage("KMSManagerSocketHandler.handleConnectionClosing: " + stationID, KMS_LOG, Logger.LEVEL.TRACE);
        if(stationID.compareTo("UNKNOWN") != 0){
            KMSManager.logEvent(Integer.parseInt(stationID),null, null, MMHEvent.KMSEventType.STATION_OFFLINE, name + " has disconnected", null);
            broadcast(this, status, event, buildInformationMessage(stationID,null, null, name + " has disconnected"), true);
        }
    }


    @Override
    public void handleIncomingMessage(String rawMessage) {
        Logger.logMessage("KMSMAnagerSocketTester.handleIncomingMessage incoming message from "+ getStationID() +": " + rawMessage, KMS_LOG, Logger.LEVEL.DEBUG);

        MMHSocketMessage message = new Gson().fromJson(rawMessage, MMHSocketMessage.class);
        //when we get a message from our client, we want to sent it to everyone else

        if(message != null){//the message has been properly formatted

            MMHSocketMessage parsedMessage = null;
            try{
                switch (message.event){
    //                case MMHEvent.AUTHENTICATE:
    //                    //This event is kind of pointless in message parsing
    //                    break;
                    case MMHEvent.KMSEventType.MANUAL_BACKUP:
                        parsedMessage = new Gson().fromJson(rawMessage, stationType);
                        break;
                    case MMHEvent.ORDER_STATUS_UPDATE:
                        parsedMessage = new Gson().fromJson(rawMessage, statusType);
                        break;

                    case MMHEvent.ORDER_ITEM_STATUS_UPDATE:
                        parsedMessage = new Gson().fromJson(rawMessage, statusType);
                        break;
                    case MMHEvent.ORDER_UPLOAD:
                        parsedMessage = new Gson().fromJson(rawMessage, ordersType);
                        break;
                    case MMHEvent.KMSEventType.TRANSFER:
                        parsedMessage = new Gson().fromJson(rawMessage, transferType);
                        break;
                    case MMHEvent.PRINT_ORDER:
                        parsedMessage = new Gson().fromJson(rawMessage, printType);
                        break;
                    case MMHEvent.KMSEventType.ITEM_UPDATE:
                        //TODO: this event is unused
                        break;
                    case MMHEvent.ORPHANED_ORDER:
                        parsedMessage = new Gson().fromJson(rawMessage, infoType);
                        break;
                    case MMHEvent.INQUIRY:
                        parsedMessage = new Gson().fromJson(rawMessage, queryType);
                        break;
                    case MMHEvent.KMSEventType.LOG_MESSAGE:
                        JsonObject stnMsg1 = new Gson().fromJson(rawMessage, JsonObject.class);
                        KMSFrontEndLogger.logMessage(stnMsg1);
                        // parse the message to ensure we can still access the message reference
                        parsedMessage = new Gson().fromJson(rawMessage, logMessageType);
                        break;
                    case MMHEvent.TRIGGER_CONFIG_UPDATE:
                        parsedMessage = new Gson().fromJson(rawMessage, triggerConfigUpdateType);
                        break;
                    case "TEST":
                        parsedMessage = new MMHSocketMessage();
                        parsedMessage.event = "TEST";
                        break;
                    default:
                        Type hashMapType = new TypeToken<MMHSocketMessage<HashMap<String, Object>>>(){}.getType();
                        MMHSocketMessage<HashMap<String, Object>> defaultMessage = new Gson().fromJson(rawMessage, hashMapType);

                }
                Logger.logMessage("KMSManagerSocketHandler.handleIncomingMessage: determined message of type" + message.event, KMS_LOG, Logger.LEVEL.TRACE);
                incomingMessageUUID = parsedMessage.reference;
            }catch (Exception e){
                Logger.logException(e);
            }finally {
                if(parsedMessage == null){
                    sendErrorMessage(getStationID(),message.event, null,"Message JSON failed to be parsed.", null);
                    return;
                }
            }
            try{
                switch (message.event){
    //                case MMHEvent.AUTHENTICATE:
    //                    //This event is kind of pointless in message parsing
    //                    break;
                    case MMHEvent.KMSEventType.MANUAL_BACKUP:
                        MMHSocketMessage<KMSStationModel> stationMessage = parsedMessage;
                        String stationBeingBackedUp = stationMessage.model.getKmsStationID() + "";
                        KMSManager.getInstance().backupManually(getStationID(), stationBeingBackedUp, stationMessage.reference);
                        break;
                    case MMHEvent.ORDER_STATUS_UPDATE:
                        MMHSocketMessage<KMSStatusModel> orderStatusMessage = parsedMessage;
                        KMSManager.getInstance().updateOrderStatus(getStationID(), orderStatusMessage.model, getName() + " sent order status update of " + orderStatusMessage.model.KMSOrderStatusID, orderStatusMessage.reference);
                        break;

                    case MMHEvent.ORDER_ITEM_STATUS_UPDATE:
                        MMHSocketMessage<KMSStatusModel> itemStatusMessage = parsedMessage;
                        KMSManager.getInstance().updateOrderProductStatus(getStationID(), itemStatusMessage.model, getName() + " sent item status update of " + itemStatusMessage.model.KMSOrderStatusID, itemStatusMessage.reference);
                        break;
                    case MMHEvent.ORDER_UPLOAD:
                        MMHSocketMessage<KMSOrderModel[]> orderStateMessage = parsedMessage;
                        KMSManager.getInstance().uploadOrdersFromStation(getStationID(), orderStateMessage.model, orderStateMessage.reference);
                        KMSManagerSocketHandler.sendMessageToListeningOnlineStationsButNotOriginating(getStationID(), MMHEvent.ORDER_UPLOAD, orderStateMessage.model, orderStateMessage.reference);
                        break;
                    case MMHEvent.KMSEventType.TRANSFER:
                        MMHSocketMessage<KMSTransferModel> transferMessage = parsedMessage;
                        Logger.logMessage("DEBUG LINE: Transfer Reference in event case" + transferMessage.reference, Logger.LEVEL.DEBUG);
                        //handle the "ALL" case
                        if(transferMessage.model.PATransactionID != null && transferMessage.model.PATransactionID.compareToIgnoreCase("ALL") == 0){
                            //get all of the orders IDs for a specific station...
                            //loop through them and transfer every one
                            KMSManager.getInstance().transferALL(getStationID(), transferMessage.model, transferMessage.reference);
                        }else{
                            KMSManager.getInstance().transfer(getStationID(), transferMessage.model, transferMessage.reference);
                        }
                        break;
                    case MMHEvent.PRINT_ORDER:
                        MMHSocketMessage<KMSPrintModel> printMessage = parsedMessage;
                        KMSManager.getInstance().printOrder(getStationID(), printMessage.model, printMessage.reference);
                        break;
                    case MMHEvent.KMSEventType.ITEM_UPDATE:
                        //TODO: what to do when the contents of an order change?
                        break;
                    case MMHEvent.ORPHANED_ORDER:
                        MMHSocketMessage<KMSInfoModel> orphanMessage = parsedMessage;
                        KMSManager.getInstance().receivedOrphanOrder(getStationID(), orphanMessage.model, orphanMessage.reference);
                        break;
                    case MMHEvent.INQUIRY:
                        MMHSocketMessage<KMSQueryModel> queryMessage = parsedMessage;
                        KMSManager.getInstance().query(getStationID(), queryMessage.model, queryMessage.reference);
                        break;
                    case MMHEvent.KMSEventType.LOG_MESSAGE:
                        // do nothing - we have already logged the message
                        break;
                    case MMHEvent.TRIGGER_CONFIG_UPDATE:
                        KMSManagerSocketHandler.broadcastMessage("-1", MMHEvent.TRIGGER_CONFIG_UPDATE, null, MMHEvent.Status.OKAY, null);
                        break;
                    //TODO: remove this
                    case "TEST":
                        //Inspect what config we have with a breakpoint here
//                        ConcurrentHashMap<String, CopyOnWriteArraySet<String>> stationIDsToListenerIDs = KMSManager.stationIDsToListenerIDs;
//                        ConcurrentHashMap<String,String> stationIDToManualBackupStationID = KMSManager.stationIDToManualBackupStationID;
//                        ConcurrentHashMap<String,String> stationIDToAutomaticBackupStationID = KMSManager.stationIDToAutomaticBackupStationID;
//                        ConcurrentHashMap<String,String> stationIDToMirrorStationID = KMSManager.stationIDToMirrorStationID;
//                        ConcurrentHashMap<String, MMHSocket> connections = stationIDToConnections;
//                        //get the actual type of the message
//                        Type modelType = new TypeToken<MMHSocketMessage<KMSStyleProfileModel>>(){}.getType();
//                        MMHSocketMessage<KMSStyleProfileModel> styleMessage = new Gson().fromJson(rawMessage, modelType);
//                        KMSStyleProfileModel style = styleMessage.model;
//                        testModelSending();
//                        String data = System.currentTimeMillis() +"";
//                        ByteBuffer payload = ByteBuffer.wrap(data.getBytes());
//                        session.getBasicRemote().sendPing(payload); //a pong should be sent back to the MMHSocket OnMessage handler
                        //Queue a new transaction to be sent
                        //new KMSDataManager().printOrderUsingKMSInOfflineMode(7690419);
//                        new KMSDataManager().printOrderUsingKMSInOnlineMode(7690419);
                        sendToClient(message);
                    default:
                        Type hashMapType = new TypeToken<MMHSocketMessage<HashMap<String, Object>>>(){}.getType();
                        MMHSocketMessage<HashMap<String, Object>> defaultMessage = new Gson().fromJson(rawMessage, hashMapType);

                }
            }catch(Exception e){
                Logger.logException(e);
                String event = null;
                String reference = null;
                if(message != null){
                    event = message.event;
                    reference = message.reference;
                }
                String errorMsg = "Internal Event handler error. ";
                if(e != null && e.getMessage() != null)
                     errorMsg += e.getMessage();
                sendErrorMessage(getStationID(), event, null, errorMsg, reference);
            }
        }else{
            sendErrorMessage(getStationID(), null, null, "Message wrapper failed to be parsed.", null);
        }
    }

    private String getStationID(){
        String stationID = this.getSession().getUserProperties().get("STATION").toString();
        return stationID;
    }
    private String getName(){
        String name = this.getSession().getUserProperties().get("NAME").toString();
        return name;
    }

    public static void sendErrorMessage(String stationID, String failedOperation, String modelReference, String reason, String reference){
        Logger.logMessage("Sending ERROR: originatingKMSStationID:" + stationID + " operation:" + failedOperation + " reference:" + reference + " message:" + reason, KMS_LOG, Logger.LEVEL.TRACE);
        KMSManagerSocketHandler.sendMessageDirectlyToStation(stationID, MMHEvent.CONFIRMATION,//yes, this should be a confirmation
                KMSManagerSocketHandler.buildInformationMessage(
                        stationID,
                        failedOperation,
                        modelReference,
                        reason),
                MMHEvent.Status.ERROR, reference);
    }
    public static void sendConfirmationMessage(String stationID, String operation, String modelReference, String reason, String reference){
        Logger.logMessage("Sending CONFIRMATION: originatingKMSStationID:" + stationID + " operation:" + operation + " reference:" + reference + " message:" + reason, KMS_LOG, Logger.LEVEL.TRACE);

        KMSManagerSocketHandler.sendMessageDirectlyToStationAndMirrors(stationID, MMHEvent.CONFIRMATION,
                KMSManagerSocketHandler.buildInformationMessage(
                        stationID,
                        operation,
                        modelReference,
                        reason), reference);
    }

    public static HashMap buildInformationMessage(String stationID, String operation, String reference, String message){
        HashMap hm = new HashMap();
        if(stationID != null)
            hm.put("originatingKMSStationID", stationID);
        if(operation != null)
            hm.put("operation", operation);
        if(reference != null)
            hm.put("reference", reference);
        if(message != null)
            hm.put("message", message);
        return hm;
    }

    //Handling messages from client functions

    //Handling sending messages to client functions

    public static ArrayList<String> getOnlineStationIDs(){
        ArrayList onlineStations = new ArrayList();
        for(String id: stationIDToConnections.keySet()){
            if(stationIDToConnections.containsKey(id) && stationIDToConnections.get(id) != null && stationIDToConnections.get(id).getSession().isOpen())
                onlineStations.add(id);
        }
        return onlineStations;
    }

    public static void closeAllConnections(KMSCloseReason closeReason){
        Logger.logMessage("KMSManagerSocketHandler.closeAllConnections", KMS_LOG, Logger.LEVEL.TRACE);
        //tell the frontend that all the connections are closing. Frontend should search for new printer host.
        CloseReason reason = closeReason.getCloseReason();
        for(String stationID : stationIDToConnections.keySet()){
            try {
                if(stationID != null && stationIDToConnections.containsKey(stationID) && stationIDToConnections.get(stationID) != null){
                    if(stationIDToConnections.get(stationID).getSession() != null && stationIDToConnections.get(stationID).getSession().isOpen())
                        stationIDToConnections.get(stationID).getSession().close(reason);
                }
            } catch (IOException e) {
                Logger.logMessage("Failed to cleanly close connection to " + stationID, KMS_LOG, Logger.LEVEL.ERROR);
            }
        }
    }

    //TODO: remove this later
    private void testModelSending(){

        MMHSocketMessage t1 = new MMHSocketMessage();
        t1.status = "TEST";
//        t1.event = MMHEvent.CONFIRMATION;
//        KMSOrderModel orderModel = new KMSOrderModel();
//        t1.model = orderModel;
//        sendToClient(t1);
//        t1.model = new KMSOrderTimerProfileDetailModel();
//        sendToClient(t1);
//        t1.model = new KMSOrderStatusProfileModel();
//        sendToClient(t1);
//        t1.model = new KMSOrderTimerProfileDetailModel();
//        sendToClient(t1);
//        t1.model = new KMSOrderTimerProfileModel();
//        sendToClient(t1);
//        t1.model = new KMSStationModel();
//        sendToClient(t1);
//        t1.model = new KMSStyleProfileModel();
//        sendToClient(t1);
//        t1.event = MMHEvent.CONFIGURATION_UPDATE;
//        t1.model = new KMSUpdateConfigModel("2019-09-11 13:44:57.000");
//        sendToClient(t1);
        t1.event = "TEST_MANAGER_STATUS";
        HashMap hm = KMSManager.getInstance().TESTgetStatus();
        t1.model = hm;

        sendToClient(t1);

        t1.event = MMHEvent.ORPHANED_ORDER;
        t1.model = new KMSOrderModel();
        sendToClient(t1);

        t1.event = MMHEvent.CONFIRMATION;
        t1.model = buildInformationMessage("20", MMHEvent.ORPHANED_ORDER,"ABC123TransactionID", "Some Text");
        sendToClient(t1);


        t1.event = MMHEvent.INQUIRY;
        HashMap hm2 = new HashMap();
        ArrayList onlineStations = new ArrayList();

        for(String id: stationIDToConnections.keySet()){
            if(stationIDToConnections.containsKey(id) && stationIDToConnections.get(id) != null && stationIDToConnections.get(id).getSession().isOpen())
                onlineStations.add(id);
        }
        hm2.put("onlineStations", onlineStations.toArray());
        t1.model = hm2;
        sendToClient(t1);

        //testing station lookup
//        sendMessageDirectlyToStation("20", MMHEvent.KMSEventType.ORDER_RECEIVED, new KMSOrderModel());
    }

    private void TESTSendingQCPrintJob(){
        QCPrintJob pj = new QCPrintJob();
        pj.setPersonName("");
    }
}