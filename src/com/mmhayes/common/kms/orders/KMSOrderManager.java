package com.mmhayes.common.kms.orders;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-08 12:03:37 -0400 (Fri, 08 May 2020) $: Date of last commit
    $Rev: 11675 $: Revision of last commit
    Notes: Handles communications with POS through XML-RPC.
*/

import com.mmhayes.common.utils.Logger;
import redstone.xmlrpc.handlers.ReflectiveInvocationHandler;

/**
 * <p>Handles communications with POS through XML-RPC.</p>
 *
 */
public class KMSOrderManager extends ReflectiveInvocationHandler {

    public boolean testCall () {
        Logger.logMessage("I GOT CALLED!!!!", Logger.LEVEL.IMPORTANT);
        return true;
    }

}