package com.mmhayes.common.kms.orders.models;

/*
    Last Updated (automatically updated by SVN)
    $Author: crking $: Author of last commit
    $Date: 2020-07-30 17:21:45 -0400 (Thu, 30 Jul 2020) $: Date of last commit
    $Rev: 12274 $: Revision of last commit
    Notes: Object representation of an orphaned product within a KMS order.
*/

public class KMSOrphanProductModel extends KMSProductModel {

    private int originalHandlingStationId;
    private String originalHandlingStationName;

    /**
     * <p>Constructor for a KMSOrphanProductModel.</p>
     *
     */
    public KMSOrphanProductModel () {
        super();
    }


    /**
     * <p>Constructor for a KMSOrphanProductModel.</p>
     *
     * @param originalHandlingStationID The ID of the station the product was originally sent to.
     * @param originalHandlingStationName The name {@link String} of the station the product was originally sent to.
     */
    public KMSOrphanProductModel (int originalHandlingStationID, String originalHandlingStationName) {
        this.setOriginalHandlingStationID(originalHandlingStationID);
        this.setOriginalHandlingStationName(originalHandlingStationName);
    }

    /**
     * <p>Constructor for a KMSOrphanProductModel.</p>
     *
     * @param originalHandlingStationID The ID of the station the product was originally sent to.
     * @param originalHandlingStationName The name {@link String} of the station the product was originally sent to.
     * @param originalProductModel The product model to use when creating this orphan product model.
     */
    public KMSOrphanProductModel (int originalHandlingStationID, String originalHandlingStationName, KMSProductModel originalProductModel) {
        super();

        this.setOriginalHandlingStationID(originalHandlingStationID);
        this.setOriginalHandlingStationName(originalHandlingStationName);

        this.setTransLineId(originalProductModel.getTransLineId());
        this.setProductId(originalProductModel.getProductId());
        this.setName(originalProductModel.getName());
        this.setPrepOption(originalProductModel.getPrepOption());
        this.setQuantity(originalProductModel.getQuantity());
        this.setDiningOption(originalProductModel.isDiningOption());
        this.setModifier(originalProductModel.isModifier());
        this.setOrderStatusId(originalProductModel.getOrderStatusId());
        this.setModifiers(originalProductModel.getModifiers());
        this.setLinkedFromIDs(originalProductModel.getLinkedFromIDs());

        this.printJobDetailId = originalProductModel.printJobDetailId;
        this.handlingKMSStationId = originalProductModel.handlingKMSStationId;
    }

    /**
     * <p>Getter for the originatingStationID field of the {@link KMSOrphanProductModel}.</p>
     *
     * @return The originatingStationID field of the {@link KMSOrphanProductModel}.
     */
    public int getOriginalHandlingStationID () {
        return originalHandlingStationId;
    }

    /**
     * <p>Setter for the originatingStationID field of the {@link KMSOrphanProductModel}.</p>
     *
     * @param originalHandlingStationId The originatingStationID field of the {@link KMSOrphanProductModel}.
     */
    public void setOriginalHandlingStationID (int originalHandlingStationId) {
        this.originalHandlingStationId = originalHandlingStationId;
    }

    /**
     * <p>Getter for the originatingStationName field of the {@link KMSOrphanProductModel}.</p>
     *
     * @return The originatingStationName field of the {@link KMSOrphanProductModel}.
     */
    public String getOriginalHandlingStationName () {
        return originalHandlingStationName;
    }

    /**
     * <p>Setter for the originatingStationName field of the {@link KMSOrphanProductModel}.</p>
     *
     * @param originalHandlingStationName The originatingStationName field of the {@link KMSOrphanProductModel}.
     */
    public void setOriginalHandlingStationName (String originalHandlingStationName) {
        this.originalHandlingStationName = originalHandlingStationName;
    }

}
