package com.mmhayes.common.kms.orders.models;

/*
    Last Updated (automatically updated by SVN)
    $Author: crking $: Author of last commit
    $Date: 2020-07-30 17:21:45 -0400 (Thu, 30 Jul 2020) $: Date of last commit
    $Rev: 12274 $: Revision of last commit
    Notes: Object representation of a modifier on it's own or assigned to a parent product within a KMS order.
*/

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * <p>Object representation of a modifier on it's own or assigned to a parent product within a KMS order.</p>
 *
 */
public class KMSModifierModel {

    // private member variables of a KMSModifierModel
    private String transLineId = "";
    private int productId = 0;
    private int parentProductId = 0;
    private String name = "";
    private String prepOption = "";
    private String linkedFromIDs = null;

    @JsonIgnore
    public transient int printJobDetailId = 0;

    public KMSModifierModel(){

    }

    public KMSModifierModel(String transLineId, int productId, int parentProductId, String name, String prepOption){
        this.transLineId = transLineId;
        this.productId = productId;
        this.parentProductId = parentProductId;
        this.name = name;
        this.prepOption = prepOption;
    }

    public String getTransLineId() {
        return transLineId;
    }

    public void setTransLineId(String paTransLineItemID) {
        this.transLineId = paTransLineItemID;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getParentProductId() {
        return parentProductId;
    }

    public void setParentProductId(int parentProductId) {
        this.parentProductId = parentProductId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrepOption() {
        return prepOption;
    }

    public void setPrepOption(String prepOption) {
        this.prepOption = prepOption;
    }

    public String getLinkedFromIDs() {
        return linkedFromIDs;
    }

    public void setLinkedFromIDs(String linkedFromIDs) {
        this.linkedFromIDs = linkedFromIDs;
    }
}