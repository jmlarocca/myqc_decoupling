package com.mmhayes.common.kms.orders.models;

/*
    Last Updated (automatically updated by SVN)
    $Author: crking $: Author of last commit
    $Date: 2020-07-30 17:21:45 -0400 (Thu, 30 Jul 2020) $: Date of last commit
    $Rev: 12274 $: Revision of last commit
    Notes: Object representation of a product within a KMS order.
*/

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * <p>Object representation of a product within a KMS order.</p>
 *
 */
public class KMSProductModel {

    // private member variables of a KMSProductModel
//    private BigDecimal paTransLineItemID = null;
    private String transLineId;
    private int productId = 0;
    private String name = "";
    private String prepOption = "";
    private double quantity = 0.0d;
    private boolean isDiningOption = false;
    private boolean isModifier = false;
    private int orderStatusId = 0;
    private ArrayList<KMSModifierModel> modifiers = null;
    private String linkedFromIDs = null;
    @JsonIgnore
    public transient int printJobDetailId = 0;
//    @JsonIgnore
    public transient String handlingKMSStationId;

//    public BigDecimal getPaTransLineItemID() {
//        return paTransLineItemID;
//    }
//
//    public void setPaTransLineItemID(BigDecimal paTransLineItemID) {
//        this.paTransLineItemID = paTransLineItemID;
//    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrepOption() {
        return prepOption;
    }

    public void setPrepOption(String prepOption) {
        this.prepOption = prepOption;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public boolean isDiningOption() {
        return isDiningOption;
    }

    public void setDiningOption(boolean diningOption) {
        isDiningOption = diningOption;
    }

    public boolean isModifier() {
        return isModifier;
    }

    public void setModifier(boolean modifier) {
        isModifier = modifier;
    }

    public int getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(int orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public ArrayList<KMSModifierModel> getModifiers() {
        if(modifiers == null)
            modifiers = new ArrayList<>();
        return modifiers;
    }

    public void setModifiers(ArrayList<KMSModifierModel> modifiers) {
        this.modifiers = modifiers;
    }

    public String getTransLineId() {
        return transLineId;
    }

    public void setTransLineId(String transLineId) {
        this.transLineId = transLineId;
    }

    public String getLinkedFromIDs() {
        return linkedFromIDs;
    }

    public void setLinkedFromIDs(String linkedFromIDs) {
        this.linkedFromIDs = linkedFromIDs;
    }
}