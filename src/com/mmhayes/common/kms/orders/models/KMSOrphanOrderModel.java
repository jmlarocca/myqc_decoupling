package com.mmhayes.common.kms.orders.models;

import java.util.ArrayList;

public class KMSOrphanOrderModel extends KMSOrderModel {

    private int originalHandlingStationID;
    private String originalHandlingStationName;
    private ArrayList<KMSOrphanProductModel> orphanProducts = null;

    /**
     * <p>Constructor for a KMSOrphanOrderModel.</p>
     *
     */
    public KMSOrphanOrderModel() {
        super();
    }

    /**
     * <p>Constructor for a KMSOrphanOrderModel.</p>
     *
     *
     * @param originalHandlingStationID The ID of the station the product was originally sent to.
     * @param originalHandlingStationName The name {@link String} of the station the product was originally sent to.
     * @param originalOrderModel The order model to use when creating this orphan order model.
     */
    public KMSOrphanOrderModel (int originalHandlingStationID, String originalHandlingStationName, KMSOrderModel originalOrderModel) {
        super();

        this.setOriginalHandlingStationID(originalHandlingStationID);
        this.setOriginalHandlingStationName(originalHandlingStationName);

        this.setId(originalOrderModel.getId());
        this.setOrderNumber(originalOrderModel.getOrderNumber());
        this.setOrderStatusId(originalOrderModel.getOrderStatusId());
        this.setOrderPriorityId(originalOrderModel.getOrderPriorityId());
        this.setComment(originalOrderModel.getComment());
        this.setPersonName(originalOrderModel.getPersonName());

        // TODO abstract class or interface to prevent requirement for both products instance variables
        this.setProducts(originalOrderModel.getProducts());
        this.setOrphanProducts(originalOrderModel.getProducts());

        this.setReceivedTime(originalOrderModel.getReceivedTime());
        this.setPreviousOrderNumbers(originalOrderModel.getPreviousOrderNumbers());

        this.originalPrintJob = originalOrderModel.originalPrintJob;
    }

    /**
     * <p>Getter for the originatingStationID field of the {@link KMSOrphanOrderModel}.</p>
     *
     * @return The originatingStationID field of the {@link KMSOrphanOrderModel}.
     */
    public int getOriginalHandlingStationID () {
        return originalHandlingStationID;
    }


    /**
     * <p>Setter for the originatingStationID field of the {@link KMSOrphanOrderModel}.</p>
     *
     * @param originalHandlingStationID The originatingStationID field of the {@link KMSOrphanOrderModel}.
     */
    public void setOriginalHandlingStationID (int originalHandlingStationID) {
        this.originalHandlingStationID = originalHandlingStationID;
    }

    /**
     * <p>Getter for the originatingStationName field of the {@link KMSOrphanOrderModel}.</p>
     *
     * @return The originatingStationName field of the {@link KMSOrphanOrderModel}.
     */
    public String getOriginalHandlingStationName () {
        return originalHandlingStationName;
    }

    /**
     * <p>Setter for the originatingStationName field of the {@link KMSOrphanOrderModel}.</p>
     *
     * @param originalHandlingStationName The originatingStationName field of the {@link KMSOrphanOrderModel}.
     */
    public void setOriginalHandlingStationName (String originalHandlingStationName) {
        this.originalHandlingStationName = originalHandlingStationName;
    }

    /**
     * <p>Setter for the products field of the {@link KMSOrphanOrderModel}.
     * Converts given KMSProductModel products to KMSOrphanProductModel products.</p>
     *
     * @param products The products field of the {@link KMSOrphanOrderModel}.
     */
    public void setOrphanProducts (ArrayList<KMSProductModel> products) {
        ArrayList<KMSOrphanProductModel> orphanProducts = new ArrayList<>();

        for (KMSProductModel product : products) {
            KMSOrphanProductModel orphanProduct =
                    new KMSOrphanProductModel(getOriginalHandlingStationID(), getOriginalHandlingStationName(), product);

            orphanProducts.add(orphanProduct);
        }

        this.orphanProducts = orphanProducts;
    }

    public ArrayList<KMSOrphanProductModel> getOrphanProducts() {
        return this.orphanProducts;
    }
}
