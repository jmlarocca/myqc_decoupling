package com.mmhayes.common.kms.orders.models;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-04-21 13:06:58 -0400 (Wed, 21 Apr 2021) $: Date of last commit
    $Rev: 13830 $: Revision of last commit
    Notes: Object representation of a KMS order.
*/

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.SerializedName;
import com.mmhayes.common.kitchenPrinters.QCPrintJob;
import com.mmhayes.common.kms.types.KMSOrderStatus;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;

/**
 * Object representation of a KMS order.
 *
 */
public class KMSOrderModel {

    // private member variables of a KMSOrderModel
    private String id = "";
    private String orderNumber = "";
    private int orderStatusId = 0;
    private int orderPriorityId = 0;
    private String comment = "";
    private String personName = "";
    private ArrayList<KMSProductModel> products = null;
    private String receivedTime = null;
    private String previousOrderNumbers = null;

    @SerializedName("printJob")
    public QCPrintJob originalPrintJob;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(int orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public int getOrderPriorityId() {
        return orderPriorityId;
    }

    public void setOrderPriorityId(int orderPriorityId) {
        this.orderPriorityId = orderPriorityId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ArrayList<KMSProductModel> getProducts() {
        if(products == null)
            products = new ArrayList<>();
        return products;
    }

    public void setProducts(ArrayList<KMSProductModel> products) {
        this.products = products;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(String receivedTime) {
        this.receivedTime = receivedTime;
    }

    public String getPreviousOrderNumbers() {
        return previousOrderNumbers;
    }

    public void setPreviousOrderNumbers(String previousOrderNumbers) {
        this.previousOrderNumbers = previousOrderNumbers;
    }

    /**
     * All products start as waiting and get updated to in progess and completed
     * @return
     */
    public Integer getOrderStatusBasedOnProductStatuses(){
        String KMS_LOG = "KMS.log";
        boolean hasIN_PROGRESS = false;
        boolean hasCOMPLETED = false;
        boolean hasRECEIVED = false;
        boolean hasCANCELED = false;
        boolean hasSUSPENDED = false;
        boolean hasFINALIZED = false;
        boolean hasWAITING = false;
        boolean hasSENT = false;
        boolean hasERROR = false;
        boolean hasREPLACED = false;

        boolean hasOther = false;
        Logger.logMessage("Initial hasRECEIVED:" + hasRECEIVED, KMS_LOG, Logger.LEVEL.TRACE);
        Logger.logMessage("Products of length: " + this.getProducts().size(), KMS_LOG, Logger.LEVEL.TRACE);
        for(KMSProductModel product: this.getProducts()){
            switch(product.getOrderStatusId()){
                case KMSOrderStatus.RECEIVED :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is RECEIVED", KMS_LOG, Logger.LEVEL.TRACE);
                    hasRECEIVED = true;
                    Logger.logMessage("Set hasRECEIVED = true", KMS_LOG, Logger.LEVEL.TRACE);
                    break;
                case KMSOrderStatus.IN_PROGRESS :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is IN_PROGRESS", KMS_LOG, Logger.LEVEL.TRACE);
                    hasIN_PROGRESS = true;
                    break;
                case KMSOrderStatus.COMPLETED :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is COMPLETED", KMS_LOG, Logger.LEVEL.TRACE);
                    hasCOMPLETED = true;
                    break;
                case KMSOrderStatus.CANCELED :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is CANCELED", KMS_LOG, Logger.LEVEL.TRACE);
                    hasCANCELED = true;
                    break;
                case KMSOrderStatus.SUSPENDED :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is SUSPENDED", KMS_LOG, Logger.LEVEL.TRACE);
                    hasSUSPENDED = true;
                    break;
                case KMSOrderStatus.FINALIZED :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is FINALIZED", KMS_LOG, Logger.LEVEL.TRACE);
                    hasFINALIZED = true;
                    break;
                case KMSOrderStatus.WAITING :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is WAITING", KMS_LOG, Logger.LEVEL.TRACE);
                    hasWAITING = true;
                    break;
                case KMSOrderStatus.SENT :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is SENT", KMS_LOG, Logger.LEVEL.TRACE);
                    hasSENT = true;
                    break;
                case KMSOrderStatus.ERROR :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is ERROR", KMS_LOG, Logger.LEVEL.TRACE);
                    hasERROR = true;
                    break;
                case KMSOrderStatus.REPLACED :
                    Logger.logMessage("Product " + product.getTransLineId() + " " + product.getName() + " is REPLACED", KMS_LOG, Logger.LEVEL.TRACE);
                    hasREPLACED = true;
                    break;
            }
        }
        Logger.logMessage("hasRECEIVED:" + hasRECEIVED, KMS_LOG, Logger.LEVEL.TRACE);
        hasOther = (hasRECEIVED || hasCANCELED || hasSUSPENDED || hasFINALIZED || hasWAITING || hasSENT || hasERROR);
        Logger.logMessage("hasRECEIVED:" + hasRECEIVED, KMS_LOG, Logger.LEVEL.TRACE);
        //log what the order has
        String msg = "";
        if(hasRECEIVED)    msg += " RECEIVED";
        if(hasIN_PROGRESS) msg += " IN_PROGRESS";
        if(hasCOMPLETED)   msg += " COMPLETED";
        if(hasCANCELED)    msg += " CANCELED"; //items dont have this status
        if(hasSUSPENDED)   msg += " SUSPENDED";//items dont have this status
        if(hasFINALIZED)   msg += " FINALIZED";//items dont have this status
        if(hasWAITING)     msg += " WAITING";
        if(hasSENT)        msg += " SENT";
        if(hasERROR)       msg += " ERROR";
        if(hasREPLACED)    msg += " REPLACED";
        Logger.logMessage("Determined order has products of status: " + msg, KMS_LOG, Logger.LEVEL.TRACE);

        //if everything in the order is completed, its completed
        if(hasCOMPLETED && !hasIN_PROGRESS && !hasOther) return KMSOrderStatus.COMPLETED;
        //if something is in progress or something is completed(but not everything), its in progress
        if(hasIN_PROGRESS || hasCOMPLETED) return KMSOrderStatus.IN_PROGRESS;
        //if nothing is in progress and nothing is completed, its received
        if(hasOther)return KMSOrderStatus.RECEIVED;
        return KMSOrderStatus.WAITING;
    }

    public ArrayList<KMSProductModel> getProductsByHandlingStationId(int stationId) {
        ArrayList<KMSProductModel> handledProducts = new ArrayList<>();

        for (KMSProductModel product : this.getProducts()) {
            if (StringFunctions.stringHasContent(product.handlingKMSStationId) &&
                Integer.parseInt(product.handlingKMSStationId) == stationId) {
                handledProducts.add(product);
            }
        }

        return handledProducts;
    }
}