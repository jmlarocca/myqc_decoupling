package com.mmhayes.common.kms.orders;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-07 16:26:59 -0400 (Thu, 07 May 2020) $: Date of last commit
    $Rev: 11667 $: Revision of last commit
    Notes: Enum to hold the names of methods in KMSOrderManager so they may easily called through XML-RPC.
*/

/**
 * <p>Enum to hold the names of methods in KMSOrderManager so they may easily called through XML-RPC.</p>
 *
 */
public enum KMSOrderManagerMethod {

}