package com.mmhayes.common.kms.receiptGen;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-06-19 10:43:14 -0400 (Fri, 19 Jun 2020) $: Date of last commit
    $Rev: 12031 $: Revision of last commit
    Notes: Produces a kitchen receipt factory to build a kitchen receipt of a certain type.
*/

import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.Objects;

/**
 * <p>Produces a kitchen receipt factory to build a kitchen receipt of a certain type.</p>
 *
 */
public class KitchenReceiptFactoryProducer {

    /**
     * <p>Gets a factory capable of producing the desired receipt type.</p>
     *
     * @param kitchenReceiptType The desired kitchen receipt type {@link String}.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return The {@link AbstractKitchenReceiptFactory} that will produce the desired kitchen receipt type.
     */
    public static AbstractKitchenReceiptFactory getFactory (String kitchenReceiptType, String log) {

        if (!StringFunctions.stringHasContent(kitchenReceiptType)) {
            Logger.logMessage("No kitchen receipt type has been passed to KitchenReceiptFactoryProducer.getFactory!", log, Logger.LEVEL.ERROR);
            return null;
        }

        switch (kitchenReceiptType) {
            case KitchenReceiptType.KP:
                return new KPReceiptFactory();
            case KitchenReceiptType.KDS:
                return new KDSReceiptFactory();
            case KitchenReceiptType.KMS:
                return new KMSReceiptFactory();
            default:
                Logger.logMessage(String.format("Encountered an invalid kitchen receipt type of %s in KitchenReceiptFactoryProducer.getFactory!",
                        Objects.toString(kitchenReceiptType, "N/A")), log, Logger.LEVEL.ERROR);
                return null;
        }

    }

}