package com.mmhayes.common.kms.receiptGen;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-06-19 10:43:14 -0400 (Fri, 19 Jun 2020) $: Date of last commit
    $Rev: 12031 $: Revision of last commit
    Notes: Interface that should be implemented by all kitchen receipts.
*/

import java.util.ArrayList;
import java.util.HashMap;

/**
 * <p>Interface that should be implemented by all kitchen receipts.</p>
 *
 */
public interface KitchenReceipt {

    /**
     * <p>Builds a receipt for the {@link KitchenReceipt} instance.</p>
     *
     * @param args A variable number of {@link Object} arguments to pass to the method.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print job details for the receipt.
     */
    ArrayList<HashMap> buildReceipt (Object ... args);

}