package com.mmhayes.common.kms.receiptGen;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-04-26 10:59:25 -0400 (Mon, 26 Apr 2021) $: Date of last commit
    $Rev: 13847 $: Revision of last commit
    Notes: Contains helper methods for generating a kitchen receipt.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.kms.KMSManager;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionData;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionLineItem;
import com.mmhayes.common.kms.types.KMSOrderStatus;
import com.mmhayes.common.kmsapi.KMSDataManagerMethod;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.xmlapi.XmlRpcUtil;
import org.apache.commons.lang.math.NumberUtils;

import java.util.*;

/**
 * <p>Contains helper methods for generating a kitchen receipt.</p>
 *
 */
public class KitchenReceiptHelper {

    /**
     * <p>Iterates through the line items in the transactions and determines their simple print status.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param transLineItemTrees An {@link ArrayList} of {@link MMHTree} of {@link TransactionLineItem} corresponding to the line items within the transaction.
     * @return A {@link HashMap} whose {@link TransactionLineItem} key is a transaction line item and whose {@link String} value contains the simple print status
     * for the transaction line item.
     */
    @SuppressWarnings({"Convert2streamapi", "Duplicates"})
    public static HashMap<TransactionLineItem, String> getSimplePrintStatusOfTransLineItems (String log, DataManager dataManager, ArrayList<MMHTree<TransactionLineItem>> transLineItemTrees) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getSimplePrintStatusOfTransLineItems can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid data manager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.getSimplePrintStatusOfTransLineItems can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure there are valid transaction line items
        if (DataFunctions.isEmptyCollection(transLineItemTrees)) {
            Logger.logMessage("The transaction line items passed to KitchenReceiptHelper.getSimplePrintStatusOfTransLineItems can't be null or empty!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // determine whether a transaction line item may have been printed previously or not
        ArrayList<MMHTree<TransactionLineItem>> possiblePreviouslyPrintedTransLineItems = new ArrayList<>();
        HashMap<TransactionLineItem, String> simplePrintStatusLookup = new HashMap<>();
        for (MMHTree<TransactionLineItem> productLineItemTree : transLineItemTrees) {
            if ((productLineItemTree != null) && (productLineItemTree.getData() != null)) {
                TransactionLineItem productLineItem = productLineItemTree.getData();
                if (!StringFunctions.stringHasContent(productLineItem.getLinkedFromIDs())) {
                    // the product couldn't have possibly been printed previously
                    setSimplePrintStatusForTransLineItemTree(log, productLineItemTree, TypeData.SimplePrintStatus.UNPRINTED, simplePrintStatusLookup);
                }
                else {
                    // add the product to the list of products that could have possibly been printed previously
                    possiblePreviouslyPrintedTransLineItems.add(productLineItemTree);
                }
            }
        }

        // get all the linked from transaction line item IDs
        HashSet<Integer> allLinkedFromIDs = new HashSet<>();
        if (!DataFunctions.isEmptyCollection(possiblePreviouslyPrintedTransLineItems)) {
            for (MMHTree<TransactionLineItem> productLineItemTree : transLineItemTrees) {
                if ((productLineItemTree != null) && (productLineItemTree.getData() != null)) {
                    TransactionLineItem productLineItem = productLineItemTree.getData();
                    String[] linkedFromIDsArr = productLineItem.getLinkedFromIDs().split("\\s*,\\s*", -1);
                    if (linkedFromIDsArr.length == 1) {
                        if (NumberUtils.isNumber(linkedFromIDsArr[0])) {
                            allLinkedFromIDs.add(Integer.parseInt(linkedFromIDsArr[0]));
                        }
                    }
                    else if (linkedFromIDsArr.length > 1) {
                        for (String s : linkedFromIDsArr) {
                            if ((StringFunctions.stringHasContent(s)) && (NumberUtils.isNumber(s))) {
                                allLinkedFromIDs.add(Integer.parseInt(s));
                            }
                        }
                    }
                }
            }
        }

        // query the database to try and find previous print queue details for line items within the transaction
        if (!DataFunctions.isEmptyCollection(allLinkedFromIDs)) {
            DynamicSQL sql = new DynamicSQL("data.kms.GetSimplePrintStatusLookup").addIDList(1, allLinkedFromIDs).setLogFileName(log);
            ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                // determine whether or not the transaction line items have been previously printed or not
                for (MMHTree<TransactionLineItem> possiblePreviouslyPrintedTransLineItemTree : possiblePreviouslyPrintedTransLineItems) {
                    if ((possiblePreviouslyPrintedTransLineItemTree != null) && (possiblePreviouslyPrintedTransLineItemTree.getData() != null)) {
                        TransactionLineItem possiblyPrintedProduct = possiblePreviouslyPrintedTransLineItemTree.getData();
                        String pppLinkedFromIDs = possiblyPrintedProduct.getLinkedFromIDs();
                        int pppPAPluID = possiblyPrintedProduct.getPAPluID();
                        boolean wasPrinted = false;
                        for (HashMap hm : queryRes) {
                            int hmPATransLineItemID = HashMapDataFns.getIntVal(hm, "PATRANSLINEITEMID");
                            int hmPAPluID = HashMapDataFns.getIntVal(hm, "PAPLUID");
                            int hmPrintStatusID = HashMapDataFns.getIntVal(hm, "PRINTSTATUSID");
                            int hmKMSOrderStatusID = HashMapDataFns.getIntVal(hm, "KMSORDERSTATUSID");
                            if ((pppLinkedFromIDs.contains(String.valueOf(hmPATransLineItemID)))
                                    && (pppPAPluID == hmPAPluID)
                                    && ((hmPrintStatusID == PrintStatusType.DONE)
                                    || ((hmKMSOrderStatusID == KMSOrderStatus.RECEIVED)
                                    || (hmKMSOrderStatusID == KMSOrderStatus.IN_PROGRESS)
                                    || (hmKMSOrderStatusID == KMSOrderStatus.COMPLETED)
                                    || (hmKMSOrderStatusID == KMSOrderStatus.CANCELED)
                                    || (hmKMSOrderStatusID == KMSOrderStatus.SUSPENDED)
                                    || (hmKMSOrderStatusID == KMSOrderStatus.FINALIZED)
                                    || (hmKMSOrderStatusID == KMSOrderStatus.SENT)))) {
                                wasPrinted = true;
                                break;
                            }
                        }
                        if (wasPrinted) {
                            setSimplePrintStatusForTransLineItemTree(log, possiblePreviouslyPrintedTransLineItemTree, TypeData.SimplePrintStatus.PRINTED, simplePrintStatusLookup);
                        }
                        else {
                            setSimplePrintStatusForTransLineItemTree(log, possiblePreviouslyPrintedTransLineItemTree, TypeData.SimplePrintStatus.UNPRINTED, simplePrintStatusLookup);
                        }
                    }
                }
            }
            else {
                // no print queue details were found so the transaction line items haven't been printed yet
                for (MMHTree<TransactionLineItem> possiblePreviouslyPrintedTransLineItemTree : possiblePreviouslyPrintedTransLineItems) {
                    if ((possiblePreviouslyPrintedTransLineItemTree != null) && (possiblePreviouslyPrintedTransLineItemTree.getData() != null)) {
                        simplePrintStatusLookup.put(possiblePreviouslyPrintedTransLineItemTree.getData(), TypeData.SimplePrintStatus.UNPRINTED);
                    }
                }
            }
        }

        return simplePrintStatusLookup;
    }

    /**
     * <p>Sets the simple print status for the given transaction line item tree.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param transLineItemTree A {@link MMHTree} of {@link TransactionLineItem} corresponding to a product and any modifiers it has within the transaction.
     * @param sps The simple print status {@link String} to set for the transaciton line item.
     * @param spsLookup A {@link HashMap} whose {@link TransactionLineItem} key is a transaction line item and whose {@link String} value contains the simple print status
     * for the transaction line item.
     */
    @SuppressWarnings("Convert2streamapi")
    private static void setSimplePrintStatusForTransLineItemTree (String log, MMHTree<TransactionLineItem> transLineItemTree, String sps, HashMap<TransactionLineItem, String> spsLookup) {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.setSimplePrintStatusForTransLineItemTree can't be null or empty!", Logger.LEVEL.ERROR);
            return;
        }

        // make sure the transaction line item tree is valid
        if (transLineItemTree == null) {
            Logger.logMessage("The transaction line item tree passed to KitchenReceiptHelper.setSimplePrintStatusForTransLineItemTree can't be null or empty!", log, Logger.LEVEL.ERROR);
            return;
        }

        // make sure the simple print status is valid
        if (!StringFunctions.stringHasContent(sps)) {
            Logger.logMessage("The simple print status passed to KitchenReceiptHelper.setSimplePrintStatusForTransLineItemTree can't be null or empty!", log, Logger.LEVEL.ERROR);
            return;
        }
        else if ((StringFunctions.stringHasContent(sps)) && (!sps.equalsIgnoreCase(TypeData.SimplePrintStatus.UNPRINTED)) && (!sps.equalsIgnoreCase(TypeData.SimplePrintStatus.PRINTED))) {
            Logger.logMessage(String.format("The simple print status %s, passed to KitchenReceiptHelper.setSimplePrintStatusForTransLineItemTree isn't a valid print status!",
                    Objects.toString(sps, "N/A")), log, Logger.LEVEL.ERROR);
            return;
        }

        // make sure the simple print status lookup is valid
        if (spsLookup == null) {
            Logger.logMessage("The simple print status lookup passed to KitchenReceiptHelper.setSimplePrintStatusForTransLineItemTree can't be null!", log, Logger.LEVEL.ERROR);
            return;
        }

        // set the simple print status of the product
        if (transLineItemTree.getData() != null) {
            spsLookup.put(transLineItemTree.getData(), sps);
        }

        // set the simple print status of any modifiers
        if (!DataFunctions.isEmptyCollection(transLineItemTree.getChildren())) {
            for (MMHTree<TransactionLineItem> modTree : transLineItemTree.getChildren()) {
                if ((modTree != null) && (modTree.getData() != null)) {
                    spsLookup.put(modTree.getData(), sps);
                }
            }
        }
    }

    /**
     * <p>Checks whether or not the order numbers wrapped passed the maximum order number allowed in the revenue center.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param transactionData The {@link TransactionData} from the transaction.
     * @return Whether or not the order numbers wrapped passed the maximum order number allowed in the revenue center.
     */
    public static boolean didOrderNumbersWrap (String log, DataManager dataManager, TransactionData transactionData) {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.didOrderNumbersWrap can't be null or empty!", Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the DataManager is valid
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.didOrderNumbersWrap can't be null!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the transaction data is valid
        if (transactionData == null) {
            Logger.logMessage("The transaction data passed to KitchenReceiptHelper.didOrderNumbersWrap can't be null!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the transaction header data is valid
        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("The transaction header data passed to KitchenReceiptHelper.didOrderNumbersWrap can't be null!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // get any previous order numbers within the transaction
        int paTransactionID = transactionData.getPATransactionID();
        String[] previousOrderNumbers = getPreviousOrderNumbersInTransaction(log, dataManager, paTransactionID);

        if (DataFunctions.isEmptyGenericArr(previousOrderNumbers)) {
            Logger.logMessage(String.format("No the previous order numbers were found within the transaction with an ID of %s.",
                    Objects.toString(paTransactionID, "N/A")), log, Logger.LEVEL.TRACE);
            return false;
        }
        else {
            Logger.logMessage(String.format("Found the previous order numbers %s within the transaction with an ID of %s.",
                    Objects.toString(StringFunctions.buildStringFromGenericArr(previousOrderNumbers, ","), "N/A"),
                    Objects.toString(paTransactionID, "N/A")), log, Logger.LEVEL.TRACE);

            int startOrderNumber = transactionData.getTransactionHeader().getStartOrderNumber();
            int maxOrderNumber = transactionData.getTransactionHeader().getMaxOrderNumber();
            MMHPair<MMHRange, MMHRange> orderNumWrapRange = getOrderNumberWrapRange(log, startOrderNumber, maxOrderNumber);
            if ((orderNumWrapRange != null) && (orderNumWrapRange.getKey() != null) && (orderNumWrapRange.getValue() != null)) {
                MMHRange lowerRange = orderNumWrapRange.getKey();
                MMHRange upperRange = orderNumWrapRange.getValue();
                // check if there is a order number within the lower range, include the current order number too
                boolean orderNumberInLowerRange = false;
                ArrayList<String> previousOrderNumberStrs = new ArrayList<>(Arrays.asList(previousOrderNumbers));
                String currentOrderNumber = "";
                if (NumberUtils.isNumber(transactionData.getTransactionHeader().getOrderNumber())) {
                    currentOrderNumber = transactionData.getTransactionHeader().getOrderNumber();
                }
                else {
                    String[] tokens = transactionData.getTransactionHeader().getOrderNumber().split("\\-", -1);
                    if ((!DataFunctions.isEmptyGenericArr(tokens)) && (tokens.length == 2) && (NumberUtils.isNumber(tokens[1]))) {
                        currentOrderNumber = tokens[1];
                    }
                }
                if (StringFunctions.stringHasContent(currentOrderNumber)) {
                    previousOrderNumberStrs.add(currentOrderNumber);
                }
                for (String previousOrderNumberStr : previousOrderNumberStrs) {
                    // in case of an online order, split the order number before parsing it
                    String[] splitOrderNumber = previousOrderNumberStr.split("-");
                    int previousOrderNumber = Integer.parseInt(splitOrderNumber[splitOrderNumber.length - 1]);
                    if (lowerRange.containsInclusive(previousOrderNumber)) {
                        orderNumberInLowerRange = true;
                        break;
                    }
                }
                // check if there is a order number within the upper range
                boolean orderNumberInUpperRange = false;
                for (String previousOrderNumberStr : previousOrderNumbers) {
                    // in case of an online order, split the order number before parsing it
                    String[] splitOrderNumber = previousOrderNumberStr.split("-");
                    int previousOrderNumber = Integer.parseInt(splitOrderNumber[splitOrderNumber.length - 1]);
                    if (upperRange.containsInclusive(previousOrderNumber)) {
                        orderNumberInUpperRange = true;
                        break;
                    }
                }

                if ((orderNumberInLowerRange) && (orderNumberInUpperRange)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * <p>Determines any previous order numbers within the transaction with the given ID.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param paTransactionID The ID of the transaction.
     * @return A {@link String} array containing the previous order numbers within the transaction.
     */
    @SuppressWarnings("unchecked")
    public static String[] getPreviousOrderNumbersInTransaction (String log, DataManager dataManager, int paTransactionID) {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getPreviousOrderNumbersInTransaction can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the DataManager is valid
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.getPreviousOrderNumbersInTransaction can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the transaction ID is valid
        if (paTransactionID <= 0) {
            Logger.logMessage("The transaction ID passed to KitchenReceiptHelper.getPreviousOrderNumbersInTransaction must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // get previous order numbers within the transaction
        String[] previousOrderNumbers = null;
        String previousOrderNumbersStr = "";

        //first attempt to get the previous orders locally, then if they can not be found, XMLRPC
        // make sure the transaction ID is valid
        if (paTransactionID <= 0) {
            previousOrderNumbersStr = "";
        }else{

            // query the database
            ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(dataManager.parameterizedExecuteQuery("data.kms.GetPreviousOrderNumbersInTransaction", new Object[]{paTransactionID}, "", true));
            if (!DataFunctions.isEmptyCollection(queryRes)) {
                ArrayList<String> previousOrderNumberList = new ArrayList<>();
                for (HashMap hm : queryRes) {
                    String previousOrderNumber = HashMapDataFns.getStringVal(hm, "ORDERNUM");
                    if (StringFunctions.stringHasContent(previousOrderNumber)) {
                        previousOrderNumberList.add(previousOrderNumber);
                    }
                }

                if (!DataFunctions.isEmptyCollection(previousOrderNumberList)) {
                    Logger.logMessage(String.format("Found the previous order numbers %s, within the transaction with an ID of %s in KMSDataManager.getPreviousOrderNumbersInTransaction.",
                            Objects.toString(StringFunctions.buildStringFromList(previousOrderNumberList, ",") , "N/A"),
                            Objects.toString(paTransactionID, "N/A")), "", Logger.LEVEL.TRACE);
                    previousOrderNumbersStr = StringFunctions.buildStringFromList(previousOrderNumberList, ",");
                }
                else {
                    Logger.logMessage(String.format("No previous order numbers were found within the transaction with an ID of %s in KMSDataManager.getPreviousOrderNumbersInTransaction.",
                            Objects.toString(paTransactionID, "N/A")), "", Logger.LEVEL.TRACE);
                    previousOrderNumbersStr = "";
                }
            }
            else {
                Logger.logMessage(String.format("No previous order numbers were found within the transaction with an ID of %s in KMSDataManager.getPreviousOrderNumbersInTransaction.",
                        Objects.toString(paTransactionID, "N/A")), "", Logger.LEVEL.TRACE);
                previousOrderNumbersStr = "";
            }
        }

        if(previousOrderNumbersStr.compareTo("") == 0 && XmlRpcUtil.getInstance() != null){
            // make XML RPC call to the server to get the previous order numbers in the transaction
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            String method = KMSDataManagerMethod.GET_PREV_ORDER_NUMS_IN_TXN;
            Object[] args = new Object[]{paTransactionID};
            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, method, args);
            if ((xmlRpcResponse != null)
                    && (StringFunctions.stringHasContent(xmlRpcResponse.toString()))
                    && (!xmlRpcResponse.toString().equalsIgnoreCase("invalid session"))
                    && (!xmlRpcResponse.toString().equalsIgnoreCase("invalid terminal"))) {
                previousOrderNumbersStr = xmlRpcResponse.toString();
            }
        }

        if (StringFunctions.stringHasContent(previousOrderNumbersStr)) {
            previousOrderNumbers = previousOrderNumbersStr.split("\\s*,\\s*", -1);
        }

        return previousOrderNumbers;
    }

    /**'
     * <p>Determines the ranges in which order numbers would have to be within to have exceeded the maximum order number.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param startOrderNumber The starting order number within the revenue center in which the transaction took place.
     * @param maxOrderNumber The maximum order number within the revenue center in which the transaction took place.
     * @return A {@link MMHPair} whose key is a {@link MMHRange} is for the lower range and whose value is a {@link MMHRange} for the upper range.
     */
    private static MMHPair<MMHRange, MMHRange> getOrderNumberWrapRange (String log, int startOrderNumber, int maxOrderNumber) {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getOrderNumberWrapRange can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the starting order number is valid
        if (startOrderNumber <= 0) {
            Logger.logMessage("The starting order number passed to KitchenReceiptHelper.getOrderNumberWrapRange must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the maximum order number is valid
        if (maxOrderNumber <= 0) {
            Logger.logMessage("The maximum order number passed to KitchenReceiptHelper.getOrderNumberWrapRange must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        int rangeSize = Math.round(maxOrderNumber / 10);
        MMHRange lowerRange = new MMHRange(startOrderNumber, (startOrderNumber + rangeSize));
        MMHRange upperRange = new MMHRange((maxOrderNumber - rangeSize), maxOrderNumber);

        return MMHPair.of(lowerRange, upperRange);
    }

    /**
     * <p>Checks whether or not remote orders should be sent to the expeditor within the revenue center.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param revenueCenterID The ID of the revenue center to check whether or not remote orders should be sent to the expeditor for.
     * @return Whether or not remote orders should be sent to the expeditor within the revenue center.
     */
    public static boolean remoteOrdersUseExpeditor (String log, DataManager dataManager, int revenueCenterID) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.remoteOrdersUseExpeditor can't be null or empty!", Logger.LEVEL.ERROR);
            return false;
        }

        // make sure we have a valid data manager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.remoteOrdersUseExpeditor can't be null!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure we have a valid revenue center ID
        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KitchenReceiptHelper.remoteOrdersUseExpeditor must be greater than 0!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetRemOrdUseExpInRC").addIDList(1, revenueCenterID).setLogFileName(log);
        return sql.getSingleBooleanField(dataManager);
    }

    /**
     * <p>Checks whether or not an expeditor device is defined within the revenue center.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param revenueCenterID The ID of the revenue center to check whether or not an expeditor device is defined within.
     * @return The ID of an expeditor device within the revenue center.
     */
    public static int getExpeditorDevice (String log, DataManager dataManager, int revenueCenterID) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getExpeditorDevice can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid data manager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.getExpeditorDevice can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid revenue center ID
        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KitchenReceiptHelper.getExpeditorDevice must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetExpInRC").addIDList(1, revenueCenterID).setLogFileName(log);
        return sql.getSingleIntField(dataManager);
    }

    /**
     * <p>Checks whether or not an expeditor kitchen printer is defined within the revenue center.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param revenueCenterID The ID of the revenue center to check whether or not an expeditor kitchen printer is defined within.
     * @return The printer ID of an expeditor kitchen printer within the revenue center.
     */
    public static int getKPExpeditor (String log, DataManager dataManager, int revenueCenterID) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getKPExpeditor can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid data manager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.getKPExpeditor can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid revenue center ID
        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KitchenReceiptHelper.getKPExpeditor must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // get the printer ID of the expeditor device, it's still unknown whether or not it is for KP or KDS
        int expeditorDeviceID = getExpeditorDevice(log, dataManager, revenueCenterID);
        if (expeditorDeviceID <= 0) {
            Logger.logMessage(String.format("No expeditor device has been found within the revenue center with an ID of %s " +
                    "in KitchenReceiptHelper.getKPExpeditor, no kitchen printer expeditor could be found.",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
            return -1;
        }

        // check if the expeditor device is a kitchen printer
        DynamicSQL sql = new DynamicSQL("data.kms.GetIsPrinterKP").addIDList(1, expeditorDeviceID).setLogFileName(log);
        if (sql.getSingleBooleanField(dataManager)) {
            return expeditorDeviceID;
        }
        else {
            Logger.logMessage(String.format("The expeditor device ID in KitchenReceiptHelper.getKPExpeditor which is a printer ID of %s is not for a kitchen printer.",
                    Objects.toString(expeditorDeviceID, "N/A")), log, Logger.LEVEL.TRACE);
            return -1;
        }
    }

    /**
     * <p>Checks whether or not an expeditor KDS station is defined within the revenue center.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param revenueCenterID The ID of the revenue center to check whether or not an expeditor KDS station is defined within.
     * @return The printer ID of an expeditor KDS station within the revenue center.
     */
    public static int getKDSExpeditor (String log, DataManager dataManager, int revenueCenterID) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getKDSExpeditor can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid data manager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.getKDSExpeditor can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid revenue center ID
        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KitchenReceiptHelper.getKDSExpeditor must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // get the printer ID of the expeditor device, it's still unknown whether or not it is for KP or KDS
        int expeditorDeviceID = getExpeditorDevice(log, dataManager, revenueCenterID);
        if (expeditorDeviceID <= 0) {
            Logger.logMessage(String.format("No expeditor device has been found within the revenue center with an ID of %s " +
                    "in KitchenReceiptHelper.getKDSExpeditor, no KDS expeditor station could be found.",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
            return -1;
        }

        // check if the expeditor device is a KDS station
        DynamicSQL sql = new DynamicSQL("data.kms.GetIsPrinterKDS").addIDList(1, expeditorDeviceID).setLogFileName(log);
        if (sql.getSingleBooleanField(dataManager)) {
            return expeditorDeviceID;
        }
        else {
            Logger.logMessage(String.format("The expeditor device ID in KitchenReceiptHelper.getKDSExpeditor which is a printer ID of %s is not for a KDS station.",
                    Objects.toString(expeditorDeviceID, "N/A")), log, Logger.LEVEL.TRACE);
            return -1;
        }
    }

    /**
     * <p>Checks whether or not an expeditor KMS station is defined within the revenue center.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param revenueCenterID The ID of the revenue center to check whether or not an expeditor KMS station is defined within.
     * @return The KMS station ID of an expeditor KMS station within the revenue center.
     */
    public static int getKMSExpeditor (String log, DataManager dataManager, int revenueCenterID) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getKMSExpeditor can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid data manager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.getKMSExpeditor can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid revenue center ID
        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KitchenReceiptHelper.getKMSExpeditor must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // get the ID of the KMS expeditor station if it is present
        DynamicSQL sql = new DynamicSQL("data.kms.getExpeditorStationForRC").addIDList(1, revenueCenterID).setLogFileName(log);
        int queryRes = sql.getSingleIntField(dataManager);
        if (queryRes > 0) {
            Logger.logMessage(String.format("Found the expeditor KMS station with a KMS station ID of %s in KitchenReceiptHelper.getKMSExpeditor within the revenue center with an ID of %s.",
                    Objects.toString(queryRes, "N/A"),
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
            return queryRes;
        }
        else {
            Logger.logMessage(String.format("No expeditor KMS station was found in KitchenReceiptHelper.getKMSExpeditor within the revenue center with an ID of %s.",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
            return -1;
        }
    }

    /**
     * <p>Checks whether or not a remote order device is defined within the revenue center.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param revenueCenterID The ID of the revenue center to check whether or not a remote order device is defined within.
     * @return The ID of a remote order device within the revenue center.
     */
    public static int getRemoteOrderDevice (String log, DataManager dataManager, int revenueCenterID) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getRemoteOrderDevice can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid data manager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.getRemoteOrderDevice can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid revenue center ID
        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KitchenReceiptHelper.getRemoteOrderDevice must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetRemOrdInRC").addIDList(1, revenueCenterID).setLogFileName(log);
        return sql.getSingleIntField(dataManager);
    }

    /**
     * <p>Checks whether or not a remote order kitchen printer is defined within the revenue center.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param revenueCenterID The ID of the revenue center to check whether or not a remote order kitchen printer is defined within.
     * @return The printer ID of a remote order kitchen printer within the revenue center.
     */
    public static int getRemoteOrderKP (String log, DataManager dataManager, int revenueCenterID) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getRemoteOrderKP can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid data manager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.getRemoteOrderKP can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid revenue center ID
        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KitchenReceiptHelper.getRemoteOrderKP must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // get the printer ID of the remote order device, it's still unknown whether or not it is for KP or KDS
        int remoteOrderDeviceID = getRemoteOrderDevice(log, dataManager, revenueCenterID);
        if (remoteOrderDeviceID <= 0) {
            Logger.logMessage(String.format("No remote order device has been found within the revenue center with an ID of %s " +
                    "in KitchenReceiptHelper.getRemoteOrderKP, no remote order kitchen printer could be found.",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
            return -1;
        }

        // check if the remote order device is a kitchen printer
        DynamicSQL sql = new DynamicSQL("data.kms.GetIsPrinterKP").addIDList(1, remoteOrderDeviceID).setLogFileName(log);
        if (sql.getSingleBooleanField(dataManager)) {
            return remoteOrderDeviceID;
        }
        else {
            Logger.logMessage(String.format("The remote order device ID in KitchenReceiptHelper.getRemoteOrderKP which is a printer ID of %s is not for a kitchen printer.",
                    Objects.toString(remoteOrderDeviceID, "N/A")), log, Logger.LEVEL.TRACE);
            return -1;
        }
    }

    /**
     * <p>Checks whether or not a remote order KDS station is defined within the revenue center.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param revenueCenterID The ID of the revenue center to check whether or not a remote order KDS station is defined within.
     * @return The printer ID of a remote order KDS station within the revenue center.
     */
    public static int getRemoteOrderKDS (String log, DataManager dataManager, int revenueCenterID) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getRemoteOrderKDS can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid data manager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.getRemoteOrderKDS can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid revenue center ID
        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KitchenReceiptHelper.getRemoteOrderKDS must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // get the printer ID of the remote order device, it's still unknown whether or not it is for KDS or KDS
        int remoteOrderDeviceID = getRemoteOrderDevice(log, dataManager, revenueCenterID);
        if (remoteOrderDeviceID <= 0) {
            Logger.logMessage(String.format("No remote order device has been found within the revenue center with an ID of %s " +
                    "in KitchenReceiptHelper.getRemoteOrderKDS, no remote order KDS station could be found.",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
            return -1;
        }

        // check if the remote order device is a KDS station
        DynamicSQL sql = new DynamicSQL("data.kms.GetIsPrinterKDS").addIDList(1, remoteOrderDeviceID).setLogFileName(log);
        if (sql.getSingleBooleanField(dataManager)) {
            return remoteOrderDeviceID;
        }
        else {
            Logger.logMessage(String.format("The remote order device ID in KitchenReceiptHelper.getRemoteOrderKDS which is a printer ID of %s is not for a KDS station.",
                    Objects.toString(remoteOrderDeviceID, "N/A")), log, Logger.LEVEL.TRACE);
            return -1;
        }
    }

    /**
     * <p>Checks whether or not a remote order KMS station is defined within the revenue center.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param revenueCenterID The ID of the revenue center to check whether or not a remote order KMS station is defined within.
     * @return The KMS station ID of a remote order KMS station within the revenue center.
     */
    public static int getRemoteOrderKMS (String log, DataManager dataManager, int revenueCenterID) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getRemoteOrderKMS can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid data manager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.getRemoteOrderKMS can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid revenue center ID
        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KitchenReceiptHelper.getRemoteOrderKMS must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // get the ID of the remote order KMS station if it is present
        DynamicSQL sql = new DynamicSQL("data.kms.getRemoteOrderStationForRC").addIDList(1, revenueCenterID).setLogFileName(log);
        int queryRes = sql.getSingleIntField(dataManager);
        if (queryRes > 0) {
            Logger.logMessage(String.format("Found the remote order KMS station with a KMS station ID of %s in KitchenReceiptHelper.getRemoteOrderKMS within the revenue center with an ID of %s.",
                    Objects.toString(queryRes, "N/A"),
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
            return queryRes;
        }
        else {
            Logger.logMessage(String.format("No remote order KMS station was found in KitchenReceiptHelper.getRemoteOrderKMS within the revenue center with an ID of %s.",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
            return -1;
        }
    }

    /**
     * <p>Iterates through the line items in the transaction in search of a dining option.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param transactionData The {@link TransactionData} to search for a dining option within.
     * @return A {@link MMHTree} of a {@link TransactionLineItem} corresponding to the fining option within the transaction.
     */
    public static MMHTree<TransactionLineItem> getDiningOptionTransLineItem (String log, TransactionData transactionData) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getDiningOptionTransLineItem can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have valid transaction data
        if (transactionData == null) {
            Logger.logMessage("The TransactionData passed to KitchenReceiptHelper.getDiningOptionTransLineItem can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure there are line items within the transaction
        ArrayList<MMHTree<TransactionLineItem>> transLineItems = transactionData.getTransactionLineItems();
        if (DataFunctions.isEmptyCollection(transLineItems)) {
            Logger.logMessage(String.format("No transaction line items were found within the transaction with an ID of %s unable to find a dining option within the transaction.",
                    Objects.toString(transactionData.getPATransactionID(), "N/A")), log, Logger.LEVEL.ERROR);
            return null;
        }

        // search for a dining option within the transaction line items
        Logger.logMessage("Searching for a dining option in the MMHTree...", log, Logger.LEVEL.DEBUG);
        MMHTree<TransactionLineItem> diningOption = null;
        Iterator<MMHTree<TransactionLineItem>> transLineItemItr = transLineItems.iterator();
        int index = 0;
        while (transLineItemItr.hasNext()) {
            TransactionLineItem transLineItemData = transLineItemItr.next().getData();
            if ((transLineItemData != null) && (transLineItemData.getDiningOption())) {
                Logger.logMessage("FOUND THE DINING OPTION!", log, Logger.LEVEL.DEBUG);
                Logger.logMessage(String.format("Removing the dining option %s from the transaction line items at index %s within " +
                        "the transaction with an ID of %s in KitchenReceiptHelper.getDiningOptionTransLineItem!",
                        Objects.toString(transLineItemData.getProductName(), "N/A"),
                        Objects.toString(index, "N/A"),
                        Objects.toString(transactionData.getPATransactionID(), "N/A")), log, Logger.LEVEL.TRACE);
                diningOption = new MMHTree<>(transLineItemData);
                transLineItemItr.remove();
            }
            index++;
        }

        return diningOption;
    }

    /**
     * <p>Gets and modifiers for a product within the transaction.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param productTransLineItem A {@link MMHTree} of {@link TransactionLineItem} corresponding to a product within the transaction.
     * @return An {@link ArrayList} of {@link TransactionLineItem} corresponding to the modifiers for the given product.
     */
    @SuppressWarnings("Convert2streamapi")
    public static ArrayList<TransactionLineItem> getModifiersForProduct (String log, MMHTree<TransactionLineItem> productTransLineItem) {

        // make sure we have a valid log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file path passed to KitchenReceiptHelper.getModifiersForProduct can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid product line item
        if (productTransLineItem == null) {
            Logger.logMessage("The product transaction line item passed to KitchenReceiptHelper.getModifiersForProduct can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the product transaction line item has modifiers
        ArrayList<MMHTree<TransactionLineItem>> modifierTransLineItems = productTransLineItem.getChildren();
        if (DataFunctions.isEmptyCollection(modifierTransLineItems)) {
            Logger.logMessage(String.format("The product %s within the transaction with an ID of %s has no modifiers in KitchenReceiptHelper.getModifiersForProduct!",
                    Objects.toString(productTransLineItem.getData().getProductName(), "N/A"),
                    Objects.toString(productTransLineItem.getData().getPATransactionID(), "N/A")), log, Logger.LEVEL.TRACE);
            return null;
        }

        ArrayList<TransactionLineItem> modifiersForProduct = new ArrayList<>();
        for (MMHTree<TransactionLineItem> modifierTransLineItem : modifierTransLineItems) {
            TransactionLineItem modifier = modifierTransLineItem.getData();
            if (modifier != null) {
                modifiersForProduct.add(modifier);
            }
        }

        return modifiersForProduct;
    }

    /**
     * <p>Queries the database to get the name of the printer with the given printer ID.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param printerID The ID of the printer to get the name of.
     * @return The {@link String} name of the printer with the given printer ID.
     */
    public static String getPrinterNameFromID (String log, DataManager dataManager, int printerID) {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KitchenReceiptHelper.getPrinterNameFromID can't be null!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the DataManager is valid
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.getPrinterNameFromID can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the printer ID is valid
        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KitchenReceiptHelper.getPrinterNameFromID must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetPrinterName").addIDList(1, printerID).setLogFileName(log);
        return sql.getSingleStringField(dataManager);
    }

    /**
     * <p>Creates the product -> modifier(s) hierarchy from the transaction line items.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param transLineItems An {@link ArrayList} of {@link TransactionLineItem} corresponding to some transaction line items within the transaction.
     * @return An {@link ArrayList} of {@link MMHTree} of {@link TransactionLineItem} corresponding to the transaction line items organized by the product -> modifier(s) hierarchy.
     */
    @SuppressWarnings("Convert2streamapi")
    public static ArrayList<MMHTree<TransactionLineItem>> buildTransLineItemTrees (String log, ArrayList<TransactionLineItem> transLineItems) {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KitchenReceiptHelper.buildTransLineItemTrees can't be null!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure there are transaction line items
        if (DataFunctions.isEmptyCollection(transLineItems)) {
            Logger.logMessage("The transaction line items passed to KitchenReceiptHelper.buildTransLineItemTrees can't be null or empty!", log, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<MMHTree<TransactionLineItem>> transLineItemTrees = new ArrayList<>();
        for (TransactionLineItem transLineItem : transLineItems) {
            if ((transLineItem != null) && (transLineItem.getPATransLineItemID() > 0) && (transLineItem.getPATransLineItemModID() <= 0)) {
                MMHTree<TransactionLineItem> productTree = new MMHTree<>(transLineItem);
                // check if the product has any modifiers
                for (TransactionLineItem innerTransLineItem : transLineItems) {
                    if ((innerTransLineItem != null)
                            && (transLineItem.getPATransLineItemID() == innerTransLineItem.getPATransLineItemID())
                            && (innerTransLineItem.getPATransLineItemModID() > 0)) {
                        MMHTree<TransactionLineItem> modifierTree = new MMHTree<>(innerTransLineItem);
                        productTree.addChild(modifierTree);
                    }
                }
                transLineItemTrees.add(productTree);
            }
        }

        return transLineItemTrees;
    }

    /**
     * <p>Builds the line item text for a print queue detail.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param transLineItem The {@link TransactionLineItem} to build the line item text for.
     * @param isForKP Whether or not the print queue detail is for a kitchen printer.
     * @param isForKDS Whether or not the print queue detail is for a KDS station.
     * @return The line item text {@link String} for the print queue detail.
     */
    public static String buildLineItemText (String log, TransactionLineItem transLineItem, boolean isForKP, boolean isForKDS, boolean isForKMS) {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KitchenReceiptHelper.buildLineItemText can't be null!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure there is a transaction line item
        if (transLineItem == null) {
            Logger.logMessage("The transaction line item passed to KitchenReceiptHelper.buildLineItemText can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // get the name of the product or modifier represented by the transaction line item
        String productName = (StringFunctions.stringHasContent(transLineItem.getProductName()) ? transLineItem.getProductName() : "");
        // determine whether or not this transaction line item is for a modifier
        boolean isModifier = (transLineItem.getPATransLineItemModID() > 0);
        // get information about a prep option for the product
        boolean hasPrepOption = (!DataFunctions.isEmptyMap(transLineItem.getPrepOptionData()));
        String prepOptionName = (hasPrepOption ? HashMapDataFns.getStringVal(transLineItem.getPrepOptionData(), "PREPNAME") : "");
        boolean displayDefaultKitchen = (hasPrepOption ? HashMapDataFns.getBooleanVal(transLineItem.getPrepOptionData(), "DISPLAYDEFAULTKITCHEN") : false);
        boolean isDefaultOption = (hasPrepOption ? HashMapDataFns.getBooleanVal(transLineItem.getPrepOptionData(), "DEFAULTOPTION") : false);
        // determine whether or not to add the prep option to the line item text
        boolean showPrepOption = ((hasPrepOption) && (((displayDefaultKitchen) && (isDefaultOption)) || (!isDefaultOption)));
        Logger.logMessage("KitchenReceiptHelper.buildLineItemText: Prep Option Options: prepOptionName=" + prepOptionName + "hasPrepOption=" + hasPrepOption + " displayDefaultKitchen=" + displayDefaultKitchen + " isDefaultOption=" + isDefaultOption + " showPrepOption=" + showPrepOption , "KMS.log", Logger.LEVEL.TRACE);
        double quantity = 0.0d;
        if ((transLineItem.getPAPluID() > 0) && (KMSManager.isWeighted(transLineItem.getPAPluID()))) {
            // set the quantity of weighted products to 1
            quantity = 1.0d;
        }
        else {
            quantity = transLineItem.getQuantity();
        }
        String lineItemText = "";
        if (isForKP) {
            if (StringFunctions.stringHasContent(productName)) {
                // update the product name so that it is formatted for KP
                if ((!isModifier) && (quantity > 0.0d)) {
                    productName = String.format("%2dX %s", Math.round(quantity), productName);
                }
                else if ((isModifier) && (quantity > 0.0d)) {
                    productName = String.format("\t %s", productName);
                }

                if (showPrepOption) {
                    if (StringFunctions.stringHasContent(prepOptionName)) {
                        lineItemText = productName + " - " + prepOptionName;
                    }
                    else {
                        lineItemText = productName;
                    }
                }
                else {
                    lineItemText = productName;
                }
            }
            else {
                if ((!isModifier) && (quantity > 0.0d)) {
                    lineItemText = String.format("%2dX %s", Math.round(quantity), "N/A");
                }
                else if ((isModifier) && (quantity > 0.0d)) {
                    lineItemText = String.format("\t %s", productName);
                }
            }
        }
        else if ((isForKDS) || (isForKMS)) {
            if (StringFunctions.stringHasContent(productName)) {
                if(transLineItem.getDiningOption()){
                    productName = "Dining Option: "+ productName;
                }
                if ((!isModifier) && (quantity > 0.0d)) {
                    productName = String.format("%2dX %s", Math.round(quantity), productName);
                }
                else if ((isModifier) && (quantity > 0.0d)) {
                    productName = String.format("\t %s", productName);
                }

                if (showPrepOption) {
                    if (StringFunctions.stringHasContent(prepOptionName)) {
                        lineItemText = productName + " - " + prepOptionName;
                    }
                    else {
                        lineItemText = productName;
                    }
                }
                else {
                    lineItemText = productName;
                }
            }
            else {
                lineItemText = "N/A";
            }
        }

        Logger.logMessage(String.format("The line item text: %s, was built in KitchenReceiptHelper.buildLineItemText",
                Objects.toString(lineItemText, "N/A")), log, Logger.LEVEL.TRACE);
        return lineItemText;
    }

    /**
     * <p>Queries the database to get the printer host ID of the printer host being used by a printer.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param printerID The ID of the printer to get the printer host ID for.
     * @return The printer host ID of the printer host being used by the printer with the given printer ID.
     */
    public static int getPrinterHostID (String log, DataManager dataManager, int printerID) {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to KitchenReceiptHelper.getPrinterHostID can't be null!", Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure we have a valid data manager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptHelper.getPrinterHostID can't be null!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure the printer ID is valid
        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KitchenReceiptHelper.getPrinterHostID must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetPrinterHostIDForPrinter").addIDList(1, printerID).setLogFileName(log);
        return sql.getSingleIntField(dataManager);
    }

}