package com.mmhayes.common.kms.receiptGen.customerReceipt.nutritionInfo;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-18 10:24:14 -0400 (Fri, 18 Sep 2020) $: Date of last commit
    $Rev: 12683 $: Revision of last commit
    Notes: Custom XML RPC serializer that will allow a NutritionItem object to be used in XML RPC calls.
*/

import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import redstone.xmlrpc.XmlRpcCustomSerializer;
import redstone.xmlrpc.XmlRpcException;
import redstone.xmlrpc.XmlRpcMessages;
import redstone.xmlrpc.XmlRpcSerializer;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.Writer;

public class NutritionItemSerializer implements XmlRpcCustomSerializer {

    /**
     * <p>Gets the class of objects this serializer knows how to handle.</p>
     *
     * @return The {@link Class} of objects this serializer knows how to handle.
     */
    @Override
    public Class getSupportedClass () {
        return NutritionItem.class;
    }

    /**
     * <p>Serializes the object into the writer.</p>
     *
     * @param o The {@link Object} to serialize.
     * @param writer The {@link Writer} the object will be serialized into.
     * @param xmlRpcSerializer The built in {@link XmlRpcSerializer} being used.
     * @throws XmlRpcException
     * @throws IOException
     */
    @SuppressWarnings("Duplicates")
    @Override
    public void serialize (Object o, Writer writer, XmlRpcSerializer xmlRpcSerializer) throws XmlRpcException, IOException {

        // make sure the object is valid
        if (o == null) {
            Logger.logMessage("The Object supplied to NutritionItemSerializer.serialize can't be null, unable to serialize the Object!", Logger.LEVEL.ERROR);
            return;
        }

        // make sure the writer is valid
        if (writer == null) {
            Logger.logMessage("The Writer supplied to NutritionItemSerializer.serialize can't be null, unable to serialize the Object!", Logger.LEVEL.ERROR);
            return;
        }

        // make sure the XML RPC serializer is valid
        if (xmlRpcSerializer == null) {
            Logger.logMessage("The XmlRpcSerializer supplied to NutritionItemSerializer.serialize can't be null, unable to serialize the Object!", Logger.LEVEL.ERROR);
            return;
        }

        try {
            // get property descriptors for NutritionItem
            BeanInfo nutritionItemInfo = Introspector.getBeanInfo(o.getClass(), Object.class);
            PropertyDescriptor[] descriptors = nutritionItemInfo.getPropertyDescriptors();

            if (!DataFunctions.isEmptyGenericArr(descriptors)) {
                writer.write("<struct>");
                for (PropertyDescriptor descriptor : descriptors) {
                    Object propertyValue = descriptor.getReadMethod().invoke(o, ((Object[]) null));
                    if (propertyValue != null) {
                        writer.write("<member>");
                        writer.write("<name>");
                        writer.write(descriptor.getDisplayName());
                        writer.write("</name>");
                        xmlRpcSerializer.serialize(propertyValue, writer);
                        writer.write("</member>");
                    }
                }
                writer.write("</struct>");
            }
            else {
                Logger.logMessage("No property descriptors found for the NutritionItem instance in NutritionItemSerializer.serialize!", Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            throw new XmlRpcException(XmlRpcMessages.getString("NutritionItemSerializer.SerializationError"), e);
        }
    }
}