package com.mmhayes.common.kms.receiptGen.customerReceipt.nutritionInfo;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-16 11:17:08 -0400 (Wed, 16 Sep 2020) $: Date of last commit
    $Rev: 12658 $: Revision of last commit
    Notes: Represents a nutrition total.
*/

import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import java.util.Objects;

public class NutritionTotal {

    // private member variables of a NutritionTotal
    private String dvCalLabel = null;
    private String nutritionInfo1 = null;
    private String nutritionInfo2 = null;
    private String nutritionInfo3 = null;
    private String nutritionInfo4 = null;
    private String nutritionInfo5 = null;

    /**
     * <p>Empty constructor for a {@link NutritionTotal}.</p>
     *
     */
    public NutritionTotal () {}

    /**
     * <p>Builds a {@link NutritionTotal} from the given {@link XmlRpcStruct}.</p>
     *
     * @param nutritionTotalStruct The {@link XmlRpcStruct} to use to build the {@link NutritionTotal}.
     * @return The {@link NutritionTotal} instance built form a {@link XmlRpcStruct}.
     */
    public static NutritionTotal buildFromXmlRpcStruct (XmlRpcStruct nutritionTotalStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(nutritionTotalStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to NutritionTotal.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new NutritionTotal()
                .dvCalLabel(HashMapDataFns.getStringVal(nutritionTotalStruct, "DVCALLABEL", true))
                .nutritionInfo1(HashMapDataFns.getStringVal(nutritionTotalStruct, "NUTRITIONINFO1", true))
                .nutritionInfo2(HashMapDataFns.getStringVal(nutritionTotalStruct, "NUTRITIONINFO2", true))
                .nutritionInfo3(HashMapDataFns.getStringVal(nutritionTotalStruct, "NUTRITIONINFO3", true))
                .nutritionInfo4(HashMapDataFns.getStringVal(nutritionTotalStruct, "NUTRITIONINFO4", true))
                .nutritionInfo5(HashMapDataFns.getStringVal(nutritionTotalStruct, "NUTRITIONINFO5", true));
    }

    /**
     * <p>Overridden toString() method for a {@link NutritionTotal} instance.</p>
     *
     * @return The {@link NutritionTotal} instance as a {@link String};
     */
    @SuppressWarnings("OverlyComplexMethod")
    @Override
    public String toString () {

        return String.format("DVCALLABEL: %s, NUTRITIONINFO1: %s, NUTRITIONINFO2: %s, NUTRITIONINFO3: %s, NUTRITIONINFO4: %s, NUTRITIONINFO5: %s",
                Objects.toString((StringFunctions.stringHasContent(dvCalLabel) ? dvCalLabel : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(nutritionInfo1) ? nutritionInfo1 : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(nutritionInfo2) ? nutritionInfo2 : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(nutritionInfo3) ? nutritionInfo3 : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(nutritionInfo4) ? nutritionInfo4 : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(nutritionInfo5) ? nutritionInfo5 : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link NutritionTotal} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link NutritionTotal} instance.
     * @return Whether or not the {@link Object} is equal to the {@link NutritionTotal} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a NutritionTotal
        NutritionTotal nutritionTotal = (NutritionTotal) obj;
        return Objects.equals(nutritionTotal.dvCalLabel, dvCalLabel);
    }

    /**
     * <p>Overridden hashCode() method for a {@link NutritionTotal} instance.</p>
     *
     * @return The hash code for a {@link NutritionTotal} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(dvCalLabel);
    }

    /**
     * <p>Constructor for a {@link NutritionTotal} with it's dvCalLabel field set.</p>
     *
     * @param dvCalLabel The daily value calorie {@link String} for the {@link NutritionTotal}.
     * @return The {@link NutritionTotal} instance with it's dvCalLabel field set.
     */
    public NutritionTotal dvCalLabel (String dvCalLabel) {
        this.dvCalLabel = dvCalLabel;
        return this;
    }

    /**
     * <p>Constructor for a {@link NutritionTotal} with it's nutritionInfo1 field set.</p>
     *
     * @param nutritionInfo1 The nutrition info 1 {@link String} for the {@link NutritionTotal}.
     * @return The {@link NutritionTotal} instance with it's nutritionInfo1 field set.
     */
    public NutritionTotal nutritionInfo1 (String nutritionInfo1) {
        this.nutritionInfo1 = nutritionInfo1;
        return this;
    }

    /**
     * <p>Constructor for a {@link NutritionTotal} with it's nutritionInfo2 field set.</p>
     *
     * @param nutritionInfo2 The nutrition info 2 {@link String} for the {@link NutritionTotal}.
     * @return The {@link NutritionTotal} instance with it's nutritionInfo2 field set.
     */
    public NutritionTotal nutritionInfo2 (String nutritionInfo2) {
        this.nutritionInfo2 = nutritionInfo2;
        return this;
    }

    /**
     * <p>Constructor for a {@link NutritionTotal} with it's nutritionInfo3 field set.</p>
     *
     * @param nutritionInfo3 The nutrition info 3 {@link String} for the {@link NutritionTotal}.
     * @return The {@link NutritionTotal} instance with it's nutritionInfo3 field set.
     */
    public NutritionTotal nutritionInfo3 (String nutritionInfo3) {
        this.nutritionInfo3 = nutritionInfo3;
        return this;
    }

    /**
     * <p>Constructor for a {@link NutritionTotal} with it's nutritionInfo4 field set.</p>
     *
     * @param nutritionInfo4 The nutrition info 4 {@link String} for the {@link NutritionTotal}.
     * @return The {@link NutritionTotal} instance with it's nutritionInfo4 field set.
     */
    public NutritionTotal nutritionInfo4 (String nutritionInfo4) {
        this.nutritionInfo4 = nutritionInfo4;
        return this;
    }

    /**
     * <p>Constructor for a {@link NutritionTotal} with it's nutritionInfo5 field set.</p>
     *
     * @param nutritionInfo5 The nutrition info 5 {@link String} for the {@link NutritionTotal}.
     * @return The {@link NutritionTotal} instance with it's nutritionInfo5 field set.
     */
    public NutritionTotal nutritionInfo5 (String nutritionInfo5) {
        this.nutritionInfo5 = nutritionInfo5;
        return this;
    }

    /**
     * <p>Getter for the dvCalLabel field of the {@link NutritionTotal}.</p>
     *
     * @return The dvCalLabel field of the {@link NutritionTotal}.
     */
    public String getDvCalLabel () {
        return dvCalLabel;
    }

    /**
     * <p>Setter for the dvCalLabel field of the {@link NutritionTotal}.</p>
     *
     * @param dvCalLabel The dvCalLabel field of the {@link NutritionTotal}.
     */
    public void setDvCalLabel (String dvCalLabel) {
        this.dvCalLabel = dvCalLabel;
    }

    /**
     * <p>Getter for the nutritionInfo1 field of the {@link NutritionTotal}.</p>
     *
     * @return The nutritionInfo1 field of the {@link NutritionTotal}.
     */
    public String getNutritionInfo1 () {
        return nutritionInfo1;
    }

    /**
     * <p>Setter for the nutritionInfo1 field of the {@link NutritionTotal}.</p>
     *
     * @param nutritionInfo1 The nutritionInfo1 field of the {@link NutritionTotal}.
     */
    public void setNutritionInfo1 (String nutritionInfo1) {
        this.nutritionInfo1 = nutritionInfo1;
    }

    /**
     * <p>Getter for the nutritionInfo2 field of the {@link NutritionTotal}.</p>
     *
     * @return The nutritionInfo2 field of the {@link NutritionTotal}.
     */
    public String getNutritionInfo2 () {
        return nutritionInfo2;
    }

    /**
     * <p>Setter for the nutritionInfo2 field of the {@link NutritionTotal}.</p>
     *
     * @param nutritionInfo2 The nutritionInfo2 field of the {@link NutritionTotal}.
     */
    public void setNutritionInfo2 (String nutritionInfo2) {
        this.nutritionInfo2 = nutritionInfo2;
    }

    /**
     * <p>Getter for the nutritionInfo3 field of the {@link NutritionTotal}.</p>
     *
     * @return The nutritionInfo3 field of the {@link NutritionTotal}.
     */
    public String getNutritionInfo3 () {
        return nutritionInfo3;
    }

    /**
     * <p>Setter for the nutritionInfo3 field of the {@link NutritionTotal}.</p>
     *
     * @param nutritionInfo3 The nutritionInfo3 field of the {@link NutritionTotal}.
     */
    public void setNutritionInfo3 (String nutritionInfo3) {
        this.nutritionInfo3 = nutritionInfo3;
    }

    /**
     * <p>Getter for the nutritionInfo4 field of the {@link NutritionTotal}.</p>
     *
     * @return The nutritionInfo4 field of the {@link NutritionTotal}.
     */
    public String getNutritionInfo4 () {
        return nutritionInfo4;
    }

    /**
     * <p>Setter for the nutritionInfo4 field of the {@link NutritionTotal}.</p>
     *
     * @param nutritionInfo4 The nutritionInfo4 field of the {@link NutritionTotal}.
     */
    public void setNutritionInfo4 (String nutritionInfo4) {
        this.nutritionInfo4 = nutritionInfo4;
    }

    /**
     * <p>Getter for the nutritionInfo5 field of the {@link NutritionTotal}.</p>
     *
     * @return The nutritionInfo5 field of the {@link NutritionTotal}.
     */
    public String getNutritionInfo5 () {
        return nutritionInfo5;
    }

    /**
     * <p>Setter for the nutritionInfo5 field of the {@link NutritionTotal}.</p>
     *
     * @param nutritionInfo5 The nutritionInfo5 field of the {@link NutritionTotal}.
     */
    public void setNutritionInfo5 (String nutritionInfo5) {
        this.nutritionInfo5 = nutritionInfo5;
    }

}