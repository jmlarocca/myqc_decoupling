package com.mmhayes.common.kms.receiptGen.customerReceipt.nutritionInfo;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-16 11:17:08 -0400 (Wed, 16 Sep 2020) $: Date of last commit
    $Rev: 12658 $: Revision of last commit
    Notes: Represents a nutrition item.
*/

import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import java.util.Objects;

public class NutritionItem {

    // private member variables of a NutritionItem
    private int quantity = -1;
    private String productName = null;
    private String nutritionInfo1 = null;
    private String nutritionInfo2 = null;
    private String nutritionInfo3 = null;
    private String nutritionInfo4 = null;
    private String nutritionInfo5 = null;

    /**
     * <p>Empty constructor for a {@link NutritionItem}.</p>
     *
     */
    public NutritionItem () {}

    /**
     * <p>Builds a {@link NutritionItem} from the given {@link XmlRpcStruct}.</p>
     *
     * @param nutritionItemStruct The {@link XmlRpcStruct} to use to build the {@link NutritionItem}.
     * @return The {@link NutritionItem} instance built form a {@link XmlRpcStruct}.
     */
    public static NutritionItem buildFromXmlRpcStruct (XmlRpcStruct nutritionItemStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(nutritionItemStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to NutritionItem.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new NutritionItem()
                .quantity(HashMapDataFns.getIntVal(nutritionItemStruct, "QUANTITY", true))
                .productName(HashMapDataFns.getStringVal(nutritionItemStruct, "PRODUCTNAME", true))
                .nutritionInfo1(HashMapDataFns.getStringVal(nutritionItemStruct, "NUTRITIONINFO1", true))
                .nutritionInfo2(HashMapDataFns.getStringVal(nutritionItemStruct, "NUTRITIONINFO2", true))
                .nutritionInfo3(HashMapDataFns.getStringVal(nutritionItemStruct, "NUTRITIONINFO3", true))
                .nutritionInfo4(HashMapDataFns.getStringVal(nutritionItemStruct, "NUTRITIONINFO4", true))
                .nutritionInfo5(HashMapDataFns.getStringVal(nutritionItemStruct, "NUTRITIONINFO5", true));
    }

    /**
     * <p>Overridden toString() method for a {@link NutritionItem} instance.</p>
     *
     * @return The {@link NutritionItem} instance as a {@link String};
     */
    @SuppressWarnings("OverlyComplexMethod")
    @Override
    public String toString () {

        return String.format("QUANTITY: %s, PRODUCTNAME: %s, NUTRITIONINFO1: %s, NUTRITIONINFO2: %s, NUTRITIONINFO3: %s, NUTRITIONINFO4: %s, NUTRITIONINFO5: %s",
                Objects.toString((quantity > 0 ? quantity : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(productName) ? productName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(nutritionInfo1) ? nutritionInfo1 : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(nutritionInfo2) ? nutritionInfo2 : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(nutritionInfo3) ? nutritionInfo3 : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(nutritionInfo4) ? nutritionInfo4 : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(nutritionInfo5) ? nutritionInfo5 : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link NutritionItem} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link NutritionItem} instance.
     * @return Whether or not the {@link Object} is equal to the {@link NutritionItem} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a NutritionItem
        NutritionItem nutritionItem = (NutritionItem) obj;
        return Objects.equals(nutritionItem.productName, productName);
    }

    /**
     * <p>Overridden hashCode() method for a {@link NutritionItem} instance.</p>
     *
     * @return The hash code for a {@link NutritionItem} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(productName);
    }

    /**
     * <p>Constructor for a {@link NutritionItem} with it's quantity field set.</p>
     *
     * @param quantity The quantity of the {@link NutritionItem}.
     * @return The {@link NutritionItem} instance with it's quantity field set.
     */
    public NutritionItem quantity (int quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * <p>Constructor for a {@link NutritionItem} with it's productName field set.</p>
     *
     * @param productName The product name {@link String} for the {@link NutritionItem}.
     * @return The {@link NutritionItem} instance with it's productName field set.
     */
    public NutritionItem productName (String productName) {
        this.productName = productName;
        return this;
    }

    /**
     * <p>Constructor for a {@link NutritionItem} with it's nutritionInfo1 field set.</p>
     *
     * @param nutritionInfo1 The nutrition info 1 {@link String} for the {@link NutritionItem}.
     * @return The {@link NutritionItem} instance with it's nutritionInfo1 field set.
     */
    public NutritionItem nutritionInfo1 (String nutritionInfo1) {
        this.nutritionInfo1 = nutritionInfo1;
        return this;
    }

    /**
     * <p>Constructor for a {@link NutritionItem} with it's nutritionInfo2 field set.</p>
     *
     * @param nutritionInfo2 The nutrition info 2 {@link String} for the {@link NutritionItem}.
     * @return The {@link NutritionItem} instance with it's nutritionInfo2 field set.
     */
    public NutritionItem nutritionInfo2 (String nutritionInfo2) {
        this.nutritionInfo2 = nutritionInfo2;
        return this;
    }

    /**
     * <p>Constructor for a {@link NutritionItem} with it's nutritionInfo3 field set.</p>
     *
     * @param nutritionInfo3 The nutrition info 3 {@link String} for the {@link NutritionItem}.
     * @return The {@link NutritionItem} instance with it's nutritionInfo3 field set.
     */
    public NutritionItem nutritionInfo3 (String nutritionInfo3) {
        this.nutritionInfo3 = nutritionInfo3;
        return this;
    }

    /**
     * <p>Constructor for a {@link NutritionItem} with it's nutritionInfo4 field set.</p>
     *
     * @param nutritionInfo4 The nutrition info 4 {@link String} for the {@link NutritionItem}.
     * @return The {@link NutritionItem} instance with it's nutritionInfo4 field set.
     */
    public NutritionItem nutritionInfo4 (String nutritionInfo4) {
        this.nutritionInfo4 = nutritionInfo4;
        return this;
    }

    /**
     * <p>Constructor for a {@link NutritionItem} with it's nutritionInfo5 field set.</p>
     *
     * @param nutritionInfo5 The nutrition info 5 {@link String} for the {@link NutritionItem}.
     * @return The {@link NutritionItem} instance with it's nutritionInfo5 field set.
     */
    public NutritionItem nutritionInfo5 (String nutritionInfo5) {
        this.nutritionInfo5 = nutritionInfo5;
        return this;
    }

    /**
     * <p>Getter for the quantity field of the {@link NutritionItem}.</p>
     *
     * @return The quantity field of the {@link NutritionItem}.
     */
    public int getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link NutritionItem}.</p>
     *
     * @param quantity The quantity field of the {@link NutritionItem}.
     */
    public void setQuantity (int quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the productName field of the {@link NutritionItem}.</p>
     *
     * @return The productName field of the {@link NutritionItem}.
     */
    public String getProductName () {
        return productName;
    }

    /**
     * <p>Setter for the productName field of the {@link NutritionItem}.</p>
     *
     * @param productName The productName field of the {@link NutritionItem}.
     */
    public void setProductName (String productName) {
        this.productName = productName;
    }

    /**
     * <p>Getter for the nutritionInfo1 field of the {@link NutritionItem}.</p>
     *
     * @return The nutritionInfo1 field of the {@link NutritionItem}.
     */
    public String getNutritionInfo1 () {
        return nutritionInfo1;
    }

    /**
     * <p>Setter for the nutritionInfo1 field of the {@link NutritionItem}.</p>
     *
     * @param nutritionInfo1 The nutritionInfo1 field of the {@link NutritionItem}.
     */
    public void setNutritionInfo1 (String nutritionInfo1) {
        this.nutritionInfo1 = nutritionInfo1;
    }

    /**
     * <p>Getter for the nutritionInfo2 field of the {@link NutritionItem}.</p>
     *
     * @return The nutritionInfo2 field of the {@link NutritionItem}.
     */
    public String getNutritionInfo2 () {
        return nutritionInfo2;
    }

    /**
     * <p>Setter for the nutritionInfo2 field of the {@link NutritionItem}.</p>
     *
     * @param nutritionInfo2 The nutritionInfo2 field of the {@link NutritionItem}.
     */
    public void setNutritionInfo2 (String nutritionInfo2) {
        this.nutritionInfo2 = nutritionInfo2;
    }

    /**
     * <p>Getter for the nutritionInfo3 field of the {@link NutritionItem}.</p>
     *
     * @return The nutritionInfo3 field of the {@link NutritionItem}.
     */
    public String getNutritionInfo3 () {
        return nutritionInfo3;
    }

    /**
     * <p>Setter for the nutritionInfo3 field of the {@link NutritionItem}.</p>
     *
     * @param nutritionInfo3 The nutritionInfo3 field of the {@link NutritionItem}.
     */
    public void setNutritionInfo3 (String nutritionInfo3) {
        this.nutritionInfo3 = nutritionInfo3;
    }

    /**
     * <p>Getter for the nutritionInfo4 field of the {@link NutritionItem}.</p>
     *
     * @return The nutritionInfo4 field of the {@link NutritionItem}.
     */
    public String getNutritionInfo4 () {
        return nutritionInfo4;
    }

    /**
     * <p>Setter for the nutritionInfo4 field of the {@link NutritionItem}.</p>
     *
     * @param nutritionInfo4 The nutritionInfo4 field of the {@link NutritionItem}.
     */
    public void setNutritionInfo4 (String nutritionInfo4) {
        this.nutritionInfo4 = nutritionInfo4;
    }

    /**
     * <p>Getter for the nutritionInfo5 field of the {@link NutritionItem}.</p>
     *
     * @return The nutritionInfo5 field of the {@link NutritionItem}.
     */
    public String getNutritionInfo5 () {
        return nutritionInfo5;
    }

    /**
     * <p>Setter for the nutritionInfo5 field of the {@link NutritionItem}.</p>
     *
     * @param nutritionInfo5 The nutritionInfo5 field of the {@link NutritionItem}.
     */
    public void setNutritionInfo5 (String nutritionInfo5) {
        this.nutritionInfo5 = nutritionInfo5;
    }
    
}