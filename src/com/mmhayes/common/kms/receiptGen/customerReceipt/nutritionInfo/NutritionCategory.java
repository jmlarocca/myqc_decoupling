package com.mmhayes.common.kms.receiptGen.customerReceipt.nutritionInfo;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-16 11:17:08 -0400 (Wed, 16 Sep 2020) $: Date of last commit
    $Rev: 12658 $: Revision of last commit
    Notes: Represents a nutrition category.
*/

import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import java.util.Objects;

public class NutritionCategory {

    // private member variables of a NutritionCategory
    private String shortName = null;
    private String measurementLabel = null;

    /**
     * <p>Empty constructor for a {@link NutritionCategory}.</p>
     *
     */
    public NutritionCategory () {}

    /**
     * <p>Builds a {@link NutritionCategory} from the given {@link XmlRpcStruct}.</p>
     *
     * @param nutritionCategoryStruct The {@link XmlRpcStruct} to use to build the {@link NutritionCategory}.
     * @return The {@link NutritionCategory} instance built form a {@link XmlRpcStruct}.
     */
    public static NutritionCategory buildFromXmlRpcStruct (XmlRpcStruct nutritionCategoryStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(nutritionCategoryStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to NutritionCategory.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new NutritionCategory()
                .shortName(HashMapDataFns.getStringVal(nutritionCategoryStruct, "SHORTNAME", true))
                .measurementLabel(HashMapDataFns.getStringVal(nutritionCategoryStruct, "MEASUREMENTLABEL", true));
    }

    /**
     * <p>Overridden toString() method for a {@link NutritionCategory} instance.</p>
     *
     * @return The {@link NutritionCategory} instance as a {@link String};
     */
    @SuppressWarnings("OverlyComplexMethod")
    @Override
    public String toString () {

        return String.format("SHORTNAME: %s, MEASUREMENTLABEL: %s",
                Objects.toString((StringFunctions.stringHasContent(shortName) ? shortName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(measurementLabel) ? measurementLabel : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link NutritionCategory} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link NutritionCategory} instance.
     * @return Whether or not the {@link Object} is equal to the {@link NutritionCategory} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a NutritionCategory
        NutritionCategory nutritionCategory = (NutritionCategory) obj;
        return Objects.equals(nutritionCategory.shortName, shortName);
    }

    /**
     * <p>Overridden hashCode() method for a {@link NutritionCategory} instance.</p>
     *
     * @return The hash code for a {@link NutritionCategory} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(shortName);
    }

    /**
     * <p>Constructor for a {@link NutritionCategory} with it's shortName field set.</p>
     *
     * @param shortName The short name {@link String} for the {@link NutritionCategory}.
     * @return The {@link NutritionCategory} instance with it's shortName field set.
     */
    public NutritionCategory shortName (String shortName) {
        this.shortName = shortName;
        return this;
    }

    /**
     * <p>Constructor for a {@link NutritionCategory} with it's measurementLabel field set.</p>
     *
     * @param measurementLabel The measurement label {@link String} for the {@link NutritionCategory}.
     * @return The {@link NutritionCategory} instance with it's measurementLabel field set.
     */
    public NutritionCategory measurementLabel (String measurementLabel) {
        this.measurementLabel = measurementLabel;
        return this;
    }

    /**
     * <p>Getter for the shortName field of the {@link NutritionCategory}.</p>
     *
     * @return The shortName field of the {@link NutritionCategory}.
     */
    public String getShortName () {
        return shortName;
    }

    /**
     * <p>Setter for the shortName field of the {@link NutritionCategory}.</p>
     *
     * @param shortName The shortName field of the {@link NutritionCategory}.
     */
    public void setShortName (String shortName) {
        this.shortName = shortName;
    }

    /**
     * <p>Getter for the measurementLabel field of the {@link NutritionCategory}.</p>
     *
     * @return The measurementLabel field of the {@link NutritionCategory}.
     */
    public String getMeasurementLabel () {
        return measurementLabel;
    }

    /**
     * <p>Setter for the measurementLabel field of the {@link NutritionCategory}.</p>
     *
     * @param measurementLabel The measurementLabel field of the {@link NutritionCategory}.
     */
    public void setMeasurementLabel (String measurementLabel) {
        this.measurementLabel = measurementLabel;
    }

}