package com.mmhayes.common.kms.receiptGen.customerReceipt.loyaltyInfo;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-16 11:17:08 -0400 (Wed, 16 Sep 2020) $: Date of last commit
    $Rev: 12658 $: Revision of last commit
    Notes: Represents a loyalty program used in the transaction.
*/

import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcStruct;

import java.util.Objects;

public class LoyaltyData {

    // private member variables of a LoyaltyData
    private String name = null;
    private String badgeNumber = null;
    private String accountNumber = null;
    private int loyaltyRewardID = -1;
    private String programName = null;
    private String pointsBalance = null;
    private String pointsEarned = null;
    private String pointsRedeemed = null;

    /**
     * <p>Empty constructor for a {@link LoyaltyData} LoyaltyData.</p>
     *
     */
    public LoyaltyData () {}

    /**
     * <p>Builds a {@link LoyaltyData} from the given {@link XmlRpcStruct}.</p>
     *
     * @param loyaltyDataStruct The {@link XmlRpcStruct} to use to build the {@link LoyaltyData}.
     * @return The {@link LoyaltyData} instance built form a {@link XmlRpcStruct}.
     */
    public static LoyaltyData buildFromXmlRpcStruct (XmlRpcStruct loyaltyDataStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(loyaltyDataStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to LoyaltyData.buildFromXmlRpcStruct can't be null or empty!", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return new LoyaltyData()
                .name(HashMapDataFns.getStringVal(loyaltyDataStruct, "NAME", true))
                .badgeNumber(HashMapDataFns.getStringVal(loyaltyDataStruct, "BADGENUMBER", true))
                .accountNumber(HashMapDataFns.getStringVal(loyaltyDataStruct, "ACCOUNTNUMBER", true))
                .loyaltyRewardID(HashMapDataFns.getIntVal(loyaltyDataStruct, "LOYALTYREWARDID", true))
                .programName(HashMapDataFns.getStringVal(loyaltyDataStruct, "PROGRAMNAME", true))
                .pointsBalance(HashMapDataFns.getStringVal(loyaltyDataStruct, "POINTSBALANCE", true))
                .pointsEarned(HashMapDataFns.getStringVal(loyaltyDataStruct, "POINTSEARNED", true))
                .pointsRedeemed(HashMapDataFns.getStringVal(loyaltyDataStruct, "POINTSREDEEMED", true));
    }

    /**
     * <p>Overridden toString() method for a {@link LoyaltyData} instance.</p>
     *
     * @return The {@link LoyaltyData} instance as a {@link String};
     */
    @SuppressWarnings("OverlyComplexMethod")
    @Override
    public String toString () {

        return String.format("NAME: %s, BADGENUMBER: %s, ACCOUNTNUMBER: %s, LOYALTYREWARDID: %s, PROGRAMNAME: %s, POINTSBALANCE: %s, POINTSEARNED: %s, POINTSREDEEMED: %s",
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(badgeNumber) ? badgeNumber : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(accountNumber) ? accountNumber : "N/A"), "N/A"),
                Objects.toString((loyaltyRewardID > 0 ? loyaltyRewardID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(programName) ? programName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(pointsBalance) ? pointsBalance : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(pointsEarned) ? pointsEarned : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(pointsRedeemed) ? pointsRedeemed : "N/A"), "N/A"));
        
    }

    /**
     * <p>Constructor for a {@link LoyaltyData} with it's name field set.</p>
     *
     * @param name The name {@link String} of the person who made the transaction.
     * @return The {@link LoyaltyData} instance with it's name field set.
     */
    public LoyaltyData name (String name) {
        this.name = name;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyData} with it's badgeNumber field set.</p>
     *
     * @param badgeNumber The badge number {@link String} of the person who made the transaction.
     * @return The {@link LoyaltyData} instance with it's badgeNumber field set.
     */
    public LoyaltyData badgeNumber (String badgeNumber) {
        this.badgeNumber = badgeNumber;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyData} with it's accountNumber field set.</p>
     *
     * @param accountNumber The account number {@link String} of the person who made the transaction.
     * @return The {@link LoyaltyData} instance with it's accountNumber field set.
     */
    public LoyaltyData accountNumber (String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyData} with it's loyaltyRewardID field set.</p>
     *
     * @param loyaltyRewardID The ID of the loyalty reward.
     * @return The {@link LoyaltyData} instance with it's loyaltyRewardID field set.
     */
    public LoyaltyData loyaltyRewardID (int loyaltyRewardID) {
        this.loyaltyRewardID = loyaltyRewardID;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyData} with it's programName field set.</p>
     *
     * @param programName The name {@link String} of the loyalty program.
     * @return The {@link LoyaltyData} instance with it's programName field set.
     */
    public LoyaltyData programName (String programName) {
        this.programName = programName;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyData} with it's pointsBalance field set.</p>
     *
     * @param pointsBalance The points balance of the loyalty program.
     * @return The {@link LoyaltyData} instance with it's pointsBalance field set.
     */
    public LoyaltyData pointsBalance (String pointsBalance) {
        this.pointsBalance = pointsBalance;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyData} with it's pointsEarned field set.</p>
     *
     * @param pointsEarned The points earned in the loyalty program.
     * @return The {@link LoyaltyData} instance with it's pointsEarned field set.
     */
    public LoyaltyData pointsEarned (String pointsEarned) {
        this.pointsEarned = pointsEarned;
        return this;
    }

    /**
     * <p>Constructor for a {@link LoyaltyData} with it's pointsRedeemed field set.</p>
     *
     * @param pointsRedeemed The points redeemed for the loyalty program.
     * @return The {@link LoyaltyData} instance with it's pointsRedeemed field set.
     */
    public LoyaltyData pointsRedeemed (String pointsRedeemed) {
        this.pointsRedeemed = pointsRedeemed;
        return this;
    }
    
    /**
     * <p>Getter for the name field of the {@link LoyaltyData}.</p>
     *
     * @return The name field of the {@link LoyaltyData}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link LoyaltyData}.</p>
     *
     * @param name The name field of the {@link LoyaltyData}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the badgeNumber field of the {@link LoyaltyData}.</p>
     *
     * @return The badgeNumber field of the {@link LoyaltyData}.
     */
    public String getBadgeNumber () {
        return badgeNumber;
    }

    /**
     * <p>Setter for the badgeNumber field of the {@link LoyaltyData}.</p>
     *
     * @param badgeNumber The badgeNumber field of the {@link LoyaltyData}.
     */
    public void setBadgeNumber (String badgeNumber) {
        this.badgeNumber = badgeNumber;
    }

    /**
     * <p>Getter for the accountNumber field of the {@link LoyaltyData}.</p>
     *
     * @return The accountNumber field of the {@link LoyaltyData}.
     */
    public String getAccountNumber () {
        return accountNumber;
    }

    /**
     * <p>Setter for the accountNumber field of the {@link LoyaltyData}.</p>
     *
     * @param accountNumber The accountNumber field of the {@link LoyaltyData}.
     */
    public void setAccountNumber (String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * <p>Getter for the loyaltyRewardID field of the {@link LoyaltyData}.</p>
     *
     * @return The loyaltyRewardID field of the {@link LoyaltyData}.
     */
    public int getLoyaltyRewardID () {
        return loyaltyRewardID;
    }

    /**
     * <p>Setter for the loyaltyRewardID field of the {@link LoyaltyData}.</p>
     *
     * @param loyaltyRewardID The loyaltyRewardID field of the {@link LoyaltyData}.
     */
    public void setLoyaltyRewardID (int loyaltyRewardID) {
        this.loyaltyRewardID = loyaltyRewardID;
    }

    /**
     * <p>Getter for the programName field of the {@link LoyaltyData}.</p>
     *
     * @return The programName field of the {@link LoyaltyData}.
     */
    public String getProgramName () {
        return programName;
    }

    /**
     * <p>Setter for the programName field of the {@link LoyaltyData}.</p>
     *
     * @param programName The programName field of the {@link LoyaltyData}.
     */
    public void setProgramName (String programName) {
        this.programName = programName;
    }

    /**
     * <p>Getter for the pointsBalance field of the {@link LoyaltyData}.</p>
     *
     * @return The pointsBalance field of the {@link LoyaltyData}.
     */
    public String getPointsBalance () {
        return pointsBalance;
    }

    /**
     * <p>Setter for the pointsBalance field of the {@link LoyaltyData}.</p>
     *
     * @param pointsBalance The pointsBalance field of the {@link LoyaltyData}.
     */
    public void setPointsBalance (String pointsBalance) {
        this.pointsBalance = pointsBalance;
    }

    /**
     * <p>Getter for the pointsEarned field of the {@link LoyaltyData}.</p>
     *
     * @return The pointsEarned field of the {@link LoyaltyData}.
     */
    public String getPointsEarned () {
        return pointsEarned;
    }

    /**
     * <p>Setter for the pointsEarned field of the {@link LoyaltyData}.</p>
     *
     * @param pointsEarned The pointsEarned field of the {@link LoyaltyData}.
     */
    public void setPointsEarned (String pointsEarned) {
        this.pointsEarned = pointsEarned;
    }

    /**
     * <p>Getter for the pointsRedeemed field of the {@link LoyaltyData}.</p>
     *
     * @return The pointsRedeemed field of the {@link LoyaltyData}.
     */
    public String getPointsRedeemed () {
        return pointsRedeemed;
    }

    /**
     * <p>Setter for the pointsRedeemed field of the {@link LoyaltyData}.</p>
     *
     * @param pointsRedeemed The pointsRedeemed field of the {@link LoyaltyData}.
     */
    public void setPointsRedeemed (String pointsRedeemed) {
        this.pointsRedeemed = pointsRedeemed;
    }

}