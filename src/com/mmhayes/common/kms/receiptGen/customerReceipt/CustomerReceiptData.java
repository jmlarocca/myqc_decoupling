package com.mmhayes.common.kms.receiptGen.customerReceipt;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-30 16:11:42 -0400 (Wed, 30 Sep 2020) $: Date of last commit
    $Rev: 12769 $: Revision of last commit
    Notes: Contains the data needed to create a customer receipt.
*/

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kitchenPrinters.KitchenPrinterDataManagerMethod;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.kms.receiptGen.customerReceipt.loyaltyInfo.LoyaltyData;
import com.mmhayes.common.kms.receiptGen.customerReceipt.nutritionInfo.NutritionCategory;
import com.mmhayes.common.kms.receiptGen.customerReceipt.nutritionInfo.NutritionItem;
import com.mmhayes.common.kms.receiptGen.customerReceipt.nutritionInfo.NutritionTotal;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.TransactionData.*;
import com.mmhayes.common.receiptGen.TransactionData.jsonSerialization.PluAdapter;
import com.mmhayes.common.receiptGen.TransactionData.jsonSerialization.PluArrayListDeserializer;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.xmlapi.XmlRpcUtil;
import org.apache.commons.lang.math.NumberUtils;
import redstone.xmlrpc.XmlRpcStruct;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Objects;

public class CustomerReceiptData {

    // log file for CustomerReceiptData
    public static final String CUST_RCPT_LOG = "CustomerReceipt.log";

    // private member variables of a CustomerReceiptData
    private int transTypeID = -1;
    private int trainingModeTransTypeID = -1;
    private boolean isMerchantCopy = false;
    private ArrayList<String> receiptHeaders = null;
    private int terminalID = -1;
    private int orderTypeID = -1;
    private String transactionID = null;
    private String originalTranID = null;
    private String cashierName = null;
    private String transDate = null;
    private String transTime = null;
    private ArrayList<String> receiptFooters = null;
    private int userID = -1;
    private int revenueCenterID = -1;
    private boolean printOrderNumber = false;
    private boolean printNutritionInfo = false;
    private boolean printPLUCodes = false;
    private ArrayList<String> receiptAccountInfoLines = null;
    private boolean printTransactionBarcode = false;
    private String orderNumber = null;
    private int transactionStatusID = -1;
    private int paymentGatewayID = -1;
    private boolean printingNutritionalInfo = false;
    private ArrayList<NutritionItem> nutritionItems = null;
    private ArrayList<NutritionCategory> nutritionCategories = null;
    private ArrayList<NutritionTotal> nutritionTotals = null;
    private boolean needsNAFootnote = false;
    private boolean needsTruncateDigitsFootnote = false;
    private String emailAddress = null;
    private boolean printReceipt = false;
    private ArrayList<LoyaltyData> loyaltyDatas = null;
    private boolean isGiftReceipt = false;
    private String suspTransNameLbl = null;
    private String suspTransName = null;
    private boolean printGrossWeight = false;
    private boolean printNetWeight = false;
    private ArrayList<Combo> combos = null;
    private String productsJSON = null;
    private ArrayList<Plu> products = null;
    private ArrayList<Tender> tenders = null;
    private ArrayList<Discount> subtotalDiscounts = null;
    private ArrayList<Discount> itemDiscounts = null;
    private ArrayList<Tax> taxes = null;
    private ArrayList<PaidOut> paidOuts = null;
    private ArrayList<ReceivedOnAcct> receivedOnAccts = null;
    private ArrayList<LoyaltyReward> loyaltyRewards = null;
    private ArrayList<Surcharge> subtotalSurcharges = null;
    private ArrayList<Surcharge> itemSurcharges = null;

    /**
     * <p>Empty constructor for {@link CustomerReceiptData}.</p>
     *
     */
    public CustomerReceiptData () {}

    /**
     * <p>Builds a {@link CustomerReceiptData} from the given {@link XmlRpcStruct}.</p>
     *
     * @param customerReceiptDataStruct The {@link XmlRpcStruct} to use to build the {@link CustomerReceiptData}.
     * @return The {@link CustomerReceiptData} instance built form a {@link XmlRpcStruct}.
     */
    @SuppressWarnings({"unchecked", "Convert2streamapi", "Duplicates"})
    public static CustomerReceiptData buildFromXmlRpcStruct (XmlRpcStruct customerReceiptDataStruct) {

        // make sure the XmlRpcStruct is valid
        if (DataFunctions.isEmptyMap(customerReceiptDataStruct)) {
            Logger.logMessage("The XmlRpcStruct passed to CustomerReceiptData.buildFromXmlRpcStruct can't be null or empty!", CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<NutritionItem> nutritionItems = new ArrayList();
        ArrayList nutritionItemObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "NUTRITIONITEMS", true));
        if (!DataFunctions.isEmptyCollection(nutritionItemObjs)) {
            for (Object obj : nutritionItemObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    NutritionItem nutritionItem = NutritionItem.buildFromXmlRpcStruct(struct);
                    if (nutritionItem != null) {
                        nutritionItems.add(nutritionItem);
                    }
                }
            }
        }

        ArrayList<NutritionCategory> nutritionCategories = new ArrayList();
        ArrayList nutritionCategoryObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "NUTRITIONCATEGORIES", true));
        if (!DataFunctions.isEmptyCollection(nutritionCategoryObjs)) {
            for (Object obj : nutritionCategoryObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    NutritionCategory nutritionCategory = NutritionCategory.buildFromXmlRpcStruct(struct);
                    if (nutritionCategory != null) {
                        nutritionCategories.add(nutritionCategory);
                    }
                }
            }
        }

        ArrayList<NutritionTotal> nutritionTotals = new ArrayList();
        ArrayList nutritionTotalObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "NUTRITIONTOTALS", true));
        if (!DataFunctions.isEmptyCollection(nutritionTotalObjs)) {
            for (Object obj : nutritionTotalObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    NutritionTotal nutritionTotal = NutritionTotal.buildFromXmlRpcStruct(struct);
                    if (nutritionTotal != null) {
                        nutritionTotals.add(nutritionTotal);
                    }
                }
            }
        }

        ArrayList<LoyaltyData> loyaltyDatas = new ArrayList();
        ArrayList loyaltyDataObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "LOYALTYDATAS", true));
        if (!DataFunctions.isEmptyCollection(loyaltyDataObjs)) {
            for (Object obj : loyaltyDataObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    LoyaltyData loyaltyData = LoyaltyData.buildFromXmlRpcStruct(struct);
                    if (loyaltyData != null) {
                        loyaltyDatas.add(loyaltyData);
                    }
                }
            }
        }

        ArrayList<Combo> combos = new ArrayList<>();
        ArrayList comboObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "COMBOS", true));
        if (!DataFunctions.isEmptyCollection(comboObjs)) {
            for (Object obj : comboObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    Combo combo = Combo.buildFromXmlRpcStruct(struct);
                    if (combo != null) {
                        combos.add(combo);
                    }
                }
            }
        }

        ArrayList<Tender> tenders = new ArrayList<>();
        ArrayList tenderObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "TENDERS", true));
        if (!DataFunctions.isEmptyCollection(tenderObjs)) {
            for (Object obj : tenderObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    Tender tender = Tender.buildFromXmlRpcStruct(struct);
                    if (tender != null) {
                        tenders.add(tender);
                    }
                }
            }
        }

        ArrayList<Discount> subtotalDiscounts = new ArrayList<>();
        ArrayList subtotalDiscountObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "SUBTOTALDISCOUNTS", true));
        if (!DataFunctions.isEmptyCollection(subtotalDiscountObjs)) {
            for (Object obj : subtotalDiscountObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    Discount subtotalDiscount = Discount.buildFromXmlRpcStruct(struct);
                    if (subtotalDiscount != null) {
                        subtotalDiscounts.add(subtotalDiscount);
                    }
                }
            }
        }

        ArrayList<Discount> itemDiscounts = new ArrayList<>();
        ArrayList itemDiscountObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "ITEMDISCOUNTS", true));
        if (!DataFunctions.isEmptyCollection(itemDiscountObjs)) {
            for (Object obj : itemDiscountObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    Discount itemDiscount = Discount.buildFromXmlRpcStruct(struct);
                    if (itemDiscount != null) {
                        itemDiscounts.add(itemDiscount);
                    }
                }
            }
        }

        ArrayList<Tax> taxes = new ArrayList<>();
        ArrayList taxObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "TAXES", true));
        if (!DataFunctions.isEmptyCollection(taxObjs)) {
            for (Object obj : taxObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    Tax tax = Tax.buildFromXmlRpcStruct(struct);
                    if (tax != null) {
                        taxes.add(tax);
                    }
                }
            }
        }

        ArrayList<PaidOut> paidOuts = new ArrayList<>();
        ArrayList paidOutObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "PAIDOUTS", true));
        if (!DataFunctions.isEmptyCollection(paidOutObjs)) {
            for (Object obj : paidOutObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    PaidOut paidOut = PaidOut.buildFromXmlRpcStruct(struct);
                    if (paidOut != null) {
                        paidOuts.add(paidOut);
                    }
                }
            }
        }

        ArrayList<ReceivedOnAcct> receivedOnAccts = new ArrayList<>();
        ArrayList receivedOnAcctObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "RECEIVEDONACCTS", true));
        if (!DataFunctions.isEmptyCollection(receivedOnAcctObjs)) {
            for (Object obj : receivedOnAcctObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    ReceivedOnAcct receivedOnAcct = ReceivedOnAcct.buildFromXmlRpcStruct(struct);
                    if (receivedOnAcct != null) {
                        receivedOnAccts.add(receivedOnAcct);
                    }
                }
            }
        }

        ArrayList<LoyaltyReward> loyaltyRewards = new ArrayList<>();
        ArrayList loyaltyRewardObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "LOYALTYREWARDS", true));
        if (!DataFunctions.isEmptyCollection(loyaltyRewardObjs)) {
            for (Object obj : loyaltyRewardObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    LoyaltyReward loyaltyReward = LoyaltyReward.buildFromXmlRpcStruct(struct);
                    if (loyaltyReward != null) {
                        loyaltyRewards.add(loyaltyReward);
                    }
                }
            }
        }

        ArrayList<Surcharge> subtotalSurcharges = new ArrayList<>();
        ArrayList subtotalSurchargeObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "SUBTOTALSURCHARGES", true));
        if (!DataFunctions.isEmptyCollection(subtotalSurchargeObjs)) {
            for (Object obj : subtotalSurchargeObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    Surcharge subtotalSurcharge = Surcharge.buildFromXmlRpcStruct(struct);
                    if (subtotalSurcharge != null) {
                        subtotalSurcharges.add(subtotalSurcharge);
                    }
                }
            }
        }

        ArrayList<Surcharge> itemSurcharges = new ArrayList<>();
        ArrayList itemSurchargeObjs = ((ArrayList) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "ITEMSURCHARGES", true));
        if (!DataFunctions.isEmptyCollection(itemSurchargeObjs)) {
            for (Object obj : itemSurchargeObjs) {
                if ((obj != null) && (obj instanceof XmlRpcStruct)) {
                    XmlRpcStruct struct = ((XmlRpcStruct) obj);
                    Surcharge itemSurcharge = Surcharge.buildFromXmlRpcStruct(struct);
                    if (itemSurcharge != null) {
                        itemSurcharges.add(itemSurcharge);
                    }
                }
            }
        }

        // get the products from the products JSON
        Type productListType = new TypeToken<ArrayList<Plu>>(){}.getType();
        Gson gson = new GsonBuilder().registerTypeAdapter(productListType, new PluArrayListDeserializer()).create();
        String productsJSON = HashMapDataFns.getStringVal(customerReceiptDataStruct, "PRODUCTSJSON", true);
        ArrayList<Plu> products = gson.fromJson(productsJSON, productListType);

        return new CustomerReceiptData()
                .transTypeID(HashMapDataFns.getIntVal(customerReceiptDataStruct, "TRANSTYPEID", true))
                .trainingModeTransTypeID(HashMapDataFns.getIntVal(customerReceiptDataStruct, "TRAININGMODETRANSTYPEID", true))
                .isMerchantCopy(HashMapDataFns.getBooleanVal(customerReceiptDataStruct, "ISMERCHANTCOPY", true))
                .receiptHeaders(((ArrayList<String>) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "RECEIPTHEADERS", true)))
                .terminalID(HashMapDataFns.getIntVal(customerReceiptDataStruct, "TERMINALID", true))
                .orderTypeID(HashMapDataFns.getIntVal(customerReceiptDataStruct, "ORDERTYPEID", true))
                .transactionID(HashMapDataFns.getStringVal(customerReceiptDataStruct, "TRANSACTIONID", true))
                .originalTranID(HashMapDataFns.getStringVal(customerReceiptDataStruct, "ORIGINALTRANID", true))
                .cashierName(HashMapDataFns.getStringVal(customerReceiptDataStruct, "CASHIERNAME", true))
                .transDate(HashMapDataFns.getStringVal(customerReceiptDataStruct, "TRANSDATE", true))
                .transTime(HashMapDataFns.getStringVal(customerReceiptDataStruct, "TRANSTIME", true))
                .receiptFooters(((ArrayList<String>) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "RECEIPTFOOTERS", true)))
                .userID(HashMapDataFns.getIntVal(customerReceiptDataStruct, "USERID", true))
                .revenueCenterID(HashMapDataFns.getIntVal(customerReceiptDataStruct, "REVENUECENTERID", true))
                .printOrderNumber(HashMapDataFns.getBooleanVal(customerReceiptDataStruct, "PRINTORDERNUMBER", true))
                .printNutritionInfo(HashMapDataFns.getBooleanVal(customerReceiptDataStruct, "PRINTNUTRITIONINFO", true))
                .printPLUCodes(HashMapDataFns.getBooleanVal(customerReceiptDataStruct, "PRINTPLUCODES", true))
                .receiptAccountInfoLines(((ArrayList<String>) HashMapDataFns.getValFromMap(customerReceiptDataStruct, "RECEIPTACCOUNTINFOLINES", true)))
                .printTransactionBarcode(HashMapDataFns.getBooleanVal(customerReceiptDataStruct, "PRINTTRANSACTIONBARCODE", true))
                .orderNumber(HashMapDataFns.getStringVal(customerReceiptDataStruct, "ORDERNUMBER", true))
                .transactionStatusID(HashMapDataFns.getIntVal(customerReceiptDataStruct, "TRANSACTIONSTATUSID", true))
                .paymentGatewayID(HashMapDataFns.getIntVal(customerReceiptDataStruct, "PAYMENTGATEWAYID", true))
                .printingNutritionalInfo(HashMapDataFns.getBooleanVal(customerReceiptDataStruct, "PRINTINGNUTRITIONALINFO", true))
                .nutritionItems(nutritionItems)
                .nutritionCategories(nutritionCategories)
                .nutritionTotals(nutritionTotals)
                .needsNAFootnote(HashMapDataFns.getBooleanVal(customerReceiptDataStruct, "NEEDSNAFOOTNOTE", true))
                .needsTruncateDigitsFootnote(HashMapDataFns.getBooleanVal(customerReceiptDataStruct, "NEEDSTRUNCATEDIGITSFOOTNOTE", true))
                .emailAddress(HashMapDataFns.getStringVal(customerReceiptDataStruct, "EMAILADDRESS", true))
                .printReceipt(HashMapDataFns.getBooleanVal(customerReceiptDataStruct, "PRINTRECEIPT", true))
                .loyaltyDatas(loyaltyDatas)
                .isGiftReceipt(HashMapDataFns.getBooleanVal(customerReceiptDataStruct, "ISGIFTRECEIPT", true))
                .suspTransNameLbl(HashMapDataFns.getStringVal(customerReceiptDataStruct, "SUSPTRANSNAMELBL", true))
                .suspTransName(HashMapDataFns.getStringVal(customerReceiptDataStruct, "SUSPTRANSNAME", true))
                .printGrossWeight(HashMapDataFns.getBooleanVal(customerReceiptDataStruct, "PRINTGROSSWEIGHT", true))
                .printNetWeight(HashMapDataFns.getBooleanVal(customerReceiptDataStruct, "PRINTNETWEIGHT", true))
                .combos(combos)
                .productsJSON(productsJSON)
                .products(products)
                .tenders(tenders)
                .subtotalDiscounts(subtotalDiscounts)
                .itemDiscounts(itemDiscounts)
                .taxes(taxes)
                .paidOuts(paidOuts)
                .receivedOnAccts(receivedOnAccts)
                .loyaltyRewards(loyaltyRewards)
                .subtotalSurcharges(subtotalSurcharges)
                .itemSurcharges(itemSurcharges);
    }

    /**
     * <p>Builds a {@link CustomerReceiptData} from the given arguments.</p>
     *
     * @param receiptDetails An {@link ArrayList} containing details that should be added to the receipt.
     * @param printingNutritionalInfo Whether or not nutritional information will be printed on the receipt.
     * @param items An {@link ArrayList} containing nutrition items that should be added to the receipt.
     * @param categoryNames An {@link ArrayList} containing nutrition categories that should be added to the receipt.
     * @param nutriTotals An {@link ArrayList} containing nutrition totals that should be added to the receipt.
     * @param needsNAFootnote Whether or not to add the NA footnote to the receipt.
     * @param needsTruncateDigitsFootnote Whether or not to add the truncate digits footnote to the receipt.
     * @param emailAddress The email {@link String} that the receipt should be emailed to.
     * @param printReceipt Whether or not to print the receipt.
     * @param loyaltySummaryProgramData An {@link ArrayList} containing the loyalty information that should be printed on the receipt.
     * @param isGiftReceipt Whether or not the receipt is a gift receipt.
     * @param suspTransNameLbl The suspended transaction name label {@link String}.
     * @param suspTransName The suspended transaction name {@link String}.
     * @param printGrossWeight Whether or not to print the gross weight on the receipt.
     * @param printNetWeight Whether or not to print the net weight on the receipt.
     * @return The {@link CustomerReceiptData} instance built from the given arguments.
     */
    @SuppressWarnings({"unchecked", "Convert2streamapi", "ConstantConditions", "SpellCheckingInspection", "MagicNumber", "TypeMayBeWeakened", "OverlyComplexMethod"})
    public static CustomerReceiptData buildFromArgs (ArrayList receiptDetails,
                                                     boolean printingNutritionalInfo,
                                                     ArrayList items,
                                                     ArrayList categoryNames,
                                                     ArrayList nutriTotals,
                                                     boolean needsNAFootnote,
                                                     boolean needsTruncateDigitsFootnote,
                                                     String emailAddress,
                                                     boolean printReceipt,
                                                     ArrayList loyaltySummaryProgramData,
                                                     boolean isGiftReceipt,
                                                     String suspTransNameLbl,
                                                     String suspTransName,
                                                     boolean printGrossWeight,
                                                     boolean printNetWeight) {

        receiptDetails.forEach(rd -> Logger.logMessage("RD => "+rd.toString(), CUST_RCPT_LOG, Logger.LEVEL.IMPORTANT));

        // get data from within the receipt details
        int transTypeID = -1;
        int trainingModeTransTypeID = -1;
        boolean isMerchantCopy = false;
        ArrayList<String> receiptHeaders = new ArrayList<>();
        int terminalID = -1;
        String transactionID = "";
        String originalTranID = "";
        String cashierName = "";
        String transDate = "";
        String transTime = "";
        ArrayList<ArrayList> transDetails = new ArrayList<>();
        ArrayList<String> receiptFooters = new ArrayList<>();
        int userID = -1;
        int revenueCenterID = -1;
        boolean shouldPrintOrderNumber = false;
        boolean printNutritionInfo = false;
        boolean printPLUCodes = false;
        ArrayList<String> receiptAccountInfoLines = new ArrayList<>();
        boolean printTransactionBarcode = false;
        String orderNumber = "";
        int transactionStatusID = -1;
        int paymentGatewayID = -1;
        if (!DataFunctions.isEmptyCollection(receiptDetails)) {
            if ((receiptDetails.size() >= 1) && (NumberUtils.isNumber(receiptDetails.get(0).toString()))) {
                transTypeID = Integer.parseInt(receiptDetails.get(0).toString());
            }
            if ((receiptDetails.size() >= 2) && (NumberUtils.isNumber(receiptDetails.get(1).toString()))) {
                trainingModeTransTypeID = Integer.parseInt(receiptDetails.get(1).toString());
            }
            if ((receiptDetails.size() >= 4)) {
                isMerchantCopy = Boolean.parseBoolean(receiptDetails.get(3).toString());
            }
            if ((receiptDetails.size() >= 5) && (receiptDetails.get(4) instanceof ArrayList)) {
                receiptHeaders = ((ArrayList<String>) receiptDetails.get(4));
            }
            if ((receiptDetails.size() >= 6) && (NumberUtils.isNumber(receiptDetails.get(5).toString()))) {
                terminalID = Integer.parseInt(receiptDetails.get(5).toString());
            }
            if (receiptDetails.size() >= 7) {
                transactionID = receiptDetails.get(6).toString();
            }
            if (receiptDetails.size() >= 8) {
                originalTranID = receiptDetails.get(7).toString();
            }
            if (receiptDetails.size() >= 9) {
                cashierName = receiptDetails.get(8).toString();
            }
            if (receiptDetails.size() >= 10) {
                transDate = receiptDetails.get(9).toString();
            }
            if (receiptDetails.size() >= 11) {
                transTime = receiptDetails.get(10).toString();
            }
            if ((receiptDetails.size() >= 12) && (receiptDetails.get(11) instanceof ArrayList)) {
                transDetails = ((ArrayList<ArrayList>) receiptDetails.get(11));
            }
            if ((receiptDetails.size() >= 16) && (receiptDetails.get(15) instanceof ArrayList)) {
                receiptFooters = ((ArrayList<String>) receiptDetails.get(15));
            }
            if ((receiptDetails.size() >= 17) && (NumberUtils.isNumber(receiptDetails.get(16).toString()))) {
                userID = Integer.parseInt(receiptDetails.get(16).toString());
            }
            if ((receiptDetails.size() >= 18) && (NumberUtils.isNumber(receiptDetails.get(17).toString()))) {
                revenueCenterID = Integer.parseInt(receiptDetails.get(17).toString());
            }
            if ((receiptDetails.size() >= 19)) {
                shouldPrintOrderNumber = Boolean.parseBoolean(receiptDetails.get(18).toString());
            }
            if ((receiptDetails.size() >= 20)) {
                printNutritionInfo = Boolean.parseBoolean(receiptDetails.get(19).toString());
            }
            if ((receiptDetails.size() >= 21)) {
                printPLUCodes = Boolean.parseBoolean(receiptDetails.get(20).toString());
            }
            if ((receiptDetails.size() >= 22) && (receiptDetails.get(22) instanceof ArrayList)) {
                receiptAccountInfoLines = ((ArrayList<String>) receiptDetails.get(21));
            }
            if ((receiptDetails.size() >= 23)) {
                printTransactionBarcode = Boolean.parseBoolean(receiptDetails.get(22).toString());
            }
            if (receiptDetails.size() >= 24) {
                orderNumber = receiptDetails.get(23).toString();
            }
            if ((receiptDetails.size() >= 25) && (NumberUtils.isNumber(receiptDetails.get(24).toString()))) {
                transactionStatusID = Integer.parseInt(receiptDetails.get(24).toString());
            }
            if ((receiptDetails.size() >= 26) && (NumberUtils.isNumber(receiptDetails.get(25).toString()))) {
                paymentGatewayID = Integer.parseInt(receiptDetails.get(25).toString());
            }
        }

        // get the nutrition items
        ArrayList<NutritionItem> nutritionItems = new ArrayList<>();
        if (!DataFunctions.isEmptyCollection(items)) {
            for (Object itemObj : items) {
                if ((itemObj != null) && (itemObj instanceof ArrayList) && (!DataFunctions.isEmptyCollection((ArrayList) itemObj))) {
                    int quantity = -1;
                    String productName = "";
                    String nutritionInfo1 = "";
                    String nutritionInfo2 = "";
                    String nutritionInfo3 = "";
                    String nutritionInfo4 = "";
                    String nutritionInfo5 = "";
                    ArrayList itemElements = ((ArrayList) itemObj);
                    if ((itemElements.size() >= 1) && (NumberUtils.isNumber(itemElements.get(0).toString()))) {
                        quantity = Integer.parseInt(itemElements.get(0).toString());
                    }
                    if (itemElements.size() >= 2) {
                        productName = itemElements.get(1).toString();
                    }
                    if (itemElements.size() >= 3) {
                        nutritionInfo1 = itemElements.get(2).toString();
                    }
                    if (itemElements.size() >= 4) {
                        nutritionInfo2 = itemElements.get(3).toString();
                    }
                    if (itemElements.size() >= 5) {
                        nutritionInfo3 = itemElements.get(4).toString();
                    }
                    if (itemElements.size() >= 6) {
                        nutritionInfo4 = itemElements.get(5).toString();
                    }
                    if (itemElements.size() >= 7) {
                        nutritionInfo5 = itemElements.get(6).toString();
                    }
                    nutritionItems.add(new NutritionItem()
                            .quantity(quantity)
                            .productName(productName)
                            .nutritionInfo1(nutritionInfo1)
                            .nutritionInfo2(nutritionInfo2)
                            .nutritionInfo3(nutritionInfo3)
                            .nutritionInfo4(nutritionInfo4)
                            .nutritionInfo5(nutritionInfo5));
                }
            }
        }

        // get the nutrition categories
        ArrayList<NutritionCategory> nutritionCategories = new ArrayList<>();
        if (!DataFunctions.isEmptyCollection(categoryNames)) {
            for (Object categoryObj : categoryNames) {
                if ((categoryObj != null) && (categoryObj instanceof ArrayList) && (!DataFunctions.isEmptyCollection((ArrayList) categoryObj))) {
                    String shortName = "";
                    String measurementLabel = "";
                    ArrayList category = ((ArrayList) categoryObj);
                    if (category.size() >= 1) {
                        shortName = category.get(0).toString();
                    }
                    if (category.size() >= 2) {
                        measurementLabel = category.get(1).toString();
                    }
                    nutritionCategories.add(new NutritionCategory()
                            .shortName(shortName)
                            .measurementLabel(measurementLabel));
                }
            }
        }

        // get the nutrition totals
        ArrayList<NutritionTotal> nutritionTotals = new ArrayList<>();
        if (!DataFunctions.isEmptyCollection(nutriTotals)) {
            for (Object totalObj : nutriTotals) {
                if ((totalObj != null) && (totalObj instanceof ArrayList) && (!DataFunctions.isEmptyCollection((ArrayList) totalObj))) {
                    String dvCalLabel = "";
                    String nutritionInfo1 = "";
                    String nutritionInfo2 = "";
                    String nutritionInfo3 = "";
                    String nutritionInfo4 = "";
                    String nutritionInfo5 = "";
                    ArrayList total = ((ArrayList) totalObj);
                    if (total.size() >= 1) {
                        dvCalLabel = total.get(0).toString();
                    }
                    if (total.size() >= 2) {
                        nutritionInfo1 = total.get(1).toString();
                    }
                    if (total.size() >= 3) {
                        nutritionInfo2 = total.get(2).toString();
                    }
                    if (total.size() >= 4) {
                        nutritionInfo3 = total.get(3).toString();
                    }
                    if (total.size() >= 5) {
                        nutritionInfo4 = total.get(4).toString();
                    }
                    if (total.size() >= 6) {
                        nutritionInfo5 = total.get(5).toString();
                    }
                    nutritionTotals.add(new NutritionTotal()
                            .dvCalLabel(dvCalLabel)
                            .nutritionInfo1(nutritionInfo1)
                            .nutritionInfo2(nutritionInfo2)
                            .nutritionInfo3(nutritionInfo3)
                            .nutritionInfo4(nutritionInfo4)
                            .nutritionInfo5(nutritionInfo5));
                }
            }
        }

        ArrayList<LoyaltyData> loyaltyDatas = new ArrayList<>();
        if (!DataFunctions.isEmptyCollection(loyaltySummaryProgramData)) {
            String name = "";
            String badgeNumber = "";
            String accountNumber = "";
            for (int i = 0; i < loyaltySummaryProgramData.size(); i++) {
                if (i == 0) {
                    if ((loyaltySummaryProgramData.get(i) != null) && (loyaltySummaryProgramData.get(i) instanceof ArrayList)) {
                        ArrayList loyaltyUserInfo = ((ArrayList) loyaltySummaryProgramData.get(i));
                        if (!DataFunctions.isEmptyCollection(loyaltyUserInfo)) {
                            for (Object loyaltyUserInfoObj : loyaltyUserInfo) {
                                if ((loyaltyUserInfoObj != null) && (StringFunctions.stringHasContent(loyaltyUserInfoObj.toString()))) {
                                    String[] tokens = loyaltyUserInfoObj.toString().split(":");
                                    if ((!DataFunctions.isEmptyGenericArr(tokens)) && (tokens.length == 2)) {
                                        if (StringFunctions.rkContains("Name", tokens[0], 37)) {
                                            name = tokens[1];
                                        }
                                        else if (StringFunctions.rkContains("Badge", tokens[0], 37)) {
                                            badgeNumber = tokens[1];
                                        }
                                        else if (StringFunctions.rkContains("Acct No", tokens[0], 37)) {
                                            accountNumber = tokens[1];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    if ((loyaltySummaryProgramData.get(i) != null) && (loyaltySummaryProgramData.get(i) instanceof ArrayList)) {
                        ArrayList loyaltyRewardInfo = ((ArrayList) loyaltySummaryProgramData.get(i));
                        if (!DataFunctions.isEmptyCollection(loyaltyRewardInfo)) {
                            int loyaltyRewardID = -1;
                            if ((loyaltyRewardInfo.size() >= 1) && (NumberUtils.isNumber(loyaltyRewardInfo.get(0).toString()))) {
                                loyaltyRewardID = Integer.parseInt(loyaltyRewardInfo.get(0).toString());
                            }
                            String programName = "";
                            if (loyaltyRewardInfo.size() >= 2) {
                                programName = loyaltyRewardInfo.get(1).toString();
                            }
                            String pointsBalance = "";
                            if (loyaltyRewardInfo.size() >= 3) {
                                pointsBalance = loyaltyRewardInfo.get(2).toString();
                            }
                            String pointsEarned = "";
                            if (loyaltyRewardInfo.size() >= 4) {
                                pointsEarned = loyaltyRewardInfo.get(3).toString();
                            }
                            String pointsRedeemed = "";
                            if (loyaltyRewardInfo.size() >= 5) {
                                pointsRedeemed = loyaltyRewardInfo.get(4).toString();
                            }
                            loyaltyDatas.add(new LoyaltyData()
                                    .name(name)
                                    .badgeNumber(badgeNumber)
                                    .accountNumber(accountNumber)
                                    .loyaltyRewardID(loyaltyRewardID)
                                    .programName(programName)
                                    .pointsBalance(pointsBalance)
                                    .pointsEarned(pointsEarned)
                                    .pointsRedeemed(pointsRedeemed));
                        }
                    }
                }
            }
        }

        // get the transaction line items
        ArrayList<Combo> combos = new ArrayList<>();
        ArrayList<Plu> products = new ArrayList<>();
        ArrayList<Tender> tenders = new ArrayList<>();
        ArrayList<Discount> subtotalDiscounts = new ArrayList<>();
        ArrayList<Discount> itemDiscounts = new ArrayList<>();
        ArrayList<Tax> taxes = new ArrayList<>();
        ArrayList<PaidOut> paidOuts = new ArrayList<>();
        ArrayList<ReceivedOnAcct> receivedOnAccts = new ArrayList<>();
        ArrayList<LoyaltyReward> loyaltyRewards = new ArrayList<>();
        ArrayList<Surcharge> subtotalSurcharges = new ArrayList<>();
        ArrayList<Surcharge> itemSurcharges = new ArrayList<>();
        if (!DataFunctions.isEmptyCollection(transDetails)) {
            for (ArrayList transDetailAL : transDetails) {
                int itemTypeID = -1;
                if ((!DataFunctions.isEmptyCollection(transDetailAL)) && (NumberUtils.isNumber(transDetailAL.get(0).toString()))) {
                    itemTypeID = Integer.parseInt(transDetailAL.get(0).toString());
                }

                // create each transaction line item
                switch (itemTypeID) {
                    case TypeData.ItemType.COMBO:
                        combos.add(Combo.createComboFromXMLRPC(transDetailAL));
                        break;
                    case TypeData.ItemType.PLU:
                        products.add(Plu.createPluFromXMLRPC(transDetailAL));
                        break;
                    case TypeData.ItemType.TENDER:
                        tenders.add(Tender.createTenderFromXMLRPC(transDetailAL, transTypeID));
                        break;
                    case TypeData.ItemType.DISCOUNT:
                        Discount discount = Discount.createDiscountFromXMLRPC(transDetailAL, transTypeID);
                        if ((discount != null) && (discount.getIsSubtotalDsct())) {
                            subtotalDiscounts.add(discount);
                        }
                        else if ((discount != null) && (!discount.getIsSubtotalDsct())) {
                            itemDiscounts.add(discount);
                            // assign the discount to the previous parent product
                            if (!DataFunctions.isEmptyCollection(products)) {
                                for (int i = products.size() - 1; i >= 0; i--) {
                                    if ((products.get(i) != null) && (!products.get(i).getIsModifier())) {
                                        products.get(i).setItemDiscount(discount);
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    case TypeData.ItemType.TAX:
                        taxes.add(Tax.createTaxFromXMLRPC(transDetailAL, transTypeID));
                        break;
                    case TypeData.ItemType.PAIDOUT:
                        paidOuts.add(PaidOut.createPaidOutFromXMLRPC(transDetailAL));
                        break;
                    case TypeData.ItemType.RA:
                        receivedOnAccts.add(ReceivedOnAcct.createRAItemFromXMLRPC(transDetailAL, transTypeID));
                        break;
                    case TypeData.ItemType.LOYALTY_REWARD:
                        loyaltyRewards.add(LoyaltyReward.createRewardFromXMLRPC(transDetailAL, transTypeID));
                        break;
                    case TypeData.ItemType.SURCHARGE:
                        Surcharge surcharge = Surcharge.createSurchargeFromXMLRPC(transDetailAL, transTypeID);
                        if ((surcharge != null) && (surcharge.isSubtotalSurcharge())) {
                            subtotalSurcharges.add(surcharge);
                        }
                        else if ((surcharge != null) && (!surcharge.isSubtotalSurcharge())) {
                            itemSurcharges.add(surcharge);
                        }
                        break;
                    default:
                        Logger.logMessage(String.format("Encountered an invalid item type ID of %s in CustomerReceiptData.buildFromArgs!",
                                Objects.toString(itemTypeID, "N/A")), CUST_RCPT_LOG, Logger.LEVEL.ERROR);
                        break;
                }
            }
        }

        // set parent products for modifier products
        if (!DataFunctions.isEmptyCollection(products)) {
            for (Plu product : products) {
                if (product.getIsModifier()) {
                    int paTransLineItemID = product.getPATransLineItemID();
                    Plu parentProduct = null;
                    for (Plu potentialParent : products) {
                        if ((potentialParent != null) && (potentialParent.getPATransLineItemID() == paTransLineItemID) && (!potentialParent.getIsModifier())) {
                            parentProduct = potentialParent;
                            break;
                        }
                    }
                    if (parentProduct != null) {
                        product.setParentProduct(parentProduct);
                    }
                }
            }
        }

        String productsJSON = "";
        if (!DataFunctions.isEmptyCollection(products)) {
            Gson gson = new GsonBuilder().registerTypeAdapter(Plu.class, new PluAdapter()).create();
            JsonArray productsJSONArr = new JsonArray();
            for (Plu product : products) {
                JsonObject productJSON = gson.toJsonTree(product, Plu.class).getAsJsonObject();
                productsJSONArr.add(productJSON);
            }
            productsJSON = gson.toJson(productsJSONArr);
        }

        return new CustomerReceiptData()
                .transTypeID(transTypeID)
                .trainingModeTransTypeID(trainingModeTransTypeID)
                .isMerchantCopy(isMerchantCopy)
                .receiptHeaders(receiptHeaders)
                .terminalID(terminalID)
                .orderTypeID(-1)
                .transactionID(transactionID)
                .originalTranID(originalTranID)
                .cashierName(cashierName)
                .transDate(transDate)
                .transTime(transTime)
                .receiptFooters(receiptFooters)
                .userID(userID)
                .revenueCenterID(revenueCenterID)
                .printOrderNumber(shouldPrintOrderNumber)
                .printNutritionInfo(printNutritionInfo)
                .printPLUCodes(printPLUCodes)
                .receiptAccountInfoLines(receiptAccountInfoLines)
                .printTransactionBarcode(printTransactionBarcode)
                .orderNumber(orderNumber)
                .transactionStatusID(transactionStatusID)
                .paymentGatewayID(paymentGatewayID)
                .printingNutritionalInfo(printingNutritionalInfo)
                .nutritionItems(nutritionItems)
                .nutritionCategories(nutritionCategories)
                .nutritionTotals(nutritionTotals)
                .needsNAFootnote(needsNAFootnote)
                .needsTruncateDigitsFootnote(needsTruncateDigitsFootnote)
                .emailAddress(emailAddress)
                .printReceipt(printReceipt)
                .loyaltyDatas(loyaltyDatas)
                .isGiftReceipt(isGiftReceipt)
                .suspTransNameLbl(suspTransNameLbl)
                .suspTransName(suspTransName)
                .printGrossWeight(printGrossWeight)
                .printNetWeight(printNetWeight)
                .combos(combos)
                .productsJSON(productsJSON)
                .products(null)
                .tenders(tenders)
                .subtotalDiscounts(subtotalDiscounts)
                .itemDiscounts(itemDiscounts)
                .taxes(taxes)
                .paidOuts(paidOuts)
                .receivedOnAccts(receivedOnAccts)
                .loyaltyRewards(loyaltyRewards)
                .subtotalSurcharges(subtotalSurcharges)
                .itemSurcharges(itemSurcharges);
    }

//    /**
//     * <p>Gets the order type ID of the transaction.</p>
//     *
//     * @param transactionID The {@link String} ID of the transaction to get the order type ID for.
//     * @return The order type ID of the given transaction.
//     */
//    public static int getOrderTypeID (String transactionID) {
//
//        // make sure the transaction ID is valid
//        if (!StringFunctions.stringHasContent(transactionID)) {
//            Logger.logMessage("The transaction ID passed to CustomerReceiptData.getOrderTypeID can't be null or empty, unable to continue, now returning -1.", CUST_RCPT_LOG, Logger.LEVEL.ERROR);
//            return -1;
//        }
//
//        // try to get the order type ID from the local database
//        DynamicSQL sql = new DynamicSQL("data.kms.GetOrderTypeIDOfTransaction").addIDList(1, transactionID).setLogFileName(CUST_RCPT_LOG);
//        int orderTypeID = sql.getSingleIntField(new DataManager());
//
//        // try to get the order type ID from the server if we couldn't get it from the local database
//        if (orderTypeID <= 0) {
//            // make XML-RPC call to the server
//            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
//            String macAddress = PeripheralsDataManager.getPHMacAddress();
//            String hostname = PeripheralsDataManager.getPHHostname();
//            String method = KitchenPrinterDataManagerMethod.GET_ORDER_TYPE_ID_FOR_TRANSACTION.getMethodName();
//            Object[] args = new Object[]{hostname, transactionID};
//            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, method, args);
//            String orderTypeIDStr = XmlRpcUtil.getInstance().parseStringXmlRpcResponse(xmlRpcResponse, method);
//            if ((StringFunctions.stringHasContent(orderTypeIDStr)) && (NumberUtils.isNumber(orderTypeIDStr))) {
//                orderTypeID = Integer.parseInt(orderTypeIDStr);
//            }
//        }
//
//        return orderTypeID;
//    }

    /**
     * <p>Overridden toString() method for a {@link CustomerReceiptData} instance.</p>
     *
     * @return The {@link CustomerReceiptData} instance as a {@link String};
     */
    @SuppressWarnings("OverlyComplexMethod")
    @Override
    public String toString () {

    return String.format("TRANSTYPEID: %s, TRAININGMODETRANSTYPEID: %s, ISMERCHANTCOPY: %s, RECEIPTHEADERS: %s, TERMINALID: %s, ORDERTYPEID: %s, " +
            "TRANSACTIONID: %s, ORIGINALTRANID: %s, CASHIERNAME: %s, TRANSDATE: %s, TRANSTIME: %s, RECEIPTFOOTERS: %s, USERID: %s, " +
            "REVENUECENTERID: %s, PRINTORDERNUMBER: %s, PRINTNUTRITIONINFO: %s, PRINTPLUCODES: %s, RECEIPTACCOUNTINFOLINES: %s, " +
            "PRINTTRANSACTIONBARCODE: %s, ORDERNUMBER: %s, TRANSACTIONSTATUSID: %s, PAYMENTGATEWAYID: %s, PRINTINGNUTRITIONALINFO: %s, " +
            "NUTRITIONITEMS: %s, NUTRITIONCATEGORIES: %s, NUTRITIONTOTALS: %s, NEEDSNAFOOTNOTE: %s, NEEDSTRUNCATEDIGITSFOOTNOTE: %s, " +
            "EMAILADDRESS: %s, PRINTRECEIPT: %s, LOYALTYDATAS: %s, ISGIFTRECEIPT: %s, SUSPTRANSNAMELBL: %s, SUSPTRANSNAME: %s, " +
            "PRINTGROSSWEIGHT: %s, PRINTNETWEIGHT: %s, COMBOS: %s, PRODUCTS: %s, TENDERS: %s, SUBTOTALDISCOUNTS: %s, ITEMDISCOUNTS: %s, " +
            "TAXES: %s, PAIDOUTS: %s, RECEIVEDONACCTS: %s, LOYALTYREWARDS: %s, SUBTOTALSURCHARGES: %s, ITEMSURCHARGES: %s",
            Objects.toString((transTypeID > 0 ? transTypeID : "N/A"), "N/A"),
            Objects.toString((trainingModeTransTypeID > 0 ? trainingModeTransTypeID : "N/A"), "N/A"),
            Objects.toString(isMerchantCopy, "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(receiptHeaders) ? StringFunctions.collectionToBracketedString(receiptHeaders) : "N/A"), "N/A"),
            Objects.toString((terminalID > 0 ? terminalID : "N/A"), "N/A"),
            Objects.toString((orderTypeID > 0 ? orderTypeID : "N/A"), "N/A"),
            Objects.toString((StringFunctions.stringHasContent(transactionID) ? transactionID : "N/A"), "N/A"),
            Objects.toString((StringFunctions.stringHasContent(originalTranID) ? originalTranID : "N/A"), "N/A"),
            Objects.toString((StringFunctions.stringHasContent(cashierName) ? cashierName : "N/A"), "N/A"),
            Objects.toString((StringFunctions.stringHasContent(transDate) ? transDate : "N/A"), "N/A"),
            Objects.toString((StringFunctions.stringHasContent(transTime) ? transTime : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(receiptFooters) ? StringFunctions.collectionToBracketedString(receiptFooters) : "N/A"), "N/A"),
            Objects.toString((userID > 0 ? userID : "N/A"), "N/A"),
            Objects.toString((revenueCenterID > 0 ? revenueCenterID : "N/A"), "N/A"),
            Objects.toString(printOrderNumber, "N/A"),
            Objects.toString(printNutritionInfo, "N/A"),
            Objects.toString(printPLUCodes, "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(receiptAccountInfoLines) ? StringFunctions.collectionToBracketedString(receiptAccountInfoLines) : "N/A"), "N/A"),
            Objects.toString(printTransactionBarcode, "N/A"),
            Objects.toString((StringFunctions.stringHasContent(orderNumber) ? orderNumber : "N/A"), "N/A"),
            Objects.toString((transactionStatusID > 0 ? transactionStatusID : "N/A"), "N/A"),
            Objects.toString((paymentGatewayID > 0 ? paymentGatewayID : "N/A"), "N/A"),
            Objects.toString(printingNutritionalInfo, "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(nutritionItems) ? StringFunctions.collectionToBracketedString(nutritionItems) : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(nutritionCategories) ? StringFunctions.collectionToBracketedString(nutritionCategories) : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(nutritionTotals) ? StringFunctions.collectionToBracketedString(nutritionTotals) : "N/A"), "N/A"),
            Objects.toString(needsNAFootnote, "N/A"),
            Objects.toString(needsTruncateDigitsFootnote, "N/A"),
            Objects.toString((StringFunctions.stringHasContent(emailAddress) ? emailAddress : "N/A"), "N/A"),
            Objects.toString(printReceipt, "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(loyaltyDatas) ? StringFunctions.collectionToBracketedString(loyaltyDatas) : "N/A"), "N/A"),
            Objects.toString(isGiftReceipt, "N/A"),
            Objects.toString((StringFunctions.stringHasContent(suspTransNameLbl) ? suspTransNameLbl : "N/A"), "N/A"),
            Objects.toString((StringFunctions.stringHasContent(suspTransName) ? suspTransName : "N/A"), "N/A"),
            Objects.toString(printGrossWeight, "N/A"),
            Objects.toString(printNetWeight, "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(combos) ? StringFunctions.collectionToBracketedString(combos) : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(products) ? StringFunctions.collectionToBracketedString(products) : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(tenders) ? StringFunctions.collectionToBracketedString(tenders) : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(subtotalDiscounts) ? StringFunctions.collectionToBracketedString(subtotalDiscounts) : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(itemDiscounts) ? StringFunctions.collectionToBracketedString(itemDiscounts) : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(taxes) ? StringFunctions.collectionToBracketedString(taxes) : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(paidOuts) ? StringFunctions.collectionToBracketedString(paidOuts) : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(receivedOnAccts) ? StringFunctions.collectionToBracketedString(receivedOnAccts) : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(loyaltyRewards) ? StringFunctions.collectionToBracketedString(loyaltyRewards) : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(subtotalSurcharges) ? StringFunctions.collectionToBracketedString(subtotalSurcharges) : "N/A"), "N/A"),
            Objects.toString((!DataFunctions.isEmptyCollection(itemSurcharges) ? StringFunctions.collectionToBracketedString(itemSurcharges) : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link CustomerReceiptData} instance.</p>
     *
     * @param obj An {@link Object} to compare against the {@link CustomerReceiptData} instance.
     * @return Whether or not the {@link Object} is equal to the {@link CustomerReceiptData} instance.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a CustomerReceiptData
        CustomerReceiptData customerReceiptData = (CustomerReceiptData) obj;
        return Objects.equals(customerReceiptData.transactionID, transactionID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link CustomerReceiptData} instance.</p>
     *
     * @return The hash code for a {@link CustomerReceiptData} instance.
     */
    @Override
    public int hashCode () {
        return Objects.hash(transactionID);
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its transTypeID field set.</p>
     *
     * @param transTypeID The transaction type ID.
     * @return The {@link CustomerReceiptData} instance with it's transTypeID field set.
     */
    public CustomerReceiptData transTypeID (int transTypeID) {
        this.transTypeID = transTypeID;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its trainingModeTransTypeID field set.</p>
     *
     * @param trainingModeTransTypeID The training mode transaction type ID.
     * @return The {@link CustomerReceiptData} instance with it's trainingModeTransTypeID field set.
     */
    public CustomerReceiptData trainingModeTransTypeID (int trainingModeTransTypeID) {
        this.trainingModeTransTypeID = trainingModeTransTypeID;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its isMerchantCopy field set.</p>
     *
     * @param isMerchantCopy Whether or not the receipt is for a customer or merchant.
     * @return The {@link CustomerReceiptData} instance with it's isMerchantCopy field set.
     */
    public CustomerReceiptData isMerchantCopy (boolean isMerchantCopy) {
        this.isMerchantCopy = isMerchantCopy;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its receiptHeaders field set.</p>
     *
     * @param receiptHeaders An {@link ArrayList} of {@link String} corresponding to the receipt headers.
     * @return The {@link CustomerReceiptData} instance with it's receiptHeaders field set.
     */
    public CustomerReceiptData receiptHeaders (ArrayList<String> receiptHeaders) {
        this.receiptHeaders = receiptHeaders;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its terminalID field set.</p>
     *
     * @param terminalID The ID of the terminal on which the transaction took place.
     * @return The {@link CustomerReceiptData} instance with it's terminalID field set.
     */
    public CustomerReceiptData terminalID (int terminalID) {
        this.terminalID = terminalID;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its orderTypeID field set.</p>
     *
     * @param orderTypeID The ID of the order type on which the transaction took place.
     * @return The {@link CustomerReceiptData} instance with it's orderTypeID field set.
     */
    public CustomerReceiptData orderTypeID (int orderTypeID) {
        this.orderTypeID = orderTypeID;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its transactionID field set.</p>
     *
     * @param transactionID The ID {@link String} of the transaction.
     * @return The {@link CustomerReceiptData} instance with it's transactionID field set.
     */
    public CustomerReceiptData transactionID (String transactionID) {
        this.transactionID = transactionID;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its originalTranID field set.</p>
     *
     * @param originalTranID The original ID {@link String} of the transaction.
     * @return The {@link CustomerReceiptData} instance with it's originalTranID field set.
     */
    public CustomerReceiptData originalTranID (String originalTranID) {
        this.originalTranID = originalTranID;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its cashierName field set.</p>
     *
     * @param cashierName The name {@link String} of the cashier.
     * @return The {@link CustomerReceiptData} instance with it's cashierName field set.
     */
    public CustomerReceiptData cashierName (String cashierName) {
        this.cashierName = cashierName;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its transDate field set.</p>
     *
     * @param transDate The date on when the transaction took place as a {@link String}.
     * @return The {@link CustomerReceiptData} instance with it's transDate field set.
     */
    public CustomerReceiptData transDate (String transDate) {
        this.transDate = transDate;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its transTime field set.</p>
     *
     * @param transTime The time when the transaction took place as a {@link String}.
     * @return The {@link CustomerReceiptData} instance with it's transTime field set.
     */
    public CustomerReceiptData transTime (String transTime) {
        this.transTime = transTime;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its receiptFooters field set.</p>
     *
     * @param receiptFooters An {@link ArrayList} of {@link String} corresponding to the receipt footers.
     * @return The {@link CustomerReceiptData} instance with it's receiptFooters field set.
     */
    public CustomerReceiptData receiptFooters (ArrayList<String> receiptFooters) {
        this.receiptFooters = receiptFooters;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its userID field set.</p>
     *
     * @param userID The ID of the user.
     * @return The {@link CustomerReceiptData} instance with it's userID field set.
     */
    public CustomerReceiptData userID (int userID) {
        this.userID = userID;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its revenueCenterID field set.</p>
     *
     * @param revenueCenterID The ID of the revenue center in which the transaction took place.
     * @return The {@link CustomerReceiptData} instance with it's revenueCenterID field set.
     */
    public CustomerReceiptData revenueCenterID (int revenueCenterID) {
        this.revenueCenterID = revenueCenterID;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its printOrderNumber field set.</p>
     *
     * @param printOrderNumber Whether or not to print the order number.
     * @return The {@link CustomerReceiptData} instance with it's printOrderNumber field set.
     */
    public CustomerReceiptData printOrderNumber (boolean printOrderNumber) {
        this.printOrderNumber = printOrderNumber;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its printNutritionInfo field set.</p>
     *
     * @param printNutritionInfo Whether or not to print the nutrition info.
     * @return The {@link CustomerReceiptData} instance with it's printNutritionInfo field set.
     */
    public CustomerReceiptData printNutritionInfo (boolean printNutritionInfo) {
        this.printNutritionInfo = printNutritionInfo;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its printPLUCodes field set.</p>
     *
     * @param printPLUCodes Whether or not to print PLU codes.
     * @return The {@link CustomerReceiptData} instance with it's printPLUCodes field set.
     */
    public CustomerReceiptData printPLUCodes (boolean printPLUCodes) {
        this.printPLUCodes = printPLUCodes;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its receiptAccountInfoLines field set.</p>
     *
     * @param receiptAccountInfoLines An {@link ArrayList} of {@link String} corresponding to the receipt account info lines.
     * @return The {@link CustomerReceiptData} instance with it's receiptAccountInfoLines field set.
     */
    public CustomerReceiptData receiptAccountInfoLines (ArrayList<String> receiptAccountInfoLines) {
        this.receiptAccountInfoLines = receiptAccountInfoLines;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its printTransactionBarcode field set.</p>
     *
     * @param printTransactionBarcode Whether or not to print the transaction barcode.
     * @return The {@link CustomerReceiptData} instance with it's printTransactionBarcode field set.
     */
    public CustomerReceiptData printTransactionBarcode (boolean printTransactionBarcode) {
        this.printTransactionBarcode = printTransactionBarcode;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its orderNumber field set.</p>
     *
     * @param orderNumber The order number as a {@link String}.
     * @return The {@link CustomerReceiptData} instance with it's orderNumber field set.
     */
    public CustomerReceiptData orderNumber (String orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its transactionStatusID field set.</p>
     *
     * @param transactionStatusID The transaction status ID.
     * @return The {@link CustomerReceiptData} instance with it's transactionStatusID field set.
     */
    public CustomerReceiptData transactionStatusID (int transactionStatusID) {
        this.transactionStatusID = transactionStatusID;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with its paymentGatewayID field set.</p>
     *
     * @param paymentGatewayID The payment gateway ID.
     * @return The {@link CustomerReceiptData} instance with it's paymentGatewayID field set.
     */
    public CustomerReceiptData paymentGatewayID (int paymentGatewayID) {
        this.paymentGatewayID = paymentGatewayID;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's printingNutritionalInfo field set.</p>
     *
     * @param printingNutritionalInfo Whether or not to print nutritional information.
     * @return The {@link CustomerReceiptData} instance with it's printingNutritionalInfo field set.
     */
    public CustomerReceiptData printingNutritionalInfo (boolean printingNutritionalInfo) {
        this.printingNutritionalInfo = printingNutritionalInfo;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's nutritionItems field set.</p>
     *
     * @param nutritionItems An {@link ArrayList} of {@link NutritionItem} within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's nutritionItems field set.
     */
    public CustomerReceiptData nutritionItems (ArrayList<NutritionItem> nutritionItems) {
        this.nutritionItems = nutritionItems;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's nutritionCategories field set.</p>
     *
     * @param nutritionCategories An {@link ArrayList} of {@link NutritionCategory} within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's nutritionCategories field set.
     */
    public CustomerReceiptData nutritionCategories (ArrayList<NutritionCategory> nutritionCategories) {
        this.nutritionCategories = nutritionCategories;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's nutritionTotals field set.</p>
     *
     * @param nutritionTotals An {@link ArrayList} of {@link NutritionTotal} within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's nutritionTotals field set.
     */
    public CustomerReceiptData nutritionTotals (ArrayList<NutritionTotal> nutritionTotals) {
        this.nutritionTotals = nutritionTotals;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's needsNAFootnote field set.</p>
     *
     * @param needsNAFootnote Whether or not the receipt needs the NA footnote.
     * @return The {@link CustomerReceiptData} instance with it's needsNAFootnote field set.
     */
    public CustomerReceiptData needsNAFootnote (boolean needsNAFootnote) {
        this.needsNAFootnote = needsNAFootnote;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's needsTruncateDigitsFootnote field set.</p>
     *
     * @param needsTruncateDigitsFootnote Whether or not the receipt needs the truncate digits footnote.
     * @return The {@link CustomerReceiptData} instance with it's needsTruncateDigitsFootnote field set.
     */
    public CustomerReceiptData needsTruncateDigitsFootnote (boolean needsTruncateDigitsFootnote) {
        this.needsTruncateDigitsFootnote = needsTruncateDigitsFootnote;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's emailAddress field set.</p>
     *
     * @param emailAddress The email address {@link String} the receipt should be emailed to.
     * @return The {@link CustomerReceiptData} instance with it's emailAddress field set.
     */
    public CustomerReceiptData emailAddress (String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's printReceipt field set.</p>
     *
     * @param printReceipt Whether or not to print the receipt.
     * @return The {@link CustomerReceiptData} instance with it's printReceipt field set.
     */
    public CustomerReceiptData printReceipt (boolean printReceipt) {
        this.printReceipt = printReceipt;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's loyaltyDatas field set.</p>
     *
     * @param loyaltyDatas An {@link ArrayList} of {@link LoyaltyData} corresponding to the loyalty programs applied to the transaction.
     * @return The {@link CustomerReceiptData} instance with it's loyaltyDatas field set.
     */
    public CustomerReceiptData loyaltyDatas (ArrayList<LoyaltyData> loyaltyDatas) {
        this.loyaltyDatas = loyaltyDatas;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's isGiftReceipt field set.</p>
     *
     * @param isGiftReceipt Whether or not the receipt is a gift receipt.
     * @return The {@link CustomerReceiptData} instance with it's isGiftReceipt field set.
     */
    public CustomerReceiptData isGiftReceipt (boolean isGiftReceipt) {
        this.isGiftReceipt = isGiftReceipt;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's suspTransNameLbl field set.</p>
     *
     * @param suspTransNameLbl The suspended transaction name label {@link String}.
     * @return The {@link CustomerReceiptData} instance with it's suspTransNameLbl field set.
     */
    public CustomerReceiptData suspTransNameLbl (String suspTransNameLbl) {
        this.suspTransNameLbl = suspTransNameLbl;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's suspTransName field set.</p>
     *
     * @param suspTransName The suspended transaction name {@link String}.
     * @return The {@link CustomerReceiptData} instance with it's suspTransName field set.
     */
    public CustomerReceiptData suspTransName (String suspTransName) {
        this.suspTransName = suspTransName;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's printGrossWeight field set.</p>
     *
     * @param printGrossWeight  Whether or not to print gross weight on the receipt.
     * @return The {@link CustomerReceiptData} instance with it's printGrossWeight field set.
     */
    public CustomerReceiptData printGrossWeight (boolean printGrossWeight) {
        this.printGrossWeight = printGrossWeight;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's printNetWeight field set.</p>
     *
     * @param printNetWeight Whether or not to print net weight on the receipt.
     * @return The {@link CustomerReceiptData} instance with it's printNetWeight field set.
     */
    public CustomerReceiptData printNetWeight (boolean printNetWeight) {
        this.printNetWeight = printNetWeight;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's combos field set.</p>
     *
     * @param combos An {@link ArrayList} of {@link Combo} corresponding to the combo line items within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's combos field set.
     */
    public CustomerReceiptData combos (ArrayList<Combo> combos) {
        this.combos = combos;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's productsJSON field set.</p>
     *
     * @param productsJSON A {@link String} containing the product line items within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's productsJSON field set.
     */
    public CustomerReceiptData productsJSON (String productsJSON) {
        this.productsJSON = productsJSON;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's products field set.</p>
     *
     * @param products An {@link ArrayList} of {@link Plu} corresponding to the product line items within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's products field set.
     */
    public CustomerReceiptData products (ArrayList<Plu> products) {
        this.products = products;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's tenders field set.</p>
     *
     * @param tenders An {@link ArrayList} of {@link Tender} corresponding to the tender line items within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's tenders field set.
     */
    public CustomerReceiptData tenders (ArrayList<Tender> tenders) {
        this.tenders = tenders;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's subtotalDiscounts field set.</p>
     *
     * @param subtotalDiscounts An {@link ArrayList} of {@link Discount} corresponding to the subtotal discount line items within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's subtotalDiscounts field set.
     */
    public CustomerReceiptData subtotalDiscounts (ArrayList<Discount> subtotalDiscounts) {
        this.subtotalDiscounts = subtotalDiscounts;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's itemDiscounts field set.</p>
     *
     * @param itemDiscounts An {@link ArrayList} of {@link Discount} corresponding to the item discount line items within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's itemDiscounts field set.
     */
    public CustomerReceiptData itemDiscounts (ArrayList<Discount> itemDiscounts) {
        this.itemDiscounts = itemDiscounts;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's taxes field set.</p>
     *
     * @param taxes An {@link ArrayList} of {@link Tax} corresponding to the tax line items within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's taxes field set.
     */
    public CustomerReceiptData taxes (ArrayList<Tax> taxes) {
        this.taxes = taxes;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's paidOuts field set.</p>
     *
     * @param paidOuts An {@link ArrayList} of {@link PaidOut} corresponding to the paid out line items within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's paidOuts field set.
     */
    public CustomerReceiptData paidOuts (ArrayList<PaidOut> paidOuts) {
        this.paidOuts = paidOuts;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's receivedOnAccts field set.</p>
     *
     * @param receivedOnAccts An {@link ArrayList} of {@link ReceivedOnAcct} corresponding to the received on account line items within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's receivedOnAccts field set.
     */
    public CustomerReceiptData receivedOnAccts (ArrayList<ReceivedOnAcct> receivedOnAccts) {
        this.receivedOnAccts = receivedOnAccts;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's loyaltyRewards field set.</p>
     *
     * @param loyaltyRewards An {@link ArrayList} of {@link LoyaltyReward} corresponding to the loyalty reward line items within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's loyaltyRewards field set.
     */
    public CustomerReceiptData loyaltyRewards (ArrayList<LoyaltyReward> loyaltyRewards) {
        this.loyaltyRewards = loyaltyRewards;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's subtotalSurcharges field set.</p>
     *
     * @param subtotalSurcharges An {@link ArrayList} of {@link Surcharge} corresponding to the subtotal surcharge line items within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's subtotalSurcharges field set.
     */
    public CustomerReceiptData subtotalSurcharges (ArrayList<Surcharge> subtotalSurcharges) {
        this.subtotalSurcharges = subtotalSurcharges;
        return this;
    }

    /**
     * <p>Constructor for a {@link CustomerReceiptData} with it's itemSurcharges field set.</p>
     *
     * @param itemSurcharges An {@link ArrayList} of {@link Surcharge} corresponding to the item surcharge line items within the transaction.
     * @return The {@link CustomerReceiptData} instance with it's itemSurcharges field set.
     */
    public CustomerReceiptData itemSurcharges (ArrayList<Surcharge> itemSurcharges) {
        this.itemSurcharges = itemSurcharges;
        return this;
    }


    /**
     * <p>Getter for the transTypeID field of the {@link CustomerReceiptData}.</p>
     *
     * @return The transTypeID field of the {@link CustomerReceiptData}.
     */
    public int getTransTypeID () {
        return transTypeID;
    }

    /**
     * <p>Setter for the transTypeID field of the {@link CustomerReceiptData}.</p>
     *
     * @param transTypeID The transTypeID field of the {@link CustomerReceiptData}.
     */
    public void setTransTypeID (int transTypeID) {
        this.transTypeID = transTypeID;
    }

    /**
     * <p>Getter for the trainingModeTransTypeID field of the {@link CustomerReceiptData}.</p>
     *
     * @return The trainingModeTransTypeID field of the {@link CustomerReceiptData}.
     */
    public int getTrainingModeTransTypeID () {
        return trainingModeTransTypeID;
    }

    /**
     * <p>Setter for the trainingModeTransTypeID field of the {@link CustomerReceiptData}.</p>
     *
     * @param trainingModeTransTypeID The trainingModeTransTypeID field of the {@link CustomerReceiptData}.
     */
    public void setTrainingModeTransTypeID (int trainingModeTransTypeID) {
        this.trainingModeTransTypeID = trainingModeTransTypeID;
    }

    /**
     * <p>Getter for the isMerchantCopy field of the {@link CustomerReceiptData}.</p>
     *
     * @return The isMerchantCopy field of the {@link CustomerReceiptData}.
     */
    public boolean getIsMerchantCopy () {
        return isMerchantCopy;
    }

    /**
     * <p>Setter for the isMerchantCopy field of the {@link CustomerReceiptData}.</p>
     *
     * @param isMerchantCopy The isMerchantCopy field of the {@link CustomerReceiptData}.
     */
    public void setIsMerchantCopy (boolean isMerchantCopy) {
        this.isMerchantCopy = isMerchantCopy;
    }

    /**
     * <p>Getter for the receiptHeaders field of the {@link CustomerReceiptData}.</p>
     *
     * @return The receiptHeaders field of the {@link CustomerReceiptData}.
     */
    public ArrayList<String> getReceiptHeaders () {
        return receiptHeaders;
    }

    /**
     * <p>Setter for the receiptHeaders field of the {@link CustomerReceiptData}.</p>
     *
     * @param receiptHeaders The receiptHeaders field of the {@link CustomerReceiptData}.
     */
    public void setReceiptHeaders (ArrayList<String> receiptHeaders) {
        this.receiptHeaders = receiptHeaders;
    }

    /**
     * <p>Getter for the terminalID field of the {@link CustomerReceiptData}.</p>
     *
     * @return The terminalID field of the {@link CustomerReceiptData}.
     */
    public int getTerminalID () {
        return terminalID;
    }

    /**
     * <p>Setter for the terminalID field of the {@link CustomerReceiptData}.</p>
     *
     * @param terminalID The terminalID field of the {@link CustomerReceiptData}.
     */
    public void setTerminalID (int terminalID) {
        this.terminalID = terminalID;
    }

    /**
     * <p>Getter for the orderTypeID field of the {@link CustomerReceiptData}.</p>
     *
     * @return The orderTypeID field of the {@link CustomerReceiptData}.
     */
    public int getOrderTypeID () {
        return orderTypeID;
    }

    /**
     * <p>Setter for the orderTypeID field of the {@link CustomerReceiptData}.</p>
     *
     * @param orderTypeID The orderTypeID field of the {@link CustomerReceiptData}.
     */
    public void setOrderTypeID (int orderTypeID) {
        this.orderTypeID = orderTypeID;
    }

    /**
     * <p>Getter for the transactionID field of the {@link CustomerReceiptData}.</p>
     *
     * @return The transactionID field of the {@link CustomerReceiptData}.
     */
    public String getTransactionID () {
        return transactionID;
    }

    /**
     * <p>Setter for the transactionID field of the {@link CustomerReceiptData}.</p>
     *
     * @param transactionID The transactionID field of the {@link CustomerReceiptData}.
     */
    public void setTransactionID (String transactionID) {
        this.transactionID = transactionID;
    }

    /**
     * <p>Getter for the originalTranID field of the {@link CustomerReceiptData}.</p>
     *
     * @return The originalTranID field of the {@link CustomerReceiptData}.
     */
    public String getOriginalTranID () {
        return originalTranID;
    }

    /**
     * <p>Setter for the originalTranID field of the {@link CustomerReceiptData}.</p>
     *
     * @param originalTranID The originalTranID field of the {@link CustomerReceiptData}.
     */
    public void setOriginalTranID (String originalTranID) {
        this.originalTranID = originalTranID;
    }

    /**
     * <p>Getter for the cashierName field of the {@link CustomerReceiptData}.</p>
     *
     * @return The cashierName field of the {@link CustomerReceiptData}.
     */
    public String getCashierName () {
        return cashierName;
    }

    /**
     * <p>Setter for the cashierName field of the {@link CustomerReceiptData}.</p>
     *
     * @param cashierName The cashierName field of the {@link CustomerReceiptData}.
     */
    public void setCashierName (String cashierName) {
        this.cashierName = cashierName;
    }

    /**
     * <p>Getter for the transDate field of the {@link CustomerReceiptData}.</p>
     *
     * @return The transDate field of the {@link CustomerReceiptData}.
     */
    public String getTransDate () {
        return transDate;
    }

    /**
     * <p>Setter for the transDate field of the {@link CustomerReceiptData}.</p>
     *
     * @param transDate The transDate field of the {@link CustomerReceiptData}.
     */
    public void setTransDate (String transDate) {
        this.transDate = transDate;
    }

    /**
     * <p>Getter for the transTime field of the {@link CustomerReceiptData}.</p>
     *
     * @return The transTime field of the {@link CustomerReceiptData}.
     */
    public String getTransTime () {
        return transTime;
    }

    /**
     * <p>Setter for the transTime field of the {@link CustomerReceiptData}.</p>
     *
     * @param transTime The transTime field of the {@link CustomerReceiptData}.
     */
    public void setTransTime (String transTime) {
        this.transTime = transTime;
    }

    /**
     * <p>Getter for the receiptFooters field of the {@link CustomerReceiptData}.</p>
     *
     * @return The receiptFooters field of the {@link CustomerReceiptData}.
     */
    public ArrayList<String> getReceiptFooters () {
        return receiptFooters;
    }

    /**
     * <p>Setter for the receiptFooters field of the {@link CustomerReceiptData}.</p>
     *
     * @param receiptFooters The receiptFooters field of the {@link CustomerReceiptData}.
     */
    public void setReceiptFooters (ArrayList<String> receiptFooters) {
        this.receiptFooters = receiptFooters;
    }

    /**
     * <p>Getter for the userID field of the {@link CustomerReceiptData}.</p>
     *
     * @return The userID field of the {@link CustomerReceiptData}.
     */
    public int getUserID () {
        return userID;
    }

    /**
     * <p>Setter for the userID field of the {@link CustomerReceiptData}.</p>
     *
     * @param userID The userID field of the {@link CustomerReceiptData}.
     */
    public void setUserID (int userID) {
        this.userID = userID;
    }

    /**
     * <p>Getter for the revenueCenterID field of the {@link CustomerReceiptData}.</p>
     *
     * @return The revenueCenterID field of the {@link CustomerReceiptData}.
     */
    public int getRevenueCenterID () {
        return revenueCenterID;
    }

    /**
     * <p>Setter for the revenueCenterID field of the {@link CustomerReceiptData}.</p>
     *
     * @param revenueCenterID The revenueCenterID field of the {@link CustomerReceiptData}.
     */
    public void setRevenueCenterID (int revenueCenterID) {
        this.revenueCenterID = revenueCenterID;
    }

    /**
     * <p>Getter for the printOrderNumber field of the {@link CustomerReceiptData}.</p>
     *
     * @return The printOrderNumber field of the {@link CustomerReceiptData}.
     */
    public boolean getPrintOrderNumber () {
        return printOrderNumber;
    }

    /**
     * <p>Setter for the printOrderNumber field of the {@link CustomerReceiptData}.</p>
     *
     * @param printOrderNumber The printOrderNumber field of the {@link CustomerReceiptData}.
     */
    public void setPrintOrderNumber (boolean printOrderNumber) {
        this.printOrderNumber = printOrderNumber;
    }

    /**
     * <p>Getter for the printNutritionInfo field of the {@link CustomerReceiptData}.</p>
     *
     * @return The printNutritionInfo field of the {@link CustomerReceiptData}.
     */
    public boolean getPrintNutritionInfo () {
        return printNutritionInfo;
    }

    /**
     * <p>Setter for the printNutritionInfo field of the {@link CustomerReceiptData}.</p>
     *
     * @param printNutritionInfo The printNutritionInfo field of the {@link CustomerReceiptData}.
     */
    public void setPrintNutritionInfo (boolean printNutritionInfo) {
        this.printNutritionInfo = printNutritionInfo;
    }

    /**
     * <p>Getter for the printPLUCodes field of the {@link CustomerReceiptData}.</p>
     *
     * @return The printPLUCodes field of the {@link CustomerReceiptData}.
     */
    public boolean getPrintPLUCodes () {
        return printPLUCodes;
    }

    /**
     * <p>Setter for the printPLUCodes field of the {@link CustomerReceiptData}.</p>
     *
     * @param printPLUCodes The printPLUCodes field of the {@link CustomerReceiptData}.
     */
    public void setPrintPLUCodes (boolean printPLUCodes) {
        this.printPLUCodes = printPLUCodes;
    }

    /**
     * <p>Getter for the receiptAccountInfoLines field of the {@link CustomerReceiptData}.</p>
     *
     * @return The receiptAccountInfoLines field of the {@link CustomerReceiptData}.
     */
    public ArrayList<String> getReceiptAccountInfoLines () {
        return receiptAccountInfoLines;
    }

    /**
     * <p>Setter for the receiptAccountInfoLines field of the {@link CustomerReceiptData}.</p>
     *
     * @param receiptAccountInfoLines The receiptAccountInfoLines field of the {@link CustomerReceiptData}.
     */
    public void setReceiptAccountInfoLines (ArrayList<String> receiptAccountInfoLines) {
        this.receiptAccountInfoLines = receiptAccountInfoLines;
    }

    /**
     * <p>Getter for the printTransactionBarcode field of the {@link CustomerReceiptData}.</p>
     *
     * @return The printTransactionBarcode field of the {@link CustomerReceiptData}.
     */
    public boolean getPrintTransactionBarcode () {
        return printTransactionBarcode;
    }

    /**
     * <p>Setter for the printTransactionBarcode field of the {@link CustomerReceiptData}.</p>
     *
     * @param printTransactionBarcode The printTransactionBarcode field of the {@link CustomerReceiptData}.
     */
    public void setPrintTransactionBarcode (boolean printTransactionBarcode) {
        this.printTransactionBarcode = printTransactionBarcode;
    }

    /**
     * <p>Getter for the orderNumber field of the {@link CustomerReceiptData}.</p>
     *
     * @return The orderNumber field of the {@link CustomerReceiptData}.
     */
    public String getOrderNumber () {
        return orderNumber;
    }

    /**
     * <p>Setter for the orderNumber field of the {@link CustomerReceiptData}.</p>
     *
     * @param orderNumber The orderNumber field of the {@link CustomerReceiptData}.
     */
    public void setOrderNumber (String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * <p>Getter for the transactionStatusID field of the {@link CustomerReceiptData}.</p>
     *
     * @return The transactionStatusID field of the {@link CustomerReceiptData}.
     */
    public int getTransactionStatusID () {
        return transactionStatusID;
    }

    /**
     * <p>Setter for the transactionStatusID field of the {@link CustomerReceiptData}.</p>
     *
     * @param transactionStatusID The transactionStatusID field of the {@link CustomerReceiptData}.
     */
    public void setTransactionStatusID (int transactionStatusID) {
        this.transactionStatusID = transactionStatusID;
    }

    /**
     * <p>Getter for the paymentGatewayID field of the {@link CustomerReceiptData}.</p>
     *
     * @return The paymentGatewayID field of the {@link CustomerReceiptData}.
     */
    public int getPaymentGatewayID () {
        return paymentGatewayID;
    }

    /**
     * <p>Setter for the paymentGatewayID field of the {@link CustomerReceiptData}.</p>
     *
     * @param paymentGatewayID The paymentGatewayID field of the {@link CustomerReceiptData}.
     */
    public void setPaymentGatewayID (int paymentGatewayID) {
        this.paymentGatewayID = paymentGatewayID;
    }

    /**
     * <p>Getter for the printingNutritionalInfo field of the {@link CustomerReceiptData}.</p>
     *
     * @return The printingNutritionalInfo field of the {@link CustomerReceiptData}.
     */
    public boolean getPrintingNutritionalInfo () {
        return printingNutritionalInfo;
    }

    /**
     * <p>Setter for the printingNutritionalInfo field of the {@link CustomerReceiptData}.</p>
     *
     * @param printingNutritionalInfo The printingNutritionalInfo field of the {@link CustomerReceiptData}.
     */
    public void setPrintingNutritionalInfo (boolean printingNutritionalInfo) {
        this.printingNutritionalInfo = printingNutritionalInfo;
    }

    /**
     * <p>Getter for the nutritionItems field of the {@link CustomerReceiptData}.</p>
     *
     * @return The nutritionItems field of the {@link CustomerReceiptData}.
     */
    public ArrayList<NutritionItem> getNutritionItems () {
        return nutritionItems;
    }

    /**
     * <p>Setter for the nutritionItems field of the {@link CustomerReceiptData}.</p>
     *
     * @param nutritionItems The nutritionItems field of the {@link CustomerReceiptData}.
     */
    public void setNutritionItems (ArrayList<NutritionItem> nutritionItems) {
        this.nutritionItems = nutritionItems;
    }

    /**
     * <p>Getter for the nutritionCategories field of the {@link CustomerReceiptData}.</p>
     *
     * @return The nutritionCategories field of the {@link CustomerReceiptData}.
     */
    public ArrayList<NutritionCategory> getNutritionCategories () {
        return nutritionCategories;
    }

    /**
     * <p>Setter for the nutritionCategories field of the {@link CustomerReceiptData}.</p>
     *
     * @param nutritionCategories The nutritionCategories field of the {@link CustomerReceiptData}.
     */
    public void setNutritionCategories (ArrayList<NutritionCategory> nutritionCategories) {
        this.nutritionCategories = nutritionCategories;
    }

    /**
     * <p>Getter for the nutritionTotals field of the {@link CustomerReceiptData}.</p>
     *
     * @return The nutritionTotals field of the {@link CustomerReceiptData}.
     */
    public ArrayList<NutritionTotal> getNutritionTotals () {
        return nutritionTotals;
    }

    /**
     * <p>Setter for the nutritionTotals field of the {@link CustomerReceiptData}.</p>
     *
     * @param nutritionTotals The nutritionTotals field of the {@link CustomerReceiptData}.
     */
    public void setNutritionTotals (ArrayList<NutritionTotal> nutritionTotals) {
        this.nutritionTotals = nutritionTotals;
    }

    /**
     * <p>Getter for the needsNAFootnote field of the {@link CustomerReceiptData}.</p>
     *
     * @return The needsNAFootnote field of the {@link CustomerReceiptData}.
     */
    public boolean getNeedsNAFootnote () {
        return needsNAFootnote;
    }

    /**
     * <p>Setter for the needsNAFootnote field of the {@link CustomerReceiptData}.</p>
     *
     * @param needsNAFootnote The needsNAFootnote field of the {@link CustomerReceiptData}.
     */
    public void setNeedsNAFootnote (boolean needsNAFootnote) {
        this.needsNAFootnote = needsNAFootnote;
    }

    /**
     * <p>Getter for the needsTruncateDigitsFootnote field of the {@link CustomerReceiptData}.</p>
     *
     * @return The needsTruncateDigitsFootnote field of the {@link CustomerReceiptData}.
     */
    public boolean getNeedsTruncateDigitsFootnote () {
        return needsTruncateDigitsFootnote;
    }

    /**
     * <p>Setter for the needsTruncateDigitsFootnote field of the {@link CustomerReceiptData}.</p>
     *
     * @param needsTruncateDigitsFootnote The needsTruncateDigitsFootnote field of the {@link CustomerReceiptData}.
     */
    public void setNeedsTruncateDigitsFootnote (boolean needsTruncateDigitsFootnote) {
        this.needsTruncateDigitsFootnote = needsTruncateDigitsFootnote;
    }

    /**
     * <p>Getter for the emailAddress field of the {@link CustomerReceiptData}.</p>
     *
     * @return The emailAddress field of the {@link CustomerReceiptData}.
     */
    public String getEmailAddress () {
        return emailAddress;
    }

    /**
     * <p>Setter for the emailAddress field of the {@link CustomerReceiptData}.</p>
     *
     * @param emailAddress The emailAddress field of the {@link CustomerReceiptData}.
     */
    public void setEmailAddress (String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * <p>Getter for the printReceipt field of the {@link CustomerReceiptData}.</p>
     *
     * @return The printReceipt field of the {@link CustomerReceiptData}.
     */
    public boolean getPrintReceipt () {
        return printReceipt;
    }

    /**
     * <p>Setter for the printReceipt field of the {@link CustomerReceiptData}.</p>
     *
     * @param printReceipt The printReceipt field of the {@link CustomerReceiptData}.
     */
    public void setPrintReceipt (boolean printReceipt) {
        this.printReceipt = printReceipt;
    }

    /**
     * <p>Getter for the loyaltyDatas field of the {@link CustomerReceiptData}.</p>
     *
     * @return The loyaltyDatas field of the {@link CustomerReceiptData}.
     */
    public ArrayList<LoyaltyData> getLoyaltyDatas () {
        return loyaltyDatas;
    }

    /**
     * <p>Setter for the loyaltyDatas field of the {@link CustomerReceiptData}.</p>
     *
     * @param loyaltyDatas The loyaltyDatas field of the {@link CustomerReceiptData}.
     */
    public void setLoyaltyDatas (ArrayList<LoyaltyData> loyaltyDatas) {
        this.loyaltyDatas = loyaltyDatas;
    }

    /**
     * <p>Getter for the isGiftReceipt field of the {@link CustomerReceiptData}.</p>
     *
     * @return The isGiftReceipt field of the {@link CustomerReceiptData}.
     */
    public boolean getIsGiftReceipt () {
        return isGiftReceipt;
    }

    /**
     * <p>Setter for the isGiftReceipt field of the {@link CustomerReceiptData}.</p>
     *
     * @param isGiftReceipt The isGiftReceipt field of the {@link CustomerReceiptData}.
     */
    public void setIsGiftReceipt (boolean isGiftReceipt) {
        this.isGiftReceipt = isGiftReceipt;
    }

    /**
     * <p>Getter for the suspTransNameLbl field of the {@link CustomerReceiptData}.</p>
     *
     * @return The suspTransNameLbl field of the {@link CustomerReceiptData}.
     */
    public String getSuspTransNameLbl () {
        return suspTransNameLbl;
    }

    /**
     * <p>Setter for the suspTransNameLbl field of the {@link CustomerReceiptData}.</p>
     *
     * @param suspTransNameLbl The suspTransNameLbl field of the {@link CustomerReceiptData}.
     */
    public void setSuspTransNameLbl (String suspTransNameLbl) {
        this.suspTransNameLbl = suspTransNameLbl;
    }

    /**
     * <p>Getter for the suspTransName field of the {@link CustomerReceiptData}.</p>
     *
     * @return The suspTransName field of the {@link CustomerReceiptData}.
     */
    public String getSuspTransName () {
        return suspTransName;
    }

    /**
     * <p>Setter for the suspTransName field of the {@link CustomerReceiptData}.</p>
     *
     * @param suspTransName The suspTransName field of the {@link CustomerReceiptData}.
     */
    public void setSuspTransName (String suspTransName) {
        this.suspTransName = suspTransName;
    }

    /**
     * <p>Getter for the printGrossWeight field of the {@link CustomerReceiptData}.</p>
     *
     * @return The printGrossWeight field of the {@link CustomerReceiptData}.
     */
    public boolean getPrintGrossWeight () {
        return printGrossWeight;
    }

    /**
     * <p>Setter for the printGrossWeight field of the {@link CustomerReceiptData}.</p>
     *
     * @param printGrossWeight The printGrossWeight field of the {@link CustomerReceiptData}.
     */
    public void setPrintGrossWeight (boolean printGrossWeight) {
        this.printGrossWeight = printGrossWeight;
    }

    /**
     * <p>Getter for the printNetWeight field of the {@link CustomerReceiptData}.</p>
     *
     * @return The printNetWeight field of the {@link CustomerReceiptData}.
     */
    public boolean getPrintNetWeight () {
        return printNetWeight;
    }

    /**
     * <p>Setter for the printNetWeight field of the {@link CustomerReceiptData}.</p>
     *
     * @param printNetWeight The printNetWeight field of the {@link CustomerReceiptData}.
     */
    public void setPrintNetWeight (boolean printNetWeight) {
        this.printNetWeight = printNetWeight;
    }

    /**
     * <p>Getter for the combos field of the {@link CustomerReceiptData}.</p>
     *
     * @return The combos field of the {@link CustomerReceiptData}.
     */
    public ArrayList<Combo> getCombos () {
        return combos;
    }

    /**
     * <p>Setter for the combos field of the {@link CustomerReceiptData}.</p>
     *
     * @param combos The combos field of the {@link CustomerReceiptData}.
     */
    public void setCombos (ArrayList<Combo> combos) {
        this.combos = combos;
    }

    /**
     * <p>Getter for the productsJSON field of the {@link CustomerReceiptData}.</p>
     *
     * @return The productsJSON field of the {@link CustomerReceiptData}.
     */
    public String getProductsJSON () {
        return productsJSON;
    }

    /**
     * <p>Setter for the productsJSON field of the {@link CustomerReceiptData}.</p>
     *
     * @param productsJSON The productsJSON field of the {@link CustomerReceiptData}.
     */
    public void setProductsJSON (String productsJSON) {
        this.productsJSON = productsJSON;
    }

    /**
     * <p>Getter for the products field of the {@link CustomerReceiptData}.</p>
     *
     * @return The products field of the {@link CustomerReceiptData}.
     */
    public ArrayList<Plu> getProducts () {
        return products;
    }

    /**
     * <p>Setter for the products field of the {@link CustomerReceiptData}.</p>
     *
     * @param products The products field of the {@link CustomerReceiptData}.
     */
    public void setProducts (ArrayList<Plu> products) {
        this.products = products;
    }

    /**
     * <p>Getter for the tenders field of the {@link CustomerReceiptData}.</p>
     *
     * @return The tenders field of the {@link CustomerReceiptData}.
     */
    public ArrayList<Tender> getTenders () {
        return tenders;
    }

    /**
     * <p>Setter for the tenders field of the {@link CustomerReceiptData}.</p>
     *
     * @param tenders The tenders field of the {@link CustomerReceiptData}.
     */
    public void setTenders (ArrayList<Tender> tenders) {
        this.tenders = tenders;
    }

    /**
     * <p>Getter for the subtotalDiscounts field of the {@link CustomerReceiptData}.</p>
     *
     * @return The subtotalDiscounts field of the {@link CustomerReceiptData}.
     */
    public ArrayList<Discount> getSubtotalDiscounts () {
        return subtotalDiscounts;
    }

    /**
     * <p>Setter for the subtotalDiscounts field of the {@link CustomerReceiptData}.</p>
     *
     * @param subtotalDiscounts The subtotalDiscounts field of the {@link CustomerReceiptData}.
     */
    public void setSubtotalDiscounts (ArrayList<Discount> subtotalDiscounts) {
        this.subtotalDiscounts = subtotalDiscounts;
    }

    /**
     * <p>Getter for the itemDiscounts field of the {@link CustomerReceiptData}.</p>
     *
     * @return The itemDiscounts field of the {@link CustomerReceiptData}.
     */
    public ArrayList<Discount> getItemDiscounts () {
        return itemDiscounts;
    }

    /**
     * <p>Setter for the itemDiscounts field of the {@link CustomerReceiptData}.</p>
     *
     * @param itemDiscounts The itemDiscounts field of the {@link CustomerReceiptData}.
     */
    public void setItemDiscounts (ArrayList<Discount> itemDiscounts) {
        this.itemDiscounts = itemDiscounts;
    }

    /**
     * <p>Getter for the taxes field of the {@link CustomerReceiptData}.</p>
     *
     * @return The taxes field of the {@link CustomerReceiptData}.
     */
    public ArrayList<Tax> getTaxes () {
        return taxes;
    }

    /**
     * <p>Setter for the taxes field of the {@link CustomerReceiptData}.</p>
     *
     * @param taxes The taxes field of the {@link CustomerReceiptData}.
     */
    public void setTaxes (ArrayList<Tax> taxes) {
        this.taxes = taxes;
    }

    /**
     * <p>Getter for the paidOuts field of the {@link CustomerReceiptData}.</p>
     *
     * @return The paidOuts field of the {@link CustomerReceiptData}.
     */
    public ArrayList<PaidOut> getPaidOuts () {
        return paidOuts;
    }

    /**
     * <p>Setter for the paidOuts field of the {@link CustomerReceiptData}.</p>
     *
     * @param paidOuts The paidOuts field of the {@link CustomerReceiptData}.
     */
    public void setPaidOuts (ArrayList<PaidOut> paidOuts) {
        this.paidOuts = paidOuts;
    }

    /**
     * <p>Getter for the receivedOnAccts field of the {@link CustomerReceiptData}.</p>
     *
     * @return The receivedOnAccts field of the {@link CustomerReceiptData}.
     */
    public ArrayList<ReceivedOnAcct> getReceivedOnAccts () {
        return receivedOnAccts;
    }

    /**
     * <p>Setter for the receivedOnAccts field of the {@link CustomerReceiptData}.</p>
     *
     * @param receivedOnAccts The receivedOnAccts field of the {@link CustomerReceiptData}.
     */
    public void setReceivedOnAccts (ArrayList<ReceivedOnAcct> receivedOnAccts) {
        this.receivedOnAccts = receivedOnAccts;
    }

    /**
     * <p>Getter for the loyaltyRewards field of the {@link CustomerReceiptData}.</p>
     *
     * @return The loyaltyRewards field of the {@link CustomerReceiptData}.
     */
    public ArrayList<LoyaltyReward> getLoyaltyRewards () {
        return loyaltyRewards;
    }

    /**
     * <p>Setter for the loyaltyRewards field of the {@link CustomerReceiptData}.</p>
     *
     * @param loyaltyRewards The loyaltyRewards field of the {@link CustomerReceiptData}.
     */
    public void setLoyaltyRewards (ArrayList<LoyaltyReward> loyaltyRewards) {
        this.loyaltyRewards = loyaltyRewards;
    }

    /**
     * <p>Getter for the subtotalSurcharges field of the {@link CustomerReceiptData}.</p>
     *
     * @return The subtotalSurcharges field of the {@link CustomerReceiptData}.
     */
    public ArrayList<Surcharge> getSubtotalSurcharges () {
        return subtotalSurcharges;
    }

    /**
     * <p>Setter for the subtotalSurcharges field of the {@link CustomerReceiptData}.</p>
     *
     * @param subtotalSurcharges The subtotalSurcharges field of the {@link CustomerReceiptData}.
     */
    public void setSubtotalSurcharges (ArrayList<Surcharge> subtotalSurcharges) {
        this.subtotalSurcharges = subtotalSurcharges;
    }

    /**
     * <p>Getter for the itemSurcharges field of the {@link CustomerReceiptData}.</p>
     *
     * @return The itemSurcharges field of the {@link CustomerReceiptData}.
     */
    public ArrayList<Surcharge> getItemSurcharges () {
        return itemSurcharges;
    }

    /**
     * <p>Setter for the itemSurcharges field of the {@link CustomerReceiptData}.</p>
     *
     * @param itemSurcharges The itemSurcharges field of the {@link CustomerReceiptData}.
     */
    public void setItemSurcharges (ArrayList<Surcharge> itemSurcharges) {
        this.itemSurcharges = itemSurcharges;
    }

}