package com.mmhayes.common.kms.receiptGen.customerReceipt;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-29 15:10:43 -0400 (Tue, 29 Sep 2020) $: Date of last commit
    $Rev: 12753 $: Revision of last commit
    Notes: Contains methods for emailing / printing customer receipts.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.*;
import jpos.POSPrinter;
import org.apache.commons.lang.math.NumberUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class CustomerReceipt {

    /**
     * <p>Gets the settings required to send an email.</p>
     *
     * @return A {@link HashMap} whose {@link String} key is the field name and whose {@link String} value is the value of the field.
     */
    private static HashMap<String, String> getEmailSettings () {

        // the email settings we want to find
        String user;
        String server;
        String port;
        String password;
        String from;

        // query the database to get the email settings
        DynamicSQL sql = new DynamicSQL("data.ReceiptGen.EmailCfg").setLogFileName(CustomerReceiptData.CUST_RCPT_LOG);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(new DataManager()));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
            HashMap emailSettingsHM = queryRes.get(0);
            user = HashMapDataFns.getStringVal(emailSettingsHM, "SMTPUSER");
            server = HashMapDataFns.getStringVal(emailSettingsHM, "SMTPSERVER");
            port = HashMapDataFns.getStringVal(emailSettingsHM, "SMTPPORT");
            password = HashMapDataFns.getStringVal(emailSettingsHM, "SMTPPASSWORD");
            if (StringFunctions.stringHasContent(password)) {
                password = StringFunctions.decodePassword(password);
            }
            from = HashMapDataFns.getStringVal(emailSettingsHM, "SMTPFROMADDRESS");
        }
        else {
            Logger.logMessage("Unable to obtain the email settings from the database in CustomerReceipt.getEmailSettings, now " +
                    "checking the app properties file for email settings.", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.TRACE);
            user = MMHProperties.getAppSetting("site.communication.email.smtp.user");
            server = MMHProperties.getAppSetting("site.communication.email.smtp.host");
            port = MMHProperties.getAppSetting("site.communication.email.smtp.port");
            password = MMHProperties.getAppSetting("site.communication.email.smtp.pass");
            from = MMHProperties.getAppSetting("site.communication.email.fromaddress");
        }

        HashMap<String, String> emailSettings = new HashMap<>();
        emailSettings.put("SMTPUSER", user);
        emailSettings.put("SMTPSERVER", server);
        emailSettings.put("SMTPPORT", port);
        emailSettings.put("SMTPPASSWORD", password);
        emailSettings.put("SMTPFROMADDRESS", from);

        Logger.logMessage(String.format("Found the following email settings in CustomerReceipt.getEmailSettings: SMTPUSER: %s, SMTPSERVER: %s, SMTPPORT: %s, SMTPPASSWORD: %s, SMTPFROMADDRESS: %s",
                Objects.toString(user, "N/A"),
                Objects.toString(server, "N/A"),
                Objects.toString(port, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(password) ? StringFunctions.encodePassword(password) : "N/A"), "N/A"),
                Objects.toString(from, "N/A")), CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.TRACE);

        return emailSettings;
    }

    /**
     * <p>Emails a consolidated receipt to the configured email address.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param receiptGen The {@link ReceiptGen} object to use to generate the consolidated receipt.
     * @param customerReceiptData The {@link CustomerReceiptData} that should be contained on the consolidated receipt.
     * @return Whether or not the consolidated receipt could be emailed successfully.
     */
    public static boolean emailConsolidatedReceipt (String log, ReceiptGen receiptGen, CustomerReceiptData customerReceiptData) {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to CustomerReceipt.emailConsolidatedReceipt can't be null or empty, now returning false!", Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the ReceiptGen object valid
        if (receiptGen == null) {
            Logger.logMessage("The ReceiptGen passed to CustomerReceipt.emailConsolidatedReceipt can't be null, now returning false!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the customer receipt data valid
        if (customerReceiptData == null) {
            Logger.logMessage("The CustomerReceiptData passed to CustomerReceipt.emailConsolidatedReceipt can't be null, now returning false!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the email settings are valid
        HashMap<String, String> emailSettings = getEmailSettings();
        if (DataFunctions.isEmptyMap(emailSettings)) {
            Logger.logMessage("The email settings acquired in CustomerReceipt.emailConsolidatedReceipt can't be null or empty, now returning false!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // extract the email settings
        String user = HashMapDataFns.getStringVal(emailSettings, "SMTPUSER");
        String server = HashMapDataFns.getStringVal(emailSettings, "SMTPSERVER");
        String port = HashMapDataFns.getStringVal(emailSettings, "SMTPPORT");
        String password = HashMapDataFns.getStringVal(emailSettings, "SMTPPASSWORD");
        String from = HashMapDataFns.getStringVal(emailSettings, "SMTPFROMADDRESS");

        // create a html renderer for the receipt
        IRenderer htmlRenderer = receiptGen.createBasicHTMLRenderer();

        if (StringFunctions.stringHasContent(server)) {
            // always email a customer copy
            customerReceiptData.setIsMerchantCopy(false);

            // email the receipt
            if ((htmlRenderer != null) && (receiptGen.buildFromCustomerReceiptData(customerReceiptData, TypeData.ReceiptType.EMAILEDRECEIPT, htmlRenderer))) {
                int emailPort = (NumberUtils.isNumber(port) ? Integer.parseInt(port) : -1);
                boolean emailResult;
                if (StringFunctions.stringHasContent(user)) {
                    Logger.logMessage(String.format("In CustomerReceipt.emailConsolidatedReceipt attempting to send an authenticated email receipt using " +
                            "the parameters, port: %s, server: %s, user: %s, password: %s, to address: %s, from address: %s, subject: %s.",
                            Objects.toString(port, "N/A"),
                            Objects.toString(server, "N/A"),
                            Objects.toString(user, "N/A"),
                            Objects.toString(password, "N/A"),
                            Objects.toString(customerReceiptData.getEmailAddress(), "N/A"),
                            Objects.toString(from, "N/A"),
                            Objects.toString("Your receipt", "N/A")), log, Logger.LEVEL.TRACE);
                    emailResult = MMHEmail.sendAuthEmail(emailPort, server, user, password, customerReceiptData.getEmailAddress(), from, "Your receipt", htmlRenderer.toString(), "text/html");
                }
                else {
                    Logger.logMessage(String.format("In CustomerReceipt.emailConsolidatedReceipt attempting to send an unauthenticated email receipt using " +
                            "the parameters, port: %s, server: %s, to address: %s, from address: %s, subject: %s.",
                            Objects.toString(port, "N/A"),
                            Objects.toString(server, "N/A"),
                            Objects.toString(customerReceiptData.getEmailAddress(), "N/A"),
                            Objects.toString(from, "N/A"),
                            Objects.toString("Your receipt", "N/A")), log, Logger.LEVEL.TRACE);
                    emailResult = MMHEmail.sendEmail(emailPort, server, customerReceiptData.getEmailAddress(), from, "Your receipt", htmlRenderer.toString(), "text/html");
                }
                return emailResult;
            }
            else {
                Logger.logMessage(String.format("The %s in CustomerReceipt.emailConsolidatedReceipt, now returning false!",
                        Objects.toString((htmlRenderer == null ? "html renderer is null" : "consolidated receipt couldn't be generated properly from the CustomerReceiptData"), "N/A")), log, Logger.LEVEL.ERROR);
                return false;
            }
        }
        else {
            Logger.logMessage("No SMTP server was found in CustomerReceipt.emailConsolidatedReceipt, unable to email a consolidated receipt, now returning false!", log, Logger.LEVEL.ERROR);
            return false;
        }

    }

    /**
     * <p>Prints a consolidated receipt at the local receipt printer.</p>
     *
     * @param log The file path {@link String} for the file that any information related to this method should be written to.
     * @param receiptGen The {@link ReceiptGen} object to use to generate the consolidated receipt.
     * @param customerReceiptData The {@link CustomerReceiptData} that should be contained on the consolidated receipt.
     * @return Whether or not the consolidated receipt could be printed successfully.
     * @throws Exception
     */
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public static boolean printConsolidatedReceipt (String log, ReceiptGen receiptGen, CustomerReceiptData customerReceiptData) throws Exception {

        // make sure the log file is valid
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to CustomerReceipt.printConsolidatedReceipt can't be null or empty, now returning false!", Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the ReceiptGen object valid
        if (receiptGen == null) {
            Logger.logMessage("The ReceiptGen passed to CustomerReceipt.printConsolidatedReceipt can't be null, now returning false!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the customer receipt data valid
        if (customerReceiptData == null) {
            Logger.logMessage("The CustomerReceiptData passed to CustomerReceipt.printConsolidatedReceipt can't be null, now returning false!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // get the POSPrinter
        POSPrinter printer = PeripheralsDataManager.getLocalReceiptPrinter();
        String logicalPrinterName = PeripheralsDataManager.getLocalReceiptPrinterLogicalName();

        // create a renderer
        IRenderer printerRenderer = null;
        if ((customerReceiptData.getPrintReceipt()) && (printer != null) && (StringFunctions.stringHasContent(logicalPrinterName))) {
            printerRenderer = receiptGen.createPrinterRenderer(printer, logicalPrinterName);
        }

        // print the receipt
        if (printerRenderer != null) {
            receiptGen.buildFromCustomerReceiptData(customerReceiptData, TypeData.ReceiptType.PRINTEDRECEIPT, printerRenderer);
            if (printerRenderer.getLastException() != null) {
                throw printerRenderer.getLastException();
            }
        }
        else {
            Logger.logMessage("Unable to create a printer renderer in KMSDataManager.printConsolidatedReceipt, now returning false!", log, Logger.LEVEL.ERROR);
            return false;
        }

        return true;
    }

}