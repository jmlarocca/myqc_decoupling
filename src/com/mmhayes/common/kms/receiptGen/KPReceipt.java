package com.mmhayes.common.kms.receiptGen;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-29 10:11:08 -0400 (Thu, 29 Apr 2021) $: Date of last commit
    $Rev: 13900 $: Revision of last commit
    Notes: Builds a receipt to print on KP.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kitchenPrinters.KitchenPrinterCommon;
import com.mmhayes.common.kitchenPrinters.KitchenPrinterModel;
import com.mmhayes.common.kms.receiptGen.receiptData.KitchenReceiptDetail;
import com.mmhayes.common.kms.receiptGen.receiptData.KitchenReceiptHeader;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionData;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionHeader;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionLineItem;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.Commands.JPOS_COMMAND;
import com.mmhayes.common.receiptGen.Formatter.KitchenPrinterReceiptFormatter;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.*;
import org.apache.commons.lang.math.NumberUtils;

import java.util.*;
import java.util.regex.Pattern;

/**
 * <p>Builds a receipt to print on KP.</p>
 *
 */
public class KPReceipt implements KitchenReceipt {

    // private member variables of a KPReceipt
    private DataManager dataManager = null;
    private IRenderer renderer = null;
    private TransactionData transactionData = null;
    private String log = null;
    private int revenueCenterID = 0;
    private int expeditorKPID = 0;
    private int remoteOrderKPID = 0;
    private boolean remoteOrdersUseExpeditor = false;
    private boolean noExpeditorReceiptUnlessProductsAdded = false;
    private HashMap<Integer, Boolean> printerOnlyPrintsNewProductsHM = null;
    private HashMap<Integer, Integer> printerIDToModelIDLookup = null;
    private int maxCharsPerReceiptLineNormal = 32;
    private int maxCharsPerReceiptLineLarge = 16;

    /**
     * <p>Builds a receipt for a kitchen printer.</p>
     *
     * @param args A variable number of {@link Object} arguments to pass to the method.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print job details for the receipt.
     */
    @Override
    public ArrayList<HashMap> buildReceipt (Object ... args) {
        ArrayList<HashMap> kpReceiptDetails = new ArrayList<>();

        // parse out the arguments
        try {
            setArgs(args);
        }
        catch (Exception e) {
            Logger.logException(e);
            return null;
        }

        // make sure we have a renderer for the html summary
        if (renderer == null) {
            Logger.logMessage("No renderer is available to create the html summary in KPReceipt.buildReceipt!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the renderer is able to render
        if (!renderer.begin()) {
            Logger.logMessage("The renderer passed to KPReceipt.buildReceipt is unable to render!", log, Logger.LEVEL.ERROR);
            return null;
        }

        Logger.logMessage("Entering KPReceipt.buildReceipt...", log, Logger.LEVEL.IMPORTANT);

        printerIDToModelIDLookup = KitchenPrinterCommon.getPrinterIDToModelIDLookup(log, dataManager);

        // get the simple print status (PRINTED/UNPRINTED) for line items within the transaction
        HashMap<TransactionLineItem, String> spsLookup = KitchenReceiptHelper.getSimplePrintStatusOfTransLineItems(log, dataManager, transactionData.getTransactionLineItems());

        // determine which transaction line items should be sent to which kitchen printers
        HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForPrinter = determinePrintersForTransactionLineItems();

        if (!DataFunctions.isEmptyMap(transLineItemsForPrinter)) {
            kpReceiptDetails = buildKPReceiptDetails(spsLookup, transLineItemsForPrinter);
        }
        else {
            Logger.logMessage("No product or modifier transaction line items were found to be mapped to any kitchen printers in KPReceipt.buildReceipt!", log, Logger.LEVEL.TRACE);
        }

        if (!renderer.end()) {
            Logger.logMessage("The renderer was unable to finish rendering KPReceipt.buildReceipt!", log, Logger.LEVEL.ERROR);
            return null;
        }

        return kpReceiptDetails;
    }

    /**
     * <p>Iterates through the arguments passed to this method and sets them as private variables for this class.</p>
     *
     * @param args A variable number of {@link Object} arguments to pass to the method.
     */
    @SuppressWarnings("Duplicates")
    private void setArgs (Object ... args) throws Exception {

        if ((args == null) || (args.length == 0)) {
            throw new Exception("No arguments received in the method KPReceipt.setArgs!");
        }

        // find and set the arguments
        int index = 0;
        for (Object arg : args) {
            if ((arg != null) && (arg instanceof DataManager) && (index == 0)) {
                this.dataManager = ((DataManager) arg);
            }
            else if ((arg == null) && (index == 0)) {
                throw new Exception("No valid DataManager has been passed to the method KPReceipt.setArgs!");
            }

            if ((arg != null) && (arg instanceof IRenderer) && (index == 1)) {
                this.renderer = ((IRenderer) arg);
            }
            else if ((arg == null) && (index == 1)) {
                throw new Exception("No valid receipt renderer has been passed to the method KPReceipt.setArgs!");
            }

            if ((arg != null) && (arg instanceof TransactionData) && (index == 2)) {
                this.transactionData = ((TransactionData) arg);
            }
            else if ((arg == null) && (index == 2)) {
                throw new Exception("No valid transaction information has been passed to the method KPReceipt.setArgs!");
            }

            if ((arg != null) && (arg instanceof String) && (index == 3)) {
                this.log = ((String) arg);
            }
            else if ((arg == null) && (index == 3)) {
                throw new Exception("No valid log file has been passed to the method KPReceipt.setArgs!");
            }

            index++;
        }

    }

    /**
     * <p>Determines which kitchen printers to send each product or modifier transaction line item to.</p>
     *
     * @return A {@link HashMap} whose get is an {@link Integer} ID of the kitchen printer and whose value is an {@link ArrayList}
     * of {@link TransactionLineItem} corresponding to the product and modifier transaction line items that should be sent to the printer.
     */
    private HashMap<Integer, ArrayList<TransactionLineItem>> determinePrintersForTransactionLineItems () {
        HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForPrinter = new HashMap<>();

        TransactionHeader transactionHeader = transactionData.getTransactionHeader();
        if (transactionHeader == null) {
            Logger.logMessage("No valid transaction header was found in KPReceipt.determinePrintersForTransactionLineItems!", log, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<MMHTree<TransactionLineItem>> transactionLineItems = transactionData.getTransactionLineItems();
        if (DataFunctions.isEmptyCollection(transactionLineItems)) {
            Logger.logMessage("No valid transaction line items were found in KPReceipt.determinePrintersForTransactionLineItems!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // get the ID of the revenue center the transaction took place in
        revenueCenterID = transactionHeader.getRevenueCenterID();
        // get the expeditor and remote order kitchen printer IDs in the revenue center if they exist
        expeditorKPID = KitchenReceiptHelper.getKPExpeditor(log, dataManager, revenueCenterID);
        remoteOrderKPID = KitchenReceiptHelper.getRemoteOrderKP(log, dataManager, revenueCenterID);
        remoteOrdersUseExpeditor = KitchenReceiptHelper.remoteOrdersUseExpeditor(log, dataManager, revenueCenterID);

        if (expeditorKPID > 0) {
            Logger.logMessage(String.format("There is an expeditor kitchen printer with a printer ID of %s within the revenue center with an ID of %s.",
                    Objects.toString(expeditorKPID, "N/A"),
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
        }
        if (remoteOrderKPID > 0) {
            Logger.logMessage(String.format("There is an remote order kitchen printer with a printer ID of %s within the revenue center with an ID of %s.",
                    Objects.toString(remoteOrderKPID, "N/A"),
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
        }
        if (remoteOrdersUseExpeditor) {
            Logger.logMessage(String.format("The remote orders use expeditor setting is enabled within the revenue center with an ID of %s.",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
        }

        // check if there was a dining option within the transaction
        MMHTree<TransactionLineItem> diningOption = KitchenReceiptHelper.getDiningOptionTransLineItem(log, transactionData);
        if (diningOption != null) {
            Logger.logMessage(String.format("Found the dining option %s within the transaction with an ID of %s.",
                    Objects.toString((StringFunctions.stringHasContent(diningOption.getData().getProductName()) ? diningOption.getData().getProductName() : "N/A"), "N/A"),
                    Objects.toString(transactionData.getPATransactionID(), "N/A")), log, Logger.LEVEL.TRACE);
        }
        else {
            Logger.logMessage(String.format("No dining option has been found within the transaction with an ID of %s.",
                    Objects.toString(transactionData.getPATransactionID(), "N/A")), log, Logger.LEVEL.TRACE);
        }

        setPrepKPForTransLineItems(transLineItemsForPrinter);
        if (expeditorKPID > 0) {
            setExpeditorKPForTransLineItems(transLineItemsForPrinter);
        }
        if (remoteOrderKPID > 0) {
            setRemoteOrderKPForTransLineItems(transLineItemsForPrinter);
        }
        if (diningOption != null) {
            setPrintersForDiningOption(transLineItemsForPrinter, diningOption);
        }

        return transLineItemsForPrinter;
    }

    /**
     * <p>Sets which products and modifiers within the transaction will go to each prep kitchen printer.</p>
     *
     * @param transLineItemsForPrinter A {@link HashMap} whose key is the {@link Integer} ID of the kitchen printer and
     * whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to the products and modifiers that
     * should be sent to the printer.
     */
    private void setPrepKPForTransLineItems (HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForPrinter) {

        if (DataFunctions.isEmptyCollection(transactionData.getTransactionLineItems())) {
            Logger.logMessage("No valid transaction line items were found in KPReceipt.setPrepKPForTransLineItems!", log, Logger.LEVEL.ERROR);
            return;
        }

        ArrayList<TransactionLineItem> commonPrinterLineItems;
        for (MMHTree<TransactionLineItem> productLineItem : transactionData.getTransactionLineItems()) {
            TransactionLineItem product = productLineItem.getData();
            if (product != null) {
                // get any modifiers for the product
                ArrayList<TransactionLineItem> modifiers = KitchenReceiptHelper.getModifiersForProduct(log, productLineItem);

                // get the IDs of prep kitchen printers mapped to the product
                int[] printerIDs = product.getPrinterIDsMappedToProduct();
                if (!DataFunctions.isEmptyIntArr(printerIDs)) {
                    for (int printerID : printerIDs) {
                        if (transLineItemsForPrinter.containsKey(printerID)) {
                            commonPrinterLineItems = transLineItemsForPrinter.get(printerID);
                        }
                        else {
                            commonPrinterLineItems = new ArrayList<>();
                        }
                        // add the product and modifier transaction line items for the printer
                        commonPrinterLineItems.add(product);
                        if (!DataFunctions.isEmptyCollection(modifiers)) {
                            commonPrinterLineItems.addAll(modifiers);
                        }
                        transLineItemsForPrinter.put(printerID, commonPrinterLineItems);
                    }
                }
            }
        }

    }

    /**
     * <p>Sets which products and modifiers within the transaction will go to the expeditor kitchen printer.</p>
     *
     * @param transLineItemsForPrinter A {@link HashMap} whose key is the {@link Integer} ID of the kitchen printer and
     * whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to the products and modifiers that
     * should be sent to the printer.
     */
    @SuppressWarnings("ConstantConditions")
    private void setExpeditorKPForTransLineItems (HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForPrinter) {

        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("The transaction header is invalid KPReceipt.setExpeditorKPForTransLineItems!", log, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(transactionData.getTransactionLineItems())) {
            Logger.logMessage("No valid transaction line items were found in KPReceipt.setExpeditorKPForTransLineItems!", log, Logger.LEVEL.ERROR);
            return;
        }

        ArrayList<TransactionLineItem> commonPrinterLineItems = new ArrayList<>();
        for (MMHTree<TransactionLineItem> productLineItem : transactionData.getTransactionLineItems()) {
            TransactionLineItem product = productLineItem.getData();
            if (product != null) {
                // get any modifiers for the product
                ArrayList<TransactionLineItem> modifiers = KitchenReceiptHelper.getModifiersForProduct(log, productLineItem);

                // determine which transaction line items will go to the expeditor printer
                if (!remoteOrdersUseExpeditor) {
                    // the kitchen printer will function as a standard expeditor printer (relies on the PrintsOnExpeditor setting)
                    if (product.getPrintsOnExpeditor()) {
                        commonPrinterLineItems.add(product);
                        if (!DataFunctions.isEmptyCollection(modifiers)) {
                            commonPrinterLineItems.addAll(modifiers);
                        }
                    }
                }
                 else if ((remoteOrdersUseExpeditor) && (!transactionData.getTransactionHeader().getRemoteOrder())) {
                    // the kitchen printer will function as a standard expeditor printer (relies on the PrintsOnExpeditor setting)
                    if (product.getPrintsOnExpeditor()) {
                        commonPrinterLineItems.add(product);
                        if (!DataFunctions.isEmptyCollection(modifiers)) {
                            commonPrinterLineItems.addAll(modifiers);
                        }
                    }
                }
                else if ((remoteOrdersUseExpeditor) && (transactionData.getTransactionHeader().getRemoteOrder())) {
                    // the kitchen printer will function as a remote order printer (ignores the PrintsOnExpeditor setting)
                    commonPrinterLineItems.add(product);
                    if (!DataFunctions.isEmptyCollection(modifiers)) {
                        commonPrinterLineItems.addAll(modifiers);
                    }
                }
            }
        }

        // indicate the transaction line items will be sent to the expeditor printer
        if (!DataFunctions.isEmptyCollection(commonPrinterLineItems)) {
            transLineItemsForPrinter.put(expeditorKPID, commonPrinterLineItems);
        }

    }

    /**
     * <p>Sets which products and modifiers within the transaction will go to the remote order kitchen printer.</p>
     *
     * @param transLineItemsForPrinter A {@link HashMap} whose key is the {@link Integer} ID of the kitchen printer and
     * whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to the products and modifiers that
     * should be sent to the printer.
     */
    private void setRemoteOrderKPForTransLineItems (HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForPrinter) {

        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("The transaction header is invalid KPReceipt.setExpeditorKPForTransLineItems!", log, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(transactionData.getTransactionLineItems())) {
            Logger.logMessage("No valid transaction line items were found in KPReceipt.setRemoteOrderKPForTransLineItems!", log, Logger.LEVEL.ERROR);
            return;
        }

        ArrayList<TransactionLineItem> commonPrinterLineItems = new ArrayList<>();
        for (MMHTree<TransactionLineItem> productLineItem : transactionData.getTransactionLineItems()) {
            TransactionLineItem product = productLineItem.getData();
            if ((product != null) && (transactionData.getTransactionHeader().getRemoteOrder())) {
                commonPrinterLineItems.add(product);
                // get any modifiers for the product
                ArrayList<TransactionLineItem> modifiers = KitchenReceiptHelper.getModifiersForProduct(log, productLineItem);
                if (!DataFunctions.isEmptyCollection(modifiers)) {
                    commonPrinterLineItems.addAll(modifiers);
                }
            }
        }

        // indicate the transaction line items will be sent to the remote order printer
        if (!DataFunctions.isEmptyCollection(commonPrinterLineItems)) {
            transLineItemsForPrinter.put(remoteOrderKPID, commonPrinterLineItems);
        }

    }

    /**
     * <p>Sets which kitchen printers to print the dining option on.</p>
     *
     * @param transLineItemsForPrinter A {@link HashMap} whose key is the {@link Integer} ID of the kitchen printer and
     * whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to the products and modifiers that
     * should be sent to the printer.
     * @param diningOptionLineItem A {@link MMHTree} of {@link TransactionLineItem} corresponding to the dining option line item within the transaction.
     */
    @SuppressWarnings({"ConstantConditions", "Duplicates"})
    private void setPrintersForDiningOption (HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForPrinter,
                                             MMHTree<TransactionLineItem> diningOptionLineItem) {

        if ((diningOptionLineItem == null) || (diningOptionLineItem.getData() == null)) {
            Logger.logMessage("The dining option transaction line item passed to KPReceipt.setPrintersForDiningOption can't be null!", log, Logger.LEVEL.ERROR);
            return;
        }

        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("The transaction header is invalid KPReceipt.setPrintersForDiningOption!", log, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(transactionData.getTransactionLineItems())) {
            Logger.logMessage("No valid transaction line items were found in KPReceipt.setPrintersForDiningOption!", log, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyMap(transLineItemsForPrinter)) {
            Logger.logMessage("No transaction line items are being to kitchen printers in KPReceipt.setPrintersForDiningOption!", log, Logger.LEVEL.ERROR);
            return;
        }

        TransactionLineItem diningOption = diningOptionLineItem.getData();
        // determine which prep kitchen printers to print the dining option on
        // get the prep printers mapped to the dining option
        int[] mappedPrepKPs = diningOption.getPrinterIDsMappedToProduct();
        // check if there are products that will be displayed on the prep kitchen printer, if so then add the dining option
        if (!DataFunctions.isEmptyIntArr(mappedPrepKPs)) {
            for (int printerID : mappedPrepKPs) {
                if ((transLineItemsForPrinter.containsKey(printerID)) && (!DataFunctions.isEmptyCollection(transLineItemsForPrinter.get(printerID)))) {
                    // add the dining option
                    ArrayList<TransactionLineItem> lineItemsForPrinter = transLineItemsForPrinter.get(printerID);
                    lineItemsForPrinter.add(0, diningOption);
                    transLineItemsForPrinter.put(printerID, lineItemsForPrinter);
                }
            }
        }

        // determine whether or not to display the dining option on the expeditor printer
        if (transLineItemsForPrinter.containsKey(expeditorKPID)) {
            if (!remoteOrdersUseExpeditor) {
                if ((diningOption.getPrintsOnExpeditor()) && (!DataFunctions.isEmptyCollection(transLineItemsForPrinter.get(expeditorKPID)))) {
                    // add the dining option
                    ArrayList<TransactionLineItem> lineItemsForPrinter = transLineItemsForPrinter.get(expeditorKPID);
                    lineItemsForPrinter.add(0, diningOption);
                    transLineItemsForPrinter.put(expeditorKPID, lineItemsForPrinter);
                }
            }
            else if ((remoteOrdersUseExpeditor) && (!transactionData.getTransactionHeader().getRemoteOrder())) {
                if ((diningOption.getPrintsOnExpeditor()) && (!DataFunctions.isEmptyCollection(transLineItemsForPrinter.get(expeditorKPID)))) {
                    // add the dining option
                    ArrayList<TransactionLineItem> lineItemsForPrinter = transLineItemsForPrinter.get(expeditorKPID);
                    lineItemsForPrinter.add(0, diningOption);
                    transLineItemsForPrinter.put(expeditorKPID, lineItemsForPrinter);
                }
            }
            else if ((remoteOrdersUseExpeditor) && (transactionData.getTransactionHeader().getRemoteOrder())) {
                if (!DataFunctions.isEmptyCollection(transLineItemsForPrinter.get(expeditorKPID))) {
                    // add the dining option
                    ArrayList<TransactionLineItem> lineItemsForPrinter = transLineItemsForPrinter.get(expeditorKPID);
                    lineItemsForPrinter.add(0, diningOption);
                    transLineItemsForPrinter.put(expeditorKPID, lineItemsForPrinter);
                }
            }
        }

        // determine whether or not to display the dining option on the remote order printer
        if ((transLineItemsForPrinter.containsKey(remoteOrderKPID))
                && (!DataFunctions.isEmptyCollection(transLineItemsForPrinter.get(remoteOrderKPID)))
                && (transactionData.getTransactionHeader().getRemoteOrder())) {
            // add the dining option
            ArrayList<TransactionLineItem> lineItemsForPrinter = transLineItemsForPrinter.get(remoteOrderKPID);
            lineItemsForPrinter.add(0, diningOption);
            transLineItemsForPrinter.put(remoteOrderKPID, lineItemsForPrinter);
        }

    }

    /**
     * <p>Iterates through the transaction line items that should be printed on each kitchen printer and builds the details that should appear on each receipt.</p>
     *
     * @param spsLookup A {@link HashMap} whose {@link TransactionLineItem} key is a transaction line item and whose {@link String} value contains the simple print status
     * for the transaction line item.
     * @param transLineItemsForPrinter A {@link HashMap} whose key is the {@link Integer} ID of the kitchen printer and
     * whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to the products and modifiers that
     * should be sent to the printer.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the receipt details for the kitchen printer receipt.
     */
    @SuppressWarnings("Convert2streamapi")
    private ArrayList<HashMap> buildKPReceiptDetails (HashMap<TransactionLineItem, String> spsLookup, HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForPrinter) {

        if (DataFunctions.isEmptyMap(spsLookup)) {
            Logger.logMessage("The simple print status lookup passed to KPReceipt.buildKPReceiptDetails can't be null or empty!", log, Logger.LEVEL.ERROR);
        }

        if (DataFunctions.isEmptyMap(transLineItemsForPrinter)) {
            Logger.logMessage("The transaction line items mapped to a kitchen printer and passed to KPReceipt.buildKPReceiptDetails can be null or empty!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // get whether or not the expeditor will only print receipts if new products are added
        if (expeditorKPID > 0) {
            noExpeditorReceiptUnlessProductsAdded = noExpeditorReceiptUnlessProductsAdded();
            Logger.logMessage(String.format("The expeditor printer with an ID of %s within the revenue center with an ID of %s %s only print receipts if new products are added.",
                    Objects.toString(expeditorKPID, "N/A"),
                    Objects.toString(revenueCenterID, "N/A"),
                    Objects.toString((noExpeditorReceiptUnlessProductsAdded ? "will" : "won't"), "N/A")), log, Logger.LEVEL.TRACE);
        }

        // get the IDs off all printers so we can determine whether or not they will only print new products
        ArrayList<Integer> printerIDs = new ArrayList<>(transLineItemsForPrinter.keySet());
        printerOnlyPrintsNewProductsHM = getPrintOnlyPrintsNewProductsHM(printerIDs);
        if (!DataFunctions.isEmptyMap(printerOnlyPrintsNewProductsHM)) {
            for (Map.Entry<Integer, Boolean> entry : printerOnlyPrintsNewProductsHM.entrySet()) {
                Logger.logMessage(String.format("The printer with an ID of %s %s only print new products.",
                        Objects.toString(entry.getKey(), "N/A"),
                        Objects.toString(entry.getValue() ? "will" : "won't", "N/A")), log, Logger.LEVEL.TRACE);
            }
        }
        else {
            Logger.logMessage("No printers found in KPReceipt.buildKPReceiptDetails, unable to determine whether or not the printers only print new products!", log, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<HashMap> receiptDetails = new ArrayList<>();

        for (Map.Entry<Integer, ArrayList<TransactionLineItem>> entry : transLineItemsForPrinter.entrySet()) {
            int printerID = entry.getKey();
            ArrayList<TransactionLineItem> lineItems = entry.getValue();
            
            // set the maximum characters per line on the receipt
            if ((printerID > 0) && (!DataFunctions.isEmptyMap(printerIDToModelIDLookup)) && (printerIDToModelIDLookup.containsKey(printerID))) {
                int printerModelID = printerIDToModelIDLookup.get(printerID);
                KitchenPrinterModel kitchenPrinterModel = KitchenPrinterModel.get(printerModelID);
                if (kitchenPrinterModel != null) {
                    maxCharsPerReceiptLineNormal = kitchenPrinterModel.getMaxReceiptCharsPerLineNormal();
                    maxCharsPerReceiptLineLarge = kitchenPrinterModel.getMaxReceiptCharsPerLineLarge();
                }
            }

            // sort the transaction line items by their simple print status (PRINTED/UNPRINTED) and order number
            HashMap<String, HashMap<String, ArrayList<TransactionLineItem>>> sortedLineItemsForKPReceipt = sortLineItemsForKPReceipt(spsLookup, lineItems);
            logLineItemsBySimplePrintStatus(sortedLineItemsForKPReceipt);

            // don't create an expeditor receipt if noExpeditorReceiptUnlessProductsAdded is checked and there aren't any new expeditor products
            if ((printerID == expeditorKPID)
                    && (!remoteOrdersUseExpeditor)
                    && (noExpeditorReceiptUnlessProductsAdded)
                    && ((DataFunctions.isEmptyMap(sortedLineItemsForKPReceipt))
                        || ((!DataFunctions.isEmptyMap(sortedLineItemsForKPReceipt)) && (!sortedLineItemsForKPReceipt.containsKey(TypeData.SimplePrintStatus.UNPRINTED))))) {
                continue;
            }

            // determine whether or not to use the ADD ON header within the receipt header
            boolean useAddOn = useAddOn(printerID, sortedLineItemsForKPReceipt);

            // create the receipt header details
            ArrayList<HashMap> receiptHeaderDetails = buildKPReceiptHeaderDetails(printerID, useAddOn);
            if (!DataFunctions.isEmptyCollection(receiptHeaderDetails)) {
                receiptDetails.addAll(receiptHeaderDetails);
            }
            else {
                Logger.logMessage(String.format("No receipt header details were created for the transaction with an ID of %s for the printer with an ID of %s in KPReceipt.buildKPReceiptDetails!",
                        Objects.toString(transactionData.getPATransactionID(), "N/A"),
                        Objects.toString(printerID, "N/A")), log, Logger.LEVEL.ERROR);
            }

            // create receipt details for the products and modifiers within the transaction
            ArrayList<HashMap> productModifierDetails = buildKPReceiptProductModifierDetails(printerID, sortedLineItemsForKPReceipt);
            if (!DataFunctions.isEmptyCollection(productModifierDetails)) {
                receiptDetails.addAll(productModifierDetails);
            }
            else {
                Logger.logMessage(String.format("No receipt details were created for products and modifiers within the transaction " +
                        "with an ID of %s for the printer with an ID of %s in KPReceipt.buildKPReceiptDetails!",
                        Objects.toString(transactionData.getPATransactionID(), "N/A"),
                        Objects.toString(printerID, "N/A")), log, Logger.LEVEL.ERROR);
            }

            double totalNumberOfItems = calculateTheTotalNumberOfItems(printerID, receiptDetails);
            ArrayList<HashMap> receiptFooterDetails = buildReceiptFooterDetails(printerID, totalNumberOfItems);
            if (!DataFunctions.isEmptyCollection(receiptFooterDetails)) {
                receiptDetails.addAll(receiptFooterDetails);
            }
            else {
                Logger.logMessage(String.format("No receipt footer details were created for products and modifiers within the transaction " +
                        "with an ID of %s for the printer with an ID of %s in KPReceipt.buildKPReceiptDetails!",
                        Objects.toString(transactionData.getPATransactionID(), "N/A"),
                        Objects.toString(printerID, "N/A")), log, Logger.LEVEL.ERROR);
            }
        }

        return receiptDetails;
    }

    /**
     * <p>Queries to database to check whether or not the expeditor printer will only print receipts if new products should be printed on the expeditor printer.</p>
     *
     * @return Whether or not the expeditor printer will only print receipts if new products should be printed on the expeditor printer.
     */
    private boolean noExpeditorReceiptUnlessProductsAdded () {

        if (expeditorKPID <= 0) {
            Logger.logMessage("The ID of the expeditor kitchen printer must be greater than 0 in KPReceipt.noExpeditorReceiptUnlessProductsAdded", log, Logger.LEVEL.ERROR);
            return false;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetNoExpTktsUnlessExpProdAdded").addIDList(1, expeditorKPID);
        return sql.getSingleBooleanField(dataManager);
    }

    /**
     * <p>Queries to database to check whether or not the given printers only print new products.</p>
     *
     * @param printerIDs An {@link ArrayList} of {@link Integer} corresponding to the IDs of the printers to check if they only print new items.
     * @return A {@link HashMap} whose key is an {@link Integer} ID of the printer and whose value is a {@link Boolean} as to whether or not the printer only prints new items.
     */
    private HashMap<Integer, Boolean> getPrintOnlyPrintsNewProductsHM (ArrayList<Integer> printerIDs) {

        if (DataFunctions.isEmptyCollection(printerIDs)) {
            Logger.logMessage("No printer IDs have been passed to KPReceipt.getPrintOnlyPrintsNewProductsHM!", log, Logger.LEVEL.ERROR);
            return null;
        }

        HashMap<Integer, Boolean> printerOnlyPrintsNewProductsHM = new HashMap<>();

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetPrintersPrintOnlyNewItems").addIDList(1, printerIDs);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                printerOnlyPrintsNewProductsHM.put(HashMapDataFns.getIntVal(hm, "PRINTERID"), HashMapDataFns.getBooleanVal(hm, "PRINTONLYNEWITEMS"));
            }
        }

        return printerOnlyPrintsNewProductsHM;
    }

    /**
     * <p>Sorts the transaction line items that should be sent to a given printer based on whether or not they have been printed and then based on their order number.</p>
     *
     * @param spsLookup A {@link HashMap} whose {@link TransactionLineItem} key is a transaction line item and whose {@link String} value contains the simple print status
     * for the transaction line item.
     * @param lineItemsForPrinter An {@link ArrayList} of {@link TransactionLineItem} corresponding to the products and modifiers that should be sent to a given printer.
     * @return A {@link HashMap} whose key is a {@link String} of whether or not the products have been printed and whose value is a {@link HashMap} whose
     * key is the order number {@link String} and whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to transaction line items
     * with the same order number that have/haven't been printed.
     */
    private HashMap<String, HashMap<String, ArrayList<TransactionLineItem>>> sortLineItemsForKPReceipt (HashMap<TransactionLineItem, String> spsLookup,
                                                                                                        ArrayList<TransactionLineItem> lineItemsForPrinter) {
        if (DataFunctions.isEmptyMap(spsLookup)) {
            Logger.logMessage("The simple print status lookup passed to KPReceipt.sortLineItemsForKPReceipt can't be null or empty!", log, Logger.LEVEL.ERROR);
        }

        if (DataFunctions.isEmptyCollection(lineItemsForPrinter)) {
            Logger.logMessage("The transaction line items passed to KPReceipt.sortLineItemsForKPReceipt can't be null or empty!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // sort line items based on whether or not they have been printed
        ArrayList<TransactionLineItem> printedLineItems = new ArrayList<>();
        ArrayList<TransactionLineItem> unprintedLineItems = new ArrayList<>();
        // keep track of whether products are printed/unprinted, the modifiers will follow their parent product
        for (TransactionLineItem lineItem : lineItemsForPrinter) {
            if (spsLookup.containsKey(lineItem)) {
                if (spsLookup.get(lineItem).equalsIgnoreCase(TypeData.SimplePrintStatus.UNPRINTED)) {
                    unprintedLineItems.add(lineItem);
                }
                else if (spsLookup.get(lineItem).equalsIgnoreCase(TypeData.SimplePrintStatus.PRINTED)) {
                    printedLineItems.add(lineItem);
                }
            }
            else {
                Logger.logMessage(String.format("Unable to determine the simple print status of the the transaction line " +
                        "item %s in KPReceipt.sortLineItemsForKPReceipt, setting it as UNPRINTED.",
                        Objects.toString(lineItem.getProductName(), "N/A")), log, Logger.LEVEL.ERROR);
                unprintedLineItems.add(lineItem);
            }
        }

        HashMap<String, HashMap<String, ArrayList<TransactionLineItem>>> lineItemsSortedBySimplePrintStatus = new HashMap<>();

        // further sort the printed and unprinted line items based on their order number
        if (!DataFunctions.isEmptyCollection(printedLineItems)) {
            HashMap<String, ArrayList<TransactionLineItem>> lineItemsSortedByOrderNumber = sortLineItemsByOrderNumber(printedLineItems);
            if (!DataFunctions.isEmptyMap(lineItemsSortedByOrderNumber)) {
                lineItemsSortedBySimplePrintStatus.put(TypeData.SimplePrintStatus.PRINTED, lineItemsSortedByOrderNumber);
            }
        }
        if (!DataFunctions.isEmptyCollection(unprintedLineItems)) {
            HashMap<String, ArrayList<TransactionLineItem>> lineItemsSortedByOrderNumber = sortLineItemsByOrderNumber(unprintedLineItems);
            if (!DataFunctions.isEmptyMap(lineItemsSortedByOrderNumber)) {
                lineItemsSortedBySimplePrintStatus.put(TypeData.SimplePrintStatus.UNPRINTED, lineItemsSortedByOrderNumber);
            }
        }

        return lineItemsSortedBySimplePrintStatus;
    }

    /**
     * <p>Iterates through the given transaction line items and sorts them based on their order number.</p>
     *
     * @param lineItems An {@link ArrayList} of {@link TransactionLineItem} corresponding to the transaction line items to sort by order number.
     * @return A {@link HashMap} whose key is the order number {@link String} and whose value is an {@link ArrayList} of
     * {@link TransactionLineItem} corresponding to transaction line items with the same order number.
     */
    private HashMap<String, ArrayList<TransactionLineItem>> sortLineItemsByOrderNumber (ArrayList<TransactionLineItem> lineItems) {

        if (DataFunctions.isEmptyCollection(lineItems)) {
            Logger.logMessage("The transaction line items passed to KPReceipt.sortLineItemsByOrderNumber can't be null or empty!", log, Logger.LEVEL.ERROR);
            return null;
        }

        HashMap<String, ArrayList<TransactionLineItem>> lineItemsSortedByOrderNumber = new HashMap<>();
        ArrayList<TransactionLineItem> lineItemsForOrderNumber;
        // keep track of the order number for each product, modifiers will need to follow their parent product
        HashMap<Integer, String> productOrderNumber = new HashMap<>();
        for (TransactionLineItem lineItem : lineItems) {
            String orderNumber = "";
            if (lineItem.getPATransLineItemModID() <= 0) {
                // determine the order number and keep track for the product
                orderNumber = (StringFunctions.stringHasContent(lineItem.getOriginalOrderNumber()) ? lineItem.getOriginalOrderNumber() : lineItem.getOrderNumber());
                productOrderNumber.put(lineItem.getPATransLineItemID(), orderNumber);
            }
            else if (lineItem.getPATransLineItemModID() > 0) {
                // get the order number of the parent product for the modifer
                orderNumber = productOrderNumber.get(lineItem.getPATransLineItemID());
            }
            // do the sorting
            if (lineItemsSortedByOrderNumber.containsKey(orderNumber)) {
                lineItemsForOrderNumber = lineItemsSortedByOrderNumber.get(orderNumber);
            }
            else {
                lineItemsForOrderNumber = new ArrayList<>();
            }
            lineItemsForOrderNumber.add(lineItem);
            lineItemsSortedByOrderNumber.put(orderNumber, lineItemsForOrderNumber);
        }

        return lineItemsSortedByOrderNumber;
    }

    /**
     * <p>Iterates through the sorted line items for the kitchen printer receipt and logs them based on how they were sorted.</p>
     *
     * @param sortedLineItemsForKPReceipt A {@link HashMap} whose key is a {@link String} of whether or not the products have been printed and whose value is a {@link HashMap} whose
     * key is the order number {@link String} and whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to transaction line items
     * with the same order number that have/haven't been printed.
     */
    @SuppressWarnings("Convert2streamapi")
    private void logLineItemsBySimplePrintStatus (HashMap<String, HashMap<String, ArrayList<TransactionLineItem>>> sortedLineItemsForKPReceipt) {

        // make sure the sorted line items for the kitchen printer receipt Map has content
        if (DataFunctions.isEmptyMap(sortedLineItemsForKPReceipt)) {
            Logger.logMessage("The the sorted line items for the kitchen printer receipt Map passed " +
                    "to KPReceipt.logLineItemsBySimplePrintStatus can't be null or empty!", log, Logger.LEVEL.ERROR);
            return;
        }

        for (Map.Entry<String, HashMap<String, ArrayList<TransactionLineItem>>> simplePrintStatusEntry : sortedLineItemsForKPReceipt.entrySet()) {
            String simplePrintStatus = simplePrintStatusEntry.getKey();
            HashMap<String, ArrayList<TransactionLineItem>> lineItemsForOrder = simplePrintStatusEntry.getValue();
            if ((StringFunctions.stringHasContent(simplePrintStatus)) && (simplePrintStatus.equalsIgnoreCase(TypeData.SimplePrintStatus.UNPRINTED))) {
                // log which transaction line items haven't been printed yet
                if (!DataFunctions.isEmptyMap(lineItemsForOrder)) {
                    Logger.logMessage("The following transaction line items haven't been printed yet.", log, Logger.LEVEL.TRACE);
                    for (ArrayList<TransactionLineItem> unprintedLineItems : lineItemsForOrder.values()) {
                        if (!DataFunctions.isEmptyCollection(unprintedLineItems)) {
                            for (TransactionLineItem unprintedLineItem : unprintedLineItems) {
                                Logger.logMessage("--" + unprintedLineItem.toString(), log, Logger.LEVEL.TRACE);
                            }
                        }
                    }
                }
            }
            else if ((StringFunctions.stringHasContent(simplePrintStatus)) && (simplePrintStatus.equalsIgnoreCase(TypeData.SimplePrintStatus.UNPRINTED))) {
                // log which transaction line items have been printed already
                if (!DataFunctions.isEmptyMap(lineItemsForOrder)) {
                    Logger.logMessage("The following transaction line items have already been printed.", log, Logger.LEVEL.TRACE);
                    for (ArrayList<TransactionLineItem> printedLineItems : lineItemsForOrder.values()) {
                        if (!DataFunctions.isEmptyCollection(printedLineItems)) {
                            for (TransactionLineItem printedLineItem : printedLineItems) {
                                Logger.logMessage("--" + printedLineItem.toString(), log, Logger.LEVEL.TRACE);
                            }
                        }
                    }
                }
            }
        }

    }

    /**
     * <p>Determines whether or not to use the add on header.</p>
     *
     * @param printerID ID of the printer the receipt is for.
     * @param sortedLineItemsForKPReceipt A {@link HashMap} whose key is a {@link String} of whether or not the products have been printed and whose value is a {@link HashMap} whose
     * key is the order number {@link String} and whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to transaction line items
     * with the same order number that have/haven't been printed.
     * @return whether or not to use the add on header.
     */
    private boolean useAddOn (int printerID, HashMap<String, HashMap<String, ArrayList<TransactionLineItem>>> sortedLineItemsForKPReceipt) {

        if (DataFunctions.isEmptyMap(sortedLineItemsForKPReceipt)) {
            Logger.logMessage("The transaction line items that have been sorted for kitchen printer receipts and passed to KPReceipt.useAddOn, can't be null or empty!", log, Logger.LEVEL.ERROR);
            return false;
        }

        if (DataFunctions.isEmptyMap(printerOnlyPrintsNewProductsHM)) {
            Logger.logMessage("The only prints new products HashMap can't be null or empty in KPReceipt.useAddOn!", log, Logger.LEVEL.ERROR);
            return false;
        }

        if (transactionData == null) {
            Logger.logMessage("No transaction data found in KPReceipt.useAddOn!", log, Logger.LEVEL.ERROR);
            return false;
        }

        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("No transaction header was found within the transaction in KPReceipt.useAddOn!", log, Logger.LEVEL.ERROR);
            return false;
        }

        String currentOrderNumber = transactionData.getTransactionHeader().getOrderNumber();

        boolean onlyPrintNewProducts = printerOnlyPrintsNewProductsHM.get(printerID);
        boolean hasUnprintedProducts = !DataFunctions.isEmptyMap(sortedLineItemsForKPReceipt.get(TypeData.SimplePrintStatus.UNPRINTED));
        boolean hasProductsFromPreviousOrder = false;
        for (Map.Entry<String, HashMap<String, ArrayList<TransactionLineItem>>> entry : sortedLineItemsForKPReceipt.entrySet()) {
            if ((!DataFunctions.isEmptyMap(entry.getValue())) && (!DataFunctions.isEmptyCollection(entry.getValue().keySet()))) {
                for (String orderNumber : entry.getValue().keySet()) {
                    if (!orderNumber.equalsIgnoreCase(currentOrderNumber)) {
                        hasProductsFromPreviousOrder = true;
                        break;
                    }
                }
            }
            if (hasProductsFromPreviousOrder) {
                break;
            }
        }
        boolean hasProductsFromCurrentOrder = false;
        for (Map.Entry<String, HashMap<String, ArrayList<TransactionLineItem>>> entry : sortedLineItemsForKPReceipt.entrySet()) {
            if ((!DataFunctions.isEmptyMap(entry.getValue())) && (!DataFunctions.isEmptyCollection(entry.getValue().keySet()))) {
                for (String orderNumber : entry.getValue().keySet()) {
                    if (orderNumber.equalsIgnoreCase(currentOrderNumber)) {
                        hasProductsFromCurrentOrder = true;
                        break;
                    }
                }
            }
            if (hasProductsFromCurrentOrder) {
                break;
            }
        }

        return ((onlyPrintNewProducts) && (hasUnprintedProducts) && (hasProductsFromPreviousOrder) && (hasProductsFromCurrentOrder));
    }

    /**
     * <p>Builds a receipt header for kitchen printer receipts.</p>
     *
     * @param printerID The ID of the printer to build the receipt header for.
     * @param useAddOn Whether or not to add ADD ON to the receipt header.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the receipt header details.
     */
    private ArrayList<HashMap> buildKPReceiptHeaderDetails (int printerID, boolean useAddOn) {

        if (printerID <= 0) {
            Logger.logMessage("The ID of the printer passed to KPReceipt.buildKPReceiptHeaderDetails must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (transactionData == null) {
            Logger.logMessage("No transaction data found in KPReceipt.buildKPReceiptHeaderDetails!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("No transaction header was found within the transaction in KPReceipt.buildKPReceiptHeaderDetails!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (renderer == null) {
            Logger.logMessage("No renderer found in KPReceipt.buildKPReceiptHeaderDetails!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // build the receipt header
        KitchenReceiptHeader kitchenReceiptHeader =
                new KitchenReceiptHeader()
                .addPATransactionID(transactionData.getPATransactionID())
                .addOnlineOrder(transactionData.getTransactionHeader().getOnlineOrder())
                .addTransactionDate(transactionData.getTransactionHeader().getTransactionDate())
                .addQueueTime(transactionData.getTransactionHeader().getQueueTime())
                .addPrintStatusID(PrintStatusType.WAITING)
                .addTerminalID(transactionData.getTransactionHeader().getTerminalID())
                .addPAOrderTypeID(transactionData.getTransactionHeader().getPAOrderTypeID())
                .addPersonName(transactionData.getTransactionHeader().getPersonName())
                .addPhone(transactionData.getTransactionHeader().getPhone())
                .addTransComment(transactionData.getTransactionHeader().getTransactionComment())
                .addPickUpDeliveryNote(transactionData.getTransactionHeader().getPickupDeliveryNote())
                .addTransName(transactionData.getTransactionHeader().getTransactionName())
                .addTransNameLabel(transactionData.getTransactionHeader().getTransactionNameLabel())
                .addEstimatedOrderTime(transactionData.getTransactionHeader().getEstimatedOrderTime())
                .addOrderNum(transactionData.getTransactionHeader().getOrderNumber())
                .addTransTypeID(transactionData.getTransactionHeader().getTransactionTypeID())
                .addPrevKDSOrderNums(StringFunctions.buildStringFromGenericArr(transactionData.getTransactionHeader().getPreviousOrderNumbersInTransaction(), ","))
                .addPrinterName(KitchenReceiptHelper.getPrinterNameFromID(log, dataManager, printerID))
                .addDeliveryLocation((transactionData.getTransactionHeader().getPAOrderTypeID() == TypeData.OrderType.DELIVERY ? transactionData.getTransactionHeader().getAddress() : null))
                .addPrinterID(printerID)
                .addPrinterHostID(transactionData.getTransactionHeader().getPrinterHostID())
                .addCashierName(transactionData.getTransactionHeader().getCashierName())
                .addUseAddOn(useAddOn)
                .addRenderer(renderer)
                .addDataManager(dataManager)
                .addLog(log)
                .addMaxReceiptCharsPerLineNormal(maxCharsPerReceiptLineNormal)
                .addMaxReceiptCharsPerLineLarge(maxCharsPerReceiptLineLarge);

        return kitchenReceiptHeader.build();
    }

    /**
     * <p>Creates kitchen printer receipt details for products and modifiers within the transaction that should appear on the printer.</p>
     *
     * @param printerID The ID of the printer the receipt details should be created for.
     * @param sortedLineItemsForKPReceipt A {@link HashMap} whose key is a {@link String} of whether or not the products have been printed and whose value is a {@link HashMap} whose
     * key is the order number {@link String} and whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to transaction line items
     * with the same order number that have/haven't been printed.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to kitchen printer receipt details for products and modifiers.
     */
    private ArrayList<HashMap> buildKPReceiptProductModifierDetails (int printerID, HashMap<String, HashMap<String, ArrayList<TransactionLineItem>>> sortedLineItemsForKPReceipt) {
        ArrayList<HashMap> productModifierDetails = new ArrayList<>();

        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KPReceipt.buildKPReceiptProductModifierDetails must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyMap(sortedLineItemsForKPReceipt)) {
            Logger.logMessage(String.format("The sorted transaction line items passed to KPReceipt.buildKPReceiptProductModifierDetails can't be null or empty for the transaction with an ID of %s!",
                    Objects.toString((transactionData != null ? transactionData.getPATransactionID() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyMap(printerOnlyPrintsNewProductsHM)) {
            Logger.logMessage("The only prints new products HashMap can't be null or empty in KPReceipt.buildKPReceiptProductModifierDetails!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // check whether or not order numbers wrapped past the maximum order number in the revenue center
        boolean didOrderNumbersWrap = KitchenReceiptHelper.didOrderNumbersWrap(log, dataManager, transactionData);
        Logger.logMessage(String.format("Order numbers within the transaction with an ID of %s %s wrap past the maximum allowed order number in the revenue center.",
                Objects.toString(transactionData.getPATransactionID(), "N/A"),
                Objects.toString((didOrderNumbersWrap ? "did" : "didn't"), "N/A")), log, Logger.LEVEL.TRACE);

        // check whether or not the printer will only print new products
        boolean printOnlyNewItems = (printerOnlyPrintsNewProductsHM.containsKey(printerID) ? printerOnlyPrintsNewProductsHM.get(printerID) : false);

        KitchenReceiptDetail kitchenReceiptDetail;
        int kmsStationID = -1;
        int kmsOrderStatusID = -1;
        int printerHostID = KitchenReceiptHelper.getPrinterHostID(log, dataManager, printerID);

        // print the products that haven't been printed yet
        if ((sortedLineItemsForKPReceipt.containsKey(TypeData.SimplePrintStatus.UNPRINTED))
                && (!DataFunctions.isEmptyMap(sortedLineItemsForKPReceipt.get(TypeData.SimplePrintStatus.UNPRINTED)))
                && (!printOnlyNewItems)) {
            // add an empty line
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                            .addPrinterID(printerID)
                            .addKMSStationID(kmsStationID)
                            .addPrintStatusID(PrintStatusType.WAITING)
                            .addKMSOrderStatusID(kmsOrderStatusID)
                            .addPrinterHostID(printerHostID)
                            .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
                            .addRenderer(renderer)
                            .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
            productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
            // add the Already Sent sub header
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                            .addPrinterID(printerID)
                            .addKMSStationID(kmsStationID)
                            .addPrintStatusID(PrintStatusType.WAITING)
                            .addKMSOrderStatusID(kmsOrderStatusID)
                            .addPrinterHostID(printerHostID)
                            .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + "****     NEW PRODUCTS     ****")
                            .addRenderer(renderer)
                            .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
            productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));

            // sort the line items by descending order number
            LinkedHashMap<String, ArrayList<TransactionLineItem>> lineItemsByDescOrdNum;
            if (!didOrderNumbersWrap) {
                lineItemsByDescOrdNum = HashMapDataFns.makeDescKeyMap(sortedLineItemsForKPReceipt.get(TypeData.SimplePrintStatus.UNPRINTED));
            }
            else {
                lineItemsByDescOrdNum = sortOrderNumbersWhenWrapOccurred(sortedLineItemsForKPReceipt.get(TypeData.SimplePrintStatus.UNPRINTED));
            }
            // create the print queue details
            addTransLineItemsByOrderToKPReceipt(lineItemsByDescOrdNum, productModifierDetails, printerID, kmsStationID, kmsOrderStatusID, printerHostID);
        }
        else if ((sortedLineItemsForKPReceipt.containsKey(TypeData.SimplePrintStatus.UNPRINTED))
                && (!DataFunctions.isEmptyMap(sortedLineItemsForKPReceipt.get(TypeData.SimplePrintStatus.UNPRINTED)))
                && (printOnlyNewItems)) {

            // sort the line items by descending order number
            LinkedHashMap<String, ArrayList<TransactionLineItem>> lineItemsByDescOrdNum;
            if (!didOrderNumbersWrap) {
                lineItemsByDescOrdNum = HashMapDataFns.makeDescKeyMap(sortedLineItemsForKPReceipt.get(TypeData.SimplePrintStatus.UNPRINTED));
            }
            else {
                lineItemsByDescOrdNum = sortOrderNumbersWhenWrapOccurred(sortedLineItemsForKPReceipt.get(TypeData.SimplePrintStatus.UNPRINTED));
            }
            // create the print queue details
            addTransLineItemsByOrderToKPReceipt(lineItemsByDescOrdNum, productModifierDetails, printerID, kmsStationID, kmsOrderStatusID, printerHostID);
        }

        // print the products that have already been printed
        if ((sortedLineItemsForKPReceipt.containsKey(TypeData.SimplePrintStatus.PRINTED))
                && (!DataFunctions.isEmptyMap(sortedLineItemsForKPReceipt.get(TypeData.SimplePrintStatus.PRINTED)))
                && (!printOnlyNewItems)) {
            // add an empty line
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                            .addPrinterID(printerID)
                            .addKMSStationID(kmsStationID)
                            .addPrintStatusID(PrintStatusType.WAITING)
                            .addKMSOrderStatusID(kmsOrderStatusID)
                            .addPrinterHostID(printerHostID)
                            .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
                            .addRenderer(renderer)
                            .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
            productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
            // add the Already Sent sub header
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                            .addPrinterID(printerID)
                            .addKMSStationID(kmsStationID)
                            .addPrintStatusID(PrintStatusType.WAITING)
                            .addKMSOrderStatusID(kmsOrderStatusID)
                            .addPrinterHostID(printerHostID)
                            .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + "****     ALREADY SENT     ****")
                            .addRenderer(renderer)
                            .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
            productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));

            // sort the line items by descending order number
            LinkedHashMap<String, ArrayList<TransactionLineItem>> lineItemsByDescOrdNum;
            if (!didOrderNumbersWrap) {
                lineItemsByDescOrdNum = HashMapDataFns.makeDescKeyMap(sortedLineItemsForKPReceipt.get(TypeData.SimplePrintStatus.PRINTED));
            }
            else {
                lineItemsByDescOrdNum = sortOrderNumbersWhenWrapOccurred(sortedLineItemsForKPReceipt.get(TypeData.SimplePrintStatus.PRINTED));
            }
            // create the print queue details
            addTransLineItemsByOrderToKPReceipt(lineItemsByDescOrdNum, productModifierDetails, printerID, kmsStationID, kmsOrderStatusID, printerHostID);
        }

        return productModifierDetails;
    }

    /**
     * <p>Creates print queue details for the KP receipt sor the transaction line items sorted by descending order number.</p>
     *
     * @param lineItemsByDescOrdNum A {@link LinkedHashMap} whose {@link String} key is the order number and whose value is an {@link ArrayList} of {@link TransactionLineItem}
     * corresponding to the transaction line items for the order number.
     * @param productModifierDetails An {@link ArrayList} of {@link HashMap} corresponding to kitchen printer receipt details for products and modifiers.
     * @param printerID The ID of the kitchen printer the print queue details will be sent to.
     * @param kmsStationID The ID of the KMS station the print queue details will be sent to.
     * @param kmsOrderStatusID The KMS order status for the print queue details.
     * @param printerHostID The ID of the printer host the print queue details will be sent to.
     */
    private void addTransLineItemsByOrderToKPReceipt (LinkedHashMap<String, ArrayList<TransactionLineItem>> lineItemsByDescOrdNum,
                                                      ArrayList<HashMap> productModifierDetails,
                                                      int printerID,
                                                      int kmsStationID,
                                                      int kmsOrderStatusID,
                                                      int printerHostID) {

        // make sure the line items are valid
        if (DataFunctions.isEmptyMap(lineItemsByDescOrdNum)) {
            Logger.logMessage("The transaction line items sorted by descending order number and passed " +
                    "to KPReceipt.addTransLineItemsByOrderToKPReceipt can't be null or empty!", log, Logger.LEVEL.ERROR);
            return;
        }

        // make sure the details are valid
        if (productModifierDetails == null) {
            Logger.logMessage("The product and modifier print queue details list passed to KPReceipt.addTransLineItemsByOrderToKPReceipt can't be null!", log, Logger.LEVEL.ERROR);
            return;
        }

        for (Map.Entry<String, ArrayList<TransactionLineItem>> orderNumberEntry : lineItemsByDescOrdNum.entrySet()) {
            // add an empty line
            KitchenReceiptDetail kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                            .addPrinterID(printerID)
                            .addKMSStationID(kmsStationID)
                            .addPrintStatusID(PrintStatusType.WAITING)
                            .addKMSOrderStatusID(kmsOrderStatusID)
                            .addPrinterHostID(printerHostID)
                            .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
                            .addRenderer(renderer)
                            .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
            productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
            // add the Order Number sub header
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                            .addPrinterID(printerID)
                            .addKMSStationID(kmsStationID)
                            .addPrintStatusID(PrintStatusType.WAITING)
                            .addKMSOrderStatusID(kmsOrderStatusID)
                            .addPrinterHostID(printerHostID)
                            .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + "--- Order Number: " + (StringFunctions.stringHasContent(orderNumberEntry.getKey()) ? orderNumberEntry.getKey() : "N/A") + "  ---")
                            .addRenderer(renderer)
                            .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
            productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
            ArrayList<TransactionLineItem> lineItems = orderNumberEntry.getValue();
            if (!DataFunctions.isEmptyCollection(lineItems)) {
                for (TransactionLineItem lineItem : lineItems) {
                    // add each line item
                    kitchenReceiptDetail =
                            new KitchenReceiptDetail()
                                    .addPrinterID(printerID)
                                    .addKMSStationID(kmsStationID)
                                    .addPrintStatusID(PrintStatusType.WAITING)
                                    .addKMSOrderStatusID(kmsOrderStatusID)
                                    .addPrinterHostID(printerHostID)
                                    .addLineDetail(KitchenReceiptHelper.buildLineItemText(log, lineItem, true, false, false))
                                    .addProductID(lineItem.getPAPluID())
                                    .addQuantity(lineItem.getQuantity())
                                    .addIsModifier((lineItem.getPATransLineItemModID() > 0))
                                    .addTransLineItemID(lineItem.getPATransLineItemID())
                                    .addLinkedFromIDs(lineItem.getLinkedFromIDs())
                                    .addRenderer(renderer)
                                    .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
                    productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
                }
            }
        }
    }

    /**
     * <p>Sorts the order numbers within the transaction when the order numbers within the transaction wrapped past the
     * maximum allowed order number within the same revenue center as the transaction.</p>
     *
     * @param ordersToSort A {@link HashMap} to sort by order number whose {@link String} key is the order number and whose
     * value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to transaction line items within the order.
     * @return A {@link LinkedHashMap} whose {@link String} key is the order number and whose value is an {@link ArrayList}
     * of {@link TransactionLineItem} corresponding to transaction line items within the order.
     */
    @SuppressWarnings("Convert2streamapi")
    private LinkedHashMap<String, ArrayList<TransactionLineItem>> sortOrderNumbersWhenWrapOccurred (HashMap<String, ArrayList<TransactionLineItem>> ordersToSort) {

        // make sure the orders that need sorting are valid
        if (DataFunctions.isEmptyMap(ordersToSort)) {
            Logger.logMessage("The orders and transaction line items within those orders passed to KPReceipt.sortOrderNumbersWhenWrapOccurred " +
                    "can't be null or empty!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // calculate the ranges
        int startOrderNumber = transactionData.getTransactionHeader().getStartOrderNumber();
        int maxOrderNumber = transactionData.getTransactionHeader().getMaxOrderNumber();
        int halfTotalOrderNumRange = Math.round((startOrderNumber + maxOrderNumber) / 2);
        MMHRange lowerRange = new MMHRange(startOrderNumber, halfTotalOrderNumRange);
        MMHRange upperRange = new MMHRange(halfTotalOrderNumRange, maxOrderNumber);

        // sort the order numbers within the transaction based on the range they are within
        ArrayList<String> lowerRangeOrderNums = new ArrayList<>();
        ArrayList<String> upperRangeOrderNums = new ArrayList<>();
        for (String orderNumberStr : ordersToSort.keySet()) {
            int orderNumber = -1;
            if (NumberUtils.isNumber(orderNumberStr)) {
                orderNumber = Integer.parseInt(orderNumberStr);
            }
            else {
                String[] tokens = orderNumberStr.split("\\-", -1);
                if ((!DataFunctions.isEmptyGenericArr(tokens)) && (tokens.length == 2) && (NumberUtils.isNumber(tokens[1]))) {
                    orderNumber = Integer.parseInt(tokens[1]);
                }
            }
            if ((orderNumber > 0) && (lowerRange.containsInclusive(orderNumber))) {
                lowerRangeOrderNums.add(orderNumberStr);
            }
            else if ((orderNumber > 0) && (upperRange.containsInclusive(orderNumber))) {
                upperRangeOrderNums.add(orderNumberStr);
            }
        }

        // sort the lower range order numbers
        if (!DataFunctions.isEmptyCollection(lowerRangeOrderNums)) {
            Collections.sort(lowerRangeOrderNums);
            Collections.reverse(lowerRangeOrderNums);
        }

        // sort the upper range order numbers
        if (!DataFunctions.isEmptyCollection(upperRangeOrderNums)) {
            Collections.sort(upperRangeOrderNums);
            Collections.reverse(upperRangeOrderNums);
        }

        // build the LinkedHashMap sorted by lower range order numbers first and upper range order numbers second
        LinkedHashMap<String, ArrayList<TransactionLineItem>> transLineItemsSortedByOrderNum = new LinkedHashMap<>();
        if (!DataFunctions.isEmptyCollection(lowerRangeOrderNums)) {
            for (String orderNumber : lowerRangeOrderNums) {
                if (ordersToSort.containsKey(orderNumber)) {
                    transLineItemsSortedByOrderNum.put(orderNumber, ordersToSort.get(orderNumber));
                }
            }
        }
        if (!DataFunctions.isEmptyCollection(upperRangeOrderNums)) {
            for (String orderNumber : upperRangeOrderNums) {
                if (ordersToSort.containsKey(orderNumber)) {
                    transLineItemsSortedByOrderNum.put(orderNumber, ordersToSort.get(orderNumber));
                }
            }
        }

        return transLineItemsSortedByOrderNum;
    }

    /**
     * <p>Iterates through the kitchen printer receipt details and calculates the total number of items on the receipt, excluding modifiers for a product.</p>
     *
     * @param printerID ID of the printer to calculate the total number of items for.
     * @param kpReceiptDetails An {@link ArrayList} of {@link HashMap} corresponding to the receipt details for the kitchen printer receipt.
     * @return The total number of items on the receipt.
     */
    private double calculateTheTotalNumberOfItems (int printerID, ArrayList<HashMap> kpReceiptDetails) {

        if (DataFunctions.isEmptyCollection(kpReceiptDetails)) {
            Logger.logMessage("The kitchen printer receipt details passed to KPReceipt.calculateTheTotalNumberOfItems can't be null or empty!", log, Logger.LEVEL.ERROR);
            return -1.0d;
        }

        // calculate the total number of items for the printer
        double total = 0.0d;
        for (HashMap kpReceiptDetail : kpReceiptDetails) {
            if (!DataFunctions.isEmptyMap(kpReceiptDetail)) {
                double quantity = HashMapDataFns.getDoubleVal(kpReceiptDetail, "QUANTITY");
                String lineDetail = KitchenPrinterCommon.decode(HashMapDataFns.getStringVal(kpReceiptDetail, "LINEDETAIL"), log);
                Pattern productPattern = Pattern.compile("^[^\\d]*(\\d+X).*");
                if ((StringFunctions.stringHasContent(lineDetail)) && (productPattern.matcher(lineDetail).matches()) && (printerID == HashMapDataFns.getIntVal(kpReceiptDetail, "PRINTERID")) && (quantity > 0.0d)) {
                    total += quantity;
                }
            }
        }

        return total;
    }

    /**
     * <p>Adds the total number of items, a barcode (if applicable) and the command to cut the kitchen printer receipt.</p>
     *
     * @param printerID The ID of the printer containing the receipt to add the receipt footer to.
     * @param totalNumberOfItems The total number of items on the kitchen printer receipt.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to kitchen printer receipt details for the receipt footer.
     */
    private ArrayList<HashMap> buildReceiptFooterDetails (int printerID, double totalNumberOfItems) {

        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KPReceipt.buildReceiptFooterDetails must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (totalNumberOfItems <= 0.0d) {
            Logger.logMessage("The total number of items passed to KPReceipt.buildReceiptFooterDetails must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (transactionData == null) {
            Logger.logMessage("The transaction data in KPReceipt.buildReceiptFooterDetails can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("The transaction header data in KPReceipt.buildReceiptFooterDetails can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<HashMap> kpReceiptFooterDetails = new ArrayList<>();
        KitchenReceiptDetail kitchenReceiptDetail;
        int kmsStationID = -1;
        int kmsOrderStatusID = -1;
        int printerHostID = KitchenReceiptHelper.getPrinterHostID(log, dataManager, printerID);
        // add an empty line
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
                .addPrinterID(printerID)
                .addKMSStationID(kmsStationID)
                .addPrintStatusID(PrintStatusType.WAITING)
                .addKMSOrderStatusID(kmsOrderStatusID)
                .addPrinterHostID(printerHostID)
                .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
                .addRenderer(renderer)
                .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
        kpReceiptFooterDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        // add a line of asterisks
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
                .addPrinterID(printerID)
                .addKMSStationID(kmsStationID)
                .addPrintStatusID(PrintStatusType.WAITING)
                .addKMSOrderStatusID(kmsOrderStatusID)
                .addPrinterHostID(printerHostID)
                .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + "******************************")
                .addRenderer(renderer)
                .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
        kpReceiptFooterDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        // add an empty line
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
                .addPrinterID(printerID)
                .addKMSStationID(kmsStationID)
                .addPrintStatusID(PrintStatusType.WAITING)
                .addKMSOrderStatusID(kmsOrderStatusID)
                .addPrinterHostID(printerHostID)
                .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
                .addRenderer(renderer)
                .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
        kpReceiptFooterDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        // add the total number of items
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
                .addPrinterID(printerID)
                .addKMSStationID(kmsStationID)
                .addPrintStatusID(PrintStatusType.WAITING)
                .addKMSOrderStatusID(kmsOrderStatusID)
                .addPrinterHostID(printerHostID)
                .addLineDetail(String.format("Total Number of Items: %s", Math.round(totalNumberOfItems)))
                .addRenderer(renderer)
                .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
        kpReceiptFooterDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        // add the barcode if applicable
        if (shouldPrintBarcode(printerID, transactionData.getTransactionHeader().getRevenueCenterID())) {
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(PrintStatusType.WAITING)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(KitchenPrinterReceiptFormatter.BARCODE+transactionData.getPATransactionID())
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
            kpReceiptFooterDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }
        // add a command to cut the receipt paper
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
                .addPrinterID(printerID)
                .addKMSStationID(kmsStationID)
                .addPrintStatusID(PrintStatusType.WAITING)
                .addKMSOrderStatusID(kmsOrderStatusID)
                .addPrinterHostID(printerHostID)
                .addLineDetail(KitchenPrinterReceiptFormatter.JPOS_CUT)
                .addRenderer(renderer)
                .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
        kpReceiptFooterDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));

        return kpReceiptFooterDetails;
    }

    /**
     * <p>Check whether or not to print a barcode on the receipt based on the revenue center setting.</p>
     *
     * @param printerID ID of the printer that will print the kitchen receipt.
     * @param revenueCenterID ID of the revenue center to check.
     * @return Whether or not to print a barcode on the receipt based on the revenue center setting.
     */
    private boolean shouldPrintBarcode (int printerID, int revenueCenterID) {

        try {
            if (printerID <= 0) {
                Logger.logMessage("The printer ID passed to KPReceipt.shouldPrintBarcode must be greater than 0!", log, Logger.LEVEL.ERROR);
                return false;
            }

            if (revenueCenterID <= 0) {
                Logger.logMessage("The revenue center ID passed to KPReceipt.shouldPrintBarcode must be greater than 0!", log, Logger.LEVEL.ERROR);
                return false;
            }

            if (dataManager == null) {
                Logger.logMessage("The DataManager in KPReceipt.shouldPrintBarcode can't be null!", log, Logger.LEVEL.ERROR);
                return false;
            }

            // if the printer type is a TM-U220 don't print a barcode because it isn't supported
            if ((!DataFunctions.isEmptyMap(printerIDToModelIDLookup)) && (printerIDToModelIDLookup.containsKey(printerID)) && (printerIDToModelIDLookup.get(printerID) == KitchenPrinterModel.TM_U220.getPrinterModelID())) {
                return false;
            }

            // query the database
            DynamicSQL sql =
                    new DynamicSQL("data.kms.GetShouldPrintBarcode")
                    .addIDList(1, revenueCenterID);
            return sql.getSingleBooleanField(dataManager);
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not to add a barcode to kitchen " +
                    "printer receipts within the revenue center with an ID of %s in KPReceipt.shouldPrintBarcode!",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return false;
    }

}