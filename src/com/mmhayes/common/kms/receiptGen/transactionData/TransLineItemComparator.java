package com.mmhayes.common.kms.receiptGen.transactionData;

import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHTree;

import java.util.Comparator;
import java.util.HashMap;

public class TransLineItemComparator implements Comparator<MMHTree<TransactionLineItem>> {
    //

    public TransLineItemComparator(){
        //
    }
    public int compare(MMHTree<TransactionLineItem> first , MMHTree<TransactionLineItem> second) {
        int valueA = first.getData().getPATransLineItemID();
        int valueB = second.getData().getPATransLineItemID();

        return Integer.compare(valueA, valueB);
    }
}
