package com.mmhayes.common.kms.receiptGen.transactionData;

/*
    Last Updated (automatically updated by SVN)
    $Author: crking $: Author of last commit
    $Date: 2020-10-07 11:33:35 -0400 (Wed, 07 Oct 2020) $: Date of last commit
    $Rev: 12811 $: Revision of last commit
    Notes: Represents a line item within a transaction.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kms.types.KMSOrderStatus;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHTree;
import com.mmhayes.common.utils.StringFunctions;

import java.util.*;
import java.util.Map.Entry;

/**
 * <p>Represents a line item within a transaction.</p>
 *
 */
public class TransactionLineItem {

    // private member variables of a TransactionLineItem
    private int paTransactionID = -1;
    private int paTransLineItemID = -1;
    private int paTransLineItemModID = -1;
    private int paPluID = -1;
    private int printStatusID = -1;
    private int kmsOrderStatusID = -1;
    private int[] printerIDsMappedToProduct = null;
    private int[] kdsStationPrinterIDsMappedToProduct = null;
    private int[] kmsStationIDsMappedToProduct = null;
    private double quantity = -1.0d;
    private String productName = "";
    private String pluCode = "";
    private String orderNumber = "";
    private String originalOrderNumber = "";
    private boolean diningOption = false;
    private boolean printsOnExpeditor = false;
    private String linkedFromIDs = "";
    private HashMap prepOptionData = null;

    /**
     * <p>Builds a transaction line item.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param productAndModifiers A {@link Entry} whose key is the product {@link HashMap} and whose value
     * is an {@link ArrayList} of {@link HashMap} corresponding to any modifiers for the product.
     * @param printersMappedToProducts A {@link HashMap} whose key is the {@link Integer} ID of a product and whose value is an int array corresponding
     * to the IDs of prep printers mapped to the given products.
     * @param kdsStationsMappedToProducts A {@link HashMap} whose key is the {@link Integer} ID of a product and whose value is an int array corresponding
     * to the IDs of KDS prep stations mapped to the given products.
     * @param kmsStationsMappedToProducts A {@link HashMap} whose key is the {@link Integer} ID of a product and whose value is an int array corresponding
     * to the IDs of KMS prep station mapped to the given products.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return The newly constructed {@link MMHTree} of {@link TransactionLineItem}.
     */
    @SuppressWarnings("Convert2streamapi")
    public static MMHTree<TransactionLineItem> buildFromDB (DataManager dataManager,
                                                            Map.Entry<HashMap, ArrayList<HashMap>> productAndModifiers,
                                                            HashMap<Integer, int[]> printersMappedToProducts,
                                                            HashMap<Integer, int[]> kdsStationsMappedToProducts,
                                                            HashMap<Integer, int[]> kmsStationsMappedToProducts,
                                                            String log) {

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionLineItem.buildFromDB can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if ((productAndModifiers == null) || (DataFunctions.isEmptyMap(productAndModifiers.getKey()))) {
            Logger.logMessage("No product or modifier line items have been passed to TransactionLineItem.buildFromDB!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // build the transaction line item
        MMHTree<TransactionLineItem> productTransactionLineItem = new MMHTree<>(buildFromHM(dataManager, productAndModifiers.getKey(), printersMappedToProducts, kdsStationsMappedToProducts, kmsStationsMappedToProducts, log));
        // add any modifiers
        if (!DataFunctions.isEmptyCollection(productAndModifiers.getValue())) {
            for (HashMap modifierHM : productAndModifiers.getValue()) {
                if (!DataFunctions.isEmptyMap(modifierHM)) {
                    // create the modifier transaction line item and add it to the product transaction line item
                    MMHTree<TransactionLineItem> modifierTransactionLineItem = new MMHTree<>(buildFromHM(dataManager, modifierHM, printersMappedToProducts, kdsStationsMappedToProducts, kmsStationsMappedToProducts, log));
                    productTransactionLineItem.addChild(modifierTransactionLineItem);
                }
            }
        }

        return productTransactionLineItem;
    }

    /**
     * <p>Builds a {@link TransactionLineItem} instance for a product or modifier.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param lineItemHM The {@link HashMap} to use to build the {@link TransactionLineItem}.
     * @param printersMappedToProducts A {@link HashMap} whose key is the {@link Integer} ID of a product and whose value is an int array corresponding
     * to the IDs of prep printers mapped to the given products.
     * @param kdsStationsMappedToProducts A {@link HashMap} whose key is the {@link Integer} ID of a product and whose value is an int array corresponding
     * to the IDs of KDS prep stations mapped to the given products.
     * @param kmsStationsMappedToProducts A {@link HashMap} whose key is the {@link Integer} ID of a product and whose value is an int array corresponding
     * to the IDs of KMS prep station mapped to the given products.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return The {@link TransactionLineItem} instance.
     */
    @SuppressWarnings("unchecked")
    private static TransactionLineItem buildFromHM (DataManager dataManager,
                                                    HashMap lineItemHM,
                                                    HashMap<Integer, int[]> printersMappedToProducts,
                                                    HashMap<Integer, int[]> kdsStationsMappedToProducts,
                                                    HashMap<Integer, int[]> kmsStationsMappedToProducts,
                                                    String log) {
        TransactionLineItem transactionLineItem = new TransactionLineItem();

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionLineItem.buildFromHM can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyMap(lineItemHM)) {
            Logger.logMessage("The transaction line item HashMap passed to TransactionLineItem.buildFromHM can't be null or empty!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // set the transaction line item data
        transactionLineItem.paTransactionID = HashMapDataFns.getIntVal(lineItemHM, "PATRANSACTIONID");
        transactionLineItem.paTransLineItemID = HashMapDataFns.getIntVal(lineItemHM, "PATRANSLINEITEMID");
        transactionLineItem.paTransLineItemModID = HashMapDataFns.getIntVal(lineItemHM, "PATRANSLINEITEMMODID");
        transactionLineItem.paPluID = HashMapDataFns.getIntVal(lineItemHM, "PAPLUID");
        transactionLineItem.printStatusID = HashMapDataFns.getIntVal(lineItemHM, "PRINTSTATUSID");
        transactionLineItem.kmsOrderStatusID = HashMapDataFns.getIntVal(lineItemHM, "KMSORDERSTATUSID");
        if ((!DataFunctions.isEmptyMap(printersMappedToProducts)) && (printersMappedToProducts.containsKey(HashMapDataFns.getIntVal(lineItemHM, "PAPLUID")))) {
            transactionLineItem.printerIDsMappedToProduct = printersMappedToProducts.get(HashMapDataFns.getIntVal(lineItemHM, "PAPLUID"));
        }
        if ((!DataFunctions.isEmptyMap(kdsStationsMappedToProducts)) && (kdsStationsMappedToProducts.containsKey(HashMapDataFns.getIntVal(lineItemHM, "PAPLUID")))) {
            transactionLineItem.kdsStationPrinterIDsMappedToProduct = kdsStationsMappedToProducts.get(HashMapDataFns.getIntVal(lineItemHM, "PAPLUID"));
        }
        if ((!DataFunctions.isEmptyMap(kmsStationsMappedToProducts)) && (kmsStationsMappedToProducts.containsKey(HashMapDataFns.getIntVal(lineItemHM, "PAPLUID")))) {
            transactionLineItem.kmsStationIDsMappedToProduct = kmsStationsMappedToProducts.get(HashMapDataFns.getIntVal(lineItemHM, "PAPLUID"));
        }
        transactionLineItem.quantity = HashMapDataFns.getDoubleVal(lineItemHM, "QUANTITY");
        transactionLineItem.productName = HashMapDataFns.getStringVal(lineItemHM, "PRODUCTNAME");
        transactionLineItem.pluCode = HashMapDataFns.getStringVal(lineItemHM, "PLUCODE");
        transactionLineItem.orderNumber = HashMapDataFns.getStringVal(lineItemHM, "ORDERNUM");
        transactionLineItem.originalOrderNumber = HashMapDataFns.getStringVal(lineItemHM, "ORIGINALORDERNUM");
        transactionLineItem.diningOption = HashMapDataFns.getBooleanVal(lineItemHM, "ISDININGOPTION");
        transactionLineItem.printsOnExpeditor = HashMapDataFns.getBooleanVal(lineItemHM, "PRINTSONEXPEDITOR");
        if (lineItemHM.containsKey("LINKEDFROMPATRANSLINEITEMIDS")) {
            transactionLineItem.linkedFromIDs = HashMapDataFns.getStringVal(lineItemHM, "LINKEDFROMPATRANSLINEITEMIDS");
        }
        if ((transactionLineItem.linkedFromIDs == null || transactionLineItem.linkedFromIDs.trim().length() == 0) && lineItemHM.containsKey("LINKEDFROMPATRANSLINEITEMMODIDS")) {
            transactionLineItem.linkedFromIDs = HashMapDataFns.getStringVal(lineItemHM, "LINKEDFROMPATRANSLINEITEMMODIDS");
        }
        if(transactionLineItem.linkedFromIDs == null){
            transactionLineItem.linkedFromIDs = "";
            Logger.logMessage("Set the LINKED to blank" + transactionLineItem.paTransactionID + "_" + transactionLineItem.paTransLineItemID +  "_"  + transactionLineItem.linkedFromIDs, "KMS.log", Logger.LEVEL.IMPORTANT);
        }else{
            Logger.logMessage("Set the LINKED " + transactionLineItem.paTransactionID + "_" + transactionLineItem.paTransLineItemID +  "_"  + transactionLineItem.linkedFromIDs, "KMS.log", Logger.LEVEL.IMPORTANT);
        }

        // prep option data contains PrepName, DisplayDefaultReceipts, DisplayDefaultKitchen, and DefaultOption
        HashMap prepOptionDataHM = new HashMap();
        prepOptionDataHM.put("PREPNAME", HashMapDataFns.getStringVal(lineItemHM, "PREPNAME"));
        prepOptionDataHM.put("DISPLAYDEFAULTRECEIPTS", HashMapDataFns.getBooleanVal(lineItemHM, "DISPLAYDEFAULTRECEIPTS"));
        prepOptionDataHM.put("DISPLAYDEFAULTKITCHEN", HashMapDataFns.getBooleanVal(lineItemHM, "DISPLAYDEFAULTKITCHEN"));
        prepOptionDataHM.put("DEFAULTOPTION", HashMapDataFns.getBooleanVal(lineItemHM, "DEFAULTOPTION"));
        transactionLineItem.prepOptionData = prepOptionDataHM;

        return transactionLineItem;
    }

    /**
     * <p>Overridden toString() method for a {@link TransactionLineItem}.</p>
     *
     * @return A {@link String} representation of this {@link TransactionLineItem}.
     */
    @Override
    public String toString () {

        return String.format("PATRANSACTIONID: %s, PATRANSLINEITEMID: %s, PATRANSLINEITEMMODID: %s, PAPLUID: %s, PRINTSTATUSID: %s, KMSORDERSTATUSID: %s, " +
                "PRINTERIDSMAPPEDTOPRODUCT: %s, KDSSTATIONPRINTERIDSMAPPEDTOPRODUCT: %s, KMSSTATIONIDSMAPPEDTOPRODUCT: %s, QUANTITY: %s, PRODUCTNAME: %s, " +
                "PLUCODE: %s, ORDERNUMBER: %s, ORIGINALORDERNUMBER: %s, DININGOPTION: %s, PRINTSONEXPEDITOR: %s, LINKEDFROMIDS: %s, PREPOPTIONDATA: %s",
                Objects.toString((paTransactionID > 0 ? paTransactionID : "N/A"), "N/A"),
                Objects.toString((paTransLineItemID > 0 ? paTransLineItemID : "N/A"), "N/A"),
                Objects.toString((paTransLineItemModID > 0 ? paTransLineItemModID : "N/A"), "N/A"),
                Objects.toString((paPluID > 0 ? paPluID : "N/A"), "N/A"),
                Objects.toString((printStatusID > 0 ? printStatusID : "N/A"), "N/A"),
                Objects.toString((kmsOrderStatusID > 0 ? kmsOrderStatusID : "N/A"), "N/A"),
                Objects.toString((printerIDsMappedToProduct != null ? StringFunctions.buildStringFromIntArr(printerIDsMappedToProduct, ",") : "N/A"), "N/A"),
                Objects.toString((kdsStationPrinterIDsMappedToProduct != null ? StringFunctions.buildStringFromIntArr(kdsStationPrinterIDsMappedToProduct, ",") : "N/A"), "N/A"),
                Objects.toString((kmsStationIDsMappedToProduct != null ? StringFunctions.buildStringFromIntArr(kmsStationIDsMappedToProduct, ",") : "N/A"), "N/A"),
                Objects.toString((quantity > 0.0d ? quantity : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(productName) ? productName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(pluCode) ? pluCode : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(orderNumber) ? orderNumber : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(originalOrderNumber) ? originalOrderNumber : "N/A"), "N/A"),
                Objects.toString(diningOption, "N/A"),
                Objects.toString(printsOnExpeditor, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(linkedFromIDs) ? linkedFromIDs : "N/A"), "N/A"),
                Objects.toString((!DataFunctions.isEmptyMap(prepOptionData) ? Collections.singletonList(prepOptionData) : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link TransactionLineItem}.
     * Two {@link TransactionLineItem} are defined as being equal if they have the same paTransLineItemID and paTransLineItemModID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link TransactionLineItem}.
     * @return Whether or not the {@link Object} is equal to this {@link TransactionLineItem}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a TransactionLineItem and check if the obj's paTransLineItemID and paTransLineItemModID is equal to this TransactionLineItem's paTransLineItemID and paTransLineItemModID
        TransactionLineItem transactionLineItem = ((TransactionLineItem) obj);
        return ((Objects.equals(transactionLineItem.paTransLineItemID, paTransLineItemID)) && (Objects.equals(transactionLineItem.paTransLineItemModID, paTransLineItemModID)));
    }

    /**
     * <p>Overridden hashCode() method for a {@link TransactionLineItem}.</p>
     *
     * @return The unique hash code for this {@link TransactionLineItem}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(paTransLineItemID, paTransLineItemModID);
    }

    /**
     * <p>Getter for the paTransactionID field of the {@link TransactionLineItem}.</p>
     *
     * @return The paTransactionID field of the {@link TransactionLineItem}.
     */
    public int getPATransactionID () {
        return paTransactionID;
    }

    /**
     * <p>Setter for the paTransactionID field of the {@link TransactionLineItem}.</p>
     *
     * @param paTransactionID The paTransactionID field of the {@link TransactionLineItem}.
     */
    public void setPATransactionID (int paTransactionID) {
        this.paTransactionID = paTransactionID;
    }

    /**
     * <p>Getter for the paTransLineItemID field of the {@link TransactionLineItem}.</p>
     *
     * @return The paTransLineItemID field of the {@link TransactionLineItem}.
     */
    public int getPATransLineItemID () {
        return paTransLineItemID;
    }

    /**
     * <p>Setter for the paTransLineItemID field of the {@link TransactionLineItem}.</p>
     *
     * @param paTransLineItemID The paTransLineItemID field of the {@link TransactionLineItem}.
     */
    public void setPATransLineItemID (int paTransLineItemID) {
        this.paTransLineItemID = paTransLineItemID;
    }

    /**
     * <p>Getter for the paTransLineItemModID field of the {@link TransactionLineItem}.</p>
     *
     * @return The paTransLineItemModID field of the {@link TransactionLineItem}.
     */
    public int getPATransLineItemModID () {
        return paTransLineItemModID;
    }

    /**
     * <p>Setter for the paTransLineItemModID field of the {@link TransactionLineItem}.</p>
     *
     * @param paTransLineItemModID The paTransLineItemModID field of the {@link TransactionLineItem}.
     */
    public void setPATransLineItemModID (int paTransLineItemModID) {
        this.paTransLineItemModID = paTransLineItemModID;
    }

    /**
     * <p>Getter for the paPluID field of the {@link TransactionLineItem}.</p>
     *
     * @return The paPluID field of the {@link TransactionLineItem}.
     */
    public int getPAPluID () {
        return paPluID;
    }

    /**
     * <p>Setter for the paPluID field of the {@link TransactionLineItem}.</p>
     *
     * @param paPluID The paPluID field of the {@link TransactionLineItem}.
     */
    public void setPAPluID (int paPluID) {
        this.paPluID = paPluID;
    }

    /**
     * <p>Getter for the printStatusID field of the {@link TransactionLineItem}.</p>
     *
     * @return The printStatusID field of the {@link TransactionLineItem}.
     */
    public int getPrintStatusID () {
        return printStatusID;
    }

    /**
     * <p>Setter for the printStatusID field of the {@link TransactionLineItem}.</p>
     *
     * @param printStatusID The printStatusID field of the {@link TransactionLineItem}.
     */
    public void setPrintStatusID (int printStatusID) {
        this.printStatusID = printStatusID;
    }

    /**
     * <p>Getter for the kmsOrderStatusID field of the {@link TransactionLineItem}.</p>
     *
     * @return The kmsOrderStatusID field of the {@link TransactionLineItem}.
     */
    public int getKMSOrderStatusID () {
        return kmsOrderStatusID;
    }

    /**
     * <p>Setter for the kmsOrderStatusID field of the {@link TransactionLineItem}.</p>
     *
     * @param kmsOrderStatusID The kmsOrderStatusID field of the {@link TransactionLineItem}.
     */
    public void setKMSOrderStatusID (int kmsOrderStatusID) {
        this.kmsOrderStatusID = kmsOrderStatusID;
    }

    /**
     * <p>Getter for the printerIDsMappedToProduct field of the {@link TransactionLineItem}.</p>
     *
     * @return The printerIDsMappedToProduct field of the {@link TransactionLineItem}.
     */
    public int[] getPrinterIDsMappedToProduct () {
        return printerIDsMappedToProduct;
    }

    /**
     * <p>Setter for the printerIDsMappedToProduct field of the {@link TransactionLineItem}.</p>
     *
     * @param printerIDsMappedToProduct The printerIDsMappedToProduct field of the {@link TransactionLineItem}.
     */
    public void setPrinterIDsMappedToProduct (int[] printerIDsMappedToProduct) {
        this.printerIDsMappedToProduct = printerIDsMappedToProduct;
    }

    /**
     * <p>Getter for the kdsStationPrinterIDsMappedToProduct field of the {@link TransactionLineItem}.</p>
     *
     * @return The kdsStationPrinterIDsMappedToProduct field of the {@link TransactionLineItem}.
     */
    public int[] getKdsStationPrinterIDsMappedToProduct () {
        return kdsStationPrinterIDsMappedToProduct;
    }

    /**
     * <p>Setter for the kdsStationPrinterIDsMappedToProduct field of the {@link TransactionLineItem}.</p>
     *
     * @param kdsStationPrinterIDsMappedToProduct The kdsStationPrinterIDsMappedToProduct field of the {@link TransactionLineItem}.
     */
    public void setKdsStationPrinterIDsMappedToProduct (int[] kdsStationPrinterIDsMappedToProduct) {
        this.kdsStationPrinterIDsMappedToProduct = kdsStationPrinterIDsMappedToProduct;
    }

    /**
     * <p>Getter for the kmsStationIDsMappedToProduct field of the {@link TransactionLineItem}.</p>
     *
     * @return The kmsStationIDsMappedToProduct field of the {@link TransactionLineItem}.
     */
    public int[] getKmsStationIDsMappedToProduct () {
        return kmsStationIDsMappedToProduct;
    }

    /**
     * <p>Setter for the kmsStationIDsMappedToProduct field of the {@link TransactionLineItem}.</p>
     *
     * @param kmsStationIDsMappedToProduct The kmsStationIDsMappedToProduct field of the {@link TransactionLineItem}.
     */
    public void setKmsStationIDsMappedToProduct (int[] kmsStationIDsMappedToProduct) {
        this.kmsStationIDsMappedToProduct = kmsStationIDsMappedToProduct;
    }

    /**
     * <p>Getter for the quantity field of the {@link TransactionLineItem}.</p>
     *
     * @return The quantity field of the {@link TransactionLineItem}.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link TransactionLineItem}.</p>
     *
     * @param quantity The quantity field of the {@link TransactionLineItem}.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the productName field of the {@link TransactionLineItem}.</p>
     *
     * @return The productName field of the {@link TransactionLineItem}.
     */
    public String getProductName () {
        return productName;
    }

    /**
     * <p>Setter for the productName field of the {@link TransactionLineItem}.</p>
     *
     * @param productName The productName field of the {@link TransactionLineItem}.
     */
    public void setProductName (String productName) {
        this.productName = productName;
    }

    /**
     * <p>Getter for the pluCode field of the {@link TransactionLineItem}.</p>
     *
     * @return The pluCode field of the {@link TransactionLineItem}.
     */
    public String getPluCode () {
        return pluCode;
    }

    /**
     * <p>Setter for the pluCode field of the {@link TransactionLineItem}.</p>
     *
     * @param pluCode The pluCode field of the {@link TransactionLineItem}.
     */
    public void setPluCode (String pluCode) {
        this.pluCode = pluCode;
    }

    /**
     * <p>Getter for the orderNumber field of the {@link TransactionLineItem}.</p>
     *
     * @return The orderNumber field of the {@link TransactionLineItem}.
     */
    public String getOrderNumber () {
        return orderNumber;
    }

    /**
     * <p>Setter for the orderNumber field of the {@link TransactionLineItem}.</p>
     *
     * @param orderNumber The orderNumber field of the {@link TransactionLineItem}.
     */
    public void setOrderNumber (String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * <p>Getter for the originalOrderNumber field of the {@link TransactionLineItem}.</p>
     *
     * @return The originalOrderNumber field of the {@link TransactionLineItem}.
     */
    public String getOriginalOrderNumber () {
        return originalOrderNumber;
    }

    /**
     * <p>Setter for the originalOrderNumber field of the {@link TransactionLineItem}.</p>
     *
     * @param originalOrderNumber The originalOrderNumber field of the {@link TransactionLineItem}.
     */
    public void setOriginalOrderNumber (String originalOrderNumber) {
        this.originalOrderNumber = originalOrderNumber;
    }

    /**
     * <p>Getter for the diningOption field of the {@link TransactionLineItem}.</p>
     *
     * @return The diningOption field of the {@link TransactionLineItem}.
     */
    public boolean getDiningOption () {
        return diningOption;
    }

    /**
     * <p>Setter for the diningOption field of the {@link TransactionLineItem}.</p>
     *
     * @param diningOption The diningOption field of the {@link TransactionLineItem}.
     */
    public void setDiningOption (boolean diningOption) {
        Logger.logMessage("Set dining option to " + diningOption, "DiningOptionDebug.log", Logger.LEVEL.TRACE);
        this.diningOption = diningOption;
    }

    /**
     * <p>Getter for the printsOnExpeditor field of the {@link TransactionLineItem}.</p>
     *
     * @return The printsOnExpeditor field of the {@link TransactionLineItem}.
     */
    public boolean getPrintsOnExpeditor () {
        return printsOnExpeditor;
    }

    /**
     * <p>Setter for the printsOnExpeditor field of the {@link TransactionLineItem}.</p>
     *
     * @param printsOnExpeditor The printsOnExpeditor field of the {@link TransactionLineItem}.
     */
    public void setPrintsOnExpeditor (boolean printsOnExpeditor) {
        this.printsOnExpeditor = printsOnExpeditor;
    }

    /**
     * <p>Getter for the linkedFromIDs field of the {@link TransactionLineItem}.</p>
     *
     * @return The linkedFromIDs field of the {@link TransactionLineItem}.
     */
    public String getLinkedFromIDs () {
        return linkedFromIDs;
    }

    /**
     * <p>Setter for the linkedFromIDs field of the {@link TransactionLineItem}.</p>
     *
     * @param linkedFromIDs The linkedFromIDs field of the {@link TransactionLineItem}.
     */
    public void setLinkedFromIDs (String linkedFromIDs) {
        this.linkedFromIDs = linkedFromIDs;
    }

    /**
     * <p>Getter for the prepOptionData field of the {@link TransactionLineItem}.</p>
     *
     * @return The prepOptionData field of the {@link TransactionLineItem}.
     */
    public HashMap getPrepOptionData () {
        return prepOptionData;
    }

    /**
     * <p>Setter for the prepOptionData field of the {@link TransactionLineItem}.</p>
     *
     * @param prepOptionData The prepOptionData field of the {@link TransactionLineItem}.
     */
    public void setPrepOptionData (HashMap prepOptionData) {
        this.prepOptionData = prepOptionData;
    }
}