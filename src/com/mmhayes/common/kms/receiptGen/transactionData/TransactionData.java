package com.mmhayes.common.kms.receiptGen.transactionData;

/*
    Last Updated (automatically updated by SVN)
    $Author: prsmith $: Author of last commit
    $Date: 2021-05-17 11:07:47 -0400 (Mon, 17 May 2021) $: Date of last commit
    $Rev: 14000 $: Revision of last commit
    Notes: Contains information related to the transaction.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.*;

import java.util.*;

/**
 * <p>Contains information related to the transaction.</p>
 *
 */
public class TransactionData {

    // private member variables of a TransactionData
    private static final String COMMON_LOG_FILE = "ConsolidatedReceipt.log";
    private int paTransactionID = 0;
    private TransactionHeader transactionHeader = null;
    private ArrayList<MMHTree<TransactionLineItem>> transactionLineItems = null;

    /**
     * <p>Constructor for {@link TransactionData} used to create a kitchen receipt.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param paTransactionID The ID of the transaction to get the data for.
     * @return The constructed {@link TransactionData}.
     */
    public static TransactionData buildFromDB (DataManager dataManager, int paTransactionID) {
        TransactionData transactionData = new TransactionData();

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionData.buildFromDB can't be null!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        if (paTransactionID <= 0) {
            Logger.logMessage("The transaction ID passed to TransactionData.buildFromDB must be greater than 0!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        // get the transaction information from the database
        ArrayList<HashMap> transactionInfo = getTransactionInfo(dataManager, paTransactionID);
        if (!DataFunctions.isEmptyCollection(transactionInfo)) {
            // set the transaction header
            setTransactionHeader(transactionData, dataManager, transactionInfo);
            // set the PATransactionID
            if (transactionData.transactionHeader != null) {
                transactionData.paTransactionID = transactionData.transactionHeader.getPATransactionID();
            }
            // set the transaction line items
            setTransactionLineItems(transactionData, dataManager, transactionInfo);
        }
        else {
            Logger.logMessage(String.format("No information was found in the database for the transaction with an ID of %s in TransactionData.buildFromDB!",
                    Objects.toString(paTransactionID, "N/A")), COMMON_LOG_FILE, Logger.LEVEL.ERROR);
        }

        return transactionData;
    }

    /**
     * <p>Constructor for {@link TransactionData} used to create a kitchen receipt.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param paTransactionID The ID of the transaction to get the data for.
     * @return The constructed {@link TransactionData}.
     */
    public static TransactionData buildFromServerData (DataManager dataManager, int paTransactionID, ArrayList data) {
        TransactionData transactionData = new TransactionData();

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionData.buildFromDB can't be null!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        if (paTransactionID <= 0) {
            Logger.logMessage("The transaction ID passed to TransactionData.buildFromDB must be greater than 0!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        // get the transaction information from the database
        ArrayList<HashMap> transactionInfo = DataFunctions.purgeAlOfHm(data);
        if (!DataFunctions.isEmptyCollection(transactionInfo)) {
            // set the transaction header
            setTransactionHeader(transactionData, dataManager, transactionInfo);
            // set the PATransactionID
            if (transactionData.transactionHeader != null) {
                transactionData.paTransactionID = transactionData.transactionHeader.getPATransactionID();
            }
            // set the transaction line items
            setTransactionLineItems(transactionData, dataManager, transactionInfo);
        }
        else {
            Logger.logMessage(String.format("No information was found in the database for the transaction with an ID of %s in TransactionData.buildFromDB!",
                    Objects.toString(paTransactionID, "N/A")), COMMON_LOG_FILE, Logger.LEVEL.ERROR);
        }

        return transactionData;
    }

    /**
     * <p>Queries the database to obtain information about the transaction with the given ID.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param paTransactionID The ID of the transaction to get the data for.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to information about the transaction with the given ID.
     */
    private static ArrayList<HashMap> getTransactionInfo (DataManager dataManager, int paTransactionID) {
        ArrayList<HashMap> transactionData = new ArrayList<>();

        try {
            if (dataManager == null) {
                Logger.logMessage("The DataManager passed to TransactionData.getTransactionInfo can't be null!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
                return null;
            }

            if (paTransactionID <= 0) {
                Logger.logMessage("The transaction ID passed to TransactionData.getTransactionInfo must be greater than 0!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
                return null;
            }

            // query the database
            DynamicSQL sql = new DynamicSQL("data.kms.GetTransactionData").addIDList(1, paTransactionID);
            transactionData = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        }
        catch (Exception e) {
            Logger.logException(e, COMMON_LOG_FILE);
            Logger.logMessage(String.format("There was a problem trying to get information about the transaction with an ID of %s in TransactionData.getTransactionInfo!",
                    Objects.toString(paTransactionID, "N/A")), COMMON_LOG_FILE, Logger.LEVEL.ERROR);
        }

        return transactionData;
    }

    /**
     * <p>Obtains header information for the transaction.</p>
     *
     * @param transactionDataInstance The {@link TransactionData} instance the transaction header applies to.
     * @param dataManager The {@link DataManager} to be used to query the database.
     * @param transactionData An {@link ArrayList} of {@link HashMap} corresponding to the transaction data.
     */
    private static void setTransactionHeader (TransactionData transactionDataInstance, DataManager dataManager, ArrayList<HashMap> transactionData) {

        if (transactionDataInstance == null) {
            Logger.logMessage("The TransactionData instance passed to TransactionData.setTransactionHeader can't be null!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionData.setTransactionHeader can't be null!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(transactionData)) {
            Logger.logMessage("The transaction data passed to TransactionData.setTransactionHeader can't be null or empty!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return;
        }

        // set the transaction header information
        transactionDataInstance.transactionHeader = TransactionHeader.buildFromDB(dataManager, transactionData, COMMON_LOG_FILE);

        // log the transaction header
        Logger.logMessage("Transaction Header: "+(transactionDataInstance.transactionHeader != null ? "" : "N/A"), COMMON_LOG_FILE, Logger.LEVEL.TRACE);
        if (transactionDataInstance.transactionHeader != null) {
            Logger.logMessage("--"+transactionDataInstance.transactionHeader.toString(), COMMON_LOG_FILE, Logger.LEVEL.TRACE);
        }
    }

    /**
     * <p>Obtains all the product and modifier transaction line items within the transaction.</p>
     *
     * @param transactionDataInstance The {@link TransactionData} instance the product transaction line items apply to.
     * @param dataManager The {@link DataManager} to be used to query the database.
     * @param transactionData An {@link ArrayList} of {@link HashMap} corresponding to the transaction data.
     */
    @SuppressWarnings("Convert2streamapi")
    private static void setTransactionLineItems (TransactionData transactionDataInstance, DataManager dataManager, ArrayList<HashMap> transactionData) {

        if (transactionDataInstance == null) {
            Logger.logMessage("The TransactionData instance passed to TransactionData.setTransactionLineItems can't be null!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionData.setTransactionLineItems can't be null!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(transactionData)) {
            Logger.logMessage("The transaction data passed to TransactionData.setTransactionLineItems can't be null or empty!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return;
        }

        // set the transaction line item information
        ArrayList<MMHTree<TransactionLineItem>> transactionLineItems = new ArrayList<>();
        HashMap<HashMap, ArrayList<HashMap>> productAndModifiers = sortByProductAndModifiers(transactionData);

        HashSet<Integer> productIDs = getProductIDs(productAndModifiers);
        int revenueCenterID = (transactionDataInstance.transactionHeader != null ? transactionDataInstance.transactionHeader.getRevenueCenterID() : -1);
        int printerHostID = (transactionDataInstance.transactionHeader != null ? transactionDataInstance.transactionHeader.getPrinterHostID() : -1);
        HashMap<Integer, int[]> printersMappedToProducts = getPrintersMappedToProducts(dataManager, productIDs, revenueCenterID, printerHostID);
        HashMap<Integer, int[]> kdsStationsMappedToProducts = getKDSStationsMappedToProducts(dataManager, productIDs, revenueCenterID, printerHostID);
        HashMap<Integer, int[]> kmsStationsMappedToProducts = getKMSStationsMappedToProducts(dataManager, productIDs, revenueCenterID);

        if (!DataFunctions.isEmptyMap(productAndModifiers)) {
            for (Map.Entry<HashMap, ArrayList<HashMap>> productAndModifiersEntry : productAndModifiers.entrySet()) {
                transactionLineItems.add(TransactionLineItem.buildFromDB(dataManager, productAndModifiersEntry, printersMappedToProducts, kdsStationsMappedToProducts, kmsStationsMappedToProducts, COMMON_LOG_FILE));
            }
        }

        // set the transaction line items
        Collections.sort(transactionLineItems, new TransLineItemComparator());
        transactionDataInstance.transactionLineItems = transactionLineItems;

        // log the transaction line items
        Logger.logMessage("Product and Modifier Transaction Line Items: "+(!DataFunctions.isEmptyCollection(transactionDataInstance.transactionLineItems) ? "" : "N/A"), COMMON_LOG_FILE, Logger.LEVEL.TRACE);
        if (!DataFunctions.isEmptyCollection(transactionDataInstance.transactionLineItems)) {
            for (MMHTree<TransactionLineItem> transactionLineItem : transactionDataInstance.transactionLineItems) {
                transactionLineItem.logTree(transactionLineItem, "--", COMMON_LOG_FILE, Logger.LEVEL.TRACE);
            }
        }
    }

    /**
     * <p>Sorts the transaction line items so that they are sorted by the product and any modifiers.</p>
     *
     * @param transactionData An {@link ArrayList} of {@link HashMap} corresponding to the transaction line item records.
     * @return A {@link HashMap} whose key is the {@link HashMap} record for a product and whose value is an {@link ArrayList} of {@link HashMap} corresponding to modifiers mapped to the product.
     */
    @SuppressWarnings("Convert2streamapi")
    private static HashMap<HashMap, ArrayList<HashMap>> sortByProductAndModifiers (ArrayList<HashMap> transactionData) {
        HashMap<HashMap, ArrayList<HashMap>> productAndModifiers = new HashMap<>();

        if (DataFunctions.isEmptyCollection(transactionData)) {
            Logger.logMessage("The transaction data passed to TransactionData.sortByParentAndChildProducts can't be null or empty!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        // get all the products and all the modifiers
        ArrayList<HashMap> allProducts = new ArrayList<>();
        ArrayList<HashMap> allModifiers = new ArrayList<>();
        for (HashMap lineItemHM : transactionData) {
            if (HashMapDataFns.getIntVal(lineItemHM, "PATRANSLINEITEMMODID") <= 0) {
                allProducts.add(lineItemHM);
            }
            else {
                allModifiers.add(lineItemHM);
            }
        }

        // iterate through all the product line items and create an entry for each of them in the productAndModifiers HashMap
        for (HashMap productHM : allProducts) {
            productAndModifiers.put(productHM, new ArrayList<>());
        }

        // add the modifiers to the products in the productAndModifiers HashMap
        for (Map.Entry<HashMap, ArrayList<HashMap>> productAndModifiersEntry : productAndModifiers.entrySet()) {
            HashMap productHM = productAndModifiersEntry.getKey();
            ArrayList<HashMap> modifiers = productAndModifiersEntry.getValue();
            for (HashMap modifierHM : allModifiers) {
                if (HashMapDataFns.getIntVal(productHM, "PATRANSLINEITEMID").intValue() == HashMapDataFns.getIntVal(modifierHM, "PATRANSLINEITEMID").intValue()) {
                    modifiers.add(modifierHM);
                }
            }
            productAndModifiers.put(productHM, modifiers);
        }

        return productAndModifiers;
    }

    /**
     * <p>Extracts the product IDs from the products and modifiers map.</p>
     *
     * @param productAndModifiers A {@link HashMap} whose key is the {@link HashMap} record for a product and whose value
     * is an {@link ArrayList} of {@link HashMap} corresponding to modifiers mapped to the product.
     * @return A {@link HashSet} of {@link Integer} corresponding to the IDs of products within the transaction.
     */
    @SuppressWarnings("Convert2streamapi")
    private static HashSet<Integer> getProductIDs (HashMap<HashMap, ArrayList<HashMap>> productAndModifiers) {

        if (DataFunctions.isEmptyMap(productAndModifiers)) {
            Logger.logMessage("No products or modifier line items were passed to TransactionData.getProductIDs", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        HashSet<Integer> productIDs = new HashSet<>();
        for (HashMap productHM : productAndModifiers.keySet()) {
            if (!DataFunctions.isEmptyMap(productHM)) {
                productIDs.add(HashMapDataFns.getIntVal(productHM, "PAPLUID"));
            }
        }

        return productIDs;
    }

    /**
     * <p>Queries the database to determine which prep printers are mapped to the given products.</p>
     *
     * @param dataManager The {@link DataManager} to be used to query the database.
     * @param productIDs A {@link HashSet} of {@link Integer} corresponding to the IDs of products within the transaction.
     * @param revenueCenterID The ID of the revenue center in which the transaction took place.
     * @param printerHostID The ID of the printer host to use within the revenue center the transaction was made in.
     * @return A {@link HashMap} whose key is the {@link Integer} ID of a product and whose value is an int array corresponding
     * to the IDs of prep printers mapped to the given products.
     */
    @SuppressWarnings("Duplicates")
    private static HashMap<Integer, int[]> getPrintersMappedToProducts (DataManager dataManager, HashSet<Integer> productIDs, int revenueCenterID, int printerHostID) {

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionData.getPrintersMappedToProducts can't be null!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyCollection(productIDs)) {
            Logger.logMessage("The product IDs passed to TransactionData.getPrintersMappedToProducts can't be null or empty!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to TransactionData.getPrintersMappedToProducts must be greater than 0!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        if (printerHostID <= 0) {
            Logger.logMessage("The printer host ID passed to TransactionData.getPrintersMappedToProducts must be greater than 0!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetPrepPrintersMappedToProducts").addIDList(1, productIDs).addIDList(2, revenueCenterID).addIDList(3, printerHostID);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        HashMap<Integer, ArrayList<Integer>> tempProductToPrinterMappings = new HashMap<>();
        ArrayList<Integer> printerIDs;
        // build the map
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                int paPluID = HashMapDataFns.getIntVal(hm, "PAPLUID");
                int printerID = HashMapDataFns.getIntVal(hm, "PRINTERID");
                if (tempProductToPrinterMappings.containsKey(paPluID)) {
                    printerIDs = tempProductToPrinterMappings.get(paPluID);
                }
                else {
                    printerIDs = new ArrayList<>();
                }
                printerIDs.add(printerID);
                tempProductToPrinterMappings.put(paPluID, printerIDs);
            }
        }

        HashMap<Integer, int[]> productToPrinterMappings = new HashMap<>();
        if (!DataFunctions.isEmptyMap(tempProductToPrinterMappings)) {
            for (Map.Entry<Integer, ArrayList<Integer>> entry : tempProductToPrinterMappings.entrySet()) {
                productToPrinterMappings.put(entry.getKey(), DataFunctions.convertIntALToIntArr(entry.getValue()));
            }
        }

        return productToPrinterMappings;
    }

    /**
     * <p>Queries the database to determine which KDS prep stations are mapped to the given products.</p>
     *
     * @param dataManager The {@link DataManager} to be used to query the database.
     * @param productIDs A {@link HashSet} of {@link Integer} corresponding to the IDs of products within the transaction.
     * @param revenueCenterID The ID of the revenue center in which the transaction took place.
     * @param printerHostID The ID of the printer host to use within the revenue center the transaction was made in.
     * @return A {@link HashMap} whose key is the {@link Integer} ID of a product and whose value is an int array corresponding
     * to the IDs of KDS prep stations mapped to the given products.
     */
    @SuppressWarnings("Duplicates")
    private static HashMap<Integer, int[]> getKDSStationsMappedToProducts (DataManager dataManager, HashSet<Integer> productIDs, int revenueCenterID, int printerHostID) {

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionData.getKDSStationsMappedToProducts can't be null!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyCollection(productIDs)) {
            Logger.logMessage("The product IDs passed to TransactionData.getKDSStationsMappedToProducts can't be null or empty!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to TransactionData.getKDSStationsMappedToProducts must be greater than 0!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        if (printerHostID <= 0) {
            Logger.logMessage("The printer host ID passed to TransactionData.getKDSStationsMappedToProducts must be greater than 0!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetKDSPrepStationsMappedToProducts").addIDList(1, productIDs).addIDList(2, revenueCenterID).addIDList(3, printerHostID);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        HashMap<Integer, ArrayList<Integer>> tempProductToKDSStationMappings = new HashMap<>();
        ArrayList<Integer> printerIDs;
        // build the map
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                int paPluID = HashMapDataFns.getIntVal(hm, "PAPLUID");
                int printerID = HashMapDataFns.getIntVal(hm, "PRINTERID");
                if (tempProductToKDSStationMappings.containsKey(paPluID)) {
                    printerIDs = tempProductToKDSStationMappings.get(paPluID);
                }
                else {
                    printerIDs = new ArrayList<>();
                }
                printerIDs.add(printerID);
                tempProductToKDSStationMappings.put(paPluID, printerIDs);
            }
        }
        HashMap<Integer, int[]> productToKDSStationMappings = new HashMap<>();
        if (!DataFunctions.isEmptyMap(tempProductToKDSStationMappings)) {
            for (Map.Entry<Integer, ArrayList<Integer>> entry : tempProductToKDSStationMappings.entrySet()) {
                productToKDSStationMappings.put(entry.getKey(), DataFunctions.convertIntALToIntArr(entry.getValue()));
            }
        }

        return productToKDSStationMappings;
    }

    /**
     * <p>Queries the database to determine which KMS prep stations are mapped to the given products.</p>
     *
     * @param dataManager The {@link DataManager} to be used to query the database.
     * @param productIDs A {@link HashSet} of {@link Integer} corresponding to the IDs of products within the transaction.
     * @param revenueCenterID The ID of the revenue center in which the transaction took place.
     * @return A {@link HashMap} whose key is the {@link Integer} ID of a product and whose value is an int array corresponding
     * to the IDs of KMS prep stations mapped to the given products.
     */
    private static HashMap<Integer, int[]> getKMSStationsMappedToProducts (DataManager dataManager, HashSet<Integer> productIDs, int revenueCenterID) {

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionData.getKMSStationsMappedToProducts can't be null!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyCollection(productIDs)) {
            Logger.logMessage("The product IDs passed to TransactionData.getKMSStationsMappedToProducts can't be null or empty!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to TransactionData.getKMSStationsMappedToProducts must be greater than 0!", COMMON_LOG_FILE, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetKMSPrepStationsMappedToProducts").addIDList(1, productIDs).addIDList(2, revenueCenterID);
        HashMap<Integer, int[]> productToKMSStationMappings = new HashMap<>();
        HashMap<Integer, HashSet<Integer>> tempProductToKMSStationMappings = new HashMap<>();
        HashSet<Integer> kmsStationIDs;
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                int productID = HashMapDataFns.getIntVal(hm, "PAPLUID");
                int kmsStationID = HashMapDataFns.getIntVal(hm, "KMSSTATIONID");
                if (tempProductToKMSStationMappings.containsKey(productID)) {
                    kmsStationIDs = tempProductToKMSStationMappings.get(productID);
                }
                else {
                    kmsStationIDs = new HashSet<>();
                }
                kmsStationIDs.add(kmsStationID);
                tempProductToKMSStationMappings.put(productID, kmsStationIDs);
            }
        }
        if (!DataFunctions.isEmptyMap(tempProductToKMSStationMappings)) {
            for (Map.Entry<Integer, HashSet<Integer>> entry : tempProductToKMSStationMappings.entrySet()) {
                productToKMSStationMappings.put(entry.getKey(), DataFunctions.makeArrOfUniqueInt(new ArrayList<>(entry.getValue())));
            }
        }

        return productToKMSStationMappings;
    }

    /**
     * <p>Creates a shallow copy of this {@link TransactionData} instance.</p>
     * 
     * @return A shallow copy of this {@link TransactionData} instance.
     */
    @SuppressWarnings("unchecked")
    public TransactionData shallowCopy () {

        // create the shallow copy
        TransactionData copy = new TransactionData();
        copy.setPATransactionID(getPATransactionID());
        copy.setTransactionHeader(getTransactionHeader());
        copy.setTransactionLineItems((ArrayList<MMHTree<TransactionLineItem>>)getTransactionLineItems().clone());

        return copy;
    }

    /**
     * <p>Creates a deep copy of this {@link TransactionData} instance.</p>
     *
     * @return A deep copy of this {@link TransactionData} instance.
     */
    public TransactionData deepCopy () {

        // create the deep copy
        TransactionData copy = new TransactionData();
        copy.setPATransactionID(getPATransactionID());
        copy.setTransactionHeader(getTransactionHeader());
        ArrayList<MMHTree<TransactionLineItem>> lineItems = new ArrayList<>();
        if(getTransactionLineItems() != null){
            for (MMHTree<TransactionLineItem> tree : getTransactionLineItems()) {
                MMHTree<TransactionLineItem> treeCopy = tree.deepCopyRoot();
                lineItems.add(treeCopy);
            }
            copy.setTransactionLineItems(lineItems);
        }

        return copy;
    }

    /**
     * <p>Overridden equals() method for a {@link TransactionData}.
     * Two {@link TransactionData} are defined as being equal if they have the same paTransactionID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link TransactionData}.
     * @return Whether or not the {@link Object} is equal to this {@link TransactionData}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a TransactionData and check if the obj's paTransactionID is equal to this TransactionData's paTransactionID
        TransactionData transactionData = ((TransactionData) obj);
        return Objects.equals(transactionData.paTransactionID, paTransactionID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link TransactionData}.</p>
     *
     * @return The unique hash code for this {@link TransactionData}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(paTransactionID);
    }

    /**
     * <p>Getter for the paTransactionID field of the {@link TransactionData}.</p>
     *
     * @return The paTransactionID field of the {@link TransactionData}.
     */
    public int getPATransactionID () {
        return paTransactionID;
    }

    /**
     * <p>Setter for the paTransactionID field of the {@link TransactionData}.</p>
     *
     * @param paTransactionID The paTransactionID field of the {@link TransactionData}.
     */
    public void setPATransactionID (int paTransactionID) {
        this.paTransactionID = paTransactionID;
    }

    /**
     * <p>Getter for the transactionHeader field of the {@link TransactionData}.</p>
     *
     * @return The transactionHeader field of the {@link TransactionData}.
     */
    public TransactionHeader getTransactionHeader () {
        return transactionHeader;
    }

    /**
     * <p>Setter for the transactionHeader field of the {@link TransactionData}.</p>
     *
     * @param transactionHeader The transactionHeader field of the {@link TransactionData}.
     */
    public void setTransactionHeader (TransactionHeader transactionHeader) {
        this.transactionHeader = transactionHeader;
    }

    /**
     * <p>Getter for the transactionLineItems field of the {@link TransactionData}.</p>
     *
     * @return The transactionLineItems field of the {@link TransactionData}.
     */
    public ArrayList<MMHTree<TransactionLineItem>> getTransactionLineItems () {
        return transactionLineItems;
    }

    /**
     * <p>Setter for the transactionLineItems field of the {@link TransactionData}.</p>
     *
     * @param transactionLineItems The transactionLineItems field of the {@link TransactionData}.
     */
    public void setTransactionLineItems (ArrayList<MMHTree<TransactionLineItem>> transactionLineItems) {
        this.transactionLineItems = transactionLineItems;
    }

}