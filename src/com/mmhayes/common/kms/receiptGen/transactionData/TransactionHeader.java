package com.mmhayes.common.kms.receiptGen.transactionData;

/*
    Last Updated (automatically updated by SVN)
    $Author: prsmith $: Author of last commit
    $Date: 2021-03-12 10:15:55 -0500 (Fri, 12 Mar 2021) $: Date of last commit
    $Rev: 13633 $: Revision of last commit
    Notes: Represents common transaction information.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kms.receiptGen.KitchenReceiptHelper;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents common transaction information.</p>
 *
 */
public class TransactionHeader {

    // private member variables of a TransactionHeader
    private int paTransactionID = -1;
    private int transactionTypeID = -1;
    private int paOrderTypeID = -1;
    private int terminalID = -1;
    private int revenueCenterID = -1;
    private int printerHostID = -1;
    private int[] printerIDs = null;
    private int[] kmsStationIDs = null;
    private String[] previousOrderNumbersInTransaction = null;
    private LocalDateTime transactionDate = null;
    private LocalDateTime queueTime = null;
    private LocalDateTime estimatedOrderTime = null;
    private boolean onlineOrder = false;
    private boolean remoteOrder = false;
    private String orderNumber = "";
    private String cashierName = "";
    private String personName = "";
    private String address = "";
    private String phone = "";
    private String transactionNameLabel = "";
    private String transactionName = "";
    private String transactionComment = "";
    private String pickupDeliveryNote = "";
    private int startOrderNumber = -1;
    private int nextOrderNumber = -1;
    private int maxOrderNumber = -1;
    private final String INSERT_PRINT_QUEUE_HEADER = "data.newKitchenPrinter.InsertKMSOrderIntoPAPrinterQueueHeader";

    /**
     * <p>Builds a {@link TransactionHeader} for the transaction.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param transactionData An {@link ArrayList} of {@link HashMap} corresponding to the transaction data.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return The constructed {@link TransactionHeader}.
     */
    @SuppressWarnings({"OverlyComplexMethod", "TypeMayBeWeakened"})
    public static TransactionHeader buildFromDB (DataManager dataManager, ArrayList<HashMap> transactionData, String log) {
        TransactionHeader transactionHeader = new TransactionHeader();

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionHeader.buildFromDB can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyCollection(transactionData)) {
            Logger.logMessage("The transaction data passed to TransactionHeader.buildFromDB can't be null or empty!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // extract information from the transaction data passed to this method
        HashMap transactionInfoHM = transactionData.get(0);
        if ((!DataFunctions.isEmptyMap(transactionInfoHM)) && (HashMapDataFns.getIntVal(transactionInfoHM, "PATRANSACTIONID") > 0)) {
            // get terminal related information using the terminal ID
            HashMap terminalInfo = getTransactionTerminalInfo(dataManager, HashMapDataFns.getIntVal(transactionInfoHM, "TERMINALID"), log);
            // get revenue center related information using the revenue center ID
            HashMap revenueCenterInfo = getRevenueCenterInfo(dataManager, HashMapDataFns.getIntVal(terminalInfo, "REVENUECENTERID"), log);
            // get the print queue related information using the transaction ID
            HashMap printQueueInfo = getPrintQueueInfo(dataManager, HashMapDataFns.getIntVal(transactionInfoHM, "PATRANSACTIONID"), log);

            // set the transaction header information
            transactionHeader.paTransactionID = HashMapDataFns.getIntVal(transactionInfoHM, "PATRANSACTIONID");
            transactionHeader.transactionTypeID = HashMapDataFns.getIntVal(transactionInfoHM, "TRANSTYPEID");
            transactionHeader.paOrderTypeID = HashMapDataFns.getIntVal(transactionInfoHM, "PAORDERTYPEID");
            transactionHeader.terminalID = HashMapDataFns.getIntVal(transactionInfoHM, "TERMINALID");
            transactionHeader.revenueCenterID = HashMapDataFns.getIntVal(terminalInfo, "REVENUECENTERID");
            transactionHeader.printerHostID = HashMapDataFns.getIntVal(revenueCenterInfo, "PRINTERHOSTID");
            transactionHeader.printerIDs = HashMapDataFns.getIntArrVal(revenueCenterInfo, "MAPPEDPRINTERIDS");
            transactionHeader.kmsStationIDs = HashMapDataFns.getIntArrVal(revenueCenterInfo, "MAPPEDKMSSTATIONIDS");
            transactionHeader.previousOrderNumbersInTransaction = getPreviousOrderNumbersInTransaction(dataManager, HashMapDataFns.getIntVal(transactionInfoHM, "PATRANSACTIONID"), log);
            transactionHeader.transactionDate = DataFunctions.convertDateToLocalDateTime(HashMapDataFns.getDateVal(transactionInfoHM, "TRANSACTIONDATE"));
            transactionHeader.queueTime = DataFunctions.convertDateToLocalDateTime(HashMapDataFns.getDateVal(printQueueInfo, "QUEUETIME"));
            LocalDateTime txnEstOrderTime = DataFunctions.convertDateToLocalDateTime(HashMapDataFns.getDateVal(transactionInfoHM, "ESTIMATEDORDERTIME"));
            LocalDateTime pqEstOrderTime = DataFunctions.convertDateToLocalDateTime(HashMapDataFns.getDateVal(printQueueInfo, "ESTIMATEDORDERTIME"));
            transactionHeader.estimatedOrderTime = (pqEstOrderTime != null ? pqEstOrderTime : txnEstOrderTime);
            transactionHeader.onlineOrder = HashMapDataFns.getBooleanVal(transactionInfoHM, "ONLINEORDER");
            transactionHeader.remoteOrder = ((HashMapDataFns.getBooleanVal(terminalInfo, "REMOTEORDERTERMINAL")) || (HashMapDataFns.getBooleanVal(printQueueInfo, "ONLINEORDER")));
            String txnOrderNumber = HashMapDataFns.getStringVal(transactionInfoHM, "ORDERNUM");
            String pqOrderNumber = HashMapDataFns.getStringVal(printQueueInfo, "ORDERNUM");
            transactionHeader.orderNumber = (StringFunctions.stringHasContent(pqOrderNumber) ? pqOrderNumber : txnOrderNumber);
            transactionHeader.cashierName = getCashierName(dataManager, HashMapDataFns.getIntVal(transactionInfoHM, "USERID"), log);
            transactionHeader.personName = HashMapDataFns.getStringVal(transactionInfoHM, "PERSONNAME");
            transactionHeader.address = HashMapDataFns.getStringVal(transactionInfoHM, "ADDRESS");
            String txnPhone = HashMapDataFns.getStringVal(transactionInfoHM, "PHONE");
            String pqPhone = HashMapDataFns.getStringVal(printQueueInfo, "PHONE");
            transactionHeader.phone = sanitizePhoneNumber((StringFunctions.stringHasContent(pqPhone) ? pqPhone : txnPhone), log);
            transactionHeader.transactionNameLabel = HashMapDataFns.getStringVal(terminalInfo, "PATRANSNAMELABEL");
            transactionHeader.transactionName = HashMapDataFns.getStringVal(transactionInfoHM, "TRANSNAME");
            String txnComment = HashMapDataFns.getStringVal(transactionInfoHM, "TRANSCOMMENT");
            String pqComment = HashMapDataFns.getStringVal(printQueueInfo, "TRANSCOMMENT");
            transactionHeader.transactionComment = (StringFunctions.stringHasContent(pqComment) ? pqComment : txnComment);
            if (StringFunctions.stringHasContent(HashMapDataFns.getStringVal(transactionInfoHM, "PICKUPDELIVERYNOTE"))) {
                transactionHeader.pickupDeliveryNote = HashMapDataFns.getStringVal(transactionInfoHM, "PICKUPDELIVERYNOTE");
            }
            else if (shouldBuildPickupDeliveryNote(transactionHeader.paTransactionID, dataManager, log)) {
                // if the transaction is from a kiosk with Send Orders Directly to Printer Host checked then the pickup/delivery note needs to be built
                transactionHeader.pickupDeliveryNote = buildPickupDeliveryNote(transactionHeader.paTransactionID, dataManager, log);
            }
            transactionHeader.startOrderNumber = HashMapDataFns.getIntVal(revenueCenterInfo, "PASTARTORDERNUM");
            transactionHeader.nextOrderNumber = HashMapDataFns.getIntVal(revenueCenterInfo, "PANEXTORDERNUM");
            transactionHeader.maxOrderNumber = HashMapDataFns.getIntVal(revenueCenterInfo, "PAMAXORDERNUM");
        }

        return transactionHeader;
    }

    /**
     * <p>Creates an array of objects that will be inserted into the QC_PAPrinterQueue table.</p>
     *
     * @param onlineTrans Whether or not the transaction was made in online mode.
     * @return An array of {@link Object} corresponding to a row that will be inserted into the QC_PAPrinterQueue table.
     */
    @SuppressWarnings({"MagicNumber", "OverlyComplexMethod"})
    public Object[] toQCPrintQueueRecord (boolean onlineTrans) {
        Object[] printQueueRecord = new Object[16];

        printQueueRecord[0] = (paTransactionID > 0 ? String.valueOf(paTransactionID) : "N/A");
        printQueueRecord[1] = (transactionDate != null ? transactionDate.toString().replace("T", " ") : "N/A");
        printQueueRecord[2] = (terminalID > 0 ? String.valueOf(terminalID) : "N/A");
        printQueueRecord[3] = (paOrderTypeID > 0 ? String.valueOf(paOrderTypeID) : "N/A");
        printQueueRecord[4] = (StringFunctions.stringHasContent(personName) ? personName : "");
        printQueueRecord[5] = (StringFunctions.stringHasContent(phone) ? phone : "");
        printQueueRecord[6] = (StringFunctions.stringHasContent(transactionComment) ? transactionComment : "");
        printQueueRecord[7] = (StringFunctions.stringHasContent(pickupDeliveryNote) ? pickupDeliveryNote : "");
        printQueueRecord[8] = (onlineOrder ? "1" : "0");
        printQueueRecord[9] = estimatedOrderTime;
        printQueueRecord[10] = (StringFunctions.stringHasContent(orderNumber) ? orderNumber : "N/A");
        printQueueRecord[11] = (StringFunctions.stringHasContent(transactionName) ? transactionName : "");
        printQueueRecord[12] = (StringFunctions.stringHasContent(transactionNameLabel) ? transactionNameLabel : "");
        printQueueRecord[13] = (transactionTypeID > 0 ? String.valueOf(transactionTypeID) : "N/A");
        printQueueRecord[14] = (!DataFunctions.isEmptyGenericArr(previousOrderNumbersInTransaction) ? StringFunctions.buildStringFromGenericArr(previousOrderNumbersInTransaction, ",") : "");
        printQueueRecord[15] = onlineTrans;

        if (!DataFunctions.isEmptyGenericArr(printQueueRecord)) {
            return printQueueRecord;
        }
        else {
            return null;
        }
    }

    /**
     * <p>Retrieves the SQL property to insert a print queue record into the QC_PAPrinterQueue table.</p>
     *
     * @return The SQL property {@link String} for inserting a print queue header record.
     */
    public String getSQLInsertString () {
        return INSERT_PRINT_QUEUE_HEADER;
    }

    /**
     * <p>Queries the database to get information about the terminal that made the transaction.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param terminalID The ID of the terminal that made the transaction.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return A {@link HashMap} containing information about the terminal that made the transaction.
     */
    private static HashMap getTransactionTerminalInfo (DataManager dataManager, int terminalID, String log) {
        HashMap terminalInfo = new HashMap();

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionHeader.getTransactionTerminalInfo can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (terminalID <= 0) {
            Logger.logMessage("The ID of the terminal passed to TransactionHeader.getTransactionTerminalInfo must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetTransactionTerminalData").addIDList(1, terminalID);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
            terminalInfo = queryRes.get(0);
        }

        return terminalInfo;
    }

    /**
     * <p>Queries the database to get information about the revenue center that made the transaction was made in.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param revenueCenterID The ID of the revenue center that the transaction was made in.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return A {@link HashMap} containing information about the revenue center that made the transaction was made in.
     */
    private static HashMap getRevenueCenterInfo (DataManager dataManager, int revenueCenterID, String log) {
        HashMap revenueCenterInfo = new HashMap();

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionHeader.getTransactionTerminalInfo can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (revenueCenterID <= 0) {
            Logger.logMessage("The ID of the revenue center passed to TransactionHeader.getTransactionTerminalInfo must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetTransactionRevenueCenterData").addIDList(1, revenueCenterID);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
            revenueCenterInfo = queryRes.get(0);
        }

        return revenueCenterInfo;
    }

    /**
     * <p>Queries the database to find previous order numbers within the transaction (for open transactions).</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param paTransactionID The ID of the transaction.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return A {@link String} array containing previous order numbers within the transaction.
     */
    private static String[] getPreviousOrderNumbersInTransaction (DataManager dataManager, int paTransactionID, String log) {
        String[] previousOrderNumbers = null;

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionHeader.getPreviousOrderNumbersInTransaction can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (paTransactionID <= 0) {
            Logger.logMessage("The ID of the transaction passed to TransactionHeader.getPreviousOrderNumbersInTransaction must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        previousOrderNumbers = KitchenReceiptHelper.getPreviousOrderNumbersInTransaction(log, dataManager,paTransactionID);
        int s = 0; if(previousOrderNumbers != null) s = previousOrderNumbers.length;
        Logger.logMessage("getPreviousOrderNumbersInTransaction got " + previousOrderNumbers + " of size " + s + " for transaction " + paTransactionID);
        return previousOrderNumbers;
    }

    /**
     * <p>Queries the database to find transaction information in the print queue.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param paTransactionID The ID of the transaction.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return A {@link HashMap} containing transaction information in the print queue.
     */
    private static HashMap getPrintQueueInfo (DataManager dataManager, int paTransactionID, String log) {
        HashMap printQueueInfo = new HashMap();

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionHeader.getPrintQueueInfo can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (paTransactionID <= 0) {
            Logger.logMessage("The ID of the transaction passed to TransactionHeader.getPrintQueueInfo must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        DynamicSQL sql = new DynamicSQL("data.kms.GetPrinterQueueData").addIDList(1, paTransactionID);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
            printQueueInfo = queryRes.get(0);
        }

        return printQueueInfo;
    }

    /**
     * <p>Queries the database to find the name of the cashier.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param qcUserID The QuickCharge user ID of the cashier.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return A {@link String} corresponding to the cashier's name.
     */
    private static String getCashierName (DataManager dataManager, int qcUserID, String log) {
        String cashierName = "";

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionHeader.getCashierName can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (qcUserID <= 0) {
            Logger.logMessage("The QuickCharge user ID passed to TransactionHeader.getCashierName must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetCashierName").addIDList(1, qcUserID);
        Object queryRes = sql.getSingleField(dataManager);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString()))) {
            cashierName = queryRes.toString();
        }

        return cashierName;
    }

    /**
     * <p>Removes invalid characters and appends the US country code to the given phone number.</p>
     *
     * @param phoneNumber The phone number {@link String} to sanitize.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return The sanitized phone number {@link String}.
     */
    private static String sanitizePhoneNumber (String phoneNumber, String log) {

        if (!StringFunctions.stringHasContent(phoneNumber)) {
            Logger.logMessage("The phone number passed to TransactionHeader.sanitizePhoneNumber can't be null or empty!", log, Logger.LEVEL.TRACE);
            return null;
        }

        Logger.logMessage(String.format("Now attempting to sanitize the phone number %s.",
                Objects.toString(phoneNumber, "N/A")), log, Logger.LEVEL.TRACE);

        // remove any invalid characters
        phoneNumber = phoneNumber.replaceAll("[^0-9]", "");

        if (phoneNumber.length() == 10) {
            return "1" + phoneNumber;
        }
        else if (phoneNumber.length() == 11) {
            Logger.logMessage("newphonewhodis",log, Logger.LEVEL.LUDICROUS);
            return phoneNumber;
        }
        else {
            Logger.logMessage("After removing invalid characters the phone number is of an invalid length in TransactionHeader.sanitizePhoneNumber!", log, Logger.LEVEL.ERROR);
            return null;
        }
    }

    /**
     * <p>Determines whether or not to build a pickup delivery note for the receipt header.</p>
     *
     * @param paTransactionID The ID of the transaction to check if we should build a pickup/delivery note for.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return Whether or not to build a pickup/delivery note for the receipt header.
     */
    private static boolean shouldBuildPickupDeliveryNote (int paTransactionID, DataManager dataManager, String log) {

        try {
            if (paTransactionID <= 0) {
                Logger.logMessage("The transaction ID passed to TransactionHeader.shouldBuildPickupDeliveryNote must be greater than 0!", log, Logger.LEVEL.ERROR);
                return false;
            }

            if (dataManager == null) {
                Logger.logMessage("The DataManager passed to TransactionHeader.shouldBuildPickupDeliveryNote can't be null!", log, Logger.LEVEL.ERROR);
                return false;
            }

            DynamicSQL sql =
                    new DynamicSQL("data.kms.TransactionNeedsPickupDeliveryNote")
                            .addIDList(1, paTransactionID);
            return sql.getSingleBooleanField(dataManager);
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to determine whether or not to build a pickup/delivery note in TransactionHeader.shouldBuildPickupDeliveryNote!", log, Logger.LEVEL.ERROR);
        }

        return false;
    }

    /**
     * <p>Builds a pickup/delivery note for the receipt header.</p>
     *
     * @param paTransactionID The ID of the transaction to build the pickup/delivery note for.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return The pickup/delivery note {@link String} for the receipt header.
     */
    private static String buildPickupDeliveryNote (int paTransactionID, DataManager dataManager, String log) {

        if (paTransactionID <= 0) {
            Logger.logMessage("The transaction ID passed to TransactionHeader.buildPickupDeliveryNote must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to TransactionHeader.buildPickupDeliveryNote can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        HashMap pickupDeliveryNoteInfo = getPickupDeliveryNoteInfo(paTransactionID, dataManager, log);
        if (DataFunctions.isEmptyMap(pickupDeliveryNoteInfo)) {
            Logger.logMessage("Unable to build the pickup/delivery note in TransactionHeader.buildPickupDeliveryNote because " +
                    "no pickup or delivery note information was found in the database!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // build the pickup/delivery note
        int paOrderTypeID = HashMapDataFns.getIntVal(pickupDeliveryNoteInfo, "PAORDERTYPEID");
        LocalDateTime transactionDate = DataFunctions.convertDateToLocalDateTime(HashMapDataFns.getDateVal(pickupDeliveryNoteInfo, "TRANSACTIONDATE"));
        String diningOption = HashMapDataFns.getStringVal(pickupDeliveryNoteInfo, "DININGOPTION");

        String readyTime = "N/A";
        if (transactionDate != null) {
            readyTime = DateTimeFormatter.ofPattern(MMHTimeFormatString.HR_MIN_AMPM).format(transactionDate);
        }

        String pickupDeliveryNote = "";
        if (paOrderTypeID == TypeData.OrderType.DELIVERY) {
            pickupDeliveryNote += "Has to be delivered at " + readyTime+".";
        }
        else if (paOrderTypeID == TypeData.OrderType.PICKUP) {
            pickupDeliveryNote += "Will be picked up at " + readyTime+".";
        }

        // add the dining option to the pickup/delivery note if applicable
        if (StringFunctions.stringHasContent(diningOption)) {
            pickupDeliveryNote += " \\n For " + diningOption + ".";
        }

        if (StringFunctions.stringHasContent(pickupDeliveryNote)) {
            String noteType = "N/A";
            if (paOrderTypeID == TypeData.OrderType.DELIVERY) {
                noteType = "delivery";
            }
            if (paOrderTypeID == TypeData.OrderType.PICKUP) {
                noteType = "pickup";
            }
            Logger.logMessage(String.format("A %s note of, \"%s\", has been generated for the transaction with an ID of %s.",
                    Objects.toString(noteType, "N/A"),
                    Objects.toString(pickupDeliveryNote, "N/A"),
                    Objects.toString(paTransactionID, "N/A")));
        }

        return pickupDeliveryNote;
    }

    /**
     * <p>Queries the database to get information to build the pickup/delivery note for the receipt header.</p>
     *
     * @param paTransactionID The ID of the transaction to build the pickup/delivery note for.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return A {@link HashMap} containing information to build the pickup/delivery note for the receipt header.
     */
    private static HashMap getPickupDeliveryNoteInfo (int paTransactionID, DataManager dataManager, String log) {

        try {
            if (paTransactionID <= 0) {
                Logger.logMessage("The transaction ID passed to TransactionHeader.getPickupDeliveryNoteInfo must be greater than 0!", log, Logger.LEVEL.ERROR);
                return null;
            }

            if (dataManager == null) {
                Logger.logMessage("The DataManager passed to TransactionHeader.getPickupDeliveryNoteInfo can't be null!", log, Logger.LEVEL.ERROR);
                return null;
            }

            // query the database
            DynamicSQL sql =
                    new DynamicSQL("data.kms.GetPickupDeliveryNoteInfo")
                            .addIDList(1, paTransactionID);
            ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
            if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1)) {
                return queryRes.get(0);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to get the information to build a pickup/delivery note in TransactionHeader.getPickupDeliveryNoteInfo!", log, Logger.LEVEL.ERROR);
        }

        return null;
    }

    /**
     * <p>Overridden toString() method for a {@link TransactionHeader}.</p>
     *
     * @return A {@link String} representation of this {@link TransactionHeader}.
     */
    @Override
    public String toString () {

        return String.format("PATRANSACTIONID: %s, TRANSACTIONTYPEID: %s, PAORDERTYPEID: %s, TERMINALID: %s, REVENUECENTERID: %s, " +
                "PRINTERHOSTID: %s, PRINTERIDS: %s, KMSSTATIONIDS: %s, PREVIOUSORDERNUMBERSINTRANSACTION: %s, TRANSACTIONDATE: %s, " +
                "QUEUETIME: %s, ESTIMATEDORDERTIME: %s, ONLINEORDER: %s, REMOTEORDER: %s, ORDERNUMBER: %s, CASHIERNAME: %s, " +
                "PERSONNAME: %s, ADDRESS: %s, PHONE: %s, TRANSACTIONNAMELABEL: %s, TRANSACTIONNAME: %s, TRANSACTIONCOMMENT: %s, " +
                "PICKUPDELIVERYNOTE: %s, STARTORDERNUMBER: %s, NEXTORDERNUMBER: %s, MAXORDERNUMBER: %s",
                Objects.toString((paTransactionID > 0 ? paTransactionID : "N/A"), "N/A"),
                Objects.toString((transactionTypeID > 0 ? transactionTypeID : "N/A"), "N/A"),
                Objects.toString((paOrderTypeID > 0 ? paOrderTypeID : "N/A"), "N/A"),
                Objects.toString((terminalID > 0 ? terminalID : "N/A"), "N/A"),
                Objects.toString((revenueCenterID > 0 ? revenueCenterID : "N/A"), "N/A"),
                Objects.toString((printerHostID > 0 ? printerHostID : "N/A"), "N/A"),
                Objects.toString((printerIDs != null ? StringFunctions.buildStringFromIntArr(printerIDs, ",") : "N/A"), "N/A"),
                Objects.toString((kmsStationIDs != null ? StringFunctions.buildStringFromIntArr(kmsStationIDs, ",") : "N/A"), "N/A"),
                Objects.toString((previousOrderNumbersInTransaction != null ? StringFunctions.buildStringFromGenericArr(previousOrderNumbersInTransaction, ",") : "N/A"), "N/A"),
                Objects.toString((transactionDate != null ? transactionDate.toString() : "N/A"), "N/A"),
                Objects.toString((queueTime != null ? queueTime.toString() : "N/A"), "N/A"),
                Objects.toString((estimatedOrderTime != null ? estimatedOrderTime.toString() : "N/A"), "N/A"),
                Objects.toString(onlineOrder, "N/A"),
                Objects.toString(remoteOrder, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(orderNumber) ? orderNumber : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(cashierName) ? cashierName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(personName) ? personName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(address) ? address : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(phone) ? phone : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transactionNameLabel) ? transactionNameLabel : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transactionName) ? transactionName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transactionComment) ? transactionComment : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(pickupDeliveryNote) ? pickupDeliveryNote : "N/A"), "N/A"),
                Objects.toString(startOrderNumber > 0 ? startOrderNumber : "N/A", "N/A"),
                Objects.toString(nextOrderNumber > 0 ? nextOrderNumber : "N/A", "N/A"),
                Objects.toString(maxOrderNumber > 0 ? maxOrderNumber : "N/A", "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link TransactionHeader}.
     * Two {@link TransactionHeader} are defined as being equal if they have the same paTransactionID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link TransactionHeader}.
     * @return Whether or not the {@link Object} is equal to this {@link TransactionHeader}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a TransactionHeader and check if the obj's paTransactionID is equal to this TransactionHeader's paTransactionID
        TransactionHeader transactionHeader = ((TransactionHeader) obj);
        return Objects.equals(transactionHeader.paTransactionID, paTransactionID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link TransactionHeader}.</p>
     *
     * @return The unique hash code for this {@link TransactionHeader}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(paTransactionID);
    }

    /**
     * <p>Getter for the paTransactionID field of the {@link TransactionHeader}.</p>
     *
     * @return The paTransactionID field of the {@link TransactionHeader}.
     */
    public int getPATransactionID () {
        return paTransactionID;
    }

    /**
     * <p>Setter for the paTransactionID field of the {@link TransactionHeader}.</p>
     *
     * @param paTransactionID The paTransactionID field of the {@link TransactionHeader}.
     */
    public void setPATransactionID (int paTransactionID) {
        this.paTransactionID = paTransactionID;
    }

    /**
     * <p>Getter for the transactionTypeID field of the {@link TransactionHeader}.</p>
     *
     * @return The transactionTypeID field of the {@link TransactionHeader}.
     */
    public int getTransactionTypeID () {
        return transactionTypeID;
    }

    /**
     * <p>Setter for the transactionTypeID field of the {@link TransactionHeader}.</p>
     *
     * @param transactionTypeID The transactionTypeID field of the {@link TransactionHeader}.
     */
    public void setTransactionTypeID (int transactionTypeID) {
        this.transactionTypeID = transactionTypeID;
    }

    /**
     * <p>Getter for the paOrderTypeID field of the {@link TransactionHeader}.</p>
     *
     * @return The paOrderTypeID field of the {@link TransactionHeader}.
     */
    public int getPAOrderTypeID () {
        return paOrderTypeID;
    }

    /**
     * <p>Setter for the paOrderTypeID field of the {@link TransactionHeader}.</p>
     *
     * @param paOrderTypeID The paOrderTypeID field of the {@link TransactionHeader}.
     */
    public void setPAOrderTypeID (int paOrderTypeID) {
        this.paOrderTypeID = paOrderTypeID;
    }

    /**
     * <p>Getter for the terminalID field of the {@link TransactionHeader}.</p>
     *
     * @return The terminalID field of the {@link TransactionHeader}.
     */
    public int getTerminalID () {
        return terminalID;
    }

    /**
     * <p>Setter for the terminalID field of the {@link TransactionHeader}.</p>
     *
     * @param terminalID The terminalID field of the {@link TransactionHeader}.
     */
    public void setTerminalID (int terminalID) {
        this.terminalID = terminalID;
    }

    /**
     * <p>Getter for the revenueCenterID field of the {@link TransactionHeader}.</p>
     *
     * @return The revenueCenterID field of the {@link TransactionHeader}.
     */
    public int getRevenueCenterID () {
        return revenueCenterID;
    }

    /**
     * <p>Setter for the revenueCenterID field of the {@link TransactionHeader}.</p>
     *
     * @param revenueCenterID The revenueCenterID field of the {@link TransactionHeader}.
     */
    public void setRevenueCenterID (int revenueCenterID) {
        this.revenueCenterID = revenueCenterID;
    }

    /**
     * <p>Getter for the printerHostID field of the {@link TransactionHeader}.</p>
     *
     * @return The printerHostID field of the {@link TransactionHeader}.
     */
    public int getPrinterHostID () {
        return printerHostID;
    }

    /**
     * <p>Setter for the printerHostID field of the {@link TransactionHeader}.</p>
     *
     * @param printerHostID The printerHostID field of the {@link TransactionHeader}.
     */
    public void setPrinterHostID (int printerHostID) {
        this.printerHostID = printerHostID;
    }

    /**
     * <p>Getter for the printerIDs field of the {@link TransactionHeader}.</p>
     *
     * @return The printerIDs field of the {@link TransactionHeader}.
     */
    public int[] getPrinterIDs () {
        return printerIDs;
    }

    /**
     * <p>Setter for the printerIDs field of the {@link TransactionHeader}.</p>
     *
     * @param printerIDs The printerIDs field of the {@link TransactionHeader}.
     */
    public void setPrinterIDs (int[] printerIDs) {
        this.printerIDs = printerIDs;
    }

    /**
     * <p>Getter for the kmsStationIDs field of the {@link TransactionHeader}.</p>
     *
     * @return The kmsStationIDs field of the {@link TransactionHeader}.
     */
    public int[] getKMSStationIDs () {
        return kmsStationIDs;
    }

    /**
     * <p>Setter for the kmsStationIDs field of the {@link TransactionHeader}.</p>
     *
     * @param kmsStationIDs The kmsStationIDs field of the {@link TransactionHeader}.
     */
    public void setKMSStationIDs (int[] kmsStationIDs) {
        this.kmsStationIDs = kmsStationIDs;
    }

    /**
     * <p>Getter for the previousOrderNumbersInTransaction field of the {@link TransactionHeader}.</p>
     *
     * @return The previousOrderNumbersInTransaction field of the {@link TransactionHeader}.
     */
    public String[] getPreviousOrderNumbersInTransaction () {
        return previousOrderNumbersInTransaction;
    }

    /**
     * <p>Setter for the previousOrderNumbersInTransaction field of the {@link TransactionHeader}.</p>
     *
     * @param previousOrderNumbersInTransaction The previousOrderNumbersInTransaction field of the {@link TransactionHeader}.
     */
    public void setPreviousOrderNumbersInTransaction (String[] previousOrderNumbersInTransaction) {
        this.previousOrderNumbersInTransaction = previousOrderNumbersInTransaction;
    }

    /**
     * <p>Getter for the transactionDate field of the {@link TransactionHeader}.</p>
     *
     * @return The transactionDate field of the {@link TransactionHeader}.
     */
    public LocalDateTime getTransactionDate () {
        return transactionDate;
    }

    /**
     * <p>Setter for the transactionDate field of the {@link TransactionHeader}.</p>
     *
     * @param transactionDate The transactionDate field of the {@link TransactionHeader}.
     */
    public void setTransactionDate (LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * <p>Getter for the queueTime field of the {@link TransactionHeader}.</p>
     *
     * @return The queueTime field of the {@link TransactionHeader}.
     */
    public LocalDateTime getQueueTime () {
        return queueTime;
    }

    /**
     * <p>Setter for the queueTime field of the {@link TransactionHeader}.</p>
     *
     * @param queueTime The queueTime field of the {@link TransactionHeader}.
     */
    public void setQueueTime (LocalDateTime queueTime) {
        this.queueTime = queueTime;
    }

    /**
     * <p>Getter for the estimatedOrderTime field of the {@link TransactionHeader}.</p>
     *
     * @return The estimatedOrderTime field of the {@link TransactionHeader}.
     */
    public LocalDateTime getEstimatedOrderTime () {
        return estimatedOrderTime;
    }

    /**
     * <p>Setter for the estimatedOrderTime field of the {@link TransactionHeader}.</p>
     *
     * @param estimatedOrderTime The estimatedOrderTime field of the {@link TransactionHeader}.
     */
    public void setEstimatedOrderTime (LocalDateTime estimatedOrderTime) {
        this.estimatedOrderTime = estimatedOrderTime;
    }

    /**
     * <p>Getter for the onlineOrder field of the {@link TransactionHeader}.</p>
     *
     * @return The onlineOrder field of the {@link TransactionHeader}.
     */
    public boolean getOnlineOrder () {
        return onlineOrder;
    }

    /**
     * <p>Setter for the onlineOrder field of the {@link TransactionHeader}.</p>
     *
     * @param onlineOrder The onlineOrder field of the {@link TransactionHeader}.
     */
    public void setOnlineOrder (boolean onlineOrder) {
        this.onlineOrder = onlineOrder;
    }

    /**
     * <p>Getter for the remoteOrder field of the {@link TransactionHeader}.</p>
     *
     * @return The remoteOrder field of the {@link TransactionHeader}.
     */
    public boolean getRemoteOrder () {
        return remoteOrder;
    }

    /**
     * <p>Setter for the remoteOrder field of the {@link TransactionHeader}.</p>
     *
     * @param remoteOrder The remoteOrder field of the {@link TransactionHeader}.
     */
    public void setRemoteOrder (boolean remoteOrder) {
        this.remoteOrder = remoteOrder;
    }

    /**
     * <p>Getter for the orderNumber field of the {@link TransactionHeader}.</p>
     *
     * @return The orderNumber field of the {@link TransactionHeader}.
     */
    public String getOrderNumber () {
        return orderNumber;
    }

    /**
     * <p>Setter for the orderNumber field of the {@link TransactionHeader}.</p>
     *
     * @param orderNumber The orderNumber field of the {@link TransactionHeader}.
     */
    public void setOrderNumber (String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * <p>Getter for the cashierName field of the {@link TransactionHeader}.</p>
     *
     * @return The cashierName field of the {@link TransactionHeader}.
     */
    public String getCashierName () {
        return cashierName;
    }

    /**
     * <p>Setter for the cashierName field of the {@link TransactionHeader}.</p>
     *
     * @param cashierName The cashierName field of the {@link TransactionHeader}.
     */
    public void setCashierName (String cashierName) {
        this.cashierName = cashierName;
    }

    /**
     * <p>Getter for the personName field of the {@link TransactionHeader}.</p>
     *
     * @return The personName field of the {@link TransactionHeader}.
     */
    public String getPersonName () {
        return personName;
    }

    /**
     * <p>Setter for the personName field of the {@link TransactionHeader}.</p>
     *
     * @param personName The personName field of the {@link TransactionHeader}.
     */
    public void setPersonName (String personName) {
        this.personName = personName;
    }

    /**
     * <p>Getter for the address field of the {@link TransactionHeader}.</p>
     *
     * @return The address field of the {@link TransactionHeader}.
     */
    public String getAddress () {
        return address;
    }

    /**
     * <p>Setter for the address field of the {@link TransactionHeader}.</p>
     *
     * @param address The address field of the {@link TransactionHeader}.
     */
    public void setAddress (String address) {
        this.address = address;
    }

    /**
     * <p>Getter for the phone field of the {@link TransactionHeader}.</p>
     *
     * @return The phone field of the {@link TransactionHeader}.
     */
    public String getPhone () {
        return phone;
    }

    /**
     * <p>Setter for the phone field of the {@link TransactionHeader}.</p>
     *
     * @param phone The phone field of the {@link TransactionHeader}.
     */
    public void setPhone (String phone) {
        this.phone = phone;
    }

    /**
     * <p>Getter for the transactionNameLabel field of the {@link TransactionHeader}.</p>
     *
     * @return The transactionNameLabel field of the {@link TransactionHeader}.
     */
    public String getTransactionNameLabel () {
        return transactionNameLabel;
    }

    /**
     * <p>Setter for the transactionNameLabel field of the {@link TransactionHeader}.</p>
     *
     * @param transactionNameLabel The transactionNameLabel field of the {@link TransactionHeader}.
     */
    public void setTransactionNameLabel (String transactionNameLabel) {
        this.transactionNameLabel = transactionNameLabel;
    }

    /**
     * <p>Getter for the transactionName field of the {@link TransactionHeader}.</p>
     *
     * @return The transactionName field of the {@link TransactionHeader}.
     */
    public String getTransactionName () {
        return transactionName;
    }

    /**
     * <p>Setter for the transactionName field of the {@link TransactionHeader}.</p>
     *
     * @param transactionName The transactionName field of the {@link TransactionHeader}.
     */
    public void setTransactionName (String transactionName) {
        this.transactionName = transactionName;
    }

    /**
     * <p>Getter for the transactionComment field of the {@link TransactionHeader}.</p>
     *
     * @return The transactionComment field of the {@link TransactionHeader}.
     */
    public String getTransactionComment () {
        return transactionComment;
    }

    /**
     * <p>Setter for the transactionComment field of the {@link TransactionHeader}.</p>
     *
     * @param transactionComment The transactionComment field of the {@link TransactionHeader}.
     */
    public void setTransactionComment (String transactionComment) {
        this.transactionComment = transactionComment;
    }

    /**
     * <p>Getter for the pickupDeliveryNote field of the {@link TransactionHeader}.</p>
     *
     * @return The pickupDeliveryNote field of the {@link TransactionHeader}.
     */
    public String getPickupDeliveryNote () {
        return pickupDeliveryNote;
    }

    /**
     * <p>Setter for the pickupDeliveryNote field of the {@link TransactionHeader}.</p>
     *
     * @param pickupDeliveryNote The pickupDeliveryNote field of the {@link TransactionHeader}.
     */
    public void setPickupDeliveryNote (String pickupDeliveryNote) {
        this.pickupDeliveryNote = pickupDeliveryNote;
    }

    /**
     * <p>Getter for the startOrderNumber field of the {@link TransactionHeader}.</p>
     *
     * @return The startOrderNumber field of the {@link TransactionHeader}.
     */
    public int getStartOrderNumber () {
        return startOrderNumber;
    }

    /**
     * <p>Setter for the startOrderNumber field of the {@link TransactionHeader}.</p>
     *
     * @param startOrderNumber The startOrderNumber field of the {@link TransactionHeader}.
     */
    public void setStartOrderNumber (int startOrderNumber) {
        this.startOrderNumber = startOrderNumber;
    }

    /**
     * <p>Getter for the nextOrderNumber field of the {@link TransactionHeader}.</p>
     *
     * @return The nextOrderNumber field of the {@link TransactionHeader}.
     */
    public int getNextOrderNumber () {
        return nextOrderNumber;
    }

    /**
     * <p>Setter for the nextOrderNumber field of the {@link TransactionHeader}.</p>
     *
     * @param nextOrderNumber The nextOrderNumber field of the {@link TransactionHeader}.
     */
    public void setNextOrderNumber (int nextOrderNumber) {
        this.nextOrderNumber = nextOrderNumber;
    }

    /**
     * <p>Getter for the maxOrderNumber field of the {@link TransactionHeader}.</p>
     *
     * @return The maxOrderNumber field of the {@link TransactionHeader}.
     */
    public int getMaxOrderNumber () {
        return maxOrderNumber;
    }

    /**
     * <p>Setter for the maxOrderNumber field of the {@link TransactionHeader}.</p>
     *
     * @param maxOrderNumber The maxOrderNumber field of the {@link TransactionHeader}.
     */
    public void setMaxOrderNumber (int maxOrderNumber) {
        this.maxOrderNumber = maxOrderNumber;
    }

}