package com.mmhayes.common.kms.receiptGen;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-11 12:39:17 -0400 (Fri, 11 Sep 2020) $: Date of last commit
    $Rev: 12616 $: Revision of last commit
    Notes: Builds a receipt that should be displayed on KDS.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kitchenPrinters.KDS.KDSCommon;
import com.mmhayes.common.kms.receiptGen.receiptData.KitchenReceiptDetail;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionData;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionHeader;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionLineItem;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHTree;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

/**
 * Builds a receipt that should be displayed on KDS.
 *
 */
public class KDSReceipt implements KitchenReceipt {

    // private member variables of a KDSReceipt
    private DataManager dataManager = null;
    private TransactionData transactionData = null;
    private TransactionHeader transactionHeader = null;
    private ArrayList<MMHTree<TransactionLineItem>> transLineItems = null;
    private String log = null;
    private int revenueCenterID = 0;
    private int expeditorKDSID = 0;
    private int remoteOrderKDSID = 0;
    private boolean remoteOrdersUseExpeditor = false;
    private boolean isRemoteOrder = false;
    private HashMap<Integer, Integer> printerIDToKDSStationIDLookup = null;
    private HashMap<Integer, Integer> kdsStationIDToPrinterIDLookup = null;
    private HashMap<Integer, Integer> printerIDToPrinterHostIDLookup = null;

    /**
     * <p>Builds a receipt for a kitchen display system station.</p>
     *
     * @param args A variable number of {@link Object} arguments to pass to the method.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print job details for the receipt.
     */
    @Override
    public ArrayList<HashMap> buildReceipt (Object ... args) {

        // parse out the arguments
        try {
            setArgs(args);
        }
        catch (Exception e) {
            Logger.logException(e);
            return null;
        }

        Logger.logMessage("Entering KDSReceipt.buildReceipt...", log, Logger.LEVEL.IMPORTANT);

        // set the ID revenue center in which the transaction took place
        revenueCenterID = transactionData.getTransactionHeader().getRevenueCenterID();

        // set the printer ID of the expeditor KDS station if it is present
        expeditorKDSID = KitchenReceiptHelper.getKDSExpeditor(log, dataManager, revenueCenterID);
        if (expeditorKDSID > 0) {
            Logger.logMessage(String.format("There is an expeditor KDS station with a printer ID of %s within the revenue center with an ID of %s.",
                    Objects.toString(expeditorKDSID, "N/A"),
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
        }
        else {
            Logger.logMessage(String.format("There isn't an expeditor KDS station enabled within the revenue center with an ID of %s.",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
        }

        // set the printer ID of the remote order KDS station if it is present
        remoteOrderKDSID = KitchenReceiptHelper.getRemoteOrderKDS(log, dataManager, revenueCenterID);
        if (remoteOrderKDSID > 0) {
            Logger.logMessage(String.format("There is an remote order KDS station with a printer ID of %s within the revenue center with an ID of %s.",
                    Objects.toString(remoteOrderKDSID, "N/A"),
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
        }
        else {
            Logger.logMessage(String.format("There isn't a remote order KDS station enabled within the revenue center with an ID of %s.",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
        }

        // set whether or not the remote orders use expeditor setting is enabled within the revenue center
        remoteOrdersUseExpeditor = KitchenReceiptHelper.remoteOrdersUseExpeditor(log, dataManager, revenueCenterID);
        Logger.logMessage(String.format("The remote orders use expeditor setting is %s within the revenue center with an ID of %s.",
                Objects.toString((remoteOrdersUseExpeditor ? "enabled" : "disabled"), "N/A"),
                Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);

        // set the lookups between printer and KDS station ID
        setLookups();

        return buildKDSReceiptDetails();
    }

    /**
     * <p>Iterates through the arguments passed to this method and sets them as private variables for this class.</p>
     *
     * @param args A variable number of {@link Object} arguments to pass to the method.
     */
    @SuppressWarnings("Duplicates")
    private void setArgs (Object ... args) throws Exception {

        if ((args == null) || (args.length == 0)) {
            throw new Exception("No arguments received in the method KDSReceipt.setArgs!");
        }

        // find and set the arguments
        int index = 0;
        for (Object arg : args) {
            if ((arg != null) && (arg instanceof DataManager) && (index == 0)) {
                this.dataManager = ((DataManager) arg);
            }
            else if ((arg == null) && (index == 0)) {
                throw new Exception("No valid DataManager has been passed to the method KDSReceipt.setArgs!");
            }

            if ((arg != null) && (arg instanceof TransactionData) && (index == 1)) {
                TransactionData transactionInfo = ((TransactionData) arg);
                this.transactionData = transactionInfo;
                this.transactionHeader = transactionInfo.getTransactionHeader();
                this.transLineItems = transactionInfo.getTransactionLineItems();
            }
            else if ((arg == null) && (index == 1)) {
                throw new Exception("No valid transaction information has been passed to the method KDSReceipt.setArgs!");
            }

            if ((arg != null) && (arg instanceof String) && (index == 2)) {
                this.log = ((String) arg);
            }
            else if ((arg == null) && (index == 2)) {
                throw new Exception("No valid log file has been passed to the method KDSReceipt.setArgs!");
            }

            index++;
        }

    }

    /**
     * <p>Sets the lookup Maps for printer ID to KDS station ID and KDS station ID to printer ID as well as printer ID to printer host ID.</p>
     *
     */
    @SuppressWarnings("Duplicates")
    private void setLookups () {

        // query the database to get the printer ID to KDS station ID lookup Map
        DynamicSQL sql = new DynamicSQL("data.kms.GetPrinterIDToKDSStationIDLookup").setLogFileName(log);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        // build the printer ID to KDS station ID lookup Map
        HashMap<Integer, Integer> lookup = new HashMap<>();
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                int printerID = HashMapDataFns.getIntVal(hm, "PRINTERID");
                int kdsStationID = HashMapDataFns.getIntVal(hm, "STATIONID");
                lookup.put(printerID, kdsStationID);
            }
        }
        // set the lookup Maps
        if (!DataFunctions.isEmptyMap(lookup)) {
            this.printerIDToKDSStationIDLookup = lookup;
            this.kdsStationIDToPrinterIDLookup = DataFunctions.swapMapKV(lookup);
        }

        // query the database to get the printer ID to printer host ID lookup Map
        sql = new DynamicSQL("data.kms.GetPrinterIDToPrinterHostIDLookup").setLogFileName(log);
        queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        // build the printer ID to printer host ID lookup Map
        lookup = new HashMap<>();
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                int printerID = HashMapDataFns.getIntVal(hm, "PRINTERID");
                int printerHostID = HashMapDataFns.getIntVal(hm, "PRINTERHOSTID");
                lookup.put(printerID, printerHostID);
            }
        }
        // set the printer ID to printer host ID lookup Map
        if (!DataFunctions.isEmptyMap(lookup)) {
            this.printerIDToPrinterHostIDLookup = lookup;
        }

    }

    /**
     * <p>Builds the print queue details that will be sent to any KDS stations.</p>
     *
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print queue details that will be sent to any KDS stations.
     */
    private ArrayList<HashMap> buildKDSReceiptDetails () {

        // the print queue details that will be sent to KDS
        ArrayList<HashMap> kdsDetails = new ArrayList<>();

        // search for a dining option within the transaction
        TransactionLineItem diningOption = getDiningOption();
        if (diningOption != null) {
            Logger.logMessage(String.format("The dining option %s was found in KDSReceipt.buildKDSReceiptDetails while iterating " +
                    "through the transaction line items within the transaction with an ID of %s.",
                    Objects.toString(diningOption.getProductName(), "N/A"),
                    Objects.toString(transactionData.getPATransactionID(), "N/A")), log, Logger.LEVEL.TRACE);
        }
        else {
            Logger.logMessage(String.format("No dining option was found in KDSReceipt.buildKDSReceiptDetails while iterating " +
                    "through the transaction line items within the transaction with an ID of %s.",
                    Objects.toString(transactionData.getPATransactionID(), "N/A")), log, Logger.LEVEL.TRACE);
        }

        // get any details to be sent to prep KDS stations
        ArrayList<HashMap> prepKDSDetails = getDetailsForPrepKDSStations(diningOption);
        if (!DataFunctions.isEmptyCollection(prepKDSDetails)) {
            kdsDetails.addAll(prepKDSDetails);
        }

        // get any details to be sent to an expeditor KDS station
        if (expeditorKDSID > 0) {
            ArrayList<HashMap> expeditorKDSDetails = getDetailsForAnExpeditorKDSStation(diningOption);
            if (!DataFunctions.isEmptyCollection(expeditorKDSDetails)) {
                kdsDetails.addAll(expeditorKDSDetails);
            }
        }

        // get any details to be sent to a remote order KDS station
        if (remoteOrderKDSID > 0) {
            ArrayList<HashMap> remoteOrderKDSDetails = getDetailsForARemoteOrderKDSStation(diningOption);
            if (!DataFunctions.isEmptyCollection(remoteOrderKDSDetails)) {
                kdsDetails.addAll(remoteOrderKDSDetails);
            }
        }

        // create print queue details for a dining option on any KDS stations
        if (diningOption != null) {
            // get any dining option details to be sent to prep KDS stations
            ArrayList<HashMap> dinOptPrepKDSDetails = getDinOptDetailsForPrepKDS(diningOption, kdsDetails);
            if (!DataFunctions.isEmptyCollection(dinOptPrepKDSDetails)) {
                // sort the dining option details to be sent to prep KDS stations by printer ID
                ArrayList<HashMap> samePrepStnDetails;
                HashMap<Integer, ArrayList<HashMap>> dinOptPrepKDSDetailsByPtrID  = new HashMap<>();
                for (HashMap dinOptPrepKDSDetail : dinOptPrepKDSDetails) {
                    int printerID = HashMapDataFns.getIntVal(dinOptPrepKDSDetail, "PRINTERID");
                    if (dinOptPrepKDSDetailsByPtrID.containsKey(printerID)) {
                        samePrepStnDetails = dinOptPrepKDSDetailsByPtrID.get(printerID);
                    }
                    else {
                        samePrepStnDetails = new ArrayList<>();
                    }
                    samePrepStnDetails.add(dinOptPrepKDSDetail);
                    dinOptPrepKDSDetailsByPtrID.put(printerID, samePrepStnDetails);
                }
                // determine where to insert the dining option print queue details for the prep KDS stations
                if (!DataFunctions.isEmptyMap(dinOptPrepKDSDetailsByPtrID)) {
                    for (int printerID : dinOptPrepKDSDetailsByPtrID.keySet()) {
                        int dinOptInsertIndex = getDiningOptionInsertionIndex(printerID, kdsDetails);
                        if (dinOptInsertIndex >= 0) {
                            kdsDetails.addAll(dinOptInsertIndex, dinOptPrepKDSDetailsByPtrID.get(printerID));
                        }
                        else {
                            Logger.logMessage(String.format("Unable to determine the index at which to insert print queue details for the dining " +
                                    "option %s in KDSReceipt.buildKDSReceiptDetails on the prep KDS station with a printer ID of %s.",
                                    Objects.toString(diningOption.getProductName(), "N/A"),
                                    Objects.toString(printerID, "N/A")), log, Logger.LEVEL.ERROR);
                        }
                    }
                }
            }

            // get any dining option details to be sent to an expeditor KDS station
            if (expeditorKDSID > 0) {
                ArrayList<HashMap> dinOptExpeditorKDSDetails = getDinOptDetailsForExpeditorKDS(diningOption, kdsDetails);
                if (!DataFunctions.isEmptyCollection(dinOptExpeditorKDSDetails)) {
                    int dinOptInsertIndex = getDiningOptionInsertionIndex(expeditorKDSID, kdsDetails);
                    if (dinOptInsertIndex >= 0) {
                        kdsDetails.addAll(dinOptInsertIndex, dinOptExpeditorKDSDetails);
                    }
                    else {
                        // it's possible to have print queue details for only a dining option on an expeditor KDS station as
                        // long as some non dining option products will also be displayed on the expeditor KDS station
                        kdsDetails.addAll(0, dinOptExpeditorKDSDetails);
                    }
                }
            }

            // get any dining option details to be sent to a remote order KDS station
            if (remoteOrderKDSID > 0) {
                ArrayList<HashMap> dinOptRemoteOrderKDSDetails = getDinOptDetailsForRemoteOrderKDS(diningOption, kdsDetails);
                if (!DataFunctions.isEmptyCollection(dinOptRemoteOrderKDSDetails)) {
                    int dinOptInsertIndex = getDiningOptionInsertionIndex(remoteOrderKDSID, kdsDetails);
                    if (dinOptInsertIndex >= 0) {
                        kdsDetails.addAll(dinOptInsertIndex, dinOptRemoteOrderKDSDetails);
                    }
                    else {
                        Logger.logMessage(String.format("Unable to determine the index at which to insert print queue details for the dining " +
                                "option %s in KDSReceipt.buildKDSReceiptDetails on the remote order KDS station with a printer ID of %s.",
                                Objects.toString(diningOption.getProductName(), "N/A"),
                                Objects.toString(remoteOrderKDSID, "N/A")), log, Logger.LEVEL.ERROR);
                    }
                }
            }
        }

        return kdsDetails;
    }

    /**
     * <p>Iterates through the transaction line items from the transaction in search of a dining option.</p>
     *
     * @return A dining option {@link TransactionLineItem} if it is within the transaction.
     */
    private TransactionLineItem getDiningOption () {

        for (MMHTree<TransactionLineItem> productTree : transLineItems) {
            if ((productTree != null) && (productTree.getData() != null) && (productTree.getData().getDiningOption())) {
                return productTree.getData();
            }
        }

        return null;
    }

    /**
     * <p>Creates print queue details that should be sent to any prep KDS stations.</p>
     * @see <a href="https://mmhsource/svn/docs/pos/Development/Specs/KP_AND_KDS/KDS%20Flow%20Diagrams/HideOnExpeditorFlowForProduct.jpg">Flow Diagram</a>
     *
     * @param diningOption A dining option {@link TransactionLineItem} within the transaction if it is present.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print queue details that should be sent to any prep KDS stations.
     */
    private ArrayList<HashMap> getDetailsForPrepKDSStations (TransactionLineItem diningOption) {

        // the print queue details that should be sent to prep KDS stations
        ArrayList<HashMap> prepKDSDetails = new ArrayList<>();

        for (MMHTree<TransactionLineItem> productTree : transLineItems) {
            if ((productTree != null) && (productTree.getData() != null)) {
                TransactionLineItem product = productTree.getData();
                // skip the dining option if we encounter it
                if (product.equals(diningOption)) {
                    continue;
                }
                // get any modifiers the product may have
                ArrayList<TransactionLineItem> modifiers = KitchenReceiptHelper.getModifiersForProduct(log, productTree);

                // determine the prep KDS stations the product should be sent to
                int[] prepKDSStationPrinterIDs = product.getKdsStationPrinterIDsMappedToProduct();

                // create print queue details for each product and it's modifiers on each prep KDS station
                if (!DataFunctions.isEmptyIntArr(prepKDSStationPrinterIDs)) {
                    for (int prepKDSStationPrinterID : prepKDSStationPrinterIDs) {
                        if (expeditorKDSID > 0) {
                            if (product.getPrintsOnExpeditor()) {
                                ArrayList<HashMap> detailsForProduct =
                                        getPrintQueueDetailsForProduct(prepKDSStationPrinterID, printerIDToPrinterHostIDLookup.get(prepKDSStationPrinterID), false, product, modifiers);
                                if (!DataFunctions.isEmptyCollection(detailsForProduct)) {
                                    prepKDSDetails.addAll(detailsForProduct);
                                }
                            }
                            else {
                                if (isRemoteOrder) {
                                    if (remoteOrdersUseExpeditor) {
                                        ArrayList<HashMap> detailsForProduct =
                                                getPrintQueueDetailsForProduct(prepKDSStationPrinterID, printerIDToPrinterHostIDLookup.get(prepKDSStationPrinterID), false, product, modifiers);
                                        if (!DataFunctions.isEmptyCollection(detailsForProduct)) {
                                            prepKDSDetails.addAll(detailsForProduct);
                                        }
                                    }
                                    else {
                                        ArrayList<HashMap> detailsForProduct =
                                                getPrintQueueDetailsForProduct(prepKDSStationPrinterID, printerIDToPrinterHostIDLookup.get(prepKDSStationPrinterID), true, product, modifiers);
                                        if (!DataFunctions.isEmptyCollection(detailsForProduct)) {
                                            prepKDSDetails.addAll(detailsForProduct);
                                        }
                                    }
                                }
                                else {
                                    ArrayList<HashMap> detailsForProduct =
                                            getPrintQueueDetailsForProduct(prepKDSStationPrinterID, printerIDToPrinterHostIDLookup.get(prepKDSStationPrinterID), true, product, modifiers);
                                    if (!DataFunctions.isEmptyCollection(detailsForProduct)) {
                                        prepKDSDetails.addAll(detailsForProduct);
                                    }
                                }
                            }
                        }
                        else {
                            ArrayList<HashMap> detailsForProduct =
                                    getPrintQueueDetailsForProduct(prepKDSStationPrinterID, printerIDToPrinterHostIDLookup.get(prepKDSStationPrinterID), false, product, modifiers);
                            if (!DataFunctions.isEmptyCollection(detailsForProduct)) {
                                prepKDSDetails.addAll(detailsForProduct);
                            }
                        }
                    }
                }
            }
        }

        return prepKDSDetails;
    }

    /**
     * <p>Creates print queue details that should be sent to an expeditor KDS station.</p>
     * @see <a href="https://mmhsource/svn/docs/pos/Development/Specs/KP_AND_KDS/KDS%20Flow%20Diagrams/CreateKDSExpPrintJobDetailFlowForProduct.jpg">Flow Diagram</a>
     *
     * @param diningOption A dining option {@link TransactionLineItem} within the transaction if it is present.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print queue details that should be sent to an expeditor KDS station.
     */
    @SuppressWarnings("Duplicates")
    private ArrayList<HashMap> getDetailsForAnExpeditorKDSStation (TransactionLineItem diningOption) {

        // the print queue details that should be sent to an expeditor KDS station
        ArrayList<HashMap> expeditorKDSDetails = new ArrayList<>();

        for (MMHTree<TransactionLineItem> productTree : transLineItems) {
            if ((productTree != null) && (productTree.getData() != null)) {
                TransactionLineItem product = productTree.getData();
                // skip the dining option if we encounter it
                if (product.equals(diningOption)) {
                    continue;
                }
                // get any modifiers the product may have
                ArrayList<TransactionLineItem> modifiers = KitchenReceiptHelper.getModifiersForProduct(log, productTree);

                if (isRemoteOrder) {
                    if (remoteOrdersUseExpeditor) {
                        if (!productSentToPrepKDSStation(product)) {
                            ArrayList<HashMap> detailsForProduct = getPrintQueueDetailsForProduct(expeditorKDSID, printerIDToPrinterHostIDLookup.get(expeditorKDSID), false, product, modifiers);
                            if (!DataFunctions.isEmptyCollection(detailsForProduct)) {
                                expeditorKDSDetails.addAll(detailsForProduct);
                            }
                        }
                    }
                    else {
                        if (product.getPrintsOnExpeditor()) {
                            if (!productSentToPrepKDSStation(product)) {
                                ArrayList<HashMap> detailsForProduct = getPrintQueueDetailsForProduct(expeditorKDSID, printerIDToPrinterHostIDLookup.get(expeditorKDSID), false, product, modifiers);
                                if (!DataFunctions.isEmptyCollection(detailsForProduct)) {
                                    expeditorKDSDetails.addAll(detailsForProduct);
                                }
                            }
                        }
                    }
                }
                else {
                    if (product.getPrintsOnExpeditor()) {
                        if (!productSentToPrepKDSStation(product)) {
                            ArrayList<HashMap> detailsForProduct = getPrintQueueDetailsForProduct(expeditorKDSID, printerIDToPrinterHostIDLookup.get(expeditorKDSID), false, product, modifiers);
                            if (!DataFunctions.isEmptyCollection(detailsForProduct)) {
                                expeditorKDSDetails.addAll(detailsForProduct);
                            }
                        }
                    }
                }
            }
        }

        return expeditorKDSDetails;
    }

    /**
     * <p>Determines whether or not a product will be sent to a prep KDS station.</p>
     *
     * @param product The {@link TransactionLineItem} to check for being sent to a prep KDS station.
     * @return Whether or not a product will be sent to a prep KDS station.
     */
    private boolean productSentToPrepKDSStation (TransactionLineItem product) {

        // make sure the product is valid
        if (product == null) {
            Logger.logMessage("The TransactionLineItem passed to KDSReceipt.productSentToPrepKDSStation can't be null!", log, Logger.LEVEL.ERROR);
            return false;
        }

        return ((!DataFunctions.isEmptyIntArr(product.getKdsStationPrinterIDsMappedToProduct())) && (product.getKdsStationPrinterIDsMappedToProduct().length > 0));
    }

    /**
     * <p>Creates print queue details that should be sent to an remote order KDS station.</p>
     * @see <a href="https://mmhsource/svn/docs/pos/Development/Specs/KP_AND_KDS/KDS%20Flow%20Diagrams/CreateKDSRemOrdPrintJobDetailFlowForProduct.jpg">Flow Diagram</a>
     *
     * @param diningOption A dining option {@link TransactionLineItem} within the transaction if it is present.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print queue details that should be sent to an remote order KDS station.
     */
    private ArrayList<HashMap> getDetailsForARemoteOrderKDSStation (TransactionLineItem diningOption) {

        // the print queue details that should be sent to an remote order KDS station
        ArrayList<HashMap> remoteOrderKDSDetails = new ArrayList<>();

        for (MMHTree<TransactionLineItem> productTree : transLineItems) {
            if ((productTree != null) && (productTree.getData() != null)) {
                TransactionLineItem product = productTree.getData();
                // skip the dining option if we encounter it
                if (product.equals(diningOption)) {
                    continue;
                }
                // get any modifiers the product may have
                ArrayList<TransactionLineItem> modifiers = KitchenReceiptHelper.getModifiersForProduct(log, productTree);

                if (isRemoteOrder) {
                    ArrayList<HashMap> detailsForProduct = getPrintQueueDetailsForProduct(remoteOrderKDSID, printerIDToPrinterHostIDLookup.get(remoteOrderKDSID), false, product, modifiers);
                    if (!DataFunctions.isEmptyCollection(detailsForProduct)) {
                        remoteOrderKDSDetails.addAll(detailsForProduct);
                    }
                }
            }
        }

        return remoteOrderKDSDetails;
    }

    /**
     * <p>Creates the print queue details for a dining option on any prep KDS stations.</p>
     * @see <a href="https://mmhsource/svn/docs/pos/Development/Specs/KP_AND_KDS/KDS%20Flow%20Diagrams/AddDiningOptionToPrepStationFlow.jpg">Flow Diagram</a>
     *
     * @param diningOption A dining option {@link TransactionLineItem} within the transaction if it is present.
     * @param kdsDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details for non-dining option line items that will be sent to KDS.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print queue details for a dining option on any prep KDS stations.
     */
    private ArrayList<HashMap> getDinOptDetailsForPrepKDS (TransactionLineItem diningOption, ArrayList<HashMap> kdsDetails) {

        ArrayList<HashMap> dinOptPrepDetails = new ArrayList<>();
        // get the prep KDS stations that the dining option is mapped to
        int[] prepKDSStnPrinterIDs = diningOption.getKdsStationPrinterIDsMappedToProduct();
        if (!DataFunctions.isEmptyIntArr(prepKDSStnPrinterIDs)) {
            for (int prepKDSStnPrinterID : prepKDSStnPrinterIDs) {
                if (nonDiningOptionSentToStation(prepKDSStnPrinterID, kdsDetails)) {
                    if (expeditorKDSID > 0) {
                        if (diningOption.getPrintsOnExpeditor()) {
                            ArrayList<HashMap> detailsForProduct = getPrintQueueDetailsForProduct(prepKDSStnPrinterID, printerIDToPrinterHostIDLookup.get(prepKDSStnPrinterID), true, diningOption, null);
                            if (!DataFunctions.isEmptyCollection(detailsForProduct)) {
                                dinOptPrepDetails.addAll(detailsForProduct);
                            }
                        }
                        else {
                            ArrayList<HashMap> detailsForProduct = getPrintQueueDetailsForProduct(prepKDSStnPrinterID, printerIDToPrinterHostIDLookup.get(prepKDSStnPrinterID), false, diningOption, null);
                            if (!DataFunctions.isEmptyCollection(detailsForProduct)) {
                                dinOptPrepDetails.addAll(detailsForProduct);
                            }
                        }
                    }
                    else {
                        ArrayList<HashMap> detailsForProduct = getPrintQueueDetailsForProduct(prepKDSStnPrinterID, printerIDToPrinterHostIDLookup.get(prepKDSStnPrinterID), false, diningOption, null);
                        if (!DataFunctions.isEmptyCollection(detailsForProduct)) {
                            dinOptPrepDetails.addAll(detailsForProduct);
                        }
                    }
                }
            }
        }

        return dinOptPrepDetails;
    }

    /**
     * <p>Creates the print queue details for a dining option on an expeditor KDS station.</p>
     * @see <a href="https://mmhsource/svn/docs/pos/Development/Specs/KP_AND_KDS/KDS%20Flow%20Diagrams/AddDiningOptionToExpeditorStationFlow.jpg">Flow Diagram</a>
     *
     * @param diningOption A dining option {@link TransactionLineItem} within the transaction if it is present.
     * @param kdsDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details for non-dining option line items that will be sent to KDS.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print queue details for a dining option on an expeditor KDS station.
     */
    private ArrayList<HashMap> getDinOptDetailsForExpeditorKDS (TransactionLineItem diningOption, ArrayList<HashMap> kdsDetails) {

        ArrayList<HashMap> dinOptExpeditorDetails = new ArrayList<>();
        if (isRemoteOrder) {
            if (remoteOrdersUseExpeditor) {
                if (nonDiningOptionSentToStation(expeditorKDSID, kdsDetails)) {
                    dinOptExpeditorDetails = getPrintQueueDetailsForProduct(expeditorKDSID, printerIDToPrinterHostIDLookup.get(expeditorKDSID), false, diningOption, null);
                }
            }
            else {
                if (diningOption.getPrintsOnExpeditor()) {
                    if (nonDiningOptionSentToStation(expeditorKDSID, kdsDetails)) {
                        dinOptExpeditorDetails = getPrintQueueDetailsForProduct(expeditorKDSID, printerIDToPrinterHostIDLookup.get(expeditorKDSID), false, diningOption, null);
                    }
                }
            }
        }
        else {
            if (diningOption.getPrintsOnExpeditor()) {
                if (nonDiningOptionSentToStation(expeditorKDSID, kdsDetails)) {
                    dinOptExpeditorDetails = getPrintQueueDetailsForProduct(expeditorKDSID, printerIDToPrinterHostIDLookup.get(expeditorKDSID), false, diningOption, null);
                }
            }
        }

        return dinOptExpeditorDetails;
    }

    /**
     * <p>Creates the print queue details for a dining option on a remote order KDS station.</p>
     * @see <a href="https://mmhsource/svn/docs/pos/Development/Specs/KP_AND_KDS/KDS%20Flow%20Diagrams/AddDiningOptionToRemoteOrderStationFlow.jpg">Flow Diagram</a>
     *
     * @param diningOption A dining option {@link TransactionLineItem} within the transaction if it is present.
     * @param kdsDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details for non-dining option line items that will be sent to KDS.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print queue details for a dining option on a remote order KDS station.
     */
    private ArrayList<HashMap> getDinOptDetailsForRemoteOrderKDS (TransactionLineItem diningOption, ArrayList<HashMap> kdsDetails) {

        ArrayList<HashMap> dinOptRemoteOrderDetails = new ArrayList<>();
        if ((isRemoteOrder) && (nonDiningOptionSentToStation(remoteOrderKDSID, kdsDetails))) {
            dinOptRemoteOrderDetails = getPrintQueueDetailsForProduct(remoteOrderKDSID, printerIDToPrinterHostIDLookup.get(remoteOrderKDSID), false, diningOption, null);
        }

        return dinOptRemoteOrderDetails;
    }

    /**
     * <p>Checks whether or not a product that isn't a dining option has been sent to the KDS station.</p>
     *
     * @param printerID The printer ID of the KDS station to check for a non dining option.
     * @param kdsDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details for non-dining option line items that will be sent to KDS.
     * @return Whether or not a product that isn't a dining option has been sent to the KDS station.
     */
    private boolean nonDiningOptionSentToStation (int printerID, ArrayList<HashMap> kdsDetails) {

        // make sure the printer ID is valid
        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KDSReceipt.nonDiningOptionSentToStation must be greater than 0!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure there are existing KDS print queue details
        if (DataFunctions.isEmptyCollection(kdsDetails)) {
            Logger.logMessage("The print queue details for KDS passed to KDSReceipt.nonDiningOptionSentToStation can't be null or empty!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // check if a non dining option product was explicitly sent to the KDS station.
        for (HashMap hm : kdsDetails) {
            if (!DataFunctions.isEmptyMap(hm)) {
                int detailPrinterID = HashMapDataFns.getIntVal(hm, "PRINTERID");
                if (detailPrinterID == printerID) {
                    return true;
                }
            }
        }

        // check if a non dining option product was implicitly sent to an expeditor KDS station, in other words, a print queue detail doesn't
        // exist for the product but the expeditor station still gets the product
        if (printerID == expeditorKDSID) {
            for (HashMap hm : kdsDetails) {
                if ((!DataFunctions.isEmptyMap(hm)) && (!StringFunctions.stringHasContent(HashMapDataFns.getStringVal(hm, "HIDDENSTATION")))) {
                    // if the product wasn't hidden for the expeditor KDS station then it will show on the expeditor KDS station
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * <p>Determines the index within the print queue details for KDS at which to insert the print queue detail for the dining option.</p>
     *
     * @param printerID The printer ID of the KDS station to check for.
     * @param kdsDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details that will be sent to KDS.
     * @return The index within the print queue details for KDS at which to insert the print queue detail for the dining option.
     */
    private int getDiningOptionInsertionIndex (int printerID, ArrayList<HashMap> kdsDetails) {

        // make sure the printer ID is valid
        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KDSReceipt.getDiningOptionInsertionIndex must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // make sure there are existing KDS print queue details
        if (DataFunctions.isEmptyCollection(kdsDetails)) {
            Logger.logMessage("The print queue details for KDS passed to KDSReceipt.getDiningOptionInsertionIndex can't be null or empty!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        for (int index = 0; index < kdsDetails.size(); index++) {
            if (!DataFunctions.isEmptyMap(kdsDetails.get(index))) {
                int detailPrinterID = HashMapDataFns.getIntVal(kdsDetails.get(index), "PRINTERID");
                if (detailPrinterID == printerID) {
                    return index;
                }
            }
        }

        return -1;
    }

    /**
     * <p>Creates print queue details for the product and it's modifiers.</p>
     *
     * @param printerID The printer ID of the KDS station that the print queue details will be sent to.
     * @param printerHostID The ID of the printer host that will send the print queue details to the KDS station.
     * @param hideOnExpeditor Whether or not the product should be hidden on an expeditor KDS station.
     * @param product The product {@link TransactionLineItem} to create print queue details for.
     * @param modifiers An {@link ArrayList} of {@link TransactionLineItem} corresponding to the modifiers for the product.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print queue details for the product and it's modifiers.
     */
    private ArrayList<HashMap> getPrintQueueDetailsForProduct (int printerID, int printerHostID, boolean hideOnExpeditor, TransactionLineItem product, ArrayList<TransactionLineItem> modifiers) {

        // make sure the printer ID is valid
        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to KDSReceipt.getPrintQueueDetailsForProduct must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the printer host ID is valid
        if (printerHostID <= 0) {
            Logger.logMessage("The printer host ID passed to KDSReceipt.getPrintQueueDetailsForProduct must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the product is valid
        if (product == null) {
            Logger.logMessage("The product TransactionLineItem passed to KDSReceipt.getPrintQueueDetailsForProduct can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<HashMap> printQueueDetailsForProduct = new ArrayList<>();

        // create a print queue detail for the product
        KitchenReceiptDetail kitchenReceiptDetail =
                new KitchenReceiptDetail()
                .addPrinterID(printerID)
                .addPrintStatusID(PrintStatusType.WAITING)
                .addKMSOrderStatusID(-1)
                .addPrinterHostID(printerHostID)
                .addLineDetail(KitchenReceiptHelper.buildLineItemText(log, product, false, true, false))
                .addProductID(product.getPAPluID())
                .addQuantity(product.getQuantity())
                .addIsModifier(false)
                .addTransLineItemID(product.getPATransLineItemID())
                .addLinkedFromIDs(product.getLinkedFromIDs())
                .addHiddenStation((hideOnExpeditor ? String.valueOf(printerIDToKDSStationIDLookup.get(expeditorKDSID)) : ""));
        printQueueDetailsForProduct.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));

        // create print queue details for each of the products modifiers
        if (!DataFunctions.isEmptyCollection(modifiers)) {
            for (TransactionLineItem modifier : modifiers) {
                if (modifier != null) {
                    kitchenReceiptDetail =
                            new KitchenReceiptDetail()
                            .addPrinterID(printerID)
                            .addPrintStatusID(PrintStatusType.WAITING)
                            .addKMSOrderStatusID(-1)
                            .addPrinterHostID(printerHostID)
                            .addLineDetail(KitchenReceiptHelper.buildLineItemText(log, modifier, false, true, false))
                            .addProductID(modifier.getPAPluID())
                            .addQuantity(modifier.getQuantity())
                            .addIsModifier(true)
                            .addTransLineItemID(modifier.getPATransLineItemID())
                            .addLinkedFromIDs(modifier.getLinkedFromIDs())
                            .addHiddenStation((hideOnExpeditor ? String.valueOf(printerIDToKDSStationIDLookup.get(expeditorKDSID)) : ""));
                    printQueueDetailsForProduct.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
                }
            }
        }

        return printQueueDetailsForProduct;
    }

}