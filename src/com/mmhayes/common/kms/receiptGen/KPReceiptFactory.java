package com.mmhayes.common.kms.receiptGen;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-06-19 11:24:27 -0400 (Fri, 19 Jun 2020) $: Date of last commit
    $Rev: 12033 $: Revision of last commit
    Notes: Creates a receipt to print on KP.
*/

/**
 * <p>Creates a receipt to print on KP.</p>
 *
 */
public class KPReceiptFactory extends AbstractKitchenReceiptFactory {

    /**
     * <p>Gets a kitchen printer instance of a {@link KitchenReceipt}.</p>
     *
     * @return A {@link KitchenReceipt} instance for a kitchen printer.
     */
    @Override
    public KitchenReceipt getReceipt () {
        return new KPReceipt();
    }

}