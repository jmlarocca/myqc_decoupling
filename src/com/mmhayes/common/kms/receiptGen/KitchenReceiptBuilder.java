package com.mmhayes.common.kms.receiptGen;

/*
    Last Updated (automatically updated by SVN)
    $Author: crking $: Author of last commit
    $Date: 2020-12-11 17:36:52 -0500 (Fri, 11 Dec 2020) $: Date of last commit
    $Rev: 13287 $: Revision of last commit
    Notes: Determines what types of receipts should be built and builds them.
*/

import com.google.gson.Gson;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kitchenPrinters.KitchenPrinterCommon;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionData;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * <p>Determines what types of receipts should be built and builds them.</p>
 *
 */
public class KitchenReceiptBuilder {

    // private member variables of a KitchenReceiptBuilder
    private DataManager dataManager = null;
    private IRenderer receiptRenderer = null;
    private TransactionData transactionData = null;
    private String log = null;

    /**
     * <p>Builds and returns the details of kitchen receipts.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param renderer An {@link IRenderer} instance to render a kitchen receipt.
     * @param transactionData The {@link TransactionData} information for the transaction which the kitchen receipts apply to.
     * @param log The {@link String} path of the file to log any errors or information to for this method.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the details for receipts.
     */
    public static ArrayList<HashMap> buildReceiptDetails (DataManager dataManager,
                                                          IRenderer renderer,
                                                          TransactionData transactionData,
                                                          String log) {
        ArrayList<HashMap> receiptDetails = new ArrayList<>();

        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to KitchenReceiptBuilder.buildReceiptDetails can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (renderer == null) {
            Logger.logMessage("The IRenderer instance passed to KitchenReceiptBuilder.buildReceiptDetails can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (transactionData == null) {
            Logger.logMessage("No transaction data has been passed to KitchenReceiptBuilder.buildReceiptDetails, unable to build any kitchen receipts!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // get any kitchen printer receipt details
        AbstractKitchenReceiptFactory kpReceiptFactory = KitchenReceiptFactoryProducer.getFactory(KitchenReceiptType.KP, log);
        KitchenReceipt kpReceipt = null;
        if (kpReceiptFactory != null) {
            kpReceipt = kpReceiptFactory.getReceipt();
        }
        ArrayList<HashMap> kpReceiptDetails = new ArrayList<>();
        if (kpReceipt != null) {
            kpReceiptDetails = kpReceipt.buildReceipt(dataManager, renderer, transactionData.deepCopy(), log);
        }
        if (!DataFunctions.isEmptyCollection(kpReceiptDetails)) {
            Logger.logMessage("*** START KP RECEIPT DETAILS ***", log, Logger.LEVEL.TRACE);
            for (HashMap kpDetail : kpReceiptDetails) {
                Logger.logMessage(Collections.singletonList(kpDetail).toString(), log, Logger.LEVEL.TRACE);
            }
            Logger.logMessage("*** END KP RECEIPT DETAILS ***", log, Logger.LEVEL.TRACE);
            receiptDetails.addAll(kpReceiptDetails);
        }

        // get any kitchen display system receipt details
        AbstractKitchenReceiptFactory kdsReceiptFactory = KitchenReceiptFactoryProducer.getFactory(KitchenReceiptType.KDS, log);
        KitchenReceipt kdsReceipt = null;
        if (kdsReceiptFactory != null) {
            kdsReceipt = kdsReceiptFactory.getReceipt();
        }
        ArrayList<HashMap> kdsReceiptDetails = new ArrayList<>();
        if (kdsReceipt != null) {
            kdsReceiptDetails = kdsReceipt.buildReceipt(dataManager, transactionData.deepCopy(), log);
        }
        if (!DataFunctions.isEmptyCollection(kdsReceiptDetails)) {
            Logger.logMessage("*** START KDS RECEIPT DETAILS ***", log, Logger.LEVEL.TRACE);
            for (HashMap kdsDetail : kdsReceiptDetails) {
                Logger.logMessage(Collections.singletonList(kdsDetail).toString(), log, Logger.LEVEL.TRACE);
            }
            Logger.logMessage("*** END KDS RECEIPT DETAILS ***", log, Logger.LEVEL.TRACE);
            receiptDetails.addAll(kdsReceiptDetails);
        }

        // get any kitchen management system receipt details
        AbstractKitchenReceiptFactory kmsReceiptFactory = KitchenReceiptFactoryProducer.getFactory(KitchenReceiptType.KMS, log);
        KitchenReceipt kmsReceipt = null;
        if (kmsReceiptFactory != null) {
            kmsReceipt = kmsReceiptFactory.getReceipt();
        }
        ArrayList<HashMap> kmsReceiptDetails = new ArrayList<>();
        if (kmsReceipt != null) {
            Logger.logMessage("Calling KMSReceipt.buildReceipt", "KMS.log", Logger.LEVEL.IMPORTANT);
            kmsReceiptDetails = kmsReceipt.buildReceipt(dataManager, transactionData.deepCopy(), "KMS.log");
        }
        if (!DataFunctions.isEmptyCollection(kmsReceiptDetails)) {
            Logger.logMessage("*** START KMS RECEIPT DETAILS ***", log, Logger.LEVEL.TRACE);
            for (HashMap kmsDetail : kmsReceiptDetails) {
                String decodedLineDetail = KitchenPrinterCommon.decode(kmsDetail.getOrDefault("LINEDETAIL", "").toString(), log);
                Logger.logMessage(String.format("%-60s", decodedLineDetail) + Collections.singletonList(kmsDetail).toString(), log, Logger.LEVEL.TRACE);
            }
            Logger.logMessage("*** END KMS RECEIPT DETAILS ***", log, Logger.LEVEL.TRACE);
            receiptDetails.addAll(kmsReceiptDetails);
        }

        Logger.logMessage("Kitchen ReceiptBuilder: Built Receipt Details: " + new Gson().toJson(receiptDetails.toString()), "KMS.log", Logger.LEVEL.DEBUG);
        return receiptDetails;
    }

}