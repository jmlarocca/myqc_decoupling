package com.mmhayes.common.kms.receiptGen;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-06-19 11:24:27 -0400 (Fri, 19 Jun 2020) $: Date of last commit
    $Rev: 12033 $: Revision of last commit
    Notes: Creates a receipt to be displayed on KDS.
*/

/**
 * <p>Creates a receipt to be displayed on KDS.</p>
 *
 */
public class KDSReceiptFactory extends AbstractKitchenReceiptFactory {

    /**
     * <p>Gets a kitchen display system instance of a {@link KitchenReceipt}.</p>
     *
     * @return A {@link KitchenReceipt} instance for a kitchen display system.
     */
    @Override
    public KitchenReceipt getReceipt () {
        return new KDSReceipt();
    }

}