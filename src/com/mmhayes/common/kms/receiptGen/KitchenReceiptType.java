package com.mmhayes.common.kms.receiptGen;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-06-19 10:43:14 -0400 (Fri, 19 Jun 2020) $: Date of last commit
    $Rev: 12031 $: Revision of last commit
    Notes: The different types of kitchen receipts that may be built.
*/

/**
 * <p>The different types of kitchen receipts that may be built.</p>
 *
 */
public class KitchenReceiptType {

    public static final String KP = "KITCHEN_PRINTER";
    public static final String KDS = "KITCHEN_DISPLAY_SYSTEM";
    public static final String KMS = "KITCHEN_MANAGEMENT_SYSTEM";

}