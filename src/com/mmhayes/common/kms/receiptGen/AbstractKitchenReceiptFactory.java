package com.mmhayes.common.kms.receiptGen;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-06-19 11:24:27 -0400 (Fri, 19 Jun 2020) $: Date of last commit
    $Rev: 12033 $: Revision of last commit
    Notes: An abstract kitchen receipt factory.
*/

/**
 * <p>An abstract kitchen receipt factory.</p>
 *
 */
public abstract class AbstractKitchenReceiptFactory {

    /**
     * <p>Gets a {@link KitchenReceipt}.</p>
     *
     * @return A {@link KitchenReceipt} instance.
     */
    abstract KitchenReceipt getReceipt ();

}