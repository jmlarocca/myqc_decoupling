package com.mmhayes.common.kms.receiptGen;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-22 12:26:54 -0400 (Thu, 22 Apr 2021) $: Date of last commit
    $Rev: 13834 $: Revision of last commit
    Notes: Builds a receipt that should be displayed on KMS.
*/

import com.google.gson.Gson;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kms.receiptGen.receiptData.KitchenReceiptDetail;
import com.mmhayes.common.kms.receiptGen.receiptData.KitchenReceiptHeader;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionData;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionHeader;
import com.mmhayes.common.kms.receiptGen.transactionData.TransactionLineItem;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.Commands.JPOS_COMMAND;
import com.mmhayes.common.receiptGen.Formatter.KitchenPrinterReceiptFormatter;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHTree;
import com.mmhayes.common.utils.StringFunctions;

import java.util.*;
import java.util.stream.IntStream;

/**
 * <p>Builds a receipt that should be displayed on KMS.</p>
 *
 */
//TODO: KitchenReceipt could be a class and remove some duplicated code without preventing functionality....
public class KMSReceipt implements KitchenReceipt {

    // private member variables of a KMSReceipt
    private DataManager dataManager = null;
    private TransactionData transactionData = null;
    private String log = "KMS.log";
    private int revenueCenterID = 0;
    private int expeditorStationID = 0;
    private int remoteOrderStationID = 0;
    private boolean remoteOrdersUseExpeditor = false;
    private boolean noExpeditorReceiptUnlessProductsAdded = false;
    private int maxCharsPerReceiptLineNormal = 32;
    private int maxCharsPerReceiptLineLarge = 16;

    /**
     * <p>Builds a receipt for a kitchen management system station.</p>
     *
     * @param args A variable number of {@link Object} arguments to pass to the method.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print job details for the receipt.
     */
    @Override
    public ArrayList<HashMap> buildReceipt (Object ... args) {
        ArrayList<HashMap> kmsReceiptDetails = new ArrayList<>();
        // parse out the arguments
        try {
            setArgs(args);
        }
        catch (Exception e) {
            Logger.logException(e);
            return null;
        }

        Logger.logMessage("Entering KMSReceipt.buildReceipt...", log, Logger.LEVEL.IMPORTANT);

        //TODO: transLines need the FromPATransLineIDs for open transactions...
        // determine which transaction line items should be sent to which kitchen printers
        Logger.logMessage("KMSReceipt.buildReceipt", "KMS.log", Logger.LEVEL.TRACE);
        HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForStations = determineStationsForTransactionLineItems();

        if (!DataFunctions.isEmptyMap(transLineItemsForStations)) {
            kmsReceiptDetails = buildKMSReceiptDetails(transLineItemsForStations);
        }
        else {
            Logger.logMessage("No product or modifier transaction line items were found to be mapped to any kitchen printers in KMSReceipt.buildReceipt!", log, Logger.LEVEL.TRACE);
        }

        return kmsReceiptDetails;
    }

    /**
     * <p>Iterates through the arguments passed to this method and sets them as private variables for this class.</p>
     *
     * @param args A variable number of {@link Object} arguments to pass to the method.
     */
    @SuppressWarnings("Duplicates")
    private void setArgs (Object ... args) throws Exception {

        if ((args == null) || (args.length == 0)) {
            throw new Exception("No arguments received in the method KMSReceipt.setArgs!");
        }

        // find and set the arguments
        int index = 0;
        for (Object arg : args) {
            if ((arg != null) && (arg instanceof DataManager) && (index == 0)) {
                this.dataManager = ((DataManager) arg);
            }
            else if ((arg == null) && (index == 0)) {
                throw new Exception("No valid DataManager has been passed to the method KMSReceipt.setArgs!");
            }

            if ((arg != null) && (arg instanceof TransactionData) && (index == 1)) {
                this.transactionData = ((TransactionData) arg);
            }
            else if ((arg == null) && (index == 1)) {
                throw new Exception("No valid transaction information has been passed to the method KMSReceipt.setArgs!");
            }

            if ((arg != null) && (arg instanceof String) && (index == 2)) {
                this.log = ((String) arg);
            }
            else if ((arg == null) && (index == 2)) {
                throw new Exception("No valid log file has been passed to the method KMSReceipt.setArgs!");
            }

            index++;
        }

    }

    //region transaction data sorting
    /**
     * <p>Sorts the transaction line items that should be sent to a given station based on whether or not they have been printed and then based on their order number.</p>
     *
     * @param lineItemsForPrinter An {@link ArrayList} of {@link TransactionLineItem} corresponding to the products and modifiers that should be sent to a given printer.
     * @return A {@link HashMap} whose key is a {@link String} of whether or not the products have been printed and whose value is a {@link HashMap} whose
     * key is the order number {@link String} and whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to transaction line items
     * with the same order number that have/haven't been printed.
     */
    private HashMap<String, HashMap<String, ArrayList<TransactionLineItem>>> sortLineItemsForKMSReceipt (ArrayList<TransactionLineItem> lineItemsForPrinter) {

        if (DataFunctions.isEmptyCollection(lineItemsForPrinter)) {
            Logger.logMessage("The transaction line items passed to KMSReceipt.sortLineItemsForKMSReceipt can't be null or empty!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // sort line items based on whether or not they have been printed
        ArrayList<TransactionLineItem> printedLineItems = new ArrayList<>();
        ArrayList<TransactionLineItem> unprintedLineItems = new ArrayList<>();
        // keep track of whether products are printed/unprinted, the modifiers will follow their parent product
        HashMap<Integer, Boolean> productPrintedStatus = new HashMap<>();
        for (TransactionLineItem lineItem : lineItemsForPrinter) {
            if (lineItem.getPATransLineItemModID() <= 0) {
                // the product has been printed
                printedLineItems.add(lineItem);
                productPrintedStatus.put(lineItem.getPATransLineItemID(), true);
            }
            else if ((lineItem.getPATransLineItemModID() > 0) && (productPrintedStatus.get(lineItem.getPATransLineItemID()))) {
                // the modifiers parent product has been printed, that means the modifier has too
                printedLineItems.add(lineItem);
            }
            else {
                if (lineItem.getPATransLineItemModID() <= 0) {
                    // make sure we indicate that the product hasn't been printed yet
                    productPrintedStatus.put(lineItem.getPATransLineItemID(), false);
                }
                // the product or modifier line item hasn't been printed yet
                unprintedLineItems.add(lineItem);
            }
        }

        HashMap<String, HashMap<String, ArrayList<TransactionLineItem>>> lineItemsSortedBySimplePrintStatus = new HashMap<>();

        // further sort the printed and unprinted line items based on their order number
        if (!DataFunctions.isEmptyCollection(printedLineItems)) {
            HashMap<String, ArrayList<TransactionLineItem>> lineItemsSortedByOrderNumber = sortLineItemsByOrderNumber(printedLineItems);
            if (!DataFunctions.isEmptyMap(lineItemsSortedByOrderNumber)) {
                lineItemsSortedBySimplePrintStatus.put(TypeData.SimplePrintStatus.PRINTED, lineItemsSortedByOrderNumber);
            }
        }
        if (!DataFunctions.isEmptyCollection(unprintedLineItems)) {
            HashMap<String, ArrayList<TransactionLineItem>> lineItemsSortedByOrderNumber = sortLineItemsByOrderNumber(unprintedLineItems);
            if (!DataFunctions.isEmptyMap(lineItemsSortedByOrderNumber)) {
                lineItemsSortedBySimplePrintStatus.put(TypeData.SimplePrintStatus.UNPRINTED, lineItemsSortedByOrderNumber);
            }
        }

        return lineItemsSortedBySimplePrintStatus;
    }

    /**
     * <p>Iterates through the given transaction line items and sorts them based on their order number.</p>
     *
     * @param lineItems An {@link ArrayList} of {@link TransactionLineItem} corresponding to the transaction line items to sort by order number.
     * @return A {@link HashMap} whose key is the order number {@link String} and whose value is an {@link ArrayList} of
     * {@link TransactionLineItem} corresponding to transaction line items with the same order number.
     */
    private HashMap<String, ArrayList<TransactionLineItem>> sortLineItemsByOrderNumber (ArrayList<TransactionLineItem> lineItems) {

        if (DataFunctions.isEmptyCollection(lineItems)) {
            Logger.logMessage("The transaction line items passed to KMSReceipt.sortLineItemsByOrderNumber can't be null or empty!", log, Logger.LEVEL.ERROR);
            return null;
        }

        HashMap<String, ArrayList<TransactionLineItem>> lineItemsSortedByOrderNumber = new HashMap<>();
        ArrayList<TransactionLineItem> lineItemsForOrderNumber;
        // keep track of the order number for each product, modifiers will need to follow their parent product
        HashMap<Integer, String> productOrderNumber = new HashMap<>();
        for (TransactionLineItem lineItem : lineItems) {
            String orderNumber = "";
            if (lineItem.getPATransLineItemModID() <= 0) {
                // determine the order number and keep track for the product
                orderNumber = (StringFunctions.stringHasContent(lineItem.getOriginalOrderNumber()) ? lineItem.getOriginalOrderNumber() : lineItem.getOrderNumber());
                productOrderNumber.put(lineItem.getPATransLineItemID(), orderNumber);
            }
            else if (lineItem.getPATransLineItemModID() > 0) {
                // get the order number of the parent product for the modifer
                orderNumber = productOrderNumber.get(lineItem.getPATransLineItemID());
            }
            // do the sorting
            if (lineItemsSortedByOrderNumber.containsKey(orderNumber)) {
                lineItemsForOrderNumber = lineItemsSortedByOrderNumber.get(orderNumber);
            }
            else {
                lineItemsForOrderNumber = new ArrayList<>();
            }
            lineItemsForOrderNumber.add(lineItem);
            lineItemsSortedByOrderNumber.put(orderNumber, lineItemsForOrderNumber);
        }

        return lineItemsSortedByOrderNumber;
    }
    //endregion transaction data sorting

    //region receipt detail building
    /**
     * <p>Iterates through the transaction line items that should be printed on each kitchen printer and builds the details that should appear on each receipt.</p>
     *
     * @param transLineItemsForStations A {@link HashMap} whose key is the {@link Integer} ID of the kitchen printer and
     * whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to the products and modifiers that
     * should be sent to the printer.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the receipt details for the kitchen printer receipt.
     */
    private ArrayList<HashMap> buildKMSReceiptDetails (HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForStations) {

        if (DataFunctions.isEmptyMap(transLineItemsForStations)) {
            Logger.logMessage("The transaction line items mapped to a kitchen printer can be null or empty");
            return null;
        }
        // get whether or not the expeditor will only print receipts if new products are added
        if (expeditorStationID > 0) {
            noExpeditorReceiptUnlessProductsAdded = true;//DEFAULT for KDS and KMS
            Logger.logMessage(String.format("The expeditor station with an ID of %s within the revenue center with an ID of %s %s only print receipts if new products are added.",
                    Objects.toString(expeditorStationID, "N/A"),
                    Objects.toString(revenueCenterID, "N/A"),
                    Objects.toString((noExpeditorReceiptUnlessProductsAdded ? "will" : "won't"), "N/A")), log, Logger.LEVEL.TRACE);
        }

//        // get the IDs off all printers so we can determine whether or not they will only print new products
//        ArrayList<Integer> printerIDs = new ArrayList<>(transLineItemsForStations.keySet());
//        printerOnlyPrintsNewProductsHM = getPrintOnlyPrintsNewProductsHM(printerIDs);
//        if (!DataFunctions.isEmptyMap(printerOnlyPrintsNewProductsHM)) {
//            for (Map.Entry<Integer, Boolean> entry : printerOnlyPrintsNewProductsHM.entrySet()) {
//                Logger.logMessage(String.format("The printer with an ID of %s %s only print new products.",
//                        Objects.toString(entry.getKey(), "N/A"),
//                        Objects.toString(entry.getValue() ? "will" : "won't", "N/A")), log, Logger.LEVEL.TRACE);
//            }
//        }
//        else {
//            Logger.logMessage("No printers found in KMSReceipt.buildKMSReceiptDetails, unable to determine whether or not the printers only print new products!", log, Logger.LEVEL.ERROR);
//            return null;
//        }

        ArrayList<HashMap> receiptDetails = new ArrayList<>();

        for (Map.Entry<Integer, ArrayList<TransactionLineItem>> entry : transLineItemsForStations.entrySet()) {
            int stationID = entry.getKey();
            ArrayList<TransactionLineItem> lineItems = entry.getValue();

            // sort the transaction line items by their simple print status (PRINTED/UNPRINTED) and order number
            HashMap<String, HashMap<String, ArrayList<TransactionLineItem>>> sortedLineItemsForKMSReceipt = sortLineItemsForKMSReceipt(lineItems);

            // don't create an expeditor receipt if noExpeditorReceiptUnlessProductsAdded is checked and there aren't any new expeditor products
//            if ((stationID == expeditorStationID)
//                    && (!remoteOrdersUseExpeditor)
//                    && (noExpeditorReceiptUnlessProductsAdded)
//                    && ((DataFunctions.isEmptyMap(sortedLineItemsForKMSReceipt))
//                    || ((!DataFunctions.isEmptyMap(sortedLineItemsForKMSReceipt)) && (!sortedLineItemsForKMSReceipt.containsKey(TypeData.SimplePrintStatus.UNPRINTED))))) {
//                continue;
//            }

            // determine whether or not to use the ADD ON header within the receipt header
            boolean useAddOn = useAddOn(stationID, sortedLineItemsForKMSReceipt);

            // create the receipt header details
            ArrayList<HashMap> receiptHeaderDetails = buildKMSReceiptHeaderDetails(stationID, useAddOn);
            if (!DataFunctions.isEmptyCollection(receiptHeaderDetails)) {
                receiptDetails.addAll(receiptHeaderDetails);
            }
            else {
                Logger.logMessage(String.format("No receipt header details were created for the transaction with an ID of %s for the printer with an ID of %s in KMSReceipt.buildKMSReceiptDetails!",
                        Objects.toString(transactionData.getPATransactionID(), "N/A"),
                        Objects.toString(stationID, "N/A")), log, Logger.LEVEL.ERROR);
                return null;
            }

            // create receipt details for the products and modifiers within the transaction
            ArrayList<HashMap> productModifierDetails = buildKMSReceiptProductModifierDetails(stationID, sortedLineItemsForKMSReceipt);
            if (!DataFunctions.isEmptyCollection(productModifierDetails)) {
                receiptDetails.addAll(productModifierDetails);
            }
            else {
                Logger.logMessage(String.format("No receipt details were created for products and modifiers within the transaction " +
                        "with an ID of %s for the printer with an ID of %s in KMSReceipt.buildKMSReceiptDetails!",
                        Objects.toString(transactionData.getPATransactionID(), "N/A"),
                        Objects.toString(stationID, "N/A")), log, Logger.LEVEL.ERROR);
                return null;
            }

            double totalNumberOfItems = calculateTheTotalNumberOfItems(stationID, receiptDetails);
            ArrayList<HashMap> receiptFooterDetails = buildReceiptFooterDetails(stationID, totalNumberOfItems);
            if (!DataFunctions.isEmptyCollection(receiptFooterDetails)) {
                receiptDetails.addAll(receiptFooterDetails);
            }
            else {
                Logger.logMessage(String.format("No receipt footer details were created for products and modifiers within the transaction " +
                        "with an ID of %s for the printer with an ID of %s in KMSReceipt.buildKMSReceiptDetails!",
                        Objects.toString(transactionData.getPATransactionID(), "N/A"),
                        Objects.toString(stationID, "N/A")), log, Logger.LEVEL.ERROR);
                return null;
            }
        }

        return receiptDetails;
    }

    /**
     * <p>Builds a receipt header for kitchen printer receipts.</p>
     *
     * @param stationID The ID of the station to build the receipt header for.
     * @param useAddOn Whether or not to add ADD ON to the receipt header.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the receipt header details.
     */
    private ArrayList<HashMap> buildKMSReceiptHeaderDetails (int stationID, boolean useAddOn) {

        if (stationID <= 0) {
            Logger.logMessage("The ID of the stationID passed to KMSReceipt.buildKMSReceiptHeaderDetails must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (transactionData == null) {
            Logger.logMessage("No transaction data found in KMSReceipt.buildKMSReceiptHeaderDetails!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("No transaction header was found within the transaction in KMSReceipt.buildKMSReceiptHeaderDetails!", log, Logger.LEVEL.ERROR);
            return null;
        }

//        if (renderer == null) {
//            Logger.logMessage("No renderer found in KMSReceipt.buildKMSReceiptHeaderDetails!", log, Logger.LEVEL.ERROR);
//            return null;
//        }

        // build the receipt header
        KitchenReceiptHeader kitchenReceiptHeader =
                new KitchenReceiptHeader()
                        .addPATransactionID(transactionData.getPATransactionID())
                        .addOnlineOrder(transactionData.getTransactionHeader().getOnlineOrder())
                        .addTransactionDate(transactionData.getTransactionHeader().getTransactionDate())
                        .addQueueTime(transactionData.getTransactionHeader().getQueueTime())
                        .addPrintStatusID(PrintStatusType.WAITING)
                        .addTerminalID(transactionData.getTransactionHeader().getTerminalID())
                        .addPAOrderTypeID(transactionData.getTransactionHeader().getPAOrderTypeID())
                        .addPersonName(transactionData.getTransactionHeader().getPersonName())
                        .addPhone(transactionData.getTransactionHeader().getPhone())
                        .addTransComment(transactionData.getTransactionHeader().getTransactionComment())
                        .addPickUpDeliveryNote(transactionData.getTransactionHeader().getPickupDeliveryNote())
                        .addTransName(transactionData.getTransactionHeader().getTransactionName())
                        .addTransNameLabel(transactionData.getTransactionHeader().getTransactionNameLabel())
                        .addEstimatedOrderTime(transactionData.getTransactionHeader().getEstimatedOrderTime())
                        .addOrderNum(transactionData.getTransactionHeader().getOrderNumber())
                        .addTransTypeID(transactionData.getTransactionHeader().getTransactionTypeID())
                        .addPrevKDSOrderNums(StringFunctions.buildStringFromGenericArr(transactionData.getTransactionHeader().getPreviousOrderNumbersInTransaction(), ","))
                        .addStationName(getStationName(stationID))
                        .addKMSStationName(getStationName(stationID))
                        .addDeliveryLocation((transactionData.getTransactionHeader().getPAOrderTypeID() == TypeData.OrderType.DELIVERY ? transactionData.getTransactionHeader().getAddress() : null))
                        .addKMSStationID(stationID)
                        .addPrinterHostID(transactionData.getTransactionHeader().getPrinterHostID())
                        .addCashierName(transactionData.getTransactionHeader().getCashierName())
                        .addUseAddOn(useAddOn)
                        .addDataManager(dataManager)
                        .addLog(log)
                        .addMaxReceiptCharsPerLineNormal(maxCharsPerReceiptLineNormal)
                        .addMaxReceiptCharsPerLineLarge(maxCharsPerReceiptLineLarge);
        Logger.logMessage("Built PrevKDSOrderNums to be " + kitchenReceiptHeader.getPrevKDSOrderNums() + " from " + transactionData.getTransactionHeader().getPreviousOrderNumbersInTransaction(), "KMS.log", Logger.LEVEL.TRACE);
        return kitchenReceiptHeader.build();
    }

    /**
     * <p>Creates kitchen printer receipt details for products and modifiers within the transaction that should appear on the printer.</p>
     *
     * @param stationID The ID of the station the receipt details should be created for.
     * @param sortedLineItemsForKMSReceipt A {@link HashMap} whose key is a {@link String} of whether or not the products have been printed and whose value is a {@link HashMap} whose
     * key is the order number {@link String} and whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to transaction line items
     * with the same order number that have/haven't been printed.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to kitchen printer receipt details for products and modifiers.
     */
    private ArrayList<HashMap> buildKMSReceiptProductModifierDetails (int stationID, HashMap<String, HashMap<String, ArrayList<TransactionLineItem>>> sortedLineItemsForKMSReceipt) {
        ArrayList<HashMap> productModifierDetails = new ArrayList<>();
        Logger.logMessage("buildKMSReceiptProductModifierDetails", "KMS.log", Logger.LEVEL.TRACE);
        if (stationID <= 0) {
            Logger.logMessage("The stationID ID passed to KMSReceipt.buildKMSReceiptProductModifierDetails must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (DataFunctions.isEmptyMap(sortedLineItemsForKMSReceipt)) {
            Logger.logMessage(String.format("The sorted transaction line items passed to KMSReceipt.buildKMSReceiptProductModifierDetails can't be null or empty for the transaction with an ID of %s!",
                    Objects.toString((transactionData != null ? transactionData.getPATransactionID() : "N/A"), "N/A")), log, Logger.LEVEL.ERROR);
            return null;
        }

//        if (DataFunctions.isEmptyMap(printerOnlyPrintsNewProductsHM)) {
//            Logger.logMessage("The only prints new products HashMap can't be null or empty in KMSReceipt.buildKMSReceiptProductModifierDetails!", log, Logger.LEVEL.ERROR);
//            return null;
//        }

        boolean printOnlyNewItems = true;//(printerOnlyPrintsNewProductsHM.containsKey(printerID) ? printerOnlyPrintsNewProductsHM.get(printerID) : false);

        KitchenReceiptDetail kitchenReceiptDetail;
        int kmsStationID = stationID;
        int kmsOrderStatusID = -1;
        int printerHostID = getPrinterHostID();
        for (Map.Entry<String, HashMap<String, ArrayList<TransactionLineItem>>> simplePrintStatusEntry : sortedLineItemsForKMSReceipt.entrySet()) {
            if ((simplePrintStatusEntry.getKey().equalsIgnoreCase(TypeData.SimplePrintStatus.PRINTED)) && (!printOnlyNewItems)) {
                // add an empty line
                kitchenReceiptDetail =
                        new KitchenReceiptDetail()
//                                .addPrinterID(printerID)
                                .addKMSStationID(kmsStationID)
                                .addPrintStatusID(PrintStatusType.WAITING)
                                .addKMSOrderStatusID(kmsOrderStatusID)
                                .addPrinterHostID(printerHostID)
                                .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
//                                .addRenderer(renderer)
                                .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
                productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
                // add the Already Sent sub header
                kitchenReceiptDetail =
                        new KitchenReceiptDetail()
//                                .addPrinterID(printerID)
                                .addKMSStationID(kmsStationID)
                                .addPrintStatusID(PrintStatusType.WAITING)
                                .addKMSOrderStatusID(kmsOrderStatusID)
                                .addPrinterHostID(printerHostID)
                                .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + "****     ALREADY SENT     ****")
//                                .addRenderer(renderer)
                                .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
                productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
            }
            else if ((simplePrintStatusEntry.getKey().equalsIgnoreCase(TypeData.SimplePrintStatus.UNPRINTED)) && (!printOnlyNewItems)) {
                // add an empty line
                kitchenReceiptDetail =
                        new KitchenReceiptDetail()
//                                .addPrinterID(printerID)
                                .addKMSStationID(kmsStationID)
                                .addPrintStatusID(PrintStatusType.WAITING)
                                .addKMSOrderStatusID(kmsOrderStatusID)
                                .addPrinterHostID(printerHostID)
                                .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
//                                .addRenderer(renderer)
                                .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
                productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
                // add the Already Sent sub header
                kitchenReceiptDetail =
                        new KitchenReceiptDetail()
//                                .addPrinterID(printerID)
                                .addKMSStationID(kmsStationID)
                                .addPrintStatusID(PrintStatusType.WAITING)
                                .addKMSOrderStatusID(kmsOrderStatusID)
                                .addPrinterHostID(printerHostID)
                                .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + "****     NEW PRODUCTS     ****")
//                                .addRenderer(renderer)
                                .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
                productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
            }
            HashMap<String, ArrayList<TransactionLineItem>> lineItemsByOrderNumber = simplePrintStatusEntry.getValue();
            if (!DataFunctions.isEmptyMap(lineItemsByOrderNumber)) {
                for (Map.Entry<String, ArrayList<TransactionLineItem>> orderNumberEntry : lineItemsByOrderNumber.entrySet()) {
                    // add an empty line
                    kitchenReceiptDetail =
                            new KitchenReceiptDetail()
//                                    .addPrinterID(printerID)
                                    .addKMSStationID(kmsStationID)
                                    .addPrintStatusID(PrintStatusType.WAITING)
                                    .addKMSOrderStatusID(kmsOrderStatusID)
                                    .addPrinterHostID(printerHostID)
                                    .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
//                                    .addRenderer(renderer)
                                    .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
                    productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
                    // add the Order Number sub header
                    kitchenReceiptDetail =
                            new KitchenReceiptDetail()
//                                    .addPrinterID(printerID)
                                    .addKMSStationID(kmsStationID)
                                    .addPrintStatusID(PrintStatusType.WAITING)
                                    .addKMSOrderStatusID(kmsOrderStatusID)
                                    .addPrinterHostID(printerHostID)
                                    .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + "--- Order Number: " + (StringFunctions.stringHasContent(orderNumberEntry.getKey()) ? orderNumberEntry.getKey() : "N/A") + "  ---")
//                                    .addRenderer(renderer)
                                    .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
                    productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
                    ArrayList<TransactionLineItem> lineItems = orderNumberEntry.getValue();
                    if (!DataFunctions.isEmptyCollection(lineItems)) {
                        for (TransactionLineItem lineItem : lineItems) {
                            // add each line item
                            boolean isModifier = (lineItem.getPATransLineItemModID() > 0);
                            String lineItemStr;
//                            if (!isModifier) {
//                                lineItemStr = String.format("%2dX %s", Math.round(lineItem.getQuantity()), lineItem.getProductName());
//                            }
//                            else {
                                lineItemStr = String.format("%s", lineItem.getProductName());
//                            }
                            kitchenReceiptDetail =
                                    new KitchenReceiptDetail()
//                                            .addPrinterID(printerID)
                                            .addKMSStationID(kmsStationID)
                                            .addPrintStatusID(PrintStatusType.WAITING)
                                            .addKMSOrderStatusID(kmsOrderStatusID)
                                            .addPrinterHostID(printerHostID)
                                            //.addLineDetail(lineItemStr)
                                            .addLineDetail(KitchenReceiptHelper.buildLineItemText(log, lineItem, false, false, true))
                                            .addProductID(lineItem.getPAPluID())
                                            .addQuantity(lineItem.getQuantity())
                                            .addIsModifier(isModifier)
                                            .addTransLineItemID(lineItem.getPATransLineItemID())
                                            .addLinkedFromIDs(lineItem.getLinkedFromIDs())
//                                            .addRenderer(renderer)
                                            .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
                            Logger.logMessage("lineItem.getPAPluID(): " + lineItem.getPAPluID() + " kitchenReceiptDetail.getProductID(): " + kitchenReceiptDetail.getProductID(), Logger.LEVEL.LUDICROUS);
                            //TODO: the mod ID probably belongs in it's own field, but we dont have one at the moment soo
                            if(lineItem.getPATransLineItemID() > 0){
                                kitchenReceiptDetail.addTransLineItemID(lineItem.getPATransLineItemID());
                            }
                            if(lineItem.getPATransLineItemModID() > 0){
                                kitchenReceiptDetail.addTransLineItemID(lineItem.getPATransLineItemModID());
                            }
                            if(lineItem.getLinkedFromIDs() != null ){
                                kitchenReceiptDetail.addLinkedFromIDs(lineItem.getLinkedFromIDs());
                            }

                            ArrayList<KitchenReceiptDetail> details = kitchenReceiptDetail.build();
                            //remove the quantity on all wrapped line details
                            for(int i = 1; i < details.size(); i++){
                                details.get(i).setQuantity(0);
                            }
                            productModifierDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(details)));
                        }
                    }
                }
            }
        }

        return productModifierDetails;
    }

    /**
     * <p>Adds the total number of items, a barcode (if applicable) and the command to cut the kitchen printer receipt.</p>
     *
     * @param stationID The ID of the station containing the receipt to add the receipt footer to.
     * @param totalNumberOfItems The total number of items on the kitchen printer receipt.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to kitchen printer receipt details for the receipt footer.
     */
    private ArrayList<HashMap> buildReceiptFooterDetails (int stationID, double totalNumberOfItems) {

        if (stationID <= 0) {
            Logger.logMessage("The station ID passed to KMSReceipt.buildReceiptFooterDetails must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (totalNumberOfItems <= 0.0d) {
            Logger.logMessage("The total number of items passed to KMSReceipt.buildReceiptFooterDetails must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (transactionData == null) {
            Logger.logMessage("The transaction data in KMSReceipt.buildReceiptFooterDetails can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("The transaction header data in KMSReceipt.buildReceiptFooterDetails can't be null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<HashMap> KMSReceiptFooterDetails = new ArrayList<>();
        KitchenReceiptDetail kitchenReceiptDetail;
        int kmsStationID = stationID;
        int kmsOrderStatusID = -1;
        int printerHostID = getPrinterHostID();
        // add an empty line
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
//                        .addPrinterID(printerID)
                        .addKMSStationID(kmsStationID)
                        .addPrintStatusID(PrintStatusType.WAITING)
                        .addKMSOrderStatusID(kmsOrderStatusID)
                        .addPrinterHostID(printerHostID)
                        .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
//                        .addRenderer(renderer)
                        .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
        KMSReceiptFooterDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        // add a line of asterisks
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
//                        .addPrinterID(printerID)
                        .addKMSStationID(kmsStationID)
                        .addPrintStatusID(PrintStatusType.WAITING)
                        .addKMSOrderStatusID(kmsOrderStatusID)
                        .addPrinterHostID(printerHostID)
                        .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + "******************************")
//                        .addRenderer(renderer)
                        .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
        KMSReceiptFooterDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        // add an empty line
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
//                        .addPrinterID(printerID)
                        .addKMSStationID(kmsStationID)
                        .addPrintStatusID(PrintStatusType.WAITING)
                        .addKMSOrderStatusID(kmsOrderStatusID)
                        .addPrinterHostID(printerHostID)
                        .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
//                        .addRenderer(renderer)
                        .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
        KMSReceiptFooterDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        // add the total number of items
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
//                        .addPrinterID(printerID)
                        .addKMSStationID(kmsStationID)
                        .addPrintStatusID(PrintStatusType.WAITING)
                        .addKMSOrderStatusID(kmsOrderStatusID)
                        .addPrinterHostID(printerHostID)
                        .addLineDetail(String.format("Total Number of Items: %s", Math.round(totalNumberOfItems)))
//                        .addRenderer(renderer)
                        .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
        KMSReceiptFooterDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        // add the barcode if applicable
        if (shouldPrintBarcode(transactionData.getTransactionHeader().getRevenueCenterID())) {
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
//                            .addPrinterID(printerID)
                            .addKMSStationID(kmsStationID)
                            .addPrintStatusID(PrintStatusType.WAITING)
                            .addKMSOrderStatusID(kmsOrderStatusID)
                            .addPrinterHostID(printerHostID)
                            .addLineDetail(KitchenPrinterReceiptFormatter.BARCODE + transactionData.getPATransactionID())
//                            .addRenderer(renderer)
                            .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
            KMSReceiptFooterDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }
        // add a command to cut the receipt paper
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
//                        .addPrinterID(printerID)
                        .addKMSStationID(kmsStationID)
                        .addPrintStatusID(PrintStatusType.WAITING)
                        .addKMSOrderStatusID(kmsOrderStatusID)
                        .addPrinterHostID(printerHostID)
                        .addLineDetail(KitchenPrinterReceiptFormatter.JPOS_CUT)
//                        .addRenderer(renderer)
                        .addFixWordWrap(true, maxCharsPerReceiptLineNormal);
        KMSReceiptFooterDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));

        return KMSReceiptFooterDetails;
    }

    //region other receipt detail building functions

    /**
     * <p>Determines whether or not to use the add on header.</p>
     *
     * @param stationID ID of the station the receipt is for.
     * @param sortedLineItemsForKMSReceipt A {@link HashMap} whose key is a {@link String} of whether or not the products have been printed and whose value is a {@link HashMap} whose
     * key is the order number {@link String} and whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to transaction line items
     * with the same order number that have/haven't been printed.
     * @return whether or not to use the add on header.
     */
    private boolean useAddOn (int stationID, HashMap<String, HashMap<String, ArrayList<TransactionLineItem>>> sortedLineItemsForKMSReceipt) {

        if (DataFunctions.isEmptyMap(sortedLineItemsForKMSReceipt)) {
            Logger.logMessage("The transaction line items that have been sorted for kitchen printer receipts and passed to KMSReceipt.useAddOn, can't be null or empty!", log, Logger.LEVEL.ERROR);
            return false;
        }

        if (transactionData == null) {
            Logger.logMessage("No transaction data found in KMSReceipt.useAddOn!", log, Logger.LEVEL.ERROR);
            return false;
        }

        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("No transaction header was found within the transaction in KMSReceipt.useAddOn!", log, Logger.LEVEL.ERROR);
            return false;
        }

        String currentOrderNumber = transactionData.getTransactionHeader().getOrderNumber();

        boolean onlyPrintNewProducts = true;
        boolean hasUnprintedProducts = !DataFunctions.isEmptyMap(sortedLineItemsForKMSReceipt.get(TypeData.SimplePrintStatus.UNPRINTED));
        boolean hasProductsFromPreviousOrder = false;
        for (Map.Entry<String, HashMap<String, ArrayList<TransactionLineItem>>> entry : sortedLineItemsForKMSReceipt.entrySet()) {
            if ((!DataFunctions.isEmptyMap(entry.getValue())) && (!DataFunctions.isEmptyCollection(entry.getValue().keySet()))) {
                for (String orderNumber : entry.getValue().keySet()) {
                    if (!orderNumber.equalsIgnoreCase(currentOrderNumber)) {
                        hasProductsFromPreviousOrder = true;
                        break;
                    }
                }
            }
            if (hasProductsFromPreviousOrder) {
                break;
            }
        }
        boolean hasProductsFromCurrentOrder = false;
        for (Map.Entry<String, HashMap<String, ArrayList<TransactionLineItem>>> entry : sortedLineItemsForKMSReceipt.entrySet()) {
            if ((!DataFunctions.isEmptyMap(entry.getValue())) && (!DataFunctions.isEmptyCollection(entry.getValue().keySet()))) {
                for (String orderNumber : entry.getValue().keySet()) {
                    if (orderNumber.equalsIgnoreCase(currentOrderNumber)) {
                        hasProductsFromCurrentOrder = true;
                        break;
                    }
                }
            }
            if (hasProductsFromCurrentOrder) {
                break;
            }
        }

        return ((onlyPrintNewProducts) && (hasUnprintedProducts) && (hasProductsFromPreviousOrder) && (hasProductsFromCurrentOrder));
    }

    /**
     * <p>Queries the database to get the name of the printer with the given printer ID.</p>
     *
     * @param stationID The ID of the station to get the name of.
     * @return The {@link String} name of the printer with the given printer ID.
     */
    private String getStationName (int stationID) {
        String printerName = "";

        try {
            if (stationID <= 0) {
                Logger.logMessage("The stationID passed to KMSReceipt.getPrinterName must be greater than 0!", log, Logger.LEVEL.ERROR);
                return null;
            }

            DynamicSQL sql =
                    new DynamicSQL("data.kms.GetStationName")
                            .addIDList(1, stationID);
            printerName = sql.getSingleStringField(dataManager);
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("A problem occurred while trying to get the name of the stationID with an ID of %s in KMSReceipt.getPrinterName!",
                    Objects.toString(stationID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return printerName;
    }

    /**
     * <p>Iterates through the kitchen printer receipt details and calculates the total number of items on the receipt, excluding modifiers for a product.</p>
     *
     * @param KMSReceiptDetails An {@link ArrayList} of {@link HashMap} corresponding to the receipt details for the kitchen printer receipt.
     * @return The total number of items on the receipt.
     */
    private double calculateTheTotalNumberOfItems (int stationID, ArrayList<HashMap> KMSReceiptDetails) {

        if (DataFunctions.isEmptyCollection(KMSReceiptDetails)) {
            Logger.logMessage("The kitchen printer receipt details passed to KMSReceipt.calculateTheTotalNumberOfItems can't be null or empty!", log, Logger.LEVEL.ERROR);
            return -1.0d;
        }

        // calculate the total number of items for the printer
        double total = 0.0d;
        for (HashMap KMSReceiptDetail : KMSReceiptDetails) {
            if (!DataFunctions.isEmptyMap(KMSReceiptDetail)) {
                boolean isModifier = HashMapDataFns.getBooleanVal(KMSReceiptDetail, "ISMODIFIER");
                double quantity = HashMapDataFns.getDoubleVal(KMSReceiptDetail, "QUANTITY");
                if ((stationID == HashMapDataFns.getIntVal(KMSReceiptDetail, "KMSSTATIONID")) && (!isModifier) && (quantity > 0.0d)) {
                    total += quantity;
                }
            }
        }

        return total;
    }
    //endregion other receipt detail building functions
    //endregion receipt detail building

    //region station mappers

    /**
     * <p>Determines which KMS stations to send each product or modifier transaction line item to.</p>
     *
     * @return A {@link HashMap} whose get is an {@link Integer} ID of the kms station and whose value is an {@link ArrayList}
     * of {@link TransactionLineItem} corresponding to the product and modifier transaction line items that should be sent to the station.
     */
    private HashMap<Integer, ArrayList<TransactionLineItem>> determineStationsForTransactionLineItems() {
        Logger.logMessage("KMSReceipt.determineStationsForTransactionLineItems", log, Logger.LEVEL.IMPORTANT);
        HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForStation = new HashMap<>();
        TransactionHeader transactionHeader = transactionData.getTransactionHeader();
        if (transactionHeader == null) {
            Logger.logMessage("No valid transaction header was found in KMSReceipt.determinePrintersForTransactionLineItems!", log, Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<MMHTree<TransactionLineItem>> transactionLineItems = transactionData.getTransactionLineItems();
        if (DataFunctions.isEmptyCollection(transactionLineItems)) {
            Logger.logMessage("No valid transaction line items were found in KMSReceipt.determinePrintersForTransactionLineItems!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // get the ID of the revenue center the transaction took place in
        revenueCenterID = transactionHeader.getRevenueCenterID();
        // get the expeditor and remote order kitchen printer IDs in the revenue center if they exist
        expeditorStationID = KitchenReceiptHelper.getKMSExpeditor(log, dataManager, revenueCenterID);
        remoteOrderStationID = KitchenReceiptHelper.getRemoteOrderKMS(log, dataManager, revenueCenterID);
        remoteOrdersUseExpeditor = KitchenReceiptHelper.remoteOrdersUseExpeditor(log, dataManager, revenueCenterID);

        if (expeditorStationID > 0) {
            Logger.logMessage(String.format("There is an expeditor station with a station ID of %s within the revenue center with an ID of %s.",
                    Objects.toString(expeditorStationID, "N/A"),
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
        }

        if (remoteOrderStationID > 0) {
            Logger.logMessage(String.format("There is an remote order station with a station ID of %s within the revenue center with an ID of %s.",
                    Objects.toString(remoteOrderStationID, "N/A"),
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
        }
        if (remoteOrdersUseExpeditor) {
            Logger.logMessage(String.format("The remote orders use expeditor setting is enabled within the revenue center with an ID of %s.",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.TRACE);
        }

        // check if there was a dining option within the transaction
        MMHTree<TransactionLineItem> diningOption = KitchenReceiptHelper.getDiningOptionTransLineItem(log, transactionData);
        if (diningOption != null) {
            Logger.logMessage(String.format("Found the dining option %s within the transaction with an ID of %s.",
                    Objects.toString((StringFunctions.stringHasContent(diningOption.getData().getProductName()) ? diningOption.getData().getProductName() : "N/A"), "N/A"),
                    Objects.toString(transactionData.getPATransactionID(), "N/A")), log, Logger.LEVEL.TRACE);
        }
        else {
            Logger.logMessage(String.format("No dining option has been found within the transaction with an ID of %s.",
                    Objects.toString(transactionData.getPATransactionID(), "N/A")), log, Logger.LEVEL.TRACE);
        }

        setPrepStationsForTransLineItems(transLineItemsForStation);
        if (expeditorStationID > 0) {
            setExpeditorStationForTransLineItems(transLineItemsForStation);
        }
        if (remoteOrderStationID > 0) {
            setRemoteOrderStationForTransLineItems(transLineItemsForStation);
        }

        if (diningOption != null) {
            setStationsForDiningOption(transLineItemsForStation, diningOption);
        }

        return transLineItemsForStation;
    }

    /**
     * <p>Sets which products and modifiers within the transaction will go to each prep kitchen printer.</p>
     *
     * @param transLineItemsForStation A {@link HashMap} whose key is the {@link Integer} ID of the kitchen printer and
     * whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to the products and modifiers that
     * should be sent to the printer.
     */
    private void setPrepStationsForTransLineItems (HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForStation) {

        if (DataFunctions.isEmptyCollection(transactionData.getTransactionLineItems())) {
            Logger.logMessage("No valid transaction line items were found in KMSReceipt.setPrepKPForTransLineItems!", log, Logger.LEVEL.ERROR);
            return;
        }

        ArrayList<TransactionLineItem> commonPrinterLineItems;
        for (MMHTree<TransactionLineItem> productLineItem : transactionData.getTransactionLineItems()) {
            TransactionLineItem product = productLineItem.getData();
            if ((product != null) && (!product.getDiningOption())) {
                // get any modifiers for the product
                ArrayList<TransactionLineItem> modifiers = KitchenReceiptHelper.getModifiersForProduct(log, productLineItem);

                // get the IDs of prep stations mapped to the product

                int[] stationIDs = getStationIDsMappedToProduct(product.getPAPluID());
                if (!DataFunctions.isEmptyIntArr(stationIDs)) {
                    for (int stationID : stationIDs) {
                        if (transLineItemsForStation.containsKey(stationID)) {
                            commonPrinterLineItems = transLineItemsForStation.get(stationID);
                        }
                        else {
                            commonPrinterLineItems = new ArrayList<>();
                        }
                        // add the product and modifier transaction line items for the printer
                        commonPrinterLineItems.add(product);
                        if (!DataFunctions.isEmptyCollection(modifiers)) {
                            commonPrinterLineItems.addAll(modifiers);
                        }
                        transLineItemsForStation.put(stationID, commonPrinterLineItems);
                    }
                }
            }
        }

    }

    /**
     * <p>Sets which products and modifiers within the transaction will go to the expeditor kitchen printer.</p>
     *
     * @param transLineItemsForStation A {@link HashMap} whose key is the {@link Integer} ID of the kitchen printer and
     * whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to the products and modifiers that
     * should be sent to the printer.
     */
    @SuppressWarnings("ConstantConditions")
    private void setExpeditorStationForTransLineItems(HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForStation) {
        Logger.logMessage("Entering setExpeditorStationForTransLineItems", log, Logger.LEVEL.TRACE);
        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("The transaction header is invalid KMSReceipt.setExpeditorKPForTransLineItems!", log, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(transactionData.getTransactionLineItems())) {
            Logger.logMessage("No valid transaction line items were found in KMSReceipt.setExpeditorKPForTransLineItems!", log, Logger.LEVEL.ERROR);
            return;
        }

        ArrayList<TransactionLineItem> commonPrinterLineItems = new ArrayList<>();
        for (MMHTree<TransactionLineItem> productLineItem : transactionData.getTransactionLineItems()) {
            TransactionLineItem product = productLineItem.getData();
            if (product != null) {
                // get any modifiers for the product
                ArrayList<TransactionLineItem> modifiers = KitchenReceiptHelper.getModifiersForProduct(log, productLineItem);

                // determine which transaction line items will go to the expeditor printer
                if (!remoteOrdersUseExpeditor) {
                    // the kitchen printer will function as a standard expeditor printer (relies on the PrintsOnExpeditor setting)
                    Logger.logMessage(String.format("The KMS station with an ID of %s will function as a standard expeditor printer (relies on the PrintsOnExpeditor setting)",
                            Objects.toString(expeditorStationID, "N/A")), log, Logger.LEVEL.TRACE);
                    if (product.getPrintsOnExpeditor()) {
                        commonPrinterLineItems.add(product);
                        if (!DataFunctions.isEmptyCollection(modifiers)) {
                            commonPrinterLineItems.addAll(modifiers);
                        }
                    }
                }
                else if ((remoteOrdersUseExpeditor) && (!transactionData.getTransactionHeader().getRemoteOrder())) {
                    // the kitchen printer will function as a standard expeditor printer (relies on the PrintsOnExpeditor setting)
                    if (product.getPrintsOnExpeditor()) {
                        commonPrinterLineItems.add(product);
                        if (!DataFunctions.isEmptyCollection(modifiers)) {
                            commonPrinterLineItems.addAll(modifiers);
                        }
                    }
                }
                else if ((remoteOrdersUseExpeditor) && (transactionData.getTransactionHeader().getRemoteOrder())) {
                    // the kitchen printer will function as a remote order printer (ignores the PrintsOnExpeditor setting)
                    commonPrinterLineItems.add(product);
                    if (!DataFunctions.isEmptyCollection(modifiers)) {
                        commonPrinterLineItems.addAll(modifiers);
                    }
                }else{
                    Logger.logMessage("the kitchen printer will not receive this line item", log, Logger.LEVEL.TRACE);
                }
            }
        }

        // indicate the transaction line items will be sent to the expeditor printer
        if (!DataFunctions.isEmptyCollection(commonPrinterLineItems)) {
            transLineItemsForStation.put(expeditorStationID, commonPrinterLineItems);
        }

    }

    /**
     * <p>Sets which products and modifiers within the transaction will go to the remote order kitchen printer.</p>
     *
     * @param transLineItemsForStation A {@link HashMap} whose key is the {@link Integer} ID of the kitchen printer and
     * whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to the products and modifiers that
     * should be sent to the printer.
     */
    private void setRemoteOrderStationForTransLineItems(HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForStation) {

        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("The transaction header is invalid KMSReceipt.setExpeditorKPForTransLineItems!", log, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(transactionData.getTransactionLineItems())) {
            Logger.logMessage("No valid transaction line items were found in KMSReceipt.setRemoteOrderKPForTransLineItems!", log, Logger.LEVEL.ERROR);
            return;
        }

        ArrayList<TransactionLineItem> commonPrinterLineItems = new ArrayList<>();
        for (MMHTree<TransactionLineItem> productLineItem : transactionData.getTransactionLineItems()) {
            TransactionLineItem product = productLineItem.getData();
            if ((product != null) && (transactionData.getTransactionHeader().getRemoteOrder())) {
                commonPrinterLineItems.add(product);
                // get any modifiers for the product
                ArrayList<TransactionLineItem> modifiers = KitchenReceiptHelper.getModifiersForProduct(log, productLineItem);
                if (!DataFunctions.isEmptyCollection(modifiers)) {
                    commonPrinterLineItems.addAll(modifiers);
                }
            }
        }

        // indicate the transaction line items will be sent to the remote order printer
        if (!DataFunctions.isEmptyCollection(commonPrinterLineItems)) {
            transLineItemsForStation.put(remoteOrderStationID, commonPrinterLineItems);
        }

    }


    /**
     * <p>Sets which kitchen printers to print the dining option on.</p>
     *
     * @param transLineItemsForStation A {@link HashMap} whose key is the {@link Integer} ID of the kitchen printer and
     * whose value is an {@link ArrayList} of {@link TransactionLineItem} corresponding to the products and modifiers that
     * should be sent to the printer.
     * @param diningOptionLineItem A {@link MMHTree} of {@link TransactionLineItem} corresponding to the dining option line item within the transaction.
     */
    @SuppressWarnings({"ConstantConditions", "Duplicates"})
    private void setStationsForDiningOption(HashMap<Integer, ArrayList<TransactionLineItem>> transLineItemsForStation,
                                            MMHTree<TransactionLineItem> diningOptionLineItem) {
        Logger.logMessage("KMSReceipt.setStationsForDiningOption called", "KMS.log", Logger.LEVEL.TRACE);
        if ((diningOptionLineItem == null) || (diningOptionLineItem.getData() == null)) {
            Logger.logMessage("KMSReceipt.setStationsForDiningOption The dining option transaction line item passed to KMSReceipt.setStationsForDiningOption can't be null!", log, Logger.LEVEL.ERROR);
            return;
        }

        if (transactionData.getTransactionHeader() == null) {
            Logger.logMessage("KMSReceipt.setStationsForDiningOption The transaction header is invalid KMSReceipt.setStationsForDiningOption!", log, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyCollection(transactionData.getTransactionLineItems())) {
            Logger.logMessage("KMSReceipt.setStationsForDiningOption No valid transaction line items were found in KMSReceipt.setStationsForDiningOption!", log, Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyMap(transLineItemsForStation)) {
            Logger.logMessage("KMSReceipt.setStationsForDiningOption No transaction line items are being to kitchen stations in KMSReceipt.setStationsForDiningOption!", log, Logger.LEVEL.ERROR);
            return;
        }

        TransactionLineItem diningOption = diningOptionLineItem.getData();
        Logger.logMessage("diningOption is " + diningOption.toString(), "KMS.log", Logger.LEVEL.DEBUG);
        // determine which prep kitchen stations to print the dining option on
        // get the prep stations mapped to the dining option
        int[] mappedPrepStations = diningOption.getKmsStationIDsMappedToProduct();
        // check if there are products that will be displayed on the prep kitchen station, if so then add the dining option
        if (!DataFunctions.isEmptyIntArr(mappedPrepStations)) {
            for (int stationID : mappedPrepStations) {
                if ((transLineItemsForStation.containsKey(stationID)) && (!DataFunctions.isEmptyCollection(transLineItemsForStation.get(stationID)))) {
                    // add the dining option
                    ArrayList<TransactionLineItem> lineItemsForStation = transLineItemsForStation.get(stationID);
                    lineItemsForStation.add(0, diningOption);
                    transLineItemsForStation.put(stationID, lineItemsForStation);
                }
            }
        }

        // determine whether or not to display the dining option on the expeditor station
        if (transLineItemsForStation.containsKey(expeditorStationID)) {
            if (!remoteOrdersUseExpeditor) {
                if ((diningOption.getPrintsOnExpeditor()) && (!DataFunctions.isEmptyCollection(transLineItemsForStation.get(expeditorStationID)))) {
                    // add the dining option
                    ArrayList<TransactionLineItem> lineItemsForStation = transLineItemsForStation.get(expeditorStationID);
                    lineItemsForStation.add(0, diningOption);
                    transLineItemsForStation.put(expeditorStationID, lineItemsForStation);
                }
            }
            else if ((remoteOrdersUseExpeditor) && (!transactionData.getTransactionHeader().getRemoteOrder())) {
                if ((diningOption.getPrintsOnExpeditor()) && (!DataFunctions.isEmptyCollection(transLineItemsForStation.get(expeditorStationID)))) {
                    // add the dining option
                    ArrayList<TransactionLineItem> lineItemsForStation = transLineItemsForStation.get(expeditorStationID);
                    lineItemsForStation.add(0, diningOption);
                    transLineItemsForStation.put(expeditorStationID, lineItemsForStation);
                }
            }
            else if ((remoteOrdersUseExpeditor) && (transactionData.getTransactionHeader().getRemoteOrder())) {
                if (!DataFunctions.isEmptyCollection(transLineItemsForStation.get(expeditorStationID))) {
                    // add the dining option
                    ArrayList<TransactionLineItem> lineItemsForStation = transLineItemsForStation.get(expeditorStationID);
                    lineItemsForStation.add(0, diningOption);
                    transLineItemsForStation.put(expeditorStationID, lineItemsForStation);
                }
            }
        }

        // determine whether or not to display the dining option on the remote order station
        if ((transLineItemsForStation.containsKey(remoteOrderStationID))
                && (!DataFunctions.isEmptyCollection(transLineItemsForStation.get(remoteOrderStationID)))
                && (transactionData.getTransactionHeader().getRemoteOrder())) {
            // add the dining option
            ArrayList<TransactionLineItem> lineItemsForStation = transLineItemsForStation.get(remoteOrderStationID);
            Logger.logMessage("KMSReceipt.setStationsForDiningOption Dining Option: " + new Gson().toJson(diningOption), "KMS.log", Logger.LEVEL.DEBUG);
            lineItemsForStation.add(0, diningOption);
            transLineItemsForStation.put(remoteOrderStationID, lineItemsForStation);
        }

    }

    //endregion

    //region DB Getters

    /**
     * <p>Queries the database to check if the remote orders use expeditor setting is enabled in the same revenue center as the transaction.</p>
     *
     * @return If the remote orders use expeditor setting is enabled in the same revenue center as the transaction.
     */
    private boolean remoteOrdersUseExpeditor () {

        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KMSReceipt.remoteOrdersUseExpeditor must be greater than 0!", log, Logger.LEVEL.ERROR);
            return false;
        }

        // query the database
        Object result = dataManager.parameterizedExecuteScalar("data.kms.GetRemOrdUseExpInRC", new Object[]{revenueCenterID});
        if(result != null && result.toString().length() > 0){
            return Boolean.parseBoolean(result.toString());
        }

        Logger.logMessage("The revenue center ID passed to KMSReceipt.getExpeditorKP must be greater than 0!", log, Logger.LEVEL.ERROR);
        return false;
    }

    private int getExpeditorStationID () {

        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KMSReceipt.getExpeditorKP must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // query the database
        Object result = dataManager.parameterizedExecuteScalar("data.kms.getExpeditorStationForRC", new Object[]{revenueCenterID});
        if(result != null && result.toString().length() > 0){
            return Integer.parseInt(result.toString());
        }

        Logger.logMessage("The revenue center ID passed to KMSReceipt.getExpeditorKP must be greater than 0!", log, Logger.LEVEL.ERROR);
        return -1;
    }

    /**
     * <p>Finds a dining option transaction line item if it exists and returns it.</p>
     *
     * @return A {@link MMHTree} of {@link TransactionLineItem} corresponding to the dining option transaction line item.
     */
    private MMHTree<TransactionLineItem> getDiningOptionTransactionLineItem () {
        Logger.logMessage("KMSReceipt.getDiningOptionTransactionLineItem", log, Logger.LEVEL.TRACE);
        MMHTree<TransactionLineItem> diningOption = null;

        if (DataFunctions.isEmptyCollection(transactionData.getTransactionLineItems())) {
            Logger.logMessage("No valid transaction line items were found in KMSReceipt.remoteOrdersUseExpeditor", log, Logger.LEVEL.ERROR);
            return null;
        }

        // search for the dining option transaction line item
        int index = 0;
        for (MMHTree<TransactionLineItem> transactionLineItem : transactionData.getTransactionLineItems()) {
            TransactionLineItem productTransactionLineItem = transactionLineItem.getData();
            if ((productTransactionLineItem != null) && (productTransactionLineItem.getDiningOption())) {
                Logger.logMessage("KMSReceipt Found Dining Option", "KMS.log", Logger.LEVEL.DEBUG);
                diningOption = transactionLineItem;
                Logger.logMessage("KMSReceipt.getDiningOptionTransactionLineItem FOUND DINING OPTION", log, Logger.LEVEL.TRACE);
                break; // dining option has been found
            }
            index++;
        }

        // remove the dining option from the transaction line items
        if (diningOption != null) {
            Logger.logMessage("KMSReceipt.getDiningOptionTransactionLineItem Removing the dining option from the other transline items", log, Logger.LEVEL.TRACE);
            transactionData.getTransactionLineItems().remove(index);
        }

        return diningOption;
    }

    /**
     * <p>Queries the database to check if there is a remote order kitchen printer in the same revenue center as the transaction.</p>
     *
     * @return ID of a remote order kitchen printer in the same revenue center as the transaction.
     */
    private int getRemoteOrderStationID () {

        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID passed to KMSReceipt.getRemoteOrderStationID must be greater than 0!", log, Logger.LEVEL.ERROR);
            return -1;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.getRemoteOrderStationForRC").addIDList(1, revenueCenterID);
        return sql.getSingleIntField(dataManager);
    }

    /**
     * <p>Determine which OMS prep stations the product should be sent to.</p>
     *
     * @param productID The ID of the product to get the IDs of OMS prep stations for.
     * @return An int array containing the IDs of the OMS prep stations the product should be sent to.
     */
    private int[] getStationIDsMappedToProduct (int productID) {
        if (revenueCenterID <= 0) {
            Logger.logMessage("The revenue center ID within KMSReceipt.getStationIDsMappedToProduct must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (productID <= 0) {
            Logger.logMessage("The product ID passed to KMSReceipt.getStationIDsMappedToProduct must be greater than 0!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database to get the OMS prep stations mapped to the given product
        ArrayList<HashMap> queryRes = DataFunctions.getGenericALFromObjList(dataManager.parameterizedExecuteQuery("data.kms.getPrepStationsMappedToProduct", new Object[]{productID, revenueCenterID}, true), HashMap.class);
        queryRes = DataFunctions.purgeAlOfHm(queryRes);

        int[] mappedStationIDs = null;
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            mappedStationIDs = new int[queryRes.size()];
            for (int i = 0; i < queryRes.size(); i++) {
                mappedStationIDs[i] = HashMapDataFns.getIntVal(queryRes.get(i), "KMSSTATIONID");
            }
        }

        // remove any potential duplicated station IDs
        if (!DataFunctions.isEmptyIntArr(mappedStationIDs)) {
            mappedStationIDs = IntStream.of(mappedStationIDs).distinct().toArray();
        }

        if (!DataFunctions.isEmptyIntArr(mappedStationIDs)) {
            Logger.logMessage(String.format("The following OMS prep station IDs of %s, were found to be mapped to the product with an ID of %s in KMSReceipt.getStationIDsMappedToProduct!",
                    Objects.toString(StringFunctions.buildStringFromIntArr(mappedStationIDs, ","), "N/A"),
                    Objects.toString(productID, "N/A")), log, Logger.LEVEL.TRACE);
        }
        else {
            Logger.logMessage(String.format("No OMS prep station IDs were found to be mapped to the product with an ID of %s in KMSReceipt.getStationIDsMappedToProduct!",
                    Objects.toString(productID, "N/A")), log, Logger.LEVEL.TRACE);
        }

        return mappedStationIDs;
    }

    /**
     * <p>Check whether or not to print a barcode on the receipt based on the revenue center setting.</p>
     *
     * @param revenueCenterID ID of the revenue center to check.
     * @return Whether or not to print a barcode on the receipt based on the revenue center setting.
     */
    private boolean shouldPrintBarcode (int revenueCenterID) {

        try {
            if (revenueCenterID <= 0) {
                Logger.logMessage("The revenue center ID passed to KMSReceipt.shouldPrintBarcode must be greater than 0!", log, Logger.LEVEL.ERROR);
                return false;
            }

            if (dataManager == null) {
                Logger.logMessage("The DataManager in KMSReceipt.shouldPrintBarcode can't be null!", log, Logger.LEVEL.ERROR);
                return false;
            }

            // query the database
            DynamicSQL sql =
                    new DynamicSQL("data.kms.GetShouldPrintBarcode")
                            .addIDList(1, revenueCenterID);
            return sql.getSingleBooleanField(dataManager);
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to determine whether or not to add a barcode to kitchen " +
                    "printer receipts within the revenue center with an ID of %s in KMSReceipt.shouldPrintBarcode!",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return false;
    }

    /**
     * <p>Queries the database to determine the printer host ID of the printer host for the given printer.</p>
     *
     * @return The printer host ID of the printer host for the given printer.
     */
    private int getPrinterHostID () {

        try {
            if (revenueCenterID <= 0) {
                Logger.logMessage("The revenue center ID passed to KMSReceipt.getRemoteOrderStationID must be greater than 0!", log, Logger.LEVEL.ERROR);
                return -1;
            }

            if (dataManager == null) {
                Logger.logMessage("The DataManager in KMSReceipt.getPrinterHostID can't be null!", log, Logger.LEVEL.ERROR);
                return -1;
            }

            // query the database
            Object result = dataManager.parameterizedExecuteScalar("data.kms.GetPrinterHostIDForRC", new Object[]{revenueCenterID});
            if(result != null && result.toString().length() > 0){
                return Integer.parseInt(result.toString());
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to determine the printer host ID for the Revenue center with an ID of %s in KMSReceipt.getPrinterHostID!",
                    Objects.toString(revenueCenterID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return -1;
    }

    /**
     * <p>Iterates through the transaction line items from the transaction in search of a dining option.</p>
     *
     * @return A dining option {@link TransactionLineItem} if it is within the transaction.
     */
    private TransactionLineItem getDiningOption (ArrayList<MMHTree<TransactionLineItem>> transLineItems) {

        for (MMHTree<TransactionLineItem> productTree : transLineItems) {
            if ((productTree != null) && (productTree.getData() != null) && (productTree.getData().getDiningOption())) {
                return productTree.getData();
            }
        }

        return null;
    }

    //endregion

}