package com.mmhayes.common.kms.receiptGen.receiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-22 12:26:54 -0400 (Thu, 22 Apr 2021) $: Date of last commit
    $Rev: 13834 $: Revision of last commit
    Notes: Represents a detail on a kitchen receipt (KP/KDS/KMS).
*/

import com.mmhayes.common.receiptGen.Formatter.kitchenReceipt.KitchenReceiptCommon;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a detail on a kitchen receipt (KP/KDS/KMS).</p>
 *
 */
public class KitchenReceiptDetail {

    // private member variables of a KitchenReceiptDetail
    private int printerID = -1;
    private int printStatusID = -1;
    private int printerHostID = -1;
    private int productID = -1;
    private double quantity = -1.0d;
    private boolean isModifier = false;
    private String hiddenStation = "";
    private int maxCharsPerLine = 36;
    private boolean fixWordWrap = false;
    private String lineDetail = "";
    private IRenderer renderer = null;
    private int kmsOrderStatusID = -1;
    private int kmsStationID = -1;
    private int transLineItemID = -1;
    private String linkedFromIDs = null;

    /**
     * <p>Constructor for a {@link KitchenReceiptDetail}.</p>
     *
     */
    public KitchenReceiptDetail () {}

    /**
     * <p>Sets the printerID field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param printerID
     * @return The {@link KitchenReceiptDetail} instance with it's printerID field set.
     */
    public KitchenReceiptDetail addPrinterID (int printerID) {
        this.printerID = printerID;
        return this;
    }

    /**
     * <p>Sets the printStatusID field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param printStatusID
     * @return The {@link KitchenReceiptDetail} instance with it's printStatusID field set.
     */
    public KitchenReceiptDetail addPrintStatusID (int printStatusID) {
        this.printStatusID = printStatusID;
        return this;
    }

    /**
     * <p>Sets the printerHostID field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param printerHostID
     * @return The {@link KitchenReceiptDetail} instance with it's printerHostID field set.
     */
    public KitchenReceiptDetail addPrinterHostID (int printerHostID) {
        this.printerHostID = printerHostID;
        return this;
    }

    /**
     * <p>Sets the productID field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param productID
     * @return The {@link KitchenReceiptDetail} instance with it's productID field set.
     */
    public KitchenReceiptDetail addProductID (int productID) {
        this.productID = productID;
        return this;
    }

    /**
     * <p>Sets the quantity field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param quantity
     * @return The {@link KitchenReceiptDetail} instance with it's quantity field set.
     */
    public KitchenReceiptDetail addQuantity (double quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * <p>Sets the isModifier field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param isModifier
     * @return The {@link KitchenReceiptDetail} instance with it's isModifier field set.
     */
    public KitchenReceiptDetail addIsModifier (boolean isModifier) {
        this.isModifier = isModifier;
        return this;
    }

    /**
     * <p>Sets the hiddenStation field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param hiddenStation
     * @return The {@link KitchenReceiptDetail} instance with it's hiddenStation field set.
     */
    public KitchenReceiptDetail addHiddenStation (String hiddenStation) {
        this.hiddenStation = hiddenStation;
        return this;
    }

    /**
     * <p>Sets the fixWordWrap and maxCharsPerLine fields for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param fixWordWrap
     * @param maxCharsPerLine
     * @return The {@link KitchenReceiptDetail} instance with it's fixWordWrap field set.
     */
    public KitchenReceiptDetail addFixWordWrap (boolean fixWordWrap, int maxCharsPerLine) {
        this.fixWordWrap = fixWordWrap;
        this.maxCharsPerLine = maxCharsPerLine;
        return this;
    }

    /**
     * <p>Sets the lineDetail field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param lineDetail
     * @return The {@link KitchenReceiptDetail} instance with it's lineDetail field set.
     */
    public KitchenReceiptDetail addLineDetail (String lineDetail) {
        this.lineDetail = lineDetail;
        return this;
    }

    /**
     * <p>Sets the renderer field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param renderer
     * @return The {@link KitchenReceiptDetail} instance with it's renderer field set.
     */
    public KitchenReceiptDetail addRenderer (IRenderer renderer) {
        this.renderer = renderer;
        return this;
    }

    /**
     * <p>Sets the kmsOrderStatusID field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param kmsOrderStatusID
     * @return The {@link KitchenReceiptDetail} instance with it's kmsOrderStatusID field set.
     */
    public KitchenReceiptDetail addKMSOrderStatusID (int kmsOrderStatusID) {
        this.kmsOrderStatusID = kmsOrderStatusID;
        return this;
    }

    /**
     * <p>Sets the kmsStationID field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param kmsStationID
     * @return The {@link KitchenReceiptDetail} instance with it's kmsStationID field set.
     */
    public KitchenReceiptDetail addKMSStationID (int kmsStationID) {
        this.kmsStationID = kmsStationID;
        return this;
    }

    /**
     * <p>Sets the transLineItemID field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param transLineItemID
     * @return The {@link KitchenReceiptDetail} instance with it's transLineItemID field set.
     */
    public KitchenReceiptDetail addTransLineItemID (int transLineItemID) {
        this.transLineItemID = transLineItemID;
        return this;
    }

    /**
     * <p>Sets the linkedFromIDs field for this {@link KitchenReceiptDetail} instance.</p>
     *
     * @param linkedFromIDs
     * @return The {@link KitchenReceiptDetail} instance with it's linkedFromIDs field set.
     */
    public KitchenReceiptDetail addLinkedFromIDs(String linkedFromIDs) {
        this.linkedFromIDs = linkedFromIDs;
        return this;
    }

    /**
     * <p>Builds the {@link KitchenReceiptDetail}.</p>
     *
     * @return The constructed {@link KitchenReceiptDetail}.
     */
    @SuppressWarnings("Convert2streamapi")
    public ArrayList<KitchenReceiptDetail> build () {
        ArrayList<KitchenReceiptDetail> kitchenReceiptDetails = new ArrayList<>();
        ArrayList<String> newLineDetails = new ArrayList<>();
        // fix word wrap if necessary
        if (this.fixWordWrap) {
            newLineDetails = KitchenReceiptCommon.fixWordWrap(this.maxCharsPerLine, this.lineDetail);
        }
        else {
            newLineDetails.add(this.lineDetail);
        }
        for (String newLineDetail : newLineDetails) {
            if (this.renderer != null) {
                // remove JPOS commands and add the line detail to the HTML summary
                this.renderer.addLine(KitchenReceiptCommon.removeJPOSCommands(this.lineDetail));
            }
            kitchenReceiptDetails.add(
                    new KitchenReceiptDetail()
                            .addPrinterID(this.printerID)
                            .addPrintStatusID(this.printStatusID)
                            .addPrinterHostID(this.printerHostID)
                            .addProductID(this.productID)
                            .addQuantity(this.quantity)
                            .addIsModifier(this.isModifier)
                            .addHiddenStation(this.hiddenStation)
                            .addLineDetail(KitchenReceiptCommon.encode(newLineDetail))
                            .addKMSOrderStatusID(this.kmsOrderStatusID)
                            .addKMSStationID(this.kmsStationID)
                            .addTransLineItemID(this.transLineItemID)
                            .addLinkedFromIDs(this.linkedFromIDs)
            );
        }

        return kitchenReceiptDetails;
    }

    /**
     * <p>Converts the kitchen receipt details into HashMaps.</p>
     *
     * @param kitchenReceiptDetails An {@link ArrayList} of {@link KitchenReceiptDetail} corresponding to the kitchen receipt details to convert.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the converted kitchen receipt details.
     */
    @SuppressWarnings({"Convert2streamapi", "unchecked"})
    public static ArrayList<HashMap> getKitchenReceiptDetailsAsHMs (ArrayList<KitchenReceiptDetail> kitchenReceiptDetails) {

        if (kitchenReceiptDetails == null) {
            return null;
        }

        ArrayList<HashMap> kitchenReceiptHMs = new ArrayList<>();
        for (KitchenReceiptDetail kitchenReceiptDetail : kitchenReceiptDetails) {
            if (kitchenReceiptDetail != null) {
                HashMap kitchenReceiptHM = new HashMap();
                kitchenReceiptHM.put("PRINTERID", kitchenReceiptDetail.getPrinterID());
                kitchenReceiptHM.put("PRINTSTATUSID", kitchenReceiptDetail.getPrintStatusID());
                kitchenReceiptHM.put("PRINTERHOSTID", kitchenReceiptDetail.getPrinterHostID());
                kitchenReceiptHM.put("PRODUCTID", kitchenReceiptDetail.getProductID());
                kitchenReceiptHM.put("QUANTITY", kitchenReceiptDetail.getQuantity());
                kitchenReceiptHM.put("ISMODIFIER", kitchenReceiptDetail.getIsModifier());
                kitchenReceiptHM.put("HIDDENSTATION", kitchenReceiptDetail.getHiddenStation());
                kitchenReceiptHM.put("LINEDETAIL", kitchenReceiptDetail.getLineDetail());
                kitchenReceiptHM.put("KMSORDERSTATUSID", kitchenReceiptDetail.getKMSOrderStatusID());
                kitchenReceiptHM.put("KMSSTATIONID", kitchenReceiptDetail.getKMSStationID());
                kitchenReceiptHM.put("TRANSLINEITEMID", kitchenReceiptDetail.getTransLineItemID());
                if(kitchenReceiptDetail.getLinkedFromIDs() != null)
                    kitchenReceiptHM.put("LINKEDFROMIDS", kitchenReceiptDetail.getLinkedFromIDs());
                kitchenReceiptHMs.add(kitchenReceiptHM);
            }
        }

        return kitchenReceiptHMs;
    }

    /**
     * <p>Overridden toString() method for a {@link KitchenReceiptDetail}.</p>
     *
     * @return A {@link String} representation of this {@link KitchenReceiptDetail}.
     */
    @Override
    public String toString () {

        return String.format("PRINTERID: %s, PRINTSTATUSID: %s, PRINTERHOSTID: %s, PRODUCTID: %s, QUANTITY: %s, " +
                "ISMODIFIER: %s, HIDDENSTATION: %s, LINEDETAIL: %s, KMSORDERSTATUSID: %s, KMSSTATIONID: %s, TRANSLINEITEMID: %s, LINKEDFROMIDS: %s",
                Objects.toString((printerID > 0 ? printerID : "N/A"), "N/A"),
                Objects.toString((printStatusID > 0 ? printStatusID : "N/A"), "N/A"),
                Objects.toString((printerHostID > 0 ? printerHostID : "N/A"), "N/A"),
                Objects.toString((productID > 0 ? productID : "N/A"), "N/A"),
                Objects.toString((quantity > 0.0d ? quantity : "N/A"), "N/A"),
                Objects.toString(isModifier, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(hiddenStation) ? hiddenStation : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(lineDetail) ? lineDetail : "N/A"), "N/A"),
                Objects.toString((kmsOrderStatusID > 0 ? kmsOrderStatusID : "N/A"), "N/A"),
                Objects.toString((kmsStationID > 0 ? kmsStationID : "N/A"), "N/A"),
                Objects.toString((transLineItemID > 0 ? transLineItemID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(linkedFromIDs) ? linkedFromIDs : "N/A"), "N/A"));
    }

    /**
     * <p>Overridden equals() method for a {@link KitchenReceiptDetail}.
     * Two {@link KitchenReceiptDetail} are defined as being equal if they have the same printerID, productID, and lineDetail.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KitchenReceiptDetail}.
     * @return Whether or not the {@link Object} is equal to this {@link KitchenReceiptDetail}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KitchenReceiptDetail and check if the obj's printerID, kmsStationID, productID, and lineDetail
        // is equal to this KitchenReceiptDetail's printerID, kmsStationID, productID, and lineDetail
        KitchenReceiptDetail kitchenReceiptDetail = ((KitchenReceiptDetail) obj);
        return ((Objects.equals(kitchenReceiptDetail.printerID, printerID))
                && (Objects.equals(kitchenReceiptDetail.kmsStationID, kmsStationID))
                && (Objects.equals(kitchenReceiptDetail.productID, productID))
                && (Objects.equals(kitchenReceiptDetail.lineDetail, lineDetail)));
    }

    /**
     * <p>Overridden hashCode() method for a {@link KitchenReceiptDetail}.</p>
     *
     * @return The unique hash code for this {@link KitchenReceiptDetail}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(printerID, kmsStationID, productID, lineDetail);
    }

    /**
     * <p>Getter for the printerID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @return The printerID field of the {@link KitchenReceiptDetail}.
     */
    public int getPrinterID () {
        return printerID;
    }

    /**
     * <p>Setter for the printerID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @param printerID The printerID field of the {@link KitchenReceiptDetail}.
     */
    public void setPrinterID (int printerID) {
        this.printerID = printerID;
    }

    /**
     * <p>Getter for the printStatusID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @return The printStatusID field of the {@link KitchenReceiptDetail}.
     */
    public int getPrintStatusID () {
        return printStatusID;
    }

    /**
     * <p>Setter for the printStatusID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @param printStatusID The printStatusID field of the {@link KitchenReceiptDetail}.
     */
    public void setPrintStatusID (int printStatusID) {
        this.printStatusID = printStatusID;
    }

    /**
     * <p>Getter for the printerHostID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @return The printerHostID field of the {@link KitchenReceiptDetail}.
     */
    public int getPrinterHostID () {
        return printerHostID;
    }

    /**
     * <p>Setter for the printerHostID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @param printerHostID The printerHostID field of the {@link KitchenReceiptDetail}.
     */
    public void setPrinterHostID (int printerHostID) {
        this.printerHostID = printerHostID;
    }

    /**
     * <p>Getter for the productID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @return The productID field of the {@link KitchenReceiptDetail}.
     */
    public int getProductID () {
        return productID;
    }

    /**
     * <p>Setter for the productID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @param productID The productID field of the {@link KitchenReceiptDetail}.
     */
    public void setProductID (int productID) {
        this.productID = productID;
    }

    /**
     * <p>Getter for the quantity field of the {@link KitchenReceiptDetail}.</p>
     *
     * @return The quantity field of the {@link KitchenReceiptDetail}.
     */
    public double getQuantity () {
        return quantity;
    }

    /**
     * <p>Setter for the quantity field of the {@link KitchenReceiptDetail}.</p>
     *
     * @param quantity The quantity field of the {@link KitchenReceiptDetail}.
     */
    public void setQuantity (double quantity) {
        this.quantity = quantity;
    }

    /**
     * <p>Getter for the isModifier field of the {@link KitchenReceiptDetail}.</p>
     *
     * @return The isModifier field of the {@link KitchenReceiptDetail}.
     */
    public boolean getIsModifier () {
        return isModifier;
    }

    /**
     * <p>Setter for the isModifier field of the {@link KitchenReceiptDetail}.</p>
     *
     * @param isModifier The isModifier field of the {@link KitchenReceiptDetail}.
     */
    public void setIsModifier (boolean isModifier) {
        this.isModifier = isModifier;
    }

    /**
     * <p>Getter for the hiddenStation field of the {@link KitchenReceiptDetail}.</p>
     *
     * @return The hiddenStation field of the {@link KitchenReceiptDetail}.
     */
    public String getHiddenStation () {
        return hiddenStation;
    }

    /**
     * <p>Setter for the hiddenStation field of the {@link KitchenReceiptDetail}.</p>
     *
     * @param hiddenStation The hiddenStation field of the {@link KitchenReceiptDetail}.
     */
    public void setHiddenStation (String hiddenStation) {
        this.hiddenStation = hiddenStation;
    }

    /**
     * <p>Getter for the lineDetail field of the {@link KitchenReceiptDetail}.</p>
     *
     * @return The lineDetail field of the {@link KitchenReceiptDetail}.
     */
    public String getLineDetail () {
        return lineDetail;
    }

    /**
     * <p>Setter for the lineDetail field of the {@link KitchenReceiptDetail}.</p>
     *
     * @param lineDetail The lineDetail field of the {@link KitchenReceiptDetail}.
     */
    public void setLineDetail (String lineDetail) {
        this.lineDetail = lineDetail;
    }

    /**
     * <p>Getter for the kmsOrderStatusID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @return The kmsOrderStatusID field of the {@link KitchenReceiptDetail}.
     */
    public int getKMSOrderStatusID () {
        return kmsOrderStatusID;
    }

    /**
     * <p>Setter for the kmsOrderStatusID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @param kmsOrderStatusID The kmsOrderStatusID field of the {@link KitchenReceiptDetail}.
     */
    public void setKMSOrderStatusID (int kmsOrderStatusID) {
        this.kmsOrderStatusID = kmsOrderStatusID;
    }

    /**
     * <p>Getter for the kmsStationID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @return The kmsStationID field of the {@link KitchenReceiptDetail}.
     */
    public int getKMSStationID () {
        return kmsStationID;
    }

    /**
     * <p>Setter for the kmsStationID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @param kmsStationID The kmsStationID field of the {@link KitchenReceiptDetail}.
     */
    public void setKMSStationID (int kmsStationID) {
        this.kmsStationID = kmsStationID;
    }

    /**
     * <p>Getter for the transLineItemID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @return The transLineItemID field of the {@link KitchenReceiptDetail}.
     */
    public int getTransLineItemID () {
        return transLineItemID;
    }

    /**
     * <p>Setter for the transLineItemID field of the {@link KitchenReceiptDetail}.</p>
     *
     * @param transLineItemID The transLineItemID field of the {@link KitchenReceiptDetail}.
     */
    public void setTransLineItemID (int transLineItemID) {
        this.transLineItemID = transLineItemID;
    }

    /**
     * <p>Getter for the linkedFromIDs field of the {@link KitchenReceiptDetail}.</p>
     *
     * @return The linkedFromIDs field of the {@link KitchenReceiptDetail}.
     */
    public String getLinkedFromIDs () {
        return linkedFromIDs;
    }

    /**
     * <p>Setter for the linkedFromIDs field of the {@link KitchenReceiptDetail}.</p>
     *
     * @param linkedFromIDs The linkedFromIDs field of the {@link KitchenReceiptDetail}.
     */
    public void setLinkedFromIDs (String linkedFromIDs) {
        this.linkedFromIDs = linkedFromIDs;
    }
    
}