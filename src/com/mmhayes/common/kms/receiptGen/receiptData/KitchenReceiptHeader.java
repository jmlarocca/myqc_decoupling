package com.mmhayes.common.kms.receiptGen.receiptData;

/*
    Last Updated (automatically updated by SVN)
    $Author: mjbatko $: Author of last commit
    $Date: 2021-06-28 08:20:00 -0400 (Mon, 28 Jun 2021) $: Date of last commit
    $Rev: 14229 $: Revision of last commit
    Notes: Represents a header for kitchen receipts (KP/KDS/KMS).
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.receiptGen.Commands.JPOS_COMMAND;
import com.mmhayes.common.receiptGen.Formatter.kitchenReceipt.KitchenReceiptCommon;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHTimeFormatString;
import com.mmhayes.common.utils.StringFunctions;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Represents a header for kitchen receipts (KP/KDS/KMS).</p>
 *
 */
public class KitchenReceiptHeader {

    // private member variables of a KitchenReceiptHeader
    private int paTransactionID	 = -1;
    private boolean onlineOrder	= false;
    private LocalDateTime transactionDate = null;
    private LocalDateTime queueTime = null;
    private int printStatusID = -1;
    private int terminalID = -1;
    private int paOrderTypeID = -1;
    private String personName = "";
    private String phone = "";
    private String transComment = "";
    private String pickUpDeliveryNote = "";
    private String transName = "";
    private String transNameLabel = "";
    private LocalDateTime estimatedOrderTime = null;
    private String orderNum = "";
    private int transTypeID = 0;
    private String prevKDSOrderNums = "";
    private int kmsOrderStatusID = -1;
    private String printerName = "";
    private String stationName = "";
    private String deliveryLocation = "";
    private int printerID = -1;
    private int kmsStationID = -1;
    private String kmsStationName = "";
    private int printerHostID = -1;
    private String cashierName = "";
    private boolean useAddOn = false;
    private IRenderer renderer = null;
    private DataManager dataManager = null;
    private String log = "";
    private int maxReceiptCharsPerLineNormal = 32;
    private int maxReceiptCharsPerLineLarge = 16;

    /**
     * <p>Constructor for a {@link KitchenReceiptHeader}.</p>
     *
     */
    public KitchenReceiptHeader () {}

    /**
     * <p>Sets the paTransactionID	field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param paTransactionID The ID of the transaction.
     * @return The {@link KitchenReceiptHeader} instance with it's paTransactionID field set.
     */
    public KitchenReceiptHeader addPATransactionID (int paTransactionID) {
        this.paTransactionID = paTransactionID;
        return this;
    }

    /**
     * <p>Sets the onlineOrder field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param onlineOrder Whether or not the transaction is an online order.
     * @return The {@link KitchenReceiptHeader} instance with it's onlineOrder field set.
     */
    public KitchenReceiptHeader addOnlineOrder (boolean onlineOrder) {
        this.onlineOrder = onlineOrder;
        return this;
    }

    /**
     * <p>Sets the transactionDate field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param transactionDate The {@link LocalDateTime} of when transaction will be completed by the kitchen.
     * @return The {@link KitchenReceiptHeader} instance with it's transactionDate field set.
     */
    public KitchenReceiptHeader addTransactionDate (LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    /**
     * <p>Sets the queueTime field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param queueTime The {@link LocalDateTime} the transaction is added to the print queue.
     * @return The {@link KitchenReceiptHeader} instance with it's queueTime field set.
     */
    public KitchenReceiptHeader addQueueTime (LocalDateTime queueTime) {
        this.queueTime = queueTime;
        return this;
    }

    /**
     * <p>Sets the printStatusID field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param printStatusID The kitchen print status of the transaction.
     * @return The {@link KitchenReceiptHeader} instance with it's printStatusID field set.
     */
    public KitchenReceiptHeader addPrintStatusID (int printStatusID) {
        this.printStatusID = printStatusID;
        return this;
    }

    /**
     * <p>Sets the terminalID field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param terminalID ID of the terminal the transaction was made on.
     * @return The {@link KitchenReceiptHeader} instance with it's terminalID field set.
     */
    public KitchenReceiptHeader addTerminalID (int terminalID) {
        this.terminalID = terminalID;
        return this;
    }

    /**
     * <p>Sets the paOrderTypeID field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param paOrderTypeID The type of order.
     * @return The {@link KitchenReceiptHeader} instance with it's paOrderTypeID field set.
     */
    public KitchenReceiptHeader addPAOrderTypeID (int paOrderTypeID) {
        this.paOrderTypeID = paOrderTypeID;
        return this;
    }

    /**
     * <p>Sets the personName field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param personName Name {@link String} of the person who placed the order.
     * @return The {@link KitchenReceiptHeader} instance with it's personName field set.
     */
    public KitchenReceiptHeader addPersonName (String personName) {
        this.personName = personName;
        return this;
    }

    /**
     * <p>Sets the phone field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param phone Phone number {@link String} of the person who placed the order.
     * @return The {@link KitchenReceiptHeader} instance with it's phone field set.
     */
    public KitchenReceiptHeader addPhone (String phone) {
        this.phone = phone;
        return this;
    }

    /**
     * <p>Sets the transComment field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param transComment Any comment realted to the transaction as a {@link String}.
     * @return The {@link KitchenReceiptHeader} instance with it's transComment field set.
     */
    public KitchenReceiptHeader addTransComment (String transComment) {
        this.transComment = transComment;
        return this;
    }

    /**
     * <p>Sets the pickUpDeliveryNote field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param pickUpDeliveryNote The pickup delivery note {@link String} for the order.
     * @return The {@link KitchenReceiptHeader} instance with it's pickUpDeliveryNote field set.
     */
    public KitchenReceiptHeader addPickUpDeliveryNote (String pickUpDeliveryNote) {
        this.pickUpDeliveryNote = pickUpDeliveryNote;
        return this;
    }

    /**
     * <p>Sets the transName field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param transName The name {@link String} of the transaction.
     * @return The {@link KitchenReceiptHeader} instance with it's transName field set.
     */
    public KitchenReceiptHeader addTransName (String transName) {
        this.transName = transName;
        return this;
    }

    /**
     * <p>Sets the transNameLabel field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param transNameLabel The label {@link String} for the transaction.
     * @return The {@link KitchenReceiptHeader} instance with it's transNameLabel field set.
     */
    public KitchenReceiptHeader addTransNameLabel (String transNameLabel) {
        this.transNameLabel = transNameLabel;
        return this;
    }

    /**
     * <p>Sets the estimatedOrderTime field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param estimatedOrderTime The {@link LocalDateTime} of when it's estimated that the order was placed.
     * @return The {@link KitchenReceiptHeader} instance with it's estimatedOrderTime field set.
     */
    public KitchenReceiptHeader addEstimatedOrderTime (LocalDateTime estimatedOrderTime) {
        this.estimatedOrderTime = estimatedOrderTime;
        return this;
    }

    /**
     * <p>Sets the orderNum field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param orderNum The order number {@link String}.
     * @return The {@link KitchenReceiptHeader} instance with it's orderNum field set.
     */
    public KitchenReceiptHeader addOrderNum (String orderNum) {
        this.orderNum = orderNum;
        return this;
    }

    /**
     * <p>Sets the transTypeID field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param transTypeID The type of transaction.
     * @return The {@link KitchenReceiptHeader} instance with it's transTypeID field set.
     */
    public KitchenReceiptHeader addTransTypeID (int transTypeID) {
        this.transTypeID = transTypeID;
        return this;
    }

    /**
     * <p>Sets the prevKDSOrderNums field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param prevKDSOrderNums Any order number {@link String} for transactions that were left open and are continued by this current transaction.
     * @return The {@link KitchenReceiptHeader} instance with it's prevKDSOrderNums field set.
     */
    public KitchenReceiptHeader addPrevKDSOrderNums (String prevKDSOrderNums) {
        this.prevKDSOrderNums = prevKDSOrderNums;
        return this;
    }

    /**
     * <p>Sets the kmsOrderStatusID field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param kmsOrderStatusID Order status of the transaction for KMS.
     * @return The {@link KitchenReceiptHeader} instance with it's kmsOrderStatusID field set.
     */
    public KitchenReceiptHeader addKMSOrderStatusID (int kmsOrderStatusID) {
        this.kmsOrderStatusID = kmsOrderStatusID;
        return this;
    }

    /**
     * <p>Sets the printerName field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param printerName The name {@link String} of the printer.
     * @return The {@link KitchenReceiptHeader} instance with it's printerName field set.
     */
    public KitchenReceiptHeader addPrinterName (String printerName) {
        this.printerName = printerName;
        return this;
    }

    /**
     * <p>Sets the stationName field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param stationName The name {@link String} of the station.
     * @return The {@link KitchenReceiptHeader} instance with it's stationName field set.
     */
    public KitchenReceiptHeader addStationName (String stationName) {
        this.stationName = stationName;
        return this;
    }

    /**
     * <p>Sets the deliveryLocation field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param deliveryLocation The address {@link String} where the order should be delivered to.
     * @return The {@link KitchenReceiptHeader} instance with it's deliveryLocation field set.
     */
    public KitchenReceiptHeader addDeliveryLocation (String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
        return this;
    }

    /**
     * <p>Sets the printerID field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param printerID ID of the printer the receipt header will appear on.
     * @return The {@link KitchenReceiptHeader} instance with it's printerID field set.
     */
    public KitchenReceiptHeader addPrinterID (int printerID) {
        this.printerID = printerID;
        return this;
    }

    /**
     * <p>Sets the kmsStationID field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param kmsStationID ID of the KMS station the receipt header will appear on.
     * @return The {@link KitchenReceiptHeader} instance with it's kmsStationID field set.
     */
    public KitchenReceiptHeader addKMSStationID (int kmsStationID) {
        this.kmsStationID = kmsStationID;
        return this;
    }

    /**
     * <p>Sets the kmsStationName field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param kmsStationName The name {@link String} of the KMS station the receipt header will appear on.
     * @return The {@link KitchenReceiptHeader} instance with it's kmsStationName field set.
     */
    public KitchenReceiptHeader addKMSStationName (String kmsStationName) {
        this.kmsStationName = kmsStationName;
        return this;
    }

    /**
     * <p>Sets the printerHostID field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param printerHostID ID of the printer host the receipt header will appear on.
     * @return The {@link KitchenReceiptHeader} instance with it's printerHostID field set.
     */
    public KitchenReceiptHeader addPrinterHostID (int printerHostID) {
        this.printerHostID = printerHostID;
        return this;
    }

    /**
     * <p>Sets the cashierName field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param cashierName The name {@link String} of the KMS station the receipt header will appear on.
     * @return The {@link KitchenReceiptHeader} instance with it's cashierName field set.
     */
    public KitchenReceiptHeader addCashierName (String cashierName) {
        this.cashierName = cashierName;
        return this;
    }

    /**
     * <p>Sets the useAddOn field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param useAddOn Whether or not to add ADD ON to the receipt header.
     * @return The {@link KitchenReceiptHeader} instance with it's useAddOn field set.
     */
    public KitchenReceiptHeader addUseAddOn (boolean useAddOn) {
        this.useAddOn = useAddOn;
        return this;
    }

    /**
     * <p>Sets the renderer field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param renderer The {@link IRenderer} instance to use to render the receipt.
     * @return The {@link KitchenReceiptHeader} instance with it's renderer field set.
     */
    public KitchenReceiptHeader addRenderer (IRenderer renderer) {
        this.renderer = renderer;
        return this;
    }

    /**
     * <p>Sets the dataManager field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @return The {@link KitchenReceiptHeader} instance with it's dataManager field set.
     */
    public KitchenReceiptHeader addDataManager (DataManager dataManager) {
        this.dataManager = dataManager;
        return this;
    }

    /**
     * <p>Sets the log field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param log The file path {@link String} of the log file to loag any information related to this class.
     * @return The {@link KitchenReceiptHeader} instance with it's log field set.
     */
    public KitchenReceiptHeader addLog (String log) {
        this.log = log;
        return this;
    }

    /**
     * <p>Sets the maxReceiptCharsPerLineNormal field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param maxReceiptCharsPerLineNormal The number of characters that can fit on a receipt line using the normal font size.
     * @return The {@link KitchenReceiptHeader} instance with it's maxReceiptCharsPerLineNormal field set.
     */
    public KitchenReceiptHeader addMaxReceiptCharsPerLineNormal (int maxReceiptCharsPerLineNormal) {
        this.maxReceiptCharsPerLineNormal = maxReceiptCharsPerLineNormal;
        return this;
    }

    /**
     * <p>Sets the maxReceiptCharsPerLineLarge field for this {@link KitchenReceiptHeader} instance.</p>
     *
     * @param maxReceiptCharsPerLineLarge The number of characters that can fit on a receipt line using the large font size.
     * @return The {@link KitchenReceiptHeader} instance with it's maxReceiptCharsPerLineLarge field set.
     */
    public KitchenReceiptHeader addMaxReceiptCharsPerLineLarge (int maxReceiptCharsPerLineLarge) {
        this.maxReceiptCharsPerLineLarge = maxReceiptCharsPerLineLarge;
        return this;
    }


    /**
     * <p>Builds the details of the receipt header.</p>
     *
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print job details of the receipt header.
     */
    public ArrayList<HashMap> build () {
        ArrayList<HashMap> receiptHeaderDetails = new ArrayList<>();

        KitchenReceiptDetail kitchenReceiptDetail;
        // add the transaction date and time
        if (transactionDate != null) {
            DateTimeFormatter transactionDateFmtr = DateTimeFormatter.ofPattern(MMHTimeFormatString.MO_DY_YR);
            String transactionDateStr = transactionDateFmtr.format(transactionDate);
            DateTimeFormatter transactionTimeFmtr = DateTimeFormatter.ofPattern(MMHTimeFormatString.HR_MIN_SEC_AMPM);
            String transactionTimeStr = transactionTimeFmtr.format(transactionDate);
            String transactionDateReceiptLine = KitchenReceiptCommon.twoColumn(maxReceiptCharsPerLineNormal, transactionDateStr, transactionTimeStr);
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(transactionDateReceiptLine)
                    .addRenderer(renderer);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }

        // add the cashier name and terminal ID
        if ((StringFunctions.stringHasContent(cashierName)) && (terminalID > 0)) {
            String cashierTerminalReceiptLine = KitchenReceiptCommon.twoColumn(maxReceiptCharsPerLineNormal, "Cashier: " + cashierName, "TID: " + String.valueOf(terminalID));
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(cashierTerminalReceiptLine)
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }

        // add the transaction type and transaction ID
        if ((transTypeID > 0) && (paTransactionID > 0)) {
            String transactionTypeIDReceiptLine = TypeData.getTransactionTypeName(transTypeID) + ": " + String.valueOf(paTransactionID);
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(transactionTypeIDReceiptLine)
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }
        // add an empty line
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
                        .addPrinterID(printerID)
                        .addKMSStationID(kmsStationID)
                        .addPrintStatusID(printStatusID)
                        .addKMSOrderStatusID(kmsOrderStatusID)
                        .addPrinterHostID(printerHostID)
                        .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
                        .addRenderer(renderer)
                        .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
        receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));

        // alert the kitchen of any refunded or voided orders
        if (transTypeID != TypeData.TranType.SALE) {
            String msg = "";
            if (transTypeID == TypeData.TranType.VOID) {
                msg = "Order VOID";
            }
            else if (transTypeID == TypeData.TranType.REFUND) {
                msg = "Order REFUND";
            }
            if (StringFunctions.stringHasContent(msg)) {
                kitchenReceiptDetail =
                        new KitchenReceiptDetail()
                        .addPrinterID(printerID)
                        .addKMSStationID(kmsStationID)
                        .addPrintStatusID(printStatusID)
                        .addKMSOrderStatusID(kmsOrderStatusID)
                        .addPrinterHostID(printerHostID)
                        .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + msg)
                        .addRenderer(renderer)
                        .addFixWordWrap(true, maxReceiptCharsPerLineLarge);
                receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
            }
        }

        // add the printer / station name
        if (StringFunctions.stringHasContent(printerName)) {
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + printerName)
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }
        else if (StringFunctions.stringHasContent(stationName)) {
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + stationName)
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }

        // add the order number
        if (StringFunctions.stringHasContent(orderNum)) {
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.LARGE_FONT + orderNum)
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineLarge);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }

        // add the ADD ON header
        if ((!isExpeditorPrinter()) && (!isExpeditorKMSStation()) && (!isRemoteOrderPrinter()) && (!isRemoteOrderKMSStation()) && (useAddOn)) {
            // add an empty line
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
            // add the ADD ON header
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.LARGE_FONT + "**** ADD ON ****")
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineLarge);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
            // add an empty line
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }

        // add an empty line
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
                .addPrinterID(printerID)
                .addKMSStationID(kmsStationID)
                .addPrintStatusID(printStatusID)
                .addKMSOrderStatusID(kmsOrderStatusID)
                .addPrinterHostID(printerHostID)
                .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
                .addRenderer(renderer)
                .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
        receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));

        // add the pickup or delivery note
        if ((paOrderTypeID != TypeData.OrderType.NORMAL_SALE) && (StringFunctions.stringHasContent(pickUpDeliveryNote))) {
            // add the actual note
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                            .addPrinterID(printerID)
                            .addKMSStationID(kmsStationID)
                            .addPrintStatusID(printStatusID)
                            .addKMSOrderStatusID(kmsOrderStatusID)
                            .addPrinterHostID(printerHostID)
                            .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.LARGE_FONT + pickUpDeliveryNote)
                            .addRenderer(renderer)
                            .addFixWordWrap(true, maxReceiptCharsPerLineLarge);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
            // add an empty line
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }

        // add the person name if it isn't the same as the transaction name
        if ((StringFunctions.stringHasContent(personName)) && ((!StringFunctions.stringHasContent(transName)) || (!personName.equalsIgnoreCase(transName)))) {
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail("Customer: "+personName)
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }

        // add the delivery location
        if (StringFunctions.stringHasContent(deliveryLocation)) {
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(deliveryLocation)
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }

        // add the transaction name
        if (StringFunctions.stringHasContent(transName)) {
            String label = (StringFunctions.stringHasContent(transNameLabel) ? transNameLabel + ": " : "");
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail(label + transName)
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }

        // add the transaction comment
        if (StringFunctions.stringHasContent(transComment)) {
            kitchenReceiptDetail =
                    new KitchenReceiptDetail()
                    .addPrinterID(printerID)
                    .addKMSStationID(kmsStationID)
                    .addPrintStatusID(printStatusID)
                    .addKMSOrderStatusID(kmsOrderStatusID)
                    .addPrinterHostID(printerHostID)
                    .addLineDetail("Comment: "+transComment)
                    .addRenderer(renderer)
                    .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
            receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));
        }

        // add an empty line
        kitchenReceiptDetail =
                new KitchenReceiptDetail()
                .addPrinterID(printerID)
                .addKMSStationID(kmsStationID)
                .addPrintStatusID(printStatusID)
                .addKMSOrderStatusID(kmsOrderStatusID)
                .addPrinterHostID(printerHostID)
                .addLineDetail(JPOS_COMMAND.ALIGN_CENTER + " ")
                .addRenderer(renderer)
                .addFixWordWrap(true, maxReceiptCharsPerLineNormal);
        receiptHeaderDetails.addAll(DataFunctions.purgeAlOfHm(KitchenReceiptDetail.getKitchenReceiptDetailsAsHMs(kitchenReceiptDetail.build())));

        return receiptHeaderDetails;
    }

    /**
     * <p>Queries the database to check whether or not we are creating a receipt header for an expeditor printer.</p>
     *
     * @return Whether or not we are creating a receipt header for an expeditor printer.
     */
    private boolean isExpeditorPrinter () {

        try {
            if (printerID <= 0) {
                Logger.logMessage("The printer ID in KitchenReceiptHeader.isExpeditorPrinter must be greater than 0!", log, Logger.LEVEL.ERROR);
                return false;
            }

            if (dataManager == null) {
                Logger.logMessage("The DataManager in KitchenReceiptHeader.isExpeditorPrinter can't be null!", log, Logger.LEVEL.ERROR);
                return false;
            }

            // query the database
            DynamicSQL sql =
                    new DynamicSQL("data.kms.GetIsExpeditorPrinter")
                    .addIDList(1, printerID);
            return sql.getSingleBooleanField(dataManager);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("Unable to determine whether or not the printer with an ID of %s is an expeditor printer in KitchenReceiptHeader.isExpeditorPrinter!",
                    Objects.toString(printerID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return false;
    }

    /**
     * <p>Queries the database to check whether or not we are creating a receipt header for an expeditor kms station.</p>
     *
     * @return Whether or not we are creating a receipt header for an expeditor kms station.
     */
    private boolean isExpeditorKMSStation () {

        try {
            if (kmsStationID <= 0) {
                Logger.logMessage("The kmsStation ID in KitchenReceiptHeader.isExpeditorKMSStation must be greater than 0!", log, Logger.LEVEL.ERROR);
                return false;
            }

            if (dataManager == null) {
                Logger.logMessage("The DataManager in KitchenReceiptHeader.isExpeditorKMSStation can't be null!", log, Logger.LEVEL.ERROR);
                return false;
            }

            // query the database
            DynamicSQL sql =
                    new DynamicSQL("data.kms.GetIsExpeditorKMSStation")
                            .addIDList(1, kmsStationID);
            return sql.getSingleBooleanField(dataManager);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("Unable to determine whether or not the kms station with an ID of %s is an expeditor kmsStation in KitchenReceiptHeader.isExpeditorKMSStation!",
                    Objects.toString(kmsStationID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return false;
    }

    /**
     * <p>Queries the database to check whether or not we are creating a receipt header for a remote order printer.</p>
     *
     * @return Whether or not we are creating a receipt header for a remote order printer.
     */
    private boolean isRemoteOrderPrinter () {

        try {
            if (printerID <= 0) {
                Logger.logMessage("The printer ID in KitchenReceiptHeader.isRemoteOrderPrinter must be greater than 0!", log, Logger.LEVEL.ERROR);
                return false;
            }

            if (dataManager == null) {
                Logger.logMessage("The DataManager in KitchenReceiptHeader.isRemoteOrderPrinter can't be null!", log, Logger.LEVEL.ERROR);
                return false;
            }

            // query the database
            DynamicSQL sql =
                    new DynamicSQL("data.kms.GetIsRemoteOrderPrinter")
                    .addIDList(1, printerID);
            return sql.getSingleBooleanField(dataManager);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("Unable to determine whether or not the printer with an ID of %s is a remote order printer in KitchenReceiptHeader.isRemoteOrderPrinter!",
                    Objects.toString(printerID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return false;
    }

    /**
     * <p>Queries the database to check whether or not we are creating a receipt header for a remote order kms station.</p>
     *
     * @return Whether or not we are creating a receipt header for a remote order kms station.
     */
    private boolean isRemoteOrderKMSStation () {

        try {
            if (kmsStationID <= 0) {
                Logger.logMessage("The kms station ID in KitchenReceiptHeader.isRemoteOrderKMSStation must be greater than 0!", log, Logger.LEVEL.ERROR);
                return false;
            }

            if (dataManager == null) {
                Logger.logMessage("The DataManager in KitchenReceiptHeader.isRemoteOrderKMSStation can't be null!", log, Logger.LEVEL.ERROR);
                return false;
            }

            // query the database
            DynamicSQL sql =
                    new DynamicSQL("data.kms.GetIsRemoteOrderKMSStation")
                            .addIDList(1, kmsStationID);
            return sql.getSingleBooleanField(dataManager);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("Unable to determine whether or not the kms station with an ID of %s is a remote order kmsStation in KitchenReceiptHeader.isRemoteOrderKMSStation!",
                    Objects.toString(kmsStationID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return false;
    }

    /**
     * <p>Overridden toString() method for a {@link KitchenReceiptHeader}.</p>
     *
     * @return A {@link String} representation of this {@link KitchenReceiptHeader}.
     */
    @Override
    public String toString () {

        return String.format("PATRANSACTIONID: %s, ONLINEORDER: %s, TRANSACTIONDATE: %s, QUEUETIME: %s, PRINTSTATUSID: %s, " +
                "TERMINALID: %s, PAORDERTYPEID: %s, PERSONNAME: %s, PHONE: %s, TRANSCOMMENT: %s, PICKUPDELIVERYNOTE: %s, " +
                "TRANSNAME: %s, TRANSNAMELABEL: %s, ESTIMATEDORDERTIME: %s, ORDERNUM: %s, TRANSTYPEID: %s, PREVKDSORDERNUMS: %s, " +
                "KMSORDERSTATUSID: %s, PRINTERNAME: %s, DELIVERYLOCATION: %s, PRINTERID: %s, KMSSTATIONID: %s, KMSSTATIONNAME: %s, " +
                "PRINTERHOSTID: %s, CASHIERNAME: %s, USEADDON: %s",
                Objects.toString((paTransactionID > 0 ? paTransactionID : "N/A"), "N/A"),
                Objects.toString(onlineOrder, "N/A"),
                Objects.toString((transactionDate != null ? DateTimeFormatter.ofPattern(MMHTimeFormatString.YR_MO_DY_HR_MIN_SEC_MS).format(transactionDate) : "N/A"), "N/A"),
                Objects.toString((queueTime != null ? DateTimeFormatter.ofPattern(MMHTimeFormatString.YR_MO_DY_HR_MIN_SEC_MS).format(queueTime) : "N/A"), "N/A"),
                Objects.toString((printStatusID > 0 ? printStatusID : "N/A"), "N/A"),
                Objects.toString((terminalID > 0 ? terminalID : "N/A"), "N/A"),
                Objects.toString((paOrderTypeID > 0 ? paOrderTypeID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(personName) ? personName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(phone) ? phone : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transComment) ? transComment : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(pickUpDeliveryNote) ? pickUpDeliveryNote : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transName) ? transName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(transNameLabel) ? transNameLabel : "N/A"), "N/A"),
                Objects.toString((estimatedOrderTime != null ? DateTimeFormatter.ofPattern(MMHTimeFormatString.YR_MO_DY_HR_MIN_SEC_MS).format(estimatedOrderTime) : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(orderNum) ? orderNum : "N/A"), "N/A"),
                Objects.toString((transTypeID > 0 ? orderNum : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(prevKDSOrderNums) ? prevKDSOrderNums : "N/A"), "N/A"),
                Objects.toString((kmsOrderStatusID > 0 ? prevKDSOrderNums : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(printerName) ? printerName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(deliveryLocation) ? deliveryLocation : "N/A"), "N/A"),
                Objects.toString((printerID > 0 ? printerID : "N/A"), "N/A"),
                Objects.toString((kmsStationID > 0 ? kmsStationID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(kmsStationName) ? kmsStationName : "N/A"), "N/A"),
                Objects.toString((printerHostID > 0 ? printerHostID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(cashierName) ? cashierName : "N/A"), "N/A"),
                Objects.toString(useAddOn, "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link KitchenReceiptHeader}.
     * Two {@link KitchenReceiptHeader} are defined as being equal if they have the same paTransactionID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KitchenReceiptHeader}.
     * @return Whether or not the {@link Object} is equal to this {@link KitchenReceiptHeader}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KitchenReceiptHeader and check if the obj's paTransactionID is equal to this KitchenReceiptHeader's paTransactionID
        KitchenReceiptHeader kitchenReceiptHeader = ((KitchenReceiptHeader) obj);
        return (Objects.equals(kitchenReceiptHeader.paTransactionID, paTransactionID));
    }

    /**
     * <p>Overridden hashCode() method for a {@link KitchenReceiptHeader}.</p>
     *
     * @return The unique hash code for this {@link KitchenReceiptHeader}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(paTransactionID);
    }

    /**
     * <p>Getter for the paTransactionID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The paTransactionID field of the {@link KitchenReceiptHeader}.
     */
    public int getPATransactionID () {
        return paTransactionID;
    }

    /**
     * <p>Setter for the paTransactionID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param paTransactionID The paTransactionID field of the {@link KitchenReceiptHeader}.
     */
    public void setPATransactionID (int paTransactionID) {
        this.paTransactionID = paTransactionID;
    }

    /**
     * <p>Getter for the onlineOrder field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The onlineOrder field of the {@link KitchenReceiptHeader}.
     */
    public boolean getOnlineOrder () {
        return onlineOrder;
    }

    /**
     * <p>Setter for the onlineOrder field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param onlineOrder The onlineOrder field of the {@link KitchenReceiptHeader}.
     */
    public void setOnlineOrder (boolean onlineOrder) {
        this.onlineOrder = onlineOrder;
    }

    /**
     * <p>Getter for the transactionDate field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The transactionDate field of the {@link KitchenReceiptHeader}.
     */
    public LocalDateTime getTransactionDate () {
        return transactionDate;
    }

    /**
     * <p>Setter for the transactionDate field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param transactionDate The transactionDate field of the {@link KitchenReceiptHeader}.
     */
    public void setTransactionDate (LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * <p>Getter for the queueTime field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The queueTime field of the {@link KitchenReceiptHeader}.
     */
    public LocalDateTime getQueueTime () {
        return queueTime;
    }

    /**
     * <p>Setter for the queueTime field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param queueTime The queueTime field of the {@link KitchenReceiptHeader}.
     */
    public void setQueueTime (LocalDateTime queueTime) {
        this.queueTime = queueTime;
    }

    /**
     * <p>Getter for the printStatusID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The printStatusID field of the {@link KitchenReceiptHeader}.
     */
    public int getPrintStatusID () {
        return printStatusID;
    }

    /**
     * <p>Setter for the printStatusID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param printStatusID The printStatusID field of the {@link KitchenReceiptHeader}.
     */
    public void setPrintStatusID (int printStatusID) {
        this.printStatusID = printStatusID;
    }

    /**
     * <p>Getter for the terminalID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The terminalID field of the {@link KitchenReceiptHeader}.
     */
    public int getTerminalID () {
        return terminalID;
    }

    /**
     * <p>Setter for the terminalID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param terminalID The terminalID field of the {@link KitchenReceiptHeader}.
     */
    public void setTerminalID (int terminalID) {
        this.terminalID = terminalID;
    }

    /**
     * <p>Getter for the paOrderTypeID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The paOrderTypeID field of the {@link KitchenReceiptHeader}.
     */
    public int getPAOrderTypeID () {
        return paOrderTypeID;
    }

    /**
     * <p>Setter for the paOrderTypeID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param paOrderTypeID The paOrderTypeID field of the {@link KitchenReceiptHeader}.
     */
    public void setPAOrderTypeID (int paOrderTypeID) {
        this.paOrderTypeID = paOrderTypeID;
    }

    /**
     * <p>Getter for the personName field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The personName field of the {@link KitchenReceiptHeader}.
     */
    public String getPersonName () {
        return personName;
    }

    /**
     * <p>Setter for the personName field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param personName The personName field of the {@link KitchenReceiptHeader}.
     */
    public void setPersonName (String personName) {
        this.personName = personName;
    }

    /**
     * <p>Getter for the phone field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The phone field of the {@link KitchenReceiptHeader}.
     */
    public String getPhone () {
        return phone;
    }

    /**
     * <p>Setter for the phone field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param phone The phone field of the {@link KitchenReceiptHeader}.
     */
    public void setPhone (String phone) {
        this.phone = phone;
    }

    /**
     * <p>Getter for the transComment field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The transComment field of the {@link KitchenReceiptHeader}.
     */
    public String getTransComment () {
        return transComment;
    }

    /**
     * <p>Setter for the transComment field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param transComment The transComment field of the {@link KitchenReceiptHeader}.
     */
    public void setTransComment (String transComment) {
        this.transComment = transComment;
    }

    /**
     * <p>Getter for the pickUpDeliveryNote field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The pickUpDeliveryNote field of the {@link KitchenReceiptHeader}.
     */
    public String getPickUpDeliveryNote () {
        return pickUpDeliveryNote;
    }

    /**
     * <p>Setter for the pickUpDeliveryNote field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param pickUpDeliveryNote The pickUpDeliveryNote field of the {@link KitchenReceiptHeader}.
     */
    public void setPickUpDeliveryNote (String pickUpDeliveryNote) {
        this.pickUpDeliveryNote = pickUpDeliveryNote;
    }

    /**
     * <p>Getter for the transName field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The transName field of the {@link KitchenReceiptHeader}.
     */
    public String getTransName () {
        return transName;
    }

    /**
     * <p>Setter for the transName field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param transName The transName field of the {@link KitchenReceiptHeader}.
     */
    public void setTransName (String transName) {
        this.transName = transName;
    }

    /**
     * <p>Getter for the transNameLabel field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The transNameLabel field of the {@link KitchenReceiptHeader}.
     */
    public String getTransNameLabel () {
        return transNameLabel;
    }

    /**
     * <p>Setter for the transNameLabel field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param transNameLabel The transNameLabel field of the {@link KitchenReceiptHeader}.
     */
    public void setTransNameLabel (String transNameLabel) {
        this.transNameLabel = transNameLabel;
    }

    /**
     * <p>Getter for the estimatedOrderTime field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The estimatedOrderTime field of the {@link KitchenReceiptHeader}.
     */
    public LocalDateTime getEstimatedOrderTime () {
        return estimatedOrderTime;
    }

    /**
     * <p>Setter for the estimatedOrderTime field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param estimatedOrderTime The estimatedOrderTime field of the {@link KitchenReceiptHeader}.
     */
    public void setEstimatedOrderTime (LocalDateTime estimatedOrderTime) {
        this.estimatedOrderTime = estimatedOrderTime;
    }

    /**
     * <p>Getter for the orderNum field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The orderNum field of the {@link KitchenReceiptHeader}.
     */
    public String getOrderNum () {
        return orderNum;
    }

    /**
     * <p>Setter for the orderNum field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param orderNum The orderNum field of the {@link KitchenReceiptHeader}.
     */
    public void setOrderNum (String orderNum) {
        this.orderNum = orderNum;
    }

    /**
     * <p>Getter for the transTypeID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The transTypeID field of the {@link KitchenReceiptHeader}.
     */
    public int getTransTypeID () {
        return transTypeID;
    }

    /**
     * <p>Setter for the transTypeID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param transTypeID The transTypeID field of the {@link KitchenReceiptHeader}.
     */
    public void setTransTypeID (int transTypeID) {
        this.transTypeID = transTypeID;
    }

    /**
     * <p>Getter for the prevKDSOrderNums field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The prevKDSOrderNums field of the {@link KitchenReceiptHeader}.
     */
    public String getPrevKDSOrderNums () {
        return prevKDSOrderNums;
    }

    /**
     * <p>Setter for the prevKDSOrderNums field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param prevKDSOrderNums The prevKDSOrderNums field of the {@link KitchenReceiptHeader}.
     */
    public void setPrevKDSOrderNums (String prevKDSOrderNums) {
        this.prevKDSOrderNums = prevKDSOrderNums;
    }

    /**
     * <p>Getter for the kmsOrderStatusID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The kmsOrderStatusID field of the {@link KitchenReceiptHeader}.
     */
    public int getKMSOrderStatusID () {
        return kmsOrderStatusID;
    }

    /**
     * <p>Setter for the kmsOrderStatusID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param kmsOrderStatusID The kmsOrderStatusID field of the {@link KitchenReceiptHeader}.
     */
    public void setKMSOrderStatusID (int kmsOrderStatusID) {
        this.kmsOrderStatusID = kmsOrderStatusID;
    }

    /**
     * <p>Getter for the printerName field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The printerName field of the {@link KitchenReceiptHeader}.
     */
    public String getPrinterName () {
        return printerName;
    }

    /**
     * <p>Setter for the printerName field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param printerName The printerName field of the {@link KitchenReceiptHeader}.
     */
    public void setPrinterName (String printerName) {
        this.printerName = printerName;
    }

    /**
     * <p>Getter for the deliveryLocation field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The deliveryLocation field of the {@link KitchenReceiptHeader}.
     */
    public String getDeliveryLocation () {
        return deliveryLocation;
    }

    /**
     * <p>Setter for the deliveryLocation field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param deliveryLocation The deliveryLocation field of the {@link KitchenReceiptHeader}.
     */
    public void setDeliveryLocation (String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    /**
     * <p>Getter for the printerID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The printerID field of the {@link KitchenReceiptHeader}.
     */
    public int getPrinterID () {
        return printerID;
    }

    /**
     * <p>Setter for the printerID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param printerID The printerID field of the {@link KitchenReceiptHeader}.
     */
    public void setPrinterID (int printerID) {
        this.printerID = printerID;
    }

    /**
     * <p>Getter for the kmsStationID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The kmsStationID field of the {@link KitchenReceiptHeader}.
     */
    public int getKMSStationID () {
        return kmsStationID;
    }

    /**
     * <p>Setter for the kmsStationID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param kmsStationID The kmsStationID field of the {@link KitchenReceiptHeader}.
     */
    public void setKMSStationID (int kmsStationID) {
        this.kmsStationID = kmsStationID;
    }

    /**
     * <p>Getter for the kmsStationName field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The kmsStationName field of the {@link KitchenReceiptHeader}.
     */
    public String getKMSStationName () {
        return kmsStationName;
    }

    /**
     * <p>Setter for the kmsStationName field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param kmsStationName The kmsStationName field of the {@link KitchenReceiptHeader}.
     */
    public void setKMSStationName (String kmsStationName) {
        this.kmsStationName = kmsStationName;
    }

    /**
     * <p>Getter for the printerHostID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The printerHostID field of the {@link KitchenReceiptHeader}.
     */
    public int getPrinterHostID () {
        return printerHostID;
    }

    /**
     * <p>Setter for the printerHostID field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param printerHostID The printerHostID field of the {@link KitchenReceiptHeader}.
     */
    public void setPrinterHostID (int printerHostID) {
        this.printerHostID = printerHostID;
    }

    /**
     * <p>Getter for the cashierName field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The cashierName field of the {@link KitchenReceiptHeader}.
     */
    public String getCashierName () {
        return cashierName;
    }

    /**
     * <p>Setter for the cashierName field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param cashierName The cashierName field of the {@link KitchenReceiptHeader}.
     */
    public void setCashierName (String cashierName) {
        this.cashierName = cashierName;
    }

    /**
     * <p>Getter for the useAddOn field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The useAddOn field of the {@link KitchenReceiptHeader}.
     */
    public boolean getUseAddOn () {
        return useAddOn;
    }

    /**
     * <p>Setter for the useAddOn field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param useAddOn The useAddOn field of the {@link KitchenReceiptHeader}.
     */
    public void setUseAddOn (boolean useAddOn) {
        this.useAddOn = useAddOn;
    }

    /**
     * <p>Getter for the renderer field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The renderer field of the {@link KitchenReceiptHeader}.
     */
    public IRenderer getRenderer () {
        return renderer;
    }

    /**
     * <p>Setter for the renderer field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param renderer The renderer field of the {@link KitchenReceiptHeader}.
     */
    public void setRenderer (IRenderer renderer) {
        this.renderer = renderer;
    }

    /**
     * <p>Getter for the dataManager field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The dataManager field of the {@link KitchenReceiptHeader}.
     */
    public DataManager getDataManager () {
        return dataManager;
    }

    /**
     * <p>Setter for the dataManager field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param dataManager The dataManager field of the {@link KitchenReceiptHeader}.
     */
    public void setDataManager (DataManager dataManager) {
        this.dataManager = dataManager;
    }

    /**
     * <p>Getter for the log field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The log field of the {@link KitchenReceiptHeader}.
     */
    public String getLog () {
        return log;
    }

    /**
     * <p>Setter for the log field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param log The log field of the {@link KitchenReceiptHeader}.
     */
    public void setLog (String log) {
        this.log = log;
    }

    /**
     * <p>Getter for the maxReceiptCharsPerLineNormal field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The maxReceiptCharsPerLineNormal field of the {@link KitchenReceiptHeader}.
     */
    public int getMaxReceiptCharsPerLineNormal () {
        return maxReceiptCharsPerLineNormal;
    }

    /**
     * <p>Setter for the maxReceiptCharsPerLineNormal field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param maxReceiptCharsPerLineNormal The maxReceiptCharsPerLineNormal field of the {@link KitchenReceiptHeader}.
     */
    public void setMaxReceiptCharsPerLineNormal (int maxReceiptCharsPerLineNormal) {
        this.maxReceiptCharsPerLineNormal = maxReceiptCharsPerLineNormal;
    }

    /**
     * <p>Getter for the maxReceiptCharsPerLineLarge field of the {@link KitchenReceiptHeader}.</p>
     *
     * @return The maxReceiptCharsPerLineLarge field of the {@link KitchenReceiptHeader}.
     */
    public int getMaxReceiptCharsPerLineLarge () {
        return maxReceiptCharsPerLineLarge;
    }

    /**
     * <p>Setter for the maxReceiptCharsPerLineLarge field of the {@link KitchenReceiptHeader}.</p>
     *
     * @param maxReceiptCharsPerLineLarge The maxReceiptCharsPerLineLarge field of the {@link KitchenReceiptHeader}.
     */
    public void setMaxReceiptCharsPerLineLarge (int maxReceiptCharsPerLineLarge) {
        this.maxReceiptCharsPerLineLarge = maxReceiptCharsPerLineLarge;
    }

}