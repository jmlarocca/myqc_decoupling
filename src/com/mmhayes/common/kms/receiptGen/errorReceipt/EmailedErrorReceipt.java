package com.mmhayes.common.kms.receiptGen.errorReceipt;

/*
    $Author: jmkimber $: Author of last commit
    $Date: 2021-04-30 11:47:25 -0400 (Fri, 30 Apr 2021) $: Date of last commit
    $Rev: 13910 $: Revision of last commit
    Notes: Represents a receipt that failed to be sent to the kitchen and will be emailed.
*/

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.kitchenPrinters.KitchenPrinterDataManagerMethod;
import com.mmhayes.common.kitchenPrinters.KitchenPrinterTerminal;
import com.mmhayes.common.kitchenPrinters.LocalPrintJob;
import com.mmhayes.common.kitchenPrinters.LocalPrintJobAdapter;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.kms.KMSCommon;
import com.mmhayes.common.kmsapi.KMSDataManagerMethod;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.Commands.JPOS_COMMAND;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.utils.stringMatchingUtils.RabinKarp;
import com.mmhayes.common.xmlapi.XmlRpcManager;
import org.apache.commons.lang.math.NumberUtils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;

/**
 * <p>Represents a receipt that failed to be sent to the kitchen and will be emailed.</p>
 *
 */
public class EmailedErrorReceipt extends ErrorReceipt {

// <editor-fold desc="Constructor">
    /**
     * <p>Constructor for an {@link EmailedErrorReceipt}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this class to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param printJobs An {@link ArrayList} of {@link ArrayList} corresponding to the print jobs that failed to be sent to the kitchen.
     */
    public EmailedErrorReceipt (String log, DataManager dataManager, ArrayList<ArrayList> printJobs) {
        super(log, dataManager, printJobs);
    }
// </editor-fold>

// <editor-fold desc="BuildErrorReceipt Implementation">
    /**
     * <p>Emails a receipt for a print job that failed to be sent to the kitchen.</p>
     *
     * @param printJob A {@link LocalPrintJob} corresponding to the print jobs that failed to tbe sent to the kitchen.
     * @return Whether or not the receipt could be created successfully.
     */
    @Override
    public boolean buildErrorReceipt (LocalPrintJob printJob) {

        XmlRpcManager xmlRpcManager;
        try {
            xmlRpcManager = XmlRpcManager.getInstance();
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("An exception occurred while trying to obtain the XmlRpcManager instance in EmailedErrorReceipt.buildErrorReceipt, " +
                    "the server can't be reached, now returning false.", log, Logger.LEVEL.ERROR);
            return false;
        }

        // make a XML RPC call to the server to send the email
        if (xmlRpcManager != null) {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(LocalPrintJob.class, new LocalPrintJobAdapter());
            Gson gson = builder.create();
            Object[] args = new Object[]{PeripheralsDataManager.getPHHostname(), gson.toJson(printJob)};
            Object xmlRpcResponse = xmlRpcManager.xmlRpcInvokeInstanceOnServer(log, KMSDataManagerMethod.SEND_ERROR_RECEIPT, args);
            return xmlRpcManager.getBooleanResponse(log, xmlRpcResponse);
        }

        return false;
    }
// </editor-fold>

// <editor-fold desc="Get Email Addresses of People that will Get the Email">
    /**
     * <p>Queries the database to get the email addresses of people that should receipt the email.</p>
     *
     * @param terminalID The ID of the terminal on which the transaction took place.
     * @param log The file path {@link String} of the file to log any information related to this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @return An {@link ArrayList} of {@link String} corresponding to the email addresses of people that should receipt the email.
     */
    public static ArrayList<String> getEmailRecipients (int terminalID, String log, DataManager dataManager) {

        // validate the log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to EmailedErrorReceipt.getEmailRecipients can't be null or empty, unable to get the email recipients, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        // validate the DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to EmailedErrorReceipt.getEmailRecipients can't be null, unable to get the email recipients, now returning null.", log, Logger.LEVEL.ERROR);
            return null;
        }

        // validate the terminalID
        if (terminalID <= 0) {
            Logger.logMessage("The terminal ID passed to EmailedErrorReceipt.getEmailRecipients must be greater than 0, unable to get the email recipients, now returning null.", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetErrorReceiptEmailRecipients").addIDList(1, terminalID).setLogFileName(log);
        String emails = sql.getSingleStringField(dataManager);
        Logger.logMessage(String.format("Found the email recipients %s in EmailedErrorReceipt.getEmailRecipients, these emails will receive errored kitchen receipts.",
                Objects.toString(emails, "N/A")), log, Logger.LEVEL.TRACE);
        ArrayList<String> emailRecipients = new ArrayList<>();
        if ((StringFunctions.stringHasContent(emails)) && (emails.contains(","))) {
            emailRecipients = StringFunctions.split(emails, ",");
        }
        else if (StringFunctions.stringHasContent(emails)) {
            emailRecipients.add(emails);
        }

        return emailRecipients;
    }
// </editor-fold>

// <editor-fold desc="Send KMS Error Receipts to Recipients Through Email">
    /**
     * <p>Builds and sends out emails for the print job that failed to be sent to the kitchen.</p>
     *
     * @param requestingTerminalHostname The hostname {@link String} of the terminal that requested the emailed error receipt.
     * @param localPrintJob The {@link LocalPrintJob} that failed to be sent to the kitchen and will be sent through email.
     * @param emailSettings A {@link HashMap} whose {@link String} key is a property and whose {@link String} value is the value for that property.
     * @param emailRecipients An {@link ArrayList} of {@link String} corresponding to the email address og those who will receive the email.
     * @param log The file path {@link String} of the file to log any information related to this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @return Whether or not the emails could be sent without error.
     */
    public static boolean sendEmailsForLocalPrintJob (String requestingTerminalHostname,
                                                      LocalPrintJob localPrintJob,
                                                      HashMap<String, String> emailSettings,
                                                      ArrayList<String> emailRecipients,
                                                      String log,
                                                      DataManager dataManager) {
        // validate the MAC address of the terminal that requested the emailed error receipt
        if (!StringFunctions.stringHasContent(requestingTerminalHostname)) {
            Logger.logMessage("The MAC address of the terminal that requested an emailed error receipt and passed to EmailedErrorReceipt.sendEmailsForLocalPrintJob " +
                    "can't be null or empty, unable to send email to the desired recipients, now returning false.");
            return false;
        }

        // validate the log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to EmailedErrorReceipt.sendEmailsForLocalPrintJob can't be null or empty, " +
                    "unable to send email to the desired recipients, now returning false.", Logger.LEVEL.ERROR);
            return false;
        }

        // validate the DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to EmailedErrorReceipt.sendEmailsForLocalPrintJob can't be null, " +
                    "unable to send email to the desired recipients, now returning false.", log, Logger.LEVEL.ERROR);
            return false;
        }

        // validate the LocalPrintJob
        if (localPrintJob == null) {
            Logger.logMessage("The LocalPrintJob passed to EmailedErrorReceipt.sendEmailsForLocalPrintJob can't be null, " +
                    "unable to send email to the desired recipients, now returning false.", log, Logger.LEVEL.ERROR);
            return false;
        }

        // validate the email settings
        if (DataFunctions.isEmptyMap(emailSettings)) {
            Logger.logMessage("The email settings passed to EmailedErrorReceipt.sendEmailsForLocalPrintJob can't be null or empty, " +
                    "unable to send email to the desired recipients, now returning false.", log, Logger.LEVEL.ERROR);
            return false;
        }

        // validate the email recipients
        if (DataFunctions.isEmptyCollection(emailRecipients)) {
            Logger.logMessage("The list of email recipients passed to EmailedErrorReceipt.sendEmailsForLocalPrintJob can't be null or empty, " +
                    "unable to send email to the desired recipients, now returning false.", log, Logger.LEVEL.ERROR);
            return false;
        }

        boolean errorOccurred = false;
        for (String emailAddress : emailRecipients) {
            // build the email
            String subject = buildEmailSubject(localPrintJob, log, dataManager);
            String message = buildEmailMessage(subject, localPrintJob, log, dataManager);

            // send the email
            if (!sendEmail(requestingTerminalHostname, localPrintJob, emailSettings, emailAddress, subject, message, log)) {
                errorOccurred = true;
            }
        }

        return !errorOccurred;
    }
// </editor-fold>

// <editor-fold desc="Build the Email">
    /**
     * <p>Builds the email's subject line.</p>
     *
     * @param localPrintJob The {@link LocalPrintJob} that failed to be sent to the kitchen and will be sent through email.
     * @param log The file path {@link String} of the file to log any information related to this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @return The subject line {@link String} for the email.
     */
    private static String buildEmailSubject (LocalPrintJob localPrintJob, String log, DataManager dataManager) {

        // the LocalPrintJob, log file, and DataManager have been validated in the method that invoked this one

        // get email subject line information
        HashMap<String, String> emailSubjectInfo = getEmailSubjectInfo(localPrintJob, log, dataManager);
        String orderNumber = "";
        String printerName = "";
        String kdsStationName = "";
        String kmsStationName = "";
        String companyName = "";
        if (!DataFunctions.isEmptyMap(emailSubjectInfo)) {
            orderNumber = (emailSubjectInfo.containsKey("ORDERNUM") ? emailSubjectInfo.get("ORDERNUM") : "");
            printerName = (emailSubjectInfo.containsKey("PRINTERNAME") ? emailSubjectInfo.get("PRINTERNAME") : "");
            kdsStationName = (emailSubjectInfo.containsKey("KDSSTATIONNAME") ? emailSubjectInfo.get("KDSSTATIONNAME") : "");
            kmsStationName = (emailSubjectInfo.containsKey("KMSSTATIONNAME") ? emailSubjectInfo.get("KMSSTATIONNAME") : "");
            companyName = (emailSubjectInfo.containsKey("COMPANYNAME") ? emailSubjectInfo.get("COMPANYNAME") : "");
        }

        // define what information should be put into the email subject line
        String[][] emailSubjectLines = new String[][]{
                {"print", orderNumber, "printer", printerName, companyName},
                {"display", orderNumber, "KDS station", kdsStationName, companyName},
                {"display", orderNumber, "KMS station", kmsStationName, companyName}
        };

        // build the email subject line and return it
        boolean isForKP = ((localPrintJob.getPrinterID() > 0) && (!localPrintJob.getIsKDS()));
        boolean isForKDS = ((localPrintJob.getPrinterID() > 0) && (localPrintJob.getIsKDS()));
        boolean isForKMS = ((localPrintJob.getPrinterID() <= 0) && (localPrintJob.getKMSStationID() > 0));

        if (isForKP) {
            return String.format("Failed to %s the order: %s on the %s: %s at %s",
                    Objects.toString(emailSubjectLines[0][0], "N/A"),
                    Objects.toString(emailSubjectLines[0][1], "N/A"),
                    Objects.toString(emailSubjectLines[0][2], "N/A"),
                    Objects.toString(emailSubjectLines[0][3], "N/A"),
                    Objects.toString(emailSubjectLines[0][4], "N/A"));
        }
        else if (isForKDS) {
            return String.format("Failed to %s the order: %s on the %s: %s at %s",
                    Objects.toString(emailSubjectLines[1][0], "N/A"),
                    Objects.toString(emailSubjectLines[1][1], "N/A"),
                    Objects.toString(emailSubjectLines[1][2], "N/A"),
                    Objects.toString(emailSubjectLines[1][3], "N/A"),
                    Objects.toString(emailSubjectLines[1][4], "N/A"));
        }
        else if (isForKMS) {
            return String.format("Failed to %s the order: %s on the %s: %s at %s",
                    Objects.toString(emailSubjectLines[2][0], "N/A"),
                    Objects.toString(emailSubjectLines[2][1], "N/A"),
                    Objects.toString(emailSubjectLines[2][2], "N/A"),
                    Objects.toString(emailSubjectLines[2][3], "N/A"),
                    Objects.toString(emailSubjectLines[2][4], "N/A"));
        }

        return null;
    }

    /**
     * <p>Queries the database to get information necessary to build the subject line for the email.</p>
     *
     * @param localPrintJob The {@link LocalPrintJob} that failed to be sent to the kitchen and will be sent through email.
     * @param log The file path {@link String} of the file to log any information related to this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @return A {@link HashMap} whose {@link String} key is a property and whose {@link String} value is the value for that property.
     */
    private static HashMap<String, String> getEmailSubjectInfo (LocalPrintJob localPrintJob, String log, DataManager dataManager) {

        // the LocalPrintJob, log file, and DataManager have been validated in the method EmailedErrorReceipt.sendEmailsForLocalPrintJob

        HashMap<String, String> emailSubjectInfo = new HashMap<>();

        // add the order number to the email subject line
        String orderNum = "";
        if (!DataFunctions.isEmptyCollection(localPrintJob.getDetails())) {
            orderNum = HashMapDataFns.getStringVal(localPrintJob.getDetails().get(0), "ORDERNUM");
        }
        emailSubjectInfo.put("ORDERNUM", orderNum);

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetEmailSubjectLineInfo").addIDList(1, localPrintJob.getPrinterID()).addIDList(2, localPrintJob.getKMSStationID()).setLogFileName(log);
        HashMap queryRes = sql.getSingleHashMap(dataManager);

        if (!DataFunctions.isEmptyMap(queryRes)) {
            String printerName = HashMapDataFns.getStringVal(queryRes, "PRINTERNAME");
            String kdsStationName = HashMapDataFns.getStringVal(queryRes, "KDSSTATIONNAME");
            String kmsStationName = HashMapDataFns.getStringVal(queryRes, "KMSSTATIONNAME");
            String companyName = HashMapDataFns.getStringVal(queryRes, "REGCOMPANY");
            emailSubjectInfo.put("PRINTERNAME", printerName);
            emailSubjectInfo.put("KDSSTATIONNAME", kdsStationName);
            emailSubjectInfo.put("KMSSTATIONNAME", kmsStationName);
            emailSubjectInfo.put("COMPANYNAME", companyName);
        }

        return emailSubjectInfo;
    }

    /**
     * <p>Builds the content within the email.</p>
     *
     * @param subject The subjetc line {@link String} for the email.
     * @param localPrintJob The {@link LocalPrintJob} that failed to be sent to the kitchen and will be sent through email.
     * @param log The file path {@link String} of the file to log any information related to this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @return The content {@link String} for the email.
     */
    @SuppressWarnings("Convert2streamapi")
    private static String buildEmailMessage (String subject, LocalPrintJob localPrintJob, String log, DataManager dataManager) {

        // the LocalPrintJob, log file, and DataManager have been validated in the method that invoked this one

        // instantiate a new HTML builder
        MMHHTMLBuilder htmlBuilder = new MMHHTMLBuilder();

        // add the subject line to the email's content
        if (!StringFunctions.stringHasContent(subject)) {
            Logger.logMessage("The email subject line passed to EmailedErrorReceipt.buildEmailMessage was null or empty, unable to add it to the email.", log, Logger.LEVEL.ERROR);
        }
        else {
            htmlBuilder.addCompleteTagNoAttrs(MMHHTMLBuilder.HTMLTag.H3, subject);
        }

        // create the receipt
        htmlBuilder.addCompleteStartTag(MMHHTMLBuilder.HTMLTag.TABLE, new HashMap<>(Collections.singletonMap("style", "border: 1px solid #000000; border-collapse: collapse; max-width: 380px; width: 100%; margin-left: auto; margin-right: auto;")));

        // iterate through the details in the print job
        if (!DataFunctions.isEmptyCollection(localPrintJob.getDetails())) {
            for (HashMap detail : localPrintJob.getDetails()) {
                // add an empty line to the beginning of the receipt
                htmlBuilder.addCompleteStartTagNoAttrs(MMHHTMLBuilder.HTMLTag.TR);
                htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.TD, "&nbsp;", new HashMap<>(Collections.singletonMap("colspan", "2")));
                htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TR);
                if (!DataFunctions.isEmptyMap(detail)) {
                    String lineDetail = KMSCommon.base64Decode(HashMapDataFns.getStringVal(detail, "LINEDETAIL"));
                    if ((StringFunctions.containsIgnoreCase(lineDetail, "---CUT---")) || (StringFunctions.containsIgnoreCase(lineDetail, "---BARCODE---"))) {
                        continue;
                    }
                    if (lineDetail.equalsIgnoreCase(JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER)) {
                        htmlBuilder.addCompleteStartTagNoAttrs(MMHHTMLBuilder.HTMLTag.TR);
                        htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.TD, "&nbsp;", new HashMap<>(Collections.singletonMap("colspan", "2")));
                        htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TR);
                    }
                    else if (lineDetail.startsWith(JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.ESC + JPOS_COMMAND.LARGE_FONT)) {
                        htmlBuilder.addCompleteStartTagNoAttrs(MMHHTMLBuilder.HTMLTag.TR);
                        htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.TD, JPOS_COMMAND.removeCommands(lineDetail), new HashMap<>(ImmutableMap.<String, String>builder().put ("colspan", "2").put("style", "font-size: 28px; font-weight: bold; text-align: center;").build()));
                        htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TR);
                    }
                    else if ((StringFunctions.stringHasContent(lineDetail))
                            && (lineDetail.length() >= 22)
                            && (isValidTxnDate(lineDetail.trim().substring(0, 10).trim()))
                            && (isValidTxnTime(lineDetail.trim().substring(11).trim()))) {

                        String leftColText = lineDetail.trim().substring(0, 10).trim();
                        String rightColText = lineDetail.trim().substring(11).trim();
                        htmlBuilder.addCompleteStartTagNoAttrs(MMHHTMLBuilder.HTMLTag.TR);
                        htmlBuilder.addCompleteStartTag(MMHHTMLBuilder.HTMLTag.TD, new HashMap<>(Collections.singletonMap("colspan", "2")));
                        htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.DIV, leftColText, new HashMap<>(Collections.singletonMap("style", "font-size: 14px; float: left;")));
                        htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.DIV, rightColText, new HashMap<>(Collections.singletonMap("style", "font-size: 14px; float: right;")));
                        htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TD);
                        htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TR);
                    }
                    else if ((StringFunctions.stringHasContent(lineDetail))
                            && (lineDetail.length() >= 14)
                            && (lineDetail.startsWith("Cashier"))) {
                        int tidIndex = lineDetail.indexOf("TID:");
                        if (tidIndex < 0) {
                            throw new IllegalArgumentException(String.format("Couldn't find the String %s in %s!",
                                    Objects.toString("TID:", "N/A"),
                                    Objects.toString(lineDetail, "N/A")));
                        }
                        String leftColText = lineDetail.trim().substring(0, tidIndex).trim();
                        String rightColText = lineDetail.trim().substring(tidIndex).trim();
                        htmlBuilder.addCompleteStartTagNoAttrs(MMHHTMLBuilder.HTMLTag.TR);
                        htmlBuilder.addCompleteStartTag(MMHHTMLBuilder.HTMLTag.TD, new HashMap<>(Collections.singletonMap("colspan", "2")));
                        htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.DIV, leftColText, new HashMap<>(Collections.singletonMap("style", "font-size: 14px; float: left;")));
                        htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.DIV, rightColText, new HashMap<>(Collections.singletonMap("style", "font-size: 14px; float: right;")));
                        htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TD);
                        htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TR);
                    }
                    else if (StringFunctions.rkContains(JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_RIGHT, lineDetail, 37)) {
                        ArrayList<Integer> commandLocation = RabinKarp.search(JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_RIGHT, lineDetail, 37);
                        if ((!DataFunctions.isEmptyCollection(commandLocation)) && (commandLocation.size() == 1)) {
                            String leftColText = lineDetail.substring(0, commandLocation.get(0));
                            String rightColText = lineDetail.substring(commandLocation.get(0) + (JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_RIGHT).length());
                            htmlBuilder.addCompleteStartTagNoAttrs(MMHHTMLBuilder.HTMLTag.TR);
                            htmlBuilder.addCompleteStartTag(MMHHTMLBuilder.HTMLTag.TD, new HashMap<>(Collections.singletonMap("colspan", "2")));
                            htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.DIV, leftColText, new HashMap<>(Collections.singletonMap("style", "font-size: 14px; float: left;")));
                            htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.DIV, rightColText, new HashMap<>(Collections.singletonMap("style", "font-size: 14px; float: right;")));
                            htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TD);
                            htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TR);
                        }
                    }
                    else if ((StringFunctions.rkContains(JPOS_COMMAND.ALIGN_CENTER, lineDetail, 37)) && (lineDetail.length() > JPOS_COMMAND.ALIGN_CENTER.length())) {
                        htmlBuilder.addCompleteStartTagNoAttrs(MMHHTMLBuilder.HTMLTag.TR);
                        htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.TD, JPOS_COMMAND.removeCommands(lineDetail), new HashMap<>(ImmutableMap.<String, String>builder().put("colspan", "2").put("style", "font-size: 14px; text-align: center;").build()));
                        htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TR);
                    }
                    else {
                        if (HashMapDataFns.getBooleanVal(detail, "ISMODIFIER")) {
                            htmlBuilder.addCompleteStartTagNoAttrs(MMHHTMLBuilder.HTMLTag.TR);
                            htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.TD, "&nbsp;&nbsp;&nbsp;&nbsp;" + JPOS_COMMAND.removeCommands(lineDetail), new HashMap<>(ImmutableMap.<String, String>builder().put("colspan", "2").put("style", "font-size: 14px; text-align: left;").build()));
                            htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TR);
                        }
                        else {
                            htmlBuilder.addCompleteStartTagNoAttrs(MMHHTMLBuilder.HTMLTag.TR);
                            Pattern dupQtyPattern = Pattern.compile("(\\s+)?\\d+X\\s+\\d+X\\s+.*");
                            if (dupQtyPattern.matcher(lineDetail).matches()) {
                                lineDetail = lineDetail.replaceFirst("(\\s+)?\\s+\\d+X", "").trim();
                            }
                            htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.TD, JPOS_COMMAND.removeCommands(lineDetail), new HashMap<>(ImmutableMap.<String, String>builder().put("colspan", "2").put("style", "font-size: 14px; text-align: left;").build()));
                            htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TR);
                        }
                    }
                }
            }
        }

        // end of the receipt
        htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.TABLE);

        // add some empty lines to provide some separation between the receipt and the email footer
        htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.P, "&nbsp;", new HashMap<>(Collections.singletonMap("colspan", "2")));
        htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.P, "&nbsp;", new HashMap<>(Collections.singletonMap("colspan", "2")));
        htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.P, "&nbsp;", new HashMap<>(Collections.singletonMap("colspan", "2")));

        // add MM Hayes contact information to the receipt
        String baseURL = new commonMMHFunctions().getQCBaseURL();
        String instanceName = commonMMHFunctions.getInstanceName();
        String mmhLogo = baseURL + "/" + instanceName + "/images/email/mmhlogo.png";
        Logger.logMessage(String.format("Looking for the MM Hayes logo image at the path %s, for the email.",
                Objects.toString(mmhLogo, "N/A")), log, Logger.LEVEL.TRACE);
        htmlBuilder.addCompleteSingleElementStartTag(MMHHTMLBuilder.HTMLTag.IMG, new HashMap<>(ImmutableMap.<String, String>builder().put("alt", "MM Hayes Co., Inc.").put("src", mmhLogo).put("border", "0").put("style", "outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;").build()));
        htmlBuilder.addCompleteStartTagNoAttrs(MMHHTMLBuilder.HTMLTag.P);
        htmlBuilder.addContent("To assist MM Hayes in serving you better, please place all service requests through the Help Desk. ");
        htmlBuilder.addContent("The Help Desk can be reached by calling (518) 459-5545 or via email at ");
        htmlBuilder.addCompleteTag(MMHHTMLBuilder.HTMLTag.A, "helpdesk@mmhayes.com", new HashMap<>(ImmutableMap.<String, String>builder().put("href", "mailto:helpdesk@mmhayes.com").put("target", "_blank").put("title", "mailto:helpdesk@mmhayes.com").put("style", "color: blue;").build()));
        htmlBuilder.addContent(".");
        htmlBuilder.addCompleteEndTag(MMHHTMLBuilder.HTMLTag.P);

        // log the email message
        Logger.logMessage("Now logging the email message for the errored kitchen print job...", log, Logger.LEVEL.TRACE);
        Logger.logMessage(htmlBuilder.getHTML(), log, Logger.LEVEL.TRACE);

        return htmlBuilder.getHTML();
    }
// </editor-fold>

// <editor-fold desc="Check the Validity of the Transaction Date and Transaction Time">
    /**
     * <p>Checks whether or not the given transaction date {@link String} is valid.</p>
     *
     * @param time The time {@link String} to check.
     * @return Whether or not the given transaction date {@link String} is valid.
     */
    private static boolean isValidTxnDate (String time) {

        if (!StringFunctions.stringHasContent(time)) {
            return false;
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(MMHTimeFormatString.MO_DY_YR);

        try {
            LocalDate.parse(time, formatter);
            return true;
        }
        catch (Exception e) {
            return false;
        }

    }

    /**
     * <p>Checks whether or not the given transaction time {@link String} is valid.</p>
     *
     * @param time The time {@link String} to check.
     * @return Whether or not the given transaction time {@link String} is valid.
     */
    private static boolean isValidTxnTime (String time) {

        if (!StringFunctions.stringHasContent(time)) {
            return false;
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(MMHTimeFormatString.HR_MIN_SEC_AMPM);

        try {
            LocalTime.parse(time, formatter);
            return true;
        }
        catch (Exception e) {
            return false;
        }

    }
// </editor-fold>

// <editor-fold desc="Send the Email">
    /**
     * <p>Sends an email containing information about the print job that failed to make it to the kitchen.</p>
     *
     * @param requestingTerminalHostname The hostname {@link String} of the terminal that requested the emailed error receipt.
     * @param localPrintJob The {@link LocalPrintJob} that failed to be sent to the kitchen and will be sent through email.
     * @param emailSettings A {@link HashMap} whose {@link String} key is a property and whose {@link String} value is the value for that property.
     * @param sendToAddress The email address {@link String} that the email will be sent to.
     * @param subject The subject line {@link String} for the email.
     * @param message The content {@link String} for the email.
     * @param log The file path {@link String} of the file to log any information related to this method to.
     * @return Whether or not the email could be sent successfully.
     */
    private static boolean sendEmail (String requestingTerminalHostname,
                                      LocalPrintJob localPrintJob,
                                      HashMap<String, String> emailSettings,
                                      String sendToAddress,
                                      String subject,
                                      String message,
                                      String log) {

        // the requesting terminal's hostname, local print job, email settings, and the log file have been validated in the method that invoked this one

        // validate the email address to send the email to
        if (!StringFunctions.stringHasContent(sendToAddress)) {
            Logger.logMessage("The email address to send the email to and passed to EmailedErrorReceipt.sendEmail can't be null or " +
                    "empty, unable to send the email, now returning false.", log, Logger.LEVEL.ERROR);
            return false;
        }

        // validate the subject line
        if (!StringFunctions.stringHasContent(subject)) {
            Logger.logMessage("The subject line for the email passed to EmailedErrorReceipt.sendEmail can't be null or " +
                    "empty, unable to send the email, now returning false.", log, Logger.LEVEL.ERROR);
            return false;
        }

        // validate the message for the email
        if (!StringFunctions.stringHasContent(message)) {
            Logger.logMessage("The content for the email message passed to EmailedErrorReceipt.sendEmail can't be null or " +
                    "empty, unable to send the email, now returning false.", log, Logger.LEVEL.ERROR);
            return false;
        }

        // remove whitespace from the beginning and ending of the email address
        sendToAddress = sendToAddress.trim();

        // make sure the send to email address itself, is valid
        if (!CommonAPI.isValidEmail(sendToAddress)) {
            Logger.logMessage(String.format("The email address of %s, which was the email address to send the email was found to " +
                    "be invalid in EmailedErrorReceipt.sendEmail, unable to send the email, now returning false.",
                    Objects.toString(sendToAddress, "N/A")), log, Logger.LEVEL.ERROR);
            return false;
        }

        // extract the email settings
        String smtpServer = (emailSettings.containsKey("SMTPSERVER") ? emailSettings.get("SMTPSERVER") : "");
        String smtpUser = (emailSettings.containsKey("SMTPUSER") ? emailSettings.get("SMTPUSER") : "");
        String smtpPassword = (emailSettings.containsKey("SMTPPASSWORD") ? emailSettings.get("SMTPPASSWORD") : "");
        int smtpPort = (((emailSettings.containsKey("SMTPPORT")) && (NumberUtils.isNumber(emailSettings.get("SMTPPORT")))) ? Integer.parseInt(emailSettings.get("SMTPPORT")) : -1);
        String smtpFromAddress = (emailSettings.containsKey("SMTPFROMADDRESS") ? emailSettings.get("SMTPFROMADDRESS") : "");

        boolean emailSent;
        if (StringFunctions.stringHasContent(smtpUser)) {
            // send an authenticated email
            Logger.logMessage(String.format("Sending an authenticated email receipt to %s using the mail server %s and port %s in EmailedErrorReceipt.sendEmail.",
                    Objects.toString(sendToAddress, "N/A"),
                    Objects.toString(smtpServer, "N/A"),
                    Objects.toString(smtpPort, "N/A")), log, Logger.LEVEL.TRACE);
            if (!MMHEmail.sendAuthEmail(smtpPort, smtpServer, smtpUser, smtpPassword, sendToAddress, smtpFromAddress, subject, message, "text/html")) {
                // if sending an authenticated email doesn't work try to send an unauthenticated email
                emailSent = MMHEmail.sendEmail(smtpPort, smtpServer, sendToAddress, smtpFromAddress, subject, message, "text/html");
            }
            else {
                emailSent = true;
            }
        }
        else {
            // send an unauthenticated email
            Logger.logMessage(String.format("Sending an unauthenticated email receipt to %s using the mail server %s and port %s in EmailedErrorReceipt.sendEmail.",
                    Objects.toString(sendToAddress, "N/A"),
                    Objects.toString(smtpServer, "N/A"),
                    Objects.toString(smtpPort, "N/A")), log, Logger.LEVEL.TRACE);
            emailSent = MMHEmail.sendEmail(smtpPort, smtpServer, sendToAddress, smtpFromAddress, subject, message, "text/html");
        }

        return emailSent;
    }
// </editor-fold>

}