package com.mmhayes.common.kms.receiptGen.errorReceipt;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-08-20 16:25:56 -0400 (Fri, 20 Aug 2021) $: Date of last commit
    $Rev: 15135 $: Revision of last commit
    Notes: Represents a receipt that failed to be sent to the kitchen.
*/

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.kitchenPrinters.*;
import com.mmhayes.common.kms.KMSCommon;
import com.mmhayes.common.printing.PrintStatus;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.Commands.JPOS_COMMAND;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.xmlapi.XmlRpcManager;
import org.apache.commons.lang.math.NumberUtils;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.mmhayes.common.kms.PeripheralsDataManager.getPHHostname;

/**
 * <p>Represents a receipt that failed to be sent to the kitchen.</p>
 *
 */
public abstract class ErrorReceipt {

// <editor-fold desc="Member Variables">
    // private member variables of an ErrorReceipt
    protected String log = "";
    protected DataManager dataManager = null;
    private ArrayList<ArrayList> printJobs = new ArrayList<>();
// </editor-fold>

// <editor-fold desc="Constructor">
    /**
     * <p>Constructor for an {@link ErrorReceipt}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this class to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param printJobs An {@link ArrayList} of {@link ArrayList} corresponding to the print jobs that failed to be sent to the kitchen.
     */
    public ErrorReceipt (String log, DataManager dataManager, ArrayList<ArrayList> printJobs) {
        this.log = log;
        this.dataManager = dataManager;
        this.printJobs = printJobs;
    }
// </editor-fold>

// <editor-fold desc="Abstract Methods">
    /**
     * <p>Defines how to build the error receipt.</p>
     *
     * @param printJob A {@link LocalPrintJob} corresponding to a print job that failed to tbe sent to the kitchen.
     * @return Whether or not the error receipt could be properly built and printed/emailed.
     */
    abstract boolean buildErrorReceipt (LocalPrintJob printJob);
// </editor-fold>

// <editor-fold desc="Correct the Print Job Details for Receipt Consistency">
    /**
     * <p>Corrects details within the print jobs.</p>
     *
     * @return An {@link ArrayList} of {@link LocalPrintJob} corresponding to the fixed print jobs.
     */
    public ArrayList<LocalPrintJob> fixPrintJobs () {

        // convert the ArrayList<ArrayList>> into an ArrayList<LocalPrintJob>
        ArrayList<LocalPrintJob> localPrintJobs = convertToLocalPrintJobs();

        // make sure there are local print jobs that need to be fixed
        if (DataFunctions.isEmptyCollection(localPrintJobs)) {
            Logger.logMessage("A problem occurred while trying to create local print job objects in ErrorReceipt.fixPrintJobs, " +
                    "unable to fix the local print jobs because none exist, now returning null.", log, Logger.LEVEL.ERROR);
            return null;
        }

        for (LocalPrintJob localPrintJob : localPrintJobs) {
            // skip null print jobs
            if (localPrintJob == null) {
                continue;
            }

            // check if the print job is for KDS
            if (localPrintJob.getIsKDS()) {
                // add a redirected header
                addRedirectedHeaderToKDSDetails(localPrintJob);
                // add the header information
                addHeaderInformationToKDSDetails(localPrintJob);
                // add quantities to products
                addQuantityToProducts(localPrintJob);
                // add the total number of items
                addItemTotalToKDSDetails(localPrintJob);
                // add command to cut the receipt
                int detailsSize = localPrintJob.getDetails().size();
                HashMap cutPaperDetail = ((HashMap) localPrintJob.getDetails().get(detailsSize - 1).clone());
                localPrintJob.addDetail("LINEDETAIL", JPOS_COMMAND.ESC + JPOS_COMMAND.CUT, cutPaperDetail);
            }

            // check if the print job is for KMS
            if (localPrintJob.getKMSStationID() > 0) {
                // add quantities to products
                addQuantityToProducts(localPrintJob);
            }

            // the pickup delivery note will need to be built manually and added into the details if the transaction was made on a
            // kiosk that is sending orders directly to the printer host
            if (isKioskSendingOrdersToPrinterHost(localPrintJob.getTerminalID())) {
                Set<Integer> productIDs = DataFunctions.getUniqueValsAtKeyFromALOfHM(localPrintJob.getDetails(), "PAPLUID");
                // check if there was a dining option within the transaction
                MMHPair<Integer, String> diningOption = getDiningOption(productIDs);
                // build the pickup/delivery note
                HashMap aDetail = null;
                if (!DataFunctions.isEmptyCollection(localPrintJob.getDetails())) {
                    aDetail = localPrintJob.getDetails().get(0);
                }
                LocalDateTime transactionDate = HashMapDataFns.getLocalDateTimeVal(aDetail, "TRANSACTIONDATE");
                int orderTypeID = HashMapDataFns.getIntVal(aDetail, "PAORDERTYPEID");
                String pickupDeliveryNote = buildPickupDeliveryNote(transactionDate, orderTypeID, diningOption);
                addPickupDeliveryNoteToPrintJobDetails(localPrintJob, pickupDeliveryNote);
            }
        }

        return localPrintJobs;
    }
// </editor-fold>

// <editor-fold desc="Convert ArrayList<ArrayList> print jobs to ArrayList<LocalPrintJob>">
    /**
     * <p>Converts an {@link ArrayList} of {@link ArrayList} into an {@link ArrayList} of {@link LocalPrintJob}.</p>
     *
     * @return An {@link ArrayList} of {@link LocalPrintJob} built from an {@link ArrayList} of {@link ArrayList}.
     */
    @SuppressWarnings({"unchecked", "Duplicates"})
    private ArrayList<LocalPrintJob> convertToLocalPrintJobs () {

        // make sure there are print jobs that can be converted
        if (DataFunctions.isEmptyCollection(printJobs)) {
            Logger.logMessage("The print jobs in ErrorReceipt.convertToLocalPrintJobs can't be null or " +
                    "empty, unable to convert any failed kitchen print jobs to LocalPrintJobs, returning null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // organize the print job details by print queue ID and by their destination within the kitchen
        HashMap<Integer, HashMap<String, ArrayList<HashMap>>> organizedPrintJobs = new HashMap<>();
        for (ArrayList<HashMap> printJobDetails : printJobs) {
            if ((!DataFunctions.isEmptyCollection(printJobDetails)) && (!DataFunctions.isEmptyMap(printJobDetails.get(0)))) {
                int paPrinterQueueID = HashMapDataFns.getIntVal(printJobDetails.get(0), "PAPRINTERQUEUEID");
                HashMap<String, ArrayList<HashMap>> detailsForDestLookup = new HashMap<>();
                ArrayList<HashMap> detailsForDest;
                for (HashMap printJobDetail : printJobDetails) {
                    int printerID = HashMapDataFns.getIntVal(printJobDetail, "PRINTERID");
                    int kmsStationID = HashMapDataFns.getIntVal(printJobDetail, "KMSSTATIONID");
                    if (printerID > 0) {
                        if (detailsForDestLookup.containsKey("PTR_" + printerID)) {
                            detailsForDest = detailsForDestLookup.get("PTR_" + printerID);
                        }
                        else {
                            detailsForDest = new ArrayList<>();
                        }
                        detailsForDest.add(printJobDetail);
                        detailsForDestLookup.put("PTR_" + printerID, detailsForDest);
                    }
                    else {
                        if (detailsForDestLookup.containsKey("KMS_" + kmsStationID)) {
                            detailsForDest = detailsForDestLookup.get("KMS_" + kmsStationID);
                        }
                        else {
                            detailsForDest = new ArrayList<>();
                        }
                        detailsForDest.add(printJobDetail);
                        detailsForDestLookup.put("KMS_" + kmsStationID, detailsForDest);
                    }
                }
                organizedPrintJobs.put(paPrinterQueueID, detailsForDestLookup);
            }
        }

        // build LocalPrintJob objects from the organized print jobs
        ArrayList<LocalPrintJob> printJobsToPrintLocally = new ArrayList<>();
        if (!DataFunctions.isEmptyMap(organizedPrintJobs)) {

            // cache terminal ID and revenue center ID lookup, as well as whether or not a printer ID is for KDS lookup, and all the dining options
            HashMap<Integer, Integer> trmIDToRCIDLookup = KitchenPrinterCommon.getTrmToRCLookup(log, dataManager);
            HashMap<Integer, Boolean> isPtrIDForKDSLookup = KitchenPrinterCommon.getIsPtrIDForKDS(log, dataManager);
            ArrayList<HashMap> allDiningOptions = KitchenPrinterCommon.getAllDiningOptions(dataManager);

            for (Map.Entry<Integer, HashMap<String, ArrayList<HashMap>>> organizedPrintJobsEntry : organizedPrintJobs.entrySet()) {
                int paPrinterQueueID = organizedPrintJobsEntry.getKey();
                HashMap<String, ArrayList<HashMap>> detailsForDestLookup = organizedPrintJobsEntry.getValue();
                if (!DataFunctions.isEmptyMap(detailsForDestLookup)) {
                    for (Map.Entry<String, ArrayList<HashMap>> detailsForDestLookupEntry : detailsForDestLookup.entrySet()) {
                        String dest = detailsForDestLookupEntry.getKey();

                        int printerID = -1;
                        if ((StringFunctions.stringHasContent(dest)) && (dest.substring(0, 4).equalsIgnoreCase("PTR_")) && (NumberUtils.isNumber(dest.substring(4)))) {
                            printerID = Integer.parseInt(dest.substring(4));
                        }

                        int kmsStationID = -1;
                        if ((StringFunctions.stringHasContent(dest)) && (dest.substring(0, 4).equalsIgnoreCase("KMS_")) && (NumberUtils.isNumber(dest.substring(4)))) {
                            kmsStationID = Integer.parseInt(dest.substring(4));
                        }

                        // get the rest of the information needed to build a LocalPrintJob
                        if ((!DataFunctions.isEmptyCollection(detailsForDestLookupEntry.getValue())) && (!DataFunctions.isEmptyMap(detailsForDestLookupEntry.getValue().get(0)))) {
                            int terminalID = HashMapDataFns.getIntVal(detailsForDestLookupEntry.getValue().get(0), "TERMINALID");
                            int revenueCenterID = -1;
                            if ((!DataFunctions.isEmptyMap(trmIDToRCIDLookup)) && (trmIDToRCIDLookup.containsKey(terminalID))) {
                                revenueCenterID = trmIDToRCIDLookup.get(terminalID);
                            }
                            boolean isKDS = false;
                            if ((printerID > 0) && (!DataFunctions.isEmptyMap(isPtrIDForKDSLookup)) && (isPtrIDForKDSLookup.containsKey(printerID))) {
                                isKDS = isPtrIDForKDSLookup.get(printerID);
                            }
                            boolean isOnlineOrder = HashMapDataFns.getBooleanVal(detailsForDestLookupEntry.getValue().get(0), "ONLINEORDER");

                            ArrayList<HashMap> printJobDetails = detailsForDestLookupEntry.getValue();
                            if ((!DataFunctions.isEmptyCollection(printJobDetails)) && (!onlyHasDiningOption(allDiningOptions, printJobDetails))) {
                                Logger.logMessage(String.format("The print job with a PAPrinterQueueID of %s intended for %s had products that " +
                                        "weren't dining options, a LocalPrintJob will be created for it.",
                                        Objects.toString(paPrinterQueueID, "N/A"),
                                        Objects.toString((printerID > 0 ? "the printer with ID " + printerID + "," : "the KMS station with ID " + kmsStationID + ","), "N/A")), log, Logger.LEVEL.TRACE);
                                // build the LocalPrintJob object
                                printJobsToPrintLocally.add(
                                        new LocalPrintJob()
                                        .paPrinterQueueID(paPrinterQueueID)
                                        .printerID(printerID)
                                        .kmsStationID(kmsStationID)
                                        .terminalID(terminalID)
                                        .revenueCenterID(revenueCenterID)
                                        .isKDS(isKDS)
                                        .isOnlineOrder(isOnlineOrder)
                                        .details(printJobDetails));
                            }
                            else if ((!DataFunctions.isEmptyCollection(printJobDetails)) && (onlyHasDiningOption(allDiningOptions, printJobDetails))) {
                                Logger.logMessage(String.format("The print job with a PAPrinterQueueID of %s intended for %s didn't have products that " +
                                        "weren't dining options, a LocalPrintJob will not be created for it!",
                                        Objects.toString(paPrinterQueueID, "N/A"),
                                        Objects.toString((printerID > 0 ? "the printer with ID " + printerID + "," : "the KMS station with ID " + kmsStationID + ","), "N/A")), log, Logger.LEVEL.ERROR);
                                PrintJobStatusHandler printJobStatusHandler = null;
                                try {
                                    printJobStatusHandler = PrintJobStatusHandler.getInstance();
                                }
                                catch (Exception e) {
                                    Logger.logException(e, log);
                                }
                                PrintStatus printStatus = new PrintStatus().addPrintStatusType(PrintStatusType.FAILEDLOCALPRINT).addUpdatedDTM(LocalDateTime.now());
                                if (printJobStatusHandler != null) {
                                    if (printerID > 0) {
                                        // update the print job details so that we don't try to continually email an error receipt
                                        printJobStatusHandler.updatePrintStatusMapForPrinter(printerID, printStatus, printJobDetails);
                                    }
                                    else if (kmsStationID > 0) {
                                        // update the print job details so that we don't try to continually email an error receipt
                                        printJobStatusHandler.updatePrintStatusMapForPrintJobByStationId(printStatus, new QCPrintJob(printJobDetails, null), kmsStationID);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            Logger.logMessage("A problem occurred while trying to organize the print jobs in ErrorReceipt.convertToLocalPrintJobs, unable " +
                    "to build the LocalPrintJobs that will be printed locally, now returning null.", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (!DataFunctions.isEmptyCollection(printJobsToPrintLocally)) {
            Logger.logMessage("ErrorReceipt.convertToLocalPrintJobs: the following LocalPrintJobs have been created to be printed locally.", log, Logger.LEVEL.TRACE);
            printJobsToPrintLocally.forEach(localPrintJob -> Logger.logMessage("--" + localPrintJob.toString(), log, Logger.LEVEL.TRACE));
        }

        return printJobsToPrintLocally;
    }

    /**
     * <p>Converts an {@link ArrayList} of {@link ArrayList} into an {@link ArrayList} of {@link LocalPrintJob}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this class to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param printJobs An {@link ArrayList} of {@link ArrayList} corresponding to the print jobs that failed to be sent to the kitchen.
     * @return An {@link ArrayList} of {@link LocalPrintJob} built from an {@link ArrayList} of {@link ArrayList}.
     */
    @SuppressWarnings({"unchecked", "TypeMayBeWeakened", "Duplicates"})
    public static ArrayList<LocalPrintJob> convertToLocalPrintJobs (String log, DataManager dataManager, ArrayList<ArrayList> printJobs) {

        // make sure there are print jobs that can be converted
        if (DataFunctions.isEmptyCollection(printJobs)) {
            Logger.logMessage("The print jobs in ErrorReceipt.convertToLocalPrintJobs can't be null or " +
                    "empty, unable to convert any failed kitchen print jobs to LocalPrintJobs, returning null!", log, Logger.LEVEL.ERROR);
            return null;
        }

        // organize the print job details by print queue ID and by their destination within the kitchen
        HashMap<Integer, HashMap<String, ArrayList<HashMap>>> organizedPrintJobs = new HashMap<>();
        for (ArrayList<HashMap> printJobDetails : printJobs) {
            if ((!DataFunctions.isEmptyCollection(printJobDetails)) && (!DataFunctions.isEmptyMap(printJobDetails.get(0)))) {
                int paPrinterQueueID = HashMapDataFns.getIntVal(printJobDetails.get(0), "PAPRINTERQUEUEID");
                HashMap<String, ArrayList<HashMap>> detailsForDestLookup = new HashMap<>();
                ArrayList<HashMap> detailsForDest;
                for (HashMap printJobDetail : printJobDetails) {
                    int printerID = HashMapDataFns.getIntVal(printJobDetail, "PRINTERID");
                    int kmsStationID = HashMapDataFns.getIntVal(printJobDetail, "KMSSTATIONID");
                    if (printerID > 0) {
                        if (detailsForDestLookup.containsKey("PTR_" + printerID)) {
                            detailsForDest = detailsForDestLookup.get("PTR_" + printerID);
                        }
                        else {
                            detailsForDest = new ArrayList<>();
                        }
                        detailsForDest.add(printJobDetail);
                        detailsForDestLookup.put("PTR_" + printerID, detailsForDest);
                    }
                    else {
                        if (detailsForDestLookup.containsKey("KMS_" + kmsStationID)) {
                            detailsForDest = detailsForDestLookup.get("KMS_" + kmsStationID);
                        }
                        else {
                            detailsForDest = new ArrayList<>();
                        }
                        detailsForDest.add(printJobDetail);
                        detailsForDestLookup.put("KMS_" + kmsStationID, detailsForDest);
                    }
                }
                organizedPrintJobs.put(paPrinterQueueID, detailsForDestLookup);
            }
        }

        // build LocalPrintJob objects from the organized print jobs
        ArrayList<LocalPrintJob> printJobsToPrintLocally = new ArrayList<>();
        if (!DataFunctions.isEmptyMap(organizedPrintJobs)) {

            // cache terminal ID and revenue center ID lookup, as well as whether or not a printer ID is for KDS lookup
            HashMap<Integer, Integer> trmIDToRCIDLookup = KitchenPrinterCommon.getTrmToRCLookup(log, dataManager);
            HashMap<Integer, Boolean> isPtrIDForKDSLookup = KitchenPrinterCommon.getIsPtrIDForKDS(log, dataManager);
            ArrayList<HashMap> allDiningOptions = KitchenPrinterCommon.getAllDiningOptions(dataManager);

            for (Map.Entry<Integer, HashMap<String, ArrayList<HashMap>>> organizedPrintJobsEntry : organizedPrintJobs.entrySet()) {
                int paPrinterQueueID = organizedPrintJobsEntry.getKey();
                HashMap<String, ArrayList<HashMap>> detailsForDestLookup = organizedPrintJobsEntry.getValue();
                if (!DataFunctions.isEmptyMap(detailsForDestLookup)) {
                    for (Map.Entry<String, ArrayList<HashMap>> detailsForDestLookupEntry : detailsForDestLookup.entrySet()) {
                        String dest = detailsForDestLookupEntry.getKey();

                        int printerID = -1;
                        if ((StringFunctions.stringHasContent(dest)) && (dest.substring(0, 4).equalsIgnoreCase("PTR_")) && (NumberUtils.isNumber(dest.substring(4)))) {
                            printerID = Integer.parseInt(dest.substring(4));
                        }

                        int kmsStationID = -1;
                        if ((StringFunctions.stringHasContent(dest)) && (dest.substring(0, 4).equalsIgnoreCase("KMS_")) && (NumberUtils.isNumber(dest.substring(4)))) {
                            kmsStationID = Integer.parseInt(dest.substring(4));
                        }

                        // get the rest of the information needed to build a LocalPrintJob
                        if ((!DataFunctions.isEmptyCollection(detailsForDestLookupEntry.getValue())) && (!DataFunctions.isEmptyMap(detailsForDestLookupEntry.getValue().get(0)))) {
                            int terminalID = HashMapDataFns.getIntVal(detailsForDestLookupEntry.getValue().get(0), "TERMINALID");
                            int revenueCenterID = -1;
                            if ((!DataFunctions.isEmptyMap(trmIDToRCIDLookup)) && (trmIDToRCIDLookup.containsKey(terminalID))) {
                                revenueCenterID = trmIDToRCIDLookup.get(terminalID);
                            }
                            boolean isKDS = false;
                            if ((printerID > 0) && (!DataFunctions.isEmptyMap(isPtrIDForKDSLookup)) && (isPtrIDForKDSLookup.containsKey(printerID))) {
                                isKDS = isPtrIDForKDSLookup.get(printerID);
                            }
                            boolean isOnlineOrder = HashMapDataFns.getBooleanVal(detailsForDestLookupEntry.getValue().get(0), "ONLINEORDER");

                            ArrayList<HashMap> printJobDetails = detailsForDestLookupEntry.getValue();
                            if ((!DataFunctions.isEmptyCollection(printJobDetails)) && (!onlyHasDiningOption(allDiningOptions, printJobDetails))) {
                                Logger.logMessage(String.format("The print job with a PAPrinterQueueID of %s intended for %s had products that " +
                                        "weren't dining options, a LocalPrintJob will be created for it.",
                                        Objects.toString(paPrinterQueueID, "N/A"),
                                        Objects.toString((printerID > 0 ? "the printer with ID " + printerID + "," : "the KMS station with ID " + kmsStationID + ","), "N/A")), log, Logger.LEVEL.TRACE);
                                // build the LocalPrintJob object
                                printJobsToPrintLocally.add(
                                        new LocalPrintJob()
                                        .paPrinterQueueID(paPrinterQueueID)
                                        .printerID(printerID)
                                        .kmsStationID(kmsStationID)
                                        .terminalID(terminalID)
                                        .revenueCenterID(revenueCenterID)
                                        .isKDS(isKDS)
                                        .isOnlineOrder(isOnlineOrder)
                                        .details(printJobDetails));
                            }
                            else if ((!DataFunctions.isEmptyCollection(printJobDetails)) && (onlyHasDiningOption(allDiningOptions, printJobDetails))) {
                                Logger.logMessage(String.format("The print job with a PAPrinterQueueID of %s intended for %s didn't have products that " +
                                        "weren't dining options, a LocalPrintJob will not be created for it!",
                                        Objects.toString(paPrinterQueueID, "N/A"),
                                        Objects.toString((printerID > 0 ? "the printer with ID " + printerID + "," : "the KMS station with ID " + kmsStationID + ","), "N/A")), log, Logger.LEVEL.ERROR);
                                PrintJobStatusHandler printJobStatusHandler = null;
                                try {
                                    printJobStatusHandler = PrintJobStatusHandler.getInstance();
                                }
                                catch (Exception e) {
                                    Logger.logException(e, log);
                                }
                                PrintStatus printStatus = new PrintStatus().addPrintStatusType(PrintStatusType.FAILEDLOCALPRINT).addUpdatedDTM(LocalDateTime.now());
                                if (printJobStatusHandler != null) {
                                    if (printerID > 0) {
                                        // update the print job details so that we don't try to continually email an error receipt
                                        printJobStatusHandler.updatePrintStatusMapForPrinter(printerID, printStatus, printJobDetails);
                                    }
                                    else if (kmsStationID > 0) {
                                        // update the print job details so that we don't try to continually email an error receipt
                                        printJobStatusHandler.updatePrintStatusMapForPrintJobByStationId(printStatus, new QCPrintJob(printJobDetails, null), kmsStationID);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            Logger.logMessage("A problem occurred while trying to organize the print jobs in ErrorReceipt.convertToLocalPrintJobs, unable " +
                    "to build the LocalPrintJobs that will be printed locally, now returning null.", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (!DataFunctions.isEmptyCollection(printJobsToPrintLocally)) {
            Logger.logMessage("ErrorReceipt.convertToLocalPrintJobs: the following LocalPrintJobs have been created to be printed locally.", log, Logger.LEVEL.TRACE);
            printJobsToPrintLocally.forEach(localPrintJob -> Logger.logMessage("--" + localPrintJob.toString(), log, Logger.LEVEL.TRACE));
        }

        return printJobsToPrintLocally;
    }
// </editor-fold>

// <editor-fold desc="Check for Dining Option Only">
    /**
     * <p>Iterates through the details and checks whether or not the only product within them is a dining option.</p>
     *
     * @param allDiningOptions An {@link ArrayList} of {@link HashMap} corresponding to all the dining options.
     * @param details An {@link ArrayList} of {@link HashMap} containing the print job details.
     * @return Whether or not the only product within the print job details is a dining option.
     */
    private static boolean onlyHasDiningOption (ArrayList<HashMap> allDiningOptions, ArrayList<HashMap> details) {

        // if there are no dining options there is no way this can be the case
        if (DataFunctions.isEmptyCollection(allDiningOptions)) {
            Logger.logMessage("ErrorReceipt.onlyHasDiningOption: no dining options are configured so the order can't only have dining options.", Logger.LEVEL.TRACE);
            return false;
        }

        // if there are no print job details there is no way this can be the case either
        if (DataFunctions.isEmptyCollection(details)) {
            Logger.logMessage("ErrorReceipt.onlyHasDiningOption: there aren't any print job details so the order can't only have dining options.", Logger.LEVEL.TRACE);
            return false;
        }

        // check if all the details are dining options, if not, return false
        for (HashMap detail : details) {
            String detailsProductIdStr = HashMapDataFns.getStringVal(detail, "PAPLUID").trim();
            BigInteger detailProductId = new BigInteger(detailsProductIdStr);
            boolean isProductNotDiningOpt = false;
            for (HashMap diningOpt : allDiningOptions) {
                String diningOptProductIdStr = HashMapDataFns.getStringVal(diningOpt, "PAPLUID").trim();
                BigInteger diningOptProductId = new BigInteger(diningOptProductIdStr);
                if (!detailProductId.equals(diningOptProductId)) {
                    isProductNotDiningOpt = true;
                    break;
                }
            }
            if (isProductNotDiningOpt) {
                return false;
            }
        }

        return true;
    }
// </editor-fold>

// <editor-fold desc="Transactions Made On Kiosk Sending Orders to Printer Host Related Handling">
    /**
     * <p>Checks whether or not the given terminal ID is for a Kiosk sending orders directly to the printer host.</p>
     *
     * @param terminalID The ID of the terminal to check.
     * @return Whether or not the given terminal ID is for a Kiosk sending orders directly to the printer host.
     */
    private boolean isKioskSendingOrdersToPrinterHost (int terminalID) {

        // validate the terminal ID
        if (terminalID <= 0) {
            Logger.logMessage("The terminal ID passed to ErrorReceipt.isKioskSendingOrdersToPrinterHost must be greater " +
                    "than 0, unable to determine whether or not the given terminal ID is for a Kiosk sending orders directly to the " +
                    "printer host, now returning false.", log, Logger.LEVEL.ERROR);
            return false;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.IsKioskSendOrdersDirectToPH").addIDList(1, terminalID).setLogFileName(log);

        return sql.getSingleBooleanField(dataManager);
    }

    /**
     * <p>Checks if one of the products within the transaction was a dining option.</p>
     *
     * @param productIDs A {@link Set} of {@link Integer} corresponding to the IDs of products in the transaction.
     * @return A {@link MMHPair} with an {@link Integer} key corresponding to the product ID of the dining option and
     * a {@link String} value corresponding to the name of the dining option.
     */
    private MMHPair<Integer, String> getDiningOption (Set<Integer> productIDs) {

        // validate the product IDs
        if (DataFunctions.isEmptyCollection(productIDs)) {
            Logger.logMessage("The product IDs passed to ErrorReceipt.getDiningOption can't be null or empty unable to " +
                    "determine if one of the products within the transaction was a dining option, now returning null.", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.FindDiningOptInListOfProducts").addIDList(1, productIDs).setLogFileName(log);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
            int papluID = HashMapDataFns.getIntVal(queryRes.get(0), "PAPLUID");
            String name = HashMapDataFns.getStringVal(queryRes.get(0), "NAME");
            if ((papluID > 0) && (StringFunctions.stringHasContent(name))) {
                return MMHPair.of(papluID, name);
            }
        }

        return null;
    }

    /**
     * <p>Builds a pickup/delivery note for transactions placed on a Kiosk that is sending it's orders directly to the printer host.</p>
     *
     * @param transactionDate The {@link LocalDateTime} of when the transaction was made.
     * @param orderTypeID ID corresponding to the type of order.
     * @param diningOption A {@link MMHPair} with an {@link Integer} key corresponding to the product ID of the dining option and
     * a {@link String} value corresponding to the name of the dining option.
     */
    private String buildPickupDeliveryNote (LocalDateTime transactionDate, int orderTypeID, MMHPair<Integer, String> diningOption) {

        // validate the transaction date
        if (transactionDate == null) {
            Logger.logMessage("The transaction date passed to ErrorReceipt.buildPickupDeliveryNote can't be null, " +
                    "unable to build a pickup/delivery note for transactions placed on a Kiosk that is sending it's orders directly to " +
                    "the printer host, now returning null.", log, Logger.LEVEL.ERROR);
            return null;
        }

        // validate the order type ID
        if (orderTypeID <= 0) {
            Logger.logMessage("The order type ID passed to ErrorReceipt.buildPickupDeliveryNote must be greater than 0, " +
                    "unable to build a pickup/delivery note for transactions placed on a Kiosk that is sending it's orders directly to " +
                    "the printer host, now returning null.", log, Logger.LEVEL.ERROR);
            return null;
        }

        String pickupDeliveryNote = "";
        if (orderTypeID == TypeData.OrderType.DELIVERY) {
            pickupDeliveryNote += "Has to be delivered \\nat " + DateTimeFormatter.ofPattern(MMHTimeFormatString.HR_MIN_AMPM).format(transactionDate) + ".";
        }
        else if (orderTypeID == TypeData.OrderType.PICKUP) {
            pickupDeliveryNote += "Will be picked up \\nat " + DateTimeFormatter.ofPattern(MMHTimeFormatString.HR_MIN_AMPM).format(transactionDate) + ".";
        }

        if (diningOption != null) {
            pickupDeliveryNote += " \\nFor " + diningOption.getValue() + ".";
        }

        return pickupDeliveryNote;
    }

    /**
     * <p>Adds the pickup/delivery note detail to the details for the {@link LocalPrintJob}.</p>
     *
     * @param localPrintJob The {@link LocalPrintJob} to add the pickup/delivery note to.
     * @param pickupDeliveryNote The pickup/delivery note {@link String}.
     */
    private void addPickupDeliveryNoteToPrintJobDetails (LocalPrintJob localPrintJob, String pickupDeliveryNote) {

        // validate the LocalPrintJob
        if (localPrintJob == null) {
            Logger.logMessage("The LocalPrintJob passed to ErrorReceipt.addPickupDeliveryNoteToPrintJobDetails can't be null, " +
                    "unable to add the pickup/delivery note to the LocalPrintJob's details, now returning from the method.", log, Logger.LEVEL.ERROR);
            return;
        }

        // validate the pickup/delivery note
        if (!StringFunctions.stringHasContent(pickupDeliveryNote)) {
            Logger.logMessage("The pickup/delivery note passed to ErrorReceipt.addPickupDeliveryNoteToPrintJobDetails can't be null or " +
                    "empty, there is no pickup/delivery note to be added to the LocalPrintJob's details, now returning from the method.", log, Logger.LEVEL.ERROR);
            return;
        }

        HashMap aDetail;
        if (!DataFunctions.isEmptyCollection(localPrintJob.getDetails())) {
            aDetail = localPrintJob.getDetails().get(0);
            String orderNumber = HashMapDataFns.getStringVal(aDetail, "ORDERNUM");
            int index = 0;
            for (HashMap detail : localPrintJob.getDetails()) {
                String lineDetail = KMSCommon.base64Decode(HashMapDataFns.getStringVal(detail, "LINEDETAIL"));
                if (lineDetail.equalsIgnoreCase(JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.ESC + JPOS_COMMAND.LARGE_FONT + orderNumber)) {
                    index += 1;
                    break;
                }
                index++;
            }

            // add the pickup/delivery note to the details at the specified index
            Logger.logMessage("PICKUP NOTE WHOLE => "+pickupDeliveryNote, log, Logger.LEVEL.IMPORTANT);
            String[] pickupDeliveryNoteLines = pickupDeliveryNote.split("\\\\n", -1);
            int pickupDeliveryNoteLineIndex = index;
            for (String pickupDeliveryNoteLine : pickupDeliveryNoteLines) {
                Logger.logMessage("ADDING LINE "+pickupDeliveryNoteLine+" TO DETAILS AT INDEX "+pickupDeliveryNoteLineIndex, log, Logger.LEVEL.IMPORTANT);
                localPrintJob.addDetail(pickupDeliveryNoteLineIndex, "LINEDETAIL", JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.ESC + JPOS_COMMAND.LARGE_FONT + pickupDeliveryNoteLine, ((HashMap) aDetail.clone()));
                pickupDeliveryNoteLineIndex++;
            }
        }

    }
// </editor-fold>

// <editor-fold desc="Add a Redirected Header to the Failed KDS Print Job Details">
    /**
     * <p>Adds a redirected header to the failed KDS print job's details.</p>
     *
     * @param localPrintJob The failed KDS {@link LocalPrintJob} to add the redirected header to.
     */
    @SuppressWarnings("unchecked")
    private void addRedirectedHeaderToKDSDetails (LocalPrintJob localPrintJob) {

        // validate the LocalPrintJob
        if (localPrintJob == null) {
            Logger.logMessage("The LocalPrintJob passed to ErrorReceipt.addRedirectedHeaderToKDSDetails can't be null, unable to add the redirected " +
                    "header to the failed KDS print job, now returning from the method.", log, Logger.LEVEL.ERROR);
            return;
        }

        if (!DataFunctions.isEmptyCollection(localPrintJob.getDetails())) {
            String[] redirectedHeaderLines = new String[]{
                    JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER,
                    JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.ESC + JPOS_COMMAND.LARGE_FONT + "******************",
                    JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.ESC + JPOS_COMMAND.LARGE_FONT + "Redirected due to",
                    JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.ESC + JPOS_COMMAND.LARGE_FONT + "Error or Timeout",
                    JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.ESC + JPOS_COMMAND.LARGE_FONT + "******************"};

            int index = 0;
            for (String redirectedHeaderLine : redirectedHeaderLines) {
                HashMap aDetail = ((HashMap) localPrintJob.getDetails().get(0).clone());
                aDetail.put("PAPLUID", -1);
                localPrintJob.addDetail(index, "LINEDETAIL", redirectedHeaderLine, aDetail);
                index++;
            }
        }

    }
// </editor-fold>

// <editor-fold desc="Add a Header Information to the Failed KDS Print Job Details">
    /**
     * <p>Adds header information to the failed KDS print job details</p>
     *
     * @param localPrintJob The failed KDS {@link LocalPrintJob} to add the header details to.
     */
    @SuppressWarnings("unchecked")
    private void addHeaderInformationToKDSDetails (LocalPrintJob localPrintJob) {

        // validate the LocalPrintJob
        if (localPrintJob == null) {
            Logger.logMessage("The LocalPrintJob passed to ErrorReceipt.addHeaderInformationToKDSDetails can't be null, unable to add the " +
                    "header information to the failed KDS print job, now returning from the method.", log, Logger.LEVEL.ERROR);
            return;
        }

        // make sure the print job details are valid
        if (DataFunctions.isEmptyCollection(localPrintJob.getDetails())) {
            Logger.logMessage("No print job details have been passed to ErrorReceipt.addHeaderInformationToKDSDetails, unable " +
                    "to add the header information to the print job details, now returning from the method.", log, Logger.LEVEL.ERROR);
            return;
        }

        // get information to build the receipt header
        HashMap headerInfo = new HashMap();
        if (CommonAPI.isInLocalPrintQueue(localPrintJob.getPAPrinterQueueID())) {
            DynamicSQL sql = new DynamicSQL("data.kms.GetReceiptHeaderInfoLocally").addIDList(1, localPrintJob.getPAPrinterQueueID()).setLogFileName(log);
            ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
            if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
                headerInfo = queryRes.get(0);
            }
        }
        else {
            // make XML RPC call to the server to try and get the cashier name
            XmlRpcManager xmlRpcManager = null;
            try {
                xmlRpcManager = XmlRpcManager.getInstance();
            }
            catch (Exception e) {
                Logger.logException(e, log);
                Logger.logMessage("An exception occurred while attempting to get the XmlRpcManager instance.", log, Logger.LEVEL.ERROR);
            }
            String cashierName = "N/A";
            if (xmlRpcManager != null) {
                String method = KitchenPrinterDataManagerMethod.GET_CASHIER_NAME.getMethodName();
                Object[] args = new Object[]{getPHHostname(), HashMapDataFns.getStringVal(localPrintJob.getDetails().get(0), "PATRANSACTIONID")};
                Object xmlRpcResponse = xmlRpcManager.xmlRpcInvokeInstanceOnServer(log, method, args);
                if ((xmlRpcResponse != null) && (StringFunctions.stringHasContent(xmlRpcManager.getStringResponse(log, xmlRpcResponse)))) {
                    cashierName = xmlRpcManager.getStringResponse(log, xmlRpcResponse);
                }
            }
            DynamicSQL sql = new DynamicSQL("data.kms.GetReceiptHeaderInfo").addIDList(1, localPrintJob.getPAPrinterQueueID()).setLogFileName(log);
            ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
            if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
                headerInfo = queryRes.get(0);
            }
            headerInfo.put("CASHIERNAME", cashierName);
        }

        // get a lookup between the PrinterID and KDS station name
        HashMap<String, String> ptrIDToKDSStnNameLookup = CommonAPI.getNameLookup("PRINTERID", "NAME", "QC_Printer", " WHERE Active = 1 AND PrinterHardwareTypeID = 2");

        // add the header information
        LocalDateTime transactionDate = HashMapDataFns.getLocalDateTimeVal(headerInfo, "TransactionDate");
        String cashierName = (StringFunctions.stringHasContent(HashMapDataFns.getStringVal(headerInfo, "CASHIERNAME")) ? HashMapDataFns.getStringVal(headerInfo, "CASHIERNAME") : "N/A");
        int terminalID = HashMapDataFns.getIntVal(headerInfo, "TERMINALID");
        String transactionID = HashMapDataFns.getStringVal(headerInfo, "PATRANSACTIONID");
        String stationName = "";
        if ((!DataFunctions.isEmptyMap(ptrIDToKDSStnNameLookup))
                && (StringFunctions.stringHasContent(HashMapDataFns.getStringVal(ptrIDToKDSStnNameLookup, String.valueOf(localPrintJob.getPrinterID()).toUpperCase())))) {
            stationName = HashMapDataFns.getStringVal(ptrIDToKDSStnNameLookup, String.valueOf(localPrintJob.getPrinterID()));
        }
        String orderNumber = HashMapDataFns.getStringVal(headerInfo, "ORDERNUM");
        String pickupDeliveryNote = HashMapDataFns.getStringVal(headerInfo, "PICKUPDELIVERYNOTE");
        String transNameLabel = HashMapDataFns.getStringVal(headerInfo, "TRANSNAMELABEL");
        String transName = HashMapDataFns.getStringVal(headerInfo, "TRANSNAME");
        String personName = HashMapDataFns.getStringVal(headerInfo, "PERSONNAME");
        String transComment = HashMapDataFns.getStringVal(headerInfo, "TRANSCOMMENT");
        int transTypeID = HashMapDataFns.getIntVal(headerInfo, "TRANSTYPEID");
        String transTypeName = TypeData.getTransactionTypeName(transTypeID);

        int headerInfoInsertIndex = 5; // where to start adding header information, accounts for the redirected header

        // add the transaction date line
        if (transactionDate != null) {
            String transDate = DateTimeFormatter.ofPattern(MMHTimeFormatString.MO_DY_YR).format(transactionDate);
            String transTime = DateTimeFormatter.ofPattern(MMHTimeFormatString.HR_MIN_SEC_AMPM).format(transactionDate);
            String transDateLine = twoColumn(transDate, transTime);
            HashMap transDateDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
            if (StringFunctions.stringHasContent(transDateLine)) {
                localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", transDateLine, transDateDetail);
                headerInfoInsertIndex++;
            }
        }

        // add the cashier and terminal ID
        if ((StringFunctions.stringHasContent(cashierName)) && (terminalID > 0)) {
            String cashierTIDLine = twoColumn("Cashier: " + cashierName, "TID: " + terminalID);
            HashMap cashierTIDDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
            if (StringFunctions.stringHasContent(cashierTIDLine)) {
                localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", cashierTIDLine, cashierTIDDetail);
                headerInfoInsertIndex++;
            }
        }

        // add the transaction ID
        if ((StringFunctions.stringHasContent(transTypeName)) && (StringFunctions.stringHasContent(transactionID))) {
            String transIDLine = transTypeName + ": " + transactionID;
            HashMap transIDDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
            if (StringFunctions.stringHasContent(transIDLine)) {
                localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", transIDLine, transIDDetail);
                headerInfoInsertIndex++;
            }
        }

        // add a voided or refunded message if applicable
        if (transTypeID != TypeData.TranType.SALE) {
            String msg = "";
            if (transTypeID == TypeData.TranType.VOID) {
                msg = JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + "Order VOID";
            }
            else if (transTypeID == TypeData.TranType.REFUND) {
                msg = JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + "Order REFUND";
            }
            if (StringFunctions.stringHasContent(msg)) {
                HashMap kitchenAlertDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
                localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", msg, kitchenAlertDetail);
                headerInfoInsertIndex++;
            }
        }

        // add an empty line
        HashMap emptyLineDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
        localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER, emptyLineDetail);
        headerInfoInsertIndex++;

        // add the KDS station name
        if (StringFunctions.stringHasContent(stationName)) {
            String stationNameLine = JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + stationName;
            HashMap stationNameDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
            if (StringFunctions.stringHasContent(stationNameLine)) {
                localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", stationNameLine, stationNameDetail);
                headerInfoInsertIndex++;
            }
        }

        // add the order number
        if (StringFunctions.stringHasContent(orderNumber)) {
            String orderNumberLine = JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.ESC + JPOS_COMMAND.LARGE_FONT + orderNumber;
            HashMap orderNumberDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
            if (StringFunctions.stringHasContent(orderNumberLine)) {
                localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", orderNumberLine, orderNumberDetail);
                headerInfoInsertIndex++;
            }
        }

        // add an empty line
        emptyLineDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
        localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER, emptyLineDetail);
        headerInfoInsertIndex++;

        // add the person name as long as it isn't the same as the transaction name
        if (StringFunctions.stringHasContent(personName) && (!personName.equalsIgnoreCase(transName))) {
            String personNameLine = "Customer: " + KitchenPrinterCommon.correctAposAndQues(personName);
            HashMap personNameDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
            if (StringFunctions.stringHasContent(personNameLine)) {
                localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", personNameLine, personNameDetail);
                headerInfoInsertIndex++;
            }
        }

        // add the transaction name
        if (StringFunctions.stringHasContent(transName)) {
            String transNameLine = KitchenPrinterCommon.correctAposAndQues((StringFunctions.stringHasContent(transNameLabel) ? transNameLabel + ": " + transName : transName));
            HashMap transNameDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
            if (StringFunctions.stringHasContent(transNameLine)) {
                localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", transNameLine, transNameDetail);
                headerInfoInsertIndex++;
            }
        }

        // add the transaction comment
        if (StringFunctions.stringHasContent(transComment)) {
            String transCommentLine = KitchenPrinterCommon.correctAposAndQues("Comment: " + transComment);
            HashMap transCommentDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
            if (StringFunctions.stringHasContent(transCommentLine)) {
                localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", transCommentLine, transCommentDetail);
                headerInfoInsertIndex++;
            }
        }

        // add the pickup/delivery note
        if (StringFunctions.stringHasContent(pickupDeliveryNote)) {
            String pickupDeliveryNoteLine = KitchenPrinterCommon.correctAposAndQues(pickupDeliveryNote);
            HashMap pickupDeliveryNoteDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
            if (StringFunctions.stringHasContent(pickupDeliveryNoteLine)) {
                localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", pickupDeliveryNoteLine, pickupDeliveryNoteDetail);
                headerInfoInsertIndex++;
            }
        }

        // add an empty line
        emptyLineDetail = ((HashMap) localPrintJob.getDetails().get(headerInfoInsertIndex - 1).clone());
        localPrintJob.addDetail(headerInfoInsertIndex, "LINEDETAIL", JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER, emptyLineDetail);

    }
// </editor-fold>

//<editor-fold desc="Add KMS Station Name to KMS Print Job Details">
    /**
     * <p>Adds the name of the KMS station to the failed KMS print job.</p>
     *
     * @param localPrintJob The failed KMS {@link LocalPrintJob} to add the station name to.
     */
    private void addKMSStationNameToDetails (LocalPrintJob localPrintJob) {

        // validate the LocalPrintJob
        if (localPrintJob == null) {
            Logger.logMessage("The LocalPrintJob passed to ErrorReceipt.addKMSStationNameToDetails can't be null, unable to add the name " +
                    "of the KMS station to the failed KMS print job, now returning from the method.", log, Logger.LEVEL.ERROR);
            return;
        }

        // get a lookup between KMS station ID and KMS station name
        HashMap<String, String> kmsStationNameLookup = CommonAPI.getNameLookup("KMSSTATIONID", "NAME", "QC_KMSStation", "WHERE Active = 1");
        // validate the kmsStationNameLookup
        if (DataFunctions.isEmptyMap(kmsStationNameLookup)) {
            Logger.logMessage("The lookup between KMS station ID and Name in addKMSStationNameToDetails can't be null or empty, unable to add the name " +
                    "of the KMS station to the failed KMS print job, now returning from the method.", log, Logger.LEVEL.ERROR);
            return;
        }

        String kmsStationName = HashMapDataFns.getStringVal(kmsStationNameLookup, String.valueOf(localPrintJob.getKMSStationID()), true);
        // validate the KMS station name
        if (!StringFunctions.stringHasContent(kmsStationName)) {
            Logger.logMessage("Can't determine the name of the KMS station in addKMSStationNameToDetails, unable to add the name " +
                    "of the KMS station to the failed KMS print job, now returning from the method.", log, Logger.LEVEL.ERROR);
            return;
        }

        // determine where to insert the KMS station name
        if (!DataFunctions.isEmptyCollection(localPrintJob.getDetails())) {
            HashMap aDetail = ((HashMap) localPrintJob.getDetails().get(0).clone());
            String orderNumber = HashMapDataFns.getStringVal(aDetail, "ORDERNUM");
            int index = 0;
            for (HashMap detail : localPrintJob.getDetails()) {
                String lineDetail = KMSCommon.base64Decode(HashMapDataFns.getStringVal(detail, "LINEDETAIL"));
                if (lineDetail.equalsIgnoreCase(JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.ESC + JPOS_COMMAND.LARGE_FONT + orderNumber)) {
                    break;
                }
                index++;
            }
            localPrintJob.addDetail(index, "LINEDETAIL", JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + kmsStationName, aDetail);
        }
    }
//</editor-fold>

//<editor-fold desc="Append the Quantity to the Beginning of Product Lines Within the Print Job Details.">
    /**
     * <p>Iterates through the details in the {@link LocalPrintJob} and adds the quantity before the product.</p>
     *
     * @param localPrintJob The {@link LocalPrintJob} to add the quantity to product details for.
     */
    @SuppressWarnings("unchecked")
    private void addQuantityToProducts (LocalPrintJob localPrintJob) {

        // validate the LocalPrintJob
        if (localPrintJob == null) {
            Logger.logMessage("The LocalPrintJob passed to ErrorReceipt.addQuantityToProducts can't be null, unable to add the quantity to the products " +
                    "of the KMS station to the failed KMS print job, now returning from the method.", log, Logger.LEVEL.ERROR);
            return;
        }

        if (!DataFunctions.isEmptyCollection(localPrintJob.getDetails())) {
            for (HashMap detail : localPrintJob.getDetails()) {
                int papluID = HashMapDataFns.getIntVal(detail, "PAPLUID");
                boolean isModifier = HashMapDataFns.getBooleanVal(detail, "ISMODIFIER");
                if ((!isModifier) && (papluID > 0)) {
                    String lineDetail = KMSCommon.base64Decode(HashMapDataFns.getStringVal(detail, "LINEDETAIL"));
                    double quantity = HashMapDataFns.getDoubleVal(detail, "QUANTITY");
                    if (quantity > 0.0d) {
                        detail.put("LINEDETAIL", KMSCommon.base64Encode(String.format("%2dX %s", Math.round(quantity), lineDetail)));
                    }
                    else {
                        detail.put("LINEDETAIL", KMSCommon.base64Encode(lineDetail));
                    }
                }
                else if (isModifier) {
                    String lineDetail = KMSCommon.base64Decode(HashMapDataFns.getStringVal(detail, "LINEDETAIL"));
                    detail.put("LINEDETAIL", KMSCommon.base64Encode(String.format("\t %s", lineDetail)));
                }
            }
        }

    }
//</editor-fold>

//<editor-fold desc="Calculate and Add Total Items to the Failed KDS Print Job Details.">
    /**
     * <p>Calculates and adds the total number of items to the failed KDS print job details.</p>
     *
     * @param localPrintJob The {@link LocalPrintJob} to add the total number of items to.
     */
    private void addItemTotalToKDSDetails (LocalPrintJob localPrintJob) {

        // validate the LocalPrintJob
        if (localPrintJob == null) {
            Logger.logMessage("The LocalPrintJob passed to ErrorReceipt.addItemTotalToKDSDetails can't be null, unable to add " +
                    "the total number of items to the failed KDS print job, now returning from the method.", log, Logger.LEVEL.ERROR);
            return;
        }

        double totalQuantity = 0.0d;
        for (HashMap detail : localPrintJob.getDetails()) {
            if ((!DataFunctions.isEmptyMap(detail))
                    && (!HashMapDataFns.getBooleanVal(detail, "ISMODIFIER"))
                    && (HashMapDataFns.getIntVal(detail, "PAPLUID") > 0)
                    && (HashMapDataFns.getDoubleVal(detail, "QUANTITY") > 0.0d)) {
                totalQuantity += HashMapDataFns.getDoubleVal(detail, "QUANTITY");
            }
        }
        String[] totalQuantityLines = new String[]{
                JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER,
                JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + "******************************",
                JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER,
                String.format("Total Number of Items: %s", Math.round(totalQuantity))
        };
        for (String line : totalQuantityLines) {
            HashMap totalQtyDetail = ((HashMap) localPrintJob.getDetails().get(0).clone());
            localPrintJob.addDetail("LINEDETAIL", line, totalQtyDetail);
        }

    }
//</editor-fold>

// <editor-fold desc="Two Column">
    /**
     * <p>Creates a single receipt line where the text is left and right justified.</p>
     *
     * @param leftColText The {@link String} to left justify.
     * @param rightColText The {@link String} to right justify.
     * @return The single receipt line {@link String} with left and right justified columns.
     */
    private String twoColumn (String leftColText, String rightColText) {

        if (!StringFunctions.stringHasContent(leftColText)) {
            Logger.logMessage("The text that should appear within the left column on a receipt line can't be null or empty " +
                    "in ErrorReceipt.twoColumn!", log, Logger.LEVEL.ERROR);
            return null;
        }

        if (!StringFunctions.stringHasContent(rightColText)) {
            Logger.logMessage("The text that should appear within the right column on a receipt line can't be null or empty " +
                    "in ErrorReceipt.twoColumn!", log, Logger.LEVEL.ERROR);
            return null;
        }

        final int MAX_SMALL_CHARS = 36;

        // determine the maximum number of characters that can fit in each column of the receipt line
        final int LEFT_WIDTH = ((MAX_SMALL_CHARS * 2) / 3);
        final int RIGHT_WIDTH = (MAX_SMALL_CHARS - LEFT_WIDTH);

        // truncate the text in the left column if necessary
        if (leftColText.length() > LEFT_WIDTH) {
            leftColText = leftColText.substring(0, LEFT_WIDTH);
        }
        if (rightColText.length() > RIGHT_WIDTH) {
            rightColText = rightColText.substring(0, RIGHT_WIDTH - 1);
        }

        return String.format("%s" + JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_RIGHT + "%s", leftColText, rightColText);
    }
// </editor-fold>

}