package com.mmhayes.common.kms.receiptGen.errorReceipt;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-06-21 09:19:43 -0400 (Mon, 21 Jun 2021) $: Date of last commit
    $Rev: 14148 $: Revision of last commit
    Notes: Represents a receipt that failed to be sent to the kitchen and will be printed locally.
*/

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kitchenPrinters.LocalPrintJob;
import com.mmhayes.common.kms.KMSCommon;
import com.mmhayes.common.receiptGen.Commands.JPOS_COMMAND;
import com.mmhayes.common.receiptGen.Formatter.KitchenPrinterReceiptFormatter;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>Represents a receipt that failed to be sent to the kitchen and will be printed locally.</p>
 *
 */
public class PrintedErrorReceipt extends ErrorReceipt {

// <editor-fold desc="Private Member Variables">
    // private member variables of a PrintedErrorReceipt
    private IRenderer renderer = null;
// </editor-fold>

// <editor-fold desc="Constructor">
    /**
     * <p>Constructor for a {@link PrintedErrorReceipt}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this class to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param printJobs An {@link ArrayList} of {@link ArrayList} corresponding to the print jobs that failed to be sent to the kitchen.
     * @param renderer The {@link IRenderer} to use to render the printed receipt.
     */
    public PrintedErrorReceipt (String log, DataManager dataManager, ArrayList<ArrayList> printJobs, IRenderer renderer) {
        super(log, dataManager, printJobs);
        this.renderer = renderer;
    }
// </editor-fold>

// <editor-fold desc="BuildErrorReceipt Implementation">
    /**
     * <p>Builds a printed receipt for a print job that failed to be sent to the kitchen.</p>
     *
     * @param printJob A {@link LocalPrintJob} corresponding to the print jobs that failed to tbe sent to the kitchen.
     * @return Whether or not the receipt could be created successfully.
     */
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    @Override
    public boolean buildErrorReceipt (LocalPrintJob printJob) {

        // validate the renderer
        if (renderer == null) {
            Logger.logMessage("The renderer in PrintedErrorReceipt.buildErrorReceipt cant't be null, unable to render the details for the printed receipt, now returning false.", log, Logger.LEVEL.ERROR);
            return false;
        }

        // validate the LocalPrintJob
        if (printJob == null) {
            Logger.logMessage("The print job passed to PrintedErrorReceipt.buildErrorReceipt cant't be null, unable to render the details for the printed receipt, now returning false.", log, Logger.LEVEL.ERROR);
            return false;
        }

        // make sure there are print job details to print
        if (DataFunctions.isEmptyCollection(printJob.getDetails())) {
            Logger.logMessage("The print jobs passed to PrintedErrorReceipt.buildErrorReceipt cant't be null or empty, unable to render the details for the printed receipt, now returning false.", log, Logger.LEVEL.ERROR);
            return false;
        }

        for (HashMap detail : printJob.getDetails()) {
            String lineDetail = KMSCommon.base64Decode(HashMapDataFns.getStringVal(detail, "LINEDETAIL"));
            if (StringFunctions.stringHasContent(lineDetail)) {
                if ((StringFunctions.rkContains(JPOS_COMMAND.ESC + JPOS_COMMAND.CUT, lineDetail, 37)) || (StringFunctions.rkContains(KitchenPrinterReceiptFormatter.JPOS_CUT, lineDetail, 37))) {
                    renderer.cutPaper();
                }
                else if (StringFunctions.rkContains(KitchenPrinterReceiptFormatter.BARCODE, lineDetail, 37)) {
                    String paTransactionID = lineDetail.replace(KitchenPrinterReceiptFormatter.BARCODE, "");
                    renderer.addBlankLine();
                    renderer.addBlankLine();
                    renderer.addBarcode("QCPOS" + paTransactionID);
                }
                else {
                    Logger.logMessage(String.format("Added line to the local error receipt: %s",
                            Objects.toString(lineDetail, "N/A")), log, Logger.LEVEL.TRACE);
                    if ((HashMapDataFns.getIntVal(detail, "KMSSTATIONID") > 0) && (HashMapDataFns.getDoubleVal(detail, "QUANTITY") > 0.0d)) {
                        lineDetail = removeDuplicateQuantitiesOnKMS(lineDetail);
                    }
                    renderer.addLine(lineDetail);
                    if (renderer.getLastException() != null) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * <p>Recursively removes duplicate quantities on OMS error receipts.</p>
     *
     * @param lineDetail The line detail {@link String} to modify.
     * @return The modified line detail {@link String}.
     */
    private String removeDuplicateQuantitiesOnKMS (String lineDetail) {

        if (StringFunctions.stringHasContent(lineDetail)) {
            Pattern pattern = Pattern.compile("(\\s+)?((\\d+)X)(\\s+)?((\\d+)X)");
            Matcher matcher = pattern.matcher(lineDetail);

            if (matcher.find()) {
                lineDetail = lineDetail.trim();
                lineDetail = lineDetail.replaceFirst("((\\d+)X)", "");
                return removeDuplicateQuantitiesOnKMS(lineDetail);
            }
            else {
                return lineDetail.trim();
            }

        }

        return lineDetail;
    }
// </editor-fold>

}