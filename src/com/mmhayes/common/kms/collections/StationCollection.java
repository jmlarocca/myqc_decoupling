package com.mmhayes.common.kms.collections;

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kms.models.KMSStationModel;
import com.mmhayes.common.utils.DataFunctions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StationCollection {

    private static DataManager dm = new DataManager();

    public StationCollection () {

    }

    public static List<KMSStationModel> getStations(int stationID) {

        List<KMSStationModel> stations = new ArrayList<>();
        //Returns all Station Models in the Revenue Center of the requesting Station.
        ArrayList<HashMap> stylesHM = DataFunctions.getGenericALFromObjList(dm.parameterizedExecuteQuery("data.KMSResource.getKMSStationsInSameRevenueCenter",new Object[]{stationID}, true), HashMap.class);
        for(HashMap hm: stylesHM){
            stations.add(KMSStationModel.buildFromHM(dm, null, hm));
        }

        return stations;
    }

    public static KMSStationModel getStation(int stationID) {
        /**
         *  Returns the Station Model for the specified Station if one exists.
         The Station Model should also return the following fields (and these fields should be included on the model, populated on creation):
         •	MirrorStationName
         •	ExpeditorStationName
         •	BackupStationName
         The Station Model does NOT need to return the following fields to the front end:
         •	Active
         •	Logo File Name
         •	Token
         •	MirrorKMSStationID
         •	ExpeditorKMSStationID
         •	BackupKMSStationID

         */

        KMSStationModel station = null;

        ArrayList<HashMap> stationHM = DataFunctions.getGenericALFromObjList(dm.parameterizedExecuteQuery("data.KMSResource.getKMSStation",new Object[]{stationID}, true), HashMap.class);
        if(stationHM != null && stationHM.size() > 0){
            station = KMSStationModel.buildFromHM(dm, null, stationHM.get(0));
            //                if(stationHM.get(0).get("MIRRORSTATIONNAME") != null){
            //                    station.setMirrorStationName(stationHM.get(0).get("MIRRORSTATIONNAME").toString());
            //                }
            //                if(stationHM.get(0).get("EXPEDITORSTATIONNAME") != null){
            //                    station.setMirrorStationName(stationHM.get(0).get("EXPEDITORSTATIONNAME").toString());
            //                }
            //                if(stationHM.get(0).get("BACKUPSTATIONNAME") != null){
            //                    station.setBackupStationName(stationHM.get(0).get("BACKUPSTATIONNAME").toString());
            //                }
        }

        return station;
    }

    public static String getStationName (int stationID) {
        String name = "";

        KMSStationModel station = getStation(stationID);
        if (station != null) {
            name = station.getName();
        }

        return name;
    }

}

