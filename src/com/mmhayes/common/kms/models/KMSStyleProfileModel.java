package com.mmhayes.common.kms.models;

/*
    Last Updated (automatically updated by SVN)
    $Author: crking $: Author of last commit
    $Date: 2020-05-13 16:12:43 -0400 (Wed, 13 May 2020) $: Date of last commit
    $Rev: 11731 $: Revision of last commit
    Notes: Object representation of style profile which holds styling information for a KMS station.
*/

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Object representation of style profile which holds styling information for a KMS station.</p>
 *
 */
public class KMSStyleProfileModel {

    // private member variables of a KMSStyleProfileModel
    private int styleProfileID = 0;
    private String name = "";
    private String description = "";
    private int ownershipGroupID = 0;
    private boolean active = false;
    private String fontColor = "";
    private String fontWeight = "";
    private String fontStyle = "";
    private String backgroundColor = "";
    private String borderWidth = "";
    private String borderStyle = "";
    private String borderColor = "";
    private int fontFamilyID = 0;
    private int fontSizeID = 0;

    /**
     * <p>Constructs a new {@link KMSStyleProfileModel} from the given {@link HashMap}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param hm A {@link HashMap} with data corresponding to the style profile to build a {@link KMSStyleProfileModel} for.
     * @return The {@link KMSStyleProfileModel} built from the given {@link HashMap}.
     */
    public static KMSStyleProfileModel buildFromHM (String log, HashMap hm) {
        KMSStyleProfileModel kmsStyleProfileModel = new KMSStyleProfileModel();

        try {
            if (!DataFunctions.isEmptyMap(hm)) {
                kmsStyleProfileModel.styleProfileID = HashMapDataFns.getIntVal(hm, "STYLEPROFILEID");
                kmsStyleProfileModel.name = HashMapDataFns.getStringVal(hm, "NAME");
                kmsStyleProfileModel.description = HashMapDataFns.getStringVal(hm, "DESCRIPTION");
                kmsStyleProfileModel.ownershipGroupID = HashMapDataFns.getIntVal(hm, "OWNERSHIPGROUPID");
                kmsStyleProfileModel.active = HashMapDataFns.getBooleanVal(hm, "ACTIVE");
                kmsStyleProfileModel.fontColor = HashMapDataFns.getStringVal(hm, "FONTCOLOR");
                kmsStyleProfileModel.fontWeight = HashMapDataFns.getStringVal(hm, "FONTWEIGHT");
                kmsStyleProfileModel.fontStyle = HashMapDataFns.getStringVal(hm, "FONTSTYLE");
                kmsStyleProfileModel.backgroundColor = HashMapDataFns.getStringVal(hm, "BACKGROUNDCOLOR");
                kmsStyleProfileModel.borderWidth = HashMapDataFns.getStringVal(hm, "BORDERWIDTH");
                kmsStyleProfileModel.borderStyle = HashMapDataFns.getStringVal(hm, "BORDERSTYLE");
                kmsStyleProfileModel.borderColor = HashMapDataFns.getStringVal(hm, "BORDERCOLOR");
                kmsStyleProfileModel.fontFamilyID = HashMapDataFns.getIntVal(hm, "FONTFAMILYID");
                kmsStyleProfileModel.fontSizeID = HashMapDataFns.getIntVal(hm, "FONTSIZEID");
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build a KMSStyleProfileModel object with the given HashMap in KMSStyleProfileModel.buildFromHM", log, Logger.LEVEL.ERROR);
        }

        return kmsStyleProfileModel;
    }

    /**
     * <p>Constructs an {@link ArrayList} of {@link KMSStyleProfileModel} from the given styleProfileIDs.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param styleProfileIDs IDs of the style profiles to build {@link KMSStyleProfileModel} objects for.
     * @return An {@link ArrayList} of {@link KMSStyleProfileModel} constructed from the given styleProfileIDs.
     */
    @SuppressWarnings("Convert2streamapi")
    public static ArrayList<KMSStyleProfileModel> buildFromIDs (DataManager dataManager, String log, int[] styleProfileIDs) {
        ArrayList<KMSStyleProfileModel> kmsStyleProfileModels = new ArrayList<>();

        try {
            if (styleProfileIDs.length > 0) {
                // query the database to get the style profile records
                ArrayList<String> idList = new ArrayList<>();
                for (int styleProfileID : styleProfileIDs) {
                    if (styleProfileID > 0) {
                        idList.add(String.valueOf(styleProfileID));
                    }
                }
                ArrayList<HashMap> queryRes = new DynamicSQL("data.kms.GetKMSStyleProfilesByIDs").addIDList(1, idList).serialize(dataManager);

                // convert the style profile records into KMSStyleProfile objects
                if (!DataFunctions.isEmptyCollection(queryRes)) {
                    for (HashMap hm : queryRes) {
                        if (!DataFunctions.isEmptyMap(hm)) {
                            kmsStyleProfileModels.add(buildFromHM(log, hm));
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build KMSStyleProfileModel objects for the style " +
                    "profiles with IDs of %s in KMSStyleProfileModel.buildFromIDs",
                    Objects.toString(Arrays.toString(styleProfileIDs), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return kmsStyleProfileModels;
    }

    /**
     * <p>Constructs a {@link KMSStyleProfileModel} using the given JSON {@link String}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param jsonString The JSON {@link String} to build a {@link KMSStyleProfileModel} from.
     * @return A {@link KMSStyleProfileModel} built from the given JSON {@link String}.
     */
    public static KMSStyleProfileModel buildFromJSON (String log, String jsonString) {
        KMSStyleProfileModel kmsStyleProfileModel = new KMSStyleProfileModel();

        try {
            if (StringFunctions.stringHasContent(jsonString)) {
                Gson gson = new Gson();
                JsonParser jsonParser = new JsonParser();
                JsonElement jsonElement = jsonParser.parse(jsonString);
                kmsStyleProfileModel = gson.fromJson(jsonElement, KMSStyleProfileModel.class);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build the KMSStyleProfileModel from the JSON String %s in KMSStyleProfileModel.buildFromJSON",
                    Objects.toString(jsonString, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return kmsStyleProfileModel;
    }



    /**
     * <p>Overridden toString() method for a {@link KMSStyleProfileModel}.</p>
     *
     * @return A {@link String} representation of this {@link KMSStyleProfileModel}.
     */
    @Override
    public String toString () {

        return String.format("STYLEPROFILEID: %s, NAME: %s, DESCRIPTION: %s, OWNERSHIPGROUPID: %s, ACTIVE: %s, FONTCOLOR: %s, FONTWEIGHT: %s, FONTSTYLE: %s, " +
                "BACKGROUNDCOLOR: %s, BORDERWIDTH: %s, BORDERSTYLE: %s, BORDERCOLOR: %s, FONTFAMILYID: %s, FONTSIZEID: %s",
                Objects.toString((styleProfileID != -1 ? styleProfileID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString((ownershipGroupID != -1 ? ownershipGroupID : "N/A"), "N/A"),
                Objects.toString(active, "N/A"),
                Objects.toString((StringFunctions.stringHasContent(fontColor) ? fontColor : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(fontWeight) ? fontWeight : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(fontStyle) ? fontStyle : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(backgroundColor) ? backgroundColor : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(borderWidth) ? borderWidth : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(borderStyle) ? borderStyle : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(borderColor) ? borderColor : "N/A"), "N/A"),
                Objects.toString((fontFamilyID != -1 ? fontFamilyID : "N/A"), "N/A"),
                Objects.toString((fontSizeID != -1 ? fontSizeID : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link KMSStyleProfileModel}.
     * Two {@link KMSStyleProfileModel} are defined as being equal if they have the same styleProfileID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KMSStyleProfileModel}.
     * @return Whether or not the {@link Object} is equal to this {@link KMSStyleProfileModel}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KMSStyleProfileModel and check if the obj's styleProfileID is equal to this KMSStyleProfileModel's styleProfileID
        KMSStyleProfileModel kmsStyleProfileModel = ((KMSStyleProfileModel) obj);
        return Objects.equals(kmsStyleProfileModel.styleProfileID, styleProfileID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link KMSStyleProfileModel}.</p>
     *
     * @return The unique hash code for this {@link KMSStyleProfileModel}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(styleProfileID);
    }

    /**
     * <p>Getter for the styleProfileID field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The styleProfileID field of the {@link KMSStyleProfileModel}.
     */
    public int getStyleProfileID () {
        return styleProfileID;
    }

    /**
     * <p>Setter for the styleProfileID field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param styleProfileID The styleProfileID field of the {@link KMSStyleProfileModel}.
     */
    public void setStyleProfileID (int styleProfileID) {
        this.styleProfileID = styleProfileID;
    }

    /**
     * <p>Getter for the name field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The name field of the {@link KMSStyleProfileModel}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param name The name field of the {@link KMSStyleProfileModel}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the description field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The description field of the {@link KMSStyleProfileModel}.
     */
    public String getDescription () {
        return description;
    }

    /**
     * <p>Setter for the description field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param description The description field of the {@link KMSStyleProfileModel}.
     */
    public void setDescription (String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the ownershipGroupID field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The ownershipGroupID field of the {@link KMSStyleProfileModel}.
     */
    public int getOwnershipGroupID () {
        return ownershipGroupID;
    }

    /**
     * <p>Setter for the ownershipGroupID field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param ownershipGroupID The ownershipGroupID field of the {@link KMSStyleProfileModel}.
     */
    public void setOwnershipGroupID (int ownershipGroupID) {
        this.ownershipGroupID = ownershipGroupID;
    }

    /**
     * <p>Getter for the active field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The active field of the {@link KMSStyleProfileModel}.
     */
    public boolean getActive () {
        return active;
    }

    /**
     * <p>Setter for the active field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param active The active field of the {@link KMSStyleProfileModel}.
     */
    public void setActive (boolean active) {
        this.active = active;
    }

    /**
     * <p>Getter for the fontColor field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The fontColor field of the {@link KMSStyleProfileModel}.
     */
    public String getFontColor () {
        return fontColor;
    }

    /**
     * <p>Setter for the fontColor field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param fontColor The fontColor field of the {@link KMSStyleProfileModel}.
     */
    public void setFontColor (String fontColor) {
        this.fontColor = fontColor;
    }

    /**
     * <p>Getter for the fontWeight field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The fontWeight field of the {@link KMSStyleProfileModel}.
     */
    public String getFontWeight () {
        return fontWeight;
    }

    /**
     * <p>Setter for the fontWeight field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param fontWeight The fontWeight field of the {@link KMSStyleProfileModel}.
     */
    public void setFontWeight (String fontWeight) {
        this.fontWeight = fontWeight;
    }

    /**
     * <p>Getter for the fontStyle field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The fontStyle field of the {@link KMSStyleProfileModel}.
     */
    public String getFontStyle () {
        return fontStyle;
    }

    /**
     * <p>Setter for the fontStyle field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param fontStyle The fontStyle field of the {@link KMSStyleProfileModel}.
     */
    public void setFontStyle (String fontStyle) {
        this.fontStyle = fontStyle;
    }

    /**
     * <p>Getter for the backgroundColor field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The backgroundColor field of the {@link KMSStyleProfileModel}.
     */
    public String getBackgroundColor () {
        return backgroundColor;
    }

    /**
     * <p>Setter for the backgroundColor field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param backgroundColor The backgroundColor field of the {@link KMSStyleProfileModel}.
     */
    public void setBackgroundColor (String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * <p>Getter for the borderWidth field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The borderWidth field of the {@link KMSStyleProfileModel}.
     */
    public String getBorderWidth () {
        return borderWidth;
    }

    /**
     * <p>Setter for the borderWidth field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param borderWidth The borderWidth field of the {@link KMSStyleProfileModel}.
     */
    public void setBorderWidth (String borderWidth) {
        this.borderWidth = borderWidth;
    }

    /**
     * <p>Getter for the borderStyle field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The borderStyle field of the {@link KMSStyleProfileModel}.
     */
    public String getBorderStyle () {
        return borderStyle;
    }

    /**
     * <p>Setter for the borderStyle field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param borderStyle The borderStyle field of the {@link KMSStyleProfileModel}.
     */
    public void setBorderStyle (String borderStyle) {
        this.borderStyle = borderStyle;
    }

    /**
     * <p>Getter for the borderColor field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The borderColor field of the {@link KMSStyleProfileModel}.
     */
    public String getBorderColor () {
        return borderColor;
    }

    /**
     * <p>Setter for the borderColor field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param borderColor The borderColor field of the {@link KMSStyleProfileModel}.
     */
    public void setBorderColor (String borderColor) {
        this.borderColor = borderColor;
    }

    /**
     * <p>Getter for the fontFamilyID field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The fontFamilyID field of the {@link KMSStyleProfileModel}.
     */
    public int getFontFamilyID () {
        return fontFamilyID;
    }

    /**
     * <p>Setter for the fontFamilyID field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param fontFamilyID The fontFamilyID field of the {@link KMSStyleProfileModel}.
     */
    public void setFontFamilyID (int fontFamilyID) {
        this.fontFamilyID = fontFamilyID;
    }

    /**
     * <p>Getter for the fontSizeID field of the {@link KMSStyleProfileModel}.</p>
     *
     * @return The fontSizeID field of the {@link KMSStyleProfileModel}.
     */
    public int getFontSizeID () {
        return fontSizeID;
    }

    /**
     * <p>Setter for the fontSizeID field of the {@link KMSStyleProfileModel}.</p>
     *
     * @param fontSizeID The fontSizeID field of the {@link KMSStyleProfileModel}.
     */
    public void setFontSizeID (int fontSizeID) {
        this.fontSizeID = fontSizeID;
    }

}