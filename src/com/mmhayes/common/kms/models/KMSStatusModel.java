package com.mmhayes.common.kms.models;

import redstone.xmlrpc.XmlRpcStruct;

import java.util.HashMap;
import java.util.Objects;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class KMSStatusModel {
    public Integer KMSOrderStatusID;
    public String PATransactionID;
    //if this is defined, we know the update is for a specific product, NOT the transaction.
    public String PATransLineItemID;//TODO: should this be just the incremented detail ID or a whole PATransLineItem?
    public Integer PreviousKMSOrderStatusID;

    public KMSStatusModel(){}
    public KMSStatusModel(XmlRpcStruct map){
        if(map.containsKey("KMSOrderStatusID"))
            KMSOrderStatusID = map.getInteger("KMSOrderStatusID");
        if(map.containsKey("PATransactionID"))
            PATransactionID = map.get("PATransactionID").toString();
        if(map.containsKey("PATransLineItemID"))
            PATransLineItemID = map.get("PATransLineItemID").toString();
        if(map.containsKey("PreviousKMSOrderStatusID"))
            PreviousKMSOrderStatusID = map.getInteger("PreviousKMSOrderStatusID");
    }

    public HashMap serialize(){
        HashMap hm = new HashMap();
        if(KMSOrderStatusID != null)
            hm.put("KMSOrderStatusID", KMSOrderStatusID);
        if(PATransactionID != null)
            hm.put("PATransactionID", PATransactionID);
        if(PATransLineItemID != null)
            hm.put("PATransLineItemID", PATransLineItemID);
        if(PreviousKMSOrderStatusID != null)
            hm.put("PreviousKMSOrderStatusID", PreviousKMSOrderStatusID);
        return hm;
    }

    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a CreditCardTxnData and check if the obj's ccTxnInfo is equal to this CreditCardTxnData's ccTxnInfo
        KMSStatusModel status = ((KMSStatusModel) obj);
        return Objects.equals(status.PATransactionID, PATransactionID)
                && Objects.equals(status.PATransLineItemID, PATransLineItemID)
                && Objects.equals(status.KMSOrderStatusID, KMSOrderStatusID);
    }

    @Override
    public int hashCode () {
        return Objects.hash(PATransactionID + "_" + PATransLineItemID + "_" + KMSOrderStatusID);
    }
}
