package com.mmhayes.common.kms.models.serializers;

import com.mmhayes.common.kms.models.KMSEventLogLine;
import com.mmhayes.common.kms.models.KMSModel;
import com.mmhayes.common.kms.models.KMSStatusUpdateModel;
import com.mmhayes.common.utils.Logger;
import redstone.xmlrpc.XmlRpcCustomSerializer;
import redstone.xmlrpc.XmlRpcException;
import redstone.xmlrpc.XmlRpcMessages;
import redstone.xmlrpc.XmlRpcSerializer;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.Writer;

/**
 * <p>Custom XML RPC serializer that will enable methods to return a {@link com.mmhayes.common.kms.models.KMSEventLogLine} through a XML RPC call.</p>
 *
 */
public class KMSSerializer implements XmlRpcCustomSerializer {

    /**
     * <p>Overridden getSupportedClass method for a {@link KMSSerializer}.</p>
     *
     * @return The supported {@link Class} that the serializer knows how to handle.
     */
    @Override
    public Class getSupportedClass () {
        return KMSStatusUpdateModel.class;
    }

    /**
     * <p>Overridden serialize method for a {@link KMSSerializer}.</p>
     *
     * @param value The {@link Object} to serialize.
     * @param writer The {@link java.io.Writer} to place the serialized data.
     * @param xmlRpcSerializer The built in {@link redstone.xmlrpc.XmlRpcSerializer} used by the "server".
     * @throws redstone.xmlrpc.XmlRpcException
     * @throws java.io.IOException
     */
    @Override
    public void serialize (Object value, Writer writer, XmlRpcSerializer xmlRpcSerializer) throws XmlRpcException, IOException {
        writer.write("<struct>");

        try {
            BeanInfo qcKitchenPrinterInfo = Introspector.getBeanInfo(value.getClass(), Object.class);
            PropertyDescriptor[] descriptors = qcKitchenPrinterInfo.getPropertyDescriptors();

            if (descriptors.length > 0) {
                for (PropertyDescriptor descriptor : descriptors) {
                    Object propertyValue = descriptor.getReadMethod().invoke(value, (Object[]) null);
                    if (propertyValue != null) {
                        writer.write("<member>");
                        writer.write("<name>");
                        writer.write(descriptor.getDisplayName());
                        writer.write("</name>");
                        xmlRpcSerializer.serialize(propertyValue, writer);
                        writer.write("</member>");
                    }
                }
            }
            else {
                Logger.logMessage("No property descriptors found for the KMSEventLogLine in KMSEventLogLineSerializer.serialize", Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            throw new XmlRpcException(XmlRpcMessages.getString("KMSEventLogLineSerializer.SerializationError"), e);
        }

        writer.write("</struct>");
    }

}