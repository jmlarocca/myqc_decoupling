package com.mmhayes.common.kms.models;

import java.util.HashMap;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class KMSUpdateConfigModel {
    public String updateTime;
    public HashMap<String, Object> mappings;

    public KMSUpdateConfigModel(String newTime, HashMap<String,Object> mappings){
        //this should be in the moment.JS format of "ddd MMM D HH:mm:ss *** YYYY"
        updateTime = newTime + ""; //so we get the string "null" when the time is null, otherwise it wouldnt be serialized
        this.mappings = mappings;
    }
}
