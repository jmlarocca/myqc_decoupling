package com.mmhayes.common.kms.models;

import redstone.xmlrpc.XmlRpcStruct;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu; 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class KMSEventLogLine extends KMSModel{
    private int KMSStationID;
    private int KMSEventTypeID;
    private String PATransactionID;
    private String PATransLineItemID;
    private String eventDTM;
    private String NOTE;
    private Integer KMSOrderStatusID;

    public KMSEventLogLine(){

    }
    public KMSEventLogLine(XmlRpcStruct struct){
        if(struct == null)
            return;
        if(struct.containsKey("KMSStationID"))
            this.KMSStationID = struct.getInteger("KMSStationID");
        if(struct.containsKey("KMSEventTypeID"))
            this.KMSEventTypeID = struct.getInteger("KMSEventTypeID");
        if(struct.containsKey("PATransactionID"))
            this.PATransactionID = (struct.get("PATransactionID").toString());
        if(struct.containsKey("PATransLineItemID"))
            this.PATransLineItemID = (struct.get("PATransLineItemID").toString());
        if(struct.containsKey("KMSOrderStatusID"))
            this.KMSOrderStatusID = struct.getInteger("KMSOrderStatusID");
        this.NOTE = struct.getOrDefault("NOTE", "").toString();
        this.eventDTM = struct.getOrDefault("eventDTM", "").toString();
    }

    public KMSEventLogLine(String timestamp, Integer stationID, String PATransactionID, String PATransLineItemID, Integer eventID, String note, Integer KMSOrderStatusID){
        this.KMSStationID = stationID;
        this.PATransactionID = PATransactionID;
        this.PATransLineItemID = PATransLineItemID;
        this.NOTE = note;
        this.KMSOrderStatusID = KMSOrderStatusID;
        this.KMSEventTypeID = eventID;
        this.eventDTM = timestamp;
    }

    public String log(){
        String line = "" +
                "KMSStationID" + this.KMSStationID + " " +
                "PATransactionID" + this.PATransactionID + " " +
                "PATransLineItemID" + this.PATransLineItemID + " " +
                "NOTE" + this.NOTE + " " +
                "KMSOrderStatusID" + this.KMSOrderStatusID + " " +
                "KMSEventTypeID" + this.KMSEventTypeID + " " +
                "EventDTM" + this.eventDTM;
        return line;
    }

    public int getKMSStationID() {
        return KMSStationID;
    }

    public void setKMSStationID(int KMSStationID) {
        this.KMSStationID = KMSStationID;
    }

    public int getKMSEventTypeID() {
        return KMSEventTypeID;
    }

    public void setKMSEventTypeID(int KMSEventTypeID) {
        this.KMSEventTypeID = KMSEventTypeID;
    }

    public String getPATransactionID() {
        return PATransactionID;
    }

    public void setPATransactionID(String PATransactionID) {
        this.PATransactionID = PATransactionID;
    }

    public String getPATransLineItemID() {
        return PATransLineItemID;
    }

    public void setPATransLineItemID(String PATransLineItemID) {
        this.PATransLineItemID = PATransLineItemID;
    }

    public String getEventDTM() {
        return eventDTM;
    }

    public void setEventDTM(String eventDTM) {
        this.eventDTM = eventDTM;
    }

    public String getNOTE() {
        return NOTE;
    }

    public void setNOTE(String NOTE) {
        this.NOTE = NOTE;
    }

    public Integer getKMSOrderStatusID() {
        return KMSOrderStatusID;
    }

    public void setKMSOrderStatusID(Integer KMSOrderStatusID) {
        this.KMSOrderStatusID = KMSOrderStatusID;
    }
}
