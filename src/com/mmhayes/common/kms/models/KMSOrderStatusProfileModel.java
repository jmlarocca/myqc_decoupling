package com.mmhayes.common.kms.models;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-14 16:03:47 -0400 (Thu, 14 May 2020) $: Date of last commit
    $Rev: 11744 $: Revision of last commit
    Notes: Object representation of a KMS order status profile.
*/

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Object representation of a KMS order status profile.</p>
 *
 */
public class KMSOrderStatusProfileModel {

    // private member variables of a KMSOrderStatusProfileModel
    private int kmsOrderStatusProfileID = 0;
    private String name = "";
    private String description = "";
    private boolean active = false;
    private int ownershipGroupID = 0;
    private ArrayList<KMSOrderStatusProfileDetailModel> kmsOrderStatusProfileDetailModels = null;

    /**
     * <p>Constructs a {@link KMSOrderStatusProfileDetailModel} from the given kmsOrderStatusProfileID.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kmsOrderStatusProfileID ID of the order status profile to build the {@link KMSOrderStatusProfileModel} for.
     * @return A {@link KMSOrderStatusProfileModel} constructed from the given kmsOrderStatusProfileID.
     */
    @SuppressWarnings("unchecked")
    public static KMSOrderStatusProfileModel buildFromID (DataManager dataManager, String log, int kmsOrderStatusProfileID) {
        KMSOrderStatusProfileModel kmsOrderStatusProfileModel = new KMSOrderStatusProfileModel();

        try {
            if (kmsOrderStatusProfileID > 0) {
                // query the database to get the order timer profile record
                HashMap hm = new HashMap();
                ArrayList<HashMap> queryRes = dataManager.parameterizedExecuteQuery("data.kms.GetKMSOrderStatusProfileByID", new Object[]{kmsOrderStatusProfileID}, true);
                if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
                    hm = queryRes.get(0);
                }

                // convert the record to a KMSOrderStatusProfileModel object
                if (!DataFunctions.isEmptyMap(hm)) {
                    kmsOrderStatusProfileModel.kmsOrderStatusProfileID = HashMapDataFns.getIntVal(hm, "KMSORDERSTATUSPROFILEID");
                    kmsOrderStatusProfileModel.name = HashMapDataFns.getStringVal(hm, "NAME");
                    kmsOrderStatusProfileModel.description = HashMapDataFns.getStringVal(hm, "DESCRIPTION");
                    kmsOrderStatusProfileModel.active = HashMapDataFns.getBooleanVal(hm, "ACTIVE");
                    kmsOrderStatusProfileModel.ownershipGroupID = HashMapDataFns.getIntVal(hm, "OWNERSHIPGROUPID");

                    // get the order status profile details
                    if (StringFunctions.stringHasContent(HashMapDataFns.getStringVal(hm, "KMSORDERSTATUSPROFILEDETAILIDS"))) {
                        String[] kmsOrderStatusProfileIDStrs = HashMapDataFns.getStringVal(hm, "KMSORDERSTATUSPROFILEDETAILIDS").split("\\s*,\\s*", -1);
                        // convert the String array to an int array
                        if (kmsOrderStatusProfileIDStrs.length > 0) {
                            int[] kmsOrderStatusProfileIDs = new int[kmsOrderStatusProfileIDStrs.length];
                            for (int i = 0; i < kmsOrderStatusProfileIDStrs.length; i++) {
                                if ((StringFunctions.stringHasContent(kmsOrderStatusProfileIDStrs[i])) && (NumberUtils.isNumber(kmsOrderStatusProfileIDStrs[i]))) {
                                    kmsOrderStatusProfileIDs[i] = Integer.parseInt(kmsOrderStatusProfileIDStrs[i]);
                                }
                            }

                            if (kmsOrderStatusProfileIDs.length > 0) {
                                kmsOrderStatusProfileModel.kmsOrderStatusProfileDetailModels = KMSOrderStatusProfileDetailModel.buildFromIDs(dataManager, log, kmsOrderStatusProfileIDs);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build a KMSOrderStatusProfileModel object for the order status " +
                    "profile with an ID of %s in KMSOrderStatusProfileModel.buildFromID",
                    Objects.toString(kmsOrderStatusProfileID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return kmsOrderStatusProfileModel;
    }

    /**
     * <p>Constructs a {@link KMSOrderStatusProfileModel} using the given JSON {@link String}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param jsonString The JSON {@link String} to build a {@link KMSOrderStatusProfileModel} from.
     * @return A {@link KMSOrderStatusProfileModel} built from the given JSON {@link String}.
     */
    public static KMSOrderStatusProfileModel buildFromJSON (String log, String jsonString) {
        KMSOrderStatusProfileModel kmsOrderStatusProfileModel = new KMSOrderStatusProfileModel();

        try {
            if (StringFunctions.stringHasContent(jsonString)) {
                Gson gson = new Gson();
                JsonParser jsonParser = new JsonParser();
                JsonElement jsonElement = jsonParser.parse(jsonString);
                kmsOrderStatusProfileModel = gson.fromJson(jsonElement, KMSOrderStatusProfileModel.class);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build the KMSOrderStatusProfileModel from the JSON String %s in KMSOrderStatusProfileModel.buildFromJSON",
                    Objects.toString(jsonString, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return kmsOrderStatusProfileModel;
    }

    /**
     * <p>Overridden toString() method for a {@link KMSOrderStatusProfileModel}.</p>
     *
     * @return A {@link String} representation of this {@link KMSOrderStatusProfileModel}.
     */
    @Override
    public String toString () {

        // build a String of all the kmsOrderStatusProfileDetailModels
        String kmsOrderStatusProfileDetailModelsStr = "";
        if (!DataFunctions.isEmptyCollection(kmsOrderStatusProfileDetailModels)) {
            for (KMSOrderStatusProfileDetailModel kmsOrderStatusProfileDetailModel : kmsOrderStatusProfileDetailModels) {
                kmsOrderStatusProfileDetailModelsStr += "["+kmsOrderStatusProfileDetailModel.toString()+"]";
            }
        }

        return String .format("KMSORDERSTATUSPROFILEID: %s, NAME: %s, DESCRIPTION: %s, ACTIVE: %s, OWNERSHIPGROUPID: %s, KMSORDERSTATUSPROFILEDETAILMODELS: %s",
                Objects.toString((kmsOrderStatusProfileID != -1 ? kmsOrderStatusProfileID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString(active, "N/A"),
                Objects.toString((ownershipGroupID != -1 ? ownershipGroupID : "N/A"), "N/A"),
                Objects.toString((!DataFunctions.isEmptyCollection(kmsOrderStatusProfileDetailModels) ? kmsOrderStatusProfileDetailModelsStr : "N/A"), "N/A"));
        
    }

    /**
     * <p>Overridden equals() method for a {@link KMSOrderStatusProfileModel}.
     * Two {@link KMSOrderStatusProfileModel} are defined as being equal if they have the same kmsOrderStatusProfileID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KMSOrderStatusProfileModel}.
     * @return Whether or not the {@link Object} is equal to this {@link KMSOrderStatusProfileModel}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KMSOrderStatusProfileModel and check if the obj's kmsOrderStatusProfileID is equal to this KMSOrderStatusProfileModel's kmsOrderStatusProfileID
        KMSOrderStatusProfileModel kmsOrderStatusProfileModel = ((KMSOrderStatusProfileModel) obj);
        return Objects.equals(kmsOrderStatusProfileModel.kmsOrderStatusProfileID, kmsOrderStatusProfileID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link KMSOrderStatusProfileModel}.</p>
     *
     * @return The unique hash code for this {@link KMSOrderStatusProfileModel}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(kmsOrderStatusProfileID);
    }

    /**
     * <p>Getter for the kmsOrderStatusProfileID field of the {@link KMSOrderStatusProfileModel}.</p>
     *
     * @return The kmsOrderStatusProfileID field of the {@link KMSOrderStatusProfileModel}.
     */
    public int getKmsOrderStatusProfileID () {
        return kmsOrderStatusProfileID;
    }

    /**
     * <p>Setter for the kmsOrderStatusProfileID field of the {@link KMSOrderStatusProfileModel}.</p>
     *
     * @param kmsOrderStatusProfileID The kmsOrderStatusProfileID field of the {@link KMSOrderStatusProfileModel}.
     */
    public void setKmsOrderStatusProfileID (int kmsOrderStatusProfileID) {
        this.kmsOrderStatusProfileID = kmsOrderStatusProfileID;
    }

    /**
     * <p>Getter for the name field of the {@link KMSOrderStatusProfileModel}.</p>
     *
     * @return The name field of the {@link KMSOrderStatusProfileModel}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link KMSOrderStatusProfileModel}.</p>
     *
     * @param name The name field of the {@link KMSOrderStatusProfileModel}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the description field of the {@link KMSOrderStatusProfileModel}.</p>
     *
     * @return The description field of the {@link KMSOrderStatusProfileModel}.
     */
    public String getDescription () {
        return description;
    }

    /**
     * <p>Setter for the description field of the {@link KMSOrderStatusProfileModel}.</p>
     *
     * @param description The description field of the {@link KMSOrderStatusProfileModel}.
     */
    public void setDescription (String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the active field of the {@link KMSOrderStatusProfileModel}.</p>
     *
     * @return The active field of the {@link KMSOrderStatusProfileModel}.
     */
    public boolean getActive () {
        return active;
    }

    /**
     * <p>Setter for the active field of the {@link KMSOrderStatusProfileModel}.</p>
     *
     * @param active The active field of the {@link KMSOrderStatusProfileModel}.
     */
    public void setActive (boolean active) {
        this.active = active;
    }

    /**
     * <p>Getter for the ownershipGroupID field of the {@link KMSOrderStatusProfileModel}.</p>
     *
     * @return The ownershipGroupID field of the {@link KMSOrderStatusProfileModel}.
     */
    public int getOwnershipGroupID () {
        return ownershipGroupID;
    }

    /**
     * <p>Setter for the ownershipGroupID field of the {@link KMSOrderStatusProfileModel}.</p>
     *
     * @param ownershipGroupID The ownershipGroupID field of the {@link KMSOrderStatusProfileModel}.
     */
    public void setOwnershipGroupID (int ownershipGroupID) {
        this.ownershipGroupID = ownershipGroupID;
    }

    /**
     * <p>Getter for the kmsOrderStatusProfileDetailModels field of the {@link KMSOrderStatusProfileModel}.</p>
     *
     * @return The kmsOrderStatusProfileDetailModels field of the {@link KMSOrderStatusProfileModel}.
     */
    public ArrayList<KMSOrderStatusProfileDetailModel> getKmsOrderStatusProfileDetailModels () {
        return kmsOrderStatusProfileDetailModels;
    }

    /**
     * <p>Setter for the kmsOrderStatusProfileDetailModels field of the {@link KMSOrderStatusProfileModel}.</p>
     *
     * @param kmsOrderStatusProfileDetailModels The kmsOrderStatusProfileDetailModels field of the {@link KMSOrderStatusProfileModel}.
     */
    public void setKmsOrderStatusProfileDetailModels (ArrayList<KMSOrderStatusProfileDetailModel> kmsOrderStatusProfileDetailModels) {
        this.kmsOrderStatusProfileDetailModels = kmsOrderStatusProfileDetailModels;
    }
    
}