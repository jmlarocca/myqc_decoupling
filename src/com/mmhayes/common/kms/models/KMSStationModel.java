package com.mmhayes.common.kms.models;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-07-08 14:34:35 -0400 (Thu, 08 Jul 2021) $: Date of last commit
    $Rev: 14307 $: Revision of last commit
    Notes: Object representation of a KMS station.
*/

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Object representation of a KMS station.</p>
 *
 */
public class KMSStationModel {

    // private member variables of a KMSStationModel
    private int kmsStationID = 0;
    private String name = "";
    private String description = "";
    private boolean active = false;
    private int ownershipGroupID = 0;
    private int kmsStationTypeID = 0;
    private int kmsStationModeID = 0;
    private int kmsStationLayoutID = 0;
    private int numColsPerPage = 0;
    private boolean displayTitleBar = false;
    private boolean displayStationName = false;
    private boolean displayDateTime = false;
    private int titleBarAlignmentID = 0;
    private String logoFileName = "";
    private String logoTokenizedFileName = "";
    private String token = "";
    private boolean displayOrderNumber = false;
    private boolean displayCustomerName = false;
    private boolean displayLegend = false;
    private boolean displayRecent = false;
    private int mirrorKMSStationID = 0;
    private int backupKMSStationID = 0;
    private int titleStyleProfileID = 0;
    private int contentStyleProfileID = 0;
    private int rushOrderStyleProfileID = 0;
    private int orderStatusProfileID = 0;
    private int orderTimerProfileID = 0;
    private int completedOrderTimeDisplayed = 0;
    private boolean bumpProductsToCompleted = false;
    private boolean displayModsInProgress = false;
    private boolean bumpIndividualProducts = false;
    private int touchControlStyleProfileID = 0;
    private boolean printOrdersOnCompleted = false;
    private boolean displayOrderStatusSummary = false;
    private int printerID = 0;
    private KMSStyleProfileModel titleStyleProfile = null;
    private KMSStyleProfileModel contentStyleProfile = null;
    private KMSStyleProfileModel rushOrderStyleProfile = null;
    private KMSStyleProfileModel touchControlStyleProfile = null;
    private KMSOrderStatusProfileModel orderStatusProfile = null;
    private KMSOrderTimerProfileModel orderTimerProfile = null;
    //websocket config
    private int websocketPingFrequencyMS = 1000;
    private int websocketMissedPongMax = 3;
    private int offlineOrderTransferTimeMS = 60000;
    //required for the station/:kmsStationID endpoint
    private String MirrorStationName = "";
    private String ExpeditorStationName = "";
    private String BackupStationName = "";

    /**
     * <p>Constructs a new {@link KMSStationModel} from the given {@link HashMap}.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param hm A {@link HashMap} with data corresponding to the KMS station to build a {@link KMSStationModel} for.
     * @return The {@link KMSStationModel} built from the given {@link HashMap}.
     */
    public static KMSStationModel buildFromHM (DataManager dataManager, String log, HashMap hm) {
        KMSStationModel kmsStationModel = new KMSStationModel();

        try {
            // convert the record to a KMSStationModel object
            if (!DataFunctions.isEmptyMap(hm)) {
                kmsStationModel.kmsStationID = HashMapDataFns.getIntVal(hm, "KMSSTATIONID");
                kmsStationModel.name = HashMapDataFns.getStringVal(hm, "NAME");
                kmsStationModel.description = HashMapDataFns.getStringVal(hm, "DESCRIPTION");
                kmsStationModel.active = HashMapDataFns.getBooleanVal(hm, "ACTIVE");
                kmsStationModel.ownershipGroupID = HashMapDataFns.getIntVal(hm, "OWNERSHIPGROUPID");
                kmsStationModel.kmsStationTypeID = HashMapDataFns.getIntVal(hm, "KMSSTATIONTYPEID");
                kmsStationModel.kmsStationModeID = HashMapDataFns.getIntVal(hm, "KMSSTATIONMODEID");
                kmsStationModel.kmsStationLayoutID = HashMapDataFns.getIntVal(hm, "KMSSTATIONLAYOUTID");
                kmsStationModel.numColsPerPage = HashMapDataFns.getIntVal(hm, "NUMCOLSPERPAGE");
                kmsStationModel.displayTitleBar = HashMapDataFns.getBooleanVal(hm, "DISPLAYTITLEBAR");
                kmsStationModel.displayStationName = HashMapDataFns.getBooleanVal(hm, "DISPLAYSTATIONNAME");
                kmsStationModel.displayDateTime = HashMapDataFns.getBooleanVal(hm, "DISPLAYDATETIME");
                kmsStationModel.titleBarAlignmentID = HashMapDataFns.getIntVal(hm, "TITLEBARALIGNMENTID");
                kmsStationModel.logoFileName = HashMapDataFns.getStringVal(hm, "LOGOFILENAME");
                kmsStationModel.logoTokenizedFileName = HashMapDataFns.getStringVal(hm, "LOGOTOKENIZEDFILENAME");
                kmsStationModel.token = HashMapDataFns.getStringVal(hm, "TOKEN");
                kmsStationModel.displayOrderNumber = HashMapDataFns.getBooleanVal(hm, "DISPLAYORDERNUMBER");
                kmsStationModel.displayCustomerName = HashMapDataFns.getBooleanVal(hm, "DISPLAYCUSTOMERNAME");
                kmsStationModel.displayLegend = HashMapDataFns.getBooleanVal(hm, "DISPLAYLEGEND");
                kmsStationModel.displayRecent = HashMapDataFns.getBooleanVal(hm, "DISPLAYRECENT");
                kmsStationModel.mirrorKMSStationID = HashMapDataFns.getIntVal(hm, "MIRRORKMSSTATIONID");
                kmsStationModel.backupKMSStationID = HashMapDataFns.getIntVal(hm, "BACKUPKMSSTATIONID");
                kmsStationModel.titleStyleProfileID = HashMapDataFns.getIntVal(hm, "TITLESTYLEPROFILEID");
                kmsStationModel.contentStyleProfileID = HashMapDataFns.getIntVal(hm, "CONTENTSTYLEPROFILEID");
                kmsStationModel.rushOrderStyleProfileID = HashMapDataFns.getIntVal(hm, "RUSHORDERSTYLEPROFILEID");
                kmsStationModel.orderStatusProfileID = HashMapDataFns.getIntVal(hm, "ORDERSTATUSPROFILEID");
                kmsStationModel.orderTimerProfileID = HashMapDataFns.getIntVal(hm, "ORDERTIMERPROFILEID");
                kmsStationModel.completedOrderTimeDisplayed = HashMapDataFns.getIntVal(hm, "COMPLETEDORDERTIMEDISPLAYED");
                kmsStationModel.bumpProductsToCompleted = HashMapDataFns.getBooleanVal(hm, "BUMPPRODUCTSTOCOMPLETED");
                kmsStationModel.displayModsInProgress = HashMapDataFns.getBooleanVal(hm, "DISPLAYMODSINPROGRESS");
                kmsStationModel.bumpIndividualProducts = HashMapDataFns.getBooleanVal(hm, "BUMPINDIVIDUALPRODUCTS");
                kmsStationModel.touchControlStyleProfileID = HashMapDataFns.getIntVal(hm, "TOUCHCONTROLSTYLEPROFILEID");
                kmsStationModel.printOrdersOnCompleted = HashMapDataFns.getBooleanVal(hm, "PRINTORDERSONCOMPLETED");
                kmsStationModel.displayOrderStatusSummary = HashMapDataFns.getBooleanVal(hm, "DISPLAYORDERSTATUSSUMMARY");
                kmsStationModel.printerID = HashMapDataFns.getIntVal(hm, "PRINTERID");

                // get the title style profile
                ArrayList<KMSStyleProfileModel> titleStyleProfileModels = new ArrayList<>();
                if (kmsStationModel.titleStyleProfileID > 0) {
                    titleStyleProfileModels = KMSStyleProfileModel.buildFromIDs(dataManager, log, new int[]{kmsStationModel.titleStyleProfileID});
                }
                if (!DataFunctions.isEmptyCollection(titleStyleProfileModels)) {
                    kmsStationModel.titleStyleProfile = titleStyleProfileModels.get(0);
                }

                // get the content style profile
                ArrayList<KMSStyleProfileModel> contentStyleProfileModels = new ArrayList<>();
                if (kmsStationModel.contentStyleProfileID > 0) {
                    contentStyleProfileModels = KMSStyleProfileModel.buildFromIDs(dataManager, log, new int[]{kmsStationModel.contentStyleProfileID});
                }
                if (!DataFunctions.isEmptyCollection(contentStyleProfileModels)) {
                    kmsStationModel.contentStyleProfile = contentStyleProfileModels.get(0);
                }

                // get the rush order style profile
                ArrayList<KMSStyleProfileModel> rushOrderStyleProfileModels = new ArrayList<>();
                if (kmsStationModel.rushOrderStyleProfileID > 0) {
                    rushOrderStyleProfileModels = KMSStyleProfileModel.buildFromIDs(dataManager, log, new int[]{kmsStationModel.rushOrderStyleProfileID});
                }
                if (!DataFunctions.isEmptyCollection(rushOrderStyleProfileModels)) {
                    kmsStationModel.rushOrderStyleProfile = rushOrderStyleProfileModels.get(0);
                }

                // get the touch control style profile
                ArrayList<KMSStyleProfileModel> touchControlStyleProfileModels = new ArrayList<>();
                if (kmsStationModel.touchControlStyleProfileID > 0) {
                    touchControlStyleProfileModels = KMSStyleProfileModel.buildFromIDs(dataManager, log, new int[]{kmsStationModel.touchControlStyleProfileID});
                }
                if (!DataFunctions.isEmptyCollection(touchControlStyleProfileModels)) {
                    kmsStationModel.touchControlStyleProfile = touchControlStyleProfileModels.get(0);
                }

                // get the order status profile
                if (kmsStationModel.orderStatusProfileID > 0) {
                    kmsStationModel.orderStatusProfile = KMSOrderStatusProfileModel.buildFromID(dataManager, log, kmsStationModel.orderStatusProfileID);
                }

                // get the order timer profile
                if (kmsStationModel.orderTimerProfileID > 0) {
                    kmsStationModel.orderTimerProfile = KMSOrderTimerProfileModel.buildFromID(dataManager, log, kmsStationModel.orderTimerProfileID);
                }
                //get the Websocket stuff
                kmsStationModel.websocketPingFrequencyMS = Integer.parseInt(hm.getOrDefault("WEBSOCKETPINGFREQUENCYMS", "1000").toString());
                kmsStationModel.websocketMissedPongMax = Integer.parseInt(hm.getOrDefault("WEBSOCKETMISSEDPONGMAX", "3").toString());
                kmsStationModel.offlineOrderTransferTimeMS = Integer.parseInt(hm.getOrDefault("OFFLINEORDERTRANSFERTIMEMS", "60000").toString());
                // get the ID names
                kmsStationModel.MirrorStationName = HashMapDataFns.getStringVal(hm, "MIRRORSTATIONNAME");
                kmsStationModel.ExpeditorStationName = HashMapDataFns.getStringVal(hm, "EXPEDITORSTATIONNAME");
                kmsStationModel.BackupStationName = HashMapDataFns.getStringVal(hm, "BACKUPSTATIONNAME");
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build a KMSStationModel object with the given HashMap in KMSStationModel.buildFromHM", log, Logger.LEVEL.ERROR);
        }

        return kmsStationModel;
    }

    /**
     * <p>Constructs a {@link KMSStationModel} from the given kmsStationID.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kmsStationID ID of the KMS station to build the {@link KMSStationModel} for.
     * @return A {@link KMSStationModel} constructed from the given kmsStationID.
     */
    @SuppressWarnings("unchecked")
    public static KMSStationModel buildFromID (DataManager dataManager, String log, int kmsStationID) {
        KMSStationModel kmsStationModel = new KMSStationModel();

        try {
            if (kmsStationID > 0) {
                // query the database to get the KMS station record
                HashMap hm = new HashMap();
                ArrayList<HashMap> queryRes = dataManager.parameterizedExecuteQuery("data.kms.GetKMSStationByID", new Object[]{kmsStationID}, true);
                if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
                    hm = queryRes.get(0);
                }

                if (!DataFunctions.isEmptyMap(hm)) {
                    kmsStationModel = buildFromHM(dataManager, log, hm);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build a KMSStationModel object for the station with " +
                    "an ID of %s in KMSStationModel.buildFromID",
                    Objects.toString(kmsStationID, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return kmsStationModel;
    }

    /**
     * <p>Builds a {@link KMSStationModel} built from the given token {@link String}.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param token The token {@link String} to build the {@link KMSStationModel} from.
     * @return The {@link KMSStationModel} built from the given token {@link String}.
     */
    @SuppressWarnings("unchecked")
    public static KMSStationModel buildFromToken (DataManager dataManager, String log, String token) {
        KMSStationModel kmsStationModel = new KMSStationModel();

        try {
            if (StringFunctions.stringHasContent(token)) {
                ArrayList<HashMap> queryRes = dataManager.parameterizedExecuteQuery("data.kms.GetKMSStationByToken", new Object[]{token}, true);
                if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
                    kmsStationModel = buildFromHM(dataManager, log, queryRes.get(0));
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build a KMSStationModel object from the " +
                    "token of %s in KMSStationModel.buildFromToken",
                    Objects.toString(token, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return kmsStationModel;
    }

    /**
     * <p>Constructs a {@link KMSStationModel} using the given JSON {@link String}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param jsonString The JSON {@link String} to build a {@link KMSStationModel} from.
     * @return A {@link KMSStationModel} built from the given JSON {@link String}.
     */
    public static KMSStationModel buildFromJSON (String log, String jsonString) {
        KMSStationModel kmsStationModel = new KMSStationModel();

        try {
            if (StringFunctions.stringHasContent(jsonString)) {
                Gson gson = new Gson();
                JsonParser jsonParser = new JsonParser();
                JsonElement jsonElement = jsonParser.parse(jsonString);
                kmsStationModel = gson.fromJson(jsonElement, KMSStationModel.class);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build the KMSStationModel from the JSON String %s in KMSStationModel.buildFromJSON",
                    Objects.toString(jsonString, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return kmsStationModel;
    }


    /**
     * <p>Called through a REST endpoint to clear the token {@link String} for this {@link KMSStationModel}.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @return Whether or not the token {@link String} for this {@link KMSStationModel} could be cleared successfully.
     */
    public boolean clearTokenForKMSStation (DataManager dataManager, String log) {
        boolean result = false;

        try {
            if (dataManager.parameterizedExecuteNonQuery("data.kms.clearTokenByKMSStationID", new Object[]{kmsStationID}) > 0) {
                // we were able to successfully clear the token
                result = true;
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to clear the token for the KMS station %s in KMSStationModel.clearTokenForKMSStation",
                    Objects.toString(name, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return result;
    }

    /**
     * <p>Overridden toString() method for a {@link KMSStationModel}.</p>
     *
     * @return A {@link String} representation of this {@link KMSStationModel}.
     */
    @Override
    public String toString () {

        return String.format("KMSSTATIONID: %s, NAME: %s, DESCRIPTION: %s, ACTIVE: %s, OWNERSHIPGROUPID: %s, KMSSTATIONTYPEID: %s, KMSSTATIONMODEID: %s, " +
                "KMSSTATIONLAYOUTID: %s, NUMCOLSPERPAGE: %s, DISPLAYTITLEBAR: %s, DISPLAYSTATIONNAME: %s, DISPLAYDATETIME: %s, TITLEBARALIGNMENTID: %s, " +
                "LOGOFILENAME: %s, LOGOTOKENIZEDFILENAME: %s, TOKEN: %s, DISPLAYORDERNUMBER: %s, DISPLAYCUSTOMERNAME: %s, DISPLAYLEGEND: %s, DISPLAYRECENT: %s, " +
                "MIRRORKMSSTATIONID: %s, BACKUPKMSSTATIONID: %s, TITLESTYLEPROFILEID: %s, CONTENTSTYLEPROFILEID: %s, RUSHORDERSTYLEPROFILEID: %s, " +
                "ORDERSTATUSPROFILEID: %s, ORDERTIMERPROFILEID: %s, COMPLETEDORDERTIMEDISPLAYED: %s, BUMPPRODUCTSTOCOMPLETED: %s, DISPLAYMODSINPROGRESS: %s, " +
                "BUMPINDIVIDUALPRODUCTS: %s, TOUCHCONTROLSTYLEPROFILEID: %s, PRINTORDERSONCOMPLETED: %s, TITLESTYLEPROFILE: %s, CONTENTSTYLEPROFILE: %s, " +
                "RUSHORDERSTYLEPROFILE: %s, TOUCHCONTROLSTYLEPROFILE: %s, ORDERSTATUSPROFILE: %s, ORDERTIMERPROFILE: %s, MIRRORSTATIONNAME: %s, EPEDITORSTATIONNAME %s, BACKUPSTATIONNAME: %s" +
                "OFFLINEORDERTRANSFERTIMEMS: %s, WEBSOCKETPINGFREQUENCYMS: %s, WEBSOCKETMISSEDPONGMAX: %s",
                Objects.toString((kmsStationID != -1 ? kmsStationID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString(active, "N/A"),
                Objects.toString((ownershipGroupID != -1 ? ownershipGroupID : "N/A"), "N/A"),
                Objects.toString((kmsStationTypeID != -1 ? kmsStationTypeID : "N/A"), "N/A"),
                Objects.toString((kmsStationModeID != -1 ? kmsStationModeID : "N/A"), "N/A"),
                Objects.toString((kmsStationLayoutID != -1 ? kmsStationLayoutID : "N/A"), "N/A"),
                Objects.toString((numColsPerPage != -1 ? numColsPerPage : "N/A"), "N/A"),
                Objects.toString(displayTitleBar, "N/A"),
                Objects.toString(displayStationName, "N/A"),
                Objects.toString(displayDateTime, "N/A"),
                Objects.toString((titleBarAlignmentID != -1 ? titleBarAlignmentID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(logoFileName) ? logoFileName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(logoTokenizedFileName) ? logoTokenizedFileName : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(token) ? token : "N/A"), "N/A"),
                Objects.toString(displayOrderNumber, "N/A"),
                Objects.toString(displayCustomerName, "N/A"),
                Objects.toString(displayLegend, "N/A"),
                Objects.toString(displayRecent, "N/A"),
                Objects.toString((mirrorKMSStationID != -1 ? mirrorKMSStationID : "N/A"), "N/A"),
                Objects.toString((backupKMSStationID != -1 ? backupKMSStationID : "N/A"), "N/A"),
                Objects.toString((titleStyleProfileID != -1 ? titleStyleProfileID : "N/A"), "N/A"),
                Objects.toString((contentStyleProfileID != -1 ? contentStyleProfileID : "N/A"), "N/A"),
                Objects.toString((rushOrderStyleProfileID != -1 ? rushOrderStyleProfileID : "N/A"), "N/A"),
                Objects.toString((orderStatusProfileID != -1 ? orderStatusProfileID : "N/A"), "N/A"),
                Objects.toString((orderTimerProfileID != -1 ? orderTimerProfileID : "N/A"), "N/A"),
                Objects.toString((completedOrderTimeDisplayed != -1 ? completedOrderTimeDisplayed : "N/A"), "N/A"),
                Objects.toString(bumpProductsToCompleted, "N/A"),
                Objects.toString(displayModsInProgress, "N/A"),
                Objects.toString(bumpIndividualProducts, "N/A"),
                Objects.toString((touchControlStyleProfileID != -1 ? touchControlStyleProfileID : "N/A"), "N/A"),
                Objects.toString(printOrdersOnCompleted, "N/A"),
                Objects.toString((titleStyleProfile != null ? "["+titleStyleProfile.toString()+"]" : "N/A"), "N/A"),
                Objects.toString((contentStyleProfile != null ? "["+contentStyleProfile.toString()+"]" : "N/A"), "N/A"),
                Objects.toString((rushOrderStyleProfile != null ? "["+rushOrderStyleProfile.toString()+"]" : "N/A"), "N/A"),
                Objects.toString((touchControlStyleProfile != null ? "["+touchControlStyleProfile.toString()+"]" : "N/A"), "N/A"),
                Objects.toString((orderStatusProfile != null ? "["+orderStatusProfile.toString()+"]" : "N/A"), "N/A"),
                Objects.toString((orderTimerProfile != null ? "["+orderTimerProfile.toString()+"]" : "N/A"), "N/A"),
                Objects.toString(MirrorStationName, "N/A"),
                Objects.toString(ExpeditorStationName, "N/A"),
                Objects.toString(BackupStationName, "N/A"),
                Objects.toString(offlineOrderTransferTimeMS, "N/A"),
                Objects.toString(websocketPingFrequencyMS, "N/A"),
                Objects.toString(websocketMissedPongMax, "N/A")
        );

    }

    /**
     * <p>Overridden equals() method for a {@link KMSStationModel}.
     * Two {@link KMSStationModel} are defined as being equal if they have the same kmsStationID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KMSStationModel}.
     * @return Whether or not the {@link Object} is equal to this {@link KMSStationModel}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KMSStationModel and check if the obj's kmsStationID is equal to this KMSStationModel's kmsStationID
        KMSStationModel kmsStationModel = ((KMSStationModel) obj);
        return Objects.equals(kmsStationModel.kmsStationID, kmsStationID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link KMSStationModel}.</p>
     *
     * @return The unique hash code for this {@link KMSStationModel}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(kmsStationID);
    }

    /**
     * <p>Getter for the kmsStationID field of the {@link KMSStationModel}.</p>
     *
     * @return The kmsStationID field of the {@link KMSStationModel}.
     */
    public int getKmsStationID() {
        return kmsStationID;
    }

    /**
     * <p>Setter for the kmsStationID field of the {@link KMSStationModel}.</p>
     *
     * @param kmsStationID The kmsStationID field of the {@link KMSStationModel}.
     */
    public void setKmsStationID(int kmsStationID) {
        this.kmsStationID = kmsStationID;
    }

    /**
     * <p>Getter for the name field of the {@link KMSStationModel}.</p>
     *
     * @return The name field of the {@link KMSStationModel}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link KMSStationModel}.</p>
     *
     * @param name The name field of the {@link KMSStationModel}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the description field of the {@link KMSStationModel}.</p>
     *
     * @return The description field of the {@link KMSStationModel}.
     */
    public String getDescription () {
        return description;
    }

    /**
     * <p>Setter for the description field of the {@link KMSStationModel}.</p>
     *
     * @param description The description field of the {@link KMSStationModel}.
     */
    public void setDescription (String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the active field of the {@link KMSStationModel}.</p>
     *
     * @return The active field of the {@link KMSStationModel}.
     */
    public boolean getActive () {
        return active;
    }

    /**
     * <p>Setter for the active field of the {@link KMSStationModel}.</p>
     *
     * @param active The active field of the {@link KMSStationModel}.
     */
    public void setActive (boolean active) {
        this.active = active;
    }

    /**
     * <p>Getter for the ownershipGroupID field of the {@link KMSStationModel}.</p>
     *
     * @return The ownershipGroupID field of the {@link KMSStationModel}.
     */
    public int getOwnershipGroupID () {
        return ownershipGroupID;
    }

    /**
     * <p>Setter for the ownershipGroupID field of the {@link KMSStationModel}.</p>
     *
     * @param ownershipGroupID The ownershipGroupID field of the {@link KMSStationModel}.
     */
    public void setOwnershipGroupID (int ownershipGroupID) {
        this.ownershipGroupID = ownershipGroupID;
    }

    /**
     * <p>Getter for the kmsStationTypeID field of the {@link KMSStationModel}.</p>
     *
     * @return The kmsStationTypeID field of the {@link KMSStationModel}.
     */
    public int getKmsStationTypeID () {
        return kmsStationTypeID;
    }

    /**
     * <p>Setter for the kmsStationTypeID field of the {@link KMSStationModel}.</p>
     *
     * @param kmsStationTypeID The kmsStationTypeID field of the {@link KMSStationModel}.
     */
    public void setKmsStationTypeID (int kmsStationTypeID) {
        this.kmsStationTypeID = kmsStationTypeID;
    }

    /**
     * <p>Getter for the kmsStationModeID field of the {@link KMSStationModel}.</p>
     *
     * @return The kmsStationModeID field of the {@link KMSStationModel}.
     */
    public int getKmsStationModeID () {
        return kmsStationModeID;
    }

    /**
     * <p>Setter for the kmsStationModeID field of the {@link KMSStationModel}.</p>
     *
     * @param kmsStationModeID The kmsStationModeID field of the {@link KMSStationModel}.
     */
    public void setKmsStationModeID (int kmsStationModeID) {
        this.kmsStationModeID = kmsStationModeID;
    }

    /**
     * <p>Getter for the kmsStationLayoutID field of the {@link KMSStationModel}.</p>
     *
     * @return The kmsStationLayoutID field of the {@link KMSStationModel}.
     */
    public int getKmsStationLayoutID () {
        return kmsStationLayoutID;
    }

    /**
     * <p>Setter for the kmsStationLayoutID field of the {@link KMSStationModel}.</p>
     *
     * @param kmsStationLayoutID The kmsStationLayoutID field of the {@link KMSStationModel}.
     */
    public void setKmsStationLayoutID (int kmsStationLayoutID) {
        this.kmsStationLayoutID = kmsStationLayoutID;
    }

    /**
     * <p>Getter for the numColsPerPage field of the {@link KMSStationModel}.</p>
     *
     * @return The numColsPerPage field of the {@link KMSStationModel}.
     */
    public int getNumColsPerPage () {
        return numColsPerPage;
    }

    /**
     * <p>Setter for the numColsPerPage field of the {@link KMSStationModel}.</p>
     *
     * @param numColsPerPage The numColsPerPage field of the {@link KMSStationModel}.
     */
    public void setNumColsPerPage (int numColsPerPage) {
        this.numColsPerPage = numColsPerPage;
    }

    /**
     * <p>Getter for the displayTitleBar field of the {@link KMSStationModel}.</p>
     *
     * @return The displayTitleBar field of the {@link KMSStationModel}.
     */
    public boolean getDisplayTitleBar () {
        return displayTitleBar;
    }

    /**
     * <p>Setter for the displayTitleBar field of the {@link KMSStationModel}.</p>
     *
     * @param displayTitleBar The displayTitleBar field of the {@link KMSStationModel}.
     */
    public void setDisplayTitleBar (boolean displayTitleBar) {
        this.displayTitleBar = displayTitleBar;
    }

    /**
     * <p>Getter for the displayStationName field of the {@link KMSStationModel}.</p>
     *
     * @return The displayStationName field of the {@link KMSStationModel}.
     */
    public boolean getDisplayStationName () {
        return displayStationName;
    }

    /**
     * <p>Setter for the displayStationName field of the {@link KMSStationModel}.</p>
     *
     * @param displayStationName The displayStationName field of the {@link KMSStationModel}.
     */
    public void setDisplayStationName (boolean displayStationName) {
        this.displayStationName = displayStationName;
    }

    /**
     * <p>Getter for the displayDateTime field of the {@link KMSStationModel}.</p>
     *
     * @return The displayDateTime field of the {@link KMSStationModel}.
     */
    public boolean getDisplayDateTime () {
        return displayDateTime;
    }

    /**
     * <p>Setter for the displayDateTime field of the {@link KMSStationModel}.</p>
     *
     * @param displayDateTime The displayDateTime field of the {@link KMSStationModel}.
     */
    public void setDisplayDateTime (boolean displayDateTime) {
        this.displayDateTime = displayDateTime;
    }

    /**
     * <p>Getter for the titleBarAlignmentID field of the {@link KMSStationModel}.</p>
     *
     * @return The titleBarAlignmentID field of the {@link KMSStationModel}.
     */
    public int getTitleBarAlignmentID () {
        return titleBarAlignmentID;
    }

    /**
     * <p>Setter for the titleBarAlignmentID field of the {@link KMSStationModel}.</p>
     *
     * @param titleBarAlignmentID The titleBarAlignmentID field of the {@link KMSStationModel}.
     */
    public void setTitleBarAlignmentID (int titleBarAlignmentID) {
        this.titleBarAlignmentID = titleBarAlignmentID;
    }

    /**
     * <p>Getter for the logoFileName field of the {@link KMSStationModel}.</p>
     *
     * @return The logoFileName field of the {@link KMSStationModel}.
     */
    public String getLogoFileName () {
        return logoFileName;
    }

    /**
     * <p>Setter for the logoFileName field of the {@link KMSStationModel}.</p>
     *
     * @param logoFileName The logoFileName field of the {@link KMSStationModel}.
     */
    public void setLogoFileName (String logoFileName) {
        this.logoFileName = logoFileName;
    }

    /**
     * <p>Getter for the logoTokenizedFileName field of the {@link KMSStationModel}.</p>
     *
     * @return The logoTokenizedFileName field of the {@link KMSStationModel}.
     */
    public String getLogoTokenizedFileName () {
        return logoTokenizedFileName;
    }

    /**
     * <p>Setter for the logoTokenizedFileName field of the {@link KMSStationModel}.</p>
     *
     * @param logoTokenizedFileName The logoTokenizedFileName field of the {@link KMSStationModel}.
     */
    public void setLogoTokenizedFileName (String logoTokenizedFileName) {
        this.logoTokenizedFileName = logoTokenizedFileName;
    }

    /**
     * <p>Getter for the token field of the {@link KMSStationModel}.</p>
     *
     * @return The token field of the {@link KMSStationModel}.
     */
    public String getToken () {
        return token;
    }

    /**
     * <p>Setter for the token field of the {@link KMSStationModel}.</p>
     *
     * @param token The token field of the {@link KMSStationModel}.
     */
    public void setToken (String token) {
        this.token = token;
    }

    /**
     * <p>Getter for the displayOrderNumber field of the {@link KMSStationModel}.</p>
     *
     * @return The displayOrderNumber field of the {@link KMSStationModel}.
     */
    public boolean getDisplayOrderNumber () {
        return displayOrderNumber;
    }

    /**
     * <p>Setter for the displayOrderNumber field of the {@link KMSStationModel}.</p>
     *
     * @param displayOrderNumber The displayOrderNumber field of the {@link KMSStationModel}.
     */
    public void setDisplayOrderNumber (boolean displayOrderNumber) {
        this.displayOrderNumber = displayOrderNumber;
    }

    /**
     * <p>Getter for the displayCustomerName field of the {@link KMSStationModel}.</p>
     *
     * @return The displayCustomerName field of the {@link KMSStationModel}.
     */
    public boolean getDisplayCustomerName () {
        return displayCustomerName;
    }

    /**
     * <p>Setter for the displayCustomerName field of the {@link KMSStationModel}.</p>
     *
     * @param displayCustomerName The displayCustomerName field of the {@link KMSStationModel}.
     */
    public void setDisplayCustomerName (boolean displayCustomerName) {
        this.displayCustomerName = displayCustomerName;
    }

    /**
     * <p>Getter for the displayLegend field of the {@link KMSStationModel}.</p>
     *
     * @return The displayLegend field of the {@link KMSStationModel}.
     */
    public boolean getDisplayLegend () {
        return displayLegend;
    }

    /**
     * <p>Setter for the displayLegend field of the {@link KMSStationModel}.</p>
     *
     * @param displayLegend The displayLegend field of the {@link KMSStationModel}.
     */
    public void setDisplayLegend (boolean displayLegend) {
        this.displayLegend = displayLegend;
    }

    /**
     * <p>Getter for the displayRecent field of the {@link KMSStationModel}.</p>
     *
     * @return The displayRecent field of the {@link KMSStationModel}.
     */
    public boolean getDisplayRecent () {
        return displayRecent;
    }

    /**
     * <p>Setter for the displayRecent field of the {@link KMSStationModel}.</p>
     *
     * @param displayRecent The displayRecent field of the {@link KMSStationModel}.
     */
    public void setDisplayRecent (boolean displayRecent) {
        this.displayRecent = displayRecent;
    }

    /**
     * <p>Getter for the mirrorKMSStationID field of the {@link KMSStationModel}.</p>
     *
     * @return The mirrorKMSStationID field of the {@link KMSStationModel}.
     */
    public int getMirrorKMSStationID () {
        return mirrorKMSStationID;
    }

    /**
     * <p>Setter for the mirrorKMSStationID field of the {@link KMSStationModel}.</p>
     *
     * @param mirrorKMSStationID The mirrorKMSStationID field of the {@link KMSStationModel}.
     */
    public void setMirrorKMSStationID (int mirrorKMSStationID) {
        this.mirrorKMSStationID = mirrorKMSStationID;
    }

    /**
     * <p>Getter for the backupKMSStationID field of the {@link KMSStationModel}.</p>
     *
     * @return The backupKMSStationID field of the {@link KMSStationModel}.
     */
    public int getBackupKMSStationID () {
        return backupKMSStationID;
    }

    /**
     * <p>Setter for the backupKMSStationID field of the {@link KMSStationModel}.</p>
     *
     * @param backupKMSStationID The backupKMSStationID field of the {@link KMSStationModel}.
     */
    public void setBackupKMSStationID (int backupKMSStationID) {
        this.backupKMSStationID = backupKMSStationID;
    }

    /**
     * <p>Getter for the titleStyleProfileID field of the {@link KMSStationModel}.</p>
     *
     * @return The titleStyleProfileID field of the {@link KMSStationModel}.
     */
    public int getTitleStyleProfileID () {
        return titleStyleProfileID;
    }

    /**
     * <p>Setter for the titleStyleProfileID field of the {@link KMSStationModel}.</p>
     *
     * @param titleStyleProfileID The titleStyleProfileID field of the {@link KMSStationModel}.
     */
    public void setTitleStyleProfileID (int titleStyleProfileID) {
        this.titleStyleProfileID = titleStyleProfileID;
    }

    /**
     * <p>Getter for the contentStyleProfileID field of the {@link KMSStationModel}.</p>
     *
     * @return The contentStyleProfileID field of the {@link KMSStationModel}.
     */
    public int getContentStyleProfileID () {
        return contentStyleProfileID;
    }

    /**
     * <p>Setter for the contentStyleProfileID field of the {@link KMSStationModel}.</p>
     *
     * @param contentStyleProfileID The contentStyleProfileID field of the {@link KMSStationModel}.
     */
    public void setContentStyleProfileID (int contentStyleProfileID) {
        this.contentStyleProfileID = contentStyleProfileID;
    }

    /**
     * <p>Getter for the rushOrderStyleProfileID field of the {@link KMSStationModel}.</p>
     *
     * @return The rushOrderStyleProfileID field of the {@link KMSStationModel}.
     */
    public int getRushOrderStyleProfileID () {
        return rushOrderStyleProfileID;
    }

    /**
     * <p>Setter for the rushOrderStyleProfileID field of the {@link KMSStationModel}.</p>
     *
     * @param rushOrderStyleProfileID The rushOrderStyleProfileID field of the {@link KMSStationModel}.
     */
    public void setRushOrderStyleProfileID (int rushOrderStyleProfileID) {
        this.rushOrderStyleProfileID = rushOrderStyleProfileID;
    }

    /**
     * <p>Getter for the orderStatusProfileID field of the {@link KMSStationModel}.</p>
     *
     * @return The orderStatusProfileID field of the {@link KMSStationModel}.
     */
    public int getOrderStatusProfileID () {
        return orderStatusProfileID;
    }

    /**
     * <p>Setter for the orderStatusProfileID field of the {@link KMSStationModel}.</p>
     *
     * @param orderStatusProfileID The orderStatusProfileID field of the {@link KMSStationModel}.
     */
    public void setOrderStatusProfileID (int orderStatusProfileID) {
        this.orderStatusProfileID = orderStatusProfileID;
    }

    /**
     * <p>Getter for the orderTimerProfileID field of the {@link KMSStationModel}.</p>
     *
     * @return The orderTimerProfileID field of the {@link KMSStationModel}.
     */
    public int getOrderTimerProfileID () {
        return orderTimerProfileID;
    }

    /**
     * <p>Setter for the orderTimerProfileID field of the {@link KMSStationModel}.</p>
     *
     * @param orderTimerProfileID The orderTimerProfileID field of the {@link KMSStationModel}.
     */
    public void setOrderTimerProfileID (int orderTimerProfileID) {
        this.orderTimerProfileID = orderTimerProfileID;
    }

    /**
     * <p>Getter for the completedOrderTimeDisplayed field of the {@link KMSStationModel}.</p>
     *
     * @return The completedOrderTimeDisplayed field of the {@link KMSStationModel}.
     */
    public int getCompletedOrderTimeDisplayed () {
        return completedOrderTimeDisplayed;
    }

    /**
     * <p>Setter for the completedOrderTimeDisplayed field of the {@link KMSStationModel}.</p>
     *
     * @param completedOrderTimeDisplayed The completedOrderTimeDisplayed field of the {@link KMSStationModel}.
     */
    public void setCompletedOrderTimeDisplayed (int completedOrderTimeDisplayed) {
        this.completedOrderTimeDisplayed = completedOrderTimeDisplayed;
    }

    /**
     * <p>Getter for the bumpProductsToCompleted field of the {@link KMSStationModel}.</p>
     *
     * @return The bumpProductsToCompleted field of the {@link KMSStationModel}.
     */
    public boolean getBumpProductsToCompleted () {
        return bumpProductsToCompleted;
    }

    /**
     * <p>Setter for the bumpProductsToCompleted field of the {@link KMSStationModel}.</p>
     *
     * @param bumpProductsToCompleted The bumpProductsToCompleted field of the {@link KMSStationModel}.
     */
    public void setBumpProductsToCompleted (boolean bumpProductsToCompleted) {
        this.bumpProductsToCompleted = bumpProductsToCompleted;
    }

    /**
     * <p>Getter for the displayModsInProgress field of the {@link KMSStationModel}.</p>
     *
     * @return The displayModsInProgress field of the {@link KMSStationModel}.
     */
    public boolean getDisplayModsInProgress () {
        return displayModsInProgress;
    }

    /**
     * <p>Setter for the displayModsInProgress field of the {@link KMSStationModel}.</p>
     *
     * @param displayModsInProgress The displayModsInProgress field of the {@link KMSStationModel}.
     */
    public void setDisplayModsInProgress (boolean displayModsInProgress) {
        this.displayModsInProgress = displayModsInProgress;
    }

    /**
     * <p>Getter for the bumpIndividualProducts field of the {@link KMSStationModel}.</p>
     *
     * @return The bumpIndividualProducts field of the {@link KMSStationModel}.
     */
    public boolean getBumpIndividualProducts () {
        return bumpIndividualProducts;
    }

    /**
     * <p>Setter for the bumpIndividualProducts field of the {@link KMSStationModel}.</p>
     *
     * @param bumpIndividualProducts The bumpIndividualProducts field of the {@link KMSStationModel}.
     */
    public void setBumpIndividualProducts (boolean bumpIndividualProducts) {
        this.bumpIndividualProducts = bumpIndividualProducts;
    }

    /**
     * <p>Getter for the touchControlStyleProfileID field of the {@link KMSStationModel}.</p>
     *
     * @return The touchControlStyleProfileID field of the {@link KMSStationModel}.
     */
    public int getTouchControlStyleProfileID () {
        return touchControlStyleProfileID;
    }

    /**
     * <p>Setter for the touchControlStyleProfileID field of the {@link KMSStationModel}.</p>
     *
     * @param touchControlStyleProfileID The touchControlStyleProfileID field of the {@link KMSStationModel}.
     */
    public void setTouchControlStyleProfileID (int touchControlStyleProfileID) {
        this.touchControlStyleProfileID = touchControlStyleProfileID;
    }

    /**
     * <p>Getter for the printOrdersOnCompleted field of the {@link KMSStationModel}.</p>
     *
     * @return The printOrdersOnCompleted field of the {@link KMSStationModel}.
     */
    public boolean getPrintOrdersOnCompleted () {
        return printOrdersOnCompleted;
    }

    /**
     * <p>Setter for the printOrdersOnCompleted field of the {@link KMSStationModel}.</p>
     *
     * @param printOrdersOnCompleted The printOrdersOnCompleted field of the {@link KMSStationModel}.
     */
    public void setPrintOrdersOnCompleted (boolean printOrdersOnCompleted) {
        this.printOrdersOnCompleted = printOrdersOnCompleted;
    }

    /**
     * <p>Getter for the titleStyleProfile field of the {@link KMSStationModel}.</p>
     *
     * @return The titleStyleProfile field of the {@link KMSStationModel}.
     */
    public KMSStyleProfileModel getTitleStyleProfile () {
        return titleStyleProfile;
    }

    /**
     * <p>Setter for the titleStyleProfile field of the {@link KMSStationModel}.</p>
     *
     * @param titleStyleProfile The titleStyleProfile field of the {@link KMSStationModel}.
     */
    public void setTitleStyleProfile (KMSStyleProfileModel titleStyleProfile) {
        this.titleStyleProfile = titleStyleProfile;
    }

    /**
     * <p>Getter for the contentStyleProfile field of the {@link KMSStationModel}.</p>
     *
     * @return The contentStyleProfile field of the {@link KMSStationModel}.
     */
    public KMSStyleProfileModel getContentStyleProfile () {
        return contentStyleProfile;
    }

    /**
     * <p>Setter for the contentStyleProfile field of the {@link KMSStationModel}.</p>
     *
     * @param contentStyleProfile The contentStyleProfile field of the {@link KMSStationModel}.
     */
    public void setContentStyleProfile (KMSStyleProfileModel contentStyleProfile) {
        this.contentStyleProfile = contentStyleProfile;
    }

    /**
     * <p>Getter for the rushOrderStyleProfile field of the {@link KMSStationModel}.</p>
     *
     * @return The rushOrderStyleProfile field of the {@link KMSStationModel}.
     */
    public KMSStyleProfileModel getRushOrderStyleProfile () {
        return rushOrderStyleProfile;
    }

    /**
     * <p>Setter for the rushOrderStyleProfile field of the {@link KMSStationModel}.</p>
     *
     * @param rushOrderStyleProfile The rushOrderStyleProfile field of the {@link KMSStationModel}.
     */
    public void setRushOrderStyleProfile (KMSStyleProfileModel rushOrderStyleProfile) {
        this.rushOrderStyleProfile = rushOrderStyleProfile;
    }

    /**
     * <p>Getter for the touchControlStyleProfile field of the {@link KMSStationModel}.</p>
     *
     * @return The touchControlStyleProfile field of the {@link KMSStationModel}.
     */
    public KMSStyleProfileModel getTouchControlStyleProfile () {
        return touchControlStyleProfile;
    }

    /**
     * <p>Setter for the touchControlStyleProfile field of the {@link KMSStationModel}.</p>
     *
     * @param touchControlStyleProfile The touchControlStyleProfile field of the {@link KMSStationModel}.
     */
    public void setTouchControlStyleProfile (KMSStyleProfileModel touchControlStyleProfile) {
        this.touchControlStyleProfile = touchControlStyleProfile;
    }

    /**
     * <p>Getter for the orderStatusProfile field of the {@link KMSStationModel}.</p>
     *
     * @return The orderStatusProfile field of the {@link KMSStationModel}.
     */
    public KMSOrderStatusProfileModel getOrderStatusProfile () {
        return orderStatusProfile;
    }

    /**
     * <p>Setter for the orderStatusProfile field of the {@link KMSStationModel}.</p>
     *
     * @param orderStatusProfile The orderStatusProfile field of the {@link KMSStationModel}.
     */
    public void setOrderStatusProfile (KMSOrderStatusProfileModel orderStatusProfile) {
        this.orderStatusProfile = orderStatusProfile;
    }

    /**
     * <p>Getter for the orderTimerProfile field of the {@link KMSStationModel}.</p>
     *
     * @return The orderTimerProfile field of the {@link KMSStationModel}.
     */
    public KMSOrderTimerProfileModel getOrderTimerProfile () {
        return orderTimerProfile;
    }

    /**
     * <p>Setter for the orderTimerProfile field of the {@link KMSStationModel}.</p>
     *
     * @param orderTimerProfile The orderTimerProfile field of the {@link KMSStationModel}.
     */
    public void setOrderTimerProfile (KMSOrderTimerProfileModel orderTimerProfile) {
        this.orderTimerProfile = orderTimerProfile;
    }

    public String getMirrorStationName() {
        return MirrorStationName;
    }

    public void setMirrorStationName(String mirrorStationName) {
        MirrorStationName = mirrorStationName;
    }

    public String getExpeditorStationName() {
        return ExpeditorStationName;
    }

    public void setExpeditorStationName(String expeditorStationName) {
        ExpeditorStationName = expeditorStationName;
    }

    public String getBackupStationName() {
        return BackupStationName;
    }

    public void setBackupStationName(String backupStationName) {
        BackupStationName = backupStationName;
    }


    public int getPrinterID() {
        return printerID;
    }

    public void setPrinterID(int printerID) {
        this.printerID = printerID;
    }
    public long getWebsocketPingFrequencyMS() {
        return websocketPingFrequencyMS;
    }

    public void setWebsocketPingFrequencyMS(int websocketPingFrequencyMS) {
        this.websocketPingFrequencyMS = websocketPingFrequencyMS;
    }

    public int getWebsocketMissedPongMax() {
        return websocketMissedPongMax;
    }

    public void setWebsocketMissedPongMax(int websocketMissedPongMax) {
        this.websocketMissedPongMax = websocketMissedPongMax;
    }

    public int getOfflineOrderTransferTimeMS() {
        return offlineOrderTransferTimeMS;
    }

    public void setOfflineOrderTransferTimeMS(int offlineOrderTransferTimeMS) {
        this.offlineOrderTransferTimeMS = offlineOrderTransferTimeMS;
    }

    public boolean isDisplayOrderStatusSummary() {
        return displayOrderStatusSummary;
    }

    public void setDisplayOrderStatusSummary(boolean displayOrderStatusSummary) {
        this.displayOrderStatusSummary = displayOrderStatusSummary;
    }
}