package com.mmhayes.common.kms.models;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class KMSInfoModel {
    public String originatingKMSStationId;
    public int originalHandlingKMSStationId;
    public String operation;
    public String reference;
    public String message;
}
