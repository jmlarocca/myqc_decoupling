package com.mmhayes.common.kms.models;

/*
    Last Updated (automatically updated by SVN)
    $Author: crking $: Author of last commit
    $Date: 2020-05-13 16:12:43 -0400 (Wed, 13 May 2020) $: Date of last commit
    $Rev: 11731 $: Revision of last commit
    Notes: Object representation of a KMS order timer profile.
*/

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Object representation of a KMS order timer profile.</p>
 *
 */
public class KMSOrderTimerProfileModel {

    // private member variables of a KMSOrderTimerProfileModel
    private int kmsOrderTimerProfileID = 0;
    private String name = "";
    private String description = "";
    private boolean active = false;
    private int ownershipGroupID = 0;
    private ArrayList<KMSOrderTimerProfileDetailModel> kmsOrderTimerProfileDetailModels = null;

    /**
     * <p>Constructs a {@link KMSOrderTimerProfileDetailModel} from the given kmsOrderTimerProfileID.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kmsOrderTimerProfileID ID of the order timer profile to build the {@link KMSOrderTimerProfileModel} for.
     * @return A {@link KMSOrderTimerProfileModel} constructed from the given kmsOrderTimerProfileID.
     */
    @SuppressWarnings("unchecked")
    public static KMSOrderTimerProfileModel buildFromID (DataManager dataManager, String log, int kmsOrderTimerProfileID) {
        KMSOrderTimerProfileModel kmsOrderTimerProfileModel = new KMSOrderTimerProfileModel();
        
        try {
            if (kmsOrderTimerProfileID > 0) {
                // query the database to get the order timer profile record
                HashMap hm = new HashMap();
                ArrayList<HashMap> queryRes = dataManager.parameterizedExecuteQuery("data.kms.GetKMSOrderTimerProfileByID", new Object[]{kmsOrderTimerProfileID}, true);
                if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
                    hm = queryRes.get(0);
                }

                // convert the record to a KMSOrderTimerProfileModel object
                if (!DataFunctions.isEmptyMap(hm)) {
                    kmsOrderTimerProfileModel.kmsOrderTimerProfileID = HashMapDataFns.getIntVal(hm, "KMSORDERTIMERPROFILEID");
                    kmsOrderTimerProfileModel.name = HashMapDataFns.getStringVal(hm, "NAME");
                    kmsOrderTimerProfileModel.description = HashMapDataFns.getStringVal(hm, "DESCRIPTION");
                    kmsOrderTimerProfileModel.active = HashMapDataFns.getBooleanVal(hm, "ACTIVE");
                    kmsOrderTimerProfileModel.ownershipGroupID = HashMapDataFns.getIntVal(hm, "OWNERSHIPGROUPID");

                    // get the order timer profile details
                    if (StringFunctions.stringHasContent(HashMapDataFns.getStringVal(hm, "KMSORDERTIMERPROFILEDETAILIDS"))) {
                        String[] kmsOrderTimerProfileIDStrs = HashMapDataFns.getStringVal(hm, "KMSORDERTIMERPROFILEDETAILIDS").split("\\s*,\\s*", -1);
                        // convert the String array to an int array
                        if (kmsOrderTimerProfileIDStrs.length > 0) {
                            int[] kmsOrderTimerProfileIDs = new int[kmsOrderTimerProfileIDStrs.length];
                            for (int i = 0; i < kmsOrderTimerProfileIDStrs.length; i++) {
                                if ((StringFunctions.stringHasContent(kmsOrderTimerProfileIDStrs[i])) && (NumberUtils.isNumber(kmsOrderTimerProfileIDStrs[i]))) {
                                    kmsOrderTimerProfileIDs[i] = Integer.parseInt(kmsOrderTimerProfileIDStrs[i]);
                                }
                            }

                            if (kmsOrderTimerProfileIDs.length > 0) {
                                kmsOrderTimerProfileModel.kmsOrderTimerProfileDetailModels = KMSOrderTimerProfileDetailModel.buildFromIDs(dataManager, log, kmsOrderTimerProfileIDs);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build a KMSOrderTimerProfileModel object for the order timer " +
                    "profile with an ID of %s in KMSOrderTimerProfileModel.buildFromID",
                    Objects.toString(kmsOrderTimerProfileID, "N/A")), log, Logger.LEVEL.ERROR);
        }
        
        return kmsOrderTimerProfileModel;
    }

    /**
     * <p>Constructs a {@link KMSOrderTimerProfileModel} using the given JSON {@link String}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param jsonString The JSON {@link String} to build a {@link KMSOrderTimerProfileModel} from.
     * @return A {@link KMSOrderTimerProfileModel} built from the given JSON {@link String}.
     */
    public static KMSOrderTimerProfileModel buildFromJSON (String log, String jsonString) {
        KMSOrderTimerProfileModel kmsOrderTimerProfileModel = new KMSOrderTimerProfileModel();

        try {
            if (StringFunctions.stringHasContent(jsonString)) {
                Gson gson = new Gson();
                JsonParser jsonParser = new JsonParser();
                JsonElement jsonElement = jsonParser.parse(jsonString);
                kmsOrderTimerProfileModel = gson.fromJson(jsonElement, KMSOrderTimerProfileModel.class);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build the KMSOrderTimerProfileModel from the JSON String %s in KMSOrderTimerProfileModel.buildFromJSON",
                    Objects.toString(jsonString, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return kmsOrderTimerProfileModel;
    }



    /**
     * <p>Overridden toString() method for a {@link KMSOrderTimerProfileModel}.</p>
     *
     * @return A {@link String} representation of this {@link KMSOrderTimerProfileModel}.
     */
    @Override
    public String toString () {

        // build a String of all the kmsOrderTimerProfileDetailModels
        String kmsOrderTimerProfileDetailModelsStr = "";
        if (!DataFunctions.isEmptyCollection(kmsOrderTimerProfileDetailModels)) {
            for (KMSOrderTimerProfileDetailModel kmsOrderTimerProfileDetailModel : kmsOrderTimerProfileDetailModels) {
                kmsOrderTimerProfileDetailModelsStr += "["+kmsOrderTimerProfileDetailModel.toString()+"]";
            }
        }

        return String .format("KMSORDERTIMERPROFILEID: %s, NAME: %s, DESCRIPTION: %s, ACTIVE: %s, OWNERSHIPGROUPID: %s, KMSORDERTIMERPROFILEDETAILMODELS: %s",
                Objects.toString((kmsOrderTimerProfileID != -1 ? kmsOrderTimerProfileID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(description) ? description : "N/A"), "N/A"),
                Objects.toString(active, "N/A"),
                Objects.toString((ownershipGroupID != -1 ? ownershipGroupID : "N/A"), "N/A"),
                Objects.toString((!DataFunctions.isEmptyCollection(kmsOrderTimerProfileDetailModels) ? kmsOrderTimerProfileDetailModelsStr : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link KMSOrderTimerProfileModel}.
     * Two {@link KMSOrderTimerProfileModel} are defined as being equal if they have the same kmsOrderTimerProfileID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KMSOrderTimerProfileModel}.
     * @return Whether or not the {@link Object} is equal to this {@link KMSOrderTimerProfileModel}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KMSOrderTimerProfileModel and check if the obj's kmsOrderTimerProfileID is equal to this KMSOrderTimerProfileModel's kmsOrderTimerProfileID
        KMSOrderTimerProfileModel kmsOrderTimerProfileModel = ((KMSOrderTimerProfileModel) obj);
        return Objects.equals(kmsOrderTimerProfileModel.kmsOrderTimerProfileID, kmsOrderTimerProfileID);
    }
    
    /**
     * <p>Overridden hashCode() method for a {@link KMSOrderTimerProfileModel}.</p>
     *
     * @return The unique hash code for this {@link KMSOrderTimerProfileModel}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(kmsOrderTimerProfileID);
    }

    /**
     * <p>Getter for the kmsOrderTimerProfileID field of the {@link KMSOrderTimerProfileModel}.</p>
     *
     * @return The kmsOrderTimerProfileID field of the {@link KMSOrderTimerProfileModel}.
     */
    public int getKmsOrderTimerProfileID () {
        return kmsOrderTimerProfileID;
    }

    /**
     * <p>Setter for the kmsOrderTimerProfileID field of the {@link KMSOrderTimerProfileModel}.</p>
     *
     * @param kmsOrderTimerProfileID The kmsOrderTimerProfileID field of the {@link KMSOrderTimerProfileModel}.
     */
    public void setKmsOrderTimerProfileID (int kmsOrderTimerProfileID) {
        this.kmsOrderTimerProfileID = kmsOrderTimerProfileID;
    }

    /**
     * <p>Getter for the name field of the {@link KMSOrderTimerProfileModel}.</p>
     *
     * @return The name field of the {@link KMSOrderTimerProfileModel}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link KMSOrderTimerProfileModel}.</p>
     *
     * @param name The name field of the {@link KMSOrderTimerProfileModel}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the description field of the {@link KMSOrderTimerProfileModel}.</p>
     *
     * @return The description field of the {@link KMSOrderTimerProfileModel}.
     */
    public String getDescription () {
        return description;
    }

    /**
     * <p>Setter for the description field of the {@link KMSOrderTimerProfileModel}.</p>
     *
     * @param description The description field of the {@link KMSOrderTimerProfileModel}.
     */
    public void setDescription (String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the active field of the {@link KMSOrderTimerProfileModel}.</p>
     *
     * @return The active field of the {@link KMSOrderTimerProfileModel}.
     */
    public boolean getActive () {
        return active;
    }

    /**
     * <p>Setter for the active field of the {@link KMSOrderTimerProfileModel}.</p>
     *
     * @param active The active field of the {@link KMSOrderTimerProfileModel}.
     */
    public void setActive (boolean active) {
        this.active = active;
    }

    /**
     * <p>Getter for the ownershipGroupID field of the {@link KMSOrderTimerProfileModel}.</p>
     *
     * @return The ownershipGroupID field of the {@link KMSOrderTimerProfileModel}.
     */
    public int getOwnershipGroupID () {
        return ownershipGroupID;
    }

    /**
     * <p>Setter for the ownershipGroupID field of the {@link KMSOrderTimerProfileModel}.</p>
     *
     * @param ownershipGroupID The ownershipGroupID field of the {@link KMSOrderTimerProfileModel}.
     */
    public void setOwnershipGroupID (int ownershipGroupID) {
        this.ownershipGroupID = ownershipGroupID;
    }

    /**
     * <p>Getter for the kmsOrderTimerProfileDetailModels field of the {@link KMSOrderTimerProfileModel}.</p>
     *
     * @return The kmsOrderTimerProfileDetailModels field of the {@link KMSOrderTimerProfileModel}.
     */
    public ArrayList<KMSOrderTimerProfileDetailModel> getKmsOrderTimerProfileDetailModels () {
        return kmsOrderTimerProfileDetailModels;
    }

    /**
     * <p>Setter for the kmsOrderTimerProfileDetailModels field of the {@link KMSOrderTimerProfileModel}.</p>
     *
     * @param kmsOrderTimerProfileDetailModels The kmsOrderTimerProfileDetailModels field of the {@link KMSOrderTimerProfileModel}.
     */
    public void setKmsOrderTimerProfileDetailModels (ArrayList<KMSOrderTimerProfileDetailModel> kmsOrderTimerProfileDetailModels) {
        this.kmsOrderTimerProfileDetailModels = kmsOrderTimerProfileDetailModels;
    }
    
}