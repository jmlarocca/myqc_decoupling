package com.mmhayes.common.kms.models;

import redstone.xmlrpc.XmlRpcStruct;

import java.util.HashMap;
import java.util.Objects;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class KMSStatusUpdateModel {
    public static final int NOT_SENT = 0;
    public static final int SENT_TO_SERVER = 1;
    public static final int SAVED_IN_SERVER_DB = 2;

    public KMSStatusModel status;
    public Long updateTime;
    public int currentUpdateLocation;//This field must not be in equals or hashcode. //0 not sent to server, 1: send to server but not updated in DB, 2:updated in DB

    public KMSStatusUpdateModel(){

    }

    public KMSStatusUpdateModel(XmlRpcStruct struct){
        if(struct == null)
            return;
        if(struct.containsKey("updateTime"))
            updateTime = Long.parseLong(struct.get("updateTime").toString());
        if(struct.containsKey("currentUpdateLocation"))
            currentUpdateLocation = struct.getInteger("currentUpdateLocation");

        status = new KMSStatusModel(struct);
    }

    public boolean updateIsForSameItem(KMSStatusModel other){
        return this.status.PATransLineItemID == other.PATransLineItemID && this.status.PATransactionID == other.PATransactionID;
    }

    public HashMap serialize(){
        HashMap hm = new HashMap();
        hm.put("currentUpdateLocation", currentUpdateLocation);
        if(updateTime != null)
            hm.put("updateTime", updateTime + "");
        if(status != null)
            hm.putAll(status.serialize());
        return hm;
    }

    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a CreditCardTxnData and check if the obj's ccTxnInfo is equal to this CreditCardTxnData's ccTxnInfo
        KMSStatusUpdateModel other = ((KMSStatusUpdateModel) obj);
        return Objects.equals(other.status, status)
                && Objects.equals(other.updateTime, updateTime);
    }

    @Override
    public int hashCode () {
        return Objects.hash(status.hashCode() + updateTime);
    }
}
