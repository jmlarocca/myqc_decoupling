package com.mmhayes.common.kms.models;

/*
    Last Updated (automatically updated by SVN)
    $Author: crking $: Author of last commit
    $Date: 2020-05-13 16:12:43 -0400 (Wed, 13 May 2020) $: Date of last commit
    $Rev: 11731 $: Revision of last commit
    Notes: Object representation of a KMS order timer profile detail.
*/

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Object representation of a KMS order timer profile detail.</p>
 *
 */
public class KMSOrderTimerProfileDetailModel {

    // private member variables of a KMSOrderTimerProfileDetailModel
    private int kmsOrderTimerProfileDetailID = 0;
    private int kmsOrderTimerProfileID = 0;
    private int styleProfileID = 0;
    private String name = "";
    private int time = 0;
    private KMSStyleProfileModel kmsStyleProfileModel = null;

    /**
     * <p>Constructs a new {@link KMSOrderTimerProfileDetailModel} from the given {@link HashMap}.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param hm A {@link HashMap} with data corresponding to the order timer profile detail to build a {@link KMSOrderTimerProfileDetailModel} for.
     * @return The {@link KMSOrderTimerProfileDetailModel} built from the given {@link HashMap}.
     */
    public static KMSOrderTimerProfileDetailModel buildFromHM (DataManager dataManager, String log, HashMap hm) {
        KMSOrderTimerProfileDetailModel kmsOrderTimerProfileDetailModel = new KMSOrderTimerProfileDetailModel();

        try {
            if (!DataFunctions.isEmptyMap(hm)) {
                kmsOrderTimerProfileDetailModel.kmsOrderTimerProfileDetailID = HashMapDataFns.getIntVal(hm, "KMSORDERTIMERPROFILEDETAILID");
                kmsOrderTimerProfileDetailModel.kmsOrderTimerProfileID = HashMapDataFns.getIntVal(hm, "KMSORDERTIMERPROFILEID");
                kmsOrderTimerProfileDetailModel.styleProfileID = HashMapDataFns.getIntVal(hm, "STYLEPROFILEID");
                kmsOrderTimerProfileDetailModel.name = HashMapDataFns.getStringVal(hm, "NAME");
                kmsOrderTimerProfileDetailModel.time = HashMapDataFns.getIntVal(hm, "TIME");

                // get the style profile
                ArrayList<KMSStyleProfileModel> styleProfileModels = new ArrayList<>();
                if (kmsOrderTimerProfileDetailModel.styleProfileID > 0) {
                    styleProfileModels = KMSStyleProfileModel.buildFromIDs(dataManager, log, new int[]{kmsOrderTimerProfileDetailModel.styleProfileID});
                }
                if (!DataFunctions.isEmptyCollection(styleProfileModels)) {
                    kmsOrderTimerProfileDetailModel.kmsStyleProfileModel = styleProfileModels.get(0);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build a KMSOrderTimerProfileDetailModel object with the given HashMap in KMSOrderTimerProfileDetailModel.buildFromHM", log, Logger.LEVEL.ERROR);
        }

        return kmsOrderTimerProfileDetailModel;
    }

    /**
     * <p>Constructs an {@link ArrayList} of {@link KMSOrderTimerProfileDetailModel} from the given kmsOrderTimerProfileDetailIDs.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kmsOrderTimerProfileDetailIDs IDs of the order timer profile details to build {@link KMSOrderTimerProfileDetailModel} objects for.
     * @return An {@link ArrayList} of {@link KMSOrderTimerProfileDetailModel} constructed from the given kmsOrderTimerProfileDetailIDs.
     */
    @SuppressWarnings("Convert2streamapi")
    public static ArrayList<KMSOrderTimerProfileDetailModel> buildFromIDs (DataManager dataManager, String log, int[] kmsOrderTimerProfileDetailIDs) {
        ArrayList<KMSOrderTimerProfileDetailModel> kmsOrderTimerProfileDetailModels = new ArrayList<>();

        try {
            if (kmsOrderTimerProfileDetailIDs.length > 0) {
                // query the database to get the order timer profile detail records
                ArrayList<String> idList = new ArrayList<>();
                for (int kmsOrderTimerProfileDetailID : kmsOrderTimerProfileDetailIDs) {
                    if (kmsOrderTimerProfileDetailID > 0) {
                        idList.add(String.valueOf(kmsOrderTimerProfileDetailID));
                    }
                }
                ArrayList<HashMap> queryRes = new DynamicSQL("data.kms.GetKMSOrderTimerProfileDetailsByIDs").addIDList(1, idList).serialize(dataManager);

                // convert the order timer profile detail records into KMSOrderTimerProfileDetailModel objects
                if (!DataFunctions.isEmptyCollection(queryRes)) {
                    for (HashMap hm : queryRes) {
                        if (!DataFunctions.isEmptyMap(hm)) {
                            kmsOrderTimerProfileDetailModels.add(buildFromHM(dataManager, log, hm));
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build a KMSOrderTimerProfileDetailModel objects for the order timer " +
                    "profile details with IDs of %s in KMSOrderTimerProfileDetailModel.buildFromIDs",
                    Objects.toString(Arrays.toString(kmsOrderTimerProfileDetailIDs), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return kmsOrderTimerProfileDetailModels;
    }

    /**
     * <p>Constructs a {@link KMSOrderTimerProfileDetailModel} using the given JSON {@link String}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param jsonString The JSON {@link String} to build a {@link KMSOrderTimerProfileDetailModel} from.
     * @return A {@link KMSOrderTimerProfileDetailModel} built from the given JSON {@link String}.
     */
    public static KMSOrderTimerProfileDetailModel buildFromJSON (String log, String jsonString) {
        KMSOrderTimerProfileDetailModel kmsOrderTimerProfileDetailModel = new KMSOrderTimerProfileDetailModel();

        try {
            if (StringFunctions.stringHasContent(jsonString)) {
                Gson gson = new Gson();
                JsonParser jsonParser = new JsonParser();
                JsonElement jsonElement = jsonParser.parse(jsonString);
                kmsOrderTimerProfileDetailModel = gson.fromJson(jsonElement, KMSOrderTimerProfileDetailModel.class);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build the KMSOrderTimerProfileDetailModel from the JSON String %s in KMSOrderTimerProfileDetailModel.buildFromJSON",
                    Objects.toString(jsonString, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return kmsOrderTimerProfileDetailModel;
    }



    /**
     * <p>Overridden toString() method for a {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @return A {@link String} representation of this {@link KMSOrderTimerProfileDetailModel}.
     */
    @Override
    public String toString () {

        return String.format("KMSORDERTIMERPROFILEDETAILID: %s, KMSORDERTIMERPROFILEID: %s, STYLEPROFILEID: %s, NAME: %s, TIME: %s, KMSSTYLEPROFILEMODEL: %s",
                Objects.toString((kmsOrderTimerProfileDetailID != -1 ? kmsOrderTimerProfileDetailID : "N/A"), "N/A"),
                Objects.toString((kmsOrderTimerProfileID != -1 ? kmsOrderTimerProfileID : "N/A"), "N/A"),
                Objects.toString((styleProfileID != -1 ? styleProfileID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(name) ? name : "N/A"), "N/A"),
                Objects.toString((time != -1 ? time : "N/A"), "N/A"),
                Objects.toString((kmsStyleProfileModel != null ? "["+kmsStyleProfileModel.toString()+"]" : "N/A"), "N/A"));
    }

    /**
     * <p>Overridden equals() method for a {@link KMSOrderTimerProfileDetailModel}.
     * Two {@link KMSOrderTimerProfileDetailModel} are defined as being equal if they have the same kmsOrderTimerProfileDetailID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KMSOrderTimerProfileDetailModel}.
     * @return Whether or not the {@link Object} is equal to this {@link KMSOrderTimerProfileDetailModel}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KMSOrderTimerProfileDetailModel and check if the obj's kmsOrderTimerProfileDetailID is equal to this KMSOrderTimerProfileDetailModel's kmsOrderTimerProfileDetailID
        KMSOrderTimerProfileDetailModel kmsOrderTimerProfileDetailModel = ((KMSOrderTimerProfileDetailModel) obj);
        return Objects.equals(kmsOrderTimerProfileDetailModel.kmsOrderTimerProfileDetailID, kmsOrderTimerProfileDetailID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @return The unique hash code for this {@link KMSOrderTimerProfileDetailModel}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(kmsOrderTimerProfileDetailID);
    }

    /**
     * <p>Getter for the kmsOrderTimerProfileDetailID field of the {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @return The kmsOrderTimerProfileDetailID field of the {@link KMSOrderTimerProfileDetailModel}.
     */
    public int getKmsOrderTimerProfileDetailID () {
        return kmsOrderTimerProfileDetailID;
    }

    /**
     * <p>Setter for the kmsOrderTimerProfileDetailID field of the {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @param kmsOrderTimerProfileDetailID The kmsOrderTimerProfileDetailID field of the {@link KMSOrderTimerProfileDetailModel}.
     */
    public void setKmsOrderTimerProfileDetailID (int kmsOrderTimerProfileDetailID) {
        this.kmsOrderTimerProfileDetailID = kmsOrderTimerProfileDetailID;
    }

    /**
     * <p>Getter for the kmsOrderTimerProfileID field of the {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @return The kmsOrderTimerProfileID field of the {@link KMSOrderTimerProfileDetailModel}.
     */
    public int getKmsOrderTimerProfileID () {
        return kmsOrderTimerProfileID;
    }

    /**
     * <p>Setter for the kmsOrderTimerProfileID field of the {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @param kmsOrderTimerProfileID The kmsOrderTimerProfileID field of the {@link KMSOrderTimerProfileDetailModel}.
     */
    public void setKmsOrderTimerProfileID (int kmsOrderTimerProfileID) {
        this.kmsOrderTimerProfileID = kmsOrderTimerProfileID;
    }

    /**
     * <p>Getter for the styleProfileID field of the {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @return The styleProfileID field of the {@link KMSOrderTimerProfileDetailModel}.
     */
    public int getStyleProfileID () {
        return styleProfileID;
    }

    /**
     * <p>Setter for the styleProfileID field of the {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @param styleProfileID The styleProfileID field of the {@link KMSOrderTimerProfileDetailModel}.
     */
    public void setStyleProfileID (int styleProfileID) {
        this.styleProfileID = styleProfileID;
    }

    /**
     * <p>Getter for the name field of the {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @return The name field of the {@link KMSOrderTimerProfileDetailModel}.
     */
    public String getName () {
        return name;
    }

    /**
     * <p>Setter for the name field of the {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @param name The name field of the {@link KMSOrderTimerProfileDetailModel}.
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the time field of the {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @return The time field of the {@link KMSOrderTimerProfileDetailModel}.
     */
    public int getTime () {
        return time;
    }

    /**
     * <p>Setter for the time field of the {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @param time The time field of the {@link KMSOrderTimerProfileDetailModel}.
     */
    public void setTime (int time) {
        this.time = time;
    }

    /**
     * <p>Getter for the kmsStyleProfileModel field of the {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @return The kmsStyleProfileModel field of the {@link KMSOrderTimerProfileDetailModel}.
     */
    public KMSStyleProfileModel getKmsStyleProfileModel () {
        return kmsStyleProfileModel;
    }

    /**
     * <p>Setter for the kmsStyleProfileModel field of the {@link KMSOrderTimerProfileDetailModel}.</p>
     *
     * @param kmsStyleProfileModel The kmsStyleProfileModel field of the {@link KMSOrderTimerProfileDetailModel}.
     */
    public void setKmsStyleProfileModel (KMSStyleProfileModel kmsStyleProfileModel) {
        this.kmsStyleProfileModel = kmsStyleProfileModel;
    }

}