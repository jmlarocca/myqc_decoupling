package com.mmhayes.common.kms.models;

/*
    Last Updated (automatically updated by SVN)
    $Author: crking $: Author of last commit
    $Date: 2020-05-13 16:12:43 -0400 (Wed, 13 May 2020) $: Date of last commit
    $Rev: 11731 $: Revision of last commit
    Notes: Object representation of a KMS order status profile detail.
*/

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

/**
 * <p>Object representation of a KMS order status profile detail.</p>
 *
 */
public class KMSOrderStatusProfileDetailModel {

    // private member variables of a KMSOrderStatusProfileDetailModel
    private int kmsOrderStatusProfileDetailID = 0;
    private int kmsOrderStatusProfileID = 0;
    private int kmsOrderStatusID = 0;
    private String displayName = "";
    private int styleProfileID = 0;
    private int secondaryStyleProfileID = 0;
    private KMSStyleProfileModel kmsStyleProfileModel = null;
    private KMSStyleProfileModel kmsSecondaryStyleProfileModel = null;

    /**
     * <p>Constructs a new {@link KMSOrderStatusProfileDetailModel} from the given {@link HashMap}.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param hm A {@link HashMap} with data corresponding to the order status profile detail to build a {@link KMSOrderStatusProfileDetailModel} for.
     * @return The {@link KMSOrderStatusProfileDetailModel} built from the given {@link HashMap}.
     */
    public static KMSOrderStatusProfileDetailModel buildFromHM (DataManager dataManager, String log, HashMap hm) {
        KMSOrderStatusProfileDetailModel kmsOrderStatusProfileDetailModel = new KMSOrderStatusProfileDetailModel();

        try {
            if (!DataFunctions.isEmptyMap(hm)) {
                kmsOrderStatusProfileDetailModel.kmsOrderStatusProfileDetailID = HashMapDataFns.getIntVal(hm, "KMSORDERSTATUSPROFILEDETAILID");
                kmsOrderStatusProfileDetailModel.kmsOrderStatusProfileID = HashMapDataFns.getIntVal(hm, "KMSORDERSTATUSPROFILEID");
                kmsOrderStatusProfileDetailModel.kmsOrderStatusID = HashMapDataFns.getIntVal(hm, "KMSORDERSTATUSID");
                kmsOrderStatusProfileDetailModel.displayName = HashMapDataFns.getStringVal(hm, "DISPLAYNAME");
                kmsOrderStatusProfileDetailModel.styleProfileID = HashMapDataFns.getIntVal(hm, "STYLEPROFILEID");
                kmsOrderStatusProfileDetailModel.secondaryStyleProfileID = HashMapDataFns.getIntVal(hm, "SECONDARYSTYLEPROFILEID");

                // get the style profile
                ArrayList<KMSStyleProfileModel> styleProfileModels = new ArrayList<>();
                if (kmsOrderStatusProfileDetailModel.styleProfileID > 0) {
                    styleProfileModels = KMSStyleProfileModel.buildFromIDs(dataManager, log, new int[]{kmsOrderStatusProfileDetailModel.styleProfileID});
                }
                if (!DataFunctions.isEmptyCollection(styleProfileModels)) {
                    kmsOrderStatusProfileDetailModel.kmsStyleProfileModel = styleProfileModels.get(0);
                }

                // get the secondary style profile
                ArrayList<KMSStyleProfileModel> secondaryStyleProfileModels = new ArrayList<>();
                if (kmsOrderStatusProfileDetailModel.secondaryStyleProfileID > 0) {
                    secondaryStyleProfileModels = KMSStyleProfileModel.buildFromIDs(dataManager, log, new int[]{kmsOrderStatusProfileDetailModel.secondaryStyleProfileID});
                }
                if (!DataFunctions.isEmptyCollection(secondaryStyleProfileModels)) {
                    kmsOrderStatusProfileDetailModel.kmsSecondaryStyleProfileModel = secondaryStyleProfileModels.get(0);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to build a KMSOrderStatusProfileDetailModel object with the given HashMap in KMSOrderStatusProfileDetailModel.buildFromHM", log, Logger.LEVEL.ERROR);
        }

        return kmsOrderStatusProfileDetailModel;
    }

    /**
     * <p>Constructs an {@link ArrayList} of {@link KMSOrderStatusProfileDetailModel} from the given kmsOrderStatusProfileDetailIDs.</p>
     *
     * @param dataManager The {@link DataManager} to use to query the database.
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param kmsOrderStatusProfileDetailIDs IDs of the order status profile details to build {@link KMSOrderStatusProfileDetailModel} objects for.
     * @return An {@link ArrayList} of {@link KMSOrderStatusProfileDetailModel} constructed from the given kmsOrderStatusProfileDetailIDs.
     */
    @SuppressWarnings("Convert2streamapi")
    public static ArrayList<KMSOrderStatusProfileDetailModel> buildFromIDs (DataManager dataManager, String log, int[] kmsOrderStatusProfileDetailIDs) {
        ArrayList<KMSOrderStatusProfileDetailModel> kmsOrderStatusProfileDetailModels = new ArrayList<>();

        try {
            if (kmsOrderStatusProfileDetailIDs.length > 0) {
                // query the database to get the order status profile detail records
                ArrayList<String> idList = new ArrayList<>();
                for (int kmsOrderStatusProfileDetailID : kmsOrderStatusProfileDetailIDs) {
                    if (kmsOrderStatusProfileDetailID > 0) {
                        idList.add(String.valueOf(kmsOrderStatusProfileDetailID));
                    }
                }
                ArrayList<HashMap> queryRes = new DynamicSQL("data.kms.GetKMSOrderStatusProfileDetailsByIDs").addIDList(1, idList).serialize(dataManager);

                // convert the order status profile detail records into KMSOrderStatusProfileDetailModel objects
                if (!DataFunctions.isEmptyCollection(queryRes)) {
                    for (HashMap hm : queryRes) {
                        if (!DataFunctions.isEmptyMap(hm)) {
                            kmsOrderStatusProfileDetailModels.add(buildFromHM(dataManager, log, hm));
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build a KMSOrderStatusProfileDetailModel objects for the order status " +
                    "profile details with IDs of %s in KMSOrderStatusProfileDetailModel.buildFromIDs",
                    Objects.toString(Arrays.toString(kmsOrderStatusProfileDetailIDs), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return kmsOrderStatusProfileDetailModels;
    }

    /**
     * <p>Constructs a {@link KMSOrderStatusProfileDetailModel} using the given JSON {@link String}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param jsonString The JSON {@link String} to build a {@link KMSOrderStatusProfileDetailModel} from.
     * @return A {@link KMSOrderStatusProfileDetailModel} built from the given JSON {@link String}.
     */
    public static KMSOrderStatusProfileDetailModel buildFromJSON (String log, String jsonString) {
        KMSOrderStatusProfileDetailModel kmsOrderStatusProfileDetailModel = new KMSOrderStatusProfileDetailModel();

        try {
            if (StringFunctions.stringHasContent(jsonString)) {
                Gson gson = new Gson();
                JsonParser jsonParser = new JsonParser();
                JsonElement jsonElement = jsonParser.parse(jsonString);
                kmsOrderStatusProfileDetailModel = gson.fromJson(jsonElement, KMSOrderStatusProfileDetailModel.class);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to build the KMSOrderStatusProfileDetailModel from the JSON String %s in KMSOrderStatusProfileDetailModel.buildFromJSON",
                    Objects.toString(jsonString, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return kmsOrderStatusProfileDetailModel;
    }


    /**
     * <p>Overridden toString() method for a {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @return A {@link String} representation of this {@link KMSOrderStatusProfileDetailModel}.
     */
    @Override
    public String toString () {

        return String.format("KMSORDERSTATUSPROFILEDETAILID: %s, KMSORDERSTATUSPROFILEID: %s, KMSORDERSTATUSID: %s, DISPLAYNAME: %s, " +
                "STYLEPROFILEID: %s, SECONDARYSTYLEPROFILEID: %s, KMSSTYLEPROFILEMODEL: %s, KMSSECONDARYSTYLEPROFILEMODEL: %s",
                Objects.toString((kmsOrderStatusProfileDetailID != -1 ? kmsOrderStatusProfileDetailID : "N/A"), "N/A"),
                Objects.toString((kmsOrderStatusProfileID != -1 ? kmsOrderStatusProfileID : "N/A"), "N/A"),
                Objects.toString((kmsOrderStatusID != -1 ? kmsOrderStatusID : "N/A"), "N/A"),
                Objects.toString((StringFunctions.stringHasContent(displayName) ? displayName : "N/A"), "N/A"),
                Objects.toString((styleProfileID != -1 ? styleProfileID : "N/A"), "N/A"),
                Objects.toString((secondaryStyleProfileID != -1 ? secondaryStyleProfileID : "N/A"), "N/A"),
                Objects.toString((kmsStyleProfileModel != null ? "["+kmsStyleProfileModel.toString()+"]" : "N/A"), "N/A"),
                Objects.toString((kmsSecondaryStyleProfileModel != null ? "["+kmsSecondaryStyleProfileModel.toString()+"]" : "N/A"), "N/A"));

    }

    /**
     * <p>Overridden equals() method for a {@link KMSOrderStatusProfileDetailModel}.
     * Two {@link KMSOrderStatusProfileDetailModel} are defined as being equal if they have the same kmsOrderStatusProfileDetailID.</p>
     *
     * @param obj The {@link Object} to compare against this {@link KMSOrderStatusProfileDetailModel}.
     * @return Whether or not the {@link Object} is equal to this {@link KMSOrderStatusProfileDetailModel}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a KMSOrderStatusProfileDetailModel and check if the obj's kmsOrderStatusProfileDetailID is equal to this KMSOrderStatusProfileDetailModel's kmsOrderStatusProfileDetailID
        KMSOrderStatusProfileDetailModel kmsOrderStatusProfileDetailModel = ((KMSOrderStatusProfileDetailModel) obj);
        return Objects.equals(kmsOrderStatusProfileDetailModel.kmsOrderStatusProfileDetailID, kmsOrderStatusProfileDetailID);
    }

    /**
     * <p>Overridden hashCode() method for a {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @return The unique hash code for this {@link KMSOrderStatusProfileDetailModel}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(kmsOrderStatusProfileDetailID);
    }

    /**
     * <p>Getter for the kmsOrderStatusProfileDetailID field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @return The kmsOrderStatusProfileDetailID field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public int getKmsOrderStatusProfileDetailID () {
        return kmsOrderStatusProfileDetailID;
    }

    /**
     * <p>Setter for the kmsOrderStatusProfileDetailID field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @param kmsOrderStatusProfileDetailID The kmsOrderStatusProfileDetailID field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public void setKmsOrderStatusProfileDetailID (int kmsOrderStatusProfileDetailID) {
        this.kmsOrderStatusProfileDetailID = kmsOrderStatusProfileDetailID;
    }

    /**
     * <p>Getter for the kmsOrderStatusProfileID field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @return The kmsOrderStatusProfileID field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public int getKmsOrderStatusProfileID () {
        return kmsOrderStatusProfileID;
    }

    /**
     * <p>Setter for the kmsOrderStatusProfileID field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @param kmsOrderStatusProfileID The kmsOrderStatusProfileID field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public void setKmsOrderStatusProfileID (int kmsOrderStatusProfileID) {
        this.kmsOrderStatusProfileID = kmsOrderStatusProfileID;
    }

    /**
     * <p>Getter for the kmsOrderStatusID field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @return The kmsOrderStatusID field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public int getKmsOrderStatusID () {
        return kmsOrderStatusID;
    }

    /**
     * <p>Setter for the kmsOrderStatusID field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @param kmsOrderStatusID The kmsOrderStatusID field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public void setKmsOrderStatusID (int kmsOrderStatusID) {
        this.kmsOrderStatusID = kmsOrderStatusID;
    }

    /**
     * <p>Getter for the displayName field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @return The displayName field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public String getDisplayName () {
        return displayName;
    }

    /**
     * <p>Setter for the displayName field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @param displayName The displayName field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public void setDisplayName (String displayName) {
        this.displayName = displayName;
    }

    /**
     * <p>Getter for the styleProfileID field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @return The styleProfileID field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public int getStyleProfileID () {
        return styleProfileID;
    }

    /**
     * <p>Setter for the styleProfileID field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @param styleProfileID The styleProfileID field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public void setStyleProfileID (int styleProfileID) {
        this.styleProfileID = styleProfileID;
    }

    /**
     * <p>Getter for the secondaryStyleProfileID field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @return The secondaryStyleProfileID field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public int getSecondaryStyleProfileID () {
        return secondaryStyleProfileID;
    }

    /**
     * <p>Setter for the secondaryStyleProfileID field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @param secondaryStyleProfileID The secondaryStyleProfileID field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public void setSecondaryStyleProfileID (int secondaryStyleProfileID) {
        this.secondaryStyleProfileID = secondaryStyleProfileID;
    }

    /**
     * <p>Getter for the kmsStyleProfileModel field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @return The kmsStyleProfileModel field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public KMSStyleProfileModel getKmsStyleProfileModel () {
        return kmsStyleProfileModel;
    }

    /**
     * <p>Setter for the kmsStyleProfileModel field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @param kmsStyleProfileModel The kmsStyleProfileModel field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public void setKmsStyleProfileModel (KMSStyleProfileModel kmsStyleProfileModel) {
        this.kmsStyleProfileModel = kmsStyleProfileModel;
    }

    /**
     * <p>Getter for the kmsSecondaryStyleProfileModel field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @return The kmsSecondaryStyleProfileModel field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public KMSStyleProfileModel getKmsSecondaryStyleProfileModel () {
        return kmsSecondaryStyleProfileModel;
    }

    /**
     * <p>Setter for the kmsSecondaryStyleProfileModel field of the {@link KMSOrderStatusProfileDetailModel}.</p>
     *
     * @param kmsSecondaryStyleProfileModel The kmsSecondaryStyleProfileModel field of the {@link KMSOrderStatusProfileDetailModel}.
     */
    public void setKmsSecondaryStyleProfileModel (KMSStyleProfileModel kmsSecondaryStyleProfileModel) {
        this.kmsSecondaryStyleProfileModel = kmsSecondaryStyleProfileModel;
    }

}