package com.mmhayes.common.kms;

//import com.mmhayes.common.kitchenapi.PeripheralsDataManager;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.conn_pool.JDCConnection;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.kitchenPrinters.*;
import com.mmhayes.common.kitchenapi.KitchenPrinterDataManager;
import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.kms.receiptGen.errorReceipt.EmailedErrorReceipt;
import com.mmhayes.common.kms.receiptGen.errorReceipt.ErrorReceipt;
import com.mmhayes.common.kms.receiptGen.errorReceipt.PrintedErrorReceipt;
import com.mmhayes.common.printing.PrintJobStatusType;
import com.mmhayes.common.printing.PrintStatusType;
import com.mmhayes.common.receiptGen.Commands.JPOS_COMMAND;
import com.mmhayes.common.receiptGen.Formatter.GiftReceiptFormatter;
import com.mmhayes.common.receiptGen.Formatter.KitchenPrinterReceiptFormatter;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.Models.CcProcessorReceiptDetail;
import com.mmhayes.common.receiptGen.Models.TransactionReceiptModel;
import com.mmhayes.common.receiptGen.ReceiptData.KPJobDetail;
import com.mmhayes.common.receiptGen.ReceiptData.KPJobHeader;
import com.mmhayes.common.receiptGen.ReceiptData.KitchenDisplaySystemJobInfo;
import com.mmhayes.common.receiptGen.ReceiptData.KitchenPrinterJobInfo;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.transaction.models.TenderLineItemModel;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.xmlapi.XmlRpcUtil;
import jpos.POSPrinter;
import org.apache.commons.lang.math.NumberUtils;

import java.io.IOException;
import java.net.NetworkInterface;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class PeripheralsDataManager extends com.mmhayes.common.kitchenapi.PeripheralsDataManager {


    public PeripheralsDataManager() {
        super();

        // check if we should start the kitchen printing threads
        String war = MMHServletStarter.getInstanceIdentifier();
        String useKitchenPrinters = MMHProperties.getAppSetting("site.kitchenPrinter.active");
        boolean isKmsWar = ((StringFunctions.stringHasContent(war)) && (war.equalsIgnoreCase("oms")));
        boolean isKitchenPrintingEnabled = ((StringFunctions.stringHasContent(useKitchenPrinters)) && (useKitchenPrinters.equalsIgnoreCase("yes")));
        Logger.logMessage(String.format("Instantiating a new PeripheralsDataManager on the %s war, in the PeripheralsDataManager constructor.",
                Objects.toString((StringFunctions.stringHasContent(war) ? war : "N/A"), "N/A")), Logger.LEVEL.IMPORTANT);
        Logger.logMessage(String.format("The use kitchen printers setting is %s in the PeripheralsDataManager constructor.",
                Objects.toString((isKitchenPrintingEnabled ? "enabled" : "disabled"), "N/A")), Logger.LEVEL.IMPORTANT);

        // start the kitchen printing threads
        if ((isKmsWar) && (isKitchenPrintingEnabled)) {
            kitchenPrinterTerminal = KitchenPrinterTerminal.init(dm, this);
        }
    }

    /**
     * <p>Gets the local receipt printer as defined by the Peripherals.properties.</p>
     *
     * @return The local receipt {@link POSPrinter}.
     */
    public static POSPrinter getLocalReceiptPrinter () {

        if (printer == null) {
            Logger.logMessage("The local receipt printer was found to be null in PeripheralsDataManager.getLocalReceiptPrinter, " +
                    "now calling openConnToPrinter.", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            openConnToPrinter();
        }

        if (printer == null) {
            Logger.logMessage("The local receipt printer was found to be null after trying to open a connection to the printer in PeripheralsDataManager.getLocalReceiptPrinter, " +
                    "now returning null.", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return printer.getPOSPrinter();
    }

    /**
     * <p>Gets the logical name of the local receipt printer.</p>
     *
     * @return The logical name {@link String} of the local receipt printer.
     */
    public static String getLocalReceiptPrinterLogicalName () {

        if (printer == null) {
            Logger.logMessage("The local receipt printer was found to be null in PeripheralsDataManager.getLocalReceiptPrinterLogicalName, " +
                    "now returning null.", CustomerReceiptData.CUST_RCPT_LOG, Logger.LEVEL.ERROR);
            return null;
        }

        return printer.getLogicalPrinterName();
    }


    //--------------------------------------------------------------------------
    //  region PRINTER METHODS
    //--------------------------------------------------------------------------

    /**
     * Get printer hosts that are a backup to the primary printer host.
     *
     * @return {@link ArrayList < HashMap >} The backup printer host records from the QC_PrintController table.
     */
    @SuppressWarnings("unchecked")
    public static ArrayList<HashMap> getBackupPrinterHostRecords () {

        try {
            // get the MAC address of the primary printer host
            String macAddress = getTerminalMacAddress();
            ArrayList<HashMap> backupPrinterHostRecords = new DataManager().parameterizedExecuteQuery(
                    "data.kitchenPrinter.getBackupPrinterHosts", new Object[]{macAddress}, logFileName, true);

            if (backupPrinterHostRecords != null) {
                return backupPrinterHostRecords;
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying ot get the backup printer host records associated with " +
                            "the MAC address "+ getTerminalMacAddress()+" in PeripheralsDataManager.getBackupPrinterHosts",
                    logFileName, Logger.LEVEL.ERROR);
        }

        return new ArrayList<>();
    }

    /**
     * Get all printer hosts that are associated with the primary printer host.
     *
     * @return {@link ArrayList<HashMap>} Printer host records from the QC_PrintController table.
     */
    @SuppressWarnings("unchecked")
    public static ArrayList<HashMap> getAllConfiguredPrinterHostRecords () {

        try {
            ArrayList<HashMap> configuredPrinterHostRecords = new DataManager().parameterizedExecuteQuery(
                    "data.kitchenPrinter.getAllConfiguredPrinterHosts",
                    new Object[]{getTerminalMacAddress()}, logFileName, true);

            if (configuredPrinterHostRecords != null) {
                return configuredPrinterHostRecords;
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to get the printer host records associated with the printer " +
                    "host running on the machine with a hostname of "+ Objects.toString(getPHHostname(), "NULL")+" in " +
                    "PeripheralsDataManager.getAllConfiguredPrinterHostRecords", logFileName, Logger.LEVEL.ERROR);
        }

        return new ArrayList<>();
    }

    /**
     * Get the MAC address of the in service printer host.
     *
     * @return {@link String} The MAC address of the in service printer host.
     */
    public static String getInServicePHMacAddress() {
        String macAddress = "";

        Logger.logMessage("Getting in service printer host mac address", logFileName, Logger.LEVEL.DEBUG);

        try {
            // get the mac address that has been explicitly set as the in service printer host
            String macAddressFromKPT = KitchenPrinterTerminal.getInstance().getActivePHMacAddress();
            // if an address has been explicitly set, just use that
            if (macAddressFromKPT != null) {
                Logger.logMessage("Retrieved in service printer host mac address from KPT", logFileName, Logger.LEVEL.DEBUG);
                macAddress = macAddressFromKPT;
            }
            // if no address has been explicitly set, retrieve the current terminal's mac address
            else {
                macAddress = getTerminalMacAddress();
            }
        }
        catch (Exception ex) {
            Logger.logMessage("Error getting in service printer host mac address in " +
                    "PeripheralsDataManager.getInServicePHMacAddress", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return macAddress;
    }

    /**
     * Get the printers associated with the printer host configured to run on this WAR.
     *
     * @return {@link ArrayList<HashMap>} The printer records from the QC_Printer table.
     */
    @SuppressWarnings("unchecked")
    public static ArrayList<HashMap> getPHPrinters () {
        ArrayList<HashMap> printers = new ArrayList<>();

        try {
            ArrayList<HashMap> queryRes = new DataManager().parameterizedExecuteQuery(
                    "data.kitchenPrinter.getPHPrinters", new Object[]{getTerminalMacAddress()}, logFileName, true);
            if ((queryRes != null) && (!queryRes.isEmpty())) {
                printers = queryRes;
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to get the printers that are associated with the printer " +
                            "host configured to run on this WAR in PeripheralsDataManager.getPHPrinters", logFileName,
                    Logger.LEVEL.ERROR);
        }

        return printers;
    }

    /**
     * Called through XML RPC to set the status of the printer host.
     *
     * @param printerHostStatus {@link String} The status we wish to set for the printer host.
     * @return Whether or not the status of the printer host was set successfully.
     */
    public boolean setPrinterHostStatus (String printerHostStatus) {
        boolean printerHostStatusSet = false;

        try {
            kitchenPrinterTerminal.addToPrinterHostStatusTracker(getTerminalMacAddress(),
                    PrinterHostStatus.getPHStatusFromString(printerHostStatus));
            printerHostStatusSet = true;
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to set the status of the printer host running on the machine " +
                    "with a hostname of "+Objects.toString(getPHHostname(), "NULL")+" in " +
                    "PeripheralsDataManager.setPrinterHostStatus", logFileName, Logger.LEVEL.ERROR);
        }

        return printerHostStatusSet;
    }

    /**
     * Called through XML RPC to get the printer host status of the printer host at the given MAC address.
     *
     * @param macAddress {@link String} MAC address of the printer host whose status we wish to obtain.
     * @return {@link String} The status of the printer host.
     */
    public String getPrinterHostStatus (String macAddress) {
        String printerHostStatus = "";

        try {
            if (kitchenPrinterTerminal.getPrinterHostStatusTracker().containsKey(macAddress)) {
                printerHostStatus =
                        kitchenPrinterTerminal.getPrinterHostStatusTracker().get(macAddress).getPrinterHostStatus();
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to get the status of the printer host running on the " +
                    "machine with a hostname of "+Objects.toString(getTerminalHostname(macAddress), "NULL")+" in " +
                    "PeripheralsDataManager.getPrinterHostStatus", logFileName, Logger.LEVEL.ERROR);
        }

        return printerHostStatus;
    }

    /**
     * <p>Called through XML RPC to update the in-service printer host on the server.</p>
     *
     * @param inSvcPHMac The MAC address {@link String} of the in-service printer host.
     * @return Whether or not the update was successful.
     */
    public boolean updateInServicePH (String inSvcPHMac) {
        boolean updateSuccess = false;

        try {
            if (StringFunctions.stringHasContent(inSvcPHMac)) {
                // get the ID of the in-service printer host
                int printerHostID = 0;
                Object queryRes = dm.parameterizedExecuteScalar("data.kitchenPrinter.getPrinterHostIDFromMacAddress", new Object[]{inSvcPHMac}, logFileName);
                if ((queryRes != null) && (!queryRes.toString().isEmpty()) && (NumberUtils.isNumber(queryRes.toString()))) {
                    printerHostID = Integer.parseInt(queryRes.toString());
                }
                // update the in-service printer host on the server
                if ((printerHostID > 0) && (dm.parameterizedExecuteNonQuery("data.kitchenPrinter.updateInServicePH", new Object[]{inSvcPHMac, printerHostID}, logFileName) > 0)) {
                    updateSuccess = true;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("Unable to set the in service printer host as %s on the server in " +
                            "PeripheralsDataManager.updateInServicePH",
                    Objects.toString(getTerminalHostname(inSvcPHMac), "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return updateSuccess;
    }

    /**
     * Called through XML RPC to get the name of the war being used by the machine with the given MAC address.
     *
     * @param macAddress {@link String} MAC address of the machine to get it's war name for.
     * @return {@link String} The name of the war being used by the machine with the given MAC address.
     */
    public String getWarNameOnServer (String macAddress) {
        String war = "";

        try {
            Object queryRes = dm.parameterizedExecuteScalar("data.kitchenPrinter.GetWarName", new Object[]{macAddress}, logFileName);
            if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString()))) {
                war = queryRes.toString();
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage(String.format("There was a problem trying to get the name of the war being used on the " +
                            "machine with a hostname of %s in PeripheralsDataManager.getWarNameOnServer",
                    Objects.toString(getTerminalHostname(macAddress), "NULL")), logFileName, Logger.LEVEL.ERROR);
        }

        return war;
    }

    // Called by the XML RPC Handler Servlet when the server is started up.
    public static synchronized void loadPeripheralSettings()
    {
        Logger.logMessage("loadPeripheralSettings starting", logFileName, Logger.LEVEL.DEBUG);
        try
        {
            openConnToPrinter();
        }
        catch (Exception e)
        {
            Logger.logMessage("Peripherals Data Manager: loadPeripheralsSettings EXCEPTION", logFileName, Logger.LEVEL.ERROR);
            Logger.logMessage("Peripherals Data Manager: " + e.getStackTrace().toString(), logFileName, Logger.LEVEL.ERROR);
        }

        Logger.logMessage("loadPeripheralSettings finished ", logFileName, Logger.LEVEL.DEBUG);
    }

    /**
     *
     * @param transactionReceiptModel
     * @throws Exception
     */
    public void printReceipts(TransactionReceiptModel transactionReceiptModel) throws Exception {
        try {
            ReceiptGen rcptGen = new ReceiptGen();
            if (transactionReceiptModel.getReceiptModel().isGiftReceipt()) {
                rcptGen.setReceiptFormatter(new GiftReceiptFormatter());
            }

            if (rcptGen != null) {
                rcptGen.setReceiptData(rcptGen.createReceiptData());
            }

            //If contentType is set to None, don't print or send anything
            if (transactionReceiptModel.getReceiptModel().getContentType().isEmpty()
                    || transactionReceiptModel.getReceiptModel().getContentType().equalsIgnoreCase("None")) {
                return;
            }

            // encapsulate the nutrition info
            ArrayList nutritionInfo = new ArrayList();
            if (transactionReceiptModel.getReceiptConfigModel().isPrintNutritionInfo()) {
                nutritionInfo.add(transactionReceiptModel.getReceiptConfigModel().isNeedsNAFootNote());
                nutritionInfo.add(transactionReceiptModel.getReceiptConfigModel().isNeedsTruncateDigitsFootnote());
            }

            //Print Customer Receipt (QCPOS or KOA)
            Logger.logMessage("Printing Customer Copy", logFileName, Logger.LEVEL.DEBUG);
            transactionReceiptModel.getReceiptModel().setMerchantCopy(false);
            printConsolidatedReceipt(transactionReceiptModel, rcptGen);

            if (!transactionReceiptModel.getTenderLinesHM().isEmpty()) {
                Boolean merchantReceiptPrinted = false;

                //For each tender, check to see if receipts need to be printed
                for (Map.Entry<Integer, TenderLineItemModel> tenderTransLinesHM : transactionReceiptModel.getTenderLinesHM().entrySet()) {

                    TenderLineItemModel tenderLineItemModel = tenderTransLinesHM.getValue();

                    //Print Merchant Copy
                    if (tenderLineItemModel.getTender().isPrintSignatureReceipt() && tenderLineItemModel.doesMeetMinSignatureAmount() && !merchantReceiptPrinted) {
                        Logger.logMessage("Printing Merchant Copy", logFileName, Logger.LEVEL.DEBUG);
                        transactionReceiptModel.getReceiptModel().setMerchantCopy(true);
                        printConsolidatedReceipt(transactionReceiptModel, rcptGen);
                        merchantReceiptPrinted = true;
                    }

                    if (tenderLineItemModel.getTender().getTenderTypeId() == PosAPIHelper.TenderType.CREDIT_CARD.toInt()) { //Handle Credit Card receipts
                        int ccCount = 0;
                        for (Map.Entry<Integer, TenderLineItemModel> tenderLineItemCCcount : transactionReceiptModel.getTenderLinesHM().entrySet()) {
                            if (tenderLineItemCCcount.getValue().getTender().getTenderTypeId() == PosAPIHelper.TenderType.CREDIT_CARD.toInt()) {
                                ccCount = ccCount + 1;
                            }
                        }

                        if (transactionReceiptModel.getReceiptConfigModel().isPrintProcessorCustomerCopy()) { //Processor Customer Copy (Set on Terminal, Print Credit Card Processor Receipt)
                            Logger.logMessage("Printing Processor Customer Copy", logFileName, Logger.LEVEL.DEBUG);
                            transactionReceiptModel.getReceiptModel().setMerchantCopy(false);

                            for (CcProcessorReceiptDetail ccProcessorReceiptDetail : transactionReceiptModel.getReceiptModel().getCcProcessorReceiptDetails()) {
                                if (ccProcessorReceiptDetail.getCreditCardTransInfo().equals(tenderLineItemModel.getCreditCardTransInfo())) {
                                    if (!ccProcessorReceiptDetail.getCustomerCopyReceiptText().isEmpty()) {
                                        if (transactionReceiptModel.getReceiptConfigModel().getPaymentProcessorId() != null && !transactionReceiptModel.getReceiptConfigModel().getPaymentProcessorId().toString().isEmpty()) {
                                            ArrayList receiptArrayList = new ArrayList();
                                            receiptArrayList.add(ccProcessorReceiptDetail.getCustomerCopyReceiptText());

                                            switch (transactionReceiptModel.getReceiptConfigModel().getPaymentProcessorId()) {
                                                case 1: //Stripe
                                                case 2: //Datacap NETePay
                                                case 3: //Datacap NETePay EMV
                                                    break;
                                                case 4: //Merchant Link
                                                    printMerchantLinkReceipt(receiptArrayList);
                                                    break;
                                                case 5: //Datacap NETePay
                                                    break;
                                                case 6:
                                                    printFreedomPayReceipt(receiptArrayList, transactionReceiptModel.getReceiptModel());
                                                    break; //FreedomPay
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        } else {
                            //Logger.logMessage("QC_Terminals.PrintMerchantLinkCustCopy is set to NO", logFileName, Logger.LEVEL.TRACE);
                        }

                        //Processor Merchant Copy
                        if (tenderLineItemModel.getTender().isPrintSignatureReceipt() && tenderLineItemModel.doesMeetMinSignatureAmount()) {
                            Logger.logMessage("Printing Processor Customer Copy", logFileName, Logger.LEVEL.DEBUG);
                            transactionReceiptModel.getReceiptModel().setMerchantCopy(true);

                            for (CcProcessorReceiptDetail ccProcessorReceiptDetail : transactionReceiptModel.getReceiptModel().getCcProcessorReceiptDetails()) {
                                if (ccProcessorReceiptDetail.getCreditCardTransInfo().equals(tenderLineItemModel.getCreditCardTransInfo())) {
                                    if (!ccProcessorReceiptDetail.getMerchantCopyReceiptText().isEmpty()) {
                                        if (transactionReceiptModel.getReceiptConfigModel().getPaymentProcessorId() != null && !transactionReceiptModel.getReceiptConfigModel().getPaymentProcessorId().toString().isEmpty()) {
                                            ArrayList receiptArrayList = new ArrayList();
                                            receiptArrayList.add(ccProcessorReceiptDetail.getMerchantCopyReceiptText());

                                            switch (transactionReceiptModel.getReceiptConfigModel().getPaymentProcessorId()) {
                                                case 1: //Stripe
                                                case 2: //Datacap NETePay
                                                case 3: //Datacap NETePay EMV
                                                    break;
                                                case 4: //Merchant Link
                                                    printMerchantLinkReceipt(receiptArrayList);
                                                    break;
                                                case 5: //Datacap NETePay
                                                    break;
                                                case 6:
                                                    printFreedomPayReceipt(receiptArrayList, transactionReceiptModel.getReceiptModel());
                                                    break; //FreedomPay
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                } //end of tender line loop
            }
        } catch (Exception e) {
            Logger.logException("PeripheralsDataManager.printReceipts error", logFileName, e);
        }
    }

    /**
     * Get the revenue centers associated with the printer host configured to run on this WAR.
     *
     * @return {@link String} Comma delimited String of revenue centers IDs associated with the printer host configured
     *         to run on this WAR.
     */
    @SuppressWarnings({"unchecked", "TypeMayBeWeakened"})
    public static String getPHRevCenters () {
        String revCenterIDs = "";

        try {
            // query the database to get the revenue center IDs
            ArrayList<HashMap> revCenters = new DataManager().parameterizedExecuteQuery(
                    "data.kitchenPrinter.getPHRevenueCenters", new Object[]{getTerminalMacAddress()}, logFileName, true);
            ArrayList<Integer> revCenterIDsList = new ArrayList<>();
            if ((revCenters != null) && (!revCenters.isEmpty())) {
                revCenters.forEach((revCenterHM) -> {
                    if (revCenterHM.containsKey("REVENUECENTERID")) {
                        revCenterIDsList.add(Integer.parseInt(revCenterHM.get("REVENUECENTERID").toString()));
                    }
                });
                if (!revCenterIDsList.isEmpty()) {
                    revCenterIDs = DataFunctions.arrayListToString(revCenterIDsList);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to get the revenue centers that this printer host is " +
                    "assigned to in PeripheralsDataManager.getPHRevCenters", logFileName, Logger.LEVEL.ERROR);
        }

        return revCenterIDs;
    }

    /**
     * Get the health check frequency for this WAR.
     *
     * @return The frequency at which the printer host check executor should poll all the configured print controllers
     * for this WAR.
     */
    @SuppressWarnings("unchecked")
    public static long getHealthCheckFrequency () {
        long healthCheckFrequency = 60;

        try {
            ArrayList<HashMap> queryRes = new DataManager().parameterizedExecuteQuery(
                    "data.kitchenPrinter.getHealthCheckFrequency", new Object[]{getTerminalMacAddress()}, logFileName, true);
            if ((queryRes != null) && (!queryRes.isEmpty())) {
                HashMap healthCheckFrequencyHM = queryRes.get(0);
                if ((healthCheckFrequencyHM != null) && (!healthCheckFrequencyHM.isEmpty())
                        && healthCheckFrequencyHM.containsKey("HEALTHCHECKFREQUENCYSECONDS")) {
                    healthCheckFrequency =
                            Long.valueOf(healthCheckFrequencyHM.get("HEALTHCHECKFREQUENCYSECONDS").toString());
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, logFileName);
            Logger.logMessage("There was a problem trying to get the health check frequency for the WAR with " +
                            "the printer host MAC ADDRESS: "+ getTerminalMacAddress()+" and HOSTNAME: "+getPHHostname()+" configured " +
                            "as its printer host in PeripheralsDataManager.getHealthCheckFrequency", logFileName,
                    Logger.LEVEL.ERROR);
        }

        return healthCheckFrequency;
    }

    private void printConsolidatedReceipt(TransactionReceiptModel transactionReceiptModel, ReceiptGen rcptGen) throws Exception{

        // If there's an email address, email the receipt
        if (!transactionReceiptModel.getReceiptModel().getEmailAddress().isEmpty()) {
            // If we need to pull email settings, do that
            if (emailServer == null) {
                setEmailSettings();
            }

            //Build render for html receipt
            IRenderer htmlRenderer = rcptGen.createBasicHTMLRenderer();
            if (emailServer.isEmpty()) {
                Logger.logMessage("In QC_Globals SMTPSERVER is empty and this is required to send email", logFileName, Logger.LEVEL.TRACE);
            } else {
                transactionReceiptModel.getReceiptConfigModel().setReceiptTypeId(TypeData.ReceiptType.EMAILEDRECEIPT);

                // Try to email the receipt
                if (htmlRenderer != null && rcptGen.buildFromTransactionModel(transactionReceiptModel, htmlRenderer)) {
                    if (emailUser.isEmpty()) {
                        Logger.logMessage("In PrintConsolidatedReceipt, about to send unauthenticated email receipt to " + transactionReceiptModel.getReceiptModel().getEmailAddress() + " using mail server " + emailServer + " and port " + emailPort + ". ", logFileName, Logger.LEVEL.TRACE);
                        MMHEmail.sendEmail(Integer.parseInt(emailPort), emailServer, transactionReceiptModel.getReceiptModel().getEmailAddress(), emailFromAddress, "Your receipt", htmlRenderer.toString(), "text/html");
                    } else {
                        Logger.logMessage("In PrintConsolidatedReceipt, about to send authenticated email receipt to " + transactionReceiptModel.getReceiptModel().getEmailAddress() + " using mail server " + emailServer + " and port " + emailPort + ". ", logFileName, Logger.LEVEL.TRACE);
                        MMHEmail.sendAuthEmail(Integer.parseInt(emailPort), emailServer, emailUser, emailPassword, transactionReceiptModel.getReceiptModel().getEmailAddress(), emailFromAddress, "Your receipt", htmlRenderer.toString(), "text/html");
                    }
                }
            }
        } else {
            // Print the receipt
            String retVal = "";
            POSPrinter ptr = null;
            boolean debugModeDev = false; //Used to test the data locally

            if (debugModeDev) {
                //this will just test the data
                if (transactionReceiptModel.getReceiptConfigModel().isPrintReceipt()) {
                    //To debug data, create HTML Renderer
                    IRenderer htmlRenderer = rcptGen.createBasicHTMLRenderer();
                    // print the receipt
                    rcptGen.buildFromTransactionModel(transactionReceiptModel, htmlRenderer);
                }
            } else {

                if (printer != null) {
                    if (transactionReceiptModel.getReceiptConfigModel().isPrintReceipt()) {
                        ptr = printer.getPOSPrinter();
                        IRenderer printerRenderer = null;
                        if (ptr != null) {
                            printerRenderer = rcptGen.createPrinterRenderer(ptr, printer.getLogicalPrinterName());
                        }

                        // print the receipt
                        if (printerRenderer != null) {
                            rcptGen.buildFromTransactionModel(transactionReceiptModel, printerRenderer);
                            if (printerRenderer.getLastException() != null) {
                                throw printerRenderer.getLastException();
                            }
                        }
                    }
                }
            }
        }
    }

    // endregion

    //--------------------------------------------------------------------------
    //  region KITCHEN PRINTER METHODS
    //--------------------------------------------------------------------------
    // Terminals call kpprint on the print controller to actually perform a kitchen print
    public ArrayList kpPrint(ArrayList printData)
    {
        Logger.logMessage("PeripheralsDataManager.kpPrint called.", "KMS.log", Logger.LEVEL.TRACE);
        if(kitchenPrinterTerminal == null)
        {
            Logger.logMessage("ERROR: PeripheralsDataManager.kpPrint - kitchenPrinterTerminal is null", getKPLogFileName(), Logger.LEVEL.ERROR);
            return new ArrayList();
        }
        else
        {
            return kitchenPrinterTerminal.queuePrintData(printData);
        }
    }

    // The print controller calls kpCheck on the server to look for online orders for its revenue center
    public ArrayList kpCheck(String revenueCenterIds)
    {
        ArrayList retVal = new ArrayList();

        try
        {
            if (revenueCenterIds.equals("") == false)
            {
                // Update QC_PrintController.LastOnlineOrderCheck
//                Logger.logMessage("Updating LastOnlineOrderCheck for print controllers in revenue center IDs: " + revenueCenterIds, getKPLogFileName(), Logger.LEVEL.DEBUG);
//                dm.parameterizedExecuteNonQuery("data.kitchenPrinter.updateLastOnlineOrderCheck", new Object[]{revenueCenterIds}, getKPLogFileName());

                ArrayList onlineOrders = dm.serializeSqlWithColNames("data.newKitchenPrinter.GetOnlineOrdersForRevCenters", new Object[]{revenueCenterIds}, null, getKPLogFileName());
                if (onlineOrders.size() > 0)
                {
                    Logger.logMessage("A total of " + onlineOrders.size() + " found through KPCheck() and will be passed to the print controller.", getKPLogFileName(), Logger.LEVEL.TRACE);
                }
                for (Object o : onlineOrders)   //loop through each print job/row we need to expand print detail for
                {
                    HashMap onlineOrderHM = (HashMap) o;
                    if (onlineOrderHM.containsKey("PATRANSACTIONID"))
                    {
                        int paTransactionID = Integer.parseInt(onlineOrderHM.get("PATRANSACTIONID").toString());
                        Logger.logMessage("In KPCheck() building receipt data for PATransactionID " + paTransactionID, getKPLogFileName(), Logger.LEVEL.DEBUG);

                        try {
                            ReceiptGen receiptGen = new ReceiptGen();
                            receiptGen.setReceiptFormatter(new KitchenPrinterReceiptFormatter(new DataManager()));
                            IRenderer htmlRenderer = receiptGen.createBasicHTMLRenderer();
                            receiptGen.setReceiptData(receiptGen.createReceiptData());
                            receiptGen.getReceiptData().setPopupVal(true);
                            if (htmlRenderer != null)
                            {
                                receiptGen.buildFromDBOrderNum(paTransactionID, TypeData.ReceiptType.KITCHENPTRRECEIPT, htmlRenderer);

                                retVal.add(receiptGen.getReceiptData().getKpJobInfo().getPlainTextDetails());
                            }
                            else
                            {
                                Logger.logMessage("In kpCheck() the htmlRenderer was found to be null. Unable to generate the receipt data for PATransactionID " + paTransactionID, getKPLogFileName(), Logger.LEVEL.ERROR);
                            }
                        }
                        catch (Exception exc)
                        {
                            Logger.logException("KPCheck(): Unable to generate the receipt data for PATransactionID " + paTransactionID, getKPLogFileName(), exc);
                        }

                        //Now that this transaction has been expanded and written to the database, we need to mark the status as sent so it wont get
                        //picked up next time kpcheck() runs
                        if (dm.serializeUpdate("data.newKitchenPrinter.SetPrintQueueStatusToDoneByPATransactionID", new Object[]{paTransactionID}, null, getKPLogFileName(), 0, false) <= 0)
                        {
                            //We've successfully updated the print job header status to "DONE" so the job won't get re-run
                        }
                        if (dm.serializeUpdate("data.newKitchenPrinter.SetPrintQueueDetailStatusToDoneByPATransactionID", new Object[]{paTransactionID}, null, getKPLogFileName(), 0, false) <= 0)
                        {
                            //We've successfully updated the print job detail status to "DONE". This wouldn't re-run the job anyways, but seems like good housekeeping
                        }
                    }
                }
            }
            else
            {
                Logger.logMessage("In KPCheck() the RevenueCenterID was found to be -1. Unable to look for online orders as this is not a valid ID.", getKPLogFileName(), Logger.LEVEL.ERROR);
            }
        }
        catch (Exception ex)
        {
            Logger.logException("An exception occurred in KPCheck(), online orders may not be reaching the print controller: " + ex.toString(), getKPLogFileName(), ex);
        }

        return retVal;
    }

    // This is called by the terminal (in online mode) to get transaction details for printing to the kitchen printer
    // Returns ArrayList of print job details
    public ArrayList kpGetTranData(int paTransactionID)
    {
        // We have to put the info we need inside an arraylist because otherwise xml rpc can't parse the class
        ArrayList plainTextKPJobInfo = new ArrayList();

        try {
            Logger.logMessage("In kpGetTranData() getting the transaction data for transactionID " + paTransactionID, getKPLogFileName(), Logger.LEVEL.DEBUG);

            if (paTransactionID != -1)
            {
                ReceiptGen receiptGen = new ReceiptGen();
                Logger.logMessage("got new receiptGen " + receiptGen, getKPLogFileName(), Logger.LEVEL.DEBUG);
                receiptGen.setReceiptFormatter(new KitchenPrinterReceiptFormatter(new DataManager()));
                Logger.logMessage("setReceiptFormatter", getKPLogFileName(), Logger.LEVEL.DEBUG);
                IRenderer htmlRenderer = receiptGen.createBasicHTMLRenderer();
                Logger.logMessage("made HTML renderer " + htmlRenderer, getKPLogFileName(), Logger.LEVEL.DEBUG);
                receiptGen.setReceiptData(receiptGen.createReceiptData());
                Logger.logMessage("created receipt data" + htmlRenderer, getKPLogFileName(), Logger.LEVEL.DEBUG);
                receiptGen.getReceiptData().setPopupVal(true);
                Logger.logMessage("set popup value", getKPLogFileName(), Logger.LEVEL.DEBUG);
                if (htmlRenderer != null)
                {
                    // buildFromDBOrderNum is where we'll retrieve all of the transaction details from the database
                    receiptGen.buildFromDBOrderNum(paTransactionID, TypeData.ReceiptType.KITCHENPTRRECEIPT, htmlRenderer);
                    Logger.logMessage("built from DB order num", getKPLogFileName(), Logger.LEVEL.DEBUG);
                    plainTextKPJobInfo = receiptGen.getReceiptData().getKpJobInfo().getPlainTextDetails();
                    Logger.logMessage("got plain text details " + plainTextKPJobInfo, getKPLogFileName(), Logger.LEVEL.DEBUG);
                }
                else
                {
                    Logger.logMessage("In kpGetTranData() the htmlRenderer was found to be null. Unable to generate the receipt data for this transaction.", getKPLogFileName(), Logger.LEVEL.ERROR);
                }
            }
            else
            {
                Logger.logMessage("In kpGetTranData() the transactionID was found to be -1. Unable to get the data for this transaction as this is not a valid ID.", getKPLogFileName(), Logger.LEVEL.ERROR);
            }
        }
        catch (Exception ex)
        {
            Logger.logException("An exception occurred in kpGetTranData(), unable to generate the receipt data for this transaction", getKPLogFileName(), ex);
        }

        return plainTextKPJobInfo;
    }

    //  For online orders, transaction detail is on the server.
    //  This method will get the details from server and write it to our local print queue
    public boolean printOrderOnKitchenPrintersOnlineMode(int paTransactionID)
    {
        if (kitchenPrinterTerminal != null)
        {
            Logger.logMessage("PeripheralsDataManager.printOrderOnKitchenPrintersOnlineMode, PATransactionID=" + paTransactionID, getKPLogFileName(), Logger.LEVEL.DEBUG);
        }
        else
        {
            Logger.logMessage("printOrderOnKitchenPrintersOnlineMode: kitchenPrinterTerminal obj is null, returning", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        // Get transaction details from server
        ArrayList rcptDataFromServer = kitchenPrinterTerminal.getKitchenPrinterDataForOrderFromServer(paTransactionID);
        Logger.logMessage("PeripheralsDataManager.printOrderOnKitchenPrintersOnlineMode, rcptDataFromServer=" + rcptDataFromServer, getKPLogFileName(), Logger.LEVEL.DEBUG);
        // Create print job and add to the queue in the local DB
        if(rcptDataFromServer != null)
        {
            int printerQueueID = -1;
            JDCConnection conn = null;

            try {
                try {
                    conn = dm.pool.getConnection(false, getKPLogFileName());
                } catch (SQLException qe) {
                    Logger.logException("PeripheralsDataManager.printOrderOnKitchenPrintersOnlineMode no connection to DB.", getKPLogFileName(), qe);
                }

                conn.setAutoCommit(false);

                for (Object line : rcptDataFromServer)
                {
                    HashMap hm = (HashMap) line;

                    if (rcptDataFromServer.indexOf(line) == 0)
                    {
                        String estimatedOrderTime = hm.get("EstimatedOrderTime").toString().isEmpty() ? null : hm.get("EstimatedOrderTime").toString();

                        // Insert header (it's always the first hashmap in the arraylist)
                        printerQueueID = dm.parameterizedExecuteNonQuery("data.newKitchenPrinter.InsertPAPrinterQueueHeader",
                                new Object[]{
                                        Integer.parseInt(hm.get("PATransactionID").toString()), hm.get("TransactionDate"), hm.get("TerminalID"), hm.get("OrderTypeID"), hm.get("PersonName"),
                                        hm.get("PhoneNumber"), hm.get("TransComment"), hm.get("PickupDeliveryNote"), hm.get("IsOnlineOrder"), estimatedOrderTime, hm.get("OrderNum"),
                                        hm.get("TransName"), hm.get("TransNameLabel"), hm.get("TransTypeID"), hm.get("PrevOrderNumsForKDS")
                                }, getKPLogFileName(), conn );
                    }
                    else
                    {
                        // Insert detail
                        dm.parameterizedExecuteNonQuery("data.newKitchenPrinter.InsertIntoPAPrinterQueueDetail", new Object[]{ printerQueueID, hm.get("PrinterID"), hm.get("PrintStatusID"),
                                hm.get("PrintControllerID"), hm.get("PAPluID"), hm.get("Quantity"), hm.get("IsModifier"), hm.get("LineDetail"), hm.get("HideStation")}, getKPLogFileName(), conn);
                    }
                }

                conn.commit();
            }
            catch (SQLException se)
            {
                Logger.logException("PeripheralsDataManager.printOrderOnKitchenPrintersOnlineMode error", getKPLogFileName(), se);
            }
            finally
            {
                try
                {
                    conn.setAutoCommit(true);
                    dm.pool.returnConnection(conn, getKPLogFileName());
                }
                catch (SQLException e)
                {
                    Logger.logException(e, getKPLogFileName());
                }
            }
            Logger.logMessage("PeripheralsDataManager.printOrderOnKitchenPrintersOnlineMode, returning true", getKPLogFileName(), Logger.LEVEL.DEBUG);
            return true;
        }
        Logger.logMessage("PeripheralsDataManager.printOrderOnKitchenPrintersOnlineMode, returning false", getKPLogFileName(), Logger.LEVEL.DEBUG);
        return false;
    }


    // For offline orders transaction details are in the local db.
    // This method will write print data to our local print queue
    public boolean printOrderOnKitchenPrintersOfflineMode(int paTransactionID)
    {
        if (kitchenPrinterTerminal != null)
        {
            Logger.logMessage("PeripheralsDataManager.printOrderOnKitchenPrintersOfflineMode, paTransactionID=" + paTransactionID, getKPLogFileName(), Logger.LEVEL.DEBUG);
        }
        else
        {
            Logger.logMessage("printOrderOnKitchenPrintersOfflineMode: kitchenPrinterTerminal obj is null, returning", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        HashMap<Integer, Integer> printerKDSStationIDMap = getPrinterIDKDSLookup();

        ReceiptGen receiptGen = new ReceiptGen();
        receiptGen.setReceiptFormatter(new KitchenPrinterReceiptFormatter(dm));
        IRenderer htmlRenderer = receiptGen.createBasicHTMLRenderer();
        receiptGen.setReceiptData(receiptGen.createReceiptData());
        if (htmlRenderer != null)
        {
            if(receiptGen.buildFromDBOrderNum(paTransactionID, TypeData.ReceiptType.KITCHENPTRRECEIPT, htmlRenderer))
            {
                int printerQueueID = -1;

                //return true;
                // If we successfully built the print lines, then put them into the local database/print queue (QC_PAPrintQueue and QC_PAPrintQueueDetail)
                KitchenPrinterJobInfo kpJobInfo = receiptGen.getReceiptData().getKpJobInfo();
                KPJobHeader kpJobHeader = kpJobInfo.getKpJobHeader();

                // Add the header to the QC_PAPrinterQueue table
                String transactionID = String.valueOf(kpJobHeader.getPATransactionID());
                String transactionDate = kpJobHeader.getTransactionDate();
                String terminalID = String.valueOf(kpJobHeader.getTerminalID());
                String orderTypeID = String.valueOf(kpJobHeader.getOrderTypeID());
                String personName = kpJobHeader.getPersonName();
                String phoneNumber = kpJobHeader.getPhoneNumber();
                String transComment = kpJobHeader.getTransComment();
                String pickupDeliveryNote = kpJobHeader.getPickupDeliveryNote();
                String isOnlineOrder = String.valueOf((kpJobHeader.getIsOnlineOrder() ? 1 : 0));
                String orderNum = kpJobHeader.getOrderNumber();
                String transName = kpJobHeader.getTransName();
                String transNameLabel = kpJobHeader.getTransNameLabel();
                String transTypeID = String.valueOf(kpJobHeader.getTransTypeID());
                String prevOrderNumsForKDS = (((kpJobHeader.getPrevOrderNumsForKDS() != null) && (!kpJobHeader.getPrevOrderNumsForKDS().isEmpty())) ? kpJobHeader.getPrevOrderNumsForKDS() : "");

                printerQueueID = dm.parameterizedExecuteNonQuery("data.newKitchenPrinter.InsertPAPrinterQueueHeader",
                        new Object[]{
                                paTransactionID, transactionDate, terminalID, orderTypeID, personName,
                                phoneNumber, transComment, pickupDeliveryNote, isOnlineOrder, null, orderNum,
                                transName, transNameLabel, transTypeID, prevOrderNumsForKDS
                        }, getKPLogFileName() );

                // Add all of the details to the QC_PAPrinterQueueDetail table
                for (KPJobDetail detail : kpJobInfo.getDetails())
                {
                    String printerID = String.valueOf(detail.getPrinterID());
                    String printStatusID = String.valueOf(detail.getPrintStatusID());
                    String printControllerID = String.valueOf(detail.getPrinterHostID());
                    String paPluID = String.valueOf(detail.getPAPluID());
                    String quantity = String.valueOf(detail.getQuantity());
                    String isModifier = String.valueOf((detail.getIsModifier() ? 1 : 0));
                    String lineDetail = "";
                    if ((isKDS(detail.getPrinterID(), printerKDSStationIDMap)) && (detail.getPAPluID() > 0)) {
                        String decodedLineDetail = new String(Base64.getDecoder().decode(detail.getLineDetail().getBytes()));
                        String indicatorText = (decodedLineDetail.startsWith("\t") ? KitchenDisplaySystemJobInfo.MODIFIER_INDICATOR : KitchenDisplaySystemJobInfo.PRODUCT_INDICATOR);
                        decodedLineDetail = decodedLineDetail.replaceAll("\\d+X", "").trim(); // remove the quantity
                        String lineItemIDText = (detail.getPaTransLineItemID() > 0 ? String.valueOf(detail.getPaTransLineItemID()) + "__" : "");
                        String printerIDText = printerID + "__";
                        lineDetail = Base64.getEncoder().encodeToString((indicatorText + lineItemIDText + printerIDText + decodedLineDetail).getBytes());
                    }
                    else if (!isKDS(detail.getPrinterID(), printerKDSStationIDMap)) {
                        lineDetail = detail.getLineDetail();
                    }
                    if (!StringFunctions.stringHasContent(lineDetail)) {
                        continue;
                    }
                    String hideStation = "";
                    if ((detail.getHideStation() != null) && (!detail.getHideStation().isEmpty()) && (org.apache.commons.lang.NumberUtils.isNumber(detail.getHideStation()))) {
                        hideStation = detail.getHideStation();
                    }

                    // Insert detail
                    dm.parameterizedExecuteNonQuery("data.newKitchenPrinter.InsertIntoPAPrinterQueueDetail", new Object[]{ printerQueueID, printerID, printStatusID,
                            printControllerID, paPluID, quantity, isModifier, lineDetail, hideStation}, getKPLogFileName());
                }
            }
        }

        return false;
    }


    /**
     * <p>Gets a map between printer ID and KDS station ID's.</p>
     *
     * @return A {@link HashMap} whose {@link Integer} key is a printer ID and whose {@link Integer} value is the corresponding KDS station ID.
     */
    private HashMap<Integer, Integer> getPrinterIDKDSLookup () {

        HashMap<Integer, Integer> lookup = new HashMap<>();

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetPtrKDSStnIDs").setLogFileName(getKPLogFileName());
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dm));
        for (HashMap hm : queryRes) {
            int printerID = HashMapDataFns.getIntVal(hm, "PRINTERID");
            int kdsStationID = HashMapDataFns.getIntVal(hm, "STATIONID");
            lookup.put(printerID, kdsStationID);
        }

        return lookup;
    }


    /**
     * <p>Determines whether or not the printer ID if for a KDS station.</p>
     *
     * @param printerID Printer ID to check.
     * @param lookup A {@link HashMap} whose {@link Integer} key is a printer ID and whose {@link Integer} value is the corresponding KDS station ID.
     * @return Whether or not the printer ID if for a KDS station.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private boolean isKDS (int printerID, HashMap<Integer, Integer> lookup) {

        // check we have a printer ID
        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to PeripheralsDataManager.isKDS must be greater than 0!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        // check we have a lookup map
        if (DataFunctions.isEmptyMap(lookup)) {
            Logger.logMessage("The lookup map passed to PeripheralsDataManager.isKDS can't be null!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        if ((lookup.containsKey(printerID)) && (lookup.get(printerID) != null) && (lookup.get(printerID) > 0)) {
            return true;
        }

        return false;
    }


    /**
     * <p>Prints, emails, or prints and emails kitchen print jobs.</p>
     *
     * @param printJobs An {@link ArrayList} of {@link ArrayList} corresponding to the print jobs to print locally.
     */
    public void handleKitchenPrintJobLocally (ArrayList<ArrayList> printJobs) {

        // confirm that there are print jobs that need to be printed
        if (DataFunctions.isEmptyCollection(printJobs)) {
            Logger.logMessage("The print jobs passed to PeripheralsDataManager.handleKitchenPrintJobLocally can't be null or " +
                    "empty, unable to print or email any failed kitchen print jobs!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return;
        }

        // determine if the error receipt should be printed, emailed, or both
        boolean printReceipt = false;
        boolean emailReceipt = false;
        HashMap aPrintJobDetail = ((HashMap) printJobs.get(0).get(0));
        int terminalID = HashMapDataFns.getIntVal(aPrintJobDetail, "TERMINALID");
        // if the order was placed from an online ordering terminal than use error email settings of the printer host
        if (KitchenPrinterCommon.isOOTerminal(getKPLogFileName(), dm, terminalID)) {
            terminalID = KitchenPrinterCommon.getTerminalIDFromMacAndHostname(getKPLogFileName(), dm, getTerminalMacAddress(), getPHHostname());
        }
        if (terminalID > 0) {
            Logger.logMessage(String.format("Now checking whether or not the terminal with an ID of %s should print errored receipts, email errored receipts, or both...",
                    Objects.toString(terminalID, "N/A")), getKPLogFileName(), Logger.LEVEL.TRACE);
            DynamicSQL sql = new DynamicSQL("data.kms.GetKitchenErrorReceiptType").addIDList(1, terminalID).setLogFileName(getKPLogFileName());
            ArrayList<HashMap> queryRes = sql.serialize(dm);
            if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
                printReceipt = HashMapDataFns.getBooleanVal(queryRes.get(0), "PRINTERRORRECEIPTSLOCALLY");
                emailReceipt = HashMapDataFns.getBooleanVal(queryRes.get(0), "EMAILERRORRECEIPT");
            }
        }
        else {
            // try to print the receipt locally
            printReceipt = true;
        }

        Logger.logMessage("Print error receipt for terminal: " + terminalID + "? " + (printReceipt ? "YES" : "NO"), getKPLogFileName(), Logger.LEVEL.TRACE);
        Logger.logMessage("Email error receipt for terminal: " + terminalID + "? " + (emailReceipt ? "YES" : "NO"), getKPLogFileName(), Logger.LEVEL.TRACE);

        if ((printReceipt) && (emailReceipt)) {
            if (!printKitchenPrintJobsLocally(printJobs)) {
                Logger.logMessage("Failed to print the kitchen print jobs locally in PeripheralsDataManager.handleKitchenPrintJobLocally!", getKPLogFileName(), Logger.LEVEL.ERROR);
            }
            if (!emailKitchenPrintJobsLocally(printJobs)) {
                Logger.logMessage("Failed to email the kitchen print jobs locally in PeripheralsDataManager.handleKitchenPrintJobLocally!", getKPLogFileName(), Logger.LEVEL.ERROR);
            }
        }
        else if (printReceipt) {
            if (!printKitchenPrintJobsLocally(printJobs)) {
                Logger.logMessage("Failed to print the kitchen print jobs locally in PeripheralsDataManager.handleKitchenPrintJobLocally!", getKPLogFileName(), Logger.LEVEL.ERROR);
            }
        }
        else if (emailReceipt) {
            if (!emailKitchenPrintJobsLocally(printJobs)) {
                Logger.logMessage("Failed to email the kitchen print jobs locally in PeripheralsDataManager.handleKitchenPrintJobLocally!", getKPLogFileName(), Logger.LEVEL.ERROR);
            }
        }
        else {
            // default to printing a receipt
            if (!printKitchenPrintJobsLocally(printJobs)) {
                Logger.logMessage("Failed to print the kitchen print jobs locally in PeripheralsDataManager.handleKitchenPrintJobLocally!", getKPLogFileName(), Logger.LEVEL.ERROR);
            }
        }

    }


    /**
     * <p>Prints the details in the local print job locally.</p>
     *
     * @param renderer The {@link IRenderer} to use to render the receipt.
     * @param localPrintJob The {@link LocalPrintJob} that contains the details to print.
     * @return Whether or not the details on the local print job printed locally.
     */
    @SuppressWarnings({"ThrowableResultOfMethodCallIgnored", "unchecked", "Convert2streamapi"})
    private boolean printDetailsLocally (IRenderer renderer, LocalPrintJob localPrintJob) {

        // validate the renderer
        if (renderer == null) {
            Logger.logMessage("The renderer passed to PeripheralsDataManager.printDetailsLocally can't be null, unable to print " +
                    "the print job's details locally, now returning null.", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        // validate the LocalPrintJob
        if (localPrintJob == null) {
            Logger.logMessage("The LocalPrintJob passed to PeripheralsDataManager.printDetailsLocally can't be null, unable to print " +
                    "the print job's details locally, now returning null.", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        // make sure there are details to print
        if (DataFunctions.isEmptyCollection(localPrintJob.getDetails())) {
            Logger.logMessage("There are no details within the print job that should be printed locally in PeripheralsDataManager.printDetailsLocally, " +
                    "now returning false.", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        for (HashMap detail : localPrintJob.getDetails()) {
            if (!DataFunctions.isEmptyMap(detail)) {
                // get the line detail to be printed
                String encodedLineDetail = HashMapDataFns.getStringVal(detail, "LINEDETAIL");
                String lineDetail = "";
                if (StringFunctions.stringHasContent(encodedLineDetail)) {
                    lineDetail = new String(Base64.getDecoder().decode(encodedLineDetail));
                }

                // print the line detail
                if (StringFunctions.stringHasContent(lineDetail)) {
                    if (StringFunctions.rkContains(KitchenPrinterReceiptFormatter.JPOS_CUT, lineDetail, 37)) {
                        renderer.cutPaper();
                    }
                    else if (StringFunctions.rkContains(KitchenPrinterReceiptFormatter.BARCODE, lineDetail, 37)) {
                        String paTransactionID = lineDetail.replace(KitchenPrinterReceiptFormatter.BARCODE, "");
                        renderer.addBlankLine();
                        renderer.addBlankLine();
                        renderer.addBarcode("QCPOS" + paTransactionID);
                    }
                    else {
                        Logger.logMessage(String.format("Added line to the local error receipt: %s",
                                Objects.toString(lineDetail, "N/A")), getKPLogFileName(), Logger.LEVEL.TRACE);
                        renderer.addLine(lineDetail);
                        if (renderer.getLastException() != null) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }


    /**
     * <p>Emails out the kitchen print jobs.</p>
     *
     * @param printJobs An {@link ArrayList} of {@link ArrayList} corresponding to the print jobs to email.
     * @return Whether or not the kitchen print jobs could be emailed successfully.
     */
    private boolean emailKitchenPrintJobsLocally (ArrayList<ArrayList> printJobs) {

        // confirm that there are print jobs that need to be printed
        if (DataFunctions.isEmptyCollection(printJobs)) {
            Logger.logMessage("The print jobs passed to PeripheralsDataManager.emailKitchenPrintJobsLocally can't be null or " +
                    "empty, unable to email any failed kitchen print jobs, returning false!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        EmailedErrorReceipt emailedErrorReceipt = new EmailedErrorReceipt(getKPLogFileName(), dm, printJobs);
        ArrayList<LocalPrintJob> localPrintJobs = emailedErrorReceipt.fixPrintJobs();

        boolean errorHappened = false;
        if (!DataFunctions.isEmptyCollection(localPrintJobs)) {
            // iterate through the local print jobs and send an email for each
            for (LocalPrintJob localPrintJob : localPrintJobs) {
                if (emailedErrorReceipt.buildErrorReceipt(localPrintJob)) {
                    errorHappened = false;
                    if (!KitchenPrinterTerminal.getInstance().getPeripheralsDataManager().updatePrintStatusOfPAPrinterQueue(localPrintJob.getPAPrinterQueueID(), PrintStatusType.PRINTED)) {
                        Logger.logMessage(String.format("Failed to update the status of the print queue record with a PAPrinterQueueID of %s to a print " +
                                        "status ID of %s in PeripheralsDataManager.emailKitchenPrintJobsLocally!",
                                Objects.toString(localPrintJob.getPAPrinterQueueID(), "N/A"),
                                Objects.toString(PrintStatusType.PRINTED, "N/A")), getKPLogFileName(), Logger.LEVEL.TRACE);
                    }

                    // update the print status of the print queue details
                    Set<Integer> paPrinterQueueDetailIDs = DataFunctions.getUniqueValsAtKeyFromALOfHM(localPrintJob.getDetails(), "PAPRINTERQUEUEDETAILID");
                    if ((!DataFunctions.isEmptyCollection(paPrinterQueueDetailIDs))
                            && (!KitchenPrinterTerminal.getInstance().getPeripheralsDataManager().updatePrintStatusOfPAPrinterQueueDetails(new ArrayList<>(paPrinterQueueDetailIDs), PrintStatusType.PRINTED))) {
                        Logger.logMessage(String.format("Failed to update the status of the print queue detail records for PAPrinterQueueIDs %s to a print " +
                                        "status ID of %s in PeripheralsDataManager.emailKitchenPrintJobsLocally!",
                                Objects.toString(StringFunctions.buildStringFromList(new ArrayList<>(paPrinterQueueDetailIDs), ","), "N/A"),
                                Objects.toString(PrintStatusType.PRINTED, "N/A")), getKPLogFileName(), Logger.LEVEL.TRACE);
                    }
                }
                else {
                    errorHappened = true;
                    // update the print status of the print queue record
                    if (!KitchenPrinterTerminal.getInstance().getPeripheralsDataManager().updatePrintStatusOfPAPrinterQueue(localPrintJob.getPAPrinterQueueID(), PrintStatusType.FAILEDLOCALPRINT)) {
                        Logger.logMessage(String.format("Failed to update the status of the print queue record with a PAPrinterQueueID of %s to a print " +
                                        "status ID of %s in PeripheralsDataManager.emailKitchenPrintJobsLocally!",
                                Objects.toString(localPrintJob.getPAPrinterQueueID(), "N/A"),
                                Objects.toString(PrintStatusType.FAILEDLOCALPRINT, "N/A")), getKPLogFileName(), Logger.LEVEL.ERROR);
                    }

                    // update the print status of the print queue details
                    Set<Integer> paPrinterQueueDetailIDs = DataFunctions.getUniqueValsAtKeyFromALOfHM(localPrintJob.getDetails(), "PAPRINTERQUEUEDETAILID");
                    if ((!DataFunctions.isEmptyCollection(paPrinterQueueDetailIDs))
                            && (!KitchenPrinterTerminal.getInstance().getPeripheralsDataManager().updatePrintStatusOfPAPrinterQueueDetails(new ArrayList<>(paPrinterQueueDetailIDs), PrintStatusType.FAILEDLOCALPRINT))) {
                        Logger.logMessage(String.format("Failed to update the status of the print queue detail records for PAPrinterQueueIDs %s to a print " +
                                        "status ID of %s in PeripheralsDataManager.emailKitchenPrintJobsLocally!",
                                Objects.toString(StringFunctions.buildStringFromList(new ArrayList<>(paPrinterQueueDetailIDs), ","), "N/A"),
                                Objects.toString(PrintStatusType.FAILEDLOCALPRINT, "N/A")), getKPLogFileName(), Logger.LEVEL.ERROR);
                    }
                }
            }
        }

        return !errorHappened;
    }


    /**
     * <p>Confirms whether or not a print job has expired or entered an errored state.</p>
     *
     * @param localPrintJob The {@link LocalPrintJob} to check.
     * @return Whether or not a print job has expired or entered an errored state.
     */
    private boolean isLocalPrintJobExpiredOrErrored (LocalPrintJob localPrintJob) {

        // make sure the LocalPrintJob is valid
        if (localPrintJob == null) {
            Logger.logMessage("The LocalPrintJob passed to PeripheralsDataManager.isLocalPrintJobExpiredOrErrored can't be null, unable " +
                    "to confirm whether or not the print job has expired or is in an errored state, now returning false.", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the print job has details we can use to determine the status of the print job
        if ((DataFunctions.isEmptyCollection(localPrintJob.getDetails())) || ((!DataFunctions.isEmptyCollection(localPrintJob.getDetails())) && (DataFunctions.isEmptyMap(localPrintJob.getDetails().get(0))))) {
            Logger.logMessage("Unable to use the details of the LocalPrintJob passed to PeripheralsDataManager.isLocalPrintJobExpiredOrErrored to confirm " +
                    "whether or not the print job has expired or is in an errored state, now returning false.", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        // cache a detail from the print job to reference back to
        HashMap refHM = localPrintJob.getDetails().get(0);

        // check if the print job errored out
        if (HashMapDataFns.getIntVal(refHM, "PRINTSTATUSID") == PrintStatusType.ERROR) {
            return true;
        }

        // check if the print job expired
        LocalDateTime estimatedOrderTime = HashMapDataFns.getLocalDateTimeVal(refHM, "ESTIMATEDORDERTIME");
        LocalDateTime transactionDate = HashMapDataFns.getLocalDateTimeVal(refHM, "TRANSACTIONDATE");
        // determine when the print job expired
        int printJobExpirationInMins = kitchenPrinterTerminal.getPrintJobExpirationInMinutes();
        LocalDateTime expirationTime = null;
        if (estimatedOrderTime != null) {
            expirationTime = estimatedOrderTime.plusMinutes(printJobExpirationInMins);
        }
        else if (transactionDate != null) {
            expirationTime = transactionDate.plusMinutes(printJobExpirationInMins);
        }
        if (expirationTime == null) {
            Logger.logMessage(String.format("Unable to determine whether or not the errored kitchen print job with an ID of %s is expired, marking it as expired so it will print locally!",
                    Objects.toString(localPrintJob.getPAPrinterQueueID(), "N/A")), getKPLogFileName(), Logger.LEVEL.ERROR);
            return true;
        }

        return LocalDateTime.now().isAfter(expirationTime);
    }

    /**
     * <p>Adds the name of the KMS station to the error receipt details.</p>
     *
     * @param localPrintJob The {@link LocalPrintJob} for the failed print job on the KMS station.
     * @param kmsStationNameLookup A {@link HashMap} whose {@link Integer} key is the ID of a KMS station and whose {@link String} value is that name of the KMS station.
     */
    @SuppressWarnings("unchecked")
    private void addKMSStationNameToDetails (LocalPrintJob localPrintJob, HashMap<Integer, String> kmsStationNameLookup) {

        // make sure the LocalPrintJob is valid
        if (localPrintJob == null) {
            Logger.logMessage("The LocalPrintJob passed to PeripheralsDataManager.addKMSStationNameToDetails can't be null, unable " +
                    "to add the KMS station name to the print job details, now returning from the method.", getKPLogFileName(), Logger.LEVEL.ERROR);
            return;
        }

        // make sure the print job details are valid
        if (DataFunctions.isEmptyCollection(localPrintJob.getDetails())) {
            Logger.logMessage("No print job details have been passed to PeripheralsDataManager.addKMSStationNameToDetails, unable " +
                    "to add the KMS station name to the print job details, now returning from the method.", getKPLogFileName(), Logger.LEVEL.ERROR);
            return;
        }

        // make sure the station name lookup is valid
        if (DataFunctions.isEmptyMap(kmsStationNameLookup)) {
            Logger.logMessage("The lookup between KMS station ID and KMS station name passed to PeripheralsDataManager.addKMSStationNameToDetails can't be null or empty, unable " +
                    "to add the KMS station name to the print job details, now returning from the method.", getKPLogFileName(), Logger.LEVEL.ERROR);
            return;
        }

        // determine where to insert the KMS station name in the details
        int insertionIndex = 0;
        for (int i = 0; i < localPrintJob.getDetails().size(); i++) {
            HashMap detail = localPrintJob.getDetails().get(i);
            String decodedLineDetail = new String(Base64.getDecoder().decode(HashMapDataFns.getStringVal(detail, "LINEDETAIL")));
            if (StringFunctions.stringHasContent(decodedLineDetail)) {
                String orderNumber = HashMapDataFns.getStringVal(detail, "ORDERNUM");
                if (decodedLineDetail.equalsIgnoreCase(JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + JPOS_COMMAND.ESC + JPOS_COMMAND.LARGE_FONT + orderNumber)) {
                    insertionIndex = i;
                    break;
                }
            }
        }

        if (insertionIndex > 0) {
            // copy the detail at the insertion index
            HashMap copy = ((HashMap) localPrintJob.getDetails().get(insertionIndex).clone());
            // update the line detail for the copy
            copy.put("LINEDETAIL", new String(Base64.getEncoder().encode((JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_CENTER + kmsStationNameLookup.get(localPrintJob.getKMSStationID())).getBytes())));
            // add the copy into the details for the LocalPrintJob at the insertion index
            localPrintJob.getDetails().add(insertionIndex, copy);
        }

    }


    /**
     * Prints and expired or errored print jobs on the local receipt printer.
     *
     * @param printJobs {@link ArrayList<ArrayList>} Any expired or errored print jobs to be printed locally.
     */
    @SuppressWarnings({"ConstantConditions", "SynchronizeOnNonFinalField", "TypeMayBeWeakened", "ThrowableResultOfMethodCallIgnored", "MagicNumber",
            "OverlyComplexMethod", "SpellCheckingInspection", "unchecked", "Convert2streamapi", "MismatchedQueryAndUpdateOfCollection"})
    public void printKitchenPrinterJobLocally (ArrayList<ArrayList> printJobs) {

        try {
            // make sure we have a printer that can print these print jobs locally
            createPrinterIfNeeded();

            boolean failedLocalPrint = false;
            POSPrinter posPrinter;

            if (printer != null) {
                synchronized (printer) {
                    if (!printer.isInErrorState()) {
                        ReceiptGen receiptGen = new ReceiptGen();
                        posPrinter = printer.getPOSPrinter();
                        IRenderer renderer;
                        // we have a printer that can print the errored or expired print jobs locally
                        if (posPrinter != null) {
                            // create the receipt renderer
                            renderer = receiptGen.createPrinterRenderer(posPrinter, printer.getLogicalPrinterName());
                            if (renderer != null) {

                                // create and print the receipt
                                //
                                // SNBC and NCR printers take longer to print sale receipts, wait 1 second between checks
                                // to see if the sale receipt finished printing, perform up to 10 checks
                                if ((isSNBCPrinterConnected) || (isNCRPrinterConnected)) {
                                    for (int i = 0; i < 10; i++) {
                                        int printerState = posPrinter.getState();
                                        // sale receipt finished printing
                                        if (printer.getPrinterStateMsg(printerState).contains("Device is Idle")) {
                                            break;
                                        }
                                        // wait for sale receipt to finish printing
                                        else {
                                            Logger.logMessage("Printer is currently busy, waiting 1 second before trying to print " +
                                                    "again in PeripheralsDataManager.printKitchenPrinterJobLocally", getKPLogFileName(), Logger.LEVEL.TRACE);
                                            Thread.sleep(1000L);
                                        }
                                    }
                                }

                                // make sure we can turn off transaction printing mode
                                if ((!renderer.end()) && (renderer.getLastException() != null)) {
                                    Logger.logMessage(String .format("Unable to turn off transaction printing mode for the receipt renderer, " +
                                                    "marking the given print job(s) as print job(s) that failed to print locally, error message: %s, in " +
                                                    "PeripheralsDataManager.printKitchenPrinterJobLocally",
                                            Objects.toString(renderer.getLastException().getMessage(), "NULL")), getKPLogFileName(), Logger.LEVEL.ERROR);
                                    failedLocalPrint = true;
                                }

                                // make sure we can turn on transaction printing mode
                                if (!renderer.begin()) {
                                    Logger.logMessage("Unable to turn on transaction printing mode, the printer may be in an error " +
                                            "state in PeripheralsDataManager.printKitchenPrinterJobLocally", getKPLogFileName(), Logger.LEVEL.ERROR);
                                }

                                // iterate through the print job details and create print jobs for each expired print job
                                HashMap<Integer, HashMap<Integer, ArrayList<HashMap>>> organizedExpiredErroredPrintJobs = reorganizeExpiredErroredPrintJobs(printJobs);
                                if ((!DataFunctions.isEmptyMap(organizedExpiredErroredPrintJobs)) && (!failedLocalPrint)) {
                                    for (Map.Entry<Integer, HashMap<Integer, ArrayList<HashMap>>> organizedExpiredErroredPrintJobsEntry : organizedExpiredErroredPrintJobs.entrySet()) {
                                        int paPrinterQueueID = organizedExpiredErroredPrintJobsEntry.getKey();
                                        HashMap<Integer, ArrayList<HashMap>> printJob = organizedExpiredErroredPrintJobsEntry.getValue();
                                        Logger.logMessage(String.format("Now printing the print job with a PAPrinterQueueID of %s locally.",
                                                Objects.toString(paPrinterQueueID, "N/A")), getKPLogFileName(), Logger.LEVEL.TRACE);
                                        boolean didPrintJobFailLocally = false;
                                        if (!DataFunctions.isEmptyMap(printJob)) {
                                            for (Map.Entry<Integer, ArrayList<HashMap>> printJobEntry : printJob.entrySet()) {
                                                int printerID = printJobEntry.getKey();
                                                ArrayList<HashMap> printJobDetails = printJobEntry.getValue();

                                                // check if the print job should have been for KDS
                                                boolean isKDS = false;
                                                if ((!DataFunctions.isEmptyCollection(printJobDetails)) && (!DataFunctions.isEmptyMap(printJobDetails.get(0)))) {
                                                    isKDS = (HashMapDataFns.getBooleanVal(printJobDetails.get(0), "ISKDS"));
                                                }

                                                // check if the print job is from an online order
                                                boolean isOnlineOrder = false;
                                                if ((!DataFunctions.isEmptyCollection(printJobDetails)) && (!DataFunctions.isEmptyMap(printJobDetails.get(0)))) {
                                                    isOnlineOrder = (HashMapDataFns.getBooleanVal(printJobDetails.get(0), "ONLINEORDER"));
                                                }

                                                // get the ID of the terminal that made the transaction
                                                int terminalID = -1;
                                                if ((!DataFunctions.isEmptyCollection(printJobDetails)) && (!DataFunctions.isEmptyMap(printJobDetails.get(0)))) {
                                                    terminalID = (HashMapDataFns.getIntVal(printJobDetails.get(0), "TERMINALID"));
                                                }

                                                int revenueCenterID = -1;
                                                if ((terminalID > 0) && (!DataFunctions.isEmptyCollection(DataFunctions.purgeAlOfHm(printJobDetails)))) {
                                                    revenueCenterID = getRevenueCenterFromTerminal(terminalID);
                                                    for (HashMap printJobDetailHM : printJobDetails) {
                                                        printJobDetailHM.put("REVENUECENTERID", revenueCenterID);
                                                    }
                                                }

                                                Logger.logMessage("Printer ID: "+Objects.toString(printerID, "N/A"), getKPLogFileName(), Logger.LEVEL.DEBUG);
                                                Logger.logMessage("Is KDS: "+Objects.toString((isKDS ? "YES" : "NO"), "N/A"), getKPLogFileName(), Logger.LEVEL.DEBUG);
                                                Logger.logMessage("Is Online Order: "+Objects.toString((isOnlineOrder ? "YES" : "NO"), "N/A"), getKPLogFileName(), Logger.LEVEL.DEBUG);
                                                Logger.logMessage("Terminal ID: "+Objects.toString(terminalID, "N/A"), getKPLogFileName(), Logger.LEVEL.DEBUG);
                                                Logger.logMessage("Revenue Center ID: "+Objects.toString(revenueCenterID, "N/A"), getKPLogFileName(), Logger.LEVEL.DEBUG);

                                                // check if print job details for the printer are errored or expired before printing them locally
                                                if (!arePrintJobsDetailsForPrinterErroredOrExpired(paPrinterQueueID, printerID, terminalID, isKDS, isOnlineOrder)) {
                                                    continue; // don't print a receipt for failed print jobs on this printer
                                                }

                                                Logger.logMessage(String.format("Now printing the details intended for the printer with an ID of %s locally.",
                                                        Objects.toString(printerID, "N/A")), getKPLogFileName(), Logger.LEVEL.TRACE);

                                                if (isKDS) {
                                                    printJobDetails = modifyKDSPrintJobDetails(printJobDetails);
                                                    printJobDetails = swapRedirectedHeaderDetails(printJobDetails);
                                                    if (printPrintJobDetailsLocally(renderer, printJobDetails)) {
                                                        updatePrintQueueDetailsPrintStatus(printJobDetails, PrintJobStatusType.PRINTED.getTypeID());
                                                    }
                                                    else {
                                                        didPrintJobFailLocally = true;
                                                        updatePrintQueueDetailsPrintStatus(printJobDetails, PrintJobStatusType.FAILEDLOCALPRINT.getTypeID());
                                                    }
                                                }
                                                else {
                                                    printJobDetails = swapRedirectedHeaderDetails(printJobDetails);
                                                    if (printPrintJobDetailsLocally(renderer, printJobDetails)) {
                                                        updatePrintQueueDetailsPrintStatus(printJobDetails, PrintJobStatusType.PRINTED.getTypeID());
                                                    }
                                                    else {
                                                        didPrintJobFailLocally = true;
                                                        updatePrintQueueDetailsPrintStatus(printJobDetails, PrintJobStatusType.FAILEDLOCALPRINT.getTypeID());
                                                    }
                                                }
                                            }
                                        }
                                        // update the status of the print job in QC_PAPrinterQueue
                                        if (!didPrintJobFailLocally) {
                                            updatePrintQueueHeaderPrintStatus(paPrinterQueueID, PrintJobStatusType.PRINTED.getTypeID());
                                        }
                                        else {
                                            updatePrintQueueHeaderPrintStatus(paPrinterQueueID, PrintJobStatusType.FAILEDLOCALPRINT.getTypeID());
                                        }
                                    }
                                }
                                // turn off transaction printing mode
                                renderer.end();
                            }
                            else if (renderer == null) {
                                Logger.logMessage("The receipt renderer is unavailable, marking the given print job(s) as " +
                                        "print job(s) that failed to print locally in " +
                                        "PeripheralsDataManager.printKitchenPrinterJobLocally", getKPLogFileName(), Logger.LEVEL.ERROR);
                                failedLocalPrint = true;
                            }
                            else if (DataFunctions.isEmptyCollection(printJobs)) {
                                Logger.logMessage("There are no given expired or errored print job(s) to print locally " +
                                        "in PeripheralsDataManager.printKitchenPrinterJobLocally", getKPLogFileName(), Logger.LEVEL.ERROR);
                                failedLocalPrint = true;
                            }
                        }
                        else {
                            Logger.logMessage("The POS printer is unavailable, marking the given print job(s) as print job(s) that " +
                                            "failed to print locally in PeripheralsDataManager.printKitchenPrinterJobLocally",
                                    getKPLogFileName(), Logger.LEVEL.ERROR);
                            failedLocalPrint = true;
                        }
                    }
                }
            }
            else {
                Logger.logMessage("The local printer is unavailable, marking the given print job(s) as print job(s) that " +
                                "failed to print locally in PeripheralsDataManager.printKitchenPrinterJobLocally",
                        getKPLogFileName(), Logger.LEVEL.ERROR);
                failedLocalPrint = true;
            }

            // update the status of print jobs that failed to print locally
            if (failedLocalPrint) {
                ArrayList<HashMap> allPrintJobDetails = getAllPrintJobDetailsInAllPrintJobs(printJobs);
                if (!DataFunctions.isEmptyCollection(allPrintJobDetails)) {
                    updatePrintQueueDetailsPrintStatus(allPrintJobDetails, PrintJobStatusType.FAILEDLOCALPRINT.getTypeID());
                    updatePrintQueueHeaderPrintStatus(HashMapDataFns.getIntVal(allPrintJobDetails.get(0), "PAPRINTERQUEUEID"), PrintJobStatusType.FAILEDLOCALPRINT.getTypeID());
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, getKPLogFileName());
            Logger.logMessage("There was a problem trying to print an expired or errored print jobs locally in " +
                    "PeripheralsDataManager.printKitchenPrinterJobLocally", getKPLogFileName(), Logger.LEVEL.ERROR);
        }

    }


    /**
     * <p>Organizes failed print jobs by their intended printer or station.</p>
     *
     * @param printJobs An {@link ArrayList} of {@link ArrayList} corresponding to the print jobs that have expired or errored out.
     * @return A {@link HashMap} while key is the {@link Integer} ID of the print job and whose values is a {@link HashMap}
     * whose key is the {@link Integer} ID of the intended printer or station for the print job and whose value is an
     * {@link ArrayList} of {@link HashMap} corresponding to the details within the print job.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private HashMap<Integer, HashMap<Integer, ArrayList<HashMap>>> reorganizeExpiredErroredPrintJobs (ArrayList<ArrayList> printJobs) {
        HashMap<Integer, HashMap<Integer, ArrayList<HashMap>>> allPrintJobsHM = new HashMap<>(); // for all print jobs

        try {

            // make sure there are print jobs to organize
            if (DataFunctions.isEmptyCollection(printJobs)) {
                Logger.logMessage("No valid print jobs were passed to PeripheralsDataManager.reorganizeExpiredErroredPrintJobs!", getKPLogFileName(), Logger.LEVEL.ERROR);
                return null;
            }

            for (ArrayList printJob : printJobs) {
                // get all the print job details within the print job
                ArrayList<HashMap> allPrintJobDetails = getAllPrintJobDetails(printJob);
                // get the ID of the print job
                int printJobID = 0;
                if ((!DataFunctions.isEmptyCollection(allPrintJobDetails)) && (!DataFunctions.isEmptyMap(allPrintJobDetails.get(0)))) {
                    printJobID = HashMapDataFns.getIntVal(allPrintJobDetails.get(0), "PAPRINTERQUEUEID");
                }
                HashMap<Integer, ArrayList<HashMap>> sortedPrintJobDetails = sortPrintJobDetailsByPrinter(allPrintJobDetails);
                if ((printJobID > 0) && (!DataFunctions.isEmptyMap(sortedPrintJobDetails))) {
                    allPrintJobsHM.put(printJobID, sortedPrintJobDetails);
                }
            }

        }
        catch (Exception e) {
            Logger.logException(e, getKPLogFileName());
            Logger.logMessage("There was a problem trying to organize the expired and errored print jobs so they could be " +
                    "printed at the local receipt printer in PeripheralsDataManager.reorganizeExpiredErroredPrintJobs", getKPLogFileName(), Logger.LEVEL.ERROR);
        }

        return allPrintJobsHM;
    }


    /**
     * <p>Queries the database to get the ID of the revenue center the terminal that made the transaction is in.</p>
     *
     * @param terminalID The ID of the terminal that made the transaction.
     * @return The ID of the revenue center the terminal that made the transaction is in.
     */
    private int getRevenueCenterFromTerminal (int terminalID) {

        if (terminalID <= 0) {
            Logger.logMessage("The terminalID passed to PeripheralsDataManager.getRevenueCenterFromTerminal must be greater than 0!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return -1;
        }

        DynamicSQL sql =
                new DynamicSQL("data.kitchenPrinter.GetRevenueCenterIDFromTerminalID")
                        .addIDList(1, terminalID);
        Object queryRes = sql.getSingleField(dm);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString()))) {
            return Integer.parseInt(queryRes.toString());
        }

        return -1;
    }


    /**
     * <p>Gets all the print job details within all print jobs.</p>
     *
     * @param printJobs An {@link ArrayList} of {@link ArrayList} corresponding to all the print jobs to print locally.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print job details within all the print jobs.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "Convert2streamapi"})
    private ArrayList<HashMap> getAllPrintJobDetailsInAllPrintJobs (ArrayList<ArrayList> printJobs) {

        if (DataFunctions.isEmptyCollection(printJobs)) {
            Logger.logMessage("The print jobs passed to PeripheralsDataManager.getAllPrintJobDetailsInAllPrintJobs can't be null or empty!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return null;
        }

        // get all the print job details
        ArrayList<HashMap> allPrintJobDetails = new ArrayList<>();
        for (ArrayList printJob : printJobs) {
            if (!DataFunctions.isEmptyCollection(printJob)) {
                allPrintJobDetails.addAll(new ArrayList<>(DataFunctions.convertToCollectionOfHM(printJob)));
            }
        }

        return allPrintJobDetails;
    }


    /**
     * <p>Gets all the print job details within the print job.</p>
     *
     * @param printJob The {@link ArrayList} print job to get the print job details within.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the print job details within the print job.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private ArrayList<HashMap> getAllPrintJobDetails (ArrayList printJob) {

        if (DataFunctions.isEmptyCollection(printJob)) {
            Logger.logMessage("The print job passed to PeripheralsDataManager.getAllPrintJobDetails can't be null or empty!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return null;
        }

        return new ArrayList<>(DataFunctions.convertToCollectionOfHM(printJob));
    }


    /**
     * <p>Sorts the print job details by the printer they were intended to print on.</p>
     *
     * @param allPrintJobDetails An {@link ArrayList} of {@link HashMap} corresponding to the print job details to sort.
     * @return A {@link HashMap} whose key is the {@link Integer} ID of the printer and whose value is an {@link ArrayList} of {@link HashMap}
     * corresponding to the print job details that should have printed on the printer.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "Convert2streamapi"})
    private HashMap<Integer, ArrayList<HashMap>> sortPrintJobDetailsByPrinter (ArrayList<HashMap> allPrintJobDetails) {

        if (DataFunctions.isEmptyCollection(allPrintJobDetails)) {
            Logger.logMessage("The print job details passed to PeripheralsDataManager.sortPrintJobDetailsByPrinter can't be null or empty!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return null;
        }

        HashMap<Integer, ArrayList<HashMap>> sortedPrintJobDetails = new HashMap<>();
        ArrayList<HashMap> printJobDetailsForPrinter;
        for (HashMap printJobDetail : allPrintJobDetails) {
            if (!DataFunctions.isEmptyMap(printJobDetail)) {
                int printerID = HashMapDataFns.getIntVal(printJobDetail, "PRINTERID");
                if (sortedPrintJobDetails.containsKey(printerID)) {
                    printJobDetailsForPrinter = sortedPrintJobDetails.get(printerID);
                }
                else {
                    printJobDetailsForPrinter = new ArrayList<>();
                }
                printJobDetailsForPrinter.add(printJobDetail);
                sortedPrintJobDetails.put(printerID, printJobDetailsForPrinter);
            }
        }

        return sortedPrintJobDetails;
    }


    /**
     * <p>Checks that print job details for the given printer have actually expired or errored out.</p>
     *
     * @param paPrinterQueueID ID of the printer queue record the print job details are within.
     * @param printerID ID of the printer to check.
     * @param terminalID The ID of the terminal that made the transaction.
     * @param isKDS Whether or not the print job details are for KDS.
     * @param isOnlineOrder Whether or not the print jobs details are from an online order.
     * @return Whether or not print job details for the given printer have actually expired or errored out.
     */
    @SuppressWarnings({"SpellCheckingInspection", "OverlyComplexMethod", "UseOfObsoleteDateTimeApi"})
    private boolean arePrintJobsDetailsForPrinterErroredOrExpired (int paPrinterQueueID, int printerID, int terminalID, boolean isKDS, boolean isOnlineOrder) {

        if (paPrinterQueueID <= 0) {
            Logger.logMessage("The print queue ID passed to PeripheralsDataManager.arePrintJobsDetailsForPrinterErroredOrExpired must be greater than 0!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        if (printerID <= 0) {
            Logger.logMessage("The printer ID passed to PeripheralsDataManager.arePrintJobsDetailsForPrinterErroredOrExpired must be greater than 0!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        if (terminalID <= 0) {
            Logger.logMessage("The terminal ID passed to PeripheralsDataManager.arePrintJobsDetailsForPrinterErroredOrExpired must be greater than 0!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        // query the database to determine whether or ont the print jobs have actually expired or are errored
        DynamicSQL dynamicSQL = new DynamicSQL("data.kitchenPrinter.GetPrintStatusExpireDetailsForPrinterDetail")
                .addIDList(1, paPrinterQueueID)
                .addIDList(2, printerID);
        ArrayList<HashMap> queryRes = dynamicSQL.serialize(dm);
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
            HashMap queryResHM = queryRes.get(0);

            // check if the print status is ERROR
            if (HashMapDataFns.getIntVal(queryResHM, "PRINTSTATUSID") == PrintJobStatusType.ERROR.getTypeID()) {
                return true;
            }

            // check if the print job is expired
            Date queueTimeDate = HashMapDataFns.getDateVal(queryResHM, "QUEUETIME");
            Date estimatedOrderTimeDate = HashMapDataFns.getDateVal(queryResHM, "ESTIMATEDORDERTIME");
            LocalDateTime queueTime = (queueTimeDate != null ? new Timestamp(queueTimeDate.getTime()).toLocalDateTime() : null);
            LocalDateTime estimatedOrderTime = (estimatedOrderTimeDate != null ? new Timestamp(estimatedOrderTimeDate.getTime()).toLocalDateTime() : null);

            // if print job details aren't for KDS from and online order
            LocalDateTime expirationTime;
            if ((isKDS) && (isOnlineOrder) && (estimatedOrderTime != null)) {
                int prepTime = getTerminalPrepTime(terminalID);
                Logger.logMessage(String.format("Prep time for terminal ID %s is %s minutes!",
                        Objects.toString(terminalID, "N/A"),
                        Objects.toString((prepTime > 0 ? prepTime : "N/A"), "N/A")), getKPLogFileName(), Logger.LEVEL.TRACE);
                if (prepTime > 0) {
                    expirationTime = estimatedOrderTime.plusMinutes(prepTime);
                    if (LocalDateTime.now().isAfter(expirationTime)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    // use the print job expiration
                    expirationTime = LocalDateTime.now().minusMinutes(kitchenPrinterTerminal.getPrintJobExpirationInMinutes());
                }
            }
            else {
                expirationTime = LocalDateTime.now().minusMinutes(kitchenPrinterTerminal.getPrintJobExpirationInMinutes());
            }

            if ((estimatedOrderTime == null) && (queueTime != null) && (queueTime.isBefore(expirationTime))) {
                return true;
            }
            else if ((estimatedOrderTime != null) && (queueTime == null) && (estimatedOrderTime.isBefore(expirationTime))) {
                return true;
            }
            else if ((estimatedOrderTime != null) && (queueTime != null)) {
                LocalDateTime latestTime = (estimatedOrderTime.isAfter(queueTime) ? estimatedOrderTime : queueTime);
                if (latestTime.isBefore(expirationTime)) {
                    return true;
                }
            }

        }

        return false;
    }


    /**
     * <p>Queries the database to get the prep time in minutes for the terminal.</p>
     *
     * @param terminalID ID of the terminal to get the prep time for.
     * @return The prep time in minutes for the given terminal.
     */
    private int getTerminalPrepTime (int terminalID) {
        int prepTime = -1;

        if (terminalID <= 0) {
            Logger.logMessage("The terminal ID passed to PeripheralsDataManager.getTerminalPrepTime must be greater than 0!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return -1;
        }

        // query the database
        DynamicSQL sql =
                new DynamicSQL("data.kitchenPrinter.GetTerminalPrepTime")
                        .addIDList(1, terminalID);
        Object queryRes = sql.getSingleField(dm);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString())) && (NumberUtils.isNumber(queryRes.toString()))) {
            prepTime = Integer.parseInt(queryRes.toString());
        }

        return prepTime;
    }


    /**
     * <p>Adds header information and a cut command to failed/expired KDS print jobs being printed locally.</p>
     *
     * @param printJobDetails The KDS print job details to modify.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the modified print job details.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "unchecked", "OverlyComplexMethod"})
    private ArrayList<HashMap> modifyKDSPrintJobDetails (ArrayList<HashMap> printJobDetails) {

        if (DataFunctions.isEmptyCollection(printJobDetails)) {
            Logger.logMessage("The print job details passed to PeripheralsDataManager.modifyKDSPrintJobDetails can't be null or empty!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return null;
        }

        // add a redirected header
        addRedirectedHeader(printJobDetails);

        // add transaction header information
        addHeaderInfoToFailedKDSPrintJob(printJobDetails);

        // get whether or not the products within the failed print job are dining options
        HashMap<Integer, Boolean> diningOptionLookup = getFailedDiningOptionLookup(printJobDetails);

        // remove __PROD__ before product names and replace it with the quantity, replace __MOD__ before modifiers
        int currentProductID = 0;
        for (HashMap printJobDetail : printJobDetails) {
            boolean isProduct = false;
            boolean isModifier = false;
            if (!DataFunctions.isEmptyMap(printJobDetail)) {
                String encodedLineDetail = HashMapDataFns.getStringVal(printJobDetail, "LINEDETAIL");
                String decodedLineDetail = (StringFunctions.stringHasContent(encodedLineDetail) ? new String(org.apache.commons.codec.binary.Base64.decodeBase64(encodedLineDetail)) : "");
                if ((StringFunctions.stringHasContent(decodedLineDetail)) && (decodedLineDetail.startsWith(KitchenDisplaySystemJobInfo.PRODUCT_INDICATOR))) {
                    isProduct = true;
                }
                else if ((StringFunctions.stringHasContent(decodedLineDetail)) && (decodedLineDetail.startsWith(KitchenDisplaySystemJobInfo.MODIFIER_INDICATOR))) {
                    isModifier = true;
                }
                if (isProduct) {
                    // get the ID of the product and set it as the current product ID
                    currentProductID = HashMapDataFns.getIntVal(printJobDetail, "PAPLUID");
                    // update the line detail
                    String[] tokens = decodedLineDetail.split("__", -1);
                    String productName = (((!DataFunctions.isEmptyGenericArr(tokens)) && (tokens.length == 4)) ? tokens[3] : "N/A");
                    double quantity = HashMapDataFns.getDoubleVal(printJobDetail, "QUANTITY");
                    productName = String.format("%2dX %s", Math.round(quantity), productName);
                    // encode the line detail again
                    encodedLineDetail = org.apache.commons.codec.binary.Base64.encodeBase64String(productName.getBytes());
                    printJobDetail.put("LINEDETAIL", encodedLineDetail);
                    // make the parent product negative to indicate that this product doesn't have a parent product
                    printJobDetail.put("PARENTPAPLUID", -1);
                    // set whether or not this product is a dining option
                    if ((!DataFunctions.isEmptyMap(diningOptionLookup)) && (diningOptionLookup.containsKey(currentProductID))) {
                        printJobDetail.put("ISDININGOPTION", diningOptionLookup.get(currentProductID));
                    }
                }
                else if (isModifier) {
                    // update the line detail
                    String[] tokens = decodedLineDetail.split("__", -1);
                    String modifierName = (((!DataFunctions.isEmptyGenericArr(tokens)) && (tokens.length == 4)) ? tokens[3] : "N/A");
                    modifierName = String.format("\t %s", modifierName);
                    // encode the line detail again
                    encodedLineDetail = org.apache.commons.codec.binary.Base64.encodeBase64String(modifierName.getBytes());
                    printJobDetail.put("LINEDETAIL", encodedLineDetail);
                    // keep a reference to this modifier's parent product
                    printJobDetail.put("PARENTPAPLUID", currentProductID);
                    // set the dining option as false since a modifier can't be a dining option
                    printJobDetail.put("ISDININGOPTION", false);
                }
            }
        }

        // find the invalid redirected header
        int startRemoveIndex = 0;
        int endRemoveIndex = 0;
        for (int i = 0; i < printJobDetails.size(); i++) {
            if ((i >= 3)
                    && (new String(org.apache.commons.codec.binary.Base64.decodeBase64(HashMapDataFns.getStringVal(printJobDetails.get(i), "LINEDETAIL"))).contains("******************"))
                    && ((i + 1 < printJobDetails.size()) && (new String(org.apache.commons.codec.binary.Base64.decodeBase64(HashMapDataFns.getStringVal(printJobDetails.get(i + 1), "LINEDETAIL"))).contains("Error or Timeout")))
                    && ((i + 2 < printJobDetails.size()) && (new String(org.apache.commons.codec.binary.Base64.decodeBase64(HashMapDataFns.getStringVal(printJobDetails.get(i + 2), "LINEDETAIL"))).contains("Redirected due to")))
                    && ((i + 3 < printJobDetails.size()) && (new String(org.apache.commons.codec.binary.Base64.decodeBase64(HashMapDataFns.getStringVal(printJobDetails.get(i + 3), "LINEDETAIL"))).contains("******************")))) {
                startRemoveIndex = i;
                endRemoveIndex = i + 4;
            }
        }
        // remove the invalid redirected header from the print job details
        if ((startRemoveIndex > 0) && (endRemoveIndex > 0)) {
            printJobDetails.subList(startRemoveIndex, endRemoveIndex).clear();
        }

        // add cut command
        addPrintJobDetail(printJobDetails, printJobDetails.get(0), org.apache.commons.codec.binary.Base64.encodeBase64String((KitchenPrinterReceiptFormatter.JPOS_CUT).getBytes()), printJobDetails.size());

        return printJobDetails;
    }


    /**
     * <p>Adds a redirected header to the print job details.</p>
     *
     * @param printJobDetails An {@link ArrayList} of {@link HashMap} corresponding to the print job details to add the redirected header to.
     */
    public void addRedirectedHeader (ArrayList<HashMap> printJobDetails) {

        if (DataFunctions.isEmptyCollection(printJobDetails)) {
            Logger.logMessage("The print job details passed to PeripheralsDataManager.addRedirectedHeader can't be null or empty!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return;
        }

        final String JPOS_CENTER = KitchenPrinterReceiptFormatter.JPOS_ALIGN_CENTER;
        final String JPOS_LARGE_FONT = KitchenPrinterReceiptFormatter.JPOS_LARGE_FNT;
        addPrintJobDetail(printJobDetails, printJobDetails.get(0), org.apache.commons.codec.binary.Base64.encodeBase64String((JPOS_CENTER + JPOS_LARGE_FONT + "******************").getBytes()), 0);
        addPrintJobDetail(printJobDetails, printJobDetails.get(0), org.apache.commons.codec.binary.Base64.encodeBase64String((JPOS_CENTER + JPOS_LARGE_FONT + "Redirected due to").getBytes()), 0);
        addPrintJobDetail(printJobDetails, printJobDetails.get(0), org.apache.commons.codec.binary.Base64.encodeBase64String((JPOS_CENTER + JPOS_LARGE_FONT + "Error or Timeout").getBytes()), 0);
        addPrintJobDetail(printJobDetails, printJobDetails.get(0), org.apache.commons.codec.binary.Base64.encodeBase64String((JPOS_CENTER + JPOS_LARGE_FONT + "******************").getBytes()), 0);

    }


    /**
     * <p>Adds a new print job detail to the print job details at the specified index.</p>
     *
     * @param printJobDetails An {@link ArrayList} of {@link HashMap} corresponding to the print job details to modify.
     * @param printJobDetail A {@link HashMap} print job detail that can be used to clone.
     * @param lineDetail The {@link String} text to print on the line.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "unchecked"})
    private void addPrintJobDetail(ArrayList<HashMap> printJobDetails, HashMap printJobDetail, String lineDetail, int index) {

        if (DataFunctions.isEmptyCollection(printJobDetails)) {
            Logger.logMessage("The print job details passed to PeripheralsDataManager.addPrintJobDetail can't be null or empty!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return;
        }

        if (DataFunctions.isEmptyMap(printJobDetail)) {
            Logger.logMessage("The print job detail to clone passed to PeripheralsDataManager.addPrintJobDetail can't be null or empty!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return;
        }

        if (lineDetail == null) {
            Logger.logMessage("The text for the print job detail passed to PeripheralsDataManager.addPrintJobDetail can't be null!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return;
        }

        HashMap newPrintJobDetail = ((HashMap) printJobDetail.clone());
        newPrintJobDetail.put("LINEDETAIL", lineDetail);
        printJobDetails.add(index, newPrintJobDetail);

    }


    /**
     * <p>Adds the header details to the failed KDS print job.</p>
     *
     * @param printJobDetails An {@link ArrayList} of {@link HashMap} corresponding to the print job details of the failed KDS print job.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "OverlyComplexMethod"})
    private void addHeaderInfoToFailedKDSPrintJob (ArrayList<HashMap> printJobDetails) {

        if (DataFunctions.isEmptyCollection(printJobDetails)) {
            Logger.logMessage("The print job details passed to PeripheralsDataManager.addHeaderInfoToFailedKDSPrintJob can't be null or empty!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return;
        }

        if (!DataFunctions.isEmptyMap(printJobDetails.get(0))) {
            // get the date and time of the transaction
            String transactionDate = "";
            String transactionDateStr = "";
            String transactionTimeStr = "";
            try {
                transactionDate = HashMapDataFns.getStringVal(printJobDetails.get(0), "TRANSACTIONDATE");
                // extract date information from the transaction date, example transaction date => 2020-06-15 08:08:58.0
                int year = Integer.parseInt(transactionDate.substring(0, 4));
                Month month = Month.of(Integer.parseInt(transactionDate.substring(5, 7)));
                int dayOfMonth = Integer.parseInt(transactionDate.substring(8, 10));
                int hour = Integer.parseInt(transactionDate.substring(11, 13));
                int minute = Integer.parseInt(transactionDate.substring(14, 16));
                int second = Integer.parseInt(transactionDate.substring(17, 19));
                // use the extracted transaction dat information to build a LocalDateTime instance
                LocalDateTime txnLocalDateTime = LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);
                DateTimeFormatter txnDateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
                DateTimeFormatter txnTimeFormatter = DateTimeFormatter.ofPattern("hh:mm:ss a");
                transactionDateStr = txnLocalDateTime.format(txnDateFormatter); // example ==> 06/15/2020
                transactionTimeStr = txnLocalDateTime.format(txnTimeFormatter); // example ==> 08:08:58 AM
            }
            catch (Exception e) {
                Logger.logException(e, getKPLogFileName());
                Logger.logMessage(String.format("There was a problem trying to determine the date and time that the transaction took place " +
                                "given the transaction date String of %s in PeripheralsDataManager.addHeaderInfoToFailedKDSPrintJob",
                        Objects.toString(transactionDate, "N/A")), getKPLogFileName(), Logger.LEVEL.ERROR);
            }
            int terminalID = HashMapDataFns.getIntVal(printJobDetails.get(0), "TERMINALID");
            String transactionID = HashMapDataFns.getStringVal(printJobDetails.get(0), "PATRANSACTIONID");
            int printerID = HashMapDataFns.getIntVal(printJobDetails.get(0), "PRINTERID");
            String printerName = "";
            if (printerID > 0) {
                // query the database to get the name of the printer
                DynamicSQL dynamicSQL = new DynamicSQL("data.kitchenPrinter.GetPrinterNameFromID")
                        .addIDList(1, printerID);
                Object queryRes = dynamicSQL.getSingleField(dm);
                if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString()))) {
                    printerName = queryRes.toString();
                }
            }
            String orderNumber = HashMapDataFns.getStringVal(printJobDetails.get(0), "ORDERNUM");
            String personName = HashMapDataFns.getStringVal(printJobDetails.get(0), "PERSONNAME");
            String comment = HashMapDataFns.getStringVal(printJobDetails.get(0), "TRANSCOMMENT");

            // add the transaction date and time
            String firstHeaderLine = twoColumn(transactionDateStr, transactionTimeStr);
            if (StringFunctions.stringHasContent(firstHeaderLine)) {
                // encode the line
                firstHeaderLine = org.apache.commons.codec.binary.Base64.encodeBase64String(firstHeaderLine.getBytes());
            }
            addPrintJobDetail(printJobDetails, printJobDetails.get(0), firstHeaderLine, 4);

            // add the transaction and terminal ID
            String secondHeaderLine = twoColumn("SALE: " + transactionID, "TID: " + terminalID);
            if (StringFunctions.stringHasContent(secondHeaderLine)) {
                // encode the line
                secondHeaderLine = org.apache.commons.codec.binary.Base64.encodeBase64String(secondHeaderLine.getBytes());
            }
            addPrintJobDetail(printJobDetails, printJobDetails.get(0), secondHeaderLine, 5);

            // add an empty line
            addPrintJobDetail(printJobDetails, printJobDetails.get(0), org.apache.commons.codec.binary.Base64.encodeBase64String(KitchenPrinterReceiptFormatter.JPOS_ALIGN_CENTER.getBytes()), 6);

            // add the printer name
            if (StringFunctions.stringHasContent(printerName)) {
                // encode the line
                printerName = org.apache.commons.codec.binary.Base64.encodeBase64String((KitchenPrinterReceiptFormatter.JPOS_ALIGN_CENTER + printerName).getBytes());
            }
            addPrintJobDetail(printJobDetails, printJobDetails.get(0), printerName, 7);

            // add the order number
            if (StringFunctions.stringHasContent(orderNumber)) {
                // encode the line
                orderNumber = org.apache.commons.codec.binary.Base64.encodeBase64String((KitchenPrinterReceiptFormatter.JPOS_ALIGN_CENTER + KitchenPrinterReceiptFormatter.JPOS_LARGE_FNT + orderNumber).getBytes());
            }
            addPrintJobDetail(printJobDetails, printJobDetails.get(0), orderNumber, 8);

            // add an empty line
            addPrintJobDetail(printJobDetails, printJobDetails.get(0), org.apache.commons.codec.binary.Base64.encodeBase64String(KitchenPrinterReceiptFormatter.JPOS_ALIGN_CENTER.getBytes()), 9);

            // add the customer
            if (StringFunctions.stringHasContent(personName)) {
                // encode the line
                personName = org.apache.commons.codec.binary.Base64.encodeBase64String(("Customer: " + personName).getBytes());
            }
            addPrintJobDetail(printJobDetails, printJobDetails.get(0), personName, 10);

            // add the comment
            if (StringFunctions.stringHasContent(comment)) {
                // encode the line
                comment = org.apache.commons.codec.binary.Base64.encodeBase64String(("Comment: " + comment).getBytes());
            }
            addPrintJobDetail(printJobDetails, printJobDetails.get(0), comment, 11);

            // add an empty line
            addPrintJobDetail(printJobDetails, printJobDetails.get(0), org.apache.commons.codec.binary.Base64.encodeBase64String(KitchenPrinterReceiptFormatter.JPOS_ALIGN_CENTER.getBytes()), 12);

            // add an empty line
            addPrintJobDetail(printJobDetails, printJobDetails.get(0), org.apache.commons.codec.binary.Base64.encodeBase64String(KitchenPrinterReceiptFormatter.JPOS_ALIGN_CENTER.getBytes()), 13);
        }

    }


    /**
     * <p>Creates a single receipt line where the text is left and right justified.</p>
     *
     * @param leftColText The {@link String} to left justify.
     * @param rightColText The {@link String} to right justify.
     * @return The single receipt line {@link String} with left and right justified columns.
     */
    public String twoColumn (String leftColText, String rightColText) {

        if (!StringFunctions.stringHasContent(leftColText)) {
            Logger.logMessage("The text that should appear within the left column on a receipt line can't be null or empty " +
                    "in PeripheralsDataManager.twoColumn!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return null;
        }

        if (!StringFunctions.stringHasContent(rightColText)) {
            Logger.logMessage("The text that should appear within the right column on a receipt line can't be null or empty " +
                    "in PeripheralsDataManager.twoColumn!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return null;
        }

        final int MAX_SMALL_CHARS = 36;

        // determine the maximum number of characters that can fit in each column of the receipt line
        final int LEFT_WIDTH = ((MAX_SMALL_CHARS * 2) / 3);
        final int RIGHT_WIDTH = (MAX_SMALL_CHARS - LEFT_WIDTH);

        // truncate the text in the left column if necessary
        if (leftColText.length() > LEFT_WIDTH) {
            leftColText = leftColText.substring(0, LEFT_WIDTH);
        }
        if (rightColText.length() > RIGHT_WIDTH) {
            rightColText = rightColText.substring(0, RIGHT_WIDTH - 1);
        }

        return String.format("%s" + JPOS_COMMAND.ESC + JPOS_COMMAND.ALIGN_RIGHT + "%s", leftColText, rightColText);
    }


    /**
     * <p>Changes the positions within the print job details of the "Redirected due to" and "Error or Timeout" lines.</p>
     *
     * @param printJobDetails An {@link ArrayList} of {@link HashMap} corresponding to the print job details to modify.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the modified print job details.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    private ArrayList<HashMap> swapRedirectedHeaderDetails (ArrayList<HashMap> printJobDetails) {

        if (DataFunctions.isEmptyCollection(printJobDetails)) {
            Logger.logMessage("The print job details passed to PeripheralsDataManager.swapRedirectedHeaderDetails can't be null or empty!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return null;
        }

        // find the indices of interest
        int redirectedDueToIndex = -1;
        int errorOrTimeoutIndex = -1;
        int index = -1;
        boolean bothIndicesFound = false;
        for (HashMap printJobDetail : printJobDetails) {
            index++;
            if (!DataFunctions.isEmptyMap(printJobDetail)) {
                String lineDetail = decode(HashMapDataFns.getStringVal(printJobDetail, "LINEDETAIL"));
                if (lineDetail.toLowerCase().contains("redirected due to")) {
                    redirectedDueToIndex = index;
                }
                else if (lineDetail.toLowerCase().contains("error or timeout")) {
                    errorOrTimeoutIndex = index;
                }
                if ((redirectedDueToIndex >= 0) && (errorOrTimeoutIndex >= 0)) {
                    bothIndicesFound = true;
                    break; // both indices have been found no need to keep processing
                }
            }
        }

        // if we found both indices perform the swap
        if (bothIndicesFound) {
            Collections.swap(printJobDetails, redirectedDueToIndex, errorOrTimeoutIndex);
            return printJobDetails;
        }
        else {
            return printJobDetails;
        }
    }


    /**
     * <p>Iterates through the products within the failed print job and checks if each product is a dining option or not.</p>
     *
     * @param printJobDetails An {@link ArrayList} of {@link HashMap} corresponding to the details of the failed print job.
     * @return A {@link HashMap} whose {@link Integer} key is the ID of the product and whose {@link Boolean} value is whether or not the product is a dining option.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "Convert2streamapi"})
    private HashMap<Integer, Boolean> getFailedDiningOptionLookup (ArrayList<HashMap> printJobDetails) {

        // make sure we have print job details
        if (DataFunctions.isEmptyCollection(printJobDetails)) {
            Logger.logMessage("No print job details have been passed to PeripheralsDataManager.getFailedDiningOptionLookup!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return null;
        }

        // get the IDs of products from the failed print job details
        ArrayList<Integer> productIDs = new ArrayList<>();
        for (HashMap printJobDetail : printJobDetails) {
            // check if it's a product and not a modifier
            if (new String(org.apache.commons.codec.binary.Base64.decodeBase64(HashMapDataFns.getStringVal(printJobDetail, "LINEDETAIL"))).startsWith(KitchenDisplaySystemJobInfo.PRODUCT_INDICATOR)) {
                productIDs.add(HashMapDataFns.getIntVal(printJobDetail, "PAPLUID"));
            }
        }

        if (DataFunctions.isEmptyCollection(productIDs)) {
            Logger.logMessage("No products were found within the print job details in PeripheralsDataManager.getFailedDiningOptionLookup!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return null;
        }

        // query the database and populate the lookup
        HashMap<Integer, Boolean> diningOptionLookup = new HashMap<>();
        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetAreProductsDiningOptions").addIDList(1, productIDs);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dm));
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                int paPluID = HashMapDataFns.getIntVal(hm, "PAPLUID");
                boolean isDiningOption = HashMapDataFns.getBooleanVal(hm, "ISDININGOPTION");
                diningOptionLookup.put(paPluID, isDiningOption);
            }
        }

        return diningOptionLookup;
    }


    /**
     * <p>Iterates through the print job details and adds them to a receipt that will print locally.</p>
     *
     * @param renderer The {@link IRenderer} used to render the receipt to print locally.
     * @param printJobDetails An {@link ArrayList} of {@link HashMap} corresponding to the print job details that should be added to the receipt to print locally.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "Convert2streamapi", "ThrowableResultOfMethodCallIgnored"})
    private boolean printPrintJobDetailsLocally (IRenderer renderer, ArrayList<HashMap> printJobDetails) {
        boolean localPrintSuccess = true;

        if (renderer == null) {
            Logger.logMessage("The renderer passed to PeripheralsDataManager.printPrintJobDetailsLocally can't be null!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        if (DataFunctions.isEmptyCollection(printJobDetails)) {
            Logger.logMessage("The print job details passed to PeripheralsDataManager.printPrintJobDetailsLocally can't be null or empty!", getKPLogFileName(), Logger.LEVEL.ERROR);
            return false;
        }

        // TODO DELETE THIS TEST
//        KPEmailAlert.sendEmail(printJobDetails, dm, getKPLogFileName());
        //sendEmailAlert(printJobDetails, null);

        for (HashMap printJobDetail : printJobDetails) {
            if (!DataFunctions.isEmptyMap(printJobDetail)) {
                // get the encoded line detail
                String encodedPrintLineDetail = HashMapDataFns.getStringVal(printJobDetail, "LINEDETAIL");

                // decode the line detail
                String lineDetail = "";
                if (StringFunctions.stringHasContent(encodedPrintLineDetail)) {
                    lineDetail = decode(encodedPrintLineDetail);
                }

                // print the line detail
                if (StringFunctions.stringHasContent(lineDetail)) {
                    if (lineDetail.contains(KitchenPrinterReceiptFormatter.JPOS_CUT)) {
                        // cut the receipt paper
                        renderer.cutPaper();
                    }
                    else if (lineDetail.contains(KitchenPrinterReceiptFormatter.BARCODE)) {
                        // get the transaction ID from the barcode, and print the barcode
                        String paTransactionID = lineDetail.replace(KitchenPrinterReceiptFormatter.BARCODE, "");
                        renderer.addBlankLine();
                        renderer.addBlankLine();
                        renderer.addBarcode("QCPOS"+paTransactionID);
                    }
                    else {
                        Logger.logMessage(String.format("Adding the line %s to the receipt being printed locally!",
                                Objects.toString(lineDetail, "N/A")), getKPLogFileName(), Logger.LEVEL.TRACE);
                        renderer.addLine(lineDetail);
                        if (renderer.getLastException() != null) {
                            localPrintSuccess = false;
                        }
                    }
                }
            }
        }

        return localPrintSuccess;
    }


    /**
     * <p>Updates the print status of print queue details within the database.</p>
     *
     * @param printQueueDetails An {@link ArrayList} of {@link HashMap} corresponding to the print queue details to update the print status for.
     * @param printStatusID The ID of the print status to update the print queue details to.
     */
    @SuppressWarnings({"Convert2streamapi", "TypeMayBeWeakened"})
    private void updatePrintQueueDetailsPrintStatus (ArrayList<HashMap> printQueueDetails, int printStatusID) {

        try {
            if (DataFunctions.isEmptyCollection(printQueueDetails)) {
                Logger.logMessage("The print queue details passed to PeripheralsDataManager.updatePrintQueueDetailsPrintStatus can't be null or empty!", getKPLogFileName(), Logger.LEVEL.ERROR);
                return;
            }

            if (printStatusID <= 0) {
                Logger.logMessage("The print status ID passed to PeripheralsDataManager.updatePrintQueueDetailsPrintStatus must be greater than 0!", getKPLogFileName(), Logger.LEVEL.ERROR);
                return;
            }

            // get the print queue detail IDs
            Set<Integer> printQueueDetailIDs = DataFunctions.getUniqueValsAtKeyFromALOfHM(printQueueDetails, "PAPRINTERQUEUEDETAILID");

            // update the print status in the database
            if (!DataFunctions.isEmptyCollection(printQueueDetailIDs)) {
                DynamicSQL dynamicSQL = new DynamicSQL("data.kitchenPrinter.UpdatePrintQueueDetailsPrintStatusNoPrintedDTM")
                        .addLargeIDList(1, printQueueDetailIDs)
                        .addIDList(2, printStatusID);
                if (dynamicSQL.runUpdate(dm) <= 0) {
                    Logger.logMessage(String.format("Failed to update the print queue details with IDs of %s to a print status " +
                                    "of %s in PeripheralsDataManager.updatePrintQueueDetailsPrintStatus",
                            Objects.toString(StringFunctions.buildStringFromList(new ArrayList<>(printQueueDetailIDs), ","), "N/A"),
                            Objects.toString(printStatusID, "N/A")), getKPLogFileName(), Logger.LEVEL.ERROR);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, getKPLogFileName());
            Logger.logMessage("There was a problem trying to update the print status of the print queue details in the database " +
                    "in PeripheralsDataManager.updatePrintQueueDetailsPrintStatus", getKPLogFileName(), Logger.LEVEL.ERROR);
        }

    }


    /**
     * <p>Updates the print status of print queue record within the database.</p>
     *
     * @param paPrinterQueueID The ID of print queue record to update the print status for.
     * @param printStatusID The ID of the print queue record to update the print queue details to.
     */
    private void updatePrintQueueHeaderPrintStatus (int paPrinterQueueID, int printStatusID) {

        try {
            if (paPrinterQueueID <= 0) {
                Logger.logMessage("The printer queue ID passed to PeripheralsDataManager.updatePrintQueueHeaderPrintStatus must be greater than 0!", getKPLogFileName(), Logger.LEVEL.ERROR);
                return;
            }

            if (printStatusID <= 0) {
                Logger.logMessage("The print status ID passed to PeripheralsDataManager.updatePrintQueueHeaderPrintStatus must be greater than 0!", getKPLogFileName(), Logger.LEVEL.ERROR);
                return;
            }

            DynamicSQL dynamicSQL = new DynamicSQL("data.kitchenPrinter.UpdatePrintQueueHeaderPrintStatus")
                    .addIDList(1, paPrinterQueueID)
                    .addIDList(2, printStatusID);
            if (dynamicSQL.runUpdate(dm) <= 0) {
                Logger.logMessage(String.format("Failed to update the print queue record with an ID of %s to a print status " +
                                "of %s in PeripheralsDataManager.updatePrintQueueHeaderPrintStatus",
                        Objects.toString(paPrinterQueueID, "N/A"),
                        Objects.toString(printStatusID, "N/A")), getKPLogFileName(), Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, getKPLogFileName());
            Logger.logMessage("There was a problem trying to update the print status of the print queue record in the database " +
                    "in PeripheralsDataManager.updatePrintQueueHeaderPrintStatus", getKPLogFileName(), Logger.LEVEL.ERROR);
        }

    }


    /**
     * Expired or errored KDS print jobs need to have an order redirected due to error or timeout header added to them.
     *
     * @param printJobs {@link ArrayList<ArrayList>} The print jobs to fix.
     * @return {@link ArrayList<ArrayList>} The given expired and errored print jobs with error headers added to
     *         KDS print jobs that had errored or expired.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "unchecked", "Convert2streamapi", "SpellCheckingInspection"})
    private ArrayList<ArrayList> fixExpireErrorPrintJobsForKDS (ArrayList<ArrayList> printJobs) {
        ArrayList<ArrayList> fixedPrintJobs = new ArrayList<>();

        try {
            if (!DataFunctions.isEmptyCollection(printJobs)) {
                // get all the details from each print job, we will sort them by print job later
                ArrayList<HashMap> allPrintLineDetails = new ArrayList<>();
                for (ArrayList<HashMap> printJob : printJobs) {
                    for (HashMap printLineDetail : printJob) {
                        if (!DataFunctions.isEmptyMap(printLineDetail)) {
                            allPrintLineDetails.add(printLineDetail);
                        }
                    }
                }

                // sort the line details back into print jobs
                if (!DataFunctions.isEmptyCollection(allPrintLineDetails)) {
                    int prevPAPrinterQueueID = 0;
                    int prevPrinterID = 0;
                    ArrayList prevPrintJob = null;

                    for (HashMap printLineDetail : allPrintLineDetails) {
                        int currPAPrinterQueueID = HashMapDataFns.getIntVal(printLineDetail, "PAPRINTERQUEUEID");
                        int currPrinterID = HashMapDataFns.getIntVal(printLineDetail, "PRINTERID");
                        ArrayList currPrintJob;
                        if ((prevPrintJob == null) || (prevPAPrinterQueueID != currPAPrinterQueueID) || (prevPrinterID != currPrinterID)) {
                            currPrintJob = new ArrayList();
                            prevPrintJob = currPrintJob;
                            fixedPrintJobs.add(currPrintJob);
                            prevPAPrinterQueueID = currPAPrinterQueueID;
                            prevPrinterID = currPrinterID;
                        }
                        else {
                            currPrintJob = prevPrintJob;
                        }
                        currPrintJob.add(printLineDetail);
                    }
                }

                // add the redirected due to error or timeout header for expired or errored KDS print jobs
                // encrypted "******************"
                final String ENCRYPTED_ASTERISKS = "G3xjQRt8NEMqKioqKioqKioqKioqKioqKio=";
                // encrypted "Error or Timeout"
                final String ENCRYPTED_ERROR_OR_TIMEOUT = "G3xjQRt8NENFcnJvciBvciBUaW1lb3V0";
                // encrypted "Redirected due to"
                final String ENCRYPTED_REDIRECTED_DUE_TO = "G3xjQRt8NENSZWRpcmVjdGVkIGR1ZSB0bw==";
                String[] redirectedHeader = new String[]{ENCRYPTED_ASTERISKS, ENCRYPTED_REDIRECTED_DUE_TO, ENCRYPTED_ERROR_OR_TIMEOUT, ENCRYPTED_ASTERISKS};
                if (!DataFunctions.isEmptyCollection(fixedPrintJobs)) {
                    for (ArrayList<HashMap> fixedPrintJob : fixedPrintJobs) {
                        if ((!DataFunctions.isEmptyCollection(fixedPrintJob)) && (!DataFunctions.isEmptyMap(fixedPrintJob.get(0))) && (HashMapDataFns.getBooleanVal(fixedPrintJob.get(0), "ISKDS"))) {
                            for (String line : redirectedHeader) {
                                HashMap redirectedHeaderLineDetail = ((HashMap) fixedPrintJob.get(0).clone());
                                redirectedHeaderLineDetail.put("LINEDETAIL", line);
                                fixedPrintJob.add(0, redirectedHeaderLineDetail);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logMessage("There was a problem trying to fix the expired or errored print jobs for KDS in " +
                    "PeripheralsDataManager.fixExpireErrorPrintJobsForKDS", getKPLogFileName(), Logger.LEVEL.ERROR);
        }

        return fixedPrintJobs;
    }

    public static String getQueueDatetimeFormat()
    {
        return "yyyy-MM-dd HH:mm:ss";
    }


    public static Calendar parseQueueTime(String s) throws ParseException
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(getQueueDatetimeFormat());
        cal.setTime(sdf.parse(s));
        return cal;
    }


    private void updateJobsStatusForPrintQueue(ArrayList printData, PrintJobStatusType newStatusType) {
        int newStatus = newStatusType.getTypeID();
        String statusIDs = "";
        for (Object line : printData)
        {
            HashMap hm = (HashMap) line;
            statusIDs += hm.get("PAPRINTERQUEUEID").toString();
            statusIDs += ",";
        }

        if (statusIDs.length() > 0)
        {
            statusIDs = statusIDs.substring(0, statusIDs.length() - 1);

            //Update the headers by header ID(s)
            Logger.logMessage("updateJobsStatus: Updating header status to " + newStatus + " for printerQueueIds in " + statusIDs, getKPLogFileName(), Logger.LEVEL.DEBUG);
            if (statusIDs.length() > 0 && dm.serializeUpdate("data.newKitchenPrinter.SetPAPrinterQueueStatus", new Object[]{newStatus, statusIDs}, null, getKPLogFileName(), 0, false) <= 0)
            {
                Logger.logMessage("updateJobsStatus failed to update header status to " + newStatus + " for printerQueueIds in " + statusIDs, getKPLogFileName(), Logger.LEVEL.ERROR);
            }
            //Update the details by header ID(s)
            Logger.logMessage("updateJobsStatus: Updating detail status to " + newStatus + " for printerQueueIds in " + statusIDs, getKPLogFileName(), Logger.LEVEL.DEBUG);
            if (statusIDs.length() > 0 && dm.serializeUpdate("data.newKitchenPrinter.SetPAPrinterQueueDetailStatusByPrinterQueueID", new Object[]{newStatus, statusIDs}, null, getKPLogFileName(), 0, false) <= 0)
            {
                Logger.logMessage("updateJobsStatus failed to update detail status to " + newStatus + " for printerQueueIds in " + statusIDs, getKPLogFileName(), Logger.LEVEL.ERROR);
            }
        }
    }


    //private void updateJobsStatusForPrintQueueDetails(ArrayList printData, int newStatus)
    private void updateJobsStatusForPrintQueueDetails(ArrayList<ArrayList> printJobs, PrintJobStatusType newStatusType)
    {
        int newStatus = newStatusType.getTypeID();
        for (ArrayList<HashMap> printJob : printJobs)
        {
            // Get PAPrinterQueueID
            Integer paPrinterQueueID = HashMapDataFns.getIntVal(printJob.get(0), "PAPRINTERQUEUEID");

            // Get detail IDs
            String detailIDs = "";
            for (HashMap jobDetail : printJob)
            {
                detailIDs += HashMapDataFns.getStringVal(jobDetail, "PAPRINTERQUEUEDETAILID");
                detailIDs += ",";
            }
            detailIDs = detailIDs.substring(0, detailIDs.length() - 1);     // get rid of the last comma

            if (detailIDs.length() > 0)
            {
                // Update header record
                if (dm.serializeUpdate("data.newKitchenPrinter.SetPAPrinterQueueStatusByPrinterQueueDetailID", new Object[]{newStatus, detailIDs}, null, getKPLogFileName(), 0, false) <= 0)
                {
                    Logger.logMessage("updateJobsStatus failed to update header status to " + newStatus + " for PAPrinterQueueID " + paPrinterQueueID, getKPLogFileName(), Logger.LEVEL.ERROR);
                }

                //Update the details by detail ID(s)
                Logger.logMessage("updateJobsStatus: Updating detail status to " + newStatus + " for printerQueueDetailIds in " + detailIDs, getKPLogFileName(), Logger.LEVEL.DEBUG);
                if (detailIDs.length() > 0 && dm.serializeUpdate("data.newKitchenPrinter.SetPAPrinterQueueDetailStatus", new Object[]{newStatus, detailIDs}, null, getKPLogFileName(), 0, false) <= 0)
                {
                    Logger.logMessage("updateJobsStatus failed to update detail status to " + newStatus + " for printerQueueDetailIds in " + detailIDs, getKPLogFileName(), Logger.LEVEL.ERROR);
                }
            }
        }
    }


    private List<String> getAlertEmailAddress(Integer revCenterId)
    {
        List<String> emailList = new ArrayList();

        //Get the list of contact emails we have associated with this revenue center
        ArrayList resultList = dm.serializeSqlWithColNames("data.newKitchenPrinter.GetKPContactEmailByRevenueCenterID", new Object[]{revCenterId}, null, getKPLogFileName());
        if(resultList.size() > 0)
        {
            for (Object result : resultList)
            {
                HashMap hm = (HashMap) result;
                String emailString = hm.get("KPCONTACTEMAIL").toString();
                //emailString is potentially a comma separated list. Split out the individual emails and add them to the emailList
                emailList = Arrays.asList(emailString.split("\\s*,\\s*"));
            }
        }

        return emailList;
    }

    private List<String> getAlertEmailAddress()
    {
        List<String> emailList = new ArrayList();

        //Get the list of contact emails we have associated with this revenue center
        ArrayList resultList = dm.serializeSqlWithColNames("data.common.getSystemMonitoringEmail", new Object[]{}, null, getKPLogFileName());
        if(resultList.size() > 0)
        {
            for (Object result : resultList)
            {
                HashMap hm = (HashMap) result;
                String emailString = hm.get("SYSTEMMONITOREMAILLIST").toString();
                //emailString is potentially a comma separated list. Split out the individual emails and add them to the emailList
                emailList = Arrays.asList(emailString.split("\\s*,\\s*"));
            }
        }

        return emailList;
    }

    /* email should contain detail about orders we could not print..
         could also contain warning about queued data too  */
    private void sendEmailAlert(ArrayList expiredData, ArrayList queuedData)
    {
        if(expiredData.size() > 0)
        {
            int currRevCenterID = 0;
            String emailMsg = "";

            //Loop through expired print data job by job
            for (Object printJob : expiredData)
            {
                HashMap printJobHM = (HashMap)printJob;
                if(printJobHM.containsKey("REVENUECENTERID"))
                {
                    int revCenterID = Integer.parseInt(printJobHM.get("REVENUECENTERID").toString());
                    if(revCenterID != currRevCenterID)
                    {
                        //This is the first expired print job sent for this revenue center
                        //First off, see if there is an existing email we need to send
                        if(emailMsg != "")
                        {
                            emailMsg += "</table>";
                            //Send email
                            sendEmailToRevenueCenter(emailMsg,"WARNING: failed to print online order(s)", revCenterID);
                            //Blank out email string
                            emailMsg = "";
                        }

                        //Now start the new email
                        emailMsg = "<h1>Warning: failed to print online order(s)</h1><hr><br>\n";
                        emailMsg += "<table style='border:1px solid black'>\n<tr><td>Name</td><td>Order</td><td>Phone</td><td>Date</td><td>Comment</td><td>Pickup/Delivery Note</td></tr>\n";
                        emailMsg += "<tr><td style='border:1px solid black'>";
                        emailMsg += printJobHM.get("PERSONNAME").toString();
                        emailMsg += "</td><td style='border:1px solid black'>";
                        emailMsg += printJobHM.get("PATRANSACTIONID").toString();
                        emailMsg += "</td><td style='border:1px solid black'>";
                        emailMsg += printJobHM.get("PHONE").toString();
                        emailMsg += "</td><td style='border:1px solid black'>";
                        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                        emailMsg += df.format((Date)printJobHM.get("TRANSACTIONDATE")).toString();
                        emailMsg += "</td><td style='border:1px solid black'>";
                        emailMsg += printJobHM.get("TRANSCOMMENT").toString();
                        emailMsg += "</td><td style='border:1px solid black'>";
                        emailMsg += printJobHM.get("PICKUPDELIVERYNOTE").toString();
                        emailMsg += "</td></tr>\n";

                        //Update the current revenue center
                        currRevCenterID = revCenterID;
                    }
                    else
                    {
                        //This is another print job for the same revenue center, so add it to the existing email
                        emailMsg += "<tr><td style='border:1px solid black'>";
                        emailMsg += printJobHM.get("PERSONNAME").toString();
                        emailMsg += "</td><td style='border:1px solid black'>";
                        emailMsg += printJobHM.get("PATRANSACTIONID").toString();
                        emailMsg += "</td><td style='border:1px solid black'>";
                        emailMsg += printJobHM.get("PHONE").toString();
                        emailMsg += "</td><td style='border:1px solid black'>";
                        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                        emailMsg += df.format((Date)printJobHM.get("TRANSACTIONDATE")).toString();
                        emailMsg += "</td><td style='border:1px solid black'>";
                        emailMsg += printJobHM.get("TRANSCOMMENT").toString();
                        emailMsg += "</td><td style='border:1px solid black'>";
                        emailMsg += printJobHM.get("PICKUPDELIVERYNOTE").toString();
                        emailMsg += "</td></tr>\n";
                    }
                }
                else
                {
                    //There is no revenue center ID in this print job somehow.... just ignore it and log
                    Logger.logMessage("Expired print data was to be sent via email but contained no RevenueCenterID. The data cannot be sent.", getKPLogFileName(), Logger.LEVEL.ERROR);
                }
            }

            //Ok, if email msg contains a partially filled out message string we ne need to complete it and send it now
            if(emailMsg != "")
            {
                emailMsg += "</table>";
                sendEmailToRevenueCenter(emailMsg,"WARNING: failed to print online order(s)", currRevCenterID);
            }
        }
    }

    public void sendEmailToRevenueCenter(String emailMessage, String emailSubject, int revenueCenterID) {
        sendErrorNotificationEmail(emailMessage, emailSubject, revenueCenterID, false);
    }

    public void sendErrorNotificationEmail(String emailMessage, String emailSubject, int revenueCenterID, boolean notifySystemMonitoring)
    {
        //Try to get the email settings, first from the properties file, then from the database
        String emailFromAddress = "";
        String emailServer = "";
        String emailPort = "";
        String emailUser = "";
        String emailPassword = "";

        List<String> emailList;
        if(!notifySystemMonitoring) {
            emailList = getAlertEmailAddress(revenueCenterID);
        } else {
            // get system monitoring email addresses instead of printer email addresses
            emailList = getAlertEmailAddress();
        }

        for (String emailAddress : emailList)
        {
            // email the receipt
            if (!emailAddress.isEmpty())
            {
                ArrayList emailCfgAL = dm.parameterizedExecuteQuery("data.ReceiptGen.EmailCfg", null, "", true, null);
                if (emailCfgAL != null && emailCfgAL.size() > 0)
                {
                    HashMap emailCfg = (HashMap)emailCfgAL.get(0);
                    emailUser = emailCfg.get("SMTPUSER").toString();
                    emailServer = emailCfg.get("SMTPSERVER").toString();
                    emailPort = emailCfg.get("SMTPPORT").toString();
                    emailPassword = emailCfg.get("SMTPPASSWORD").toString();
                    if(emailPassword != "")
                    {
                        emailPassword = StringFunctions.decodePassword(emailPassword);
                    }
                    emailFromAddress = emailCfg.get("SMTPFROMADDRESS").toString();

                    if (emailServer.isEmpty())
                    {
                        Logger.logMessage("In QC_Globals SMTPSERVER is empty and this is required to send email", getKPLogFileName(), Logger.LEVEL.ERROR);
                    }
                    else
                    {
                        if (emailUser.isEmpty())
                        {
                            Logger.logMessage("In sendEmailAlert(), about to send unauthenticated email receipt to " + emailAddress + " using mail server " + emailServer + " and port " + emailPort, getKPLogFileName(), Logger.LEVEL.TRACE);
                            MMHEmail.sendEmail(Integer.parseInt(emailPort), emailServer, emailAddress, emailFromAddress, emailSubject, emailMessage, "text/html");
                        }
                        else
                        {
                            Logger.logMessage("In sendEmailAlert(), about to send authenticated email receipt to " + emailAddress + " using mail server " + emailServer + " and port " + emailPort, getKPLogFileName(), Logger.LEVEL.TRACE);
                            MMHEmail.sendAuthEmail(Integer.parseInt(emailPort), emailServer, emailUser, emailPassword, emailAddress, emailFromAddress, emailSubject, emailMessage, "text/html");
                        }
                    }
                }
            }
            else
            {
                Logger.logMessage("PeripheralsDataManager.sendEmailToRevenueCenter error: unable to get email config", getKPLogFileName(), Logger.LEVEL.ERROR);
            }
        }
    }


    public ArrayList checkForExpiredOrErroredJobsAndSendEmail(ArrayList printData)
    {
        ArrayList queuedData = new ArrayList(); // these have not expired yet.
        ArrayList expired = new ArrayList();

        Calendar expiration = Calendar.getInstance();
        expiration.add(Calendar.MINUTE, -1 * kitchenPrinterTerminal.getPrintJobExpirationInMinutes());

        if(printData != null)
        {
            for (Object line : printData)
            {
                HashMap hm = (HashMap) line;

                if (hm.containsKey("QUEUETIME"))
                {
                    try {
                        Calendar baseTime = null;
                        boolean isOnlineOrder = HashMapDataFns.getBooleanVal(hm, "ONLINEORDER");
                        if (isOnlineOrder) {
                            Object oEstTime = hm.get("ESTIMATEDORDERTIME");
                            if (oEstTime != null) {
                                String sEstTime = oEstTime.toString();
                                if (!sEstTime.isEmpty()) {
                                    Logger.logMessage("  Using Estimated Time", Logger.LEVEL.DEBUG);
                                    baseTime = parseQueueTime(sEstTime);
                                }
                            }
                        }

                        if (baseTime == null) {
                            Logger.logMessage("  Using Queue Time", Logger.LEVEL.DEBUG);
                            baseTime = parseQueueTime(hm.get("QUEUETIME").toString());
                        }


                        String paTransactionID = hm.get("PATRANSACTIONID").toString();
                        if(baseTime.before(expiration))
                        {
                            Logger.logMessage("Adding PATransactionID " + paTransactionID + " to list of expired print jobs", getKPLogFileName(), Logger.LEVEL.TRACE);
                            expired.add(line);
                        }
                        else
                        {
                            queuedData.add(line);
                        }
                    } catch (ParseException e) {
                        Logger.logException("PeripheralsDataManager.checkForExpiredOrErroredJobsAndSendEmail exception!", getKPLogFileName(), e);
                    }
                }
            }
        }

        // set status to ERROR and send email
        if(expired != null && expired.size() > 0)
        {
            // Update status to ERROR
            updateJobsStatusForPrintQueue(expired, PrintJobStatusType.ERROR);

            Logger.logMessage("Sending email for expired online orders!", getKPLogFileName(), Logger.LEVEL.TRACE);
            sendEmailAlert(expired, queuedData);

        }

        return queuedData;
    }



    private String decode(String input)
    {
        try
        {
            return new String(decoder.decodeBuffer(input));
        }
        catch (IOException e)
        {
            Logger.logException("QCKitchenPrinter.Decode() exception", getKPLogFileName(), e);
        }

        return "";
    }
    // endregion
    //--------------------------------------------------------------------------


}
