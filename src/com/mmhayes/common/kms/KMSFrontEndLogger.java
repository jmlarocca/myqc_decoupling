package com.mmhayes.common.kms;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-03-10 15:01:19 -0500 (Wed, 10 Mar 2021) $: Date of last commit
    $Rev: 13613 $: Revision of last commit
    Notes: Logs messages from the OMS front-end.
*/

import com.google.gson.JsonObject;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.apache.commons.lang.math.NumberUtils;

import java.util.Collections;
import java.util.Objects;

public class KMSFrontEndLogger {

    private static final String log = "OMS_F";

    /**
     * <p>Logs the message sent from an OMS station.</p>
     *
     * @param message A {@link JsonObject} that contains the message sent from the OMS station.
     */
    public static void logMessage (JsonObject message) {
        
        if (message == null) {
            Logger.logMessage("The message passed to KMSFrontEndLogger.logMessage can't be null.", log, Logger.LEVEL.ERROR);
            return;
        }

        Logger.logMessage(String.format("Raw message from OMS station: %s",
                Objects.toString(Collections.singletonList(message).toString())), log, Logger.LEVEL.TRACE);
        
        // extract the model from the message
        JsonObject model = ((JsonObject) message.get("model"));
        
        if (model == null) {
            Logger.logMessage("Unable to extract the model from the message in KMSFrontEndLogger.logMessage!", log, Logger.LEVEL.ERROR);
            return;
        }
        
        // extract the station ID, message, and the level at which to log the message
        int stationID = model.get("stn") != null && StringFunctions.stringHasContent(model.get("stn").getAsString()) && NumberUtils.isNumber(model.get("stn").getAsString())
                ? Integer.parseInt(model.get("stn").getAsString()) : -2;
        String payload = model.get("msg") != null && StringFunctions.stringHasContent(model.get("msg").getAsString()) ? model.get("msg").getAsString() : "";
        String logLevelStr = model.get("lvl") != null && StringFunctions.stringHasContent(model.get("lvl").getAsString()) ? model.get("lvl").getAsString() : "";

        if ((StringFunctions.stringHasContent(payload)) && (StringFunctions.stringHasContent(logLevelStr))) {
            Logger.LEVEL logLevel = getLogLevel(logLevelStr);
            Logger.logMessage(String.format("Stn: %s => %s",
                    Objects.toString(stationID, "N/A"),
                    Objects.toString(payload, "N/A")), log, logLevel);
        }
        
    }

    /**
     * <p>Determines the log level to be used from the given log level {@link String}.</p>
     *
     * @param logLevelStr The log level {@link String} to use to determine the log level.
     * @return The {@link com.mmhayes.common.utils.Logger.LEVEL} at which to log.
     */
    private static Logger.LEVEL getLogLevel (String logLevelStr) {

        switch (logLevelStr) {
            case "NONE":
                return Logger.LEVEL.NONE;
            case "IMPORTANT":
                return Logger.LEVEL.IMPORTANT;
            case "ERROR":
                return Logger.LEVEL.ERROR;
            case "WARNING":
                return Logger.LEVEL.WARNING;
            case "TRACE":
                return Logger.LEVEL.TRACE;
            case "DEBUG":
                return Logger.LEVEL.DEBUG;
            case "LUDICROUS":
                return Logger.LEVEL.LUDICROUS;
            default:
                return Logger.LEVEL.ERROR;
        }

    }

}