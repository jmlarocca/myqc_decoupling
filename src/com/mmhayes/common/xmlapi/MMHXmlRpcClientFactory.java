package com.mmhayes.common.xmlapi;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-20 13:10:34 -0400 (Tue, 20 Apr 2021) $: Date of last commit
    $Rev: 13819 $: Revision of last commit
    Notes: Creates instances of a MMHXmlRpcClient.
*/

import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHObjectFactory;

import java.net.MalformedURLException;

public class MMHXmlRpcClientFactory implements MMHObjectFactory<MMHXmlRpcClient> {

    // private member variables
    private String url;
    private boolean streamMessages;
    private int timeoutInMS;
    private String callerHostname;
    private String receiverHostname;

    /**
     * <p>Constructor for a {@link MMHXmlRpcClientFactory}.</p>
     *
     * @param url The url {@link String} of the endpoint this client should reach out to.
     * @param streamMessages Whether or not to stream messages.
     * @param timeoutInMS Timeout in milliseconds for the XML RPC client.
     * @param callerHostname The hostname {@link String} of the machine that is trying to invoke a method on another machine.
     * @param receiverHostname The hostname {@link String} of the machine that another machine is trying to invoke a method upon.
     */
    public MMHXmlRpcClientFactory (String url, boolean streamMessages, int timeoutInMS, String callerHostname, String receiverHostname) {
        super();
        this.url = url;
        this.streamMessages = streamMessages;
        this.timeoutInMS = timeoutInMS;
        this.callerHostname = callerHostname;
        this.receiverHostname = receiverHostname;
    }

    /**
     * <p>Tries to create a new instance of a {@link MMHXmlRpcClient}.</p>
     *
     * @return The newly created {@link MMHXmlRpcClient} instance.
     */
    @Override
    public MMHXmlRpcClient newInstance () {

        try {
            return new MMHXmlRpcClient(url, streamMessages, timeoutInMS, callerHostname, receiverHostname);
        }
        catch (MalformedURLException e) {
            Logger.logException(e);
        }

        return null ;
    }

}