package com.mmhayes.common.xmlapi;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-04-27 09:54:05 -0400 (Tue, 27 Apr 2021) $: Date of last commit
    $Rev: 13859 $: Revision of last commit
    Notes: Manages XML-RPC calls.
*/

import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcClient;
import redstone.xmlrpc.XmlRpcStruct;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>Manages XML-RPC calls.</p>
 *
 */
public class XmlRpcManager implements Serializable {

    // the only instance of a XmlRpcManager that will exist
    private static volatile XmlRpcManager xmlRpcManager = null;

    // private member variables of a XmlRpcManager
    private final String SERVER_URL =
            (StringFunctions.stringHasContent(MMHProperties.getAppSetting("site.application.serverXmlRpc.path").trim()) ? MMHProperties.getAppSetting("site.application.serverXmlRpc.path").trim() : "");
    private ConcurrentHashMap<String, XmlRpcClient> xmlRpcClients = new ConcurrentHashMap<>();

    /**
     * <p>Private constructor for a {@link XmlRpcManager}.</p>
     *
     */
    private XmlRpcManager () {}

    /**
     * <p>Gets the only instance of a {@link XmlRpcManager}.</p>
     *
     * @return The only instance of a {@link XmlRpcManager}.
     * @throws Exception
     */
    public static XmlRpcManager getInstance () throws Exception {

        // check if we have a XmlRpcManager instance to return
        if (xmlRpcManager == null) {
            xmlRpcManager = XmlRpcManager.init();
        }

        return xmlRpcManager;
    }

    /**
     * <p>Instantiates and returns the only instance of a {@link XmlRpcManager}.</p>
     *
     * @return The {@link XmlRpcManager} instance.
     * @throws Exception
     */
    public static synchronized XmlRpcManager init () throws Exception {

        // check if the XmlRpcManager instance already exists
        if (xmlRpcManager != null) {
            throw new Exception("An instance of XmlRpcManager already exists.");
        }
        else {
            xmlRpcManager = new XmlRpcManager();
        }

        return xmlRpcManager;
    }

    /**
     * <p>Implementation of readResolve() to prevent creating a {@link XmlRpcManager} through serialization.</p>
     *
     * @return The only instance of a {@link XmlRpcManager}.
     * @throws Exception
     */
    protected XmlRpcManager readResolve () throws Exception {
        return getInstance();
    }

    /**
     * <p>Calls a method on another terminal through XML-RPC.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param war Name of the war as a {@link String} running on the terminal to invoke the method on.
     * @param hostname The hostname {@link String} of the terminal to invoke the method on.
     * @param method The name {@link String} of the method to invoke on the terminal.
     * @param args An array of {@link Object} arguments that should be passed to the method being invoked on the terminal.
     * @return A response {@link Object} which contains what was returned from the method being invoked on the terminal.
     */
    public Object xmlRpcInvokeOnTerminal (String log, String war, String hostname, String method, Object[] args) {
        Object response = new Object();

        try {
            String url = "http://"+hostname+"/"+war+"/QuickCharge/XmlRpcManager";
            response = xmlRpcPost(log, url, method, args);
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to invoke the method %s on the terminal %s in XmlRpcManager.xmlRpcInvokeOnTerminal",
                    Objects.toString(method, "N/A"),
                    Objects.toString(hostname, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return response;
    }

    /**
     * <p>Calls a method on the server through XML-RPC.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param method The name {@link String} of the method to invoke on the server.
     * @param args An array of {@link Object} arguments that should be passed to the method being invoked on the server.
     * @return A response {@link Object} which contains what was returned from the method being invoked on the server.
     */
    public Object xmlRpcInvokeOnServer (String log, String method, Object[] args) {
        Object response = new Object();

        try {
            response = xmlRpcPost(log, SERVER_URL, method, args);
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to invoke the method %s on the server in XmlRpcManager.xmlRpcInvokeOnServer",
                    Objects.toString(method, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return response;
    }

    /**
     * <p>Calls a method on the server through XML-RPC.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param method The name {@link String} of the method to invoke on the server.
     * @param args An array of {@link Object} arguments that should be passed to the method being invoked on the server.
     * @return A response {@link Object} which contains what was returned from the method being invoked on the server.
     */
    public Object xmlRpcInvokeInstanceOnServer (String log, String method, Object[] args) {
        Object response = new Object();

        try {
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            if (StringFunctions.stringHasContent(url)) {
                Logger.logMessage("XmlRpcManager.xmlRpcInvokeInstanceOnServer URL = " + url, log, Logger.LEVEL.DEBUG);
                response = xmlRpcPost(log, url, method, args);
            }
            else {
                throw new Exception(String.format("Unable to invoke the method %s on the server in XmlRpcManager.xmlRpcInvokeInstanceOnServer " +
                        "due to an invalid app property provided for the server's XML RPC path!", Objects.toString(method, "N/A")));
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to invoke the method %s on the server in XmlRpcManager.xmlRpcInvokeOnServer",
                    Objects.toString(method, "N/A")), log, Logger.LEVEL.ERROR);
        }

        return response;
    }

    /**
     * <p>Attempts to invoke the method at the given url {@link String}.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param url The endpoint {@link String} where the method should be invoked.
     * @param method The name {@link String} of the method to invoke.
     * @param args An array of {@link Object} arguments that should be passed to the method being invoked.
     * @return The {@link Object} response received from the method invocation.
     */
    private Object xmlRpcPost (String log, String url, String method, Object[] args) {
        Object response = new Object();

        try {
            if (StringFunctions.stringHasContent(url)) {
                // get the XmlRpcClient if it already exists, otherwise create a new XmlRpcClient
                XmlRpcClient xmlRpcClient;
                if (!DataFunctions.isEmptyMap(xmlRpcClients)) {
                    if (xmlRpcClients.containsKey(url)) {
                        // get the existing XmlRpcClient
                        xmlRpcClient = xmlRpcClients.get(url);
                    }
                    else {
                        // create a new XmlRpcClient
                        xmlRpcClient = new XmlRpcClient(url, true);
                    }
                }
                else {
                    // create a new XmlRpcClient
                    xmlRpcClient = new XmlRpcClient(url, true);
                }

                // attempt to invoke the method
                response = xmlRpcClient.invoke(method, args);

                // if the method couldn't be invoked then login and try to invoke the method again
                if ((response.toString().toLowerCase().equalsIgnoreCase("invalid terminal")) || (response.toString().toLowerCase().equalsIgnoreCase("invalid session"))) {
                    // attempt to login
                    if (performXmlRpcLogin(log, xmlRpcClient)) {
                        // try to invoke the method again and return the response
                        response = xmlRpcClient.invoke(method, args);
                    }
                    else {
                        Logger.logMessage(String.format("The machine %s was unable to successfully perform a XML-RPC login in XmlRpcManager.xmlRpcPost",
                                Objects.toString(getHostname(log), "N/A")), log, Logger.LEVEL.ERROR);
                    }
                }
            }
            else {
                Logger.logMessage(String.format("Invalid URL of %s was passed to XmlRpcManager.xmlRpcPost", Objects.toString(url, "N/A")), log, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("There was a problem trying to invoke the method %s at the url %s in XmlRpcManager.xmlRpcPost",
                    Objects.toString(method, "N/A"),
                    Objects.toString(method, url)), log, Logger.LEVEL.ERROR);
        }

        return response;
    }

    /**
     * <p>Attempts to login the machine so that it can use XML-RPC.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param xmlRpcClient The {@link XmlRpcClient} to use to try to login with.
     * @return Whether or not this machine could login successfully to use XML-RPC.
     */
    @SuppressWarnings("ConstantConditions")
    private boolean performXmlRpcLogin (String log, XmlRpcClient xmlRpcClient) {
        boolean loginSuccess = false;

        try {
            String macAddress = getMacAddress(log);
            String hostname = getHostname(log);
            if ((StringFunctions.stringHasContent(macAddress)) && (StringFunctions.stringHasContent(hostname))) {
                Object loginResult = xmlRpcClient.invoke("XmlRpcLogin.kmsXmlRpcLogin", new Object[]{macAddress, hostname});
                if ((loginResult != null)
                        && (!loginResult.toString().toLowerCase().equalsIgnoreCase("invalid terminal"))
                        && (!loginResult.toString().toLowerCase().equalsIgnoreCase("invalid session"))) {
                    loginSuccess = true;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage(String.format("The machine %s was unable to successfully log on to use XML-RPC in XmlRpcManager.performXmlRpcLogin",
                    Objects.toString(getHostname(log), "N/A")), log, Logger.LEVEL.ERROR);
        }

        return loginSuccess;
    }

    /**
     * <p>Utility method to get the MAC address of this machine.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @return The MAC address {@link String} of the machine.
     */
    public String getMacAddress (String log) {
        String macAddress = "";

        try {
            // try to get the MAC address from the app properties
            String macAddressFromAppProps = MMHProperties.getCommonAppSetting("site.networkConnCode");
            if (StringFunctions.stringHasContent(macAddressFromAppProps)) {
                macAddress = StringFunctions.decodePassword(macAddressFromAppProps);
            }
            else {
                // failed to get the MAC address from the app properties, try to get it from the Network interfaces
                Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
                if (!DataFunctions.isEmptyEnumeration(networkInterfaces)) {
                    while (networkInterfaces.hasMoreElements()) {
                        NetworkInterface networkInterface = networkInterfaces.nextElement();
                        if (networkInterface != null) {
                            byte[] networkAddress = networkInterface.getHardwareAddress();
                            if ((networkAddress != null) && (networkAddress.length > 0)) {
                                String networkAddressStr = new String(networkAddress);
                                // make sure the MAC address is of a valid length
                                if ((StringFunctions.stringHasContent(networkAddressStr)) && (networkAddressStr.length() == 12)) {
                                    macAddress = networkAddressStr;
                                }
                                else {
                                    Logger.logMessage(String.format("The MAC address %s is of an invalid length. Unable to determine the MAC address in XmlRpcManager.getMacAddress!",
                                            Objects.toString(networkAddressStr, "N/A")), log, Logger.LEVEL.ERROR);
                                }
                            }
                            else {
                                Logger.logMessage("No valid network hardware address has been found. Unable to determine the MAC address in XmlRpcManager.getMacAddress!", log, Logger.LEVEL.ERROR);
                            }
                        }
                    }
                }
                else {
                    Logger.logMessage("No network interfaces are available for this machine. Unable to determine the MAC address in XmlRpcManager.getMacAddress!", log, Logger.LEVEL.ERROR);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to obtain the MAC address of the machine in XmlRpcManager.getMacAddress", log, Logger.LEVEL.ERROR);
        }

        return macAddress;
    }

    /**
     * <p>Utility method to get the hostname of this machine.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @return The hostname {@link String} of the machine.
     */
    public String getHostname (String log) {
        String hostname = "";

        try {
            if (StringFunctions.stringHasContent(InetAddress.getLocalHost().getHostName())) {
                hostname = InetAddress.getLocalHost().getHostName();
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to obtain the hostname of the machine in XmlRpcManager.getHostname", log, Logger.LEVEL.ERROR);
        }

        return hostname;
    }

    /**
     * <p>Utility method to get a boolean response received from a method invoked through XML-RPC.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param response The response {@link Object} received from the method invoked through XML-RPC.
     * @return The anticipated boolean response from the method that was invoked through XML-RPC.
     */
    public boolean getBooleanResponse (String log, Object response) {
        boolean responseReceived = false;

        try {
            if ((response != null) && (response instanceof XmlRpcStruct)) {
                XmlRpcStruct struct = ((XmlRpcStruct) response);
                if ((!DataFunctions.isEmptyMap(struct)) && (struct.containsKey("responseCode")) && (((int) struct.get("responseCode")) == 0)) {
                    responseReceived = struct.getBoolean("response");
                }
            }
            else if (response != null) {
                return ((boolean) response);
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to get a boolean response received from a method invoked through XML-RPC " +
                    "in XmlRpcManager.getBooleanResponse", log, Logger.LEVEL.ERROR);
        }

        return responseReceived;
    }

    /**
     * <p>Utility method to get a {@link String} response received from a method invoked through XML-RPC.</p>
     *
     * @param log The file path {@link String} of the file to log any messages for this method to.
     * @param response The response {@link Object} received from the method invoked through XML-RPC.
     * @return The anticipated {@link String} response from the method that was invoked through XML-RPC.
     */
    public String getStringResponse (String log, Object response) {
        String responseReceived = "";

        try {
            if ((response != null) && (response instanceof XmlRpcStruct)) {
                XmlRpcStruct struct = ((XmlRpcStruct) response);
                if ((!DataFunctions.isEmptyMap(struct)) && (struct.containsKey("responseCode")) && (((int) struct.get("responseCode")) == 0)) {
                    responseReceived = HashMapDataFns.getStringVal(struct, "response", true);
                }
            }
            else if (response != null) {
                return response.toString();
            }
        }
        catch (Exception e) {
            Logger.logException(e, log);
            Logger.logMessage("There was a problem trying to get a String response received from a method invoked through XML-RPC " +
                    "in XmlRpcManager.getStringResponse", log, Logger.LEVEL.ERROR);
        }

        return responseReceived;
    }

    // TODO ADD MORE RESPONSE TYPES

}