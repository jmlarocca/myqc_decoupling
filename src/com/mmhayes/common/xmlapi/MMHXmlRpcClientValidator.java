package com.mmhayes.common.xmlapi;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-09-30 13:30:03 -0400 (Thu, 30 Sep 2021) $: Date of last commit
    $Rev: 15479 $: Revision of last commit
    Notes: Validates MMHXmlRpcClient instances.
*/

import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHPool;

import java.io.IOException;

public class MMHXmlRpcClientValidator implements MMHPool.Validator<MMHXmlRpcClient> {

    /**
     * <p>Checks whether or not the {@link MMHXmlRpcClient} instance is valid.</p>
     *
     * @param mmhXmlRpcClient The {@link MMHXmlRpcClient} instance to validate.
     * @return Whether or not the {@link MMHXmlRpcClient} instance is valid.
     */
    @Override
    public boolean isValid (MMHXmlRpcClient mmhXmlRpcClient) {

        if (mmhXmlRpcClient == null) {
            return false;
        }

        return true;//mmhXmlRpcClient.isConnected();
    }

    /**
     * <p>Invalidates the {@link MMHXmlRpcClient} instance.</p>
     *
     * @param mmhXmlRpcClient The {@link MMHXmlRpcClient} instance to invalidate.
     */
    @Override
    public void invalidate (MMHXmlRpcClient mmhXmlRpcClient) {
//        try {
//            mmhXmlRpcClient.closeConnection();
//        }
//        catch (IOException e) {
//            Logger.logException(e);
//        }
    }

}