package com.mmhayes.common.xmlapi;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2019-12-19 16:49:26 -0500 (Thu, 19 Dec 2019) $: Date of last commit
    $Rev: 44750 $: Revision of last commit
    Notes: Makes XML RPC calls with the given XML RPC client.
*/

import com.mmhayes.common.kitchenPrinters.QCKitchenPrinter;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.XmlRpcClient;
import redstone.xmlrpc.XmlRpcStruct;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Singleton class responsible for making XML RPC calls.
 *
 */
public class XmlRpcUtil {

    // the only instance of a XmlRpcUtil for this WAR
    private static volatile XmlRpcUtil xmlRpcUtilInstance = null;

    // log file specific to the XmlRpcUtil
    private static final String XRU_LOG = "XmlRpcUtil.log";

    // variables within the XmlRpcUtil scope
    private static final long DEFAULT_XML_RPC_TIMEOUT = 60000L;

    // store XML RPC clients as they are created
    private ConcurrentHashMap<String, XmlRpcClient> xmlRpcClients = new ConcurrentHashMap<>();

    /**
     * Private constructor for a XmlRpcUtil.
     *
     */
    private XmlRpcUtil () {}

    /**
     * Get the only instance of XmlRpcUtil for this WAR.
     *
     * @return {@link com.mmhayes.common.xmlapi.XmlRpcUtil} The XmlRpcUtil instance for this WAR.
     */
    public static XmlRpcUtil getInstance () {

        try {
            if (xmlRpcUtilInstance == null) {
                throw new RuntimeException("The XmlRpcUtil must be instantiated before trying to obtain instance.");
            }
        }
        catch (Exception e) {
            Logger.logException(e, XRU_LOG);
            Logger.logMessage("There was a problem trying to get the only instance of a XmlRpcUtil in " +
                    "XmlRpcUtil.getInstance", XRU_LOG, Logger.LEVEL.ERROR);
        }

        return xmlRpcUtilInstance;
    }

    /**
     * Instantiate and return the only instance of XmlRpcUtil for this WAR.
     *
     * @return {@link com.mmhayes.common.xmlapi.XmlRpcUtil} The XmlRpcUtil instance for this WAR
     */
    public static synchronized XmlRpcUtil init () {

        try {
            if (xmlRpcUtilInstance != null) {
                throw new RuntimeException("The XmlRpcUtil has already been instantiated and may not be instantiated " +
                        "again.");
            }

            // create the instance
            xmlRpcUtilInstance = new XmlRpcUtil();
            Logger.logMessage("The instance of XmlRpcUtil has been instantiated", XRU_LOG, Logger.LEVEL.TRACE);
        }
        catch (Exception e) {
            Logger.logException(e, XRU_LOG);
            Logger.logMessage("There was a problem trying to instantiate and return the only instance of XmlRpcUtil " +
                    "in XmlRpcUtil.init", XRU_LOG, Logger.LEVEL.ERROR);
        }

        return xmlRpcUtilInstance;
    }

    /**
     * Get the timeout in milliseconds for the XML RPC client form the app properties, if the property is invalid or
     * isn't found then a default of 60,000 milliseconds is used.
     *
     * @return The timeout for the XML RPC client in milliseconds.
     */
    private long getXmlRpcClientTimeoutInMS () {
        long xmlRpcClientTimeoutInMS = DEFAULT_XML_RPC_TIMEOUT;

        try {
            String xmlRpcTimeoutFromProps = MMHProperties.getAppSetting("site.backend.xmlRpc.timeoutInMilliseconds");

            if ((xmlRpcTimeoutFromProps != null) && (!xmlRpcTimeoutFromProps.isEmpty())) {
                xmlRpcClientTimeoutInMS = new Long(xmlRpcTimeoutFromProps);
                Logger.logMessage("A timeout of "+xmlRpcTimeoutFromProps+" milliseconds will be used for the XML RPC " +
                        "client in XmlRpcUtil.getXmlRpcClientTimeoutInMS", XRU_LOG, Logger.LEVEL.TRACE);
            }
        }
        catch (Exception e) {
            Logger.logException(e, XRU_LOG);
            Logger.logMessage("There was a problem trying to obtain the XmlRpcClient timeout in milliseconds from the " +
                    "app properties file in XmlRpcUtil.getXmlRpcClientTimeoutInMS", XRU_LOG, Logger.LEVEL.ERROR);
        }

        return xmlRpcClientTimeoutInMS;
    }

    /**
     * Make a XML RPC call to the given method name with the given method parameters to the given endpoint.
     *
     * @param url {@link String} Endpoint of the XML RPC call.
     * @param macAddress {@link String} MAC address of the machine we are trying to call the method on through XML RPC.
     * @param hostname {@link String} Hostname of the machine we are trying to call the method on through XML RPC.
     * @param methodName {@link String} Name of the method we are trying to call through XML RPC at the endpoint.
     * @param methodParams {@link Object[]} Parameters for the method we are trying to call through XML RPC.
     * @return {@link Object} Response received from the XML RPC call.
     */
    public Object xmlRpcPost (String url, String macAddress, String hostname, String methodName, Object[] methodParams) {
        Object response = null;

        try {
            Logger.logMessage("***** START XML RPC CALL INFORMATION *****", XRU_LOG, Logger.LEVEL.DEBUG);
            Logger.logMessage(String.format("CALLER HOSTNAME: %s", Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")), XRU_LOG, Logger.LEVEL.DEBUG);
            Logger.logMessage(String.format("RECEIVER HOSTNAME: %s", Objects.toString(hostname, "NULL")), XRU_LOG, Logger.LEVEL.DEBUG);
            Logger.logMessage(String.format("METHOD CALLED: %s", Objects.toString(methodName, "NULL")), XRU_LOG, Logger.LEVEL.DEBUG);
            Logger.logMessage(String.format("ENDPOINT: %s", Objects.toString(url, "NULL")), XRU_LOG, Logger.LEVEL.DEBUG);
            Logger.logMessage("***** END XML RPC CALL INFORMATION *****", XRU_LOG, Logger.LEVEL.DEBUG);

            Logger.logMessage("XML RPC POST: methodName=" + methodName + ", url=" + url, XRU_LOG, Logger.LEVEL.DEBUG);
            // check if an XmlRpcClient already exists at the given endpoint
            XmlRpcClient xmlRpcClient;
            if ((xmlRpcClients != null) && (!xmlRpcClients.isEmpty())) {
                if (xmlRpcClients.containsKey(url)) {
                    xmlRpcClient = xmlRpcClients.get(url);
                }
                else {
                    // create a new XML RPC client
                    xmlRpcClient = new XmlRpcClient(url, true, ((int) getXmlRpcClientTimeoutInMS()));
                    xmlRpcClients.put(url, xmlRpcClient);
                }
            }
            else {
                // create a new XML RPC client
                xmlRpcClient = new XmlRpcClient(url, true, ((int) getXmlRpcClientTimeoutInMS()));
                xmlRpcClients.put(url, xmlRpcClient);
            }

            // try to invoke the desired method
            response = xmlRpcClient.invoke(methodName, methodParams);

            // if we were unable to invoke the method try to login and invoke the method again
            if ((response.toString().toLowerCase().contains("invalid session")) || ((response.toString().toLowerCase().contains("invalid terminal")))) {
                Logger.logMessage("Unable to invoke the method "+Objects.toString(methodName, "NULL")+" in " +
                        "XmlRpcUtil.xmlRpcPost ", XRU_LOG, Logger.LEVEL.ERROR);

                // try to log into the machine we wish to hit the endpoint on
                Object loginResult = xmlRpcClient.invoke("SecureLogin.kpTerminalLogin", new Object[]{macAddress, hostname});

                // check that we were able to log into the machine
                if ((loginResult != null) && (loginResult.toString().toLowerCase().contains("invalid terminal"))) {
                    Logger.logMessage("Unable to log into the machine with hostname "+Objects.toString(hostname, "NULL") +
                            ", so that the method "+Objects.toString(methodName, "NULL")+" can't be reached at the " +
                            "endpoint "+Objects.toString(url, "NULL")+" in XmlRpcUtil.xmlRpcPost", XRU_LOG,
                            Logger.LEVEL.ERROR);
                }
                else if (loginResult == null) {
                    Logger.logMessage("Unable to log into the machine with hostname "+Objects.toString(hostname, "NULL") +
                            ", so that the method "+Objects.toString(methodName, "NULL")+" can't be reached at the " +
                            "endpoint "+Objects.toString(url, "NULL")+" in XmlRpcUtil.xmlRpcPost", XRU_LOG,
                            Logger.LEVEL.ERROR);
                }
                else {
                    // call the method we want to call at the endpoint and return the result
                    response = xmlRpcClient.invoke(methodName, methodParams);
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, XRU_LOG);
            Logger.logMessage("There was a problem trying to make a XML RPC call from the machine with hostname " +
                    Objects.toString(PeripheralsDataManager.getPHHostname(), "NULL")+" to the machine with hostname " +
                    Objects.toString(hostname, "NULL")+", the method "+Objects.toString(methodName, "NULL")+" couldn't " +
                    "be reached at the endpoint "+Objects.toString(url, "NULL")+" in XmlRpcUtil.xmlRpcPost", XRU_LOG,
                    Logger.LEVEL.ERROR);
        }

        return response;
    }

    /**
     * Utility method to make an XML RPC call to the server.
     *
     * @param methodName {@link String} name of the method to call.
     * @param args {@link java.util.Objects[]} Any arguments to pass to the method we are calling.
     * @return {@link Object} Response received from the XML RPC call.
     */
    @SuppressWarnings("UnnecessaryLocalVariable")
    public Object callServer (String methodName, Object[] args) {
        Object response = null;

        try {
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, methodName, args);
            response = xmlRpcResponse;
        }
        catch (Exception e) {
            Logger.logException(e, XRU_LOG);
            Logger.logMessage(String.format("There was a problem trying to call the method %s on the server in " +
                    "XmlRpcUtil.callServer",
                    Objects.toString(methodName, "NULL")), XRU_LOG, Logger.LEVEL.ERROR);
        }

        return response;
    }

    /**
     * Parse the response received from an XML RPC call to a method that should return a boolean. Try to get the boolean
     * returned by the method if the XML RPC call was successful.
     *
     * @param xmlRpcResponse {@link Object} XML RPC response object received from the XML RPC call to the given method.
     * @param methodCalledName {@link String} Name of the method that was called through XML RPC and should return a
     *         boolean.
     * @return The boolean that was returned by the method called through XML RPC.
     */
    public boolean parseBooleanXmlRpcResponse (Object xmlRpcResponse, String methodCalledName) {
        boolean result = false;

        try {
            if ((xmlRpcResponse != null) && (xmlRpcResponse instanceof XmlRpcStruct)) {
                XmlRpcStruct response = (XmlRpcStruct) xmlRpcResponse;
                if ((!response.isEmpty()) && (response.containsKey("responseCode"))
                        && (((int) response.get("responseCode")) == 0)) {
                    result = response.getBoolean("response");
                }
            }
            else {
                Logger.logMessage("Invalid response received from XML RPC call to " +
                        Objects.toString(methodCalledName, "NULL")+" on the the active printer host", XRU_LOG,
                        Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, XRU_LOG);
            Logger.logMessage("There was a problem trying to obtain the boolean from the XML RPC call made to " +
                    Objects.toString(methodCalledName, "NULL")+" in XmlRpcUtil.parseBooleanXmlRpcResponse", XRU_LOG,
                    Logger.LEVEL.ERROR);
        }

        return result;
    }

    /**
     * Parse the response received from an XML RPC call to a method that should return a boolean. Try to get the boolean
     * returned by the method if the XML RPC call was successful.
     *
     * @param xmlRpcResponse {@link Object} XML RPC response object received from the XML RPC call to the given method.
     * @param methodCalledName {@link String} Name of the method that was called through XML RPC and should return a
     *         boolean.
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param url The url {@link String} where we are trying to invoke the method.
     * @return The boolean that was returned by the method called through XML RPC.
     */
    public boolean parseBooleanXmlRpcResponse (Object xmlRpcResponse, String methodCalledName, String callerHostname, String url) {
        boolean result = false;

        try {
            if ((xmlRpcResponse != null) && (xmlRpcResponse instanceof XmlRpcStruct)) {
                XmlRpcStruct response = (XmlRpcStruct) xmlRpcResponse;
                if ((!response.isEmpty()) && (response.containsKey("responseCode"))
                        && (((int) response.get("responseCode")) == 0)) {
                    result = response.getBoolean("response");
                }
            }
            else {
                Logger.logMessage(String.format("Invalid response received from XML RPC call to %s from the terminal %s to the URL %s in XmlRpcUtil.parseBooleanXmlRpcResponse",
                        Objects.toString(methodCalledName, "NULL"),
                        Objects.toString(callerHostname, "NULL"),
                        Objects.toString(url, "NULL")), XRU_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, XRU_LOG);
            Logger.logMessage("There was a problem trying to obtain the boolean from the XML RPC call made to " +
                    Objects.toString(methodCalledName, "NULL")+" in XmlRpcUtil.parseBooleanXmlRpcResponse", XRU_LOG,
                    Logger.LEVEL.ERROR);
        }

        return result;
    }

    /**
     * Parse the response received from an XML RPC call to a method that should return a String. Try to get the String
     * returned by the method if the XML RPC call was successful.
     *
     * @param xmlRpcResponse {@link Object} XML RPC response object received from the XML RPC call to the given method.
     * @param methodCalledName {@link String} Name of the method that was called through XML RPC and should return a
     *         String.
     * @return {@link String} The String that was returned by the method called through XML RPC.
     */
    public String parseStringXmlRpcResponse (Object xmlRpcResponse, String methodCalledName) {
        String result = "";

        try {
            if ((xmlRpcResponse != null) && (xmlRpcResponse instanceof XmlRpcStruct)) {
                XmlRpcStruct response = (XmlRpcStruct) xmlRpcResponse;
                if ((!response.isEmpty()) && (response.containsKey("responseCode"))
                        && (((int) response.get("responseCode")) == 0)) {
                    result = response.getString("response");
                }
            }
            else {
                Logger.logMessage("Invalid response received from XML RPC call to " +
                        Objects.toString(methodCalledName, "NULL")+" on the the active printer host", XRU_LOG,
                        Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, XRU_LOG);
            Logger.logMessage("There was a problem trying to obtain the String from the XML RPC call made to " +
                    Objects.toString(methodCalledName, "NULL")+" in XmlRpcUtil.parseStringXmlRpcResponse", XRU_LOG,
                    Logger.LEVEL.ERROR);
        }

        return result;
    }

    /**
     * Parse the response received from an XML RPC call to a method that should return a java.util.Date. Try to get the
     * Date returned by the method if the XML RPC call was successful.
     *
     * @param xmlRpcResponse {@link Object} XML RPC response object received from the XML RPC call to the given method.
     * @param methodCalledName {@link String} Name of the method that was called through XML RPC and should return a
     *         Date.
     * @return {@link java.util.Date} The String that was returned by the method called through XML RPC.
     */
    public Date parseDateXmlRpcResponse (Object xmlRpcResponse, String methodCalledName) {
        Date result = null;

        try {
            if ((xmlRpcResponse != null) && (xmlRpcResponse instanceof XmlRpcStruct)) {
                XmlRpcStruct response = (XmlRpcStruct) xmlRpcResponse;
                if ((!response.isEmpty()) && (response.containsKey("responseCode"))
                        && (((int) response.get("responseCode")) == 0)) {
                    result = response.getDate("response");
                }
            }
            else {
                Logger.logMessage("Invalid response received from XML RPC call to " +
                        Objects.toString(methodCalledName, "NULL")+" on the the active printer host", XRU_LOG,
                        Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, XRU_LOG);
            Logger.logMessage("There was a problem trying to obtain the Date from the XML RPC call made to " +
                    Objects.toString(methodCalledName, "NULL")+" in XmlRpcUtil.parseDateXmlRpcResponse", XRU_LOG,
                    Logger.LEVEL.ERROR);
        }

        return result;
    }

    /**
     * Parse the response received from an XML RPC call to a method that should return an ArrayList. Try to get
     * the ArrayList returned by the method if the XML RPC call was successful.
     *
     * @param xmlRpcResponse {@link Object} XML RPC response object received from the XML RPC call to the given method.
     * @param methodCalledName {@link String} Name of the method that was called through XML RPC and should return an
     *         ArrayList.
     * @return {@link java.util.ArrayList} The ArrayList that was returned by the method called through XML RPC.
     */
    public ArrayList parseArrayListXmlRpcResponse (Object xmlRpcResponse, String methodCalledName) {
        ArrayList data = new ArrayList();

        try {
            if ((xmlRpcResponse != null) && (xmlRpcResponse instanceof XmlRpcStruct)) {
                XmlRpcStruct response = (XmlRpcStruct) xmlRpcResponse;
                if ((!response.isEmpty()) && (response.containsKey("responseCode"))
                        && (((int) response.get("responseCode")) == 0)) {
                    data = (ArrayList) response.get("response");
                }
            }
            else {
                Logger.logMessage("Invalid response received from XML RPC call to " +
                        Objects.toString(methodCalledName, "NULL")+"  in XmlRpcUtil.parseArrayListXmlRpcResponse",
                        XRU_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, XRU_LOG);
            Logger.logMessage("There was a problem trying to the ArrayList from the XML RPC call made to " +
                    Objects.toString(methodCalledName, "NULL")+" in XmlRpcUtil.parseArrayListXmlRpcResponse",
                    XRU_LOG, Logger.LEVEL.ERROR);
        }

        return data;
    }

    /**
     * <p>Parses the response {@link Object} from the XML RPC call to the given method {@link String} to find a {@link QCKitchenPrinter} in the response.</p>
     *
     * @param xmlRpcResponse The response {@link Object} received from the XML RPC call.
     * @param callingMethod The method {@link String} that was called through XML RPC.
     * @return The {@link QCKitchenPrinter} in the response received from the XML RPC call.
     */
    public QCKitchenPrinter parseQCKitchenPrinterXmlRpcResponse (Object xmlRpcResponse, String callingMethod) {
        QCKitchenPrinter qcKitchenPrinter = null;

        try {
            if ((xmlRpcResponse != null) && (xmlRpcResponse instanceof XmlRpcStruct)) {
                XmlRpcStruct response = ((XmlRpcStruct) xmlRpcResponse);
                if ((!DataFunctions.isEmptyMap(response)) && (response.containsKey("responseCode")) && (((int) response.get("responseCode")) == 0)) {
                    if (response.get("response") instanceof XmlRpcStruct) {
                        XmlRpcStruct qcKitchenPrinterStruct = response.getStruct("response");
                        qcKitchenPrinter = new QCKitchenPrinter(qcKitchenPrinterStruct);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e, XRU_LOG);
            Logger.logMessage(String.format("There was a problem trying to parse the QCKitchenPrinter from the response " +
                    "received from the XML RPC call to %s in XmlRpcUtil.parseQCKitchenPrinterXmlRpcResponse",
                    Objects.toString(callingMethod, "NULL")), XRU_LOG, Logger.LEVEL.ERROR);
        }

        return qcKitchenPrinter;
    }

    /**
     * Builds the URL for a XML RPC client.
     *
     * @param macAddress {@link String} MAC address of the machine to hit the endpoint on.
     * @param hostname {@link String} Hostname of the machine to hit the endpoint on.
     * @return {@link String} URL for a XML RPC client.
     */
    public String buildURL (String macAddress, String hostname) {
        return (StringFunctions.stringHasContent(hostname) ? "http://" + hostname + "/oms/QuickCharge/XmlRpcManager" : "");
    }

}
