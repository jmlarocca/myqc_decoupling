package com.mmhayes.common.xmlapi;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-20 13:10:34 -0400 (Tue, 20 Apr 2021) $: Date of last commit
    $Rev: 13819 $: Revision of last commit
    Notes: Represents a XML RPC client.
*/

import redstone.xmlrpc.XmlRpcClient;

import java.net.MalformedURLException;
import java.net.URL;

public class MMHXmlRpcClient extends XmlRpcClient {

    // private member variables
    private String url;
    private int timeoutInMS;
    private String callerHostname;
    private String receiverHostname;

    /**
     * <p>Constructor for a {@link MMHXmlRpcClient}.</p>
     *
     * @param url The url {@link String} of the endpoint this client should reach out to.
     * @param streamMessages Whether or not to stream messages.
     * @param callerHostname The hostname {@link String} of the machine that is trying to invoke a method on another machine.
     * @param receiverHostname The hostname {@link String} of the machine that another machine is trying to invoke a method upon.
     * @throws MalformedURLException
     */
    public MMHXmlRpcClient (String url, boolean streamMessages, String callerHostname, String receiverHostname) throws MalformedURLException {
        super(url, streamMessages);
        this.url = url;
        this.callerHostname = callerHostname;
        this.receiverHostname = receiverHostname;
    }

    /**
     * <p>Constructor for a {@link MMHXmlRpcClient}.</p>
     *
     * @param url The url {@link String} of the endpoint this client should reach out to.
     * @param streamMessages Whether or not to stream messages.
     * @param timeoutInMS Timeout in milliseconds for the XML RPC client.
     * @param callerHostname The hostname {@link String} of the machine that is trying to invoke a method on another machine.
     * @param receiverHostname The hostname {@link String} of the machine that another machine is trying to invoke a method upon.
     * @throws MalformedURLException
     */
    public MMHXmlRpcClient (String url, boolean streamMessages, int timeoutInMS, String callerHostname, String receiverHostname) throws MalformedURLException {
        super(url, streamMessages, timeoutInMS);
        this.url = url;
        this.callerHostname = callerHostname;
        this.receiverHostname = receiverHostname;
    }

    /**
     * <p>Constructor for a {@link MMHXmlRpcClient}.</p>
     *
     * @param url The {@link URL} of the endpoint this client should reach out to.
     * @param streamMessages Whether or not to stream messages.
     * @param callerHostname The hostname {@link String} of the machine that is trying to invoke a method on another machine.
     * @param receiverHostname The hostname {@link String} of the machine that another machine is trying to invoke a method upon.
     */
    public MMHXmlRpcClient (URL url, boolean streamMessages, String callerHostname, String receiverHostname) {
        super(url, streamMessages);
        this.url = url.toString();
        this.callerHostname = callerHostname;
        this.receiverHostname = receiverHostname;
    }

    /**
     * <p>Constructor for a {@link MMHXmlRpcClient}.</p>
     *
     * @param url The {@link URL} of the endpoint this client should reach out to.
     * @param streamMessages Whether or not to stream messages.
     * @param timeoutInMS Timeout in milliseconds for the XML RPC client.  
     * @param callerHostname The hostname {@link String} of the machine that is trying to invoke a method on another machine.
     * @param receiverHostname The hostname {@link String} of the machine that another machine is trying to invoke a method upon.
     */
    public MMHXmlRpcClient (URL url, boolean streamMessages, int timeoutInMS, String callerHostname, String receiverHostname) {
        super(url, streamMessages, timeoutInMS);
        this.url = url.toString();
        this.timeoutInMS = timeoutInMS;
        this.callerHostname = callerHostname;
        this.receiverHostname = receiverHostname;
    }

    /**
     * <p>Getter for the url field of the {@link MMHXmlRpcClient}.</p>
     *
     * @return The url field of the {@link MMHXmlRpcClient}.
     */
    public String getURL () {
        return url;
    }

    /**
     * <p>Setter for the url field of the {@link MMHXmlRpcClient}.</p>
     *
     * @param url The url field of the {@link MMHXmlRpcClient}.
     */
    public void setURL (String url) {
        this.url = url;
    }

    /**
     * <p>Getter for the timeoutInMS field of the {@link MMHXmlRpcClient}.</p>
     *
     * @return The timeoutInMS field of the {@link MMHXmlRpcClient}.
     */
    public int getTimeoutInMS () {
        return timeoutInMS;
    }

    /**
     * <p>Setter for the timeoutInMS field of the {@link MMHXmlRpcClient}.</p>
     *
     * @param timeoutInMS The timeoutInMS field of the {@link MMHXmlRpcClient}.
     */
    public void setTimeoutInMS (int timeoutInMS) {
        this.timeoutInMS = timeoutInMS;
    }

    /**
     * <p>Getter for the callerHostname field of the {@link MMHXmlRpcClient}.</p>
     *
     * @return The callerHostname field of the {@link MMHXmlRpcClient}.
     */
    public String getCallerHostname () {
        return callerHostname;
    }

    /**
     * <p>Setter for the callerHostname field of the {@link MMHXmlRpcClient}.</p>
     *
     * @param callerHostname The callerHostname field of the {@link MMHXmlRpcClient}.
     */
    public void setCallerHostname (String callerHostname) {
        this.callerHostname = callerHostname;
    }

    /**
     * <p>Getter for the receiverHostname field of the {@link MMHXmlRpcClient}.</p>
     *
     * @return The receiverHostname field of the {@link MMHXmlRpcClient}.
     */
    public String getReceiverHostname () {
        return receiverHostname;
    }

    /**
     * <p>Setter for the receiverHostname field of the {@link MMHXmlRpcClient}.</p>
     *
     * @param receiverHostname The receiverHostname field of the {@link MMHXmlRpcClient}.
     */
    public void setReceiverHostname (String receiverHostname) {
        this.receiverHostname = receiverHostname;
    }

}