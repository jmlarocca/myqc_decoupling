package com.mmhayes.common.storeandforward.collections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.storeandforward.models.SAFTransactionModel;
import com.mmhayes.common.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nyu on 8/24/2017.
 */
public class SAFTransactionCollection
{
    @JsonIgnore private String logFileName = "SAF_Check.log";

    private List<SAFTransactionModel> safTransactionModels = new ArrayList<SAFTransactionModel>();
    
    public SAFTransactionCollection()
    {
    }

    public void getAllTransactionsForUpdating(String terminalIdStr) throws Exception
    {
        DataManager dm = new DataManager();
        int terminalId = Integer.parseInt(terminalIdStr);
        safTransactionModels.clear();
        
        try
        {
            ArrayList<HashMap> safLines = dm.parameterizedExecuteQuery("data.posanywhere.getSAFLinesForUpdating", new Object[]{new Integer(terminalId)}, logFileName, true);

            for (HashMap hm : safLines)
            {
                SAFTransactionModel safTxn = new SAFTransactionModel();
                safTxn.setPaTransSAFInfoId(hm.get("PATRANSSAFINFOID").toString());
                safTxn.setPaTransLineItemId(hm.get("PATRANSLINEITEMID").toString());
                safTxn.setTerminalId(hm.get("TERMINALID").toString());
                safTxn.setSafTransId(hm.get("SAFTRANSID").toString());
                safTxn.setClientMac(hm.get("MLCLIENTMAC").toString());
                safTxn.setCompanyCode(hm.get("CCCOMPANYCODE").toString());
                safTxn.setSiteCode(hm.get("CCSITECODE").toString());
                safTxn.setLaneName(hm.get("CCLANENAME").toString());
                safTxn.setTransLinkMac(hm.get("TRANSLINKMAC").toString());
                safTxn.setMlTerminalNum(hm.get("PACCTERMINALNUM").toString());

                safTransactionModels.add(safTxn);
            }
        }
        catch (Exception ex)
        {
            Logger.logException(ex, logFileName);
        }
    }

    //region Getters and Setters
    public List<SAFTransactionModel> getCollection() {
        return safTransactionModels;
    }

    public void setCollection(List<SAFTransactionModel> collection) {
        safTransactionModels = collection;
    }

    //endregion
}
