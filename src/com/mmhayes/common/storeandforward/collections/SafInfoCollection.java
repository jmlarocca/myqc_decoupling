package com.mmhayes.common.storeandforward.collections;

//MMHayes Dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.storeandforward.models.SafInfoModel;
import com.mmhayes.common.transaction.models.OtmModel;
import com.mmhayes.common.utils.Logger;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-07-09 08:21:22 -0400 (Mon, 09 Jul 2018) $: Date of last commit
 $Rev: 7390 $: Revision of last commit
*/
public class SafInfoCollection {
    private List<SafInfoModel> collection = new ArrayList<>();

    public static SafInfoCollection getAllTransByOtmId(OtmModel otmModel, Integer paymentProcessorId, String logFileName) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> safTransactionList = dm.parameterizedExecuteQuery("data.posapi30.getAllSafTransactions",
                new Object[]{otmModel.getId(), paymentProcessorId},
                logFileName,
                true
        );

        SafInfoCollection safInfoModels = new SafInfoCollection();

        for (HashMap safInfoHM : safTransactionList) {
            //CommonAPI.checkIsNullOrEmptyObject(safInfoHM.get("ID"), "SafInfo Id cannot be found.", terminalModel.getId());
            //create a new model and add to the collection
            safInfoModels.getCollection().add(SafInfoModel.createSafInfoModel(safInfoHM));
        }

        return safInfoModels;

    }

    public static List<SafInfoModel> updateSafTransactions(List<SafInfoModel> safInfoModels, String logFileName) throws Exception {
        DataManager dm = new DataManager();

        if (safInfoModels != null) {

            for (SafInfoModel safInfoModel : safInfoModels) {
                //update Saf record
                if (safInfoModel.getId() != null && !safInfoModel.getId().toString().isEmpty()) {

                    Integer iUpdatedRows = 0;
                    iUpdatedRows = dm.parameterizedExecuteNonQuery("data.posapi30.updateTransSafInfo", new Object[]{
                            safInfoModel.getId(),
                            safInfoModel.getOldCCTransInfo()

                    }, logFileName);

                    if (iUpdatedRows < 0) {
                        Logger.logMessage("Trans Saf Info could not be update", logFileName, Logger.LEVEL.ERROR);
                        throw new MissingDataException("Trans Saf Info could not be updated", safInfoModel.getTerminalId());
                    }
                }

                //update PATransline record
                if (safInfoModel.getLineItemId() != null && !safInfoModel.getLineItemId().toString().isEmpty()) {

                    Integer iUpdatedRows = 0;
                    iUpdatedRows = dm.parameterizedExecuteNonQuery("data.posapi30.updateTransLineCCInfo", new Object[]{
                            safInfoModel.getLineItemId(),
                            safInfoModel.getNewCCTransInfo(),
                            PosAPIHelper.CreditCardAuthType.APPROVED_ORIGINALLY_OFFLINE.toInt() //Update the CC Authorization Type to "Approved-Originally Offline"

                    }, logFileName);

                    if (iUpdatedRows < 0) {
                        Logger.logMessage("Trans Line CC Info could not be updated", logFileName, Logger.LEVEL.ERROR);

                        throw new MissingDataException("Trans Line CC Info not be update", safInfoModel.getTerminalId());
                    }
                }
            }
        } else {
            safInfoModels = new ArrayList<SafInfoModel>(); //if null is passed in, return a new list so it can be handled better by the client
        }

        return safInfoModels;

    }

    public List<SafInfoModel> getCollection() {
        return collection;
    }

    public void setCollection(List<SafInfoModel> collection) {
        this.collection = collection;
    }
}
