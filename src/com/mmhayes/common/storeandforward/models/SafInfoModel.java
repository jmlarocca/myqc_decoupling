package com.mmhayes.common.storeandforward.models;

//MMHayes Dependencies
import com.mmhayes.common.api.CommonAPI;

//Other Dependencies
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-06-28 16:46:46 -0400 (Thu, 28 Jun 2018) $: Date of last commit
 $Rev: 7349 $: Revision of last commit
*/
public class SafInfoModel {

    private Integer id = null;
    private Integer lineItemId = null;
    private Integer terminalId = null;

    private Integer paymentProcessorId = null;
    private String oldCCTransInfo = "";
    private String newCCTransInfo = "";
    private String safTransInfo= "";
    private String lastUpdatedDate = "";
    private String paymentProcessorName = "";
    private String fpTerminalId= "";
    private String fpStoreId= "";
    private String fpPortNum = "";
    private String fpUrl = "";

    public SafInfoModel(){

    }

    public SafInfoModel(HashMap safInfoHM){
        setModelProperties(safInfoHM);
    }

    //setter for all of this model's properties
    public SafInfoModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setLineItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSLINEITEMID")));
        setLastUpdatedDate(CommonAPI.convertModelDetailToString(modelDetailHM.get("UPDATEDDATETIME")));
        setTerminalId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALID")));
        setOldCCTransInfo(CommonAPI.convertModelDetailToString(modelDetailHM.get("OLDCCTRANSINFO")));
        setNewCCTransInfo(CommonAPI.convertModelDetailToString(modelDetailHM.get("NEWCCTRANSINFO")));
        setSafTransInfo(CommonAPI.convertModelDetailToString(modelDetailHM.get("SAFTRANSID")));

        setFpStoreId(CommonAPI.convertModelDetailToString(modelDetailHM.get("STORENUM")));
        setFpTerminalId(CommonAPI.convertModelDetailToString(modelDetailHM.get("TERMINALNUM")));
        setFpUrl(CommonAPI.convertModelDetailToString(modelDetailHM.get("URL")));
        setFpPortNum(CommonAPI.convertModelDetailToString(modelDetailHM.get("PORTNUM")));
        setPaymentProcessorId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAYMENTPROCESSORID")));
        setPaymentProcessorName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTPROCESSORNAME")));

        return this;
    }

    public static SafInfoModel createSafInfoModel(HashMap safInfoHM) throws Exception {
        return new SafInfoModel(safInfoHM);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLineItemId() {
        return lineItemId;
    }

    public void setLineItemId(Integer lineItemId) {
        this.lineItemId = lineItemId;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public String getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(String lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getOldCCTransInfo() {
        return oldCCTransInfo;
    }

    public void setOldCCTransInfo(String oldCCTransInfo) {
        this.oldCCTransInfo = oldCCTransInfo;
    }

    public String getNewCCTransInfo() {
        return newCCTransInfo;
    }

    public void setNewCCTransInfo(String newCCTransInfo) {
        this.newCCTransInfo = newCCTransInfo;
    }

    public String getSafTransInfo() {
        return safTransInfo;
    }

    public void setSafTransInfo(String safTransInfo) {
        this.safTransInfo = safTransInfo;
    }

    public String getFpTerminalId() {
        return fpTerminalId;
    }

    public void setFpTerminalId(String fpTerminalId) {
        this.fpTerminalId = fpTerminalId;
    }

    public String getFpStoreId() {
        return fpStoreId;
    }

    public void setFpStoreId(String fpStoreId) {
        this.fpStoreId = fpStoreId;
    }

    public String getFpPortNum() {
        return fpPortNum;
    }

    public void setFpPortNum(String fpPortNum) {
        this.fpPortNum = fpPortNum;
    }

    public String getFpUrl() {
        return fpUrl;
    }

    public void setFpUrl(String fpUrl) {
        this.fpUrl = fpUrl;
    }

    public Integer getPaymentProcessorId() {
        return paymentProcessorId;
    }

    public void setPaymentProcessorId(Integer paymentProcessorId) {
        this.paymentProcessorId = paymentProcessorId;
    }

    public String getPaymentProcessorName() {
        return paymentProcessorName;
    }

    public void setPaymentProcessorName(String paymentProcessorName) {
        this.paymentProcessorName = paymentProcessorName;
    }
}
