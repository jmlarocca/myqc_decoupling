package com.mmhayes.common.storeandforward.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;

/**
 * Created by nyu on 8/24/2017.
 */
public class SAFTransactionModel
{
    private String paTransSAFInfoId = "";
    private String paTransLineItemId = "";
    private String terminalId = "";
    private String safTransId = "";
    @JsonInclude(JsonInclude.Include.NON_NULL) private String updatedDateTime = "";
    @JsonInclude(JsonInclude.Include.NON_NULL) private String oldCCTransInfo = "";
    private String newCCTransInfo = "";

    private String clientMac = "";
    private String companyCode = "";
    private String siteCode = "";
    private String laneName = "";
    private String transLinkMac = "";
    private String mlTerminalNum = "";

    @JsonIgnore private String logFileName = "SAF_Check.log";

    public SAFTransactionModel()
    {
    }

    public int updateDBWithNewAuthInfo()
    {
        int result = -1;
        
        try
        {
            Logger.logMessage("Updating CreditCardTransInfo for PATransLineItemID " + paTransSAFInfoId + ", PATransSAFInfoID " + paTransSAFInfoId, logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("New CreditCardTransInfo value: " + newCCTransInfo, logFileName, Logger.LEVEL.DEBUG);
            Logger.logMessage("Old CreditCardTransInfo value: " + oldCCTransInfo, logFileName, Logger.LEVEL.DEBUG);
            
            DataManager dm = new DataManager();

            // Get original CreditCardTransInfo value
            Object oldCCTransInfoObj = dm.parameterizedExecuteScalar("data.posanywhere.getCreditCardTransInfoForPATransLineItem", new Object[] {paTransLineItemId}, logFileName);

            if (oldCCTransInfoObj != null)
            {
                // Update PATransSAFInfo record with original CreditCardTransInfo value
                // Update PATransLineItem record with new authorization details
                result = dm.parameterizedExecuteNonQuery("data.posanywhere.updateNewAuthorizationDetails",
                        new Object[]{
                            oldCCTransInfoObj.toString(),
                            new Integer(paTransSAFInfoId),
                            newCCTransInfo,
                            new Integer(paTransLineItemId)},
                            logFileName);

                if (result >= 0)
                {
                    Logger.logMessage("Successfully updated authorization details for PATransLineItemID " + paTransLineItemId, logFileName, Logger.LEVEL.DEBUG);
                }
            }
            else
            {
                Logger.logMessage("Could not retrieve original CreditCardTransInfo value for PATransLineItemID " + paTransSAFInfoId, logFileName, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception ex)
        {
            Logger.logException(ex, logFileName);
            Logger.logMessage("Exception caught in updateDBWithNewAuthInfo", Logger.LEVEL.ERROR);
        }

        return result;
    }
    
    //region Getters and Setters
    public String getPaTransSAFInfoId() {
        return paTransSAFInfoId;
    }

    public void setPaTransSAFInfoId(String paTransSAFInfoId) {
        this.paTransSAFInfoId = paTransSAFInfoId;
    }

    public String getPaTransLineItemId() {
        return paTransLineItemId;
    }

    public void setPaTransLineItemId(String paTransLineItemId) {
        this.paTransLineItemId = paTransLineItemId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getSafTransId() {
        return safTransId;
    }

    public void setSafTransId(String safTransId) {
        this.safTransId = safTransId;
    }

    public String getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(String updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public String getOldCCTransInfo() {
        return oldCCTransInfo;
    }

    public void setOldCCTransInfo(String oldCCTransInfo) {
        this.oldCCTransInfo = oldCCTransInfo;
    }

    public String getClientMac() {
        return clientMac;
    }

    public void setClientMac(String clientMac) {
        this.clientMac = clientMac;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    public String getLaneName() {
        return laneName;
    }

    public void setLaneName(String laneName) {
        this.laneName = laneName;
    }

    public String getTransLinkMac() {
        return transLinkMac;
    }

    public void setTransLinkMac(String transLinkMac) {
        this.transLinkMac = transLinkMac;
    }

    public String getMlTerminalNum() {
        return mlTerminalNum;
    }

    public void setMlTerminalNum(String mlTerminalNum) {
        this.mlTerminalNum = mlTerminalNum;
    }

    public String getNewCCTransInfo() {
        return newCCTransInfo;
    }

    public void setNewCCTransInfo(String newCCTransInfo) {
        this.newCCTransInfo = newCCTransInfo;
    }

    //endregion

}
