package com.mmhayes.common.funding.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

//funding dependencies
//other dependencies

/* Account Payment Method Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-12-27 10:00:27 -0500 (Fri, 27 Dec 2019) $: Date of last commit
 $Rev: 10348 $: Revision of last commit

 Notes: Model that contains information for a single payment method (such as a credit card) for an account
*/
public class AccountPaymentMethodModel {
    Integer id = null; //identifier of model - QC_EmployeePaymentMethod.EmployeePaymentMethodID
    Integer accountID = null; //identifier of the account (employee) that this payment method is for - QC_Employees.EmployeeID
    Integer paymentMethodTypeID = null; //ID of the type of this model - QC_PaymentMethodType.PaymentMethodTypeID
    Integer paymentProcessorID = null; //TODO: Replace with PaymentProcessorModel //ID of the processor who will "process" the payment method - such as "Stripe" or "First Data"
    Integer failedCardAttempts = null; //the number of times a payment method has been attempted and failed
    Integer failedCardRefundAttempts = null; //the number of times a payment method REFUND has been attempted and failed
    String personName = null; //the name of the person or employee for recording Funding PATransactions
    Integer paSurchargeID = null; //the surcharge ID set on the funding terminal for recording fees
    String surchargeName = null; //name of the surcharge for recording credit card fee
    String paymentMethodType = null; //name of the type of the model - QC_PaymentMethodType.Name
    String paymentProcessor = null; //TODO: Replace with PaymentProcessorModel //name of the processor who will "process" the payment method - such as "Stripe" or "First Data"
    String APIURL = null; //TODO: Replace with PaymentProcessorModel //URL end point to process the payment method - like Stripe API URL
    String publicAPIKey = null; //TODO: Replace with PaymentProcessorModel //processor API key that can be shared publicly
    String paymentPageFileName = null; //TODO: Replace with PaymentProcessorModel //name (and GET params) of file that will be provided from the MMHayes Payment Gateway (related to PCI compliance!)
    String processorPaymentToken = null; //reusable token that represents a payment method - used by the Payment Processor for charging with the actual numbers!
    String processorPaymentOneTimeToken = null; //one time use token from processor to process payment method - such as "FDToken" or "Stripe One Time Token"
    String paymentMethodFullName = null; //full name of the Payment Method Holder (such as John R. Smith)
    String paymentMethodEmailAddress = null; //email address of the Payment Method Holder (such as jrsmithrox2017@gmail.com)
    String paymentMethodLastFour = null; //last four digits of payment method (this data is SAFE to store)
    String paymentMethodExpirationMonth = null; //expiration month of payment method (this data is SAFE to store)
    String paymentMethodExpirationYear = null; //expiration year of payment method (this data is SAFE to store)
    String paymentMethodZipCode = null; //zip code that payment method resides in (this data is SAFE to store)
    Boolean modelIsDefault = false; //is this the default payment method on file for the QC Account? (NOTE: In 8.3.1/1.2 there can ONLY be one payment method -jrmitaly 2/14/2017)
    Boolean active = true; //is this payment method available for use?
    Boolean archived = true; //is this payment method not available for charges or refunds
    Boolean allowPartialCCAuth = false; //is the terminal able to do partial auth
    String fundingMethod = null; //name of the method used, either manual or automatic funding (one-time)
    Integer personID = null; //ID of the person account for this payment method
    String fundingFeeLabel = null; //label for what the fee for funding is called
    String fundingFeeDisclaimer = null; //additional disclaimer to show when fees are turned on for funding
    Integer fundingFeeCalculationMethodID = null; //how the fee should be calculated
    BigDecimal fundingFlatFeeAmount = null; //the flat amount used for calculating a funding fee
    BigDecimal fundingPercentageFeeAmount = null; //the percentage amount used for calculating a funding fee
    BigDecimal fundingFee = null; // the fee charged for a funding payment
    boolean terminalHasFundingFee = false; //boolean to represent if there is a funding fee
    @JsonIgnore private String privateAPIKey = null; //TODO: Replace with PaymentProcessorModel //private API key provided by payment processor that should only be accessible to the payment processor itself
    @JsonIgnore private String paymentProcessorStoreNum = null;
    @JsonIgnore private String paymentProcessorTerminalNum = null;
    @JsonIgnore private static DataManager dm = new DataManager();

    //default constructor - allows for serializing JSON to Java
    public AccountPaymentMethodModel() {

    }

    //if passed the ID for this model, then simply query the information and set it
    public AccountPaymentMethodModel(Integer employeePaymentMethodID) throws Exception {
        this(employeePaymentMethodID, false);
    }

    //for getting all the details related to an
    public AccountPaymentMethodModel(Integer employeePaymentMethodID, Integer verifiedAccountID) throws Exception {
        this(employeePaymentMethodID, verifiedAccountID, false);
    }

    //if passed the ID for this model, then simply query the information and set it
    public AccountPaymentMethodModel(Integer employeePaymentMethodID, boolean checkDelayAutoFundingDTM) throws Exception {
        populateModel(employeePaymentMethodID, checkDelayAutoFundingDTM);
    }

    //for getting all the details related to an
    public AccountPaymentMethodModel(Integer employeePaymentMethodID, Integer verifiedAccountID, boolean checkDelayAutoFundingDTM) throws Exception {
        populateModel(employeePaymentMethodID, verifiedAccountID, checkDelayAutoFundingDTM);
    }

    //constructor - takes hashmap that get sets to this models properties
    public AccountPaymentMethodModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public AccountPaymentMethodModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("EMPLOYEEPAYMENTMETHODID")));

        setAccountID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("EMPLOYEEID")));

        setPaymentProcessorID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAYMENTPROCESSORID")));

        setPaymentMethodTypeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAYMENTMETHODTYPEID")));

        setPaymentMethodType(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTMETHODTYPE")));

        setPaymentProcessor(CommonAPI.convertModelDetailToString(modelDetailHM.get("PROCESSORNAME")));

        setAPIURL(CommonAPI.convertModelDetailToString(modelDetailHM.get("APIURL")));

        setPublicAPIKey(CommonAPI.convertModelDetailToString(modelDetailHM.get("PUBLICAPIKEY")));

        setPaymentPageFileName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTPAGEFILENAME")));

        setProcessorPaymentToken(CommonAPI.convertModelDetailToString(modelDetailHM.get("PROCESSORPAYMENTTOKEN")));

        setPaymentMethodFullName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTMETHODFULLNAME")));

        setPaymentMethodEmailAddress(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTMETHODEMAILADDRESS")));

        setPaymentMethodLastFour(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTMETHODLASTFOUR")));

        setPaymentMethodExpirationMonth(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTMETHODEXPIRATIONMONTH")));

        setPaymentMethodExpirationYear(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTMETHODEXPIRATIONYEAR")));

        setPaymentMethodZipCode(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTMETHODZIPCODE")));

        setModelIsDefault(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ISDEFAULT")));

        setPrivateAPIKey(CommonAPI.convertModelDetailToString(modelDetailHM.get("PRIVATEAPIKEY")));

        setPaymentProcessorStoreNum(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTPROCESSORSTORENUM")));

        setPaymentProcessorTerminalNum(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTPROCESSORTERMINALNUM")));

        setFailedCardAttempts(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("FAILEDCARDRELATEDATTEMPT")));

        setFailedCardRefundAttempts(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("FAILEDCARDREFUNDATTEMPT")));

        setActive(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ACTIVE")));

        setArchived(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ARCHIVED")));

        setPersonID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PERSONID")));

        setFundingFlatFeeAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("FUNDINGFLATFEEAMOUNT")));

        setFundingPercentageFeeAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("FUNDINGPERCENTAGEFEEAMOUNT")));

        setFundingFeeLabel(CommonAPI.convertModelDetailToString(modelDetailHM.get("FUNDINGFEELABEL")));

        setFundingFeeDisclaimer(CommonAPI.convertModelDetailToString(modelDetailHM.get("FUNDINGFEEDISCLAIMER")));

        setFundingFeeCalculationMethodID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("FUNDINGFEECALCULATIONMETHODID")));

        setPersonName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PERSONNAME")));

        setPASurchargeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASURCHARGEID")));

        setSurchargeName(CommonAPI.convertModelDetailToString(modelDetailHM.get("SURCHARGENAME")));

        boolean hasFundingFee = modelDetailHM.containsKey("TERMINALHASFUNDINGFEE") && CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALHASFUNDINGFEE")) == 1;
        setTerminalHasFundingFee(hasFundingFee);

        return this;
    }

    //populates this model by querying the database
    private AccountPaymentMethodModel populateModel(Integer employeePaymentMethodID, boolean checkDelayAutoFundingDTM) throws Exception {
        LocalDateTime currentTime = LocalDateTime.now();
        String currentTimeOfDayParam = currentTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));

        //get all models in an array list
        ArrayList<HashMap> employeePaymentMethodList = dm.parameterizedExecuteQuery("data.common.funding.getEmployeePaymentMethod",
                new Object[]{
                    employeePaymentMethodID,
                    currentTimeOfDayParam
                },
                true
        );

        //employee payment method list should only contain one record which we will use to populate this model
        if (employeePaymentMethodList != null && employeePaymentMethodList.size() == 1) {
            HashMap modelDetailHM = employeePaymentMethodList.get(0);
            validatePaymentMethod(modelDetailHM, checkDelayAutoFundingDTM);
        } else {
            throw new MissingDataException();
        }

        return this;
    }

    //populates this model by querying the database
    private AccountPaymentMethodModel populateModel(Integer employeePaymentMethodID, Integer verifiedAccountID, boolean checkDelayAutoFundingDTM) throws Exception {
        LocalDateTime currentTime = LocalDateTime.now();
        String currentTimeOfDayParam = currentTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));

        //get all models in an array list
        ArrayList<HashMap> employeePaymentMethodList = dm.parameterizedExecuteQuery("data.common.funding.getPersonPaymentMethod",
                new Object[]{
                        employeePaymentMethodID,
                        verifiedAccountID,
                        currentTimeOfDayParam
                },
                true
        );

        //employee payment method list should only contain one record which we will use to populate this model
        if (employeePaymentMethodList != null && employeePaymentMethodList.size() == 1) {
            HashMap modelDetailHM = employeePaymentMethodList.get(0);
            validatePaymentMethod(modelDetailHM, checkDelayAutoFundingDTM);
        } else {
            throw new MissingDataException();
        }

        return this;
    }

    private AccountPaymentMethodModel validatePaymentMethod(HashMap modelDetailHM, boolean checkDelayAutoFundingDTM) throws Exception  {
        Integer delayFunding = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DELAYAUTOFUNDING"));

        //if the value of DelayAutoFundingUntilDTM is after now,
        if ( checkDelayAutoFundingDTM && delayFunding == 1 ) {
            return this;
        }

        setModelProperties(modelDetailHM);

        return this;
    }

    public AccountPaymentMethodModel calculatePaymentFee(BigDecimal fundingAmount) throws Exception {
        //if funding is not ON (no fee amounts set) return null, should always be 0, but also check for nulls
        if ( !getTerminalHasFundingFee() ) {
            return this;
        }

        BigDecimal feeAmount;
        BigDecimal percentageAsDecimal = getFundingPercentageFeeAmount().setScale(4, BigDecimal.ROUND_HALF_UP).divide( new BigDecimal("100").setScale(4, BigDecimal.ROUND_UNNECESSARY), BigDecimal.ROUND_HALF_UP );

        //for option 1, the formula is Fee = ( FundingAmount * PercentFeeAsDecimal ) + FlatFee
        if ( getFundingFeeCalculationMethodID() == 1 ) {
            feeAmount = ( fundingAmount.multiply( percentageAsDecimal ) ).setScale(4, BigDecimal.ROUND_HALF_UP).add( getFundingFlatFeeAmount() );
        } else {
            //for option 2, the formula is Fee = ( ( FundingAmount + FlatFee ) / ( 1 - PercentFeeAsDecimal ) ) - FundingAmount
            BigDecimal percentageForCalc = BigDecimal.ONE.subtract( percentageAsDecimal ).setScale(4, BigDecimal.ROUND_HALF_UP);
            BigDecimal fundingAmountAndFee = ( fundingAmount.add( getFundingFlatFeeAmount() ) ).setScale(4, BigDecimal.ROUND_HALF_UP).divide(percentageForCalc, BigDecimal.ROUND_HALF_UP).setScale(4, BigDecimal.ROUND_HALF_UP);
            feeAmount = fundingAmountAndFee.subtract( fundingAmount );
        }

        setFundingFee( feeAmount.setScale(2, BigDecimal.ROUND_HALF_UP) );

        return this;
    }

    //update the payment processor details if using the online ordering terminal's merchant account
    public void updatePaymentMethodDetails(String publicAPIKey, String privateAPIKey, String paymentProcessorStoreNum, String paymentProcessorTerminalNum) {

        //Stripe
        if(this.getPaymentProcessorID().equals(1)) {
            this.setPublicAPIKey(publicAPIKey);
            this.setPrivateAPIKey(privateAPIKey);

        //FreedomPay
        } else if (this.getPaymentProcessorID().equals(6)) {
            this.setPaymentProcessorStoreNum(paymentProcessorStoreNum);
            this.setPaymentProcessorTerminalNum(paymentProcessorTerminalNum);
        }
    }

    //getter for the privateAPIKey - "normal" getter is not used because we never want the private API key serialized to JSON!
    protected String retrievePrivateAPIKeySafely(){
        return privateAPIKey;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountID() {
        return accountID;
    }

    public void setAccountID(Integer accountID) {
        this.accountID = accountID;
    }

    public Integer getPaymentMethodTypeID() {
        return paymentMethodTypeID;
    }

    public void setPaymentMethodTypeID(Integer paymentMethodTypeID) {
        this.paymentMethodTypeID = paymentMethodTypeID;
    }

    public Integer getPaymentProcessorID() {
        return paymentProcessorID;
    }

    public void setPaymentProcessorID(Integer paymentProcessorID) {
        this.paymentProcessorID = paymentProcessorID;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getPaymentProcessor() {
        return paymentProcessor;
    }

    public void setPaymentProcessor(String paymentProcessor) {
        this.paymentProcessor = paymentProcessor;
    }

    public String getAPIURL() {
        return APIURL;
    }

    public void setAPIURL(String APIURL) {
        this.APIURL = APIURL;
    }

    public String getPublicAPIKey() {
        return publicAPIKey;
    }

    public void setPublicAPIKey(String publicAPIKey) {
        this.publicAPIKey = publicAPIKey;
    }

    public String getPaymentPageFileName() {
        return paymentPageFileName;
    }

    public void setPaymentPageFileName(String paymentPageFileName) {
        this.paymentPageFileName = paymentPageFileName;
    }

    public String getProcessorPaymentToken() {
        return processorPaymentToken;
    }

    public void setProcessorPaymentToken(String processorPaymentToken) {
        this.processorPaymentToken = processorPaymentToken;
    }

    public String getProcessorPaymentOneTimeToken() {
        return processorPaymentOneTimeToken;
    }

    public void setProcessorPaymentOneTimeToken(String processorPaymentOneTimeToken) {
        this.processorPaymentOneTimeToken = processorPaymentOneTimeToken;
    }

    public String getPaymentMethodFullName() {
        return paymentMethodFullName;
    }

    public void setPaymentMethodFullName(String paymentMethodFullName) {
        this.paymentMethodFullName = paymentMethodFullName;
    }

    public String getPaymentMethodEmailAddress() {
        return paymentMethodEmailAddress;
    }

    public void setPaymentMethodEmailAddress(String paymentMethodEmailAddress) {
        this.paymentMethodEmailAddress = paymentMethodEmailAddress;
    }

    public String getPaymentMethodLastFour() {
        return paymentMethodLastFour;
    }

    public void setPaymentMethodLastFour(String paymentMethodLastFour) {
        this.paymentMethodLastFour = paymentMethodLastFour;
    }

    public String getPaymentMethodExpirationMonth() {
        return paymentMethodExpirationMonth;
    }

    public void setPaymentMethodExpirationMonth(String paymentMethodExpirationMonth) {
        this.paymentMethodExpirationMonth = paymentMethodExpirationMonth;
    }

    public String getPaymentMethodExpirationYear() {
        return paymentMethodExpirationYear;
    }

    public void setPaymentMethodExpirationYear(String paymentMethodExpirationYear) {
        this.paymentMethodExpirationYear = paymentMethodExpirationYear;
    }

    public String getPaymentMethodZipCode() {
        return paymentMethodZipCode;
    }

    public void setPaymentMethodZipCode(String paymentMethodZipCode) {
        this.paymentMethodZipCode = paymentMethodZipCode;
    }

    public Boolean getModelIsDefault() {
        return modelIsDefault;
    }

    public void setModelIsDefault(Boolean modelIsDefault) {
        this.modelIsDefault = modelIsDefault;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    private String getPrivateAPIKey() {
        return privateAPIKey;
    }

    private void setPrivateAPIKey(String privateAPIKey) {
        this.privateAPIKey = privateAPIKey;
    }

    public String getFundingMethod() {
        return fundingMethod;
    }

    public void setFundingMethod(String fundingMethod) {
        this.fundingMethod = fundingMethod;
    }

    public Integer getFailedCardAttempts() {
        return failedCardAttempts;
    }

    public void setFailedCardAttempts(Integer failedCardAttempts) {
        this.failedCardAttempts = failedCardAttempts;
    }

    protected String getPaymentProcessorTerminalNum() {
        return paymentProcessorTerminalNum;
    }

    private void setPaymentProcessorTerminalNum(String paymentProcessorTerminalNum) {
        this.paymentProcessorTerminalNum = paymentProcessorTerminalNum;
    }

    protected String getPaymentProcessorStoreNum() {
        return paymentProcessorStoreNum;
    }

    private void setPaymentProcessorStoreNum(String paymentProcessorStoreNum) {
        this.paymentProcessorStoreNum = paymentProcessorStoreNum;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    public String getFundingFeeLabel() {
        return fundingFeeLabel;
    }

    public void setFundingFeeLabel(String fundingFeeLabel) {
        this.fundingFeeLabel = fundingFeeLabel;
    }

    public String getFundingFeeDisclaimer() {
        return fundingFeeDisclaimer;
    }

    public void setFundingFeeDisclaimer(String fundingFeeDisclaimer) {
        this.fundingFeeDisclaimer = fundingFeeDisclaimer;
    }

    public Integer getFundingFeeCalculationMethodID() {
        return fundingFeeCalculationMethodID;
    }

    public void setFundingFeeCalculationMethodID(Integer fundingFeeCalculationMethodID) {
        this.fundingFeeCalculationMethodID = fundingFeeCalculationMethodID;
    }

    public BigDecimal getFundingFlatFeeAmount() {
        return fundingFlatFeeAmount;
    }

    public void setFundingFlatFeeAmount(BigDecimal fundingFlatFeeAmount) {
        this.fundingFlatFeeAmount = fundingFlatFeeAmount;
    }

    public BigDecimal getFundingPercentageFeeAmount() {
        return fundingPercentageFeeAmount;
    }

    public void setFundingPercentageFeeAmount(BigDecimal fundingPercentageFeeAmount) {
        this.fundingPercentageFeeAmount = fundingPercentageFeeAmount;
    }

    public boolean getTerminalHasFundingFee() {
        return terminalHasFundingFee;
    }

    public void setTerminalHasFundingFee(boolean terminalHasFundingFee) {
        this.terminalHasFundingFee = terminalHasFundingFee;
    }

    public BigDecimal getFundingFee() {
        return fundingFee;
    }

    public void setFundingFee(BigDecimal fundingFee) {
        this.fundingFee = fundingFee;
    }

    public Integer getFailedCardRefundAttempts() {
        return failedCardRefundAttempts;
    }

    public void setFailedCardRefundAttempts(Integer failedCardRefundAttempts) {
        this.failedCardRefundAttempts = failedCardRefundAttempts;
    }

    public Boolean getArchived() {
        return archived;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public Integer getPASurchargeID() {
        return paSurchargeID;
    }

    public void setPASurchargeID(Integer paSurchargeID) {
        this.paSurchargeID = paSurchargeID;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getSurchargeName() {
        return surchargeName;
    }

    public void setSurchargeName(String surchargeName) {
        this.surchargeName = surchargeName;
    }

    public Boolean getAllowPartialCCAuth() {
        return allowPartialCCAuth;
    }

    public void setAllowPartialCCAuth(Boolean allowPartialCCAuth) {
        this.allowPartialCCAuth = allowPartialCCAuth;
    }

}
