package com.mmhayes.common.funding.models;

//mmhayes dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;

//funding dependencies
import com.mmhayes.common.funding.collections.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.*;

/* Payment Processor Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-12-04 17:17:16 -0500 (Wed, 04 Dec 2019) $: Date of last commit
 $Rev: 10208 $: Revision of last commit

 Notes: Model that contains information for a single payment processor
*/
public class PaymentProcessorModel {
    Integer id = null; //identifier of model - QC_EmployeePaymentMethod.EmployeePaymentMethodID
    Integer paSurchargeID = null; //the surcharge ID set on the funding terminal for recording fees
    Integer receivedOnAccountsID = null; //the roa ID set on the funding terminal for recording Funding PATransactions
    Integer creditCardTenderID = null; //the credit card tender ID set on the funding terminal for recording Funding PATransactions
    String name = null; //name of the processor who will "process" the payment method - such as "Stripe" or "First Data"
    String APIURL = null; //URL end point to process the payment method - like Stripe API URL
    String publicAPIKey = null; //processor API key that can be shared publicly
    String paymentPageFileName = null; //name (and GET params) of file that will be provided from the MMHayes Payment Gateway (related to PCI compliance!)
    String paymentProcessorStoreNum = null; //name (and GET params) of file that will be provided from the MMHayes Payment Gateway (related to PCI compliance!)
    String paymentProcessorTerminalNum = null; //name (and GET params) of file that will be provided from the MMHayes Payment Gateway (related to PCI compliance!)
    @JsonIgnore private String privateAPIKey = null; //private API key provided by payment processor that should only be accessible to the payment processor itself
    @JsonIgnore private static DataManager dm = new DataManager();
    @JsonIgnore private Integer loggedInEmployeeID = null;

    //default constructor - allows for serializing JSON to Java
    public PaymentProcessorModel() {

    }

    //constructor - takes hashmap that get sets to this models properties
    public PaymentProcessorModel(Integer authenticatedEmployeeID) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(authenticatedEmployeeID);

        //populate the model
        populateModel();
    }

    //constructor - takes hashmap that get sets to this models properties
    public PaymentProcessorModel(HttpServletRequest request) throws Exception {

        if ( CommonAuthResource.isKOA( request ) ) {
            setLoggedInEmployeeID(CommonAuthResource.getAccountIDFromAccountHeader(request));
        } else {
            //determine the authenticated account ID from the request
            setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));
        }

        //populate the model
        populateModel();
    }

    //setter for all of this model's properties
    public PaymentProcessorModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAYMENTPROCESSORID")));

        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        setAPIURL(CommonAPI.convertModelDetailToString(modelDetailHM.get("APIURL")));

        setPublicAPIKey(CommonAPI.convertModelDetailToString(modelDetailHM.get("PUBLICAPIKEY")));

        setPaymentPageFileName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTPAGEFILENAME")));

        setPrivateAPIKey(CommonAPI.convertModelDetailToString(modelDetailHM.get("PRIVATEAPIKEY")));

        setPaymentProcessorTerminalNum(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTPROCESSORTERMINALNUM")));

        setPaymentProcessorStoreNum(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTPROCESSORSTORENUM")));

        setPASurchargeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASURCHARGEID")));

        setReceivedOnAccountsID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("FUNDINGPARECEIVEDACCTID")));

        setCreditCardTenderID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("CCTENDERID")));

        return this;
    }

    //populates this model by querying the database
    //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
    private PaymentProcessorModel populateModel() throws Exception {

        //throw InvalidAuthException if no authenticated employee is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> paymentProcessorList = dm.parameterizedExecuteQuery("data.common.funding.getPaymentProcessorForSingleMyQCTerminal",
            new Object[]{
                getLoggedInEmployeeID()
            },
            true
        );

        //automatic funding list should only contain one account which we will use to populate this model
        if (paymentProcessorList != null && paymentProcessorList.size() == 1) {
            HashMap modelDetailHM = paymentProcessorList.get(0);
            setModelProperties(modelDetailHM);
        } else {
            throw new MissingDataException();
        }

        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAPIURL() {
        return APIURL;
    }

    public void setAPIURL(String APIURL) {
        this.APIURL = APIURL;
    }

    public String getPublicAPIKey() {
        return publicAPIKey;
    }

    public void setPublicAPIKey(String publicAPIKey) {
        this.publicAPIKey = publicAPIKey;
    }

    public String getPaymentPageFileName() {
        return paymentPageFileName;
    }

    public void setPaymentPageFileName(String paymentPageFileName) {
        this.paymentPageFileName = paymentPageFileName;
    }

    private String getPrivateAPIKey() {
        return privateAPIKey;
    }

    private void setPrivateAPIKey(String privateAPIKey) {
        this.privateAPIKey = privateAPIKey;
    }

    private Integer getLoggedInEmployeeID() {
        return loggedInEmployeeID;
    }

    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

    public String getPaymentProcessorTerminalNum() {
        return paymentProcessorTerminalNum;
    }

    public void setPaymentProcessorTerminalNum(String paymentProcessorTerminalNum) {
        this.paymentProcessorTerminalNum = paymentProcessorTerminalNum;
    }

    public String getPaymentProcessorStoreNum() {
        return paymentProcessorStoreNum;
    }

    public void setPaymentProcessorStoreNum(String paymentProcessorStoreNum) {
        this.paymentProcessorStoreNum = paymentProcessorStoreNum;
    }

    public Integer getReceivedOnAccountsID() {
        return receivedOnAccountsID;
    }

    public void setReceivedOnAccountsID(Integer receivedOnAccountsID) {
        this.receivedOnAccountsID = receivedOnAccountsID;
    }

    public Integer getCreditCardTenderID() {
        return creditCardTenderID;
    }

    public void setCreditCardTenderID(Integer creditCardTenderID) {
        this.creditCardTenderID = creditCardTenderID;
    }

    public Integer getPASurchargeID() {
        return paSurchargeID;
    }

    public void setPASurchargeID(Integer paSurchargeID) {
        this.paSurchargeID = paSurchargeID;
    }
}

