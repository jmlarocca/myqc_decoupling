package com.mmhayes.common.funding.models;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;

//funding dependencies
import com.mmhayes.common.funding.collections.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.*;

/* Account Auto Funding Model
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2018-08-17 09:42:07 -0400 (Fri, 17 Aug 2018) $: Date of last commit
 $Rev: 7647 $: Revision of last commit

 Notes: Contains automatic funding information for a single account
*/
public class AccountAutoFundingModel {
    BigDecimal reloadAmount = null; //the amount that should be automatically reloaded when the reload threshold is reached
    BigDecimal reloadThreshold = null; //the amount when reached or below would iniate an automatic reload
    Integer reloadPaymentMethodID = null; //the payment method id that the account is using for auto funding
    Integer reloadSetUpByPersonID = null; //who set up the auto funding for the account
    String reloadSetUpByName = ""; //who set up the auto funding for the account
    @JsonIgnore public String reloadToken = null;  //not used by My QC - used by "legacy" funding systems - needs to be handled accordingly
    @JsonIgnore private Integer reloadTerminalID = null; //not used by My QC - used by "legacy" funding systems - needs to be handled accordingly
    @JsonIgnore private static DataManager dm = new DataManager();
    @JsonIgnore private Integer loggedInEmployeeID = null;

    //default constructor - allows for serializing JSON to Java
    public AccountAutoFundingModel() {

    }

    //constructor - used to verify authentication and then populates this model
    public AccountAutoFundingModel(HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate the model
        populateModel();

    }

    //constructor - sets the employeeID and then populates this model
    public AccountAutoFundingModel(Integer employeeID) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(employeeID);

        //populate the model
        populateModel();

    }

    //setter for all of this model's properties
    public AccountAutoFundingModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setReloadAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("RELOADAMOUNT")));

        setReloadThreshold(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("RELOADTHRESHOLD")));

        setReloadToken(CommonAPI.convertModelDetailToString(modelDetailHM.get("RELOADTOKEN")));

        setReloadTerminalID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("RELOADTERMINALID")));

        setReloadPaymentMethodID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("RELOADPAYMENTMETHODID")));

        setReloadSetUpByPersonID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("RELOADSETBY")));

        setReloadSetUpByName(CommonAPI.convertModelDetailToString(modelDetailHM.get("RELOADSETBYNAME").toString()));

        return this;
    }

    //populates this model by querying the database
    private AccountAutoFundingModel populateModel() throws Exception {

        //throw InvalidAuthException if no authenticated employee is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> accountAutoFundingList = dm.parameterizedExecuteQuery("data.common.funding.getAccountAutoFundingInfo",
                new Object[]{
                    getLoggedInEmployeeID()
                },
                true
        );

        //automatic funding list should only contain one account which we will use to populate this model
        if (accountAutoFundingList != null && accountAutoFundingList.size() == 1) {
            HashMap modelDetailHM = accountAutoFundingList.get(0);
            setModelProperties(modelDetailHM);
        } else {
            throw new MissingDataException();
        }

        return this;
    }

    public BigDecimal getReloadAmount() {
        return reloadAmount;
    }

    public void setReloadAmount(BigDecimal reloadAmount) {
        this.reloadAmount = reloadAmount;
    }

    public BigDecimal getReloadThreshold() {
        return reloadThreshold;
    }

    public void setReloadThreshold(BigDecimal reloadThreshold) {
        this.reloadThreshold = reloadThreshold;
    }

    public String getReloadToken() {
        return reloadToken;
    }

    public void setReloadToken(String reloadToken) {
        this.reloadToken = reloadToken;
    }

    private Integer getReloadTerminalID() {
        return reloadTerminalID;
    }

    private void setReloadTerminalID(Integer reloadTerminalID) {
        this.reloadTerminalID = reloadTerminalID;
    }

    private Integer getLoggedInEmployeeID() {
        return loggedInEmployeeID;
    }

    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

    public Integer getReloadPaymentMethodID() {
        return reloadPaymentMethodID;
    }

    public void setReloadPaymentMethodID(Integer reloadPaymentMethodID) {
        this.reloadPaymentMethodID = reloadPaymentMethodID;
    }

    public Integer getReloadSetUpByPersonID() {
        return reloadSetUpByPersonID;
    }

    public void setReloadSetUpByPersonID(Integer reloadSetUpByPersonID) {
        this.reloadSetUpByPersonID = reloadSetUpByPersonID;
    }

    public String getReloadSetUpByName() {
        return reloadSetUpByName;
    }

    public void setReloadSetUpByName(String reloadSetUpByName) {
        this.reloadSetUpByName = reloadSetUpByName;
    }


}