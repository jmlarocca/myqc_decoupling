package com.mmhayes.common.funding.models;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.TransactionBuilder;
import com.mmhayes.common.api.errorhandling.exceptions.FundingCardException;
import com.mmhayes.common.api.errorhandling.exceptions.FundingException;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.funding.FundingHelper;
import com.mmhayes.common.loyalty.models.LoyaltyAccountModel;
import com.mmhayes.common.processors.freedompay.FreedomPayHelper;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.Logger;
import com.stripe.Stripe;
import com.stripe.exception.*;
import com.stripe.model.Charge;
import com.stripe.model.Refund;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

//funding dependencies
//stripe dependencies
//other dependencies

/* Funding Model
 Last Updated (automatically updated by SVN)
 $Author: ecdyer $: Author of last commit
 $Date: 2021-04-28 10:05:55 -0400 (Wed, 28 Apr 2021) $: Date of last commit
 $Rev: 13866 $: Revision of last commit

 Notes: Core model used to process payment methods and to fund and refund accounts
*/
public class FundingModel {
    AccountPaymentMethodModel accountPaymentMethod = null; //which payment method on file was "processed" (a.k.a. "charged")
    PaymentProcessorModel paymentProcessorModel = null; //which payment processor was used for the payment method
    BigDecimal processedAmount = null; //contains the exact dollar amount of what was "processed" (a.k.a "charged") at a payment processor
    BigDecimal fundedAmount = null; //contains the exact dollar amount of what was "funded" for the QC Account
    BigDecimal feeAmount = BigDecimal.ZERO; //contains the exact dollar amount of convenience fee
    BigDecimal processedRefundAmount = null; //contains the exact dollar amount that was "processed" for refund (a.k.a "refunded") at a payment processor
    BigDecimal refundedAmount = null; //contains the exact dollar amount that was "refunded" for the QC Account
    Integer transactionID = null; //identifies the "QuickCharge" Transaction involved with this fund/refund - QC_Transactions.TransactionID
    Integer fundingTerminalID = null; //identifies the "My QC Funding" terminal used to fund
    String processorRefNum = null; //provided from the Payment Processor - for charges/refunds -used for further looks up against the payment processor
    Integer failedCardMaxAttempts = null;
    Integer failedCardDelayHours = null;
    Integer personId = null;
    boolean fundingTransaction = true; //flag to differentiate if the charge is from funding or from a Credit Card transaction
    @JsonIgnore private Integer loggedInUserID = null; //logged in user ID - QC User ID - so likely a manager DO NOT USE for accounts (employees)
    @JsonIgnore private static DataManager dm = new DataManager();
    @JsonIgnore private static commonMMHFunctions commonMMHFunctions = new commonMMHFunctions();

    //default constructor - allows for serializing JSON to Java
    public FundingModel() throws Exception {

    }

    //constructor that retrieves either the "default" account payment method model for the authenticated account OR for the person account
    public FundingModel(HttpServletRequest request) throws Exception {
        Integer authenticatedEmployeeID;
        Integer personID = null;

        //throw InvalidAuthException if no authenticated employee is found
        if ( CommonAuthResource.isKOA( request ) ) {
            authenticatedEmployeeID = CommonAuthResource.getAccountIDFromAccountHeader(request);
        } else {
            //determine the authenticated account ID from the request
            HashMap authenticationHM = CommonAuthResource.determineAuthDetailsFromAuthHeader(request);
            AccountModel authenticatedEmployeeModel = (AccountModel) authenticationHM.get("accountModel");
            authenticatedEmployeeID = authenticatedEmployeeModel.getId();
            personID = CommonAPI.convertModelDetailToInteger(authenticationHM.get("personID"));
            this.personId = personID;
        }

        //always need employeeID, as it is the account that will be funded even for person payment methods
        if ( authenticatedEmployeeID == null ) {
            throw new InvalidAuthException();
        }

        //get the default payment method for the account or person if a valid personID is provided
        getDefaultAccountPaymentMethod(authenticatedEmployeeID, personID);

        //set the payment processor details for recording the PATransaction
        setPaymentProcessorModel(new PaymentProcessorModel(request));
    }

    //constructor that retrieves the "default" account payment method model for the authenticated account
    public FundingModel(Integer authenticatedEmployeeID) throws Exception {
        this(authenticatedEmployeeID, false);
    }

    //constructor that retrieves either the "default" account payment method model for the authenticated account OR the payment method set up for auto funding (could be a person's payment method)
    public FundingModel(Integer authenticatedEmployeeID, boolean retrieveAutoFundingPaymentMethod) throws Exception {
        if ( !retrieveAutoFundingPaymentMethod ) {
            getDefaultAccountPaymentMethod(authenticatedEmployeeID);
        } else {
            getAutoFundingPaymentMethod(authenticatedEmployeeID);
        }

        setPaymentProcessorModel(new PaymentProcessorModel(authenticatedEmployeeID));
    }

    //constructor that simply sets the accountPaymentMethod
    public FundingModel(AccountPaymentMethodModel accountPaymentMethod) throws Exception {
        setAccountPaymentMethod(accountPaymentMethod);
    }

    //attempts to fund a QC account by creating a "charge" against the payment method processor
    public FundingModel chargeThenFundAccount(BigDecimal fundAmount, AccountPaymentMethodModel accountPaymentMethod, String fundingMethod) throws Exception {

        //throw missing data exception if fundAmount could not be determined
        if (fundAmount == null) {
            Logger.logMessage("While attempting to fund account.. but could not find a valid charge!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if accountPaymentMethod could not be determined
        if (accountPaymentMethod == null) {
            Logger.logMessage("While attempting to fund account.. could not find payment method information!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        if(fundingMethod != null) {
            accountPaymentMethod.setFundingMethod(fundingMethod);
        }

        Integer fundingTerminalID = FundingHelper.getMyQCFundingTerminalID(accountPaymentMethod.getAccountID());

        //validate the account and payment method on file before attempt to charge the processor
        validateAccountAndPaymentMethodForFunding(accountPaymentMethod);

        //validate that the CC Tender is mapped to the same Revenue Center as the Funding Terminal
        verifyTenderMappedToFundingTerminal(fundingTerminalID);

        //validate that the Received On Account is mapped to the same Revenue Center as the Funding Terminal
        verifyRoaMappedToFundingTerminal(fundingTerminalID);

        //calculate fee and add here
        BigDecimal paymentFee = accountPaymentMethod.calculatePaymentFee(fundAmount).getFundingFee();

        //if there is no fee set to zero
        if ( paymentFee == null ) {
            paymentFee = BigDecimal.ZERO;
        }

        //capture fee for use later sending email.
        setFeeAmount(paymentFee);

        setProcessedAmount(createProcessorCharge(fundAmount.add(paymentFee), accountPaymentMethod));

        //if a valid processedAmount is returned - then attempt to fund the QC account the exact amount (processedAmount) that was charged by the processor
        if ( getProcessedAmount() != null ) {

            //if there is no fee or the fee is zero, continue as normal
            if ( paymentFee.compareTo(BigDecimal.ZERO) == 0 ) {
                //create a QC Transaction for the "processed amount", then set what amount was actually funded on this model
                setFundedAmount(fundAccount(getProcessedAmount(), accountPaymentMethod));
            } else {
                //there is a fee, process properly
                setFundedAmount(fundAccount(fundAmount, accountPaymentMethod, paymentFee));
            }

            if (getFundedAmount() == null) {
                Logger.logMessage("Attempt to fund failed, could not determine funded amount, for account ID# "+accountPaymentMethod.getAccountID(), Logger.LEVEL.ERROR);
                // REFUND CHARGE
                voidChargeOnError(accountPaymentMethod,processorRefNum,fundAmount);
                throw new FundingException();
            }
        } else {
            Logger.logMessage("Attempt to fund failed, could not determine processed amount, for account ID# "+accountPaymentMethod.getAccountID(), Logger.LEVEL.ERROR);
            // REFUND CHARGE
            voidChargeOnError(accountPaymentMethod,processorRefNum,fundAmount);
            throw new FundingException();
        }

        return this;
    }

    //validate the account and payment method on file for attempting to fund the account
    private void validateAccountAndPaymentMethodForFunding(AccountPaymentMethodModel accountPaymentMethod) throws Exception {

        //method variable - has the account been verified entirely?
        Boolean verifiedAccount = false;

        //throw missing data exception if required data is missing
        if (accountPaymentMethod == null) {
            Logger.logMessage("Account Payment Method is Invalid - Could not find any payment details.", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        if (accountPaymentMethod.getId() == null) {
            Logger.logMessage("Account Payment Method is Invalid - Could not find Payment Method ID.", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        if (accountPaymentMethod.getAccountID() == null) {
            Logger.logMessage("Account Payment Method is Invalid - Could not find Account ID.", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        if (accountPaymentMethod.retrievePrivateAPIKeySafely() == null) {
            Logger.logMessage("Account Payment Method is Invalid - Could not find private API key for payment method ID# "+accountPaymentMethod.getId(), Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        if (accountPaymentMethod.getProcessorPaymentToken() == null) {
            Logger.logMessage("Account Payment Method is Invalid - Could not find processor payment token for payment method ID# "+accountPaymentMethod.getId(), Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if credit card tender on the funding terminal could not be determined
        if(getPaymentProcessorModel() == null) {
            Logger.logMessage("Payment Processor is Invalid - could not find payment processor information on funding terminal" , Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if credit card tender on the funding terminal could not be determined
        if (getPaymentProcessorModel().getCreditCardTenderID() == null) {
            Logger.logMessage("Payment Processor is Invalid - could not find a valid credit card tender on the funding terminal!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if received on account ID on the funding terminal could not be determined
        if (getPaymentProcessorModel().getReceivedOnAccountsID() == null) {
            Logger.logMessage("Payment Processor is Invalid - could not find a valid received on accounts on the funding terminal!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //query for additional account information not found on the accountPaymentMethod model
        ArrayList<HashMap> verifyAccountList = dm.parameterizedExecuteQuery("data.common.funding.verifyAccountForFunding",
                new Object[]{
                        accountPaymentMethod.getAccountID()
                },
                true
        );

        //if the verify account list contains one account...
        if (verifyAccountList != null && verifyAccountList.size() == 1) {
            HashMap verifyAccountHM = verifyAccountList.get(0);
            if (verifyAccountHM.get("ACCOUNTSTATUS") != null
                    && verifyAccountHM.get("ALLOWMYQCFUNDING") != null) {

                String accountStatus = verifyAccountHM.get("ACCOUNTSTATUS").toString();
                Boolean allowMyQCFunding =  Boolean.parseBoolean(verifyAccountHM.get("ALLOWMYQCFUNDING").toString());

                //verify that the account is NOT "inactive" AND the account is in an account group that has "Allow My QC Funding" turned on
                if (!accountStatus.equals("I") && allowMyQCFunding) {
                    verifiedAccount = true;
                }
            }
        }

        if (!verifiedAccount) {
            Logger.logMessage("Could not find additional required account information for account ID# "+accountPaymentMethod.getAccountID(), Logger.LEVEL.ERROR);
            throw new FundingException();
        }

    }

    public void verifyTenderMappedToFundingTerminal(Integer fundingTerminalID) throws Exception{

        Boolean verifiedTender = false;
        String objectTypeID = "3";

        ArrayList<HashMap> tenderRevCenterMapping = dm.parameterizedExecuteQuery("data.common.funding.verifyObjectRCInTerminalRC",
                new Object[]{
                        fundingTerminalID,
                        getPaymentProcessorModel().getCreditCardTenderID(),
                        objectTypeID
                },
                true
        );

        if(tenderRevCenterMapping != null && tenderRevCenterMapping.size() == 1){
            HashMap revCenterMappingHM = tenderRevCenterMapping.get(0);
            if(revCenterMappingHM.get("REVCENTMAPID") != null){
                verifiedTender = true;
            }

        }

        if(!verifiedTender){
            Logger.logMessage("The selected Tender is not in the same Revenue Center as the Funding Terminal");
            throw new MissingDataException();
        }

    }

    public void verifyRoaMappedToFundingTerminal(Integer fundingTerminalID) throws Exception{

        Boolean verifiedRoa = false;
        String objectTypeID = "11";

        ArrayList<HashMap> tenderRevCenterMapping = dm.parameterizedExecuteQuery("data.common.funding.verifyObjectRCInTerminalRC",
                new Object[]{
                        fundingTerminalID,
                        getPaymentProcessorModel().getReceivedOnAccountsID(),
                        objectTypeID
                },
                true
        );

        if(tenderRevCenterMapping != null && tenderRevCenterMapping.size() == 1){
            HashMap revCenterMappingHM = tenderRevCenterMapping.get(0);
            if(revCenterMappingHM.get("REVCENTMAPID") != null){
                verifiedRoa = true;
            }

        }

        if(!verifiedRoa){
            Logger.logMessage("The selected ROA is not in the same Revenue Center as the Funding Terminal");
            throw new MissingDataException();
        }

    }

    //attempts to create a "charge" against the payment method processor
    public FundingModel chargeAccount(BigDecimal chargeAmount, AccountPaymentMethodModel accountPaymentMethod, String fundingMethod) throws Exception {

        //throw missing data exception if chargeAmount could not be determined
        if (chargeAmount == null) {
            Logger.logMessage("While attempting to fund account.. but could not find a valid charge!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if accountPaymentMethod could not be determined
        if (accountPaymentMethod == null) {
            Logger.logMessage("While attempting to fund account.. could not find payment method information!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        if(fundingMethod != null) {
            accountPaymentMethod.setFundingMethod(fundingMethod);
        }

        //validate the account and payment method on file before attempt to charge the processor
        validateAccountAndPaymentMethodForCharge(accountPaymentMethod);

        //calculate fee and add here
        BigDecimal paymentFee = accountPaymentMethod.calculatePaymentFee(chargeAmount).getFundingFee();

        //if there is no fee set to zero
        if ( paymentFee == null ) {
            paymentFee = BigDecimal.ZERO;
        }

        //create a charge
        setProcessedAmount(createProcessorCharge(chargeAmount.add(paymentFee), accountPaymentMethod));

        //if a valid processedAmount is not returned throw error
        if (getProcessedAmount() == null) {
            Logger.logMessage("Attempt to charge account failed, could not determine processed amount, for account ID# "+accountPaymentMethod.getAccountID(), Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        return this;
    }

    //validate the account and payment method on file for attempting to charge the account
    private void validateAccountAndPaymentMethodForCharge(AccountPaymentMethodModel accountPaymentMethod) throws Exception {

        //method variable - has the account been verified entirely?
        Boolean verifiedAccount = false;

        //throw missing data exception if required data is missing
        if (accountPaymentMethod == null) {
            Logger.logMessage("Account Payment Method is Invalid - Could not find any payment details.", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        if (accountPaymentMethod.getId() == null) {
            Logger.logMessage("Account Payment Method is Invalid - Could not find Payment Method ID.", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        if (accountPaymentMethod.getAccountID() == null) {
            Logger.logMessage("Account Payment Method is Invalid - Could not find Account ID.", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        if (accountPaymentMethod.retrievePrivateAPIKeySafely() == null) {
            Logger.logMessage("Account Payment Method is Invalid - Could not find private API key for payment method ID# "+accountPaymentMethod.getId(), Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        if (accountPaymentMethod.getProcessorPaymentToken() == null) {
            Logger.logMessage("Account Payment Method is Invalid - Could not find processor payment token for payment method ID# "+accountPaymentMethod.getId(), Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //query for additional account information not found on the accountPaymentMethod model
        ArrayList<HashMap> verifyAccountList = dm.parameterizedExecuteQuery("data.common.funding.verifyAccountForFunding",
                new Object[]{
                        accountPaymentMethod.getAccountID()
                },
                true
        );

        //if the verify account list contains one account...
        if (verifyAccountList != null && verifyAccountList.size() == 1) {
            HashMap verifyAccountHM = verifyAccountList.get(0);
            if (verifyAccountHM.get("ACCOUNTSTATUS") != null) {

                String accountStatus = verifyAccountHM.get("ACCOUNTSTATUS").toString();

                //verify that the account is NOT "inactive" AND the account is in an account group that has "Allow My QC Funding" turned on
                if (!accountStatus.equals("I")) {
                    verifiedAccount = true;
                }
            }
        }

        if (!verifiedAccount) {
            Logger.logMessage("Could not find additional required account information for account ID# "+accountPaymentMethod.getAccountID(), Logger.LEVEL.ERROR);
            throw new FundingException();
        }

    }

    //attempts to charge against the processor - returns valid processed amount if successful
    private BigDecimal createProcessorCharge(BigDecimal fundAmount, AccountPaymentMethodModel accountPaymentMethod) throws Exception {

        //method variable - the exact amount that was processed by the processor for the passed in account payment method
        BigDecimal processedAmount = null;

        //determine the processor and charge appropriately
        if (accountPaymentMethod.getPaymentProcessorID() == 1) { //STRIPE
            processedAmount = createStripeCharge(fundAmount, accountPaymentMethod);
        } else if(accountPaymentMethod.getPaymentProcessorID() == 6) { //FreedomPay client
            processedAmount = createFreedomPayCharge(fundAmount, accountPaymentMethod);
        } else {
            Logger.logMessage("Did not attempt to charge payment processor - handling for payment processor does not exist -for account ID# "+accountPaymentMethod.getAccountID(), Logger.LEVEL.WARNING);
        }

        // Send Funding Email if processedAmount > 0
        if(processedAmount != null && processedAmount.compareTo(BigDecimal.ZERO) == 1 && isFundingTransaction()){
            // Build params HashMap
            HashMap params = new HashMap();
            params.put("RecipientName", accountPaymentMethod.getPaymentMethodFullName());
            params.put("Amount", processedAmount.setScale(2,BigDecimal.ROUND_HALF_UP).abs().toString());
            params.put("ProcessedAmount", processedAmount.setScale(2,BigDecimal.ROUND_HALF_UP).abs().toString());
            params.put("FeeAmount", getFeeAmount().setScale(2,BigDecimal.ROUND_HALF_UP).abs().toString());
            params.put("FeeLabel", accountPaymentMethod.getFundingFeeLabel() == null ? "" : accountPaymentMethod.getFundingFeeLabel());

            // Get EmployeeID from Funding Email
            String emailAddress = accountPaymentMethod.getPaymentMethodEmailAddress();

            int employeeID = 0;
            if(accountPaymentMethod.getAccountID() != null) {
                employeeID = accountPaymentMethod.getAccountID();
            } else if (accountPaymentMethod.getPaymentProcessorID() == 1){  //only get the employeeID from the email if using stripe, freedom pay doesn't save email on payment method
                employeeID = commonMMHFunctions.getEmployeeIDFromFundingEmailAddress(emailAddress);
            }

            if(employeeID > -1){
                params.put("EmployeeID", employeeID);
            }
            if(accountPaymentMethod.getPersonID() != null ){
                params.put("PersonID", accountPaymentMethod.getPersonID());
            }
            // Define EmailTypeID
            int emailTypeID = 6;
            // Send Email
            commonMMHFunctions common = new commonMMHFunctions();
            if(!emailAddress.isEmpty()){
                if(common.sendTemplateEmail(emailAddress, 0, emailTypeID, "", params)){
                    Logger.logMessage("Successfully sent funding success e-mail to "+emailAddress, Logger.LEVEL.TRACE);
                } else {
                    Logger.logMessage("Failed to send funding success e-mail to "+emailAddress, Logger.LEVEL.ERROR);
                }
            }
        }

        return processedAmount;
    }

    //attempts to charge against Stripe - returns valid processed amount if successful
    private BigDecimal createStripeCharge(BigDecimal chargeAmount, AccountPaymentMethodModel accountPaymentMethod) throws Exception {

        //method variable - the exact amount that was processed by the processor for the passed in account payment method
        BigDecimal processedAmount;

        //throw missing data exception if required data is missing
        if (chargeAmount == null) {
            Logger.logMessage("Missing charge amount for processing a Stripe Charge. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //Stripe requires that the chargeAmount sent to them is in "penny" format ($15.00 should be 1500)
        Integer chargeAmountAsPennies = convertAmountFromDollarsToPennies(chargeAmount);

        //throw missing data exception if required data is missing
        if (accountPaymentMethod == null) {
            Logger.logMessage("Missing all payment method information for processing a Stripe Charge. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        String processorPaymentToken = accountPaymentMethod.getProcessorPaymentToken();
        if (processorPaymentToken == null) {
            Logger.logMessage("Missing Stripe Customer ID (token) for processing a Stripe Charge. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        Integer accountID = accountPaymentMethod.getAccountID();
        if (accountID == null) {
            Logger.logMessage("Missing QC Account ID for processing a Stripe Charge. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        String privateAPIKey = accountPaymentMethod.retrievePrivateAPIKeySafely();
        if (privateAPIKey == null || privateAPIKey.trim().isEmpty()) {
            Logger.logMessage("Missing private API key for processing a Stripe Charge. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //log before attempting to communicate with Stripe...
        Logger.logMessage("Attempting to charge ("+chargeAmount+") Stripe Customer ("+accountPaymentMethod.getProcessorPaymentToken()+") for account ID# "+accountPaymentMethod.getAccountID());
        Logger.logMessage("Payment Method ID: "+accountPaymentMethod.getId(), Logger.LEVEL.DEBUG);
        Logger.logMessage("Payment Method Type: "+accountPaymentMethod.getPaymentMethodType(), Logger.LEVEL.DEBUG);
        Logger.logMessage("Fund Amount as pennies: "+chargeAmountAsPennies, Logger.LEVEL.DEBUG);

        //attempt to communicate with Stripe API
        try {

            //set the privateAPIKey on the Stripe Object
            Stripe.apiKey = privateAPIKey;

            //set amount, description and "customer" (paymentProcessorToken) in temp hashmap
            Map<String, Object> chargeParams = new HashMap<>();
            chargeParams.put("amount", chargeAmountAsPennies);
            chargeParams.put("currency", "usd");
            chargeParams.put("description", "Charge using Stripe Customer for QC Account ID# "+accountID);
            chargeParams.put("customer", processorPaymentToken);

            //charge the account by making a request to Stripe API
            Charge charge = Charge.create(chargeParams);

            //set the stripe charge reference number (must store - required for doing refunds)
            setProcessorRefNum(charge.getId());

            //convert the processed amount back to dollars
            processedAmount = convertAmountFromPenniesToDollars(charge.getAmount());

            //log after communicating with Stripe...
            Logger.logMessage("Successfully charged ("+processedAmount+") to Stripe Customer ("+accountPaymentMethod.getProcessorPaymentToken()+") for account ID# "+accountPaymentMethod.getAccountID());
            Logger.logMessage("Stripe Charge Reference Number: "+charge.getId(), Logger.LEVEL.TRACE);
            Logger.logMessage("Stripe Charge Status: "+charge.getStatus(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Processed Amount (as pennies): "+charge.getAmount(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Payment Method ID: "+accountPaymentMethod.getId(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Payment Method Type: "+accountPaymentMethod.getPaymentMethodType(), Logger.LEVEL.DEBUG);

        } catch (CardException stripeCardEx) {
            //log stripe exception - means the card was declined or something similar
            Logger.logMessage("Card Exception from Stripe API. Card may have been declined.", Logger.LEVEL.ERROR);
            Logger.logMessage("Status is: " + stripeCardEx.getCode(), Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeCardEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeCardEx);

            Integer accountStatus = updateFailedFundingAttempts(accountPaymentMethod, stripeCardEx.getMessage());
            String accountStatusMsg = "";
            if(accountStatus == 0) {
                accountStatusMsg = " Please check your email for more information.";
            }
            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingException(stripeCardEx.getMessage() + accountStatusMsg);
        } catch (RateLimitException stripeRateLimitEx) {
            //log stripe exception - means too many requests made to the API too quickly
            Logger.logMessage("Rate Limit Exception from Stripe API. Too many requests made to the API too quickly.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeRateLimitEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeRateLimitEx);

            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingCardException("Too many requests made to the API too quickly");
        } catch (InvalidRequestException stripeInvalidRequestEx) {
            //log stripe exception - means invalid parameters were supplied to Stripe's API
            Logger.logMessage("Invalid Request Exception from Stripe API. Means invalid parameters were sent to Stripe's API.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeInvalidRequestEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeInvalidRequestEx);

            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingCardException("Invalid request was sent to Stripe's API");
        } catch (AuthenticationException stripeAuthEx) {
            //log stripe exception - Authentication with Stripe's API failed
            Logger.logMessage("Stripe Authentication Exception from Stripe API. Authentication with Stripe's API failed.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeAuthEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeAuthEx);

            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingCardException("Authentication with Stripe's API failed");
        } catch (APIConnectionException stripeAPIConnectionEx) {
            //log stripe exception - Network communication with Stripe failed
            Logger.logMessage("Stripe Authentication Exception from Stripe API. Network communication with Stripe failed.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeAPIConnectionEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeAPIConnectionEx);

            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingCardException("Network communication with Stripe failed");
        } catch (StripeException stripeEx) {
            //log stripe exception - generic exception
            Logger.logMessage("Stripe Authentication Exception from Stripe API. Generic error.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeEx);

            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingCardException("Authentication with Stripe API failed");
        }

        return processedAmount;
    }

    private BigDecimal createFreedomPayCharge(BigDecimal chargeAmount, AccountPaymentMethodModel accountPaymentMethod) throws Exception {

        //method variable - the exact amount that was processed by the processor for the passed in account payment method
        BigDecimal processedAmount;

        // Freedompay api identification vars
        String terminalID = accountPaymentMethod.getPaymentProcessorTerminalNum();
        if(terminalID.isEmpty()){
            Logger.logMessage("Missing private TerminalID API key for processing a FreedomPay Charge. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        String storeID = accountPaymentMethod.getPaymentProcessorStoreNum();
        if(storeID.isEmpty()){
            Logger.logMessage("Missing private StoreID API key for processing a FreedomPay Charge. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        if (chargeAmount == null) {
            Logger.logMessage("Missing charge amount for processing a FreedomPay Charge. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        if (accountPaymentMethod == null) {
            Logger.logMessage("Missing all payment method information for processing a FreedomPay Charge. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        String processorPaymentToken = accountPaymentMethod.getProcessorPaymentToken();
        if (processorPaymentToken == null || processorPaymentToken.isEmpty()) {
            Logger.logMessage("Missing FreedomPay Customer ID (token) for processing a FreedomPay Charge. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        Integer accountID = accountPaymentMethod.getAccountID();
        if (accountID == null) {
            Logger.logMessage("Missing QC Account ID for processing a FreedomPay Charge. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //log before attempting to communicate with API...
        Logger.logMessage("Attempting to charge ("+chargeAmount+") FreedomPay Customer ("+accountPaymentMethod.getProcessorPaymentToken()+
                ") for account ID# "+accountPaymentMethod.getAccountID());
        Logger.logMessage("Payment Method ID: "+accountPaymentMethod.getId(), Logger.LEVEL.DEBUG);
        Logger.logMessage("Payment Method Type: "+accountPaymentMethod.getPaymentMethodType(), Logger.LEVEL.DEBUG);

        //attempt to communicate with FreedomPay API
        try {

            //charge the account by making a request to Stripe API
            FreedomPayHelper freedompay = new FreedomPayHelper();
            freedompay.setStoreID(storeID);
            freedompay.setTerminalID(terminalID);
            freedompay.setPaymentToken(processorPaymentToken);
            // Execute Charge Call
            processedAmount = freedompay.charge(chargeAmount, accountPaymentMethod.getAllowPartialCCAuth());

            //set the stripe charge reference number (must store - required for doing refunds)
            setProcessorRefNum(freedompay.getRequestID());

            //log after communicating with Stripe...
            Logger.logMessage("Successfully charged ("+processedAmount+") to FreedomPay Customer ("+accountPaymentMethod.getProcessorPaymentToken()+") for account ID# "+accountPaymentMethod.getAccountID());
            Logger.logMessage("FreedomPay Charge Reference Number: "+freedompay.getRequestID(), Logger.LEVEL.TRACE);
            Logger.logMessage("FreedomPay Charge Status: "+freedompay.getStatus(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Processed Amount: "+processedAmount.toString(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Payment Method ID: "+accountPaymentMethod.getId(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Payment Method Type: "+accountPaymentMethod.getPaymentMethodType(), Logger.LEVEL.DEBUG);

        } catch (Exception ex) {
            //log exception - means the card was declined or something similar
            Logger.logMessage("Card Exception from FreedomPay API. Card may have been declined.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + ex.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(ex);

            String errorMessage = ex.getMessage();

            if ( errorMessage == null ) {
                errorMessage = "An error occurred when attempting to use this payment method.";
            }

            Integer accountStatus = updateFailedFundingAttempts(accountPaymentMethod, errorMessage);
            String accountStatusMsg = "";
            if(accountStatus == 0) {
                accountStatusMsg = " Please check your email for more information.";
            }
            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingException(errorMessage + accountStatusMsg);
        }

        return processedAmount;
    }

    //creates a QC Transaction that "funds" any QC account passed in (pass in as POSITIVE BigDecimal) - returns valid funded amount if successful
    private BigDecimal fundAccount(BigDecimal fundAmount, AccountPaymentMethodModel accountPaymentMethod) throws Exception {
        return fundAccount(fundAmount, accountPaymentMethod, null);
    }

    //creates a QC Transaction that "funds" any QC account passed in (pass in as POSITIVE BigDecimal) - returns valid funded amount if successful
    private BigDecimal fundAccount(BigDecimal fundAmount, AccountPaymentMethodModel accountPaymentMethod, BigDecimal feeAmount) throws Exception {

        //throw missing data exception if fundAmount could not be determined
        if (fundAmount == null) {
            Logger.logMessage("While funding account.. could not find a valid charge!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if accountPaymentMethod could not be determined
        if (accountPaymentMethod == null) {
            Logger.logMessage("While funding account.. could not find payment method information!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if employeePaymentMethodID could not be determined
        if (accountPaymentMethod.getId() == null) {
            Logger.logMessage("While funding account.. could not find payment method identifier!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if employeePaymentMethodID could not be determined
        if (getProcessorRefNum() == null) {
            Logger.logMessage("While funding account.. could not find processor charge reference number!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
        //determine the terminal for this funding
        Integer fundingTerminalID = FundingHelper.getMyQCFundingTerminalID(accountPaymentMethod.getAccountID());
        setFundingTerminalID(fundingTerminalID);

        //determine the QC User responsible for this funding
        Integer transactionQCUserID = commonMMHFunctions.determineMyQCUserID();

        //create PATransaction
        TransactionModel transactionModel= null;
        try{
            transactionModel = createFundingPATransaction(fundingTerminalID, fundAmount, feeAmount);
        } catch(Exception ex){
            Logger.logMessage("Error creating funding PATransaction: "+ex.getMessage(), Logger.LEVEL.ERROR);
            // TODO VOID CHARGE ON ERROR
            voidChargeOnError(accountPaymentMethod, processorRefNum,fundAmount);
            throw new FundingException();
        }

        //throw missing data exception if paTransactionID is null
        if (transactionModel.getId() == null) {
            Logger.logMessage("While funding account.. could not record PATransaction!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //change the sign of the fundAmount by multiply it by negative one - technically should be NEGATIVE in QC_Transactions for funding
        fundAmount = fundAmount.multiply(new BigDecimal(-1));

        //the actual amount is the fund amount plus the fee, or just the fundAmount if fee is null
        BigDecimal actualAmount = fundAmount;

        if ( feeAmount != null && feeAmount.compareTo(BigDecimal.ZERO) > 0 ) {
            //change the sign of the feeAmount by multiply it by negative one - technically should be NEGATIVE in QC_Transactions for funding
            feeAmount = feeAmount.multiply(new BigDecimal(-1));
            actualAmount = fundAmount.add(feeAmount);
        }

        if(transactionModel.getReceivedOnAccounts().size() > 0 && transactionModel.getReceivedOnAccounts().get(0).getQcTransaction() != null
                && transactionModel.getReceivedOnAccounts().get(0).getQcTransaction().getId() != null)  {

            Integer qcTransactionId = transactionModel.getReceivedOnAccounts().get(0).getQcTransaction().getId();

            //if the update result does not return as 1 - something went wrong so throw an exception
            Integer updateTransResult = dm.parameterizedExecuteNonQuery("data.common.funding.updateFundingQCTransaction",
                    new Object[]{
                            qcTransactionId,
                            accountPaymentMethod.getId(),
                            getProcessorRefNum(),
                            accountPaymentMethod.getPersonID(),
                            actualAmount
                    }
            );

            if (updateTransResult != 1) {
                if ( accountPaymentMethod.getPersonID() != null ) {
                    Logger.logMessage("Could not update transaction ID "+qcTransactionId+" with Employee Payment Method and Processor Ref Num info - For person ID# "+accountPaymentMethod.getPersonID(), Logger.LEVEL.ERROR);
                } else {
                    Logger.logMessage("Could not update transaction ID "+qcTransactionId+" with Employee Payment Method and Processor Ref Num info - For account ID# "+accountPaymentMethod.getAccountID(), Logger.LEVEL.ERROR);
                }
                // TODO VOID CHARGE ON ERROR
                voidChargeOnError(accountPaymentMethod, processorRefNum,fundAmount);
                throw new FundingException();
            } else {
                //set qc transaction ID
                setTransactionID(qcTransactionId);
            }
        }

        return fundAmount;
    }

    private void voidChargeOnError(AccountPaymentMethodModel paymentMethodModel, String processorRefNum, BigDecimal amount) throws Exception {

        BigDecimal voidAmount = new BigDecimal(BigInteger.ZERO);
        Logger.logMessage("Starting method voidChargeOnError()", Logger.LEVEL.DEBUG);

        //VOID STRIPE or FP Charge if there is an error in recording the FUNDING Transaction
        if(paymentMethodModel.getPaymentProcessorID().equals(6)){
            // FREEDOMPAY
            FreedomPayHelper freedomPayHelper = new FreedomPayHelper();
            freedomPayHelper.setStoreID(accountPaymentMethod.getPaymentProcessorStoreNum());
            freedomPayHelper.setTerminalID(accountPaymentMethod.getPaymentProcessorTerminalNum());
            try{
                if(freedomPayHelper.voidFPTransaction(processorRefNum)){
                    voidAmount = amount;
                }
            } catch(Exception ex){
                throw new Exception("Freedompay Void Processing Error");
            }
        } else if(paymentMethodModel.getPaymentProcessorID().equals(1)){
            // STRIPE
            try {
                voidAmount = createStripeRefund(processorRefNum, amount, accountPaymentMethod);
            } catch(Exception ex){
                throw new Exception("Stripe Void Processing Error");
            }
        } else {
            Logger.logMessage("UNSUPPORTED PAYMENT PROCESSOR ENCOUNTERED", Logger.LEVEL.ERROR);
            throw new Exception("UNSUPPORTED PAYMENT PROCESSOR ENCOUNTERED");
        }

        Logger.logMessage("Amount Voided/Refunded: "+voidAmount.toString() , Logger.LEVEL.TRACE);
        Logger.logMessage("Ending method voidChargeOnError()", Logger.LEVEL.DEBUG);
    }

    //attempts to refund a QC account by creating (potentially) multiple refunds against the payment method processor to reach the requested amount
    public FundingModel refundChargedAmount(BigDecimal requestedRefundAmount, Integer verifiedAccountID, Integer loggedInUserID) throws Exception {

        //throw missing data exception if requestedRefundAmount could not be determined
        if (requestedRefundAmount == null) {
            Logger.logMessage("While attempting to refund account.. but could not find a valid requested refund amount!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if verifiedAccountID could not be determined
        if (verifiedAccountID == null) {
            Logger.logMessage("While attempting to fund account.. could not find the account ID!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if loggedInUserID could not be determined
        if (loggedInUserID == null) {
            Logger.logMessage("While attempting to refund account.. but could not find the logged in user ID!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        } else {
            //set the logged in user ID - this will be used on "refund" QC_Transactions records
            setLoggedInUserID(loggedInUserID);
        }

        //log debug information
        Logger.logMessage("About to attempt to refund ("+requestedRefundAmount+") for account ID ("+verifiedAccountID+") initiated by the QC User ID ("+loggedInUserID+")", Logger.LEVEL.DEBUG);

        //attempt to refund this charge
        setRefundedAmount(createProcessorRefund(getProcessorRefNum(), requestedRefundAmount, getAccountPaymentMethod()));

        return this;
    }

    // OVERLOAD - attempts to refund a QC account by creating (potentially) multiple refunds against the payment method processor to reach the requested amount
    public FundingModel refundChargedAmount(HttpServletRequest request) throws Exception {
        Integer authenticatedEmployeeID = null;

        // get employee ID from request
        if ( CommonAuthResource.isKOA( request ) ) {
            authenticatedEmployeeID = CommonAuthResource.getAccountIDFromAccountHeader(request);
        } else {
            authenticatedEmployeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        }

        return refundChargedAmount(getProcessedAmount(), authenticatedEmployeeID);
    }

    //attempts to refund an employee account by creating (potentially) multiple refunds against the payment method processor to reach the requested amount
    public FundingModel refundChargedAmount(BigDecimal requestedRefundAmount, Integer verifiedAccountID) throws Exception {

        //throw missing data exception if requestedRefundAmount could not be determined
        if (requestedRefundAmount == null) {
            Logger.logMessage("While attempting to refund account.. but could not find a valid requested refund amount!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if verifiedAccountID could not be determined
        if (verifiedAccountID == null) {
            Logger.logMessage("While attempting to fund account.. could not find the account ID!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //log debug information
        Logger.logMessage("About to attempt to refund ("+requestedRefundAmount+") for account ID ("+verifiedAccountID+")", Logger.LEVEL.DEBUG);

        //attempt to refund this charge
        setRefundedAmount(createProcessorRefund(getProcessorRefNum(), requestedRefundAmount, getAccountPaymentMethod()));

        return this;
    }

    //attempts to refund a QC account by creating (potentially) multiple refunds against the payment method processor to reach the requested amount
    public FundingModel refundRequestedAmount(BigDecimal requestedRefundAmount, Integer verifiedAccountID, Integer loggedInUserID) throws Exception {

        //throw missing data exception if requestedRefundAmount could not be determined
        if (requestedRefundAmount == null) {
            Logger.logMessage("While attempting to refund account.. but could not find a valid requested refund amount!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if verifiedAccountID could not be determined
        if (verifiedAccountID == null) {
            Logger.logMessage("While attempting to fund account.. could not find the account ID!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if loggedInUserID could not be determined
        if (loggedInUserID == null) {
            Logger.logMessage("While attempting to refund account.. but could not find the logged in user ID!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        } else {
            //set the logged in user ID - this will be used on "refund" QC_Transactions records
            setLoggedInUserID(loggedInUserID);
        }

        //log debug information
        Logger.logMessage("About to attempt to refund ("+requestedRefundAmount+") for account ID ("+verifiedAccountID+") initiated by the QC User ID ("+loggedInUserID+")", Logger.LEVEL.DEBUG);

        //validate that the requested refund amount does not exceed the max refund amount allowed for this account
        validateRequestedRefundAmountForAccount(requestedRefundAmount, verifiedAccountID);

        //determine the QC transactions that eligible for refunding for this account
        ArrayList<HashMap> eligibleTransactionsForRefund = getEligibleTransactionsForRefund(verifiedAccountID);

        //if there is at least one eligibleTransactionForRefund we should attempt to do some refunds
        if (eligibleTransactionsForRefund != null && eligibleTransactionsForRefund.size() > 0) {

            //refunds as many of the eligible transactions as needed to reach the requestedRefundAmount via the processor
            ArrayList<HashMap> qcTransactionsToRefund = refundEligibleTransactionsViaProcessor(eligibleTransactionsForRefund, requestedRefundAmount, verifiedAccountID);

            //creates a QC Transaction that "refunds" a list of existing QC Transactions for any account passed in - returns total refunded funded amount if successful
            setRefundedAmount(refundAccount(qcTransactionsToRefund, verifiedAccountID));

            if (getRefundedAmount() == null) {
                Logger.logMessage("Attempt to refund in QC failed, could not determine refunded amount, for account ID# "+verifiedAccountID, Logger.LEVEL.ERROR);
                throw new MissingDataException();
            }
        } else {
            // No Eligible Transactions for Refund update
            updateEmployeeDisableRefunds(verifiedAccountID);
        }

        return this;
    }

    //validate that the requested refund amount does not exceen the max refund amount allowed for this account
    private void validateRequestedRefundAmountForAccount(BigDecimal requestedRefundAmount, Integer verifiedAccountID) throws Exception {

        //throw missing data exception if requestedRefundAmount could not be determined
        if (requestedRefundAmount == null) {
            Logger.logMessage("While attempting to validate account.. but could not find a valid requested refund amount!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if verifiedAccountID could not be determined
        if (verifiedAccountID == null) {
            Logger.logMessage("While attempting to validate account.. could not find the account ID!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //first, we need to determine the maxRefundAmount - which is the store balance * -1 for the funding terminal for this user
        String storeBalanceAsString = null;

        Integer fundingTerminalID = FundingHelper.getMyQCFundingTerminalID(verifiedAccountID);

        //query for account funding information for this account - we want the store balance
        ArrayList<HashMap> accountFundingInformationList = dm.parameterizedExecuteQuery("data.common.funding.getSingleFundingStoreAndTerminalBalanceForAccount_BP",
                new Object[]{verifiedAccountID, fundingTerminalID}, true
        );

        //if only one record is returned, then we succeeded
        if (accountFundingInformationList != null && accountFundingInformationList.size() == 1) {
            HashMap accountFundingInformation = accountFundingInformationList.get(0);
            storeBalanceAsString = accountFundingInformation.get("STOREBALANCE").toString();
        } else {
            Logger.logMessage("Could not determine account funding information for verified account ID#: "+verifiedAccountID, Logger.LEVEL.ERROR);
            throw new FundingException();
        }

        //determine the max amount that can be refunded - which is currently the store balance  - make sure to set it as OPPOSITE of what it is stored in DB (* -1)
        BigDecimal maxRefundAmount = new BigDecimal(storeBalanceAsString).multiply(new BigDecimal(-1));

        //ensure that the requested amount is smaller than the maxRefundAmount (which as of 3/9/2017 is simply the Store Balance -jrmitaly)
        if (requestedRefundAmount.compareTo(maxRefundAmount) > 0) { //if positive, that means the requestRefundAmount is larger than the maxRefundAmount
            Logger.logMessage("Requested refund amount is greater than the max refund amount! Aborting! For verified account ID#: "+verifiedAccountID, Logger.LEVEL.ERROR);
            throw new FundingException();
        }

    }

    //retrieves the QC transactions that eligible for refunding for this account
    private ArrayList<HashMap> getEligibleTransactionsForRefund(Integer verifiedAccountID) throws Exception {

        //throw missing data exception if verifiedAccountID could not be determined
        if (verifiedAccountID == null) {
            Logger.logMessage("While attempting to determine eligible transactions for refund for account.. could not find the account ID!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //query for processor charge transactions that have not been fully refunded - this query also factors in partial QC refunds when determining eligible refund amounts
        ArrayList<HashMap> eligibleTransactionsForRefund = dm.parameterizedExecuteQuery("data.common.funding.getEligibleTransactionsForRefund",
                new Object[]{ verifiedAccountID }, true
        );

        //make sure we got some kind of data from the database..
        if (eligibleTransactionsForRefund == null) {
            Logger.logMessage("Error occurred when attempting to find eligible transactions for account ID# "+verifiedAccountID, Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //what if there really is no transactions? - log a warning for now -jrmitaly 3/9/2017
        if (eligibleTransactionsForRefund.size() == 0) {
            Logger.logMessage("Could not find ANY eligible transactions for refunding for account ID# " + verifiedAccountID, Logger.LEVEL.WARNING);
        }

        return eligibleTransactionsForRefund;
    }

    //refunds as many of the eligible transactions as needed to reach the requestedRefundAmount via the processor
    private ArrayList<HashMap> refundEligibleTransactionsViaProcessor(ArrayList<HashMap> eligibleTransactionsForRefund, BigDecimal requestedRefundAmount, Integer verifiedAccountID) throws Exception {

        //throw missing data exception if eligibleTransactionsForRefund could not be determined
        if (eligibleTransactionsForRefund == null) {
            Logger.logMessage("While attempting to refund eligible transactions for account.. could not find any eligibleTransactionsForRefund!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if requestedRefundAmount could not be determined
        if (requestedRefundAmount == null) {
            Logger.logMessage("While attempting to refund eligible transactions for account.. but could not find a valid requested refund amount!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if verifiedAccountID could not be determined
        if (verifiedAccountID == null) {
            Logger.logMessage("While attempting to refund eligible transactions for account.. could not find the account ID!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //log important debug log before attempting to communicate with processors
        Logger.logMessage("About to iterate through ("+eligibleTransactionsForRefund.size()+") eligble transactions for refund to reach the requested refund amount ("+requestedRefundAmount+") for verified account ID ("+verifiedAccountID+")", Logger.LEVEL.DEBUG);

        //set the actual refunded amount to 0 for starters
        BigDecimal actualRefundedAmount = new BigDecimal("0");

        //initialize these outside of the transaction loop
        Integer transactionLoopCounter = 0;
        BigDecimal actualRefundedAmountForThisTransaction;
        BigDecimal checkActualRefundedAmount;
        BigDecimal checkSingleTransactionRequestedRefundAmount;
        Integer lastEmployeePaymentMethodID = 0;
        AccountPaymentMethodModel accountPaymentMethodModel = null;

        //iterate through every eligible transaction for refund until we reach the requestedRefundAmount
        for (HashMap eligibleTransactionForRefund : eligibleTransactionsForRefund) {

            //reset this to zero before doing anything...
            actualRefundedAmountForThisTransaction = new BigDecimal("0");

            //grab required information off the transaction
            Integer transactionID = Integer.parseInt(eligibleTransactionForRefund.get("TRANSACTIONID").toString());
            String processorRefNum =  eligibleTransactionForRefund.get("PROCESSORREFNUM").toString();
            Integer employeePaymentMethodID = Integer.parseInt(eligibleTransactionForRefund.get("EMPLOYEEPAYMENTMETHODID").toString());
            Integer personID = CommonAPI.convertModelDetailToInteger(eligibleTransactionForRefund.get("PERSONID"));
            boolean fundedByPerson = (personID != null);

            //the single transaction amount is the EXACT amount we are going to attempt to refund from Stripe (which is the EXACT amount of what is eligible to refund for this transaction)
            BigDecimal singleTransactionRequestedRefundAmount = new BigDecimal(eligibleTransactionForRefund.get("ELIGIBLEREFUNDAMOUNT").toString());

            //log information before we get started
            Logger.logMessage("Attempting to refund transaction via processor - transaction details are...", Logger.LEVEL.DEBUG);
            Logger.logMessage("QC Transaction ID: "+transactionID, Logger.LEVEL.DEBUG);
            Logger.logMessage("Processor Charge Reference Number: "+processorRefNum, Logger.LEVEL.DEBUG);
            Logger.logMessage("Employee Payment Method ID: "+employeePaymentMethodID, Logger.LEVEL.DEBUG);
            Logger.logMessage("Eligible Refund Amount (Single Transaction Requested Refund Amount): "+singleTransactionRequestedRefundAmount, Logger.LEVEL.DEBUG);
            Logger.logMessage("Funded by Person?: "+ (fundedByPerson ? "true" : "false"), Logger.LEVEL.DEBUG);

            //attempt to refund this transaction against the processor
            try {

                //if the payment method is different than the last transaction...
                if (!lastEmployeePaymentMethodID.equals(employeePaymentMethodID)) {

                    //create an account payment method model for whatever the account payment method model was for this transaction
                    if ( !fundedByPerson ) {
                        accountPaymentMethodModel = new AccountPaymentMethodModel(employeePaymentMethodID, true);
                        setAccountPaymentMethod(accountPaymentMethodModel);
                    } else {
                        accountPaymentMethodModel = new AccountPaymentMethodModel(employeePaymentMethodID, verifiedAccountID, true);
                        setAccountPaymentMethod(accountPaymentMethodModel);
                    }

                    //check the result of accountPaymentMethodModel, may be null based on the DelayDTM
                    if ( accountPaymentMethodModel.getId() == null) {
                        Logger.logMessage("Transaction could not be refunded due to Payment Method's DelayAutoFundingUntilDTM value.", Logger.LEVEL.WARNING);
                        eligibleTransactionForRefund.put("PROCESSEDREFUNDAMOUNT", actualRefundedAmountForThisTransaction);
                        continue;
                    }

                    //set the funding method type to automatic
                    accountPaymentMethodModel.setFundingMethod("auto-refund");

                    //set this so we don't keep looking up the same account payment method model over and over and over
                    lastEmployeePaymentMethodID = employeePaymentMethodID;

                }

                //add the singleTransactionRequestedRefundAmount to the actualRefundedAmount to a temp variable "checkTotalActualRefundedAmount"
                checkActualRefundedAmount = actualRefundedAmount.add(singleTransactionRequestedRefundAmount);

                //check checkActualRefundedAmount to prevent over refunding the account
                if (checkActualRefundedAmount.compareTo(requestedRefundAmount) > 0) { //if positive, that means the checkActualRefundedAmount is larger than the requestedRefundAmount

                    //to remedy the situation, we will subtract the actualRefundedAmount from the requestedRefundAmount to figure out how much we have to go and then do a "partial" refund
                    checkSingleTransactionRequestedRefundAmount = requestedRefundAmount.subtract(actualRefundedAmount);

                    //NOTE: if we add the singleTransactionRequestedRefundAmount in it's current state we will refund more than what was requested, we don't want to do that -jrmitaly 3/10/2017
                    //set the "check" to the singleTransactionRequestedRefundAmount - this allows us to perform a partial refund for the processor by sending over LESS than the total of the transaction
                    singleTransactionRequestedRefundAmount = checkSingleTransactionRequestedRefundAmount;
                    Logger.logMessage("Prepared a partial refund amount of ("+checkSingleTransactionRequestedRefundAmount+") for this transaction from the original amount of ("+singleTransactionRequestedRefundAmount+")", Logger.LEVEL.DEBUG);

                }

                //attempt to refund this specific QC Transaction via the processor
                actualRefundedAmountForThisTransaction = createProcessorRefund(processorRefNum, singleTransactionRequestedRefundAmount, accountPaymentMethodModel);

                //put the processor refund reference number for this transaction on this transaction's hashmap (we need it later for doing the QC refund)
                eligibleTransactionForRefund.put("PROCESSORREFUNDREFNUMBER", getProcessorRefNum());

                //put the actual refunded amount for this transaction on this transaction's hashmap (we need it later for doing the QC refund)
                eligibleTransactionForRefund.put("PROCESSEDREFUNDAMOUNT", actualRefundedAmountForThisTransaction);
            } catch (FundingException ex) {
                Logger.logMessage("Exception occurred when attempting to communicate with processor during refund! Logging exception and continuing on to the next transaction...", Logger.LEVEL.ERROR);
                Logger.logException(ex); //always log exceptions
                throw new FundingException();
            } catch (Exception ex) {
                Logger.logMessage("Exception occurred when attempting to communicate with processor during refund! Logging exception and continuing on to the next transaction...", Logger.LEVEL.ERROR);
                Logger.logException(ex); //always log exceptions

                //set this so we know to skip over this one later on when attempting to create QC refunds
                actualRefundedAmountForThisTransaction = new BigDecimal("0");
                eligibleTransactionForRefund.put("PROCESSEDREFUNDAMOUNT", actualRefundedAmountForThisTransaction);
            }

            //add the actual refunded amount for this transaction to the total refunded
            actualRefundedAmount = actualRefundedAmount.add(actualRefundedAmountForThisTransaction);

            //if the actualRefundedAmount is equal to OR greater than requestedRefundAmount then stop iterating
            if (actualRefundedAmount.compareTo(requestedRefundAmount) == 0 || actualRefundedAmount.compareTo(requestedRefundAmount) > 0) { //if positive, that means the actualRefundedAmount is larger than the requestedRefundAmount
                if (eligibleTransactionsForRefund.size() > 1) {
                    eligibleTransactionsForRefund.subList(transactionLoopCounter+1, eligibleTransactionsForRefund.size()).clear();
                }
                break;
            } else {
                transactionLoopCounter++;
            }
        }

        //set the actual refunded amount on this funding model
        setProcessedRefundAmount(actualRefundedAmount);

        //check if we were able to successfully refund everything that was requested! log accordingly..
        if (getProcessedRefundAmount().compareTo(requestedRefundAmount) == 0 || getProcessedRefundAmount().compareTo(requestedRefundAmount) > 0) {
            Logger.logMessage("Requested Refund Reached! Successfully went through ("+eligibleTransactionsForRefund.size()+") eligble transactions and refunded ("+getProcessedRefundAmount()+") out of ("+requestedRefundAmount+") for verified account ID ("+verifiedAccountID+")", Logger.LEVEL.DEBUG);
        } else {
            Logger.logMessage("Requested Refund was not reached. Successfully went through ("+eligibleTransactionsForRefund.size()+") eligble transactions and refunded ("+getProcessedRefundAmount()+") out of ("+requestedRefundAmount+") for verified account ID ("+verifiedAccountID+")", Logger.LEVEL.DEBUG);
        }

        return eligibleTransactionsForRefund;
    }

    //UNDERLOAD - attempts to refund against the processor - returns valid processed refund amount if successful
    private BigDecimal createProcessorRefund(String processorRefNum, AccountPaymentMethodModel accountPaymentMethod) throws Exception {
        return createProcessorRefund(processorRefNum, null, accountPaymentMethod);
    }

    //attempts to refund against the processor - returns valid processed refund amount if successful
    private BigDecimal createProcessorRefund(String processorRefNum, BigDecimal singleTransactionRequestedRefundAmount, AccountPaymentMethodModel accountPaymentMethod) throws Exception {

        //method variable - the exact amount that was refunded and processed by the processor for the passed in account payment method
        BigDecimal processedRefundAmount = null;

        //determine the processor and charge appropriately
        if (accountPaymentMethod.getPaymentProcessorID() == 1) { //STRIPE
            processedRefundAmount = createStripeRefund(processorRefNum, singleTransactionRequestedRefundAmount, accountPaymentMethod);
        } else if (accountPaymentMethod.getPaymentProcessorID() == 6) {
            processedRefundAmount = createFreedomPayRefund(processorRefNum, singleTransactionRequestedRefundAmount, accountPaymentMethod);
        } else {
            Logger.logMessage("Did not attempt to refund payment processor - handling for payment processor does not exist -for account ID# "+accountPaymentMethod.getAccountID(), Logger.LEVEL.WARNING);
        }

        return processedRefundAmount;
    }

    //attempts to refund against Stripe - returns valid processed refund amount if successful
    private BigDecimal createStripeRefund(String stripeChargeReferenceNumber, BigDecimal singleTransactionRequestedRefundAmount, AccountPaymentMethodModel accountPaymentMethod) throws Exception {

        //method variable - the exact amount that was refunded and processed by the processor for the passed in account payment method
        BigDecimal processedRefundAmount = null;

        //throw missing data exception if required data is missing
        if (stripeChargeReferenceNumber == null) {
            Logger.logMessage("Missing stripe charge reference number for processing a Stripe Refund. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        if (accountPaymentMethod == null) {
            Logger.logMessage("Missing all payment method information for processing a Stripe Refund. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //the actual refunded amount is not required for Stripe - if not passed it will refund the entire amount of the charge
        Integer singleTransactionRequestedRefundAmountAsPennies = null;
        if (singleTransactionRequestedRefundAmount == null) {
            Logger.logMessage("Specific refund amount not specified for Stripe Refund. Stripe will refund whatever it can on the charge. Continuing on...", Logger.LEVEL.WARNING);
        } else {
            //Stripe requires that the chargeAmount sent to them is in "penny" format ($15.00 should be 1500)
            singleTransactionRequestedRefundAmountAsPennies = convertAmountFromDollarsToPennies(singleTransactionRequestedRefundAmount);
        }

        //throw missing data exception if required data is missing
        String processorPaymentToken = accountPaymentMethod.getProcessorPaymentToken();
        if (processorPaymentToken == null) {
            Logger.logMessage("Missing Stripe Customer ID (token) for processing a Stripe Refund. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        Integer accountID = accountPaymentMethod.getAccountID();
        if (accountID == null) {
            Logger.logMessage("Missing QC Account ID for processing a Stripe Refund. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        String privateAPIKey = accountPaymentMethod.retrievePrivateAPIKeySafely();
        if (privateAPIKey == null || privateAPIKey.trim().isEmpty()) {
            Logger.logMessage("Missing private API key for processing a Stripe Refund. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //log before attempting to communicate with Stripe...
        Logger.logMessage("Attempting to refund Stripe Charge Reference# ("+stripeChargeReferenceNumber+") Stripe Customer ("+processorPaymentToken+") for account ID# "+accountPaymentMethod.getAccountID(), Logger.LEVEL.TRACE);
        Logger.logMessage("Payment Method ID: "+accountPaymentMethod.getId(), Logger.LEVEL.DEBUG);
        Logger.logMessage("Payment Method Type: "+accountPaymentMethod.getPaymentMethodType(), Logger.LEVEL.DEBUG);

        if (singleTransactionRequestedRefundAmountAsPennies != null) {
            Logger.logMessage("Single Transaction Requested Refund Amount: "+singleTransactionRequestedRefundAmount, Logger.LEVEL.DEBUG);
            Logger.logMessage("Single Transaction Requested Refund Amount (as pennies): "+singleTransactionRequestedRefundAmountAsPennies, Logger.LEVEL.DEBUG);
        }

        //attempt to communicate with Stripe API
        try {

            //set the privateAPIKey on the Stripe Object
            Stripe.apiKey = privateAPIKey;

            //set "charge" (stripeChargeReferenceNumber) in temp hashmap
            Map<String, Object> refundParams = new HashMap<String, Object>();
            refundParams.put("charge", stripeChargeReferenceNumber);
            if (singleTransactionRequestedRefundAmountAsPennies != null) {
                refundParams.put("amount", singleTransactionRequestedRefundAmountAsPennies);
            }

            //charge the account by making a request to Stripe API
            Refund refund = Refund.create(refundParams);

            //set the stripe charge reference number (must store - required for doing refunds)
            setProcessorRefNum(refund.getId());

            //convert the processed amount back to dollars
            processedRefundAmount = convertAmountFromPenniesToDollars(refund.getAmount());

            //log after communicating with Stripe...
            Logger.logMessage("Successfully refunded ("+processedRefundAmount+") to Stripe Customer ("+accountPaymentMethod.getProcessorPaymentToken()+") for account ID# "+accountPaymentMethod.getAccountID(), Logger.LEVEL.TRACE);
            Logger.logMessage("Stripe Refund Reference Number: "+refund.getId(), Logger.LEVEL.TRACE);
            Logger.logMessage("Stripe Refund Status: "+refund.getStatus(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Processed Refunded Amount (as pennies): "+refund.getAmount(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Payment Method ID: "+accountPaymentMethod.getId(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Payment Method Type: "+accountPaymentMethod.getPaymentMethodType(), Logger.LEVEL.DEBUG);

        } catch (CardException stripeCardEx) {
            //log stripe exception - means the card was declined or something similar
            Logger.logMessage("Card Exception from Stripe API. Card may have been declined.", Logger.LEVEL.ERROR);
            Logger.logMessage("Status is: " + stripeCardEx.getCode(), Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeCardEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeCardEx);

            // Update EmployeePaymentMethod and increment FailedCardRefundAttempt field
            updateRefundFailureCount();

            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingException(stripeCardEx.getMessage());
        } catch (RateLimitException stripeRateLimitEx) {
            //log stripe exception - means too many requests made to the API too quickly
            Logger.logMessage("Rate Limit Exception from Stripe API. Too many requests made to the API too quickly.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeRateLimitEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeRateLimitEx);

            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingCardException("Too many requests made to the API too quickly"); //stripe error msg is not presentable to users
        } catch (InvalidRequestException stripeInvalidRequestEx) {
            //log stripe exception - means invalid parameters were supplied to Stripe's API
            Logger.logMessage("Invalid Request Exception from Stripe API. Means invalid parameters were sent to Stripe's API.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeInvalidRequestEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeInvalidRequestEx);

            // Update EmployeePaymentMethod and increment FailedCardRefundAttempt field
            updateRefundFailureCount();

            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingCardException("Invalid request was sent to Stripe's API");
        } catch (AuthenticationException stripeAuthEx) {
            //log stripe exception - Authentication with Stripe's API failed
            Logger.logMessage("Stripe Authentication Exception from Stripe API. Authentication with Stripe's API failed.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeAuthEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeAuthEx);

            // Update EmployeePaymentMethod and increment FailedCardRefundAttempt field
            updateRefundFailureCount();

            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingCardException("Authentication with Stripe API failed");
        } catch (APIConnectionException stripeAPIConnectionEx) {
            //log stripe exception - Network communication with Stripe failed
            Logger.logMessage("Stripe Authentication Exception from Stripe API. Network communication with Stripe failed.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeAPIConnectionEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeAPIConnectionEx);

            // Update EmployeePaymentMethod and increment FailedCardRefundAttempt field
            updateRefundFailureCount();

            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingCardException("Network communication with Stripe failed");
        } catch (StripeException stripeEx) {
            //log stripe exception - generic exception
            Logger.logMessage("Stripe Authentication Exception from Stripe API. Generic error.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeEx);

            // Update EmployeePaymentMethod and increment FailedCardRefundAttempt field
            updateRefundFailureCount();

            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingCardException("Authentication with Stripe API failed");
        }

        return processedRefundAmount;
    }

    private BigDecimal createFreedomPayRefund(String chargeReferenceNumber, BigDecimal singleTransactionRequestedRefundAmount, AccountPaymentMethodModel accountPaymentMethod) throws Exception {

        //method variable - the exact amount that was refunded and processed by the processor for the passed in account payment method
        BigDecimal processedRefundAmount;

        //throw missing data exception if required data is missing
        if (chargeReferenceNumber == null) {
            Logger.logMessage("Missing FreedomPay charge referenece number for processing a FreedomPay Refund. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        if (accountPaymentMethod == null) {
            Logger.logMessage("Missing all payment method information for processing a FreedomPay Refund. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //the actual refunded amount is required for FreedomPay
        if (singleTransactionRequestedRefundAmount == null) {
            Logger.logMessage("Specific refund amount not specified for FreedomPay Refund.", Logger.LEVEL.WARNING);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        String processorPaymentToken = accountPaymentMethod.getProcessorPaymentToken();
        if (processorPaymentToken == null) {
            Logger.logMessage("Missing FreedomPay Customer (token) for processing a FreedomPay Refund. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        Integer accountID = accountPaymentMethod.getAccountID();
        if (accountID == null) {
            Logger.logMessage("Missing QC Account ID for processing a FreedomPay Refund. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if required data is missing
        String privateAPIKey = accountPaymentMethod.retrievePrivateAPIKeySafely();
        if (privateAPIKey == null) {
            Logger.logMessage("Missing private API key for processing a FreedomPay Refund. Aborting...", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //log before attempting to communicate with Stripe...
        Logger.logMessage("Attempting to refund FreedomPay Charge Reference# ("+chargeReferenceNumber+") FreedomPay Customer ("+processorPaymentToken+") for account ID# "+accountPaymentMethod.getAccountID());
        Logger.logMessage("Payment Method ID: "+accountPaymentMethod.getId(), Logger.LEVEL.DEBUG);
        Logger.logMessage("Payment Method Type: "+accountPaymentMethod.getPaymentMethodType(), Logger.LEVEL.DEBUG);
        Logger.logMessage("Single Transaction Requested Refund Amount: "+singleTransactionRequestedRefundAmount, Logger.LEVEL.DEBUG);

        //attempt to communicate with FreedomPay API
        try {
            FreedomPayHelper freedomPayHelper = new FreedomPayHelper();
            freedomPayHelper.setStoreID(accountPaymentMethod.getPaymentProcessorStoreNum());
            freedomPayHelper.setTerminalID(accountPaymentMethod.getPaymentProcessorTerminalNum());
            processedRefundAmount = freedomPayHelper.refund(chargeReferenceNumber,singleTransactionRequestedRefundAmount);

            //set the stripe charge reference number (must store - required for doing refunds)
            setProcessorRefNum(freedomPayHelper.getResponseID());

            //log after communicating with FreedomPay...
            Logger.logMessage("Successfully refunded ("+processedRefundAmount+") to Customer ("+accountPaymentMethod.getProcessorPaymentToken()+") for account ID# "+accountPaymentMethod.getAccountID(), Logger.LEVEL.TRACE);
            Logger.logMessage("Payment Method ID: "+accountPaymentMethod.getId(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Payment Method Type: "+accountPaymentMethod.getPaymentMethodType(), Logger.LEVEL.DEBUG);

        } catch (Exception ex) {
            //log stripe exception - means the card was declined or something similar
            Logger.logMessage("Card Exception from FreedomPay API.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + ex.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(ex);

            //updateFailedFundingAttempts(accountPaymentMethod, ex.getMessage());
            updateRefundFailureCount();

            //throw generic REST Client exception since we have logged the Stripe Exception
            throw new FundingException(ex.getMessage());
        }

        return processedRefundAmount;
    }

    //creates a QC Transaction that "refunds" a list of existing QC Transactions for any account passed in - returns total refunded funded amount if successful
    private BigDecimal refundAccount(ArrayList<HashMap> qcTransactionsToRefund, Integer verifiedAccountID) throws Exception {

        //throw missing data exception if qcTransactionsToRefund could not be determined
        if (qcTransactionsToRefund == null) {
            Logger.logMessage("While refunding account.. could not find valid qc transactions to refund!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if verifiedAccountID could not be determined
        if (verifiedAccountID == null) {
            Logger.logMessage("While refunding account.. could not find the account ID!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //initialize the totalRefundedAmount at 0
        BigDecimal totalRefundedAmount = new BigDecimal("0");

        //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
        //determine the terminal for this refunding
        Integer refundingTerminalID = FundingHelper.getMyQCFundingTerminalID(verifiedAccountID);
        setFundingTerminalID(refundingTerminalID);

        //determine the QC User responsible for this funding
        Integer refundTransactionQCUserID = null;

        //ideally, we would have the logged in user ID and it would be set to the manager who is performing the refund
        if (getLoggedInUserID() != null) {
            refundTransactionQCUserID = getLoggedInUserID();
        } else { //the My QC User ID is probably better than a NULL QC User ID here, at least then we would know it's related to My QC somehow
            refundTransactionQCUserID = commonMMHFunctions.determineMyQCUserID();
        }

        //iterate through every processed refund transaction and preform a QC refund for them
        for (HashMap qcTransactionToRefund : qcTransactionsToRefund) {

            //determine the processed refund amount - this is the amount the new QC_Transaction should be for
            BigDecimal processedRefundAmountForThisTransaction = new BigDecimal(qcTransactionToRefund.get("PROCESSEDREFUNDAMOUNT").toString());

            //we don't ever refund a QC transaction unless we are sure the processor has funded their payment method!
            if (processedRefundAmountForThisTransaction.compareTo(BigDecimal.ZERO) == 0) {
                Logger.logMessage("Transaction to refund did not provide a valid processed refund amount. Did something go wrong with the processor? Check upstream logs.", Logger.LEVEL.ERROR);
            } else {

                //grab required information off the transaction
                String processorRefundRefNumber = qcTransactionToRefund.get("PROCESSORREFUNDREFNUMBER").toString();
                Integer originalPaymentMethodID =  Integer.parseInt(qcTransactionToRefund.get("EMPLOYEEPAYMENTMETHODID").toString());
                Integer originalTransactionID =  Integer.parseInt(qcTransactionToRefund.get("TRANSACTIONID").toString());
                Integer personID = CommonAPI.convertModelDetailToInteger(qcTransactionToRefund.get("PERSONID"));

                //log information before we get started
                Logger.logMessage("Attempting to refund QC transaction via QC - transaction details are...", Logger.LEVEL.DEBUG);
                Logger.logMessage("Original Transaction ID (the transaction we are refunding now): "+originalTransactionID, Logger.LEVEL.DEBUG);
                Logger.logMessage("Original Payment Method ID (the payment method we just refunded): "+originalPaymentMethodID, Logger.LEVEL.DEBUG);
                Logger.logMessage("Processor Refund Reference Number: "+processorRefundRefNumber, Logger.LEVEL.DEBUG);
                Logger.logMessage("Processed Refund Amount for this Transaction: "+processedRefundAmountForThisTransaction, Logger.LEVEL.DEBUG);

                //first, create a new "refund" QC_Transaction for each refunded transaction
                ArrayList<HashMap> insertedTransList = dm.parameterizedExecuteQuery("data.common.funding.createRefundTransaction",
                        new Object[]{
                                verifiedAccountID,
                                refundingTerminalID,
                                processedRefundAmountForThisTransaction,
                                refundTransactionQCUserID,
                                originalPaymentMethodID,
                                processorRefundRefNumber,
                                originalTransactionID,
                                personID
                        },
                        true
                );

                //determine and check insertedTransactionID - if the insert result does not return as a positive integer - something went wrong
                HashMap insertedTransHM = insertedTransList.get(0);
                Integer insertedTransactionID = Integer.parseInt(insertedTransHM.get("TRANSACTIONID").toString());
                if (insertedTransactionID < 0) {
                    Logger.logMessage("Could not determine inserted transaction ID for refunding transaction - For account ID# "+accountPaymentMethod.getAccountID(), Logger.LEVEL.ERROR);
                    throw new FundingException();
                } else {
                    //set inserted transaction ID
                    setTransactionID(insertedTransactionID);
                }

            }

            //add the actual refunded amount for this transaction to the total refunded
            totalRefundedAmount = totalRefundedAmount.add(processedRefundAmountForThisTransaction);

        }

        return totalRefundedAmount;
    }

    //method that retrieves the "default" account payment method model for the authenticated account
    private AccountPaymentMethodModel getDefaultAccountPaymentMethod(Integer authenticatedEmployeeID) throws Exception {
        return getDefaultAccountPaymentMethod(authenticatedEmployeeID, null);
    }

    private AccountPaymentMethodModel getDefaultAccountPaymentMethod(Integer authenticatedEmployeeID, Integer personID) throws Exception {
        //method variable - the default payment method for the passed in authenticated account
        AccountPaymentMethodModel defaultAccountPaymentMethod = null;

        //throw InvalidAuthException if no authenticated employee is found
        if (authenticatedEmployeeID == null) {
            throw new InvalidAuthException();
        }

        //if a personID is supplied, then the default payment method will be for the person account and NOT the account
        boolean forPerson = (personID != null);
        String getPaymentMethodSQL = !forPerson ? "data.common.funding.getDefaultAccountPaymentMethod" :  "data.common.funding.getDefaultPersonAccountPaymentMethod";
        Integer paymentMethodAccountID = !forPerson ? authenticatedEmployeeID : personID;

        //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
        //get all payment methods for this account in an array list
        ArrayList<HashMap> accountPaymentMethodList = dm.parameterizedExecuteQuery(getPaymentMethodSQL,
                new Object[]{ paymentMethodAccountID }, true
        );

        //iterate through list, create models and add them to the collection
        if (accountPaymentMethodList != null && accountPaymentMethodList.size() == 1) {
            HashMap accountPaymentMethodHM = accountPaymentMethodList.get(0);
            if (accountPaymentMethodHM.get("EMPLOYEEPAYMENTMETHODID") != null && !accountPaymentMethodHM.get("EMPLOYEEPAYMENTMETHODID").toString().isEmpty()) {
                if ( forPerson ) {
                    //if for person, then the employeeID will not be set, so set it
                    accountPaymentMethodHM.put("EMPLOYEEID", authenticatedEmployeeID);

                    //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
                    //the api keys will not be set, retrieve and set them
                    accountPaymentMethodHM = FundingHelper.setPaymentProcessorCredentials(accountPaymentMethodHM);
                }

                defaultAccountPaymentMethod = new AccountPaymentMethodModel(accountPaymentMethodHM);
            } else {
                Logger.logMessage("Found too many default payment methods when attempting to fund account ID# "+authenticatedEmployeeID, Logger.LEVEL.ERROR);
                throw new MissingDataException();
            }
        }

        //throw missing data exception if defaultAccountPaymentMethod could not be determined
        if (defaultAccountPaymentMethod == null) {
            Logger.logMessage("Could not find default payment method for account ID# "+authenticatedEmployeeID, Logger.LEVEL.ERROR);
            throw new FundingException("deactivated", true);
        }

        //if there is a fee, but no surcharge, error appropriately
        if ( defaultAccountPaymentMethod.getTerminalHasFundingFee() && defaultAccountPaymentMethod.getPASurchargeID() == null ) {
            Logger.logMessage("No surcharge set for terminal configured to charge fees, for account ID# "+authenticatedEmployeeID+" , payment method ID# "+defaultAccountPaymentMethod.getId().toString(), Logger.LEVEL.ERROR);
            throw new FundingException("Store is not properly configured for surcharges");
        }

        //set default account method on this instance of FundingModel
        setAccountPaymentMethod(defaultAccountPaymentMethod);

        return defaultAccountPaymentMethod;
    }

    private AccountPaymentMethodModel getAutoFundingPaymentMethod(Integer authenticatedEmployeeID) throws Exception {

        //method variable - the default payment method for the passed in authenticated account
        AccountPaymentMethodModel autoFundingPaymentMethod = null;

        //throw InvalidAuthException if no authenticated employee is found
        if (authenticatedEmployeeID == null) {
            throw new InvalidAuthException();
        }

        //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
        //get all payment methods for this account in an array list
        ArrayList<HashMap> accountPaymentMethodList = dm.parameterizedExecuteQuery("data.common.funding.getAccountAutoFundingPaymentMethod",
                new Object[]{ authenticatedEmployeeID }, true
        );

        //iterate through list, create models and add them to the collection
        if (accountPaymentMethodList != null && accountPaymentMethodList.size() == 1) {
            HashMap accountPaymentMethodHM = accountPaymentMethodList.get(0);
            if (accountPaymentMethodHM.get("EMPLOYEEPAYMENTMETHODID") != null && !accountPaymentMethodHM.get("EMPLOYEEPAYMENTMETHODID").toString().isEmpty()) {
                autoFundingPaymentMethod = new AccountPaymentMethodModel(accountPaymentMethodHM);
            } else {
                Logger.logMessage("Found too many default payment methods when attempting to fund account ID# "+authenticatedEmployeeID, Logger.LEVEL.ERROR);
                throw new MissingDataException();
            }
        }

        //throw missing data exception if autoFundingPaymentMethod could not be determined
        if (autoFundingPaymentMethod == null) {
            Logger.logMessage("Could not find default payment method for account ID# "+authenticatedEmployeeID, Logger.LEVEL.ERROR);
            throw new FundingException("deactivated", true);
        }

        //set default account method on this instance of FundingModel
        setAccountPaymentMethod(autoFundingPaymentMethod);

        return autoFundingPaymentMethod;
    }

    //converts a BigDecimal "chargeAmount" to pennies (cents) - this needed by certain processors (such as Stripe)
    private Integer convertAmountFromDollarsToPennies(BigDecimal chargeAmount) {

        //ensure that BigDecimal is properly rounded before attempting this
        BigDecimal roundedChargeAmount = chargeAmount.setScale(2, BigDecimal.ROUND_CEILING);

        //multiple the newly rounded funding amount by 100 (convert from "dollars" to "pennies")
        BigDecimal bigDecimalInPennies = roundedChargeAmount.multiply(new BigDecimal(100));

        //use int value exact to convert the BigDecimal to an Integer
        return bigDecimalInPennies.intValueExact();
    }

    //for manual funding, method updates number attempts on payment method and sends corresponding email if payment method is deactivated or not
    public Integer updateFailedFundingAttempts(AccountPaymentMethodModel accountPaymentMethod, String stripeErrorMsg) {

        Integer active = 1;
        Integer isDefault = 1;

        //determine the allowed max attempts and delay time
        setMaxAttemptsAndDelayHours();

        if (accountPaymentMethod.getId() != null) {

            //get the number of attempts on the payment method and payment method id
            Integer employeeAttempts = accountPaymentMethod.getFailedCardAttempts();
            Integer employeePaymentMethodID = accountPaymentMethod.getId();
            Integer employeeID = accountPaymentMethod.getAccountID();

            LocalDateTime localDateTime = LocalDateTime.now();
            LocalDateTime updatedTime = LocalDateTime.now();

            //increment the attempts
            employeeAttempts++;

            Logger.logMessage("Account payment method - "+accountPaymentMethod.getFundingMethod(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Stripe error message - "+stripeErrorMsg, Logger.LEVEL.DEBUG);

            if(employeeAttempts < getFailedCardMaxAttempts()) {  //if there are attempts still available
                if(accountPaymentMethod.getFundingMethod().equals("auto")) {
                    updatedTime = localDateTime.plusHours(getFailedCardDelayHours());  //automatic funding gets delayed
                    sendCardRelatedIssueEmail(accountPaymentMethod, updatedTime, false, stripeErrorMsg);
                } else if (accountPaymentMethod.getFundingMethod().equals("auto-refund")) {
                    updatedTime = localDateTime.plusHours(getFailedCardDelayHours());  //update delay time to try again in {24} hours & send email
                    sendCardRelatedIssueEmail(accountPaymentMethod, updatedTime, true, stripeErrorMsg);
                } else if (accountPaymentMethod.getFundingMethod().equals("manual"))  {
                    sendCardRelatedIssueEmail(accountPaymentMethod, null, false, stripeErrorMsg);  //manual funding does NOT get delayed
                }
            } else {  //otherwise maxed out number of attempts
                try { //needed to surround with try catch because of method throw exceptions
                    active = 0;  //deactivate the account
                    isDefault = 0;
                    if (accountPaymentMethod.getFundingMethod().equals("auto-refund")) {
                        sendDeactivatedPaymentMethodEmail(accountPaymentMethod, true, stripeErrorMsg);
                    } else {
                        sendDeactivatedPaymentMethodEmail(accountPaymentMethod, false, stripeErrorMsg);
                    }
                    FundingHelper.disableAutoReloadsForAllAccounts(employeePaymentMethodID);

                } catch (Exception ex) {
                    Logger.logMessage("Error: Could not send deactivated payment method email or disable automatic reload for the employeeID: " + employeeID, Logger.LEVEL.ERROR);
                    Logger.logException(ex);
                }
            }

            //update the account with the number of attempts, time delayed and the account status
            updatePaymentMethodInfo(employeePaymentMethodID, employeeAttempts, updatedTime.toString(), active, isDefault);

        }
        return active;
    }

    //get the max attempts and delay hours for failed cards from QC_Globals and set to global variables
    public void setMaxAttemptsAndDelayHours() {

        ArrayList<HashMap> cardAttemptInfo = dm.parameterizedExecuteQuery("data.common.funding.getMaxAttemptsAndDelayHours", new Object[]{}, true);

        //if the max attempts allowed and delay hours is set in QC_Globals
        if (cardAttemptInfo != null && cardAttemptInfo.size() == 1) {
            HashMap cardAttemptInfoHM = cardAttemptInfo.get(0);
            if (cardAttemptInfoHM.get("AUTOFUNDCARDRELATEDMAXATTEMPTS") != null
                    && cardAttemptInfoHM.get("AUTOFUNDCARDRELATEDDELAYHOURS") != null) {

                //set the global variables
                setFailedCardMaxAttempts(Integer.parseInt(cardAttemptInfoHM.get("AUTOFUNDCARDRELATEDMAXATTEMPTS").toString()));
                setFailedCardDelayHours(Integer.parseInt(cardAttemptInfoHM.get("AUTOFUNDCARDRELATEDDELAYHOURS").toString()));

            }
        }
    }

    public void updateRefundFailureCount(){
        // This method will update the Employee Payment Method and increment the FundingFailedRefundAttempt column
        // This indicates that an attempt to refund an employee manually or automatically has failed
        try {
            if(getAccountPaymentMethod().getAccountID() == null){
                Logger.logMessage("Error in method updateRefundFailureCount: AccountID is NULL", Logger.LEVEL.ERROR);
                return;
            }
            if(getAccountPaymentMethod().getId() == null){
                Logger.logMessage("Error in method updateRefundFailureCount: EmployeePaymentMethodID is NULL", Logger.LEVEL.ERROR);
                return;
            }

            // Retrieve and Set Failed Max Card Attempts
            setMaxAttemptsAndDelayHours();

            // Get Failed Refund Attempts and Increment
            int attempts = (getAccountPaymentMethod().getFailedCardRefundAttempts() != null ? getAccountPaymentMethod().getFailedCardRefundAttempts() : 0) + 1;
            getAccountPaymentMethod().setFailedCardRefundAttempts(attempts);

            Date delayDate = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(delayDate);
            calendar.add(Calendar.HOUR, getFailedCardDelayHours());
            delayDate = calendar.getTime();

            // Update FundingFailedRefundAttempt on EmployeePaymentMethod record
            dm.parameterizedExecuteNonQuery("data.funding.updateRefundFailureCount",new Object[]{
                    getAccountPaymentMethod().getId(),
                    attempts,
                    delayDate
            });

            // Check if Refund Attempts exceeds Limit
            if(attempts >= getFailedCardMaxAttempts()){
                updateEmployeeDisableRefunds(getAccountPaymentMethod().getAccountID());
            }

        } catch(Exception ex) {
            Logger.logMessage("Error in method FundingModel.updateRefundFailureCount()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    public void updateEmployeeDisableRefunds(Integer accountID){
        // This method will update the Employee and set the RefundingDisabled Flag - This should stop future refunding attempts
        // This indicates that an attempt to refund an employee manually or automatically has failed
        try {
            // Validate AccountID
            if(accountID == null){
                Logger.logMessage("Unable to Update Employee and set REFUNDINGDISABLED flag EmployeeID is NULL!", Logger.LEVEL.ERROR );
                return;
            }
            // Update Employee
            int result = dm.parameterizedExecuteNonQuery("data.funding.updateEmployeeDisableRefunds",new Object[]{accountID});
            if(result < 1){
                Logger.logMessage("Method updateEmployeeDisableRefunds did not update EmployeeID: " + getAccountPaymentMethod().getAccountID(), Logger.LEVEL.ERROR);
                return;
            } else {
                if(this.getAccountPaymentMethod() != null && this.getAccountPaymentMethod().getId() != null){
                    Integer epmID = this.getAccountPaymentMethod().getId();
                    result = dm.parameterizedExecuteNonQuery("data.funding.resetRefundFailureCount",new Object[]{epmID});
                    if(result < 1){
                        Logger.logMessage("Error when resetting FundingFailedRefund Attempt on EmployeePaymentMethodID: " +epmID , Logger.LEVEL.ERROR);
                        return;
                    }
                }
            }
            Logger.logMessage("Disabled Refunding for EmployeeID: "+accountID, Logger.LEVEL.TRACE);
        } catch(Exception ex) {
            Logger.logMessage("Error in method FundingModel.updateRefundFailureCount()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    //updates the employee's card account with the number of attempts, time delayed to and account status
    private void updatePaymentMethodInfo(Integer employeePaymentMethodID, Integer employeeAttempts, String employeeDelayTime, Integer active, Integer isDefault) {

        //note: if manual funding, delay time will be the current time right now (time doesn't matter). If automatic funding, delay time will be time in the future
        Integer insertResult = dm.parameterizedExecuteNonQuery("data.common.funding.updatePaymentMethodInfo",
                new Object[]{
                        employeePaymentMethodID,
                        employeeAttempts,
                        employeeDelayTime,
                        active,
                        isDefault
                }
        );
        if (insertResult != 1) {
            Logger.logMessage("Could not insert in FundingModel.updatePaymentMethodInfo", Logger.LEVEL.ERROR);
        }
    }

    //sends a card related issue email if there are still attempts left on the payment method
    private void sendCardRelatedIssueEmail(AccountPaymentMethodModel accountPaymentMethod, LocalDateTime employeeDelayTime, Boolean isRefund, String stripeErrorMsg) {

        Logger.logMessage("Reached sendCardRelatedIssueEmail function in FundingModel", Logger.LEVEL.DEBUG);

        String emailAddress = accountPaymentMethod.getPaymentMethodEmailAddress();
        String accountName = accountPaymentMethod.getPaymentMethodFullName();
        String paymentHtml;
        String paymentMsg = "";
        if ( stripeErrorMsg == null ) {
            stripeErrorMsg = "";
        } else {
            stripeErrorMsg = stripeErrorMsg.substring(0, stripeErrorMsg.length()-1);
        }

        //verify parameter passed into method
        if (emailAddress == null) {
            emailAddress = "";
            Logger.logMessage("While attempting to email recently funded account... could not determine email address!", Logger.LEVEL.ERROR);
        }

        //verify parameter passed into method
        if (accountName == null) {
            Logger.logMessage("While attempting to email recently funded account... could not determine account name!", Logger.LEVEL.ERROR);
        }

        if(employeeDelayTime != null) {
            //check if the time the email is sent (now) is before the delay time
            LocalDateTime now = LocalDateTime.now();
            if (now.isBefore(employeeDelayTime)) {
                //format the time and date for email appearance
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
                String time = (formatter.format(employeeDelayTime.toLocalTime()));
                String month = employeeDelayTime.getMonth().toString().substring(0,1) + employeeDelayTime.getMonth().toString().substring(1).toLowerCase();
                String date = month + "," + employeeDelayTime.getDayOfMonth() + " " + employeeDelayTime.getYear();
                paymentMsg = "Your payment method will be attempted again at approximately " + time + " on " + date;
            }
        } else {
            paymentMsg = "Please try contacting the provider of the payment method to see why there are card related issues";
        }

        paymentHtml = "<td style=\" -webkit-text-size-adjust: none; -ms-text-size-adjust: none; font-size: 12px; line-height: 18px;mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse;text-align:left;font-family: Arial, Helvetica, Geneva, sans-serif; color: #000000;\" >" +
                "<p style=\"-webkit-text-size-adjust: none; -ms-text-size-adjust: none; font-size: 12px; line-height: 18px; color: #000000; margin: 0;\">"+paymentMsg+".</p>" +
                "</td>" +
                "</tr>" +
                "<tr>";

        int emailTypeID = 8; //Account Funding - Automatic Funding Failure Email
        if(employeeDelayTime == null){
            emailTypeID = 11;  //Account Funding - Manual Funding Failure Email
        }
        // Check for isRefund flag
        if(isRefund) {
            emailTypeID = 9; //Account Funding - Automatic Refund Failure Email
        }

        // Build params HashMap
        HashMap<String, Object> params = new HashMap<>();
        if(this.personId != null) {
            params.put("PersonID", this.personId);
        }
        params.put("EmployeeID", accountPaymentMethod.getAccountID());
        params.put("PaymentHtml", paymentHtml);
        params.put("ErrorMessage", stripeErrorMsg);

        // Get EmployeeID from Funding Email
        int employeeID = commonMMHFunctions.getEmployeeIDFromFundingEmailAddress(emailAddress);
        if(employeeID > -1){params.put("EmployeeID", employeeID);}

        // Send Email
        commonMMHFunctions common = new commonMMHFunctions();
        if(!emailAddress.isEmpty()){
            if(common.sendTemplateEmail(emailAddress, 0, emailTypeID, "", params)){
                Logger.logMessage("Successfully sent emailTypeID: " + emailTypeID + " to " + emailAddress, Logger.LEVEL.TRACE);
            } else {
                Logger.logMessage("Failed to send e-mail to " + emailAddress, Logger.LEVEL.ERROR);
            }
        }
    }

    //send a deactivated payment method email if the attempts on the payment method are maxed out
    private void sendDeactivatedPaymentMethodEmail(AccountPaymentMethodModel accountPaymentMethod, Boolean isRefund, String stripeErrorMsg) {
        try {
            String emailAddress = accountPaymentMethod.getPaymentMethodEmailAddress();
            String accountName = accountPaymentMethod.getPaymentMethodFullName();
            if ( stripeErrorMsg == null ) {
                stripeErrorMsg = "";
            } else {
                stripeErrorMsg = stripeErrorMsg.substring(0, stripeErrorMsg.length()-1);
            }

            //verify parameter passed into method
            if (emailAddress == null) {
                Logger.logMessage("While attempting to email recently funded account... could not determine email address!", Logger.LEVEL.ERROR);
                return;
            }

            //verify parameter passed into method
            if (accountName == null) {
                Logger.logMessage("While attempting to email recently funded account... could not determine account name!", Logger.LEVEL.ERROR);
                return;
            }

            String errorMessage = "Your payment method on file has encountered a card related issue ("+ stripeErrorMsg +") and we were not able to process your payment";

            int emailTypeID = 10;
            String automaticFundingMessage = "Your payment method has been deactivated.  Please try contacting the provider of the payment method to see why there are card related issues.  Alternatively, you could try to add a different payment method";
            if(isRefund) {
                automaticFundingMessage = "Your payment method has been deactivated.  Please try contacting the provider of the payment method to see why there are card related issues.";
            }

            // Build params HashMap
            HashMap params = new HashMap();
            params.put("EmployeeID", accountPaymentMethod.getAccountID());
            params.put("ErrorMessage", errorMessage);
            params.put("PaymentHtml", automaticFundingMessage);

            // Send Email
            commonMMHFunctions common = new commonMMHFunctions();
            if(!emailAddress.isEmpty()){
                if(common.sendTemplateEmail(emailAddress, 0, emailTypeID, "", params)){
                    Logger.logMessage("Successfully sent deactivated payment method e-mail to " + emailAddress, Logger.LEVEL.TRACE);
                } else {
                    Logger.logMessage("Failed to send deactivated payment method e-mail to " + emailAddress, Logger.LEVEL.ERROR);
                }
            }
        } catch(Exception ex){
            Logger.logMessage("Error in method sendDeactivatedPaymentMethodEmail(): "+ex.getMessage());
            Logger.logException(ex);
        }
    }

    //converts a Integer "chargedAmount" from pennies (cents) to dollars  - this needed by certain processors (such as Stripe)
    private BigDecimal convertAmountFromPenniesToDollars(Long chargeAmount) {
        return BigDecimal.valueOf(chargeAmount).movePointLeft(2);
    }

    // OVERLOADING - checks if the AllowMyQCFunding flag is on for the account's payroll group, if its not don't show 'Fund Now' container
    public static boolean isAccountFundingAllowedForAccount(HttpServletRequest request) throws Exception {
        Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        return isAccountFundingAllowedForAccount(employeeID);
    }


    //checks if the AllowMyQCFunding flag is on for the account's payroll group, if its not don't show 'Fund Now' container
    public static boolean isAccountFundingAllowedForAccount(Integer employeeID) throws Exception {
        Boolean fundingAllowed = false;

        Object isFundingAllowedObj = dm.parameterizedExecuteScalar("data.common.funding.checkIfAccountFundingIsAllowedForAccount", new Object[] {employeeID});

        if ( isFundingAllowedObj != null ) {
            fundingAllowed = Boolean.parseBoolean(isFundingAllowedObj.toString());
        }

        return fundingAllowed;
    }

    private TransactionModel createFundingPATransaction(Integer fundingTerminalID, BigDecimal fundAmount, BigDecimal feeAmount) throws Exception{
        com.mmhayes.common.transaction.models.TransactionModel transactionModel = new com.mmhayes.common.transaction.models.TransactionModel();
        transactionModel.setApiActionEnum(PosAPIHelper.ApiActionType.ROA);

        //throw missing data exception if credit card tender on the funding terminal could not be determined
        if(getPaymentProcessorModel() == null) {
            Logger.logMessage("While recording funding transaction.. could not find payment processor information on funding terminal!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if credit card tender on the funding terminal could not be determined
        if (getPaymentProcessorModel().getCreditCardTenderID() == null) {
            Logger.logMessage("While recording funding transaction.. could not find a valid credit card tender on the funding terminal!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //throw missing data exception if received on account ID on the funding terminal could not be determined
        if (getPaymentProcessorModel().getReceivedOnAccountsID() == null) {
            Logger.logMessage("While recording funding transaction.. could not find a valid received on accounts on the funding terminal!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        transactionModel.setIsInquiry(false);

        //userID
        Integer userID = CommonAPI.getQCUserID();
        if (userID == null || userID == 0) {
            throw new MissingDataException();
        }

        transactionModel.setUserId(userID);
        transactionModel.setName(getAccountPaymentMethod().getPersonName());

        //set the Loyalty Account Information
        LoyaltyAccountModel loyaltyAccountModel = new LoyaltyAccountModel();
        loyaltyAccountModel.setId(getAccountPaymentMethod().getAccountID());
        loyaltyAccountModel.setHasFetchedAvailableRewards(false);
        transactionModel.setLoyaltyAccount(loyaltyAccountModel);

        //set submitted time string for use in submit transaction later
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        transactionModel.setTimeStamp(LocalDateTime.now().format(dtf));

        //order type
        transactionModel.setOrderType("NORMAL SALE");

        //transaction type
        transactionModel.setTransactionTypeId(11);
        transactionModel.setTransactionType("ROA");
        transactionModel.setApiActionEnum(PosAPIHelper.ApiActionType.ROA);

        //create the TerminalModel based on the terminalID and set the terminalID
        TerminalModel terminalModel = TerminalModel.createTerminalModel(fundingTerminalID, true);
        transactionModel.setTerminalId(fundingTerminalID);

        //get the account model
        AccountModel accountModel = new AccountModel();
        accountModel.setId(getAccountPaymentMethod().getAccountID());

        Integer creditCardTenderID = getPaymentProcessorModel().getCreditCardTenderID();
        Integer receivedOnAcctID = getPaymentProcessorModel().getReceivedOnAccountsID();

        //populate ROA
        ReceivedOnAccountModel receivedOnAccountModel = new ReceivedOnAccountModel();
        receivedOnAccountModel.setId(receivedOnAcctID);
        receivedOnAccountModel.setQC(true);

        ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel = new ReceivedOnAccountLineItemModel();
        receivedOnAccountLineItemModel.setItemId(receivedOnAcctID);
        receivedOnAccountLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.RECEIVEDONACCOUNT.toInt());
        receivedOnAccountLineItemModel.setReceivedOnAccount(receivedOnAccountModel);
        receivedOnAccountLineItemModel.setAccount(accountModel);

        receivedOnAccountLineItemModel.setQuantity(BigDecimal.ONE);
        receivedOnAccountLineItemModel.setAmount(fundAmount);
        receivedOnAccountLineItemModel.setExtendedAmount(fundAmount);

        //set the ROA on the TransactionModel
        transactionModel.getReceivedOnAccounts().add(receivedOnAccountLineItemModel);

        //the actual amount is the fund amount plus the fee, or just the fundAmount if fee is null
        BigDecimal actualAmount = fundAmount;

        if ( feeAmount != null && feeAmount.compareTo(BigDecimal.ZERO) > 0 ) {
            actualAmount = fundAmount.add(feeAmount);

            //throw missing data exception if received on account ID on the funding terminal could not be determined
            if (getPaymentProcessorModel().getPASurchargeID() == null) {
                Logger.logMessage("While recording funding transaction.. could not find a valid surcharge on the funding terminal!", Logger.LEVEL.ERROR);
                throw new MissingDataException();
            }

            Integer surchargeID = getPaymentProcessorModel().getPASurchargeID();

            //popular surcharge
            SurchargeLineItemModel surchargeLineItemModel = new SurchargeLineItemModel();
            surchargeLineItemModel.setItemId(surchargeID);
            surchargeLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.SURCHARGE.toInt());

            //validate Surcharge and set SurchargeDisplayModel
            SurchargeModel validatedSurchargeModel = SurchargeModel.getSurchargeModel(surchargeID, terminalModel);
            SurchargeDisplayModel surchargeDisplayModel = SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel);
            surchargeLineItemModel.setSurcharge(surchargeDisplayModel);

            surchargeLineItemModel.setQuantity(BigDecimal.ONE);
            surchargeLineItemModel.setAmount(feeAmount);
            surchargeLineItemModel.setExtendedAmount(feeAmount);
            surchargeLineItemModel.setEligibleAmount(fundAmount);

            //set the Surcharge on the TransactionModel
            transactionModel.getSurcharges().add(surchargeLineItemModel);
        }

        //populate tender
        TenderDisplayModel tenderDisplayModel = new TenderDisplayModel();
        tenderDisplayModel.setId(creditCardTenderID); //set TenderID as the Credit Card Tender on the Funding Terminal

        TenderLineItemModel tenderLineItemModel = new TenderLineItemModel();
        tenderLineItemModel.setTender(tenderDisplayModel);
        tenderLineItemModel.setItemId(creditCardTenderID);
        tenderLineItemModel.setItemTypeId(PosAPIHelper.ObjectType.TENDER.toInt());

        tenderLineItemModel.setAmount(getProcessedAmount());
        tenderLineItemModel.setExtendedAmount(actualAmount);
        tenderLineItemModel.setQuantity(BigDecimal.ONE);

        //get and set credit card transaction info
        String creditCardTransInfo = CommonAPI.getCreditCardTransInfo(getAccountPaymentMethod(), false);
        tenderLineItemModel.setCreditCardTransInfo(creditCardTransInfo);
        // Adding Payment Method ID to tenderLineItemModel - Is already encoded in creditCardTransInfo
        tenderLineItemModel.setPaymentMethodTypeId(getAccountPaymentMethod().getPaymentMethodTypeID());

        //set the account model on the ROA
        tenderLineItemModel.setAccount(accountModel);

        //set the Tender on the TransactionModel
        transactionModel.getTenders().add(tenderLineItemModel);

        //build and save the transaction
        TransactionBuilder transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, transactionModel, PosAPIHelper.ApiActionType.ROA, PosAPIHelper.TransactionType.ROA);
        transactionBuilder.getTransactionModel().setIsInquiry(true);
        transactionBuilder.buildTransaction();
        transactionBuilder.saveTransaction();

        return transactionModel;
    }

    public AccountPaymentMethodModel getAccountPaymentMethod() {
        return accountPaymentMethod;
    }

    public void setAccountPaymentMethod(AccountPaymentMethodModel accountPaymentMethod) {
        this.accountPaymentMethod = accountPaymentMethod;
    }

    public BigDecimal getProcessedAmount() {
        return processedAmount;
    }

    public void setProcessedAmount(BigDecimal processedAmount) {
        this.processedAmount = processedAmount;
    }

    public BigDecimal getFundedAmount() {
        return fundedAmount;
    }

    public void setFundedAmount(BigDecimal fundedAmount) {
        this.fundedAmount = fundedAmount;
    }

    public BigDecimal getProcessedRefundAmount() {
        return processedRefundAmount;
    }

    public void setProcessedRefundAmount(BigDecimal processedRefundAmount) {
        this.processedRefundAmount = processedRefundAmount;
    }

    public BigDecimal getRefundedAmount() {
        return refundedAmount;
    }

    public void setRefundedAmount(BigDecimal refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    public Integer getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Integer transactionID) {
        this.transactionID = transactionID;
    }

    public Integer getFundingTerminalID() {
        return fundingTerminalID;
    }

    public void setFundingTerminalID(Integer fundingTerminalID) {
        this.fundingTerminalID = fundingTerminalID;
    }

    public String getProcessorRefNum() {
        return processorRefNum;
    }

    public void setProcessorRefNum(String processorRefNum) {
        this.processorRefNum = processorRefNum;
    }

    public Integer getLoggedInUserID() {
        return loggedInUserID;
    }

    public void setLoggedInUserID(Integer loggedInUserID) {
        this.loggedInUserID = loggedInUserID;
    }
    public Integer getFailedCardMaxAttempts() {
        return failedCardMaxAttempts;
    }

    public void setFailedCardMaxAttempts(Integer failedCardMaxAttempts) {
        this.failedCardMaxAttempts = failedCardMaxAttempts;
    }

    public Integer getFailedCardDelayHours() {
        return failedCardDelayHours;
    }

    public void setFailedCardDelayHours(Integer failedCardDelayHours) {
        this.failedCardDelayHours = failedCardDelayHours;
    }

    public boolean isFundingTransaction() {
        return fundingTransaction;
    }

    public void setFundingTransaction(boolean fundingTransaction) {
        this.fundingTransaction = fundingTransaction;
    }

    public BigDecimal getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(BigDecimal feeAmount) {
        this.feeAmount = feeAmount;
    }

    public PaymentProcessorModel getPaymentProcessorModel() {
        return paymentProcessorModel;
    }

    public void setPaymentProcessorModel(PaymentProcessorModel paymentProcessorModel) {
        this.paymentProcessorModel = paymentProcessorModel;
    }

}
