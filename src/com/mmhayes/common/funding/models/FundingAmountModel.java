package com.mmhayes.common.funding.models;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;

//funding dependencies
import com.mmhayes.common.funding.collections.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.time.LocalTime;
import java.util.*;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.*;

/* Funding Amount Model
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-11-15 17:41:26 -0500 (Wed, 15 Nov 2017) $: Date of last commit
 $Rev: 5682 $: Revision of last commit

 Notes: Contains information for a single funding amount option - QC_FundingAmount
*/
public class FundingAmountModel {
    Integer id = null; //identifier of model - QC_FundingAmount.FundingAmountID
    Integer amountTypeID = null; //type of model - QC_FundingAmount.FundingAmountTypeID
    String name = null; //name of the model
    BigDecimal amount = null; //The actual "Funding Amount" itself - used to populate selectors in the My QC application
    Boolean defaulted = false; //if there are no selections for the selector in the My QC Application - use this funding amount as the default selection
    Boolean selected = false; //should this funding amount be selected in the My QC application (should "overpower" the "defaulted" funding amount)

    //constructor - takes hashmap that get sets to this models properties
    public FundingAmountModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    //setter for all of this model's properties
    public FundingAmountModel setModelProperties(HashMap modelDetailHM) throws Exception {

        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("FUNDINGAMOUNTID")));

        setAmountTypeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("FUNDINGAMOUNTTYPEID")));

        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("AMOUNT")));

        setDefaulted(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DEFAULTED")));

        setSelected(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SELECTED")));

        return this;
    }

    public Integer getID() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmountTypeID() {
        return amountTypeID;
    }

    public void setAmountTypeID(Integer amountTypeID) {
        this.amountTypeID = amountTypeID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Boolean getDefaulted() {
        return defaulted;
    }

    public void setDefaulted(Boolean defaulted) {
        this.defaulted = defaulted;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

}
