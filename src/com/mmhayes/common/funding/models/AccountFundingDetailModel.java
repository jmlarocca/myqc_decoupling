package com.mmhayes.common.funding.models;

//mmhayes dependencies

import com.mmhayes.common.funding.collections.AccountPaymentMethodCollection;
import com.mmhayes.common.funding.collections.FundingAmountCollection;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

//funding dependencies
//other dependencies

/* Account Funding Detail Model
 Last Updated (automatically updated by SVN)
 $Author: zdhirschman $: Author of last commit
 $Date: 2020-10-21 16:36:35 -0400 (Wed, 21 Oct 2020) $: Date of last commit
 $Rev: 12948 $: Revision of last commit

 Notes: Model that contains payment methods for a specific account along with funding amounts
*/
public class AccountFundingDetailModel {
    List<AccountPaymentMethodModel> accountPaymentMethods = null;
    List<FundingAmountModel> fundingAmounts = null;
    AccountAutoFundingModel accountAutoFundingInfo = null;
    PaymentProcessorModel paymentProcessor = null;
    boolean isAccountFundingAllowed = false;

    public AccountFundingDetailModel()  {}

    //constructor - populates all collections
    public AccountFundingDetailModel(HttpServletRequest request) throws Exception {
        setAccountPaymentMethods(new AccountPaymentMethodCollection(request).getCollection());
        setFundingAmounts(new FundingAmountCollection(request).getCollection());
        setAccountAutoFundingInfo(new AccountAutoFundingModel(request));
        setPaymentProcessor(new PaymentProcessorModel(request));
        setAccountFundingAllowed(FundingModel.isAccountFundingAllowedForAccount(request));
    }

    //constructor - populates account funding allowed property
    public AccountFundingDetailModel(Integer employeeId) throws Exception {
        setAccountFundingAllowed(FundingModel.isAccountFundingAllowedForAccount(employeeId));
    }

    public List<AccountPaymentMethodModel> getAccountPaymentMethods() {
        return accountPaymentMethods;
    }

    public void setAccountPaymentMethods(List<AccountPaymentMethodModel> accountPaymentMethods) {
        this.accountPaymentMethods = accountPaymentMethods;
    }

    public List<FundingAmountModel> getFundingAmounts() {
        return fundingAmounts;
    }

    public void setFundingAmounts(List<FundingAmountModel> fundingAmounts) {
        this.fundingAmounts = fundingAmounts;
    }

    public AccountAutoFundingModel getAccountAutoFundingInfo() {
        return accountAutoFundingInfo;
    }

    public void setAccountAutoFundingInfo(AccountAutoFundingModel accountAutoFundingInfo) {
        this.accountAutoFundingInfo = accountAutoFundingInfo;
    }

    public PaymentProcessorModel getPaymentProcessor() {
        return paymentProcessor;
    }

    public void setPaymentProcessor(PaymentProcessorModel paymentProcessor) {
        this.paymentProcessor = paymentProcessor;
    }

    public boolean isAccountFundingAllowed() {
        return isAccountFundingAllowed;
    }

    public void setAccountFundingAllowed(boolean accountFundingAllowed) {
        isAccountFundingAllowed = accountFundingAllowed;
    }
}
