package com.mmhayes.common.funding.freedompay;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.utils.Logger;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: DAO for FreedomPay GetTransaction method
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FreedomPayGetTransactionResponse {

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String ResponseMessage;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String CaptureResponse;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String CardIssuer;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String CreditApplicationInformation;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String DynExpMonth;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String DynExpYear;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String FreewayResponseCode;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String InvoiceNumber;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String LastPromoCode;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String LastPromoDescription;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String MaskedCardNumber;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String MerchantReferenceCode;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String NameOnCard;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String ProcessorResponseCode;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String StoreName;
    public FreedompayTokenInformation TokenInformation;

    @JsonIgnore public Object OriginalRequest;
    @JsonIgnore public Object AuthResponse;
    @JsonIgnore public Object FailedResponses;

    @JsonIgnore
    public String DealerCustomerNumber;

    @JsonIgnore
    public String getLastPromoCode() {
        return LastPromoCode;
    }

    public void setLastPromoCode(String lastPromoCode) {
        LastPromoCode = lastPromoCode;
    }

    @JsonIgnore
    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        ResponseMessage = responseMessage;
    }

    @JsonIgnore
    public String getCaptureResponse() {
        return CaptureResponse;
    }

    public void setCaptureResponse(String captureResponse) {
        CaptureResponse = captureResponse;
    }

    @JsonIgnore
    public String getCardIssuer() {
        return CardIssuer;
    }

    public void setCardIssuer(String cardIssuer) {
        CardIssuer = cardIssuer;
    }

    @JsonIgnore
    public String getCreditApplicationInformation() {
        return CreditApplicationInformation;
    }

    public void setCreditApplicationInformation(String creditApplicationInformation) {
        CreditApplicationInformation = creditApplicationInformation;
    }

    @JsonIgnore
    public String getDynExpMonth() {
        return DynExpMonth;
    }

    public void setDynExpMonth(String dynExpMonth) {
        DynExpMonth = dynExpMonth;
    }

    @JsonIgnore
    public String getDynExpYear() {
        return DynExpYear;
    }

    public void setDynExpYear(String dynExpYear) {
        DynExpYear = dynExpYear;
    }

    @JsonIgnore
    public String getFreewayResponseCode() {
        return FreewayResponseCode;
    }

    public void setFreewayResponseCode(String freewayResponseCode) {
        FreewayResponseCode = freewayResponseCode;
    }

    @JsonIgnore
    public String getInvoiceNumber() {
        return InvoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        InvoiceNumber = invoiceNumber;
    }

    @JsonIgnore
    public String getLastPromoDescription() {
        return LastPromoDescription;
    }

    public void setLastPromoDescription(String lastPromoDescription) {
        LastPromoDescription = lastPromoDescription;
    }

    @JsonIgnore
    public String getMaskedCardNumber() {
        return MaskedCardNumber;
    }

    public void setMaskedCardNumber(String maskedCardNumber) {
        MaskedCardNumber = maskedCardNumber;
    }

    @JsonIgnore
    public String getMerchantReferenceCode() {
        return MerchantReferenceCode;
    }

    public void setMerchantReferenceCode(String merchantReferenceCode) {
        MerchantReferenceCode = merchantReferenceCode;
    }

    @JsonIgnore
    public String getNameOnCard() {
        return NameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        NameOnCard = nameOnCard;
    }

    @JsonIgnore
    public String getProcessorResponseCode() {
        return ProcessorResponseCode;
    }

    public void setProcessorResponseCode(String processorResponseCode) {
        ProcessorResponseCode = processorResponseCode;
    }

    @JsonIgnore
    public String getStoreName() {
        return StoreName;
    }

    public void setStoreName(String storeName) {
        StoreName = storeName;
    }

    @JsonIgnore
    public FreedompayTokenInformation getTokenInformation() {
        return TokenInformation;
    }

    public void setTokenInformation(FreedompayTokenInformation tokenInformation) {
        TokenInformation = tokenInformation;
    }

    // Card Issuer seems to need to be set by the user (us) based on the MaskedCardNumber that is being provided
    // https://manager.freedompay.us/Map/BinMap2.txt has the information below
    // updated 12/22/2021
    public void setCardIssuerFromBIN() {
        try {
            if (MaskedCardNumber == null || MaskedCardNumber.length() != 6) { // we need at least 6 numbers for a credit card to be checked here, they can vary from 12-19 digits
                Logger.logMessage("Could not set the Card Issuer for this card. Masked Card Number is not available. FreedomPay Response: \n" + this.toString(), Logger.LEVEL.DEBUG);
                return;
            }
            // small ranges done here
            if (MaskedCardNumber.startsWith("34") || MaskedCardNumber.startsWith("37")) {
                CardIssuer = "AMEX";
            } else if (MaskedCardNumber.startsWith("4")) {
                CardIssuer = "VISA";
            } else if (MaskedCardNumber.startsWith("36") || MaskedCardNumber.startsWith("38") || MaskedCardNumber.startsWith("39") || MaskedCardNumber.startsWith("3095")) {
                CardIssuer = "DINERS CLUB";
            } else if (MaskedCardNumber.startsWith("65") || MaskedCardNumber.startsWith("6011")) {
                CardIssuer = "DISCOVER";
            } else { // large ranges done here
                // 2 digit ranges
                int bin = Integer.parseInt(MaskedCardNumber.substring(0, 2));
                if(51 <= bin && bin <= 55) {
                    CardIssuer = "MASTERCARD";
                    return;
                }
                // 3 digit ranges
                bin = Integer.parseInt(MaskedCardNumber.substring(0, 3));
                if (300 <= bin && bin <= 305) {
                    CardIssuer = "DINERS CLUB";
                    return;
                } else if (644 <= bin && bin <= 649) {
                    CardIssuer = "DISCOVER";
                    return;
                }
                // 4 digit ranges
                bin = Integer.parseInt(MaskedCardNumber.substring(0, 4));
                if (2221 <= bin && bin <= 2720) {
                    CardIssuer = "MASTERCARD";
                    return;
                } else if (3528 <= bin && bin <= 3589) {
                    CardIssuer = "JCB";
                    return;
                }
                // 6 digit ranges
                bin = Integer.parseInt(MaskedCardNumber.substring(0, 6));
                if (622126 <= bin && bin <= 622925) {
                    CardIssuer = "DISCOVER";
                }
            }
        } catch(Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Issue setting the Card Issuer from BIN.", Logger.LEVEL.ERROR);
            CardIssuer = "";
        }
    }

    @Override
    public String toString() {
        return "FreedomPayGetTransactionResponse{" +
                "ResponseMessage='" + ResponseMessage + '\'' +
                ", CaptureResponse='" + CaptureResponse + '\'' +
                ", CardIssuer='" + CardIssuer + '\'' +
                ", CreditApplicationInformation='" + CreditApplicationInformation + '\'' +
                ", DynExpMonth='" + DynExpMonth + '\'' +
                ", DynExpYear='" + DynExpYear + '\'' +
                ", FreewayResponseCode='" + FreewayResponseCode + '\'' +
                ", InvoiceNumber='" + InvoiceNumber + '\'' +
                ", LastPromoCode='" + LastPromoCode + '\'' +
                ", LastPromoDescription='" + LastPromoDescription + '\'' +
                ", MaskedCardNumber='" + MaskedCardNumber + '\'' +
                ", MerchantReferenceCode='" + MerchantReferenceCode + '\'' +
                ", NameOnCard='" + NameOnCard + '\'' +
                ", ProcessorResponseCode='" + ProcessorResponseCode + '\'' +
                ", StoreName='" + StoreName + '\'' +
                ", TokenInformation=" + TokenInformation.toString() +
                ", OriginalRequest=" + OriginalRequest +
                ", AuthResponse=" + AuthResponse +
                ", FailedResponses=" + FailedResponses +
                ", DealerCustomerNumber='" + DealerCustomerNumber + '\'' +
                '}';
    }
}
