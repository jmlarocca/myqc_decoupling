package com.mmhayes.common.funding.freedompay;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Freedompay Token DAO
 */
public class FreedompayTokenInformation {

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String CardExpirationMonth;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String CardExpirationYear;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String Token;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String TokenExpiration;

    /*"TokenInformation": {
        "CardExpirationMonth": "12",
        "CardExpirationYear": "2049",
        "Token": "8150160663552977",
        "TokenExpiration": "2020-05-17T00:00:00.0000000Z"
    }*/

    @JsonIgnore
    public String getCardExpirationMonth() {
        return CardExpirationMonth;
    }

    public void setCardExpirationMonth(String cardExpirationMonth) {
        CardExpirationMonth = cardExpirationMonth;
    }

    @JsonIgnore
    public String getCardExpirationYear() {
        return CardExpirationYear;
    }

    public void setCardExpirationYear(String cardExpirationYear) {
        CardExpirationYear = cardExpirationYear;
    }

    @JsonIgnore
    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    @JsonIgnore
    public String getTokenExpiration() {
        return TokenExpiration;
    }

    public void setTokenExpiration(String tokenExpiration) {
        TokenExpiration = tokenExpiration;
    }

    @Override
    public String toString() {
        return "FreedompayTokenInformation{" +
                "CardExpirationMonth='" + CardExpirationMonth + '\'' +
                ", CardExpirationYear='" + CardExpirationYear + '\'' +
                ", Token='" + Token + '\'' +
                ", TokenExpiration='" + TokenExpiration + '\'' +
                '}';
    }
}
