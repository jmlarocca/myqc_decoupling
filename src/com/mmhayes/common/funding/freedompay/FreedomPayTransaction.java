package com.mmhayes.common.funding.freedompay;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class FreedomPayTransaction {

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String TransactionId = "";

    // GETTER / SETTERS
    @JsonIgnore
    public String getTransactionID() {
        return TransactionId;
    }

    public void setTransactionID(String transactionID) {
        this.TransactionId = transactionID;
    }
}
