package com.mmhayes.common.funding.freedompay;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Request object for FreedomPay CreateTransaction method
 */
public class FreedomPayCreateTransactionRequest {

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String StoreId;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String TerminalId;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String TransactionTotal;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String RequestToken = "false";
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String TokenValue;
    @JsonSerialize
    private Integer TimeoutMinutes;
    @JsonSerialize
    private Integer TokenType;
    // GETTER / SETTERS

    public String getStoreID() {
        return StoreId;
    }

    public void setStoreID(String storeID) {
        this.StoreId = storeID;
    }

    public String getTerminalID() {
        return TerminalId;
    }

    public void setTerminalID(String terminalID) {
        this.TerminalId = terminalID;
    }

    public String getTransactionTotal() {
        return TransactionTotal;
    }

    public void setTransactionTotal(String transactionTotal) {
        this.TransactionTotal = transactionTotal;
    }

    public String isRequestToken() {
        return RequestToken;
    }

    public void setRequestToken(String requestToken) {
        this.RequestToken = requestToken;
    }

    public String getTokenValue() {
        return TokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.TokenValue = tokenValue;
    }

    public Integer getTimeoutMinutes() {
        return TimeoutMinutes;
    }

    public void setTimeoutMinutes(Integer timeoutMinutes) {
        TimeoutMinutes = timeoutMinutes;
    }

    public Integer getTokenType() {
        return TokenType;
    }

    public void setTokenType(Integer tokenType) {
        TokenType = tokenType;
    }
}
