package com.mmhayes.common.funding.freedompay;

import java.math.BigDecimal;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: FreedomPay DAO
 */
public class FreedomPayRequest {

    public String TerminalId;
    public String StoreId;
    public BigDecimal TransactionTotal;
    public Integer TimeoutMinutes = 3;
    public Integer TokenType = 11;
    public String CSSID;

    public String getTerminalID() {
        return TerminalId;
    }

    public void setTerminalID(String terminalID) {
        this.TerminalId = terminalID;
    }

    public String getStoreID() {
        return StoreId;
    }

    public void setStoreID(String storeID) {
        this.StoreId = storeID;
    }

    public BigDecimal getTransactionTotal() {
        return TransactionTotal;
    }

    public void setTransactionTotal(BigDecimal transactionTotal) {
        this.TransactionTotal = transactionTotal;
    }

    public Integer getTimeoutMinutes() {
        return TimeoutMinutes;
    }

    public void setTimeoutMinutes(Integer timeoutMinutes) {
        TimeoutMinutes = timeoutMinutes;
    }

    public String getCSSID() {
        return CSSID;
    }

    public void setCSSID(String CSSID) {
        this.CSSID = CSSID;
    }

    public Integer getTokenType() {
        return TokenType;
    }

    public void setTokenType(Integer tokenType) {
        TokenType = tokenType;
    }
}
