package com.mmhayes.common.funding.freedompay;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: DAO object for FreedomPay Capture method request
 */

/*
{
    "ResponseMessage": "Missing transaction ID",
    "AuthorizationDecision": null,
    "BillingAddress": null,
    "DCCInfo": null,
    "DCCOptIn": false,
    "FreewayResponse": null,
    "WasAuthorized": false,
    "WasCaptured": false
}
 */

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

public class FreedomPayCaptureTransactionResponse {

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String ResponseMessage;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String AuthorizationDecision;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String BillingAddress;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String DCCInfo;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String DCCOptIn;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String FreewayResponse;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String WasAuthorized;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    String WasCaptured;

    // GETTER / SETTERS

    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        ResponseMessage = responseMessage;
    }

    public String getAuthorizationDecision() {
        return AuthorizationDecision;
    }

    public void setAuthorizationDecision(String authorizationDecision) {
        AuthorizationDecision = authorizationDecision;
    }

    public String getBillingAddress() {
        return BillingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        BillingAddress = billingAddress;
    }

    public String getDCCInfo() {
        return DCCInfo;
    }

    public void setDCCInfo(String DCCInfo) {
        this.DCCInfo = DCCInfo;
    }

    public String getDCCOptIn() {
        return DCCOptIn;
    }

    public void setDCCOptIn(String DCCOptIn) {
        this.DCCOptIn = DCCOptIn;
    }

    public String getFreewayResponse() {
        return FreewayResponse;
    }

    public void setFreewayResponse(String freewayResponse) {
        FreewayResponse = freewayResponse;
    }

    public String isWasAuthorized() {
        return WasAuthorized;
    }

    public void setWasAuthorized(String wasAuthorized) {
        WasAuthorized = wasAuthorized;
    }

    public String isWasCaptured() {
        return WasCaptured;
    }

    public void setWasCaptured(String wasCaptured) {
        WasCaptured = wasCaptured;
    }
}
