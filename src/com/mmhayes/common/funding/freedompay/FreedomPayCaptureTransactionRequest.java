package com.mmhayes.common.funding.freedompay;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: DAO object for FreedomPay Capture method request
 */

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

public class FreedomPayCaptureTransactionRequest {

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    private String TransactionId;

    // GETTER / SETTERS
    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }
}
