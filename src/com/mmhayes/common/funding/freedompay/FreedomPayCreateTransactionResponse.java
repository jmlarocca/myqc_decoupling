package com.mmhayes.common.funding.freedompay;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Request object for FreedomPay CreateTransaction method
 */
public class FreedomPayCreateTransactionResponse {

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String ResponseMessage;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String CheckoutUrl;
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    public String TransactionId;
    @JsonIgnore
    public String [] Messages;

    /*
    {
        "ResponseMessage": "No data in request",
        "CheckoutUrl": null,
        "TransactionId": "00000000-0000-0000-0000-000000000000"
    }
     */

    // GETTER / SETTERS
    @JsonIgnore
    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.ResponseMessage = responseMessage;
    }

    @JsonIgnore
    public String getCheckoutUrl() {
        return CheckoutUrl;
    }

    public void setCheckoutUrl(String checkoutUrl) {
        this.CheckoutUrl = checkoutUrl;
    }

    @JsonIgnore
    public String getTransactionID() {
        return TransactionId;
    }

    public void setTransactionID(String transactionID) {
        this.TransactionId = transactionID;
    }
}
