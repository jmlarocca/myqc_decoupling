package com.mmhayes.common.funding.freedompay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FreedomPayCreateTransactionResponseProduction extends FreedomPayCreateTransactionResponse {}
