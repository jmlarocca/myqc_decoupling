package com.mmhayes.common.funding;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.Validation;
import com.mmhayes.common.api.clients.FreedomPayClient;
import com.mmhayes.common.api.errorhandling.exceptions.FundingCardException;
import com.mmhayes.common.api.errorhandling.exceptions.FundingException;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.funding.freedompay.FreedomPayGetTransactionResponse;
import com.mmhayes.common.funding.models.AccountAutoFundingModel;
import com.mmhayes.common.funding.models.AccountPaymentMethodModel;
import com.mmhayes.common.funding.models.FundingModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.MMHRunnable;
import com.stripe.Stripe;
import com.stripe.exception.*;
import com.stripe.model.Customer;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.sql.Timestamp;

//funding dependencies
//stripe dependencies
//other dependencies

/* Funding API Helper
 Last Updated (automatically updated by SVN)
 $Author: mjbatko $: Author of last commit
 $Date: 2021-07-09 12:07:32 -0400 (Fri, 09 Jul 2021) $: Date of last commit
 $Rev: 14323 $: Revision of last commit

 Notes: This class provides methods that assist with the Funding API (generally, the Funding Resource itself but not limited too)
*/
public class FundingHelper extends MMHRunnable {
    @JsonIgnore private static DataManager dm = new DataManager();
    @JsonIgnore private static commonMMHFunctions commonMMHFunctions = new commonMMHFunctions();
    @JsonIgnore private String validationTarget = "N/A"; //default target to "N/A"
    @JsonIgnore private String validationPath = "N/A"; //default path to "N/A"
    @JsonIgnore private Validation responseValidation; //initialize response object
    @JsonIgnore private static Client validationClient = ClientBuilder.newClient(); //creates a validationClient for all requests from the instance

    public static final int BACKGROUND_TASK_ID = 5;
    public static String timeOfLastCheck = "";
    private int applicationID;//the app running this instance of the funding helper. 1 = QC, 2 = MyQC
    private static final String[] MYQC_VERSION_THRESHOLD = "4.0.42".split("\\.");
    private static final String[] QC_VERSION_THRESHOLD = "9.4.0.6".split("\\.");

    public FundingHelper(int appID) {
        super(BACKGROUND_TASK_ID);
        applicationID = appID;
    }

    //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
    //this method will create a new default payment method for an account - it will also inactivate the current default payment method if there is one
    public static AccountPaymentMethodModel createNewDefaultPaymentMethodForAccount(AccountPaymentMethodModel accountPaymentMethodModel, HttpServletRequest request) throws Exception {

        //get session details from request
        HashMap authenticationHM = CommonAuthResource.determineAuthDetailsFromAuthHeader(request);
        AccountModel authenticatedEmployeeModel = (AccountModel) authenticationHM.get("accountModel");
        Integer authenticatedEmployeeID = authenticatedEmployeeModel.getId();
        Integer personID = CommonAPI.convertModelDetailToInteger(authenticationHM.get("personID"));
        boolean forPerson = personID != null && personID > 0;

        //if not for a person account and accountID is null, throw invalid auth
        if ( !forPerson && authenticatedEmployeeID == null ) {
            throw new InvalidAuthException();
        }

        //throw missing data exception if accountPaymentMethodModel was not passed in
        if (accountPaymentMethodModel == null) {
            throw new MissingDataException();
        }

        //get all payment methods for this account in an array list
        String getPaymentMethodSQL = !forPerson ? "data.common.funding.getDefaultAccountPaymentMethod" :  "data.common.funding.getDefaultPersonAccountPaymentMethod";
        Integer paymentMethodAccountID = !forPerson ? authenticatedEmployeeID : personID;

        ArrayList<HashMap> accountPaymentMethodList = dm.parameterizedExecuteQuery(getPaymentMethodSQL,
            new Object[]{
                paymentMethodAccountID
            },
            true
        );

        if (accountPaymentMethodList != null && accountPaymentMethodList.size() > 1) { //too many account payment methods, do nothing
            //TODO: A better exception here? Generic "InvalidDataException" or "FunctionNotSupportedException" ?
            Logger.logMessage("Found more than default account payment method on file! This should not happen! For account ID# " + authenticatedEmployeeID, Logger.LEVEL.ERROR);
            throw new MissingDataException();
        } else {
            Integer previousDefaultEmployeePaymentMethodID = null;

            //inactivate the existing default account payment method if one exists
            if (accountPaymentMethodList != null && accountPaymentMethodList.size() == 1) {
                //get the previous default payment method ID for replacing later with the new one for auto funding accounts
                HashMap accountPaymentMethodHM = accountPaymentMethodList.get(0);
                if (accountPaymentMethodHM.get("EMPLOYEEPAYMENTMETHODID") != null) {
                    previousDefaultEmployeePaymentMethodID = Integer.parseInt(accountPaymentMethodHM.get("EMPLOYEEPAYMENTMETHODID").toString());
                }

                //do NOT disable automatic reloads in this case because we just created a new default payment method
                inactivateDefaultPaymentMethodForAccount(false, request);

            }

            //set the payment method personID if applicable
            if ( forPerson ) {
                accountPaymentMethodModel.setPersonID(personID);
            }

            //attempt to get the processor payment token BEFORE writing to our database
            accountPaymentMethodModel = retrieveProcessorPaymentToken(authenticatedEmployeeID, accountPaymentMethodModel, forPerson);

            //set this payment method as the default
            accountPaymentMethodModel.setModelIsDefault(true);

            String createPaymentMethodSQL = "data.common.funding.createAccountPaymentMethod";
            if ( forPerson ) {
                createPaymentMethodSQL = "data.common.funding.createPersonPaymentMethod";
            }

            //insert payment method
            Integer insertedResultID = dm.parameterizedExecuteNonQuery(createPaymentMethodSQL,
                    new Object[]{
                            accountPaymentMethodModel.getPaymentProcessorID(),
                            accountPaymentMethodModel.getProcessorPaymentToken(),
                            accountPaymentMethodModel.getPaymentMethodTypeID(),
                            accountPaymentMethodModel.getPaymentMethodLastFour(),
                            accountPaymentMethodModel.getPaymentMethodExpirationMonth(),
                            accountPaymentMethodModel.getPaymentMethodExpirationYear(),
                            accountPaymentMethodModel.getPaymentMethodFullName(),
                            accountPaymentMethodModel.getPaymentMethodZipCode(),
                            accountPaymentMethodModel.getPaymentMethodEmailAddress(),
                            accountPaymentMethodModel.getModelIsDefault(),
                            accountPaymentMethodModel.getActive(),
                            paymentMethodAccountID
                    }
            );

            //if the insert result does not return as a positive integer - something went wrong
            if (insertedResultID < 0) {
                //TODO: A better exception here? Generic "InvalidDataException" or "SQLResultMismatchException"?
                Logger.logMessage("Creating account payment method failed for" + (forPerson ? "person ID# " : "account ID# ") + paymentMethodAccountID, Logger.LEVEL.ERROR);
                throw new MissingDataException();
            } else {

                //set the brand new ID on this model as we successfully created a new default payment method
                accountPaymentMethodModel.setId(insertedResultID);

                if ( previousDefaultEmployeePaymentMethodID != null ) {
                    replaceAutoFundingPaymentMethodsWithNewDefault(previousDefaultEmployeePaymentMethodID, insertedResultID);
                }

                // If employee then make sure Reset the Refunding Disabled flag.
                try {
                    if(!forPerson){
                        Integer updated = dm.parameterizedExecuteNonQuery("data.common.funding.updateEmployeeResetRefundingDisabled",
                                new Object[]{authenticatedEmployeeID}
                        );
                        if(updated == 0){
                            Logger.logMessage("Unable to reset Refunding Disabled flag for: "+accountPaymentMethodModel.getAccountID(), Logger.LEVEL.ERROR);
                        }
                    }
                } catch(Exception ex){
                    Logger.logMessage("Error when trying to reset Refunding Disabled flag.", Logger.LEVEL.ERROR);
                    Logger.logException(ex);
                }
            }
        }

        return accountPaymentMethodModel;
    }

    //this method will remove the default payment method for the account (can disable automatic reloads as well)
    public static void inactivateDefaultPaymentMethodForAccount(Boolean disableAutomaticReload, HttpServletRequest request) throws Exception {

        //get session details from request
        HashMap authenticationHM = CommonAuthResource.determineAuthDetailsFromAuthHeader(request);
        AccountModel authenticatedEmployeeModel = (AccountModel) authenticationHM.get("accountModel");
        Integer authenticatedEmployeeID = authenticatedEmployeeModel.getId();
        Integer personID = CommonAPI.convertModelDetailToInteger(authenticationHM.get("personID"));
        boolean forPerson = personID != null && personID > 0;

        //if not for a person account and accountID is null, throw invalid auth
        if ( !forPerson && authenticatedEmployeeID == null ) {
            throw new InvalidAuthException();
        }

        //get all payment methods for this account in an array list
        String getPaymentMethodSQL = !forPerson ? "data.common.funding.getDefaultAccountPaymentMethod" :  "data.common.funding.getDefaultPersonAccountPaymentMethod";
        Integer paymentMethodAccountID = !forPerson ? authenticatedEmployeeID : personID;

        ArrayList<HashMap> accountPaymentMethodList = dm.parameterizedExecuteQuery(getPaymentMethodSQL,
            new Object[]{
                paymentMethodAccountID
            },
            true
        );

        //account payment method list should only contain ONE payment method which we will inactivate
        if (accountPaymentMethodList != null && accountPaymentMethodList.size() == 1) {
            HashMap accountPaymentMethodHM = accountPaymentMethodList.get(0);
            if (accountPaymentMethodHM.get("EMPLOYEEPAYMENTMETHODID") != null) {
                Integer employeePaymentMethodID = Integer.parseInt(accountPaymentMethodHM.get("EMPLOYEEPAYMENTMETHODID").toString());

                //if the update result does not return as 1 - something went wrong so throw an exception
                Integer updateResult = dm.parameterizedExecuteNonQuery("data.common.funding.inactivateAccountPaymentMethod",
                        new Object[]{
                            employeePaymentMethodID
                        }
                );

                //if the update result does not return as 1 - something went wrong so throw an exception
                if (updateResult != 1) {
                    //TODO: A better exception here? Generic "InvalidDataException" or "SQLResultMismatchException"?
                    Logger.logMessage("ERROR: Removing account payment method failed for account ID# "+authenticatedEmployeeID, Logger.LEVEL.ERROR);
                    throw new MissingDataException();
                } else if (disableAutomaticReload) {

                    //if logged in as a person, check if the person set up the auto reloads, if they did disabled it
                    if(forPerson)  {
                        disableAutoReloadsForAllAccounts(employeePaymentMethodID);

                    //if logged in as an account, check if the auto reloads was set up by a person, if not disable it
                    } else if(!checkIfPaymentMethodSetByPerson(authenticatedEmployeeID)){
                        disableAutomaticReload(authenticatedEmployeeID);
                    }
                }
            }
        } else if (accountPaymentMethodList != null && accountPaymentMethodList.size() > 1) {
            //TODO: A better exception here? Generic "InvalidDataException" or "FunctionNotSupportedException" ?
            Logger.logMessage("ERROR: Found more than account payment method on file. This method only supports accounts with a single payment method on file for account ID# "+authenticatedEmployeeID);
            throw new MissingDataException();
        } else {
            //TODO: A better exception here? Generic "InvalidDataException" or "FunctionNotSupportedException" ?
            Logger.logMessage("ERROR: Could not determine payment information for account ID# "+authenticatedEmployeeID, Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

    }

    //replaces all auto funding methods set up with oldPaymentMethodID with the newPaymentMethodID
    private static void replaceAutoFundingPaymentMethodsWithNewDefault(Integer oldPaymentMethodID, Integer newPaymentMethodID) throws Exception {
        Object oldPaymentMethodSetCount = dm.parameterizedExecuteScalar("data.commmon.funding.getAutoFundingPaymentMethodSetCount",
                new Object[]{
                        oldPaymentMethodID
                });

        //if no accounts have the old payment method set, just return
        if ( oldPaymentMethodSetCount == null || Integer.parseInt( oldPaymentMethodSetCount.toString() ) == 0 ) {
            return;
        }

        //if the update result does not return as 1 - something went wrong so throw an exception
        Integer updateResult = dm.parameterizedExecuteNonQuery("data.common.funding.replaceAutoFundPaymentMethods",
                new Object[]{
                        oldPaymentMethodID,
                        newPaymentMethodID
                }
        );

        //if the update result does not return as the same value as the count of set payment methods - something went wrong so throw an exception
        if (updateResult != Integer.parseInt( oldPaymentMethodSetCount.toString() ) ) {
            Logger.logMessage("ERROR: Replacing auto funding payment methods failed for EmployeePaymentMethodID # "+newPaymentMethodID, Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }
    }

    //this method will modify or enable the automatic reload amount / reload threshold for the authenticated account
    public static AccountAutoFundingModel modifyOrEnableAutomaticReload(AccountAutoFundingModel accountAutoFundingModel, HttpServletRequest request) throws Exception {

        //NOTE: "Modifying" and "Enabling" are essentially the same because there will always be a QC_Employees record - jrmitaly 2/16/2017
        //throw InvalidAuthException if no authenticated employee is found
        Integer authenticatedEmployeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        if (authenticatedEmployeeID == null) {
            throw new InvalidAuthException();
        }

        //throw missing data exception if accountAutoFundingModel was not passed in
        if (accountAutoFundingModel == null) {
            throw new MissingDataException();
        }

        //get current automatic funding information (for logging later on)
        AccountAutoFundingModel oldAccountFundingModel = new AccountAutoFundingModel(request);

        //modify or enable account auto fund for account
        Integer updateResult = dm.parameterizedExecuteNonQuery("data.common.funding.modifyAccountAutoFund",
                new Object[]{
                        accountAutoFundingModel.getReloadAmount(),
                        accountAutoFundingModel.getReloadThreshold().negate(), //reload threshold should be stored as NEGATIVEaccountAutoFundingModel
                        accountAutoFundingModel.getReloadPaymentMethodID(),
                        authenticatedEmployeeID
                }
        );

        //if the update result does not return as 1 - something went wrong so throw an exception
        if (updateResult != 1) {
            //TODO: A better exception here? Generic "InvalidDataException" or "SQLResultMismatchException"?
            Logger.logMessage("Modifiying / Enabling Automatic Reload Settings failed for account ID# "+authenticatedEmployeeID, Logger.LEVEL.ERROR);
            throw new MissingDataException();
        } else {
            //log changes to QC_EmployeeChanges table
            logAutomaticFundingChanges(accountAutoFundingModel.getReloadAmount(), accountAutoFundingModel.getReloadThreshold().negate(), "", accountAutoFundingModel.getReloadPaymentMethodID().toString(), oldAccountFundingModel, authenticatedEmployeeID);
        }

        return accountAutoFundingModel;
    }

    //this method will modify the automatic reload amount / reload threshold for the authenticated account
    public static void disableAutomaticReload(HttpServletRequest request) throws Exception {

        //throw InvalidAuthException if no authenticated employee is found
        Integer authenticatedEmployeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        if (authenticatedEmployeeID == null) {
            throw new InvalidAuthException();
        }

        disableAutomaticReload(authenticatedEmployeeID);
    }

    //this method will modify the automatic reload amount / reload threshold for the authenticated account
    public static void disableAutomaticReload(Integer authenticatedEmployeeID) throws Exception {

        //throw InvalidAuthException if no authenticated employee is found
        if (authenticatedEmployeeID == null) {
            throw new InvalidAuthException();
        }

        //get current automatic funding information (for logging later on)
        AccountAutoFundingModel oldAccountFundingModel = new AccountAutoFundingModel(authenticatedEmployeeID);

        //current datetime the user is making this request
        Date currentDateTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        String formattedDate = formatter.format(currentDateTime);

        //the new value of the reload token
        String cancelledReloadToken = "CANCELLED ON "+formattedDate;

        //disable account auto fund for account
        Integer updateResult = dm.parameterizedExecuteNonQuery("data.common.funding.disableAccountAutoFund",
                new Object[]{
                        cancelledReloadToken,
                        authenticatedEmployeeID
                }
        );

        //if the update result does not return as 1 - something went wrong so throw an exception
        if (updateResult != 1) {
            //TODO: A better exception here? Generic "InvalidDataException" or "SQLResultMismatchException"?
            Logger.logMessage("Disabling Automatic Reload Settings failed for account ID# "+authenticatedEmployeeID, Logger.LEVEL.ERROR);
            throw new MissingDataException();
        } else {
            //log changes to QC_EmployeeChanges table
            logAutomaticFundingChanges(new BigDecimal("0"), new BigDecimal("0"), cancelledReloadToken, "", oldAccountFundingModel, authenticatedEmployeeID);
        }

    }

    //checks if there is a payment method for this personID that is the same ID as the employee's reload payment method id
    public static void determinePersonPaymentMethod(Integer personID, Integer employeeID) throws Exception {

        //get the auto funding payment method the employee is set to that was created by this person
        ArrayList<HashMap> foundPersonPaymentMethod = dm.parameterizedExecuteQuery("data.common.funding.findPersonPaymentMethod",
                new Object[]{
                        employeeID,
                        personID
                },
                true
        );

        if (foundPersonPaymentMethod != null && foundPersonPaymentMethod.size() > 0) {
            disableAutomaticReload(employeeID);
        }
    }

    //gets all the employees whose auto reload ID is set to payment method that was removed and disables their automatic reloads
    public static void disableAutoReloadsForAllAccounts(Integer reloadPaymentMethodID) throws Exception {

        //get all the employees who are set to the reloadPaymentMethodID that needs to be removed
        ArrayList<HashMap> employeeList = dm.parameterizedExecuteQuery("data.common.funding.getEmployeesNeedingAuoReloadDisabled",
                new Object[]{
                        reloadPaymentMethodID
                       },
                true
        );

        if (employeeList != null && employeeList.size() > 0) {
            for(HashMap employeeHM : employeeList) {
                Integer employeeID = Integer.parseInt(employeeHM.get("EMPLOYEEID").toString());
                disableAutomaticReload(employeeID);
            }
        }
    }

    //checks if there the employee's reloadPaymentMethodID is set to a payment method set up by a person
    public static boolean checkIfPaymentMethodSetByPerson(Integer employeeID) throws Exception {
        boolean wasPaymentMethodSetByPerson = false;
        //get the auto funding payment method the employee is set to that was created by this person
        ArrayList<HashMap> foundPaymentMethodSetByPerson = dm.parameterizedExecuteQuery("data.common.funding.findPaymentMethodSetByPerson",
                new Object[]{
                        employeeID
                },
                true
        );

        if (foundPaymentMethodSetByPerson != null && foundPaymentMethodSetByPerson.size() > 0) {
            HashMap foundPaymentMethodSetByPersonHM = foundPaymentMethodSetByPerson.get(0);

            //if the employee's reloadPaymentMethodID ties to a payment method that was set by a personID, return true
            if(foundPaymentMethodSetByPersonHM.get("PERSONID") != null && !foundPaymentMethodSetByPersonHM.get("PERSONID").toString().equals("")) {
                wasPaymentMethodSetByPerson = true;
            }
        }
        return wasPaymentMethodSetByPerson;
    }

    //determines the fee based on the terminal
    public static AccountPaymentMethodModel determineFundingFee(BigDecimal fundAmount, HttpServletRequest request) throws Exception {
        FundingModel fundingModel = new FundingModel(request);

        if ( fundingModel.getAccountPaymentMethod() == null ) {
            throw new MissingDataException("Could not determine payment method");
        }

        return fundingModel.getAccountPaymentMethod().calculatePaymentFee(fundAmount);
    }

    //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
    //OVERLOAD - determines the funding "Store Balance" (a.k.a funding "terminal group balance") for any authenticated account
    public static HashMap determineFundingStoreBalanceInfo(HttpServletRequest request) throws Exception {
        System.out.print(request);
        //throw InvalidAuthException if no authenticated employee is found
        Integer authenticatedEmployeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        if (authenticatedEmployeeID == null) {
            throw new InvalidAuthException();
        }

        return determineFundingStoreBalanceInfo(authenticatedEmployeeID);

    }

    //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
    //OVERLOAD - determines the funding "Store" (a.k.a funding "terminal group") balance information for any authenticated account
    public static HashMap determineFundingStoreBalanceInfo(Integer authenticatedEmployeeID) throws Exception {

        //method variables
        ArrayList<HashMap> fundingStoreAndTerminalInfoList = null;

        //throw InvalidAuthException if no authenticated employee is found
        if (authenticatedEmployeeID == null) {
            throw new InvalidAuthException();
        }

        Integer fundingTerminalID = FundingHelper.getMyQCFundingTerminalID(authenticatedEmployeeID);

        //query the "background processor" SQL view (should not query raw transactional data) - QCV_EmployeeBalanceDetailBP
        fundingStoreAndTerminalInfoList = dm.parameterizedExecuteQuery("data.common.funding.getSingleFundingStoreAndTerminalBalanceForAccount_BP",
            new Object[]{
                authenticatedEmployeeID,
                fundingTerminalID
            },
            true
        );

        if (fundingStoreAndTerminalInfoList == null) {
            Logger.logMessage("Could not determine any funding store balance information - invalid query - for accountID# "+authenticatedEmployeeID, Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        HashMap fundingStoreAndTerminalInfoHM = fundingStoreAndTerminalInfoList.get(0);
        if (fundingStoreAndTerminalInfoHM.get("STORENAME") != null
                && fundingStoreAndTerminalInfoHM.get("STORELIMIT") != null
                && fundingStoreAndTerminalInfoHM.get("STOREBALANCE") != null
                && fundingStoreAndTerminalInfoHM.get("STOREAVAILABLE") != null
                && fundingStoreAndTerminalInfoHM.get("TERMINALNAME") != null
                && fundingStoreAndTerminalInfoHM.get("TERMINALBALANCE") != null) {

            //add lastSynced to the fundingStoreAndTerminalInfoHM
            fundingStoreAndTerminalInfoHM.put("LASTSYNCED", commonMMHFunctions.getLastSyncDateTimeForAccount(authenticatedEmployeeID));

            //Log Store Name, Store Balance and Terminal Balance
            Logger.logMessage("Successfully retrieved store and terminal balance information - for accountID# "+authenticatedEmployeeID, Logger.LEVEL.DEBUG);
            Logger.logMessage("Store Name: "+fundingStoreAndTerminalInfoHM.get("STORENAME").toString(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Store Limit: "+fundingStoreAndTerminalInfoHM.get("STORELIMIT").toString(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Store Balance: "+fundingStoreAndTerminalInfoHM.get("STOREBALANCE").toString(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Store Available: "+fundingStoreAndTerminalInfoHM.get("STOREAVAILABLE").toString(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Terminal Name: "+fundingStoreAndTerminalInfoHM.get("TERMINALNAME").toString(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Terminal Balance: "+fundingStoreAndTerminalInfoHM.get("TERMINALBALANCE").toString(), Logger.LEVEL.DEBUG);
            Logger.logMessage("Last Synced: "+fundingStoreAndTerminalInfoHM.get("LASTSYNCED").toString(), Logger.LEVEL.DEBUG);

        } else {
            Logger.logMessage("Could not determine required funding store balance information - for accountID# "+authenticatedEmployeeID, Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        return fundingStoreAndTerminalInfoHM;
    }

    //reads property file for how long the system waits (in seconds) after the war has been initialized before starting to check accounts
    public static Integer getInitialDelayTimeForAutomaticFundingMonitor() {
        try {
            String initialDelaySecs = MMHProperties.getAppSetting("site.funding.auto.initialdelay.seconds");
            if ((initialDelaySecs == null) || (initialDelaySecs.length() == 0)) {
                initialDelaySecs = "60"; //default to 60 seconds
            }
            return Integer.parseInt(initialDelaySecs);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return 60; //default to 60 seconds
        }
    }

    //reads property file for how long the system waits (in seconds) before checking all accounts in the system again (from the time it finished the last fund)
    public static Integer getFrequencyForAutomaticFundingMonitor() {
        try {
            String initialDelaySecs = MMHProperties.getAppSetting("site.funding.auto.checkagainafter.seconds");
            if ((initialDelaySecs == null) || (initialDelaySecs.length() == 0)) {
                initialDelaySecs = "900"; //default to 900 seconds (15 minutes)
            }
            return Integer.parseInt(initialDelaySecs);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return 900; //default to 900 seconds (15 minutes)
        }
    }

    //funds any valid and eligible account in the system that needs to be automatically funded right now
    private static void automaticallyFundAccounts() {
        try {
            Logger.logMessage("About to check for accounts that may need automatic funding....", Logger.LEVEL.IMPORTANT);

            //method variables
            ArrayList<HashMap> accountsNeedingFundingNow = null;

            //first, determine if there are any accounts that need to be funded right now by checking their store balances
            accountsNeedingFundingNow = checkForAccountsNeedingAutomaticFunding();

            if (accountsNeedingFundingNow != null && accountsNeedingFundingNow.size() > 0) {

                //third, iterate through the accountsNeedingFundingNow list and attempt to fund each account via their default payment method
                fundAndNotifyAccountsNeedingAutomaticFunding(accountsNeedingFundingNow);

            }

            Logger.logMessage("Automatic funding completed....", Logger.LEVEL.IMPORTANT);

        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Exception caught in automatically fund accounts (executor service runnable!). Please check logs!", Logger.LEVEL.ERROR);
        }
    }

    //determine if accountsEligibleForFunding need to be funded right now by checking their store balances
    private static ArrayList<HashMap> checkForAccountsNeedingAutomaticFunding() throws Exception {

        //method variables
        ArrayList<HashMap> accountsNeedingFunding = new ArrayList<HashMap>();

        //get My QC Funding Terminals - so we can iterate through each one
        ArrayList<HashMap> myQCFundingTerminalsToCheck = dm.parameterizedExecuteQuery("data.common.funding.getMyQCFundingTerminalIDs",
                new Object[]{

                },
                true
        );

        if (myQCFundingTerminalsToCheck == null || myQCFundingTerminalsToCheck.size() == 0) {
            Logger.logMessage("While attempting to check account store balances for automatic funding.. Could not find any My QC Funding Terminal Types. Will not automatically fund any accounts now.", Logger.LEVEL.ERROR);
            return null;
        }

        //iterate through every myQCFundingTerminal and determine which accounts need funding now
        for (HashMap myQCFundingTerminal : myQCFundingTerminalsToCheck) {
            if (myQCFundingTerminal != null && myQCFundingTerminal.get("TERMINALID") != null) {
                Integer myQCFundingTerminalID = Integer.parseInt(myQCFundingTerminal.get("TERMINALID").toString());

                Boolean terminalValid = validateTerminalBeforeFunding(myQCFundingTerminal);

                if(!terminalValid) continue;


                LocalDateTime currentTime = LocalDateTime.now();
                String currentTimeOfDayParam = currentTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
                timeOfLastCheck = currentTimeOfDayParam;
                //determine if any accounts in the system need funding for this My QC Funding Terminal
                ArrayList <HashMap> accountsNeedingFundingAtSpecificTerminal = dm.parameterizedExecuteQuery("data.common.funding.checkForAccountsNeedingAutomaticFunding",
                        new Object[]{
                                myQCFundingTerminalID,
                                currentTimeOfDayParam
                        },
                        true
                );

                //set to "empty" if no accounts were found at any terminal
                if (accountsNeedingFundingAtSpecificTerminal == null) {
                    accountsNeedingFundingAtSpecificTerminal = new ArrayList<HashMap>();
                }

                //log how many accounts need funding now - per terminal
                Logger.logMessage("Determined that ("+accountsNeedingFundingAtSpecificTerminal.size() +") number of accounts need to be funded now for My Quickcharge Terminal ID# "+myQCFundingTerminalID, Logger.LEVEL.DEBUG);

                //add this specific terminals accounts to the "master" array list
                accountsNeedingFunding.addAll(accountsNeedingFundingAtSpecificTerminal);

            } else {
                Logger.logMessage("While attempting to determine if any accounts need funding... My QC Funding Terminal ID could not be determined!", Logger.LEVEL.ERROR);
            }
        }

        //log how many accounts need funding now - overall
        Logger.logMessage("Determined that ("+accountsNeedingFunding.size() +") number of accounts need to be funded overall.", Logger.LEVEL.IMPORTANT);

        return accountsNeedingFunding;
    }

    //attempt to fund each account via their default payment method and then notify them on success
    private static void fundAndNotifyAccountsNeedingAutomaticFunding( ArrayList<HashMap> accountsNeedingFunding) throws Exception {

        //ensure we actually have accounts needing funding
        if (accountsNeedingFunding == null) {
            Logger.logMessage("While attempting to fund and notify accounts that need automatic funding.. but could not determine any accounts needing funding now!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //iterate through every account needing funding
        for (HashMap accountNeedingFunding : accountsNeedingFunding) {

            if (accountNeedingFunding != null && accountNeedingFunding.get("EMPLOYEEID") != null) {
                Integer personID = CommonAPI.convertModelDetailToInteger(accountNeedingFunding.get("PERSONID"));
                Integer accountID = CommonAPI.convertModelDetailToInteger(accountNeedingFunding.get("EMPLOYEEID"));

                //log various information before attempting to fund the account
                Logger.logMessage("About to fund account (#"+accountID.toString()+") . Details.... ", Logger.LEVEL.DEBUG);
                Logger.logMessage("Account Name: "+accountNeedingFunding.get("NAME").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("My QC Funding Terminal ID:"+accountNeedingFunding.get("MYQCFUNDINGTERMINALID").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Terminal Name: "+accountNeedingFunding.get("TERMINALNAME").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Terminal Balance: "+accountNeedingFunding.get("TERMINALBALANCE").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Store Name: "+accountNeedingFunding.get("STORENAME").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Store Limit: "+accountNeedingFunding.get("STORELIMIT").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Store Balance: "+accountNeedingFunding.get("STOREBALANCE").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Store Available: "+accountNeedingFunding.get("STOREAVAILABLE").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Reload Threshold: "+accountNeedingFunding.get("RELOADTHRESHOLD").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Reload Amount: "+accountNeedingFunding.get("RELOADAMOUNT").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Account Email Address: "+accountNeedingFunding.get("EMAILADDRESS").toString(), Logger.LEVEL.DEBUG);

                if ( personID != null && personID > 0 ) {
                    Logger.logMessage("Person ID: "+personID.toString(), Logger.LEVEL.DEBUG);
                }

                //fund the account for the specified fundingAmount
                BigDecimal fundingAmount = new BigDecimal(accountNeedingFunding.get("RELOADAMOUNT").toString());
                FundingModel fundingModel = new FundingModel(accountID, true);
                try {
                    fundingModel = fundingModel.chargeThenFundAccount(fundingAmount, fundingModel.getAccountPaymentMethod(), "auto");
                } catch(Exception ex){
                    // LOG BUT CONTINUE to allow other accounts to be funded
                    Logger.logMessage("Error reported from chargeThenFundAccount() for: " + (accountID > 0 ? accountID.toString() : personID.toString()), Logger.LEVEL.ERROR);
                }

                //if the funding was successful
                if (fundingModel.getFundedAmount() != null) {
                    //log that the automatic funding was successful
                    if ( personID != null && personID > 0 ) {
                        Logger.logMessage("Successfully automatically funded account ID# ("+accountID+") via person ID# ("+personID+") an amount of "+fundingModel.getProcessedAmount());
                    } else {
                        Logger.logMessage("Successfully automatically funded account ID# ("+accountID+") an amount of "+fundingModel.getProcessedAmount());
                    }
                } else {
                    if ( personID != null && personID > 0 ) {
                        Logger.logMessage("Could not automatically fund account - could not determine funded amount - for account ID #"+accountID+" via person ID #"+personID, Logger.LEVEL.ERROR);
                    } else {
                        Logger.logMessage("Could not automatically fund account - could not determine funded amount - for account ID #"+accountID, Logger.LEVEL.ERROR);
                    }

                    // SEND TEMPLATE EMAIL

                    // Define EmailTypeID
                    int emailTypeID = 8;
                    String emailAddress =  accountNeedingFunding.get("EMAILADDRESS").toString();

                    // Build params HashMap
                    HashMap params = new HashMap();
                    params.put("RecipientName", accountNeedingFunding.get("NAME").toString());
                    params.put("ErrorMessage","Could not automatically fund account.");
                    params.put("EmployeeID", accountID);

                    // Send Email
                    commonMMHFunctions common = new commonMMHFunctions();
                    if(!emailAddress.isEmpty()){
                        if(common.sendTemplateEmail(emailAddress, 0, emailTypeID, "", params)){
                            Logger.logMessage("Successfully sent automatic refund e-mail to "+emailAddress, Logger.LEVEL.DEBUG);
                        } else {
                            Logger.logMessage("Failed to send automatic refund e-mail to "+emailAddress, Logger.LEVEL.ERROR);
                        }
                    } else {
                        Logger.logMessage("Unable to send email to: "+accountID+" No Email Address found.", Logger.LEVEL.ERROR);
                    }
                }

            } else {
                Logger.logMessage("Could not determine account information for account that needed funding!", Logger.LEVEL.ERROR);
            }
        }

    }

    //send email notification to recently funded account letting them know their account was automatically funded
    private static void emailRecentlyFundedAccount(String accountName, String fundedStoreName, String emailAddress, FundingModel fundingModel) throws Exception {

        //verify parameter passed into method
        if (emailAddress == null) {
            Logger.logMessage("While attempting to email recently funded account... could not determine email address!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //verify parameter passed into method
        if (fundingModel.getFundedAmount() == null) {
            Logger.logMessage("While attempting to email recently funded account... could not determine funded amount!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //verify parameter passed into method
        if (accountName == null) {
            Logger.logMessage("While attempting to email recently funded account... could not determine account name!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //verify parameter passed into method
        if (fundedStoreName == null) {
            Logger.logMessage("While attempting to email recently funded account... could not determine any funded store name!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        // Determine feeLabel
        String feeLabel = fundingModel.getAccountPaymentMethod().getFundingFeeLabel() == null ? "" : fundingModel.getAccountPaymentMethod().getFundingFeeLabel();

        // Build params HashMap
        HashMap params = new HashMap();
        params.put("RecipientName", accountName);
        params.put("StoreName", fundedStoreName);
        params.put("Amount", fundingModel.getFundedAmount().setScale(2,BigDecimal.ROUND_HALF_UP).abs().toString());
        params.put("ProcessedAmount", fundingModel.getProcessedAmount().setScale(2,BigDecimal.ROUND_HALF_UP).abs().toString());
        params.put("FeeAmount", fundingModel.getFeeAmount().setScale(2,BigDecimal.ROUND_HALF_UP).abs().toString());
        params.put("FeeLabel", feeLabel);

        // Get EmployeeID from Funding Email
        int employeeID = commonMMHFunctions.getEmployeeIDFromFundingEmailAddress(emailAddress);
        if(employeeID > -1){
            params.put("EmployeeID", employeeID);
        }
        if(fundingModel.getAccountPaymentMethod().getPersonID() != null){
            params.put("PersonID", fundingModel.getAccountPaymentMethod().getPersonID());
        }

        // Define EmailTypeID
        int emailTypeID = 6;

        // Send Email
        commonMMHFunctions common = new commonMMHFunctions();
        if(!emailAddress.isEmpty()){
            if(common.sendTemplateEmail(emailAddress, 0, emailTypeID, "", params)){
                Logger.logMessage("Successfully sent funding success email to "+emailAddress, Logger.LEVEL.DEBUG);
            } else {
                Logger.logMessage("Failed to send funding success email to " + emailAddress, Logger.LEVEL.ERROR);
            }
        }

    }

    //refunds any valid and eligible account in the system that needs to be automatically refunded right now
    private static void automaticallyRefundAccounts() {
        try {

            //check the global setting AllowAccountInactivationRefund to determine if we should even do this
            Boolean allowAccountInactivationRefund = Boolean.parseBoolean(dm.parameterizedExecuteScalar("data.common.funding.getAllowInactivationRefundFlag", new Object[]{}).toString());
            if (!allowAccountInactivationRefund) {
                Logger.logMessage("Account Inactivation Refund Global Flag is OFF... NOT checking for accounts that need automatic refunding....", Logger.LEVEL.DEBUG);
            } else {
                Logger.logMessage("Account Inactivation Refund Global Flag is ON... About to check for accounts that may need automatic refunding....", Logger.LEVEL.DEBUG);

                ArrayList<HashMap> accountsNeedingRefundingNow = null;

                //determine if there are any accounts that need to be refunded right now by checking their account status and balances at every "store" (terminal group)
                accountsNeedingRefundingNow = checkForAccountsNeedingAutomaticRefunding();

                if (accountsNeedingRefundingNow != null && accountsNeedingRefundingNow.size() > 0) {

                    //iterate through the accountsNeedingRefundingNow list and attempt to refund each account their entire store balance for each store
                    refundAndNotifyAccountsNeedingAutomaticRefunding(accountsNeedingRefundingNow);

                }
            }

        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Exception caught in automatically fund accounts (executor service runnable!). Please check logs!", Logger.LEVEL.ERROR);
        }
    }

    //determine if there are any accounts that need to be refunded right now by checking their account status and balances at every "store" (terminal group)
    private static ArrayList<HashMap> checkForAccountsNeedingAutomaticRefunding() throws Exception {

        //method variables
        ArrayList<HashMap> accountsNeedingRefunding = new ArrayList<HashMap>();

        //get My QC Funding Terminals - so we can iterate through each one
        ArrayList<HashMap> myQCFundingTerminalsToCheck = dm.parameterizedExecuteQuery("data.common.funding.getMyQCFundingTerminalIDs",
                new Object[]{},
                true
        );

        if (myQCFundingTerminalsToCheck == null || myQCFundingTerminalsToCheck.size() == 0) {
            Logger.logMessage("While attempting to check account store balances for automatic refunding.. Could not find any My QC Funding Terminal Types. Will not automatically refund any accounts now.", Logger.LEVEL.ERROR);
            return null;
        }

        //iterate through every myQCFundingTerminal and determine which accounts need refunding now
        for (HashMap myQCFundingTerminal : myQCFundingTerminalsToCheck) {
            if (myQCFundingTerminal != null && myQCFundingTerminal.get("TERMINALID") != null) {
                Integer myQCFundingTerminalID = Integer.parseInt(myQCFundingTerminal.get("TERMINALID").toString());

                Boolean terminalValid = validateTerminalBeforeFunding(myQCFundingTerminal);

                if(!terminalValid) continue;


                //determine if any accounts in the system need refunding for this My QC Funding Terminal
                ArrayList <HashMap> accountsNeedingRefundingAtSpecificTerminal = dm.parameterizedExecuteQuery("data.common.funding.checkForAccountsNeedingAutomaticRefunding",
                        new Object[]{
                                myQCFundingTerminalID
                        },
                        true
                );

                //set to "empty" if no accounts were found at any terminal
                if (accountsNeedingRefundingAtSpecificTerminal == null) {
                    accountsNeedingRefundingAtSpecificTerminal = new ArrayList<>();
                }

                //log how many accounts need funding now - per terminal
                Logger.logMessage("Determined that ("+accountsNeedingRefundingAtSpecificTerminal.size() +") number of accounts need to be refunded now for My Quickcharge Terminal ID# "+myQCFundingTerminalID, Logger.LEVEL.DEBUG);

                //add this specific terminals accounts to the "master" array list
                accountsNeedingRefunding.addAll(accountsNeedingRefundingAtSpecificTerminal);

            } else {
                Logger.logMessage("While attempting to determine if any accounts need refunding... My QC Funding Terminal ID could not be determined!", Logger.LEVEL.ERROR);
            }
        }

        //log how many accounts need funding now - overall
        Logger.logMessage("Determined that ("+accountsNeedingRefunding.size() +") number of accounts need to be refunded overall.", Logger.LEVEL.DEBUG);

        return accountsNeedingRefunding;
    }

    //iterate through the accountsNeedingRefundingNow list and attempt to refund each account their entire store balance for each store they have a balance at
    private static void refundAndNotifyAccountsNeedingAutomaticRefunding( ArrayList<HashMap> accountsNeedingRefunding) throws Exception {

        //ensure we actually have accounts needing funding
        if (accountsNeedingRefunding == null) {
            Logger.logMessage("While attempting to refund and notify accounts that need automatic refunding.. but could not determine any accounts needing refunding now!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //iterate through every account needing refunding
        for (HashMap accountNeedingRefunding : accountsNeedingRefunding) {

            if (accountNeedingRefunding != null && accountNeedingRefunding.get("EMPLOYEEID") != null) {

                //log various information before attempting to refund the account
                Logger.logMessage("About to refund account (#"+accountNeedingRefunding.get("EMPLOYEEID").toString()+") . Details.... ", Logger.LEVEL.DEBUG);
                Logger.logMessage("Account Name: "+accountNeedingRefunding.get("NAME").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("My QC Funding Terminal ID:"+accountNeedingRefunding.get("MYQCFUNDINGTERMINALID").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Terminal Name: "+accountNeedingRefunding.get("TERMINALNAME").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Terminal Balance: "+accountNeedingRefunding.get("TERMINALBALANCE").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Store Name: "+accountNeedingRefunding.get("STORENAME").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Store Limit: "+accountNeedingRefunding.get("STORELIMIT").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Store Balance: "+accountNeedingRefunding.get("STOREBALANCE").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Store Available: "+accountNeedingRefunding.get("STOREAVAILABLE").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Reload Threshold: "+accountNeedingRefunding.get("RELOADTHRESHOLD").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Reload Amount: "+accountNeedingRefunding.get("RELOADAMOUNT").toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage("Account Email Address: "+accountNeedingRefunding.get("EMAILADDRESS").toString(), Logger.LEVEL.DEBUG);

                // Get AccountID
                Integer accountID = Integer.parseInt(accountNeedingRefunding.get("EMPLOYEEID").toString());

                // Get GlobalBalance for Employee
                BigDecimal requestedRefundAmount;
                ArrayList<HashMap>  globalBalanceList = dm.parameterizedExecuteQuery("data.common.funding.getGlobalBalanceForEmployee",new Object[]{accountID},true);
                if(!globalBalanceList.isEmpty()){
                    requestedRefundAmount = new BigDecimal(globalBalanceList.get(0).get("GLOBALBALANCE").toString()).multiply(new BigDecimal("-1"));
                    if(requestedRefundAmount.equals(BigDecimal.ZERO)){
                        Logger.logMessage("Global Balance for account is zero. No Need to refund account.", Logger.LEVEL.ERROR);
                        continue;
                    }
                } else {
                    Logger.logMessage("Could not determine Global Balance for account that needed refunding!", Logger.LEVEL.ERROR);
                    continue;
                }

                // CHECK IF EMPLOYEE IS TOTALIZED
                if(!isEmployeeTotalized(accountID)){
                    Logger.logMessage("Can not auto refund employee: "+accountNeedingRefunding.get("EMPLOYEEID")+" Account is NOT totalized.", Logger.LEVEL.TRACE);
                    continue;
                } else {
                    Logger.logMessage("Employee: "+accountNeedingRefunding.get("EMPLOYEEID")+" is totalized.", Logger.LEVEL.DEBUG);
                }

                // CHECK if employee's Spending profile has only One debit profile assigned
                if(!doesSpendingProfileHaveOneDebitProfile(accountID)){
                    Logger.logMessage("Can not auto refund employee: "+accountNeedingRefunding.get("EMPLOYEEID")+" Spending Profile has more than one Purchase Limit assigned.", Logger.LEVEL.TRACE);
                    continue;
                } else {
                    Logger.logMessage("Employee: "+accountNeedingRefunding.get("EMPLOYEEID")+" passed check doesSpendingProfileHaveOneDebitProfile()", Logger.LEVEL.DEBUG);
                }

                //do the actual refunding - both processor and QC side - use "Super User" ID of 1 for the QC User on the Refund Transaction
                FundingModel fundingModel = new FundingModel();
                try{
                    fundingModel = fundingModel.refundRequestedAmount(requestedRefundAmount, accountID, 1);
                } catch(Exception ex){
                    // Log exception but CONTINUE to attempt refunding other accounts
                    Logger.logMessage("Error thrown in refundRequestedAmount(): "+ex.getMessage(), Logger.LEVEL.ERROR);
                    Logger.logException(ex);
                }

                //if the refunding was successful
                if (fundingModel.getRefundedAmount() != null && fundingModel.getRefundedAmount().compareTo(BigDecimal.ZERO) == 1) {

                    //log that the automatic refunding was successful
                    Logger.logMessage("Successfully automatically refunded account ID# ("+accountID+") an amount of a processed amount of "+fundingModel.getProcessedRefundAmount()+" and a refunded amount of "+fundingModel.getRefundedAmount(), Logger.LEVEL.TRACE);

                    //email account, if emails are configured properly, and that the refunding was successful
                    if (!CommonAPI.getEmailSMTPUser().equals("N/A") && accountNeedingRefunding.get("EMAILADDRESS") != null && !accountNeedingRefunding.get("EMAILADDRESS").toString().isEmpty()) {
                        emailRecentlyRefundedAccount(accountNeedingRefunding.get("NAME").toString(), accountNeedingRefunding.get("STORENAME").toString(), accountNeedingRefunding.get("EMAILADDRESS").toString(), fundingModel.getRefundedAmount());
                    }

                } else {
                    // Payment Method may be null
                    if(fundingModel.getAccountPaymentMethod() != null && fundingModel.getAccountPaymentMethod().getId() != null){

                        Logger.logMessage("Could not automatically refund account ID #" + accountID, Logger.LEVEL.ERROR);
                        // SEND TEMPLATE EMAIL - Define EmailTypeID
                        int emailTypeID = 9;
                        String emailAddress = accountNeedingRefunding.get("EMAILADDRESS").toString();

                        // Build params HashMap
                        HashMap params = new HashMap();
                        params.put("RecipientName", accountNeedingRefunding.get("NAME").toString());
                        params.put("ErrorMessage", "Could not automatically refund account.");

                        // Get EmployeeID from Funding Email
                        int employeeID = commonMMHFunctions.getEmployeeIDFromFundingEmailAddress(emailAddress);
                        if (employeeID > -1) {
                            params.put("EmployeeID", employeeID);
                        }
                        if(fundingModel.getAccountPaymentMethod().getPersonID() != null ){
                            params.put("PersonID", fundingModel.getAccountPaymentMethod().getPersonID());
                        }

                        // Send Email
                        commonMMHFunctions common = new commonMMHFunctions();
                        if(!emailAddress.isEmpty()){
                            if (common.sendTemplateEmail(emailAddress, 0, emailTypeID, "", params)) {
                                Logger.logMessage("Successfully sent automatic refund failure email to " + emailAddress, Logger.LEVEL.TRACE);
                            } else {
                                Logger.logMessage("Failed to send automatic refund failure email to " + emailAddress, Logger.LEVEL.ERROR);
                            }
                        }
                    } else {
                        Logger.logMessage("No payment method available to refund for account: "+accountID, Logger.LEVEL.TRACE);
                        // DISABLE Further Refunds for the Employee/Person if there are no DELAYED Employee Payment methods
                        // NOTE: Delayed Employee Payment methods MAY cause AccountPaymentMethods to be null
                        if(checkEmployeePaymentMethodsDelayed(accountID)){
                            fundingModel.updateEmployeeDisableRefunds(accountID);
                        }
                    }
                }
            } else {
                Logger.logMessage("Could not determine account information for account that needed refunding!", Logger.LEVEL.ERROR);
            }
        }

    }

    private static boolean doesSpendingProfileHaveOneDebitProfile(Integer accountID) {
        boolean hasOne = false;
        try {
            // EXECUTE QUERY TO GET COUNT of Debit Profiles in Spending Profile
            // ONLY ALLOW AUTO REFUNDING if there is one DEBIT Profile Assigned to Spending Profile
            ArrayList<HashMap> profileList = dm.parameterizedExecuteQuery("data.common.funding.getDebitProfilesInSpendingProfile",
                    new Object[]{accountID},
                    true);
            if(profileList.size() == 1){
                hasOne = true;
            }
        } catch (Exception ex){
            Logger.logMessage("Error in method doesSpendingProfileHaveOneDebitProfile", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return hasOne;
    }

    private static boolean isEmployeeTotalized(Integer employeeID) {
        boolean result = false;
        try {
            // EXECUTE QUERY TO GET EMPLOYEE SYNC STATUS
            ArrayList<HashMap> isTotalizedList = dm.parameterizedExecuteQuery("data.common.funding.getEmployeeTotalizedStatus",new Object[]{employeeID},true);
            if(!isTotalizedList.isEmpty()){
                if(isTotalizedList.get(0).get("QC_SYNC_STATUS").toString().equalsIgnoreCase("SYNCHED")){
                    result = true;
                }
            }
        } catch (Exception ex){
            Logger.logMessage("Error in method isEmployeeTotalized", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return result;
    }

    //send email notification to recently funded account letting them know their account was automatically funded
    private static void emailRecentlyRefundedAccount(String accountName, String refundedStoreName, String emailAddress, BigDecimal refundedAmount) throws Exception {

        //verify parameter passed into method
        if (emailAddress == null) {
            Logger.logMessage("While attempting to email recently refunded account... could not determine email address!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //verify parameter passed into method
        if (refundedAmount == null) {
            Logger.logMessage("While attempting to email recently refunded account... could not determine refunded amount!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //verify parameter passed into method
        if (accountName == null) {
            Logger.logMessage("While attempting to email recently refunded account... could not determine account name!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //verify parameter passed into method
        if (refundedStoreName == null) {
            Logger.logMessage("While attempting to email recently refunded account... could not determine any refunded store name!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        // Build params HashMap
        HashMap params = new HashMap();
        params.put("RecipientName", accountName);
        params.put("StoreName", refundedStoreName);
        params.put("Amount", refundedAmount.abs().toString());

        // Get EmployeeID from Funding Email
        int employeeID = commonMMHFunctions.getEmployeeIDFromFundingEmailAddress(emailAddress);
        if(employeeID > -1){params.put("EmployeeID", employeeID);}

        // Define EmailTypeID
        int emailTypeID = 7;

        // Send Email
        commonMMHFunctions common = new commonMMHFunctions();
        if(!emailAddress.isEmpty()){
            if(common.sendTemplateEmail(emailAddress, 0, emailTypeID, "", params)){
                Logger.logMessage("Successfully sent automatic refund e-mail to "+emailAddress, Logger.LEVEL.TRACE);
            } else {
                Logger.logMessage("Failed to send automatic refund e-mail to "+emailAddress, Logger.LEVEL.ERROR);
            }
        }
    }

    //retrieves and sets a processor payment token - this is used later on for funding/refunding
    private static AccountPaymentMethodModel retrieveProcessorPaymentToken(Integer authenticatedEmployeeID, AccountPaymentMethodModel accountPaymentMethodModel) throws Exception {
        return retrieveProcessorPaymentToken(authenticatedEmployeeID, accountPaymentMethodModel, false);
    }

    private static AccountPaymentMethodModel retrieveProcessorPaymentToken(Integer authenticatedEmployeeID, AccountPaymentMethodModel accountPaymentMethodModel, boolean forPerson) throws Exception {

        //throw InvalidAuthException if no authenticated employee is found
        if ( ( !forPerson && authenticatedEmployeeID == null ) || ( forPerson && accountPaymentMethodModel.getPersonID() == null) ) {
            throw new InvalidAuthException();
        }

        //throw missing data exception if accountPaymentMethodModel was not passed in
        if (accountPaymentMethodModel == null) {
            throw new MissingDataException();
        }

        //throw missing data exception if payment processor was not passed in
        Integer paymentProcessorID = accountPaymentMethodModel.getPaymentProcessorID();

        if (paymentProcessorID == null) {
            if ( forPerson ) {
                Logger.logMessage("Cannot find payment processorID - did the client send it in? For person ID# "+accountPaymentMethodModel.getPersonID());
            } else {
                Logger.logMessage("Cannot find payment processorID - did the client send it in? For account ID# "+authenticatedEmployeeID);
            }
            throw new MissingDataException();
        }

        //decide if we need to get processor payment token for the payment processor
        if (paymentProcessorID == 1) { //STRIPE
            accountPaymentMethodModel.setProcessorPaymentToken(createStripeCustomer(authenticatedEmployeeID, accountPaymentMethodModel, forPerson));
        } else if (paymentProcessorID == 6){
            accountPaymentMethodModel.setProcessorPaymentToken(createFreedomPayCustomer(authenticatedEmployeeID, accountPaymentMethodModel, forPerson));
        } else {
            if ( forPerson ) {
                Logger.logMessage("Did not attempt to create payment processor token for person ID# "+accountPaymentMethodModel.getPersonID());
            } else {
                Logger.logMessage("Did not attempt to create payment processor token for account ID#"+authenticatedEmployeeID, Logger.LEVEL.WARNING);
            }
        }

        return accountPaymentMethodModel;
    }

    //creates a "Stripe" customer for a SINGLE account payment method - this is used later on for funding/refunding
    private static String createStripeCustomer(Integer authenticatedEmployeeID, AccountPaymentMethodModel accountPaymentMethodModel) throws Exception {
        return createStripeCustomer(authenticatedEmployeeID, accountPaymentMethodModel, false);
    }

    //overload - creates a "Stripe" customer for a SINGLE account payment method - this is used later on for funding/refunding
    private static String createStripeCustomer(Integer authenticatedEmployeeID, AccountPaymentMethodModel accountPaymentMethodModel, boolean forPerson) throws Exception {
        //method variable which will be populated from a response from the Stripe API
        String stripeCustomerID = null;

        //throw InvalidAuthException if no authenticated employee is found
        if ( ( !forPerson && authenticatedEmployeeID == null ) || ( forPerson && accountPaymentMethodModel.getPersonID() == null) ) {
            throw new InvalidAuthException();
        }

        //throw missing data exception if accountPaymentMethodModel was not passed in
        if (accountPaymentMethodModel == null || accountPaymentMethodModel.getProcessorPaymentOneTimeToken() == null) {
            throw new MissingDataException();
        }

        //log before attempting to communicate with Stripe...
        if ( forPerson ) {
            Logger.logMessage("Attempting to create Stripe Customer for person ID# "+accountPaymentMethodModel.getPersonID());
        } else {
            Logger.logMessage("Attempting to create Stripe Customer for account ID# "+authenticatedEmployeeID);
        }

        //get private API key needed for communicating with Stripe API
        //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
        Integer paymentProcessorID = 1;
        String privateAPIKey = null;
        Object privateAPIKeyObj = dm.parameterizedExecuteScalar("data.common.funding.getPrivateKeyForProcessor",
            new Object[]{
                paymentProcessorID,
                authenticatedEmployeeID
            }
        );
        if (privateAPIKeyObj == null) {
            Logger.logMessage("Failed to retrieve Private API Key for Stripe.", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }
        privateAPIKey = privateAPIKeyObj.toString();

        //attempt to communicate with Stripe API
        try {

            //set the privateAPIKey on the Stripe Object
            Stripe.apiKey = privateAPIKey;

            String description;
            if ( forPerson ) {
                description = "Creating Stripe Customer for QC Person ID# "+accountPaymentMethodModel.getPersonID();
            } else {
                description = "Creating Stripe Customer for QC Account ID# "+authenticatedEmployeeID;
            }

            //set description and "source" (one time token) on temp hashmap
            Map<String, Object> customerParams = new HashMap<String, Object>();
            customerParams.put("description", description);
            customerParams.put("source", accountPaymentMethodModel.getProcessorPaymentOneTimeToken());

            //retrieve the stripeCustomerID (processorPaymentToken) from the Stripe Response and set to method variable
            Customer stripeCustomerObject = Customer.create(customerParams);
            stripeCustomerID = stripeCustomerObject.getId();

            //log after communicating with Stripe...
            if ( forPerson ) {
                Logger.logMessage("Successfully created Stripe Customer for person ID# "+accountPaymentMethodModel.getPersonID());
            } else {
                Logger.logMessage("Successfully created Stripe Customer for account ID# "+authenticatedEmployeeID);
            }

        } catch (CardException stripeCardEx) {
            //log stripe exception - means the card was declined or something similar
            Logger.logMessage("Card Exception from Stripe API. Card may have been declined.", Logger.LEVEL.ERROR);
            Logger.logMessage("Status is: " + stripeCardEx.getCode(), Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeCardEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeCardEx);

            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw generic Funding exception since we have logged the Stripe Exception
            throw new FundingException(stripeCardEx.getMessage());
        } catch (RateLimitException stripeRateLimitEx) {
            //log stripe exception - means too many requests made to the API too quickly
            Logger.logMessage("Rate Limit Exception from Stripe API. Too many requests made to the API too quickly.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeRateLimitEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeRateLimitEx);

            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw specific Funding Card exception since we have logged the Stripe Exception
            throw new FundingCardException("Too many requests made to the API too quickly"); //the stripe error msg is not presentable to users
        } catch (InvalidRequestException stripeInvalidRequestEx) {
            //log stripe exception - means invalid parameters were supplied to Stripe's API
            Logger.logMessage("Invalid Request Exception from Stripe API. Means invalid parameters were sent to Stripe's API.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeInvalidRequestEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeInvalidRequestEx);

            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw specific Funding Card exception since we have logged the Stripe Exception
            throw new FundingCardException("Invalid request was sent to Stripe's API");
        } catch (AuthenticationException stripeAuthEx) {
            //log stripe exception - Authentication with Stripe's API failed
            Logger.logMessage("Stripe Authentication Exception from Stripe API. Authentication with Stripe's API failed.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeAuthEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeAuthEx);

            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw specific Funding Card exception since we have logged the Stripe Exception
            throw new FundingCardException("Authentication with Stripe API failed");
        } catch (APIConnectionException stripeAPIConnectionEx) {
            //log stripe exception - Network communication with Stripe failed
            Logger.logMessage("Stripe Authentication Exception from Stripe API. Network communication with Stripe failed.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeAPIConnectionEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeAPIConnectionEx);

            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw specific Funding Card exception since we have logged the Stripe Exception
            throw new FundingCardException("Network communication with Stripe failed");
        } catch (StripeException stripeEx) {
            //log stripe exception - generic exception
            Logger.logMessage("Stripe Authentication Exception from Stripe API. Generic error.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + stripeEx.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(stripeEx);

            //TODO: Should likely replace these with more granular exceptions so we can deal with these scenarios differently -jrmitaly 2/16/2017
            //throw specific Funding Card exception since we have logged the Stripe Exception
            throw new FundingCardException("Authentication with Stripe API failed");
        }

        return stripeCustomerID;
    }

    private static String createFreedomPayCustomer(Integer authenticatedEmployeeID, AccountPaymentMethodModel accountPaymentMethodModel) throws Exception {
        return createFreedomPayCustomer(authenticatedEmployeeID, accountPaymentMethodModel, false);
    }

    private static String createFreedomPayCustomer(Integer authenticatedEmployeeID, AccountPaymentMethodModel accountPaymentMethodModel, boolean forPerson) throws Exception {

        //method variable which will be populated from a response from the Stripe API
        String customerToken;

        //throw InvalidAuthException if no authenticated employee is found
        if ( ( !forPerson && authenticatedEmployeeID == null ) || ( forPerson && accountPaymentMethodModel.getPersonID() == null) ) {
            throw new InvalidAuthException();
        }

        //throw missing data exception if accountPaymentMethodModel was not passed in
        if (accountPaymentMethodModel == null || accountPaymentMethodModel.getProcessorPaymentOneTimeToken() == null) {
            throw new MissingDataException();
        }

        //log before attempting to communicate with FreedomPay ...
        if ( forPerson ) {
            Logger.logMessage("Attempting to create FreedomPay Customer for person ID# "+accountPaymentMethodModel.getPersonID());
        } else {
            Logger.logMessage("Attempting to create FreedomPay Customer for account ID# "+authenticatedEmployeeID);
        }

        //get private API key needed for communicating with API
        Integer paymentProcessorID = accountPaymentMethodModel.getPaymentProcessorID(); // FREEDOMPAY

        //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
        ArrayList<HashMap> results = dm.parameterizedExecuteQuery("data.common.funding.getFreedomPayKeysForProcessor",
            new Object[] { paymentProcessorID, authenticatedEmployeeID }, true
        );

        if (results.isEmpty()) {
            Logger.logMessage("Failed to retrieve API keys for FreedomPay.", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }
        if(!results.get(0).containsKey("PAYMENTPROCESSORSTORENUM") || results.get(0).get("PAYMENTPROCESSORSTORENUM").toString().isEmpty()){
            Logger.logMessage("Failed to retrieve StoredID API key for FreedomPay.", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }
        String storeID = results.get(0).get("PAYMENTPROCESSORSTORENUM").toString();

        if(!results.get(0).containsKey("PAYMENTPROCESSORTERMINALNUM") || results.get(0).get("PAYMENTPROCESSORTERMINALNUM").toString().isEmpty()){
            Logger.logMessage("Failed to retrieve TerminalID API key for FreedomPay.", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }
        String terminalID = results.get(0).get("PAYMENTPROCESSORTERMINALNUM").toString();

        // retrieve Token from FreedomPay API
        try {

            // Using the TransactionID get the Token from the transaction
            // NOTE: This is a REST API CALL. DOES NOT appear to be available via the FREEWAY SOAP SERVICE

            FreedomPayClient freedomPayClient = new FreedomPayClient();
            String transactionID = accountPaymentMethodModel.getProcessorPaymentOneTimeToken();
            FreedomPayGetTransactionResponse response = freedomPayClient.getTransaction(transactionID, null);

            if(response.getResponseMessage() != null){
                // ERROR!! ERROR!! ERROR!!
                throw new MissingDataException();
            } else {
                customerToken = response.getTokenInformation().getToken();
            }

            if(response.getCardIssuer() == null || response.getCardIssuer().isEmpty()) {
                Logger.logMessage("Freedompay HPP has no Card Issuer - response: \n" + response.toString(), Logger.LEVEL.WARNING);
                response.setCardIssuerFromBIN();
            }

            //set the paymentMethodTypeID
            Integer paymentMethodTypeID;
            switch (response.getCardIssuer().toUpperCase()) {
                case "VISA":
                    paymentMethodTypeID = 1;
                    break;
                case "AMEX":
                    paymentMethodTypeID = 2;
                    break;
                case "MASTERCARD":
                    paymentMethodTypeID = 3;
                    break;
                case "DISCOVER":
                    paymentMethodTypeID = 4;
                    break;
                case "JCB":
                    paymentMethodTypeID = 5;
                    break;
                case "DINERS CLUB":
                    paymentMethodTypeID = 6;
                    break;
                default:
                    paymentMethodTypeID = 7;
                    break;
            }
            accountPaymentMethodModel.setPaymentMethodTypeID(paymentMethodTypeID);

            //set expiration month and year
            accountPaymentMethodModel.setPaymentMethodExpirationMonth(response.getDynExpMonth());
            accountPaymentMethodModel.setPaymentMethodExpirationYear(response.getDynExpYear());

            //set last four digits
            String lastFour = response.getMaskedCardNumber().substring(response.getMaskedCardNumber().length() - 4);
            accountPaymentMethodModel.setPaymentMethodLastFour(lastFour);

            //log after communicating with FreedomPay...
            if ( forPerson ) {
                Logger.logMessage("Successfully retrieved FreedomPay Customer Token for person ID# "+accountPaymentMethodModel.getPersonID());
            } else {
                Logger.logMessage("Successfully retrieved FreedomPay Customer Token for account ID# "+authenticatedEmployeeID, Logger.LEVEL.DEBUG);
            }

        } catch (Exception ex) {
            //log exception - means the card was declined or something similar
            Logger.logMessage("Card Exception from Freedom API. Card may have been declined.", Logger.LEVEL.ERROR);
            Logger.logMessage("Message is: " + ex.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(ex);
            //throw generic Funding exception since we have logged the Stripe Exception
            throw new FundingException(ex.getMessage());
        }

        return customerToken;
    }

    //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
    //retrieve and set the stripe api keys / freedompay credentials (since person account isn't linked to the funding terminal through a spending profile)
    public static HashMap setPaymentProcessorCredentials(HashMap accountPaymentMethodHM) throws Exception {
        ArrayList<HashMap> paymentProcessorList = dm.parameterizedExecuteQuery("data.common.funding.getPaymentProcessorForSingleMyQCTerminal",
            new Object[]{
                CommonAPI.convertModelDetailToInteger(accountPaymentMethodHM.get("EMPLOYEEID"))
            },
            true
        );

        if ( paymentProcessorList == null || paymentProcessorList.size() == 0 ) {
            throw new MissingDataException();
        }

        HashMap modelDetailHM = paymentProcessorList.get(0);
        Integer paymentProcessorID = CommonAPI.convertModelDetailToInteger( modelDetailHM.get("PAYMENTPROCESSORID") );
        if ( paymentProcessorID == 1 ) {
            accountPaymentMethodHM.put("PUBLICAPIKEY", CommonAPI.convertModelDetailToString(modelDetailHM.get("PUBLICAPIKEY")));
            accountPaymentMethodHM.put("PRIVATEAPIKEY", CommonAPI.convertModelDetailToString(modelDetailHM.get("PRIVATEAPIKEY")));
        } else if ( paymentProcessorID == 6 ) {
            accountPaymentMethodHM.put("PAYMENTPROCESSORSTORENUM", CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTPROCESSORSTORENUM")));
            accountPaymentMethodHM.put("PAYMENTPROCESSORTERMINALNUM", CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTPROCESSORTERMINALNUM")));
        }

        boolean terminalHasFundingFee = CommonAPI.convertModelDetailToInteger( modelDetailHM.get("TERMINALHASFUNDINGFEE") ) == 1;
        if ( terminalHasFundingFee ) {
            accountPaymentMethodHM.put("TERMINALHASFUNDINGFEE", 1);

            accountPaymentMethodHM.put("FUNDINGFLATFEEAMOUNT", ( CommonAPI.convertModelDetailToBigDecimal( modelDetailHM.get("FUNDINGFLATFEEAMOUNT") ) ) );

            accountPaymentMethodHM.put("FUNDINGPERCENTAGEFEEAMOUNT", ( CommonAPI.convertModelDetailToBigDecimal( modelDetailHM.get("FUNDINGPERCENTAGEFEEAMOUNT") ) ) );

            accountPaymentMethodHM.put("FUNDINGFEELABEL", ( CommonAPI.convertModelDetailToString(modelDetailHM.get("FUNDINGFEELABEL")) ) );

            accountPaymentMethodHM.put("FUNDINGFEEDISCLAIMER", ( CommonAPI.convertModelDetailToString( modelDetailHM.get("FUNDINGFEEDISCLAIMER") ) ) );

            accountPaymentMethodHM.put("FUNDINGFEECALCULATIONMETHODID", ( CommonAPI.convertModelDetailToInteger(modelDetailHM.get("FUNDINGFEECALCULATIONMETHODID")) ) );

            accountPaymentMethodHM.put("PASURCHARGEID", ( CommonAPI.convertModelDetailToInteger( modelDetailHM.get("PASURCHARGEID") ) ) );

            accountPaymentMethodHM.put("SURCHARGENAME", ( CommonAPI.convertModelDetailToString( modelDetailHM.get("SURCHARGENAME") ) ) );
        }

        return accountPaymentMethodHM;
    }

    //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
    //retrieves the ID of the ONLY "My QC Funding" terminal in the spending profile for this employee
    public static Integer getMyQCFundingTerminalID(Integer accountID) throws Exception {
        Integer myQCFundingTerminalID = null;
        Object myQCFundingTerminalObj = dm.parameterizedExecuteScalar("data.common.funding.getMyQCFundingTerminal", new Object[]{
                accountID
        });
        if (myQCFundingTerminalObj == null) {
            Logger.logMessage("Failed to get My QC Funding Terminal ID.", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }
        myQCFundingTerminalID = Integer.parseInt(myQCFundingTerminalObj.toString());
        return myQCFundingTerminalID;
    }

    //log automatic funding changes to the QC_EmployeeChanges table
    private static void logAutomaticFundingChanges(BigDecimal newReloadAmount, BigDecimal newReloadThreshold, String newReloadToken, String newReloadPaymentMethodID, AccountAutoFundingModel oldAccountFundingModel, Integer authenticatedEmployeeID) throws Exception {

        //throw InvalidAuthException if no authenticated employee is found
        if (authenticatedEmployeeID == null) {
            throw new InvalidAuthException();
        }

        //verify parameter passed into method
        if (oldAccountFundingModel == null) {
            Logger.logMessage("While attempting to log automatic funding changes... could not determine old automatic funding model - nothing to compare to!", Logger.LEVEL.ERROR);
            throw new MissingDataException();
        }

        //if there has been change, for reload amount, log it in QC_Employee changes table as the My QC User
        if (!newReloadAmount.equals(oldAccountFundingModel.getReloadAmount())) {
            Integer insertResult = dm.parameterizedExecuteNonQuery("data.myqc.recordAccountChangeAsMyQCUser",
                    new Object[]{
                        authenticatedEmployeeID,
                        "Reload Amount",
                         oldAccountFundingModel.getReloadAmount(),
                         newReloadAmount
                    }
            );
            if (insertResult != 1) {
                Logger.logMessage("Could not insert log record for reload amount.");
                //TODO: Need better exception here - more generic? Something else??
                throw new MissingDataException();
            }
        }

        //if there has been change, for reload threshold, log it in QC_Employee changes table as the My QC User
        if (!newReloadThreshold.equals(oldAccountFundingModel.getReloadThreshold())) {
            Integer insertResult = dm.parameterizedExecuteNonQuery("data.myqc.recordAccountChangeAsMyQCUser",
                    new Object[]{
                            authenticatedEmployeeID,
                            "Reload Threshold",
                            oldAccountFundingModel.getReloadThreshold(),
                            newReloadThreshold
                    }
            );
            if (insertResult != 1) {
                Logger.logMessage("Could not insert log record for reload threshold.");
                //TODO: Need better exception here - more generic? Something else??
                throw new MissingDataException();
            }
        }

        //if there has been change, for reload token, log it in QC_Employee changes table as the My QC User
        if (!newReloadToken.equals(oldAccountFundingModel.getReloadToken())) {
            Integer insertResult = dm.parameterizedExecuteNonQuery("data.myqc.recordAccountChangeAsMyQCUser",
                    new Object[]{
                            authenticatedEmployeeID,
                            "Reload Token",
                            oldAccountFundingModel.getReloadToken(),
                            newReloadToken
                    }
            );
            if (insertResult != 1) {
                Logger.logMessage("Could not insert log record for reload token.");
                //TODO: Need better exception here - more generic? Something else??
                throw new MissingDataException();
            }
        }

        //if there has been change, for reload amount, log it in QC_Employee changes table as the My QC User
        if (!newReloadPaymentMethodID.equals(oldAccountFundingModel.getReloadPaymentMethodID())) {
            Integer insertResult = dm.parameterizedExecuteNonQuery("data.myqc.recordAccountChangeAsMyQCUser",
                    new Object[]{
                            authenticatedEmployeeID,
                            "Reload Payment Method ID",
                            oldAccountFundingModel.getReloadPaymentMethodID(),
                            newReloadPaymentMethodID
                    }
            );
            if (insertResult != 1) {
                Logger.logMessage("Could not insert log record for reload payment method ID.");
                //TODO: Need better exception here - more generic? Something else??
                throw new MissingDataException();
            }
        }

    }

    //this method gets gateway user information by sending an instanceUserID to the gateway
    public Validation getConfigurableEmailAppCode(Validation requestValidation, HttpServletRequest request) {
        try {
            //determine the validation target from a gatewayID
            Integer gatewayID = CommonAPI.getGatewayID();
            if (!gatewayID.equals(0)) {

                //determine validation target from database
                validationTarget = dm.getSingleField("data.validation.getGatewayLocation", new Object[]{gatewayID }).toString();

                //this being hardcoded here keeps the actual path of the API out of the database
                validationPath = "auth/code/instance/"+CommonAPI.getInstanceID().toString();

                //prepare requestValidation for secure communication
                requestValidation.prepareInstanceToGatewayComm(request);

                //make the request
                responseValidation = securePostRequest(requestValidation);

            } else {
                String errorDetails = "Error in InstanceClient.getConfigurableEmailAppCode";
                Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
                requestValidation.setErrorDetails(errorDetails); //set error details
                return requestValidation;
            }
        } catch (Exception ex) {
            Logger.logMessage("Exception in InstanceClient.getConfigurableEmailAppCode", Logger.LEVEL.ERROR);
            Logger.logMessage("validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.getConfigurableEmailAppCode");
            return requestValidation;
        }

        try { //handle response - responseValidation
            return checkGatewayResponseValidationObject(responseValidation, false);
        } catch (Exception ex) {
            Logger.logMessage("Exception in responseValidation in InstanceClient.getConfigurableEmailAppCode", Logger.LEVEL.ERROR);
            Logger.logMessage("validationTarget: "+validationTarget, Logger.LEVEL.ERROR);
            Logger.logMessage("validationPath: "+validationPath, Logger.LEVEL.ERROR);
            requestValidation.handleGenericException(ex, "InstanceClient.getConfigurableEmailAppCode");
            return responseValidation;
        }
    }

    public static void fundAccountBeforeOrderSubmission(Integer employeeID, AccountAutoFundingModel accountFundingModel) {
        try {

            BigDecimal fundingAmount = new BigDecimal(accountFundingModel.getReloadAmount().toString());
            FundingModel fundingModel = new FundingModel(employeeID, true);
            fundingModel = fundingModel.chargeThenFundAccount(fundingAmount, fundingModel.getAccountPaymentMethod(), "auto");

            //if the funding was successful
            if (fundingModel.getFundedAmount() != null) {
                //log that the automatic funding was successful
                Logger.logMessage("Successfully automatically funded account ID# ("+employeeID+") an amount of "+fundingModel.getProcessedAmount());
            } else {
                Logger.logMessage("Could not automatically fund account - could not determine funded amount - for account ID #"+employeeID, Logger.LEVEL.ERROR);
            }

        } catch (Exception e) {
            Logger.logMessage("Error attempting to fund and notify accounts that have automatic funding before order submission.. in FundingHelper.fundAccountBeforeOrderSubmission!", Logger.LEVEL.ERROR);
        }
    }

    public static long getMinutesSinceFundingExecutorRan() {
        long minutes = 0;

        if(timeOfLastCheck.equals("")) {
            return minutes;
        }

        int backgroundTaskID = BACKGROUND_TASK_ID;

        String repeatInterval = dm.getSingleField("data.common.funding.getAutoFundingRepeatInterval",new Object[]{backgroundTaskID},null,"",false,true).toString();

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        LocalDateTime dateTimeOfLastCheck = LocalDateTime.parse(timeOfLastCheck, dateTimeFormatter);
        dateTimeOfLastCheck = dateTimeOfLastCheck.plusMinutes(Integer.parseInt(repeatInterval));

        minutes = LocalDateTime.now().until(dateTimeOfLastCheck, ChronoUnit.MINUTES);

        return minutes;
    }

    //this method sends a POST request using the secure Validation object and returns the response as a secure Validation object
    //TODO: (HIGH) needs SSL !! See createSSLClient method
    private Validation securePostRequest(Validation requestValidation) {

        //FOR TESTING - sends requests through fiddler (or any proxy really) -jrmitaly
        //System.setProperty("http.proxyHost", "10.1.246.11");
        //System.setProperty("http.proxyPort", "8888");

        return validationClient.target(validationTarget)
                .path(validationPath)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(requestValidation, MediaType.APPLICATION_JSON), Validation.class);
    }

    //this method checks the response validation object for errors or other issues before returning to the client
    private Validation checkGatewayResponseValidationObject(Validation responseValidation, Boolean validate) throws Exception {
        Logger.logMessage("INFO: Handling response from gateway host: "+responseValidation.getGatewayHostName(), Logger.LEVEL.TRACE);
        if (!responseValidation.verifyHash()) { //verify hash - checks against instance
            Logger.logMessage("ERROR: Error verifying hash in InstanceClient.checkGatewayResponseValidationObject!", Logger.LEVEL.ERROR);
            Logger.logMessage("DETAILS: "+responseValidation.getDetails(), Logger.LEVEL.ERROR); //log details
            Logger.logMessage("ERROR DETAILS: "+responseValidation.getErrorDetails(), Logger.LEVEL.ERROR); //log error details
        }
        if (validate) {
            if (!responseValidation.getIsValid()) { //check to see if the validation object is valid now..
                Logger.logMessage("ERROR: Tried to validate request - but it was not valid! In InstanceClient.checkGatewayResponseValidationObject", Logger.LEVEL.ERROR);
                Logger.logMessage("DETAILS: " + responseValidation.getDetails(), Logger.LEVEL.ERROR); //log details
                Logger.logMessage("ERROR DETAILS: " + responseValidation.getErrorDetails(), Logger.LEVEL.ERROR); //log error details
            }
        }
        return responseValidation;
    }

    private static boolean checkEmployeePaymentMethodsDelayed(Integer accountID) {
        // THIS METHOD Checks if there are any employeePaymentMethods for the employee/person which have DelayAutoFundingUntilDTM greater than today
        // IF There are EPM with a DelayAutoFundingUntilDTM then the fundingModel may have a NULL paymentMethod.
        boolean foundDelayed = false;

        try {
            ArrayList<HashMap> results = dm.parameterizedExecuteQuery("data.funding.checkForDelayedEmployeePaymentMethods",new Object[]{accountID}, true);
            if(!results.isEmpty()){
                // Found Delayed EPM
                foundDelayed = true;
            }
        } catch (Exception ex){
            Logger.logMessage("Error in method checkEmployeePaymentMethodsDelayed(): "+ex.getMessage());
            Logger.logException(ex);
        }

        return foundDelayed;
    }

    private static boolean validateTerminalBeforeFunding(HashMap myQCFundingTerminal){

        Boolean terminalValid = true;

        Integer myQCFundingTerminalID = Integer.parseInt(myQCFundingTerminal.get("TERMINALID").toString());

        // Validate Tender, Roa, and Processor fields aren't null.
        Boolean tenderValid = !myQCFundingTerminal.get("CCTENDERID").equals("");
        Boolean roaValid = !myQCFundingTerminal.get("FUNDINGPARECEIVEDACCTID").equals("");
        Boolean processorValid = !myQCFundingTerminal.get("FUNDINGPAYMENTPROCESSORID").equals("");

        // Validate that the Tender and ROA are both mapped to the same RC as the Funding Terminal
        Boolean tenderRCMappingValid = !myQCFundingTerminal.get("TENDERREVCENTERMAPID").equals("");
        Boolean roaRCMappingValid = !myQCFundingTerminal.get("ROAREVCENTERMAPID").equals("");

        // Inactive Values
        Boolean roaActive = !myQCFundingTerminal.get("ROAACTIVE").equals(false);
        Boolean tenderActive = !myQCFundingTerminal.get("TENDERACTIVE").equals(false);

        //Error Message
        String baseError = "Warning: While looking for Funding Terminals to auto fund accounts, found a terminal with an invalid configuration - Terminal ("+ myQCFundingTerminalID +") Has the following Problems:";
        ArrayList<String> errors = new ArrayList<>();

        if(!tenderValid){
            terminalValid = false;
            errors.add("The CCTenderID column is NULL");
        }

        if(!roaValid){
            terminalValid = false;
            errors.add("The FundingPAReceivedAcctID column is NULL");
        }

        if(!processorValid){
            terminalValid = false;
            errors.add("The FundingPaymentProcessorID column is NULL");
        }

        // Make sure the problem is the Revenue center isn't mapped correctly and not that the tender is just empty
        if(!tenderRCMappingValid && tenderValid){
            terminalValid = false;
            errors.add("The Tender is not mapped to the same Revenue Center as the Funding Terminal");
        }

        // Make sure the problem is the Revenue center isn't mapped correctly and not that the roa is just empty
        if(!roaRCMappingValid && roaValid){
            terminalValid = false;
            errors.add("The Received On Accounts is not mapped to the same Revenue Center as the Funding Terminal");
        }

        if(!roaActive){
            terminalValid = false;
            errors.add("The Received on Accounts is Inactive");
        }

        if(!tenderActive){
            terminalValid = false;
            errors.add("The Tender is Inactive");
        }

        if(!terminalValid){
            Logger.logMessage(baseError, Logger.LEVEL.IMPORTANT);
            if(errors.size() > 0){
                errors.forEach(error -> {
                    Logger.logMessage(error, Logger.LEVEL.IMPORTANT);
                });

                Logger.logMessage("These errors must be corrected before Funding/Refunding is allowed at this terminal", Logger.LEVEL.IMPORTANT);
            }
        }

        return terminalValid;

    }

    @Override
    public void run() {
        //Only run the funding when both WARs are up to date
        Logger.logMessage("Checking if account funding should be handeled on this WAR...", Logger.LEVEL.TRACE);
        //Object QCShouldHandleFunding = dm.parameterizedExecuteScalar("data.servletListener.shouldQCHandleAccountFunding", new Object[]{"4.0.42","9.4.0.6"});
        boolean QCShouldRunFunding = true;
        try{
            //TEST THINGS OUT TODO: remove
//            assert shouldQCRunFunding("4.0.42","9.4.0.6") == true :     "default both ==";
//            assert shouldQCRunFunding("4.0.43","9.4.0.7") == true :     "both greater";
//            assert shouldQCRunFunding("4.1","9.5") == true :            "both greater (truncated)";
//            assert shouldQCRunFunding("4.1.0","9.5.0") == true :        "both greater truncated";
//            assert shouldQCRunFunding("4.1.0","10.0") == true :         "10 test, both greater";
//            assert shouldQCRunFunding("4.1.1","10.0.1") == true :       "10 test, both greater";
//            assert shouldQCRunFunding("5","10") == true :               "10test, both greater truncated";
//            assert shouldQCRunFunding("4.0.100","9.4.0.10") == true :   "10test, both greater";
//            assert shouldQCRunFunding("4.0.42.1","9.4.0.6.1") == true : "slightly larger (one more sub-version than threshold)";
//
//            assert shouldQCRunFunding("5.0.42","9.3.0.12") == false :   "qc less, myqc greater";
//            assert shouldQCRunFunding("4.0.41","9.4.0.6") == false :    "myqc less";
//            assert shouldQCRunFunding("4.0.42","9.4.0.5") == false :    "qc less";
//            assert shouldQCRunFunding("4.0.41","9.4.0.5") == false :    "both less";
//            assert shouldQCRunFunding("3.0.42","8.4.0.6") == false :    "both less in a different way";
//            assert shouldQCRunFunding("4.0.42","9.3.0.12") == false :   "qc less in a different way";
//            assert shouldQCRunFunding("4.0.42","9.4.0") == false :      "qc less (truncated)";
//            assert shouldQCRunFunding("4.0","9.4.0.6") == false :       "myqc less (truncated)";
//            assert shouldQCRunFunding("4.0.10","9.4.0.10") == false :   "10 test";
//            assert shouldQCRunFunding("4","10") == false :              "10test, qc greater truncated";
//            assert shouldQCRunFunding("4.0.41.1","9.4.0.5.1") == false :"one sub version less";
//            assert shouldQCRunFunding("","") == false :                 "blank";
//            assert shouldQCRunFunding("4.0.42","") == false :           "qc blank";
//            assert shouldQCRunFunding("","9.4.0.6") == false :          "myqc blank";
//            assert shouldQCRunFunding("5.0.0","9.4.0.13") == true :     "current build version";
//            assert shouldQCRunFunding("4.0.001", "9.4.0.7") == false :  "myqc version too low with leading 0s";
//            assert shouldQCRunFunding("4.0.43", "9.4.0.001") == false : "qc version too low with leading 0s";
//            assert shouldQCRunFunding("4.0.042", "9.4.0.06") == true :  "myqc version and qc version at threshold with leading 0s";

            ArrayList<HashMap> versions = dm.parameterizedExecuteQuery("data.servletListener.getAccountFundingVersions", new Object[]{}, true);
            //compare version numbers,
            QCShouldRunFunding = shouldQCRunFunding(versions.get(0).get("MYQCAPIVERSION").toString(), versions.get(0).get("QCVERSION").toString());
        }catch (Exception e){
            Logger.logException(e);
        }catch(Throwable t){
            Logger.logMessage(t.getMessage(), Logger.LEVEL.ERROR);
        }

        if(((applicationID == 1 && QCShouldRunFunding) || (applicationID == 2 && !QCShouldRunFunding))){
            Logger.logMessage("Determined account funding should be handeled on this WAR.", Logger.LEVEL.IMPORTANT);
            if(isActive()) {
                setAutoFundingApplicationID();
                //funds any valid and eligible account in the system that needs to be automatically funded right now
                automaticallyFundAccounts();

                //refunds any valid and eligible account in the system that needs to be automatically refunded right now
                automaticallyRefundAccounts();
            }
        }
    }

    private boolean shouldQCRunFunding(String myQCVersion, String QCVersion){
        int myQCComp = versionCompare(myQCVersion.split("\\."), MYQC_VERSION_THRESHOLD);
        int QCComp = versionCompare(QCVersion.split("\\."), QC_VERSION_THRESHOLD);
        return(myQCComp >= 0 && QCComp >= 0);
    }
    /**
     * Compares 2 lists of version number strings major to minor
     * NOTE: this will "break" when comparing "1.001" to "1.1" (version 1.001 is equivalent to version 1.1)
     * @param version
     * @param reference
     * @return -1 when the version is lower than the reference, 1 when the version is greater than the referece and 0 when they are equal
     */
    private int versionCompare(String[] version, String[] reference){
        int cmp = 0;
        int v = 0;
        while(cmp == 0 && v < reference.length && v < version.length){
            if(Integer.parseInt(version[v]) > Integer.parseInt(reference[v])){
                cmp = 1;
            }else if (Integer.parseInt(version[v]) < Integer.parseInt(reference[v])){
                cmp = -1;
            }else{
                cmp = 0;
            }
            v++;
        }
        //the above only goes as far as the shortest version... if there is anything left over and cmp is still 0, the longer is a higher version
        if(cmp == 0 && (v < reference.length || v < version.length)){
            if(version.length > reference.length) cmp = 1;
            else if(version.length < reference.length) cmp = -1;
        }

        return cmp;
    }

    private void setAutoFundingApplicationID() {
        try {
            // get the application id set in the database
            int oldApplicationID = Integer.parseInt(dm.parameterizedExecuteScalar("data.common.funding.getApplicationID", null).toString());

            // if the application ID set in the DB matches the one hardcoded in the project, return
            if( oldApplicationID == applicationID ) {
                return;
            }
            // otherwise, update the record and record the change
            else {
                // update QC_BackgroundTask.ApplicationID to the applicationID
                int check = dm.parameterizedExecuteNonQuery("data.common.funding.setApplicationID",
                        new Object [] {
                                applicationID
                        });

                // if the number of affected rows is not 1, must return
                if(check != 1) {
                    Logger.logMessage("Could not set the Application ID in FundingHelper.setAutoFundingApplicationID", Logger.LEVEL.ERROR);
                    return;
                }

                // get necessary params for the change table - SchemaTableID & SchemaTableColumnID
                ArrayList<HashMap> changedData = dm.parameterizedExecuteQuery("data.globalAuditLogger.getSchemaTableColumnID",
                        new Object [] {
                                "QC_BackgroundTask",
                                "ApplicationID"
                        },
                        true);

                int schemaTableID = 0;
                int schemaTableColumnID = 0;

                if( changedData.get(0).get("SCHEMATABLEID") == null || changedData.get(0).get("SCHEMATABLECOLUMNID") == null ) {
                    Logger.logMessage("Could not get the SchemaTableID and/or SchemaTableColumnID in FundingHelper.setAutoFundingApplicationID", Logger.LEVEL.ERROR);
                }
                else {
                    schemaTableID = Integer.parseInt(changedData.get(0).get("SCHEMATABLEID").toString());
                    schemaTableColumnID = Integer.parseInt(changedData.get(0).get("SCHEMATABLECOLUMNID").toString());
                }

                /* insert into change data table
                    1 SchemaTableID
                    2 ObjectID - 5 is ApplicationID
                    3 SchemaTableColumnID
                    4 UserID - 1 is superuser
                    5 BeforeValue - oldApplicationID
                    6 AfterValue - applicationID
                    7 changeDTM - use Timestamp as there is a check for null, cannot use NULLIF
                 */
                Object [] changedDataArr = new Object [] {
                        schemaTableID,
                        5,
                        schemaTableColumnID,
                        1,
                        oldApplicationID,
                        applicationID,
                        new Timestamp(System.currentTimeMillis())
                };

                check = dm.parameterizedExecuteNonQuery("data.globalAuditLogger.insertChangeDataRecord", changedDataArr);

                if(check != 1) {
                    Logger.logMessage("Could not add funding to FundingHelper.setAutoFundingApplicationID", Logger.LEVEL.ERROR);
                }

            }
        }
        catch(Exception e) {
            Logger.logMessage("Error in FundingHelper.setAutoFundingApplicationID: " + e.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(e);
        }

    }

}