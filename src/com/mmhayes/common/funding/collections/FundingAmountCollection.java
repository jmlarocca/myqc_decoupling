package com.mmhayes.common.funding.collections;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//funding dependencies
import com.mmhayes.common.funding.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.*;

/* Funding Amount Collection
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-08-27 15:23:27 -0400 (Tue, 27 Aug 2019) $: Date of last commit
 $Rev: 9357 $: Revision of last commit

 Notes: Collection of Funding Amount Models
*/
public class FundingAmountCollection {
    List<FundingAmountModel> collection = new ArrayList<FundingAmountModel>();
    @JsonIgnore private static DataManager dm = new DataManager();
    @JsonIgnore private Integer loggedInEmployeeID = null;

    //constructor - populates the collection
    public FundingAmountCollection(HttpServletRequest request) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(CommonAuthResource.checkRequestHeaderForMMHayesAuth(request));

        //populate this collection with models
        populateCollection();

    }

    //constructor - populates the collection
    public FundingAmountCollection(Integer employeeID) throws Exception {

        //determine the employeeID from the request
        setLoggedInEmployeeID(employeeID);

        //populate this collection with models
        populateCollection();

    }

    //populates this collection with models
    public FundingAmountCollection populateCollection() throws Exception {

        //throw InvalidAuthException if no authenticated employee is found
        if (getLoggedInEmployeeID() == null) {
            throw new InvalidAuthException();
        }

        //get all models in an array list
        ArrayList<HashMap> fundingAmountList = dm.parameterizedExecuteQuery("data.funding.getFundingAmounts",
                new Object[]{
                        getLoggedInEmployeeID()
                },
                true
        );

        //iterate through list, create models and add them to the collection
        if (fundingAmountList != null && fundingAmountList.size() > 0) {
            for (HashMap fundingAmountHM : fundingAmountList) {
                if (fundingAmountHM.get("FUNDINGAMOUNTID") != null && !fundingAmountHM.get("FUNDINGAMOUNTID").toString().isEmpty()) {
                    getCollection().add(new FundingAmountModel(fundingAmountHM));
                }
            }
        }

        return this;
    }

    //getter
    public List<FundingAmountModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

}
