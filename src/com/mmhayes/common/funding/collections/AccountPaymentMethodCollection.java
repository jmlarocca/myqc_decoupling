package com.mmhayes.common.funding.collections;

//mmhayes dependencies
import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.api.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.funding.FundingHelper;
import com.mmhayes.common.utils.*;

//funding dependencies
import com.mmhayes.common.funding.models.*;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.*;

/* Account Payment Method Collection
 Last Updated (automatically updated by SVN)
 $Author: jmdottavio $: Author of last commit
 $Date: 2019-09-18 14:46:58 -0400 (Wed, 18 Sep 2019) $: Date of last commit
 $Rev: 9455 $: Revision of last commit

 Notes: Collection of Account Payment Method Models
*/
public class AccountPaymentMethodCollection {
    List<AccountPaymentMethodModel> collection = new ArrayList<AccountPaymentMethodModel>();
    @JsonIgnore private static DataManager dm = new DataManager();
    @JsonIgnore private Integer loggedInEmployeeID = null;
    @JsonIgnore private Integer personID = null;

    public AccountPaymentMethodCollection() {

    }

    //constructor - populates the collection - used for getting the employee's payment method when sending a purchase confirmation email
    public AccountPaymentMethodCollection(Integer authenticatedEmployeeID) throws Exception {

        //set the employeeID
        setLoggedInEmployeeID(authenticatedEmployeeID);

        //populate this collection with models
        populateCollection();
    }

    //constructor - populates the collection
    public AccountPaymentMethodCollection(HttpServletRequest request) throws Exception {
        if ( CommonAuthResource.isKOA( request ) ) {
            setLoggedInEmployeeID(CommonAuthResource.getAccountIDFromAccountHeader(request));
        } else {
            //throw InvalidAuthException if no authenticated employee is found
            HashMap authenticationHM = CommonAuthResource.determineAuthDetailsFromAuthHeader(request);
            AccountModel authenticatedEmployeeModel = (AccountModel) authenticationHM.get("accountModel");
            Integer authenticatedEmployeeID = authenticatedEmployeeModel.getId();
            Integer personID = CommonAPI.convertModelDetailToInteger(authenticationHM.get("personID"));

            setLoggedInEmployeeID(authenticatedEmployeeID);
            setPersonID(personID);
        }

        //populate this collection with models
        populateCollection();
    }

    //populates this collection with models
    public AccountPaymentMethodCollection populateCollection() throws Exception {

        boolean forPerson = (getPersonID() != null);
        String getPaymentMethodSQL = !forPerson ? "data.common.funding.getAccountPaymentMethods" :  "data.common.funding.getPersonAccountPaymentMethods";
        Integer paymentMethodAccountID = !forPerson ? getLoggedInEmployeeID() : getPersonID();

        //get all models in an array list
        ArrayList<HashMap> accountPaymentMethodList = dm.parameterizedExecuteQuery(getPaymentMethodSQL,
            new Object[]{
                    paymentMethodAccountID
            },
            true
        );

        //iterate through list, create models and add them to the collection
        if (accountPaymentMethodList != null && accountPaymentMethodList.size() > 0) {
            for (HashMap accountPaymentMethodHM : accountPaymentMethodList) {
                if (accountPaymentMethodHM.get("EMPLOYEEPAYMENTMETHODID") != null && !accountPaymentMethodHM.get("EMPLOYEEPAYMENTMETHODID").toString().isEmpty()) {
                    if ( forPerson ) {
                        //if for person, then the employeeID will not be set, so set it
                        accountPaymentMethodHM.put("EMPLOYEEID", getLoggedInEmployeeID());

                        //TODO: This method relies on one "My QC Funding" terminal type per spending profile. One day it should support multiple -jmd 8/27/18
                        //the api keys will not be set, retrieve and set them
                        accountPaymentMethodHM = FundingHelper.setPaymentProcessorCredentials(accountPaymentMethodHM);
                    }

                    getCollection().add(new AccountPaymentMethodModel(accountPaymentMethodHM));
                }
            }
        }

        return this;
    }

    //getter
    public List<AccountPaymentMethodModel> getCollection() {
        return this.collection;
    }

    //getter
    private Integer getLoggedInEmployeeID() {
        return this.loggedInEmployeeID;
    }

    //setter
    private void setLoggedInEmployeeID(Integer loggedInEmployeeID) {
        this.loggedInEmployeeID = loggedInEmployeeID;
    }

    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

}
