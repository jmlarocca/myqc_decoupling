package com.mmhayes.common.keypad.models;

//MMHayes Dependencies

import com.fasterxml.jackson.annotation.JsonGetter;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

//Other Dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: nyu $: Author of last commit
 $Date: 2020-04-14 19:40:45 -0400 (Tue, 14 Apr 2020) $: Date of last commit
 $Rev: 11368 $: Revision of last commit
*/
public class KeypadDetailModel {

    //region Private Variables

    //Keypad Fields
    private Integer keypadId = null;
    private Boolean showPrices = false;
    private String kpMenuLabel = "";
    private String keypadName = "";
    private Integer kpButtonIconPosition = null;
    private String kpButtonFontName = "";
    private Integer kpButtonFontSize = null;
    private Boolean kpButtonFontBold = false;
    private String kpButtonFontColor = "";
    private String kpButtonBackgroundColor = null;
    private Integer numColumns = null;
    private Integer numRows = null;
    private Boolean kpIsModifier = false;
    private Boolean kpIsUpsell = false;
    private Boolean kpIsPayment = false;
    private Boolean isRotatingKP = false;
    private Boolean kpDisplayDescOnPOS = false;
    private String kpDescription = "";

    //Keypad Detail Fields
    private Integer buttonSortOrder = null;
    private Integer buttonTypeId = null;
    private Integer buttonItemId = null;
    private String buttonText = "";
    private String kpButtonIconFileName = "";
    private Integer kpDetailButtonIconPosition = null;
    private Boolean kpDetailUseImageMargin = false;
    private Boolean kpDetailShrinkImageToFit = false;
    private Boolean showNameOverlay = false;
    private String kpDetailTokenizedIconFileName = "";
    private String kpDetailButtonFontName = "";
    private Integer kpDetailButtonFontSize = null;
    private String kpDetailButtonFontColor = null;
    private String kpDetailButtonBackgroundColor = null;
    private String subMenuName = "";
    private Boolean kpDetailAutoShowPrepOptionSet = true;
    private Integer kpDetailPrepOptionSetId = null;

    //PLU Fields
    private Integer paPluId = null;
    private String name = "";
    private String pluDescription = "";
    private Boolean pluActive = false;
    private String pluCode = "";
    private BigDecimal price = BigDecimal.ZERO;
    private BigDecimal priceLvl1 = BigDecimal.ZERO;
    private BigDecimal priceLvl2 = BigDecimal.ZERO;
    private BigDecimal priceLvl3 = BigDecimal.ZERO;
    private BigDecimal priceLvl4 = BigDecimal.ZERO;
    private BigDecimal priceLvl5 = BigDecimal.ZERO;
    private BigDecimal priceLvl6 = BigDecimal.ZERO;
    private BigDecimal priceLvl7 = BigDecimal.ZERO;
    private BigDecimal priceLvl8 = BigDecimal.ZERO;
    private BigDecimal priceLvl9 = BigDecimal.ZERO;
    private BigDecimal minQuantity = BigDecimal.ZERO;
    private BigDecimal maxQuantity = BigDecimal.ZERO;
    private Integer modifierSetId = null;
    private Integer pluLinkedItemTypeId = null;
    private Integer pluLinkedItemId = null;
    private BigDecimal pluLinkedMinQuantity = BigDecimal.ZERO;
    private BigDecimal pluLinkedMaxQuantity = BigDecimal.ZERO;
    private String taxIds = "";
    private String discountIds = "";
    private String pluButtonText = "";
    private String pluButtonIconFileName = "";
    private String pluTokenizedIconFileName = "";
    private Boolean pluShrinkImageToFit = false;
    private Boolean pluUseImageMargin = false;

    private Integer pluButtonIconPosition = null;
    private String pluButtonFontName = "";
    private Integer pluButtonFontSize = null;
    private Boolean pluButtonFontBold = false;
    private String pluButtonFontColor = "";
    private String pluButtonBackgroundColor = "";
    private Boolean isModifier = false;
    private Boolean commentRequired = false;
    private Boolean itemDisabled = false;
    private Boolean allowDisabled = false;
    private Boolean scaleUsed = false;
    private Integer paTareId = null;
    private Boolean priceOpen = false;
    private Boolean pricePreset = false;
    private Boolean tareOpen = false;
    private Boolean inventoryItem = false;
    private BigDecimal maxOpenPrice = BigDecimal.ZERO;
    private BigDecimal minOpenPrice = BigDecimal.ZERO;
    private Integer ownershipGroupId = null;
    private Integer paSubDeptId = null;
    private Integer paDepartmentId = null;
    private BigDecimal nutritionInfo1 = BigDecimal.ZERO;
    private BigDecimal nutritionInfo2 = BigDecimal.ZERO;
    private BigDecimal nutritionInfo3 = BigDecimal.ZERO;
    private BigDecimal nutritionInfo4 = BigDecimal.ZERO;
    private BigDecimal nutritionInfo5 = BigDecimal.ZERO;
    private Boolean wellness = false;
    private Boolean autoShowModSet = false;
    private String printerIds = "";
    private String rewardIds = "";
    private Boolean pluAutoShowPrepOptionSet = true;
    private Integer pluPrepOptionSetId = null;
    private Integer pluUpsellComplementaryKeypadID = null;
    private String taxDeleteIds = "";
    private BigDecimal fixedNetWeight = null;
    private BigDecimal grossWeight = null;
    private Boolean pluStarbucksCardEligible = false;

    //PA Discount Fields
    private Integer paDiscountId = null;
    private String dscName = "";
    private String dscButtonText = "";

    //Hierarchical Fields
    private String iconFileName = "";
    private Boolean useImageMargin = false;
    private Boolean shrinkImageToFit = false;
    private Integer iconPosition = null;
    private String tokenizedIconFileName = "";
    private String fontColor = "";
    private String fontName = "";
    private Integer fontSize = null;
    private Boolean fontBold = false;
    private String backgroundColor = "";

    //endregion

    //region Constructors

    public KeypadDetailModel() {

    }

    public KeypadDetailModel(HashMap modelDetailHM) {
        setModelProperties(modelDetailHM);

    }

    //endregion

    public KeypadDetailModel setModelProperties(HashMap modelDetailHM) {
        setKeypadId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("KPID")));
        setShowPrices(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("KPSHOWPRICE"), false));
        setKpMenuLabel(CommonAPI.convertModelDetailToString(modelDetailHM.get("KPMENULABEL")));
        setKeypadName(CommonAPI.convertModelDetailToString(modelDetailHM.get("KPNAME")));
        setKpButtonIconPosition(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("KPBUTTONICONPOSITION")));
        setKpButtonFontName(CommonAPI.convertModelDetailToString(modelDetailHM.get("KPBUTTONFONTNAME")));
        setKpButtonFontSize(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("KPBUTTONFONTSIZE")));
        setKpButtonFontBold(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("KPBUTTONFONTBOLD"), false));
        setKpButtonFontColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("KPBUTTONFONTCOLOR")));
        setKpButtonBackgroundColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("KPBUTTONBACKGROUNDCOLOR")));
        setNumColumns(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("KPNUMCOLUMNS")));
        setNumRows(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("KPNUMROWS")));
        setKpIsModifier(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("KPISMODIFIER"), false));
        setKpIsUpsell(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("KPISUPSELL"), false));
        setKpIsPayment(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("KPISPAYMENT"), false));
        setRotatingKP(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ISROTATIONKP"), false));
        setKpDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("KPDESCRIPTION")));
        setKpDisplayDescOnPOS(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("KPDISPLAYDESCONPOS"), false));

        //Keypad Detail fields
        setButtonSortOrder(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("BUTTONSORTORDER")));
        setButtonTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("BUTTONTYPEID")));
        setButtonItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("BUTTONITEMID")));
        setButtonText(CommonAPI.convertModelDetailToString(modelDetailHM.get("BUTTONTEXT")));
        setKpButtonIconFileName(CommonAPI.convertModelDetailToString(modelDetailHM.get("KPBUTTONICONFILENAME")));
        setKpDetailButtonIconPosition(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("KPDETAILBUTTONICONPOSITION")));
        setKpDetailUseImageMargin(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("KPDETAILUSEIMAGEMARGIN"), false));
        setKpDetailShrinkImageToFit(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("KPDETAILSHRINKIMAGETOFIT"), false));
        setShowNameOverlay(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SHOWNAMEOVERLAY"), false));
        setKpDetailTokenizedIconFileName(CommonAPI.convertModelDetailToString(modelDetailHM.get("KPDETAILTOKENIZEDICONFILENAME")));
        setKpDetailButtonFontName(CommonAPI.convertModelDetailToString(modelDetailHM.get("KPDETAILBUTTONFONTNAME")));
        setKpDetailButtonFontSize(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("KPDETAILBUTTONFONTSIZE")));
        setKpDetailButtonFontColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("KPDETAILBUTTONFONTCOLOR")));
        setKpDetailButtonBackgroundColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("KPDETAILBUTTONBACKGROUNDCOLOR")));
        setSubMenuName(CommonAPI.convertModelDetailToString(modelDetailHM.get("SUBMENUNAME")));
        setKpDetailAutoShowPrepOptionSet(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("KPDETAILAUTOSHOWPREPOPTIONSET"), false));
        setKpDetailPrepOptionSetId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("KPDETAILPREPOPTIONSETID")));

        //PLU Fields
        setPaPluId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAPLUID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setPluDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("PLUDESCRIPTION")));
        setPluActive(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PLUACTIVE"), false));
        setPluCode(CommonAPI.convertModelDetailToString(modelDetailHM.get("PLUCODE")));
        setPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICE")));
        setPriceLvl1(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICELVL1")));
        setPriceLvl2(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICELVL2")));
        setPriceLvl3(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICELVL3")));
        setPriceLvl4(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICELVL4")));
        setPriceLvl5(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICELVL5")));
        setPriceLvl6(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICELVL6")));
        setPriceLvl7(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICELVL7")));
        setPriceLvl8(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICELVL8")));
        setPriceLvl9(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICELVL9")));
        setMinQuantity(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("MINQUANTITY")));
        setMaxQuantity(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("MAXQUANTITY")));
        setModifierSetId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("MODIFIERSETID")));
        setPluLinkedItemTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PLULINKEDITEMTYPEID")));
        setPluLinkedItemId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PLULINKEDITEMID")));
        setPluLinkedMinQuantity(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PLULINKEDMINQUANTITY")));
        setPluLinkedMaxQuantity(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PLULINKEDMAXQUANTITY")));
        setTaxIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("TAXIDS")));
        setDiscountIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("DISCOUNTIDS")));
        setPluButtonText(CommonAPI.convertModelDetailToString(modelDetailHM.get("PLUBUTTONTEXT")));
        setPluButtonIconFileName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PLUBUTTONICONFILENAME")));
        setPluTokenizedIconFileName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PLUTOKENIZEDICONFILENAME")));
        setPluShrinkImageToFit(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PLUSHRINKIMAGETOFIT"), false));
        setPluUseImageMargin(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PLUUSEIMAGEMARGIN"), false));
        setPluButtonIconPosition(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PLUBUTTONICONPOSITION")));
        setPluButtonFontName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PLUBUTTONFONTNAME")));
        setPluButtonFontSize(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PLUBUTTONFONTSIZE")));
        setPluButtonFontBold(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PLUBUTTONFONTBOLD"), false));
        setPluButtonFontColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("PLUBUTTONFONTCOLOR")));
        setPluButtonBackgroundColor(CommonAPI.convertModelDetailToString(modelDetailHM.get("PLUBUTTONBACKGROUNDCOLOR")));
        setIsModifier(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ISMODIFIER"), false));
        setCommentRequired(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("COMMENTREQUIRED"), false));
        setItemDisabled(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ITEMDISABLED"), false));
        setAllowDisabled(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWDISABLED"), false));
        setScaleUsed(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("SCALEUSED"), false));
        setPaTareId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATAREID")));
        setPriceOpen(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PRICEOPEN"), false));
        setPricePreset(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PRICEPRESET"), false));
        setTareOpen(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("TAREOPEN"), false));
        setInventoryItem(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("INVENTORYITEM")));
        setMaxOpenPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("MAXOPENPRICE")));
        setMinOpenPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("MINOPENPRICE")));
        setOwnershipGroupId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("OWNERSHIPGROUPID")));
        setPaSubDeptId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASUBDEPTID")));
        setPaDepartmentId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADEPARTMENTID")));
        setNutritionInfo1(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO1")));
        setNutritionInfo2(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO2")));
        setNutritionInfo3(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO3")));
        setNutritionInfo4(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO4")));
        setNutritionInfo5(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("NUTRITIONINFO5")));
        setWellness(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("WELLNESS"), false));
        setAutoShowModSet(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("AUTOSHOWMODSET"), false));
        setPrinterIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("PRINTERIDS")));
        setRewardIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("REWARDIDS")));
        setPluAutoShowPrepOptionSet(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PLUAUTOSHOWPREPOPTIONSET"), false));
        setPluPrepOptionSetId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PLUPREPOPTIONSETID")));
        setUpsellComplementaryKeypadID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PLUUPSELLCOMPLEMENTARYKEYPADID")));
        setTaxDeleteIds(CommonAPI.convertModelDetailToString(modelDetailHM.get("TAXDELETEIDS")));
        setGrossWeight(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("GROSSWEIGHT")));
        setFixedNetWeight(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("FIXEDNETWEIGHT")));
        setPluStarbucksCardEligible(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("STARBUCKSCARDELIGIBLE"), false));

        //Discount Fields
        setPaDiscountId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADISCOUNTID")));
        setDscName(CommonAPI.convertModelDetailToString(modelDetailHM.get("DSCNAME")));
        setDscButtonText(CommonAPI.convertModelDetailToString(modelDetailHM.get("DSCBUTTONTEXT")));

        return this;
    }

    public static boolean isKeypadValidAndActive(TerminalModel terminalModel, Integer keypadId) throws Exception {
        DataManager dm = new DataManager();
        boolean result = false;

        //get all models in an array list
        ArrayList<HashMap> keypadList = dm.parameterizedExecuteQuery("data.posapi30.GetOneKeypadId",
                new Object[]{terminalModel.getRevenueCenterId(), keypadId},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //keypad list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(keypadList, "Keypad Not Found", terminalModel.getId());

        HashMap keypadIdHM = keypadList.get(0);
        Integer validatedKeypadId = CommonAPI.convertModelDetailToInteger(keypadIdHM.get("ID"));
        if (validatedKeypadId != null){
            result = true;
        }

        return result;
    }

    //region Getters/Setters
    //Always Return
    public Integer getKeypadId() {
        return keypadId;
    }

    public void setKeypadId(Integer keypadId) {
        this.keypadId = keypadId;
    }

    //Always Return
    public Boolean getShowPrices() {
        return showPrices;
    }

    public void setShowPrices(Boolean showPrices) {
        this.showPrices = showPrices;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getKpMenuLabel() {
        return kpMenuLabel;
    }

    public void setKpMenuLabel(String kpMenuLabel) {
        this.kpMenuLabel = kpMenuLabel;
    }

    //Always Return
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getKeypadName() {
        return keypadName;
    }

    public void setKeypadName(String keypadName) {
        this.keypadName = keypadName;
    }

    @JsonIgnore
    public Integer getKpButtonIconPosition() {
        return kpButtonIconPosition;
    }

    public void setKpButtonIconPosition(Integer kpButtonIconPosition) {
        this.kpButtonIconPosition = kpButtonIconPosition;
    }

    @JsonIgnore
    public String getKpButtonFontName() {
        return kpButtonFontName;
    }

    public void setKpButtonFontName(String kpButtonFontName) {
        this.kpButtonFontName = kpButtonFontName;
    }

    @JsonIgnore
    public Integer getKpButtonFontSize() {
        return kpButtonFontSize;
    }

    public void setKpButtonFontSize(Integer kpButtonFontSize) {
        this.kpButtonFontSize = kpButtonFontSize;
    }

    @JsonIgnore
    public Boolean getKpButtonFontBold() {
        return kpButtonFontBold;
    }

    public void setKpButtonFontBold(Boolean kpButtonFontBold) {
        this.kpButtonFontBold = kpButtonFontBold;
    }

    @JsonIgnore
    public String getKpButtonFontColor() {
        return kpButtonFontColor;
    }

    public void setKpButtonFontColor(String kpButtonFontColor) {
        this.kpButtonFontColor = kpButtonFontColor;
    }

    @JsonIgnore
    public String getKpButtonBackgroundColor() {
        return kpButtonBackgroundColor;
    }

    public void setKpButtonBackgroundColor(String kpButtonBackgroundColor) {
        this.kpButtonBackgroundColor = kpButtonBackgroundColor;
    }

    //Always Return
    public Integer getNumColumns() {
        return numColumns;
    }

    public void setNumColumns(Integer numColumns) {
        this.numColumns = numColumns;
    }

    //Always Return
    public Integer getNumRows() {
        return numRows;
    }

    public void setNumRows(Integer numRows) {
        this.numRows = numRows;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getKpIsModifier() {
        return kpIsModifier;
    }

    public void setKpIsModifier(Boolean kpIsModifier) {
        this.kpIsModifier = kpIsModifier;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getKpIsUpsell() {
        return kpIsUpsell;
    }

    public void setKpIsUpsell(Boolean kpIsUpsell) {
        this.kpIsUpsell = kpIsUpsell;
    }

    public void setKpIsPayment(Boolean kpIsPayment) {
        this.kpIsPayment = kpIsPayment;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getKpIsPayment() {
        return kpIsPayment;
    }

    @JsonGetter("kpDisplayDescOnPOS")
    public Boolean getKpDisplayDescOnPOS() {
        return kpDisplayDescOnPOS;
    }

    public void setKpDisplayDescOnPOS(Boolean kpDisplayDescOnPOS) {
        this.kpDisplayDescOnPOS = kpDisplayDescOnPOS;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    @JsonGetter("kpDescription")
    public String getKpDescription() {
        return kpDescription;
    }

    public void setKpDescription(String kpDescription) {
        this.kpDescription = kpDescription;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getRotatingKP() {
        return isRotatingKP;
    }

    public void setRotatingKP(Boolean rotatingKP) {
        isRotatingKP = rotatingKP;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getButtonSortOrder() {
        return buttonSortOrder;
    }

    public void setButtonSortOrder(Integer buttonSortOrder) {
        this.buttonSortOrder = buttonSortOrder;
    }

    //Always return
    public Integer getButtonTypeId() {
        return buttonTypeId;
    }

    public void setButtonTypeId(Integer buttonTypeId) {
        this.buttonTypeId = buttonTypeId;
    }

    //Always return
    public Integer getButtonItemId() {
        return buttonItemId;
    }

    public void setButtonItemId(Integer buttonItemId) {
        this.buttonItemId = buttonItemId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    @JsonIgnore
    public String getKpButtonIconFileName() {
        return kpButtonIconFileName;
    }

    public void setKpButtonIconFileName(String kpButtonIconFileName) {
        this.kpButtonIconFileName = kpButtonIconFileName;
    }

    @JsonIgnore
    public Integer getKpDetailButtonIconPosition() {
        return kpDetailButtonIconPosition;
    }

    public void setKpDetailButtonIconPosition(Integer kpDetailButtonIconPosition) {
        this.kpDetailButtonIconPosition = kpDetailButtonIconPosition;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getKpDetailUseImageMargin() {
        return kpDetailUseImageMargin;
    }

    public void setKpDetailUseImageMargin(Boolean kpDetailUseImageMargin) {
        this.kpDetailUseImageMargin = kpDetailUseImageMargin;
    }

    @JsonIgnore
    public Boolean getKpDetailShrinkImageToFit() {
        return kpDetailShrinkImageToFit;
    }

    public void setKpDetailShrinkImageToFit(Boolean kpDetailShrinkImageToFit) {
        this.kpDetailShrinkImageToFit = kpDetailShrinkImageToFit;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getShowNameOverlay() {
        return showNameOverlay;
    }

    public void setShowNameOverlay(Boolean showNameOverlay) {
        this.showNameOverlay = showNameOverlay;
    }

    @JsonIgnore
    public String getKpDetailTokenizedIconFileName() {
        return kpDetailTokenizedIconFileName;
    }

    public void setKpDetailTokenizedIconFileName(String kpDetailTokenizedIconFileName) {
        this.kpDetailTokenizedIconFileName = kpDetailTokenizedIconFileName;
    }

    @JsonIgnore
    public String getKpDetailButtonFontName() {
        return kpDetailButtonFontName;
    }

    public void setKpDetailButtonFontName(String kpDetailButtonFontName) {
        this.kpDetailButtonFontName = kpDetailButtonFontName;
    }

    @JsonIgnore
    public Integer getKpDetailButtonFontSize() {
        return kpDetailButtonFontSize;
    }

    public void setKpDetailButtonFontSize(Integer kpDetailButtonFontSize) {
        this.kpDetailButtonFontSize = kpDetailButtonFontSize;
    }

    @JsonIgnore
    public String getKpDetailButtonFontColor() {
        return kpDetailButtonFontColor;
    }

    public void setKpDetailButtonFontColor(String kpDetailButtonFontColor) {
        this.kpDetailButtonFontColor = kpDetailButtonFontColor;
    }

    @JsonIgnore
    public String getKpDetailButtonBackgroundColor() {
        return kpDetailButtonBackgroundColor;
    }

    public void setKpDetailButtonBackgroundColor(String kpDetailButtonBackgroundColor) {
        this.kpDetailButtonBackgroundColor = kpDetailButtonBackgroundColor;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getSubMenuName() {
        return subMenuName;
    }

    public void setSubMenuName(String subMenuName) {
        this.subMenuName = subMenuName;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getKpDetailAutoShowPrepOptionSet() {
        return kpDetailAutoShowPrepOptionSet;
    }

    public void setKpDetailAutoShowPrepOptionSet(Boolean kpDetailAutoShowPrepOptionSet) {
        this.kpDetailAutoShowPrepOptionSet = kpDetailAutoShowPrepOptionSet;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getKpDetailPrepOptionSetId() {
        return kpDetailPrepOptionSetId;
    }

    public void setKpDetailPrepOptionSetId(Integer kpDetailPrepOptionSetId) {
        this.kpDetailPrepOptionSetId = kpDetailPrepOptionSetId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPaPluId() {
        return paPluId;
    }

    public void setPaPluId(Integer paPluId) {
        this.paPluId = paPluId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPluDescription() {
        return pluDescription;
    }

    public void setPluDescription(String pluDescription) {
        this.pluDescription = pluDescription;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getPluActive() {
        return pluActive;
    }

    public void setPluActive(Boolean pluActive) {
        this.pluActive = pluActive;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPluCode() {
        return pluCode;
    }

    public void setPluCode(String pluCode) {
        this.pluCode = pluCode;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getPriceLvl1() {
        return priceLvl1;
    }

    public void setPriceLvl1(BigDecimal priceLvl1) {
        this.priceLvl1 = priceLvl1;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getPriceLvl2() {
        return priceLvl2;
    }

    public void setPriceLvl2(BigDecimal priceLvl2) {
        this.priceLvl2 = priceLvl2;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getPriceLvl3() {
        return priceLvl3;
    }

    public void setPriceLvl3(BigDecimal priceLvl3) {
        this.priceLvl3 = priceLvl3;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getPriceLvl4() {
        return priceLvl4;
    }

    public void setPriceLvl4(BigDecimal priceLvl4) {
        this.priceLvl4 = priceLvl4;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getPriceLvl5() {
        return priceLvl5;
    }

    public void setPriceLvl5(BigDecimal priceLvl5) {
        this.priceLvl5 = priceLvl5;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getPriceLvl6() {
        return priceLvl6;
    }

    public void setPriceLvl6(BigDecimal priceLvl6) {
        this.priceLvl6 = priceLvl6;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getPriceLvl7() {
        return priceLvl7;
    }

    public void setPriceLvl7(BigDecimal priceLvl7) {
        this.priceLvl7 = priceLvl7;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getPriceLvl8() {
        return priceLvl8;
    }

    public void setPriceLvl8(BigDecimal priceLvl8) {
        this.priceLvl8 = priceLvl8;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getPriceLvl9() {
        return priceLvl9;
    }

    public void setPriceLvl9(BigDecimal priceLvl9) {
        this.priceLvl9 = priceLvl9;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(BigDecimal minQuantity) {
        this.minQuantity = minQuantity;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(BigDecimal maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getModifierSetId() {
        return modifierSetId;
    }

    public void setModifierSetId(Integer modifierSetId) {
        this.modifierSetId = modifierSetId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPluLinkedItemTypeId() {
        return pluLinkedItemTypeId;
    }

    public void setPluLinkedItemTypeId(Integer pluLinkedItemTypeId) {
        this.pluLinkedItemTypeId = pluLinkedItemTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPluLinkedItemId() {
        return pluLinkedItemId;
    }

    public void setPluLinkedItemId(Integer pluLinkedItemId) {
        this.pluLinkedItemId = pluLinkedItemId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getPluLinkedMinQuantity() {
        return pluLinkedMinQuantity;
    }

    public void setPluLinkedMinQuantity(BigDecimal pluLinkedMinQuantity) {
        this.pluLinkedMinQuantity = pluLinkedMinQuantity;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getPluLinkedMaxQuantity() {
        return pluLinkedMaxQuantity;
    }

    public void setPluLinkedMaxQuantity(BigDecimal pluLinkedMaxQuantity) {
        this.pluLinkedMaxQuantity = pluLinkedMaxQuantity;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTaxIds() {
        return taxIds;
    }

    public void setTaxIds(String taxIds) {
        this.taxIds = taxIds;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getDiscountIds() {
        return discountIds;
    }

    public void setDiscountIds(String discountIds) {
        this.discountIds = discountIds;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPluButtonText() {
        return pluButtonText;
    }

    public void setPluButtonText(String pluButtonText) {
        this.pluButtonText = pluButtonText;
    }

    @JsonIgnore
    public String getPluButtonIconFileName() {
        return pluButtonIconFileName;
    }

    public void setPluButtonIconFileName(String pluButtonIconFileName) {
        this.pluButtonIconFileName = pluButtonIconFileName;
    }

    @JsonIgnore
    public String getPluTokenizedIconFileName() {
        return pluTokenizedIconFileName;
    }

    public void setPluTokenizedIconFileName(String pluTokenizedIconFileName) {
        this.pluTokenizedIconFileName = pluTokenizedIconFileName;
    }

    @JsonIgnore
    public Boolean getPluShrinkImageToFit() {
        return pluShrinkImageToFit;
    }

    public void setPluShrinkImageToFit(Boolean pluShrinkImageToFit) {
        this.pluShrinkImageToFit = pluShrinkImageToFit;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getPluUseImageMargin() {
        return pluUseImageMargin;
    }

    public void setPluUseImageMargin(Boolean pluUseImageMargin) {
        this.pluUseImageMargin = pluUseImageMargin;
    }

    @JsonIgnore
    public String getPluButtonFontName() {
        return pluButtonFontName;
    }

    public void setPluButtonFontName(String pluButtonFontName) {
        this.pluButtonFontName = pluButtonFontName;
    }

    @JsonIgnore
    public Integer getPluButtonFontSize() {
        return pluButtonFontSize;
    }

    public void setPluButtonFontSize(Integer pluButtonFontSize) {
        this.pluButtonFontSize = pluButtonFontSize;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getPluButtonFontBold() {
        return pluButtonFontBold;
    }

    public void setPluButtonFontBold(Boolean pluButtonFontBold) {
        this.pluButtonFontBold = pluButtonFontBold;
    }

    @JsonIgnore
    public String getPluButtonFontColor() {
        return pluButtonFontColor;
    }

    public void setPluButtonFontColor(String pluButtonFontColor) {
        this.pluButtonFontColor = pluButtonFontColor;
    }

    @JsonIgnore
    public String getPluButtonBackgroundColor() {
        return pluButtonBackgroundColor;
    }

    public void setPluButtonBackgroundColor(String pluButtonBackgroundColor) {
        this.pluButtonBackgroundColor = pluButtonBackgroundColor;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getIsModifier() {
        return isModifier;
    }

    public void setIsModifier(Boolean isModifier) {
        this.isModifier = isModifier;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getCommentRequired() {
        return commentRequired;
    }

    public void setCommentRequired(Boolean commentRequired) {
        this.commentRequired = commentRequired;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getItemDisabled() {
        return itemDisabled;
    }

    public void setItemDisabled(Boolean itemDisabled) {
        this.itemDisabled = itemDisabled;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getAllowDisabled() {
        return allowDisabled;
    }

    public void setAllowDisabled(Boolean allowDisabled) {
        this.allowDisabled = allowDisabled;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getScaleUsed() {
        return scaleUsed;
    }

    public void setScaleUsed(Boolean scaleUsed) {
        this.scaleUsed = scaleUsed;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPaTareId() {
        return paTareId;
    }

    public void setPaTareId(Integer paTareId) {
        this.paTareId = paTareId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getPriceOpen() {
        return priceOpen;
    }

    public void setPriceOpen(Boolean priceOpen) {
        this.priceOpen = priceOpen;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getPricePreset() {
        return pricePreset;
    }

    public void setPricePreset(Boolean pricePreset) {
        this.pricePreset = pricePreset;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getTareOpen() {
        return tareOpen;
    }

    public void setTareOpen(Boolean tareOpen) {
        this.tareOpen = tareOpen;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getInventoryItem() {
        return inventoryItem;
    }

    public void setInventoryItem(Boolean inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getMaxOpenPrice() {
        return maxOpenPrice;
    }

    public void setMaxOpenPrice(BigDecimal maxOpenPrice) {
        this.maxOpenPrice = maxOpenPrice;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getMinOpenPrice() {
        return minOpenPrice;
    }

    public void setMinOpenPrice(BigDecimal minOpenPrice) {
        this.minOpenPrice = minOpenPrice;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getOwnershipGroupId() {
        return ownershipGroupId;
    }

    public void setOwnershipGroupId(Integer ownershipGroupId) {
        this.ownershipGroupId = ownershipGroupId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPaSubDeptId() {
        return paSubDeptId;
    }

    public void setPaSubDeptId(Integer paSubDeptId) {
        this.paSubDeptId = paSubDeptId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPaDepartmentId() {
        return paDepartmentId;
    }

    public void setPaDepartmentId(Integer paDepartmentId) {
        this.paDepartmentId = paDepartmentId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getNutritionInfo1() {
        return nutritionInfo1;
    }

    public void setNutritionInfo1(BigDecimal nutritionInfo1) {
        this.nutritionInfo1 = nutritionInfo1;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getNutritionInfo2() {
        return nutritionInfo2;
    }

    public void setNutritionInfo2(BigDecimal nutritionInfo2) {
        this.nutritionInfo2 = nutritionInfo2;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getNutritionInfo3() {
        return nutritionInfo3;
    }

    public void setNutritionInfo3(BigDecimal nutritionInfo3) {
        this.nutritionInfo3 = nutritionInfo3;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getNutritionInfo4() {
        return nutritionInfo4;
    }

    public void setNutritionInfo4(BigDecimal nutritionInfo4) {
        this.nutritionInfo4 = nutritionInfo4;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getNutritionInfo5() {
        return nutritionInfo5;
    }

    public void setNutritionInfo5(BigDecimal nutritionInfo5) {
        this.nutritionInfo5 = nutritionInfo5;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getWellness() {
        return wellness;
    }

    public void setWellness(Boolean wellness) {
        this.wellness = wellness;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getAutoShowModSet() {
        return autoShowModSet;
    }

    public void setAutoShowModSet(Boolean autoShowModSet) {
        this.autoShowModSet = autoShowModSet;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }
    public void setFixedNetWeight(BigDecimal fixedNetWeight) {
        this.fixedNetWeight = fixedNetWeight;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getFixedNetWeight() {
        return fixedNetWeight;
    }
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPrinterIds() {
        return printerIds;
    }

    public void setPrinterIds(String printerIds) {
        this.printerIds = printerIds;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getRewardIds() {
        return rewardIds;
    }

    public void setRewardIds(String rewardIds) {
        this.rewardIds = rewardIds;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getPluAutoShowPrepOptionSet() {
        return pluAutoShowPrepOptionSet;
    }

    public void setPluAutoShowPrepOptionSet(Boolean pluAutoShowPrepOptionSet) {
        this.pluAutoShowPrepOptionSet = pluAutoShowPrepOptionSet;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPluPrepOptionSetId() {
        return pluPrepOptionSetId;
    }

    public void setPluPrepOptionSetId(Integer pluPrepOptionSetId) {
        this.pluPrepOptionSetId = pluPrepOptionSetId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonGetter("pluUpsellComplementaryKeypadID")
    public Integer getUpsellComplementaryKeypadID() {
        return pluUpsellComplementaryKeypadID;
    }

    public void setUpsellComplementaryKeypadID(Integer pluUpsellComplementaryKeypadID) {
        this.pluUpsellComplementaryKeypadID = pluUpsellComplementaryKeypadID;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTaxDeleteIds() {
        return taxDeleteIds;
    }

    public void setTaxDeleteIds(String taxDeleteIds) {
        this.taxDeleteIds = taxDeleteIds;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPaDiscountId() {
        return paDiscountId;
    }

    public void setPaDiscountId(Integer paDiscountId) {
        this.paDiscountId = paDiscountId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getDscName() {
        return dscName;
    }

    public void setDscName(String dscName) {
        this.dscName = dscName;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getDscButtonText() {
        return dscButtonText;
    }

    public void setDscButtonText(String dscButtonText) {
        this.dscButtonText = dscButtonText;
    }

    @JsonIgnore
    public Integer getPluButtonIconPosition() {
        return pluButtonIconPosition;
    }

    public void setPluButtonIconPosition(Integer pluButtonIconPosition) {
        this.pluButtonIconPosition = pluButtonIconPosition;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getIconFileName() {
        if (this.getKpButtonIconFileName() != null && !this.getKpButtonIconFileName().isEmpty()) {
            return this.getKpButtonIconFileName();
        } else if (this.getPluButtonIconFileName() != null && !this.getPluButtonIconFileName().isEmpty()) {
            return this.getPluButtonIconFileName();
        } else {
            return iconFileName;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getUseImageMargin()
    {
        if (this.getKpDetailUseImageMargin() != null)
        {
            return this.getKpDetailUseImageMargin();
        }
        else if (this.getPluUseImageMargin() != null)
        {
            return this.getPluUseImageMargin();
        }
        else
        {
            return useImageMargin;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getShrinkImageToFit()
    {
        if (this.getKpDetailShrinkImageToFit() != null)
        {
            return this.getKpDetailShrinkImageToFit();
        }
        else if (this.getPluShrinkImageToFit() != null)
        {
            return this.getPluShrinkImageToFit();
        }
        else
        {
            return shrinkImageToFit;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getIconPosition()
    {
        if (this.getKpDetailButtonIconPosition() != null && !this.getKpDetailButtonIconPosition().toString().isEmpty())
        {
            return this.getKpDetailButtonIconPosition();
        }
        else if (this.getPluButtonIconPosition() != null && !this.getPluButtonIconPosition().toString().isEmpty())
        {
            return this.getPluButtonIconPosition();
        }
        else if (this.getKpButtonIconPosition() != null && !this.getKpButtonIconPosition().toString().isEmpty())
        {
            return this.getKpButtonIconPosition();
        }
        else
        {
            return iconPosition;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTokenizedIconFileName() {
        if (this.getKpDetailTokenizedIconFileName() != null && !this.getKpDetailTokenizedIconFileName().isEmpty()) {
            return this.getKpDetailTokenizedIconFileName();
        } else if (this.getPluTokenizedIconFileName() != null && !this.getPluTokenizedIconFileName().isEmpty()) {
            return this.getPluTokenizedIconFileName();
        } else {
            return tokenizedIconFileName;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getFontColor() {
        if (this.getKpDetailButtonFontColor() != null && !this.getKpDetailButtonFontColor().isEmpty()) {
            return this.getKpDetailButtonFontColor();
        } else if (this.getPluButtonFontColor() != null && !this.getPluButtonFontColor().isEmpty()) {
            return this.getPluButtonFontColor();
        } else if (this.getKpButtonFontColor() != null && !this.getKpButtonFontColor().isEmpty()) {
            return this.getKpButtonFontColor();
        } else {
            return fontColor;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getFontName() {
        if (this.getKpDetailButtonFontName() != null && !this.getKpDetailButtonFontName().isEmpty()) {
            return this.getKpDetailButtonFontName();
        } else if (this.getPluButtonFontName() != null && !this.getPluButtonFontName().isEmpty()) {
            return this.getPluButtonFontName();
        } else if (this.getKpButtonFontName() != null && !this.getKpButtonFontName().isEmpty()) {
            return this.getKpButtonFontName();
        } else {
            return fontName;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getFontSize() {
        if (this.getKpDetailButtonFontSize() != null && !this.getKpDetailButtonFontSize().toString().isEmpty()) {
            return this.getKpDetailButtonFontSize();
        } else if (this.getPluButtonFontSize() != null && !this.getPluButtonFontSize().toString().isEmpty()) {
            return this.getPluButtonFontSize();
        } else if (this.getKpButtonFontSize() != null && !this.getKpButtonFontSize().toString().isEmpty()) {
            return this.getKpButtonFontSize();
        } else {
            return fontSize;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getFontBold() {

        if (this.getPluButtonFontBold() != null && this.getPluButtonFontBold()) {
            return this.getPluButtonFontBold();
        } else if (this.getKpButtonFontBold() != null && this.getKpButtonFontBold()) {
            return this.getKpButtonFontBold();
        } else {
            return fontBold;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getBackgroundColor() {
        if (this.getKpDetailButtonBackgroundColor() != null && !this.getKpDetailButtonBackgroundColor().isEmpty()) {
            return this.getKpDetailButtonBackgroundColor();
        } else if (this.getPluButtonBackgroundColor() != null && !this.getPluButtonBackgroundColor().isEmpty()) {
            return this.getPluButtonBackgroundColor();
        } else if (this.getKpButtonBackgroundColor() != null && !this.getKpButtonBackgroundColor().isEmpty()) {
            return this.getKpButtonBackgroundColor();
        } else {
            return backgroundColor;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Boolean getPluStarbucksCardEligible() {
        return pluStarbucksCardEligible;
    }

    public void setPluStarbucksCardEligible(Boolean pluStarbucksCardEligible) {
        this.pluStarbucksCardEligible = pluStarbucksCardEligible;
    }

    //endregion

}
