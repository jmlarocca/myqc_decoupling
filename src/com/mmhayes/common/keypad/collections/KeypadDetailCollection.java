package com.mmhayes.common.keypad.collections;

//MMHayes Dependencies

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.keypad.models.KeypadDetailModel;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.terminal.models.TerminalModel;

//API Dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: rawithers $: Author of last commit
 $Date: 2021-09-20 19:15:04 -0400 (Mon, 20 Sep 2021) $: Date of last commit
 $Rev: 15413 $: Revision of last commit
*/
public class KeypadDetailCollection {
    List<KeypadDetailModel> collection = new ArrayList<>();
    DataManager dm = new DataManager();
    private final static String ITEM_TYPE_KEYPAD = "1";
    public final static String ITEM_TYPE_PLU = "2";
    public final static String ITEM_TYPE_COMBO = "36";

    public KeypadDetailCollection() {

    }

    public List<KeypadDetailModel> getAllKeypadDetailsForTerminal(TerminalModel terminalModel) throws Exception {
        KeypadDetailCollection keypadDetailCollection = new KeypadDetailCollection();
        ArrayList<HashMap> keypadList = keypadDetailCollection.getKeypadGroupDetails(null, terminalModel);

        for (HashMap keypadHM : keypadList) {
            CommonAPI.checkIsNullOrEmptyObject(keypadHM.get("KPID"), "Keypad Id could not be found", terminalModel.getId());
            //create a new model and add to the collection
            keypadDetailCollection.getCollection().add(new KeypadDetailModel(keypadHM));
        }

        return keypadDetailCollection.getCollection();
    }

    public ArrayList getKeypadGroupDetails(String sessionID, TerminalModel terminalModel) {
        ArrayList keypadsResult = new ArrayList();
        HashMap<Integer, ArrayList> dbKeypadsHM = new HashMap<Integer, ArrayList>();
        HashMap<Integer, ArrayList> neededKeypads = new HashMap<Integer, ArrayList>();
        HashMap<Integer, ArrayList> modifierSets = new HashMap<Integer, ArrayList>();

        int terminalId = terminalModel.getId();
        int homeKeypadID = 0;
        int navKeypadID = 0;
        int quickKeypadID = 0;
        int selfSvcKeypadID = 0;
        int revenueCenterID = 0;
        int diningOptionsPaKeypadId = 0;
        List<Integer> upsellProfileKeypadIds = new ArrayList<>();
        List<Integer> upsellComplementaryKeypadIds = new ArrayList<>();
        List<Integer> productUpsellComplementaryKeypadIds = new ArrayList<>();
        List<Integer> comboKeypadDetails = new ArrayList<>();

        ArrayList dbKeypads = dm.serializeSqlWithColNames("data.posapi30.GetKeypadGroupDetails",
                new Object[]{terminalModel.getRevenueCenterId()}, null, PosAPIHelper.getLogFileName(terminalModel.getId()));

        // Get home, nav, quick, and self svc keypad ids for this terminal
        ArrayList terminalKeypadInfoAL = dm.serializeSqlWithColNames("data.posapi30.GetTerminalMainKeypadIDS", new Object[]{new Integer(terminalId)}, null, PosAPIHelper.getLogFileName(terminalModel.getId()));

        if (terminalKeypadInfoAL.size() > 0) {
            HashMap terminalKeypadInfo = (HashMap) terminalKeypadInfoAL.get(0);
            homeKeypadID = HashMapDataFns.getIntVal(terminalKeypadInfo, "PAHOMEKEYPADIDDEFAULT");
            navKeypadID = HashMapDataFns.getIntVal(terminalKeypadInfo, "PANAVKEYPADIDDEFAULT");
            quickKeypadID = HashMapDataFns.getIntVal(terminalKeypadInfo, "PAQUICKKEYPADIDDEFAULT");
            selfSvcKeypadID = HashMapDataFns.getIntVal(terminalKeypadInfo, "PASELFSVCKEYPADDEFAULTID");
            revenueCenterID = HashMapDataFns.getIntVal(terminalKeypadInfo, "REVENUECENTERID");
            diningOptionsPaKeypadId = HashMapDataFns.getIntVal(terminalKeypadInfo, "DININGOPTIONSPAKEYPADID");
        }

        ArrayList upsellCompKeypadIDs = dm.serializeSqlWithColNames("data.posapi30.GetTerminalUpsellComplementaryKeypadIDS", new Object[]{new Integer(terminalModel.getRevenueCenterId())}, null, PosAPIHelper.getLogFileName(terminalModel.getId()));
        //get complementary keypad ids from sub departments in the revenue center of the terminal
        for(Object upsellObj : upsellCompKeypadIDs) {
            HashMap upsellHM = (HashMap) upsellObj;
            int complementaryKeypadId = HashMapDataFns.getIntVal(upsellHM, "COMPLEMENTARYKEYPADID");
            if(complementaryKeypadId != -1) {
                upsellComplementaryKeypadIds.add(complementaryKeypadId);
            }
        }

        ArrayList upsellProfKeypadIDs = dm.serializeSqlWithColNames("data.posapi30.GetTerminalUpsellProfileKeypadIDS", new Object[]{new Integer(terminalModel.getRevenueCenterId()), new Integer(terminalId)}, null, PosAPIHelper.getLogFileName(terminalModel.getId()));
        //get complementary keypad ids from sub departments in the revenue center of the terminal
        for(Object upsellProfObj : upsellProfKeypadIDs) {
            HashMap upsellProfHM = (HashMap) upsellProfObj;
            int upsellProfileKeypadId = HashMapDataFns.getIntVal(upsellProfHM, "PAKEYPADID");
            if(upsellProfileKeypadId != -1) {
                upsellProfileKeypadIds.add(upsellProfileKeypadId);
            }
        }

        // Get Linked Combo Keypads for this revenue center
        ArrayList dbLinkedComboKeypads = dm.serializeSqlWithColNames("data.posanywhere.GetLinkedComboKeypads", new Object[]{new Integer(revenueCenterID)}, null, PosAPIHelper.getLogFileName(terminalModel.getId()));

        // Get modifier set details for this revenue center
        ArrayList dbModifierSetDetails = dm.serializeSqlWithColNames("data.posanywhere.GetModifierSets", new Object[]{new Integer(revenueCenterID)}, null, PosAPIHelper.getLogFileName(terminalModel.getId()));

        // Group modifier set details by PAModifierSetID
        for (int x = 0; x < dbModifierSetDetails.size(); x++) {
            HashMap modSetDetail = (HashMap) dbModifierSetDetails.get(x);
            Integer modifierSetID = HashMapDataFns.getIntVal(modSetDetail, "PAMODIFIERSETID");
            ArrayList modifierSetDetails;

            if (modifierSets.containsKey(modifierSetID) == false) {
                modifierSetDetails = new ArrayList();
                modifierSetDetails.add(modSetDetail);

                modifierSets.put(modifierSetID, modifierSetDetails);
            } else {
                modifierSetDetails = modifierSets.get(modifierSetID);
                modifierSetDetails.add(modSetDetail);
            }
        }

        // Group keypad details by keypad ID in hashmap
        for (int x = 0; x < dbKeypads.size(); x++) {
            HashMap keypadDetail = (HashMap) dbKeypads.get(x);
            Integer keypadID = HashMapDataFns.getIntVal(keypadDetail, "KPID");
            Integer complementaryKeypadID = HashMapDataFns.getIntVal(keypadDetail, "PLUUPSELLCOMPLEMENTARYKEYPADID");
            ArrayList keypadDetails;

            if (dbKeypadsHM.containsKey(keypadID) == false) {
                keypadDetails = new ArrayList();
                keypadDetails.add(keypadDetail);

                dbKeypadsHM.put(keypadID, keypadDetails);
            } else {
                keypadDetails = dbKeypadsHM.get(keypadID);
                keypadDetails.add(keypadDetail);
            }

            if( complementaryKeypadID != -1 && !productUpsellComplementaryKeypadIds.contains(complementaryKeypadID) ) {
                productUpsellComplementaryKeypadIds.add(complementaryKeypadID);
            }
        }

        // Build self svc keypad details
        if (selfSvcKeypadID != -1) {
            getKeypadDetailsForKeypadID(selfSvcKeypadID, neededKeypads, dbKeypadsHM, modifierSets, terminalId);
        }

        // Build dining options keypad details
        if (diningOptionsPaKeypadId != -1){
            getKeypadDetailsForKeypadID(diningOptionsPaKeypadId, neededKeypads, dbKeypadsHM, modifierSets, terminalId);
        }

        // Build upsell profile keypad details
        if (upsellProfileKeypadIds.size() > 0){
            for(Integer upsellProfileKeypadId : upsellProfileKeypadIds) {
                getKeypadDetailsForKeypadID(upsellProfileKeypadId, neededKeypads, dbKeypadsHM, modifierSets, terminalId);
            }
        }

        // Build upsell complementary keypad details for sub department
        if (upsellComplementaryKeypadIds.size() > 0){
            for(Integer upsellComplementaryKeypadId : upsellComplementaryKeypadIds) {
                getKeypadDetailsForKeypadID(upsellComplementaryKeypadId, neededKeypads, dbKeypadsHM, modifierSets, terminalId);
            }
        }

        // Build upsell complementary keypad details for product
        if (productUpsellComplementaryKeypadIds.size() > 0){
            for(Integer prodUpsellComplementaryKeypadId : productUpsellComplementaryKeypadIds) {
                getKeypadDetailsForKeypadID(prodUpsellComplementaryKeypadId, neededKeypads, dbKeypadsHM, modifierSets, terminalId);
            }
        }

        // Build quick keys details
        getKeypadDetailsForKeypadID(quickKeypadID, neededKeypads, dbKeypadsHM, modifierSets, terminalId);

        // Build nav keypad details
        getKeypadDetailsForKeypadID(navKeypadID, neededKeypads, dbKeypadsHM, modifierSets, terminalId);

        // Build home keypad details
        getKeypadDetailsForKeypadID(homeKeypadID, neededKeypads, dbKeypadsHM, modifierSets, terminalId);

        // Build Combo keypad details
        for (int x = 0; x < dbLinkedComboKeypads.size(); x++) {
            HashMap comboKPDetail = (HashMap) dbLinkedComboKeypads.get(x);
            Integer comboKPID = HashMapDataFns.getIntVal(comboKPDetail, "PAKEYPADID");
            getKeypadDetailsForKeypadID(comboKPID, neededKeypads, dbKeypadsHM, modifierSets, terminalId);
        }


        // Put all keypad details into arraylist to return
        for (ArrayList keypadDetails : neededKeypads.values()) {
            for (int x = 0; x < keypadDetails.size(); x++) {
                keypadsResult.add(keypadDetails.get(x));
            }
        }

        Logger.logMessage("GET KEYPAD GROUP DETAILS RESULT SET SIZE: " + keypadsResult.size(), PosAPIHelper.getLogFileName(terminalModel.getId()), Logger.LEVEL.TRACE);

        return keypadsResult;
    }

    private HashMap<Integer, ArrayList> getKeypadDetailsForKeypadID(Integer keypadID, HashMap<Integer, ArrayList> neededKeypads, HashMap<Integer, ArrayList> dbKeypadInfo, HashMap<Integer, ArrayList> modifierSets, int terminalID) {
        // Do we have info for this keypad ID? If so, return
        if (neededKeypads.containsKey(keypadID)) {
            return neededKeypads;
        }

        // If we don't have info for this keypad ID, add it
        if (dbKeypadInfo.containsKey(keypadID)) {
            ArrayList keypadDetails = dbKeypadInfo.get(keypadID);

            neededKeypads.put(keypadID, keypadDetails);

            // Also get details for each SUB keypad
            for (int x = 0; x < keypadDetails.size(); x++) {
                HashMap detail = (HashMap) keypadDetails.get(x);
                int buttonTypeID = HashMapDataFns.getIntVal(detail, "BUTTONTYPEID");
                int buttonItemID = HashMapDataFns.getIntVal(detail, "BUTTONITEMID");
                Integer modifierSetID = HashMapDataFns.getIntVal(detail, "MODIFIERSETID");

                if (buttonTypeID == new Integer(ITEM_TYPE_KEYPAD).intValue())
                {
                    getKeypadDetailsForKeypadID(buttonItemID, neededKeypads, dbKeypadInfo, modifierSets, terminalID);
                }
                else if (buttonTypeID == new Integer(ITEM_TYPE_PLU).intValue() && modifierSetID != -1 && modifierSets.containsKey(modifierSetID))
                {
                    Logger.logMessage("PAPLUID: " + buttonItemID, PosAPIHelper.getLogFileName(terminalID), Logger.LEVEL.DEBUG);
                    Logger.logMessage("GET KEYPADS FOR MODIFIER SET ID: " + modifierSetID, PosAPIHelper.getLogFileName(terminalID), Logger.LEVEL.DEBUG);

                    ArrayList modifierSetDetails = modifierSets.get(modifierSetID);

                    for (int y = 0; y < modifierSetDetails.size(); y++) {
                        HashMap modSetDetail = (HashMap) modifierSetDetails.get(y);
                        Integer paKeypadID = HashMapDataFns.getIntVal(modSetDetail, "PAKEYPADID");

                        Logger.logMessage("MODIFIER - GET KEYPAD: " + paKeypadID, PosAPIHelper.getLogFileName(terminalID), Logger.LEVEL.DEBUG);
                        getKeypadDetailsForKeypadID(paKeypadID, neededKeypads, dbKeypadInfo, modifierSets, terminalID);
                    }
                }
            }
        }

        return neededKeypads;
    }

    public List<KeypadDetailModel> getCollection() {
        return collection;
    }

    public void setCollection(List<KeypadDetailModel> collection) {
        this.collection = collection;
    }

}
