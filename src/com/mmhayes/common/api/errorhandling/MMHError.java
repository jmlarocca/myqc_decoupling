package com.mmhayes.common.api.errorhandling;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//javax.ws.rs dependencies

//other dependencies
import java.io.PrintWriter;
import java.io.StringWriter;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-05-30 15:44:46 -0400 (Tue, 30 May 2017) $: Date of last commit
 $Rev: 4055 $: Revision of last commit

 Notes: Standardized "error" object for clients that consume MMHayes REST APIs. Provides consistent JSON responses to all clients.
*/
public class MMHError {
    Integer httpStatus = APIException.HTTP_SERVER_CODES.GENERIC.getCode(); //default to 500 (internal server error)
    String errorCode = APIException.MMH_ERROR_CODES.GENERIC.getCode(); //used tp describe the type of exception - 0 is for unknown exceptions
    String details = "N/A"; //used to describe what was going on in the code
    private String description = "N/A"; //description of the exception itself
    private String stackTrace = "N/A"; //the stack trace for the exception
    private String logFileName = "";

    //constructor - DEFAULT
    public MMHError() {}

    //constructor
    public MMHError(APIException apiEx) {
        try {

            //set MMHError properties from the passed in APIException
            setHttpStatus(apiEx.getHttpStatus());
            setErrorCode(apiEx.getErrorCode());
            setDetails(apiEx.getDetails());
            setDescription(apiEx.getDescription());

            //extract and the stack trace
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            apiEx.printStackTrace(pw);
            setStackTrace(sw.toString());

        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in MMHError!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor - Handles "generic" RunTime exceptions (or other uncaught exceptions)
    public MMHError(Throwable ex) {
        try {

            //set the error code to UNKNOWN - the mapper will set the HTTP Status appropriately
            setErrorCode(APIException.MMH_ERROR_CODES.UNKNOWN.getCode());
            setDetails("An error occurred. Please check logs and try again.");
            setDescription("An unknown exception occurred. Please see the stack trace.");

            //extract and the stack trace
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            setStackTrace(sw.toString());

        } catch (Exception innerEx) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in MMHError! Throwable constructor!", Logger.LEVEL.ERROR);
            Logger.logException(innerEx); //always log exceptions
        }
    }

    public MMHError(APIException apiEx, boolean setLogFileName) {
        try {

            //set MMHError properties from the passed in APIException
            setHttpStatus(apiEx.getHttpStatus());
            setErrorCode(apiEx.getErrorCode());
            setDetails(apiEx.getDetails());
            setDescription(apiEx.getDescription());
            setLogFileName(apiEx.getLogFileName());

            //extract and the stack trace
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            apiEx.printStackTrace(pw);
            setStackTrace(sw.toString());

        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in MMHError!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //logs information stored in the MMHError
    public Boolean logMMHError() {
        String customMessage = null;
        return logMMHError(customMessage);
    }

    //OVERLOAD: logs information stored in the MMHError
    public Boolean logMMHError(String customMessage) {
        Boolean successFullyLogged = false;
        try {

            //log a custom message if one was passed
            if (customMessage != null) {
                Logger.logMessage("ERROR: - CUSTOM MESSAGE: " + customMessage, Logger.LEVEL.ERROR);
            }

            //log the HTTP Status
            Logger.logMessage("ERROR: - HTTP STATUS: " + this.getHttpStatus().toString(), Logger.LEVEL.ERROR);

            //log the Error Code
            Logger.logMessage("ERROR: - ERROR CODE: " + this.getErrorCode().toString(), Logger.LEVEL.ERROR);

            //log the details of the error
            Logger.logMessage("ERROR: - DETAILS: " + this.getDetails(), Logger.LEVEL.ERROR);

            //log the description of the stack trace
            Logger.logMessage("ERROR: - DESCRIPTION: " + this.getDescription(), Logger.LEVEL.ERROR);

            //log a shortened version of the stack trace
            Logger.logMessage("ERROR: Caught exception: " + this.getShortStackTrace(), Logger.LEVEL.ERROR);

        } catch (Exception innerEx) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in MMHError.logMMHError()", Logger.LEVEL.ERROR);
            Logger.logException(innerEx); //always log exceptions
        }
        return successFullyLogged;
    }

    //logs information stored in the MMHError
    public Boolean logMMHError(boolean useLogFileName) {
        String customMessage = null;


        return logMMHError(customMessage, true);
    }

    //OVERLOAD: logs information stored in the MMHError
    public Boolean logMMHError(String customMessage, boolean useLogFileName) {
        Boolean successFullyLogged = false;
        String logFileName = this.getLogFileName();

        try {

            //log a custom message if one was passed
            if (customMessage != null) {
                Logger.logMessage("ERROR: - CUSTOM MESSAGE: " + customMessage, logFileName, Logger.LEVEL.ERROR);
            }

            //log the HTTP Status
            Logger.logMessage("ERROR: - HTTP STATUS: " + this.getHttpStatus().toString(), logFileName, Logger.LEVEL.ERROR);

            //log the Error Code
            Logger.logMessage("ERROR: - ERROR CODE: " + this.getErrorCode().toString(), logFileName, Logger.LEVEL.ERROR);

            //log the details of the error
            Logger.logMessage("ERROR: - DETAILS: " + this.getDetails(), logFileName, Logger.LEVEL.ERROR);

            //log the description of the stack trace
            Logger.logMessage("ERROR: - DESCRIPTION: " + this.getDescription(), logFileName, Logger.LEVEL.ERROR);

            //log a shortened version of the stack trace
            Logger.logMessage("ERROR: Caught exception: " + this.getShortStackTrace(), logFileName, Logger.LEVEL.ERROR);

        } catch (Exception innerEx) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in MMHError.logMMHError()", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(innerEx, logFileName); //always log exceptions
        }
        return successFullyLogged;
    }

    //getter
    public Integer getHttpStatus() {
        return this.httpStatus;
    }

    //setter
    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    //getter
    public String getErrorCode() {
        return this.errorCode;
    }

    //setter
    public void setErrorCode(String errorCode) {
        this.errorCode = commonMMHFunctions.sanitizeStringFromXSS(errorCode);
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = commonMMHFunctions.sanitizeStringFromXSS(this.getErrorCode() + " - " + details);
    }

    //getter
    private String getDescription() {
        return description;
    }

    //setter
    private void setDescription(String description) {
        this.description = description;
    }

    //getter
    private String getStackTrace() {
        return stackTrace;
    }

    //setter
    private void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    //getter
    //only log the first 5 lines of the Stack Trace
    //VO - B-03941 03-14-2017 egl
    private String getShortStackTrace() {
        StringBuilder sbStack = new StringBuilder();

        if (stackTrace != null && !stackTrace.isEmpty()) {
            String[] stackArray = stackTrace.split("\r\n\t");

            for (Integer iCount = 1; iCount < 10; iCount++) {
                if (stackArray.length >= iCount) {
                    sbStack.append(stackArray[iCount - 1]);
                    sbStack.append("\r\n\t");
                }
            }
        }
        return sbStack.toString();
    }

    @JsonIgnore
    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }
}
