package com.mmhayes.common.api.errorhandling.exceptions;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-05-30 16:03:08 -0400 (Tue, 30 May 2017) $: Date of last commit
 $Rev: 4056 $: Revision of last commit
*/
public class TransactionIndividualLimitException extends  APIException{
    String description = "This Exception means that the referenced Transaction exceeds Individual Transaction limit."; //description of the exception itself
    String details = "Transaction exceeds Daily Transaction limit.  Please Try Again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public TransactionIndividualLimitException() {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TRANSINDIVIDUALLIMIT.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TransactionIndividualLimitException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public TransactionIndividualLimitException(String details) {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TRANSINDIVIDUALLIMIT.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TransactionIndividualLimitException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    public TransactionIndividualLimitException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TRANSINDIVIDUALLIMIT.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TransactionIndividualLimitException!", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //constructor
    public TransactionIndividualLimitException(String details, Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TRANSINDIVIDUALLIMIT.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TransactionIndividualLimitException!", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }
}
