package com.mmhayes.common.api.errorhandling.exceptions;

import com.mmhayes.common.utils.*;

/**
 * Created by admin on 12/27/2016.
 */
public class SessionTimeoutException extends APIException {
    String description = "This exception means the client could not be authenticated due to an expired session."; //description of the exception itself
    String details = "Your session has timed out, please login again."; //used to tell the client what went wrong =

    //constructor - DEFAULT
    public SessionTimeoutException() {
        try {
            setHttpStatus(APIException.HTTP_CLIENT_CODES.FORBIDDEN.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.SESSIONEXPIRED.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in SessionTimeoutException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public SessionTimeoutException(String details) {
        try {
            setHttpStatus(APIException.HTTP_CLIENT_CODES.FORBIDDEN.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.SESSIONEXPIRED.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in SessionTimeoutException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }

}
