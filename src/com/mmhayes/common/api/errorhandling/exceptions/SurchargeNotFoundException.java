package com.mmhayes.common.api.errorhandling.exceptions;

//MMHayes Dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-03-25 08:10:10 -0400 (Mon, 25 Mar 2019) $: Date of last commit
 $Rev: 8585 $: Revision of last commit
*/
public class SurchargeNotFoundException extends APIException {
    String description = "This Exception means that the referenced Surcharge was not found."; //description of the exception itself
    String details = "Surcharge Not Found.  Please Try Again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public SurchargeNotFoundException() {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.SURCHARGENOTFOUND.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in SurchargeNotFoundException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public SurchargeNotFoundException(String details) {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_POS_API_ERROR_CODES.SURCHARGENOTFOUND.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in SurchargeNotFoundException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    public SurchargeNotFoundException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.SURCHARGENOTFOUND.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in SurchargeNotFoundException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //constructor
    public SurchargeNotFoundException(String details, Integer terminalId) {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_POS_API_ERROR_CODES.SURCHARGENOTFOUND.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in SurchargeNotFoundException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }
}
