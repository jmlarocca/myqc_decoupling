package com.mmhayes.common.api.errorhandling.exceptions;

//MMHayes Dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-02-27 09:23:59 -0500 (Thu, 27 Feb 2020) $: Date of last commit
 $Rev: 10914 $: Revision of last commit
*/
public class ComboNotFoundException extends APIException {
    String description = "This Exception means that the referenced Combo was not found."; //description of the exception itself
    String details = "Combo Not Found.  Please Try Again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public ComboNotFoundException() {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.COMBONOTFOUND.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in ComboNotFoundException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public ComboNotFoundException(String details) {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_POS_API_ERROR_CODES.COMBONOTFOUND.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in ComboNotFoundException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    public ComboNotFoundException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.COMBONOTFOUND.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in ComboNotFoundException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //constructor
    public ComboNotFoundException(String details, Integer terminalId) {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_POS_API_ERROR_CODES.COMBONOTFOUND.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in ComboNotFoundException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }
}
