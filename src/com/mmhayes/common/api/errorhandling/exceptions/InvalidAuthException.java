package com.mmhayes.common.api.errorhandling.exceptions;

//mmhayes dependencies
import com.mmhayes.common.utils.*;

//javax.ws.rs dependencies

//other dependencies


/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2016-09-02 10:53:21 -0400 (Fri, 02 Sep 2016) $: Date of last commit
 $Rev: 2841 $: Revision of last commit

 Notes: Custom (checked) exception for invalid auth (ex. DSKey invalid or missing)
*/
public class InvalidAuthException extends APIException {
    String description = "This exception means the client could not be authenticated. Likely due to a missing/invalid session."; //description of the exception itself
    String details = "Invalid Authorization. Please login again."; //used to tell the client what went wrong =

    //constructor - DEFAULT
    public InvalidAuthException() {
        try {
            setHttpStatus(APIException.HTTP_CLIENT_CODES.FORBIDDEN.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.INVALIDAUTH.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidAuthException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public InvalidAuthException(String details) {
        try {
            setHttpStatus(APIException.HTTP_CLIENT_CODES.FORBIDDEN.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.INVALIDAUTH.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidAuthException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }

}
