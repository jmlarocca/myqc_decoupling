package com.mmhayes.common.api.errorhandling.exceptions;

//MMHayes Dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2020-08-31 10:36:03 -0400 (Mon, 31 Aug 2020) $: Date of last commit
 $Rev: 12508 $: Revision of last commit
*/
public class DonationNotFoundException extends APIException {
    String description = "This Exception means that the referenced Donation was not found."; //description of the exception itself
    String details = "Donation Not Found.  Please Try Again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public DonationNotFoundException() {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.DONATIONNOTFOUND.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in DonationNotFoundException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public DonationNotFoundException(String details) {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.DONATIONNOTFOUND.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in DonationNotFoundException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    public DonationNotFoundException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.DONATIONNOTFOUND.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in DonationNotFoundException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //constructor
    public DonationNotFoundException(String details, Integer terminalId) {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.DONATIONNOTFOUND.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in DonationNotFoundException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }
}
