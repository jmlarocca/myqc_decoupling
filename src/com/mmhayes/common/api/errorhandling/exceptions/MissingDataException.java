package com.mmhayes.common.api.errorhandling.exceptions;

//mmhayes dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.*;

//javax.ws.rs dependencies

//other dependencies


/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-06-28 16:46:46 -0400 (Thu, 28 Jun 2018) $: Date of last commit
 $Rev: 7349 $: Revision of last commit

 Notes: Custom (checked) exception for missing data
*/
public class MissingDataException extends APIException {
    String description = "This exception means the application was expecting something and it was not there. Likely a problem with configuration or data."; //description of the exception itself
    String details = "Invalid Data. Please try again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public MissingDataException() {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.MISSINGDATA.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in MissingDataException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public MissingDataException(String details) {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.MISSINGDATA.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in MissingDataException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    public MissingDataException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.MISSINGDATA.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in MissingDataException!", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //constructor
    public MissingDataException(String details, Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.MISSINGDATA.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in MissingDataException!", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    public MissingDataException(String details, String logFileName) {
        try {
            setLogFileName(logFileName);
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.MISSINGDATA.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in MissingDataException!", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }

}
