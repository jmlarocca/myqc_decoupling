package com.mmhayes.common.api.errorhandling.exceptions;

//mmhayes dependencies
import com.mmhayes.common.utils.*;

//javax.ws.rs dependencies

//other dependencies


/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2016-09-02 10:53:21 -0400 (Fri, 02 Sep 2016) $: Date of last commit
 $Rev: 2841 $: Revision of last commit

 Notes: For back-end resources that could not be found - Overrides the standard "404" error - ex. typo in URL
*/
public class ResourceNotFoundException extends APIException {
    String description = "This exception means the server could not find the resource requested. This is likely due to a typo in the URL, web application down, or a server down."; //description of the exception itself
    String details = "Could not find requested resource. Please try again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public ResourceNotFoundException() {
        try {
            setHttpStatus(APIException.HTTP_CLIENT_CODES.NOTFOUND.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.NOTFOUND.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in ResourceNotFoundException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public ResourceNotFoundException(String details) {
        try {
            setHttpStatus(APIException.HTTP_CLIENT_CODES.NOTFOUND.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.NOTFOUND.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in ResourceNotFoundException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }

}
