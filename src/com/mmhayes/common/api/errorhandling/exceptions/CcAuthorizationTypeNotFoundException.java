package com.mmhayes.common.api.errorhandling.exceptions;

//MMHayes Dependencies
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-06-18 10:54:52 -0400 (Mon, 18 Jun 2018) $: Date of last commit
 $Rev: 7288 $: Revision of last commit
*/
public class CcAuthorizationTypeNotFoundException extends APIException {
    String description = "This Exception means that the referenced CC Authorization Type was not found or invalid."; //description of the exception itself
    String details = "Missing or Invalid CC Authorization Type.  Please Try Again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public CcAuthorizationTypeNotFoundException() {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.CCAUTHORIZATIONTYPENOTFOUND.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in CcAuthorizationTypeNotFoundException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public CcAuthorizationTypeNotFoundException(String details) {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_POS_API_ERROR_CODES.CCAUTHORIZATIONTYPENOTFOUND.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in CcAuthorizationTypeNotFoundException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    public CcAuthorizationTypeNotFoundException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.CCAUTHORIZATIONTYPENOTFOUND.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in CcAuthorizationTypeNotFoundException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }
}