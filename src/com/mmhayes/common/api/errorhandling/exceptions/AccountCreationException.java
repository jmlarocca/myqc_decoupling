package com.mmhayes.common.api.errorhandling.exceptions;

import com.mmhayes.common.utils.Logger;

public class AccountCreationException extends APIException{

    public AccountCreationException(Integer accountTypeID) {
        try {
            if(accountTypeID == null) {
                accountTypeID = 0;
            }

            String description = "This exception means there was an error creating the account through MyQC. There could be an issue with licenses or account group configuration.";

            // client provides bad account group, set 400 error
            this.setHttpStatus(HTTP_CLIENT_CODES.BADREQUEST.getCode());
            this.setDetails(setErrorDetails(accountTypeID));
            this.setErrorCode(determineErrorCode(accountTypeID));
            this.setDescription(description);


        } catch(Exception ex) {
            Logger.logMessage("Error in creating AccountCreationException", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    private String setErrorDetails(Integer accountTypeID) {
        switch(accountTypeID.intValue()){
            case 1:
                details = "Cannot create payroll deduction accounts at this time.";
                break;
            case 2:
                details = "Cannot create prepaid accounts at this time.";
                break;
            case 5:
                details = "Cannot create guest accounts at this time. ";
                break;
            default:
                details = "Cannot your account at this time.";
                break;
        }
        return details;
    }

    private String determineErrorCode(Integer accountTypeID) {
        // Guest
        if(accountTypeID.equals(5)) {
            return MMH_POS_API_ERROR_CODES.GUESTACCOUNTLICENSENOTAVAILABLE.getCode();
        // Payroll Deduct or Prepaid
        } else if(accountTypeID.equals(1) || accountTypeID.equals(2)) {
            return MMH_POS_API_ERROR_CODES.EMPLOYEEACCOUNTLICENSENOTAVAILABLE.getCode();
        // Other, unknown account type
        } else {
            return MMH_POS_API_ERROR_CODES.UNKNOWNACCOUNTLICENSETYPE.getCode();
        }
    }
}
