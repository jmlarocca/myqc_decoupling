package com.mmhayes.common.api.errorhandling.exceptions;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;


/*
 Last Updated (automatically updated by SVN)
 $Author: nyu $: Author of last commit
 $Date: 2021-08-16 18:52:44 -0400 (Mon, 16 Aug 2021) $: Date of last commit
 $Rev: 14893 $: Revision of last commit

 Notes: Custom (checked) exception for invalid auth (ex. DSKey invalid or missing)
*/
public class InvalidVoucherException extends APIException {
    String description = "This Exception means that the given Voucher Code was not valid."; //description of the exception itself
    String details = "Invalid Voucher Code.  Please Try Again."; //used to tell the client what went wrong

    //Default constructor
    public InvalidVoucherException() {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TENDERNOTFOUND.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidVoucherException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    public InvalidVoucherException(String details) {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_POS_API_ERROR_CODES.TENDERNOTFOUND.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidVoucherException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    public InvalidVoucherException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TENDERNOTFOUND.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidVoucherException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    public InvalidVoucherException(String details, Integer terminalId) {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_POS_API_ERROR_CODES.TENDERNOTFOUND.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidVoucherException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getters / setters
    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    public String getDetails() { return details; }
    public void setDetails(String details) { this.details = details; }

}
