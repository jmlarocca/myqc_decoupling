package com.mmhayes.common.api.errorhandling.exceptions;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.*;

//javax.ws.rs dependencies

//other dependencies


/*
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-08-23 10:19:22 -0400 (Mon, 23 Aug 2021) $: Date of last commit
 $Rev: 15153 $: Revision of last commit

 Notes: The "base" for all custom (checked) exceptions for MMHayes APIs
*/
public class APIException extends Exception {
    Integer httpStatus = HTTP_SERVER_CODES.GENERIC.getCode(); //default to 500 (internal server error)
    String errorCode = MMH_ERROR_CODES.UNKNOWN.getCode(); //default to error code 0 (unknown error code)
    String description = "N/A"; //description of the exception itself
    String details = "N/A"; //used to tell the client what went wrong
    String logFileName = "";  //overridden log file name

    public static enum HTTP_CLIENT_CODES { //HTTP client error codes (based on IETF standards)
        BADREQUEST(400), FORBIDDEN(403), NOTFOUND(404), BADMETHOD(405), BADMEDIA(415);
        private final int value;

        private HTTP_CLIENT_CODES(int value) {
            this.value = value;
        }

        public int getCode() {
            return value;
        }
    }

    public static enum HTTP_SERVER_CODES { //HTTP server error codes (based on IETF standards)
        GENERIC(500), UNKNOWN(501), BADGATEWAY(502), TEMPOFFLINE(503), BADHTTPVER(505);
        private final int value;

        private HTTP_SERVER_CODES(int value) {
            this.value = value;
        }

        public int getCode() {
            return value;
        }
    }

    public static enum MMH_ERROR_CODES { //MMHayes API Error Codes - specific to MMHayes
        UNKNOWN("E0000"), //could not determine what the error was - likely an uncaught exception
        GENERIC("E1000"), //for all errors that do not currently have another error code to go into
        INVALIDAUTH("E2000"), //client did not provide the proper session identifier for the server to trust it
        SESSIONEXPIRED("E2001"), //client did provide the proper session identifier, but it was expired
        INVALIDHASH("E2500"), //for communication from instances to/from gateways - a valid hash is generally needed for these communications
        INVALIDREQ("E3000"), //client made some errors in the data structure format or attempted to send a property that does not exist
        NOTFOUND("E4000"), //could not find the resource specified
        MISSINGDATA("E5000"), //the application expected data but could not find it
        CLIENTISSUE("E6000"), //the application ran into problems acting as a client
        FUNDINGISSUE("E7700"), //a generic error occurred trying to process credit card information
        FUNDINGCARDISSUE("E7701"), //a specific error occurred trying to process credit card information
        DATAACCESS("E9000"), //an error occurred trying to access persistent data
        INACTIVEKIOSK("E9001"); //the in use kiosk is inactive
        private final String value;

        private MMH_ERROR_CODES(String value) {
            this.value = value;
        }

        public String getCode() {
            return value;
        }
    }


    //MMH_POS_API_ERROR_CODES
    public static enum MMH_POS_API_ERROR_CODES { //MMHayes API Error Codes - specific to MMHayes
        TRANSNOTFOUND("E7000"), //the referenced  was found as inactive
        TRANSINDIVIDUALLIMIT("E7001"), //the request exceeded the Individual amount
        TRANSSTORELIMIT("E7002"), //the request exceeded the Store Limit
        TRANSGLOBALLIMIT("E7003"), //the request exceeded the Global Limit
        TRANSDUPLICATE("E7004"), //the transaction request is already saved in the database
        TRANSTIMEDOUT("E7005"), //the transaction request timed out
        TRANSMISSINGDATATIMEOUTSECONDS("E7030"), // missing property for timeout
        TRANSMISSINGDATASUBMISSIONTIME("E7031"), // missing submission time
        ACCOUNTNOTFOUND("E7100"), //the application could not find the account
        ACCOUNTINACTIVE("E7101"), //the referenced account was found as inactive
        ACCOUNTDUPLICATEEMAIL("E7102"), //the referenced account is trying to save a duplicate email
        ACCOUNTINVALIDSPENDINGPROFILE("E7103"), //the referenced account cannot access the terminal's spending profile
        GUESTACCOUNTLICENSENOTAVAILABLE("E7104"), // there are no more available licenses for account type "Guest"
        EMPLOYEEACCOUNTLICENSENOTAVAILABLE("E7105"), // there are no more available licenses for account types "Prepaid" or "Payroll Deduct"
        UNKNOWNACCOUNTLICENSETYPE("E7106"), // backup error code, they have used an account group with a type not 1, 2, or 5
        DISCOUNTNOTFOUND("E7200"), //the referenced Discount was not found
        DISCOUNTINVALID("E7201"), //the referenced Discount is invalid
        LOYALTYPROGRAMNOTFOUND("E7300"), //the referenced Loyalty Program was not found
        LOYALTYREWARDNOTFOUND("E7301"), //the referenced Loyalty Reward was not found
        TOOMANYFREEPRODUCTREWARDS("E7302"), //the Free Product type Loyalty Rewards outnumbered eligible Products
        INVALIDREWARDSUBMITTED("E7303"), //the referenced Loyalty Reward was not valid for the given transaction
        PRODUCTNOTFOUND("E7400"), //the referenced product was not found
        TERMINALNOTFOUND("E7500"), //the referenced Terminal was not found
        TENDERNOTFOUND("E7600"), //the referenced Tender was not found
        PAYMENTMETHODTYPENOTFOUND("E7601"), //the referenced Payment Method Type was not found
        CCAUTHORIZATIONTYPENOTFOUND("E7602"), //the referenced CC Authorization Type was not found
        SURCHARGENOTFOUND("E7700"), //the referenced Surcharge was not found
        PURCHASERESTRICTION("E7800"), //the referenced Purchase Restriction was not found
        COMBONOTFOUND("E7900"), //the referenced Combo was not found
        DONATIONNOTFOUND("E7304"); //the referenced Donation was not found


        private final String value;

        private MMH_POS_API_ERROR_CODES(String value) {
            this.value = value;
        }

        public String getCode() {
            return value;
        }
    }

    //constructor - DEFAULT
    public APIException() {
        setLogFileName(PosAPIHelper.getLogFileName());
    }

    //constructor
    public APIException(Integer httpStatus, String errorCode) {
        try {
            setHttpStatus(httpStatus);
            setErrorCode(errorCode);
            setLogFileName(PosAPIHelper.getLogFileName());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in APIException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    public APIException(Integer terminalId) {
        setLogFileName(PosAPIHelper.getLogFileName(terminalId));
    }

    //constructor
    public APIException(Integer httpStatus, String errorCode, Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);

            setHttpStatus(httpStatus);
            setErrorCode(errorCode);
            setLogFileName(logFileName);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in APIException!", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public Integer getHttpStatus() {
        return this.httpStatus;
    }

    //setter
    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    //getter
    public String getErrorCode() {
        return this.errorCode;
    }

    //setter
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }

    //getter
    public String getLogFileName() {
        return logFileName;
    }

    //setter
    //only override the logFileName if it is available
    public void setLogFileName(String logFileName) {

        if (logFileName != null && !logFileName.isEmpty()) {
            this.logFileName = logFileName;
        }
    }
}
