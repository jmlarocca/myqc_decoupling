package com.mmhayes.common.api.errorhandling.exceptions;

//mmhayes dependencies
import com.mmhayes.common.utils.*;

//javax.ws.rs dependencies

//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2016-09-02 10:53:45 -0400 (Fri, 02 Sep 2016) $: Date of last commit
 $Rev: 2842 $: Revision of last commit

 Notes: Custom (checked) exception for generic SQLRunner exceptions
*/
public class SQLRunnerException extends APIException {
    String description = "This exception means that a generic SQL error occurred when using SQLRunner. Could be database or query related."; //description of the exception itself
    String details = "Data Access Error. Please try again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public SQLRunnerException() {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.DATAACCESS.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in SQLRunnerException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public SQLRunnerException(String details) {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.DATAACCESS.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in SQLRunnerException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }

}
