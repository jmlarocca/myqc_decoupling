package com.mmhayes.common.api.errorhandling.exceptions;

import com.mmhayes.common.utils.Logger;


/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-03-22 08:57:24 -0400 (Wed, 22 Mar 2017) $: Date of last commit
 $Rev: 3751 $: Revision of last commit
*/
public class TerminalNotFoundException extends APIException {
    String description = "This Exception means that the referenced Terminal was not found in the request."; //description of the exception itself
    String details = "Terminal Not Found.  Please Try Again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public TerminalNotFoundException() {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TERMINALNOTFOUND.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TerminalNotFoundException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public TerminalNotFoundException(String details) {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_POS_API_ERROR_CODES.TERMINALNOTFOUND.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TerminalNotFoundException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }
}
