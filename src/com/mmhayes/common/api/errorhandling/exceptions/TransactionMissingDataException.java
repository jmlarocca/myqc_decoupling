package com.mmhayes.common.api.errorhandling.exceptions;

import com.mmhayes.common.utils.Logger;

public class TransactionMissingDataException extends APIException {

    // E5000 error due to missing data
    String description = "This exception is due to missing data in the transaction."; //description of the exception itself
    String details = "A critical error has occurred. Please contact M.M. Hayes support."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public TransactionMissingDataException() {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_ERROR_CODES.MISSINGDATA.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TransactionMissingDataException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    // constructor with specified details
    public TransactionMissingDataException(Integer reasonCode) {
        if(reasonCode == null) {
            new TransactionMissingDataException();
        } else if(reasonCode.equals(-1)) { // missing seconds
            setErrorCode(MMH_POS_API_ERROR_CODES.TRANSMISSINGDATATIMEOUTSECONDS.getCode());
            Logger.logMessage("TransactionMissingDataException: Missing the transaction timeout seconds from transaction.", Logger.LEVEL.ERROR);
        } else if(reasonCode.equals(-2)) { // missing order submission time
            setErrorCode(MMH_POS_API_ERROR_CODES.TRANSMISSINGDATASUBMISSIONTIME.getCode());
            Logger.logMessage("TransactionMissingDataException: Missing the order submission time from transaction.", Logger.LEVEL.ERROR);
        } else {
            new TransactionMissingDataException();
        }
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getDetails() {
        return details;
    }

    @Override
    public void setDetails(String details) {
        this.details = details;
    }

}
