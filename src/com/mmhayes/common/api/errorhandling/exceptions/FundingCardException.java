package com.mmhayes.common.api.errorhandling.exceptions;

//mmhayes dependencies

import com.mmhayes.common.utils.Logger;

//javax.ws.rs dependencies

//other dependencies


/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2016-09-02 10:53:21 -0400 (Fri, 02 Sep 2016) $: Date of last commit
 $Rev: 2841 $: Revision of last commit

 Notes: Custom (checked) exception for when REST Clients encounter an issue
*/
public class FundingCardException extends APIException {
    String description = "This exception means the server encountered an exception when acting as a client. Check previous logs to see what went wrong when the request was made."; //description of the exception itself
    String details = "An error occurred. Please check logs and try again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public FundingCardException() {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_ERROR_CODES.FUNDINGCARDISSUE.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in FundingCardException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public FundingCardException(String details) {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_ERROR_CODES.FUNDINGCARDISSUE.getCode());
            setDetails(details);
            Logger.logMessage("FundingCardException HTTP STATUS: "+getHttpStatus().toString(), Logger.LEVEL.ERROR);
            Logger.logMessage("FundingCardException ERROR CODE: "+getErrorCode(), Logger.LEVEL.ERROR);
            Logger.logMessage("FundingCardException DETAILS: "+getDetails(), Logger.LEVEL.ERROR);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in FundingCardException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }

}
