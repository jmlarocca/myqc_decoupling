package com.mmhayes.common.api.errorhandling.exceptions;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-04-05 11:09:14 -0400 (Wed, 05 Apr 2017) $: Date of last commit
 $Rev: 3793 $: Revision of last commit
*/
public class TransactionGlobalLimitException extends  APIException{
    String description = "This Exception means that the referenced Transaction exceeds the Global Transaction limit."; //description of the exception itself
    String details = "Transaction exceeds Global Transaction limit.  Please Try Again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public TransactionGlobalLimitException() {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TRANSGLOBALLIMIT.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TransactionGlobalLimitException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public TransactionGlobalLimitException(String details) {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TRANSGLOBALLIMIT.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TransactionGlobalLimitException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    public TransactionGlobalLimitException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TRANSGLOBALLIMIT.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TransactionGlobalLimitException!", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //constructor
    public TransactionGlobalLimitException(String details, Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TRANSGLOBALLIMIT.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TransactionGlobalLimitException!", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }
}