package com.mmhayes.common.api.errorhandling.exceptions;

//mmhayes dependencies
import com.mmhayes.common.utils.*;

//javax.ws.rs dependencies

//other dependencies


/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2016-09-02 10:53:21 -0400 (Fri, 02 Sep 2016) $: Date of last commit
 $Rev: 2841 $: Revision of last commit

 Notes: For handling invalid JSON sent from clients
*/
public class InvalidJSONTypeException extends APIException {
    String description = "This exception means that a property sent by the client was of the wrong type. Highly likely that invalid JSON was sent from the client."; //description of the exception itself
    String details = "Invalid Request. Please try again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public InvalidJSONTypeException() {
        try {
            setHttpStatus(APIException.HTTP_CLIENT_CODES.BADREQUEST.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.INVALIDREQ.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidJSONTypeException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public InvalidJSONTypeException(String details) {
        try {
            setHttpStatus(APIException.HTTP_CLIENT_CODES.BADREQUEST.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.INVALIDREQ.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidJSONTypeException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }

}
