package com.mmhayes.common.api.errorhandling.exceptions;

//mmhayes dependencies
import com.mmhayes.common.utils.*;

//javax.ws.rs dependencies

//other dependencies


/*
 Last Updated (automatically updated by SVN)
 $Author: cjczyzewski $: Author of last commit
 $Date: 2018-01-12 15:37:45 -0500 (Fri, 12 Jan 2018) $: Date of last commit
 $Rev: 6013 $: Revision of last commit

 Notes: Custom (checked) exception for when REST Clients encounter an issue
*/
public class RESTClientException extends APIException {
    String description = "This exception means the server encountered an exception when acting as a client. Check previous logs to see what went wrong when the request was made."; //description of the exception itself
    String details = "An error occurred. Please check logs and try again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public RESTClientException() {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.CLIENTISSUE.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidAuthException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public RESTClientException(String details) {
        try {
            setHttpStatus(APIException.HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.CLIENTISSUE.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidAuthException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String getMessage() {
        return getDetails();
    }

}
