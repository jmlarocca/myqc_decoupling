package com.mmhayes.common.api.errorhandling.exceptions;

//mmhayes dependencies
import com.mmhayes.common.utils.*;

//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-03-23 11:01:06 -0400 (Thu, 23 Mar 2017) $: Date of last commit
 $Rev: 3755 $: Revision of last commit

 Notes: For handling invalid JSON sent from clients
*/
public class InvalidJSONPropertyException extends APIException {
    String description = "This exception means a property of the object does not exist and was sent by the client. Highly likely that invalid JSON was sent from the client."; //description of the exception itself
    String details = "Invalid Request. Please try again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public InvalidJSONPropertyException() {
        try {
            setHttpStatus(APIException.HTTP_CLIENT_CODES.BADREQUEST.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.INVALIDREQ.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidJSONPropertyException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public InvalidJSONPropertyException(String details) {
        try {
            setHttpStatus(APIException.HTTP_CLIENT_CODES.BADREQUEST.getCode());
            setErrorCode(APIException.MMH_ERROR_CODES.INVALIDREQ.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidJSONPropertyException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }

}
