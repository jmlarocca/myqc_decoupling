package com.mmhayes.common.api.errorhandling.exceptions;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;

/*
 Last Updated (automatically updated by SVN)
 $Author: jmdottavio $: Author of last commit
 $Date: 2018-04-13 16:54:57 -0400 (Fri, 13 Apr 2018) $: Date of last commit
 $Rev: 6835 $: Revision of last commit
*/
public class InvalidRewardSubmittedException extends APIException {
    String description = "This Exception means that a reward was submitted that is not valid for the given transaction."; //description of the exception itself
    String details = "Invalid Reward Submitted.  Please Try Again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public InvalidRewardSubmittedException() {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.INVALIDREWARDSUBMITTED.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidRewardSubmittedException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public InvalidRewardSubmittedException(String details) {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.INVALIDREWARDSUBMITTED.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidRewardSubmittedException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    public InvalidRewardSubmittedException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.INVALIDREWARDSUBMITTED.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidRewardSubmittedException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //constructor
    public InvalidRewardSubmittedException(String details, Integer terminalId) {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.INVALIDREWARDSUBMITTED.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in InvalidRewardSubmittedException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }
}
