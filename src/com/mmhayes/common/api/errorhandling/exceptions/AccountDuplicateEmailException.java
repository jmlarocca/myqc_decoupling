package com.mmhayes.common.api.errorhandling.exceptions;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-06-05 10:49:22 -0400 (Tue, 05 Jun 2018) $: Date of last commit
 $Rev: 7224 $: Revision of last commit
*/
public class AccountDuplicateEmailException extends APIException {
    String description = "This Exception means that the referenced Account was trying to save a duplicate Email Address found in the database."; //description of the exception itself
    String details = "Email Address is already in use, please choose another."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public AccountDuplicateEmailException() {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.ACCOUNTDUPLICATEEMAIL.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in AccountDuplicateEmailException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public AccountDuplicateEmailException(String details) {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_POS_API_ERROR_CODES.ACCOUNTDUPLICATEEMAIL.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in AccountDuplicateEmailException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor - DEFAULT
    public AccountDuplicateEmailException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.ACCOUNTDUPLICATEEMAIL.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in AccountDuplicateEmailException!", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //constructor
    public AccountDuplicateEmailException(String details, Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_POS_API_ERROR_CODES.ACCOUNTDUPLICATEEMAIL.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in AccountDuplicateEmailException!", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }
}