package com.mmhayes.common.api.errorhandling.exceptions;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;

/*
 Last Updated (automatically updated by SVN)
 $Author: jmdottavio $: Author of last commit
 $Date: 2018-03-28 10:59:16 -0400 (Wed, 28 Mar 2018) $: Date of last commit
 $Rev: 6664 $: Revision of last commit
*/
public class TooManyFreeProductRewardsException extends APIException {
    String description = "This Exception means that more free product rewards were applied than eligible products in the transaction."; //description of the exception itself
    String details = "Too Many Free Product Rewards.  Please Try Again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public TooManyFreeProductRewardsException() {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TOOMANYFREEPRODUCTREWARDS.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TooManyFreeProductRewardsException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public TooManyFreeProductRewardsException(String details) {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TOOMANYFREEPRODUCTREWARDS.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TooManyFreeProductRewardsException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    public TooManyFreeProductRewardsException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TOOMANYFREEPRODUCTREWARDS.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TooManyFreeProductRewardsException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //constructor
    public TooManyFreeProductRewardsException(String details, Integer terminalId) {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.TOOMANYFREEPRODUCTREWARDS.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in TooManyFreeProductRewardsException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }
}
