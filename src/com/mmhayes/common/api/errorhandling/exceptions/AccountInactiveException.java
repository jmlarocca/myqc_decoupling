package com.mmhayes.common.api.errorhandling.exceptions;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-11-08 16:31:18 -0500 (Wed, 08 Nov 2017) $: Date of last commit
 $Rev: 5630 $: Revision of last commit
*/
public class AccountInactiveException extends APIException {
    String description = "This Exception means that the referenced Account was found to be inactive in the database."; //description of the exception itself
    String details = "Inactive Account.  Please Try Again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public AccountInactiveException() {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.ACCOUNTINACTIVE.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in AccountInactiveException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public AccountInactiveException(String details) {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_POS_API_ERROR_CODES.ACCOUNTINACTIVE.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in AccountInactiveException!", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor - DEFAULT
    public AccountInactiveException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);

            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.ACCOUNTINACTIVE.getCode());
            setLogFileName(logFileName);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in AccountInactiveException!", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //constructor
    public AccountInactiveException(String details, Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(APIException.MMH_POS_API_ERROR_CODES.ACCOUNTINACTIVE.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in AccountInactiveException!", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }

}

