package com.mmhayes.common.api.errorhandling.exceptions;

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-09-16 12:02:53 -0400 (Mon, 16 Sep 2019) $: Date of last commit
 $Rev: 9442 $: Revision of last commit
*/
public class PurchaseRestrictionException extends APIException {
    String description = "This Exception means there is a Purchase Restriction on the Item so it is not available."; //description of the exception itself
    String details = "Purchase Restriction on Item.  Please Try Again."; //used to tell the client what went wrong

    //constructor - DEFAULT
    public PurchaseRestrictionException() {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.PURCHASERESTRICTION.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in PurchaseRestrictionException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    //constructor
    public PurchaseRestrictionException(String details) {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.PURCHASERESTRICTION.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in PurchaseRestrictionException!");
            Logger.logException(ex); //always log exceptions
        }
    }

    public PurchaseRestrictionException(Integer terminalId) {
        try {
            String logFileName = PosAPIHelper.getLogFileName(terminalId);
            setLogFileName(logFileName);
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.PURCHASERESTRICTION.getCode());
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in PurchaseRestrictionException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //constructor
    public PurchaseRestrictionException(String details, Integer terminalId) {
        try {
            setHttpStatus(HTTP_SERVER_CODES.GENERIC.getCode());
            setErrorCode(MMH_POS_API_ERROR_CODES.PURCHASERESTRICTION.getCode());
            setDetails(details);
        } catch (Exception ex) {
            Logger.logMessage("ERROR - CRITICAL ERROR - Exception caught in PurchaseRestrictionException!", logFileName);
            Logger.logException(ex, logFileName); //always log exceptions
        }
    }

    //getter
    public String getDescription() {
        return description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public String getDetails() {
        return details;
    }

    //setter
    public void setDetails(String details) {
        this.details = details;
    }
}
