package com.mmhayes.common.api.errorhandling.mappers;

//mmhayes dependencies
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.utils.Logger;

//javax.ws.rs dependencies
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

//other dependencies


/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-02-16 10:57:27 -0500 (Thu, 16 Feb 2017) $: Date of last commit
 $Rev: 3551 $: Revision of last commit

 Notes: Mappers handle exceptions (of all types) by using them to create an MMHError. MMHError is then responded to the client.
*/

@Provider
public class ResourceNotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

    //respond with an MMHError entity in JSON
    @Override
    public Response toResponse(NotFoundException ex) {
        Logger.logException(ex); //log the original exception as well
        ResourceNotFoundException apiEx = new ResourceNotFoundException();
        MMHError mmhError = new MMHError(apiEx);
        mmhError.logMMHError(); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}