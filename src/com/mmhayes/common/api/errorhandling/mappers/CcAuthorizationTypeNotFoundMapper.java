package com.mmhayes.common.api.errorhandling.mappers;

//API Dependencies
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//Other Dependencies
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-06-18 10:54:52 -0400 (Mon, 18 Jun 2018) $: Date of last commit
 $Rev: 7288 $: Revision of last commit

 Notes: Mappers handle exceptions (of all types) by using them to create an MMHError. MMHError is then responded to the client.
*/

@Provider
public class CcAuthorizationTypeNotFoundMapper implements ExceptionMapper<CcAuthorizationTypeNotFoundException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(CcAuthorizationTypeNotFoundException apiEx) {
        MMHError mmhError = new MMHError(apiEx);
        mmhError.logMMHError(); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}