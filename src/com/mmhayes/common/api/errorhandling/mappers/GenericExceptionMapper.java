package com.mmhayes.common.api.errorhandling.mappers;

//common dependencies
import com.mmhayes.common.api.errorhandling.*;

//javax.ws.rs dependencies
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

//other dependencies


/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2016-06-07 11:59:59 -0400 (Tue, 07 Jun 2016) $: Date of last commit
 $Rev: 2458 $: Revision of last commit

 Notes: Notes: Mappers handle exceptions (of all types) by using them to create an MMHError. MMHError is then responded to the client.
*/

@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {

    @Override
    //respond with an MMHError entity in JSON
    public Response toResponse(Throwable throwableEx) {
        MMHError mmhError = new MMHError(throwableEx);
        setHttpStatus(throwableEx, mmhError);
        mmhError.logMMHError(); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    //if the exception was NOT a web application exception - set to 500
    private void setHttpStatus(Throwable ex, MMHError mmhError) {
        if(ex instanceof WebApplicationException ) {
            mmhError.setHttpStatus(((WebApplicationException)ex).getResponse().getStatus());
        } else {
            mmhError.setHttpStatus(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }
    }

}