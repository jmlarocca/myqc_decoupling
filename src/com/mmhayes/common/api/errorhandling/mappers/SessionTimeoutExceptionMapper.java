package com.mmhayes.common.api.errorhandling.mappers;

//mmhayes dependencies
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//javax.ws.rs dependencies
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

//other dependencies
/**
 * Created by admin on 12/28/2016.
 */

@Provider
public class SessionTimeoutExceptionMapper implements ExceptionMapper<SessionTimeoutException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(SessionTimeoutException apiEx) {
        MMHError mmhError = new MMHError(apiEx);
        mmhError.logMMHError(); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
