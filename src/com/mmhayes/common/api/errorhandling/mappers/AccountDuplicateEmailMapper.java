package com.mmhayes.common.api.errorhandling.mappers;

//API Dependencies
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.AccountDuplicateEmailException;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-06-05 10:49:22 -0400 (Tue, 05 Jun 2018) $: Date of last commit
 $Rev: 7224 $: Revision of last commit

 Notes: Mappers handle exceptions (of all types) by using them to create an MMHError. MMHError is then responded to the client.
*/

@Provider
public class AccountDuplicateEmailMapper implements ExceptionMapper<AccountDuplicateEmailException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(AccountDuplicateEmailException apiEx) {
        MMHError mmhError = new MMHError(apiEx, true);
        mmhError.logMMHError(true); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
