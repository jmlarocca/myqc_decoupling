package com.mmhayes.common.api.errorhandling.mappers;

//MMHayes dependencies
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.ComboNotFoundException;

//API dependencies
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-02-27 09:23:59 -0500 (Thu, 27 Feb 2020) $: Date of last commit
 $Rev: 10914 $: Revision of last commit
*/

@Provider
public class ComboNotFoundMapper implements ExceptionMapper<ComboNotFoundException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(ComboNotFoundException apiEx) {
        MMHError mmhError = new MMHError(apiEx, true);
        mmhError.logMMHError(true); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}