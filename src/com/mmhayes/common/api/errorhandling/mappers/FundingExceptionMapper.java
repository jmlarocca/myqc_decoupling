package com.mmhayes.common.api.errorhandling.mappers;

//mmhayes dependencies

import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.FundingException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

//javax.ws.rs dependencies

//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-10-23 11:37:40 -0400 (Mon, 23 Oct 2017) $: Date of last commit
 $Rev: 5493 $: Revision of last commit

 Notes: Mappers handle exceptions (of all types) by using them to create an MMHError. MMHError is then responded to the client.
*/

@Provider
public class FundingExceptionMapper implements ExceptionMapper<FundingException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(FundingException apiEx) {
        MMHError mmhError = new MMHError(apiEx);
        mmhError.logMMHError(); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}