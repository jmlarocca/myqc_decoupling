package com.mmhayes.common.api.errorhandling.mappers;

/*
 Last Updated (automatically updated by SVN)
 $Author: jmdottavio $: Author of last commit
 $Date: 2018-03-28 10:59:16 -0400 (Wed, 28 Mar 2018) $: Date of last commit
 $Rev: 6664 $: Revision of last commit

 Notes: Mappers handle exceptions (of all types) by using them to create an MMHError. MMHError is then responded to the client.
*/

import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.TenderNotFoundException;
import com.mmhayes.common.api.errorhandling.exceptions.TooManyFreeProductRewardsException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class TooManyFreeProductRewardsMapper implements ExceptionMapper<TooManyFreeProductRewardsException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(TooManyFreeProductRewardsException apiEx) {
        MMHError mmhError = new MMHError(apiEx, true);
        mmhError.logMMHError(true); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
