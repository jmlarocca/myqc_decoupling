package com.mmhayes.common.api.errorhandling.mappers;

import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidVoucherException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InvalidVoucherMapper implements ExceptionMapper<InvalidVoucherException> {
    //respond with an MMHError entity in JSON
    public Response toResponse(InvalidVoucherException apiEx) {
        MMHError mmhError = new MMHError(apiEx, true);
        mmhError.logMMHError(true); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}