package com.mmhayes.common.api.errorhandling.mappers;

//MMHayes dependencies
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.DonationNotFoundException;

//API dependencies
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-03-29 10:05:06 -0400 (Fri, 29 Mar 2019) $: Date of last commit
 $Rev: 8636 $: Revision of last commit
*/

@Provider
public class DonationExceptionMapper implements ExceptionMapper<DonationNotFoundException> {
    //respond with an MMHError entity in JSON
    public Response toResponse(DonationNotFoundException apiEx) {
        MMHError mmhError = new MMHError(apiEx, true);
        mmhError.logMMHError(true); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}

