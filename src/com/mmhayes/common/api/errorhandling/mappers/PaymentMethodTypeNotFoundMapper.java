package com.mmhayes.common.api.errorhandling.mappers;

import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.*;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-11-01 14:41:45 -0400 (Wed, 01 Nov 2017) $: Date of last commit
 $Rev: 5560 $: Revision of last commit

 Notes: Mappers handle exceptions (of all types) by using them to create an MMHError. MMHError is then responded to the client.
*/

@Provider
public class PaymentMethodTypeNotFoundMapper implements ExceptionMapper<PaymentMethodTypeNotFoundException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(PaymentMethodTypeNotFoundException apiEx) {
        MMHError mmhError = new MMHError(apiEx);
        mmhError.logMMHError(); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}