package com.mmhayes.common.api.errorhandling.mappers;

//API Dependencies
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.AccountSpendingProfileException;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-07-09 08:21:22 -0400 (Mon, 09 Jul 2018) $: Date of last commit
 $Rev: 7390 $: Revision of last commit

 Notes: Mappers handle exceptions (of all types) by using them to create an MMHError. MMHError is then responded to the client.
*/

@Provider
public class AccountSpendingProfileMapper implements ExceptionMapper<AccountSpendingProfileException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(AccountSpendingProfileException apiEx) {
        MMHError mmhError = new MMHError(apiEx, true);
        mmhError.logMMHError(true); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}