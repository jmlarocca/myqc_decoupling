package com.mmhayes.common.api.errorhandling.mappers;
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.DiscountNotFoundException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-10-19 17:17:09 -0400 (Thu, 19 Oct 2017) $: Date of last commit
 $Rev: 5483 $: Revision of last commit
*/

@Provider
public class DiscountNotFoundMapper implements ExceptionMapper<DiscountNotFoundException> {
    //respond with an MMHError entity in JSON
    public Response toResponse(DiscountNotFoundException apiEx) {
        MMHError mmhError = new MMHError(apiEx, true);
        mmhError.logMMHError(true); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
