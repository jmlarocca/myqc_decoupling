package com.mmhayes.common.api.errorhandling.mappers;

//mmhayes dependencies
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//javax.ws.rs dependencies
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2016-07-14 14:23:02 -0400 (Thu, 14 Jul 2016) $: Date of last commit
 $Rev: 2593 $: Revision of last commit

 Notes: Mappers handle exceptions (of all types) by using them to create an MMHError. MMHError is then responded to the client.
*/

@Provider
public class RESTClientExceptionMapper implements ExceptionMapper<RESTClientException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(RESTClientException apiEx) {
        MMHError mmhError = new MMHError(apiEx);
        mmhError.logMMHError(); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}