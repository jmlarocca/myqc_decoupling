package com.mmhayes.common.api.errorhandling.mappers;

//API Dependencies
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.SurchargeNotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 11/6/18
 * Time: 4:30 PM
 * To change this template use File | Settings | File Templates.
 */

@Provider
public class SurchargeNotFoundMapper implements ExceptionMapper<SurchargeNotFoundException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(SurchargeNotFoundException apiEx) {
        MMHError mmhError = new MMHError(apiEx, true);
        mmhError.logMMHError(true); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}