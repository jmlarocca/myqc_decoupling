package com.mmhayes.common.api.errorhandling.mappers;

//mmhayes dependencies
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//javax.ws.rs dependencies
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

//other dependencies


/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2016-06-07 11:59:59 -0400 (Tue, 07 Jun 2016) $: Date of last commit
 $Rev: 2458 $: Revision of last commit

 Notes: Mappers handle exceptions (of all types) by using them to create an MMHError. MMHError is then responded to the client.
*/

@Provider
public class InvalidHashExceptionMapper implements ExceptionMapper<InvalidHashException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(InvalidHashException apiEx) {
        MMHError mmhError = new MMHError(apiEx);
        mmhError.logMMHError(); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}