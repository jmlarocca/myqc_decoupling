package com.mmhayes.common.api.errorhandling.mappers;

import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.*;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-04-05 11:09:14 -0400 (Wed, 05 Apr 2017) $: Date of last commit
 $Rev: 3793 $: Revision of last commit

 Notes: Mappers handle exceptions (of all types) by using them to create an MMHError. MMHError is then responded to the client.
*/

@Provider
public class DuplicateTransactionMapper implements ExceptionMapper<DuplicateTransactionException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(DuplicateTransactionException apiEx) {
        MMHError mmhError = new MMHError(apiEx, true);
        mmhError.logMMHError(true); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
