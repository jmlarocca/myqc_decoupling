package com.mmhayes.common.api.errorhandling.mappers;

//MMHayes dependencies
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.api.errorhandling.exceptions.TransactionTimeoutException;

//API dependencies
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-02-08 15:52:12 -0500 (Fri, 08 Feb 2019) $: Date of last commit
 $Rev: 8398 $: Revision of last commit

 Notes: Mappers handle exceptions (of all types) by using them to create an MMHError. MMHError is then responded to the client.
*/

@Provider
public class TransactionTimeoutMapper implements ExceptionMapper<TransactionTimeoutException> {

    //respond with an MMHError entity in JSON
    public Response toResponse(TransactionTimeoutException apiEx) {
        MMHError mmhError = new MMHError(apiEx);
        mmhError.logMMHError(); //always log the error before responding to client
        return Response
                .status(mmhError.getHttpStatus())
                .entity(mmhError)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}