package com.mmhayes.common.api;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;

//other dependencies
import java.util.*;

/* Model Paginator
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-05-25 17:59:02 -0400 (Thu, 25 May 2017) $: Date of last commit
 $Rev: 4042 $: Revision of last commit

 Notes: For paginating collections of models
*/
public class ModelPaginator {
    public Integer models = 20; //how many models should be displayed per request
    public Integer modelCount = 20; //how many models were counted in total for the collection of models
    public Integer page = 1; //what page of models should be returned for this request
    public double pageCount = 1; //how many pages are there total for this collection of models
    public Integer startingModelIndex = 0; //which model index is at the start of this.page
    public Integer endingModelIndex = 20; //which model index is at the end of this.page
    public String sort = "date"; //what object should the models be sorted by
    public String order = "desc"; //what is the order the models should be sorted in

    //constructor - populates initial pagination properties (models, page, sort and order)
    public ModelPaginator(HashMap<String, LinkedList> paginationParamsHM) {
        try {
            //sets initial properties of this collection's pagination properties
            initializePaginationProperties(paginationParamsHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //sets initial properties of this ModelPaginator's pagination properties
    public ModelPaginator initializePaginationProperties(HashMap<String, LinkedList> paginationParamsHM) {
        try {
            if (paginationParamsHM != null) {
                if (paginationParamsHM.get("MODELS") != null && paginationParamsHM.get("MODELS").get(0) != null) {
                    setModels(Integer.parseInt(paginationParamsHM.get("MODELS").get(0).toString()));
                }
                if (paginationParamsHM.get("PAGE") != null && paginationParamsHM.get("PAGE").get(0) != null) {
                    setPage(Integer.parseInt(paginationParamsHM.get("PAGE").get(0).toString()));
                }
                if (paginationParamsHM.get("SORT") != null && paginationParamsHM.get("SORT").get(0) != null) {
                    setSort(paginationParamsHM.get("SORT").get(0).toString());
                }
                if (paginationParamsHM.get("ORDER") != null && paginationParamsHM.get("ORDER").get(0) != null) {
                    setOrder(paginationParamsHM.get("ORDER").get(0).toString());
                }
            }

            //validate the pagination properties for this instance of this collection
            validatePaginationProperties();

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return this;
    }

    //validate the pagination properties for this instance of this ModelPaginator
    public void validatePaginationProperties() {
        try {

            //if models property is negative, it is invalid
            if (getModels() < 0) {
                setModels(20);
                Logger.logMessage("WARNING: Invalid model property in ModelPaginator.validatePaginationProperties() . Setting to default: "+getModels(), Logger.LEVEL.DEBUG);
            }

            //if modelCount property is negative, it is invalid
            if (getModelCount() < 0) {
                setModelCount(20);
                Logger.logMessage("WARNING: Invalid modelCount property in ModelPaginator.validatePaginationProperties() . Setting to default: "+getModelCount(), Logger.LEVEL.DEBUG);
            }

            //if page property is negative, it is invalid
            if (getPage() < 0) {
                setPage(1);
                Logger.logMessage("WARNING: Invalid page property in ModelPaginator.validatePaginationProperties() . Setting to default: "+getPage(), Logger.LEVEL.DEBUG);
            }

            //if pageCount property is negative, it is invalid
            if (getPageCount() < 0) {
                setPageCount(1);
                Logger.logMessage("WARNING: Invalid pageCount property in ModelPaginator.validatePaginationProperties() . Setting to default: "+getPageCount(), Logger.LEVEL.DEBUG);
            }

            //if startingModelIndex property is negative, it is invalid
            if (getStartingModelIndex() < 0) {
                setStartingModelIndex(0);
                Logger.logMessage("WARNING: Invalid startingModelIndex property in ModelPaginator.validatePaginationProperties() . Setting to default: "+getStartingModelIndex(), Logger.LEVEL.DEBUG);
            }

            //if endingModelIndex property is negative, it is invalid
            if (getEndingModelIndex() < 0) {
                setEndingModelIndex(20);
                Logger.logMessage("WARNING: Invalid endingModelIndex property in ModelPaginator.validatePaginationProperties() . Setting to default: "+getEndingModelIndex(), Logger.LEVEL.DEBUG);
            }

            //validate the "sort" property - used to protect against SQL Injection as the client could send "any string"
            validateSortProperty();

            //if order property is not "ASC" or "DESC", it is invalid
            if (!getOrder().toUpperCase().equals("ASC") && !getOrder().toUpperCase().equals("DESC")) {
                setOrder("desc");
                Logger.logMessage("WARNING: Invalid order property in ModelPaginator.validatePaginationProperties() . Setting to default: "+getOrder().toUpperCase(), Logger.LEVEL.DEBUG);
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //determines, sets and validates pagination properties
    public void finalizePaginationProperties() {
        try {

            //determine and set the page count
            if (getModelCount() > 0) { //if there is more than one total model
                if (getModelCount() % getModels() == 0) { //if the modelCount is divisible by the models
                    setPageCount(getModelCount() / getModels());
                } else { //else the modelCount is not divisible by the models, so we need to round up and add one
                    setPageCount(Math.ceil(getModelCount() / getModels()) + 1);
                }
            }
            if (getPageCount() < 1 && getModelCount() > 0) { //if the model count is greater than 0, we should have at least one page
                setPageCount(1);
            }

            //determine and set starting and ending model indexes
            if (getPageCount() > 0) {
                setEndingModelIndex(getModels() * getPage());
                setStartingModelIndex(getEndingModelIndex() - getModels());
            }

            //validate the pagination properties for this instance of ModelPaginator
            validatePaginationProperties();

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
    }

    //validate the "sort" property - used to protect against SQL Injection as the client could send "any string"
    private void validateSortProperty() {
        try {

            //if the sort sent from the client doesn't match a valid sort object - we will default to "date" - to protect against SQL injection
            if (getSort().toUpperCase().equals("DATE")) {
                setSort("date");
            } else if (getSort().toUpperCase().equals("ID")) {
                setSort("ID");
            } else if (getSort().toUpperCase().equals("NAME")) {
                setSort("name");
            } else if (getSort().toUpperCase().equals("NUMBER")) {
                setSort("number");
            } else if (getSort().toUpperCase().equals("BADGE")) {
                setSort("badge");
            } else {
                setSort("date");
                Logger.logMessage("Invalid sort property in ModelPaginator.validatePaginationProperties() . Setting to default: "+getSort().toUpperCase(), Logger.LEVEL.WARNING);
            }

        } catch (Exception ex) {
            setSort("date");
            Logger.logMessage("Validating the sort property failed due to critical error. Setting to default: "+getSort().toUpperCase(), Logger.LEVEL.WARNING);
            Logger.logException(ex); //always log exceptions
        }
    }

    //getter
    public Integer getModels() {
        return this.models;
    }

    //setter
    public void setModels(Integer models) {
        this.models = models;
    }

    //getter
    public Integer getModelCount() {
        return this.modelCount;
    }

    //setter
    public void setModelCount(Integer modelCount) {
        this.modelCount = modelCount;
    }

    //getter
    public Integer getPage() {
        return this.page;
    }

    //setter
    public void setPage(Integer page) {
        this.page = page;
    }

    //getter
    public double getPageCount() {
        return this.pageCount;
    }

    //setter
    public void setPageCount(double pageCount) {
        this.pageCount = pageCount;
    }

    //getter
    public Integer getStartingModelIndex() {
        return this.startingModelIndex;
    }

    //setter
    public void setStartingModelIndex(Integer startingModelIndex) {
        this.startingModelIndex = startingModelIndex;
    }

    //getter
    public Integer getEndingModelIndex() {
        return this.endingModelIndex;
    }

    //setter
    public void setEndingModelIndex(Integer endingModelIndex) {
        this.endingModelIndex = endingModelIndex;
    }

    //getter
    public String getSort() {
        return sort;
    }

    //setter
    public void setSort(String sort) {
        this.sort = sort;
    }

    //getter
    public String getOrder() {
        return order;
    }

    //setter
    public void setOrder(String order) {
        this.order = order;
    }

}
