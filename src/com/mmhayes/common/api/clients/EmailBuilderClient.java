package com.mmhayes.common.api.clients;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.Validation;
import com.mmhayes.common.api.errorhandling.exceptions.RESTClientException;
import com.mmhayes.common.utils.Logger;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Rest client to retrieve MMH Cloud login name for use in config emails
 */
public class EmailBuilderClient extends RestClient {

    //takes the instance host name and endpoint path to build the target
    public EmailBuilderClient(String target, String path){
        String buildTarget = target + path;
        setTarget(buildTarget);
        setPath(path);
    }

    //this method sends a POST request using the Rest Client object and returns the MMH Cloud Account login name
    public String getCloudAccount(Integer userID, Integer instanceUserTypeID ) throws Exception{

        try {
            Validation validation = new Validation();
            validation.setInstanceUserID(userID);
            validation.setInstanceID(CommonAPI.getInstanceID());

            // Put InstanceUserTypeID in data field of Validation object
            HashMap dataMap = new HashMap();
            dataMap.put("InstanceUserTypeID",instanceUserTypeID);
            ArrayList<HashMap> data = new ArrayList<>();
            data.add(dataMap);
            validation.setData(data);

            validation.buildHash();

            Validation result = getClient().target(getTarget())
                    .request(getType())
                    .accept(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(validation, MediaType.APPLICATION_JSON_TYPE), Validation.class);

            return result.getLoginName();

        } catch(Exception ex){
            Logger.logMessage("Error: getCloudAccount()");
            Logger.logException(ex);
            throw new RESTClientException();
        }
    }

}
