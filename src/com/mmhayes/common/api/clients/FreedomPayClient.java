package com.mmhayes.common.api.clients;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.api.errorhandling.exceptions.RESTClientException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.funding.freedompay.*;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Rest Client for communicating with the FreedomPay Hosted Payments Page (HPP) Service
 */
public class FreedomPayClient extends RestClient {

    private String apiTarget;
    private final static String FREEDOMPAY_DEBUG = "site.freedompay.debug";
    private final static String FREEDOMPAY_DEBUG_URL = "https://payments.uat.freedompay.com/CheckoutService/CheckoutService.svc";

    //takes the instance host name and endpoint path to build the target
    public FreedomPayClient(){

        // Constructor
        String DEFAULT_TARGET = "https://payments.freedompay.com/CheckoutService/CheckoutService.svc";

        //apiTarget = MMHProperties.getAppSetting(PROP_API_URL);
        DataManager dm = new DataManager();
        ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.common.funding.getFreedomPayApiEndPointURL", new Object[]{}, true);
        if(!result.isEmpty()){
            if(result.get(0).containsKey("APIURL2")){
                apiTarget = result.get(0).get("APIURL2").toString();
            }
        }

        // use default if configuration not found
        if(apiTarget == null || apiTarget.isEmpty() || apiTarget.equals("N/A") ){
            // if NOT Found use default
            apiTarget = DEFAULT_TARGET;
        }

        // Check for Debug setting
        String debug = MMHProperties.getAppSetting(FREEDOMPAY_DEBUG);
        if(!debug.equals("") && debug.toLowerCase().equals("true")){
            apiTarget = FREEDOMPAY_DEBUG_URL;
            Logger.logMessage("Using FreedomPay HPP Debug URL: "+apiTarget, Logger.LEVEL.DEBUG);
        } else {
            Logger.logMessage("Using FreedomPay HPP Production URL: "+apiTarget, Logger.LEVEL.DEBUG);
        }
    }

    // this method calls the FreedomPay HPP method createTransaction
    public FreedomPayCreateTransactionResponse createTransaction(FreedomPayRequest freedomPayRequest, HttpServletRequest servletRequest) throws Exception{

        Logger.logMessage("In method createTransaction.");

        try {
            //throw InvalidAuthException if no authenticated employee is found
            Integer authenticatedEmployeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(servletRequest);
            if (authenticatedEmployeeID == null) {
                throw new InvalidAuthException();
            }

            // INIT METHOD Target
            String path = "/createTransaction";
            setTarget(apiTarget+path);

            // Create Request
            FreedomPayCreateTransactionRequest request = new FreedomPayCreateTransactionRequest();
            request.setTerminalID(freedomPayRequest.getTerminalID());
            request.setStoreID(freedomPayRequest.getStoreID());
            request.setTransactionTotal(freedomPayRequest.getTransactionTotal().toPlainString());

            // Post Request
            FreedomPayCreateTransactionResponse response = getClient().target(getTarget())
                    .request(getType())
                    .accept(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE), FreedomPayCreateTransactionResponse.class);

            Logger.logMessage("CreateTransaction() Response: "+response.getResponseMessage(), Logger.LEVEL.DEBUG);

            return response;

        } catch(Exception ex){
            Logger.logMessage("Error: in method createTransaction()");
            Logger.logException(ex);
            throw new RESTClientException();
        }
    }

    public FreedomPayCreateTransactionResponse createTokenTransaction(FreedomPayRequest freedomPayRequest, HttpServletRequest servletRequest) throws Exception{

        try {
            //throw InvalidAuthException if no authenticated employee is found
            Integer authenticatedEmployeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(servletRequest);
            if (authenticatedEmployeeID == null) {
                throw new InvalidAuthException();
            }

            // INIT METHOD Target
            String path = "/createTokenTransaction";
            setTarget(apiTarget+path);

            //create request
            FreedomPayCreateTransactionRequest request = new FreedomPayCreateTransactionRequest();
            request.setTerminalID(freedomPayRequest.getTerminalID());
            request.setStoreID(freedomPayRequest.getStoreID());
            request.setTimeoutMinutes(freedomPayRequest.getTimeoutMinutes());
            request.setTokenType(freedomPayRequest.getTokenType());
            request.setRequestToken("true");

            // Post Request
            FreedomPayCreateTransactionResponse response;
            if(CommonAPI.envUsesFreedomPayUAT()) {
                Logger.logMessage("UAT Environment, using the regular FP Txn Response", Logger.LEVEL.DEBUG);
                response = getClient().target(getTarget())
                        .request(getType())
                        .accept(MediaType.APPLICATION_JSON)
                        .post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE), FreedomPayCreateTransactionResponse.class);
            } else {
                Logger.logMessage("Production environment found, using prod FP Txn Response", Logger.LEVEL.DEBUG);
                response = getClient().target(getTarget())
                        .request(getType())
                        .accept(MediaType.APPLICATION_JSON)
                        .post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE), FreedomPayCreateTransactionResponseProduction.class);
            }

            Logger.logMessage("CreateTokenTransaction response: "+response.getResponseMessage(), Logger.LEVEL.DEBUG);

            return response;

        } catch(Exception ex){
            Logger.logMessage("Error: in method createTokenTransaction()");
            Logger.logException(ex);
            throw new RESTClientException();
        }
    }

    public FreedomPayCreateTransactionResponse createVerificationTransaction(FreedomPayRequest freedomPayRequest, HttpServletRequest servletRequest) throws Exception{

        try {
            //throw InvalidAuthException if no authenticated employee is found
            Integer authenticatedEmployeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(servletRequest);
            if (authenticatedEmployeeID == null) {
                throw new InvalidAuthException();
            }

            // INIT METHOD Target
            String path = "/createVerificationTransaction";
            setTarget(apiTarget+path);

            FreedomPayCreateTransactionRequest request = new FreedomPayCreateTransactionRequest();
            request.setTerminalID(freedomPayRequest.getTerminalID());
            request.setStoreID(freedomPayRequest.getStoreID());

            FreedomPayCreateTransactionResponse response = getClient().target(getTarget())
                    .request(getType())
                    .accept(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE), FreedomPayCreateTransactionResponse.class);

            Logger.logMessage("CreateVerificationTransaction Response: "+response.getResponseMessage(), Logger.LEVEL.DEBUG);

            return response;

        } catch(Exception ex){
            Logger.logMessage("Error: in method createVerificationTransaction()");
            Logger.logException(ex);
            throw new RESTClientException();
        }
    }

    public FreedomPayGetTransactionResponse getTransaction(String transactionID, HttpServletRequest servletRequest) throws Exception{

        try {

            if(servletRequest!= null){
                //throw InvalidAuthException if no authenticated employee is found
                Integer authenticatedEmployeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(servletRequest);
                if (authenticatedEmployeeID == null) {
                    throw new InvalidAuthException();
                }
            }

            // Validate transactionID starts and ends with quotes
            if(!transactionID.startsWith("\"")){transactionID = "\""+transactionID;}
            if(!transactionID.endsWith("\"")){transactionID = transactionID+"\"";}

            // INIT METHOD Target
            String path = "/gettransaction";
            setTarget(apiTarget+path);

            FreedomPayGetTransactionResponse response = getClient().target(getTarget())
                    .request(getType())
                    .accept(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(transactionID,MediaType.APPLICATION_JSON), FreedomPayGetTransactionResponse.class);

            Logger.logMessage("GetTransaction: "+response, Logger.LEVEL.DEBUG);

            return response;

        } catch(Exception ex){
            Logger.logMessage("Error: in method getTransaction()");
            Logger.logException(ex);
            throw new RESTClientException();
        }
    }

    public boolean cancelTransaction(String transactionID, HttpServletRequest servletRequest) throws Exception{

        try {
            //throw InvalidAuthException if no authenticated employee is found
            Integer authenticatedEmployeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(servletRequest);
            if (authenticatedEmployeeID == null) {
                throw new InvalidAuthException();
            }

            // INIT METHOD Target
            String path = "/cancelTransaction";
            setTarget(apiTarget+path);

            FreedomPayCaptureTransactionRequest request = new FreedomPayCaptureTransactionRequest();
            request.setTransactionId(transactionID);

            Boolean response = getClient().target(getTarget())
                    .request(getType())
                    .accept(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE), java.lang.Boolean.class);

            Logger.logMessage("CancelTransaction: "+response, Logger.LEVEL.DEBUG);

            return response;

        } catch(Exception ex){
            Logger.logMessage("Error: in method cancelTransaction()");
            Logger.logException(ex);
            throw new RESTClientException();
        }
    }

    public FreedomPayCaptureTransactionResponse captureTransaction(String transactionID, HttpServletRequest servletRequest) throws Exception{

        try {
            //throw InvalidAuthException if no authenticated employee is found
            Integer authenticatedEmployeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(servletRequest);
            if (authenticatedEmployeeID == null) {
                throw new InvalidAuthException();
            }

            // INIT METHOD Target
            String path = "/captureTransaction";
            setTarget(apiTarget+path);

            FreedomPayCaptureTransactionRequest request = new FreedomPayCaptureTransactionRequest();
            request.setTransactionId(transactionID);

            FreedomPayCaptureTransactionResponse response = getClient().target(getTarget())
                    .request(getType())
                    .accept(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE), FreedomPayCaptureTransactionResponse.class);

            Logger.logMessage("CaptureTransaction response: "+response.getResponseMessage()+" WasCaptured: "+response.isWasCaptured(), Logger.LEVEL.DEBUG);

            return response;

        } catch(Exception ex){
            Logger.logMessage("Error: in method captureTransaction()");
            Logger.logException(ex);
            throw new RESTClientException();
        }
    }

}
