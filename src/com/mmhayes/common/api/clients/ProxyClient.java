package com.mmhayes.common.api.clients;

import com.mmhayes.common.utils.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class ProxyClient extends RestClient {

    public ProxyClient(String target) {
        try{
            setTarget(target);
        }catch (Exception ex){
            Logger.logException(ex);
        }
    }

    public Response testGet(){
        // get Request
        try{
            //setTarget("https://qc92devmsdn.mmhayes.com/qc/api/auth/errorHTML");
            //setTarget("https://www.ncdc.noaa.gov/cdo-web/api/v2/data?datasetid=GHCND&locationid=ZIP:28801&startdate=2010-05-01&enddate=2010-05-01");  //NOAA API test

//            String username = (String)getClient().getConfiguration().getProperty(ClientProperties.PROXY_USERNAME);
//            String password = (String)getClient().getConfiguration().getProperty(ClientProperties.PROXY_PASSWORD);
//            String uri = (String)getClient().getConfiguration().getProperty(ClientProperties.PROXY_URI);
//            String encoded = "Basic " + Base64.getEncoder().encodeToString((username+":"+password).getBytes());//.getBytes(StandardCharsets.UTF_8)
            Response response = getClient().target(getTarget())
                    .request(getType())

                    .accept(MediaType.TEXT_HTML)
                    //.header("token", "yWepjWkBuVVWCNHKwjzJPVJijRQZdduu") //for the NOAA api test
                    .get();//Entity.entity(map, MediaType.APPLICATION_JSON_TYPE), Response.class);

            Logger.logMessage(response.toString());
            //Logger.logMessage(response.readEntity(String.class));
            return response;

        }catch(Exception ex){
            Logger.logException(ex);
        }
        return null;
    }
    public Response testGet(String token){
        // get Request
        try{
            Response response = getClient().target(getTarget())
                    .request(getType())
                    .accept(MediaType.TEXT_HTML)
                    .header("token", token) //for the NOAA api test
                    .get();

            Logger.logMessage(response.toString());
            return response;

        }catch(Exception ex){
            Logger.logException(ex);
        }
        return null;
    }
}
