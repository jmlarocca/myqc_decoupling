package com.mmhayes.common.api.clients;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.HashMap;
import java.util.Properties;

//javax.ws.rs dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: ecdyer $: Author of last commit
 $Date: 2020-02-05 15:24:14 -0500 (Wed, 05 Feb 2020) $: Date of last commit
 $Rev: 10756 $: Revision of last commit

Notes: This client is a generic object to consume REST resources
*/
public class RestClient {
    private Client client = null; //jersey REST client
    private String target = null; //target for http/https requests - ex. "https://www.mmhcloud.com/api"
    private String path = null; //path for REST resources - appended to the target - ex. "/transaction/add"
    private String type = MediaType.APPLICATION_JSON; //request type - defaults to JSON
    private HashMap<String, String> headers = null; //for setting HTTP headers in requests

    protected RestClient() {
        try {
            setClient(createSSLClient());
        } catch (Exception ex) {
            Logger.logException(ex);
        }
    }

    //creates a REST client for all requests from the instance - SSL Enabled
    private Client createSSLClient() {
        try {
            DataManager dm = new DataManager();
            ClientConfig clientConfig = new ClientConfig();

            // SET TIMEOUTS
            clientConfig.property(ClientProperties.READ_TIMEOUT, CommonAPI.getRestReadTimeout());
            clientConfig.property(ClientProperties.CONNECT_TIMEOUT, CommonAPI.getRestConnectionTimeout());

            RestClientHelper restClientHelper = new RestClientHelper();

            HashMap results = (HashMap) dm.parameterizedExecuteQuery("data.globalSettings.getProxySettings", new Object[]{}, true).get(0);
            String password = StringFunctions.decodePassword((String)results.get("PROXYPASSWORD"));
            String username = (String)results.get("PROXYUSERNAME");
            String uri = (String)results.get("PROXYURI");

            if(uri != null && uri.length() > 0){
                Properties systemProperties = System.getProperties();
                String[] parts = uri.split(":");
                String host = parts[parts.length-2];
                String port = parts[parts.length-1];
                systemProperties.setProperty("http.proxyHost",host);
                systemProperties.setProperty("http.proxyPort",port);
                systemProperties.setProperty("https.proxyHost",host);
                systemProperties.setProperty("https.proxyPort",port);

                Logger.logMessage("REST CLIENT: proxy uri configuration set.", Logger.LEVEL.DEBUG);

                //if there is a username and password set up the properties for them.
                if(username != null && password != null && username.length() > 0 && password.length() > 0){
                    systemProperties.setProperty("http.proxyUser", username);
                    systemProperties.setProperty("http.proxyPassword", password);
                    systemProperties.setProperty("https.proxyUser", username);
                    systemProperties.setProperty("https.proxyPassword", password);
                    //set up auth using the username and password
                    Authenticator.setDefault(new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            Logger.logMessage("REST CLIENT: returning proxy auth.");
                            return new PasswordAuthentication(username, password.toCharArray());
                        }
                    });
                    Logger.logMessage("REST CLIENT: proxy auth configuration set.", Logger.LEVEL.DEBUG);
                }else{//we dont have a user/pass configuration
                    System.clearProperty("http.proxyUser");
                    System.clearProperty("http.proxyPassword");
                    System.clearProperty("https.proxyUser");
                    System.clearProperty("https.proxyPassword");
                    Logger.logMessage("REST CLIENT: proxy auth configuration removed.", Logger.LEVEL.DEBUG);
                }

                  //Alternative Connection method through Apache
//                ApacheConnectorProvider connector = new ApacheConnectorProvider();
//                clientConfig.connectorProvider(connector);
//                clientConfig.property(ClientProperties.PROXY_URI, uri);
//                clientConfig.property(ClientProperties.PROXY_PASSWORD, password);
//                clientConfig.property(ClientProperties.PROXY_USERNAME, username);

                return restClientHelper.buildSSLClient(clientConfig);
            }else{//remove all proxy settings.
                System.clearProperty("http.proxyHost");
                System.clearProperty("http.proxyPort");
                System.clearProperty("http.proxyUser");
                System.clearProperty("http.proxyPassword");

                System.clearProperty("https.proxyHost");
                System.clearProperty("https.proxyPort");
                System.clearProperty("https.proxyUser");
                System.clearProperty("https.proxyPassword");

                Logger.logMessage("REST CLIENT: proxy configuration removed.", Logger.LEVEL.DEBUG);

                return restClientHelper.buildSSLClient(clientConfig);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }
        return null;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    protected String getTarget() {
        return target;
    }

    protected void setTarget(String target) {
        this.target = target;
    }

    protected String getPath() {
        return path;
    }

    protected void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(HashMap<String, String> headers) {
        this.headers = headers;
    }

}