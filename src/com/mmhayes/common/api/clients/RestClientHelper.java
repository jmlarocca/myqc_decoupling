package com.mmhayes.common.api.clients;

//mmhayes dependencies

//payment dependencies

//javax.ws.rs dependencies
import javax.ws.rs.client.*;
import javax.net.ssl.*;
import javax.ws.rs.core.Configuration;

//other dependencies
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/*
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2016-07-14 14:23:02 -0400 (Thu, 14 Jul 2016) $: Date of last commit
 $Rev: 2593 $: Revision of last commit

Notes: This object is a helper object to the RestClient object - used to build Jersey SSL Clients
*/
class RestClientHelper {

    //for ignoring invalid certificates
    TrustManager[] certs = new TrustManager[] {
            new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }
            }
    };

    //builds a Jersey SSL client
    Client buildSSLClient(Configuration config) throws Exception {
        SSLContext ctx = SSLContext.getInstance("SSL");
        ctx.init(null, certs, new SecureRandom());
        return ClientBuilder.newBuilder()
                .withConfig(config)
                .hostnameVerifier(new TrustAllHostNameVerifier())
                .sslContext(ctx)
                .build();
    }

    //for trusting all host names
    private static class TrustAllHostNameVerifier implements HostnameVerifier {

        public boolean verify(String hostname, SSLSession session) {
            return true;
        }

    }

}