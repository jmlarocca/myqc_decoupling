package com.mmhayes.common.api.clients;

import com.mmhayes.common.utils.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class MonitoringClient extends RestClient {

    public MonitoringClient(String target){
        super();
        setTarget(target);
    }

    public Response checkGet(String path){
        // get Request
        try{
           Response response = getClient().target(getTarget())
                    .path(path)
                    .request(getType())
                    .accept(MediaType.MEDIA_TYPE_WILDCARD)//(MediaType.TEXT_HTML)
                    .get();
            return response;

        }catch(Exception ex){
            Logger.logException(ex);
        }
        return null;
    }
}
