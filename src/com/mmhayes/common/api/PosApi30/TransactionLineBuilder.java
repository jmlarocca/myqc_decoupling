package com.mmhayes.common.api.PosApi30;

//mmhayes dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.account.models.AccountQueryParams;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.combo.models.ComboModel;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.donation.models.DonationLineItemModel;
import com.mmhayes.common.donation.models.DonationModel;
import com.mmhayes.common.keypad.models.KeypadDetailModel;
import com.mmhayes.common.loyalty.collections.ItemLoyaltyPointCollection;
import com.mmhayes.common.loyalty.models.*;
import com.mmhayes.common.product.collections.ItemDiscountCollection;
import com.mmhayes.common.product.collections.ItemRewardCollection;
import com.mmhayes.common.product.collections.ItemSurchargeCollection;
import com.mmhayes.common.product.collections.ItemTaxCollection;
import com.mmhayes.common.product.models.*;
import com.mmhayes.common.purchase_restriction.collections.PurchaseRestrictionProfileCollection;
import com.mmhayes.common.purchase_restriction.models.PurchaseRestrictionProfileDetailModel;
import com.mmhayes.common.transaction.collections.*;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.voucher.models.*;

//API dependencies
import com.mmhayes.common.api.errorhandling.exceptions.*;

//other dependencies
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.mmhayes.common.api.PosApi30.PosAPIHelper.ApiActionType.REFUND_MANUAL;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-08-27 13:16:59 -0400 (Fri, 27 Aug 2021) $: Date of last commit
 $Rev: 15215 $: Revision of last commit
*/
public class TransactionLineBuilder {
    private TransactionModel transactionModel = null;
    private PosAPIModelCache posAPIModelCache = null;
    private Integer terminalId = null;

    //region Constructors

    public TransactionLineBuilder() {

    }

    public TransactionLineBuilder(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
        this.terminalId = transactionModel.getTerminal().getId();
    }

    //endregion

    //region GET Methods

    public static TransactionModel getTransactionLines(ArrayList<HashMap> transactionLinesHM, TransactionModel transactionModel) throws Exception {
        TransactionLineBuilder transactionLineBuilder = new TransactionLineBuilder(transactionModel);

        for (HashMap transactionLineHM : transactionLinesHM) {
            CommonAPI.checkIsNullOrEmptyObject(transactionLineHM.get("ID"), "Transaction Line Item Id cannot be found.", transactionModel.getTerminal().getId());

            PosAPIHelper.ObjectType objectTypeEnum = PosAPIHelper.ObjectType.convertStringToEnum(transactionLineHM.get("PAITEMTYPENAME").toString());

            switch (objectTypeEnum) {
                case PRODUCT:
                    transactionModel.getProducts().add(transactionLineBuilder.setProductModelProperties(transactionLineHM));
                    break;
                case TENDER:
                    transactionModel.getTenders().add(transactionLineBuilder.setTenderModelProperties(transactionLineHM));
                    break;
                case TAX:
                case TAX_DELETE:
                    transactionModel.getTaxes().add(transactionLineBuilder.setTaxModelProperties(transactionLineHM));
                    break;
                case POSDISCOUNT:
                    //this is a POS Discount
                    transactionModel.getDiscounts().add(transactionLineBuilder.setDiscountModelProperties(transactionLineHM));
                    break;
                case PAIDOUT:
                    transactionModel.getPaidOuts().add(transactionLineBuilder.setPaidOutModelProperties(transactionLineHM));
                    break;
                case RECEIVEDONACCOUNT:
                    transactionModel.getReceivedOnAccounts().add(transactionLineBuilder.setReceivedOnAccountProperties(transactionLineHM));
                    break;
                //case QCDISCOUNT:
                //this is included in the pos discounts section
                //break;
                case LOYALTYREWARD:
                    transactionModel.getRewards().add(transactionLineBuilder.setLoyaltyRewardProperties(transactionLineHM));
                    break;
                case SURCHARGE:
                    Boolean createSurcharge = true;
                    if (transactionLineHM.get("SURCHARGENAME") != null && !transactionLineHM.get("SURCHARGENAME").toString().isEmpty()) {

                        //Check for the Surcharge description = "Third Party POS"
                        if (transactionLineHM.get("SURCHARGEDESCRIPTION") != null &&
                                !transactionLineHM.get("SURCHARGEDESCRIPTION").toString().isEmpty() &&
                                transactionLineHM.get("SURCHARGEDESCRIPTION").toString().equalsIgnoreCase("THIRD PARTY POS")) {

                            if (transactionLineHM.get("SURCHARGENAME").toString().equalsIgnoreCase("SERVICE CHARGE")) {
                                transactionModel.getServiceCharges().add(transactionLineBuilder.setServiceChargeModelProperties(transactionLineHM));
                                createSurcharge = false;

                            } else if (transactionLineHM.get("SURCHARGENAME").toString().equalsIgnoreCase("GRATUITY")) {

                                transactionModel.getGratuities().add(transactionLineBuilder.setGratuityModelProperties(transactionLineHM));
                                createSurcharge = false;
                            } else if (transactionLineHM.get("SURCHARGENAME").toString().equalsIgnoreCase("TIP")) {

                                transactionModel.getGratuities().add(transactionLineBuilder.setGratuityModelProperties(transactionLineHM));
                                createSurcharge = false;
                            }
                        }
                    }

                    if (createSurcharge) {
                        transactionModel.getSurcharges().add(transactionLineBuilder.setSurchargeModelProperties(transactionLineHM));
                    }

                    break;
                case COMBO:
                    transactionModel.getCombos().add(transactionLineBuilder.setComboModelProperties(transactionLineHM));
                    break;
                case DONATION:
                    transactionModel.getDonations().add(transactionLineBuilder.setDonationModelProperties(transactionLineHM));
                    break;
            }
        }

        return transactionModel;
    }

    public static TransactionModel getTransactionLinesForCashierEndShift(ArrayList<HashMap> transactionLinesHM, TransactionModel transactionModel) throws Exception {
        TransactionLineBuilder transactionLineBuilder = new TransactionLineBuilder(transactionModel);

        for (HashMap transactionLineHM : transactionLinesHM) {
            //CommonAPI.checkIsNullOrEmptyObject(transactionLineHM.get("ID"), "Transaction Line Item Id cannot be found.");

            PosAPIHelper.ObjectType objectTypeEnum = PosAPIHelper.ObjectType.convertIntToEnum(CommonAPI.convertModelDetailToInteger(transactionLineHM.get("PAITEMTYPEID")));

            switch (objectTypeEnum) {
                case PRODUCT:
                    transactionModel.getProducts().add(transactionLineBuilder.setProductModelProperties(transactionLineHM));
                    break;
                case TENDER:
                    transactionModel.getTenders().add(transactionLineBuilder.setTenderModelProperties(transactionLineHM));
                    break;
                case TAX:
                case TAX_DELETE:
                    transactionModel.getTaxes().add(transactionLineBuilder.setTaxModelProperties(transactionLineHM));
                    break;
                case POSDISCOUNT:
                    //this is a POS Discount
                    transactionModel.getDiscounts().add(transactionLineBuilder.setDiscountModelProperties(transactionLineHM));
                    break;
                case PAIDOUT:
                    transactionModel.getPaidOuts().add(transactionLineBuilder.setPaidOutModelProperties(transactionLineHM));
                    break;
                case RECEIVEDONACCOUNT:
                    transactionModel.getReceivedOnAccounts().add(transactionLineBuilder.setReceivedOnAccountProperties(transactionLineHM));
                    break;
                //case QCDISCOUNT:
                //this is included in the pos discounts section
                //break;
                case LOYALTYREWARD:
                    transactionModel.getRewards().add(transactionLineBuilder.setLoyaltyRewardProperties(transactionLineHM));
                    break;
                case SURCHARGE:
                    transactionModel.getSurcharges().add(transactionLineBuilder.setSurchargeModelProperties(transactionLineHM));
                    break;
                case COMBO:
                    transactionModel.getCombos().add(transactionLineBuilder.setComboModelProperties(transactionLineHM));
                    break;
                case DONATION:
                    transactionModel.getDonations().add(transactionLineBuilder.setDonationModelProperties(transactionLineHM));
                    break;
            }
        }

        return transactionModel;
    }

    private ProductLineItemModel setProductModelProperties(HashMap modelDetailHM) throws Exception {
        ProductLineItemModel productLineItemModel = new ProductLineItemModel();
        productLineItemModel.setModelProperties(modelDetailHM);
        productLineItemModel.setProductModelProperties(modelDetailHM);

        //validate Products
        ProductModel validatedProductModel = this.getPosAPIModelCache().checkProductCache(productLineItemModel.getItemId());
        if (validatedProductModel != null) {
            productLineItemModel.setProduct(validatedProductModel);
        } else {
            validatedProductModel = ProductModel.getProductModelById(productLineItemModel.getItemId(), terminalId);
            productLineItemModel.setProduct(validatedProductModel);
            this.getPosAPIModelCache().getValidatedProductModels().add(validatedProductModel);
        }

        //fetch modifier line items
        productLineItemModel.setModifiers(ModifierLineItemCollection.getAllModifierLineItems(productLineItemModel, terminalId).getCollection());

        for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()) {

            //validate Products
            ProductModel validatedProductModifierModel = this.getPosAPIModelCache().checkProductCache(modifierLineItemModel.getItemId());
            if (validatedProductModifierModel != null) {
                modifierLineItemModel.setProduct(validatedProductModifierModel);
            } else {
                validatedProductModifierModel = ProductModel.getProductModelById(modifierLineItemModel.getItemId(), terminalId);
                modifierLineItemModel.setProduct(validatedProductModifierModel);
                this.getPosAPIModelCache().getValidatedProductModels().add(validatedProductModifierModel);
            }

            //set modifier prep option
            if (modifierLineItemModel.getPrepOption() != null) {

                //validate prep option
                PrepOptionModel validatedPrepOptionModel = this.getPosAPIModelCache().checkPrepOptionCache(modifierLineItemModel.getPrepOption().getId(), modifierLineItemModel.getPrepOption().getPrepOptionSetId());
                if (validatedPrepOptionModel != null) {
                    modifierLineItemModel.setPrepOption(validatedPrepOptionModel);
                } else {
                    validatedPrepOptionModel = PrepOptionModel.getPrepOptionModelByIdAndSet(modifierLineItemModel.getPrepOption().getId(), modifierLineItemModel.getPrepOption().getPrepOptionSetId(), terminalId);
                    modifierLineItemModel.setPrepOption(validatedPrepOptionModel);
                    this.getPosAPIModelCache().getValidatedPrepOptionModels().add(validatedPrepOptionModel);
                }
            }
        }

        productLineItemModel.setPrepOption(PrepOptionModel.getPrepOptionModel(modelDetailHM));

        //set prep option
        if (productLineItemModel.getPrepOption() != null) {

            //validate prep option
            PrepOptionModel validatedPrepOptionModel = this.getPosAPIModelCache().checkPrepOptionCache(productLineItemModel.getPrepOption().getId(), productLineItemModel.getPrepOption().getPrepOptionSetId());
            if (validatedPrepOptionModel != null) {
                productLineItemModel.setPrepOption(validatedPrepOptionModel);
            } else {
                validatedPrepOptionModel = PrepOptionModel.getPrepOptionModelByIdAndSet(productLineItemModel.getPrepOption().getId(), productLineItemModel.getPrepOption().getPrepOptionSetId(), terminalId);
                productLineItemModel.setPrepOption(validatedPrepOptionModel);
                this.getPosAPIModelCache().getValidatedPrepOptionModels().add(validatedPrepOptionModel);
            }
        }

        //fetch tax line items
        productLineItemModel.setTaxes(ItemTaxCollection.getAllItemTaxes(productLineItemModel.getId(), terminalId).getCollection());

        //refresh the tax object if it's missing from the line item
        for (ItemTaxModel taxItemModel : productLineItemModel.getTaxes()) {
            if (taxItemModel.getTax() == null) {
                if (taxItemModel.getItemId() != null && !taxItemModel.getItemId().toString().isEmpty()) {
                    TaxModel taxModel = TaxModel.getTaxModelFromCache(taxItemModel.getItemId(), transactionModel.getTerminal().getId(), false);
                    taxItemModel.setTax(taxModel);
                }
            }
        }

        //fetch discount line items
        productLineItemModel.setDiscounts(ItemDiscountCollection.getAllItemDiscounts(productLineItemModel.getId(), terminalId).getCollection());

        //refresh the discount object if it's missing from the line item
        for (ItemDiscountModel discounItemModel : productLineItemModel.getDiscounts()) {
            if (discounItemModel.getItemId() != null && !discounItemModel.getItemId().toString().isEmpty()) {
                //check to see if we have validated this Discount Model previously
                DiscountModel validatedDiscountModel = this.getPosAPIModelCache().checkDiscountCache(discounItemModel.getItemId());
                if (validatedDiscountModel != null) {
                    discounItemModel.setDiscount(validatedDiscountModel);
                } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                    validatedDiscountModel = DiscountModel.getDiscountModel(discounItemModel.getItemId(), terminalId);
                    discounItemModel.setDiscount(validatedDiscountModel);
                    this.getPosAPIModelCache().getValidatedDiscountModels().add(validatedDiscountModel);
                }
            }
        }

        //fetch discount line items
        productLineItemModel.setLoyaltyPointDetails(ItemLoyaltyPointCollection.getAllItemLoyaltyPointsAndDetailsByTransLineId(productLineItemModel.getId(), terminalId).getCollection());


  /*      //refresh the discount object if it's missing from the line item
        for (ItemLoyaltyPointModel loyaltyPointModel : productLineItemModel.getLoyaltyPointDetails()) {
            if (loyaltyPointModel.getLoyaltyProgramId() == null) {
                if (rewardItemModel.getItemId() != null && !rewardItemModel.getItemId().toString().isEmpty()) {

                    //check to see if we have validated this Discount Model previously
                    LoyaltyRewardModel validatedRewardModel = posAPIModelCache.checkLoyaltyRewardCache(rewardItemModel.getItemId());
                    if (validatedRewardModel != null) {
                        rewardItemModel.setReward(validatedRewardModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        validatedRewardModel = LoyaltyRewardModel.getLoyaltyRewardModel(rewardItemModel.getItemId());
                        rewardItemModel.setReward(validatedRewardModel);
                        posAPIModelCache.getValidatedLoyaltyRewardModels().add(validatedRewardModel);
                    }
                }
            }
        }*/

        //fetch Reward lines
        productLineItemModel.setRewards(ItemRewardCollection.getAllItemRewards(productLineItemModel.getId(), terminalId).getCollection());

        //refresh the Reward object if it's missing from the line item
        for (ItemRewardModel rewardItemModel : productLineItemModel.getRewards()) {
            if (rewardItemModel.getItemId() != null && !rewardItemModel.getItemId().toString().isEmpty()) {
                //check to see if we have validated this Discount Model previously
                LoyaltyRewardModel validatedRewardModel = this.getPosAPIModelCache().checkLoyaltyRewardCache(rewardItemModel.getItemId());
                if (validatedRewardModel != null) {
                    rewardItemModel.setReward(validatedRewardModel);
                } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                    validatedRewardModel = LoyaltyRewardModel.getLoyaltyRewardModel(rewardItemModel.getItemId(), terminalId);
                    rewardItemModel.setReward(validatedRewardModel);
                    this.getPosAPIModelCache().getValidatedLoyaltyRewardModels().add(validatedRewardModel);
                }
            }
        }

        //fetch ItemSurcharges
        productLineItemModel.setSurcharges(ItemSurchargeCollection.getAllItemSurcharges(productLineItemModel.getId(), terminalId).getCollection());

        //refresh the Reward object if it's missing from the line item
        for (ItemSurchargeModel itemSurchargeModel : productLineItemModel.getSurcharges()) {

            if (itemSurchargeModel.getItemId() != null && !itemSurchargeModel.getItemId().toString().isEmpty()) {
                //check to see if we have validated this Discount Model previously
                SurchargeModel validatedSurchargeModel = this.getPosAPIModelCache().checkSurchargeCache(itemSurchargeModel.getItemId());
                if (validatedSurchargeModel != null) {
                    itemSurchargeModel.setSurcharge(SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel));
                } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                    validatedSurchargeModel = SurchargeModel.getSurchargeModel(itemSurchargeModel.getItemId(), transactionModel.getTerminal());
                    itemSurchargeModel.setSurcharge(SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel));
                    this.getPosAPIModelCache().getValidatedSurchargeModels().add(validatedSurchargeModel);
                }
            }
        }

        return productLineItemModel;
    }

    private TenderLineItemModel setTenderModelProperties(HashMap modelDetailHM) throws Exception {
        TenderLineItemModel tenderLineItemModel = new TenderLineItemModel();
        tenderLineItemModel.setModelProperties(modelDetailHM);     //map the core transaction line item fields
        tenderLineItemModel.setTenderModelProperties(modelDetailHM, terminalId);  //map the specific transaction line item fields

        if (tenderLineItemModel.getAccount() == null
                && (tenderLineItemModel.getEmployeeId() != null
                && !tenderLineItemModel.getEmployeeId().toString().isEmpty()
                && !tenderLineItemModel.getEmployeeId().equals(0))) {
            //check to see if we have validated this Account Model previously
            AccountModel validatedAccountModel = this.getPosAPIModelCache().checkAccountCache(tenderLineItemModel.getEmployeeId());
            if (validatedAccountModel != null) {
                tenderLineItemModel.setAccount(validatedAccountModel);
            } else {
                //B-05770: On Transaction fetch, ignore the Account Status and Rev Center Mapping
                //HP-2010: Return Badge Alias if it was used on the original transaction
                AccountQueryParams accountQueryParams = new AccountQueryParams(true, true, tenderLineItemModel.getBadgeAssignmentId());
                validatedAccountModel = AccountModel.getAccountModelByIdForTender(transactionModel.getTerminal(), BigDecimal.ONE, tenderLineItemModel.getEmployeeId(), accountQueryParams);
                tenderLineItemModel.setAccount(validatedAccountModel);
                this.getPosAPIModelCache().getValidatedAccountModels().add(validatedAccountModel);
                transactionModel.checkAndSetLoyaltyAccountBadgeAlias(tenderLineItemModel.getAccount());
            }
        }

        //fetch any QC Transactions that were saved with this tender line item
        tenderLineItemModel.setQcTransaction(QcTransactionModel.createQcTransactionModel(tenderLineItemModel, terminalId));

        //fetch any QC Discount Transactions associated with this tender line item id
        if (tenderLineItemModel.getAccount() != null) {
            tenderLineItemModel.getAccount().setQcDiscountsApplied(QcDiscountLineItemCollection.getAllQcDiscountsByPATransLineItemId(tenderLineItemModel.getId(), terminalId).getCollection());
        }

        //check to see if we have validated this Tax Model previously
        TenderModel validatedTenderModel = this.getPosAPIModelCache().checkTenderCache(tenderLineItemModel.getItemId());
        if (validatedTenderModel != null) {
            tenderLineItemModel.setTender(TenderDisplayModel.createTenderModel(validatedTenderModel));
        } else {
            validatedTenderModel = TenderModel.getTenderModel(tenderLineItemModel.getItemId(), terminalId);
            tenderLineItemModel.setTender(TenderDisplayModel.createTenderModel(validatedTenderModel));
            this.getPosAPIModelCache().getValidatedTenderModels().add(validatedTenderModel);
        }

        return tenderLineItemModel;
    }

    private TaxLineItemModel setTaxModelProperties(HashMap modelDetailHM) throws Exception {
        TaxLineItemModel taxLineItemModel = new TaxLineItemModel();
        taxLineItemModel.setModelProperties(modelDetailHM);

        TaxModel taxModel = TaxModel.getTaxModelFromCache(taxLineItemModel.getItemId(), transactionModel.getTerminal().getId(), false);
        taxLineItemModel.setTax(taxModel);
        return taxLineItemModel;
    }

    private DiscountLineItemModel setDiscountModelProperties(HashMap modelDetailHM) throws Exception {
        DiscountLineItemModel discountLineItemModel = new DiscountLineItemModel();
        discountLineItemModel.setModelProperties(modelDetailHM);

        //check to see if we have validated this Discount Model previously
        DiscountModel validatedDiscountModel = this.getPosAPIModelCache().checkDiscountCache(discountLineItemModel.getItemId());
        if (validatedDiscountModel != null) {
            discountLineItemModel.setDiscount(validatedDiscountModel);
        } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
            validatedDiscountModel = DiscountModel.getDiscountModel(discountLineItemModel.getItemId(), terminalId);
            discountLineItemModel.setDiscount(validatedDiscountModel);
            this.getPosAPIModelCache().getValidatedDiscountModels().add(validatedDiscountModel);
        }

        //fetch tax line items
        discountLineItemModel.setTaxes((ItemTaxCollection.getAllItemTaxes(discountLineItemModel.getId(), terminalId).getCollection()));

        //refresh the tax object if it's missing from the line item
        for (ItemTaxModel taxItemModel : discountLineItemModel.getTaxes()) {
            if (taxItemModel.getTax() == null) {
                if (taxItemModel.getItemId() != null && !taxItemModel.getItemId().toString().isEmpty()) {
                    TaxModel taxModel = TaxModel.getTaxModelFromCache(taxItemModel.getItemId(), transactionModel.getTerminal().getId(), false);
                    taxItemModel.setTax(taxModel);
                }
            }
        }

        return discountLineItemModel;
    }

    private PaidOutLineItemModel setPaidOutModelProperties(HashMap modelDetailHM) throws Exception {
        PaidOutLineItemModel paidOutLineItemModel = new PaidOutLineItemModel();
        paidOutLineItemModel.setPaidOutModelProperties(modelDetailHM, terminalId);

        if (paidOutLineItemModel.getItemTypeId() != null) {
            paidOutLineItemModel.setPaidOut(PaidOutModel.getPaidOutModel(paidOutLineItemModel.getItemId(), terminalId));
        }

        return paidOutLineItemModel;
    }

    private ReceivedOnAccountLineItemModel setReceivedOnAccountProperties(HashMap modelDetailHM) throws Exception {
        ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel = new ReceivedOnAccountLineItemModel();
        receivedOnAccountLineItemModel.setReceivedOnAccountModelProperties(modelDetailHM, terminalId);

        if (receivedOnAccountLineItemModel.getAccount() == null
                && (receivedOnAccountLineItemModel.getEmployeeId() != null
                && !receivedOnAccountLineItemModel.getEmployeeId().toString().isEmpty()
                && !receivedOnAccountLineItemModel.getEmployeeId().equals(0))) {
            //check to see if we have validated this Account Model previously
            AccountModel validatedAccountModel = this.getPosAPIModelCache().checkAccountCache(receivedOnAccountLineItemModel.getEmployeeId());
            if (validatedAccountModel != null) {
                receivedOnAccountLineItemModel.setAccount(validatedAccountModel);
            } else {
                //HP-2010: Return Badge Alias if it was used on the original transaction
                AccountQueryParams accountQueryParams = new AccountQueryParams(true, true, receivedOnAccountLineItemModel.getBadgeAssignmentId());
                validatedAccountModel = AccountModel.getAccountModelByIdForTender(transactionModel.getTerminal(), BigDecimal.ONE, receivedOnAccountLineItemModel.getEmployeeId(), accountQueryParams);
                receivedOnAccountLineItemModel.setAccount(validatedAccountModel);
                this.getPosAPIModelCache().getValidatedAccountModels().add(validatedAccountModel);
            }
        }

        //fetch any QC Transactions that were saved with this ROA line item
        receivedOnAccountLineItemModel.setQcTransaction(QcTransactionModel.createQcTransactionModel(receivedOnAccountLineItemModel, terminalId));

        if (receivedOnAccountLineItemModel.getItemTypeId() != null) {
            receivedOnAccountLineItemModel.setReceivedOnAccount(ReceivedOnAccountModel.getReceivedOnAccountModel(receivedOnAccountLineItemModel.getItemId(), terminalId));
        }

        return receivedOnAccountLineItemModel;
    }

    private LoyaltyRewardLineItemModel setLoyaltyRewardProperties(HashMap modelDetailHM) throws Exception {
        LoyaltyRewardLineItemModel loyaltyRewardLineItemModel = new LoyaltyRewardLineItemModel();
        loyaltyRewardLineItemModel.setModelProperties(modelDetailHM);
        loyaltyRewardLineItemModel.setReward((LoyaltyRewardModel.getLoyaltyRewardModel(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAITEMID")), terminalId)));

        //populate the Loyalty Account Points
        loyaltyRewardLineItemModel.setLoyaltyPoint(LoyaltyPointModel.getOneLoyaltyPointModelByTransLineItemId(loyaltyRewardLineItemModel.getId(), terminalId));

        //populate the Loyalty Point Details
        loyaltyRewardLineItemModel.setLoyaltyPointDetails(ItemLoyaltyPointCollection.getAllItemLoyaltyPointsByTransLineId(loyaltyRewardLineItemModel.getId(), terminalId).getCollection());

        //fetch tax line items
        loyaltyRewardLineItemModel.setTaxes((ItemTaxCollection.getAllItemTaxes(loyaltyRewardLineItemModel.getId(), terminalId).getCollection()));

        //refresh the tax object if it's missing from the line item
        for (ItemTaxModel taxItemModel : loyaltyRewardLineItemModel.getTaxes()) {
            if (taxItemModel.getTax() == null) {
                if (taxItemModel.getItemId() != null && !taxItemModel.getItemId().toString().isEmpty()) {
                    TaxModel taxModel = TaxModel.getTaxModelFromCache(taxItemModel.getItemId(), transactionModel.getTerminal().getId(), false);
                    taxItemModel.setTax(taxModel);
                }
            }
        }

        return loyaltyRewardLineItemModel;
    }

    private ServiceChargeLineItemModel setServiceChargeModelProperties(HashMap modelDetailHM) throws Exception {
        ServiceChargeLineItemModel serviceChargeLineItemModel = new ServiceChargeLineItemModel();
        serviceChargeLineItemModel.setModelProperties(modelDetailHM);

        //validate Products
        SurchargeModel validatedSurchargeModel = this.getPosAPIModelCache().checkSurchargeCache(serviceChargeLineItemModel.getItemId());
        if (validatedSurchargeModel != null) {
            serviceChargeLineItemModel.setServiceCharge(new ServiceChargeModel(validatedSurchargeModel));
        } else {
            validatedSurchargeModel = SurchargeModel.getSurchargeModel(serviceChargeLineItemModel.getItemId(), this.getTransactionModel().getTerminal());
            serviceChargeLineItemModel.setServiceCharge(new ServiceChargeModel(validatedSurchargeModel));
            this.getPosAPIModelCache().getValidatedSurchargeModels().add(validatedSurchargeModel);
        }

        return serviceChargeLineItemModel;
    }

    private GratuityLineItemModel setGratuityModelProperties(HashMap modelDetailHM) throws Exception {
        GratuityLineItemModel gratuityLineItemModel = new GratuityLineItemModel();
        gratuityLineItemModel.setModelProperties(modelDetailHM);

        //validate Products
        SurchargeModel validatedSurchargeModel = this.getPosAPIModelCache().checkSurchargeCache(gratuityLineItemModel.getItemId());
        if (validatedSurchargeModel != null) {
            gratuityLineItemModel.setGratuity(new GratuityModel(validatedSurchargeModel));
        } else {
            validatedSurchargeModel = SurchargeModel.getSurchargeModel(gratuityLineItemModel.getItemId(), this.getTransactionModel().getTerminal());
            gratuityLineItemModel.setGratuity(new GratuityModel(validatedSurchargeModel));
            this.getPosAPIModelCache().getValidatedSurchargeModels().add(validatedSurchargeModel);
        }

        return gratuityLineItemModel;
    }

    private SurchargeLineItemModel setSurchargeModelProperties(HashMap modelDetailHM) throws Exception {
        SurchargeLineItemModel surchargeLineItemModel = new SurchargeLineItemModel();
        surchargeLineItemModel.setModelProperties(modelDetailHM);

        //validate Surcharge
        SurchargeModel validatedSurchargeModel = this.getPosAPIModelCache().checkSurchargeCache(surchargeLineItemModel.getItemId());
        if (validatedSurchargeModel != null) {
            surchargeLineItemModel.setSurcharge(SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel));
        } else {
            validatedSurchargeModel = SurchargeModel.getSurchargeModel(surchargeLineItemModel.getItemId(), transactionModel.getTerminal());
            surchargeLineItemModel.setSurcharge(SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel));
            this.getPosAPIModelCache().getValidatedSurchargeModels().add(validatedSurchargeModel);
        }

        return surchargeLineItemModel;
    }

    private ComboLineItemModel setComboModelProperties(HashMap modelDetailHM) throws Exception {
        ComboLineItemModel comboLineItemModel = new ComboLineItemModel();
        comboLineItemModel.setModelProperties(modelDetailHM);

        //validate Combo
        ComboModel validatedComboModel = this.getPosAPIModelCache().checkComboCache(comboLineItemModel.getItemId());
        if (validatedComboModel != null) {
            comboLineItemModel.setCombo(ComboModel.createComboModel(validatedComboModel));
        } else {
            validatedComboModel = ComboModel.getComboModel(comboLineItemModel.getItemId(), transactionModel.getTerminal());
            comboLineItemModel.setCombo(ComboModel.createComboModel(validatedComboModel));
            this.getPosAPIModelCache().getValidatedComboModels().add(validatedComboModel);
        }

        return comboLineItemModel;
    }

    private DonationLineItemModel setDonationModelProperties(HashMap modelDetailHM) throws Exception {
        DonationLineItemModel donationLineItemModel = new DonationLineItemModel();
        donationLineItemModel.setModelProperties(modelDetailHM);

        //validate Donation
        DonationModel validatedDonationModel = this.getPosAPIModelCache().checkDonationCache(donationLineItemModel.getItemId());
        if (validatedDonationModel != null) {
            donationLineItemModel.setDonation(validatedDonationModel);
        } else {
            validatedDonationModel = DonationModel.getOneDonationModelById(donationLineItemModel.getItemId(), transactionModel.getTerminal().getId());
            donationLineItemModel.setDonation(validatedDonationModel);
            this.getPosAPIModelCache().getValidatedDonationModels().add(validatedDonationModel);
        }

        return donationLineItemModel;
    }

//endregion

    //region Common Validation Methods

    public static TransactionModel validateTransactionLines(TransactionModel transactionModel) throws Exception {
        TransactionLineBuilder transactionLineBuilder = new TransactionLineBuilder(transactionModel);

        //build products (add missing fields)
        if (transactionModel != null) {

            //tenders
            transactionLineBuilder.validateTenderLineItems();

            //For Third party login, check the DSKey from the database for Employee information
            transactionLineBuilder.validateEmployeeIdOnDSKey();

            //validate Tender Accounts
            transactionLineBuilder.validateTenderAccounts();

            //Loyalty Account and Tender Accounts
            transactionLineBuilder.validateLoyaltyAccount();

            //For Third part login, validate EmployeeID on Loyalty Account and Quickcharge Account
            transactionLineBuilder.validateSubmittedEmployeeIdToDSKey();

            //products
            transactionLineBuilder.validateProductLineItems();

            //item purchase restriction
            transactionLineBuilder.validatePurchaseItemRestrictions();

            //taxes
            transactionLineBuilder.validateTaxLineItems();

            /*//tenders  12/22/2017 egl - moved this to the first to get validated.  Therefore we can tell the Tender type when we check the DSKey for an Employee ID
            transactionLineBuilder.validateTenderLineItems();*/

            //POS Discounts
            transactionLineBuilder.validatePosDiscountLineItems();

            //Paid Outs
            transactionLineBuilder.validatePaidOutLineItems();

            //Received On Accounts
            transactionLineBuilder.validateReceivedOnAccountLineItems();

            //Qc Discounts
            //transactionLineBuilder.validateQcDiscountLineItems();

            //Loyalty Rewards
            transactionLineBuilder.validateLoyaltyRewardLineItems();

            transactionLineBuilder.validateServiceChargeLineItems();

            transactionLineBuilder.validateGratuityLineItems();

            transactionLineBuilder.validateSurchargeLineItems();

            transactionLineBuilder.validateComboLineItems();

            transactionLineBuilder.validateDonationLineItems();

            transactionLineBuilder.validateQcDiscounts();

            transactionLineBuilder.mapProductItemProfiles();

            transactionLineBuilder.getTransactionModel().validateAndSetTransactionItemNumbers();

        }

        return transactionModel;
    }

    /**
     * Validate Transaction Lines
     * Fetch Transaction Line objects, i.e.- fetch the TenderModel, SurchargeModel, etc. information from the db
     * Don't validate Account information
     * @param transactionModel
     * @return
     * @throws Exception
     */
    public static TransactionModel validateTransactionLinesBasic(TransactionModel transactionModel) throws Exception {
        TransactionLineBuilder transactionLineBuilder = new TransactionLineBuilder(transactionModel);

        //build products (add missing fields)
        if (transactionModel != null) {

            //tenders
            transactionLineBuilder.validateTenderLineItems();

            //products
            transactionLineBuilder.validateProductLineItems();

            //taxes
            transactionLineBuilder.validateTaxLineItems();

            //POS Discounts
            transactionLineBuilder.validatePosDiscountLineItems();

            //Paid Outs
            transactionLineBuilder.validatePaidOutLineItems();

            //Received On Accounts
            transactionLineBuilder.validateReceivedOnAccountLineItems();

            //Loyalty Rewards
            transactionLineBuilder.validateLoyaltyRewardLineItems();

            transactionLineBuilder.validateServiceChargeLineItems();

            transactionLineBuilder.validateGratuityLineItems();

            transactionLineBuilder.validateSurchargeLineItems();

            transactionLineBuilder.validateQcDiscounts();

        }

        return transactionModel;
    }

    private void validateLoyaltyAccount() throws Exception {

        switch (transactionModel.getApiActionTypeEnum()) {
           /* case ROA:
                //don't validate the Loyalty Account on an ROA transaction
                break;
                This was set by HP816 which broke ROA's through QCPos
                HP1256 - We now validate Loyalty Accounts for all transaction types
*/
            default: //validate the Loyalty Account by default
                AccountFieldAvailable accountFieldAvailable = AccountFieldAvailable.NONE;

                //validate the Loyalty Account
                //check here to see if we have the Employee ID, Number, or Badge for the Loyalty Account
                if (this.getTransactionModel().getLoyaltyAccount() != null && this.getTransactionModel().getHasValidatedLoyaltyAccount() == false) {

                    if (this.getTransactionModel().getLoyaltyAccount().getId() != null && !this.getTransactionModel().getLoyaltyAccount().getId().toString().isEmpty() && !this.getTransactionModel().getLoyaltyAccount().getId().equals(0)) {
                        accountFieldAvailable = AccountFieldAvailable.ID;
                    } else if (this.getTransactionModel().getLoyaltyAccount().getNumber() != null && !this.getTransactionModel().getLoyaltyAccount().getNumber().isEmpty() && !this.getTransactionModel().getLoyaltyAccount().getNumber().equals("0")) {
                        accountFieldAvailable = AccountFieldAvailable.NUMBER;
                    } else if (this.getTransactionModel().getLoyaltyAccount().getBadge() != null && !this.getTransactionModel().getLoyaltyAccount().getBadge().isEmpty() && !this.getTransactionModel().getLoyaltyAccount().getBadge().equals("0")) {
                        accountFieldAvailable = AccountFieldAvailable.BADGE;
                    } else {
                        throw new MissingDataException("Loyalty Account Not Found.", terminalId);
                    }

                    LoyaltyAccountModel loyaltyAccountModel = null;
                    AccountModel accountModel = null;

                    AccountQueryParams accountQueryParams = new AccountQueryParams();
                    if (this.getTransactionModel().getAccountQueryParams() != null){
                        accountQueryParams = this.getTransactionModel().getAccountQueryParams(); //override Account fetch settings
                    }

                    switch (accountFieldAvailable) {
                        case ID:
                            accountModel = this.getPosAPIModelCache().checkAccountCache(this.getTransactionModel().getLoyaltyAccount().getId());
                            if (accountModel == null) {
                                accountModel = AccountModel.getAccountModelByIdForTender(transactionModel.getTerminal(), BigDecimal.ZERO, this.getTransactionModel().getLoyaltyAccount().getId(), accountQueryParams);
                                this.getPosAPIModelCache().getValidatedAccountModels().add(accountModel);
                            }
                            accountModel.setTerminal(this.getTransactionModel().getTerminal());
                            break;
                        case NUMBER:
                            accountModel = this.getPosAPIModelCache().checkAccountCache(this.getTransactionModel().getLoyaltyAccount().getNumber(), true);
                            if (accountModel == null) {
                                accountModel = AccountModel.getAccountModelByNumberForTender(transactionModel.getTerminal(), BigDecimal.ZERO, this.getTransactionModel().getLoyaltyAccount().getNumber(), accountQueryParams);
                                this.getPosAPIModelCache().getValidatedAccountModels().add(accountModel);
                            }
                            accountModel.setTerminal(this.getTransactionModel().getTerminal());
                            break;
                        case BADGE:
                            accountModel = this.getPosAPIModelCache().checkAccountCache(this.getTransactionModel().getLoyaltyAccount().getBadge());
                            if (accountModel == null) {
                                accountModel = AccountModel.getAccountModelByBadgeForTender(transactionModel.getTerminal(), BigDecimal.ZERO, this.getTransactionModel().getLoyaltyAccount().getBadge(), accountQueryParams);
                                this.getPosAPIModelCache().getValidatedAccountModels().add(accountModel);
                            }
                            accountModel.setTerminal(this.getTransactionModel().getTerminal());
                            break;
                    }

                    loyaltyAccountModel = new LoyaltyAccountModel(accountModel, true, transactionModel);

                    this.getTransactionModel().setHasValidatedLoyaltyAccount(true);
                    this.getTransactionModel().setLoyaltyAccount(loyaltyAccountModel);

                    //set the note on each reward if the terminal is in offline mode
                    this.getTransactionModel().getLoyaltyAccount().setRewardsAvailableNotesIfOffline(this.getTransactionModel().isOfflineMode());
                }

                break;
        }
    }

    private void validateTenderAccounts() throws Exception {
        AccountFieldAvailable accountFieldAvailable = AccountFieldAvailable.NONE;

        Integer iFirstRecord = 1;

        //accounts on tender lines
        for (TenderLineItemModel tenderLineModel : this.getTransactionModel().getTenders()) {

            //check if the account is included on QC Tender
            if ((tenderLineModel.getTender() != null &&
                    tenderLineModel.getTender().getTenderTypeId() != null) &&
                    tenderLineModel.getTender().getTenderTypeId().equals(PosAPIHelper.TenderType.QUICKCHARGE.toInt())) {
                if (tenderLineModel.getAccount() == null) {
                    throw new MissingDataException("Invalid Tender submitted. Missing Tender Details", terminalId);
                }
            }

            if (tenderLineModel.getAccount() == null) {
                continue;
            }

            accountFieldAvailable = AccountFieldAvailable.NONE;

            //check here to see if we have the Employee ID, Number, or Badge for the Tender Account
            if ((tenderLineModel.getAccount().getId() != null && !tenderLineModel.getAccount().getId().toString().isEmpty()) && !tenderLineModel.getAccount().getId().equals(0)) {
                accountFieldAvailable = AccountFieldAvailable.ID;
            } else if (tenderLineModel.getAccount().getNumber() != null && !tenderLineModel.getAccount().getNumber().isEmpty() && !tenderLineModel.getAccount().getNumber().equals(0)) {
                accountFieldAvailable = AccountFieldAvailable.NUMBER;
            } else if (tenderLineModel.getAccount().getBadge() != null && !tenderLineModel.getAccount().getBadge().isEmpty()) {
                accountFieldAvailable = AccountFieldAvailable.BADGE;
            } else {
                throw new MissingDataException("Account cannot be found.", terminalId);
            }

            AccountModel accountModel = null;

            //preserve the QcDiscountsApplied and add them back after the AccountModel query
            List<QcDiscountLineItemModel> qcDiscountsApplied = new ArrayList();
            if (tenderLineModel.getAccount() != null && tenderLineModel.getAccount().getQcDiscountsApplied() != null && !tenderLineModel.getAccount().getQcDiscountsApplied().isEmpty()) {
                qcDiscountsApplied = tenderLineModel.getAccount().getQcDiscountsApplied();
            }

            AccountQueryParams accountQueryParams = configureAccountQueryParams();

            switch (accountFieldAvailable) {
                case ID:
                    accountModel = this.getPosAPIModelCache().checkAccountCache(tenderLineModel.getAccount().getId());
                    if (accountModel == null) {
                        //HP-2010: Return Badge Alias if it was used on the original transaction
                        accountQueryParams.setBadgeAssignmentId(tenderLineModel.getBadgeAssignmentId());
                        accountModel = AccountModel.getAccountModelByIdForTender(transactionModel.getTerminal(), tenderLineModel.getAmount(), tenderLineModel.getAccount().getId(), accountQueryParams);
                        this.getPosAPIModelCache().getValidatedAccountModels().add(accountModel);
                    }
                    break;
                case NUMBER:
                    accountModel = this.getPosAPIModelCache().checkAccountCache(tenderLineModel.getAccount().getNumber(), true);
                    if (accountModel == null) {
                        accountModel = AccountModel.getAccountModelByNumberForTender(transactionModel.getTerminal(), tenderLineModel.getAmount(), tenderLineModel.getAccount().getNumber(), accountQueryParams);
                        this.getPosAPIModelCache().getValidatedAccountModels().add(accountModel);
                    }
                    break;
                case BADGE:
                    accountModel = this.getPosAPIModelCache().checkAccountCache(tenderLineModel.getAccount().getBadge());
                    if (accountModel == null) {
                        accountModel = AccountModel.getAccountModelByBadgeForTender(transactionModel.getTerminal(), tenderLineModel.getAmount(), tenderLineModel.getAccount().getBadge(), accountQueryParams);
                        this.getPosAPIModelCache().getValidatedAccountModels().add(accountModel);
                    }
                    break;
            }

            accountModel.setQcDiscountsApplied(qcDiscountsApplied);

            //populate the Badge Assignment ID if it is missing on the tenderLineItemModel
            if (accountModel != null) {
                if (accountModel.getBadgeAssignmentId() != null
                        && tenderLineModel.getBadgeAssignmentId() == null) {
                    tenderLineModel.setBadgeAssignmentId(accountModel.getBadgeAssignmentId()); //set the Badge Assignment ID
                }

                //populate the Employee ID if it's missing on the tenderLineItemModel
                if (accountModel.getId() != null) {
                    tenderLineModel.setEmployeeId(accountModel.getId());
                }
            }

            //check if there is a loyalty account model
            if (iFirstRecord == 1) {
                if (this.getTransactionModel().getLoyaltyAccount() == null) {
                    accountModel.setTerminal(this.getTransactionModel().getTerminal());
                    this.getTransactionModel().setLoyaltyAccount(new LoyaltyAccountModel(accountModel, true, transactionModel));
                    this.getTransactionModel().getLoyaltyAccount().setQcDiscountsAvailable(new ArrayList<QcDiscountModel>());
                    //For transaction inquiries and sales, don't return the qcDiscountsAvailable
                    this.getTransactionModel().setHasValidatedLoyaltyAccount(true);
                }
            }

            //Enforce the Restrict By Account Groups setting on the Tender
            switch (this.getTransactionModel().getPosType()) {
                case THIRD_PARTY:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.LOYALTY_ADJUSTMENT.toInt()
                            && tenderLineModel.getTender().isRestrictByAccountGroup()
                            && tenderLineModel.getTender().getTenderAccountGroupMappings() != null && !tenderLineModel.getTender().getTenderAccountGroupMappings().isEmpty()) {
                        if (accountModel.getPayrollGroupingId() != null) {
                            if (!tenderLineModel.getTender().getTenderAccountGroupMappings().contains(accountModel.getPayrollGroupingId().toString())) {
                                throw new MissingDataException("Invalid Tender submitted for this account group");
                            }
                        }
                    }
                    break;

                default:
                    //This setting is enforced on the front of QCPOS
                    break;
            }

            tenderLineModel.setAccount(accountModel);
            iFirstRecord++;
        }
    }

    private void validatePurchaseItemRestrictions() throws Exception {
        //check the Terminal Type, make sure it's MMHayes POS or Online Ordering
        switch (transactionModel.getPosType()) {
            case ONLINE_ORDERING:
            case KIOSK_ORDERING:
            case MMHAYES_POS:
                switch (transactionModel.getTransactionTypeEnum()) {
                    case SALE:
                        validatePurchaseItemRestrictionsByTender();
                        break;
                 }
                break;
            default:
                break;
        }
    }

    private void validatePurchaseItemRestrictionsByTender() throws Exception {
        //only check for restrictions when building the transactions on submit
        if( transactionModel.isInquiry() ) {
            return;
        }

        if (hasNonQcTender())
        {
            Logger.logMessage("Non QC tender found, Purchase Restrictions will not be enforced.  TransactionLineBuilder.validatePurchaseItemRestrictionsByTender.", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.DEBUG);
            return;
        }
        else if (transactionModel.getTenders().isEmpty())
        {
            Logger.logMessage("No tenders found, Purchase Restrictions will not be enforced.  TransactionLineBuilder.validatePurchaseItemRestrictionsByTender.", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.DEBUG);
            return;
        }

        List<String> ineligibleProducts = new ArrayList<>();
        int  numValidTenders = 0;

        for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()) {
            boolean allProductsValid = true;

            //only check for purchase restrictions if using Quickcharge Tender
            if (tenderLineItemModel.getTender() != null && tenderLineItemModel.getTender().getTenderTypeId().equals(PosAPIHelper.TenderType.QUICKCHARGE.toInt())) {

                Logger.logMessage("Validating purchase item restrictions.  QCPosTransactionLineBuilder.validatePurchaseItemRestrictions.", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.DEBUG);

                //get the purchase restrictions based on the terminal and user's spending profile
                PurchaseRestrictionProfileCollection purchaseRestrictionProfileCollection = PurchaseRestrictionProfileCollection.getPurchaseRestrictionCollectionByEmployeeAndTerminalID(tenderLineItemModel.getAccount().getId(), transactionModel.getTerminal().getId());

                if(purchaseRestrictionProfileCollection.getCollection().size() == 0) {
                    return;
                }

                //loop over each product
                for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {

                    if (ProductLineItemModel.getAdjValOfExtendedAmountForPurchaseRestrictions(productLineItemModel).compareTo(BigDecimal.ZERO) <= 0){
                        continue; //don't enforce Purchase Restrictions for a product with a $0 extended amount
                    }

                    for(PurchaseRestrictionProfileDetailModel purchaseRestriction: purchaseRestrictionProfileCollection.getCollection()) {

                        //restriction on product
                        if(purchaseRestriction.getPurchaseRestrictionType() == 1 && purchaseRestriction.getPAPluID() != null && purchaseRestriction.getPAPluID().equals(productLineItemModel.getProduct().getId())) {
                            if (!ineligibleProducts.contains(productLineItemModel.getProduct().getName())){
                                ineligibleProducts.add(productLineItemModel.getProduct().getName());
                            }
                            allProductsValid = false;

                            break;

                            //restriction on sub department
                        } else if(purchaseRestriction.getPurchaseRestrictionType() == 2 && purchaseRestriction.getPASubDeptID() != null && purchaseRestriction.getPASubDeptID().equals(productLineItemModel.getProduct().getSubDepartmentId())) {
                            if (!ineligibleProducts.contains(productLineItemModel.getProduct().getName())){
                                ineligibleProducts.add(productLineItemModel.getProduct().getName());
                            }
                            allProductsValid = false;

                            break;

                            //restriction on department
                        } else if(purchaseRestriction.getPurchaseRestrictionType() == 3 && purchaseRestriction.getPADepartmentID() != null && purchaseRestriction.getPADepartmentID().equals(productLineItemModel.getProduct().getDepartmentId())) {
                            if (!ineligibleProducts.contains(productLineItemModel.getProduct().getName())){
                                ineligibleProducts.add(productLineItemModel.getProduct().getName());
                            }
                            allProductsValid = false;

                            break;
                        }
                    }
                }

                //if all products are valid, this tender is valid
                if (allProductsValid){
                    numValidTenders++;
                }
            }
        }

        //if zero valid tenders, throw Purchase Restriction Exception
        if (numValidTenders == 0) {
            String productNames = String.join(", ", ineligibleProducts);  //join the names together for error displayed to user
            throw new PurchaseRestrictionException("Purchase Restriction on " + productNames + ".", terminalId);
        }
    }

    /**
     * Check for any Non QC Tenders.
     * @return
     */
    private boolean hasNonQcTender(){
        for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()) {
            if (tenderLineItemModel.getTender() != null && !tenderLineItemModel.getTender().getTenderTypeId().equals(PosAPIHelper.TenderType.QUICKCHARGE.toInt())) {
                return true;
            }
        }

        return false;
    }

    //Product Lines
    private void validateProductLineItems() throws Exception {

        Logger.logMessage("Validating products.  QCPosTransactionLineBuilder.validateProductLineItems.", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.DEBUG);
        ProductCollection productCollection = new ProductCollection(this.getTransactionModel());

        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {

            if (productLineItemModel.getAmount() == null || productLineItemModel.getAmount().toString().isEmpty()) {
                throw new MissingDataException("Missing Amount on Product Line Item", terminalId);
            }

            switch (transactionModel.getTransactionTypeEnum()) {
                case SALE:

                    //check for negative amount
                    if (productLineItemModel.getAmount() != null &&
                            !productLineItemModel.getAmount().toString().isEmpty() &&
                            productLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == -1) {
                        throw new MissingDataException("Invalid Amount on Product Line Item", terminalId);
                    }

                    if (productLineItemModel.getQuantity() != null &&
                            !productLineItemModel.getQuantity().toString().isEmpty() &&
                            (productLineItemModel.getQuantity().compareTo(BigDecimal.ZERO) == -1 || productLineItemModel.getQuantity().compareTo(BigDecimal.ZERO) == 0)) {
                        throw new MissingDataException("Invalid Quantity on Product Line Item", terminalId);
                    }

                    break;
                case REFUND:
                case VOID:

                    /*if (!transactionModel.isInquiry()) {
                        //check for negative amount
                        if (productLineItemModel.getAmount() != null &&
                                !productLineItemModel.getAmount().toString().isEmpty() &&
                                productLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == -1) {
                            throw new MissingDataException("Invalid Amount on Product Line Item");
                        }

                        if (productLineItemModel.getQuantity() != null &&
                                !productLineItemModel.getQuantity().toString().isEmpty() &&
                                (productLineItemModel.getQuantity().compareTo(BigDecimal.ZERO) == -1 || productLineItemModel.getQuantity().compareTo(BigDecimal.ZERO) == 0)) {
                            throw new MissingDataException("Invalid Quantity on Product Line Item");
                        }
                    }*/

                    break;
            }

            //Validate Product Models
            if (productLineItemModel.getProduct() != null) {

                if ((productLineItemModel.getProduct().getId() == null || productLineItemModel.getProduct().getId().equals(0))
                        && (productLineItemModel.getProduct().getProductCode() == null || productLineItemModel.getProduct().getProductCode().isEmpty())
                        && (productLineItemModel.getProduct().getName() == null || productLineItemModel.getProduct().getName().isEmpty())
                        && (productLineItemModel.getProduct().getSubDepartment() == null || productLineItemModel.getProduct().getSubDepartment().isEmpty())
                        && (productLineItemModel.getProduct().getDepartment() == null || productLineItemModel.getProduct().getDepartment().isEmpty())) {
                    throw new MissingDataException("Missing or Invalid Product Information", terminalId);
                }

                //id
                if (productLineItemModel.getProduct().getId() != null
                        && !productLineItemModel.getProduct().getId().toString().isEmpty() && !productLineItemModel.getProduct().getId().equals(0)) {
                    ProductModel validatedProductModel = this.getPosAPIModelCache().checkProductCache(productLineItemModel.getProduct().getId());
                    if (validatedProductModel != null) {
                        productLineItemModel.setProduct(validatedProductModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        switch (transactionModel.getPosType()) {
                            case ONLINE_ORDERING:
                            case KIOSK_ORDERING:
                            case MMHAYES_POS:
                            case THIRD_PARTY:
                                validatedProductModel = ProductModel.getActiveProductModelById(productLineItemModel.getProduct().getId(), transactionModel.getTerminal());
                                break;
                            case MY_QC_FUNDING:
                                validatedProductModel = ProductModel.getActiveProductModelByIdWithoutTerminal(productLineItemModel.getProduct().getId(), transactionModel.getTerminal());
                                break;
                        }
                        productLineItemModel.setProduct(validatedProductModel);
                        this.getPosAPIModelCache().getValidatedProductModels().add(validatedProductModel);
                    } //product code
                } else if (productLineItemModel.getProduct().getProductCode() != null
                        && !productLineItemModel.getProduct().getProductCode().toString().isEmpty()) {
                    ProductModel validatedProductModel = this.getPosAPIModelCache().checkProductPluCache(productLineItemModel.getProduct().getProductCode());
                    if (validatedProductModel != null) {
                        productLineItemModel.setProduct(validatedProductModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        validatedProductModel = ProductModel.getActiveProductModelByPluCode(productLineItemModel.getProduct().getProductCode(), transactionModel.getTerminal());
                        productLineItemModel.setProduct(validatedProductModel);
                        this.getPosAPIModelCache().getValidatedProductModels().add(validatedProductModel);
                    }  //product name
                } else if (productLineItemModel.getProduct().getName() != null
                        && !productLineItemModel.getProduct().getName().isEmpty()) {
                    ProductModel validatedProductModel = this.getPosAPIModelCache().checkProductCache(productLineItemModel.getProduct().getName());
                    if (validatedProductModel != null) {
                        productLineItemModel.setProduct(validatedProductModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        validatedProductModel = ProductModel.getActiveProductModelByName(productLineItemModel.getProduct().getName(), transactionModel.getTerminal());
                        productLineItemModel.setProduct(validatedProductModel);
                        this.getPosAPIModelCache().getValidatedProductModels().add(validatedProductModel);
                    }
                } else if (productLineItemModel.getProduct().getSubDepartment() != null
                        && !productLineItemModel.getProduct().getSubDepartment().isEmpty()) {
                    productLineItemModel.setOriginallyJustSubDepartment(true);
                    ProductModel validatedProductModel = this.getPosAPIModelCache().checkProductSubDepartmentCache(productLineItemModel.getProduct().getSubDepartment());
                    if (validatedProductModel != null) {
                        productLineItemModel.setProduct(validatedProductModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        validatedProductModel = ProductModel.getActiveProductModelBySubDepartmentName(productLineItemModel.getProduct().getSubDepartment(), transactionModel.getTerminal());
                        productLineItemModel.setProduct(validatedProductModel);
                        this.getPosAPIModelCache().getValidatedProductModels().add(validatedProductModel);
                    }
                } else if (productLineItemModel.getProduct().getDepartment() != null
                        && !productLineItemModel.getProduct().getDepartment().isEmpty()) {
                    productLineItemModel.setOriginallyJustDepartment(true);
                    ProductModel validatedProductModel = this.getPosAPIModelCache().checkProductDepartmentCache(productLineItemModel.getProduct().getDepartment());
                    if (validatedProductModel != null) {
                        productLineItemModel.setProduct(validatedProductModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        validatedProductModel = ProductModel.getActiveProductModelByDepartmentName(productLineItemModel.getProduct().getDepartment(), transactionModel.getTerminal());
                        productLineItemModel.setProduct(validatedProductModel);
                        this.getPosAPIModelCache().getValidatedProductModels().add(validatedProductModel);
                    }
                }
            } else {
                throw new MissingDataException("Product information Missing", terminalId);
            }

            //validate taxes on product line items
            for (ItemTaxModel itemTaxModel : productLineItemModel.getTaxes()) {
                if (itemTaxModel.getTax() != null) {
                    if (itemTaxModel.getTax().getId() != null) {
                        TaxModel taxModel = TaxModel.getTaxModelFromCache(itemTaxModel.getTax().getId(), transactionModel.getTerminal().getId(), true);
                        itemTaxModel.setTax(taxModel);
                        if (taxModel != null && taxModel.getId() != null) {
                            itemTaxModel.setItemId(taxModel.getId());
                        }
                    } else if (itemTaxModel.getTax().getName() != null) {
                        //check to see if we have validated this Tax Model previously
                        TaxModel validatedTaxModel = this.getPosAPIModelCache().checkTaxCache(itemTaxModel.getTax().getName());
                        if (validatedTaxModel != null) {
                            itemTaxModel.setTax(validatedTaxModel);
                        } else {
                            validatedTaxModel = TaxModel.getTaxModelWithRevenueCenter(itemTaxModel.getTax().getName(), transactionModel.getTerminal().getRevenueCenterId(), terminalId);
                            itemTaxModel.setTax(validatedTaxModel);
                            this.getPosAPIModelCache().getValidatedTaxModels().add(validatedTaxModel);
                        }
                        if (validatedTaxModel != null && validatedTaxModel.getId() != null) {
                            itemTaxModel.setItemId(validatedTaxModel.getId());
                        }
                    }
                }

                if (itemTaxModel.getAmount() != null && itemTaxModel.getAmount().compareTo(BigDecimal.ZERO) != 0) {
                    itemTaxModel.setAmount(itemTaxModel.getAmount().setScale(4, RoundingMode.HALF_UP));
                }

            }

            //validate discounts on product line items
            for (ItemDiscountModel discountItemModel : productLineItemModel.getDiscounts()) {
                if (discountItemModel.getDiscount() != null) {
                    if (discountItemModel.getDiscount().getId() != null) {
                        DiscountModel validatedDiscountModel = this.getPosAPIModelCache().checkDiscountCache(discountItemModel.getDiscount().getId());
                        if (validatedDiscountModel != null) {
                            discountItemModel.setDiscount(validatedDiscountModel);
                        } else {
                            validatedDiscountModel = DiscountModel.getDiscountModelWithRevenueCenter(discountItemModel.getDiscount().getId(), transactionModel.getTerminal().getRevenueCenterId(), terminalId);
                            discountItemModel.setDiscount(validatedDiscountModel);
                            this.getPosAPIModelCache().getValidatedDiscountModels().add(validatedDiscountModel);
                        }
                        if (validatedDiscountModel != null && validatedDiscountModel.getId() != null) {
                            discountItemModel.setItemId(validatedDiscountModel.getId());
                        }
                    } else if (discountItemModel.getDiscount().getName() != null) {
                        DiscountModel validatedDiscountModel = this.getPosAPIModelCache().checkDiscountCache(discountItemModel.getDiscount().getName());
                        if (validatedDiscountModel != null) {
                            discountItemModel.setDiscount(validatedDiscountModel);
                        } else {
                            validatedDiscountModel = DiscountModel.getDiscountModelWithRevenueCenter(discountItemModel.getDiscount().getName(), transactionModel.getTerminal().getRevenueCenterId(), terminalId);
                            discountItemModel.setDiscount(validatedDiscountModel);
                            this.getPosAPIModelCache().getValidatedDiscountModels().add(validatedDiscountModel);
                        }
                        if (validatedDiscountModel != null && validatedDiscountModel.getId() != null) {
                            discountItemModel.setItemId(validatedDiscountModel.getId());
                        }
                    } else {
                        throw new MissingDataException("Invalid POS Discount information submitted.", terminalId);
                    }
                }

                if (discountItemModel.getAmount() != null && discountItemModel.getAmount().compareTo(BigDecimal.ZERO) != 0) {
                    discountItemModel.setAmount(discountItemModel.getAmount().setScale(4, RoundingMode.HALF_UP));
                }

                //if the Item Discount Amount is positive and the transaction is a Sale then flip the sign
                switch (transactionModel.getApiActionTypeEnum()) {
                    case SALE:
                        if (discountItemModel.getAmount().compareTo(BigDecimal.ZERO) == 1) {
                            discountItemModel.setAmount(discountItemModel.getAmount().negate());
                            //discountItemModel.setExtendedAmount(discountLineItemModel.getAmount().multiply(discountLineItemModel.getQuantity()));
                        }
                        break;
                }
            }

            for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {

                //if the reward is positive and the transaction is a Sale
                switch (transactionModel.getTransactionTypeEnum()) {
                    case SALE:
                        if (itemRewardModel.getAmount().compareTo(BigDecimal.ZERO) == 1) {
                            itemRewardModel.setAmount(itemRewardModel.getAmount().multiply(new BigDecimal(-1)));
                            //itemRewardModel.itemRewardModel(itemRewardModel.getAmount().multiply(itemRewardModel.getQuantity()));
                        }
                        break;
                }

                //validate RewardModel
                if (itemRewardModel.getReward().getId() != null
                        && !itemRewardModel.getReward().getId().toString().isEmpty()) {
                    LoyaltyRewardModel validatedRewardModel = this.getPosAPIModelCache().checkLoyaltyRewardCache(itemRewardModel.getReward().getId());
                    if (validatedRewardModel != null) {
                        itemRewardModel.setReward(validatedRewardModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        validatedRewardModel = LoyaltyRewardModel.getLoyaltyRewardModel(itemRewardModel.getReward().getId(), this.getTransactionModel().getTerminal().getId());
                        itemRewardModel.setReward(validatedRewardModel);
                        this.getPosAPIModelCache().getValidatedLoyaltyRewardModels().add(validatedRewardModel);
                    }
                    if (validatedRewardModel != null && validatedRewardModel.getId() != null) {
                        itemRewardModel.setItemId(validatedRewardModel.getId());
                    }
                } else if (itemRewardModel.getReward().getName() != null
                        && !itemRewardModel.getReward().getName().isEmpty()) {
                    LoyaltyRewardModel validatedRewardModel = this.getPosAPIModelCache().checkLoyaltyRewardCache(itemRewardModel.getReward().getName());
                    if (validatedRewardModel != null) {
                        itemRewardModel.setReward(validatedRewardModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        validatedRewardModel = LoyaltyRewardModel.getLoyaltyRewardModel(itemRewardModel.getReward().getName(), this.getTransactionModel().getTerminal().getId());
                        itemRewardModel.setReward(validatedRewardModel);
                        this.getPosAPIModelCache().getValidatedLoyaltyRewardModels().add(validatedRewardModel);
                    }
                    if (validatedRewardModel != null && validatedRewardModel.getId() != null) {
                        itemRewardModel.setItemId(validatedRewardModel.getId());
                    }
                }

                if (itemRewardModel.getAmount() != null && itemRewardModel.getAmount().compareTo(BigDecimal.ZERO) != 0) {
                    itemRewardModel.setAmount(itemRewardModel.getAmount().setScale(4, RoundingMode.HALF_UP));
                }
            }

            //validate Product Model on the Modifier
            for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()) {
                if ((modifierLineItemModel.getProduct().getId() == null || productLineItemModel.getProduct().getId().equals(0))
                        && (modifierLineItemModel.getProduct().getProductCode() == null || productLineItemModel.getProduct().getProductCode().isEmpty())
                        && (modifierLineItemModel.getProduct().getName() == null || productLineItemModel.getProduct().getName().isEmpty())) {
                    throw new MissingDataException("Missing or Invalid Modifier Information", terminalId);
                }

                //id
                if (modifierLineItemModel.getProduct().getId() != null
                        && !modifierLineItemModel.getProduct().getId().toString().isEmpty() && !modifierLineItemModel.getProduct().getId().equals(0)) {
                    ProductModel validatedProductModel = this.getPosAPIModelCache().checkProductCache(modifierLineItemModel.getProduct().getId());
                    if (validatedProductModel != null) {
                        modifierLineItemModel.setProduct(validatedProductModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        validatedProductModel = ProductModel.getActiveProductModelById(modifierLineItemModel.getProduct().getId(), transactionModel.getTerminal());
                        modifierLineItemModel.setProduct(validatedProductModel);
                        this.getPosAPIModelCache().getValidatedProductModels().add(validatedProductModel);
                    } //product code
                } else if (modifierLineItemModel.getProduct().getProductCode() != null
                        && !modifierLineItemModel.getProduct().getProductCode().toString().isEmpty()) {
                    ProductModel validatedProductModel = this.getPosAPIModelCache().checkProductPluCache(modifierLineItemModel.getProduct().getProductCode());
                    if (validatedProductModel != null) {
                        modifierLineItemModel.setProduct(validatedProductModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        validatedProductModel = ProductModel.getActiveProductModelByPluCode(modifierLineItemModel.getProduct().getProductCode(), transactionModel.getTerminal());
                        modifierLineItemModel.setProduct(validatedProductModel);
                        this.getPosAPIModelCache().getValidatedProductModels().add(validatedProductModel);
                    }  //product name
                } else if (modifierLineItemModel.getProduct().getName() != null
                        && !modifierLineItemModel.getProduct().getName().isEmpty()) {
                    ProductModel validatedProductModel = this.getPosAPIModelCache().checkProductCache(modifierLineItemModel.getProduct().getName());
                    if (validatedProductModel != null) {
                        modifierLineItemModel.setProduct(validatedProductModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        validatedProductModel = ProductModel.getActiveProductModelByName(modifierLineItemModel.getProduct().getName(), transactionModel.getTerminal());
                        modifierLineItemModel.setProduct(validatedProductModel);
                        this.getPosAPIModelCache().getValidatedProductModels().add(validatedProductModel);
                    }
                }

                //validate keypadID field on the modifierLineItemModel
                if (modifierLineItemModel.getKeypadId() != null && !modifierLineItemModel.getKeypadId().toString().isEmpty()) {
                    boolean keypadIdIsCached = this.getPosAPIModelCache().checkKeypadIdIsCached(modifierLineItemModel.getKeypadId());
                    if (!keypadIdIsCached) { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        boolean keypadIdIsValidAndActive = KeypadDetailModel.isKeypadValidAndActive(transactionModel.getTerminal(), modifierLineItemModel.getKeypadId());
                        if (keypadIdIsValidAndActive) {
                            this.getPosAPIModelCache().getValidatedKeypadIds().add(modifierLineItemModel.getKeypadId());
                        }
                    }
                }

                //validate PrepOptions on modifier
                if (modifierLineItemModel.getPrepOption() != null){
                    if (modifierLineItemModel.getPrepOption().getId() != null){
                        PrepOptionModel validatedPrepOptionModel = PrepOptionModel.getPrepOptionModelByIdAndSet(modifierLineItemModel.getPrepOption().getId(), modifierLineItemModel.getPrepOption().getPrepOptionSetId(), transactionModel.getTerminal().getId());
                        modifierLineItemModel.setPrepOption(validatedPrepOptionModel);
                    } else {
                        throw new MissingDataException("Invalid Prep Option submitted: Missing Prep Option Details.", terminalId);
                    }
                }
            }

            //validate keypadID field on the productLineItemModel
            if (productLineItemModel.getKeypadId() != null && !productLineItemModel.getKeypadId().toString().isEmpty()) {
                boolean keypadIdIsCached = this.getPosAPIModelCache().checkKeypadIdIsCached(productLineItemModel.getKeypadId());
                if (!keypadIdIsCached) { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                    boolean keypadIdIsValidAndActive = KeypadDetailModel.isKeypadValidAndActive(transactionModel.getTerminal(), productLineItemModel.getKeypadId());
                    if (keypadIdIsValidAndActive) {
                        this.getPosAPIModelCache().getValidatedKeypadIds().add(productLineItemModel.getKeypadId());
                    }
                }
            }

            //validate Item Surcharge Models
            for (ItemSurchargeModel itemSurchargeModel : productLineItemModel.getSurcharges()) {
                //check for amount
                if (itemSurchargeModel.getAmount() != null &&
                        itemSurchargeModel.getAmount().toString().isEmpty()) {
                    throw new MissingDataException("Invalid Amount on Surcharge Line Item", terminalId);
                }

                if (itemSurchargeModel.getSurcharge() != null) {
                    if (itemSurchargeModel.getSurcharge().getId() != null) {
                        //check to see if we have validated this Surcharge Model previously
                        SurchargeModel validatedSurchargeModel = this.getPosAPIModelCache().checkSurchargeCache(itemSurchargeModel.getSurcharge().getId());
                        if (validatedSurchargeModel != null) {
                            itemSurchargeModel.setSurcharge(SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel));
                        } else {
                            validatedSurchargeModel = SurchargeModel.getActiveSurchargeModel(itemSurchargeModel.getSurcharge().getId(), transactionModel.getTerminal());
                            itemSurchargeModel.setSurcharge(SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel));
                            this.getPosAPIModelCache().getValidatedSurchargeModels().add(validatedSurchargeModel);
                        }
                        if (validatedSurchargeModel != null && validatedSurchargeModel.getId() != null) {
                            itemSurchargeModel.setItemId(validatedSurchargeModel.getId());
                        }
                    } else if (itemSurchargeModel.getSurcharge().getName() != null) {
                        //check to see if we have validated this Surcharge Model previously
                        SurchargeModel validatedSurchargeModel = this.getPosAPIModelCache().checkSurchargeCache(itemSurchargeModel.getSurcharge().getName());
                        if (validatedSurchargeModel != null) {
                            itemSurchargeModel.setSurcharge(SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel));
                        } else {
                            validatedSurchargeModel = SurchargeModel.getActiveSurchargeModel(itemSurchargeModel.getSurcharge().getName(), transactionModel.getTerminal());
                            itemSurchargeModel.setSurcharge(SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel));
                            this.getPosAPIModelCache().getValidatedSurchargeModels().add(validatedSurchargeModel);
                        }
                        if (validatedSurchargeModel != null && validatedSurchargeModel.getId() != null) {
                            itemSurchargeModel.setItemId(validatedSurchargeModel.getId());
                        }
                    }
                } else {
                    throw new MissingDataException("Invalid Surcharge submitted: Missing Surcharge Details.", terminalId);
                }
            }

            //validate PrepOptions
            if (productLineItemModel.getPrepOption() != null){
                if (productLineItemModel.getPrepOption().getId() != null){
                    PrepOptionModel validatedPrepOptionModel = PrepOptionModel.getPrepOptionModelByIdAndSet(productLineItemModel.getPrepOption().getId(), productLineItemModel.getPrepOption().getPrepOptionSetId(), transactionModel.getTerminal().getId());
                    productLineItemModel.setPrepOption(validatedPrepOptionModel);
                } else {
                    throw new MissingDataException("Invalid Prep Option submitted: Missing Prep Option Details.", terminalId);
                }
            }
        }
    }

    //Tax Lines
    private void validateTaxLineItems() throws Exception {

        //taxes
        Logger.logMessage("Validating taxes.  TransactionLineBuilder.validateTaxLineItems.", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.DEBUG);
        for (TaxLineItemModel taxLineItemModel : transactionModel.getTaxes()) {

            if (taxLineItemModel.getAmount() == null || taxLineItemModel.getAmount().toString().isEmpty()) {
                throw new MissingDataException("Missing Amount on Tax Line Item", terminalId);
            }

            if (taxLineItemModel.getTax() != null) {
                if (taxLineItemModel.getTax().getId() != null) {
                    //check to see if we have validated this Tax Model previously
                    TaxModel taxModel = TaxModel.getTaxModelFromCache(taxLineItemModel.getTax().getId(), transactionModel.getTerminal().getId(), true);
                    taxLineItemModel.setTax(taxModel);
                } else if (taxLineItemModel.getTax().getName() != null) {
                    //check to see if we have validated this Tax Model previously
                    TaxModel validatedTaxModel = this.getPosAPIModelCache().checkTaxCache(taxLineItemModel.getTax().getName());
                    if (validatedTaxModel != null) {
                        taxLineItemModel.setTax(validatedTaxModel);
                    } else {
                        validatedTaxModel = TaxModel.getTaxModelWithRevenueCenter(taxLineItemModel.getTax().getName(), transactionModel.getTerminal().getRevenueCenterId(), terminalId);
                        taxLineItemModel.setTax(validatedTaxModel);
                        this.getPosAPIModelCache().getValidatedTaxModels().add(validatedTaxModel);
                    }
                }
            } else {
                throw new MissingDataException("Invalid Tax submitted: Missing Tax Details.", terminalId);
            }
        }
    }

    //Tender Lines
    private void validateTenderLineItems() throws Exception {
        Logger.logMessage("Validating tenders.  TransactionLineBuilder.validateTenderLineItems.", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.DEBUG);
        int numVouchers = 0;
        int maxVouchers = 1; //TODO: Phase 2 will allow for a more than one voucher, so this will need to be updated
        ArrayList<String> usedCodes = new ArrayList<>();

        if (!transactionModel.getPosType().equals(PosAPIHelper.PosType.MMHAYES_POS) &&
                !transactionModel.getPosType().equals(PosAPIHelper.PosType.ONLINE_ORDERING) &&
                !transactionModel.getPosType().equals(PosAPIHelper.PosType.KIOSK_ORDERING)
        ) {
            if (!transactionModel.isInquiry()) {
                switch (transactionModel.getTransactionTypeEnum()) {
                    case SALE:
                    case REFUND:
                    case ROA:
                    case PO:

                        if (transactionModel.getRewards() == null || transactionModel.getRewards().isEmpty()) {

                            if (transactionModel.getTenders() == null || transactionModel.getTenders().isEmpty()) {
                                throw new MissingDataException("Invalid Request - Missing Tender", terminalId);
                            }
                        }
                        break;
                }
            }
        }

        for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()) {
            //If this is a voucher, process it as a tender
            if (tenderLineItemModel.getVoucherCode() != null) {
                //Check for max # of voucher codes; this should always error regardless of other handling rules. Additionally, this should not be checked for voids, refunds, or cancels.
                //TODO: Phase 2 will allow for a more than one voucher, so this will need to be updated
                if (++numVouchers > maxVouchers
                //  && !(transactionModel.getApiActionTypeEnum() == PosAPIHelper.ApiActionType.REFUND)
                //  && !(transactionModel.getApiActionTypeEnum() == PosAPIHelper.ApiActionType.VOID)
                  && !(transactionModel.getApiActionTypeEnum() == PosAPIHelper.ApiActionType.CANCEL)
                ) { throw new InvalidVoucherException("Cannot apply more than " + String.valueOf(maxVouchers) + " voucher(s) for this transaction." ); }

                try {
                    //Check if the voucher is being used offline and whether the RC allows offline vouchers
                    if (transactionModel.isOfflineMode() && !transactionModel.getTerminal().checkAllowOfflineVouchers()) {
                        throw new InvalidVoucherException("This location does not allow vouchers in Offline transactions.");
                    }

                    //Check if the voucher is in an Open, non-inquiry transaction
                    if (transactionModel.getApiActionTypeEnum() == PosAPIHelper.ApiActionType.OPEN) {
                        throw new InvalidVoucherException("Vouchers are not allowed in Open transactions.");
                    }

                    //Check if the voucher is in a Manual Refund
                    if (transactionModel.getApiActionTypeEnum() == PosAPIHelper.ApiActionType.REFUND_MANUAL) {
                        throw new InvalidVoucherException("Vouchers are not allowed in Manual Refund transactions.");
                    }

                    //Check for duplicate voucher codes
                    for (String prevCode : usedCodes) {
                        if (prevCode.equalsIgnoreCase(tenderLineItemModel.getVoucherCode().getCode())) {
                            throw new InvalidVoucherException("Duplicate Voucher Code: " + prevCode);
                        }
                    }

                    //Recreate the voucher from the code, and validate it
                    VoucherCodeModel curVoucherCodeModel = tenderLineItemModel.getVoucherCode(); //Get the existing voucherCodeModel
                    curVoucherCodeModel.createVoucherFromCode(transactionModel.getTerminal());   //Rebuild the voucher using the code
                    curVoucherCodeModel.validateVoucherCode(transactionModel);                   //Validate the voucher properties

                    //Validate the passed voucher amount
                    if (transactionModel.isInquiry()) {
                        //Inquire requests should just set the max voucher amount
                        tenderLineItemModel.setAmount(curVoucherCodeModel.getVoucher().getAmount());
                    } else if (tenderLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == 0
                        && !transactionModel.getApiActionTypeEnum().equals(PosAPIHelper.ApiActionType.REFUND)) {
                        //Non-inquires should not accept $0 amounts
                        throw new InvalidVoucherException("Vouchers cannot be redeemed for $0.");
                    } else if (tenderLineItemModel.getAmount().compareTo(curVoucherCodeModel.getVoucher().getAmount()) > 0) {
                        //Non-inquires should not accept vouchers > the max amount
                        throw new InvalidVoucherException("Voucher value provided is greater than the defined voucher value.");
                    }

                    //If we're here, that means that the voucher has passed all validation
                    usedCodes.add(tenderLineItemModel.getVoucherCode().getCode());  //Add the current code to the list of used codes
                    tenderLineItemModel.setVoucherCode(curVoucherCodeModel);        //Override the existing VoucherCodeModel with the updated changes

                    //Convert the voucher to a tender, and update the line item to that converted tender
                    tenderLineItemModel.setTender(TenderDisplayModel.getActiveTenderModel(curVoucherCodeModel.getVoucher().getTenderID(), transactionModel.getTerminal()));

                } catch (InvalidVoucherException e) {
                    handleVoucherException(transactionModel, tenderLineItemModel, e);
                    continue; //no need to process this line further, as either an error is thrown or the tender is already marked as invalid
                }
            }

            if (tenderLineItemModel.getAmount() == null || tenderLineItemModel.getAmount().toString().isEmpty()) {
                throw new MissingDataException("Missing Amount on Tender Line Item", terminalId);
            }

            //validate the tender model
            if (tenderLineItemModel.getTender() != null) {
                if (tenderLineItemModel.getTender().getId() != null) {
                    tenderLineItemModel.setTender(TenderDisplayModel.getActiveTenderModel(tenderLineItemModel.getTender().getId(), transactionModel.getTerminal()));
                } else if (tenderLineItemModel.getTender().getName() != null) {
                    tenderLineItemModel.setTender(TenderDisplayModel.getActiveTenderModel(tenderLineItemModel.getTender().getName(), transactionModel.getTerminal()));
                } else {
                    throw new MissingDataException("Invalid Tender submitted.", terminalId);
                }
            } else {
                throw new MissingDataException("Invalid Tender submitted. Missing Tender Details", terminalId);
            }

            //Voucher tenders without codes should throw errors
            if (tenderLineItemModel.getTender().getTenderTypeId().equals(PosAPIHelper.TenderType.VOUCHER.toInt()) && tenderLineItemModel.getVoucherCode() == null) {
                throw new MissingDataException("Voucher tender was submitted without a voucher code.", terminalId);
            }

            //Validate properties for tender type
            //I had to do this check after the TenderModel was queried in order to have the TenderTypeID

            if (!transactionModel.isCancelTransactionType() &&
                    !transactionModel.isTrainingTransactionType() &&
                    !transactionModel.isPaymentGatewayInquireTransactionType()) {

                switch (transactionModel.getApiActionTypeEnum()) {
                    case SALE: //For Sale Transactions
                    case VOID:
                    case REFUND:
                    case ROA:

                        if (tenderLineItemModel.getTender() != null && tenderLineItemModel.getTender().getTenderTypeId() != null) {

                            PosAPIHelper.TenderType tenderTypeEnum = PosAPIHelper.TenderType.intToEnum(tenderLineItemModel.getTender().getTenderTypeId());

                            switch (transactionModel.getPosType()) {
                                case MMHAYES_POS:
                                    switch (tenderTypeEnum) {
                                        case CREDIT_CARD:
                                            if (tenderLineItemModel.getPaymentMethodType() == null || tenderLineItemModel.getPaymentMethodType().isEmpty()) {
                                                throw new MissingDataException("Invalid Payment Method Type submitted.", terminalId);
                                            }

                                            if (tenderLineItemModel.getCreditCardTransInfo() == null || tenderLineItemModel.getCreditCardTransInfo().isEmpty()) {
                                                throw new MissingDataException("Invalid Credit Card Trans Info submitted.", terminalId);
                                            }

                                            //validate the CC Authorization Type Info
                                            if (tenderLineItemModel.getCcAuthorizationTypeId() == null || tenderLineItemModel.getCcAuthorizationTypeId().toString().isEmpty()) {
                                                throw new MissingDataException("Invalid Credit Card Authorization Type Info submitted.", terminalId);
                                            }

                                            break;
                                        case INTEGRATED_GIFT_CARD:
                                            if (tenderLineItemModel.getCreditCardTransInfo() == null || tenderLineItemModel.getCreditCardTransInfo().isEmpty()) {
                                                throw new MissingDataException("Invalid Credit Card Trans Info submitted.", terminalId);
                                            }

                                            break;
                                        default:
                                            //validate the Payment Method Type for Non Credit card types
                                            if (tenderLineItemModel.getPaymentMethodType() != null && !tenderLineItemModel.getPaymentMethodType().isEmpty()) {
                                                throw new MissingDataException("Invalid Payment Method Type submitted.", terminalId);
                                            }
                                            //validate the Credit Card Trans Info for Non Credit card types
                                            if (tenderLineItemModel.getCreditCardTransInfo() != null && !tenderLineItemModel.getCreditCardTransInfo().isEmpty()) {
                                                throw new MissingDataException("Invalid Credit Card Saf Trans Info submitted.", terminalId);
                                            }
                                            //validate the SAF Info
                                            if (tenderLineItemModel.getSafTransInfo() != null && !tenderLineItemModel.getSafTransInfo().isEmpty()) {
                                                throw new MissingDataException("Invalid Credit Card Saf Trans Info submitted.", terminalId);
                                            }
                                            //validate the CC Authorization Type Info
                                            if (tenderLineItemModel.getCcAuthorizationTypeId() != null && !tenderLineItemModel.getCcAuthorizationTypeId().toString().isEmpty()) {
                                                throw new MissingDataException("Invalid Credit Card Authorization Type Info submitted.", terminalId);
                                            }

                                            break;
                                    }

                                    break;
                                case ONLINE_ORDERING:  //needs to be looked at again after CC Info is added for consolidated receipts
                                case MY_QC_FUNDING:
                                    switch (tenderTypeEnum) {
                                        case CREDIT_CARD:
                                            if (tenderLineItemModel.getCreditCardTransInfo() == null || tenderLineItemModel.getCreditCardTransInfo().isEmpty()) {
                                                throw new MissingDataException("Invalid Credit Card Trans Info submitted.", terminalId);
                                            }
                                            break;
                                        default:

                                            break;
                                    }
                                    break;
                                case KIOSK_ORDERING:
                                    break;
                                default: //Validate for other Terminal Types

                                    if (tenderLineItemModel.getCreditCardTransInfo() != null && !tenderLineItemModel.getCreditCardTransInfo().isEmpty()) {
                                        throw new MissingDataException("Invalid Credit Card Trans Info submitted.", terminalId);
                                    }

                                    //validate the SAF Info
                                    if (tenderLineItemModel.getSafTransInfo() != null && !tenderLineItemModel.getSafTransInfo().isEmpty()) {
                                        throw new MissingDataException("Invalid Credit Card Saf Trans Info submitted.", terminalId);
                                    }

                                    //validate the CC Authorization Type Info
                                    if (tenderLineItemModel.getCcAuthorizationTypeId() != null && !tenderLineItemModel.getCcAuthorizationTypeId().toString().isEmpty()) {
                                        throw new MissingDataException("Invalid Credit Card Authorization Type Info submitted.", terminalId);
                                    }

                                    switch (tenderTypeEnum) {
                                        case CREDIT_CARD:

                                            break;
                                        default:
                                            //validate the Payment Method Type for Non Credit card types
                                            if (tenderLineItemModel.getPaymentMethodType() != null && !tenderLineItemModel.getPaymentMethodType().isEmpty()) {
                                                throw new MissingDataException("Invalid Payment Method Type submitted.", terminalId);
                                            }
                                            break;
                                    }
                                    break;
                            }
                        }

                        break;
                }
            }

            //validate Purchase Categories
            if (tenderLineItemModel.getQcTransaction() != null) {

                for (QcTransactionLineItemModel qcTransactionLineItemModel : tenderLineItemModel.getQcTransaction().getTransactionDetails()) {
                    //validate Purchase Category
                    if (qcTransactionLineItemModel.getPurchaseCategory() != null) {
                        if (qcTransactionLineItemModel.getPurchaseCategory().getId() != null) {
                            qcTransactionLineItemModel.setPurchaseCategory(PurchaseCategoryModel.getPurchaseCategoryModel(qcTransactionLineItemModel.getPurchaseCategory().getId(), terminalId));
                        } else if (qcTransactionLineItemModel.getPurchaseCategory().getName() != null) {
                            qcTransactionLineItemModel.setPurchaseCategory(PurchaseCategoryModel.getPurchaseCategoryModel(qcTransactionLineItemModel.getPurchaseCategory().getName(), terminalId));
                        } else {
                            throw new MissingDataException("Invalid Purchase Category submitted.", terminalId);
                        }
                    }
                }
            }

            //check for sandbox mode, for QC Tender for amount 3.33
            //Throw an invalid Tender error during the transaction
            if (transactionModel.getTerminal().isSandBoxMode()) {
                if (tenderLineItemModel.getExtendedAmount() != null && tenderLineItemModel.getExtendedAmount().equals(new BigDecimal("3.33"))) {
                    throw new TenderNotFoundException("Tender Not Found.  Please Try Again. (Sandbox Mode)");
                }

                if (tenderLineItemModel.getExtendedAmount() != null && tenderLineItemModel.getExtendedAmount().equals(new BigDecimal("3.35"))) {
                    throw new TransactionGlobalLimitException("Transaction exceeds Global Transaction limit.  Please Try Again. (Sandbox Mode)");//"E7600 - Tender Not Found.  Please Try Again. (Sandbox Mode)"
                }
            }
        }

        //If there are invalid tenders, remove them from the tenders collection
        for (TenderLineItemModel lineModel : transactionModel.getInvalidTenders()) {
            transactionModel.getTenders().remove(lineModel);
        }
    }

    //handle exceptions thrown due to voucher issues
    private void handleVoucherException(TransactionModel txnModel, TenderLineItemModel lineModel, InvalidVoucherException e) throws Exception {
        //if inquire, don't throw errors
        if (txnModel.isInquiry()) {
            //simply clean the invalid tender and add it to the invalid tenders collection within the transaction model
            lineModel.setAmount(BigDecimal.ZERO);                //set amount to 0
            lineModel.setExtendedAmount(BigDecimal.ZERO);        //set extended amount to 0
            lineModel.setEligibleAmount(BigDecimal.ZERO);        //set eligible amount to 0
            lineModel.setQuantity(BigDecimal.ZERO);              //set quantity to 0
            lineModel.getVoucherCode().setNotes(e.getDetails()); //update notes to the reason for failure
            txnModel.getInvalidTenders().add(lineModel);         //add tender to invalid tender collection

            //update expired vouchers if the terminal is not offline and the transaction is not marked as a cancel
            if (e.getDetails().equalsIgnoreCase("VOUCHER HAS EXPIRED.") && !transactionModel.isOfflineMode() && !transactionModel.isCancelTransactionType()) {
                new DataManager().parameterizedExecuteNonQuery(
                    "data.posapi30.updateVoucherCode",
                    new Object[] {
                        PosAPIHelper.VoucherStatus.EXPIRED.toInt(),
                        lineModel.getVoucherCode().getId(),
                        txnModel.getTimeStamp()
                    },
                    PosAPIHelper.getLogFileName(txnModel.getTerminalId())
                );
            }
            Logger.logMessage("Voucher will not be applied: " + e.getDetails(), PosAPIHelper.getLogFileName(txnModel.getTerminal().getId()), Logger.LEVEL.TRACE);
        }
        //otherwise, throw that exception like a baseball sized spider that dropped into your hand from the ceiling
        else {
            Logger.logMessage("Error applying voucher: " + e.getDetails(), PosAPIHelper.getLogFileName(txnModel.getTerminal().getId()), Logger.LEVEL.ERROR);
            Logger.logException(e, PosAPIHelper.getLogFileName(txnModel.getTerminal().getId()));
            throw e;
        }
    }

    //Discount Lines
    private void validatePosDiscountLineItems() throws Exception {
        Logger.logMessage("Validating POS Discounts.  TransactionLineBuilder.validatePosDiscountLineItems.", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.DEBUG);

        for (DiscountLineItemModel discountLineItemModel : transactionModel.getDiscounts()) {

            if (discountLineItemModel.getAmount() == null || discountLineItemModel.getAmount().toString().isEmpty()) {
                throw new DiscountInvalidException("Missing Amount on Discount Line Item", terminalId);
            }

            //if the POS Discount is positive and the transaction is a Sale then flip the sign
            switch (transactionModel.getApiActionTypeEnum()) {
                case SALE:
                    if (discountLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == 1) {
                        discountLineItemModel.setAmount(discountLineItemModel.getAmount().multiply(new BigDecimal(-1)));
                        discountLineItemModel.setExtendedAmount(discountLineItemModel.getAmount().multiply(discountLineItemModel.getQuantity()));
                    }
                    break;
            }

            if (discountLineItemModel.getDiscount() != null) {
                //For Open Discounts, QCPOS will override the discount amount by sending in the DiscountModel.amount field
                BigDecimal overriddenDiscountAmount = BigDecimal.ZERO;
                if (discountLineItemModel.getDiscount().getAmount() != null) {
                    overriddenDiscountAmount = discountLineItemModel.getDiscount().getAmount();
                }

                if (discountLineItemModel.getDiscount().getId() != null) {

                    DiscountModel validatedDiscountModel = this.getPosAPIModelCache().checkDiscountCache(discountLineItemModel.getDiscount().getId());
                    if (validatedDiscountModel != null) {
                        discountLineItemModel.setDiscount(validatedDiscountModel);
                    } else {
                        validatedDiscountModel = DiscountModel.getDiscountModelWithRevenueCenter(discountLineItemModel.getDiscount().getId(), getTransactionModel().getTerminal().getRevenueCenterId(), terminalId);
                        discountLineItemModel.setDiscount(validatedDiscountModel);
                        this.getPosAPIModelCache().getValidatedDiscountModels().add(validatedDiscountModel);
                    }
                } else if (discountLineItemModel.getDiscount().getName() != null) {
                    DiscountModel validatedDiscountModel = this.getPosAPIModelCache().checkDiscountCache(discountLineItemModel.getDiscount().getName());
                    if (validatedDiscountModel != null) {
                        discountLineItemModel.setDiscount(validatedDiscountModel);
                    } else {
                        validatedDiscountModel = DiscountModel.getDiscountModelWithRevenueCenter(discountLineItemModel.getDiscount().getName(), getTransactionModel().getTerminal().getRevenueCenterId(), terminalId);
                        discountLineItemModel.setDiscount(validatedDiscountModel);
                        this.getPosAPIModelCache().getValidatedDiscountModels().add(validatedDiscountModel);
                    }
                } else {
                    throw new DiscountInvalidException("Invalid POS Discount information submitted.", terminalId);
                }

                //Validate any overridden Discount Amount sent in
                if (discountLineItemModel.getDiscount().isOpenPrice()) {
                    if (discountLineItemModel.getDiscount().isPresetPrice()) {
                        //If it's open and Preset, check for an overridden amount
                        if (overriddenDiscountAmount != null && overriddenDiscountAmount.compareTo(BigDecimal.ZERO) != 0) {
                            discountLineItemModel.getDiscount().setAmount(overriddenDiscountAmount);
                        }
                    } else {
                        //If it's open and not preset
                        if (overriddenDiscountAmount != null) {

                            //Check if the Max Open Percentage is set on the POS Discount
                            if (discountLineItemModel.getDiscount().getDiscountTypeId().equals(PosAPIHelper.DiscountType.PERCENT.toInt())){
                                if (discountLineItemModel.getDiscount().getMaximumOpenPercent() != null && discountLineItemModel.getDiscount().getMaximumOpenPercent() != 0){
                                    BigDecimal maxOpenPercent = new BigDecimal(discountLineItemModel.getDiscount().getMaximumOpenPercent());
                                    if (overriddenDiscountAmount.compareTo(maxOpenPercent) == 1){
                                        throw new DiscountInvalidException("Invalid POS Discount information submitted: Maximum Open Percentage", terminalId);
                                    }
                                }
                            }

                            discountLineItemModel.getDiscount().setAmount(overriddenDiscountAmount);
                        } else {
                            //throw error, invalid discount configuration.  A price is required
                            throw new DiscountInvalidException("Invalid POS Discount information submitted.  Amount required on an Open POS Discount.", terminalId);
                        }
                    }
                }
                // allow overriding the amount for pos anywhere refunds, but only so long as the amount is -1 * original amount
                else if (transactionModel.getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.POSANYWHERE.toInt()) && transactionModel.getApiActionTypeEnum() == REFUND_MANUAL) {
                    if (overriddenDiscountAmount.multiply(new BigDecimal(-1)).compareTo(discountLineItemModel.getDiscount().getAmount()) == 0) {
                        if (overriddenDiscountAmount != null && overriddenDiscountAmount.compareTo(BigDecimal.ZERO) != 0) {
                            discountLineItemModel.getDiscount().setAmount(overriddenDiscountAmount);
                        }
                    }
                }
            } else {
                throw new DiscountInvalidException("Invalid POS Discount submitted: Missing Discount Details", terminalId);
            }

            if (discountLineItemModel.getQcDiscount() != null) {
                if (discountLineItemModel.getQcDiscount().getId() != null) {

                    QcDiscountModel validatedQcDiscount = this.getPosAPIModelCache().checkQcDiscountCache(discountLineItemModel.getQcDiscount().getId());
                    if (validatedQcDiscount != null) {
                        discountLineItemModel.setQcDiscount(validatedQcDiscount);
                    } else {
                        validatedQcDiscount = QcDiscountModel.getQcDiscountModel(discountLineItemModel.getQcDiscount().getId(), terminalId);
                        discountLineItemModel.setQcDiscount(validatedQcDiscount);
                        this.getPosAPIModelCache().getValidatedQcDiscountModels().add(validatedQcDiscount);
                    }
                } else if (discountLineItemModel.getDiscount().getName() != null) {
                    QcDiscountModel validatedQcDiscount = this.getPosAPIModelCache().checkQcDiscountCache(discountLineItemModel.getQcDiscount().getName());
                    if (validatedQcDiscount != null) {
                        discountLineItemModel.setQcDiscount(validatedQcDiscount);
                    } else {
                        validatedQcDiscount = QcDiscountModel.getQcDiscountModel(discountLineItemModel.getQcDiscount().getName(), terminalId);
                        discountLineItemModel.setQcDiscount(validatedQcDiscount);
                        this.getPosAPIModelCache().getValidatedQcDiscountModels().add(validatedQcDiscount);
                    }
                } else {
                    throw new DiscountInvalidException("Invalid QC Discount information submitted.", terminalId);
                }

                //For this QC Discount, validate the EmployeeID
                if (discountLineItemModel.getEmployeeId() != null && !discountLineItemModel.getEmployeeId().toString().isEmpty()) {
                    if (this.getTransactionModel().getLoyaltyAccount() == null) {
                        AccountModel accountModel = new AccountModel();
                        accountModel.setId(discountLineItemModel.getEmployeeId());
                        accountModel.setTerminal(this.getTransactionModel().getTerminal());
                        this.getTransactionModel().setLoyaltyAccount(new LoyaltyAccountModel(accountModel, true, transactionModel));
                        this.getTransactionModel().setHasValidatedLoyaltyAccount(true);
                    }
                }

                //Check if the terminal allows QC Discount Coupons in offline mode
                if (discountLineItemModel.getQcDiscount().getDiscountTypeId() != null
                        && !discountLineItemModel.getQcDiscount().getDiscountTypeId().toString().isEmpty()
                        && discountLineItemModel.getQcDiscount().getDiscountTypeId().equals(PosAPIHelper.DiscountType.COUPON.toInt())) {

                    if (transactionModel.isOfflineMode() && !transactionModel.getTerminal().getAllowQcCouponDiscountOffline()) {
                        throw new DiscountInvalidException("QC Coupon Discount cannot be submitted in offline mode.", terminalId);
                    }
                }
            }
        }
    }

    //Paid Out Lines
    private void validatePaidOutLineItems() throws Exception {
        Logger.logMessage("Validating Paid Outs.  TransactionLineBuilder.validatePaidOutLineItems.", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.DEBUG);
        for (PaidOutLineItemModel paidOutLineItemModel : transactionModel.getPaidOuts()) {
            if (paidOutLineItemModel.getPaidOut() != null) {
                if (paidOutLineItemModel.getPaidOut().getId() != null) {
                    paidOutLineItemModel.setPaidOut(PaidOutModel.getPaidOutModel(paidOutLineItemModel.getPaidOut().getId(), terminalId));
                } else if (paidOutLineItemModel.getPaidOut().getName() != null) {
                    paidOutLineItemModel.setPaidOut(PaidOutModel.getPaidOutModel(paidOutLineItemModel.getPaidOut().getName(), terminalId));
                }
            } else {
                throw new MissingDataException("Invalid Paid Out submitted: Missing Paid Out details", terminalId);
            }
        }
    }

    //Received On Account Lines
    private void validateReceivedOnAccountLineItems() throws Exception {
        Logger.logMessage("Validating Received On Accounts.  TransactionLineBuilder.validateReceivedOnAccountLineItems.", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.DEBUG);

        switch (transactionModel.getTransactionTypeEnum()) {
            case ROA:
                if (transactionModel.getReceivedOnAccounts() == null || transactionModel.getReceivedOnAccounts().isEmpty()) {
                    throw new MissingDataException("Invalid Request - Missing Received On Account Line Items", terminalId);
                }
                break;
        }

        for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : transactionModel.getReceivedOnAccounts()) {
            if (receivedOnAccountLineItemModel.getReceivedOnAccount() != null) {
                if (receivedOnAccountLineItemModel.getReceivedOnAccount().getId() != null) {
                    receivedOnAccountLineItemModel.setReceivedOnAccount(ReceivedOnAccountModel.getActiveReceivedOnAccountModel(receivedOnAccountLineItemModel.getReceivedOnAccount().getId(), transactionModel.getTerminal()));
                } else if (receivedOnAccountLineItemModel.getReceivedOnAccount().getName() != null)
                    receivedOnAccountLineItemModel.setReceivedOnAccount(ReceivedOnAccountModel.getActiveReceivedOnAccountModel(receivedOnAccountLineItemModel.getReceivedOnAccount().getName(), transactionModel.getTerminal()));
            } else {
                throw new MissingDataException("Invalid Received On Account submitted: Missing Received On Account details", terminalId);
            }

            if (receivedOnAccountLineItemModel.getAmount() == null
                    || receivedOnAccountLineItemModel.getAmount().toString().isEmpty()) {
                throw new MissingDataException("Missing Amount on Received On Account Line Item", terminalId);
            }

            if (receivedOnAccountLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == 0) {
                throw new MissingDataException("Invalid Amount on Received On Account Line Item", terminalId);
            }

            /*
            * Validate the Account on the Received On Account line item
            */

            //For QCPos, if the employee Id is sent in, use that to validate the Employee
            if (receivedOnAccountLineItemModel.getEmployeeId() != null
                    && !receivedOnAccountLineItemModel.getEmployeeId().toString().isEmpty()
                    && !receivedOnAccountLineItemModel.getEmployeeId().equals(0)
                    && receivedOnAccountLineItemModel.getAccount() == null) {
                AccountModel newAccountModel = new AccountModel();
                newAccountModel.setId(receivedOnAccountLineItemModel.getEmployeeId());
                receivedOnAccountLineItemModel.setAccount(newAccountModel);
            }

            //if the ReceivedOnAccountModel is set to "IsQC", then an Account Model is required
            if (receivedOnAccountLineItemModel.getReceivedOnAccount() != null
                    && receivedOnAccountLineItemModel.getReceivedOnAccount().isQC()) {
                if (receivedOnAccountLineItemModel.getAccount() == null) {
                    throw new MissingDataException("Missing Account for Received On Account Line Item.", terminalId);
                }
            }

            //validate the Account Model if it's included
            if (receivedOnAccountLineItemModel.getAccount() != null) {

                AccountFieldAvailable accountFieldAvailable = AccountFieldAvailable.NONE;
                accountFieldAvailable = AccountFieldAvailable.NONE;

                //check here to see if we have the Employee ID, Number, or Badge for the Tender Account
                if ((receivedOnAccountLineItemModel.getAccount().getId() != null && !receivedOnAccountLineItemModel.getAccount().getId().toString().isEmpty()) && !receivedOnAccountLineItemModel.getAccount().getId().equals(0)) {
                    accountFieldAvailable = AccountFieldAvailable.ID;
                } else if (receivedOnAccountLineItemModel.getAccount().getNumber() != null && !receivedOnAccountLineItemModel.getAccount().getNumber().isEmpty() && !receivedOnAccountLineItemModel.getAccount().getNumber().equals(0)) {
                    accountFieldAvailable = AccountFieldAvailable.NUMBER;
                } else if (receivedOnAccountLineItemModel.getAccount().getBadge() != null && !receivedOnAccountLineItemModel.getAccount().getBadge().isEmpty()) {
                    accountFieldAvailable = AccountFieldAvailable.BADGE;
                } else {
                    throw new MissingDataException("Account cannot be found.", terminalId);
                }

                AccountModel accountModel = null;

                switch (accountFieldAvailable) {
                    case ID:
                        accountModel = this.getPosAPIModelCache().checkAccountCache(receivedOnAccountLineItemModel.getAccount().getId());
                        if (accountModel == null) {
                            //HP-2010: Return Badge Alias if it was used on the original transaction
                            AccountQueryParams accountQueryParams = new AccountQueryParams(true, false, receivedOnAccountLineItemModel.getBadgeAssignmentId());
                            accountModel = AccountModel.getAccountModelByIdForTender(transactionModel.getTerminal(), BigDecimal.ZERO, receivedOnAccountLineItemModel.getAccount().getId(), accountQueryParams);
                            this.getPosAPIModelCache().getValidatedAccountModels().add(accountModel);
                        }
                        break;
                    case NUMBER:
                        accountModel = this.getPosAPIModelCache().checkAccountCache(receivedOnAccountLineItemModel.getAccount().getNumber(), true);
                        if (accountModel == null) {
                            accountModel = AccountModel.getAccountModelByNumberForTender(transactionModel.getTerminal(), BigDecimal.ZERO, receivedOnAccountLineItemModel.getAccount().getNumber(), new AccountQueryParams());
                            this.getPosAPIModelCache().getValidatedAccountModels().add(accountModel);
                        }
                        break;
                    case BADGE:
                        accountModel = this.getPosAPIModelCache().checkAccountCache(receivedOnAccountLineItemModel.getAccount().getBadge());
                        if (accountModel == null) {
                            accountModel = AccountModel.getAccountModelByBadgeForTender(transactionModel.getTerminal(), BigDecimal.ZERO, receivedOnAccountLineItemModel.getAccount().getBadge(), new AccountQueryParams());
                            this.getPosAPIModelCache().getValidatedAccountModels().add(accountModel);
                        }
                        break;
                }

                receivedOnAccountLineItemModel.setAccount(accountModel);

                //populate the Badge Assignment ID if it is missing on the roaLineItemModel
                if (accountModel != null) {
                    if (accountModel.getBadgeAssignmentId() != null
                            && receivedOnAccountLineItemModel.getBadgeAssignmentId() == null) {
                        receivedOnAccountLineItemModel.setBadgeAssignmentId(accountModel.getBadgeAssignmentId()); //set the Badge Assignment ID
                    }
                }

                   /*
            * 07/10/2017 egl - the Received On Account can't be funded with a Quickcharge tender of the same Account
            */

                for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()) {
                    if (tenderLineItemModel.getTender() != null) {
                        if (tenderLineItemModel.getTender().getTenderTypeId().equals(PosAPIHelper.TenderType.QUICKCHARGE.toInt())) {
                            if (tenderLineItemModel.getAccount() != null) {
                                if (tenderLineItemModel.getAccount().getId().equals(receivedOnAccountLineItemModel.getAccount().getId())) {
                                    throw new MissingDataException("Received On Account and Tender Line Items cannot have same Account.", terminalId);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //Loyalty Reward Lines
    //this validates the Loyalty Reward ID and Name, and fills in all the information

    private void validateLoyaltyRewardLineItems() throws Exception {
        //Loyalty Rewards
        Logger.logMessage("Validating Loyalty Rewards.  TransactionLineBuilder.validateLoyaltyRewardLineItems.", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.DEBUG);

        //if rewards are redeemed, make sure there is a Loyalty Account
        if (this.getTransactionModel().getLoyaltyAccount() == null &&
                this.getTransactionModel().getRewards() != null && !this.getTransactionModel().getRewards().isEmpty()) {
            throw new MissingDataException("Loyalty Account Not Found.  Loyalty Account Required when redeeming Rewards.", terminalId);
        }

        Integer lineItemSequence = 0;

        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {

            loyaltyRewardLineItemModel.setLineItemSequence(lineItemSequence++); //make sure rewards are returned in the same order as they arrived in

            //Validate LoyaltyRewardModel.Product
            if (loyaltyRewardLineItemModel.getReward() != null) {

                if (loyaltyRewardLineItemModel.getExtendedAmount().compareTo(BigDecimal.ZERO) == 0) {
                    throw new MissingDataException("Invalid Reward submitted: Reward has value of Zero", terminalId);
                }

                //if the reward is positive and the transaction is a Sale then flip the sign
                switch (transactionModel.getApiActionTypeEnum()) {
                    case SALE:
                        if (loyaltyRewardLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == 1) {
                            loyaltyRewardLineItemModel.setAmount(loyaltyRewardLineItemModel.getAmount().multiply(new BigDecimal(-1)));
                            loyaltyRewardLineItemModel.setExtendedAmount(loyaltyRewardLineItemModel.getAmount().multiply(loyaltyRewardLineItemModel.getQuantity()));
                        }
                        break;
                }

                //validate RewardModel
                if (loyaltyRewardLineItemModel.getReward().getId() != null
                        && !loyaltyRewardLineItemModel.getReward().getId().toString().isEmpty()) {
                    LoyaltyRewardModel validatedRewardModel = this.getPosAPIModelCache().checkLoyaltyRewardCache(loyaltyRewardLineItemModel.getReward().getId());
                    if (validatedRewardModel != null) {
                        loyaltyRewardLineItemModel.setReward(validatedRewardModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        validatedRewardModel = LoyaltyRewardModel.getLoyaltyRewardModel(loyaltyRewardLineItemModel.getReward().getId(), this.getTransactionModel().getTerminal().getId());
                        loyaltyRewardLineItemModel.setReward(validatedRewardModel);
                        this.getPosAPIModelCache().getValidatedLoyaltyRewardModels().add(validatedRewardModel);
                    }
                } else if (loyaltyRewardLineItemModel.getReward().getName() != null
                        && !loyaltyRewardLineItemModel.getReward().getName().isEmpty()) {
                    LoyaltyRewardModel validatedRewardModel = this.getPosAPIModelCache().checkLoyaltyRewardCache(loyaltyRewardLineItemModel.getReward().getName());
                    if (validatedRewardModel != null) {
                        loyaltyRewardLineItemModel.setReward(validatedRewardModel);
                    } else { //if the model doesn't exist in the cache, fetch it, and add it to the cache
                        validatedRewardModel = LoyaltyRewardModel.getLoyaltyRewardModel(loyaltyRewardLineItemModel.getReward().getName(), this.getTransactionModel().getTerminal().getId());
                        loyaltyRewardLineItemModel.setReward(validatedRewardModel);
                        this.getPosAPIModelCache().getValidatedLoyaltyRewardModels().add(validatedRewardModel);
                    }
                }

                /**
                 *For Transaction Credit Rewards, check the value passed into the API.  If
                 */
                if (loyaltyRewardLineItemModel.getReward().getTypeName().equalsIgnoreCase(PosAPIHelper.LoyaltyRewardType.TRANSACTION_CREDIT.toStringName())) {
                    if (loyaltyRewardLineItemModel.getExtendedAmount().abs().compareTo(loyaltyRewardLineItemModel.getReward().getValue().abs()) == 1) {
                        throw new MissingDataException("Invalid Reward submitted: Reward value is too high", terminalId);
                    }
                }

            } else {
                throw new MissingDataException("Invalid Reward submitted: Missing Reward details", transactionModel.getTerminal().getId());
            }
        }
    }

    private void validateServiceChargeLineItems() throws Exception {
        //Loyalty Rewards
        Logger.logMessage("Validating Service Charges.  TransactionLineBuilder.validateServiceChargeLineItems.", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.DEBUG);

        for (ServiceChargeLineItemModel serviceChargeLineItemModel : this.getTransactionModel().getServiceCharges()) {

            //check for amount
            if (serviceChargeLineItemModel.getAmount() != null &&
                    serviceChargeLineItemModel.getAmount().toString().isEmpty()) {
                throw new MissingDataException("Invalid Amount on Service Charge Line Item", terminalId);
            }

            if (serviceChargeLineItemModel.getServiceCharge() != null) {
                if (serviceChargeLineItemModel.getServiceCharge().getId() != null) {
                    //check to see if we have validated this Surcharge Model previously
                    SurchargeModel validatedSurchargeModel = this.getPosAPIModelCache().checkSurchargeCache(serviceChargeLineItemModel.getServiceCharge().getId());
                    if (validatedSurchargeModel != null) {
                        serviceChargeLineItemModel.setServiceCharge(new ServiceChargeModel(validatedSurchargeModel));
                    } else {
                        validatedSurchargeModel = SurchargeModel.getActiveSurchargeModel(serviceChargeLineItemModel.getServiceCharge().getId(), transactionModel.getTerminal());
                        serviceChargeLineItemModel.setServiceCharge(new ServiceChargeModel(validatedSurchargeModel));
                        this.getPosAPIModelCache().getValidatedSurchargeModels().add(validatedSurchargeModel);
                    }
                } else if (serviceChargeLineItemModel.getServiceCharge().getName() != null) {
                    //check to see if we have validated this Surcharge Model previously
                    SurchargeModel validatedSurchargeModel = this.getPosAPIModelCache().checkSurchargeCache(serviceChargeLineItemModel.getServiceCharge().getName());
                    if (validatedSurchargeModel != null) {
                        serviceChargeLineItemModel.setServiceCharge(new ServiceChargeModel(validatedSurchargeModel));
                    } else {
                        validatedSurchargeModel = SurchargeModel.getActiveSurchargeModel(serviceChargeLineItemModel.getServiceCharge().getName(), transactionModel.getTerminal());
                        serviceChargeLineItemModel.setServiceCharge(new ServiceChargeModel(validatedSurchargeModel));
                        this.getPosAPIModelCache().getValidatedSurchargeModels().add(validatedSurchargeModel);
                    }
                }
            } else {
                throw new MissingDataException("Invalid Surcharge submitted: Missing Surcharge Details.", terminalId);
            }

        }
    }

    private void validateGratuityLineItems() throws Exception {
        //Loyalty Rewards
        Logger.logMessage("Validating Gratuity Line Items.  TransactionLineBuilder.validateGratuityLineItems.", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.DEBUG);

        for (GratuityLineItemModel gratuityLineItemModel : this.getTransactionModel().getGratuities()) {

            //check for amount
            if (gratuityLineItemModel.getAmount() != null &&
                    gratuityLineItemModel.getAmount().toString().isEmpty()) {
                throw new MissingDataException("Invalid Amount on Gratuity Line Item", terminalId);
            }

            if (gratuityLineItemModel.getGratuity() != null) {
                if (gratuityLineItemModel.getGratuity().getId() != null) {
                    //check to see if we have validated this Surcharge Model previously
                    SurchargeModel validatedSurchargeModel = this.getPosAPIModelCache().checkSurchargeCache(gratuityLineItemModel.getGratuity().getId());
                    if (validatedSurchargeModel != null) {
                        gratuityLineItemModel.setGratuity(new GratuityModel(validatedSurchargeModel));
                    } else {
                        validatedSurchargeModel = SurchargeModel.getActiveSurchargeModel(gratuityLineItemModel.getGratuity().getId(), transactionModel.getTerminal());
                        gratuityLineItemModel.setGratuity(new GratuityModel(validatedSurchargeModel));
                        this.getPosAPIModelCache().getValidatedSurchargeModels().add(validatedSurchargeModel);
                    }
                } else if (gratuityLineItemModel.getGratuity().getName() != null) {
                    //check to see if we have validated this Surcharge Model previously
                    SurchargeModel validatedSurchargeModel = this.getPosAPIModelCache().checkSurchargeCache(gratuityLineItemModel.getGratuity().getName());
                    if (validatedSurchargeModel != null) {
                        gratuityLineItemModel.setGratuity(new GratuityModel(validatedSurchargeModel));
                    } else {
                        validatedSurchargeModel = SurchargeModel.getActiveSurchargeModel(gratuityLineItemModel.getGratuity().getName(), transactionModel.getTerminal());
                        gratuityLineItemModel.setGratuity(new GratuityModel(validatedSurchargeModel));
                        this.getPosAPIModelCache().getValidatedSurchargeModels().add(validatedSurchargeModel);
                    }
                }
            } else {
                throw new MissingDataException("Invalid Surcharge submitted: Missing Surcharge Details.", terminalId);
            }

        }
    }

    private void validateSurchargeLineItems() throws Exception {
        //Surcharges
        Logger.logMessage("Validating Surcharge Line Items.  TransactionLineBuilder.validateSurchargeLineItems.", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.DEBUG);

        //Validate the transaction type for Surcharges
        switch (transactionModel.getTransactionTypeEnum()) {
            case SALE:
            case VOID:
            case REFUND:
                //Sales, Voids, and Refunds are allowed.
                break;
            case ROA:
                switch(transactionModel.getPosType()) {
                    case MY_QC_FUNDING:
                        break;
                    default:
                        if (this.getTransactionModel().getSurcharges() != null && this.getTransactionModel().getSurcharges().size() > 0) {
                            throw new SurchargeNotFoundException("Surcharges are not allowed for this Transaction Type", terminalId);
                        }
                        break;
                }
                break;
            default:
                if (this.getTransactionModel().getSurcharges() != null && this.getTransactionModel().getSurcharges().size() > 0) {
                    throw new SurchargeNotFoundException("Surcharges are not allowed for this Transaction Type", terminalId);
                }
                break;
        }

        for (SurchargeLineItemModel surchargeLineItemModel : this.getTransactionModel().getSurcharges()) {

            //check for amount
            if (surchargeLineItemModel.getAmount() != null &&
                    surchargeLineItemModel.getAmount().toString().isEmpty()) {
                throw new SurchargeNotFoundException("Invalid Amount on Surcharge Line Item", terminalId);
            }

            if (surchargeLineItemModel.getSurcharge() != null) {
                if (surchargeLineItemModel.getSurcharge().getId() != null) {
                    //check to see if we have validated this Surcharge Model previously
                    SurchargeModel validatedSurchargeModel = this.getPosAPIModelCache().checkSurchargeCache(surchargeLineItemModel.getSurcharge().getId());
                    if (validatedSurchargeModel != null) {
                        surchargeLineItemModel.setSurcharge(SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel));
                    } else {
                        validatedSurchargeModel = SurchargeModel.getActiveSurchargeModel(surchargeLineItemModel.getSurcharge().getId(), transactionModel.getTerminal());
                        surchargeLineItemModel.setSurcharge(SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel));
                        this.getPosAPIModelCache().getValidatedSurchargeModels().add(validatedSurchargeModel);
                    }
                } else if (surchargeLineItemModel.getSurcharge().getName() != null) {
                    //check to see if we have validated this Surcharge Model previously
                    SurchargeModel validatedSurchargeModel = this.getPosAPIModelCache().checkSurchargeCache(surchargeLineItemModel.getSurcharge().getName());
                    if (validatedSurchargeModel != null) {
                        surchargeLineItemModel.setSurcharge(SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel));
                    } else {
                        validatedSurchargeModel = SurchargeModel.getActiveSurchargeModel(surchargeLineItemModel.getSurcharge().getName(), transactionModel.getTerminal());
                        surchargeLineItemModel.setSurcharge(SurchargeDisplayModel.createSurchargeModel(validatedSurchargeModel));
                        this.getPosAPIModelCache().getValidatedSurchargeModels().add(validatedSurchargeModel);
                    }
                }
            } else {
                throw new SurchargeNotFoundException("Invalid Surcharge submitted: Missing Surcharge Details.", terminalId);
            }

            //if refunds with Products is true, Item Surcharges must be present
            if (surchargeLineItemModel.getSurcharge().getRefundsWithProducts()) {
                //Validate that all SurchargeLineItemModels have associated ItemSurchargeModels
                boolean surchargeHasItemSurcharges = false;
                for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {

                    for (ItemSurchargeModel itemSurchargeModel : productLineItemModel.getSurcharges()) {
                        if (surchargeLineItemModel.getSurcharge().getId().equals(itemSurchargeModel.getSurcharge().getId())) {
                            surchargeHasItemSurcharges = true;
                        }
                    }
                }

                if (!surchargeHasItemSurcharges) {
                    throw new SurchargeNotFoundException("Invalid Surcharge submitted: Missing Item Surcharge records on the Product lines.", terminalId);
                }

            } else { //if refunds with Products is true, Item Surcharges should not be present
                //Validate that all SurchargeLineItemModels have associated ItemSurchargeModels

                boolean surchargeHasItemSurcharges = false;
                for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                    for (ItemSurchargeModel itemSurchargeModel : productLineItemModel.getSurcharges()) {
                    if (surchargeLineItemModel.getSurcharge().getId().equals(itemSurchargeModel.getSurcharge().getId())) {
                            surchargeHasItemSurcharges = true;
                        }
                    }
                }
                if (surchargeHasItemSurcharges) {
                    throw new SurchargeNotFoundException("Invalid Surcharge submitted: Item Surcharge records should not be on the Product lines.", terminalId);
                }
            }
        }
    }

    private void validateComboLineItems() throws Exception {
        for (ComboLineItemModel comboLineItemModel : transactionModel.getCombos()) {
            if (comboLineItemModel.getCombo() != null) {
                if (comboLineItemModel.getCombo().getId() != null){
                    ComboModel validatedComboModel = this.getPosAPIModelCache().checkComboCache(comboLineItemModel.getCombo().getId());
                    if (validatedComboModel != null) {
                        comboLineItemModel.setCombo(ComboModel.createComboModel(validatedComboModel));
                    } else {
                        validatedComboModel = ComboModel.getComboModelActive(comboLineItemModel.getCombo().getId(), transactionModel.getTerminal());
                        comboLineItemModel.setCombo(validatedComboModel);
                        this.getPosAPIModelCache().getValidatedComboModels().add(validatedComboModel);
                    }
                } else if (comboLineItemModel.getCombo().getName() != null && !comboLineItemModel.getCombo().getName().isEmpty()){
                    ComboModel validatedComboModel = this.getPosAPIModelCache().checkComboCache(comboLineItemModel.getCombo().getName());
                    if (validatedComboModel != null) {
                        comboLineItemModel.setCombo(validatedComboModel);
                    } else {
                        validatedComboModel = ComboModel.getComboModelActive(comboLineItemModel.getCombo().getName(), transactionModel.getTerminal());
                        comboLineItemModel.setCombo(validatedComboModel);
                        this.getPosAPIModelCache().getValidatedComboModels().add(validatedComboModel);
                    }
                }
            } else {
                throw new ComboNotFoundException("Invalid Combo submitted.", terminalId);
            }
        }
    }

    private void validateQcDiscounts() throws Exception {
        for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()) {
            if (tenderLineItemModel.getAccount() != null && tenderLineItemModel.getAccount().getQcDiscountsApplied() != null) {
                for (QcDiscountLineItemModel qcDiscountLineItemModel : tenderLineItemModel.getAccount().getQcDiscountsApplied()) {

                    if (qcDiscountLineItemModel.getQcDiscount() != null) {

                        //if the QC Discount is positive and the transaction is a Sale then flip the sign
                        switch (transactionModel.getApiActionTypeEnum()) {
                            case SALE:
                                if (qcDiscountLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == 1) {
                                    qcDiscountLineItemModel.setAmount(qcDiscountLineItemModel.getAmount().multiply(new BigDecimal(-1)));
                                    qcDiscountLineItemModel.setExtendedAmount(qcDiscountLineItemModel.getAmount().multiply(qcDiscountLineItemModel.getQuantity()));
                                }
                                break;
                        }

                        if (qcDiscountLineItemModel.getQcDiscount().getId() != null) {

                            QcDiscountModel validatedQcDiscount = this.getPosAPIModelCache().checkQcDiscountCache(qcDiscountLineItemModel.getQcDiscount().getId());
                            if (validatedQcDiscount != null) {
                                qcDiscountLineItemModel.setQcDiscount(validatedQcDiscount);
                            } else {
                                validatedQcDiscount = QcDiscountModel.getQcDiscountModel(qcDiscountLineItemModel.getQcDiscount().getId(), terminalId);
                                qcDiscountLineItemModel.setQcDiscount(validatedQcDiscount);
                                this.getPosAPIModelCache().getValidatedQcDiscountModels().add(validatedQcDiscount);
                            }
                        } else if (qcDiscountLineItemModel.getQcDiscount().getName() != null) {
                            QcDiscountModel validatedQcDiscount = this.getPosAPIModelCache().checkQcDiscountCache(qcDiscountLineItemModel.getQcDiscount().getName());
                            if (validatedQcDiscount != null) {
                                qcDiscountLineItemModel.setQcDiscount(validatedQcDiscount);
                            } else {
                                validatedQcDiscount = QcDiscountModel.getQcDiscountModel(qcDiscountLineItemModel.getQcDiscount().getName(), terminalId);
                                qcDiscountLineItemModel.setQcDiscount(validatedQcDiscount);
                                this.getPosAPIModelCache().getValidatedQcDiscountModels().add(validatedQcDiscount);
                            }
                        } else {
                            throw new MissingDataException("Invalid QC Discount information submitted.", terminalId);
                        }

                        //Check if the terminal allows QC Discount Coupons in offline mode
                        if (qcDiscountLineItemModel.getQcDiscount().getDiscountType() != null
                                && !qcDiscountLineItemModel.getQcDiscount().getDiscountType().isEmpty()
                                && qcDiscountLineItemModel.getQcDiscount().getDiscountType().equalsIgnoreCase(PosAPIHelper.DiscountType.COUPON.toString())) {

                            if (transactionModel.isOfflineMode() && !transactionModel.getTerminal().getAllowQcCouponDiscountOffline()) {
                                throw new MissingDataException("QC Coupon Discount cannot be submitted in offline mode.", terminalId);
                            }
                        }

                    } else {
                        throw new MissingDataException("Invalid QC Discount submitted: Missing Discount Details", terminalId);
                    }
                }
            }
        }
    }

    //map the Item Profile Mappings
    private void mapProductItemProfiles() throws Exception {

        for (ItemProfileMapping itemProfileMapping : transactionModel.getTerminal().getItemProfileMappings()) {

            PosAPIHelper.ObjectType objectTypeEnum = PosAPIHelper.ObjectType.convertIntToEnum(itemProfileMapping.getObjectTypeId());

            switch (objectTypeEnum) {
                case PRODUCT:
                    //products
                    for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
                        if ((itemProfileMapping.getObjectTypeId() == PosAPIHelper.ObjectType.PRODUCT.toInt()) &&
                                (productLineItemModel.getProduct().getId() == itemProfileMapping.getObjectId())) {
                            productLineItemModel.getProduct().setMappedPosItem(itemProfileMapping.getItemDefaultPosItemId());
                        }
                    }
                    break;
                case TAX:

                    for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
                        for (ItemTaxModel taxItemModel : productLineItemModel.getTaxes()) {
                            if (taxItemModel.getTax().getId() == itemProfileMapping.getObjectId()) {
                                taxItemModel.getTax().setMappedPosItem(itemProfileMapping.getItemDefaultPosItemId());
                            }
                        }
                    }
                    break;
                case POSDISCOUNT:
                    for (DiscountLineItemModel discountLineItemModel : transactionModel.getDiscounts()) {
                        if (discountLineItemModel.getDiscount().getId() == itemProfileMapping.getObjectId()) {
                            discountLineItemModel.getDiscount().setMappedPosItem(itemProfileMapping.getItemDefaultPosItemId());
                        }
                    }
                    break;
                case LOYALTYREWARD:
                    for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
                        if (loyaltyRewardLineItemModel.getReward().getId() == itemProfileMapping.getObjectId()) {
                            loyaltyRewardLineItemModel.getReward().setMappedPosItem(itemProfileMapping.getItemDefaultPosItemId());
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * For Third Party's, we support an Employee ID being saved on the DSKey
     * The Tender can be sent in with no Account information on it
     */
    private void validateEmployeeIdOnDSKey() throws Exception {

        //check if the Employee ID has been populated on the LoginModel
        if (this.getTransactionModel().getTerminal() != null
                && this.getTransactionModel().getTerminal().getLoginModel() != null
                && this.getTransactionModel().getTerminal().getLoginModel().getEmployeeId() != null
                && !this.getTransactionModel().getTerminal().getLoginModel().getEmployeeId().toString().isEmpty()) {


            //now check the tenders for one of type Quickcharge Tender
            if (this.getTransactionModel().getTenders() != null
                    && !this.getTransactionModel().getTenders().isEmpty()) {

                for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {

                    //If the Account is null or the Id, Badge, and Number are all missing
                    if ((tenderLineItemModel.getAccount() == null)

                            || ((tenderLineItemModel.getAccount().getId() == null || tenderLineItemModel.getAccount().getId().toString().isEmpty())
                            && (tenderLineItemModel.getAccount().getNumber() == null || tenderLineItemModel.getAccount().getNumber().toString().isEmpty())
                            && (tenderLineItemModel.getAccount().getBadge() == null || tenderLineItemModel.getAccount().getBadge().toString().isEmpty())
                    )
                    ) {

                        if (tenderLineItemModel.getTender() != null
                                && tenderLineItemModel.getTender().getTenderTypeName() != null
                                && !tenderLineItemModel.getTender().getTenderTypeName().isEmpty()
                                && tenderLineItemModel.getTender().getTenderTypeName().equalsIgnoreCase("QUICKCHARGE")) {

                            //Update the existing account with the authenticated employee id
                            if (tenderLineItemModel.getAccount() != null) {
                                tenderLineItemModel.getAccount().setId(this.getTransactionModel().getTerminal().getLoginModel().getEmployeeId());

                            } else {
                                AccountModel newAccountModel = new AccountModel();
                                //populate the new Account with the Employee ID from the DSKey
                                newAccountModel.setId(this.getTransactionModel().getTerminal().getLoginModel().getEmployeeId());
                                tenderLineItemModel.setAccount(newAccountModel);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * For Third Party's, we support an Employee ID being saved on the DSKey
     * The Loyalty Account and Quickcharge account must match the EmployeeID on the DSKey
     */
    private void validateSubmittedEmployeeIdToDSKey() throws Exception {

        //check if the Employee ID has been populated on the LoginModel
        if (this.getTransactionModel().getTerminal() != null
                && this.getTransactionModel().getTerminal().getLoginModel() != null
                && this.getTransactionModel().getTerminal().getLoginModel().getEmployeeId() != null
                && !this.getTransactionModel().getTerminal().getLoginModel().getEmployeeId().toString().isEmpty()) {

            //validate that the Loyalty Account matches the DSKey EmployeeId
            if (this.getTransactionModel().getLoyaltyAccount() != null &&
                    this.getTransactionModel().getLoyaltyAccount().getId() != null && !this.getTransactionModel().getLoyaltyAccount().getId().toString().isEmpty()) {

                if (!this.getTransactionModel().getTerminal().getLoginModel().getEmployeeId().equals(this.getTransactionModel().getLoyaltyAccount().getId())) {
                    throw new MissingDataException("Transaction cannot be submitted for a different account.");
                }
            }

            //now check the tenders for one of type Quickcharge Tender
            if (this.getTransactionModel().getTenders() != null
                    && !this.getTransactionModel().getTenders().isEmpty()) {

                for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {

                    //If the Account is not null
                    if (tenderLineItemModel.getAccount() != null
                            && tenderLineItemModel.getAccount().getId() != null && !tenderLineItemModel.getAccount().getId().toString().isEmpty()) {

                        if (tenderLineItemModel.getTender() != null
                                && tenderLineItemModel.getTender().getTenderTypeName() != null
                                && !tenderLineItemModel.getTender().getTenderTypeName().isEmpty()
                                && tenderLineItemModel.getTender().getTenderTypeName().equalsIgnoreCase("QUICKCHARGE")) {

                            //validate that the account on the tender match the EmployeeID from the DSKey
                            if (tenderLineItemModel.getAccount().getId() != null && !tenderLineItemModel.getAccount().getId().toString().isEmpty()) {
                                if (!tenderLineItemModel.getAccount().getId().equals(this.getTransactionModel().getTerminal().getLoginModel().getEmployeeId())) {
                                    throw new MissingDataException("Transaction cannot be submitted for a different account.");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //endregion

    //region QCPOS Validation Methods

    //Product Lines
    public void validateQCPosProductLineItems() throws Exception {

        Logger.logMessage("Validating products.  QCPosTransactionLineBuilder.validateProductLineItems.", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.DEBUG);

        //for QCPos transaction lines, there must be a PLU code
        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
            if (productLineItemModel.getProduct() != null &&
                    (productLineItemModel.getProduct().getProductCode() == null || productLineItemModel.getProduct().getProductCode().isEmpty())) {
                throw new ProductNotFoundException("Product Code (PLU) is required on all products.", terminalId);
            }
        }
    }



    //endregion



    //region Donation Validation Methods

    public void validateDonationLineItems() throws Exception {

        for (DonationLineItemModel donationLineItemModel : this.getTransactionModel().getDonations()) {
            if (donationLineItemModel.getDonation() != null) {
                if (donationLineItemModel.getDonation().getId() != null){
                    DonationModel validatedDonationModel = this.getPosAPIModelCache().checkDonationCache(donationLineItemModel.getDonation().getId());
                    if (validatedDonationModel != null) {
                        donationLineItemModel.setDonation(validatedDonationModel);
                    } else {
                        validatedDonationModel = DonationModel.getOneDonationModelById(donationLineItemModel.getDonation().getId(), transactionModel.getTerminal().getId());
                        donationLineItemModel.setDonation(validatedDonationModel);
                        this.getPosAPIModelCache().getValidatedDonationModels().add(validatedDonationModel);
                    }
                }
            } else {
                throw new DonationNotFoundException("Invalid Donation submitted.", terminalId);
            }

            //validate points or amount
            boolean isValid = false;
            if (donationLineItemModel.getDonation().getDonationTypeId() != null){
                if (donationLineItemModel.getDonation().getDonationTypeId().equals(PosAPIHelper.DonationType.LOYALTY_POINT.toInt())){
                    if(donationLineItemModel.getPoints() != null) {
                        isValid = true;
                    }
                } else if (donationLineItemModel.getDonation().getDonationTypeId().equals(PosAPIHelper.DonationType.MONETARY.toInt())){
                    if (donationLineItemModel.getAmount() != null && donationLineItemModel.getAmount().compareTo(BigDecimal.ZERO) != 0){
                        isValid = true;
                    }
                }
            } else {
                throw new DonationNotFoundException("Missing Donation Type submitted.", terminalId);
            }

            if(!isValid) {
                //throw new DonationNotFoundException("Invalid Points submitted on Donation.", terminalId);
                throw new DonationNotFoundException("Invalid Donation Line Item submitted on Donation.", terminalId);
            }

            transactionModel.setHasDonation(true);
        }
    }

    private AccountQueryParams configureAccountQueryParams(){

        boolean ignoreRedacted = false;
        boolean ignoreAccountStatus = false;

        switch (this.getTransactionModel().getApiActionTypeEnum()){
            case SALE:
                //For inquiries or sales, enforce account status and redacted status
                break;
            default:
                ignoreRedacted = true;
                ignoreAccountStatus = true;
                break;

        }

        return new AccountQueryParams(ignoreAccountStatus, ignoreRedacted);
    }

    //endregion

    //region Getters and Setters

    public TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    private enum AccountFieldAvailable {
        ID, NUMBER, BADGE, NONE
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public PosAPIModelCache getPosAPIModelCache() throws Exception {

        if (posAPIModelCache == null) {

            posAPIModelCache = new PosAPIModelCache();
        }

        return posAPIModelCache;
    }

    public void setPosAPIModelCache(PosAPIModelCache posAPIModelCache) {
        this.posAPIModelCache = posAPIModelCache;
    }

    //endregion

}
