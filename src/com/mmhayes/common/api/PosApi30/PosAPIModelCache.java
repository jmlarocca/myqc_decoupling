package com.mmhayes.common.api.PosApi30;

//api dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.combo.models.ComboModel;
import com.mmhayes.common.donation.models.DonationModel;
import com.mmhayes.common.loyalty.collections.LoyaltyProgramCollection;
import com.mmhayes.common.loyalty.models.*;
import com.mmhayes.common.product.models.PrepOptionModel;
import com.mmhayes.common.product.models.ProductModel;
import com.mmhayes.common.rotations.collections.RotatorCollection;
import com.mmhayes.common.rotations.models.RotatorModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.collections.TaxCollection;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.Logger;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-06-28 12:21:21 -0400 (Mon, 28 Jun 2021) $: Date of last commit
 $Rev: 14241 $: Revision of last commit
*/
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class PosAPIModelCache {

    ArrayList<TaxModel> validatedTaxModels = null;
    ArrayList<DiscountModel> validatedDiscountModels = null;
    ArrayList<QcDiscountModel> validatedQcDiscountModels = null;
    ArrayList<ProductModel> validatedProductModels = null;
    ArrayList<TenderModel> validatedTenderModels = null;
    ArrayList<AccountModel> validatedAccountModels = null;
    ArrayList<LoyaltyRewardModel> validatedLoyaltyRewardModels = null;
    ArrayList<LoyaltyProgramModel> validatedLoyaltyProgramModels = null;
    ArrayList<Integer> validatedKeypadIds = null;
    ArrayList<ComboModel> validatedComboModels = null;
    ArrayList<SurchargeModel> validatedSurchargeModels = null;
    ArrayList<PrepOptionModel> validatedPrepOptionModels = null;
    ArrayList<DonationModel> validatedDonationModels = null;

    public static ConcurrentHashMap<Integer, HashMap<Object, HashMap<Integer, Object>>> globalCache = new ConcurrentHashMap<Integer, HashMap<Object, HashMap<Integer, Object>>>();
    public static boolean useTerminalCache = true;

    public static ArrayList<HashMap<Integer, HashMap>> keypadsPerTerminalCache = new ArrayList<>();

    public PosAPIModelCache() throws Exception {
        validatedTaxModels = new ArrayList<>();
        validatedDiscountModels = new ArrayList<>();
        validatedQcDiscountModels = new ArrayList<>();
        validatedProductModels = new ArrayList<>();
        validatedTenderModels = new ArrayList<>();
        validatedAccountModels = new ArrayList<>();
        validatedLoyaltyRewardModels = new ArrayList<>();
        validatedLoyaltyProgramModels = new ArrayList<>();
        validatedKeypadIds = new ArrayList();
        validatedComboModels = new ArrayList<>();
        validatedSurchargeModels = new ArrayList<>();
        validatedPrepOptionModels = new ArrayList<>();
        validatedDonationModels = new ArrayList<>();
    }

    public TaxModel checkTaxCache(Integer taxId) {

        for (TaxModel taxModel : validatedTaxModels) {
            if (taxModel.getId().equals(taxId)) {
                return taxModel;
            }
        }

        return null;
    }

    public TaxModel checkTaxCache(String taxName) {

        for (TaxModel taxModel : validatedTaxModels) {
            if (taxModel.getName().equals(taxName)) {
                return taxModel;
            }
        }

        return null;
    }

    public DiscountModel checkDiscountCache(Integer discountId) {

        for (DiscountModel discountModel : validatedDiscountModels) {
            if (discountModel.getId().equals(discountId)) {
                return discountModel;
            }
        }

        return null;
    }

    public DiscountModel checkDiscountCache(String discountName) {

        for (DiscountModel discountModel : validatedDiscountModels) {
            if (discountModel.getName().equals(discountName)) {
                return discountModel;
            }
        }

        return null;
    }

    public QcDiscountModel checkQcDiscountCache(Integer qcDiscountId) {

        for (QcDiscountModel qcDiscountModel : validatedQcDiscountModels) {
            if (qcDiscountModel.getId().equals(qcDiscountId)) {
                return qcDiscountModel;
            }
        }

        return null;
    }

    public QcDiscountModel checkQcDiscountCache(String qcDiscountName) {

        for (QcDiscountModel qcDiscountModel : validatedQcDiscountModels) {
            if (qcDiscountModel.getName().equals(qcDiscountName)) {
                return qcDiscountModel;
            }
        }

        return null;
    }

    public ProductModel checkProductCache(Integer productId) {

        for (ProductModel productModel : validatedProductModels) {
            if (productModel.getId().equals(productId)) {
                return productModel.clone();
            }
        }

        return null;
    }

    public ProductModel checkProductPluCache(String pluCode) {

        for (ProductModel productModel : validatedProductModels) {
            if (productModel.getProductCode().equals(pluCode)) {
                return productModel.clone();
            }
        }

        return null;
    }

    public ProductModel checkProductCache(String productName) {

        for (ProductModel productModel : validatedProductModels) {
            if (productModel.getName().equals(productName)) {
                return productModel.clone();
            }
        }

        return null;
    }

    public ProductModel checkProductSubDepartmentCache(Integer subDepartmentId) {

        for (ProductModel productModel : validatedProductModels) {
            if (productModel.getSubDepartmentId().equals(subDepartmentId)) {
                return productModel;
            }
        }

        return null;
    }

    public ProductModel checkProductSubDepartmentCache(String subDepartmentName) {

        for (ProductModel productModel : validatedProductModels) {
            if (productModel.getSubDepartment().equals(subDepartmentName)) {
                return productModel;
            }
        }

        return null;
    }

    public ProductModel checkProductDepartmentCache(String departmentName) {

        for (ProductModel productModel : validatedProductModels) {
            if (productModel.getDepartment().equals(departmentName)) {
                return productModel;
            }
        }

        return null;
    }

    public TenderModel checkTenderCache(Integer tenderId) {

        for (TenderModel tenderModel : validatedTenderModels) {
            if (tenderModel.getId().equals(tenderId)) {
                return tenderModel;
            }
        }

        return null;
    }

    public TenderModel checkTenderCache(String tenderName) {

        for (TenderModel tenderModel : validatedTenderModels) {
            if (tenderModel.getName().equals(tenderName)) {
                return tenderModel;
            }
        }

        return null;
    }

    public AccountModel checkAccountCache(Integer accountId) {

        for (AccountModel accountModel : validatedAccountModels) {
            if (accountModel.getId().equals(accountId)) {
                return accountModel;
            }
        }

        return null;
    }

    public AccountModel checkAccountCache(String number, boolean isAccountNumber) {

        for (AccountModel accountModel : validatedAccountModels) {
            if (accountModel.getNumber().equals(number)) {
                return accountModel;
            }
        }

        return null;
    }

    public AccountModel checkAccountCache(String badgeNumber) {

        for (AccountModel accountModel : validatedAccountModels) {
            if (accountModel.getBadge().equals(badgeNumber)) {
                return accountModel;
            }
        }

        return null;
    }

    public LoyaltyRewardModel checkLoyaltyRewardCache(Integer loyaltyRewardId) {

        for (LoyaltyRewardModel loyaltyRewardModel : validatedLoyaltyRewardModels) {
            if (loyaltyRewardModel.getId().equals(loyaltyRewardId)) {
                return loyaltyRewardModel;
            }
        }

        return null;
    }

    public LoyaltyRewardModel checkLoyaltyRewardCache(String loyaltyRewardName) {

        for (LoyaltyRewardModel loyaltyRewardModel : validatedLoyaltyRewardModels) {
            if (loyaltyRewardModel.getName().equals(loyaltyRewardName)) {
                return loyaltyRewardModel;
            }
        }

        return null;
    }

    public SurchargeModel checkSurchargeCache(Integer surchargeId) {

        for (SurchargeModel surchargeModel : validatedSurchargeModels) {
            if (surchargeModel.getId().equals(surchargeId)) {
                return surchargeModel;
            }
        }

        return null;
    }

    public SurchargeModel checkSurchargeCache(String surchargeName) {

        for (SurchargeModel surchargeModel : validatedSurchargeModels) {
            if (surchargeModel.getName().equals(surchargeName)) {
                return surchargeModel;
            }
        }

        return null;
    }

    public LoyaltyProgramModel checkLoyaltyProgramCache(Integer loyaltyProgramId) {

        for (LoyaltyProgramModel loyaltyProgramModel : validatedLoyaltyProgramModels) {
            if (loyaltyProgramModel.getId().equals(loyaltyProgramId)) {
                return loyaltyProgramModel;
            }
        }

        return null;
    }

    public LoyaltyProgramModel checkLoyaltyProgramCache(String loyaltyProgramName) {

        for (LoyaltyProgramModel loyaltyProgramModel : validatedLoyaltyProgramModels) {
            if (loyaltyProgramModel.getName().equals(loyaltyProgramName)) {
                return loyaltyProgramModel;
            }
        }

        return null;
    }

    public boolean checkKeypadIdIsCached(Integer keypadId){
        boolean result = false;
        for (Integer validatedKeypadId : validatedKeypadIds){
            if (keypadId.equals(validatedKeypadId)){
                return true;
            }
        }
        return result;
    }

    public ComboModel checkComboCache(Integer comboId) {

        for (ComboModel comboModel : validatedComboModels) {
            if (comboModel.getId().equals(comboId)) {
                return comboModel;
            }
        }

        return null;
    }

    public ComboModel checkComboCache(String comboName) {

        for (ComboModel comboModel : validatedComboModels) {
            if (comboModel.getName().equals(comboName)) {
                return comboModel;
            }
        }

        return null;
    }

    public PrepOptionModel checkPrepOptionCache(Integer prepOptionId, Integer prepOptionSetId) {

        for (PrepOptionModel prepOptionModel : validatedPrepOptionModels) {
            if (prepOptionModel.getId().equals(prepOptionId) && prepOptionModel.getPrepOptionSetId().equals(prepOptionSetId)) {
                return prepOptionModel;
            }
        }

        return null;
    }

    public DonationModel checkDonationCache(Integer donationId) {

        for (DonationModel donationModel : validatedDonationModels) {
            if (donationModel.getId().equals(donationId)) {
                return donationModel;
            }
        }

        return null;
    }

    public ArrayList<TaxModel> getValidatedTaxModels() {
        return validatedTaxModels;
    }

    public void setValidatedTaxModels(ArrayList<TaxModel> validatedTaxModels) {
        this.validatedTaxModels = validatedTaxModels;
    }

    public ArrayList<DiscountModel> getValidatedDiscountModels() {
        return validatedDiscountModels;
    }

    public void setValidatedDiscountModels(ArrayList<DiscountModel> validatedDiscountModels) {
        this.validatedDiscountModels = validatedDiscountModels;
    }

    public ArrayList<QcDiscountModel> getValidatedQcDiscountModels() {
        return validatedQcDiscountModels;
    }

    public void setValidatedQcDiscountModels(ArrayList<QcDiscountModel> validatedQcDiscountModels) {
        this.validatedQcDiscountModels = validatedQcDiscountModels;
    }

    public ArrayList<ProductModel> getValidatedProductModels() {
        return validatedProductModels;
    }

    public void setValidatedProductModels(ArrayList<ProductModel> validatedProductModels) {
        this.validatedProductModels = validatedProductModels;
    }

    public ArrayList<TenderModel> getValidatedTenderModels() {
        return validatedTenderModels;
    }

    public void setValidatedTenderModels(ArrayList<TenderModel> validatedTenderModels) {
        this.validatedTenderModels = validatedTenderModels;
    }

    public ArrayList<AccountModel> getValidatedAccountModels() {
        return validatedAccountModels;
    }

    public void setValidatedAccountModels(ArrayList<AccountModel> validatedAccountModels) {
        this.validatedAccountModels = validatedAccountModels;
    }

    public ArrayList<LoyaltyRewardModel> getValidatedLoyaltyRewardModels() {
        return validatedLoyaltyRewardModels;
    }

    public void setValidatedLoyaltyRewardModels(ArrayList<LoyaltyRewardModel> validatedLoyaltyRewardModels) {
        this.validatedLoyaltyRewardModels = validatedLoyaltyRewardModels;
    }

    public ArrayList<LoyaltyProgramModel> getValidatedLoyaltyProgramModels() {
        return validatedLoyaltyProgramModels;
    }

    public void setValidatedLoyaltyProgramModels(ArrayList<LoyaltyProgramModel> validatedLoyaltyProgramModels) {
        this.validatedLoyaltyProgramModels = validatedLoyaltyProgramModels;
    }

    public ArrayList<Integer> getValidatedKeypadIds() {
        return validatedKeypadIds;
    }

    public void setValidatedKeypadIds(ArrayList<Integer> validatedKeypadIds) {
        this.validatedKeypadIds = validatedKeypadIds;
    }

    public ArrayList<ComboModel> getValidatedComboModels() {
        return validatedComboModels;
    }

    public void setValidatedComboModels(ArrayList<ComboModel> validatedComboModels) {
        this.validatedComboModels = validatedComboModels;
    }

    public ArrayList<SurchargeModel> getValidatedSurchargeModels() {
        return validatedSurchargeModels;
    }

    public void setValidatedSurchargeModels(ArrayList<SurchargeModel> validatedSurchargeModels) {
        this.validatedSurchargeModels = validatedSurchargeModels;
    }

    public ArrayList<PrepOptionModel> getValidatedPrepOptionModels() {
        return validatedPrepOptionModels;
    }

    public void setValidatedPrepOptionModels(ArrayList<PrepOptionModel> validatedPrepOptionModels) {
        this.validatedPrepOptionModels = validatedPrepOptionModels;
    }

    public ArrayList<DonationModel> getValidatedDonationModels() {
        return validatedDonationModels;
    }

    public void setValidatedDonationModels(ArrayList<DonationModel> validatedDonationModels) {
        this.validatedDonationModels = validatedDonationModels;
    }

    public static void buildGlobalCache(TerminalModel terminalModel) throws Exception {
        HashMap<Object, HashMap<Integer, Object>> terminalCache = new HashMap<Object, HashMap<Integer, Object>>();
        globalCache.remove(terminalModel.getId());
        globalCache.put(terminalModel.getId(), terminalCache); //clear out cache for this request

        //Loyalty Programs
        PosAPIModelCache.setLoyaltyProgramCache(terminalModel.getId(), CommonAPI.PosModelType.LOYALTY_PROGRAM, LoyaltyProgramCollection.getAllLoyaltyProgramsForCache(terminalModel).getCollection());
        //Taxes
        PosAPIModelCache.setTaxCache(terminalModel.getId(), CommonAPI.PosModelType.TAX, TaxCollection.getAllTaxes(terminalModel).getCollection());

        //added for load testing - the size should always be 2 even if there are no taxes or loyalty programs - sometimes this wouldn't happen during load testing
        if(globalCache.get(terminalModel.getId()).size() < 2) {
            buildGlobalCache(terminalModel);
        }
    }

    //Object object, Integer objectId
    public static void setLoyaltyProgramCache(Integer terminalId, CommonAPI.PosModelType objectEnum, List<LoyaltyProgramModel> modelList) {
        HashMap<Integer, Object> objectHM = new HashMap<Integer, Object>();
        HashMap<Object, HashMap<Integer, Object>> terminalCache = new HashMap<Object, HashMap<Integer, Object>>();
        Class objectClass = modelList.getClass();

        switch (objectEnum) {
            case LOYALTY_PROGRAM:

                //if (objectClass.isInstance(ArrayList.class)){
                for (LoyaltyProgramModel loyaltyProgram : modelList) {
                    objectHM.put(loyaltyProgram.getId(), loyaltyProgram);
                }
                break;
        }

        if (globalCache.containsKey(terminalId)){ //add to the cache, don't just overwrite it
            HashMap<Object, HashMap<Integer, Object>> cachePerTerminal = globalCache.get(terminalId);
            cachePerTerminal.put(objectEnum, objectHM);
            globalCache.put(terminalId, cachePerTerminal);

        } else {
            terminalCache.put(objectEnum, objectHM);
            globalCache.put(terminalId, terminalCache);
        }
    }

    public static void setComboCache(Integer terminalId, CommonAPI.PosModelType objectEnum, List<ComboModel> combos) {
        HashMap<Integer, Object> objectHM = new HashMap<Integer, Object>();
        HashMap<Object, HashMap<Integer, Object>> terminalCache = new HashMap<Object, HashMap<Integer, Object>>();

        switch (objectEnum) {
            case COMBO:

                //if (objectClass.isInstance(ArrayList.class)){
                for (ComboModel comboModel : combos) {
                    objectHM.put(comboModel.getId(), comboModel);
                }
                break;
        }

        if (globalCache.containsKey(terminalId)){ //add to the cache, don't just overwrite it
            HashMap<Object, HashMap<Integer, Object>> cachePerTerminal = globalCache.get(terminalId);
            cachePerTerminal.put(objectEnum, objectHM);
            globalCache.put(terminalId, cachePerTerminal);

        } else {
            terminalCache.put(objectEnum, objectHM);
            globalCache.put(terminalId, terminalCache);
        }
    }

    public static void setTaxCache(Integer terminalId, CommonAPI.PosModelType objectEnum, List<TaxModel> taxes) {
        HashMap<Integer, Object> objectHM = new HashMap<Integer, Object>();
        HashMap<Object, HashMap<Integer, Object>> terminalCache = new HashMap<Object, HashMap<Integer, Object>>();

        switch (objectEnum) {
            case TAX:

                //if (objectClass.isInstance(ArrayList.class)){
                for (TaxModel taxModel : taxes) {
                    objectHM.put(taxModel.getId(), taxModel);
                }
                break;
        }

        if (globalCache.containsKey(terminalId)){ //add to the cache, don't just overwrite it
            HashMap<Object, HashMap<Integer, Object>> cachePerTerminal = globalCache.get(terminalId);
            cachePerTerminal.put(objectEnum, objectHM);
            globalCache.put(terminalId, cachePerTerminal);

        } else {
            terminalCache.put(objectEnum, objectHM);
            globalCache.put(terminalId, terminalCache);
        }

    }

    public static HashMap<Object, HashMap<Integer, Object>> getTerminalCache(Integer terminalId) {
        HashMap<Object, HashMap<Integer, Object>> cachePerTerminal = globalCache.get(terminalId);
        return cachePerTerminal;
    }

    //get all the cache for 1 type of object
    //ie - all Loyalty Programs for terminal 4
    public static HashMap<Integer, Object> getObjectCache(Integer terminalId, CommonAPI.PosModelType objectEnum) {
        HashMap<Object, HashMap<Integer, Object>> cachePerTerminal = globalCache.get(terminalId);
        HashMap<Integer, Object> loyaltyProgramCache = cachePerTerminal.get(objectEnum);
        return loyaltyProgramCache;
    }

    public static Object getOneObject(Integer terminalId, CommonAPI.PosModelType objectEnum, Integer objectId) {
        HashMap<Object, HashMap<Integer, Object>> cachePerTerminal = globalCache.get(terminalId);
        HashMap<Integer, Object> loyaltyProgramCache = cachePerTerminal.get(objectEnum);

        //added for load testing - unusual behavior - sometimes before going into the for loop it says cachePerTerminal size is 1, but after it says it is 2
        if(loyaltyProgramCache == null && cachePerTerminal.size() > 0) {
              for ( Map.Entry<Object, HashMap<Integer, Object>> entry: cachePerTerminal.entrySet() ) {
                String key = entry.getKey().toString();
                if(key.equals(objectEnum.toString())) {
                    loyaltyProgramCache = entry.getValue();
                    break;
                }
            }
        }

        //added for load testing - even after the for loop the size of cachePerTerminal would sometimes still be 1 - but when it hit this if statement it would be 2
        if(loyaltyProgramCache == null) {
            loyaltyProgramCache = cachePerTerminal.get(objectEnum);
        }

        //if the loyaltyProgramCache is still null at this point, return null
        if(loyaltyProgramCache == null) {
            return null;
        }

        Object loyaltyProgramModelOut = loyaltyProgramCache.get(objectId);
        return loyaltyProgramModelOut;
    }

    // BEGIN ROTATOR CACHE FUNCTIONS

    //create the rotation cache for the terminal
    public static void buildRotationCache(Integer terminalId, boolean usingStoreID) throws Exception {

        //if the global cache does not exist for this terminal, initialize it
        if( !(globalCache.size() > 0 && globalCache.containsKey(terminalId)) ) {
            HashMap<Object, HashMap<Integer, Object>> terminalCache = new HashMap<Object, HashMap<Integer, Object>>();
            globalCache.put(terminalId, terminalCache); //clear out cache for this request
        }

        //if using storeID, then in MyQC
        if(usingStoreID) {
            PosAPIModelCache.setRotatorCache(terminalId, CommonAPI.PosModelType.ROTATION, RotatorCollection.getAllRotationsForCache(terminalId, true));
        } else { //otherwise get rotations by terminalID
            PosAPIModelCache.setRotatorCache(terminalId, CommonAPI.PosModelType.ROTATION, RotatorCollection.getAllRotationsForCache(terminalId));
        }
    }

    //check if the store contains a rotation cache
    public static boolean checkCacheForRotations(Integer terminalId, boolean usingStoreID) throws Exception {

        //check if the global cache has been initialized and if it contains the terminal ID
        if(globalCache.size() > 0 && globalCache.containsKey(terminalId)) {
            HashMap<Object, HashMap<Integer, Object>> cachePerTerminal = globalCache.get(terminalId);
            HashMap<Integer, Object> rotatorCache = cachePerTerminal.get(CommonAPI.PosModelType.ROTATION);

            if(rotatorCache == null) {
                return false;
            }

            //if in MyQC - check if there was a change made to a rotation and the rotation collection needs to be updated
            if(usingStoreID) {
                RotatorCollection rotatorCollection = (RotatorCollection) rotatorCache.get(0);
                return !rotatorCollection.checkForUpdatesByStoreID(terminalId);
            }

            return true;
        }
        return false;
    }

    //returns a list of RotationModels if the keypadID matches the controllingKeypadID for a rotation in the store
    public static List<RotatorModel> getRotationFromCache(Integer terminalId, Integer keypadID, boolean usingStoreID) throws Exception{
        if( !checkCacheForRotations(terminalId, usingStoreID) ) {
            PosAPIModelCache.buildRotationCache(terminalId, usingStoreID);
        }

        RotatorCollection rotatorCollection = (RotatorCollection) getOneObject(terminalId, CommonAPI.PosModelType.ROTATION, 0);
        return getRotationFromCacheByKeypadID(rotatorCollection, keypadID);
    }

    //set the rotatorCollection in the rotatorCache
    public static void setRotatorCache(Integer terminalId, CommonAPI.PosModelType objectEnum, RotatorCollection rotationCollection) {
        HashMap<Integer, Object> objectHM = new HashMap<Integer, Object>();
        HashMap<Object, HashMap<Integer, Object>> rotationCache = new HashMap<Object, HashMap<Integer, Object>>();

        switch (objectEnum) {
            case ROTATION:

                //put the collection itself in the cache at key "0"
                objectHM.put(0, rotationCollection);

                break;
        }

        if (globalCache.containsKey(terminalId)){ //add to the cache, don't just overwrite it
            HashMap<Object, HashMap<Integer, Object>> cachePerRotation = globalCache.get(terminalId);
            cachePerRotation.put(objectEnum, objectHM);
            globalCache.put(terminalId, cachePerRotation);

        } else {
            rotationCache.put(objectEnum, objectHM);
            globalCache.put(terminalId, rotationCache);
        }
    }

    //get the RotatorModel from the cache by the controllerKeypadID
    public static List<RotatorModel> getRotationFromCacheByKeypadID(RotatorCollection rotatorCollection, Integer keypadID) {
        List<RotatorModel> validRotations = new ArrayList<>();

        for(RotatorModel rotatorModel: rotatorCollection.getCollection()) {
            //find the first matching rotation and returns it
            if(rotatorModel.getControlledPAKeypadID().equals(keypadID)) {
                validRotations.add(rotatorModel);
            }
        }

        return validRotations;
    }

    // END ROTATOR CACHE FUNCTIONS

    //check if the cache contains a record for the terminal and if the terminal record is not null
    public static boolean checkCacheForTerminalProducts(Integer terminalId) {

        //check if the cache has been initialized and if it contains the terminal ID
        if(keypadsPerTerminalCache.size() > 0) {
            for(HashMap terminalGroup : keypadsPerTerminalCache) {
                if(terminalGroup.containsKey(terminalId)) {
                    HashMap terminalCache = (HashMap) terminalGroup.get(terminalId);

                    return terminalCache != null && !checkKeypadsPerTerminalCacheForUpdates(terminalCache);
                }
            }
        }

        return false;
    }

    //returns a list of RotationModels if the keypadID matches the controllingKeypadID for a rotation in the store
    public static HashMap getKeypadsByTerminalFromCache(Integer terminalId, Integer homeKeypadID)  {
        if( !checkCacheForTerminalProducts(terminalId) ) {
            PosAPIModelCache.buildKeypadsPerTerminalCache(terminalId, homeKeypadID);
        }

        for(HashMap terminalGroup : keypadsPerTerminalCache) {
            if(terminalGroup.containsKey(terminalId)) {
                return (HashMap) terminalGroup.get(terminalId);
            }
        }

        return new HashMap();
    }

    public static void buildKeypadsPerTerminalCache(Integer terminalID, Integer homeKeypadID) {

        ArrayList<String> keypadsInStore = CommonAPI.determineAllKeypadsInStore(terminalID, homeKeypadID);
        String keypadList = String.join(",", keypadsInStore);

        String lastUpdateDTM = CommonAPI.getLastUpdateDTMForKeyapds(keypadList);

        LocalDateTime lastUpdate = LocalDateTime.now();
        String lastUpdateCheck = lastUpdate.toString().replace("T", " ");

        SimpleDateFormat inFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        SimpleDateFormat outFormatter = new SimpleDateFormat("M/dd/yyyy h:mm:ss a");
        try {
            //convert AM/PM time string to date object then format the date object to military time string
            lastUpdateDTM = outFormatter.format(inFormatter.parse(lastUpdateDTM));
            lastUpdateCheck = outFormatter.format(inFormatter.parse(lastUpdateCheck));
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        HashMap terminalHM = new HashMap();
        HashMap productsHM = new HashMap();

        productsHM.put("keypadList", keypadList);
        productsHM.put("lastUpdateDTM", lastUpdateDTM);
        productsHM.put("lastUpdateCheck", lastUpdateCheck);

        terminalHM.put(terminalID, productsHM);

        keypadsPerTerminalCache.add(terminalHM);
    }

    public static boolean checkKeypadsPerTerminalCacheForUpdates(HashMap terminalCache) {
        String lastUpdateDtm = CommonAPI.convertModelDetailToString(terminalCache.get("lastUpdateDTM"));
        String lastUpdateChk = CommonAPI.convertModelDetailToString(terminalCache.get("lastUpdateCheck"));
        String dateTimeOutFormat = "M/dd/yyyy h:mm:ss a";
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeOutFormat);
        LocalDateTime lastUpdateDTM = LocalDateTime.parse(lastUpdateDtm, dateTimeFormatter);
        LocalDateTime lastUpdateCheck = LocalDateTime.parse(lastUpdateChk, dateTimeFormatter);

        String keypads = CommonAPI.convertModelDetailToString(terminalCache.get("keypadList"));

        return CommonAPI.checkForUpdates(keypads, lastUpdateDTM, lastUpdateCheck);
    }

}
