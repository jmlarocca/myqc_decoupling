package com.mmhayes.common.api.PosApi30;

//MMHayes dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.OtmModel;
import com.mmhayes.common.utils.Logger;

//Other dependencies
import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;

/*
 $Author: nyu $: Author of last commit
 $Date: 2021-08-16 18:47:43 -0400 (Mon, 16 Aug 2021) $: Date of last commit
 $Rev: 14891 $: Revision of last commit
 Notes:
*/
public class PosAPIHelper {
    private static HashMap<Integer, String> logFileCache = new HashMap<>();

    public enum ObjectType {
        PRODUCT(2), TENDER(3), TAX(6), POSDISCOUNT(7), PAIDOUT(10), RECEIVEDONACCOUNT(11), TAX_DELETE(12), LOYALTYREWARD(30), SURCHARGE(32), PURCHASE_CATEGORY(96), MODIFIER(97), QCDISCOUNT(99), COMBO(36), DONATION(38);

        private int value;

        private ObjectType(int value) {
            this.value = value;
        }

        public int toInt() {
            return value;
        }

        public static ObjectType convertStringToEnum(String objectTypeString) {
            switch (objectTypeString) {
                case "QC POS Product":
                    return PRODUCT;
                case "POS Discount":
                    return POSDISCOUNT;
                case "TAX":
                case "Tax":
                    return TAX;
                case "Received on Account":
                    return RECEIVEDONACCOUNT;
                case "Tender":
                    return TENDER;
                case "Tax Delete":
                case "TAX_DELETE":
                    return TAX_DELETE;
                case "Paid Out":
                    return PAIDOUT;
                case "Loyalty Reward":
                    return LOYALTYREWARD;
                case "Surcharge":
                    return SURCHARGE;
                case "Modifier":
                    return MODIFIER;
                case "QC Discount":
                    return QCDISCOUNT;
                case "Purchase Category":
                    return PURCHASE_CATEGORY;
                case "Combos":
                case "Combo":
                    return COMBO;
                case "Donations":
                case "Donation":
                    return DONATION;
                default:
                    return PRODUCT;
            }
        }

        //PURCHASE_CATEGORY(96),  MODIFIER(97), QCDISCOUNT(99);

        public static ObjectType convertIntToEnum(int objectTypeInt) {
            switch (objectTypeInt) {
                case 2:
                    return PRODUCT;
                case 3:
                    return TENDER;
                case 6:
                    return TAX;
                case 7:
                    return POSDISCOUNT;
                case 10:
                    return PAIDOUT;
                case 11:
                    return RECEIVEDONACCOUNT;
                case 12:
                    return TAX_DELETE;
                case 30:
                    return LOYALTYREWARD;
                case 32:
                    return SURCHARGE;
                case 36:
                    return COMBO;
                case 38:
                    return DONATION;
                case 96:
                    return PURCHASE_CATEGORY;
                case 97:
                    return MODIFIER;
                case 99:
                    return QCDISCOUNT;
                default:
                    return PRODUCT;
            }
        }

    }

    //public static final Integer OBJECT_TYPE_TAX = 6;
    //public static final Integer OBJECT_TYPE_POS_DISCOUNT = 7;
    //public static final Integer OBJECT_TYPE_PAID_OUT = 10;

    public static enum OrderType {
        NORMALSALE(1, "Normal Sale"),
        DELIVERYSALE(2, "Delivery Sale"),
        PICKUPSALE(3, "Pickup Sale"),
        OTHER(99, "Other");

        private int value;
        private String name;

        private OrderType(int value, String name) {
            this.value = value;
            this.name = name;
        }

        public int toInt() {
            return value;
        }

        public String getName() {
            return name;
        }

        public static String convertIntToName(int orderTypeInt) {
            switch (orderTypeInt) {
                case 1:
                    return NORMALSALE.getName();
                case 2:
                    return DELIVERYSALE.getName();
                case 3:
                    return PICKUPSALE.getName();
                default:
                    return OTHER.getName();
            }
        }

        public static Integer convertStringToInt(String orderTypeName) {

            if (orderTypeName == null || orderTypeName.isEmpty()) {
                return NORMALSALE.toInt();
            } else {
                String orderTypeUpper = orderTypeName.toUpperCase();

                switch (orderTypeUpper) {
                    case "DELIVERY SALE":
                    case "DELIVERY":
                        return DELIVERYSALE.toInt();
                    case "PICKUP SALE":
                    case "PICKUP":
                        return PICKUPSALE.toInt();
                    case "NORMAL SALE":
                    case "NORMAL":
                        return NORMALSALE.toInt();
                    default:
                        return OTHER.toInt();
                }
            }
        }
    }

    public static String logFileName = "POSAPI_COMMON_T.log";

    public static enum ApiActionType {
        NONE(0, "None"),
        SALE(1, "Sale"),
        VOID(2, "Void"),
        REFUND(3, "Refund"),
        TRAINING(4, "Training"),
        CANCEL(5, "Cancel"),
        CASHIERSHIFTEND(6, "Cashier Shift End"),
        NOSALE(7, "No Sale"),
        BROWSERCLOSE(8, "Browser Close"),
        LOGIN(9, "Login"),
        LOGOUT(10, "Logout"),
        ROA(11, "Received On Account"),
        PO(12, "Paid Out"),
        BATCHCLOSE(13, "Batch Close"),
        LOYALTY_ADJUSTMENT(14, "Loyalty Adjustment"),
        OPEN(15, "Open"),
        PAYMENT_GATEWAY_INQUIRE(16, "Payment Gateway Inquire"),
        REFUND_MANUAL(95, "Manual Refund");

        private int value;
        private String name;

        //takes 2 default parameters
        //param 1= the int value of the enum
        //param 2= the friendly name of the enum
        private ApiActionType(int value, String name) {
            this.value = value;
            this.name = name;
        }

        public int toInt() {
            return value;
        }

        public String getName() {
            return name;
        }

        public static ApiActionType intToTransactionType(int value) {
            switch (value) {
                case 1:
                    return SALE;
                case 2:
                    return VOID;
                case 3:
                    return REFUND;
                case 4:
                    return TRAINING;
                case 5:
                    return CANCEL;
                case 6:
                    return CASHIERSHIFTEND;
                case 7:
                    return NOSALE;
                case 8:
                    return BROWSERCLOSE;
                case 9:
                    return LOGIN;
                case 10:
                    return LOGOUT;
                case 11:
                    return ROA;
                case 12:
                    return PO;
                case 13:
                    return BATCHCLOSE;
                case 14:
                    return LOYALTY_ADJUSTMENT;
                case 15:
                    return OPEN;
                case 16:
                    return PAYMENT_GATEWAY_INQUIRE;
                case 95:
                    return REFUND_MANUAL;
                default:
                    return SALE;
            }
        }
    }

    public static enum TransactionType {
        NONE(0, "None"),
        SALE(1, "Sale"),
        VOID(2, "Void"),
        REFUND(3, "Refund"),
        TRAINING(4, "Training"),
        CANCEL(5, "Cancel"),
        CASHIERSHIFTEND(6, "Cashier Shift End"),
        NOSALE(7, "No Sale"),
        BROWSERCLOSE(8, "Browser Close"),
        LOGIN(9, "Login"),
        LOGOUT(10, "Logout"),
        ROA(11, "Received On Account"),
        PO(12, "Paid Out"),
        BATCHCLOSE(13, "Batch Close"),
        LOYALTY_ADJUSTMENT(14, "Loyalty Adjustment"),
        OPEN(15, "Open"),
        PAYMENT_GATEWAY_INQUIRE(16, "Payment Gateway Inquire");

        private int value;
        private String name;

        //takes 2 default parameters
        //param 1= the int value of the enum
        //param 2= the friendly name of the enum
        private TransactionType(int value, String name) {
            this.value = value;
            this.name = name;
        }

        public int toInt() {
            return value;
        }

        public String getName() {
            return name;
        }

        public static TransactionType intToTransactionType(int value) {
            switch (value) {
                case 1:
                    return SALE;
                case 2:
                    return VOID;
                case 3:
                    return REFUND;
                case 4:
                    return TRAINING;
                case 5:
                    return CANCEL;
                case 6:
                    return CASHIERSHIFTEND;
                case 7:
                    return NOSALE;
                case 8:
                    return BROWSERCLOSE;
                case 9:
                    return LOGIN;
                case 10:
                    return LOGOUT;
                case 11:
                    return ROA;
                case 12:
                    return PO;
                case 13:
                    return BATCHCLOSE;
                case 14:
                    return LOYALTY_ADJUSTMENT;
                case 15:
                    return OPEN;
                case 16:
                    return PAYMENT_GATEWAY_INQUIRE;
                default:
                    return SALE;
            }
        }

        //use this custom method instead of valueOf
        public static TransactionType stringToTransactionType(String str) {

       /*     if (str != null && !str.isEmpty()) {
                str = "";  //this will default it to SALE if nothing is sent in
            }*/

            str = str.toUpperCase();

            switch (str) {
                case "SALE":
                    return SALE;
                case "VOID":
                    return VOID;
                case "REFUND":
                    return REFUND;
                case "TRAINING":
                    return TRAINING;
                case "CANCEL":
                    return CANCEL;
                case "CASHIERSHIFTEND":
                case "CASHIER SHIFT END":
                    return CASHIERSHIFTEND;
                case "NOSALE":
                    return NOSALE;
                case "BROWSERCLOSE":
                case "BROWSER CLOSE":
                    return BROWSERCLOSE;
                case "LOGIN":
                case "LOG IN":
                    return LOGIN;
                case "LOGOUT":
                case "LOG OUT":
                    return LOGOUT;
                case "ROA":
                case "RECEIVED ON ACCOUNT":
                    return ROA;
                case "PO":
                case "PAID OUT":
                    return PO;
                case "BATCHCLOSE":
                case "BATCH CLOSE":
                    return BATCHCLOSE;
                case "LOYALTY_ADJUSTMENT":
                case "LOYALTY ADJUSTMENT":
                    return LOYALTY_ADJUSTMENT;
                case "OPEN":
                    return OPEN;
                case "PAYMENT GATEWAY INQUIRE":
                    return PAYMENT_GATEWAY_INQUIRE;
                default:
                    return SALE;
            }
        }
    }

    public static enum LoyaltyProgramType {
        WELLNESS(1), DOLLAR(2), PRODUCT(3), NONE(-1);

        private int id;

        private LoyaltyProgramType(int id){
            this.id = id;
        }

        public static LoyaltyProgramType StringToEnum(String stringValue) {
            switch (stringValue.toUpperCase()) {
                case "DOLLAR":
                    return DOLLAR;
                case "PRODUCT LEVEL":
                case "PRODUCT":
                    return PRODUCT;
                case "WELLNESS":
                    return WELLNESS;
            }
            return null;
        }

        public static LoyaltyProgramType intToLoyaltyProgramType(Integer intValue) {

            if (intValue == null){
                return NONE;
            }

            for (LoyaltyProgramType loyaltyProgramType : LoyaltyProgramType.values()){

                if (intValue.equals(loyaltyProgramType.id)){
                    return loyaltyProgramType;
                }
            }

            return NONE;
        }
    }

    public static enum LoyaltyAccrualPolicyType {
        WELLNESS(1), DOLLAR(2), PRODUCT(3), NONE(-1);

        private int id;

        private LoyaltyAccrualPolicyType(int id){
            this.id = id;
        }

        public static LoyaltyAccrualPolicyType StringToEnum(String stringValue) {
            switch (stringValue.toUpperCase()) {
                case "DOLLAR":
                    return DOLLAR;
                case "PRODUCT LEVEL":
                case "PRODUCT":
                    return PRODUCT;
                case "WELLNESS":
                    return WELLNESS;
            }
            return null;
        }

        public static LoyaltyAccrualPolicyType intToLoyaltyAccrualPolicyType(Integer intValue) {

            if (intValue == null){
                return NONE;
            }

            for (LoyaltyAccrualPolicyType loyaltyAccrualPolicyType : LoyaltyAccrualPolicyType.values()){

                if (intValue.equals(loyaltyAccrualPolicyType.id)){
                    return loyaltyAccrualPolicyType;
                }
            }

            return NONE;
        }

        public Integer toInt(){
            return id;
        }
    }


    public static enum LoyaltyMonetaryPrecisionType {
        DOLLAR, DIME, PENNY;

        public static LoyaltyMonetaryPrecisionType fromString(String stringValue) {
            switch (Integer.parseInt(stringValue)) {
                case 1:
                    return DOLLAR;
                case 2:
                    return DIME;
                case 3:
                    return PENNY;
            }
            return null;
        }

        public static LoyaltyMonetaryPrecisionType intToLoyaltyMonetaryPrecisionType(Integer intValue) {
            switch (intValue) {
                case 1:
                    return DOLLAR;
                case 2:
                    return DIME;
                case 3:
                    return PENNY;
                default:
                    return PENNY;
            }
        }
    }

    public static enum LoyaltyMonetaryRoundingType {
        STANDARD, UP, DOWN, HALF_UP, HALF_DOWN;

        public static LoyaltyMonetaryRoundingType StringToEnum(String stringValue) {
            switch (stringValue.toUpperCase()) {
                case "STANDARD":
                case "HALF UP":
                    return HALF_UP;
                case "UP":
                    return UP;
                case "DOWN":
                    return DOWN;
                case "HALF DOWN":
                    return HALF_DOWN;
                default:
                    return HALF_UP;
            }
        }

        public static LoyaltyMonetaryRoundingType intToLoyaltyMonetaryRoundingType(Integer intValue) {
            switch (intValue) {
                case 1:
                    return HALF_UP;
                case 2:
                    return UP;
                case 3:
                    return DOWN;
                case 4:
                    return HALF_DOWN;
                default:
                    return HALF_UP;
            }
        }
    }

    public static enum LoyaltyPointRoundingType {
        STANDARD, UP, DOWN, HALF_UP, HALF_DOWN;

        public static LoyaltyPointRoundingType StringToEnum(String stringValue) {
            switch (stringValue.toUpperCase()) {
                case "STANDARD":
                case "HALF UP":
                    return HALF_UP;
                case "UP":
                    return UP;
                case "DOWN":
                    return DOWN;
                case "HALF DOWN":
                    return HALF_DOWN;
                default:
                    return HALF_UP;
            }
        }

        public static LoyaltyPointRoundingType intToLoyaltyPointRoundingType(Integer intValue) {
            switch (intValue) {
                case 1:
                    return HALF_UP;
                case 2:
                    return UP;
                case 3:
                    return DOWN;
                case 4:
                    return HALF_DOWN;
                default:
                    return HALF_UP;
            }
        }
    }

    public static enum LoyaltyLevelItemType {
        PLU(1), SUBDEPARTMENT(2), DEPARTMENT(3);

        private int id;

        private LoyaltyLevelItemType(int id) {
            this.id = id;

        }

        public int toInt() {
            return this.id;
        }

    }

    public static enum LoyaltyLevelEarningType {
        NONE(-1), POINT_PER_PRODUCT(1), PRICE_OF_PRODUCT(2);

        private int id;

        private LoyaltyLevelEarningType(int id) {
            this.id = id;
        }

        public int toInt() {
            return id;
        }

        public static LoyaltyLevelEarningType convertIntToEnum(Integer loyaltyLevelEarningTypeId) {
            switch (loyaltyLevelEarningTypeId) {
                case 1:
                    return POINT_PER_PRODUCT;
                case 2:
                    return PRICE_OF_PRODUCT;
                default:
                    return NONE;
            }
        }

    }

    public static enum LoyaltyRewardType {
        ACCOUNT_CREDIT(1, "ACCOUNT CREDIT"),
        TRANSACTION_CREDIT(2, "TRANSACTION CREDIT"),
        TRANSACTION_PERCENT_OFF(3, "TRANSACTION PERCENT OFF"),
        FREE_PRODUCTS(4, "FREE PRODUCT(S)");

        private int id;
        private String name;

        private LoyaltyRewardType(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int toInt() {
            return id;
        }

        public String toStringName() {
            return name;
        }

/*        public static LoyaltyRewardType convertStringToEnum(String loyaltyRewardTypeString) {
            switch (loyaltyRewardTypeString) {
                case "Account Credit":
                    return ACCOUNT_CREDIT;
                case "Transaction Credit":
                    return TRANSACTION_CREDIT;
                case "Transaction Percent Off":
                    return TRANSACTION_PERCENT_OFF;
                case "Free Product(s)":
                case "Free Product":
                    return FREE_PRODUCTS;
                default:
                    return ACCOUNT_CREDIT;
            }
        }*/

        public static LoyaltyRewardType convertIntToEnum(Integer loyaltyRewardTypeId) {
            switch (loyaltyRewardTypeId) {
                case 1:
                    return ACCOUNT_CREDIT;
                case 2:
                    return TRANSACTION_CREDIT;
                case 3:
                    return TRANSACTION_PERCENT_OFF;
                case 4:
                    return FREE_PRODUCTS;
                default:
                    return null;
                //return TRANSACTION_CREDIT;
            }
        }
    }

    public static enum LoyaltyRewardCalcMethod {
        FIXED_AMOUNT(1), MULTIPLIER(2);

        private int value;

        private LoyaltyRewardCalcMethod(int value) {
            this.value = value;
        }

        public int toInt() {
            return value;
        }

/*        public static LoyaltyRewardCalcMethod convertStringToEnum(String LoyaltyRewardCalcMethod) {
            switch (LoyaltyRewardCalcMethod) {
                case "Fixed Amount":
                    return FIXED_AMOUNT;
                case "Multiplier":
                    return MULTIPLIER;
                default:
                    return FIXED_AMOUNT;
            }
        }*/

        public static LoyaltyRewardCalcMethod convertIntToEnum(Integer LoyaltyRewardCalcMethodId) {
            switch (LoyaltyRewardCalcMethodId) {
                case 1:
                    return FIXED_AMOUNT;
                case 2:
                    return MULTIPLIER;
                default:
                    //return FIXED_AMOUNT;
                    return null;
            }
        }


    }

    public static enum HTTP_VERBS { //HTTP REST API verbs
        GET, PUT, DELETE, POST

    }

    public static enum PosRequestControllerType {
        AUTHENTICATION,
        SIMPLE_TRANSACTION,
        ADVANCED_TRANSACTION,
        ACCOUNT,
        LOYALTY
    }

    public static enum TerminalType {
        ADVANCE_PAYMENTS(1),
        KRONOS_TERMINAL(2),
        PC_ENTRY(3),
        POS_TERMINAL(4),   //3rd Party OTM
        MMHAYES_POS(5),    //QCPOS
        QC_KIOSK(6),
        ONLINE_ORDERING(7);

        private int value;

        private TerminalType(int value) {
            this.value = value;
        }

        public int toInt() {
            return value;
        }
    }

    public static enum PosTerminalModel {
        POSANYWHERE(102),
        QC_POS(103),
        ONLINE_ORDERING(105);

        private int value;

        private PosTerminalModel(int value) {
            this.value = value;
        }

        public int toInt() {
            return value;
        }
    }

    public static final String DISCOUNT_PERCENT = "1";
    public static final String DISCOUNT_COUPON = "2";

    //Points to the QC_DiscountType table
    public enum DiscountType {
        PERCENT(1), COUPON(2);

        private int value;

        private DiscountType(int value) {
            this.value = value;
        }

        public int toInt() {
            return value;
        }

        public String valueToString() {
            // return value.toString();
            return "";
        }
    }

    public static enum DiscountApplicationResult {
        NOT_APPLIED_YET(0, "Not Applied Yet"),
        SUCCESS(100, "Success"),
        INVALID_NO_PRODUCTS(200, "Discount Invalid - No Available Products"),
        INVALID_AMOUNT(201, "Discount Invalid - Amount Not Valid"),
        INVALID_MIN_PURCHASE_AMT(202, "Discount Invalid - Transaction does not meet Minimum Purchase Amt."),
        INVALID_COMPOUND_ITEM_DISCOUNT_RULE(203, "Discount Invalid - Cannot Compound on Item Discounts."),
        INVALID_COMPOUND_SUBTOTAL_DISCOUNT_RULE(204, "Discount Invalid - Cannot Compound on Subtotal Discounts."),
        INVALID_DUPLICATE_SUBTOTAL_DISCOUNTS(205, "Discount Invalid - Duplicate Subtotal discounts submitted."),
        INVALID_UNKNOWN(299, "Discount Invalid - Unknown.");

        private int value;
        private String message;

        private DiscountApplicationResult(int value, String message){
            this.value = value;
            this.message = message;
        }

        public int getCode(){
            return value;
        }

        public String getMessage(){
            return message;
        }

    }

    public enum EmployeeStatus {
        ACTIVE("A", "Active"), INACTIVE("I", "Inactive"), PENDING_INACTIVATION("P", "Pending Inactivation"), SUSPENDED("S", "Suspended"), FROZEN("F", "Frozen"), WAITING_FOR_SIGNUP("W", "Waiting For Signup");

        private String letter;
        private String fullName;

        private EmployeeStatus(String letter, String fullName) {
            this.letter = letter;
            this.fullName = fullName;
        }

        public static EmployeeStatus convertStringNameToEnum(String employeeStatusString) {

            if (employeeStatusString != null) {
                employeeStatusString = employeeStatusString.toUpperCase();
            }

            switch (employeeStatusString) {
                case "ACTIVE":
                    return ACTIVE;
                case "INACTIVE":
                    return INACTIVE;
                case "PENDING INACTIVATION":
                    return PENDING_INACTIVATION;
                case "SUSPENDED":
                    return SUSPENDED;
                case "FROZEN":
                    return FROZEN;
                case "WAITING FOR SIGNUP":
                    return WAITING_FOR_SIGNUP;
                default:
                    return null;
            }
        }

        public static EmployeeStatus convertLetterToEnum(String employeeStatusString) {
            switch (employeeStatusString) {
                case "A":
                    return ACTIVE;
                case "I":
                    return INACTIVE;
                case "P":
                    return PENDING_INACTIVATION;
                case "S":
                    return SUSPENDED;
                case "F":
                    return FROZEN;
                case "W":
                    return WAITING_FOR_SIGNUP;
                default:
                    return INACTIVE;
            }
        }

        public String toStringName() {
            return fullName;
        }

        public String toLetter() {
            return letter;
        }
    }

    public enum PosType {
        THIRD_PARTY(4), MMHAYES_POS(5), ONLINE_ORDERING(7), MY_QC_FUNDING(8), KIOSK_ORDERING(9);
        //THIRD_PARTY references POS TERMINAL in the db

        private int value;

        private PosType(int value) {
            this.value = value;
        }

        public int toInt() {
            return value;
        }

    }

    public static enum TenderType {
        INVALID(-1, "Invalid"),
        CASH(1, "Cash"),
        CREDIT_CARD(2, "Credit Card"),
        CHECK(3, "Check"),
        QUICKCHARGE(4, "QuickCharge"),
        CHARGE(5, "Charge"),
        INTEGRATED_GIFT_CARD(6, "Integrated Gift Card"),
        VOUCHER(7, "Voucher");

        private int id;
        private String name;

        public static TenderType intToEnum(Integer tenderTypeId) {

            switch (tenderTypeId) {
                case 1:
                    return CASH;
                case 2:
                    return CREDIT_CARD;
                case 3:
                    return CHECK;
                case 4:
                    return QUICKCHARGE;
                case 5:
                    return CHARGE;
                case 6:
                    return INTEGRATED_GIFT_CARD;
                case 7:
                    return VOUCHER;
                default:
                    return INVALID;
            }
        }

        private TenderType(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int toInt() {
            return id;
        }

        public String toStringName() {
            return name;
        }
    }

    public static enum CreditCardAuthType {
        APPROVED(1, "Approved"), APPROVED_PARTIAL_AUTH(2, "Approved - Partial Auth"), APPROVED_OFFLINE(3, "Approved - Offline"), DECLINED(4, "Declined"), DECLINED_OFFLINE(5, "Declined - Offline"), DECLINED_ERROR(6, "Declined - Error"), DECLINED_FAILED(7, "Declined - Failed"), APPROVED_ORIGINALLY_OFFLINE(8, "Approved - Originally Offline");

        private int id;
        private String name;

        private CreditCardAuthType(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int toInt() {
            return id;
        }

        public String toStringName() {
            return name;
        }
    }

    public static enum PaymentProcessor {
        STRIPE(1, "Stripe"), DATACAP_NETEPAY(2, "Datacap NETePay"), DATACAP_NETEPAY_EMV(3, "Datacap NETePay EMV"), MERCHANT_LINK(4, "Merchant Link"), DATACAP_NETEPAY_EMV_PCI(5, "Datacap NETePay EMV PCI P2PE"), FREEDOMPAY(6, "FreedomPay"), SIMPLIFY_FUSEBOX(7, "Simplify Fusebox");

        private int id;
        private String name;

        private PaymentProcessor(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int toInt() {
            return id;
        }

        public String toStringName() {
            return name;
        }
    }

    public static enum AccountType {
        PAYROLL_DEDUCTION(1, "Payroll Deduction"), PREPAY_EMPLOYEE(2, "PrePay"), BILLING_ACCOUNT(3, "Billing Account"), GIFT_CARD(4, "Gift Card"), GUEST(5, "Guest");

        private int id;
        private String name = "";

        private AccountType(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int toInt() {
            return id;
        }

        public String toStringName() {
            return name;
        }

        public static String intToName(int accountTypeId) {

            switch (accountTypeId) {
                case 1:
                    return "Payroll Deduction";
                case 2:
                    return "PrePay Employee";
                case 3:
                    return "Billing Account";
                case 4:
                    return "Gift Card";
                case 5:
                    return "Guest";
                default:
                    return "";
            }
        }

        public static Integer nameToInt(String accountType) {

            if (accountType != null) {
                accountType = accountType.toUpperCase();
            }

            switch (accountType) {
                case "PAYROLL DEDUCTION":
                    return 1;
                case "PREPAY EMPLOYEE":
                case "PREPAY":
                    return 2;
                case "BILLING ACCOUNT":
                    return 3;
                case "GIFT CARD":
                    return 4;
                case "GUEST":
                    return 5;
                default:
                    return null;
            }
        }

        public static boolean isValid(Integer accountTypeId){
            boolean isValid = false;

            for (PosAPIHelper.AccountType accountType : PosAPIHelper.AccountType.values()) {
                if (accountTypeId.equals(accountType.id)) {
                    isValid = true;
                }
            }

            return isValid;

        }

    }

    public static enum ReceiptBalanceType {
        STORE_BALANCE(1, "Store Balance"), STORE_AVAILABLE(2, "Store Available"), GLOBAL_BALANCE(3, "Global Balance"), GLOBAL_AVAILABLE(4, "Global Available");

        private int id;
        private String name = "";

        private ReceiptBalanceType(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int toInt() {
            return id;
        }

        public String toStringName() {
            return name;
        }

        public static String intToName(int receiptBalanceType) {

            switch (receiptBalanceType) {
                case 1:
                    return "Store Balance";
                case 2:
                    return "Store Available";
                case 3:
                    return "Global Balance";
                case 4:
                    return "Global Available";
                default:
                    return "";
            }
        }

        public static ReceiptBalanceType convertIntToEnum(Integer receiptBalanceTypeId) {
            switch (receiptBalanceTypeId) {
                case 1:
                    return STORE_BALANCE;
                case 2:
                    return STORE_AVAILABLE;
                case 3:
                    return GLOBAL_BALANCE;
                case 4:
                    return GLOBAL_AVAILABLE;
                default:
                    return STORE_BALANCE;
            }
        }
    }

    public static enum TransactionStatus {
        SUSPENDED(1, "Suspended"), EXPIRED(2, "Expired"), EDIT_COMPLETED(3, "Edit Completed"), EDIT_IN_PROGRESS(4, "Edit In Progress"), COMPLETED(5, "Completed"), PAYMENT_GATEWAY_INQUIRE_NEEDED(6, "Payment Gateway Inquire Needed"),
        PAYMENT_GATEWAY_INQUIRE_RESOLVED_NO_RECORDS(7, "Payment Gateway Inquire Resolved - No Records"), PAYMENT_GATEWAY_INQUIRE_RESOLVED_SUCCESSFUL_VOID(8, "Payment Gateway Inquire Resolved - Successful Void"),
        PAYMENT_GATEWAY_INQUIRE_RESOLVED_MANUAL_INTERVENTION_NEEDED(9, "Payment Gateway Inquire Resolved - Manual Intervention Needed");

        private int id;
        private String name;

        private TransactionStatus(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int toInt() {
            return id;
        }

        public String getName(){
            return name;
        }

        public static int fromStringToInt(String statusName) {

            String statusNameUpper = "";

            if (statusName != null && !statusName.isEmpty()) {
                statusNameUpper = statusName.toUpperCase();
            }

            switch (statusNameUpper) {
                case "SUSPENDED":
                case "OPEN":
                    return 1;
                //break;

                case "EXPIRED":
                    return 2;
                //break;

                case "EDIT COMPLETED":
                    return 3;
                //break;


                case "EDIT IN PROGRESS":
                    return 4;
                //break;

                case "COMPLETED":
                    return 5;
                //break;

                case "PAYMENT GATEWAY INQUIRE NEEDED":
                    return 6;

                case "PAYMENT GATEWAY INQUIRE RESOLVED - NO RECORDS":
                    return 7;

                case "PAYMENT GATEWAY INQUIRE RESOLVED - SUCCESSFUL VOID":
                    return 8;

                case "PAYMENT GATEWAY INQUIRE RESOLVED - MANUAL INTERVENTION NEEDED":
                    return 9;

                default:

                    return -1;

            }
        }
    }

    public static enum SurchargeApplicationType{
        SUBTOTAL(1, "Subtotal"), ITEM(2, "Item");

        private int id;
        private String name;

        private SurchargeApplicationType(int id, String name){
            this.id = id;
            this.name = name;
        }

        public int toInt(){
            return id;
        }

        public String getName(){
            return name;
        }
    }

    public static enum SurchargeType{
        REVENUE(1, "Revenue"), NON_REVENUE(2, "Non Revenue");

        private int id;
        private String name;

        private SurchargeType(int id, String name){
            this.id = id;
            this.name = name;
        }

        public int toInt(){
            return id;
        }

        public String getName(){
            return name;
        }
    }

    public static enum SurchargeAmountType{
        DOLLAR(1, "Dollar"), PERCENT(2, "Percent");

        private int id;
        private String name;

        private SurchargeAmountType(int id, String name){
            this.id = id;
            this.name = name;
        }

        public int toInt(){
            return id;
        }

        public String getName(){
            return name;
        }
    }

    public static enum DonationType {
        LOYALTY_POINT(1, "Loyalty Point"), MONETARY(2, "Monetary");

        private int id;
        private String name;

        private DonationType(int id, String name){
            this.id = id;
            this.name = name;
        }

        public int toInt(){
            return id;
        }

        public String getName(){
            return name;
        }
    }

    public static enum LoyaltyAdjustmentType {
        MANUAL_ADJUSTMENT(1, "Manual Adjustment"),
        POINTS_DONATION(2, "Points Donation"),
        ACCOUNT_CLOSED(3, "Account Closed");

        private int id;
        private String name;

        private LoyaltyAdjustmentType(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int toInt() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    public static enum VoucherStatus {
        AVAILABLE(1, "Available"),
        REDEEMED(2, "Redeemed"),
        EXPIRED(3, "Expired");

        private int value;
        private String name;

        private VoucherStatus(int value, String name) {
            this.value = value;
            this.name = name;
        }

        public int toInt() {
            return value;
        }

        public String getName() {
            return name;
        }

        public static String convertIntToName(int voucherStatusInt) {
            switch (voucherStatusInt) {
                case 1:
                    return AVAILABLE.getName();
                case 2:
                    return REDEEMED.getName();
                case 3:
                    return EXPIRED.getName();
                default:
                    return null;
            }
        }

        public static Integer convertStringToInt(String voucherStatusName) {

            String voucherStatusNameUpper = voucherStatusName.toUpperCase();

            switch (voucherStatusNameUpper) {
                case "AVAILABLE":
                    return AVAILABLE.toInt();
                case "REDEEMED":
                    return REDEEMED.toInt();
                case "EXPIRED":
                    return EXPIRED.toInt();
                default:
                    return null;
            }
        }
    }

    public static String getLogFileName(Integer terminalId) {

        logFileName = logFileCache.get(terminalId);

        //on all initial calls, log to the default
        if (logFileName == null) {
            logFileName = "POSAPI_COMMON_T.log";
        }

        return logFileName;

    }

    //if we don't have access to the terminal, return the default logfile name
    public static String getLogFileName() {


        String tempLogFileName = logFileCache.get(null);

        if (logFileName == null) {
            logFileCache.put(null, "POSAPI_COMMON_T.log");
            return "POSAPI_COMMON_T.log";
        }

        return "POSAPI_COMMON_T.log";

    }

    public static void setLogFileName(TerminalModel terminalModel) {

        /*terminalLogFileName is assembled in the query
        data.posapi30.getOneTerminalByTerminalType*/
        if (terminalModel != null && terminalModel.getTerminalLogFileName() != null && !terminalModel.getTerminalLogFileName().isEmpty()) {


            boolean alreadySet = false;
            alreadySet = logFileCache.get(terminalModel.getId()) != null;


            if (alreadySet) {
                //add the new terminal id to the list
                logFileCache.remove(terminalModel.getId());
            }

            logFileCache.put(terminalModel.getId(), terminalModel.getTerminalLogFileName());
            Logger.logMessage("Log file name set to " + terminalModel.getTerminalLogFileName() + " for Terminal ID: " + terminalModel.getId(), Logger.LEVEL.DEBUG);
        }
    }

    public static String getLogFileName(OtmModel otmModel) {
        if (otmModel == null || otmModel.getId() == null  || Integer.parseInt(otmModel.getId().toString()) <= 0) {
            return "QCPOS_OTM.log";
        } else {
            return "QCPOS_" + otmModel.getId().toString() + "_OTM.log";
        }
    }

    /**
     * Return a Log File Name string using the OTM ID and a custom log name string
     *
     * @param otmModel
     * @param customText
     * @return
     */
    public static String getLogFileName(OtmModel otmModel, String customText) {
        String baseLogFileName = "QCPOS_";
        String fileType = ".log";

        try {

            if (customText != null && !customText.isEmpty()) {
                if (!customText.endsWith("_")) {
                    customText = customText + "_";
                }
            }

            if (otmModel.getId() == null || Integer.parseInt(otmModel.getId().toString()) <= 0) { //id = -1
                return baseLogFileName + customText + fileType;
            } else {
                return baseLogFileName + customText + otmModel.getId().toString() + fileType;
            }

        } catch (Exception ex) {
            return baseLogFileName + "OTM_0" + fileType; //QCPOS_OTM_0.log
        }
    }

    public static String getSyncServiceLogFileName(OtmModel otmModel) {
        if (otmModel == null || otmModel.getId() == null  || Integer.parseInt(otmModel.getId().toString()) <= 0) {
            return "QCPOS_S.log";
        } else {
            return "QCPOS_" + otmModel.getId().toString() + "_S.log";
        }
    }

    public static String getSafLogFileName(OtmModel otmModel) {
        if (otmModel == null || otmModel.getId() == null  || Integer.parseInt(otmModel.getId().toString()) <= 0) {
            return "QCPOS_SAF.log";
        } else {
            return "QCPOS_SAF_" + otmModel.getId().toString() + ".log";
        }
    }

    //make sure the string parameter is a number, and a valid int
    public static Integer cleanIdentifier(String idStr, CommonAPI.PosModelType modelType, Integer terminalId) throws Exception {
        Integer id = -1;


        if (!StringUtils.isNumeric(idStr)) {
            switch (modelType) {
                case TRANSACTION:
                    throw new MissingDataException("Invalid Transaction ID - Please try again", terminalId);
                case LOYALTY_PROGRAM:
                    throw new MissingDataException("Invalid Program ID - Please try again", terminalId);
                case LOYALTY_REWARD:
                    throw new MissingDataException("Invalid Reward ID - Please try again", terminalId);
                case ACCOUNT:
                    throw new MissingDataException("Invalid Account ID - Please try again", terminalId);
                default:
                    throw new MissingDataException("Invalid Request Parameter - Please try again", terminalId);
            }
        }

        try {
            id = Integer.parseInt(idStr);
        } catch (Exception ex) {
            throw new MissingDataException("Invalid Request Parameter - Please try again", terminalId);
        }

        return id;

    }

    public static Object cleanIdentifierObj(String idStr, CommonAPI.PosModelType modelType, Integer terminalId) throws Exception {
        Object id = null;
        Boolean isAplhaNumericOk = false;

        if (!StringUtils.isNumeric(idStr)) {
            switch (modelType) {
                case TRANSACTION:
                    if (idStr.contains("T")){
                        isAplhaNumericOk = true;
                    } else {
                        throw new MissingDataException("Invalid Transaction ID - Please try again", terminalId);
                    }
                    break;
                case LOYALTY_PROGRAM:
                    throw new MissingDataException("Invalid Program ID - Please try again", terminalId);
                case LOYALTY_REWARD:
                    throw new MissingDataException("Invalid Reward ID - Please try again", terminalId);
                case ACCOUNT:
                    throw new MissingDataException("Invalid Account ID - Please try again", terminalId);
                default:
                    throw new MissingDataException("Invalid Request Parameter - Please try again", terminalId);
            }
        }

        if (isAplhaNumericOk) {
            return idStr;
        } else {
            try {
                id = Integer.parseInt(idStr);
            } catch (Exception ex) {
                throw new MissingDataException("Invalid Request Parameter - Please try again", terminalId);
            }
            return id;
        }
    }

    public static PosAPIHelper.PosType checkPosType(Integer terminalTypeId) throws Exception {
        if (terminalTypeId.equals(PosAPIHelper.PosType.MMHAYES_POS.toInt())) {
            return PosAPIHelper.PosType.MMHAYES_POS;
        } else if (terminalTypeId.equals(PosAPIHelper.PosType.THIRD_PARTY.toInt())) {
            return PosAPIHelper.PosType.THIRD_PARTY;
        } else if (terminalTypeId.equals(PosAPIHelper.PosType.ONLINE_ORDERING.toInt())) {
            return PosAPIHelper.PosType.ONLINE_ORDERING;
        } else if (terminalTypeId.equals(PosAPIHelper.PosType.MY_QC_FUNDING.toInt())) {
            return PosAPIHelper.PosType.MY_QC_FUNDING;
        } else if (terminalTypeId.equals(PosAPIHelper.PosType.KIOSK_ORDERING.toInt())) {
            return PosAPIHelper.PosType.KIOSK_ORDERING;
        } else {
            throw new MissingDataException("Terminal set with an Invalid Terminal Type");
        }
    }
}
