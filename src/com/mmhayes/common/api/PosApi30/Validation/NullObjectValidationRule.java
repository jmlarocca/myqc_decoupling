package com.mmhayes.common.api.PosApi30.Validation;

import com.mmhayes.common.api.CommonAPI;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-04-20 16:34:33 -0400 (Thu, 20 Apr 2017) $: Date of last commit
 $Rev: 3849 $: Revision of last commit
*/

public class NullObjectValidationRule implements IValidationRule {
    Object object = null;

    public NullObjectValidationRule(Object _object) {
        object = _object;
    }

    public void validate() throws Exception {
        CommonAPI.checkIsNullOrEmptyObject(object, "Object is null or empty");
    }

}
