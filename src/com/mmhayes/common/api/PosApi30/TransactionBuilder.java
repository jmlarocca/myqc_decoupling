package com.mmhayes.common.api.PosApi30;

//mmhayes dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.combo.models.ComboDetailModel;
import com.mmhayes.common.conn_pool.JDCConnection;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.donation.models.DonationLineItemModel;
import com.mmhayes.common.funding.collections.AccountPaymentMethodCollection;
import com.mmhayes.common.funding.models.FundingModel;
import com.mmhayes.common.loyalty.collections.*;
import com.mmhayes.common.loyalty.models.*;
import com.mmhayes.common.product.models.*;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.collections.*;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.utils.*;
import com.mmhayes.common.voucher.models.VoucherCalculation;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.*;

//other dependencies
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import org.apache.commons.lang3.StringUtils;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-09-10 14:46:42 -0400 (Fri, 10 Sep 2021) $: Date of last commit
 $Rev: 15347 $: Revision of last commit
*/
public class TransactionBuilder {
    DataManager dm = new DataManager();
    private TransactionModel transactionModel = new TransactionModel();
    private TerminalModel terminalModel = new TerminalModel();
    private CashierShiftEndModel cashierShiftEndModel = new CashierShiftEndModel();

    //region Constructors

    public TransactionBuilder() {

    }

    /**
     * Get one transaction by either PATransactionId or
     * @param terminalModel
     * @param transactionId
     * @param apiActionType
     * @param transactionType
     * @return
     * @throws Exception
     */
    public static TransactionBuilder createTransactionBuilder(TerminalModel terminalModel, Object transactionId, PosAPIHelper.ApiActionType apiActionType, PosAPIHelper.TransactionType transactionType) throws Exception {
        TransactionBuilder transactionBuilder = new TransactionBuilder();
        CommonAPI.checkIsNullOrEmptyObject(transactionId, CommonAPI.PosModelType.TRANSACTION, terminalModel.getId());

        mapTransactionInfo(transactionBuilder, terminalModel, apiActionType);
        if (transactionType != null) { //only set the transaction type if it's passed in
            transactionBuilder.getTransactionModel().setTransactionTypeEnum(transactionType);
        }

        return transactionBuilder;
    }

    /**
     * Used by Get One Transaction By Id
     *
     * @param terminalModel
     * @param transactionId
     * @param apiActionType
     * @param transactionType
     * @return
     * @throws Exception
     */
    public static TransactionBuilder createTransactionBuilder(TerminalModel terminalModel, Integer transactionId, PosAPIHelper.ApiActionType apiActionType, PosAPIHelper.TransactionType transactionType) throws Exception {
        TransactionBuilder transactionBuilder = new TransactionBuilder();
        CommonAPI.checkIsNullOrEmptyObject(transactionId, CommonAPI.PosModelType.TRANSACTION, terminalModel.getId());

        mapTransactionInfo(transactionBuilder, terminalModel, apiActionType);
        if (transactionType != null) { //only set the transaction type if it's passed in
            transactionBuilder.getTransactionModel().setTransactionTypeEnum(transactionType);
        }

        return transactionBuilder;
    }

    /**
     * Used by Get One Transaction by transRefNumber
     *
     * @param terminalModel
     * @param transRefNumber
     * @param apiActionType
     * @param transactionType
     * @return
     * @throws Exception
     */
    public static TransactionBuilder createTransactionBuilder(TerminalModel terminalModel, String transRefNumber, PosAPIHelper.ApiActionType apiActionType, PosAPIHelper.TransactionType transactionType) throws Exception {
        TransactionBuilder transactionBuilder = new TransactionBuilder();
        CommonAPI.checkIsNullOrEmptyObject(transRefNumber, CommonAPI.PosModelType.TRANSACTION, terminalModel.getId());

        mapTransactionInfo(transactionBuilder, terminalModel, apiActionType);
        if (transactionType != null) { //only set the transaction type if it's passed in
            transactionBuilder.getTransactionModel().setTransactionTypeEnum(transactionType);
        }

        return transactionBuilder;
    }

    //create Transaction Builder
    //takes in a Terminal Model and Transaction Model
    public static TransactionBuilder createTransactionBuilder(TerminalModel terminalModel, TransactionModel transactionModel, PosAPIHelper.ApiActionType apiActionType, PosAPIHelper.TransactionType transactionType) throws Exception {
        TransactionBuilder transactionBuilder = new TransactionBuilder();
        CommonAPI.checkIsNullOrEmptyObject(transactionModel, CommonAPI.PosModelType.TRANSACTION, terminalModel.getId());

        transactionBuilder.setTransactionModel(transactionModel);
        mapTransactionInfo(transactionBuilder, terminalModel, apiActionType);
        if (transactionType != null) { //only set the transaction type if it's passed in
            transactionBuilder.getTransactionModel().setTransactionTypeEnum(transactionType);
        }

        return transactionBuilder;
    }

    /**
     * This method looks at the transaction model to figure out what type of transaction it is
     * It first checks transactionTypeId
     * then checks transactionType
     * Then defaults to SALE
     * Currently used for inquiries
     */
    public static TransactionBuilder createTransactionBuilder(TerminalModel terminalModel, TransactionModel transactionModel, PosAPIHelper.ApiActionType apiActionType) throws Exception {
        TransactionBuilder transactionBuilder = new TransactionBuilder();
        CommonAPI.checkIsNullOrEmptyObject(transactionModel, CommonAPI.PosModelType.TRANSACTION, terminalModel.getId());

        //default the transaction type as SALE
        PosAPIHelper.TransactionType transactionType = PosAPIHelper.TransactionType.SALE;

        //check if there
        if (transactionModel.getTransactionTypeId() != null && !transactionModel.getTransactionTypeId().toString().isEmpty()) {
            transactionType = PosAPIHelper.TransactionType.intToTransactionType(transactionModel.getTransactionTypeId());
            transactionModel.setTransactionType(transactionType.toString());
            transactionModel.setTransactionTypeEnum(transactionType);

        } else if (transactionModel.getTransactionType() != null && !transactionModel.getTransactionType().isEmpty()) {
            transactionType = PosAPIHelper.TransactionType.stringToTransactionType(transactionModel.getTransactionType());
            transactionModel.setTransactionTypeId(transactionType.toInt());
            transactionModel.setTransactionTypeEnum(transactionType);

        } else {
            transactionModel.setTransactionTypeId(transactionType.toInt());
            transactionModel.setTransactionTypeEnum(transactionType);
        }

        transactionBuilder.setTransactionModel(transactionModel);
        mapTransactionInfo(transactionBuilder, terminalModel, apiActionType);

        return transactionBuilder;


    }

    public static void mapTransactionInfo(TransactionBuilder transactionBuilder, TerminalModel terminalModel, PosAPIHelper.ApiActionType apiActionType) throws Exception {
        transactionBuilder.getTransactionModel().setTerminal(terminalModel);
        transactionBuilder.getTransactionModel().setTerminalId(terminalModel.getId());
        transactionBuilder.getTransactionModel().setApiActionEnum(apiActionType);
        transactionBuilder.getTransactionModel().setPosType(PosAPIHelper.checkPosType(terminalModel.getTypeId()));

        switch (transactionBuilder.getTransactionModel().getPosType()) {
            case ONLINE_ORDERING:  //Online Ordering sets the UserId on the TransactionModel.  If it exists, don't override it
            case MY_QC_FUNDING:
            case KIOSK_ORDERING:
                if (transactionBuilder.getTransactionModel().getUserId() == null || transactionBuilder.getTransactionModel().getUserId().toString().isEmpty()) {
                    transactionBuilder.getTransactionModel().setUserId(terminalModel.getLoginModel().getUserId()); //always set the User ID to the User ID found on the session
                }
                break;
            default:
                //01-10-2018 egl- The UserId should always be set on the Transaction from the UserId in the session (DSKey)
                transactionBuilder.getTransactionModel().setUserId(terminalModel.getLoginModel().getUserId()); //always set the User ID to the User ID found on the session
        }


        //transactionBuilder.getTransactionModel().checkTransactionTypeEnum();
        if (transactionBuilder.getTransactionModel().getTransactionTypeId() != null) {
            switch (transactionBuilder.getTransactionModel().getTransactionTypeEnum()) {
                case PAYMENT_GATEWAY_INQUIRE:
                    //override the api action type for Payment Gateway Inquire
                    transactionBuilder.getTransactionModel().setPaymentGatewayInquireTransactionType(true);
                    break;
            }
        }

    }

    //endregion

    //region Validation Methods

    //region Validate Transaction
    public void buildTransaction() throws Exception {
        try {

            this.checkForOfflineMode(); //first thing, check if the POS war is running online or offline
            this.checkQCPosForDupeTransaction();
            this.validateTransactionType();
            this.validateTransactionLineItems();

            this.validatePATransHeader();
            this.validateOpenTransaction();
            this.validateSurcharges();
            this.validateVoidOrRefund();
            this.checkVoidRefund();
            this.prepareTendersForRefundVoid();
            this.checkQuickchargeBalances();
            this.validateQCTransactions();
            this.calculateTransaction(); //TransactionCalculation: Discounts, Combos, Loyalty, Taxes
            this.checkTransactionBalances();
            this.cleanUpAfterBuild();

        } catch (Exception ex) {
            Logger.logMessage("Error building transaction.  TransactionBuilder.buildTransaction()", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.ERROR);
            throw ex;
        }
    }

    public void checkQCPosForDupeTransaction() throws Exception {
        DataManager dm = new DataManager();

        if (!transactionModel.isInquiry() &&
                !transactionModel.isCancelTransactionType() &&
                !transactionModel.isPaymentGatewayInquireTransactionType()) {

            switch (transactionModel.getPosType()) {
                case THIRD_PARTY:
                case MMHAYES_POS:
                    switch (transactionModel.getApiActionTypeEnum()) {
                        case SALE:
                        case ROA:
                            //Check for duplicate transactions before it's saved in the DB
                            //Check the following fields:
                            //TerminalId, TransactionTypeId, timeStamp, userId
                            //get all models in an array list
                            ArrayList<HashMap> dupeTransactionList = dm.parameterizedExecuteQuery("data.posapi30.checkQcPosForDupeTransaction",
                                    new Object[]{this.getTransactionModel().getTerminal().getId(), this.getTransactionModel().getTransactionTypeId(), this.getTransactionModel().getTimeStamp(), (this.getTransactionModel().getUserId() == null) ? 0 : this.getTransactionModel().getUserId()},
                                    PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()),
                                    true
                            );

                            if (!dupeTransactionList.isEmpty()) {
                                throw new DuplicateTransactionException("This transaction already exists in the database.", transactionModel.getTerminal().getId());
                            }

                            break;
                    }

                    break;
            }
        }
    }

    /**
     * If a QC Discount is Applied through the third party, this creates PlaceHolder ItemDiscountModel's on the ProductLineItemModel.
     * The amount on the ItemDiscountModel is a weighted total for the QC Discount.
     * Therefore, when we calculate loyalty it takes into account the QC Discount(s)
     *
     * @throws Exception
     */
    public void applyQcDiscountPlaceHolders() throws Exception {
        if (this.getTransactionModel().hasQcDiscountApplied()) {

            BigDecimal totalProductAmount = BigDecimal.ZERO;
            BigDecimal totalQcDiscountAmount = this.getTransactionModel().getTotalQcDiscountAmount();

            if (this.getTransactionModel().getProducts() != null && !this.getTransactionModel().getProducts().isEmpty()) {
                for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                    totalProductAmount = totalProductAmount.add(ProductLineItemModel.getAdjValOfExtendedAmountForTransaction(productLineItemModel));
                }
                totalProductAmount = totalProductAmount.setScale(2, RoundingMode.HALF_UP);

                for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                    BigDecimal adjProductValue = ProductLineItemModel.getAdjValOfExtendedAmountForTransaction(productLineItemModel);
                    productLineItemModel.addQcDiscountPlaceHolderLines(totalQcDiscountAmount, adjProductValue, totalProductAmount);
                }
            }
        }
    }

    /**
     * After Loyalty Points are calculated, remove the QC Discount PlaceHolder records
     * That way these placeholder records are not returned in the JSON response and not saved to the database
     *
     * @throws Exception
     */
    public void removeQcDiscountPlaceHolders() throws Exception {

        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
            productLineItemModel.getDiscounts().removeIf(d -> d.isQcDiscountPlaceHolder());
        }
    }

    /**
     * If a QC Discount is Applied through the third party, this creates PlaceHolder ItemDiscountModel's on the ProductLineItemModel.
     * The amount on the ItemDiscountModel is a weighted total for the QC Discount.
     * Therefore, when we calculate loyalty it takes into account the QC Discount(s)
     *
     * @throws Exception
     */
    public void applyDiscountPlaceHolders() throws Exception {

        if (this.getTransactionModel().getProducts() != null && !this.getTransactionModel().getProducts().isEmpty()) {
            if (this.getTransactionModel().getDiscounts() != null && !this.getTransactionModel().getDiscounts().isEmpty()) {

                //If the Transaction Level Discounts and Product Level Discount add up, do add any placeholders
                HashMap<Integer, BigDecimal> transLevelDiscounts = new HashMap<>();
                HashMap<Integer, BigDecimal> productLevelDiscounts = new HashMap<>();
                HashMap<Integer, String> fullyAppliedDiscounts = new HashMap();

                //sum up transaction level discounts
                for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {

                    //check for the extended amount filled in
                    if (discountLineItemModel.getExtendedAmount() != null && discountLineItemModel.getExtendedAmount().compareTo(BigDecimal.ZERO) == 0) {
                        //Don't create placeholders if the amount is null or zero, the 3rd party wants MMHayes to calculate the amount
                        continue;
                    }

                    if (discountLineItemModel.getEligibleAmount() != null && discountLineItemModel.getEligibleAmount().compareTo(BigDecimal.ZERO) != 0) { //If the POS API has created discount records for QCPOS, don't create placeholders
                        continue;
                    }

                    if (transLevelDiscounts.containsKey(discountLineItemModel.getDiscount().getId())) {
                        BigDecimal transAmount = transLevelDiscounts.get(discountLineItemModel.getDiscount().getId());
                        transLevelDiscounts.put(discountLineItemModel.getDiscount().getId(), transAmount.abs().add(discountLineItemModel.getExtendedAmount().abs()));
                    } else {
                        transLevelDiscounts.put(discountLineItemModel.getDiscount().getId(), discountLineItemModel.getExtendedAmount().abs());
                    }
                }

                //total of the Product Level Discounts
                for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                    for (ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()) {
                        if (productLevelDiscounts.containsKey(itemDiscountModel.getDiscount().getId())) {
                            BigDecimal prodLevelAmt = productLevelDiscounts.get(itemDiscountModel.getDiscount().getId());
                            productLevelDiscounts.put(itemDiscountModel.getDiscount().getId(), prodLevelAmt.abs().add(itemDiscountModel.getAmount().abs()));
                        } else {
                            productLevelDiscounts.put(itemDiscountModel.getDiscount().getId(), itemDiscountModel.getAmount().abs());
                        }
                    }
                }

                //compare the two summary lists to see if the total transaction discounts equal the Product Level discounts
                if (!transLevelDiscounts.isEmpty() && !productLevelDiscounts.isEmpty()) {
                    //loop through the transaction level totals
                    for (Map.Entry<Integer, BigDecimal> transEntry : transLevelDiscounts.entrySet()) {
                        Integer transDiscountId = transEntry.getKey();
                        BigDecimal transDiscountAmtvalue = transEntry.getValue();

                        //loop through the Product Level totals
                        for (Map.Entry<Integer, BigDecimal> productLevelEntry : productLevelDiscounts.entrySet()) {
                            Integer prodLevelDiscountId = productLevelEntry.getKey();
                            BigDecimal prodLevelDiscountAmtvalue = productLevelEntry.getValue();

                            if (prodLevelDiscountId.equals(transDiscountId)) {
                                if (transDiscountAmtvalue.compareTo(prodLevelDiscountAmtvalue) == 0) {
                                    if (transLevelDiscounts.size() == 1) {
                                        return; //If there is only 1 discount, product level discount records match the transaction level, return and don't create any place holders
                                    } else {
                                        fullyAppliedDiscounts.put(prodLevelDiscountId, "");
                                        //mark the discount line item that it is already applied
                                    }
                                }
                            }
                        }
                    }
                }

                //if there are multiple discounts, and all Item Discount records are added in
                if (fullyAppliedDiscounts.size() == transLevelDiscounts.size()) {
                    return; //if all fully Applied discounts equals the number of Transaction Level Discounts
                }

                //Create the placeholder records
                BigDecimal totalProductAmount = BigDecimal.ZERO;
                BigDecimal totalSubTotalDiscountAmount = BigDecimal.ZERO; //this.getTransactionModel().getTotalQcDiscountAmount();
                BigDecimal totalProductLevelDiscounts = BigDecimal.ZERO;
                BigDecimal netDiscountsToApply = BigDecimal.ZERO;  //subTotal discounts minus the product level discounts

                //total of the Sub-Total Discounts
                for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
                    totalSubTotalDiscountAmount = totalSubTotalDiscountAmount.add(discountLineItemModel.getExtendedAmount());
                }

                //total of the Product Level Discounts
                for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                    totalProductAmount = totalProductAmount.add(ProductLineItemModel.getAdjValOfExtendedAmountForDiscount(productLineItemModel, false));

                    for (ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()) {
                        totalProductLevelDiscounts = totalProductLevelDiscounts.add(itemDiscountModel.getAmount());
                    }
                }
                totalProductAmount = totalProductAmount.setScale(2, RoundingMode.HALF_UP);

                //sub-Total discounts minus product level discounts
                netDiscountsToApply = totalSubTotalDiscountAmount.subtract(totalProductLevelDiscounts);

                Integer discountCount = 0;
                BigDecimal adjustedDiscountAmount = netDiscountsToApply;
                discountCount = this.getTransactionModel().getDiscounts().size();

                /**
                 *If there are more than 1 sub-Total Discount, get the adjusted price per discount
                 */
                adjustedDiscountAmount = netDiscountsToApply.divide(new BigDecimal(discountCount));

                for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
                    for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {

                        //get the adjusted product value, the product amount minus any product level discounts
                        BigDecimal productAmountMinusDiscounts = ProductLineItemModel.getAdjValOfExtendedAmountForDiscount(productLineItemModel, false);

                        //create ItemDiscount records using the adjusted values
                        productLineItemModel.addDiscountPlaceHolderLines(productAmountMinusDiscounts, totalProductAmount, discountLineItemModel, productLineItemModel, adjustedDiscountAmount);
                    }

                    //fill in the Eligible amount if the Third Party didn't supply it
                    if (discountLineItemModel.getEligibleAmount() == null
                            || discountLineItemModel.getEligibleAmount().toString().isEmpty()
                            || discountLineItemModel.getEligibleAmount().equals(BigDecimal.ZERO)) {

                        discountLineItemModel.setEligibleAmount(totalProductAmount);
                    }
                }
            }
        }
    }

    public void validateOpenTransaction() throws Exception {

        String lastUpdatedDate = getCurrentDate();

        //do this check for both OPEN and SALE
        switch (this.getTransactionModel().getApiActionTypeEnum()) { //do these things for both Sale and suspended
            //case OPEN:
            //case SALE:
            default:

                //parse Transaction Status
                if (this.getTransactionModel().getTransactionStatus() != null && !this.getTransactionModel().getTransactionStatus().isEmpty()) {
                    this.getTransactionModel().setTransactionStatusId(PosAPIHelper.TransactionStatus.fromStringToInt(this.getTransactionModel().getTransactionStatus()));
                }
                this.getTransactionModel().setTransStatusLastUpdated(lastUpdatedDate);

                break;
        }

        //do this check for only OPEN
        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case OPEN:
                //check the Terminal Type, make sure it's MMHayes POS or Online Ordering
                switch (this.getTransactionModel().getPosType()) {
                    case MMHAYES_POS:
                    case ONLINE_ORDERING:
                    case KIOSK_ORDERING:

                        break;

                    default:
                        throw new MissingDataException("Invalid POS Terminal Type");
                        //break;
                }

                //Check if terminal is offline
                if (this.getTransactionModel().isOfflineMode()) {

                    if (this.getTransactionModel().getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.QC_POS.toInt())
                            || this.getTransactionModel().getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.ONLINE_ORDERING.toInt())) {

                        if (!this.getTransactionModel().getTerminal().isAllowOfflineOpenTxns()) {
                            throw new MissingDataException("Transactions cannot be opened when terminal is offline", this.getTransactionModel().getTerminal().getId());
                        }
                    } else {
                        throw new MissingDataException("Transactions cannot be opened when terminal is offline", this.getTransactionModel().getTerminal().getId());
                    }

                }
        }

        this.validateUpdateOpenTransaction();
        this.validateCreateOpenTransaction();
    }

    private void validateUpdateOpenTransaction() throws Exception {

        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case OPEN:


                if (!this.getTransactionModel().isCreateOpen()) {
                    //for update, set Transaction Status to "Edit In Progress"
                    this.getTransactionModel().setTransactionStatus(PosAPIHelper.TransactionStatus.EDIT_IN_PROGRESS.getName());
                    this.getTransactionModel().setTransactionStatusId(PosAPIHelper.TransactionStatus.EDIT_IN_PROGRESS.toInt());
                }

                break;
        }
    }

    private void validateCreateOpenTransaction() throws Exception {

        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case OPEN:

                //create Suspended transaction
                if (this.getTransactionModel().isCreateOpen()) {

                    if (this.getTransactionModel().isCancelTransactionType()) {
                        //For a Cancel Transaction, set the Transaction status to "Completed"
                        this.getTransactionModel().setTransactionStatus(PosAPIHelper.TransactionStatus.COMPLETED.getName());
                        this.getTransactionModel().setTransactionStatusId(PosAPIHelper.TransactionStatus.COMPLETED.toInt());
                    } else {
                        this.getTransactionModel().setTransactionStatus(PosAPIHelper.TransactionStatus.SUSPENDED.getName());
                        this.getTransactionModel().setTransactionStatusId(PosAPIHelper.TransactionStatus.SUSPENDED.toInt());
                    }

                    //User attached to session must have the QC_UserProfile.PASuspendTxn user privilege set
                    /*UserModel userModel = UserModel.getOneUserModelById(this.getTransactionModel().getTerminal().getLoginModel().getUserId(), this.getTransactionModel().getTerminal());
                    if (!userModel.canSuspendTransaction()) {
                        throw new MissingDataException("User cannot Open Transactions");
                    }*/

                    //Check for rewards or discounts, if they exist throw an error:
                    if (this.getTransactionModel().getRewards() != null && this.getTransactionModel().getRewards().size() > 0) {
                        throw new MissingDataException("Rewards are not allowed on an Open Transaction");
                    }

                    //Check for any sub-total discounts
                    if (this.getTransactionModel().getDiscounts() != null && this.getTransactionModel().getDiscounts().size() > 0) {
                        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
                            if (discountLineItemModel.getDiscount().isSubTotalDiscount()) {
                                throw new MissingDataException("Subtotal Discounts are not allowed on an Open Transaction");
                            }
                        }
                    }
                }

                break;
        }
    }

    public void validateSurcharges() throws Exception {

        if (this.getTransactionModel().getSurcharges() != null && this.getTransactionModel().getSurcharges().size() > 0) {
            //Make sure the transaction type is valid.  Only Sale, Void, and Refund allow for Surcharges
            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:
                case VOID:
                case REFUND:
                    //This is allowed

                    break;
                case ROA:
                    switch(transactionModel.getPosType()) {
                        case MY_QC_FUNDING:
                            break;
                        default:
                            if (this.getTransactionModel().getSurcharges() != null && this.getTransactionModel().getSurcharges().size() > 0) {
                                throw new SurchargeNotFoundException("Surcharges are not allowed for this Transaction Type");
                            }
                            break;
                    }
                    break;
                default:

                    throw new MissingDataException("Surcharges are not allowed on this Transaction Type");
            }

            TransactionObjectComparison transObjectComparison = new TransactionObjectComparison(this.getTransactionModel(), PosAPIHelper.ObjectType.SURCHARGE);

            //validate that the amounts for the SurchargeLineItemModels add up to the ItemSurchargeModels
            transObjectComparison.validate(PosAPIHelper.ObjectType.SURCHARGE);

        } //end check for Surcharges
    }

    /**
     * Go through each of the products and check if
     * any of the products that have been sent in have already bean refunded or voided.
     */
    public void validateVoidOrRefund() throws Exception {
        if (!this.getTransactionModel().isCancelTransactionType() &&
                !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {

            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case VOID:
                case REFUND:
                    if (this.getTransactionModel().getId() != null) {
                        if (this.getTransactionModel().getOriginalTransaction() != null) {
                            this.checkForInvalidProductRefund();
                            this.checkForInvalidRoaRefund();
                            this.checkForInvalidTenderRefund();
                            this.checkForInvalidRewardRefund();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void checkForInvalidProductRefund() throws Exception {
        if (transactionModel.getOriginalTransaction().getProducts() != null) {
            //loop through the original products
            for (ProductLineItemModel productLineItemModel : transactionModel.getOriginalTransaction().getProducts()) {
                for (ProductLineItemModel productLineItemModelCurrent : transactionModel.getProducts()) {
                    if (productLineItemModel.getId().equals(productLineItemModelCurrent.getId())) {

                        /*check the original refundedExtendedAmt vs. original extendedAmount
                        The product matched on ID, so we know it's being refunded
                         */
                        checkVoidRefundAmountCommon(productLineItemModel.getRefundedExtAmount(), productLineItemModel.getExtendedAmount(), "Product");

                        BigDecimal roundedCurrentExtAmt = productLineItemModelCurrent.getExtendedAmount().setScale(2, RoundingMode.HALF_UP);
                        BigDecimal roundedOrigExAmount = productLineItemModel.getExtendedAmount().setScale(2, RoundingMode.HALF_UP);

                        if (roundedCurrentExtAmt.compareTo(roundedOrigExAmount) == 1) {
                            throw new MissingDataException("Invalid Product line refund amount.  The refund amount is more than the original Product line", transactionModel.getTerminal().getId());
                        }

                        //check Amount
                        if (productLineItemModelCurrent.getAmount().compareTo(productLineItemModel.getAmount()) == 1) {
                            throw new MissingDataException("Invalid Product line refund amount.  The refund amount is more than the original Product line", transactionModel.getTerminal().getId());
                        }

                        //Check Quantity
                        if (productLineItemModelCurrent.getQuantity().compareTo(productLineItemModel.getQuantity()) == 1) {
                            throw new MissingDataException("Invalid Product line refund quantity.  The quantity is more than the original Product line", transactionModel.getTerminal().getId());
                        }

                        //add the original refundedExtAmount + product extended Amount, is it more than original extended Amount
                        checkVoidRefundAmountWillBeTooMuchCommon(productLineItemModel.getRefundedExtAmount(), productLineItemModelCurrent.getExtendedAmount(), productLineItemModel.getExtendedAmount(), "Product");
                    }
                }
            }
        }
    }

    private void checkForInvalidRoaRefund() throws Exception {
        if (transactionModel.getOriginalTransaction().getReceivedOnAccounts() != null) {
            for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : transactionModel.getOriginalTransaction().getReceivedOnAccounts()) {

                //Loop through the ROAs on the current transaction and check the values
                for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModelCurrent : transactionModel.getReceivedOnAccounts()) {
                    if (receivedOnAccountLineItemModelCurrent.getId() != null && receivedOnAccountLineItemModel.getId() != null && receivedOnAccountLineItemModelCurrent.getId().equals(receivedOnAccountLineItemModel.getId())) {

                        //check the original refundedExtendedAmt vs. original extendedAmount
                        checkVoidRefundAmountCommon(receivedOnAccountLineItemModel.getRefundedExtAmount(), receivedOnAccountLineItemModel.getExtendedAmount(), "ROA");

                        //if the amount trying to be refunded is over the original amount, error out
                        if (receivedOnAccountLineItemModelCurrent.getExtendedAmount().compareTo(receivedOnAccountLineItemModel.getExtendedAmount()) == 1) {
                            throw new MissingDataException("Invalid Received On Account line refund amount.  The refund amount is more than the original Received On Account line", transactionModel.getTerminal().getId());
                        }

                        //check Amount
                        if (receivedOnAccountLineItemModelCurrent.getAmount().compareTo(receivedOnAccountLineItemModel.getAmount()) == 1) {
                            throw new MissingDataException("Invalid Received On Account line refund amount.  The refund amount is more than the original Received On Account line", transactionModel.getTerminal().getId());
                        }

                        //Check Quantity
                        if (receivedOnAccountLineItemModelCurrent.getQuantity().compareTo(receivedOnAccountLineItemModel.getQuantity()) == 1) {
                            throw new MissingDataException("Invalid Received On Account line refund quantity.  The quantity is more than the original Received On Account line", transactionModel.getTerminal().getId());
                        }

                        //add the original refundedExtAmount + product extended Amount, is it more than original extended Amount
                        checkVoidRefundAmountWillBeTooMuchCommon(receivedOnAccountLineItemModel.getRefundedExtAmount(), receivedOnAccountLineItemModelCurrent.getExtendedAmount(), receivedOnAccountLineItemModel.getExtendedAmount(), "ROA");
                    }
                }
            }
        }
    }

    private void checkForInvalidRewardRefund() throws Exception {
        if (transactionModel.getOriginalTransaction().getRewards() != null) {
            for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getOriginalTransaction().getRewards()) {
                //Loop through the rewards on the current transaction and check which rewards to refund
                for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModelCurrent : transactionModel.getRewards()) {
                    if (loyaltyRewardLineItemModelCurrent.getId() != null && loyaltyRewardLineItemModel.getId() != null && loyaltyRewardLineItemModelCurrent.getId().equals(loyaltyRewardLineItemModel.getId())) {

                        //check the original refundedExtendedAmt vs. original extendedAmount
                        checkVoidRefundAmountCommon(loyaltyRewardLineItemModel.getRefundedExtAmount(), loyaltyRewardLineItemModel.getExtendedAmount(), "Reward");

                        //if the amount trying to be refunded is over the original amount, error out
                        //Reward Line Item will be a negative amount
                        if (loyaltyRewardLineItemModelCurrent.getExtendedAmount().compareTo(loyaltyRewardLineItemModel.getExtendedAmount()) == -1) {
                            throw new MissingDataException("Invalid Loyalty Reward line refund amount.  The refund amount is more than the original Loyalty Reward line", transactionModel.getTerminal().getId());
                        }

                        //check reward line Amount
                        //Reward Line Item will be a negative amount
                        if (loyaltyRewardLineItemModelCurrent.getAmount().compareTo(loyaltyRewardLineItemModel.getAmount()) == -1) {
                            throw new MissingDataException("Invalid Loyalty Reward line refund amount.  The refund amount is more than the original Loyalty Reward line", transactionModel.getTerminal().getId());
                        }

                        //Check Quantity
                        if (loyaltyRewardLineItemModelCurrent.getQuantity().compareTo(loyaltyRewardLineItemModel.getQuantity()) == 1) {
                            throw new MissingDataException("Invalid Loyalty Reward line refund quantity.  The quantity is more than the original Loyalty Reward line", transactionModel.getTerminal().getId());
                        }

                        //add the original refundedExtAmount + product extended Amount, is it more than original extended Amount
                        checkVoidRefundAmountWillBeTooMuchCommon(loyaltyRewardLineItemModel.getRefundedExtAmount(), loyaltyRewardLineItemModelCurrent.getExtendedAmount(), loyaltyRewardLineItemModel.getExtendedAmount(), "Reward");
                    }
                }
            }
        }
    }

    private void checkForInvalidTenderRefund() throws Exception {
        if (transactionModel.getOriginalTransaction().getTenders() != null) {
            for (TenderLineItemModel tenderLineItemModel : transactionModel.getOriginalTransaction().getTenders()) {
                //Loop through the tenders on the current transaction and check the values
                for (TenderLineItemModel tenderLineItemModelCurrent : transactionModel.getTenders()) {
                    if (tenderLineItemModelCurrent.getId() != null && tenderLineItemModel.getId() != null && tenderLineItemModelCurrent.getId().equals(tenderLineItemModel.getId())) {

                        //Only validate the Original Tender line if the current Tenderline is greater than 0
                        if (tenderLineItemModelCurrent.getExtendedAmount().compareTo(BigDecimal.ZERO) != 0) {
                            //check the original refundedExtendedAmt vs. original extendedAmount
                            checkVoidRefundAmountCommon(tenderLineItemModel.getRefundedExtAmount(), tenderLineItemModel.getExtendedAmount(), "Tender");

                            //check Extended Amount
                            if (tenderLineItemModelCurrent.getExtendedAmount().compareTo(tenderLineItemModel.getExtendedAmount()) == 1) {
                                throw new MissingDataException("Invalid Tender line refund amount.  The refund amount is more than the original Tender line", transactionModel.getTerminal().getId());
                            }

                            //check Amount
                            if (tenderLineItemModelCurrent.getAmount().compareTo(tenderLineItemModel.getAmount()) == 1) {
                                throw new MissingDataException("Invalid Tender line refund amount.  The refund amount is more than the original Tender line", transactionModel.getTerminal().getId());
                            }

                            //Check Quantity
                            if (tenderLineItemModelCurrent.getQuantity().compareTo(tenderLineItemModel.getQuantity()) == 1) {
                                throw new MissingDataException("Invalid Tender line refund quantity.  The quantity is more than the original Tender line", transactionModel.getTerminal().getId());
                            }

                            //add the original refundedExtAmount + product extended Amount, is it more than original extended Amount
                            checkVoidRefundAmountWillBeTooMuchCommon(tenderLineItemModel.getRefundedExtAmount(), tenderLineItemModelCurrent.getExtendedAmount(), tenderLineItemModel.getExtendedAmount(), "Tender");
                        }
                    }
                }
            }
        }
    }

    /**
     * For all Transactions, check if the account(s) on the original transaction have access to the current Terminal (via spending profile)
     * If it is not, throw an error
     *
     * @throws Exception
     */
    private void checkForInvalidAccountSpendingProfile() throws Exception {
        boolean accountCanCharge = true;

        //Check the Loyalty Account's spending profile mapping
        if (this.getTransactionModel().getLoyaltyAccount() != null) {

            if (this.getTransactionModel().getLoyaltyAccount().getSpendingProfileMappedForTerminal() == null || this.getTransactionModel().getLoyaltyAccount().getSpendingProfileMappedForTerminal().toString().isEmpty()) {
                accountCanCharge = false;
            }
        }

        if (this.getTransactionModel().getTenders() != null) {

            for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
                //Loop through the Tender on the current transaction and check which rewards to refund

                if (tenderLineItemModel.getAccount() != null
                        && (tenderLineItemModel.getAccount().getSpendingProfileMappedForTerminal() == null || tenderLineItemModel.getAccount().getSpendingProfileMappedForTerminal().toString().isEmpty())) {
                    accountCanCharge = false;
                }
            }
        }

        if (this.getTransactionModel().getReceivedOnAccounts() != null) {

            for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : this.getTransactionModel().getReceivedOnAccounts()) {
                //Loop through the ROA's on the current transaction and check which rewards to refund

                if (receivedOnAccountLineItemModel.getAccount() != null
                        && (receivedOnAccountLineItemModel.getAccount().getSpendingProfileMappedForTerminal() == null || receivedOnAccountLineItemModel.getAccount().getSpendingProfileMappedForTerminal().toString().isEmpty())) {

                    accountCanCharge = false;
                }
            }
        }

        if(this.getTransactionModel().getPosType().equals(PosAPIHelper.PosType.KIOSK_ORDERING)) {
            accountCanCharge = true;
        }

        if (this.getTransactionModel().getTransactionTypeEnum().equals(PosAPIHelper.TransactionType.OPEN)
                && !this.getTransactionModel().isCreateOpen()){ //Allow transactions to be modified, but no new Open transactions
            accountCanCharge = true;
        }

        if (this.getTransactionModel().getTransactionTypeEnum().equals(PosAPIHelper.TransactionType.LOYALTY_ADJUSTMENT)) {
            accountCanCharge = true;
        }

        if (!accountCanCharge) {
            throw new AccountSpendingProfileException("Account cannot charge at this location.  Check the Spending Profile.", terminalModel.getId());
        }
    }

    //see if the current refund amount plus the original transaction refunded amount will push the transaction to be voided or refunded too much
    private void checkVoidRefundAmountWillBeTooMuchCommon(BigDecimal productRefundedExtAmt, BigDecimal curProductExtAmt, BigDecimal origProductExtAmt, String lineItemType) throws Exception {

        //add the amount to refund, plus what has already been refunded on the transaction
        if (productRefundedExtAmt.abs().add(curProductExtAmt).compareTo(origProductExtAmt.abs().add(new BigDecimal(.05))) == 1) { //add and .1 just in case of any rounding issues.
            throw new MissingDataException(" Too much is being voided or refunded for a " + lineItemType + " line in this Transaction", transactionModel.getTerminal().getId());
        }
    }

    //see if any line item on the original transaction was already voided or refunded
    private void checkVoidRefundAmountCommon(BigDecimal transLineRefundedExtAmt, BigDecimal transLineExtAmt, String lineItemType) throws Exception {
        if (transLineExtAmt.abs().compareTo(BigDecimal.ZERO) != 0) {  //don't do this check if the
            if (transLineRefundedExtAmt.abs().compareTo(transLineExtAmt.abs()) >= 0) {
                throw new MissingDataException(lineItemType + " lines in this Transaction were already Voided or Refunded", transactionModel.getTerminal().getId());
            }
        }
    }

    /**
     * Make sure any submitted rewards are found in the LoyaltyAccount.rewardsAvailableForTransaction List
     *
     * @throws Exception
     */
    private void validateSubmittedRewards() throws Exception {
        ArrayList<Boolean> submittedRewardListIsValid = new ArrayList<>();

        if (this.getTransactionModel().getRewards() != null && !this.getTransactionModel().getRewards().isEmpty()) {

            for (LoyaltyRewardLineItemModel loyaltyRewardLineSubmitted : this.getTransactionModel().getRewards()) {
                boolean submittedRewardIsValid = false;

                for (LoyaltyRewardModel loyaltyRewardModel : this.getTransactionModel().getLoyaltyAccount().getRewardsAvailable()) {
                    if (loyaltyRewardLineSubmitted.getReward().getId().equals(loyaltyRewardModel.getId())) {
                        submittedRewardIsValid = true;
                        break;
                    }
                }

                submittedRewardListIsValid.add(submittedRewardIsValid);
            }

            if (submittedRewardListIsValid.contains(false)) {
                throw new InvalidRewardSubmittedException("Invalid Loyalty Reward Submitted");
            }
        }
    }

    /**
     * If there transaction had a "change" tender, do this check so the "change" tender doesn't get voided or refunded.
     *
     * @throws Exception
     */
    public void prepareTendersForRefundVoid() throws Exception {

        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case REFUND:
            case VOID:

                if (transactionModel.getTenders() != null
                        && !transactionModel.getTenders().isEmpty()
                        && transactionModel.getTenders().size() > 1) {

                    //5.00 full tender
                    //-1.85 change

                    //-match on type
                    HashMap<Integer, BigDecimal> tenderHM = new HashMap<>();

                    for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()) {
                        if (tenderLineItemModel.getTender() != null && tenderLineItemModel.getTender().getId() != null && !tenderLineItemModel.getTender().getId().toString().isEmpty()) {
                            if (tenderLineItemModel.getExtendedAmount().compareTo(BigDecimal.ZERO) == -1) {
                                //tender amount is a positive.  At this point the signs haven't been flipped

                                tenderHM.put(tenderLineItemModel.getTender().getId(), tenderLineItemModel.getExtendedAmount());
                            }
                        }
                    }

                    for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()) {
                        if (tenderLineItemModel.getTender() != null && tenderLineItemModel.getTender().getId() != null && !tenderLineItemModel.getTender().getId().toString().isEmpty()) {
                            if (tenderLineItemModel.getExtendedAmount().compareTo(BigDecimal.ZERO) == 1) {
                                //tender amount is a negative, this is the amount we have to change

                                BigDecimal changeAmountToSubtract = tenderHM.get(tenderLineItemModel.getTender().getId());
                                if (changeAmountToSubtract != null) {
                                    BigDecimal newTenderAmount = tenderLineItemModel.getExtendedAmount().add(changeAmountToSubtract);

                                    //set the new tender amount
                                    tenderLineItemModel.setAmount(newTenderAmount);
                                    tenderLineItemModel.setExtendedAmount(tenderLineItemModel.getAmount().multiply(tenderLineItemModel.getQuantity()));
                                }
                            }
                        }
                    }

                    if (transactionModel.getTenders() != null
                            && !transactionModel.getTenders().isEmpty()) {
                        Iterator<TenderLineItemModel> removeTenderForChange = transactionModel.getTenders().iterator();
                        while (removeTenderForChange.hasNext()) {
                            TenderLineItemModel curTenderLineItem = removeTenderForChange.next();

                            //if tender line item amount is less then zero, that means it was part of the change
                            //burger was $3.50, tender amount = 5, change tender -$1.50
                            if (curTenderLineItem.getExtendedAmount().compareTo(BigDecimal.ZERO) == -1) {
                                removeTenderForChange.remove();
                            }
                        }
                    }
                }

                break;
        }
    }

    /**
     * For Full Refunds and Voids
     * Clear out the Loyalty Points on the TransactionModel that is sent in to the API
     * And replace them with the original Loyalty Points from the database
     *
     * @throws Exception
     */
    public void prepareLoyaltyPointsForRefundVoid() throws Exception {

        //for voids or refunds, refresh the loyalty points from the original transaction
        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case REFUND:
            case VOID:

                //remove all the loyalty points what were potentially sent in
                this.getTransactionModel().removeLoyaltyPoints();

                //reset the Loyalty Account Points from the original transaction
                this.getTransactionModel().setLoyaltyPointsAll(this.getTransactionModel().getOriginalTransaction().getLoyaltyPointsAll());

                for (ProductLineItemModel productLineItemOrig : this.getTransactionModel().getOriginalTransaction().getProducts()) {
                    for (ProductLineItemModel productLineItemRefund : this.getTransactionModel().getProducts()) {
                        if (productLineItemOrig.getId().equals(productLineItemRefund.getId())) {
                            //Reset the Item Loyalty Points from the original transaction
                            productLineItemRefund.setLoyaltyPointDetails(productLineItemOrig.getLoyaltyPointDetails());
                        }
                    }
                }
                break;
        }
    }

    public void prepareCombosForRefundVoid(){
        if (this.getTransactionModel().getCombos() != null){
            for (ComboLineItemModel comboLineItemModel : this.getTransactionModel().getCombos()){
                comboLineItemModel.setRefundComplete(true);
            }
        }
    }

    //check if the refund is a full or partial refund.
    public boolean isFullRefund() throws Exception {

        TransactionModel originalTransactionModel = new TransactionModel();
        boolean isFullRefund = true;

        //removeLoyaltyPoints(); //remove any loyalty points on the incoming transaction model, the api will recalculate them

        if (this.getTransactionModel().getOriginalTransaction() != null) {
            for (ProductLineItemModel productLineOriginal : this.getTransactionModel().getOriginalTransaction().getProducts()) {
                boolean prodIsFound = false;
                boolean prodValueIsSameAsOrigValue = false;
                boolean prodQuantIsSameAsOrigValue = false;

                for (ProductLineItemModel productLineRefund : this.getTransactionModel().getProducts()) {

                    //match original product line id on incoming product line id
                    if (productLineOriginal.getId().equals(productLineRefund.getId())) {
                        //this proves product line item is in the incoming transaction
                        prodIsFound = true;

                        //if it's a quantity update, mark the transaction line.  This is used in the refund method
                        if (productLineOriginal.getQuantity().compareTo(BigDecimal.ONE) == 1) {
                            productLineRefund.setIsQuantityRefund(true);

                            if (productLineOriginal.getRefundedAmount().compareTo(BigDecimal.ZERO) == 0) {
                                //this is a Quantity update, and some of the amount has been updated
                                productLineRefund.setRefundTheAmount(true);
                            }
                        }

                        //check for partial refunds
                        if (productLineOriginal.getExtendedAmount().setScale(2, RoundingMode.HALF_UP).compareTo(productLineRefund.getExtendedAmount().setScale(2, RoundingMode.HALF_UP)) == 0) {
                            //this proves that this product line item is a full refund
                            prodValueIsSameAsOrigValue = true;
                            //continue;
                        }

                        //check for partial refunds
                        if (productLineOriginal.getQuantity().setScale(2, RoundingMode.HALF_UP).compareTo(productLineRefund.getQuantity().setScale(2, RoundingMode.HALF_UP)) == 0) {
                            //this proves that this product line item is a full refund
                            prodQuantIsSameAsOrigValue = true;
                            //continue;
                        }
                    }
                }
                if (!prodIsFound || !prodValueIsSameAsOrigValue || !prodQuantIsSameAsOrigValue) {
                    isFullRefund = false;
                }
            }

            return isFullRefund;
        }

        //Full Refund
        if (isFullRefund) {
            return true;
            //the original transaction was already set in the Endpoint resource
            //do nothing here
        } else {  //partial refund
            return false;
        }
    }

    //validate the transaction header.  This is used for QCPOS and ThirdParty
    public void validateTransactionHeaderCommon() throws Exception {

        this.validateTimeStamp();
        this.validateLinkedFromTransactionFields();
        this.validateTicketOpenedTimeStamp();
        this.validateLoyaltyAccountPoints();
        this.checkForTenderTypes();
        this.validateOrderType();
        this.validateUserId();
        this.validatePaymentMethodType();
        this.validateCcAuthorizationTypeId();
        this.validateLoyaltyDonationTransaction();
        this.getTransactionModel().populateLinkedFromPATransactionIdsCSV();
    }

    public void validateQCPosTransactionHeader() throws Exception {

        this.validateTransactionHeaderCommon();

        //validate UserId
        CommonAPI.checkIsNullOrEmptyObject(this.getTransactionModel().getUserId(), "Invalid User ID on Transaction.  Please try again.", transactionModel.getTerminal().getId());

        //validate DrawerNumber
        CommonAPI.checkIsNullOrEmptyObject(this.getTransactionModel().getDrawerNum(), "Invalid Drawer Number on Transaction.  Please try again.", transactionModel.getTerminal().getId());

        //validate the manual refund
        //if there are Redeemed Rewards, throws an error
        if (this.getTransactionModel().getApiActionTypeEnum().equals(PosAPIHelper.ApiActionType.REFUND_MANUAL)) {
            if (this.getTransactionModel().getRewards() != null && !this.getTransactionModel().getRewards().isEmpty()) {
                throw new MissingDataException("Rewards are not valid for a Manual Refund Transaction.  Please try again.", transactionModel.getTerminal().getId());
            }
        }
    }

    public void validateThirdPartyTransactionHeader() throws Exception {
        validateTransactionHeaderCommon();

        if (!this.getTransactionModel().isCancelTransactionType() &&
                !this.getTransactionModel().isTrainingTransactionType() &&
                !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {
            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:
                case ROA:
                    // validate that transReferenceNumber is present
                    CommonAPI.checkIsNullOrEmptyObject(getTransactionModel().getTransReferenceNumber(), "Invalid transaction reference number. Please try again.", transactionModel.getTerminal().getId());
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * If the LinkedFromPATransaction field is set, validate the fields that are affected
     * TicketOpenedDTM, EstimatedOrderTime
     * This should be validated for SALE and OPEN transaction types
     * @throws Exception
     */
    public void validateLinkedFromTransactionFields() throws Exception {
        //validate Ticket Opened TimeStamp
        switch (this.getTransactionModel().getPosType()) {
            case MMHAYES_POS:
            case THIRD_PARTY:

                switch (this.getTransactionModel().getTransactionTypeEnum()){
                    case SALE:
                    case OPEN:
                        this.getTransactionModel().checkAndSetLinkedFromEarliestDates();
                        break;
                }

                break;

            case MY_QC_FUNDING:
            case ONLINE_ORDERING:
                //For Online Ordering, default the TicketOpenedDTM field to "now" which is set by the database

                break;

        }
    }

    public void validateTimeStamp() throws Exception {
        //validate TimeStamp
        CommonAPI.checkIsNullOrEmptyObject(this.getTransactionModel().getTimeStamp(), "Invalid Time Stamp on Transaction.", transactionModel.getTerminal().getId());

        boolean dateIsValid = false;
        Date validDate = null;

        try {
            //example we are looking for: 5/25/2017 9:22:07
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            dateFormat.setLenient(false);
            validDate = dateFormat.parse(this.getTransactionModel().getTimeStamp());
            dateIsValid = true;

        } catch (Exception ex) {
        }

        try {
            //example we are looking for: 5/25/2017 9:22:07 AM
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
            dateFormat.setLenient(false);
            validDate = dateFormat.parse(this.getTransactionModel().getTimeStamp());
            dateIsValid = true;

        } catch (Exception ex) {
        }

        try {
            //example we are looking for: 5-25-2017 9:22:07:000
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.S");
            dateFormat.setLenient(false);
            validDate = dateFormat.parse(this.getTransactionModel().getTimeStamp());
            dateIsValid = true;

        } catch (Exception ex) {
        }

        try {
            //example we are looking for: 2017-5-25 19:22:07:000
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            dateFormat.setLenient(false);
            validDate = dateFormat.parse(this.getTransactionModel().getTimeStamp());
            dateIsValid = true;

        } catch (Exception ex) {
        }

        if (dateIsValid && validDate != null) {
            Calendar longago = new GregorianCalendar();
            longago.add(Calendar.YEAR, -100);
            Calendar longfromnow = new GregorianCalendar();
            longfromnow.add(Calendar.YEAR, 100);
            Calendar validCal = new GregorianCalendar();
            validCal.setTime(validDate);
            if (validCal.after(longfromnow) || validCal.before(longago))
                dateIsValid = false;
        }

        if (!dateIsValid) {
            throw new MissingDataException("Invalid Time Stamp on Transaction.", transactionModel.getTerminal().getId());
        }
    }

    public void validateTicketOpenedTimeStamp() throws Exception {
        //validate Ticket Opened TimeStamp
        switch (this.getTransactionModel().getPosType()) {
            case MMHAYES_POS:
            case THIRD_PARTY:

                if (this.getTransactionModel().getTicketOpenedDTM() == null || this.getTransactionModel().getTicketOpenedDTM().isEmpty()) {
                    this.getTransactionModel().setTicketOpenedDTM(this.getTransactionModel().getTimeStamp());
                }

                break;

            case MY_QC_FUNDING:
            case ONLINE_ORDERING:
                //For Online Ordering, default the TicketOpenedDTM field to "now" which is set by the database

                break;

        }

        //Only validate the date if it is populated
        if (this.getTransactionModel().getTicketOpenedDTM() != null && !this.getTransactionModel().getTicketOpenedDTM().isEmpty()) {

            boolean dateIsValid = false;
            Date validDate = null;

            try {
                //example we are looking for: 5/25/2017 9:22:07
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                dateFormat.setLenient(false);
                validDate = dateFormat.parse(this.getTransactionModel().getTicketOpenedDTM());
                dateIsValid = true;

            } catch (Exception ex) {
            }

            try {
                //example we are looking for: 5/25/2017 9:22:07 AM
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
                dateFormat.setLenient(false);
                validDate = dateFormat.parse(this.getTransactionModel().getTicketOpenedDTM());
                dateIsValid = true;

            } catch (Exception ex) {
            }

            try {
                //example we are looking for: 5-25-2017 9:22:07:000
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.S");
                dateFormat.setLenient(false);
                validDate = dateFormat.parse(this.getTransactionModel().getTicketOpenedDTM());
                dateIsValid = true;

            } catch (Exception ex) {
            }

            try {
                //example we are looking for: 2017-5-25 19:22:07:000
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                dateFormat.setLenient(false);
                validDate = dateFormat.parse(this.getTransactionModel().getTicketOpenedDTM());
                dateIsValid = true;

            } catch (Exception ex) {
            }

            if (dateIsValid && validDate != null) {
                Calendar longago = new GregorianCalendar();
                longago.add(Calendar.YEAR, -100);
                Calendar longfromnow = new GregorianCalendar();
                longfromnow.add(Calendar.YEAR, 100);
                Calendar validCal = new GregorianCalendar();
                validCal.setTime(validDate);
                if (validCal.after(longfromnow) || validCal.before(longago))
                    dateIsValid = false;
            }

            if (!dateIsValid) {
                throw new MissingDataException("Invalid Ticket Opened Time Stamp on Transaction.", transactionModel.getTerminal().getId());
            }
        }
    }

    public void checkForTenderTypes() throws Exception {
        //check the tenders on the transaction
        //then mark the transaction whether it has cash, quickcharge, credit card, check, etc.
        if (this.getTransactionModel().getTenders() != null && this.getTransactionModel().getTenders().size() > 0) {
            for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
                if (tenderLineItemModel.getTender() != null && tenderLineItemModel.getTender().getTenderTypeId() != null) {

                    PosAPIHelper.TenderType tenderTypeEnum = PosAPIHelper.TenderType.intToEnum(tenderLineItemModel.getTender().getTenderTypeId());

                    switch (tenderTypeEnum) {
                        case CASH:
                            this.getTransactionModel().setHasCashTender(true);
                            break;
                        case QUICKCHARGE:
                            //this is used when we calculateQcLines
                            this.getTransactionModel().setQcTenderBalance(this.getTransactionModel().getQcTenderBalance().add(tenderLineItemModel.getExtendedAmount()));

                            //set that the Transaction has a QC Tender
                            this.getTransactionModel().setHasQuickChargeTender(true);
                            break;
                        case CREDIT_CARD:
                            this.getTransactionModel().setHasCreditCardTender(true);
                            break;
                        case CHECK:

                            break;
                    }
                }
            }
        }
    }

    public void validateOrderType() throws Exception {
        //Validate the OrderType.  It should be "Normal Sale", " Delivery Sale", or "Pickup Sale"
        if (this.getTransactionModel().getOrderType() != null && !this.getTransactionModel().getOrderType().isEmpty()) {
            String orderTypeName = this.getTransactionModel().getOrderType();
            Integer orderTypeID = PosAPIHelper.OrderType.convertStringToInt(orderTypeName);

            if (orderTypeID != PosAPIHelper.OrderType.NORMALSALE.toInt() && orderTypeID != PosAPIHelper.OrderType.DELIVERYSALE.toInt() && orderTypeID != PosAPIHelper.OrderType.PICKUPSALE.toInt()) {
                Logger.logMessage("Invalid Transaction Order Type was found.  TransactionBuilder.validateTransactionHeader", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.TRACE);
                throw new MissingDataException("Invalid Transaction Order Type.", transactionModel.getTerminal().getId());
            }
        }
    }

    public void validateUserId() throws Exception {
        // If UserID is provided, make sure it's in the database (and active and not locked).
        // This applies only to SALE transactions (and only if it's not training and not cancelled)
        if (getTransactionModel().getUserId() != null) {
            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:
                case REFUND:
                case REFUND_MANUAL:
                case VOID:
                case ROA:
                case PO:
                case BATCHCLOSE:
                case NOSALE:
                case OPEN:
                    //validate the userId and fetch the userName from QC_QuickChargeUsers
                    String userName = transactionModel.getValidUserName(transactionModel.getUserId()); //validate the userId on the request, not on the transaction
                    transactionModel.setUserName(userName);
                    break;
                default:
                    break;
            }
        }
    }

    public void validatePaymentMethodType() throws Exception {
        // If paymentMethodType is provided, make sure it's in the database.
        // This applies to all common transaction types (and only if it's not training and not cancelled)
        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case SALE:
            case REFUND:
            case REFUND_MANUAL:
            case VOID:
            case ROA:
                for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
                    if (tenderLineItemModel.getPaymentMethodType() != null && !tenderLineItemModel.getPaymentMethodType().isEmpty()) {
                        Integer paymentMethodTypeId = transactionModel.getValidPaymentMethodTypeId(tenderLineItemModel.getPaymentMethodType()); //validate the userId on the request, not on the transaction
                        tenderLineItemModel.setPaymentMethodTypeId(paymentMethodTypeId);
                    }
                }
                break;
            default:
                break;
        }
    }

    public void validateCcAuthorizationTypeId() throws Exception {
        // If CC Authorization Type is provided, make sure it's in the database.
        // This applies to all common transaction types (and only if it's not training and not cancelled)
        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case SALE:
            case REFUND:
            case REFUND_MANUAL:
            case VOID:
            case ROA:
                for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
                    if (tenderLineItemModel.getCcAuthorizationTypeId() != null && !tenderLineItemModel.getCcAuthorizationTypeId().toString().isEmpty()) {
                        transactionModel.getValidCcAuthorizationTypeId(tenderLineItemModel.getCcAuthorizationTypeId());
                    }
                }
                break;
            default:
                break;
        }
    }

    private void validateLoyaltyDonationTransaction() throws Exception {
        if (this.getTransactionModel().getTransactionTypeEnum() == PosAPIHelper.TransactionType.LOYALTY_ADJUSTMENT) {
            if (this.getTransactionModel().getLoyaltyAdjustmentTypeId() == null
                    || this.getTransactionModel().getLoyaltyAdjustmentTypeId() < 1) {
                throw new MissingDataException("Invalid Loyalty Adjustment Type Id submitted");
            }
        } else {
            if (this.getTransactionModel().getLoyaltyAdjustmentTypeId() != null){
                throw new MissingDataException("Invalid Loyalty Adjustment Type Id submitted");
            }
        }
    }

    /**
     * This will check the total transaction balance vs. the tender amounts
     * It will error out if the tender has AllowOverTender = false and the transaction balance is > tender  balance
     */
    private void validateTransactionBalance() throws Exception {
        BigDecimal totalTenderAmount = BigDecimal.ZERO;
        BigDecimal totalProductAmount = BigDecimal.ZERO;
        Integer tenderCount = 0;

        //Tender total
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            totalTenderAmount = totalTenderAmount.add(tenderLineItemModel.getExtendedAmount());
            tenderCount++;
        }
        Logger.logMessage("TransactionCalc:  Ttl Tenders: $" + totalTenderAmount, PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.TRACE);

        //if (this.getTransactionModel().getProducts() != null && !this.getTransactionModel().getProducts().isEmpty()) {
        switch (this.getTransactionModel().getPosType()) {
            case THIRD_PARTY:
                totalProductAmount = this.getTransactionModel().getTotalTransactionAmount();
                break;

            default:

                for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                    totalProductAmount = totalProductAmount.add(ProductLineItemModel.getAdjValOfExtendedAmountForTransaction(productLineItemModel));
                }

                for (ServiceChargeLineItemModel serviceChargeLineItemModel : this.getTransactionModel().getServiceCharges()) {
                    totalProductAmount = totalProductAmount.add(serviceChargeLineItemModel.getAmount());
                }

                for (GratuityLineItemModel gratuityLineItemModel : this.getTransactionModel().getGratuities()) {
                    totalProductAmount = totalProductAmount.add(gratuityLineItemModel.getAmount());
                }

                break;
        }

        totalProductAmount = totalProductAmount.setScale(2, RoundingMode.HALF_UP);
        Logger.logMessage("TransactionCalc:  Ttl Products: $" + totalProductAmount, PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.TRACE);

        if (this.getTransactionModel().getRewards() != null
                && !this.getTransactionModel().getRewards().isEmpty()
                && totalProductAmount.compareTo(BigDecimal.ZERO) == -1) {
            throw new MissingDataException("Reward Application Error: too much has been applied", transactionModel.getTerminal().getId());
        }

        //The total tender doesn't cover all the product costs
        if (totalTenderAmount.compareTo(totalProductAmount) == -1) {
            throw new MissingDataException("Insufficient Tender", transactionModel.getTerminal().getId());
        }

        //check each Tender to see if it is Under/Over Tendered, and if it allows to be Under/Over Tendered
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            if (tenderLineItemModel.isUnderTender(totalProductAmount)) { //Under Tender
                throw new MissingDataException("Transaction is an Under Tender, one or more tenders doesn't allow this", transactionModel.getTerminal().getId());
            } else if (tenderLineItemModel.isOverTender(this.getTransactionModel(), totalProductAmount)){
                throw new MissingDataException("Transaction is an Over Tender, one or more tenders doesn't allow this", transactionModel.getTerminal().getId());
            }
        }
    }

    /**
     * This will check the total transaction balance vs. the tender amounts
     * It will error out if the tender has AllowOverTender = false and the transaction balance is > tender  balance
     */
    private void validateROATransactionBalance() throws Exception {
        BigDecimal totalTenderAmount = BigDecimal.ZERO;
        BigDecimal totalReceivedOnAccountAmount = BigDecimal.ZERO;
        Integer tenderCount = 0;

        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            totalTenderAmount = totalTenderAmount.add(tenderLineItemModel.getExtendedAmount());
            tenderCount++;
        }

        for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : this.getTransactionModel().getReceivedOnAccounts()) {
            totalReceivedOnAccountAmount = totalReceivedOnAccountAmount.add(receivedOnAccountLineItemModel.getAmount());
        }

        //The total tender doesn't cover all the product costs
        if (totalTenderAmount.compareTo(totalReceivedOnAccountAmount) == -1) {
            throw new MissingDataException("Insufficient Tender", transactionModel.getTerminal().getId());
        }

        //check each Tender to see if it is Under Tendered, and if it allows to be Under Tendered
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            if (tenderLineItemModel.getExtendedAmount().compareTo(totalReceivedOnAccountAmount) == -1) { //Under Tender

                if (tenderLineItemModel.getTender().isAllowUnderTender() == false) {
                    {
                        throw new MissingDataException("Transaction is an Under Tender, one or more tenders doesn't allow this", transactionModel.getTerminal().getId());
                    }
                }
            } else if (tenderLineItemModel.getExtendedAmount().compareTo(totalReceivedOnAccountAmount) == 1) { //Over Tender

                if (tenderLineItemModel.getTender().isAllowOverTender() == false) {
                    throw new MissingDataException("Transaction is an Over Tender, one or more tenders doesn't allow this", transactionModel.getTerminal().getId());
                }
            }
        }
    }

    private void validateQuickChargeBalances() throws Exception {

        HashMap<AccountModel, BigDecimal> tenderTotalByAcct = new HashMap<>(); //keep track of Tender totals by account

        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {

            //check for a quick charge type
            if (((tenderLineItemModel.getTender().getTenderTypeId() != null
                    && tenderLineItemModel.getTender().getTenderTypeId() == PosAPIHelper.TenderType.QUICKCHARGE.toInt()) || (tenderLineItemModel.getTender().getTenderTypeName() != null
                    && tenderLineItemModel.getTender().getTenderTypeName().equalsIgnoreCase(PosAPIHelper.TenderType.QUICKCHARGE.toString())))) {

                switch (this.getTransactionModel().getApiActionTypeEnum()) {
                    case VOID:
                    case REFUND:
                    case REFUND_MANUAL:

                        //Don't validate balances for VOID and REFUND transaction
                        break;
                    default:
                        AccountModel accountModel = tenderLineItemModel.getAccount();
                        BigDecimal tenderAmount = tenderLineItemModel.getAmount();
                        switch (this.getTransactionModel().getPosType()) {
                            case THIRD_PARTY:
                                //For Third Party API, take the QC Discounts/Meal Plan into the calculation
                                if (this.getTransactionModel().getTotalQcDiscountAmount() != null && this.getTransactionModel().getTotalQcDiscountAmount().compareTo(BigDecimal.ZERO) != 0) {
                                    //i.e. - $5 + -$5
                                    tenderAmount = tenderAmount.add(this.getTransactionModel().getTotalQcDiscountAmount());
                                }
                                break;
                        }

                                //Check the Single Charge Limit (Daily Limit)
                                if (tenderAmount != null && accountModel.getSingleChargeLimit() != null) {
                                    if (tenderAmount.compareTo(accountModel.getSingleChargeLimit()) == 1) {
                                        throw new TransactionIndividualLimitException(this.getTransactionModel().getTerminal().getId());
                                    }
                                }

                        //Check the Store balance
                        if (accountModel.getTerminalGroupBalance() != null && accountModel.getTerminalGroupLimit() != null) {

                            //First add the Tender Amount to the Store Balance
                            BigDecimal newStoreBalanceAfterTx = accountModel.getTerminalGroupBalance().add(tenderAmount);

                            //if new balance after transaction is greater than the Global Limit, error out
                            if (newStoreBalanceAfterTx.compareTo(accountModel.getTerminalGroupLimit()) == 1) {
                                throw new TransactionStoreLimitException(this.getTransactionModel().getTerminal().getId());
                            }
                        }

                        //Check the Global Balance
                        if (accountModel.getGlobalBalance() != null && accountModel.getGlobalLimit() != null) {

                            //First add the Tender Amount to the Global Balance
                            BigDecimal newGlobalBalanceAfterTx = accountModel.getGlobalBalance().add(tenderAmount);

                            //if new balance after transaction is greater than the Global Limit, error out
                            if (newGlobalBalanceAfterTx.compareTo(accountModel.getGlobalLimit()) == 1) {
                                throw new TransactionGlobalLimitException(this.getTransactionModel().getTerminal().getId());
                            }
                        }

                        //Check Splits (or number of payments)
                        if ((tenderLineItemModel.getPayments() != null && tenderLineItemModel.getAccount().getEligiblePayments() != null)) {
                            //compare the incoming split amount verses the Eligible splits
                            if (tenderLineItemModel.getPayments() > tenderLineItemModel.getAccount().getEligiblePayments()) {
                                throw new MissingDataException("Error: Number of Payments on Tender is greater than the Eligible Splits for this Account", this.getTransactionModel().getTerminal().getId());
                            } else {
                                //set the splits on the QC Transaction
                                //this.getTransactionModel().setSplitsQC(tenderLineItemModel.getEligiblePayments());
                            }
                        }
                        break;
                }
            }
        }
    }

    //validate the incoming transaction type with the API Action Type to make sure it's valid.
    // e.g.- you can't refund a voided transaction
    private void validateTransactionType() throws Exception {

        boolean invalidTransType = false;
        StringBuilder customError = new StringBuilder();
        customError.append("This Transaction Type cannot be "); //dynamically build the error message

        if (!this.getTransactionModel().isCancelTransactionType() &&
                !this.getTransactionModel().isTrainingTransactionType() &&
                !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {

            //endpoint
            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:
                    switch (this.getTransactionModel().getTransactionTypeEnum()) {
                        case SALE:

                            break;

                        case REFUND:
                        case VOID:
                            if (this.getTransactionModel().isInquiry()) {
                                //allow this.  06/08/2017 egl - HP 523

                            } else {
                                invalidTransType = true;
                                customError.append("passed to the sale endpoint");

                            }

                            break;
                        default:
                            invalidTransType = true;
                            customError.append("passed to the sale endpoint");
                            break;
                    }
                    break;
                case VOID:

                    switch (this.getTransactionModel().getTransactionTypeEnum()) {
                        case SALE:
                        case PO:
                        case ROA:

                            break;
                        default:
                            invalidTransType = true;
                            customError.append("voided");
                            break;
                    }
                    break;
                case REFUND:
                    switch (this.getTransactionModel().getTransactionTypeEnum()) {
                        case SALE:
                        case ROA:

                            break;
                        default:
                            invalidTransType = true;
                            customError.append("refunded");
                            break;
                    }
                    break;
                case REFUND_MANUAL:
                    switch (this.getTransactionModel().getTransactionTypeEnum()) {
                        case SALE:
                        case ROA:
                        case REFUND:

                            break;
                        default:
                            invalidTransType = true;
                            customError.append("refunded");
                            break;
                    }
                    break;
                case CASHIERSHIFTEND:
                    if (this.getTransactionModel().getTransactionTypeEnum() != PosAPIHelper.TransactionType.CASHIERSHIFTEND) {
                        invalidTransType = true;
                        customError.append("used for Cashier Shift End");
                    }
                    break;
                case NOSALE:
                    if (this.getTransactionModel().getTransactionTypeEnum() != PosAPIHelper.TransactionType.NOSALE) {
                        invalidTransType = true;
                        customError.append("used for No Sale");
                    }
                    break;
                case BROWSERCLOSE:
                    if (this.getTransactionModel().getTransactionTypeEnum() != PosAPIHelper.TransactionType.BROWSERCLOSE) {
                        invalidTransType = true;
                        customError.append("used for Browser Close");
                    }
                    break;
                case LOGIN:
                    if (this.getTransactionModel().getTransactionTypeEnum() != PosAPIHelper.TransactionType.LOGIN) {
                        invalidTransType = true;
                        customError.append("used for Login");
                    }
                    break;
                case LOGOUT:
                    if (this.getTransactionModel().getTransactionTypeEnum() != PosAPIHelper.TransactionType.LOGOUT) {
                        invalidTransType = true;
                        customError.append("used for Logout");
                    }
                    break;
                case ROA:
                    switch (this.getTransactionModel().getTransactionTypeEnum()) {
                        case ROA:

                            break;
                        default:
                            invalidTransType = true;
                            customError.append("used for Received On Account");
                            break;
                    }
                    break;
                case PO:
                    switch (this.getTransactionModel().getTransactionTypeEnum()) {
                        case PO:

                            break;
                        default:
                            invalidTransType = true;
                            customError.append("used for Paid Out");
                            break;
                    }
                    break;
                case BATCHCLOSE:
                    if (this.getTransactionModel().getTransactionTypeEnum() != PosAPIHelper.TransactionType.BATCHCLOSE) {
                        invalidTransType = true;
                        customError.append("used for Batch Close");
                    }
                    break;
                case OPEN:
                    switch (this.getTransactionModel().getTransactionTypeEnum()) {
                        case OPEN:

                            break;
                        default:
                            invalidTransType = true;
                            customError.append("opened");
                            break;
                    }
                    break;
            }
            if (invalidTransType) {
                throw new MissingDataException(customError.toString(), transactionModel.getTerminal().getId());
            }


            //Validate the Transaction Type Id with the endpoint
            boolean invalidTransTypeId = false;

            //endpoint
            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:
                    switch (this.getTransactionModel().getTransactionTypeId()) {
                        case 1:
                            break;

                        case 2:
                        case 3:
                            if (this.getTransactionModel().isInquiry()) {
                                //allow this.  06/08/2017 egl - HP 523

                            } else {
                                invalidTransTypeId = true;
                            }

                            break;
                        default:
                            invalidTransTypeId = true;

                            break;
                    }
                    break;
                case VOID:

                    switch (this.getTransactionModel().getTransactionTypeId()) {
                        case 1:
                        case 11:
                        case 12:

                            break;
                        default:
                            invalidTransTypeId = true;
                            break;
                    }
                    break;
                case REFUND:
                    switch (this.getTransactionModel().getTransactionTypeId()) {
                        case 1:
                        case 11:

                            break;
                        default:
                            invalidTransTypeId = true;
                            break;
                    }
                    break;
                case REFUND_MANUAL:
                    switch (this.getTransactionModel().getTransactionTypeId()) {
                        case 1:
                        case 11:
                        case 3:

                            break;
                        default:
                            invalidTransTypeId = true;
                            break;
                    }
                    break;
                case CASHIERSHIFTEND:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.CASHIERSHIFTEND.toInt()) {
                        invalidTransTypeId = true;
                    }
                    break;
                case NOSALE:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.NOSALE.toInt()) {
                        invalidTransTypeId = true;
                    }
                    break;
                case BROWSERCLOSE:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.BROWSERCLOSE.toInt()) {
                        invalidTransTypeId = true;
                    }
                    break;
                case LOGIN:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.LOGIN.toInt()) {
                        invalidTransTypeId = true;
                    }
                    break;
                case LOGOUT:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.LOGOUT.toInt()) {
                        invalidTransTypeId = true;
                    }
                    break;
                case ROA:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.ROA.toInt()) {
                        invalidTransTypeId = true;
                    }
                    break;
                case PO:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.PO.toInt()) {
                        invalidTransTypeId = true;
                    }
                    break;
                case BATCHCLOSE:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.BATCHCLOSE.toInt()) {
                        invalidTransTypeId = true;
                    }
                    break;
                case OPEN:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.OPEN.toInt()) {
                        invalidTransTypeId = true;
                    }
                    break;

            }
            if (invalidTransTypeId) {
                throw new MissingDataException("Invalid Transaction Type Id for Endpoint", transactionModel.getTerminal().getId());
            }


            //Validate the Transaction Type Name with the endpoint
            boolean invalidTransTypeName = false;

            //endpoint
            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:
                    switch (this.getTransactionModel().getTransactionType()) {
                        case "SALE":
                        case "SUSPENDED":

                            break;
                        case "REFUND":
                        case "VOID":

                            if (this.getTransactionModel().isInquiry()) {
                                //allow this.  06/08/2017 egl - HP 523

                            } else {
                                invalidTransTypeName = true;
                            }

                            break;
                        default:
                            invalidTransTypeName = true;
                            break;
                    }
                    break;
                case VOID:
                    switch (this.getTransactionModel().getTransactionType()) {
                        case "SALE":
                        case "PO":
                        case "PAID OUT":
                        case "ROA":
                        case "RECEIVED ON ACCOUNT":

                            break;
                        default:
                            invalidTransTypeName = true;
                            break;
                    }
                    break;
                case REFUND:
                    switch (this.getTransactionModel().getTransactionType()) {
                        case "SALE":
                        case "ROA":
                        case "RECEIVED ON ACCOUNT":

                            break;
                        default:
                            invalidTransTypeName = true;
                            break;
                    }
                    break;
                case REFUND_MANUAL:
                    switch (this.getTransactionModel().getTransactionType()) {
                        case "SALE":
                        case "ROA":
                        case "RECEIVED ON ACCOUNT":
                        case "REFUND":

                            break;
                        default:
                            invalidTransTypeName = true;
                            break;
                    }
                    break;
                case CASHIERSHIFTEND:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.CASHIERSHIFTEND.toInt()) {
                        invalidTransTypeName = true;
                    }
                    break;
                case NOSALE:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.NOSALE.toInt()) {
                        invalidTransTypeName = true;
                    }
                    break;
                case BROWSERCLOSE:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.BROWSERCLOSE.toInt()) {
                        invalidTransTypeName = true;
                    }
                    break;
                case LOGIN:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.LOGIN.toInt()) {
                        invalidTransTypeName = true;
                    }
                    break;
                case LOGOUT:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.LOGOUT.toInt()) {
                        invalidTransTypeName = true;
                    }
                    break;
                case ROA:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.ROA.toInt()) {
                        invalidTransTypeName = true;
                    }
                    break;
                case PO:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.PO.toInt()) {
                        invalidTransTypeName = true;
                    }
                    break;
                case BATCHCLOSE:
                    if (this.getTransactionModel().getTransactionTypeId() != PosAPIHelper.TransactionType.BATCHCLOSE.toInt()) {
                        invalidTransTypeName = true;
                    }
                    break;
                case OPEN:
                    switch (this.getTransactionModel().getTransactionType()) {
                        case "OPEN":

                            break;
                        default:
                            invalidTransTypeName = true;
                            break;
                    }
                    break;
            }
            if (invalidTransTypeName) {
                throw new MissingDataException("Invalid Transaction Type Name for Endpoint", transactionModel.getTerminal().getId());
            }
        }
    }

    public void buildTransactionReceipt() throws Exception {
        this.setTransactionModel(TransactionLineBuilder.validateTransactionLinesBasic(this.getTransactionModel()));
    }

    public TransactionCalculation calculateTransaction() throws Exception {
        TransactionCalculation transactionCalculation = this.initTransactionCalculation();
        this.applyCombos(transactionCalculation);
        this.applyDiscounts(transactionCalculation);
        this.applyLoyaltyInfo();
        this.applyTaxes(transactionCalculation);
        this.applySurcharges(transactionCalculation);

        if (!this.getTransactionModel().isVoucherCalcInProgress()){
            this.applyVouchers(transactionCalculation);
        }

        new TransactionItemNumCollection(this.getTransactionModel()).checkAndSetTransactionItemNumbers();

        return transactionCalculation;
    }

    public TransactionCalculation initTransactionCalculation() throws Exception {
        //create the transaction calculation for POS after the trans lines are validated

        TransactionCalculation transactionCalculation = new TransactionCalculation();

        switch (transactionModel.getPosType()) {
            case MMHAYES_POS:
            case THIRD_PARTY:
                boolean allowUpdatedProductPrices = true;
                zeroOutPrepOptionPrices();
                transactionCalculation = new TransactionCalculation(this.getTransactionModel(), allowUpdatedProductPrices);
                restorePrepOptionPrices();
                break;
            case ONLINE_ORDERING:
            case KIOSK_ORDERING:
                transactionCalculation = new TransactionCalculation(this.getTransactionModel());
                break;
            default:
                break;
        }

        return transactionCalculation;
    }

    private void checkQuickchargeBalances() throws Exception {
        //Validate Quickcharge balances
        switch (transactionModel.getPosType()) {
            case MMHAYES_POS:
                // only validate quickcharge balances if the transaction isn't an inquiry
                // OR the inquiry is happening at a pos anywhere terminal
                if (!transactionModel.isInquiry() ||
                        transactionModel.getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.POSANYWHERE.toInt())) {
                    this.validateQuickChargeBalances();
                }
                break;
            case THIRD_PARTY:
                //08/02/2017 egl - Per meeting with Agilysys, check qc balances for inquire
                //10/26/2017 egl - don't check QC balances if "recordedOffline" flag is true
                if (!transactionModel.wasRecordedOffline()) {
                    this.validateQuickChargeBalances();
                }

                break;
            default:

                break;
        }
    }

    public void applyCombos(TransactionCalculation transactionCalculation) throws Exception {
        // calculate and apply combos
        switch (transactionModel.getPosType()) {
            case MMHAYES_POS:
                break;
            case THIRD_PARTY:
                break;
            case ONLINE_ORDERING:
            case KIOSK_ORDERING:
                if (this.getTransactionModel().isInquiry() && transactionCalculation != null) {
                    transactionCalculation.evaluateCombos();
                }
                break;
            default:
                break;
        }
    }

    public void applyDiscounts(TransactionCalculation transactionCalculation) throws Exception {
        // calculate and apply discounts
        switch (transactionModel.getPosType()) {
            case MMHAYES_POS:
                if (this.getTransactionModel().isInquiry() && transactionCalculation != null) {
                    transactionCalculation.setDiscountPriceReductionRecords(); //take into any already applied Discounts on the transaction model
                    transactionCalculation.setRewardPriceReductionRecords(); //take into any already applied Rewards on the transaction model

                    //Only Auto Apply QC Transactions for SALE or OPEN transactions
                    switch (this.getTransactionModel().getTransactionTypeEnum()) {
                        case SALE:
                        case OPEN:
                            if (!this.getTransactionModel().getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.POSANYWHERE.toInt())){
                                //Don't auto apply QC Discounts for PosAnywhere terminals
                                transactionCalculation.initQcDiscountAmountsForVouchers();

                                if (this.getTransactionModel().isAutoApplyQcDiscounts()) {
                                    transactionCalculation.autoApplyQCDiscountsForPOS();
                                }
                            }
                            break;
                    }
                    transactionCalculation.applyAttachedDiscounts();
                }
                break;
            case THIRD_PARTY:
                if (this.getTransactionModel().getTransactionTypeEnum() == PosAPIHelper.TransactionType.SALE
                        && transactionCalculation != null){
                    if (this.getTransactionModel().isInquiry()){
                        transactionCalculation.setDiscountPriceReductionRecords(); //take into any already applied Discounts on the transaction model
                        transactionCalculation.setRewardPriceReductionRecords(); //take into any already applied Rewards on the transaction model
                        transactionCalculation.applyAttachedDiscounts();
                    } else {
                        transactionCalculation.autoApplyQCDiscountsForThirdParty();
                    }
                }
                break;
            case ONLINE_ORDERING:
            case KIOSK_ORDERING:
                if (this.getTransactionModel().isInquiry() && transactionCalculation != null && this.getTransactionModel().getTerminal() != null && this.getTransactionModel().getTerminal().isChargeAtTimeOfPurchase()) {

                    if (this.getTransactionModel().isAutoApplyQcDiscounts()) {
                        transactionCalculation.evaluateQCDiscounts();
                    }
                }
                break;
            default:
                break;
        }
    }

    public void applyTaxes(TransactionCalculation transactionCalculation) throws Exception {
        // calculate and apply taxes
        switch (transactionModel.getPosType()) {
            case THIRD_PARTY:
                //TODO - should the Third Party requests apply Taxes?
                break;
            case MMHAYES_POS:
                if (this.getTransactionModel().isInquiry() && transactionCalculation != null) {
                    this.sortProductLinesByOriginalSequence();
                    transactionCalculation.removeTaxes();
                    //discount and reward price reduction records are created before discounts are calculated.  This enables compounding discounts
                    transactionCalculation.applyTaxes();
                }
                break;
            case ONLINE_ORDERING:
            case KIOSK_ORDERING:
                if (this.getTransactionModel().isInquiry() && transactionCalculation != null) {
                    this.sortProductLinesByOriginalSequence();
                    transactionCalculation.removeTaxes(); //TODO - Test to make sure this works with MyQC
                    transactionCalculation.setRewardPriceReductionRecords();
                    transactionCalculation.applyTaxes();
                }
                break;
            default:
                break;
        }
    }

    public void applySurcharges(TransactionCalculation transactionCalculation) throws Exception {
        // calculate and apply surcharges
        switch (transactionModel.getPosType()) {
            case MMHAYES_POS:
                break;
            case THIRD_PARTY:
                break;
            case ONLINE_ORDERING:
            case KIOSK_ORDERING:
                if (this.getTransactionModel().isInquiry() && transactionCalculation != null) {
                    transactionCalculation.removeSurcharges();
                    transactionCalculation.evaluateSurcharges();
                }
                break;
            default:
                break;
        }
    }

    private void checkTransactionBalances() throws Exception {
        //validate Transaction Balance
        switch (transactionModel.getPosType()) {
            case MMHAYES_POS:
                this.validateQCPosTransactionLineItems();  //check rules specific to QCPos
                break;
            case THIRD_PARTY:
                //only validate the transaction balance for a SALE, and the "recordedOffline" flag is false
                if (!transactionModel.isInquiry() && !transactionModel.wasRecordedOffline()) {
                    switch (this.getTransactionModel().getApiActionTypeEnum()) {
                        case SALE:
                            this.validateTransactionBalance(); //only validate the transaction balance on the SALE, not the inquire
                            break;
                        case ROA:
                            this.validateROATransactionBalance();
                            break;
                    }
                }
                this.validateThirdPartyTransactionLineItems();
                break;
            default:
                //note: MyQC will for now validate its own balances because it has special considerations already in place such as deliveryMinimum - JMD 3/11/18
                break;
        }
        checkForNegativeTransactionBalance();
    }

    private void cleanUpAfterBuild() throws Exception {
        //remove any QcDiscount placeholders
        switch (transactionModel.getPosType()) {
            case MMHAYES_POS:
            case THIRD_PARTY:
                this.removeQcDiscountPlaceHolders();
                break;
            default:
                //don't do this for Online Ordering
                break;
        }

        //sort products back to the original
        this.sortProductLinesByOriginalSequence();
        this.sortRewardLinesByOriginalSequence();
    }

    private void checkVoidRefund() throws Exception {
        //Prepare Full and Partial Refund data
        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case REFUND:
                if (this.isFullRefund()) {
                    this.prepareLoyaltyPointsForRefundVoid(); //this will handle Loyalty Points for Voids and Full Refunds
                    this.prepareCombosForRefundVoid();
                } else {

                    TransactionPartialRefund transactionPartialRefund = new TransactionPartialRefund(this.getTransactionModel());
                    transactionPartialRefund.calculatePointsForPartialRefund();
                    transactionPartialRefund.validateComboLineItemAmounts();
                    this.setTransactionModel(transactionPartialRefund.getTransactionModel());
                }
                break;
            case VOID:
                this.prepareLoyaltyPointsForRefundVoid(); //this will handle Loyalty Points for Voids and Full Refunds
                this.prepareCombosForRefundVoid();
                break;
        }
    }

    private void validatePATransHeader() throws Exception {
        //check if the account(s) associated with the transaction have a Spending Profile mapping
        if(!transactionModel.isCancelTransactionType()) {
            this.checkForInvalidAccountSpendingProfile();
        }

        switch (transactionModel.getPosType()) {
            case MMHAYES_POS:
                this.validateQCPosTransactionHeader();     //check rules specific to QCPos
                break;
            case THIRD_PARTY:
                this.validateThirdPartyTransactionHeader();
                switch (this.getTransactionModel().getApiActionTypeEnum()) {
                    case SALE: //Only create Discount placeholders for Sale transactions
                        this.getTransactionModel().getTotalQcDiscountAmount();
                        this.applyQcDiscountPlaceHolders();
                        this.applyDiscountPlaceHolders();
                        break;
                }
                break;
            default:
                validateTransactionHeaderCommon();
                break;
        }
    }

    //endregion

    //region Validate Quickcharge transaction

    protected void validateQCTransactions() throws Exception {

        // apply discounts and loyalty rewards before figuring out the balance.
        // Bug 564 - tenders which don't allow under amounts will fail if the balance is checked before discounts and rewards are applied.
        if (!this.getTransactionModel().isCancelTransactionType() &&
                !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {

            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:
                case ROA:
                    //sets the QC Discounts available for each tender
                    this.getTransactionModel().getAvailableQCDiscounts();

                    //calculate what PosItems to write to QC_PATransLineItem
                    this.calculateQCLineItems();

                    switch (this.getTransactionModel().getPosType()) {
                        case ONLINE_ORDERING:
                        case KIOSK_ORDERING:
                            //don't validate QCDiscounts
                            break;
                        default:
                            this.validateQCDiscounts();
                            break;
                    }

                    break;
                case VOID:
                case REFUND:
                    this.validateQCTransactionForVoidRefund();
                    this.validateQCDiscountsForVoidRefund();
                default:
                    //do nothing
                    break;
            }
        }
    }

    private void validateQCDiscounts() throws Exception {

        for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()) {
            if (tenderLineItemModel.getAccount() != null) {

                if (!this.getTransactionModel().wasRecordedOffline()) { //HP 1379: Don't validate QC Discounts if the transaction was submitted with recordedOffline = true

                    //if there are none available, but one is submitted, error out
                    if ((tenderLineItemModel.getAccount().getQcDiscountsAvailable() == null || tenderLineItemModel.getAccount().getQcDiscountsAvailable().isEmpty()) &&
                            (tenderLineItemModel.getAccount().getQcDiscountsApplied() != null && !tenderLineItemModel.getAccount().getQcDiscountsApplied().isEmpty())) {
                        throw new MissingDataException("Invalid Quickcharge discount submitted", transactionModel.getTerminal().getId());
                    }

                    for (QcDiscountLineItemModel qcDiscountLineItemModel : tenderLineItemModel.getAccount().getQcDiscountsApplied()) {
                        boolean isValid = false;

                        //check the 3rd party submitted discounts
                        for (QcDiscountModel qcDiscountModel : tenderLineItemModel.getAccount().getQcDiscountsAvailable()) {
                            if (qcDiscountLineItemModel.getQcDiscount().getId().equals(qcDiscountModel.getId())) {

                                BigDecimal submittedQcDiscountAmount = qcDiscountLineItemModel.getExtendedAmount();
                                if (qcDiscountLineItemModel.getExtendedAmount().compareTo(BigDecimal.ZERO) < 0){
                                    submittedQcDiscountAmount = qcDiscountLineItemModel.getExtendedAmount().negate();
                                }

                                if (submittedQcDiscountAmount.compareTo(qcDiscountModel.getAmount()) <= 0){
                                    isValid = true;
                                }
                            }
                        }

                        if (!isValid) {
                            throw new MissingDataException("Invalid Quickcharge discount submitted", transactionModel.getTerminal().getId());
                        }

                        if (qcDiscountLineItemModel.getQcDiscount() != null &&
                                qcDiscountLineItemModel.getQcDiscount().getId() != null &&
                                !qcDiscountLineItemModel.getQcDiscount().getId().toString().isEmpty()) {
                            //populate the QcDiscountID.  HP 1411
                            qcDiscountLineItemModel.setQcDiscountId(qcDiscountLineItemModel.getQcDiscount().getId());
                        }
                    }

                    //check QCPOS discounts
                    for (DiscountLineItemModel discountLineItemModel : transactionModel.getDiscounts()) {
                        if (discountLineItemModel.getQcDiscount() != null && discountLineItemModel.getEmployeeId() != null) {
                            //check on employee Id
                            if (tenderLineItemModel.getAccount().getId().equals(discountLineItemModel.getEmployeeId())) {

                                boolean isValid = false;
                                //check the 3rd party submitted discounts
                                for (QcDiscountModel qcDiscountModel : tenderLineItemModel.getAccount().getQcDiscountsAvailable()) {
                                    if (discountLineItemModel.getQcDiscount().getId().equals(qcDiscountModel.getId())) {
                                        isValid = true;
                                    }
                                }

                                if (!isValid) {
                                    throw new MissingDataException("Invalid Quickcharge discount submitted", transactionModel.getTerminal().getId());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * For Voids and Refunds, if the API is offline, or there is Item Profile Mapping
     * remove the QC Transaction off of the tender line item and received on account line item.
     * We want the Background Processor to write these transactions.
     *
     * @throws Exception
     */
    private void validateQCTransactionForVoidRefund() throws Exception {

        if (!this.getTransactionModel().shouldAPICreateQCTransactions()) {
            //remove any QC Transactions off the tender lines or Roa lines

            for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
                if (tenderLineItemModel.getQcTransaction() != null) {
                    tenderLineItemModel.setQcTransaction(null);
                }
            }

            for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : this.getTransactionModel().getReceivedOnAccounts()) {
                if (receivedOnAccountLineItemModel.getQcTransaction() != null) {
                    receivedOnAccountLineItemModel.setQcTransaction(null);
                }
            }
        }
    }

    /**
     * For Voids and Refunds, remove the QC Discount off of the Tender Line
     * remove the QC Discount off of the tender line item.
     * HP Bug 1282: QC Discounts should not be refunded or voided
     *
     * @throws Exception
     */
    private void validateQCDiscountsForVoidRefund() throws Exception {

        if (!this.getTransactionModel().shouldAPICreateQCTransactions()) {
            //remove any QC Transactions off the tender lines or Roa lines

            for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {

                if (tenderLineItemModel.getAccount() != null) {
                    if (tenderLineItemModel.getAccount().getQcDiscountsApplied() != null
                            && !tenderLineItemModel.getAccount().getQcDiscountsApplied().isEmpty()) {

                        tenderLineItemModel.getAccount().getQcDiscountsApplied().clear();
                    }
                }
            }
        }
    }

    //endregion

    //region Validate Trans Line Items
    //build new list of simple TransactionLineItems
    protected void validateTransactionLineItems() throws Exception {
        this.setTransactionModel(TransactionLineBuilder.validateTransactionLines(this.getTransactionModel()));
    }

    public void validateQCPosTransactionLineItems() throws Exception {
        TransactionLineBuilder transactionLineBuilder = new TransactionLineBuilder(getTransactionModel());
        transactionLineBuilder.validateQCPosProductLineItems();
    }

    public void validateThirdPartyTransactionLineItems() throws Exception {
        // nothing yet
    }
    //endregion

    //region Validate Loyalty

    public void applyLoyaltyInfo() throws Exception {
        //Loyalty
        //Only Loyalty if we are not in Cancel or Training Mode, and this is not a POS Anywhere Terminal
        if (this.getTransactionModel().getLoyaltyAccount() != null && !this.getTransactionModel().isOfflineMode() &&
                !this.getTransactionModel().getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.POSANYWHERE.toInt())) {
            if (!this.getTransactionModel().isCancelTransactionType() &&
                    !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {
                switch (this.getTransactionModel().getApiActionTypeEnum()) {
                    case SALE:

                        this.getTransactionModel().fetchRewardMappingsForProducts();
                        this.checkForLoyaltyRewardsForTransaction();
                        if (!this.getTransactionModel().isTrainingTransactionType()) {
                            this.getTransactionModel().removeLoyaltyPoints();  //if any loyalty points are sent in on the transaction, remove them so no duplicates are saved
                            if (!this.getTransactionModel().wasRecordedOffline()) { //only validate submitted rewards if "recordedOffline" = false
                                this.validateSubmittedRewards();
                                this.validateNumberOfRewards();
                                this.validateMultipleRewardsVsAvailablePoints();
                            }

                            this.validateRewardLineItemPoint();   //created Loyalty Account Point for reward line items

                            this.applyLoyaltyRewardsToProductsAutoRedeem();
                            this.applyLoyaltyRewardsToProducts();

                            //Removed - HP 668 & 963
                            //this.validateMaximumDiscountTransactionTotal(); //validate the rewards applied don't exceed the Maximum Discount setting
                            this.updateRewardListsForTransaction();
                            this.matchUpPointsAndRewardLineItems(); //In 1.7.1, put the availableRewards and availableRewardsForTransaction lists on the Loyalty Point Model

                            if (!this.getTransactionModel().isInquiry()) { //only return back points on a SALE
                                this.createProductLoyaltyPoints();  //do not build Points for the current transaction on an inquiry.  03/08/2017 egl
                            }
                        }

                        break;
                    case VOID:
                    case REFUND:
                        if (!this.getTransactionModel().isTrainingTransactionType()) {
                            this.validateRewardLineItemPoint();  //created Loyalty Account Point for reward line items
                        }
                        break;
                    case LOYALTY_ADJUSTMENT:

                        buildLoyaltyPointsForDonations();

                        break;
                    default:
                        //do nothing
                        break;
                }

                /**
                 * LoyaltyPointsRedeemed is setup here.  It's a summary of the rewards that were redeemed.
                 */
                switch (this.getTransactionModel().getApiActionTypeEnum()) {
                    case SALE:
                    case VOID:
                    case REFUND:
                        this.getTransactionModel().prepareLoyaltyPointsEarned();
                        this.getTransactionModel().prepareLoyaltyPointsRedeemed(false);
                }
            }
        }
    }

    //region Donations - PATransaction Level

    //if donation type is 1 - Loyalty
    //set Loyalty Point Model on donationLineItem
    private void buildLoyaltyPointsForDonations() throws Exception {

        if(this.getTransactionModel().getDonations().size() > 0) {

            validateDonationLineItems();

            for(DonationLineItemModel donationLineItemModel : this.getTransactionModel().getDonations()) {

                //check if donation typeID is 1 - Loyalty
                if(donationLineItemModel.getDonation() != null && donationLineItemModel.getDonation().getDonationTypeId() != null && donationLineItemModel.getDonation().getDonationTypeId().equals(1)) {
                    donationLineItemModel.createLoyaltyPoint(this.getTransactionModel());
                }
            }
        }
    }

    //go through each of the donations
    //validate the donation model on the donation line item
    public void validateDonationLineItems() throws Exception {
        TransactionLineBuilder transactionLineBuilder = new TransactionLineBuilder(getTransactionModel());
        transactionLineBuilder.validateDonationLineItems();
    }

    //region Rewards - PATransaction Level

    //go through each of the earned points
    //if there is a Loyalty Reward Trans Line Id, set isReward = true
    //this is used for voids and refunds
    private void validateLoyaltyAccountPoints() throws Exception {
        for (LoyaltyPointModel loyaltyPointModel : this.getTransactionModel().getLoyaltyPointsAll()) {
            if (loyaltyPointModel.getLoyaltyRewardTransLineId() != null && !loyaltyPointModel.getLoyaltyRewardTransLineId().toString().isEmpty()) {
                loyaltyPointModel.setIsReward(true);
            }
        }
    }

    //get valid Loyalty Rewards for the Points on this account
    //this also create Loyalty Account Points for Reward
    public void checkForLoyaltyRewardsForTransaction() throws Exception {

        List<LoyaltyRewardModel> loyaltyRewardsForRevenueCenter = transactionModel.getLoyaltyAccount().getRewardsAvailable();

        if (!transactionModel.getLoyaltyAccount().hasFetchedAvailableRewards()) {
            loyaltyRewardsForRevenueCenter = LoyaltyRewardCollection.getValidLoyaltyRewardsForAccount(transactionModel).getCollection();
        }

        LoyaltyRewardLineItemCollection validRewardsLineItems = new LoyaltyRewardLineItemCollection();
        this.getTransactionModel().getLoyaltyAccount().setRewardsAvailableForTransaction(new ArrayList<LoyaltyRewardLineItemModel>());


        //for each valid Loyalty Program Reward
        for (LoyaltyRewardModel loyaltyRewardModel : loyaltyRewardsForRevenueCenter) {
            //Cross reference the Valid rewards against the
            //available points
            for (LoyaltyPointModel loyaltyPointModel : transactionModel.getLoyaltyAccount().getLoyaltyPoints()) {
                if (loyaltyPointModel.getPoints() > 0) {
                    if (loyaltyRewardModel.getLoyaltyProgram().getId().equals(loyaltyPointModel.getLoyaltyProgram().getId())) {  //do the Loyalty Programs match
                        LoyaltyRewardCalculation loyaltyRewardCalculation = new LoyaltyRewardCalculation(transactionModel, loyaltyRewardModel, loyaltyPointModel);
                        if (loyaltyRewardCalculation.isRewardProgramValid()) {
                            loyaltyRewardCalculation.calculateRewards();
                            validRewardsLineItems.getCollection().add(loyaltyRewardCalculation.getAvailableRewardLineItem());
                        }
                    }
                }

                loyaltyPointModel.setRewardsAvailableForTransaction(new ArrayList<LoyaltyRewardLineItemModel>());
            }
        }

        //add available Rewards to the Loyalty Account
        this.getTransactionModel().getLoyaltyAccount().setRewardsAvailableForTransaction(validRewardsLineItems.getCollection());

    }

    /**
     * Move the available Reward line items to the Loyalty Point Model
     */
    private void matchUpPointsAndRewardLineItems() {

        if (this.getTransactionModel().getLoyaltyAccount() != null) {
            for (LoyaltyPointModel loyaltyPointModel : this.getTransactionModel().getLoyaltyAccount().getLoyaltyPoints()) {

                for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getLoyaltyAccount().getRewardsAvailableForTransaction()) {
                    if (loyaltyPointModel.getLoyaltyProgram().getId().equals(loyaltyRewardLineItemModel.getReward().getLoyaltyProgram().getId())) {

                        loyaltyPointModel.getRewardsAvailableForTransaction().add(loyaltyRewardLineItemModel);

                    }
                }
            }
        }
    }

    public void validateNumberOfRewards() throws Exception {
        if (transactionModel.getRewards() != null && !transactionModel.getRewards().isEmpty()) {
            this.validateRewardDupeTransactionCreditRewards();
            this.validateRewardMaxPerTransaction();
            this.validateRewardCanUseWithOtherRewards();
            this.validateSubmittedRewardCount();
        }
    }

    /*
     * If there are multiple rewards submitted, make sure the total number of Program points
     * covers both of the rewards
     * */
    private void validateMultipleRewardsVsAvailablePoints() throws Exception {

        //tally up all the points To Redeem on the submitted Rewards
        HashMap<Integer, Integer> rewardPointsToRedeemHM = new HashMap<>();

        //only do this check if there is more than 1 reward submitted
        if (transactionModel.getRewards() != null && !transactionModel.getRewards().isEmpty() && transactionModel.getRewards().size() > 1) {
            for (LoyaltyRewardLineItemModel rewardLineItemModel : transactionModel.getRewards()) {
                if (rewardPointsToRedeemHM.containsKey(rewardLineItemModel.getReward().getLoyaltyProgram().getId())) {
                    Integer currentPointValue = rewardPointsToRedeemHM.get(rewardLineItemModel.getReward().getLoyaltyProgram().getId());
                    rewardPointsToRedeemHM.put(rewardLineItemModel.getReward().getLoyaltyProgram().getId(), currentPointValue + rewardLineItemModel.getReward().getPointsToRedeem());
                } else {
                    //this reward doesn't exist yet, add it in
                    rewardPointsToRedeemHM.put(rewardLineItemModel.getReward().getLoyaltyProgram().getId(), rewardLineItemModel.getReward().getPointsToRedeem());
                }
            }

            //now loop through the Loyalty Points for the Loyalty Account and make sure there are enough points for the 2 rewards
            for (LoyaltyPointModel loyaltyPointModel : transactionModel.getLoyaltyAccount().getLoyaltyPoints()) {
                Integer rewardPointsUsed = rewardPointsToRedeemHM.get(loyaltyPointModel.getLoyaltyProgram().getId());

                if (rewardPointsUsed != null) {
                    if (rewardPointsUsed > loyaltyPointModel.getPoints()) {
                        throw new MissingDataException("The Loyalty Rewards applied are not fully covered by the Number of Points for the associated Loyalty Program.");
                    }
                }
            }
        }
    }

    //validate that no Transaction Credits are sent in multiple times
    private void validateRewardDupeTransactionCreditRewards() throws Exception {
        // Validate that only one of the same Transaction Credits reward was used
        HashMap<Integer, Integer> loyaltyRewardsHM = new HashMap<>();
        for (LoyaltyRewardLineItemModel loyaltyRewardRedeemed : transactionModel.getRewards()) {
            if (loyaltyRewardRedeemed.getReward().getRewardTypeId().equals(PosAPIHelper.LoyaltyRewardType.TRANSACTION_CREDIT.toInt())) {
                Integer rewardCount = loyaltyRewardsHM.containsKey(loyaltyRewardRedeemed.getReward().getId()) ? loyaltyRewardsHM.get(loyaltyRewardRedeemed.getReward().getId()) : 0;
                loyaltyRewardsHM.put(loyaltyRewardRedeemed.getReward().getId(), rewardCount + 1);
            }
        }
        //loop through the hashmap to see if there are any duplicates
        for (Map.Entry<Integer, Integer> rewardEntry : loyaltyRewardsHM.entrySet()) {
            if (rewardEntry.getValue() != null && rewardEntry.getValue() > 1) {
                throw new MissingDataException("The same Transaction Credit reward cannot be applied multiple times to a transaction", transactionModel.getTerminal().getId());
            }
        }
    }

    private void validateRewardMaxPerTransaction() throws Exception {
        //validate MaxPerTransaction
        HashMap<Integer, Integer> duplicateList = new HashMap<>();
        for (LoyaltyRewardLineItemModel loyaltyRewardRedeemed : transactionModel.getRewards()) {

            if (!duplicateList.containsKey(loyaltyRewardRedeemed.getReward().getId())) {
                duplicateList.put(loyaltyRewardRedeemed.getReward().getId(), 1);
            } else {
                duplicateList.put(loyaltyRewardRedeemed.getReward().getId(), duplicateList.get(loyaltyRewardRedeemed.getReward().getId()) + 1);
            }

            int maxAllowedForThisReward = loyaltyRewardRedeemed.getReward().getMaxPerTransaction();

            if (duplicateList.get(loyaltyRewardRedeemed.getReward().getId()) > maxAllowedForThisReward) {
                throw new MissingDataException("Reward: " + loyaltyRewardRedeemed.getReward().getName() + " cannot be used more than " + maxAllowedForThisReward + " time(s) on a transaction", transactionModel.getTerminal().getId());
            }
        }

    }

    //First check Free Product rewards, then check Transaction Credit rewards if they can be used with other rewards
    private void validateRewardCanUseWithOtherRewards() throws Exception {
        //check the "Can Use with other Rewards". First check Free Product Rewards
        for (LoyaltyRewardLineItemModel loyaltyRewardRedeemed : transactionModel.getRewards()) {

            if (loyaltyRewardRedeemed.getReward().getRewardTypeId().equals(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS.toInt())) {
                if (!loyaltyRewardRedeemed.getReward().canUseWithOtherRewards() && transactionModel.getRewards().size() > 1) {
                    throw new MissingDataException("Reward: " + loyaltyRewardRedeemed.getReward().getName() + " cannot be used with other rewards", transactionModel.getTerminal().getId());
                }
            }
        }

        //check the "Can Use with other Rewards".  Check Transaction Credit Rewards
        for (LoyaltyRewardLineItemModel loyaltyRewardRedeemed : transactionModel.getRewards()) {
            if (loyaltyRewardRedeemed.getReward().getRewardTypeId().equals(PosAPIHelper.LoyaltyRewardType.TRANSACTION_CREDIT.toInt())) {
                if (!loyaltyRewardRedeemed.getReward().canUseWithOtherRewards() && transactionModel.getRewards().size() > 1) {
                    throw new MissingDataException("Reward: " + loyaltyRewardRedeemed.getReward().getName() + " cannot be used with other rewards", transactionModel.getTerminal().getId());
                }
            }
        }
    }

    //validate the the number of submitted rewards is less than or equal to the quanities of the products
    private void validateSubmittedRewardCount() throws Exception {

        //get the product line item quantity count

        BigDecimal totalProductsQuantity = BigDecimal.ZERO;
        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {

            if (productLineItemModel.getQuantity() != null && !productLineItemModel.getQuantity().toString().isEmpty()) {
                totalProductsQuantity = totalProductsQuantity.add(productLineItemModel.getQuantityForLoyalty());
            }
        }

        BigDecimal freeProductRewardCount = BigDecimal.ZERO;
        BigDecimal transactionCreditCount = BigDecimal.ZERO;
        for (LoyaltyRewardLineItemModel loyaltyRewardRedeemed : transactionModel.getRewards()) {
            if (loyaltyRewardRedeemed.getReward().getRewardTypeId().equals(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS.toInt())) {
                freeProductRewardCount = freeProductRewardCount.add(BigDecimal.ONE);
            }

            if (loyaltyRewardRedeemed.getReward().getRewardTypeId().equals(PosAPIHelper.LoyaltyRewardType.TRANSACTION_CREDIT.toInt())) {
                transactionCreditCount = transactionCreditCount.add(BigDecimal.ONE);
            }
        }

        //too many Free Products were submitted for the transaction
        if (freeProductRewardCount.compareTo(totalProductsQuantity) == 1) {
            throw new TooManyFreeProductRewardsException("Too Many Free Product Rewards were submitted for this transaction");
        }
    }

    /**
     * Link together the LoyaltyLineItemModels, LoyaltyPointModels, and ItemLoyaltyPointModels for the SALE or REFUND/VOID
     * Link them together with a GUID
     * Therefore, when the points are saving, we can still associate them with one another even after they save with new ID's
     *
     * @throws Exception
     */
    public void validateRewardLineItemPoint() throws Exception {

        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {

            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case VOID:
                case REFUND:
                    //map any Loyalty Points earned for the reward line item
                    //this is used for voids and refunds
                    for (LoyaltyPointModel loyaltyPointModel : transactionModel.getLoyaltyPointsAll()) {

                        if (loyaltyRewardLineItemModel.getId() != null && !loyaltyRewardLineItemModel.getId().toString().isEmpty()) {
                            if (loyaltyPointModel.getLoyaltyRewardTransLineId() != null && !loyaltyPointModel.getLoyaltyRewardTransLineId().toString().isEmpty()) {
                                if (loyaltyPointModel.getLoyaltyRewardTransLineId().equals(loyaltyRewardLineItemModel.getId())) {
                                    loyaltyRewardLineItemModel.setLoyaltyPoint(loyaltyPointModel);
                                }
                            }
                        }
                    }

                    //For refunds and voids, link up the Loyalty Reward Line Item, Loyalty Account Point, and Item Loyalty Points using a GUID
                    UUID rewardGuid = UUID.randomUUID();
                    loyaltyRewardLineItemModel.setRewardGuid(rewardGuid);

                    for (LoyaltyPointModel loyaltyPointModel : transactionModel.getLoyaltyPointsAll()) {
                        if (loyaltyRewardLineItemModel.getId().equals(loyaltyPointModel.getLoyaltyRewardTransLineId())) {
                            loyaltyPointModel.setRewardGuid(rewardGuid);
                            loyaltyPointModel.setIsReward(true);

                            for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
                                for (ItemLoyaltyPointModel itemLoyaltyPointModel : productLineItemModel.getLoyaltyPointDetails()) {
                                    if (itemLoyaltyPointModel.getLoyaltyAccountPointId().equals(loyaltyPointModel.getId())) {
                                        itemLoyaltyPointModel.setRewardGuid(rewardGuid);
                                    }
                                }
                            }
                        }
                    }

                    break;
                case SALE:

                    if (loyaltyRewardLineItemModel.getLoyaltyPoint() == null) {
                        //if the Loyalty Point Model is blank, repopulate it
                        LoyaltyPointModel loyaltyPointModel = new LoyaltyPointModel(transactionModel.getLoyaltyAccount(), loyaltyRewardLineItemModel.getReward().getLoyaltyProgram(), "");
                        loyaltyPointModel.setPoints(loyaltyRewardLineItemModel.getReward().getPointsToRedeem());
                        loyaltyPointModel.setIsReward(true);
                        loyaltyPointModel.setEmployeeId(transactionModel.getLoyaltyAccount().getId());
                        loyaltyRewardLineItemModel.setLoyaltyPoint(loyaltyPointModel);
                    }
                    break;
            }
        }
    }

    //this chooses what reward will be select for auto payout
    public void applyLoyaltyRewardsToProductsAutoRedeem() throws Exception {
        if (this.getTransactionModel().getRewards() == null || this.getTransactionModel().getRewards().isEmpty()) {
            if (this.getTransactionModel().getLoyaltyAccount() != null) {

                /*only apply Auto Payout rewards if the order type is "Normal Sale"
                Auto Payouts are not allowed for Online Ordering*/
                if (PosAPIHelper.OrderType.convertStringToInt(this.getTransactionModel().getOrderType()).equals(PosAPIHelper.OrderType.NORMALSALE.toInt())) {
                    if (this.getTransactionModel().getTransactionTypeEnum() == PosAPIHelper.TransactionType.SALE) {
                        //only apply Auto Payout rewards on the inquire
                        if (this.getTransactionModel().isInquiry()) {

                            //sort the Free Product Rewards by pointsToRedeem
                            Collections.sort(this.getTransactionModel().getLoyaltyAccount().getRewardsAvailableForTransaction(), new LoyaltyRewardLineItemDescComparer("pointsToRedeem"));

                            //first try and apply free product rewards
                            for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getLoyaltyAccount().getRewardsAvailableForTransaction()) {
                                //if Reward is Auto Payout, add it to the list of rewards
                                if (loyaltyRewardLineItemModel.getReward().isAutoPayout() && loyaltyRewardLineItemModel.getReward().getRewardTypeId().equals(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS.toInt())) {
                                    this.getTransactionModel().getRewards().add(loyaltyRewardLineItemModel);
                                    return; //only add one auto payout reward
                                }
                            }

                            //then try to apply transaction credits
                            for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getLoyaltyAccount().getRewardsAvailableForTransaction()) {
                                //if Reward is Auto Payout, add it to the list of rewards
                                if (loyaltyRewardLineItemModel.getReward().isAutoPayout() && loyaltyRewardLineItemModel.getReward().getRewardTypeId().equals(PosAPIHelper.LoyaltyRewardType.TRANSACTION_CREDIT.toInt())) {
                                    this.getTransactionModel().getRewards().add(loyaltyRewardLineItemModel);
                                    return; //only add one auto payout reward
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * update rewardsAvailable and rewardsAvailableForTransaction lists on the Loyalty Account
     *
     * @throws Exception
     */
    public void updateRewardListsForTransaction() throws Exception {
        this.removeAvailableRewardsZeroAmount(); //Remove rewards with a Zero amount
        this.removeAvailableRewardsNoProductsRemaining(); //Remove rewards if No Products are remaining
        this.removeAvailableRewardsMaxPerTransaction(); //Remove rewards if "MaxPerTransaction" has been met for the reward
        //this.removeAvailableRewardsNotAvailableForAllProducts(); //remove rewards if reward was available for product already with a submitted reward
        if (this.getTransactionModel().getRewards() != null && !this.getTransactionModel().getRewards().isEmpty()) {
            this.removeAvailableRewardsCanUseWithOtherRewards(); //remove rewards if submitted reward does not have "CanUseWithOtherRewards" checked
            this.removeAvailableRewardsAutoPayout(); //remove rewards if submitted reward includes reward marked as "AutoPayout"
            this.removeAvailableRewardsMinimumCheck(); //Do a final check for the Minimum Product Amount.
        }
    }

    /**
     * Remove rewards with a Zero amount
     */
    private void removeAvailableRewardsZeroAmount() {
        //remove any rewards with a zero value
        //this way they won't get written to the PA Trans Line Item table
        Iterator<LoyaltyRewardLineItemModel> removeSubmittedZeroIterator = this.getTransactionModel().getRewards().iterator();
        while (removeSubmittedZeroIterator.hasNext()) {
            LoyaltyRewardLineItemModel curRewardLineItem = removeSubmittedZeroIterator.next();
            if (curRewardLineItem.getExtendedAmount().compareTo(BigDecimal.ZERO) == 0) {
                removeSubmittedZeroIterator.remove();
            }
        }
    }

    /**
     * Remove rewards if No Products are remaining
     */
    private void removeAvailableRewardsNoProductsRemaining() {
        Integer freeProductRewardCount = 0;
        Integer productCount = 0;

        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
            if (productLineItemModel.getRewards() != null && !productLineItemModel.getRewards().isEmpty()) {
                for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {
                    if (itemRewardModel.getReward().getTypeName().equalsIgnoreCase(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS.toStringName())) {
                        freeProductRewardCount++;
                    }
                }
            }

            //add up the product count to do the total free product for products check
            productCount = productCount + productLineItemModel.getQuantityForLoyalty().intValue();
        }

        //check the total Free Product Rewards vs. Product count.  If all products have been applied with Free Product Rewards, clear available Rewards
        if (freeProductRewardCount.compareTo(productCount) == 0) {
            this.getTransactionModel().getLoyaltyAccount().getRewardsAvailableForTransaction().clear();

            //update the rewards in the rewardsAvailable list
            for (LoyaltyRewardModel loyaltyRewardModel : this.getTransactionModel().getLoyaltyAccount().getRewardsAvailable()) {
                loyaltyRewardModel.setNotes("Reward is invalid: No product available.");
            }
        }
    }

    /**
     * Remove rewards if "MaxPerTransaction" has been met for the submitted reward(s)
     */
    private void removeAvailableRewardsMaxPerTransaction() {

        //Check if the submitted rewards has "Max uses per transaction" set, check the submitted rewards and remove it from the availableRewardsForTransaction
        if (transactionModel.getLoyaltyAccount().getRewardsAvailableForTransaction() != null && !transactionModel.getLoyaltyAccount().getRewardsAvailableForTransaction().isEmpty()) {

            HashMap<Integer, Integer> duplicateList = new HashMap<>();
            for (LoyaltyRewardLineItemModel loyaltyRewardRedeemed : transactionModel.getRewards()) {

                if (!duplicateList.containsKey(loyaltyRewardRedeemed.getReward().getId())) {
                    duplicateList.put(loyaltyRewardRedeemed.getReward().getId(), 1);
                } else {
                    duplicateList.put(loyaltyRewardRedeemed.getReward().getId(), duplicateList.get(loyaltyRewardRedeemed.getReward().getId()) + 1);
                }

                int maxAllowedForThisReward = loyaltyRewardRedeemed.getReward().getMaxPerTransaction();

                if (duplicateList.get(loyaltyRewardRedeemed.getReward().getId()) >= maxAllowedForThisReward) {
                    //if the reward is used the Max Number of Time, remove it from the loyaltyRewardsAvailableForTransaction
                    this.getTransactionModel().getLoyaltyAccount().getRewardsAvailableForTransaction().removeIf(r -> r.getReward().getId().equals(loyaltyRewardRedeemed.getReward().getId()));
                }
            }
        }
    }

    /**
     * If there are any products with a free product reward
     * remove the rewards that would be available to that product
     * HP Bug 853
     *
     * @throws Exception
     */
    private void removeAvailableRewardsNotAvailableForAllProducts() throws Exception {
        //check to see if all products are covered by free product rewards
        HashSet<LoyaltyRewardModel> rewardsToKeep = new HashSet<>();
        HashSet<LoyaltyRewardModel> rewardsToRemove = new HashSet<>();

        for (ProductLineItemModel productLineItemModel : transactionModel.getProducts()) {
            int freeProductRewardCountPerProduct = 0;

            if (productLineItemModel.getRewards() != null && !productLineItemModel.getRewards().isEmpty()) {
                for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {
                    if (itemRewardModel.getReward().getTypeName().equalsIgnoreCase(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS.toStringName())) {
                        freeProductRewardCountPerProduct++;
                    }
                }
            }

            //this product line is covered by free products
            if (productLineItemModel.getQuantityForLoyalty().compareTo(new BigDecimal(freeProductRewardCountPerProduct)) == 0) {
                rewardsToRemove.addAll(productLineItemModel.getRewardsAvailableForProduct());
            } else {
                rewardsToKeep.addAll(productLineItemModel.getRewardsAvailableForProduct());
            }
        }

        if (!rewardsToRemove.isEmpty()) {
            /*remove any rewards from the rewardsToRemove list
             * if they are available for a different product line on the transaction
             * */
            for (LoyaltyRewardModel loyaltyRewardToKeep : rewardsToKeep) {
                Iterator<LoyaltyRewardModel> removeFromRewardIterator = rewardsToRemove.iterator();
                while (removeFromRewardIterator.hasNext()) {
                    LoyaltyRewardModel curReward = removeFromRewardIterator.next();
                    if (curReward.getId().equals(loyaltyRewardToKeep.getId())) {
                        removeFromRewardIterator.remove();
                    }
                }
            }

        /*remove any rewards from getRewardsAvailableForTransaction
        that were available for a product that has a free product applied to it*/
            for (LoyaltyRewardModel loyaltyRewardToRemove : rewardsToRemove) {
                //remove the submitted reward from the Loyalty Account - available rewards for transaction list
                Iterator<LoyaltyRewardLineItemModel> removeSubmittedIterator = transactionModel.getLoyaltyAccount().getRewardsAvailableForTransaction().iterator();
                while (removeSubmittedIterator.hasNext()) {
                    LoyaltyRewardLineItemModel curRewardLine = removeSubmittedIterator.next();
                    if (loyaltyRewardToRemove.getId().equals(curRewardLine.getReward().getId())) {
                        removeSubmittedIterator.remove();
                    }
                }
            }
        }
    }

    /**
     * Remove rewards if submitted reward does not have "CanUseWithOtherRewards" checked
     */
    private void removeAvailableRewardsCanUseWithOtherRewards() {
        for (LoyaltyRewardLineItemModel submittedRewardLineItem : this.getTransactionModel().getRewards()) {

            //if any reward can't be used with other rewards, remove all from rewardsAvailableForTransaction
            if (!submittedRewardLineItem.getReward().canUseWithOtherRewards()) {
                this.getTransactionModel().getLoyaltyAccount().getRewardsAvailableForTransaction().clear();

                for (LoyaltyRewardModel loyaltyRewardModel : this.getTransactionModel().getLoyaltyAccount().getRewardsAvailable()) {

                    if (!submittedRewardLineItem.getReward().getId().equals(loyaltyRewardModel.getId())){
                        loyaltyRewardModel.setNotes("Reward is invalid: The redeemed reward on the transaction can't be used with other rewards");
                    }

                }
            }
        }
    }

    /**
     * Remove rewards if submitted reward includes reward marked as "AutoPayout"
     */
    private void removeAvailableRewardsAutoPayout() {
        for (LoyaltyRewardLineItemModel submittedRewardLineItem : this.getTransactionModel().getRewards()) {

            //if reward is Auto Payout, remove all Auto Payout rewards from the other lists
            if (submittedRewardLineItem.getReward().isAutoPayout()) {
                //this.getTransactionModel().getLoyaltyAccount().getRewardsAvailable().removeIf(LoyaltyRewardModel::isAutoPayout);

                //remove the submitted reward from the Loyalty Account - available rewards for transaction list
                this.getTransactionModel().getLoyaltyAccount().getRewardsAvailableForTransaction().removeIf(r -> r.getReward().isAutoPayout());
            }
        }
    }

    /**
     * Remove rewards if submitted reward is over Minimum Product Amount, but remaining products are not above the Minimum Product Amount
     * This check is done after the Rewards are Applied
     * This method does a similar checks to LoyaltyRewardCalculation.areAnyProductsValidForReward nad LoyaltyRewardCalculation.checkMinTransactionAmount
     * It is done here to deal with Rewards that have been submitted
     * HP Bug 1261
     */
    private void removeAvailableRewardsMinimumCheck() {

        ArrayList<Integer> rewardsToRemove = new ArrayList<>();
        HashMap<Integer, String> rewardsToRemoveHM = new HashMap<>();

        for (LoyaltyRewardLineItemModel rewardAvailForTrans : this.getTransactionModel().getLoyaltyAccount().getRewardsAvailableForTransaction()) {
            Boolean rewardIsStillAvailable = false;
            String rewardMessage = "Reward is invalid: No Products are eligible.";
            PosAPIHelper.LoyaltyRewardType rewardTypeEnum;

            rewardTypeEnum = PosAPIHelper.LoyaltyRewardType.convertIntToEnum(rewardAvailForTrans.getReward().getRewardTypeId());

            switch (rewardTypeEnum) {
                case FREE_PRODUCTS:

                    for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                        if (rewardAvailForTrans.getReward().isApplyToAllPlus()) {
                            BigDecimal calculatedProductAmountExtended = ProductLineItemModel.getAdjValOfExtendedAmountForTransaction(productLineItemModel);
                            BigDecimal adjProductAmount = BigDecimal.ZERO;

                            Integer freeProductCount = 0;
                            if (productLineItemModel.getRewards().size() > 0) {
                                for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {
                                    if (PosAPIHelper.LoyaltyRewardType.convertIntToEnum(itemRewardModel.getReward().getRewardTypeId()).equals(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS)) {
                                        freeProductCount++;
                                    }
                                }
                            }

                            BigDecimal quantityForCalculation = productLineItemModel.getQuantityForLoyalty().subtract(new BigDecimal(freeProductCount));
                            //Divide by the Quantity for Loyalty.  The Free Product Reward will only apply to one quantity
                            if (quantityForCalculation.compareTo(BigDecimal.ZERO) == 1) {
                                adjProductAmount = calculatedProductAmountExtended.divide(quantityForCalculation, 4, RoundingMode.HALF_UP);
                            }

                            if (adjProductAmount.compareTo(BigDecimal.ZERO) == 1
                                    && (rewardAvailForTrans.getReward().getMinPurchase() == null
                                    || adjProductAmount.compareTo(rewardAvailForTrans.getReward().getMinPurchase()) >= 0)) {
                                if (!rewardIsStillAvailable) { //Doing this check will ensure that the reward will be updated with the highest product amount
                                    rewardIsStillAvailable = true;
                                    rewardAvailForTrans.getReward().setValue(adjProductAmount);
                                    rewardAvailForTrans.setEligibleAmount(adjProductAmount);
                                    rewardAvailForTrans.setAmount(adjProductAmount);
                                    rewardAvailForTrans.setExtendedAmount(adjProductAmount);
                                }
                            } else if (rewardAvailForTrans.getReward().getMinPurchase() != null
                                    && adjProductAmount.compareTo(rewardAvailForTrans.getReward().getMinPurchase()) == -1) {

                                //Reward is lower than the Reward Minimum Purchase
                                rewardMessage = "Reward is invalid: the Reward Minimum Product Amount is lower than the total eligible product amount.";
                            }
                        } else {
                            for (LoyaltyRewardModel loyaltyRewardOnProduct : productLineItemModel.getProduct().getRewards()) {
                                if (rewardAvailForTrans.getReward().getId() != null && loyaltyRewardOnProduct.getId() != null) {
                                    if (rewardAvailForTrans.getReward().getId().equals(loyaltyRewardOnProduct.getId())) {
                                        //if product amount is < the Minimum Purchase amount, the reward should not be available
                                        BigDecimal calculatedProductAmountExtended = ProductLineItemModel.getAdjValOfExtendedAmountForTransaction(productLineItemModel);
                                        BigDecimal adjProductAmount = BigDecimal.ZERO;

                                        Integer freeProductCount = 0;
                                        if (productLineItemModel.getRewards().size() > 0) {
                                            for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {
                                                if (PosAPIHelper.LoyaltyRewardType.convertIntToEnum(itemRewardModel.getReward().getRewardTypeId()).equals(PosAPIHelper.LoyaltyRewardType.FREE_PRODUCTS)) {
                                                    freeProductCount++;
                                                }
                                            }
                                        }

                                        BigDecimal quantityForCalculation = productLineItemModel.getQuantityForLoyalty().subtract(new BigDecimal(freeProductCount));
                                        //Divide by the Quantity for Loyalty.  The Free Product Reward will only apply to one quantity
                                        if (quantityForCalculation.compareTo(BigDecimal.ZERO) == 1) {
                                            adjProductAmount = calculatedProductAmountExtended.divide(quantityForCalculation, 4, RoundingMode.HALF_UP);
                                        }

                                        if (adjProductAmount.compareTo(BigDecimal.ZERO) == 1
                                                && (rewardAvailForTrans.getReward().getMinPurchase() == null
                                                || adjProductAmount.compareTo(rewardAvailForTrans.getReward().getMinPurchase()) >= 0)) {
                                            if (!rewardIsStillAvailable) { //Doing this check will ensure that the reward will be updated with the highest product amount
                                                rewardIsStillAvailable = true;
                                                rewardAvailForTrans.getReward().setValue(adjProductAmount);
                                                rewardAvailForTrans.setEligibleAmount(adjProductAmount);
                                                rewardAvailForTrans.setAmount(adjProductAmount);
                                                rewardAvailForTrans.setExtendedAmount(adjProductAmount);
                                            }
                                        } else if (rewardAvailForTrans.getReward().getMinPurchase() != null
                                                && adjProductAmount.compareTo(rewardAvailForTrans.getReward().getMinPurchase()) == -1) {

                                            //Reward is lower than the Reward Minimum Purchase
                                            rewardMessage = "Reward is invalid: the Reward Minimum Product Amount is lower than the total eligible product amount.";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;

                case TRANSACTION_CREDIT:
                    //don't do anything with this yet
                    rewardIsStillAvailable = true;
                    rewardMessage = "";

                        /*//get the total valid product amount for this reward
                        BigDecimal totalProductsAmountForReward = this.getTotalProductAmountForTransCreditReward();

                        //Add up transaction total for Transaction Credits
                        if (totalProductsAmountForReward.compareTo(loyaltyReward.getMinPurchase()) == -1) { //transaction did not reach the min purchase amount
                            minTransAmtCheck = false;
                            loyaltyReward.setNotes("Reward is invalid: the Reward Minimum Product Amount is lower than the total eligible product amount.");
                        }*/

                    break;
            }

            if (!rewardIsStillAvailable) { //remove it from the "Available from Transaction list"
                rewardsToRemove.add(rewardAvailForTrans.getReward().getId());
                rewardsToRemoveHM.put(rewardAvailForTrans.getReward().getId(), rewardMessage);
            }
        }

        if (!rewardsToRemoveHM.isEmpty()) {
            Iterator<Integer> rewardToRemoveIterator = rewardsToRemoveHM.keySet().iterator();
            while (rewardToRemoveIterator.hasNext()) {
                Integer rewardToRemoveId = rewardToRemoveIterator.next();

                //Remove the Reward from the "Rewards Available For Transaction List"
                Iterator<LoyaltyRewardLineItemModel> removeSubmittedIterator = transactionModel.getLoyaltyAccount().getRewardsAvailableForTransaction().iterator();
                while (removeSubmittedIterator.hasNext()) {
                    LoyaltyRewardLineItemModel curRewardLine = removeSubmittedIterator.next();
                    if (rewardToRemoveId.equals(curRewardLine.getReward().getId())) {
                        removeSubmittedIterator.remove();
                    }
                }

                //Set the Notes on the reward available list to communicate to the client why the reward is not available
                for (LoyaltyRewardModel loyaltyReward : transactionModel.getLoyaltyAccount().getRewardsAvailable()) {
                    if (loyaltyReward.getId().equals(rewardToRemoveId)) {
                        loyaltyReward.setNotes(rewardsToRemoveHM.get(rewardToRemoveId));
                    }
                }
            }
        }
    }

    /**
     * After a transaction is saved, re-fetch the Loyalty Points
     *
     * @throws Exception
     */
    public void refreshLoyaltyAccountPointsAfterSave() throws Exception {

        if (this.getTransactionModel().getLoyaltyAccount() != null) {
            if ((this.getTransactionModel().getLoyaltyPointsEarned() != null && !this.getTransactionModel().getLoyaltyPointsEarned().isEmpty())
                    || (this.getTransactionModel().getLoyaltyPointsRedeemed() != null && !this.getTransactionModel().getLoyaltyPointsRedeemed().isEmpty())) {
                //only re-query the points if there was a change

                switch (transactionModel.getApiActionTypeEnum()) {
                    case SALE:
                        this.getTransactionModel().getLoyaltyAccount().setRewardsAvailable(LoyaltyRewardCollection.getValidLoyaltyRewardsForAccount(transactionModel).getCollection());
                        break;
                }

                this.getTransactionModel().getLoyaltyAccount().setLoyaltyPoints(LoyaltyPointCollection.getAllLoyaltyPointsForEmployeeAndActivePrograms(transactionModel.getLoyaltyAccount().getId(), this.getTransactionModel().getTerminal(), false).getCollection());
            }
        }
    }

    /**
     * Update LoyaltyAccountModel.available so it is returned properly in the JSON response
     *
     * @throws Exception
     */
    public void refreshLoyaltyAccountAvailableAfterSave(Integer accountId, BigDecimal updatedAmount) throws Exception {

        switch (transactionModel.getApiActionTypeEnum()) {
            case OPEN:
                //don't do this for OPEN Transactions
                break;

            default:

                if (this.getTransactionModel().getLoyaltyAccount() != null) {
                    if (this.getTransactionModel().getLoyaltyAccount().getId().equals(accountId)) {

                        //update Global Balance
                        this.getTransactionModel().getLoyaltyAccount().setGlobalBalance(this.getTransactionModel().getLoyaltyAccount().getGlobalBalance().add(updatedAmount));

                        //update Store Balance
                        this.getTransactionModel().getLoyaltyAccount().setTerminalGroupBalance(this.getTransactionModel().getLoyaltyAccount().getTerminalGroupBalance().add(updatedAmount));

                        //update Single Charge Limit
                        if (this.getTransactionModel().getApiActionTypeEnum().equals(PosAPIHelper.ApiActionType.SALE)) { //HP 1407: Only update the Single Charge Limit for SALEs
                            this.getTransactionModel().getLoyaltyAccount().setSingleChargeLimit(this.getTransactionModel().getLoyaltyAccount().getSingleChargeLimit().subtract(updatedAmount));
                        }

                        this.getTransactionModel().getLoyaltyAccount().initAvailable(); //get the updated amount for available
                        this.getTransactionModel().getLoyaltyAccount().initReceiptBalance(); //get the updated amount for receipt available

                    }
                }

                break;
        }
    }

    /**
     * For each QC Account on the transaction, refresh the Account Model to update the QC Balances (Terminal Group Bal, Global Bal, available)
     * originally I queried the database to do this, but if the background processor is created QC Transactions then the balance
     * wasn't updated at the time of the response
     *
     * @throws Exception
     */
    public void refreshAccountBalanceAfterSave() throws Exception {

        switch (transactionModel.getApiActionTypeEnum()) {
            case OPEN:
                //don't do this for OPEN Transactions
                break;

            default:

                for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {

                    //make sure it's a QC Tender
                    if ((tenderLineItemModel.getTender() != null &&
                            tenderLineItemModel.getTender().getTenderTypeId() != null) &&
                            tenderLineItemModel.getTender().getTenderTypeId().equals(PosAPIHelper.TenderType.QUICKCHARGE.toInt())) {

                        if ((tenderLineItemModel.getAccount() != null &&
                                tenderLineItemModel.getAccount().getId() != null &&
                                !tenderLineItemModel.getAccount().getId().toString().isEmpty()) &&
                                !tenderLineItemModel.getAccount().getId().equals(0)) {

                            //update Global Balance
                            tenderLineItemModel.getAccount().setGlobalBalance(tenderLineItemModel.getAccount().getGlobalBalance().add(tenderLineItemModel.getExtendedAmount()));

                            //update Store Balance
                            tenderLineItemModel.getAccount().setTerminalGroupBalance(tenderLineItemModel.getAccount().getTerminalGroupBalance().add(tenderLineItemModel.getExtendedAmount()));

                            //update Single Charge Limit
                            if (this.getTransactionModel().getApiActionTypeEnum().equals(PosAPIHelper.ApiActionType.SALE)) { //HP 1407: Only update the Single Charge Limit for SALEs
                                tenderLineItemModel.getAccount().setSingleChargeLimit(tenderLineItemModel.getAccount().getSingleChargeLimit().subtract(tenderLineItemModel.getExtendedAmount()));
                            }

                            tenderLineItemModel.getAccount().initAvailable();  //get the updated amount for available
                            tenderLineItemModel.getAccount().initReceiptBalance(); //get the updated amount for receipt available

                            //update the balance on the Loyalty Account
                            refreshLoyaltyAccountAvailableAfterSave(tenderLineItemModel.getAccount().getId(), tenderLineItemModel.getExtendedAmount());
                        }
                    }
                }

                for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : this.getTransactionModel().getReceivedOnAccounts()) {
                    if ((receivedOnAccountLineItemModel.getAccount() != null &&
                            receivedOnAccountLineItemModel.getAccount().getId() != null &&
                            !receivedOnAccountLineItemModel.getAccount().getId().toString().isEmpty()) &&
                            !receivedOnAccountLineItemModel.getAccount().getId().equals(0)) {

                        //update Global Balance
                        receivedOnAccountLineItemModel.getAccount().setGlobalBalance(receivedOnAccountLineItemModel.getAccount().getGlobalBalance().subtract(receivedOnAccountLineItemModel.getExtendedAmount()));

                        //update Store Balance
                        receivedOnAccountLineItemModel.getAccount().setTerminalGroupBalance(receivedOnAccountLineItemModel.getAccount().getTerminalGroupBalance().subtract(receivedOnAccountLineItemModel.getExtendedAmount()));

                        receivedOnAccountLineItemModel.getAccount().initAvailable();  //get the updated amount for available
                        receivedOnAccountLineItemModel.getAccount().initReceiptBalance(); //get the updated amount for receipt available

                        //update Single Charge Limit
                        //do not update the Single Charge Limit on a ROA - that value should never increase
                        //receivedOnAccountLineItemModel.getAccount().setSingleChargeLimit(receivedOnAccountLineItemModel.getAccount().getSingleChargeLimit().add(receivedOnAccountLineItemModel.getExtendedAmount()));

                        if (this.getTransactionModel().getLoyaltyAccount() != null) {
                            if (this.getTransactionModel().getLoyaltyAccount().getId().equals(receivedOnAccountLineItemModel.getAccount().getId())) {

                                //update Global Balance
                                this.getTransactionModel().getLoyaltyAccount().setGlobalBalance(this.getTransactionModel().getLoyaltyAccount().getGlobalBalance().subtract(receivedOnAccountLineItemModel.getExtendedAmount()));

                                //update Store Balance
                                this.getTransactionModel().getLoyaltyAccount().setTerminalGroupBalance(this.getTransactionModel().getLoyaltyAccount().getTerminalGroupBalance().subtract(receivedOnAccountLineItemModel.getExtendedAmount()));

                                this.getTransactionModel().getLoyaltyAccount().initAvailable(); //get the updated amount for available
                                this.getTransactionModel().getLoyaltyAccount().initReceiptBalance(); //get the updated amount for receipt available

                                //update Single Charge Balance
                                //do not update the Single Charge Limit on a ROA - that value should never increase
                                //this.getTransactionModel().getLoyaltyAccount().setSingleChargeLimit(this.getTransactionModel().getLoyaltyAccount().getSingleChargeLimit().subtract(updatedAmount));
                            }
                        }
                    }
                }

                break;
        }
    }

    /**
     * After the transaction sale, clear out all rewards available for transaction
     *
     * @throws Exception
     */
    public void removeRewardsAvailableForTransaction() throws Exception {
        //remove the submitted reward from the Loyalty Account - available rewards for transaction list
        if (transactionModel.getLoyaltyAccount() != null && transactionModel.getLoyaltyAccount().getRewardsAvailableForTransaction() != null && !transactionModel.getLoyaltyAccount().getRewardsAvailableForTransaction().isEmpty()) {
            Iterator<LoyaltyRewardLineItemModel> removeSubmittedIterator = transactionModel.getLoyaltyAccount().getRewardsAvailableForTransaction().iterator();
            while (removeSubmittedIterator.hasNext()) {
                LoyaltyRewardLineItemModel curRewardLine = removeSubmittedIterator.next();
                removeSubmittedIterator.remove();
            }
        }
    }

    //endregion

    //region Rewards - PATransLineItem Level

    /**
     * Create Item Reward Models for applicable products
     * @throws Exception
     */
    public void applyLoyaltyRewardsToProducts() throws Exception {
        if (this.getTransactionModel().getRewards() != null && !this.getTransactionModel().getRewards().isEmpty()) {
            boolean secondInquiry = false;
            for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                //check if there are NO details on the products
                //if so, don't apply them again
                if (productLineItemModel.getRewards() != null && !productLineItemModel.getRewards().isEmpty()) {
                    secondInquiry = true;
                }
            }

            LoyaltyRewardApplication loyaltyRewardApplication = new LoyaltyRewardApplication(this.getTransactionModel());
            this.setTransactionModel(loyaltyRewardApplication.applyRewards(secondInquiry));
        }
    }

    /*
     *   check the Maximum Discount Transaction Total.
     *   This has to be done after the ItemRewardModels are created, because Free Product prices depend on the amount on the product
     * */
    private void validateMaximumDiscountTransactionTotal() throws Exception {
        //validate Maximum Discount Transaction Total

        //total up the reward ID's with the value
        HashMap<Integer, BigDecimal> rewardTotalList = new HashMap<>();
        for (LoyaltyRewardLineItemModel loyaltyRewardRedeemed : transactionModel.getRewards()) {
            if (!rewardTotalList.containsKey(loyaltyRewardRedeemed.getReward().getId())) {
                rewardTotalList.put(loyaltyRewardRedeemed.getReward().getId(), loyaltyRewardRedeemed.getExtendedAmount().abs());
            } else {
                rewardTotalList.put(loyaltyRewardRedeemed.getReward().getId(), rewardTotalList.get(loyaltyRewardRedeemed.getReward().getId()).add(loyaltyRewardRedeemed.getExtendedAmount().abs()));
            }
        }

        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : transactionModel.getRewards()) {
            if (loyaltyRewardLineItemModel.getReward().getMaxDiscount() != null
                    && !loyaltyRewardLineItemModel.getReward().getMaxDiscount().toString().isEmpty()
                    && loyaltyRewardLineItemModel.getReward().getMaxDiscount().compareTo(BigDecimal.ZERO) == 1) { //Max Discount is > 0
                BigDecimal maxDiscountForThisReward = loyaltyRewardLineItemModel.getReward().getMaxDiscount();
                if (rewardTotalList.get(loyaltyRewardLineItemModel.getReward().getId()).compareTo(maxDiscountForThisReward) == 1) {
                    throw new MissingDataException("Reward: " + loyaltyRewardLineItemModel.getReward().getName() + " cannot have a Maximum Discount more than " + maxDiscountForThisReward + " for the transaction", transactionModel.getTerminal().getId());
                }
            }
        }
    }

    //endregion

    //region Products - PATransLineItem Level

    //PRODUCTS - builds points and point details
    //QC_LoyaltyAccountPoints
    //QC_PAItemLoyaltyPoints
    public void createProductLoyaltyPoints() throws Exception {

        //check if Loyalty Account is valid
        if (this.getTransactionModel().getLoyaltyAccount() == null) {
            Logger.logMessage("There are no valid Loyalty Account for this transaction", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.DEBUG);
            return;
        }

        //if Loyalty Account is null or Not Active
        if (this.getTransactionModel().getLoyaltyAccount().getStatus() == null || !this.getTransactionModel().getLoyaltyAccount().getStatus().equalsIgnoreCase("ACTIVE")) {
            Logger.logMessage("Loyalty Account does not have a status of Active", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.DEBUG);
            return;
        }

        //check if there are any Tenders on the transaction
        if (this.getTransactionModel().getTenders() == null || this.getTransactionModel().getTenders().isEmpty()) {
            Logger.logMessage("There are no valid Tender Lines for this transaction", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.DEBUG);
            return;
        }

        //get all Loyalty Programs for the transaction's revenue center and tender
        LoyaltyProgramCollection loyaltyProgramCollection = LoyaltyProgramCollection.getValidLoyaltyPrograms(this.getTransactionModel());

        if (loyaltyProgramCollection.getCollection() != null && !loyaltyProgramCollection.getCollection().isEmpty()) {
            //Create Loyalty Points
            LoyaltyPointCollection transactionLoyaltyPoints = new LoyaltyPointCollection();

            //Loop by Loyalty Program
            //and then by transaction
            //add a new Loyalty Point Model for each
            Logger.logMessage("*********************Start Loyalty Calculation*********************", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.TRACE);
            for (LoyaltyProgramModel loyaltyProgram : loyaltyProgramCollection.getCollection()) {
                LoyaltyPointCollection programLoyaltyPoints = new LoyaltyPointCollection();
                programLoyaltyPoints.calculatePoints(transactionModel, loyaltyProgram);
                transactionLoyaltyPoints.getCollection().addAll(programLoyaltyPoints.getCollection());
            }
            Logger.logMessage("*********************End Loyalty Calculation***********************", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.TRACE);

            //Add point totals to the transaction model
            transactionModel.setLoyaltyPointsAll(transactionLoyaltyPoints.getCollection());
        }
    }

    //endregion

    //endregion

    //endregion

    //region Save Methods

    public void saveTransaction() throws Exception {
        saveTransaction(null);
    }

    //save QCPOS transaction to database
    public void saveTransaction(JDCConnection conn) throws Exception {
        String logFileName = PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId());

        ArrayList aResponse = new ArrayList();
        int saveStatus = 0;
        Exception mainEx = null;

        try {
            if(conn == null) {
                conn = dm.pool.getConnection(false, logFileName);
            }
        } catch (SQLException qe) {
            Logger.logMessage("TransactionBuilder.save no connection to DB.", logFileName);
            Logger.logMessage(qe.getMessage(), logFileName);
            throw new MissingDataException("Could not establish a connection to the database.", transactionModel.getTerminal().getId());
        }

        int iPATransactionId = 0;

        try {

            this.setQcImportStatus(); //check if the POS API is running in offline mode, and set the QcImportStatus accordingly
            this.setTransactionType();

            conn.setAutoCommit(false);
            this.checkForTimeOut(logFileName);

            if (!this.getTransactionModel().isCancelTransactionType() &&
                    !this.getTransactionModel().isTrainingTransactionType()) {
                //Void or Refund the Original Transaction first
                this.saveVoidsAndRefunds(conn, logFileName);
                this.saveTransactionOrderNumber(conn, logFileName);
            }

            //save transaction header
            iPATransactionId = this.saveQcPosTransactionHeader(conn, logFileName);


            this.saveOpenTransactionLinkedFromTransIds(conn, logFileName);

            this.saveTransLineItems(conn, logFileName);

            if (!this.getTransactionModel().isCancelTransactionType()
                    && !this.getTransactionModel().isTrainingTransactionType()
                    && !this.getTransactionModel().isPaymentGatewayInquireTransactionType()
                    && !(this.getTransactionModel().getTransactionTypeEnum().equals(PosAPIHelper.TransactionType.OPEN)
                    && !this.getTransactionModel().getPosType().equals(PosAPIHelper.PosType.KIOSK_ORDERING)
                    && !this.getTransactionModel().getPosType().equals(PosAPIHelper.PosType.ONLINE_ORDERING) )) {

                //save a QC Transaction per Tender
                this.saveQCTransactions(conn, logFileName);

                this.saveQCDiscounts(conn, logFileName);

                //this.updateEmployeeBalance(conn, logFileName);

                this.saveProductInventory(conn, logFileName);

                this.saveCashierShiftEnd(conn, logFileName);

                //Save Product Loyalty Points
                this.saveLoyaltyProductAccountPoints(conn, logFileName);

                //Save Reward Loyalty Points
                //things get cleared out here
                this.saveLoyaltyRewardAccountPoints(conn, logFileName);

                //Save Reward Item Loyalty Points
                this.saveLoyaltyRewardAccountPointDetails(conn, logFileName);

                //Save Product Item Loyalty Points
                this.saveLoyaltyProductAccountPointDetails(conn, logFileName);

                //Save Adjustment Loyalty Points
                this.saveLoyaltyAdjustmentPoints(conn, logFileName);

                //Save Donation Loyalty Points
                this.saveLoyaltyDonationPoints(conn, logFileName);

                //this is currently used by online ordering
                this.savePrinterQueueHeaderRecord(conn, logFileName);

                this.checkForTimeOut(logFileName);
            }

            saveStatus = 1;
            conn.commit();
            Logger.logMessage("Transaction Saved Successfully (PATransactionID=" + iPATransactionId + ")", logFileName, Logger.LEVEL.DEBUG);
        } catch (Exception ex) {
            Logger.logMessage("Transaction Failed (PATransactionID=" + iPATransactionId + ")", logFileName, Logger.LEVEL.ERROR);
            mainEx = ex;

            try {
                conn.rollback();
                //conn.setAutoCommit(true);  // THIS WAS ALREADY COMMENTED OUT ... SHOULD IT HAVE BEEN?
                Logger.logMessage("Rollback Successfully (PATransactionID=" + iPATransactionId + ")", logFileName, Logger.LEVEL.ERROR);
            } catch (SQLException e) {
                Logger.logException("TransactionBuilder.save. SQL exception", logFileName, e);
            }
            saveStatus = -1;
            Logger.logException(ex, logFileName);
        } finally {
            try {
                conn.setAutoCommit(true);
                dm.pool.returnConnection(conn, logFileName);
                Logger.logMessage("Database Transaction Committed Successfully (PATransactionID=" + iPATransactionId + ")", logFileName, Logger.LEVEL.DEBUG);
            } catch (SQLException ex) {
                Logger.logException(ex, logFileName);
            }
        }

        //Now the the "finally" is done and the database should be cleaned up
        //if the save did not occur properly, return the error to the API Client
        if (saveStatus < 1) {
            if (mainEx != null) {
                throw mainEx;
            } else {
                throw new MissingDataException("Error Saving Transaction.  Check logs for more details.", transactionModel.getTerminal().getId());
            }
        }

        //refetch the updated Loyalty Points for the Loyalty Account
        this.refreshLoyaltyAccountPointsAfterSave();

        //update the Account Models so the QC balances are updated
        this.refreshAccountBalanceAfterSave();

        //remove the submitted reward from the Loyalty Account - available rewards for transaction list
        this.removeRewardsAvailableForTransaction();

        this.fixAccountBalancesForDisplay();

        // If the transaction was made in offline mode, set recorded offline to true
        if(this.getTransactionModel().isOfflineMode()) {
            this.getTransactionModel().setWasRecordedOffline(true);
        }

        //this writes to the database
        this.getTransactionModel().getTerminal().logOperationDuration(this.getTransactionModel(), conn, logFileName);

    }

    //used for QCPos transactions, and for 3rd party Loyalty transactions
    public Integer saveQcPosTransactionHeader(JDCConnection conn, String logFileName) throws Exception {
        DataManager dm = new DataManager();
        Logger.logMessage("Saving QCPos Transaction Header. TransactionBuilder.saveQcPosTransactionHeader.", logFileName, Logger.LEVEL.DEBUG);

        //Figure out what transaction type to save on the transaction header
        Integer transactionTypeId = this.getTransactionModel().getTransactionTypeId();

        //QCPos
        if (this.getTransactionModel().getTerminal().getTypeId() == PosAPIHelper.TerminalType.MMHAYES_POS.toInt()) {
            //if the transaction came in through the Cancel endpoint, update the transType to Cancel
            if (this.getTransactionModel().isCancelTransactionType()) {
                transactionTypeId = PosAPIHelper.TransactionType.CANCEL.toInt();
            }

            //if the transaction came in through the Training endpoint, update the transType to Training
            if (this.getTransactionModel().isTrainingTransactionType()) {
                transactionTypeId = PosAPIHelper.TransactionType.TRAINING.toInt();
            }

            if (this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {
                transactionTypeId = PosAPIHelper.TransactionType.PAYMENT_GATEWAY_INQUIRE.toInt();
            }
        }

        Integer iPATransactionId = 0;

        iPATransactionId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransactionMaster", new Object[]{
                //TransTypeID is passed in this first line
                transactionTypeId,
                this.getTransactionModel().getTerminal().getId(),
                this.getTransactionModel().getTimeStamp(),
                (this.getTransactionModel().getUserId()) == null ? 0 : this.getTransactionModel().getUserId(),
                this.getTransactionModel().getTransComment(), //TransComment
                this.getTransactionModel().getName(), //name
                this.getTransactionModel().getAddress(), //address,
                this.getTransactionModel().getCity(), //city,
                this.getTransactionModel().getState(), //state,
                this.getTransactionModel().getZip(), //zip,
                this.getTransactionModel().getEmail(), //email,
                this.getTransactionModel().getPhone(), //phone,
                this.getTransactionModel().getQcImportStatus(), // -1: Incomplete Transaction, 0: Awaiting Import, 1: Already Imported, 2: Not QC Transaction, 3: In Process (SET to -1 so BP and POSAnySync will leave it alone for now)
                (this.getTransactionModel().wasRecordedOffline()) ? -1 : null, //offlineTransactionID
                this.getTransactionModel().getOriginalTransactionId(), //(paTransIdOriginal == 0 ? null : new Integer(paTransIdOriginal)),
                (this.getTransactionModel().getDirectVoidQuantity() == null) ? 0 : this.getTransactionModel().getDirectVoidQuantity(), //new Integer(directVoidQty),
                (this.getTransactionModel().getDirectVoidAmount() == null) ? 0 : this.getTransactionModel().getDirectVoidAmount(), //new Double(directVoidAmt),
                (this.getTransactionModel().getIndirectVoidQuantity() == null) ? 0 : this.getTransactionModel().getIndirectVoidQuantity(), //new Integer(indirectVoidQty),
                (this.getTransactionModel().getIndirectVoidAmount() == null) ? 0 : this.getTransactionModel().getIndirectVoidAmount(), // new Double(indirectVoidAmt),
                (this.getTransactionModel().getPriceOverrideQuantity() == null) ? 0 : this.getTransactionModel().getPriceOverrideQuantity(), // new Integer(priceOverrideQty),
                (this.getTransactionModel().getTransPriceLevel() == null) ? 0 : this.getTransactionModel().getTransPriceLevel(), // new Integer(priceLevel),
                (this.getTransactionModel().getDrawerNum() == null) ? 0 : this.getTransactionModel().getDrawerNum(),
                PosAPIHelper.OrderType.convertStringToInt(this.getTransactionModel().getOrderType()), //convert the order type back to int value
                this.getTransactionModel().getTransReferenceNumber(),
                (this.getTransactionModel().getEstimatedOrderTime() != null && this.getTransactionModel().getEstimatedOrderTime().isEmpty()) ? null : this.getTransactionModel().getEstimatedOrderTime(),
                this.getTransactionModel().getOrderNumber(),
                this.getTransactionModel().getTransactionLabel(),
                (this.getTransactionModel().getTransactionStatusId() == null) ? PosAPIHelper.TransactionStatus.COMPLETED.toInt() : this.getTransactionModel().getTransactionStatusId(),
                this.getTransactionModel().getTransStatusLastUpdated(),
                this.getTransactionModel().getLinkedToPaTransactionId(), //30th SQL parameter
                (this.getTransactionModel().getLoyaltyAccount()) == null ? null : this.getTransactionModel().getLoyaltyAccount().getId(),
                this.getTransactionModel().getTicketOpenedDTM(),
                this.getTransactionModel().getPosKioskTerminalId(),
                this.getTransactionModel().getLinkedFromPaTransactionIdsCsv(),
                this.getTransactionModel().isGrabAndGoTransaction(),
                this.getTransactionModel().getLoyaltyAdjustmentTypeId()

        }, logFileName, conn);

        if (iPATransactionId <= 0) {
            throw new MissingDataException("Transaction could not be recorded in QC_PATransaction", transactionModel.getTerminal().getId());
        }

        this.getTransactionModel().setId(iPATransactionId);

        return iPATransactionId;
    }

    /**
     * Get the next Order Number stored in the QC_RevenueCenter table.  Increment the Order Number by 1
     * This should be allow for SALE transactions, and SUSPENDED create transactions (not SUSPENDED update)
     *
     * @param conn
     * @param logFileName
     * @throws Exception
     */
    public void saveTransactionOrderNumber(JDCConnection conn, String logFileName) throws Exception {

        switch (this.getTransactionModel().getPosType()) {

            case THIRD_PARTY:
            case MY_QC_FUNDING:
                //Don't generate an Order Number for Third Party Transactions
                break;
            default:

                switch (this.getTransactionModel().getApiActionTypeEnum()) {
                    case SALE:
                    case OPEN:

                        if (this.getTransactionModel().getApiActionTypeEnum().equals(PosAPIHelper.ApiActionType.OPEN)
                                && !this.getTransactionModel().isCreateOpen()
                                && this.getTransactionModel().isOfflineMode()) {
                            this.getTransactionModel().setOrderNumber(null);
                            return;
                        }

                        if (this.getTransactionModel().isPaymentGatewayInquireTransactionType()){
                            this.getTransactionModel().setOrderNumber(null);
                            return;
                        }

                        if (this.getTransactionModel().getOrderNumber() == null
                                || this.getTransactionModel().getOrderNumber().isEmpty()) { //create new order number if the order number is blank

                            Logger.logMessage("Generating new order number for Transaction. TransactionBuilder.saveTransactionOrderNumber.", logFileName, Logger.LEVEL.DEBUG);

                            ArrayList<HashMap> updatedOrderNumbers = dm.parameterizedExecuteQuery("data.posapi30.GenerateOrderNumberSP",
                                    new Object[]{
                                            this.getTransactionModel().getTerminal().getRevenueCenterId()
                                    },
                                    logFileName,
                                    true, conn
                            );

                            if (updatedOrderNumbers == null || updatedOrderNumbers.isEmpty()) {
                                throw new MissingDataException("Error generating new Order Number", transactionModel.getTerminal().getId());
                            }

                            HashMap orderNumberHM = updatedOrderNumbers.get(0);
                            Integer updatedOrderNumber = CommonAPI.convertModelDetailToInteger(orderNumberHM.get("NEXTORDERNUM"));
                            String updatedOrderNumberStr = updatedOrderNumber.toString();

                            //if terminal is offline, prepend the terminal number
                            if (this.getTransactionModel().isOfflineMode()) {
                                updatedOrderNumberStr = this.getTransactionModel().getTerminal().getId() + "-" + updatedOrderNumberStr;
                            }

                            this.getTransactionModel().setOrderNumber(updatedOrderNumberStr);
                            Logger.logMessage("New Order Number: " + updatedOrderNumberStr, logFileName, Logger.LEVEL.DEBUG);
                        }

                        break;
                    default:

                        this.getTransactionModel().setOrderNumber(null);
                        break;
                }
                break;
        }
    }

    /**
     * @param conn
     * @param logFileName
     * @throws Exception
     */
    public void saveOpenTransactionLinkedFromTransIds(JDCConnection conn, String logFileName) throws Exception {
        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case OPEN:
            case SALE:

                if (this.getTransactionModel().getLinkedFromPaTransactionIds() != null
                        && !this.getTransactionModel().getLinkedFromPaTransactionIds().isEmpty()
                        && !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {

                    Logger.logMessage("Updating Linked Transactions for Open Transaction. TransactionBuilder.saveOpenTransactionLinkedFromTransIds.", logFileName, Logger.LEVEL.DEBUG);
                    String lastUpdatedDate = getCurrentDate();

                    //use the new transaction id to update the linkedFromPATransationIDs
                    for (Object linkedTransId : this.getTransactionModel().getLinkedFromPaTransactionIds()) {

                        if (linkedTransId == null || linkedTransId.toString().isEmpty()){
                            continue;
                        }

                        //check if integer was passed in as string
                        if (StringUtils.isNumeric(linkedTransId.toString())) {
                            if (!this.getTransactionModel().isOfflineMode()) {
                                Integer updateResult = dm.parameterizedExecuteNonQuery("data.posapi30.UpdateLinkedPATransaction", new Object[]{
                                        this.getTransactionModel().getId(),
                                        PosAPIHelper.TransactionStatus.EDIT_COMPLETED.toInt(),
                                        lastUpdatedDate,
                                        linkedTransId

                                }, logFileName, conn);

                                if (updateResult == null || updateResult <= 0) {
                                    throw new MissingDataException("Error updating linked transactions", transactionModel.getTerminal().getId());
                                }
                            }
                        } else {
                            HashMap<String, Integer> transIdParts = DataFunctions.parseOfflineTransIdFormat(linkedTransId.toString(), "T");

                            Integer localPaTransId = transIdParts.get("localPaTransId");
                            Integer localTerminalId = transIdParts.get("localTerminalId");

                            String queryKey = "";
                            if (this.getTransactionModel().isOfflineMode()){
                                queryKey = "data.posapi30.UpdateLinkedPATransactionByIdLocalDB";
                            } else {
                                //On the server, search on the PATransOfflineId field
                                queryKey = "data.posapi30.UpdateLinkedPATransactionByOfflineId";
                            }

                            Integer updateResult = dm.parameterizedExecuteNonQuery(queryKey, new Object[]{
                                    this.getTransactionModel().getId(),
                                    PosAPIHelper.TransactionStatus.EDIT_COMPLETED.toInt(),
                                    lastUpdatedDate,
                                    localPaTransId,
                                    localTerminalId

                            }, logFileName, conn);

                            if (updateResult == null || updateResult <= 0) {
                                throw new MissingDataException("Error updating linked offline transactions", transactionModel.getTerminal().getId());
                            }
                        }
                    }
                }

                break;
        }
    }

    protected Integer saveProductLineItem(ProductLineItemModel productLineItemModel, JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        DataManager dm = new DataManager();

        Integer iPATransactionDetailId = 0;
        iPATransactionDetailId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItem", new Object[]{
                productLineItemModel.getItemTypeId(),
                productLineItemModel.getProduct().getId(),
                (productLineItemModel.getQuantity() == null) ? 1 : productLineItemModel.getQuantity(),
                (productLineItemModel.getAmount() == null) ? 0 : productLineItemModel.getAmount(),
                (productLineItemModel.getEligibleAmount() == null) ? 0 : productLineItemModel.getEligibleAmount(), //Eligible Amount
                productLineItemModel.getItemComment(),
                productLineItemModel.getEmployeeId(),
                null, //splits
                iPATransactionId, // new Integer(iPATransId),
                null, //credit card info
                productLineItemModel.getBadgeAssignmentId(), //badgeAssignmentId
                productLineItemModel.getKeypadId(),
                productLineItemModel.getOriginalOrderNumber(),
                productLineItemModel.getPrintStatusId(),
                productLineItemModel.getLinkedTransLineItemId(),
                productLineItemModel.getComboTransLineItemId(),
                productLineItemModel.getBasePrice(),
                productLineItemModel.getComboPrice(),
                productLineItemModel.getComboDetailId(),
                (productLineItemModel.getPrepOption() == null) ? null : productLineItemModel.getPrepOption().getId(),
                (productLineItemModel.getPrepOption() == null) ? null : productLineItemModel.getPrepOption().getPrepOptionSetId(),
                (productLineItemModel.getPrepOption() == null) ? BigDecimal.ZERO : productLineItemModel.getPrepOption().getPrice(),
                productLineItemModel.getLinkedFromPaTransLineItemIds()

        }, logFileName, conn);

        if (iPATransactionDetailId <= 0) {
            throw new MissingDataException("Product Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
        }
        productLineItemModel.setId(iPATransactionDetailId);

        for (ItemLoyaltyPointModel itemLoyaltyPointDetail : productLineItemModel.getLoyaltyPointDetails()) {
            itemLoyaltyPointDetail.setTransLineItemId(iPATransactionDetailId);
        }

        //update the corresponding discountLineItem's linkedPATransLineItemID with the product's paTransLineItemID for combos
        if(productLineItemModel.getComboDetailId() != null) {
            this.updateDiscountLinesForCombos(productLineItemModel, iPATransactionDetailId);
        }

        return iPATransactionDetailId;
    }

    public void saveTransLineItems(JDCConnection conn, String logFileName) throws Exception {
        //switch for trans line items

        Integer iPATransactionId = this.getTransactionModel().getId();
        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case SALE:
            case VOID:
            case REFUND:
            case REFUND_MANUAL:
            case ROA:
            case PO:
            case OPEN:
            case LOYALTY_ADJUSTMENT:
                this.saveComboLines(conn, logFileName, iPATransactionId);
                this.saveDiscountLines(conn, logFileName, iPATransactionId); //save Discounts first, we save the PATransLineItemID to the ItemDisocuntModels save in saveProductDiscounts
                this.saveProductLines(conn, logFileName, iPATransactionId);
                this.saveTaxLines(conn, logFileName, iPATransactionId);
                this.saveTenderLines(conn, logFileName, iPATransactionId);
                this.savePaidOuts(conn, logFileName, iPATransactionId);
                this.saveReceivedOnAccounts(conn, logFileName, iPATransactionId);
                this.saveServiceChargeLines(conn, logFileName, iPATransactionId);
                this.saveGratuityLines(conn, logFileName, iPATransactionId);
                this.saveSurchargeLines(conn, logFileName, iPATransactionId);
                this.saveDonationLineItems(conn, logFileName, iPATransactionId);

                //Only save Loyalty if we are not in Cancel or Training Mode
                if (!this.getTransactionModel().isCancelTransactionType() &&
                        !this.getTransactionModel().isTrainingTransactionType() &&
                        !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {
                    this.saveLoyaltyRewards(conn, logFileName, iPATransactionId);
                }
                break;
            default:
                //no line records are written for NOSALE, BROWSERCLOSE, LOGIN, LOGOUT, BATCH CLOSE
                break;
        }
    }

    //Update the original transactions for Voids and Refunds
    public void saveVoidsAndRefunds(JDCConnection conn, String logFileName) throws Exception {
        //switch for pre-Header save
        switch (this.getTransactionModel().getApiActionTypeEnum()) {

            case VOID:
                Logger.logMessage("Voiding QCPOS Transaction.  TransactionBuilder.voidOriginalTransaction", logFileName, Logger.LEVEL.DEBUG);
                this.getTransactionModel().setTransactionTypeEnum(PosAPIHelper.TransactionType.VOID);
                if (!this.getTransactionModel().isPaymentGatewayInquireTransactionType()){
                    voidOriginalTransaction(conn, logFileName, this.getTransactionModel().getId());
                }
                //set the Original Transaction Id for the current transaction
                getTransactionModel().setOrginalTransactionID(this.getTransactionModel().getId());
                updateTransactionTimeStamp();
                flipSignsOnTransactionModel();
                break;
            case REFUND:
                Logger.logMessage("Refunding QCPOS Transaction.  TransactionBuilder.refundOriginalTransaction", logFileName, Logger.LEVEL.DEBUG);
                this.getTransactionModel().setTransactionTypeEnum(PosAPIHelper.TransactionType.REFUND);
                if (!this.getTransactionModel().isPaymentGatewayInquireTransactionType()){
                    refundOriginalTransaction(conn, logFileName, this.getTransactionModel().getId());
                }
                //set the Original Transaction Id for the current transaction
                getTransactionModel().setOrginalTransactionID(this.getTransactionModel().getId());
                updateTransactionTimeStamp();
                flipSignsOnTransactionModel();
                break;
            default:
                break;
        }
    }

    //region Save Product Lines

    public void saveProductLines(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {

        this.sortProductLinesByOriginalSequence();

        int iPATransactionDetailId = 0;
        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
            iPATransactionDetailId = 0;

            iPATransactionDetailId = this.saveProductLineItem(productLineItemModel, conn, logFileName, iPATransactionId);
            productLineItemModel.setId(iPATransactionDetailId);

            if (productLineItemModel.getId() <= 0) {
                throw new MissingDataException("Transaction Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
            }

            //save Product sub-items
            this.saveProductModifiers(productLineItemModel, conn, logFileName);
            this.saveProductTaxes(productLineItemModel, conn, logFileName);
            this.saveProductDiscounts(productLineItemModel, conn, logFileName);
            this.saveProductRewards(productLineItemModel, conn, logFileName);
            this.saveProductSurcharges(productLineItemModel, conn, logFileName);
            this.updateProductComboDiscounts(productLineItemModel, conn, logFileName);
        }
    }

    public void saveProductModifiers(ProductLineItemModel productLineItemModel, JDCConnection conn, String logFileName) throws Exception {
        for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()) {
            Integer transLineModId = 0;

            modifierLineItemModel.setParentPaTransLineItemId(productLineItemModel.getId());

            Logger.logMessage("Saving Modifier for transaction detail number " + productLineItemModel.getId(), logFileName, Logger.LEVEL.DEBUG);
            transLineModId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItemMod", new Object[]{
                    modifierLineItemModel.getItemTypeId(),
                    modifierLineItemModel.getProduct().getId(),
                    (modifierLineItemModel.getQuantity() == null) ? 1 : modifierLineItemModel.getQuantity(),
                    (modifierLineItemModel.getAmount() == null) ? 0 : modifierLineItemModel.getAmount(),
                    modifierLineItemModel.getParentPaTransLineItemId(),
                    modifierLineItemModel.getKeypadId(),
                    (modifierLineItemModel.getPrepOption() == null) ? null : modifierLineItemModel.getPrepOption().getId(),
                    (modifierLineItemModel.getPrepOption() == null) ? null : modifierLineItemModel.getPrepOption().getPrepOptionSetId(),
                    (modifierLineItemModel.getPrepOption() == null) ? BigDecimal.ZERO : modifierLineItemModel.getPrepOption().getPrice(),
                    modifierLineItemModel.getLinkedFromPaTransLineItemModIds()
            }, logFileName, conn);

            if (transLineModId <= 0) {
                throw new MissingDataException("Product Modifier Line Item could not be recorded in QC_PATransLineItemMods", transactionModel.getTerminal().getId());
            }
            modifierLineItemModel.setId(transLineModId);
        }
    }

    public void saveProductTaxes(ProductLineItemModel productLineItemModel, JDCConnection conn, String logFileName) throws Exception {
        //Save taxes for product
        for (ItemTaxModel itemTaxModel : productLineItemModel.getTaxes()) {
            itemTaxModel.setTransLineItemId(productLineItemModel.getId());
            saveItemTaxLineItem(itemTaxModel, conn, logFileName);
        }
    }

    public void saveProductDiscounts(ProductLineItemModel productLineItemModel, JDCConnection conn, String logFileName) throws Exception {
        try {
            //Save discounts for product
            for (ItemDiscountModel discountItemModel : productLineItemModel.getDiscounts()) {
                if (discountItemModel.getTempLinkedLineItemId() != null && discountItemModel.getTempLinkedLineItemId() > 0) {
                    //Match up the discount line item with the Item Tax Discount record on the product
                    for (DiscountLineItemModel discountLineItemModel : transactionModel.getDiscounts()) {
                        if (discountLineItemModel.getTempId() != null && discountLineItemModel.getTempId() > 0) {
                            if (discountItemModel.getTempLinkedLineItemId().equals(discountLineItemModel.getTempId())) {
                                discountItemModel.setLinkedPATransLineItemId(discountLineItemModel.getId());
                            }
                        }
                    }
                }

                Integer itemDiscountId = 0;
                discountItemModel.setTransLineItemId(productLineItemModel.getId());

                Logger.logMessage("Saving discount transaction detail number " + discountItemModel.getTransLineItemId(), logFileName, Logger.LEVEL.DEBUG);
                itemDiscountId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPAItemTaxDsct", new Object[]{
                        discountItemModel.getTransLineItemId(),
                        discountItemModel.getItemTypeId(),
                        discountItemModel.getDiscount().getId(),
                        (discountItemModel.getEligibleAmount() == null) ? 0 : discountItemModel.getEligibleAmount(), //Eligible Amount
                        (discountItemModel.getAmount() == null) ? 0 : discountItemModel.getAmount(), //Amount
                        (discountItemModel.getRefundedAmount() == null) ? 0 : discountItemModel.getRefundedAmount(),
                        (discountItemModel.getRefundedAmount() == null) ? 0 : discountItemModel.getRefundedAmount(),
                        discountItemModel.getLinkedPATransLineItemId() //LinkedPATranslineItemID
                }, logFileName, conn);

                if (itemDiscountId <= 0) {
                    throw new MissingDataException("Product Discount Line Item could not be recorded in QC_PAItemTaxDsct", transactionModel.getTerminal().getId());
                }
                discountItemModel.setId(itemDiscountId);
            }
        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }
    }

    public void saveProductRewards(ProductLineItemModel productLineItemModel, JDCConnection conn, String logFileName) throws Exception {
        try {
            //Save rewards for product
            for (ItemRewardModel rewardItemModel : productLineItemModel.getRewards()) {
                Integer rewardItemId = 0;
                rewardItemModel.setTransLineItemId(productLineItemModel.getId());

                Logger.logMessage("Saving reward transaction detail number " + rewardItemModel.getId(), logFileName, Logger.LEVEL.DEBUG);
                rewardItemId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPAItemTaxDsct", new Object[]{
                        rewardItemModel.getTransLineItemId(),
                        rewardItemModel.getItemTypeId(),
                        rewardItemModel.getItemId(),
                        (rewardItemModel.getEligibleAmount() == null) ? 0 : rewardItemModel.getEligibleAmount(), //Eligible Amount
                        (rewardItemModel.getAmount() == null) ? 0 : rewardItemModel.getAmount(), //Amount
                        (rewardItemModel.getRefundedAmount() == null) ? 0 : rewardItemModel.getRefundedAmount(),
                        (rewardItemModel.getRefundedAmount() == null) ? 0 : rewardItemModel.getRefundedAmount(),
                        null //LinkedPATranslineItemID
                }, logFileName, conn);

                if (rewardItemId <= 0) {
                    throw new MissingDataException("Product Reward Line Item could not be recorded in QC_PAItemTaxDsct", transactionModel.getTerminal().getId());
                }
                rewardItemModel.setId(rewardItemId);
            }
        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }
    }

    public void saveProductSurcharges(ProductLineItemModel productLineItemModel, JDCConnection conn, String logFileName) throws Exception {
        try {

            //Save Surcharges for product
            for (ItemSurchargeModel surchargeItemModel : productLineItemModel.getSurcharges()) {
                Integer surchargeItemId = 0;
                surchargeItemModel.setTransLineItemId(productLineItemModel.getId());

                Logger.logMessage("Saving surcharge transaction detail number " + surchargeItemModel.getId(), logFileName, Logger.LEVEL.DEBUG);
                surchargeItemId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPAItemTaxDsct", new Object[]{
                        surchargeItemModel.getTransLineItemId(),
                        surchargeItemModel.getItemTypeId(),
                        surchargeItemModel.getItemId(),
                        (surchargeItemModel.getEligibleAmount() == null) ? 0 : surchargeItemModel.getEligibleAmount(), //Eligible Amount
                        (surchargeItemModel.getAmount() == null) ? 0 : surchargeItemModel.getAmount(), //Amount
                        (surchargeItemModel.getRefundedAmount() == null) ? 0 : surchargeItemModel.getRefundedAmount(),
                        (surchargeItemModel.getRefundedAmount() == null) ? 0 : surchargeItemModel.getRefundedAmount(),
                        null //LinkedPATranslineItemID
                }, logFileName, conn);

                if (surchargeItemId <= 0) {
                    throw new MissingDataException("Product Surcharge Line Item could not be recorded in QC_PAItemTaxDsct", transactionModel.getTerminal().getId());
                }
                surchargeItemModel.setId(surchargeItemId);
            }
        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }
    }

    public void updateProductComboDiscounts(ProductLineItemModel productLineItemModel, JDCConnection conn, String logFileName) throws Exception {
        try {

            if(productLineItemModel.getComboTransLineItemId() != null && productLineItemModel.getComboTransLineItemId() > 0) {

                for(ItemDiscountModel itemDiscountModel : productLineItemModel.getDiscounts()) {

                    if(!itemDiscountModel.getDiscount().isSubTotalDiscount() && itemDiscountModel.getLinkedPATransLineItemId() != null) {
                        Integer iUpdatedRows = 0;

                        Logger.logMessage("Updating Combo Discount with LinkedPATransLineItemID from Product for discount transaction detail number " + itemDiscountModel.getLinkedPATransLineItemId(), logFileName, Logger.LEVEL.DEBUG);
                        iUpdatedRows = dm.parameterizedExecuteNonQuery("data.posapi30.UpdateComboDiscountLinkedPATransLineItemId", new Object[]{
                                itemDiscountModel.getLinkedPATransLineItemId(),
                                productLineItemModel.getId()
                        }, logFileName, conn);

                        if (iUpdatedRows <= 0) {
                            throw new MissingDataException("Combo Discount Line's LinkedPATransLineItemId could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }
    }

    //endregion

    //region Save Discount Lines
    public void saveDiscountLines(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        int iPATransactionDetailId = 0;
        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
            iPATransactionDetailId = 0;

            iPATransactionDetailId = saveDiscountLineItem(discountLineItemModel, conn, logFileName, iPATransactionId);

            if (iPATransactionDetailId <= 0) {
                throw new MissingDataException("Transaction Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
            }

            discountLineItemModel.setId(iPATransactionDetailId);
            //save Discount sub-items
            this.saveDiscountTaxes(discountLineItemModel, conn, logFileName);
        }
    }

    public Integer saveDiscountLineItem(DiscountLineItemModel discountLineItemModel, JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        Integer iPATransactionDetailId = 0;
        //Save POS Discounts
        Logger.logMessage("Saving POS Discounts.  TransactionBuilder.saveDiscountLineItem.", logFileName, Logger.LEVEL.DEBUG);

        iPATransactionDetailId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItem", new Object[]{
                discountLineItemModel.getItemTypeId(),
                discountLineItemModel.getDiscount().getId(),
                (discountLineItemModel.getQuantity() == null) ? 1 : discountLineItemModel.getQuantity(),
                (discountLineItemModel.getAmount() == null) ? 0 : discountLineItemModel.getAmount(),
                (discountLineItemModel.getEligibleAmount() == null) ? 0 : discountLineItemModel.getEligibleAmount(),
                discountLineItemModel.getItemComment(),
                discountLineItemModel.getEmployeeId(),
                null, //splits
                iPATransactionId, // new Integer(iPATransId),
                null, //credit card trans info
                discountLineItemModel.getBadgeAssignmentId(),
                null, //KeypadID
                discountLineItemModel.getOriginalOrderNumber(),
                null, //Print Status ID
                discountLineItemModel.getLinkedPATransLineItemId(),
                null, //PAComboTransLineItemID
                null, //BasePrice
                null, //ComboPrice
                null, //PAComboDetailID
                null, //PAPrepOptionID
                null, //PAPrepOptionSetID
                BigDecimal.ZERO,  //PAPrepOptionPrice
                null //LinkedFromPATransLineItemIDs
        }, logFileName, conn);

        if (iPATransactionDetailId <= 0) {
            throw new MissingDataException("Discount Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
        }

        discountLineItemModel.setId(iPATransactionDetailId);
        return iPATransactionDetailId;
    }

    public void saveDiscountTaxes(DiscountLineItemModel discountLineItemModel, JDCConnection conn, String logFileName) throws Exception {
        //Save taxes for product
        for (ItemTaxModel itemTaxModel : discountLineItemModel.getTaxes()) {
            itemTaxModel.setTransLineItemId(discountLineItemModel.getId());
            saveItemTaxLineItem(itemTaxModel, conn, logFileName);
        }
    }

    public void saveSurchargeTaxes(SurchargeLineItemModel surchargeLineItemModel, JDCConnection conn, String logFileName) throws Exception {
        //Save taxes for product
        for (ItemTaxModel itemTaxModel : surchargeLineItemModel.getTaxes()) {
            itemTaxModel.setTransLineItemId(surchargeLineItemModel.getId());
            saveItemTaxLineItem(itemTaxModel, conn, logFileName);
        }
    }
    //endregion

    public void saveItemTaxLineItem(ItemTaxModel itemTaxModel, JDCConnection conn, String logFileName) throws Exception {
        Integer itemTaxId = 0;

        Logger.logMessage("Saving tax for transaction detail number " + itemTaxModel.getTransLineItemId(), logFileName, Logger.LEVEL.DEBUG);
        itemTaxId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPAItemTaxDsct", new Object[]{
                itemTaxModel.getTransLineItemId(),
                itemTaxModel.getItemTypeId(),
                itemTaxModel.getTax().getId(),
                (itemTaxModel.getEligibleAmount() == null) ? 0 : itemTaxModel.getEligibleAmount(), //EligibleAmount,
                (itemTaxModel.getAmount() == null) ? 0 : itemTaxModel.getAmount(), //Amount,
                (itemTaxModel.getRefundedAmount() == null) ? 0 : itemTaxModel.getRefundedAmount(),
                (itemTaxModel.getRefundedAmount() == null) ? 0 : itemTaxModel.getRefundedAmount(),
                null //LinkedPATranslineItemID
        }, logFileName, conn);

        if (itemTaxId <= 0) {
            throw new MissingDataException("Product Tax Line Item could not be recorded in QC_PAItemTaxDsct", transactionModel.getTerminal().getId());
        }
        itemTaxModel.setId(itemTaxId);
    }

    public void saveTaxLines(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        Integer saveResult = 0;

        Logger.logMessage("Saving taxes.  TransactionBuilder.saveTaxLines.", logFileName, Logger.LEVEL.DEBUG);
        for (TaxLineItemModel taxLineItemModel : this.getTransactionModel().getTaxes()) {

            saveResult = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItem", new Object[]{
                    taxLineItemModel.getItemTypeId(),
                    taxLineItemModel.getTax().getId(),
                    (taxLineItemModel.getQuantity() == null) ? 1 : taxLineItemModel.getQuantity(), //new Double(quantitySign * Double.parseDouble(item.get(this.DETAIL_2_QUANTITY).toString())),
                    (taxLineItemModel.getAmount() == null) ? 0 : taxLineItemModel.getAmount(),
                    (taxLineItemModel.getEligibleAmount() == null ? 0 : taxLineItemModel.getEligibleAmount()), //Eligible Amount
                    taxLineItemModel.getItemComment(), //item.get(this.DETAIL_5_COMMENT).toString(),
                    taxLineItemModel.getEmployeeId(), // (((item.size() <= this.DETAIL_6_EMPLOYEEID) || (item.get(this.DETAIL_6_EMPLOYEEID).toString().compareToIgnoreCase("null") == 0)) ? null : new Integer(item.get(this.DETAIL_6_EMPLOYEEID).toString())),
                    null, //splits
                    iPATransactionId, // new Integer(iPATransId),
                    null, //credit card trans info
                    taxLineItemModel.getBadgeAssignmentId(), //badgeAssignmentId(),
                    null, //KeypadID
                    taxLineItemModel.getOriginalOrderNumber(),
                    null, //printStatusId
                    taxLineItemModel.getLinkedTransLineItemId(),
                    null, //PAComboTransLineItemID
                    null, //BasePrice
                    null, //ComboPrice
                    null,  //PAComboDetailID
                    null, //PAPrepOptionID
                    null, //PAPrepOptionSetID
                    BigDecimal.ZERO,  //PAPrepOptionPrice
                    null //LinkedFromPATransLineItemIDs
            }, logFileName, conn);

            if (saveResult <= 0) {
                throw new MissingDataException("Tax Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
            }
            taxLineItemModel.setId(saveResult);
        }
    }

    public void saveTenderLines(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        Logger.logMessage("Saving tenders.  TransactionBuilder.saveTenderLines.", logFileName, Logger.LEVEL.DEBUG);
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {

            Integer tenderLineItemId = 0;

            tenderLineItemId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItemTender", new Object[]{
                    tenderLineItemModel.getItemTypeId(),
                    tenderLineItemModel.getTender().getId(),
                    (tenderLineItemModel.getQuantity() == null) ? 1 : tenderLineItemModel.getQuantity(), //new Double(quantitySign * Double.parseDouble(item.get(this.DETAIL_2_QUANTITY).toString())),
                    tenderLineItemModel.getAmount(), //new Double(amountSign * Double.parseDouble(item.get(this.DETAIL_3_UNITPRICE).toString())),
                    (tenderLineItemModel.getEligibleAmount() == null) ? 0 : tenderLineItemModel.getEligibleAmount(), // Eligible Amount
                    tenderLineItemModel.getItemComment(), //Item Comment for tender line item
                    (tenderLineItemModel.getAccount() != null && tenderLineItemModel.getAccount().getId() != null && !tenderLineItemModel.getAccount().getId().toString().isEmpty()) ? tenderLineItemModel.getAccount().getId() : null,
                    tenderLineItemModel.getPayments(),
                    iPATransactionId, // new Integer(iPATransId),
                    tenderLineItemModel.getCreditCardTransInfo(), //creditCardTransInfo
                    tenderLineItemModel.getBadgeAssignmentId(), //badgeAssignmentId
                    tenderLineItemModel.getPaymentMethodTypeId(),
                    tenderLineItemModel.getCcAuthorizationTypeId(),
                    tenderLineItemModel.getOriginalOrderNumber(),
                    (tenderLineItemModel.getVoucherCode() == null) ? null : tenderLineItemModel.getVoucherCode().getId()
            }, logFileName, conn);

            if (tenderLineItemId <= 0) {
                throw new MissingDataException("Tender Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
            }
            tenderLineItemModel.setId(tenderLineItemId);

            //Save Trans SAF Info
            this.saveTransSafInfo(conn, logFileName, tenderLineItemModel);

            //Update Voucher code
            this.updateVoucherCode(conn, logFileName, tenderLineItemModel, this.getTransactionModel().getTimeStamp() );
        }
    }

    public void savePaidOuts(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        Integer paidOutId = 0;

        //Save Paid Outs
        Logger.logMessage("Saving Paid Outs.  TransactionBuilder.savePaidOuts.", logFileName, Logger.LEVEL.DEBUG);
        for (PaidOutLineItemModel paidOutLineItemModel : this.getTransactionModel().getPaidOuts()) {
            paidOutId = 0;

            paidOutId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItem", new Object[]{
                    paidOutLineItemModel.getItemTypeId(),
                    paidOutLineItemModel.getPaidOut().getId(),
                    (paidOutLineItemModel.getQuantity() == null) ? 1 : paidOutLineItemModel.getQuantity(),
                    (paidOutLineItemModel.getAmount() == null) ? 0 : paidOutLineItemModel.getAmount(), //Amount
                    (paidOutLineItemModel.getEligibleAmount() == null) ? 0 : paidOutLineItemModel.getEligibleAmount(), //Eligible Amount
                    paidOutLineItemModel.getItemComment(),
                    paidOutLineItemModel.getEmployeeId(),
                    null, //splits
                    iPATransactionId,
                    (paidOutLineItemModel.getCreditCardTransInfo().isEmpty()) ? null : paidOutLineItemModel.getCreditCardTransInfo(), //creditCardTransInfo
                    paidOutLineItemModel.getBadgeAssignmentId(), //badgeAssignmentId
                    null, //KeypadID
                    paidOutLineItemModel.getOriginalOrderNumber(),
                    null, //PrintStatusId
                    paidOutLineItemModel.getLinkedTransLineItemId(),
                    null, //PAComboTransLineItemID
                    null, //BasePrice
                    null, //ComboPrice
                    null,  //PAComboDetailID
                    null, //PAPrepOptionID
                    null, //PAPrepOptionSetID
                    BigDecimal.ZERO,  //PAPrepOptionPrice
                    null //LinkedFromPATransLineItemIDs
            }, logFileName, conn);

            if (paidOutId <= 0) {
                throw new MissingDataException("Paid Out Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
            }
            paidOutLineItemModel.setId(paidOutId);
        }
    }

    public void saveReceivedOnAccounts(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        Integer receivedOnAccountId = 0;

        //Save Received On Accounts
        Logger.logMessage("Saving Received On Accounts.  TransactionBuilder.saveReceivedOnAccounts.", logFileName, Logger.LEVEL.DEBUG);
        for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : this.getTransactionModel().getReceivedOnAccounts()) {
            receivedOnAccountId = 0;
            receivedOnAccountId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItem", new Object[]{
                    receivedOnAccountLineItemModel.getItemTypeId(),
                    receivedOnAccountLineItemModel.getReceivedOnAccount().getId(),
                    (receivedOnAccountLineItemModel.getQuantity() == null) ? 1 : receivedOnAccountLineItemModel.getQuantity(),
                    (receivedOnAccountLineItemModel.getAmount() == null) ? 0 : receivedOnAccountLineItemModel.getAmount(),
                    (receivedOnAccountLineItemModel.getEligibleAmount() == null ? 0 : receivedOnAccountLineItemModel.getEligibleAmount()), //Eligible Amount
                    receivedOnAccountLineItemModel.getItemComment(),
                    //receivedOnAccountLineItemModel.getEmployeeId(),
                    (receivedOnAccountLineItemModel.getAccount() != null && receivedOnAccountLineItemModel.getAccount().getId() != null && !receivedOnAccountLineItemModel.getAccount().getId().toString().isEmpty()) ? receivedOnAccountLineItemModel.getAccount().getId() : null,
                    null,  //splits
                    iPATransactionId, // new Integer(iPATransId),
                    (receivedOnAccountLineItemModel.getCreditCardTransInfo().isEmpty()) ? null : receivedOnAccountLineItemModel.getCreditCardTransInfo(), //creditCardTransInfo
                    receivedOnAccountLineItemModel.getBadgeAssignmentId(), //badgeAssignmentId
                    null, //KeypadID
                    receivedOnAccountLineItemModel.getOriginalOrderNumber(),
                    null, //printStatusId
                    receivedOnAccountLineItemModel.getLinkedTransLineItemId(),
                    null, //PAComboTransLineItemID
                    null, //BasePrice
                    null, //ComboPrice
                    null,  //PAComboDetailID
                    null, //PAPrepOptionID
                    null, //PAPrepOptionSetID
                    BigDecimal.ZERO,  //PAPrepOptionPrice
                    null //LinkedFromPATransLineItemIDs
            }, logFileName, conn);

            if (receivedOnAccountId <= 0) {
                throw new MissingDataException("Received On Account Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
            }
            receivedOnAccountLineItemModel.setId(receivedOnAccountId);
        }
    }

    public void saveLoyaltyRewards(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        //save submitted Loyalty Rewards only if there are rewards
        if (this.getTransactionModel().getRewards() != null && !this.getTransactionModel().getRewards().isEmpty()) {
            Integer iPATransLineItemId = null;

            for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()) {
                iPATransLineItemId = null;

                //save the line item
                iPATransLineItemId = this.saveLoyaltyRewardLineItem(loyaltyRewardLineItemModel, conn, logFileName, iPATransactionId);

                //save one record in the LoyaltyAccountPoint per Reward
                //this.saveLoyaltyAccountPoints(conn, logFileName, iPATransactionId, iPATransLineItemId);

                loyaltyRewardLineItemModel.setId(iPATransLineItemId);

                saveRewardItemTaxDisc(loyaltyRewardLineItemModel, conn, logFileName, iPATransLineItemId);
            }
        }
    }

    public void saveServiceChargeLines(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        Logger.logMessage("Saving Service Charges.  TransactionBuilder.saveServiceChargeLines.", logFileName, Logger.LEVEL.DEBUG);
        for (ServiceChargeLineItemModel serviceChargeLineItemModel : this.getTransactionModel().getServiceCharges()) {
            Integer serviceChargeLineItemId = 0;

            serviceChargeLineItemId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItem", new Object[]{
                    serviceChargeLineItemModel.getItemTypeId(),
                    serviceChargeLineItemModel.getServiceCharge().getId(),
                    (serviceChargeLineItemModel.getQuantity() == null) ? 1 : serviceChargeLineItemModel.getQuantity(),
                    (serviceChargeLineItemModel.getAmount() == null) ? 0 : serviceChargeLineItemModel.getAmount(),
                    (serviceChargeLineItemModel.getEligibleAmount() == null) ? 0 : serviceChargeLineItemModel.getEligibleAmount(), //Eligible Amount
                    serviceChargeLineItemModel.getItemComment(),
                    serviceChargeLineItemModel.getEmployeeId(),
                    null, //splits
                    iPATransactionId, // new Integer(iPATransId),
                    null, //credit card info
                    serviceChargeLineItemModel.getBadgeAssignmentId(), //badgeAssignmentId
                    null, //KeypadID
                    serviceChargeLineItemModel.getOriginalOrderNumber(),
                    null, //printStatusId
                    serviceChargeLineItemModel.getLinkedTransLineItemId(),
                    null, //PAComboTransLineItemID
                    null, //BasePrice
                    null, //ComboPrice
                    null,  //PAComboDetailID
                    null, //PAPrepOptionID
                    null, //PAPrepOptionSetID
                    BigDecimal.ZERO,  //PAPrepOptionPrice
                    null //LinkedFromPATransLineItemIDs
            }, logFileName, conn);

            if (serviceChargeLineItemId <= 0) {
                throw new MissingDataException("Service Charge Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
            }
            serviceChargeLineItemModel.setId(serviceChargeLineItemId);
        }
    }

    public void saveGratuityLines(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        Logger.logMessage("Saving Gratuities.  TransactionBuilder.saveGratuityLines.", logFileName, Logger.LEVEL.DEBUG);
        for (GratuityLineItemModel gratuityLineItemModel : this.getTransactionModel().getGratuities()) {
            Integer gratuityLineItemModelItemId = 0;

            gratuityLineItemModelItemId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItem", new Object[]{
                    gratuityLineItemModel.getItemTypeId(),
                    gratuityLineItemModel.getGratuity().getId(),
                    (gratuityLineItemModel.getQuantity() == null) ? 1 : gratuityLineItemModel.getQuantity(),
                    (gratuityLineItemModel.getAmount() == null) ? 0 : gratuityLineItemModel.getAmount(),
                    (gratuityLineItemModel.getEligibleAmount() == null) ? 0 : gratuityLineItemModel.getEligibleAmount(), //Eligible Amount
                    gratuityLineItemModel.getItemComment(),
                    gratuityLineItemModel.getEmployeeId(),
                    null, //splits
                    iPATransactionId, // new Integer(iPATransId),
                    null, //credit card info
                    gratuityLineItemModel.getBadgeAssignmentId(), //badgeAssignmentId
                    null, //KeypadID
                    gratuityLineItemModel.getOriginalOrderNumber(),
                    null, //PrintStatusId
                    gratuityLineItemModel.getLinkedTransLineItemId(),
                    null, //PAComboTransLineItemID
                    null, //BasePrice
                    null, //ComboPrice
                    null,  //PAComboDetailID
                    null, //PAPrepOptionID
                    null, //PAPrepOptionSetID
                    BigDecimal.ZERO,  //PAPrepOptionPrice
                    null //LinkedFromPATransLineItemIDs
            }, logFileName, conn);

            if (gratuityLineItemModelItemId <= 0) {
                throw new MissingDataException("Gratuity Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
            }
            gratuityLineItemModel.setId(gratuityLineItemModelItemId);
        }
    }

    public void saveComboLines(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        Logger.logMessage("Saving Combos.  TransactionBuilder.saveComboLines.", logFileName, Logger.LEVEL.DEBUG);

        int iPATransactionDetailId = 0;
        for (ComboLineItemModel comboLineItemModel : this.getTransactionModel().getCombos()) {
            iPATransactionDetailId = 0;

            iPATransactionDetailId = this.saveComboLineItem(comboLineItemModel, conn, logFileName, iPATransactionId);
            comboLineItemModel.setId(iPATransactionDetailId);

            if (comboLineItemModel.getId() <= 0) {
                throw new MissingDataException("Combo Transaction Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
            }
        }
    }

    protected Integer saveComboLineItem(ComboLineItemModel comboLineItemModel, JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        DataManager dm = new DataManager();

        Integer iPATransactionDetailId = 0;
        iPATransactionDetailId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItem", new Object[]{
                comboLineItemModel.getItemTypeId(),
                comboLineItemModel.getCombo().getId(),
                (comboLineItemModel.getQuantity() == null) ? 1 : comboLineItemModel.getQuantity(),
                (comboLineItemModel.getAmount() == null) ? 0 : comboLineItemModel.getAmount(),
                (comboLineItemModel.getEligibleAmount() == null) ? 0 : comboLineItemModel.getEligibleAmount(), //Eligible Amount
                comboLineItemModel.getItemComment(),
                comboLineItemModel.getEmployeeId(),
                null, //splits
                iPATransactionId, // new Integer(iPATransId),
                null, //credit card info
                comboLineItemModel.getBadgeAssignmentId(), //badgeAssignmentId
                null,
                comboLineItemModel.getOriginalOrderNumber(),
                null,
                null,
                null,
                null,
                null,
                null,
                null, //PAPrepOptionID
                null, //PAPrepOptionSetID
                BigDecimal.ZERO,  //PAPrepOptionPrice
                null //LinkedFromPATransLineItemIDs

        }, logFileName, conn);

        if (iPATransactionDetailId <= 0) {
            throw new MissingDataException("Combo Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
        }
        comboLineItemModel.setId(iPATransactionDetailId);

        this.updateProductLinesForCombos(comboLineItemModel, iPATransactionDetailId);

        return iPATransactionDetailId;
    }

    public void saveTransSafInfo(JDCConnection conn, String logFileName, TenderLineItemModel tenderLineItemModel) throws Exception {

        if (!this.getTransactionModel().isCancelTransactionType() &&
                !this.getTransactionModel().isTrainingTransactionType() &&
                !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {

            if (!this.getTransactionModel().isOfflineMode()) { //Terminal must be online
                if (tenderLineItemModel.getTender().getTenderTypeId().equals(PosAPIHelper.TenderType.CREDIT_CARD.toInt())) {   //tender must be credit card

                    switch (this.getTransactionModel().getTransactionTypeEnum()) {
                        case SALE:
                        case REFUND:
                        case VOID:
                        case ROA:
                        case OPEN:

                            if (tenderLineItemModel.getCcAuthorizationTypeId() != null && !tenderLineItemModel.getCcAuthorizationTypeId().toString().isEmpty() &&
                                    tenderLineItemModel.getCcAuthorizationTypeId().equals(PosAPIHelper.CreditCardAuthType.APPROVED_OFFLINE.toInt())) { //tender must be approve offline

                                Logger.logMessage("Saving Trans SAF Info.  TransactionBuilder.saveTransSafInfo.", logFileName, Logger.LEVEL.DEBUG);

                                DataManager dm = new DataManager();
                                Integer transSafInfoResult = dm.parameterizedExecuteNonQuery("data.posapi30.InsertTransSAFInfo", new Object[]{
                                        tenderLineItemModel.getId(),
                                        this.getTransactionModel().getTerminal().getId(),
                                        null, //OldCCTransInfo
                                        tenderLineItemModel.getSafTransInfo()

                                }, logFileName, conn);

                                if (transSafInfoResult <= 0) {
                                    throw new MissingDataException("Trans SAF Info could not be recorded in QC_PATransSAFInfo", transactionModel.getTerminal().getId());
                                }
                            }
                            break;
                    }
                }
            }
        }
    }

    public void updateVoucherCode(JDCConnection conn, String logFileName, TenderLineItemModel tenderLineItemModel, String txnDateTime) throws Exception {

        if (tenderLineItemModel.getVoucherCode() != null
                && tenderLineItemModel.getVoucherCode().getCode() != null
                && !tenderLineItemModel.getVoucherCode().getCode().isEmpty()
        ) {

            if (!this.getTransactionModel().isCancelTransactionType() &&
                    !this.getTransactionModel().isTrainingTransactionType() &&
                    !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {

                 switch (this.getTransactionModel().getTransactionTypeEnum()) {
                    case SALE:
                    case REFUND:
                        tenderLineItemModel.getVoucherCode().setStatusId(PosAPIHelper.VoucherStatus.REDEEMED.toInt());
                        break;
                    case VOID:
                        tenderLineItemModel.getVoucherCode().setStatusId(PosAPIHelper.VoucherStatus.AVAILABLE.toInt());
                        break;
                    default:
                        //TODO - should this error our if we get to this point.  This should get caught in a validation method
                        throw new MissingDataException("Voucher submitted on invalid transaction type.");
                }

                switch (this.getTransactionModel().getTransactionTypeEnum()) {
                    case SALE:
                    case REFUND:
                    case VOID:
                        Logger.logMessage("Updating Voucher Code.  TransactionBuilder.updateVoucherCode.", logFileName, Logger.LEVEL.DEBUG);

                        DataManager dm = new DataManager();
                        Integer iResult = dm.parameterizedExecuteNonQuery("data.posapi30.updateVoucherCode", new Object[]{
                            tenderLineItemModel.getVoucherCode().getStatusId(),
                            tenderLineItemModel.getVoucherCode().getId(),
                            txnDateTime
                        }, logFileName, conn);

                        if (iResult <= 0) {
                            throw new MissingDataException("Voucher Code could not be updated in QC_PAVoucherCode", transactionModel.getTerminal().getId());
                        }
                        break;
                }
            }
        }
    }

    public void saveSurchargeLines(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {

        Logger.logMessage("Saving Surcharges.  TransactionBuilder.saveSurchargeLines.", logFileName, Logger.LEVEL.DEBUG);
        for (SurchargeLineItemModel surchargeLineItemModel : this.getTransactionModel().getSurcharges()) {

            //Set the Trans Line Item ID of the product on the Surcharge Line Item
            if (surchargeLineItemModel.getSurcharge().getApplicationTypeId().equals(PosAPIHelper.SurchargeApplicationType.ITEM.toInt())) {
                for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                    if (productLineItemModel.getTempId() != null && !productLineItemModel.getTempId().toString().isEmpty()){
                        if (productLineItemModel.getTempId().compareTo(surchargeLineItemModel.getLinkedTransLineItemId()) == 0){
                            surchargeLineItemModel.setLinkedTransLineItemId(productLineItemModel.getId()); //now that the product line item id is filled in, save that value
                        }
                    }
                }
            }

            Integer surchargeLineItemModelItemId = 0;

            surchargeLineItemModelItemId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItem", new Object[]{
                    surchargeLineItemModel.getItemTypeId(),
                    surchargeLineItemModel.getSurcharge().getId(),
                    (surchargeLineItemModel.getQuantity() == null) ? 1 : surchargeLineItemModel.getQuantity(),
                    (surchargeLineItemModel.getAmount() == null) ? 0 : surchargeLineItemModel.getAmount(),
                    (surchargeLineItemModel.getEligibleAmount() == null) ? 0 : surchargeLineItemModel.getEligibleAmount(), //Eligible Amount
                    surchargeLineItemModel.getItemComment(),
                    surchargeLineItemModel.getEmployeeId(),
                    null, //splits
                    iPATransactionId, // new Integer(iPATransId),
                    null, //credit card info
                    surchargeLineItemModel.getBadgeAssignmentId(), //badgeAssignmentId
                    null, //KeypadId
                    surchargeLineItemModel.getOriginalOrderNumber(),
                    null, //PrintStatusId
                    surchargeLineItemModel.getLinkedTransLineItemId(),
                    null, //PAComboTransLineItemID
                    null, //BasePrice
                    null, //ComboPrice
                    null,  //PAComboDetailID
                    null, //PAPrepOptionID
                    null, //PAPrepOptionSetID
                    BigDecimal.ZERO,  //PAPrepOptionPrice
                    null //LinkedFromPATransLineItemIDs

            }, logFileName, conn);

            if (surchargeLineItemModelItemId <= 0) {
                throw new MissingDataException("Surcharge Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
            }
            surchargeLineItemModel.setId(surchargeLineItemModelItemId);

            this.saveSurchargeTaxes(surchargeLineItemModel, conn, logFileName);
        }
    }

    public void saveDonationLineItems(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {

        Logger.logMessage("Saving Donations.  TransactionBuilder.saveDonationLines.", logFileName, Logger.LEVEL.DEBUG);
        for (DonationLineItemModel donationLineItemModel : this.getTransactionModel().getDonations()) {

            Integer donationLineItemModelItemId = 0;

            donationLineItemModelItemId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItem", new Object[]{
                    donationLineItemModel.getItemTypeId(),
                    donationLineItemModel.getDonation().getId(),
                    (donationLineItemModel.getQuantity() == null) ? 1 : donationLineItemModel.getQuantity(),
                    (donationLineItemModel.getAmount() == null) ? 0 : donationLineItemModel.getAmount(),
                    (donationLineItemModel.getEligibleAmount() == null) ? 0 : donationLineItemModel.getEligibleAmount(), //Eligible Amount
                    donationLineItemModel.getItemComment(),
                    donationLineItemModel.getEmployeeId(),
                    null, //splits
                    iPATransactionId, // new Integer(iPATransId),
                    null, //credit card info
                    donationLineItemModel.getBadgeAssignmentId(), //badgeAssignmentId
                    null, //KeypadId
                    donationLineItemModel.getOriginalOrderNumber(),
                    null, //PrintStatusId
                    donationLineItemModel.getLinkedTransLineItemId(),
                    null, //PAComboTransLineItemID
                    null, //BasePrice
                    null, //ComboPrice
                    null,  //PAComboDetailID
                    null, //PAPrepOptionID
                    null, //PAPrepOptionSetID
                    BigDecimal.ZERO,  //PAPrepOptionPrice
                    null //LinkedFromPATransLineItemIDs

            }, logFileName, conn);

            if (donationLineItemModelItemId <= 0) {
                throw new MissingDataException("Donation Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
            }
            donationLineItemModel.setId(donationLineItemModelItemId);

            // If a loyalty point model exists, set donation paTransLineItemId for
            if(donationLineItemModel.getLoyaltyPoint() != null) {
                donationLineItemModel.getLoyaltyPoint().setId(donationLineItemModelItemId);
            }
        }
    }

    public void saveCashierShiftEnd(JDCConnection conn, String logFileName) throws Exception {
        if (this.getTransactionModel().getApiActionTypeEnum().equals(PosAPIHelper.ApiActionType.CASHIERSHIFTEND)) {
            if (this.getCashierShiftEndModel() != null) {
                DataManager dm = new DataManager();
                Logger.logMessage("Saving Cashier Shift End. TransactionBuilder.saveCashierShiftEnd.", logFileName, Logger.LEVEL.DEBUG);

                Integer insertedRows = 0;
                insertedRows = dm.parameterizedExecuteNonQuery("data.posapi30.InsertCashierShiftEnd", new Object[]{
                        this.getCashierShiftEndModel().getTransactionId(),
                        this.getCashierShiftEndModel().getGrossSales(),
                        this.getCashierShiftEndModel().getGrossRefundVoids(),
                        this.getCashierShiftEndModel().getCustomerCount(),
                        this.getCashierShiftEndModel().getVoidModeQty(),
                        this.getCashierShiftEndModel().getVoidModeAmt(),
                        this.getCashierShiftEndModel().getRefundModeQty(),
                        this.getCashierShiftEndModel().getRefundModeAmt(),
                        this.getCashierShiftEndModel().getCancelTransQty(),
                        this.getCashierShiftEndModel().getCancelTransAmt(),
                        this.getCashierShiftEndModel().getNoSalesQty()
                }, logFileName, conn);

                if (insertedRows <= 0) {
                    throw new MissingDataException("Cashier Shift End could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
                }
            }
        }
    }

    public void saveProductInventory(JDCConnection conn, String logFileName) throws Exception {
        if (!this.getTransactionModel().isOfflineMode() && this.getTransactionModel().getPosType() != PosAPIHelper.PosType.MY_QC_FUNDING &&
                !this.getTransactionModel().getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.POSANYWHERE.toInt())) {
            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:
                case VOID:
                case REFUND:
                case REFUND_MANUAL:

                    Integer updatedInventory = 0;
                    Integer insertedRecords = 0;
                    Integer insertedRevCenterInv = 0;

                    Logger.logMessage("Saving Location Inventory.  TransactionBuilder.saveProductInventory.", logFileName, Logger.LEVEL.DEBUG);

                    if (this.getTransactionModel().getProducts() != null && this.getTransactionModel().getProducts().size() > 0) {

                        for (ProductLineItemModel productLineItem : this.getTransactionModel().getProducts()) {

                            //make sure the plu is not null
                            if (productLineItem.getProduct() != null && productLineItem.getProduct().getId() != null && !productLineItem.getProduct().getId().toString().isEmpty() &&
                                    productLineItem.getProduct().isInventoryItem()) {

                                updatedInventory = 0;
                                insertedRecords = 0;
                                insertedRevCenterInv = 0;

                                updatedInventory = dm.parameterizedExecuteNonQuery("data.posapi30.UpdatePLUInventoryCurrent.NewInv", new Object[]{
                                        productLineItem.getProduct().getId(),
                                        productLineItem.getQuantity(),
                                        this.getTransactionModel().getTerminal().getId(),
                                }, logFileName, conn);

                                //if update fails, try the insert
                                if (updatedInventory <= 0) {
                                    BigDecimal initialQuantity = new BigDecimal(0).subtract(productLineItem.getQuantity());

                                    insertedRecords = dm.parameterizedExecuteNonQuery("data.posapi30.LocationInvInsert", new Object[]{
                                            this.getTransactionModel().getTerminal().getId(),
                                            productLineItem.getProduct().getId(),
                                            initialQuantity

                                    }, logFileName, conn);

                                    if (insertedRecords <= 0) {
                                        throw new MissingDataException("Error inserting Inventory Records", transactionModel.getTerminal().getId());
                                    }
                                }

                                // Also check the QC_RevenueCenterInv for a record for this product
                                ArrayList revCenterInvCheck = dm.serializeSqlWithColNames("data.posapi30.CheckRevenueInvTbl", new Object[]{productLineItem.getProduct().getId(), new Integer(this.getTransactionModel().getTerminal().getId())}, conn, logFileName);

                                if (revCenterInvCheck.size() == 0) {
                                    // This means there's no record for this product for this revenue center!
                                    Logger.logMessage("TransactionBuilder.saveProductInventory: no record found for pluID " + productLineItem.getProduct().getId() + " and the rev center for terminal ID " + this.getTransactionModel().getTerminal().getId() + "!", logFileName, Logger.LEVEL.TRACE);

                                    insertedRevCenterInv = dm.parameterizedExecuteNonQuery("data.posapi30.InsRevenueCenterInv", new Object[]{
                                            this.getTransactionModel().getTerminal().getId(),
                                            productLineItem.getProduct().getId(),
                                    }, logFileName, conn);

                                    if (insertedRevCenterInv <= 0) {
                                        throw new MissingDataException("Error inserting Revenue Center Inventory Record", transactionModel.getTerminal().getId());
                                    }
                                }
                            }
                        }
                    }


                    break;
                default:
                    //don't update inventory for the other types
                    break;
            }
        }
    }

    //updates the DiscountLineItem linkedPATransLineItemID with the corresponding productLineItem's paTransLineItemID
    public void updateDiscountLinesForCombos(ProductLineItemModel productLineItemModel, Integer iPATransactionDetailId) throws Exception {

        for(DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {

            //the corresponding discount line with originally be set the product's comboDetailID, update it to the paTransLineItemID
            if(discountLineItemModel.getLinkedPATransLineItemId() != null && discountLineItemModel.getLinkedPATransLineItemId().equals(productLineItemModel.getComboDetailId())) {

                discountLineItemModel.setLinkedPATransLineItemId(iPATransactionDetailId);
                break;
            }
        }
    }

    //updates the ProductLineItem comboTransLineItemID with the corresponding combo's paTransLineItemID
    public void updateProductLinesForCombos(ComboLineItemModel comboLineItemModel, Integer iPATransactionDetailId) throws Exception {
        switch (this.getTransactionModel().getPosType()){
            case MMHAYES_POS:
            case THIRD_PARTY:
                for(ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                    if (comboLineItemModel.getTempId() != null && productLineItemModel.getComboTransLineItemId() != null
                            && comboLineItemModel.getTempId().equals(productLineItemModel.getComboTransLineItemId())){
                        //set product's comboTransLineItemID as the combo's paTransLineItemID
                        productLineItemModel.setComboTransLineItemId(iPATransactionDetailId);
                    }
                }
                break;
            default:
                for(ComboDetailModel comboDetailModel : comboLineItemModel.getCombo().getDetails()) {
                    for(ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
                        //update all product lines where the comboTransLineItemID matches the combo's id
                        if(productLineItemModel.getComboTransLineItemId() != null && productLineItemModel.getComboTransLineItemId() < 0 &&  productLineItemModel.getComboDetailId().equals(comboDetailModel.getId())) {
                            //set product's comboTransLineItemID as the combo's paTransLineItemID
                            productLineItemModel.setComboTransLineItemId(iPATransactionDetailId);
                            break;
                        }
                    }
                }
                break;
        }
    }

    public void updateEmployeeBalance(JDCConnection conn, String logFileName) throws Exception {
        if (!this.getTransactionModel().isOfflineMode()) {
            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:
                case VOID:
                case REFUND:
                case REFUND_MANUAL:
                case ROA:

                    if (this.getTransactionModel().hasQuickChargeTender()) {
                        //save balances in a loop to catch split tenders
                        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
                            if (tenderLineItemModel.getTender() != null && tenderLineItemModel.getTender().getTenderTypeId() != null) {

                                PosAPIHelper.TenderType tenderTypeEnum = PosAPIHelper.TenderType.intToEnum(tenderLineItemModel.getTender().getTenderTypeId());
                                switch (tenderTypeEnum) {
                                    case QUICKCHARGE:
                                        Integer result = 0;

                                        //Save Employee balance
                                        Logger.logMessage("Saving Employee Balance.  TransactionBuilder.updateEmployeeBalance.", logFileName, Logger.LEVEL.DEBUG);
                                        result = dm.parameterizedExecuteNonQuery("data.posapi30.UpdateOrInsertEmployeeBalance", new Object[]{
                                                tenderLineItemModel.getAccount().getId(),
                                                this.getTransactionModel().getTerminal().getId(),
                                                tenderLineItemModel.getAmount()

                                        }, logFileName, conn);

                                        if (result <= 0) {
                                            throw new MissingDataException("Saving Employee Balance could not be recorded in QC_EmployeeDetail", transactionModel.getTerminal().getId());
                                        }

                                        break;
                                }
                            }
                        }
                    }
                    break;
                default:
                    //don't update inventory for the other types
                    break;
            }
        }
    }

    public void savePrinterQueueHeaderRecord(JDCConnection conn, String logFileName) throws Exception
    {
        if (transactionModel.getOrderType() != null && !transactionModel.getOrderType().isEmpty()) {
            int orderTypeId = PosAPIHelper.OrderType.convertStringToInt(transactionModel.getOrderType());
            int pickupOrderTypeId = PosAPIHelper.OrderType.convertStringToInt(PosAPIHelper.OrderType.PICKUPSALE.getName());
            int deliveryOrderTypeId = PosAPIHelper.OrderType.convertStringToInt(PosAPIHelper.OrderType.DELIVERYSALE.getName());
            boolean isKioskPosType = transactionModel.getPosType().equals(PosAPIHelper.PosType.KIOSK_ORDERING);
            boolean isPOSAnywhereTerminalModelType = this.getTransactionModel().getTerminal().getTerminalModelId().equals(PosAPIHelper.PosTerminalModel.POSANYWHERE.toInt());

            // We want QC_PAPrinterQueue records written if:
            //      - The order type is Pickup or Delivery
            //      - The transaction did NOT come from a kiosk
            //      - The transaction did NOT come from a POS Anywhere terminal
            if ( (orderTypeId == pickupOrderTypeId || orderTypeId == deliveryOrderTypeId) && !isKioskPosType && !isPOSAnywhereTerminalModelType) {
                Integer onlineOrder = 1;
                Date transDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(transactionModel.getTimeStamp());

                int result = CommonAPI.insertPrinterQueueHeader(conn, logFileName, transactionModel.getId(), onlineOrder, transDate, transactionModel.getTerminal().getId(), PosAPIHelper.OrderType.convertStringToInt(transactionModel.getOrderType()), transactionModel.getName(), transactionModel.getPhone(), transactionModel.getTransComment(), transactionModel.getPickUpDeliveryNote(), transactionModel.getEstimatedOrderTime(), transactionModel.getOrderNumber(), transactionModel.getTransactionTypeId());

                if (result <= 0) {
                    throw new Exception("Error Adding Printer Header Queue for online orders");
                }
            }
        }
    }

    public void updateOpenTransaction() throws Exception {

        DataManager dm = new DataManager();
        String logFileName = PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId());

        Logger.logMessage("Updating Open Transaction. TransactionBuilder.updateOpenTransaction.", logFileName, Logger.LEVEL.DEBUG);

        String lastUpdatedDate = getCurrentDate();

        if (this.getTransactionModel().isOfflineMode()){
            //if offline, try and fetch the transaction if the QcImportStatus is 1 (Already Synced) or 3 (Sync in Progress), if found, throw error
            ArrayList<HashMap> transactionList = dm.parameterizedExecuteQuery("data.posapi.getSyncedLocalTransaction",
                    new Object[]{this.getTransactionModel().getId()},
                    PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()),
                    true
            );

            if (transactionList != null && !transactionList.isEmpty()){
                throw new MissingDataException("Transaction has already been synced or sync is currently in progress.");
            }
        }

        Integer updateResult = dm.parameterizedExecuteNonQuery("data.posapi30.UpdateOpenTransaction", new Object[]{
                this.getTransactionModel().getId(),
                PosAPIHelper.TransactionStatus.EDIT_IN_PROGRESS.toInt(),
                lastUpdatedDate

        }, logFileName);

        if (updateResult == null || updateResult <= 0) {
            throw new MissingDataException("Error updating Open Transaction", transactionModel.getTerminal().getId());
        }
    }

    public void updateOpenTransactionStatus(String newStatus) throws Exception {

        DataManager dm = new DataManager();
        String logFileName = PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId());

        Logger.logMessage("Updating Open Transaction. TransactionBuilder.updateOpenTransactionStatus.", logFileName, Logger.LEVEL.DEBUG);

        String lastUpdatedDate = getCurrentDate();
        Integer curTransStatusId = this.getTransactionModel().getTransactionStatusId();

        //Edit in Progress
        String errorMsg = "Error updating Open Transaction Status";
        Boolean cantUpdateStatus = false;
        Boolean isSameStatus = false;
        Integer newStatusId = 0;
        //parse Transaction Status
        if (newStatus != null && !newStatus.isEmpty()) {
            newStatusId = PosAPIHelper.TransactionStatus.fromStringToInt(newStatus);
            if (newStatusId<1)
            {
                cantUpdateStatus = true;
            }
        }
        else
        {
            cantUpdateStatus = true;
        }

        if (curTransStatusId.equals(newStatusId))
        {
            isSameStatus = true;
            errorMsg = "Error: Attempted to Update Open Transaction Status to Current Status";
        }
        else if (!cantUpdateStatus)
        {
            switch (curTransStatusId)
            {
                // OPEN/SUSPENDED(1, "Suspended") - Can be changed to anything
                case 1:
                    break;
                // EXPIRED(2, "Expired")
                case 2:
                    errorMsg = "Error Updating EXPIRED Transaction Status";
                    cantUpdateStatus = true;
                    break;
                // EDIT_COMPLETED(3, "Edit Completed") - can't be changed
                case 3:
                    errorMsg = "Error Updating EDIT COMPLETED Transaction Status";
                    cantUpdateStatus = true;
                    break;
                // EDIT_IN_PROGRESS(4, "Edit In Progress") - can go to edit completed (3) or open (1)
                case 4:
                    switch (newStatusId)
                    {
                        case 2:
                            errorMsg = "Error Updating EDIT IN PROGRESS Transaction Status to EXPIRED";
                            cantUpdateStatus = true;
                            break;
                        case 5:
                            errorMsg = "Error Updating EDIT IN PROGRESS Transaction Status to COMPLETED";
                            cantUpdateStatus = true;
                            break;
                    }
                    break;
                // COMPLETED(5, "Completed")  - can't be changed
                case 5:
                    errorMsg = "Error Updating COMPLETED Transaction Status";
                    cantUpdateStatus = true;
                    break;
            }
        }

        if ( (cantUpdateStatus) || (isSameStatus) )
        {
            throw new MissingDataException(errorMsg, transactionModel.getTerminal().getId());
        }
        else
        {
            Integer updateResult = dm.parameterizedExecuteNonQuery("data.posapi30.UpdateOpenTransaction", new Object[]{
                    this.getTransactionModel().getId(),
                    newStatusId,
                    lastUpdatedDate

            }, logFileName);
            if (updateResult == null || updateResult <= 0) {
                throw new MissingDataException(errorMsg, transactionModel.getTerminal().getId());
            }
            else
            {
                String updatedStatusStr = newStatus;
                if (updatedStatusStr.toUpperCase().equals("OPEN"))
                {
                    updatedStatusStr = "SUSPENDED";
                }
                this.getTransactionModel().setTransactionStatus(updatedStatusStr);
                this.getTransactionModel().setTransactionStatusId(newStatusId);
            }
        }

    }

    //region Save Quickcharge Transaction

    public void saveQCTransactions(JDCConnection conn, String logFileName) throws Exception {
        QcTransactionModel qcTransactionModel = null;

        //don't save QC transaction if there are Item Profile Mappings for the Terminal.
        if (this.getTransactionModel().shouldAPICreateQCTransactions()) {

            //Tender line items
            for (TenderLineItemModel tenderLineItemModel : getTransactionModel().getTenders()) {
                //QC Tenders
                if (tenderLineItemModel.getTender() != null && tenderLineItemModel.getTender().getTenderTypeId() != null) {
                    qcTransactionModel = null;

                    //if there are no item profile mappings,
                    //write the qc transaction and the qc discount

                    if (tenderLineItemModel.getQcTransaction() != null) {
                        qcTransactionModel = this.saveQcTransactionHeader(tenderLineItemModel, conn, logFileName);  //insert QC_Transaction Header
                    }
                }
            }

            for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : getTransactionModel().getReceivedOnAccounts()) {
                if (receivedOnAccountLineItemModel.getExtendedAmount().compareTo(BigDecimal.ZERO) != 0) {
                    if (receivedOnAccountLineItemModel.getReceivedOnAccount() != null
                            && receivedOnAccountLineItemModel.getReceivedOnAccount().isQC()) {

                        if (receivedOnAccountLineItemModel.getAccount() != null) {
                            qcTransactionModel = null;

                            //if there are no item profile mappings,
                            //write the qc transaction and the qc discount

                            if (receivedOnAccountLineItemModel.getQcTransaction() != null) {
                                qcTransactionModel = this.saveQcTransactionHeader(receivedOnAccountLineItemModel, conn, logFileName);  //insert QC_Transaction Header
                            }
                        }
                    }
                }
            }
        }
    }

    public QcTransactionModel saveQcTransactionHeader(TenderLineItemModel tenderLineItem, JDCConnection conn, String logFileName) throws Exception {

        DataManager dm = new DataManager();
        Logger.logMessage("Saving QC Transaction. TransactionBuilder.saveQcTransactionHeader.", logFileName, Logger.LEVEL.DEBUG);

        tenderLineItem.getQcTransaction().setPaTransLineItemId(tenderLineItem.getId()); //update the paTransLineItem so the model is updated

        ArrayList<HashMap> resultsHM = dm.parameterizedExecuteQuery("data.posapi30.InsertQCTransactionSP",
                new Object[]{
                        tenderLineItem.getAccount().getId(),
                        transactionModel.getTerminal().getId(),
                        tenderLineItem.getQcTransaction().getTransactionDetails().get(0).getAmount(),
                        (tenderLineItem.getQcTransaction().getPayments()) == null ? 1 : tenderLineItem.getQcTransaction().getPayments(),
                        tenderLineItem.getQcTransaction().getTimeStamp(),

                        tenderLineItem.getQcTransaction().getTransComment(),
                        tenderLineItem.getQcTransaction().getUserId(),
                        tenderLineItem.getQcTransaction().getTransactionDetails().get(0).getPurchaseCategory().getId(),
                        tenderLineItem.getQcTransaction().getPaTransLineItemId()

                }, logFileName, true, conn);

        Integer newTransId = null;

        if (resultsHM != null && !resultsHM.isEmpty()) {
            newTransId = CommonAPI.convertModelDetailToInteger(resultsHM.get(0).get("TRANSACTIONID"));
            tenderLineItem.getQcTransaction().setId(newTransId);
        }

        return tenderLineItem.getQcTransaction();
    }

    public QcTransactionModel saveQcTransactionHeader(ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel, JDCConnection conn, String logFileName) throws Exception {

        DataManager dm = new DataManager();
        Logger.logMessage("Saving QC Transaction. TransactionBuilder.saveQcTransactionHeader.", logFileName, Logger.LEVEL.DEBUG);

        receivedOnAccountLineItemModel.getQcTransaction().setPaTransLineItemId(receivedOnAccountLineItemModel.getId()); //update the paTransLineItem so the model is updated

        ArrayList<HashMap> resultsHM = dm.parameterizedExecuteQuery("data.posapi30.InsertQCTransactionSP",
                new Object[]{
                        receivedOnAccountLineItemModel.getAccount().getId(),
                        transactionModel.getTerminal().getId(),
                        receivedOnAccountLineItemModel.getQcTransaction().getTransactionDetails().get(0).getAmount(),
                        (receivedOnAccountLineItemModel.getQcTransaction().getPayments()) == null ? 1 : receivedOnAccountLineItemModel.getQcTransaction().getPayments(),
                        receivedOnAccountLineItemModel.getQcTransaction().getTimeStamp(),

                        receivedOnAccountLineItemModel.getQcTransaction().getTransComment(),
                        receivedOnAccountLineItemModel.getQcTransaction().getUserId(),
                        receivedOnAccountLineItemModel.getQcTransaction().getTransactionDetails().get(0).getPurchaseCategory().getId(),
                        receivedOnAccountLineItemModel.getQcTransaction().getPaTransLineItemId()

                }, logFileName, true, conn);

        Integer newTransId = null;

        if (resultsHM != null && !resultsHM.isEmpty()) {
            newTransId = CommonAPI.convertModelDetailToInteger(resultsHM.get(0).get("TRANSACTIONID"));
            receivedOnAccountLineItemModel.getQcTransaction().setId(newTransId);
        }

        return receivedOnAccountLineItemModel.getQcTransaction();
    }

    public void saveQCDiscounts(JDCConnection conn, String logFileName) throws Exception {

        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case SALE:
                QcTransactionModel qcTransactionModel = null;

                //QC Discounts are not allowed in offline mode
                if (!this.getTransactionModel().isOfflineMode()) {
                    //first check for "Applied Qc Discounts on the TenderLineItem.Account"
                    if (hasThirdPartyQCDiscount()) {
                        saveThirdPartyQCDiscounts(conn, logFileName);

                    } else {
                        //check the qcpos location
                        saveQcPosQCDiscounts(conn, logFileName);
                    }
                }
        }

    }

    private boolean hasThirdPartyQCDiscount() {
        boolean hasQcDiscounts = false;

        for (TenderLineItemModel tenderLineItemModel : getTransactionModel().getTenders()) {
            //QC Tenders
            if (tenderLineItemModel.getTender() != null && tenderLineItemModel.getTender().getTenderTypeId() != null) {

                if (tenderLineItemModel.getAccount() != null && tenderLineItemModel.getAccount().getQcDiscountsApplied() != null && !tenderLineItemModel.getAccount().getQcDiscountsApplied().isEmpty()) {
                    hasQcDiscounts = true;
                }
            }
        }

        return hasQcDiscounts;
    }

    //QC Discounts are not refunded or voided
    public void saveThirdPartyQCDiscounts(JDCConnection conn, String logFileName) throws Exception {
        DataManager dm = new DataManager();
        Logger.logMessage("Saving QC Discount. TransactionBuilder.saveQCDiscount.", logFileName, Logger.LEVEL.DEBUG);

        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            //check for QC Discounts from third party
            if (tenderLineItemModel.getAccount() != null && tenderLineItemModel.getAccount().getQcDiscountsApplied() != null && !tenderLineItemModel.getAccount().getQcDiscountsApplied().isEmpty()) {
                for (QcDiscountLineItemModel qcDiscountLineItemModel : tenderLineItemModel.getAccount().getQcDiscountsApplied()) {

                    qcDiscountLineItemModel.setEmployeeId(tenderLineItemModel.getAccount().getId());
                    if (tenderLineItemModel.getAccount() != null && tenderLineItemModel.getQcTransaction() != null) {
                        qcDiscountLineItemModel.setTransactionId(tenderLineItemModel.getQcTransaction().getId());
                    } else {
                        qcDiscountLineItemModel.setTransactionId(null);
                    }
                    qcDiscountLineItemModel.setTerminalId(transactionModel.getTerminal().getId());
                    qcDiscountLineItemModel.setTransactionDate(transactionModel.getTimeStamp());
                    qcDiscountLineItemModel.setItemComment(tenderLineItemModel.getItemComment());
                    qcDiscountLineItemModel.setPaTransLineItemId(tenderLineItemModel.getId());

                    qcDiscountLineItemModel = this.saveQCDiscount(qcDiscountLineItemModel, conn, logFileName);

                    //update the QcDiscount.amount field so third parties can display the updated available amount
                    try {
                        if (qcDiscountLineItemModel.getQcDiscount().getDiscountType() != null && qcDiscountLineItemModel.getQcDiscount().getDiscountType().equalsIgnoreCase(PosAPIHelper.DiscountType.COUPON.toString())) {
                            qcDiscountLineItemModel.getQcDiscount().setAmount(qcDiscountLineItemModel.getQcDiscount().getAmount().add(qcDiscountLineItemModel.getExtendedAmount()));

                            if (tenderLineItemModel.getAccount().getQcDiscountsAvailable() != null && !tenderLineItemModel.getAccount().getQcDiscountsAvailable().isEmpty()) {
                                for (QcDiscountModel qcDiscountModel : tenderLineItemModel.getAccount().getQcDiscountsAvailable()) {
                                    if (qcDiscountModel.getId().equals(qcDiscountLineItemModel.getQcDiscount().getId())) {
                                        qcDiscountModel.setAmount(qcDiscountModel.getAmount().add(qcDiscountLineItemModel.getExtendedAmount()));
                                    }
                                }
                            }
                        }
                    } catch (Exception ex) {
                        //don't throw the exception if we couldn't update the qcDiscountModel.amount field
                    }
                }
            }
        }
    }

    public void saveQcPosQCDiscounts(JDCConnection conn, String logFileName) throws Exception {
        for (DiscountLineItemModel discountLineItemModel : transactionModel.getDiscounts()) {
            if (discountLineItemModel.getEmployeeId() != null && discountLineItemModel.getQcDiscount() != null) {

                //create a QC Discount Line Item model to save
                QcDiscountLineItemModel qcDiscountLineItemModel = new QcDiscountLineItemModel();
                qcDiscountLineItemModel.setEmployeeId(discountLineItemModel.getEmployeeId());
                qcDiscountLineItemModel.setTransactionId(null);
                qcDiscountLineItemModel.setQcDiscount(discountLineItemModel.getQcDiscount());
                qcDiscountLineItemModel.setTerminalId(transactionModel.getTerminal().getId());
                qcDiscountLineItemModel.setTransactionDate(transactionModel.getTimeStamp());
                qcDiscountLineItemModel.setAmount(discountLineItemModel.getExtendedAmount());
                qcDiscountLineItemModel.setItemComment(discountLineItemModel.getItemComment());
                qcDiscountLineItemModel.setPaTransLineItemId(discountLineItemModel.getId());

                for (TenderLineItemModel tenderLineItemModel : transactionModel.getTenders()) {
                    if (tenderLineItemModel.getAccount() != null && tenderLineItemModel.getAccount().getId() != null) {
                        //there is a tender with an account for this QC Discount
                        if (tenderLineItemModel.getAccount().getId().equals(discountLineItemModel.getEmployeeId())) {

                            //override the QC Discount info with the tender/QC Transaction information
                            qcDiscountLineItemModel.setEmployeeId(tenderLineItemModel.getAccount().getId());
                            if (tenderLineItemModel.getAccount() != null && tenderLineItemModel.getQcTransaction() != null) {
                                qcDiscountLineItemModel.setTransactionId(tenderLineItemModel.getQcTransaction().getId());
                            } else {
                                qcDiscountLineItemModel.setTransactionId(null);
                            }
                            qcDiscountLineItemModel.setItemComment(tenderLineItemModel.getItemComment());
                            qcDiscountLineItemModel.setPaTransLineItemId(tenderLineItemModel.getId());

                            if (qcDiscountLineItemModel.getId() == null) { //Make sure the QC Discount only saves once
                                qcDiscountLineItemModel = this.saveQCDiscount(qcDiscountLineItemModel, conn, logFileName);
                                tenderLineItemModel.getAccount().getQcDiscountsApplied().add(qcDiscountLineItemModel);
                            }
                        }
                    }
                }

                //qc discount did not have an associated Quickcharge payment. I.E.- cash payment or free product reward
                if (qcDiscountLineItemModel.getId() == null) {
                    qcDiscountLineItemModel = this.saveQCDiscount(qcDiscountLineItemModel, conn, logFileName);
                }
            }
        }
    }

    public QcDiscountLineItemModel saveQCDiscount(QcDiscountLineItemModel qcDiscountLineItemModel, JDCConnection conn, String logFileName) throws Exception {
        Integer qcDiscountTransactionId = null;
        qcDiscountTransactionId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertQCTransactionDiscount",
                new Object[]{
                        qcDiscountLineItemModel.getEmployeeId(),
                        qcDiscountLineItemModel.getTransactionId(),
                        qcDiscountLineItemModel.getQcDiscount().getId(),
                        qcDiscountLineItemModel.getTerminalId(),
                        qcDiscountLineItemModel.getTransactionDate(),
                        qcDiscountLineItemModel.getExtendedAmount().abs(),
                        qcDiscountLineItemModel.getItemComment(),
                        qcDiscountLineItemModel.getPaTransLineItemId()
                },
                logFileName, conn);

        if (qcDiscountTransactionId <= 0) {
            throw new MissingDataException("QC Discount could not be recorded in QC_DiscountTransactions", transactionModel.getTerminal().getId());
        }

        qcDiscountLineItemModel.setId(qcDiscountTransactionId);

        return qcDiscountLineItemModel;
    }

    //endregion

    //region Save Loyalty

    //region Save Reward Lines

    //save PATransLineItem form submitted reward
    public Integer saveLoyaltyRewardLineItem(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel, JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        DataManager dm = new DataManager();
        Logger.logMessage("Saving Loyalty Reward Line Item. TransactionBuilder.saveLoyaltyRewardLineItem.", logFileName, Logger.LEVEL.DEBUG);

        Integer iPATransactionDetailId = 0;

        iPATransactionDetailId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPATransLineItem", new Object[]{
                loyaltyRewardLineItemModel.getItemTypeId(),
                loyaltyRewardLineItemModel.getReward().getId(),
                (loyaltyRewardLineItemModel.getQuantity() == null) ? 1 : loyaltyRewardLineItemModel.getQuantity(), //new Double(quantitySign * Double.parseDouble(item.get(this.DETAIL_2_QUANTITY).toString())),
                loyaltyRewardLineItemModel.getAmount(), //new Double(amountSign * Double.parseDouble(item.get(this.DETAIL_3_UNITPRICE).toString())),
                (loyaltyRewardLineItemModel.getEligibleAmount() == null) ? 0 : loyaltyRewardLineItemModel.getEligibleAmount(), //Eligible Amount
                loyaltyRewardLineItemModel.getItemComment(), //item.get(this.DETAIL_5_COMMENT).toString(),
                loyaltyRewardLineItemModel.getEmployeeId(), // (((item.size() <= this.DETAIL_6_EMPLOYEEID) || (item.get(this.DETAIL_6_EMPLOYEEID).toString().compareToIgnoreCase("null") == 0)) ? null : new Integer(item.get(this.DETAIL_6_EMPLOYEEID).toString())),
                null,
                iPATransactionId,
                null, //item.size() <= this.DETAIL_12_CREDITCARDTRANSINFO ? "" : item.get(this.DETAIL_12_CREDITCARDTRANSINFO).toString(),
                loyaltyRewardLineItemModel.getBadgeAssignmentId(),
                null, //KeypadID
                loyaltyRewardLineItemModel.getOriginalOrderNumber(),
                null, //PrintStatusId
                loyaltyRewardLineItemModel.getLinkedTransLineItemId(),
                null, //PAComboTransLineItemID
                null, //BasePrice
                null, //ComboPrice
                null, //PAComboDetailID
                null, //PAPrepOptionID
                null, //PAPrepOptionSetID
                BigDecimal.ZERO,  //PAPrepOptionPrice
                null //LinkedFromPATransLineItemIDs
        }, logFileName, conn);

        if (iPATransactionDetailId <= 0) {
            throw new MissingDataException("Loyalty Reward Line Item could not be recorded in QC_PATransLineItem", transactionModel.getTerminal().getId());
        }

        return iPATransactionDetailId;

    }

    public void saveRewardItemTaxDisc(LoyaltyRewardLineItemModel loyaltyRewardLineItemModel, JDCConnection conn, String logFileName, Integer iPATransactionDetailId) throws Exception {
        try {
            //Save rewards for product

            Integer taxItemId = 0;
            Logger.logMessage("Saving reward transaction detail number " + iPATransactionDetailId, logFileName, Logger.LEVEL.DEBUG);

            for (ItemTaxModel itemTaxModel : loyaltyRewardLineItemModel.getTaxes()) {
                itemTaxModel.setTransLineItemId(iPATransactionDetailId);

                taxItemId = 0;
                taxItemId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertPAItemTaxDsct", new Object[]{
                        itemTaxModel.getTransLineItemId(),
                        itemTaxModel.getItemTypeId(),
                        itemTaxModel.getItemId(),
                        (itemTaxModel.getEligibleAmount() == null) ? 0 : itemTaxModel.getEligibleAmount(), //Eligible Amount
                        (itemTaxModel.getAmount() == null) ? 0 : itemTaxModel.getAmount(), //Amount
                        (itemTaxModel.getRefundedAmount() == null) ? 0 : itemTaxModel.getRefundedAmount(),
                        (itemTaxModel.getRefundedAmount() == null) ? 0 : itemTaxModel.getRefundedAmount(),
                        null //LinkedPATranslineItemID
                }, logFileName, conn);

                if (taxItemId <= 0) {
                    throw new MissingDataException("Product Discount Line Item could not be recorded in QC_PAItemTaxDsct", transactionModel.getTerminal().getId());
                }
                itemTaxModel.setId(taxItemId);
            }

        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }
    }

    //endregion

    //region Loyalty Account Points - Point details off of transaction

    //save Loyalty Account Points earned for the transaction
    //TransactionModel.getLoyaltyPoints
    //QC_LoyaltyAccountPoint
    public void saveLoyaltyProductAccountPoints(JDCConnection conn, String logFileName) throws Exception {

        if (!this.getTransactionModel().isCancelTransactionType() &&
                !this.getTransactionModel().isTrainingTransactionType() &&
                !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {

            //switch for loyalty Account Point, this excludes Rewards
            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:
                case VOID:
                case REFUND:
                    for (LoyaltyPointModel loyaltyPointModel : this.getTransactionModel().getLoyaltyPointsAll()) {
                        //don't save any Reward Account Points here
                        if (loyaltyPointModel.isReward()) {
                            continue;
                        }
                        loyaltyPointModel.setTransactionId(this.getTransactionModel().getId());
                        Logger.logMessage("Saving Loyalty Point Line Item. TransactionBuilder.saveLoyaltyPointsForTransactionHeader.", logFileName, Logger.LEVEL.DEBUG);
                        Integer loyaltyPointId = 0;

                        loyaltyPointId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertLoyaltyAccountPointsForTransactionHeader", new Object[]{
                                loyaltyPointModel.getLoyaltyProgram().getId(),
                                loyaltyPointModel.getEmployeeId(),
                                loyaltyPointModel.getTransactionId(),    //use the transaction that was just inserted
                                loyaltyPointModel.getPoints(),
                                null, //(loyaltyPointModel.getLoyaltyRewardTransLineId()) != null ? loyaltyPointModel.getLoyaltyRewardTransLineId() //insert a null because it's not a reward
                                (loyaltyPointModel.getLoyaltyAccrualPolicy() == null || loyaltyPointModel.getLoyaltyAccrualPolicy().getId() == null) ? null : loyaltyPointModel.getLoyaltyAccrualPolicy().getId(),
                                null, // LoyaltyDonationID

                        }, logFileName, conn);

                        if (loyaltyPointId <= 0) {
                            throw new MissingDataException("Loyalty Point Account Line Item could not be recorded in QC_LoyaltyAccountPoint", transactionModel.getTerminal().getId());
                        }
                        loyaltyPointModel.setId(loyaltyPointId);

                        for (ItemLoyaltyPointModel itemLoyaltyPointDetail : loyaltyPointModel.getPointDetails()) {
                            itemLoyaltyPointDetail.setLoyaltyAccountPointId(loyaltyPointId);
                        }
                    }
                    break;
                default:
                    //no loyalty points are written for NOSALE, BROWSERCLOSE, LOGIN, LOGOUT, BATCH CLOSE,
                    break;
            }
        }
    }

    //save Item Loyalty Points Products (that earned points)
    //QC_PAItemLoyaltyPoint
    public void saveLoyaltyProductAccountPointDetails(JDCConnection conn, String logFileName) throws Exception {
        //Only save Loyalty if we are not in Cancel or Training Mode
        if (!this.getTransactionModel().isCancelTransactionType() &&
                !this.getTransactionModel().isTrainingTransactionType() &&
                !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {
            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:
                case REFUND:
                case VOID:
                    //Loop through the Earned Loyalty Points to get the Id for the detail record
                    for (LoyaltyPointModel loyaltyPointModel : this.getTransactionModel().getLoyaltyPointsAll()) {
                        if (!loyaltyPointModel.isReward()) {

                            for (ItemLoyaltyPointModel itemLoyaltyPointModel : loyaltyPointModel.getPointDetails()) {
                                this.saveLoyaltyPointDetails(itemLoyaltyPointModel, conn, logFileName);
                            }
                        }
                    }
            }
        }
    }


    //save Loyalty Account Points for submitted rewards
    //QC_LoyaltyAccountPoints
    public void saveLoyaltyRewardAccountPoints(JDCConnection conn, String logFileName) throws Exception {
        DataManager dm = new DataManager();


        if (!this.getTransactionModel().isCancelTransactionType() &&
                !this.getTransactionModel().isTrainingTransactionType() &&
                !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {


            //switch for loyalty Account Point, this excludes Rewards
            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:

                    /*remove the rewards off the TransactionModel.LoyaltyPointsEarned list
                    this is used for display for refunds and voids
                    at this point the Loyalty Point for the Reward lives on the LoyaltyRewardLineItem.LoyaltyPoint
                    */
                    Iterator<LoyaltyPointModel> loyaltyPointIterator = this.getTransactionModel().getLoyaltyPointsAll().iterator();
                    while (loyaltyPointIterator.hasNext()) {
                        LoyaltyPointModel loyaltyPointModel = loyaltyPointIterator.next();
                        if (loyaltyPointModel.isReward()) {
                            loyaltyPointIterator.remove();
                        }
                    }

                    for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()) {
                        //fill in the point fields
                        loyaltyRewardLineItemModel.getLoyaltyPoint().setLoyaltyRewardTransLineId(loyaltyRewardLineItemModel.getId());
                        loyaltyRewardLineItemModel.getLoyaltyPoint().setTransactionId(this.getTransactionModel().getId());
                        Integer points = 0;
                        points = loyaltyRewardLineItemModel.getLoyaltyPoint().getPoints();
                        //reverse the sign on a sale
                        if (this.getTransactionModel().getTransactionTypeEnum().equals(PosAPIHelper.TransactionType.SALE)) {
                            if (points > 0) { //if points are positive, reverse the sign
                                loyaltyRewardLineItemModel.getLoyaltyPoint().setPoints(points * -1);
                            }
                        }

                        Logger.logMessage("Saving Loyalty Point Line Item. TransactionBuilder.saveLoyaltyPointsForTransactionHeader.", logFileName, Logger.LEVEL.DEBUG);

                        Integer loyaltyPointId = 0;
                        loyaltyPointId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertLoyaltyAccountPointsForTransactionHeader", new Object[]{
                                loyaltyRewardLineItemModel.getLoyaltyPoint().getLoyaltyProgram().getId(),
                                loyaltyRewardLineItemModel.getLoyaltyPoint().getEmployeeId(),
                                loyaltyRewardLineItemModel.getLoyaltyPoint().getTransactionId(),    //use the transaction that was just inserted
                                loyaltyRewardLineItemModel.getLoyaltyPoint().getPoints(),
                                loyaltyRewardLineItemModel.getLoyaltyPoint().getLoyaltyRewardTransLineId(), //insert the Reward Trans Line Item if it exists
                                null, //LoyaltyAccrualPolicyId
                                null // LoyaltyDonationID
                        }, logFileName, conn);

                        if (loyaltyPointId <= 0) {
                            throw new MissingDataException("Loyalty Point Account Line Item could not be recorded in QC_LoyaltyAccountPoint", transactionModel.getTerminal().getId());
                        }

                        loyaltyRewardLineItemModel.getLoyaltyPoint().setId(loyaltyPointId);
                    }

                    break;
                case VOID:
                case REFUND:
                    for (LoyaltyPointModel loyaltyPointModel : this.getTransactionModel().getLoyaltyPointsAll()) {
                        //don't save any Loyalty Program Account Points here
                        if (!loyaltyPointModel.isReward()) {
                            continue;
                        }

                        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()) {

                            if (loyaltyPointModel.getRewardGuid().equals(loyaltyRewardLineItemModel.getRewardGuid())) {
                                loyaltyPointModel.setLoyaltyRewardTransLineId(loyaltyRewardLineItemModel.getId()); //update the loyalty point with the new ID
                            }
                        }

                        loyaltyPointModel.setTransactionId(this.getTransactionModel().getId());
                        Logger.logMessage("Saving Loyalty Point Line Item. TransactionBuilder.saveLoyaltyPointsForTransactionHeader.", logFileName, Logger.LEVEL.DEBUG);
                        Integer loyaltyPointId = 0;

                        loyaltyPointId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertLoyaltyAccountPointsForTransactionHeader", new Object[]{
                                loyaltyPointModel.getLoyaltyProgram().getId(),
                                loyaltyPointModel.getEmployeeId(),
                                loyaltyPointModel.getTransactionId(),    //use the transaction that was just inserted
                                loyaltyPointModel.getPoints(),
                                loyaltyPointModel.getLoyaltyRewardTransLineId(),
                                null, //LoyaltyAccrualPolicyId
                                null // LoyaltyDonationID
                        }, logFileName, conn);

                        if (loyaltyPointId <= 0) {
                            throw new MissingDataException("Loyalty Point Account Line Item could not be recorded in QC_LoyaltyAccountPoint", transactionModel.getTerminal().getId());
                        }
                        loyaltyPointModel.setId(loyaltyPointId);
                    }
                    break;
            }
        }
    }

    //endregion

    //region Item Loyalty Points - Point details off of products/rewards


    //save Item Loyalty Points for submitted rewards
    //QC_PAItemLoyaltyPoint
    public void saveLoyaltyRewardAccountPointDetails(JDCConnection conn, String logFileName) throws Exception {

        if (!this.getTransactionModel().isCancelTransactionType() &&
                !this.getTransactionModel().isTrainingTransactionType() &&
                !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {

            switch (this.getTransactionModel().getApiActionTypeEnum()) {

                case SALE:
                    for (ProductLineItemModel productLineItemModelReward : this.getTransactionModel().getProducts()) {
                        for (ItemLoyaltyPointModel itemLoyaltyPoint : productLineItemModelReward.getLoyaltyPointDetails()) {
                            //Loop through the Earned Loyalty Points to get the Id for the detail record
                            for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()) {
                                //Match on the Reward UUID
                                if (loyaltyRewardLineItemModel.getRewardGuid().equals(itemLoyaltyPoint.getRewardGuid())) {
                                    //match up the Loyalty Account Point ID
                                    itemLoyaltyPoint.setLoyaltyAccountPointId(loyaltyRewardLineItemModel.getLoyaltyPoint().getId());
                                    //match up the Trans Line Item ID
                                    itemLoyaltyPoint.setTransLineItemId(productLineItemModelReward.getId());
                                    this.saveLoyaltyPointDetails(itemLoyaltyPoint, conn, logFileName);
                                }
                            }
                        }
                    }
                    break;
                case REFUND:
                case VOID:
                    for (ProductLineItemModel productLineItemModelReward : this.getTransactionModel().getProducts()) {
                        for (ItemLoyaltyPointModel itemLoyaltyPoint : productLineItemModelReward.getLoyaltyPointDetails()) {
                            //Loop through the Earned Loyalty Points to get the Id for the detail record
                            for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()) {
                                //Match on the Reward UUID
                                if (loyaltyRewardLineItemModel.getRewardGuid().equals(itemLoyaltyPoint.getRewardGuid())) {
                                    itemLoyaltyPoint.setLoyaltyAccountPointId(loyaltyRewardLineItemModel.getLoyaltyPoint().getId());

                                    //match up the Trans Line Item ID
                                    itemLoyaltyPoint.setTransLineItemId(productLineItemModelReward.getId());
                                    this.saveLoyaltyPointDetails(itemLoyaltyPoint, conn, logFileName);
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }

    //Common for Product Details and Reward Details
    // QC_PAItemLoyaltyPoint
    public void saveLoyaltyPointDetails(ItemLoyaltyPointModel loyaltyDetailModel, JDCConnection conn, String logFileName) throws Exception {
        DataManager dm = new DataManager();
        Logger.logMessage("Saving Loyalty Point Detail. TransactionBuilder.saveLoyaltyPointDetails.", logFileName, Logger.LEVEL.DEBUG);

        Integer pointDetailId = 0;
        pointDetailId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertLoyaltyPointDetail", new Object[]{
                loyaltyDetailModel.getLoyaltyAccountPointId(),
                loyaltyDetailModel.getTransLineItemId(),    //insert the transaction line item that was just inserted for the Product Line Item
                loyaltyDetailModel.getEligibleAmount(),
                loyaltyDetailModel.getPoints()
        }, logFileName, conn);

        if (pointDetailId <= 0) {
            throw new MissingDataException("Loyalty Point Account Line Item could not be recorded in QC_LoyaltyAccountPoint", transactionModel.getTerminal().getId());
        }
        loyaltyDetailModel.setId(pointDetailId);
    }

    //endregion

    //region Loyalty Point Adjustments - Point details off of loyalty point adjustments

    public void saveLoyaltyAdjustmentPoints(JDCConnection conn, String logFileName) throws Exception {

        if (!this.getTransactionModel().isCancelTransactionType() &&
                !this.getTransactionModel().isTrainingTransactionType() &&
                !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {

            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case LOYALTY_ADJUSTMENT:
                    for (LoyaltyAdjustmentModel loyaltyAdjustmentModel : this.getTransactionModel().getLoyaltyAdjustments()) {

                        Logger.logMessage("Saving Loyalty Point Line Item. TransactionBuilder.saveLoyaltyAdjustmentPoints.", logFileName, Logger.LEVEL.DEBUG);
                        Integer loyaltyPointId = 0;

                        loyaltyPointId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertLoyaltyAccountPointsForTransactionHeader", new Object[]{
                                loyaltyAdjustmentModel.getLoyaltyProgram().getId(), // LoyaltyProgramID
                                this.getTransactionModel().getLoyaltyAccount().getId(), // EmployeeID
                                this.getTransactionModel().getId(), // PATransactionID   //use the transaction that was just inserted
                                loyaltyAdjustmentModel.getPoints(), // Points
                                null, // LoyaltyRewardLineItemID
                                null, // LoyaltyAccrualPolicyId
                                null // LoyaltyDonationID
                        }, logFileName, conn);

                        if (loyaltyPointId <= 0) {
                            throw new MissingDataException("Loyalty Point Account Line Item could not be recorded in QC_LoyaltyAccountPoint", transactionModel.getTerminal().getId());
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    //endregion

    //region Loyalty Donations - Point details off of donations

    public void saveLoyaltyDonationPoints(JDCConnection conn, String logFileName) throws Exception {

        if (!this.getTransactionModel().isCancelTransactionType() &&
                !this.getTransactionModel().isTrainingTransactionType() &&
                !this.getTransactionModel().isPaymentGatewayInquireTransactionType()) {

            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case LOYALTY_ADJUSTMENT:
                    for (DonationLineItemModel donationLineItemModel : this.getTransactionModel().getDonations()) {

                        // If the donation type is NOT 'Loyalty' - don't insert QC_LoyaltyAccountPoint record
                        if(donationLineItemModel.getDonation() != null && donationLineItemModel.getDonation().getDonationTypeId() != null  && donationLineItemModel.getDonation().getDonationTypeId() != 1) {
                            return;
                        }

                        Logger.logMessage("Saving Loyalty Point Line Item. TransactionBuilder.saveLoyaltyAdjustmentPoints.", logFileName, Logger.LEVEL.DEBUG);
                        Integer loyaltyPointId = 0;

                        loyaltyPointId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertLoyaltyAccountPointsForTransactionHeader", new Object[]{
                                donationLineItemModel.getDonation().getLoyaltyProgram().getId(), // LoyaltyProgramID
                                this.getTransactionModel().getLoyaltyAccount().getId(), // EmployeeID
                                this.getTransactionModel().getId(), // PATransactionID   //use the transaction that was just inserted
                                donationLineItemModel.getPoints(), // Points
                                null, // LoyaltyRewardLineItemID
                                null, // LoyaltyAccrualPolicyId
                                donationLineItemModel.getId() // PATransLineItemId
                        }, logFileName, conn);

                        if (loyaltyPointId <= 0) {
                            throw new MissingDataException("Loyalty Point Account Line Item could not be recorded in QC_LoyaltyAccountPoint", transactionModel.getTerminal().getId());
                        }

                        // Set the loyatlyAccountPointId on the loyalty point model
                        if(donationLineItemModel.getLoyaltyPoint() != null) {
                            donationLineItemModel.getLoyaltyPoint().setId(loyaltyPointId);
                        }

                        // Add the donation Loyalty Point model to the transaction's Loyalty Points Redeemed
                        this.getTransactionModel().getLoyaltyPointsRedeemed().add(donationLineItemModel.getLoyaltyPoint());
                    }
                    break;
                default:
                    break;
            }
        }
    }

    //endregion

//endregion

//endregion

    //region Void Methods

    private void voidOriginalTransaction(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {

        if (this.getTransactionModel().getId() == null || this.getTransactionModel().getId().toString().isEmpty()) {
            throw new TransactionNotFoundException("Original Transaction cannot be found.", transactionModel.getTerminal().getId());
        }

        //save transaction header record
        Logger.logMessage("Voiding QCPOS Transaction.  TransactionBuilder.voidOriginalTransaction.", logFileName, Logger.LEVEL.DEBUG);
        this.voidQcPosTransactionHeader(conn, logFileName, this.getTransactionModel().getId());
        this.voidTransLineItems(conn, logFileName, this.getTransactionModel().getId());
        this.voidProductModifiers(conn, logFileName, this.getTransactionModel().getId());
        this.voidItemTaxesDiscounts(conn, logFileName, this.getTransactionModel().getId());

        //this will void any Loyalty Point details for products and Loyalty Rewards
        this.voidLoyaltyPointDetails(conn, logFileName, this.getTransactionModel().getId());
        this.cleanUpItemLoyaltyPoints(conn, logFileName);
    }

    private void voidQcPosTransactionHeader(JDCConnection conn, String logFileName, Integer originalPATransactionId) throws Exception {
        try {
            Integer iUpdatedRows = 0;
            iUpdatedRows = dm.parameterizedExecuteNonQuery("data.posapi30.VoidPATransactionHeader", new Object[]{
                    originalPATransactionId,
                    this.getTransactionModel().getDirectVoidQuantity(),
                    this.getTransactionModel().getDirectVoidAmount()

            }, logFileName, conn);

            if (iUpdatedRows < 0) {
                throw new MissingDataException("Transaction Line Items could not be voided", transactionModel.getTerminal().getId());
            }

        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }
    }

    private void voidTransLineItems(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        try {
            Integer iUpdatedRows = 0;

            iUpdatedRows = dm.parameterizedExecuteNonQuery("data.posapi30.VoidPATransLineItems", new Object[]{
                    iPATransactionId

            }, logFileName, conn);

            if (iUpdatedRows < 0) {
                throw new MissingDataException("Transaction Line Items could not be voided", transactionModel.getTerminal().getId());
            }

        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }

    }

    private void voidProductModifiers(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        try {
            Integer iUpdatedRows = 0;

            iUpdatedRows = dm.parameterizedExecuteNonQuery("data.posapi30.VoidProductModifiers", new Object[]{
                    iPATransactionId

            }, logFileName, conn);

            if (iUpdatedRows < 0) {
                throw new MissingDataException("Product Modifiers could not be voided", transactionModel.getTerminal().getId());
            }

        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }
    }

    private void voidItemTaxesDiscounts(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        try {
            Integer iUpdatedRows = 0;

            iUpdatedRows = dm.parameterizedExecuteNonQuery("data.posapi30.VoidProductTaxesDiscounts", new Object[]{
                    iPATransactionId

            }, logFileName, conn);

            if (iUpdatedRows < 0) {
                throw new MissingDataException("Product Taxes could not be voided", transactionModel.getTerminal().getId());
            }

        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }
    }

    private void voidLoyaltyPointDetails(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        try {
            Integer iUpdatedRows = 0;

            iUpdatedRows = dm.parameterizedExecuteNonQuery("data.posapi30.VoidLoyaltyPointDetails", new Object[]{
                    iPATransactionId

            }, logFileName, conn);

            if (iUpdatedRows < 0) {
                throw new MissingDataException("Loyalty Point Details could not be voided", transactionModel.getTerminal().getId());
            }

        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }
    }

    //endregion

    //region Refund Methods

    private void refundOriginalTransaction(JDCConnection conn, String logFileName, Integer newiPATransactionId) throws Exception {

        if (this.getTransactionModel().getId() == null || this.getTransactionModel().getId().toString().isEmpty()) {
            throw new TransactionNotFoundException("Original Transaction cannot be found.", transactionModel.getTerminal().getId());
        }

        //save transaction header record
        Logger.logMessage("Refunding QCPOS Transaction.  TransactionBuilder.refundOriginalTransaction.", logFileName, Logger.LEVEL.DEBUG);
        //this.refundQcPosTransactionHeader(conn, logFileName, this.getTransactionModel().getId());
        this.refundProductLineItems(conn, logFileName);

        this.refundTaxLineItems(conn, logFileName);
        this.refundTenderLineItems(conn, logFileName);
        this.refundDiscountLineItems(conn, logFileName);
        this.refundPaidOutLineItems(conn, logFileName);
        this.refundReceivedOnAccountLineItems(conn, logFileName);
        this.refundLoyaltyRewardLineItems(conn, logFileName, newiPATransactionId);
        this.refundServiceChargeLineItems(conn, logFileName);
        this.refundGratuityLineItems(conn, logFileName);
        this.refundSurchargeLineItems(conn, logFileName);
        this.refundComboLineItems(conn, logFileName);
        this.cleanUpItemLoyaltyPoints(conn, logFileName);

            /*if (!this.getTransactionModel().isCancelTransactionType() && !this.getTransactionModel().isTrainingTransactionType()) {
                this.refundLoyaltyPoints(conn, logFileName);
            }*/
    }

    //common method for all the PA_TransLineItems except for Products
    public void refundPaTransLineItem(JDCConnection conn, String logFileName, Integer transLineItemId, BigDecimal refundedQuantity, BigDecimal refundedAmount) throws Exception {

        //Update PA Trans Line Item, but NO Product line items
        Integer result = dm.parameterizedExecuteNonQuery("data.posapi30.RefundPATransLineItem",
                new Object[]{
                        transLineItemId,
                        refundedQuantity,
                        refundedAmount
                }, logFileName,
                conn
        );

        if (result < 1) {
            throw new MissingDataException("Could not refund TransLineItem record", transactionModel.getTerminal().getId());
        }
    }

    //common method for all the PA_TransLineItems only for Products
    public void refundPaTransLineItemProduct(JDCConnection conn, String logFileName, Integer transLineItemId, BigDecimal refundedQuantity, BigDecimal refundedAmount) throws Exception {

        //Update PA Trans Line Item, ONLY Product line items
        Integer result = dm.parameterizedExecuteNonQuery("data.posapi30.RefundPATransLineItemProduct",
                new Object[]{
                        transLineItemId,
                        refundedQuantity,
                        refundedAmount
                }, logFileName,
                conn
        );

        if (result < 1) {
            throw new MissingDataException("Could not refund TransLineItem record", transactionModel.getTerminal().getId());
        }
    }

    /**
     *  Fully refund the PA Trans Line
     */
    public void refundPaTransLineItemComplete(JDCConnection conn, String logFileName, Integer transLineItemId) throws Exception {

        //Update PA Trans Line Item, but NO Product line items
        Integer result = dm.parameterizedExecuteNonQuery("data.posapi30.RefundPATransLineItemComplete",
                new Object[]{
                        transLineItemId
                }, logFileName,
                conn
        );

        if (result < 1) {
            throw new MissingDataException("Could not refund TransLineItem record", transactionModel.getTerminal().getId());
        }
    }

    public void refundPaTransLineItemCombo(JDCConnection conn, String logFileName, Integer transLineItemId, BigDecimal refundedExtAmount) throws Exception {

        //Update PA Trans Line Item, but NO Product line items
        Integer result = dm.parameterizedExecuteNonQuery("data.posapi30.RefundPATransLineItemCombo",
                new Object[]{
                        transLineItemId,
                        refundedExtAmount
                }, logFileName,
                conn
        );

        if (result < 1) {
            throw new MissingDataException("Could not refund TransLineItem record", transactionModel.getTerminal().getId());
        }
    }

    private void refundProductLineItems(JDCConnection conn, String logFileName) throws Exception {
        try {
            Integer iUpdatedRows = 0;

            //products
            for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {

                if (productLineItemModel.getId() != null && !productLineItemModel.getId().toString().isEmpty()) {

                    //update the trans line item
                    if (productLineItemModel.isQuantityRefund()) {

                        if (productLineItemModel.isRefundTheAmount()) {
                            refundPaTransLineItemProduct(conn, logFileName, productLineItemModel.getId(), productLineItemModel.getQuantity(), productLineItemModel.getAmount());
                        } else { //just update the quantity
                            refundPaTransLineItemProduct(conn, logFileName, productLineItemModel.getId(), productLineItemModel.getQuantity(), BigDecimal.ZERO);
                        }
                    } else { //if it's not a quantity refund, update both the quantity and amount
                        refundPaTransLineItemProduct(conn, logFileName, productLineItemModel.getId(), productLineItemModel.getQuantity(), productLineItemModel.getAmount());
                    }

                    //update the trans line item
                    //original
                    //refundPaTransLineItem(conn, logFileName, productLineItemModel.getId(), (productLineItemModel.getQuantity() == null) ? BigDecimal.ZERO : productLineItemModel.getQuantity(), productLineItemModel.getAmount());


                    //product taxes
                    for (ItemTaxModel productTaxItem : productLineItemModel.getTaxes()) {
                        refundItemTaxItem(conn, logFileName, productTaxItem.getId(), productTaxItem.getEligibleAmount(), productTaxItem.getAmount());
                    }

                    //product discounts
                    for (ItemDiscountModel productDiscountItem : productLineItemModel.getDiscounts()) {
                        refundProductDiscountItem(conn, logFileName, productDiscountItem.getId(), productDiscountItem.getEligibleAmount(), productDiscountItem.getAmount());
                    }

                    //product modifiers
                    for (ModifierLineItemModel modifierLineItem : productLineItemModel.getModifiers()) {
                        refundProductModLineItem(conn, logFileName, modifierLineItem.getId(), modifierLineItem.getQuantity(), modifierLineItem.getAmount());
                    }

                    //product rewards
                    for (ItemRewardModel productRewardItem : productLineItemModel.getRewards()) {
                        refundItemTaxItem(conn, logFileName, productRewardItem.getId(), productRewardItem.getEligibleAmount(), productRewardItem.getAmount());
                    }

                    //product Loyalty Points
                    for (ItemLoyaltyPointModel itemLoyaltyPointModel : productLineItemModel.getLoyaltyPointDetails()) {
                        refundItemLoyaltyPointItem(conn, logFileName, itemLoyaltyPointModel.getId(), itemLoyaltyPointModel.getEligibleAmount(), itemLoyaltyPointModel.getPoints());
                    }

                    //product surcharges
                    for (ItemSurchargeModel productSurchargeItem : productLineItemModel.getSurcharges()) {
                        refundProductSurchargeItem(conn, logFileName, productSurchargeItem.getId(), productSurchargeItem.getEligibleAmount(), productSurchargeItem.getAmount());
                    }
                }
            }

        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }
    }

    //update the refundedQuantity and refundedAmount for Product Taxes
    public void refundItemTaxItem(JDCConnection conn, String logFileName, Integer productTaxTransLineItemId, BigDecimal refundedEligibleAmount, BigDecimal refundedAmount) throws Exception {

        //Update PA Trans Line Item
        Integer result = dm.parameterizedExecuteNonQuery("data.posapi30.RefundProductTaxItem",
                new Object[]{
                        productTaxTransLineItemId,
                        refundedEligibleAmount,
                        refundedAmount
                }, logFileName,
                conn
        );

        if (result < 1) {
            throw new MissingDataException("Could not refund Product Tax record", transactionModel.getTerminal().getId());
        }
    }

    //update the refundedQuantity and refundedAmount forProduct Discounts
    public void refundProductDiscountItem(JDCConnection conn, String logFileName, Integer productTaxTransLineItemId, BigDecimal refundedEligibleAmount, BigDecimal refundedAmount) throws Exception {

        //Update PA Trans Line Item
        Integer result = dm.parameterizedExecuteNonQuery("data.posapi30.RefundProductDiscountItem",
                new Object[]{
                        productTaxTransLineItemId,
                        refundedEligibleAmount,
                        refundedAmount
                }, logFileName,
                conn
        );

        if (result < 1) {
            throw new MissingDataException("Could not refund Product Discount record", transactionModel.getTerminal().getId());
        }
    }

    //update the refundedQuantity and refundedAmount on the Product Modifier
    public void refundProductModLineItem(JDCConnection conn, String logFileName, Integer productModTransLineItemId, BigDecimal refundedQuantity, BigDecimal refundedAmount) throws Exception {

        //Update PA Trans Line Item
        Integer result = dm.parameterizedExecuteNonQuery("data.posapi30.RefundProductModTransLineItem",
                new Object[]{
                        productModTransLineItemId,
                        refundedQuantity,
                        refundedAmount
                }, logFileName,
                conn
        );

        if (result < 1) {
            throw new MissingDataException("Could not refund Product Modifier line record", transactionModel.getTerminal().getId());
        }
    }

    //update the refundedEligibleAmount and refundedPoints for the Loyalty Points
    public void refundItemLoyaltyPointItem(JDCConnection conn, String logFileName, Integer itemLoyaltyPointId, BigDecimal refundedEligibleAmount, BigDecimal refundedPoints) throws Exception {

        //Update PA Trans Line Item
        Integer result = dm.parameterizedExecuteNonQuery("data.posapi30.RefundItemLoyaltyPointDetail",
                new Object[]{
                        itemLoyaltyPointId,
                        refundedEligibleAmount,
                        refundedPoints
                }, logFileName,
                conn
        );

        if (result < 1) {
            throw new MissingDataException("Could not refund Product Loyalty Point line record", transactionModel.getTerminal().getId());
        }
    }

    public void refundProductSurchargeItem(JDCConnection conn, String logFileName, Integer productTransLineItemId, BigDecimal refundedEligibleAmount, BigDecimal refundedAmount) throws Exception {

        //Update PA Trans Line Item
        Integer result = dm.parameterizedExecuteNonQuery("data.posapi30.RefundProductSurchargeItem",
                new Object[]{
                        productTransLineItemId,
                        refundedEligibleAmount,
                        refundedAmount
                }, logFileName,
                conn
        );

        if (result < 1) {
            throw new MissingDataException("Could not refund Product Surcharge record", transactionModel.getTerminal().getId());
        }
    }

    public void refundTaxLineItems(JDCConnection conn, String logFileName) throws Exception {
        for (TaxLineItemModel taxLineItemModel : this.getTransactionModel().getTaxes()) {
            if (taxLineItemModel.getId() != null && !taxLineItemModel.getId().toString().isEmpty()) {
                //Only update the trans line item if the amount is not zero
                if (taxLineItemModel.getAmount() == null || taxLineItemModel.getAmount().compareTo(BigDecimal.ZERO) != 0) {
                    refundPaTransLineItem(conn, logFileName, taxLineItemModel.getId(), taxLineItemModel.getQuantity(), taxLineItemModel.getAmount());
                }
            }
        }
    }

    public void refundTenderLineItems(JDCConnection conn, String logFileName) throws Exception {
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            if (tenderLineItemModel.getId() != null && !tenderLineItemModel.getId().toString().isEmpty()) {
                //Only update the trans line item if the amount is not zero
                if (tenderLineItemModel.getAmount() == null || tenderLineItemModel.getAmount().compareTo(BigDecimal.ZERO) != 0) {
                    refundPaTransLineItem(conn, logFileName, tenderLineItemModel.getId(), tenderLineItemModel.getQuantity(), tenderLineItemModel.getAmount());
                }
            }
        }
    }

    public void refundDiscountLineItems(JDCConnection conn, String logFileName) throws Exception {
        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
            if (discountLineItemModel.getId() != null && !discountLineItemModel.getId().toString().isEmpty()) {
                //update the trans line item
                refundPaTransLineItem(conn, logFileName, discountLineItemModel.getId(), discountLineItemModel.getQuantity(), discountLineItemModel.getAmount());

                //product taxes
                for (ItemTaxModel discountTaxItem : discountLineItemModel.getTaxes()) {
                    refundItemTaxItem(conn, logFileName, discountTaxItem.getId(), discountTaxItem.getEligibleAmount(), discountTaxItem.getAmount());
                }
            }
        }
    }

    public void refundPaidOutLineItems(JDCConnection conn, String logFileName) throws Exception {
        for (PaidOutLineItemModel paidOutLineItemModel : this.getTransactionModel().getPaidOuts()) {
            if (paidOutLineItemModel.getId() != null && !paidOutLineItemModel.getId().toString().isEmpty()) {
                //update the trans line item
                refundPaTransLineItem(conn, logFileName, paidOutLineItemModel.getId(), paidOutLineItemModel.getQuantity(), paidOutLineItemModel.getAmount());
            }
        }
    }

    public void refundReceivedOnAccountLineItems(JDCConnection conn, String logFileName) throws Exception {
        for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : this.getTransactionModel().getReceivedOnAccounts()) {
            if (receivedOnAccountLineItemModel.getId() != null && !receivedOnAccountLineItemModel.getId().toString().isEmpty()) {
                //update the trans line item
                refundPaTransLineItem(conn, logFileName, receivedOnAccountLineItemModel.getId(), receivedOnAccountLineItemModel.getQuantity(), receivedOnAccountLineItemModel.getAmount());
            }
        }
    }

    public void refundLoyaltyRewardLineItems(JDCConnection conn, String logFileName, Integer iPATransactionId) throws Exception {
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()) {
            if (loyaltyRewardLineItemModel.getId() != null && !loyaltyRewardLineItemModel.getId().toString().isEmpty()) {
                //update the trans line item
                refundPaTransLineItem(conn, logFileName, loyaltyRewardLineItemModel.getId(), loyaltyRewardLineItemModel.getQuantity(), loyaltyRewardLineItemModel.getAmount());

                for (ItemLoyaltyPointModel itemLoyaltyPointModel : loyaltyRewardLineItemModel.getLoyaltyPointDetails()) {
                    refundItemLoyaltyPointItem(conn, logFileName, itemLoyaltyPointModel.getId(), itemLoyaltyPointModel.getEligibleAmount(), itemLoyaltyPointModel.getPoints());
                }

                //reward Item Tax Discount Records
                for (ItemTaxModel taxItem : loyaltyRewardLineItemModel.getTaxes()) {
                    refundItemTaxItem(conn, logFileName, taxItem.getId(), taxItem.getEligibleAmount(), taxItem.getAmount());
                }
            }
        }
    }

    private void refundServiceChargeLineItems(JDCConnection conn, String logFileName) throws Exception {
        try {
            Integer iUpdatedRows = 0;

            //products
            for (ServiceChargeLineItemModel serviceChargeLineItemModel : this.getTransactionModel().getServiceCharges()) {

                if (serviceChargeLineItemModel.getId() != null && !serviceChargeLineItemModel.getId().toString().isEmpty()) {
                    //update both the quantity and amount*/
                    refundPaTransLineItemProduct(conn, logFileName, serviceChargeLineItemModel.getId(), serviceChargeLineItemModel.getQuantity(), serviceChargeLineItemModel.getAmount());
                }
            }

        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }
    }

    private void refundGratuityLineItems(JDCConnection conn, String logFileName) throws Exception {
        try {
            Integer iUpdatedRows = 0;

            //products
            for (GratuityLineItemModel gratuityLineItemModel : this.getTransactionModel().getGratuities()) {

                if (gratuityLineItemModel.getId() != null && !gratuityLineItemModel.getId().toString().isEmpty()) {
                    //update both the quantity and amount*/
                    refundPaTransLineItemProduct(conn, logFileName, gratuityLineItemModel.getId(), gratuityLineItemModel.getQuantity(), gratuityLineItemModel.getAmount());
                }
            }

        } catch (Exception ex) {
            throw ex; //if error occurs, bubble up the error to the main method
        }
    }

    public void refundSurchargeLineItems(JDCConnection conn, String logFileName) throws Exception {
        for (SurchargeLineItemModel surchargeLineItemModel : this.getTransactionModel().getSurcharges()) {
            if (surchargeLineItemModel.getId() != null && !surchargeLineItemModel.getId().toString().isEmpty()) {
                //Only update the trans line item if the amount is not zero
                if (surchargeLineItemModel.getAmount() != null || surchargeLineItemModel.getAmount().compareTo(BigDecimal.ZERO) != 0) {
                    refundPaTransLineItem(conn, logFileName, surchargeLineItemModel.getId(), surchargeLineItemModel.getQuantity(), surchargeLineItemModel.getAmount());
                }
            }
        }
    }

    public void refundComboLineItems(JDCConnection conn, String logFileName) throws Exception {
        for (ComboLineItemModel comboLineItemModel : this.getTransactionModel().getCombos()) {
            if (comboLineItemModel.getId() != null && !comboLineItemModel.getId().toString().isEmpty()) {
                //Only update the trans line item if the amount is not zero
                if (comboLineItemModel.getExtendedAmount() != null || comboLineItemModel.getAmount().compareTo(BigDecimal.ZERO) != 0) {
                    if (comboLineItemModel.isRefundComplete()) {
                        refundPaTransLineItemComplete(conn, logFileName, comboLineItemModel.getId());
                    } else {
                        refundPaTransLineItemCombo(conn, logFileName, comboLineItemModel.getId(), comboLineItemModel.getExtendedAmount());
                    }
                }
            }
        }
    }

    public void cleanUpItemLoyaltyPoints(JDCConnection conn, String logFileName) throws Exception {

        //Loop through all the Earned Loyalty Points, if they have been refunded, update the Item Loyalty Points (RefundedPoints and RefundedEligibleAmount) on the original transaction
        for (LoyaltyPointModel loyaltyPointModel : this.getTransactionModel().getOriginalTransaction().getLoyaltyPointsAll()) {
            if (loyaltyPointModel.areAllPointsAlreadyRefunded()) {
                //Update PA Trans Line Item
                Integer result = dm.parameterizedExecuteNonQuery("data.posapi30.UpdateItemLoyaltyPointDetails",
                        new Object[]{
                                loyaltyPointModel.getId()
                        }, logFileName,
                        conn
                );

                if (result < 1) {
                    throw new MissingDataException("Could not update Item Loyalty Point Details", transactionModel.getTerminal().getId());
                }
            }
        }
    }


    //endregion

    //region Quickcharge Methods

    //create a summary of the line items
    //group the amounts by purchase category (Pos Item)
    //these summary amounts will be saved in the QC_TransactionPosItems table
    public void calculateQCLineItems() throws Exception {
        Integer defaultKey = this.getTransactionModel().getTerminal().getDefaultPurchaseCategoryId();
        BigDecimal qcTenderAmount = this.getTransactionModel().getQcTenderBalance();

        if (this.getTransactionModel().shouldAPICreateQCTransactions()) {
            if (this.getTransactionModel().hasQuickChargeTender() || this.getTransactionModel().hasQCQuickChargeTenderOnReceivedOnAccount()) {

                //for each QC tender, add the purchase category and amount
                for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
                    //only create QC Transactions for Quickcharge tender
                    if (tenderLineItemModel.getTender().getTenderTypeId().equals(PosAPIHelper.TenderType.QUICKCHARGE.toInt())) {

                        //08/14/2018 egl - We need to save QC Transactions with a 0 amount so they display in MyQC
                        QcTransactionModel qcTransactionModel = new QcTransactionModel(this.getTransactionModel(), tenderLineItemModel);
                        //create the QC Transaction Line Item records
                        List<QcTransactionLineItemModel> qcTransactionLineItems = new ArrayList<>();

                        //one tender
                        if (tenderLineItemModel.getExtendedAmount().equals(this.getTransactionModel().getQcTenderBalance())) {
                            QcTransactionLineItemModel qcTransactionLineItemModel = new QcTransactionLineItemModel(defaultKey, qcTenderAmount);

                            //populate the Purchase Category with detail
                            if (qcTransactionLineItemModel.getPosItemId() != null && !qcTransactionLineItemModel.getPosItemId().toString().isEmpty()) {
                                qcTransactionLineItemModel.setPurchaseCategory(PurchaseCategoryModel.getPurchaseCategoryModel(qcTransactionLineItemModel.getPosItemId(), this.getTransactionModel().getTerminal().getId()));
                            }
                            qcTransactionLineItems.add(qcTransactionLineItemModel);

                        } else { //multiple tenders
                            QcTransactionLineItemModel qcTransactionLineItemModel = new QcTransactionLineItemModel(defaultKey, tenderLineItemModel.getExtendedAmount());
                            qcTransactionLineItems.add(qcTransactionLineItemModel);
                        }
                        qcTransactionModel.setTransactionDetails(qcTransactionLineItems);
                        tenderLineItemModel.setQcTransaction(qcTransactionModel);
                    }
                } //end of tenderLineItem Loop


                if (this.getTransactionModel().getApiActionTypeEnum().equals(PosAPIHelper.ApiActionType.ROA)) {
                    //Create QC Transaction for received on account
                    for (ReceivedOnAccountLineItemModel receivedOnAccountLineItemModel : this.getTransactionModel().getReceivedOnAccounts()) {
                        if (receivedOnAccountLineItemModel.getExtendedAmount().compareTo(BigDecimal.ZERO) != 0) {
                            if (receivedOnAccountLineItemModel.getReceivedOnAccount() != null
                                    && receivedOnAccountLineItemModel.getReceivedOnAccount().isQC()) {

                                if (receivedOnAccountLineItemModel.getAccount() != null) {
                                    QcTransactionModel qcTransactionModel = new QcTransactionModel(this.getTransactionModel(), receivedOnAccountLineItemModel);
                                    //create the QC Transaction Line Item records
                                    List<QcTransactionLineItemModel> qcTransactionLineItems = new ArrayList<>();
                                    QcTransactionLineItemModel qcTransactionLineItemModel = new QcTransactionLineItemModel(defaultKey, receivedOnAccountLineItemModel.getExtendedAmount());

                                    //when it's an ROA, check that it's a negative
                                    if (qcTransactionLineItemModel.getAmount().compareTo(BigDecimal.ZERO) == 1) {
                                        //flip the sign so the transaction gets saved with the correct value
                                        qcTransactionLineItemModel.setAmount(qcTransactionLineItemModel.getAmount().negate());
                                    }

                                    //populate the Purchase Category with detail
                                    if (qcTransactionLineItemModel.getPosItemId() != null && !qcTransactionLineItemModel.getPosItemId().toString().isEmpty()) {
                                        qcTransactionLineItemModel.setPurchaseCategory(PurchaseCategoryModel.getPurchaseCategoryModel(qcTransactionLineItemModel.getPosItemId(), this.getTransactionModel().getTerminal().getId()));
                                    }
                                    qcTransactionLineItems.add(qcTransactionLineItemModel);

                                    qcTransactionModel.setTransactionDetails(qcTransactionLineItems);
                                    receivedOnAccountLineItemModel.setQcTransaction(qcTransactionModel);
                                }
                            }
                        }
                    }
                }
            }
        } //end of Item Mapping check if
    }

    /**
     * Set QCImportStatus on the PA Transaction
     * Does NOT Reference QC_ImportStatus, these values are not referenced in the database
     * 0 - The transaction will be written by the Background Processor
     * 1 - Open: Already processed by Background Processor.  Aka, the API will write the transaction
     * 2 - Completed: This transaction does not need to be written to the QC_Transaction table
     * 3 - In Process: The Background Processor is currently writing this transaction
     */
    public void setQcImportStatus() {
        //default the Import Status that there is no QC Tenders
        this.getTransactionModel().setQcImportStatus(2);

        if (this.getTransactionModel().getApiActionTypeEnum().equals(PosAPIHelper.ApiActionType.OPEN)) {
            this.getTransactionModel().setQcImportStatus(2);
        } else if (!this.getTransactionModel().hasQuickChargeTender() && !this.getTransactionModel().hasQCQuickChargeTenderOnReceivedOnAccount()) {
            this.getTransactionModel().setQcImportStatus(2);
        } else if (this.getTransactionModel().shouldAPICreateQCTransactions()) { //No Item Profile Mappings are setup, API will write the QC Transactions, And POS is online
            this.getTransactionModel().setQcImportStatus(1);  //API will write the QC Transaction
        } else if (this.getTransactionModel().hasQuickChargeTender() || this.getTransactionModel().hasQCQuickChargeTenderOnReceivedOnAccount()) {

            /*if there are Item Profile Mappings
             * or the POS is offline
             * or it's a VOID/Refund and the original sale hasn't been processed yet
             * have the Background processor write the QC_Transaction
             * */
            this.getTransactionModel().setQcImportStatus(0); // have the BP write the QC Transaction
        }
    }

    //endregion

    //region Misc Methods

    public void checkForOfflineMode() {
        //check if POS is in offline mode
        if ((MMHProperties.getAppSetting("site.application.offline").length() > 0) && (MMHProperties.getAppSetting("site.application.offline").compareToIgnoreCase("yes") == 0)) {
            this.getTransactionModel().setOfflineMode(true);
        }
    }

    public void setTransactionType() {
        switch (this.getTransactionModel().getApiActionTypeEnum()) {
            case REFUND:
            case REFUND_MANUAL:
                this.getTransactionModel().setTransactionTypeEnum(PosAPIHelper.TransactionType.REFUND);
                this.getTransactionModel().setTransactionTypeId(PosAPIHelper.TransactionType.REFUND.toInt());
                break;
            case VOID:
                this.getTransactionModel().setTransactionTypeEnum(PosAPIHelper.TransactionType.VOID);
                this.getTransactionModel().setTransactionTypeId(PosAPIHelper.TransactionType.VOID.toInt());
                break;
            case SALE:
                this.getTransactionModel().setTransactionTypeEnum(PosAPIHelper.TransactionType.SALE);
                this.getTransactionModel().setTransactionTypeId(PosAPIHelper.TransactionType.SALE.toInt());
                break;
            case OPEN:
                this.getTransactionModel().setTransactionTypeEnum(PosAPIHelper.TransactionType.OPEN);
                this.getTransactionModel().setTransactionTypeId(PosAPIHelper.TransactionType.OPEN.toInt());
                break;
            default:
                //do nothing
                break;
        }

        if (this.getTransactionModel().isCancelTransactionType()) {
            this.getTransactionModel().setTransactionTypeEnum(PosAPIHelper.TransactionType.CANCEL);
            this.getTransactionModel().setTransactionTypeId(PosAPIHelper.TransactionType.CANCEL.toInt());
        }

        if (this.getTransactionModel().isTrainingTransactionType()) {
            this.getTransactionModel().setTransactionTypeEnum(PosAPIHelper.TransactionType.TRAINING);
            this.getTransactionModel().setTransactionTypeId(PosAPIHelper.TransactionType.TRAINING.toInt());
        }

        if (this.getTransactionModel().isPaymentGatewayInquireTransactionType()){
            this.getTransactionModel().setTransactionTypeEnum(PosAPIHelper.TransactionType.PAYMENT_GATEWAY_INQUIRE);
            this.getTransactionModel().setTransactionTypeId(PosAPIHelper.TransactionType.PAYMENT_GATEWAY_INQUIRE.toInt());
        }

    }

    //Flip the signs on all quantities in the TransactionModel
    public void flipSignsOnTransactionModel() throws Exception {

        //reverse loyalty points for the transaction
        //Loyalty Account Points
        for (LoyaltyPointModel loyaltyPointModel : this.getTransactionModel().getLoyaltyPointsAll()) {
            loyaltyPointModel.setPoints(loyaltyPointModel.getPoints() * -1);
        }

        //we now create new LoyaltyPoints in TransactionModel.prepareLoyaltyPointsEarned due to Accrual Policies
        for (LoyaltyPointModel loyaltyPointModel : this.getTransactionModel().getLoyaltyPointsEarned()) {
            loyaltyPointModel.setPoints(loyaltyPointModel.getPoints() * -1);

            for (LoyaltyAccrualPolicyPointModel loyaltyAccrualPolicyPointModel : loyaltyPointModel.getLoyaltyAccrualPolicyPoints()){
                loyaltyAccrualPolicyPointModel.setPoints(loyaltyAccrualPolicyPointModel.getPoints() * - 1);
            }
        }

        //Tax Line Items
        for (TaxLineItemModel taxLineItemModel : this.getTransactionModel().getTaxes()) {
            taxLineItemModel.setQuantity(taxLineItemModel.getQuantity().negate());
            taxLineItemModel.refreshExtendedAmount();

            //only flip the Eligible Amount if it is positive
            if (taxLineItemModel.getEligibleAmount().compareTo(BigDecimal.ZERO) == 1) {
                taxLineItemModel.setEligibleAmount(taxLineItemModel.getEligibleAmount().negate());
            }
        }

        //Tender Line Items
        for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {
            tenderLineItemModel.setQuantity(tenderLineItemModel.getQuantity().negate());
            tenderLineItemModel.refreshExtendedAmount();

            if (tenderLineItemModel.getQcTransaction() != null) {
                for (QcTransactionLineItemModel qcTransactionLineItemModel : tenderLineItemModel.getQcTransaction().getTransactionDetails()) {
                    qcTransactionLineItemModel.setAmount(qcTransactionLineItemModel.getAmount().negate());
                }
            }
        }

        //Product Line Items
        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()) {
            //update product line item first
            productLineItemModel.setQuantity(productLineItemModel.getQuantity().negate());
            productLineItemModel.refreshExtendedAmount();

            for (ItemDiscountModel itemModel : productLineItemModel.getDiscounts()) {
                itemModel.setAmount(itemModel.getAmount().negate());
                itemModel.setEligibleAmount(itemModel.getEligibleAmount().negate());
            }

            for (ItemTaxModel itemModel : productLineItemModel.getTaxes()) {
                itemModel.setAmount(itemModel.getAmount().negate());
                itemModel.setEligibleAmount(itemModel.getEligibleAmount().negate());
            }

            for (ModifierLineItemModel productModifier : productLineItemModel.getModifiers()) {
                productModifier.setQuantity(productModifier.getQuantity().negate());
                productModifier.refreshExtendedAmount();
            }

            for (ItemRewardModel itemRewardModel : productLineItemModel.getRewards()) {
                itemRewardModel.setAmount(itemRewardModel.getAmount().negate());
                itemRewardModel.setEligibleAmount(itemRewardModel.getEligibleAmount().negate());
            }

            for (ItemLoyaltyPointModel productLoyaltyPointDetail : productLineItemModel.getLoyaltyPointDetails()) {
                productLoyaltyPointDetail.setPoints(productLoyaltyPointDetail.getPoints().negate());
                productLoyaltyPointDetail.setEligibleAmount(productLoyaltyPointDetail.getEligibleAmount().negate());
            }

            for (ItemSurchargeModel itemSurchargeModel : productLineItemModel.getSurcharges()) {
                itemSurchargeModel.setAmount(itemSurchargeModel.getAmount().negate());
                itemSurchargeModel.setEligibleAmount(itemSurchargeModel.getEligibleAmount().negate());
            }
        }

        //Paid Out Line Items
        for (PaidOutLineItemModel paidOutLineItemModel : this.getTransactionModel().getPaidOuts()) {
            paidOutLineItemModel.setQuantity(paidOutLineItemModel.getQuantity().negate());
            paidOutLineItemModel.refreshExtendedAmount();
        }

        //Received On Account Line Items
        for (ReceivedOnAccountLineItemModel roaLineItemModel : this.getTransactionModel().getReceivedOnAccounts()) {
            roaLineItemModel.setQuantity(roaLineItemModel.getQuantity().negate());
            roaLineItemModel.refreshExtendedAmount();

            if (roaLineItemModel.getQcTransaction() != null) {
                for (QcTransactionLineItemModel qcTransactionLineItemModel : roaLineItemModel.getQcTransaction().getTransactionDetails()) {
                    qcTransactionLineItemModel.setAmount(qcTransactionLineItemModel.getAmount().negate());
                }
            }
        }

        //Discount Line Items
        for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
            discountLineItemModel.setQuantity(discountLineItemModel.getQuantity().negate());
            discountLineItemModel.refreshExtendedAmount();

            //only flip the Eligible Amount if it is positive
            if (discountLineItemModel.getEligibleAmount().compareTo(BigDecimal.ZERO) == 1) {
                discountLineItemModel.setEligibleAmount(discountLineItemModel.getEligibleAmount().negate());
            }

            for (ItemTaxModel itemModel : discountLineItemModel.getTaxes()) {
                itemModel.setAmount(itemModel.getAmount().negate());
                itemModel.setEligibleAmount(itemModel.getEligibleAmount().negate());
            }
        }

        //Reward Line Items
        for (LoyaltyRewardLineItemModel loyaltyRewardLineItemModel : this.getTransactionModel().getRewards()) {
            loyaltyRewardLineItemModel.setQuantity(loyaltyRewardLineItemModel.getQuantity().negate());
            loyaltyRewardLineItemModel.refreshExtendedAmount();

            if (loyaltyRewardLineItemModel.getEligibleAmount().compareTo(BigDecimal.ZERO) == 1) { //if the Eligible Amount is positive, flip it to negative for the refund/void
                loyaltyRewardLineItemModel.setEligibleAmount(loyaltyRewardLineItemModel.getEligibleAmount().negate());
            }

            //reverse Loyalty Account Point
            /*if (loyaltyRewardLineItemModel != null && loyaltyRewardLineItemModel.getLoyaltyPoint() != null && loyaltyRewardLineItemModel.getLoyaltyPoint().getPoints() != null) {
                loyaltyRewardLineItemModel.getLoyaltyPoint().setPoints(loyaltyRewardLineItemModel.getLoyaltyPoint().getPoints() * -1);
            }*/

            //Item Tax records for the reward
            for (ItemTaxModel itemTaxModel : loyaltyRewardLineItemModel.getTaxes()) {
                itemTaxModel.setAmount(itemTaxModel.getAmount().negate());
                itemTaxModel.setEligibleAmount(itemTaxModel.getEligibleAmount().negate());
            }


            //04/12/2017 egl - the loyalty account point on the reward line item gets flipped when the "getLoyaltyPointsEarned" gets flipped

            //reverse Item Loyalty Account Point
            for (ItemLoyaltyPointModel rewardLoyaltyPointDetail : loyaltyRewardLineItemModel.getLoyaltyPointDetails()) {
                rewardLoyaltyPointDetail.setPoints(rewardLoyaltyPointDetail.getPoints().negate());
                rewardLoyaltyPointDetail.setEligibleAmount(rewardLoyaltyPointDetail.getEligibleAmount().negate());
            }
        }

        //Service Charges
        for (ServiceChargeLineItemModel serviceChargeLineItemModel : this.getTransactionModel().getServiceCharges()) {
            serviceChargeLineItemModel.setQuantity(serviceChargeLineItemModel.getQuantity().negate());
            serviceChargeLineItemModel.refreshExtendedAmount();
        }

        //Gratuities
        for (GratuityLineItemModel gratuityLineItemModel : this.getTransactionModel().getGratuities()) {
            gratuityLineItemModel.setQuantity(gratuityLineItemModel.getQuantity().negate());
            gratuityLineItemModel.refreshExtendedAmount();
        }

        //Surcharges
        for (SurchargeLineItemModel surchargeLineItemModel : this.getTransactionModel().getSurcharges()) {
            surchargeLineItemModel.setQuantity(surchargeLineItemModel.getQuantity().negate());
            surchargeLineItemModel.refreshExtendedAmount();
            surchargeLineItemModel.setEligibleAmount(surchargeLineItemModel.getEligibleAmount().negate());
        }

        //Combos
        for (ComboLineItemModel comboLineItemModel : this.getTransactionModel().getCombos()) {
            comboLineItemModel.setQuantity(comboLineItemModel.getQuantity().negate());
            comboLineItemModel.refreshExtendedAmount();
            comboLineItemModel.setEligibleAmount(comboLineItemModel.getEligibleAmount().negate());
        }
    }

    /**
     * Update the Transaction's TimeStamp for Voids and Refunds
     */
    public void updateTransactionTimeStamp() {

        if (this.getTransactionModel().getApiActionTypeEnum().equals(PosAPIHelper.ApiActionType.REFUND) ||
                this.getTransactionModel().getApiActionTypeEnum().equals(PosAPIHelper.ApiActionType.VOID)) {

            switch (this.getTransactionModel().getPosType()) {
                case THIRD_PARTY:
                    java.util.Date newSaveDate = new java.util.Date();
                    SimpleDateFormat newSaveDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                    String newSaveDateString = newSaveDateFormat.format(newSaveDate);
                    this.getTransactionModel().setTimeStamp(newSaveDateString);
                    break;
                default:
                    //do nothing for QCPOS, Online Order, etc.
                    break;
            }
        }
    }

    public void sortProductLinesByOriginalSequence() {
        //before the products are saved, sort them by the order they came in
        Collections.sort(this.getTransactionModel().getProducts(), new ProductLineItemSequenceComparerAsc());

        // reorder products if using online ordering and combos exist, need to do because loyalty reorders products
        switch (transactionModel.getPosType()) {
            case MMHAYES_POS:
                break;
            case THIRD_PARTY:
                break;
            case ONLINE_ORDERING:
            case KIOSK_ORDERING:
                if (this.getTransactionModel().isInquiry() && this.getTransactionModel().getCombos().size() > 0) {

                    //before the products are saved, sort them by the order they came in from combos
                    Collections.sort(this.getTransactionModel().getProducts(), new ProductLineItemComboSequenceComparerAsc());
                }
                break;
            default:
                break;
        }
    }

    //sortRewardLinesByOriginalSequence
    public void sortRewardLinesByOriginalSequence() {
        //before the products are saved, sort them by the order they came in
        Collections.sort(transactionModel.getRewards(), new LoyaltyRewardLineItemAscComparer("originalSequence"));
    }

    /**
     * B-04416- Addition of Receipt Fields
     * For Pre-Paid accounts, flip the sign for the response.
     * ex - if the balances is $-25, then return $25 indicating that $25 is available
     * This method should be run after the account or transaction work is done.  So that the balances aren't affected during the transaction building processes
     */
    public void fixAccountBalancesForDisplay() {

        if (this.getTransactionModel().getLoyaltyAccount() != null) {
            this.getTransactionModel().getLoyaltyAccount().flipBalanceSignsForPrePayAcct();
        }

        if (this.getTransactionModel().getTenders() != null && !this.getTransactionModel().getTenders().isEmpty()) {
            for (TenderLineItemModel tenderLineItemModel : this.getTransactionModel().getTenders()) {

                if (tenderLineItemModel.getAccount() != null) {
                    tenderLineItemModel.getAccount().flipBalanceSignsForPrePayAcct();
                }
            }
        }
    }

    public void checkForTimeOut(String logFileName) throws Exception {
        long diff = System.nanoTime() - this.getTransactionModel().getTerminal().getStartTime();
        int terminalTransactionTimeout = getTerminalTimeoutInSeconds();

        BigDecimal duration = new BigDecimal(diff);
        BigDecimal billion = new BigDecimal(1000000000);
        BigDecimal durationInSeconds = duration.divide(billion).setScale(4, RoundingMode.HALF_UP);
        BigDecimal timeoutDiff = new BigDecimal(terminalTransactionTimeout).subtract(durationInSeconds);
        Logger.logMessage("checkForTimeOut = " + durationInSeconds, PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.DEBUG);

        if (timeoutDiff.compareTo(BigDecimal.ZERO) < 0) {
            Logger.logMessage("Transaction timed out!", logFileName, Logger.LEVEL.ERROR);
            throw new TransactionTimeoutException(this.getTransactionModel().getTerminal().getId());
        }
    }

    private int getTerminalTimeoutInSeconds() {
        int terminalTransactionTimeout = 30;  //default online timeout
        int posApiTimeoutBuffer = 1;

        if (this.getTransactionModel().isOfflineMode()) {
            terminalTransactionTimeout = 45; //default offline timeout
        } else {
            if (this.getTransactionModel().getTerminal().getNumSecondsSubmitTransactionTimeout() != null && this.getTransactionModel().getTerminal().getNumSecondsSubmitTransactionTimeout() > 0) {
                terminalTransactionTimeout = this.getTransactionModel().getTerminal().getNumSecondsSubmitTransactionTimeout();
            }
        }

        terminalTransactionTimeout = terminalTransactionTimeout - posApiTimeoutBuffer; //we always want the server timeout lower than the client timeout, therefore we lessen the timeout by 1 second
        return terminalTransactionTimeout;
    }

    //query one record from the database.  Used to make sure that communication to the database is open
    public static Integer commCheck() throws Exception {
        DataManager dm = new DataManager();

        try {

            //get all models in an array list
            ArrayList<HashMap> list = dm.parameterizedExecuteQuery("data.posapi30.commCheck", null, PosAPIHelper.getLogFileName(), true);
            if (list != null && list.size() > 0) {
                return 1;
            } else {
                return 0;
            }

        } catch (Exception ex) {
            return 0;
        }
    }

    public static String getCurrentDate() {
        //Format Last updated date.
        // We didn't use the TimeStamp here because there is a chance the TimeStamp could be from an offline Transaction
        // We also didn't have the database set this field because I wanted to return this field in the Response JSON
        Date dateNow = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
        return simpleDateFormat.format(dateNow);
    }

    public void sendPurchaseConfirmationEmail() throws Exception {
        sendPurchaseConfirmationEmail("");
    }

    public String sendPurchaseConfirmationEmail(String emailAddress) throws Exception {

        //only send purchase confirmation email for My QC and Kiosk Ordering
        switch (this.getTransactionModel().getPosType()){
            case KIOSK_ORDERING:
            case ONLINE_ORDERING:
            case MY_QC_FUNDING:
            case MMHAYES_POS:

                // Build params HashMap
                HashMap params = new HashMap();
                params.put("RecipientName", getTransactionModel().getName());
                params.put("PATransactionID", getTransactionModel().getId());
                params.put("StoreName", getTransactionModel().getTerminal().getReceiptHeader1());
                params.put("AppName", "My Quickcharge"); //right now only MyQC is using this purchase confirmation email, will need to be adjusted once POS starts using it

                //determine the store name and transaction type
                switch (this.getTransactionModel().getPosType()){
                    case KIOSK_ORDERING:
                    case ONLINE_ORDERING:

                        params.put("TransactionType", "Order");
                        String storeName = getTransactionModel().getTerminal().getReceiptHeader1();
                        if (storeName.equals("")) {
                            storeName = getTransactionModel().getTerminal().getName();
                        }
                        params.put("StoreName", storeName);

                        String orderReadyDetails = "";
                        String note = getTransactionModel().getPickUpDeliveryNote();
                        if(note == null || note == "") {
                            params.put("OrderReadyDetails", "");
                            break;
                        }
                        String readyTime =  note.substring(note.indexOf("at ") + 3, note.length());

                        if(note.contains(".")) {
                            readyTime = note.substring(note.indexOf("at ") + 3, note.indexOf("."));
                        }

                        if( getTransactionModel().getOrderType().equals("pickup") && !getTransactionModel().getPickUpDeliveryNote().equals("")) {
                            orderReadyDetails = "Your order will be ready for pick up at approximately " + readyTime + ".";
                        } else if ( getTransactionModel().getOrderType().equals("delivery") && !getTransactionModel().getPickUpDeliveryNote().equals("")) {
                            orderReadyDetails = "Your order will be delivered at approximately " + readyTime + ".";
                        }

                        params.put("OrderReadyDetails", orderReadyDetails);

                        break;
                    case MY_QC_FUNDING:

                        params.put("TransactionType", "Purchase");
                        params.put("StoreName", getTransactionModel().getTerminal().getRevenueCenter().getName());
                        params.put("OrderReadyDetails", "");

                        break;
                    case MMHAYES_POS:
                        params.put("TransactionType", "Donation");
                        params.put("StoreName", getTransactionModel().getTerminal().getRevenueCenter().getName());
                        if(!emailAddress.equals("")){
                            Integer employeeIDdonation = getTransactionModel().getLoyaltyAccount().getId();
                            if(employeeIDdonation >-1){
                                params.put("EmployeeID", employeeIDdonation);
                            }
                        }
                    default:
                        break;
                }

                if( emailAddress.equals("") ) {

                    Integer employeeID = getTransactionModel().getLoyaltyAccount().getId();

                    if (employeeID > -1) {
                        params.put("EmployeeID", employeeID);
                    }

                    //if the tender is credit card, get the email from the payment method
                    if (getTransactionModel().getTenders().size() > 0 && getTransactionModel().getTenders().get(0).getTender().getTenderTypeId().equals(2)) {
                        try {
                            FundingModel fundingModel = new FundingModel(employeeID, false);
                            emailAddress = fundingModel.getAccountPaymentMethod().getPaymentMethodEmailAddress();
                        } catch (FundingException ex) {
                            Logger.logMessage("Could not find default payment method email address for employeeID: " + employeeID, Logger.LEVEL.TRACE);
                        }

                        //if email doesn't exist, check account
                        if(emailAddress.equals("") && getTransactionModel().getLoyaltyAccount() != null) {
                            emailAddress = getTransactionModel().getLoyaltyAccount().getEmail();
                        }

                    } else {  //otherwise get the email from the account model
                        AccountPaymentMethodCollection accountPaymentMethodCollection = new AccountPaymentMethodCollection(employeeID);
                        if (accountPaymentMethodCollection.getCollection().size() > 0) {
                            emailAddress = accountPaymentMethodCollection.getCollection().get(0).getPaymentMethodEmailAddress();
                        } else {
                            emailAddress = getTransactionModel().getLoyaltyAccount().getEmail();
                        }
                    }

                    if (emailAddress.equals("")) {
                        Logger.logMessage("Could not find email address to send purchase confirmation email for employeeID: " + employeeID, Logger.LEVEL.TRACE);
                        return "failure";
                    }
                }

                // Define EmailTypeID
                int emailTypeID = 13;

                // Send Email
                commonMMHFunctions common = new commonMMHFunctions();
                if (common.sendTemplateEmail(emailAddress, 0, emailTypeID, "", params)) {
                    Logger.logMessage("Successfully sent purchase confirmation e-mail to " + emailAddress, Logger.LEVEL.DEBUG);
                    return "success";
                } else {
                    Logger.logMessage("Failed to send purchase confirmation e-mail to " + emailAddress, Logger.LEVEL.ERROR);
                    return "failure";
                }

            default:
                return "success";
        }
    }

    /**
     *  Set prep option prices to zero.  In some cases the prep option price will be included in the product or modifier price
     *  Back up the prep option price to the "priceOriginal" field
     */
    public void zeroOutPrepOptionPrices(){
        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()){
            if (productLineItemModel.getPrepOption() != null){
                productLineItemModel.getPrepOption().setPriceOriginal(productLineItemModel.getPrepOption().getPrice());
                productLineItemModel.getPrepOption().setPrice(BigDecimal.ZERO);
            }

            for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()){
                if (modifierLineItemModel.getPrepOption() != null){
                    modifierLineItemModel.getPrepOption().setPriceOriginal(modifierLineItemModel.getPrepOption().getPrice());
                    modifierLineItemModel.getPrepOption().setPrice(BigDecimal.ZERO);
                }
            }
        }
    }

    /**
     * Restore the prep option price from the prep option original price
     */
    public void restorePrepOptionPrices(){
        for (ProductLineItemModel productLineItemModel : this.getTransactionModel().getProducts()){
            if (productLineItemModel.getPrepOption() != null){
                productLineItemModel.getPrepOption().setPrice(productLineItemModel.getPrepOption().getPriceOriginal());
            }

            for (ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()){
                if (modifierLineItemModel.getPrepOption() != null){
                    modifierLineItemModel.getPrepOption().setPrice(modifierLineItemModel.getPrepOption().getPriceOriginal());
                }
            }
        }
    }


    /**
     * Double check the transaction balance.  This will fix the discount amount balance if rounding has caused an over tender situation.
     * Reference HP4246, HP2613
     */
    public void checkForNegativeTransactionBalance() {

        if (this.getTransactionModel().isInquiry()) {
            switch (this.getTransactionModel().getApiActionTypeEnum()) {
                case SALE:
                    try {
                        if (this.getTransactionModel().getDiscounts() != null && !this.getTransactionModel().getDiscounts().isEmpty()) {
                            boolean doDiscountAmountCheck = false;
                            for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
                                if (discountLineItemModel.doesRequireAmountCheck()) {
                                    doDiscountAmountCheck = true;
                                }
                            }

                            if (doDiscountAmountCheck) {
                                BigDecimal transactionBalance = this.getTransactionModel().getTotalTransactionAmount().setScale(2, RoundingMode.HALF_UP);
                                if (transactionBalance.compareTo(BigDecimal.ZERO) < 0) {
                                    for (DiscountLineItemModel discountLineItemModel : this.getTransactionModel().getDiscounts()) {
                                        if (discountLineItemModel.doesRequireAmountCheck()) {
                                            Logger.logMessage("Transaction balance less than zero.  Discount amount will be updated. ", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.TRACE);
                                            //change the amount by difference
                                            BigDecimal discountChangeAmount = transactionBalance.negate();
                                            discountLineItemModel.setAmount(discountLineItemModel.getAmount().add(discountChangeAmount));
                                            discountLineItemModel.refreshExtendedAmount();
                                            Logger.logMessage("Discount amount updated to balance out transaction.  Discount amount decreased by " + discountChangeAmount, PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.TRACE);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception ex) {
                        Logger.logMessage("TransactionBuilder.checkTransactionBalance Error updating discount line.", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.ERROR);
                    }

                    break;
                default:

                    break;
            }
        }
    }

    public void applyVouchers(TransactionCalculation transactionCalculation) throws Exception {
        Logger.logMessage("Entered TransactionBuilder.applyVouchers. ", PosAPIHelper.getLogFileName(this.getTransactionModel().getTerminal().getId()), Logger.LEVEL.DEBUG);
        VoucherCalculation voucherCalculation = new VoucherCalculation(this, transactionCalculation);
        voucherCalculation.apply();
    }


    //endregion

    //region Getters and Setters

    //getter
    public TransactionModel getTransactionModel() {

        return transactionModel;
    }

    //getter
    public void setTransactionModel(TransactionModel transactionModel) {
        this.transactionModel = transactionModel;
    }

    //getter
    public TerminalModel getTerminalModel() {
        return terminalModel;
    }

    //setter
    public void setTerminalModel(TerminalModel terminalModel) {
        this.terminalModel = terminalModel;
    }

    public CashierShiftEndResponseModel getCashierShiftEndResponseModel() throws Exception {

        return CashierShiftEndResponseModel.createCashierShiftEndResponseModel(transactionModel, cashierShiftEndModel);
    }

    //returns a TransactionModel
    public TransactionModel inquire() throws Exception {

        this.getTransactionModel().getTerminal().logTransactionTime();
        this.fixAccountBalancesForDisplay();

        return getTransactionModel();
    }

    public CashierShiftEndModel getCashierShiftEndModel() {
        return cashierShiftEndModel;
    }

    public void setCashierShiftEndModel(CashierShiftEndModel cashierShiftEndModel) {
        this.cashierShiftEndModel = cashierShiftEndModel;
    }

    //endregion

}

