package com.mmhayes.common.api.providers;

//mmhayes dependencies
import com.mmhayes.common.api.json.modules.*;
import com.mmhayes.common.api.json.serializers.*;

//javax.ws.rs dependencies
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

//other dependencies
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;


@Provider
@Produces(MediaType.APPLICATION_JSON)
public class CustomJsonProvider extends JacksonJaxbJsonProvider {

    private static ObjectMapper mapper = new ObjectMapper();

    static {

        //allow using the StringXSSPreventionModule
        mapper.registerModule(new StringXSSPreventionModule());

        //EXAMPLES of other mapper properties that COULD be set on a global level -jrmitaly 7/26/2017
        //mapper.enable(SerializationFeature.INDENT_OUTPUT);
        //mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        //mapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        //mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
    }

    public CustomJsonProvider() {
        super();
        setMapper(mapper);
    }
}