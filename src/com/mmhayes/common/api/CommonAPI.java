package com.mmhayes.common.api;

//mmhayes dependencies

import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.conn_pool.JDCConnection;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.funding.models.AccountPaymentMethodModel;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.login.LoginModel;
import com.mmhayes.common.login.OtmLoginModel;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.signage.models.DisplayModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.*;
import org.apache.commons.io.FilenameUtils;
import org.owasp.esapi.ESAPI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//other dependencies

/*
 CommonAPI
 
 Created with IntelliJ IDEA.
 User: jrmitaly
 Date: 11/29/14
 Time: 1:55 PM
 
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-08-17 21:11:34 -0400 (Tue, 17 Aug 2021) $: Date of last commit
 $Rev: 14983 $: Revision of last commit

 Notes: This class contains common properties and methods to be used by any REST Resource or API related class
*/

public class CommonAPI {

    private static DataManager dm = new DataManager(); //the usual data manager
    private static commonMMHFunctions comm = new commonMMHFunctions(); //the usual common mmh functions
    private final static String COMMON_LOG_FILE = "POSAPI_COMMON_T.log";
    private final static String PROPERTY_MAXFILESIZEMB = "site.upload.maxfilesizemb";
    private final static String SECURITY_TYPE_GATEWAY = "gateway";
    public static String DM_WAR_VERSION = "";
    public static Boolean HTML_ENCODE_API_RESPONSE_STRINGS = true; //in the POS war this is used to turn off HTML encoding for POS API

    // Public list of approved attributes to maintain between sessions
    // DO NOT MODIFY - these may be transferred between users/sessions and may introduce a security hole if containing any sensitive information
    public static final String[] sessionAttributeWhiteList = {"applicationName", "themeName", "MMH_TZOFFSET", "MMH_LANGUAGE"};

    //checks to see if the provided ipAddress passed is blacklisted
    public static Boolean checkIPBlackList(String ipAddress) {
        //this overloaded dm.serializeWithColNames utilizes parameterized queries
        ArrayList ipBlackList = dm.serializeSqlWithColNames("data.validation.checkIPBlackList",
                new Object[]{
                        ipAddress
                },
                false,
                true
        );

        //check IP blacklist to determine if this IP of this request is allowed to attempt to login
        if (ipBlackList != null && ipBlackList.size() >= 1) {
            HashMap ipBlackListHM = (HashMap) ipBlackList.get(0);
            String doubleCheckBlackListedIP = ipBlackListHM.get("IPADDRESS").toString();
            if (doubleCheckBlackListedIP.equals(ipAddress)) {
                return false;
            }
        }
        return true;
    }

    //OVERLOAD - deprecated as of 1/26/2015 by jrmitaly - second parameter is no longer needed!
    public static Boolean checkIPBlackList(String ipAddress, String ipBlackListIPColumnName) {
        Logger.logMessage("Calling deprecated method checkIPBlackList(String ipAddress, String ipBlackListIPColumnName).", Logger.LEVEL.WARNING);
        Logger.logMessage("Calling checkIPBlackList(ipAddress) now...", Logger.LEVEL.DEBUG);
        return checkIPBlackList(ipAddress);
    }

    //checks to see if the provided ipAddress to see if it should be black listed
    public static Validation checkIPAddress(Validation requestValidation, String ipAddress) {
        return checkIPAddress(requestValidation, ipAddress, true);
    }

    //OVERLOAD - checks to see if the provided ipAddress to see if it should be black listed
    public static Validation checkIPAddress(Validation requestValidation, String ipAddress, Boolean sendNotification) {
        /* checkIPAddress notes
           - This method looks at IP addresses in an AuthAttempt table to determine if an IP should be black listed.
              - The ipBlackListAttempts determines how many failed attempts in a certain duration (ipBlackListSeconds) it
                 will take before the system automatically bans this IP
              - The ipBlackListSeconds determines the time duration to look for failed login attempts in the FAILED
                 table.
              - The combination of the two property settings allows for the system to be flexible or loose in it's IP
                 banning mechanism.
              - This method of IP banning really to prevent brute forcing the login page, although it is worth noting
                that if we timed out IP addresses instead of sessions this would most likely not be needed for the purposes
                of preventing brute forcing.
               created by jrmitaly on 11/26/2013  - updated on 10/15/2014 -jrmitaly - abstracted on 11/29/2014 -jrmitaly
        */
        try {
            int totalFailedAttempts = 0; //initialize at 0
            int ipBlackListSeconds_neg = getIPBlackListSeconds() * -1;

            //determine start and end time for SQL
            Date endDate = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(endDate);
            cal.add(Calendar.SECOND, ipBlackListSeconds_neg);
            Date startDate = cal.getTime();

            //convert java dates to sql friendly dates
            java.sql.Timestamp startTimeStamp = new java.sql.Timestamp(startDate.getTime());
            java.sql.Timestamp endTimeStamp = new java.sql.Timestamp(endDate.getTime());

            //this overloaded dm.serializeWithColNames utilizes parameterized queries
            ArrayList failedLoginList = dm.serializeSqlWithColNames("data.validation.getIPFailedCountInRange",
                    new Object[]{
                            ipAddress,
                            startTimeStamp,
                            endTimeStamp
                    },
                    false,
                    true
            );

            if (failedLoginList != null) {

                HashMap failedLoginHM = (HashMap) failedLoginList.get(0);
                totalFailedAttempts = Integer.parseInt(failedLoginHM.get("FAILEDCOUNT").toString());

                //Insert IP into blacklist if we counted a greater number of attempts than what is allowed
                Integer ipBlackListAttempts = getIPBlackListAttempts();
                if (ipBlackListAttempts != 0 && totalFailedAttempts >= ipBlackListAttempts) {

                    if (sendNotification && !getOnGround()) {  //if sendNotification flag is on and we aren't on the ground
                        //email cloud ops team
                        if (!sendBlackListNotification(ipAddress)) {
                            Logger.logMessage("Error sending black list notification email!", Logger.LEVEL.ERROR);
                        }
                    }

                    //invalidate request and log about black list
                    requestValidation.setIsValid(false);
                    requestValidation.setHash("");
                    requestValidation.setDetails("Please contact MMHayes support");
                    Logger.logMessage(" - CRITICAL ERROR - WARNING - Blacklisting IP for suspicious activity! IPAddress: " + ipAddress, Logger.LEVEL.ERROR);

                    //insert a record into the GatewayIPBlackList table which effectively bans the IP address indefinitely
                    String blackListSQL = dm.getSQLProperty("data.validation.blackListIPAddress", new Object[]{});
                    ArrayList<String> blackListSQLValues = new ArrayList<String>();
                    blackListSQLValues.add(0, ipAddress);
                    dm.insertPreparedStatement(blackListSQL, blackListSQLValues);
                }
            } else {
                Logger.logMessage("No result from run data.validation.getIPFailedCountInRange for IPAddress: " + ipAddress, Logger.LEVEL.DEBUG);
            }
        } catch (Exception ex) {
            requestValidation.reset();
            requestValidation.setDetails("An error has occurred. Please contact MMHayes support if the error continues.");
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "CommonAPI.checkIPAddress - Exception: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            requestValidation.setErrorDetails(errorDetails); //set error details
        }
        return requestValidation;
    }

    //determines whether to use the Validation or Request object to determine the end-user's IP Address
    public static String getEndUserIPAddress(Validation requestValidation, HttpServletRequest request) {
        try {
            if (!requestValidation.getIPAddress().equals("")) { //if the IPAddress is not an empty string, then use the value set in the requestValidation object
                return requestValidation.getIPAddress();
            } else { // else use the request object for the IPAddress
                return comm.getEndUserIPAddress(request);
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "CommonAPI.getEndUserIPAddress - Exception: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            return "ERROR";
        }
    }

    //determines whether to use the Validation or Request object to determine the end-user's user agent string
    public static String getEndUserUserAgent(Validation requestValidation, HttpServletRequest request) {
        try {
            if (!requestValidation.getUserAgent().equals("")) { //if the User Agent is not null, then use the value set in the requestValidation object
                return requestValidation.getUserAgent();
            } else { // else use the request object for the User Agent
                return request.getHeader("User-Agent");
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "CommonAPI.getEndUserUserAgent - Exception: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            return "ERROR";
        }
    }

    // Checks if the database is online by running a simple query
    public static boolean checkDBOnline() {
        try {
            dm.serializeSqlWithColNames("data.generic.checkDBOnline", new Object[]{}, false, true);
            return true;
        } catch (Exception ex) {
            Logger.logMessage("Checked database for connectivity - currently offline", Logger.LEVEL.WARNING);
            Logger.logException(ex); //always log exceptions
            return false;
        }
    }

    //reads property file for the instance ID
    public static Integer getInstanceID() {
        try {
            String instanceID = MMHProperties.getAppSetting("site.communication.instanceID");
            if ((instanceID == null) || (instanceID.length() == 0)) {
                instanceID = "0";
            }
            return Integer.parseInt(instanceID);
        } catch (Exception ex) { // Just in case the instance ID does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 0;
        }
    }

    //reads property file for the QC instance ID
    public static Integer getQCInstanceID() {
        try {
            String instanceID = MMHProperties.getAppSetting("site.communication.qcInstanceID");
            if ((instanceID == null) || (instanceID.length() == 0)) {
                instanceID = "0";
            }
            return Integer.parseInt(instanceID);
        } catch (Exception ex) { // Just in case the instance ID does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 0;
        }
    }

    //reads property file for the gateway ID
    public static Integer getGatewayID() {
        try {
            String gatewayID = MMHProperties.getAppSetting("site.communication.gatewayID");
            if ((gatewayID == null) || (gatewayID.length() == 0)) {
                gatewayID = "0";
            }
            return Integer.parseInt(gatewayID);
        } catch (Exception ex) { // Just in case the gateway ID does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 0;
        }
    }

    public static String getHostName(){
        return getHostName(null);
    }

    public static String getHostName(HttpServletRequest request){

        String hostName = "";
        try {
            // Check Globals
            ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery("data.common.getGlobalSSOSettings", new Object[]{}, true);
            if(!arrayList.isEmpty()){
                if(arrayList.get(0).containsKey("QCBASEURL")){
                    hostName = arrayList.get(0).get("QCBASEURL").toString();
                }
            }

            // Check request
            if(request != null && hostName.isEmpty()){
                String requestURL = request.getRequestURL().toString();
                String instanceName = commonMMHFunctions.getInstanceName();
                hostName = requestURL.substring(0, requestURL.indexOf("/" + instanceName + "/"));
            }

        } catch(Exception ex){
            Logger.logMessage("Error in method: getHostName()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        // Return hostname
        return hostName;
    }

    //attempts to determine passStrength from DB if it fails then tries property file
    public static String getPassStrength() {
        String passStrength = null;
        try {
            ArrayList passwordStrengthList = null;
            // Do NOT run on Gateway
            if(!isGateway()){
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                passwordStrengthList = dm.serializeSqlWithColNames("data.globalSettings.getPasswordStrengthID",
                        new Object[]{},
                        false,
                        true
                );
            }

            //check IP blacklist to determine if this IP of this request is allowed to attempt to login
            if (passwordStrengthList != null && passwordStrengthList.size() >= 1) {
                HashMap passwordStrengthHM = (HashMap) passwordStrengthList.get(0);
                String passwordStrengthID = passwordStrengthHM.get("PASSWORDSTRENGTHID").toString();
                if (passwordStrengthID.equals("1")) {
                    passStrength = "loose";
                } else if (passwordStrengthID.equals("2")) {
                    passStrength = "strict";
                }
            } else {
                Logger.logMessage("Could not determine password strength from DB in CommonAPI.getPassStrength() - trying fallback now...", Logger.LEVEL.WARNING);
                passStrength = getPassStrengthFromPropFile();
            }

            if ((passStrength == null) || (passStrength.length() == 0)) {
                passStrength = "strict";
            }

        } catch (Exception ex) {
            Logger.logMessage("Exception caught trying to use password strength fallback in CommonAPI.getPassStrength()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            passStrength = "strict";
            Logger.logMessage("Defaulting passStrength to strict.", Logger.LEVEL.WARNING);
        }

        return passStrength;
    }

    //reads property file for password strength setting
    public static String getPassStrengthFromPropFile() {
        String passStrength;
        try {
            passStrength = MMHProperties.getAppSetting("site.security.passstrength");
            if (passStrength == null || passStrength.length() == 0) {
                passStrength = "strict";
            }
        } catch (Exception ex) {
            Logger.logMessage("Exception caught trying to determine pass strength from properties file in CommonAPI.getPassStrengthFromPropFile()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            Logger.logMessage("Defaulting passStrength to strict.", Logger.LEVEL.WARNING);
            passStrength = "strict";
        }
        return passStrength;
    }

    //reads property file for if this instance is on the ground
    public static Boolean getOnGround() {
        try {
            String onGround = MMHProperties.getAppSetting("site.communication.onground");
            if (onGround == null || onGround.length() == 0 || onGround.compareToIgnoreCase("yes") == 0) {
                onGround = "true";
            }
            return Boolean.valueOf(onGround);
        } catch (Exception ex) { // Just in case the value does not parse as a boolean
            Logger.logException(ex); //always log exceptions
            return true;
        }
    }

    public static Boolean isLocalWar() {
        String appOfflineSetting = MMHProperties.getAppSetting("site.application.offline");
        boolean isLocalWar = ( (appOfflineSetting.length() > 0) && (appOfflineSetting.compareToIgnoreCase("yes") == 0) );

        return isLocalWar;
    }

    // Check to see if a cloud gateway exists (login tables will be unavailable on the instance if it does)
    public static boolean getCloudGatewayAvailable() {
        try {
            Object ret = dm.parameterizedGetSingleField(MMHProperties.getSqlString("data.validation.countGateways"), new ArrayList<>());
            if (ret != null && commonMMHFunctions.tryParseInt(ret.toString()) && Integer.parseInt(ret.toString()) > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return false;
        }
    }

    //gets the security type for this application (gateway or instance)
    public static String getSecurityType() {
        String type = MMHProperties.getAppSetting("site.security.type");
        if ((type == null) || (type.length() == 0)) {
            type = "invalid";
        }
        return type;
    }

    //gets the security key for this application
    public static String getSecurityKey() {
        String key = MMHProperties.getAppSetting("site.security.key");
        if ((key == null) || (key.length() == 0)) {
            key = "invalid";
        }
        return key;
    }

    //attempts to determine the min number of characters the user should have entered for their password from DB if it fails then tries property file
    public static Integer getMinPasswordLength() {
        String columnName = "MinPasswordLength";
        Integer configProperty = null;
        int defaultPropertyValue = 3;
        try {
            ArrayList propertyList = null;
            // Do NOT run on Gateway
            if(!isGateway()){
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                propertyList = dm.parameterizedExecuteQuery("data.globalSettings.getMinPasswordLength",
                        new Object[]{},
                        true
                );
            }

            if (propertyList != null && propertyList.size() >= 1) {
                HashMap resultHM = (HashMap) propertyList.get(0);
                configProperty = Integer.parseInt(resultHM.get(columnName.toUpperCase()).toString());
            } else {
                Logger.logMessage("Could not determine "+ columnName +" from DB in CommonAPI.getMinPasswordLength() - trying fallback now...", Logger.LEVEL.WARNING);
                configProperty = getMinPasswordLengthFromPropFile();
            }

            if (configProperty == null) {
                configProperty = defaultPropertyValue;
            }

        } catch (Exception ex) {
            Logger.logMessage("Exception caught trying to use "+ columnName +" fallback in CommonAPI.getMinPasswordLength()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            configProperty = defaultPropertyValue;
            Logger.logMessage("Defaulting " + columnName + " to " + defaultPropertyValue + ".", Logger.LEVEL.WARNING);
        }
        return configProperty;
    }

    //reads property files for the min number of characters the user should have entered for their password
    public static Integer getMinPasswordLengthFromPropFile() {
        try {
            String minPasswordLength = MMHProperties.getAppSetting("site.security.minpasswordlength");
            if ((minPasswordLength == null) || (minPasswordLength.length() == 0)) {
                minPasswordLength = "3"; //default to three characters for a min password length, anything else is bogus
            }
            return Integer.parseInt(minPasswordLength);
        } catch (Exception ex) { // Just in case the minimum password length does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 3;
        }
    }

    //attempts to determine Logon attempts for timeout from DB if it fails then tries property file
    public static Integer getFailedLoginTimeoutLimit() {
        String columnName = "LogonAttemptsForTimeout";
        Integer configProperty = null;
        int defaultPropertyValue = 5;
        try {
            ArrayList propertyList = null;
            // Do NOT run on Gateway
            if(!isGateway()){
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                propertyList = dm.parameterizedExecuteQuery("data.globalSettings.getLogonAttemptsForTimeout",
                        new Object[]{},
                        true
                );
            }

            if (propertyList != null && propertyList.size() >= 1) {
                HashMap resultHM = (HashMap) propertyList.get(0);
                configProperty = Integer.parseInt(resultHM.get(columnName.toUpperCase()).toString());
            } else {
                Logger.logMessage("Could not determine "+ columnName +" from DB in CommonAPI.getFailedLoginTimeoutLimit() - trying fallback now...", Logger.LEVEL.WARNING);
                configProperty = getFailedLoginTimeoutLimitFromPropFile();
            }

            if (configProperty == null) {
                configProperty = defaultPropertyValue;
            }

        } catch (Exception ex) {
            Logger.logMessage("Exception caught trying to use "+ columnName +" fallback in CommonAPI.getFailedLoginTimeoutLimit()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            configProperty = defaultPropertyValue;
            Logger.logMessage("Defaulting " + columnName + " to " + defaultPropertyValue + ".", Logger.LEVEL.WARNING);
        }
        return configProperty;
    }

    //reads property file for the number of seconds a user is timed out
    public static Integer getFailedLoginTimeoutLimitFromPropFile() {
        try {
            String allowedAttemptsBeforeTimeout = MMHProperties.getAppSetting("site.security.logonattemptsfortimeout");
            if ((allowedAttemptsBeforeTimeout == null) || (allowedAttemptsBeforeTimeout.length() == 0)) {
                allowedAttemptsBeforeTimeout = "3";
                //See B-06042
                Logger.logMessage("DEFAULT: site.security.logonattemptsfortimeout: "+allowedAttemptsBeforeTimeout, Logger.LEVEL.DEBUG);
            }
            return Integer.parseInt(allowedAttemptsBeforeTimeout);
        } catch (Exception ex) { // Just in case the failed login timeout limit does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 3;
        }
    }

    //attempts to determine the base login time out in seconds from DB if it fails then tries property file
    public static Integer getFailedLoginTimeoutSeconds() {
        String columnName = "LogonTimeoutSeconds";
        Integer configProperty = null;
        int defaultPropertyValue = 5;
        try {
            ArrayList propertyList = null;
            // Do NOT run on Gateway
            if(!isGateway()){
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                propertyList = dm.parameterizedExecuteQuery("data.globalSettings.getLogonTimeoutSeconds",
                        new Object[]{},
                        true
                );
            }

            if (propertyList != null && propertyList.size() >= 1) {
                HashMap resultHM = (HashMap) propertyList.get(0);
                configProperty = Integer.parseInt(resultHM.get(columnName.toUpperCase()).toString());
            } else {
                Logger.logMessage("Could not determine "+ columnName +" from DB in CommonAPI.getFailedLoginTimeoutSeconds() - trying fallback now...", Logger.LEVEL.WARNING);
                configProperty = getFailedLoginTimeoutSecondsFromPropFile();
            }

            if (configProperty == null) {
                configProperty = defaultPropertyValue;
            }

        } catch (Exception ex) {
            Logger.logMessage("Exception caught trying to use "+ columnName +" fallback in CommonAPI.getFailedLoginTimeoutSeconds()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            configProperty = defaultPropertyValue;
            Logger.logMessage("Defaulting " + columnName + " to " + defaultPropertyValue + ".", Logger.LEVEL.WARNING);
        }
        return configProperty;
    }

    //reads property file for the base login time out in seconds
    public static Integer getFailedLoginTimeoutSecondsFromPropFile() {
        try {
            String timeoutSeconds = MMHProperties.getAppSetting("site.security.logontimeoutseconds");
            if ((timeoutSeconds == null) || (timeoutSeconds.length() == 0)) {
                timeoutSeconds = "3"; //three second default timeout
            }
            return Integer.parseInt(timeoutSeconds);
        } catch (Exception ex) { // Just in case the failed login timeout seconds does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 3;
        }
    }

    //attempts to determine the number of tries before getting locked out from DB if it fails then tries property file
    public static Integer getFailedLoginLimit() {
        String columnName = "LogonAttempts";
        Integer configProperty = null;
        int defaultPropertyValue = 5;
        try {
            ArrayList propertyList = null;
            // Do NOT run on Gateway
            if(!isGateway()){
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                propertyList = dm.parameterizedExecuteQuery("data.globalSettings.getLogonAttempts",
                        new Object[]{},
                        true
                );
            }

            if (propertyList != null && propertyList.size() >= 1) {
                HashMap resultHM = (HashMap) propertyList.get(0);
                configProperty = Integer.parseInt(resultHM.get(columnName.toUpperCase()).toString());
            } else {
                Logger.logMessage("Could not determine "+ columnName +" from DB in CommonAPI.getFailedLoginLimit() - trying fallback now...", Logger.LEVEL.WARNING);
                configProperty = getFailedLoginLimitFromPropFile();
            }

            if (configProperty == null) {
                configProperty = defaultPropertyValue;
            }

        } catch (Exception ex) {
            Logger.logMessage("Exception caught trying to use "+ columnName +" fallback in CommonAPI.getFailedLoginLimit()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            configProperty = defaultPropertyValue;
            Logger.logMessage("Defaulting " + columnName + " to " + defaultPropertyValue + ".", Logger.LEVEL.WARNING);
        }
        return configProperty;
    }

    //reads property file for the number of tries before getting locked out
    public static Integer getFailedLoginLimitFromPropFile() {
        try {
            String failedLoginLimit = MMHProperties.getAppSetting("site.security.logonattempts");
            if ((failedLoginLimit == null) || (failedLoginLimit.length() == 0)) {
                failedLoginLimit = "5";
                //See B-06042
                Logger.logMessage("DEFAULT: site.security.logonattempts: "+failedLoginLimit, Logger.LEVEL.DEBUG);
            }
            return Integer.parseInt(failedLoginLimit);
        } catch (Exception ex) { // Just in case the failed login limit does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 5;
        }
    }

    //attempts to determine the number of tries before an IP address get's black listed from DB if it fails then tries property file
    public static Integer getIPBlackListAttempts() {
        String columnName = "BlackListAttempts";
        Integer configProperty = null;
        int defaultPropertyValue = 100;
        try {
            ArrayList propertyList = null;
            // Do NOT run on Gateway
            if(!isGateway()){
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                propertyList = dm.parameterizedExecuteQuery("data.globalSettings.getBlackListAttempts",
                        new Object[]{},
                        true
                );
            }

            if (propertyList != null && propertyList.size() >= 1) {
                HashMap resultHM = (HashMap) propertyList.get(0);
                configProperty = Integer.parseInt(resultHM.get(columnName.toUpperCase()).toString());
            } else {
                Logger.logMessage("Could not determine "+ columnName +" from DB in CommonAPI.getIPBlackListAttempts() - trying fallback now...", Logger.LEVEL.WARNING);
                configProperty = getIPBlackListAttemptsFromPropFile();
            }

            if (configProperty == null) {
                configProperty = defaultPropertyValue;
            }

        } catch (Exception ex) {
            Logger.logMessage("Exception caught trying to use "+ columnName +" fallback in CommonAPI.getIPBlackListAttempts()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            configProperty = defaultPropertyValue;
            Logger.logMessage("Defaulting " + columnName + " to " + defaultPropertyValue + ".", Logger.LEVEL.WARNING);
        }
        return configProperty;
    }

    //reads property file for the number of tries before an IP address get's black listed
    public static Integer getIPBlackListAttemptsFromPropFile() {
        try {
            String ipBlackListAttempts = MMHProperties.getAppSetting("site.security.ip.blacklistattempts");
            if ((ipBlackListAttempts == null) || (ipBlackListAttempts.length() == 0)) {
                ipBlackListAttempts = "100"; //100 failed tries in an hour (3600 seconds) is the default to ban an IP
            }
            return Integer.parseInt(ipBlackListAttempts);
        } catch (Exception ex) { // Just in case the black list attempts does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 100;
        }
    }

    //attempts to determine the duration (in seconds) of IP address attempts before an IP get's black listed from DB if it fails then tries property file
    public static Integer getIPBlackListSeconds() {
        String columnName = "BlackListSeconds";
        Integer configProperty = null;
        int defaultPropertyValue = 3600;
        try {
            ArrayList propertyList = null;
            // Do NOT run on Gateway
            if(!isGateway()){
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                propertyList = dm.parameterizedExecuteQuery("data.globalSettings.getBlackListSeconds",
                        new Object[]{},
                        true
                );
            }

            if (propertyList != null && propertyList.size() >= 1) {
                HashMap resultHM = (HashMap) propertyList.get(0);
                configProperty = Integer.parseInt(resultHM.get(columnName.toUpperCase()).toString());
            } else {
                Logger.logMessage("Could not determine "+ columnName +" from DB in CommonAPI.getIPBlackListSeconds() - trying fallback now...", Logger.LEVEL.WARNING);
                configProperty = getIPBlackListSecondsFromPropFile();
            }

            if (configProperty == null) {
                configProperty = defaultPropertyValue;
            }

        } catch (Exception ex) {
            Logger.logMessage("Exception caught trying to use "+ columnName +" fallback in CommonAPI.getIPBlackListSeconds()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            configProperty = defaultPropertyValue;
            Logger.logMessage("Defaulting " + columnName + " to " + defaultPropertyValue + ".", Logger.LEVEL.WARNING);
        }
        return configProperty;
    }

    //reads property file for the duration (in seconds) of IP address attempts before an IP get's black listed
    public static Integer getIPBlackListSecondsFromPropFile() {
        try {
            String ipBlackListSeconds = MMHProperties.getAppSetting("site.security.ip.blacklistseconds");
            if ((ipBlackListSeconds == null) || (ipBlackListSeconds.length() == 0)) {
                ipBlackListSeconds = "3600"; //100 failed tries in an hour (3600 seconds) is the default to ban an IP
            }
            return Integer.parseInt(ipBlackListSeconds);
        } catch (Exception ex) { // Just in case the black list seconds does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 3600;
        }
    }

    //attempts to determine the length of the session (in minutes) from DB if it fails then tries property file
    public static Integer getSessionTimeoutMinutes() {
        String columnName = "SessionTimeoutMinutes";
        Integer configProperty = null;
        int defaultPropertyValue = 30;
        try {
            ArrayList propertyList = null;
            // Do NOT run on Gateway
            if(!isGateway()){
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                propertyList = dm.parameterizedExecuteQuery("data.globalSettings.getSessionTimeoutMinutes",
                        new Object[]{},
                        true
                );
            }

            if (propertyList != null && propertyList.size() >= 1) {
                HashMap resultHM = (HashMap) propertyList.get(0);
                configProperty = Integer.parseInt(resultHM.get(columnName.toUpperCase()).toString());
            } else {
                Logger.logMessage("Could not determine "+ columnName +" from DB in CommonAPI.getSessionTimeoutMinutes() - trying fallback now...", Logger.LEVEL.WARNING);
                configProperty = getSessionTimeoutMinutesFromPropFile();
            }

            if (configProperty == null) {
                configProperty = defaultPropertyValue;
            }

        } catch (Exception ex) {
            Logger.logMessage("Exception caught trying to use "+ columnName +" fallback in CommonAPI.getSessionTimeoutMinutes()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            configProperty = defaultPropertyValue;
            Logger.logMessage("Defaulting " + columnName + " to " + defaultPropertyValue + ".", Logger.LEVEL.WARNING);
        }
        return configProperty;
    }

    //reads property file for the length of the session (in minutes)
    public static Integer getSessionTimeoutMinutesFromPropFile() {
        try {
            String sessionTimeoutMins = MMHProperties.getAppSetting("site.security.sessiontimeoutminutes");
            if ((sessionTimeoutMins == null) || (sessionTimeoutMins.length() == 0)) {
                sessionTimeoutMins = "30"; //default to 30 minutes
            }
            return Integer.parseInt(sessionTimeoutMins);
        } catch (Exception ex) { // Just in case the session timeout minutes does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 30;
        }
    }

    //reads property file for "From:" email header to send in the password reset email
    public static String getFromAddress() {
        String fromEmailAddress = MMHProperties.getAppSetting("site.communication.email.fromaddress");

        if (fromEmailAddress == null || fromEmailAddress.length() == 0) {
            try {
                Object fromAddress = dm.parameterizedExecuteScalar("data.common.getFromAddress", new Object[]{});

                if (fromAddress == null) {
                    fromEmailAddress = "noreply@mmhcloud.com"; //default to noreply@mmhcloud.com
                } else {
                    fromEmailAddress = fromAddress.toString();
                }
            } catch (Exception ex) {
                Logger.logException(ex);
                return "noreply@mmhcloud.com";
            }
        }
        return fromEmailAddress;
    }

    //reads property file for "ReplyTo:" email header to send in email
    public static String getReplyToAddress() {
        String replyToAddress = MMHProperties.getAppSetting("site.communication.email.replytoaddress");

        if (replyToAddress == null || replyToAddress.length() == 0) {
            try {
                Object replyTo = null;
                if(!isGateway()){
                   replyTo = dm.parameterizedExecuteScalar("data.common.getReplyToAddress", new Object[]{});
                }
                if (replyTo == null) {
                    replyToAddress = "";
                } else {
                    replyToAddress = replyTo.toString();
                }
            } catch (Exception ex) {
                Logger.logException(ex);
                replyToAddress = "";
            }
        }
        return replyToAddress;
    }

    //reads property file for the email server's host name to send the e-mail from (using SMTP)
    public static String getEmailSMTPHost() {
        String emailSMTPHost = MMHProperties.getAppSetting("site.communication.email.smtp.host");
        if (emailSMTPHost == null || emailSMTPHost.length() == 0) {
            try {
                Object emailHost = dm.parameterizedExecuteScalar("data.common.getEmailSMTPHost", new Object[]{});

                if (emailHost == null) {
                    emailSMTPHost = "localhost"; //default to localhost
                } else {
                    emailSMTPHost = emailHost.toString();
                }
            } catch (Exception ex) {
                Logger.logException(ex);
                return "localhost";
            }
        }
        return emailSMTPHost;
    }

    //reads property file for the email server's port (using SMTP)
    public static String getEmailSMTPPort() {
        String emailSMTPPort = MMHProperties.getAppSetting("site.communication.email.smtp.port");
        if (emailSMTPPort == null || emailSMTPPort.length() == 0) {
            try {
                Object emailPort = dm.parameterizedExecuteScalar("data.common.getEmailSMTPPort", new Object[]{});

                if (emailPort == null) {
                    emailSMTPPort = "25"; //default to port 25
                } else {
                    emailSMTPPort = emailPort.toString();
                }
            } catch (Exception ex) {
                Logger.logException(ex);
                return "25";
            }
        }
        return emailSMTPPort;
    }

    //reads property file for the SMTP User for SMTP Authentication
    public static String getEmailSMTPUser() {
        String emailSMTPUser = MMHProperties.getAppSetting("site.communication.email.smtp.user");
        if (emailSMTPUser == null || emailSMTPUser.length() == 0) {
            try {
                Object emailUser = dm.parameterizedExecuteScalar("data.common.getEmailSMTPUser", new Object[]{});

                if (emailUser == null) {
                    emailSMTPUser = "";
                } else {
                    emailSMTPUser = emailUser.toString();
                }
            } catch (Exception ex) {
                Logger.logException(ex);
                return "N/A";
            }
        }
        return emailSMTPUser;
    }

    //reads property file for the SMTP Password for SMTP Authentication
    public static String getEmailSMTPPassword() {
        String emailSMTPPass = MMHProperties.getAppSetting("site.communication.email.smtp.pass");
        if (emailSMTPPass == null || emailSMTPPass.length() == 0) {
            try {
                Object emailPass = dm.parameterizedExecuteScalar("data.common.getEmailSMTPPassword", new Object[]{});

                if (emailPass == null) {
                    emailSMTPPass = "";
                } else {
                    emailSMTPPass = StringFunctions.decodePassword(emailPass.toString());
                }
            } catch (Exception ex) {
                Logger.logException(ex);
                return "N/A";
            }
        }
        return emailSMTPPass;
    }

    //reads property file for the invite link the user should be sent to their e-mail
    public static String getInstanceURL() {
        String instanceURL = MMHProperties.getAppSetting("site.communication.URL");
        if ((instanceURL == null) || (instanceURL.length() == 0)) {
            instanceURL = "N/A"; //default to N/A
        }
        return instanceURL;
    }

    //reads property file for the invite link the user should be sent to their e-mail
    public static Long getMaxFIleSizeMB() {
        String maxsize = MMHProperties.getAppSetting(PROPERTY_MAXFILESIZEMB);
        Long result = 5L;
        if ((maxsize != null) && (maxsize.length() > 0)) {
            try{
                result = Long.parseLong(maxsize);
            }catch(Exception ex){
                Logger.logMessage("Error in method: getMaxFileSizeMB()", Logger.LEVEL.ERROR);
                Logger.logException(ex);
            }
        }
        return result;
    }

    //reads property file for the password reset link the user should be sent from their e-mail
    public static String getGatewayURL() {
        String gatewayURL = MMHProperties.getAppSetting("site.communication.URL");
        if ((gatewayURL == null) || (gatewayURL.length() == 0)) {
            gatewayURL = "https://www.mmhcloud.com"; //default to http://gateway.MMHcloud.com
        }
        return gatewayURL;
    }

    //attempts to determine the duration (in hours) of how long before a invite key is invalid from DB if it fails then tries property file
    public static Integer getInviteKeyHours() {
        String columnName = "InviteKeyHours";
        Integer configProperty = null;
        int defaultPropertyValue = 731;
        try {
            ArrayList propertyList = null;
            // Do NOT run on Gateway
            if(!isGateway()){
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                propertyList = dm.parameterizedExecuteQuery("data.globalSettings.getInviteKeyHours",
                        new Object[]{},
                        true
                );
            }

            //check IP blacklist to determine if this IP of this request is allowed to attempt to login
            if (propertyList != null && propertyList.size() >= 1) {
                HashMap resultHM = (HashMap) propertyList.get(0);
                configProperty = Integer.parseInt(resultHM.get(columnName.toUpperCase()).toString());
            } else {
                Logger.logMessage("Could not determine "+ columnName +" from DB in CommonAPI.getInviteKeyHours() - trying fallback now...", Logger.LEVEL.WARNING);
                configProperty = getInviteKeyHoursFromPropFile();
            }

            if (configProperty == null) {
                configProperty = defaultPropertyValue;
            }

        } catch (Exception ex) {
            Logger.logMessage("Exception caught trying to use "+ columnName +" fallback in CommonAPI.getInviteKeyHours()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            configProperty = defaultPropertyValue;
            Logger.logMessage("Defaulting " + columnName + " to " + defaultPropertyValue + ".", Logger.LEVEL.WARNING);
        }
        return configProperty;
    }

    //reads property file for the duration (in hours) of how long before a invite key is invalid
    public static Integer getInviteKeyHoursFromPropFile() {
        try {
            String inviteKeyHours = MMHProperties.getAppSetting("site.security.invitekeyhours");
            if ((inviteKeyHours == null) || (inviteKeyHours.length() == 0)) {
                inviteKeyHours = "731"; //731 hours (little over a month) until invite key is invalid
            }
            return Integer.parseInt(inviteKeyHours);
        } catch (Exception ex) { // Just in case the invite key hours does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 731;
        }
    }

    //attempts to determine the duration (in hours) of how long before a reset key is invalid from DB if it fails then tries property file
    public static Integer getResetKeyHours() {
        String columnName = "ResetKeyHours";
        Integer configProperty = null;
        int defaultPropertyValue = 24;
        try {
            ArrayList propertyList = null;
            // Do NOT run on Gateway
            if(!isGateway()){
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                propertyList = dm.parameterizedExecuteQuery("data.globalSettings.getResetKeyHours",
                        new Object[]{},
                        true
                );
            }

            //check IP blacklist to determine if this IP of this request is allowed to attempt to login
            if (propertyList != null && propertyList.size() >= 1) {
                HashMap resultHM = (HashMap) propertyList.get(0);
                configProperty = Integer.parseInt(resultHM.get(columnName.toUpperCase()).toString());
            } else {
                Logger.logMessage("Could not determine "+ columnName +" from DB in CommonAPI.getResetKeyHours() - trying fallback now...", Logger.LEVEL.WARNING);
                configProperty = getResetKeyHoursFromPropFile();
            }

            if (configProperty == null) {
                configProperty = defaultPropertyValue;
            }

        } catch (Exception ex) {
            Logger.logMessage("Exception caught trying to use "+ columnName +" fallback in CommonAPI.getResetKeyHours()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            configProperty = defaultPropertyValue;
            Logger.logMessage("Defaulting " + columnName + " to " + defaultPropertyValue + ".", Logger.LEVEL.WARNING);
        }
        return configProperty;
    }

    //reads property file for the duration (in hours) of how long before a reset key is invalid
    public static Integer getResetKeyHoursFromPropFile() {
        try {
            String resetKeyHours = MMHProperties.getAppSetting("site.security.resetkeyhours");
            if ((resetKeyHours == null) || (resetKeyHours.length() == 0)) {
                resetKeyHours = "24"; //24 hours until reset key is invalid
            }
            return Integer.parseInt(resetKeyHours);
        } catch (Exception ex) { // Just in case the reset key hours does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 24;
        }
    }

    //reads property file for the work factor for BCrypt encryption
    public static Integer getWorkFactor() {
        try {
            String workFactor = MMHProperties.getAppSetting("site.security.workfactor");
            if ((workFactor == null) || (workFactor.length() == 0)) {
                workFactor = "10"; //10 is the default work factor
            }
            return Integer.parseInt(workFactor);
        } catch (Exception ex) { // Just in case the work factor does not parse as an integer
            Logger.logException(ex); //always log exceptions
            return 10;
        }
    }

    // Encrypts a password (one-way) using BCrypt
    public static String encryptPassword(String plainPassword) {
        return BCrypt.HashPassword(plainPassword, BCrypt.GenerateSalt(getWorkFactor()));
    }

    //sends email to cloudops@mmhayes.com warning them of a blacklisted ip address!
    private static Boolean sendBlackListNotification(String blackListedIPAddress) {
        try {

            //email address to send the email to
            String toAddress = "cloudops@mmhayes.com";

            //email address the email will be sent from
            String fromAddress = getFromAddress();

            //subject of the email
            String subject = "WARNING! Recently blacklisted IP Address (" + blackListedIPAddress + ")!";

            //body of the email - HTML is valid
            String htmlBody = "Hello cloud operations team,"
                    + "<br/><br/> An IP Address has just been black listed for suspicious activity!"
                    + "<br/><br/> IP Address in question: <b>" + blackListedIPAddress + "</b>"
                    + "<br/><br/> Gateway ID: <b>" + getGatewayID() + "</b>"
                    + "<br/><br/> Instance ID: <b>" + getInstanceID() + "</b>"
                    + "<br/><br/> Check all logs and overall stability on this gateway!"
                    + "<br/><br/> Thank you,"
                    + "<br/> MMHayes";

            //send email and return whether or not the email was successful
            return MMHEmail.sendEmail(toAddress, fromAddress, subject, htmlBody);
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Invite.sendBlackListNotification - Exception thrown: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            return false;
        }
    }

    // Sets session variables that are on the white-list, returns success
    public static Boolean setSessionParameters(HttpServletRequest request, HashMap sessionParams) {
        try {
            HttpSession session = request.getSession();
            // Convert the HashMap to explicit Strings
            Map<String, String> params = sessionParams;
            // Iterate over the given variables
            for (Map.Entry param : params.entrySet()) {
                // Only use the variable if it's in the white list of session attributes
                if (Arrays.asList(sessionAttributeWhiteList).contains(param.getKey().toString())) {
                    // Add the variable as a session attribute
                    session.setAttribute(param.getKey().toString(), param.getValue().toString());
                }
            }
            return true;
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            Logger.logMessage("CommonAPI.setSessionParameters - Exception thrown: " + sw.toString(), Logger.LEVEL.ERROR); //log details
            return false;
        }
    }

    // Retrieves variables about the session into a HashMap
    public static HashMap getSessionParameters(HttpServletRequest request) {
        // Get data about the user for the front-end
        HashMap frontEndVariables = new HashMap();
        try {
            Long userId = commonMMHFunctions.getUserId(request);
            ArrayList frontEndVariableList = dm.serializeSqlWithColNames("data.session.getSessionParams", new Object[]{userId}, false, true);
            if (frontEndVariableList != null && frontEndVariableList.size() == 1) {
                frontEndVariables = (HashMap) frontEndVariableList.get(0);
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            Logger.logMessage("CommonAPI.getSessionParameters - Exception thrown: " + sw.toString(), Logger.LEVEL.ERROR); //log details
        }
        return frontEndVariables;
    }

    // Invalidates a session while retaining white list attributes
    public static HttpSession createOrReplaceSession(HttpServletRequest request) {
        // Get the existing session or create a new one
        HttpSession session = request.getSession();
        try {
            // Create a new session if the client already has one (prevents session fixation)
            if (!session.isNew()) {
                // Cache particular attributes that carry over from sessions
                Enumeration sessionAttributeNames = session.getAttributeNames();
                HashMap<String, Object> cacheSessionAttributes = new HashMap<String, Object>();
                // Iterate over the existing session attributes
                while (sessionAttributeNames.hasMoreElements()) {
                    String attr = sessionAttributeNames.nextElement().toString();
                    if (Arrays.asList(sessionAttributeWhiteList).contains(attr)) {
                        // Only cache the attributes that are in the white list
                        cacheSessionAttributes.put(attr, session.getAttribute(attr));
                    }
                }
                // Destroy the existing session
                session.invalidate();
                // Create a new session
                session = request.getSession();
                // Set default session attributes
                session.setMaxInactiveInterval(getSessionTimeoutMinutes() * 60); //convert getSessionTimeoutMinutes() to seconds
                // Set previously cached session attributes
                for (Map.Entry<String, Object> entry : cacheSessionAttributes.entrySet()) {
                    session.setAttribute(entry.getKey(), entry.getValue());
                }
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            Logger.logMessage("CommonAPI.recreateSession - Exception thrown: " + sw.toString(), Logger.LEVEL.ERROR); //log details
        }
        return session;
    }

    //checks the session for the logged in user's ID and returns the sessions userID
    public static String checkSession(HttpServletRequest request) {
        try {
            Logger.logMessage("Running method CommonAPI.checkSession...", Logger.LEVEL.DEBUG);
            String userID;
            HttpSession session = request.getSession();
            if (session.getAttribute("MMH_USERID") != null) {
                userID = session.getAttribute("MMH_USERID").toString();
                return userID;
            } else if (session.getAttribute("QC_USERID") != null) {
                userID = session.getAttribute("QC_USERID").toString();
                return userID;
            } else {
                return "";
            }
        } catch (Exception ex) {
            Logger.logMessage("Checking the user's session.", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return "";
        }
    }

    //checks the Windows Server for the hostname using environment variables
    public static String getWindowsHostName() {
        try {

            //check HOSTNAME system environment variable
            String hostname = System.getenv().get("HOSTNAME");
            if (hostname != null && !hostname.trim().equals("")) {
                return hostname;
            }

            //check COMPUTERNAME system environment variable
            hostname = System.getenv().get("COMPUTERNAME");
            if (hostname != null && !hostname.trim().equals("")) {
                return hostname;
            }

        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            Logger.logMessage("CommonAPI.getWindowsHostName - Exception thrown: " + sw.toString(), Logger.LEVEL.ERROR); //log details
        }
        return "N/A";
    }

    //populates the passed in paginationParamsHM with HTTP GET parameters in the uriInfo
    public static HashMap<String, LinkedList> populatePaginationParamsFromURI(@Context UriInfo uriInfo) {
        HashMap paginationParamsHM = new HashMap<String, LinkedList>();
        try {
            MultivaluedMap<String, String> httpGetParams = uriInfo.getQueryParameters();
            if (httpGetParams.get("models") != null) {
                paginationParamsHM.put("MODELS", httpGetParams.get("models"));
            }
            if (httpGetParams.get("page") != null) {
                paginationParamsHM.put("PAGE", httpGetParams.get("page"));
            }
            if (httpGetParams.get("sort") != null) {
                paginationParamsHM.put("SORT", httpGetParams.get("sort"));
            }
            if (httpGetParams.get("order") != null) {
                paginationParamsHM.put("ORDER", httpGetParams.get("order"));
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return paginationParamsHM;
    }

    //logs the acception of a TOS for the given account (employeeID) - the userID is for when a manager (qc userID) accepts the TOS on behalf of an employee
    public static Boolean logTOSAcception(Integer tosID, Integer employeeID, Integer userID, HttpServletRequest request) {
        try {
            //Logger.logMessage("Running method CommonAPI.logTOSAcception...");

            String ipAddress = comm.getEndUserIPAddress(request);
            String userAgent = request.getHeader("User-Agent");

            //if the mmhayesAuthHeader is valid, verify the contents of it (Device Session Key)
            if (tosID != null && employeeID != null && ipAddress != null) {

                if (tosID == 0 || employeeID == 0) { //verify valid tosID and employeeI
                    Logger.logMessage("Invalid tosID or employeeID in CommonAuthResource.logTOSAcception", Logger.LEVEL.ERROR);
                }

                //insert history record into QC_TOSAcceptedLog
                Integer insertResult = dm.parameterizedExecuteNonQuery("data.common.logTOSAcception",
                        new Object[]{
                                tosID,
                                employeeID,
                                userID,
                                ipAddress,
                                userAgent
                        }
                );

                //record success in response
                if (insertResult != null) {
                    return true;
                } else {
                    Logger.logMessage("Could not record in DB in CommonAuthResource.logTOSAcception", Logger.LEVEL.ERROR);
                }
            } else {
                Logger.logMessage("Could not determine employeeID or IPAddress or tosID in CommonAuthResource.logTOSAcception", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return false;
    }

    //grabs the client version from the request header, sets it on the logged in account's record and returns it
    public static String checkRequestHeaderForClientVersion(HttpServletRequest request, Integer accountID) throws Exception {
        String clientVersion = request.getHeader("X-MMHayes-AppVersion");
        if (clientVersion == null || clientVersion.isEmpty()) {
            throw new MissingDataException("Could not find X-MMHayes-AppVersion in HTTP Header");
        }

        //set the clientVersion on the account's record in QC_Employees
        Integer updateResult = dm.parameterizedExecuteNonQuery("data.myqc.setClientVersion",
                new Object[]{
                        accountID,
                        clientVersion
                }
        );
        if (updateResult == 1) {
            return clientVersion;
        } else {
            Logger.logMessage("Could not set client version in CommonAuthResource.checkRequestHeaderForClientVersion.", Logger.LEVEL.ERROR);
        }

        return clientVersion;
    }

    //grabs the client version from the request header, sets it on the logged in account's record and returns it
    public static Integer checkRequestHeaderForStoreID(HttpServletRequest request) throws Exception {
        String storeID = request.getHeader("X-MMHayes-StoreID");
        if (storeID == null || storeID.isEmpty()) {
            throw new MissingDataException("Could not find X-MMHayes-StoreID in HTTP Header");
        }
        return Integer.parseInt(storeID);
    }

    //converts a single model detail from a generic object to an Integer object
    public static Integer convertModelDetailToInteger(Object modelDetailObj) {
        return convertModelDetailToInteger(modelDetailObj, null);
    }

    //OVERLOAD - converts a single model detail from a generic object to an Integer object
    public static Integer convertModelDetailToInteger(Object modelDetailObj, Integer integerDefault) {
        return convertModelDetailToInteger(modelDetailObj, null, false);
    }

    //OVERLOAD - converts a single model detail from a generic object to an Integer object
    public static Integer convertModelDetailToInteger(Object modelDetailObj, Integer integerDefault, Boolean removeTrailingZeros) {
        try {
            Integer modelDetailAsInteger = integerDefault; //set default
            if (modelDetailObj != null) {
                String modelDetailAsStr = modelDetailObj.toString().trim();
                if (!modelDetailAsStr.isEmpty()) {

                    if (removeTrailingZeros) { //removes all trailing zeros if set ON
                        modelDetailAsStr = !modelDetailAsStr.contains(".") ? modelDetailAsStr : modelDetailAsStr.replaceAll("0*$", "").replaceAll("\\.$", "");
                    }

                    modelDetailAsInteger = Integer.parseInt(modelDetailAsStr);
                }
            }
            return modelDetailAsInteger;
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return null;
        }
    }

    //converts a single model detail from a generic object to a String object
    public static String convertModelDetailToString(Object modelDetailObj) {
        return convertModelDetailToString(modelDetailObj, "");
    }

    //OVERLOAD - converts a single model detail from a generic object to a String object
    public static String convertModelDetailToString(Object modelDetailObj, String stringDefault) {
        try {
            String modelDetailAsStr = stringDefault; //set default
            if (modelDetailObj != null) {
                modelDetailAsStr = modelDetailObj.toString();
            }
            return modelDetailAsStr;
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return null;
        }
    }

    //converts a single model detail from a generic object to a Boolean object
    public static Boolean convertModelDetailToBoolean(Object modelDetailObj) {
        return convertModelDetailToBoolean(modelDetailObj, null);
    }

    //OVERLOAD - converts a single model detail from a generic object to a Boolean object
    public static Boolean convertModelDetailToBoolean(Object modelDetailObj, Boolean booleanDefault) {
        try {
            Boolean modelDetailAsBoolean = booleanDefault; //default
            if (modelDetailObj != null) {
                String modelDetailAsStr = modelDetailObj.toString();
                if (!modelDetailAsStr.isEmpty()) {
                    modelDetailAsBoolean = Boolean.parseBoolean(modelDetailAsStr);
                }
            }
            return modelDetailAsBoolean;
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return null;
        }
    }

    //converts a single model detail from a generic object to a BigDecimal object
    public static BigDecimal convertModelDetailToBigDecimal(Object modelDetailObj) {
        return convertModelDetailToBigDecimal(modelDetailObj, null);
    }

    //OVERLOAD - converts a single model detail from a generic object to a BigDecimal object
    public static BigDecimal convertModelDetailToBigDecimal(Object modelDetailObj, BigDecimal bigDecimalDefault) {
        try {
            BigDecimal modelDetailAsBigDecimal = bigDecimalDefault; //default
            if (modelDetailObj != null) {
                String modelDetailAsStr = modelDetailObj.toString();
                if (!modelDetailAsStr.isEmpty()) {
                    modelDetailAsBigDecimal = new BigDecimal(modelDetailAsStr);
                }
            }
            return modelDetailAsBigDecimal;
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return null;
        }
    }

    //converts a single model detail from a generic object to a String object
    public static ArrayList<HashMap> convertModelDetailToArrayListOfHashMaps(Object modelDetailObj) {
        return convertModelDetailToArrayListOfHashMaps(modelDetailObj, null);
    }

    //OVERLOAD - converts a single model detail from a generic object to a String object
    public static ArrayList<HashMap> convertModelDetailToArrayListOfHashMaps(Object modelDetailObj, ArrayList<HashMap> arrayListOfHashMapDefault) {
        try {
            ArrayList<HashMap> modelDetailAsArrOfHM = arrayListOfHashMapDefault; //set default
            if (modelDetailObj != null) {
                modelDetailAsArrOfHM = (ArrayList<HashMap>) modelDetailObj;
            }
            return modelDetailAsArrOfHM;
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return null;
        }
    }

    //converts a single model detail from a generic object to a String object
    public static ArrayList<String> convertModelDetailToArrayListOfStrings(Object modelDetailObj) {
        return convertModelDetailToArrayListOfStrings(modelDetailObj, null);
    }

    //OVERLOAD - converts a single model detail from a generic object to a String object
    public static ArrayList<String> convertModelDetailToArrayListOfStrings(Object modelDetailObj, ArrayList<String> arrayListOfHashMapDefault) {
        try {
            ArrayList<String> modelDetailAsArrOfHM = arrayListOfHashMapDefault; //set default
            if (modelDetailObj != null) {
                modelDetailAsArrOfHM = (ArrayList<String>) modelDetailObj;
            }
            return modelDetailAsArrOfHM;
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return null;
        }
    }

    //verifies that any given "newVersion" is truly "newer" (greater than) or the "same" (equal to) to any given "oldVersion"
    public static Boolean verifyVersionIsSameOrGreater(String newVersion, String oldVersion) throws Exception {

        //ensure valid versions are passed
        if (newVersion == null || newVersion.isEmpty() || oldVersion == null || oldVersion.isEmpty()) {
            throw new MissingDataException("Could not find two valid versions to compare.");
        }

        //initialize newVersion variables
        Integer newMajorVersion = null; //required
        Integer newMinorVersion = null; //required
        Integer newServicePack = -1; //optional
        Integer newBuildToCheck = -1; //optional

        //split apart the newVersion string and determine the integer values for each piece
        String[] newVersionSplit = newVersion.split("\\.");
        if (0 < newVersionSplit.length && newVersionSplit[0] != null && !newVersionSplit[0].isEmpty()) {
            newMajorVersion = Integer.parseInt(newVersionSplit[0]);
        } else { //log because we should at least have a major/minor version in this method
            Logger.logMessage("Cannot determine New Major Version in CommonAPI.isVersionNewerOrEqual(newVersion, oldVersion)", Logger.LEVEL.ERROR);
        }
        if (1 < newVersionSplit.length && newVersionSplit[1] != null && !newVersionSplit[1].isEmpty()) {
            newMinorVersion = Integer.parseInt(newVersionSplit[1]);
        } else { //log because we should at least have a major/minor version in this method
            Logger.logMessage("Cannot determine New Minor Version in CommonAPI.isVersionNewerOrEqual(newVersion, oldVersion)", Logger.LEVEL.ERROR);
        }
        if (2 < newVersionSplit.length && newVersionSplit[2] != null && !newVersionSplit[2].isEmpty()) {
            newServicePack = Integer.parseInt(newVersionSplit[2]);
        }
        if (3 < newVersionSplit.length && newVersionSplit[3] != null && !newVersionSplit[3].isEmpty()) {
            newBuildToCheck = Integer.parseInt(newVersionSplit[3]);
        }

        //initialize oldVersion variables
        Integer oldMajorVersion = null; //required
        Integer oldMinorVersion = null; //required
        Integer oldServicePack = -1; //optional
        Integer oldBuildToCheck = -1; //optional

        //split apart the oldVersion string and determine the integer values for each piece
        String[] oldVersionSplit = oldVersion.split("\\.");
        if (0 < oldVersionSplit.length && oldVersionSplit[0] != null && !oldVersionSplit[0].isEmpty()) {
            oldMajorVersion = Integer.parseInt(oldVersionSplit[0]);
        } else { //log because we should at least have a major/minor version in this method
            Logger.logMessage("Cannot determine Old Minor Version in CommonAPI.isVersionNewerOrEqual(newVersion, oldVersion)", Logger.LEVEL.ERROR);
        }
        if (1 < oldVersionSplit.length && oldVersionSplit[1] != null && !oldVersionSplit[1].isEmpty()) {
            oldMinorVersion = Integer.parseInt(oldVersionSplit[1]);
        } else { //log because we should at least have a major/minor version in this method
            Logger.logMessage("Cannot determine Old Minor Version in CommonAPI.isVersionNewerOrEqual(newVersion, oldVersion)", Logger.LEVEL.ERROR);
        }
        if (2 < oldVersionSplit.length && oldVersionSplit[2] != null && !oldVersionSplit[2].isEmpty()) {
            oldServicePack = Integer.parseInt(oldVersionSplit[2]);
        }
        if (3 < oldVersionSplit.length && oldVersionSplit[3] != null && !oldVersionSplit[3].isEmpty()) {
            oldBuildToCheck = Integer.parseInt(oldVersionSplit[3]);
        }

        //if any of the newVersions are greater than or equal to the oldVersions then return true
        if (newMajorVersion != null && oldMajorVersion != null && newMajorVersion <= oldMajorVersion) { //major version is lesser or equal, continue checking
            if (newMajorVersion.equals(oldMajorVersion)) { //major version is equal, continue checking
                if (newMinorVersion != null && oldMinorVersion != null && newMinorVersion <= oldMinorVersion) { //minor version is lesser or equal, continue checking
                    if (newMinorVersion.equals(oldMinorVersion)) { //minor version is equal, continue checking
                        if (newServicePack <= oldServicePack) { //service pack is lesser or equal, continue checking
                            if (newServicePack.equals(oldServicePack)) { //service pack is equal, continue checking
                                if (newBuildToCheck >= oldBuildToCheck) {
                                    return true; //major, minor and service pack are equal, new version build is greater OR equal than old version build, return true
                                }
                            }
                        } else if (newServicePack != null && oldServicePack != null) {
                            return true; //major and minor versions are equal, new version service pack is greater than old version service pack, return true
                        }
                    }
                } else if (newMinorVersion != null && oldMinorVersion != null) {
                    return true; //major version is equal, new minor version is greater than old version, return true
                }
            }
        } else if (newMajorVersion != null && oldMajorVersion != null) {
            return true; //new major version is greater, return true
        }
        return false; //the old version is "newer" (greater than) the new version
    }

    //checks if there is a My QC Instance ID for the instanceID passed in the requestValidation, sets if found
    public static Validation checkForMyQCInstance(Validation requestValidation, HttpServletRequest request) {
        try {

            String instanceID = requestValidation.getInstanceID().toString();
            String instanceUserID = requestValidation.getInstanceUserID().toString();
            Integer instanceUserTypeID = requestValidation.fetchInstanceUserTypeID();

            //check to make sure a valid instanceID and instanceUserID were passed
            if (instanceID != null && !instanceID.equals("") && instanceUserID != null && !instanceUserID.equals("")) {

                //if the instanceUserID is negative, then map to "My Quickcharge" instance (/myqc)
                if (requestValidation.getInstanceUserID() < 0 || (instanceUserTypeID != null && instanceUserTypeID == 3) ) {

                    //determine My QC instance for this QC instance
                    Integer MyQCInstanceID = null;

                    //check the instance record to see if there an associated "My Quickcharge" instance (/myqc)
                    Object MyQCInstanceIDObj = dm.getSingleField("data.validation.getMyQCInstanceID",
                            new Object[]{
                                    instanceID
                            },
                            false,
                            true
                    );

                    //see if anything was found in the db...
                    if (MyQCInstanceIDObj != null && !MyQCInstanceIDObj.equals("0")) {
                        MyQCInstanceID = Integer.parseInt(MyQCInstanceIDObj.toString()); //set My QC Instance ID
                    } else { //else log error
                        String errorDetails = "Could not determine My QC Instance ID in CommonAPI.checkForMyQCInstance(). InstanceID:" + requestValidation.getInstanceID() + " InstanceUserID: " + requestValidation.getInstanceUserID();
                        requestValidation.setErrorDetails(errorDetails);
                        Logger.logMessage(errorDetails, Logger.LEVEL.ERROR);
                    }

                    //double check that the MyQCInstanceID found is valid...
                    if (MyQCInstanceID != null && MyQCInstanceID != 0) {
                        Logger.logMessage("Found valid My QC Instance ID in CommonAPI.checkForMyQCInstance() - MyQCInstanceID: " + instanceID + " and instanceUserID:" + instanceUserID, Logger.LEVEL.DEBUG);
                        requestValidation.setInstanceID(MyQCInstanceID);
                    } else { //else log error
                        String errorDetails = "Could not verify My QC Instance ID CommonAPI.checkForMyQCInstance(). MyQCInstanceID:" + requestValidation.getInstanceID() + " InstanceUserID: " + requestValidation.getInstanceUserID();
                        requestValidation.setErrorDetails(errorDetails);
                        Logger.logMessage(errorDetails, Logger.LEVEL.ERROR);
                    }
                }
            }
        } catch (Exception ex) {
            requestValidation.handleGenericException(ex, "CommonAPI.checkForMyQCInstance");
            return null;
        }
        return requestValidation;
    }

    //checks a user's DSKey to determine if reauthorization before submitting a purchase is necessary
    public static boolean getUserReAuthStatus(HttpServletRequest request) {
        //defaults to true in db
        boolean requireAuth = true;
        try {
            //get custom header X-MMHayes-Auth from request object
            String mmhayesAuthHeader = request.getHeader("X-MMHayes-Auth");

            //if the mmhayesAuthHeader is valid, verify and set the reauth status
            if (mmhayesAuthHeader != null) {
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                ArrayList DSKeyReAuthStatusList = dm.serializeSqlWithColNames("data.ordering.getReAuthStatus",
                        new Object[]{
                                mmhayesAuthHeader,
                        },
                        false,
                        true
                );

                if (DSKeyReAuthStatusList != null && DSKeyReAuthStatusList.size() >= 1) { //verified DSKeyReAuthStatusList
                    HashMap DSKeyReAuthStatusHM = (HashMap) DSKeyReAuthStatusList.get(0);

                    if (DSKeyReAuthStatusHM.get("REQUIREAUTHONPURCHASE").equals("0") || DSKeyReAuthStatusHM.get("REQUIREAUTHONPURCHASE").toString().equals("false")) {
                        requireAuth = false;
                    }
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }
        return requireAuth;
    }

    //returns the ReAuthBeforePurchase setting on the MyQCTerminal
    public static Integer getTerminalReAuthType(HttpServletRequest request) {
        Integer result = 0;

        try {
            //get virtual terminal
            Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

            //if the mmhayesAuthHeader is valid, verify and set the reauth status
            if (storeID != null) {
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                ArrayList ReAuthSettingList = dm.parameterizedExecuteQuery("data.ordering.getTerminalReAuthSetting",
                        new Object[]{
                                storeID,
                        },
                        true
                );

                if (ReAuthSettingList != null && ReAuthSettingList.size() == 1) { //verified DSKeyReAuthStatusList
                    HashMap ReAuthSettingHM = (HashMap) ReAuthSettingList.get(0);

                    if (ReAuthSettingHM.containsKey("REAUTHTYPEID") && ReAuthSettingHM.get("REAUTHTYPEID") != null) {
                        result = Integer.parseInt(ReAuthSettingHM.get("REAUTHTYPEID").toString());
                    } else {
                        Logger.logMessage("Could not determine REAUTHTYPEID in CommonAPI.getTerminalReAuthSetting", Logger.LEVEL.ERROR);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        return result;
    }

    //gets and returns the user's balance for a given store
    public static HashMap getUserBalance(HttpServletRequest request, BigDecimal orderTotal) throws Exception {
        HashMap userBalanceHM = new HashMap();

        try {

            Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);

            //get the authenticated account (aka logged in user)
            Integer authenticatedAccountID;
            if ( CommonAuthResource.isKOA( request ) ) {
                authenticatedAccountID = CommonAuthResource.getAccountIDFromAccountHeader(request);
            } else {
                authenticatedAccountID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
            }

            if (authenticatedAccountID == null) {
                throw new InvalidAuthException();
            }

            Integer terminalID = getUserTerminalID(storeID);

            ArrayList<HashMap> result = null;
            if ( isLocalWar() ) {
                result = dm.parameterizedExecuteQuery("data.posanywhere.PersonTerminalLookupOffline",
                        new Object[]{
                                terminalID,
                                Double.parseDouble(orderTotal.toString()),
                                authenticatedAccountID

                        },
                        true
                );
            } else {
                 result = dm.parameterizedExecuteQuery("data.posanywhere.PersonTerminalLookupBP",
                        new Object[]{
                                terminalID,
                                Double.parseDouble(orderTotal.toString()),
                                authenticatedAccountID

                        },
                        true
                );
            }


            //should only be one result
            if (result != null && result.size() == 1) {
                HashMap resultHM = result.get(0);

                userBalanceHM = CommonAPI.calculateUserCurrentBalance(resultHM);

                BigDecimal userBalance = CommonAPI.convertModelDetailToBigDecimal(userBalanceHM.get("USERBALANCE"));
                if (userBalance.compareTo(orderTotal) < 0) {
                    //This is at best mostly redundant but it is what the POS does
                    ArrayList<HashMap> response = null;

                    if ( isLocalWar() ) {
                        response = dm.parameterizedExecuteQuery("data.posanywhere.PersonTerminalLookupOffline",
                                new Object[]{
                                        terminalID,
                                        Double.parseDouble(orderTotal.toString()),
                                        authenticatedAccountID

                                },
                                true
                        );
                    } else {
                        response = dm.parameterizedExecuteQuery("data.posanywhere.PersonTerminalLookup",
                                new Object[]{
                                        terminalID,
                                        Double.parseDouble(orderTotal.toString()),
                                        authenticatedAccountID

                                },
                                true
                        );
                    }

                    if (response != null && response.size() == 1) {
                        HashMap responseHM = response.get(0);

                        userBalanceHM = CommonAPI.calculateUserCurrentBalance(resultHM);
                    }
                }
            } else {
                Logger.logMessage("Could not determine user's balance in CommonAPI.getUserBalance method", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            throw new MissingDataException("Could not determine account balance. Purchase limit may be incorrectly configured.");
        }
        return userBalanceHM;
    }

    public static HashMap calculateUserCurrentBalance(HashMap balanceHM) {
        HashMap userBalanceHM = new HashMap();
        BigDecimal userBalance = BigDecimal.ZERO;
        String userBalanceType = "";

        BigDecimal userGlobalAvailable = CommonAPI.convertModelDetailToBigDecimal(balanceHM.get("GLOBALLIMIT")).subtract(CommonAPI.convertModelDetailToBigDecimal(balanceHM.get("GLOBALBALANCE")));
        BigDecimal userStoreAvailable = CommonAPI.convertModelDetailToBigDecimal(balanceHM.get("STORELIMIT"), BigDecimal.ZERO).subtract(CommonAPI.convertModelDetailToBigDecimal(balanceHM.get("STOREBALANCE"), BigDecimal.ZERO));
        BigDecimal userTransactionLimit = CommonAPI.convertModelDetailToBigDecimal(balanceHM.get("SINGLECHARGELIMIT"), BigDecimal.ZERO);

        //if storeAvailable is greater than the globalAvailable
        if (userGlobalAvailable.compareTo(userStoreAvailable) == -1) {
            //if transactionLimit is higher than the global available
            if (userGlobalAvailable.compareTo(userTransactionLimit) == -1) {
                //user can spend up to the global available, not up to the limit
                userBalance = userGlobalAvailable;
                userBalanceType = "global";
            } else {
                //otherwise user can spend up to the limit
                userBalance = userTransactionLimit;
                userBalanceType = "transaction";
            }
        } else {
            //if globalAvailable is greater than storeAvailable
            //if transactionLimit is greater than storeAvailable
            if (userStoreAvailable.compareTo(userTransactionLimit) == -1) {
                //user can only spend what is available in the store, not up to the limit
                userBalance = userStoreAvailable;
                userBalanceType = "store";
            } else {
                //otherwise user can spend up to the limit
                userBalance = userTransactionLimit;
                userBalanceType = "transaction";
            }
        }

        userBalanceHM.put("USERBALANCE", userBalance);
        userBalanceHM.put("USERBALANCETYPE", userBalanceType);

        return userBalanceHM;
    }

    //takes a user's MyQCTerminalID and returns the actual TerminalID
    public static Integer getUserTerminalID(Integer storeID) {
        ArrayList<HashMap> result;
        Integer terminalID = 0;

        try {
            result = dm.parameterizedExecuteQuery("data.ordering.getUserTerminalID", new Object[]{storeID}, true);

            //should only be one result
            if (result != null && result.size() == 1) {
                HashMap resultHM = result.get(0);

                if (resultHM.containsKey("TERMINALID") && resultHM.get("TERMINALID") != null) {
                    terminalID = Integer.parseInt(resultHM.get("TERMINALID").toString());
                } else {
                    Logger.logMessage("TerminalID not found in CommonAPI.getUserTerminalID result.", Logger.LEVEL.ERROR);
                }
            } else {
                Logger.logMessage("Could not determine user's terminalID in CommonAPI.getUserTerminalID method.", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        return terminalID;
    }

    //takes a user's MyQCTerminalID and returns the actual TerminalID
    public static Integer getMyQCTerminalID(Integer terminalID) {
        ArrayList<HashMap> result;
        Integer myQCTerminalID = 0;

        try {
            result = dm.parameterizedExecuteQuery("data.ordering.getMyQCTerminalID", new Object[]{terminalID}, true);

            //should only be one result
            if (result != null && result.size() == 1) {
                HashMap resultHM = result.get(0);

                if (resultHM.containsKey("MYQCTERMINALID") && resultHM.get("MYQCTERMINALID") != null) {
                    myQCTerminalID = Integer.parseInt(resultHM.get("MYQCTERMINALID").toString());
                } else {
                    Logger.logMessage("MyQCTerminalID not found in CommonAPI.getMyQCTerminalID result.", Logger.LEVEL.ERROR);
                }
            } else {
                Logger.logMessage("Could not determine user's MyQCTerminalID in CommonAPI.getMyQCTerminalID method.", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        return myQCTerminalID;
    }

    //returns true if the instance user in the Validation object passed is a active (handles QC Users (managers) and QC Accounts (employees))
    public static Boolean isInstanceUserActive(Validation requestValidation) {
        Boolean instanceUserIsActive = false;
        try {
            Integer instanceUserID = requestValidation.getInstanceUserID();
            Integer instanceUserTypeID = requestValidation.fetchInstanceUserTypeID();
            Object instanceUserActiveObj = null;
            if (instanceUserID > 0) { //instance user is a QC User (manager)
                if ( instanceUserTypeID == null || instanceUserTypeID == 2 ) {
                    instanceUserActiveObj = dm.getSingleField("data.common.getQCUserActiveStatus",
                            new Object[]{
                                    instanceUserID
                            },
                            false,
                            true
                    );
                } else {
                    instanceUserActiveObj = dm.getSingleField("data.common.getPersonAccountActiveStatus",
                            new Object[]{
                                    instanceUserID
                            },
                            false,
                            true
                    );
                }
            } else { //instance user is a QC Account (employee)
                instanceUserID = instanceUserID * -1; //make positive for query
                instanceUserActiveObj = dm.getSingleField("data.common.getQCAccountActiveStatus",
                        new Object[]{
                                instanceUserID
                        },
                        false,
                        true
                );
            }
            if (instanceUserActiveObj != null) {
                if (instanceUserActiveObj.toString().equals("1") || instanceUserActiveObj.toString().equals("true")) {
                    instanceUserIsActive = true;
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex);
            instanceUserIsActive = false;
        }
        return instanceUserIsActive;
    }

    //gets the QCUser to use for submitting transactions, based on the MyQC flag on QC_UserProfile table
    public static Integer getQCUserID() {
        Integer QCUserID = null;

        try {
            ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.ordering.getQCUserID", new Object[]{}, true);

            //should only be one result
            if (result != null && result.size() == 1) {
                HashMap resultHM = result.get(0);

                if (resultHM.containsKey("QCUSERID") && resultHM.get("QCUSERID") != null) {
                    QCUserID = Integer.parseInt(resultHM.get("QCUSERID").toString());
                } else {
                    Logger.logMessage("Could not determine QCUserID in CommonAPI.getQCUserID", Logger.LEVEL.ERROR);
                }
            } else {
                Logger.logMessage("Could not retrieve QCUserID in CommonAPI.getQCUserID", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        return QCUserID;
    }

    public static void recordPrinterQueueHeader(TransactionModel transactionModel) throws Exception {
        String logFileName = PosAPIHelper.getLogFileName(transactionModel.getTerminal().getId());

        JDCConnection conn = null;
        try {
            conn = dm.pool.getConnection(false, logFileName);
        } catch (SQLException qe) {
            Logger.logMessage("TransactionBuilder.save no connection to DB.", logFileName);
            Logger.logMessage(qe.getMessage(), logFileName);
            throw new MissingDataException("Could not establish a connection to the database.", transactionModel.getTerminal().getId());
        }

        Date transDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(ESAPI.encoder().decodeForHTML(transactionModel.getTimeStamp()));

        String estimatedOrderTime = ESAPI.encoder().decodeForHTML(transactionModel.getEstimatedOrderTime());
        String name = ESAPI.encoder().decodeForHTML(transactionModel.getName());
        String phone = ESAPI.encoder().decodeForHTML(transactionModel.getPhone());
        String comment = ESAPI.encoder().decodeForHTML(transactionModel.getTransComment());
        String pickupDeliveryNote = ESAPI.encoder().decodeForHTML(transactionModel.getPickUpDeliveryNote());

        int result = CommonAPI.insertPrinterQueueHeader(conn, logFileName, transactionModel.getId(), 1, transDate, transactionModel.getTerminal().getId(), PosAPIHelper.OrderType.convertStringToInt(transactionModel.getOrderType()), name, phone, comment, pickupDeliveryNote, estimatedOrderTime,
                transactionModel.getOrderNumber(), transactionModel.getTransactionTypeId());

        if (result <= 0) {
            throw new Exception("Error Adding Printer Header Queue for online orders");
        }
    }

    //inserts receipt header to PAPrinterQueue, takes the connection and logFile in order to rollback as necessary
    public static int insertPrinterQueueHeader(JDCConnection conn, String logFileName, Integer PATransactionID, Integer onlineOrdering, Date transDTM, Integer terminalID, Integer PAOrderTypeID, String personName, String phone, String transComment, String pickupDeliveryNote, String estimatedOrderTime, String orderNumber, Integer transTypeID) throws Exception {
        Integer printStatusID = 1;
        int result;

        try {
            result = dm.parameterizedExecuteNonQuery("data.ordering.InsertPAPrinterQueueHeader",
                    new Object[]{
                            PATransactionID,
                            onlineOrdering,
                            transDTM,
                            printStatusID,
                            terminalID,
                            PAOrderTypeID,
                            personName,
                            phone,
                            transComment,
                            pickupDeliveryNote,
                            estimatedOrderTime,
                            orderNumber,
                            transTypeID
                    }
                    , logFileName, conn);
        } catch (Exception ex) {
            Logger.logMessage("Exception occurred when attempting to insert printer queue header in CommonAPI.insertPrinterQueueHeader. Logging exception and re-throwing for submitTransaction to catch", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            throw ex;
        }

        return result;
    }

    public static Integer getFreedomPayTimeoutSeconds() {
        Integer defaultTimeout = 20;
        try {
            String timeout = MMHProperties.getAppSetting("site.freedompay.timeoutseconds");
            if ((timeout == null) || (timeout.length() == 0)) {
                timeout = defaultTimeout.toString();
            }
            return Integer.parseInt(timeout);
        } catch (Exception ex) {
            Logger.logException(ex);
            return defaultTimeout;
        }
    }

    public static enum PosModelType {
        TRANSACTION,
        TRANSACTION_LINE,
        ACCOUNT,
        DISCOUNT,
        TRANSACTION_RESPONSE,
        LOYALTY_PROGRAM,
        LOYALTY_REWARD,
        PRODUCT,
        LABELPRINTJOB,
        LABELPRINTJOBDETAIL,
        TENDER,
        TERMINAL,
        COMBO,
        TAX,
        SURCHARGE,
        ROTATION
    }

    //Check if an Object parameter is equal to null or is blank, if so, throw the exception that is passed in.  Used to check for missing parameters
    public static void checkIsNullOrEmptyObject(Object obj, PosModelType modelType, Integer terminalId) throws Exception {

        if (obj == null || obj.toString().isEmpty()) {

            switch (modelType) {
                case ACCOUNT:
                    throw new AccountNotFoundException(terminalId);
                case TRANSACTION:
                    throw new TransactionNotFoundException(terminalId);
                case DISCOUNT:
                    throw new DiscountNotFoundException(terminalId);
                case PRODUCT:
                    throw new ProductNotFoundException(terminalId);
                case LOYALTY_PROGRAM:
                    throw new LoyaltyProgramNotFoundException(terminalId);
                case LOYALTY_REWARD:
                    throw new LoyaltyRewardNotFoundException(terminalId);
                case TENDER:
                    throw new TenderNotFoundException(terminalId);
                case TERMINAL:
                    throw new TerminalNotFoundException();
                case SURCHARGE:
                    throw new SurchargeNotFoundException(terminalId);
                default:
                    throw new MissingDataException(terminalId);
            }
        }
    }

    //Check if an Object parameter is equal to null or is blank, if so, throw the exception that is passed in.  Used to check for missing parameters
    public static void checkIsNullOrEmptyObject(Object obj, String errorDetails, Integer terminalId) throws Exception {

        if (obj == null || obj.toString().isEmpty()) {
            throw new MissingDataException(errorDetails, terminalId);
        }
    }

    //Check if an Object parameter is equal to null or is blank, if so, throw the exception that is passed in.  Used to check for missing parameters
    public static void checkIsNullOrEmptyList(List list, PosModelType modelType, String customMessage, Integer terminalId) throws Exception {

        if (list == null || list.isEmpty()) {

            switch (modelType) {
                case ACCOUNT:
                    if (customMessage != null && !customMessage.isEmpty()) {
                        throw new AccountNotFoundException(customMessage, terminalId);
                    }
                    throw new AccountNotFoundException(terminalId);
                case TRANSACTION:
                    if (customMessage != null && !customMessage.isEmpty()) {
                        throw new TransactionNotFoundException(customMessage, terminalId);
                    }
                    throw new TransactionNotFoundException(terminalId);
                case DISCOUNT:
                    if (customMessage != null && !customMessage.isEmpty()) {
                        throw new DiscountNotFoundException(customMessage, terminalId);
                    }
                    throw new DiscountNotFoundException(terminalId);
                case PRODUCT:
                    if (customMessage != null && !customMessage.isEmpty()) {
                        throw new ProductNotFoundException(customMessage, terminalId);
                    }
                    throw new ProductNotFoundException(terminalId);
                case LOYALTY_PROGRAM:
                    if (customMessage != null && !customMessage.isEmpty()) {
                        throw new LoyaltyProgramNotFoundException(customMessage, terminalId);
                    }
                    throw new LoyaltyProgramNotFoundException(terminalId);
                case LOYALTY_REWARD:
                    if (customMessage != null && !customMessage.isEmpty()) {
                        throw new LoyaltyRewardNotFoundException(customMessage, terminalId);
                    }
                    throw new LoyaltyRewardNotFoundException(terminalId);
                case TERMINAL:
                    if (customMessage != null && !customMessage.isEmpty()) {
                        throw new TerminalNotFoundException(customMessage);
                    }
                    throw new TerminalNotFoundException();
                case SURCHARGE:
                    if (customMessage != null && !customMessage.isEmpty()) {
                        throw new SurchargeNotFoundException(customMessage);
                    }
                    throw new SurchargeNotFoundException();
                case COMBO:
                    if (customMessage != null && !customMessage.isEmpty()) {
                        throw new ComboNotFoundException(customMessage);
                    }
                    throw new ComboNotFoundException();
                default:
                    if (customMessage != null && !customMessage.isEmpty()) {
                        throw new MissingDataException(customMessage);
                    }
                    throw new MissingDataException(terminalId);
            }
        }
    }

    //Check if an Object parameter is equal to null or is blank, if so, throw the exception that is passed in.  Used to check for missing parameters
    public static void checkIsNullOrEmptyList(List list, String customMessage, Integer terminalId) throws Exception {

        if (list == null || list.isEmpty()) {
            if (customMessage != null && !customMessage.isEmpty()) {
                throw new MissingDataException(customMessage, terminalId);
            }
            throw new MissingDataException(terminalId);
        }
    }

    //Check if an Object parameter is equal to null or is blank, if so, throw MissingDataException.  Used to check for missing parameters
    public static void checkIsNullOrEmptyObject(Object obj, String customMessage) throws Exception {
        if (obj == null || obj.toString().isEmpty()) { //throw MissingDataException object is null
            customMessage = (customMessage != null && !customMessage.isEmpty()) ? "Missing Data : " + customMessage : "Missing Data : Object was either null or empty.";
            throw new MissingDataException(customMessage);
        }
    }

    //Check if a list is equal to null or the list is empty is blank
    public static void checkIsNullOrEmptyList(List list, String customMessage) throws Exception {
        if (list == null || list.isEmpty()) { //throw MissingDataException object is null
            customMessage = (customMessage != null && !customMessage.isEmpty()) ? "Missing Data : " + customMessage : "Missing Data : List was either null or empty.";
            throw new MissingDataException(customMessage);
        }
    }

    //Check if a list has more than one record in it
    public static void checkMoreThanOneRecord(List list, String customMessage) throws Exception {
        if (list != null && !list.isEmpty() && list.size() > 1) {
            customMessage = (customMessage != null && !customMessage.isEmpty()) ? "Invalid Data : " + customMessage : "Invalid Data : More than one record was found.";

            throw new MissingDataException(customMessage);

        }
    }

    public static String getPosAPILogFileName(int terminalId) {
        if (terminalId <= 0) {
            return COMMON_LOG_FILE;
        } else {
            return "POSAPI_" + terminalId + "_T.log";
        }
    }

    public static String getPosAPILogFileName() {
        return COMMON_LOG_FILE;
    }

    //get eligible discounts (same name as QC POS Discount, mapped RC, open, preset, subtotal, meet subtotal min)
    public static HashMap<String, HashMap> getQCDiscounts(Integer employeeID, BigDecimal subtotal, TerminalModel terminalModel) throws Exception {
        HashMap QCDiscounts = new HashMap();

        ArrayList<HashMap> QCDiscountList = dm.parameterizedExecuteQuery("data.ordering.getQCDiscounts",
                new Object[]{
                        employeeID,
                        subtotal,
                        (terminalModel == null) ? null : terminalModel.getId()
                },
                true
        );

        //if any results, check the discount\'s schedules
        if (QCDiscountList == null || QCDiscountList.size() == 0) {
            Logger.logMessage("No discounts returned for this user", Logger.LEVEL.DEBUG);
            return QCDiscounts;
        }

        //NEED TO CHECK DATES AND TIMES FOR VALIDITY
        LocalDateTime currentTime = LocalDateTime.now();

        Integer timeDiff = 0;
        if (QCDiscountList.get(0).get("TIMEDIFFERENCE") != null) {
            timeDiff = CommonAPI.convertModelDetailToInteger(QCDiscountList.get(0).get("TIMEDIFFERENCE"), 0, true);
            currentTime = currentTime.plusHours(timeDiff);
        }

        String dayOfWeek = currentTime.getDayOfWeek().toString();

        //only include discounts that are valid for right now
        for (HashMap thisDiscount : QCDiscountList) {

            //check if discount is valid for today
            if (thisDiscount.get(dayOfWeek).toString().equals("false")) {
                continue;
            }

            //check if discount is valid for the current time
            String startTime = thisDiscount.get("STARTTIME").toString();
            String endTime = thisDiscount.get("ENDTIME").toString();

            if (startTime.length() == 0 || endTime.length() == 0) {
                continue;
            }

            LocalTime discountOpen = LocalTime.parse(startTime);
            LocalTime discountClose = LocalTime.parse(endTime);

            //if now is after close and before open.. continue to next discount
            if (discountOpen.compareTo(currentTime.toLocalTime()) > 0 || discountClose.compareTo(currentTime.toLocalTime()) < 0) {
                continue;
            }

            //check the amount/reset properties of any coupon discounts to determine validity
            if (CommonAPI.convertModelDetailToString(thisDiscount.get("DISCOUNTTYPEID")).equals("2")) {
                Integer discountFrequencyID = CommonAPI.convertModelDetailToInteger(thisDiscount.get("DISCOUNTFREQUENCYID"));
                SimpleDateFormat sdfMilitaryTime = new SimpleDateFormat("HH:mm");
                Date discountResetDate = new Date();
                Calendar now = Calendar.getInstance();
                now.add(Calendar.HOUR, timeDiff);

                //get the date to compare to based on the frequency setting
                switch (discountFrequencyID) {
                    case 1:
                        // DAILY
                        if (sdfMilitaryTime.format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(thisDiscount.get("RESETDTM").toString())).compareTo(sdfMilitaryTime.format(now.getTime())) <= 0) {
                            //current time of day is after reset time of day (reset today scenario)
                            discountResetDate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").parse(
                                    new SimpleDateFormat("MM/dd/yyyy").format(now.getTime()) + " " + (new SimpleDateFormat("hh:mm:ss").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(thisDiscount.get("RESETDTM").toString())))
                            );
                        } else {
                            //current time of day is before reset time, set date one day back (reset did not occur yet today)
                            Calendar c = Calendar.getInstance();
                            c.setTime(now.getTime());
                            c.add(Calendar.DATE, -1);
                            discountResetDate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").parse(
                                    new SimpleDateFormat("MM/dd/yyyy").format(c.getTime()) + " " + (new SimpleDateFormat("hh:mm:ss").format(thisDiscount.get("RESETDTM")))
                            );
                        }
                        break;
                    case 2:
                        Calendar c1 = Calendar.getInstance();
                        c1.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(thisDiscount.get("RESETDTM").toString()));
                        while (c1.getTime().compareTo(now.getTime()) < 0) {
                            c1.add(Calendar.DATE, 7);
                        }
                        c1.add(Calendar.DATE, -7);
                        discountResetDate = c1.getTime();
                        break;
                    case 3:
                        Calendar c2 = Calendar.getInstance();
                        c2.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(thisDiscount.get("RESETDTM").toString()));
                        while (c2.getTime().compareTo(now.getTime()) < 0) {
                            c2.add(Calendar.MONTH, 1);
                        }
                        c2.add(Calendar.MONTH, -1);
                        discountResetDate = c2.getTime();
                        break;
                    case 4:
                        // Transaction
                        discountResetDate = new SimpleDateFormat("MM/dd/yyyy").parse("01/01/3000");
                        break;
                }

                //get the sum of the amounts of the usage of this coupon since the last reset date
                ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.ordering.checkCouponUsage",
                        new Object[]{
                                employeeID,
                                thisDiscount.get("DISCOUNTID").toString(),
                                discountResetDate
                        },
                        true
                );

                if (result != null) {
                    //should only be 0 or 1 record in result because it is a sum of amounts, if none then amount on thisDiscount is OK
                    if (result.size() == 1) {
                        HashMap resultHM = result.get(0);

                        if (resultHM.containsKey("DISCTAKEN") && resultHM.get("DISCTAKEN") != null) {
                            BigDecimal amount = CommonAPI.convertModelDetailToBigDecimal(thisDiscount.get("AMOUNT"));
                            BigDecimal discTaken = CommonAPI.convertModelDetailToBigDecimal(resultHM.get("DISCTAKEN"));

                            if (discTaken != null && discTaken.compareTo(BigDecimal.ZERO) == 1) {
                                //if singleTransaction is on and the coupon has been used since the last reset date, set amount to zero
                                if (thisDiscount.get("SINGLETRANSACTION").toString().equals("true")) {
                                    thisDiscount.put("AMOUNT", BigDecimal.ZERO);
                                } else {
                                    //otherwise just take the difference
                                    BigDecimal amtRemaining = amount.subtract(discTaken);

                                    //prevent negatives (this could only happen with bad data)
                                    if (amtRemaining.compareTo(BigDecimal.ZERO) == -1) {
                                        amtRemaining = BigDecimal.ZERO;
                                    }

                                    thisDiscount.put("AMOUNT", amtRemaining);
                                }
                            }
                        }
                    }
                } else {
                    Logger.logMessage("There was an error attempting to get the previous transactions for coupon with DiscountID: " + thisDiscount.get("DISCOUNTID").toString(), Logger.LEVEL.ERROR);
                }
            }

            if (CommonAPI.isLocalWar()){
                if (CommonAPI.convertModelDetailToInteger(thisDiscount.get("DISCOUNTTYPEID")).equals(PosAPIHelper.DiscountType.COUPON.toInt()) &&
                        !terminalModel.getAllowQcCouponDiscountOffline()){
                    continue; //if terminal is offline, check if the terminal allows PA Discounts of type Coupon
                }
            }

            //valid discount, add to map
            if (CommonAPI.convertModelDetailToBigDecimal(thisDiscount.get("AMOUNT")) != null && CommonAPI.convertModelDetailToBigDecimal(thisDiscount.get("AMOUNT")).compareTo(new BigDecimal(0)) == 1) {
                thisDiscount.put("ORIGINALAMOUNT", thisDiscount.get("AMOUNT"));
                QCDiscounts.put(thisDiscount.get("PADISCOUNTID").toString(), thisDiscount);
            }
        }

        return QCDiscounts;
    }


    public static boolean showKeepLogged() throws Exception {
        try {
            return CommonAPI.convertModelDetailToBoolean(dm.parameterizedExecuteScalar("data.common.getShowLoggedInSetting", new Object[]{}));
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return false;
        }
    }

    public static boolean enableFingerprint(Double apiVersion) throws Exception {
        boolean enableFingerprint = false;
        try {
            if ( apiVersion != null && apiVersion >= 1.6 ) {
                return CommonAPI.convertModelDetailToBoolean(dm.parameterizedExecuteScalar("data.common.getEnableFingerprint", new Object[]{}));
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return enableFingerprint;
    }


    //returns true if the mac address is not null AND it is in the QC_Terminals db table
    public static Integer verifyMacAddressForDeploymentManager(String macAddress) {
        ArrayList<HashMap> result;
        Integer terminalID = 0;
        try {

            if (macAddress != null) {
                result =dm.parameterizedExecuteQuery("data.common.verifyMacAddressForDeploymentManager", new Object[]{macAddress}, true);

                if (result != null && result.size() > 0) {
                    HashMap resultHM = result.get(0);

                    if (resultHM.containsKey("TERMINALID") && resultHM.get("TERMINALID") != null) {
                        terminalID = Integer.parseInt(resultHM.get("TERMINALID").toString());
                    } else {
                        Logger.logMessage("TerminalID not found in CommonAPI.verifyMacAddressForDeploymentManager.", Logger.LEVEL.ERROR);
                    }
                } else {
                    Logger.logMessage("Could not determine terminalID for Mac Address in CommonAPI.verifyMacAddressForDeploymentManager method.", Logger.LEVEL.ERROR);
                }
            } else {
                Logger.logMessage("Mac Address is NULL", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logMessage("Error in CommonAPI.verifyMacAddressForDeploymentManager", Logger.LEVEL.ERROR);
        }
        return terminalID;
    }

    public static String createDSKey(Integer instanceUserID, Integer gatewayUserID, Boolean keepLoggedIn, Integer terminalID){
        return createDSKey(instanceUserID, gatewayUserID, terminalID, keepLoggedIn, "");
    }

    public static String createDSKey(Integer instanceUserID, Integer gatewayUserID, Integer terminalID, Boolean keepLoggedIn, String idpSessionKey) {
        return createDSKey(instanceUserID, gatewayUserID, terminalID, keepLoggedIn, idpSessionKey, null);
    }

    public static String createDSKey(Integer instanceUserID, Integer gatewayUserID, Integer terminalID, Boolean keepLoggedIn, String idpSessionKey, Integer instanceUserTypeID) {
        try {
            //device session keys can be for employees (accountID) or managers (userID) or people (personID) and can be created for cloud users (gatewayUserID)
            Integer accountID = null;
            Integer userID = null;
            Integer personID = null;

            //if instanceUserId is null
            if(instanceUserID == null) {

                //set it as its default value of 0
                instanceUserID = 0;

            } else if (instanceUserID < 0) {  //if the instanceUserID on this instance is negative, then we are dealing with an employee (accountID)

                //make positive to store in DB
                accountID = instanceUserID * -1;

            } else { //if positive, then manager (userID)

                if (instanceUserTypeID == null || instanceUserTypeID == 2) {
                    //set the instanceUserID to the userID so we know it's a manager (QC User)
                    userID = instanceUserID;
                } else if (instanceUserTypeID == 3) {
                    //set the instanceUserID to the personID so we know it's a person (QC Person)
                    personID = instanceUserID;
                }

            }

            if(gatewayUserID == null) {
                gatewayUserID = 0;
            }
            else if (gatewayUserID == 0) {   //if no gatewayUserID is found, the value will be it's default of 0, let's record it as NULL in the DeviceSessionKey table
                gatewayUserID = null;
            }

            //if no terminalID is found, the value will be it's default of 0, let's record it as NULL in the DeviceSessionKey table
            if (terminalID == null || terminalID == 0) {
                terminalID = null;
            }

            if(idpSessionKey.equals("")){
                idpSessionKey = null;
            }

            //get the current timestamp and set to DSKeyCreatedOn
            Instant DSKeyCreatedOn = Instant.now();

            //create a secure random key
            String DSKey = new BigInteger(256, new SecureRandom())+BigInteger.valueOf(DSKeyCreatedOn.getEpochSecond()).subtract(new BigInteger(8, new SecureRandom())).toString(64);

            //determine if the user checked "keep me logged in" to know when the DSKey should expire
            Instant DSKeyExpiresOn;
            if (keepLoggedIn) {
                //check if the app.properties 'site.security.extendedsessiontimeoutminutes' exists, if not it will set expiration to 90 days instead of 100 years - 1/25/2018 gematuszyk
                LoginModel loginModel = new LoginModel();
                loginModel.setExtendSession(true);  //if keepLoggedIn is true then we want to extend the session
                loginModel.checkForExtendedSessionTime();
                DSKeyExpiresOn = loginModel.getExtendedSessionExpirationDate();
            } else {
                DSKeyExpiresOn = DSKeyCreatedOn.plus(Duration.ofMinutes(CommonAPI.getSessionTimeoutMinutes()));
            }

            Logger.logMessage("instanceUserID "+instanceUserID, Logger.LEVEL.DEBUG);
            Logger.logMessage("gateway "+gatewayUserID, Logger.LEVEL.DEBUG);
            Logger.logMessage("user "+userID, Logger.LEVEL.DEBUG);
            Logger.logMessage("account "+accountID, Logger.LEVEL.DEBUG);
            Logger.logMessage("dskey "+DSKey, Logger.LEVEL.DEBUG);
            Logger.logMessage("idpSessionKey "+idpSessionKey, Logger.LEVEL.DEBUG);
            Logger.logMessage("person "+personID, Logger.LEVEL.DEBUG);

            Integer insertResult;
            if ( instanceUserTypeID == null ) {
                //prepare sql parameters and then execute insert SQL
                insertResult = dm.parameterizedExecuteNonQuery("data.common.createDSKey",
                    new Object[]{
                        DSKey,
                        accountID,
                        userID,
                        gatewayUserID,
                        DSKeyCreatedOn.toString(),
                        DSKeyExpiresOn.toString(),
                        terminalID,
                        idpSessionKey
                    }
                );
            } else {
                //for MyQC 2.0 and up, when calling Valdation.createDSKey with an Integer instanceUserTypeID
                insertResult = dm.parameterizedExecuteNonQuery("data.common.createDSKeyConsiderPerson",
                    new Object[]{
                        DSKey,
                        accountID,
                        userID,
                        gatewayUserID,
                        DSKeyCreatedOn.toString(),
                        DSKeyExpiresOn.toString(),
                        terminalID,
                        idpSessionKey,
                        personID
                    }
                );
            }

            //if the insert was a success
            if (insertResult != null && insertResult != -1) {
                //set the DSKey for this instance of the object
                return DSKey;
            }  else {
                Logger.logMessage("Could not execute insert SQL", Logger.LEVEL.ERROR);
                return "ERROR";
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            String errorDetails = "Error in CommonAPI.createDSKey";
            return "ERROR";
        }
    }

    public static String createDSKey(Integer instanceUserID, Integer minutesValid){

        try{

            //device session keys can be for employees (accountID) or managers (userID) or people (personID) and can be created for cloud users (gatewayUserID)
            Integer userID = instanceUserID;

            //get the current timestamp and set to DSKeyCreatedOn
            Instant DSKeyCreatedOn = Instant.now();

            //create a secure random key
            String DSKey = new BigInteger(256, new SecureRandom())+BigInteger.valueOf(DSKeyCreatedOn.getEpochSecond()).subtract(new BigInteger(8, new SecureRandom())).toString(64);

            //determine if the user checked "keep me logged in" to know when the DSKey should expire
            Instant DSKeyExpiresOn;

            DSKeyExpiresOn = DSKeyCreatedOn.plus(Duration.ofMinutes(minutesValid));

            Logger.logMessage("instanceUserID "+instanceUserID, Logger.LEVEL.DEBUG);
            Logger.logMessage("gateway "+null, Logger.LEVEL.DEBUG);
            Logger.logMessage("user "+userID, Logger.LEVEL.DEBUG);
            Logger.logMessage("account "+null, Logger.LEVEL.DEBUG);
            Logger.logMessage("dskey "+DSKey, Logger.LEVEL.DEBUG);
            Logger.logMessage("idpSessionKey "+null, Logger.LEVEL.DEBUG);
            Logger.logMessage("person "+null, Logger.LEVEL.DEBUG);

            Integer insertResult;
            //prepare sql parameters and then execute insert SQL
            insertResult = dm.parameterizedExecuteNonQuery("data.common.createDSKey",
                    new Object[]{
                            DSKey,
                            null,
                            userID,
                            null,
                            DSKeyCreatedOn.toString(),
                            DSKeyExpiresOn.toString(),
                            null,
                            null
                    }
            );

            //if the insert was a success
            if (insertResult != -1) {
                //set the DSKey for this instance of the object
                return DSKey;
            }  else {
                Logger.logMessage("Could not execute insert SQL", Logger.LEVEL.ERROR);
                return "ERROR";
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            String errorDetails = "Error in CommonAPI.createDSKey";
            return "ERROR";
        }
    }

    public static String createDSKey(DisplayModel displayModel) {
        try {
            //device session keys can be for employees (accountID) or managers (userID) or people (personID) and can be created for cloud users (gatewayUserID)
            boolean keepLoggedIn = false;

            Integer displayID = CommonAPI.convertModelDetailToInteger(displayModel.getID());

            //get the current timestamp and set to DSKeyCreatedOn
            Instant DSKeyCreatedOn = Instant.now();

            //create a secure random key
            String DSKey = new BigInteger(256, new SecureRandom())+BigInteger.valueOf(DSKeyCreatedOn.getEpochSecond()).subtract(new BigInteger(8, new SecureRandom())).toString(64);

            //add 36524 days (100 years) to the DSKeyCreatedOn to determine when the DSKey should expire
            Instant DSKeyExpiresOn = DSKeyCreatedOn.plus(Duration.ofDays(36524));

            Logger.logMessage("gateway "+null, Logger.LEVEL.DEBUG);
            Logger.logMessage("user "+null, Logger.LEVEL.DEBUG);
            Logger.logMessage("account "+null, Logger.LEVEL.DEBUG);
            Logger.logMessage("dskey "+DSKey, Logger.LEVEL.DEBUG);
            Logger.logMessage("idpSessionKey "+null, Logger.LEVEL.DEBUG);
            Logger.logMessage("displayID "+displayID, Logger.LEVEL.DEBUG);

            Integer insertResult = dm.parameterizedExecuteNonQuery("data.common.createDSKeyConsiderDisplay",
                    new Object[]{
                            DSKey,
                            DSKeyCreatedOn.toString(),
                            DSKeyExpiresOn.toString(),
                            displayID
                    }
            );

            //if the insert was a success
            if (insertResult != null && insertResult != -1) {
                //set the DSKey for this instance of the object
                return DSKey;
            }  else {
                Logger.logMessage("Could not execute insert SQL", Logger.LEVEL.ERROR);
                return "ERROR";
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            String errorDetails = "Error in CommonAPI.createDSKey for display";
            return "ERROR";
        }
    }

    public static String createDSKey(Integer instanceUserID, Integer gatewayUserID, Integer terminalID, Boolean isPosApi) {
        try {

            //device session keys can be for employees (accountID) or managers (userID) and can be created for cloud users (gatewayUserID)
            Integer accountID = null;
            Integer userID = null;

            //if instanceUserId is null
            if (instanceUserID == null) {

                //set it as its default value of 0
                instanceUserID = 0;

            } else if (instanceUserID < 0) {  //if the instanceUserID on this instance is negative, then we are dealing with an employee (accountID)

                //make positive to store in DB
                accountID = instanceUserID * -1;

            } else { //if positive, then manager (userID)

                //set the instanceUserID to the userID so we know it's a manager (QC User)
                userID = instanceUserID;
            }

            if (gatewayUserID == null) {
                gatewayUserID = 0;
            } else if (gatewayUserID == 0) {   //if no gatewayUserID is found, the value will be it's default of 0, let's record it as NULL in the DeviceSessionKey table
                gatewayUserID = null;
            }

            //if no terminalID is found, the value will be it's default of 0, let's record it as NULL in the DeviceSessionKey table
            if (terminalID == null || terminalID == 0) {
                terminalID = null;
            }

            /*if(idpSessionKey.equals("")){
                idpSessionKey = null;
            }
*/
            //get the current timestamp and set to DSKeyCreatedOn
            Instant DSKeyCreatedOn = Instant.now();

            //create a secure random key
            String DSKey = new BigInteger(256, new SecureRandom()) + BigInteger.valueOf(DSKeyCreatedOn.getEpochSecond()).subtract(new BigInteger(8, new SecureRandom())).toString(64);

            Validation val = new Validation();

            //determine if the user checked "keep me logged in" to know when the DSKey should expire
            Instant DSKeyExpiresOn;
            if (val.getKeepLogged()) {
                //add 36524 days (100 years) to the DSKeyCreatedOn to determine when the DSKey should expire
                DSKeyExpiresOn = DSKeyCreatedOn.plus(Duration.ofDays(36524));
            } else {
                DSKeyExpiresOn = DSKeyCreatedOn.plus(Duration.ofMinutes(CommonAPI.getSessionTimeoutMinutes()));
            }

            Logger.logMessage("instanceUserID " + instanceUserID, Logger.LEVEL.DEBUG);
            Logger.logMessage("gateway " + gatewayUserID, Logger.LEVEL.DEBUG);
            Logger.logMessage("user " + userID, Logger.LEVEL.DEBUG);
            Logger.logMessage("account " + accountID, Logger.LEVEL.DEBUG);
            Logger.logMessage("dskey " + DSKey, Logger.LEVEL.DEBUG);
            //Logger.logMessage("idpSessionKey "+idpSessionKey, Logger.LEVEL.DEBUG);

            //prepare sql parameters and then execute insert SQL
            Integer insertResult = dm.parameterizedExecuteNonQuery("data.posapi30.createDSKey",
                    new Object[]{
                            DSKey,
                            accountID,
                            userID,
                            gatewayUserID,
                            DSKeyCreatedOn.toString(),
                            DSKeyExpiresOn.toString(),
                            terminalID
                    }
            );

            //if the insert was a success
            if (insertResult != null && insertResult != -1) {
                //set the DSKey for this instance of the object
                return DSKey;
            } else {
                Logger.logMessage("Could not execute insert SQL", Logger.LEVEL.ERROR);
                return "ERROR";
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            String errorDetails = "Error in CommonAPI.createDSKey";
            return "ERROR";
        }

    }

    public static String createDSKey(Integer instanceUserID, Integer gatewayUserID, OtmLoginModel otmLoginModel) {
        try {

            //device session keys can be for employees (accountID) or managers (userID) and can be created for cloud users (gatewayUserID)
            Integer accountID = null;
            Integer userID = null;

            //if instanceUserId is null
            if (instanceUserID == null) {

                //set it as its default value of 0
                instanceUserID = 0;

            } else if (instanceUserID < 0) {  //if the instanceUserID on this instance is negative, then we are dealing with an employee (accountID)

                //make positive to store in DB
                accountID = instanceUserID * -1;

            } else { //if positive, then manager (userID)

                //set the instanceUserID to the userID so we know it's a manager (QC User)
                userID = instanceUserID;
            }

            if (gatewayUserID == null) {
                gatewayUserID = 0;
            } else if (gatewayUserID == 0) {   //if no gatewayUserID is found, the value will be it's default of 0, let's record it as NULL in the DeviceSessionKey table
                gatewayUserID = null;
            }

            //get the current timestamp and set to DSKeyCreatedOn
            Instant DSKeyCreatedOn = Instant.now();

            //create a secure random key
            String DSKey = new BigInteger(256, new SecureRandom()) + BigInteger.valueOf(DSKeyCreatedOn.getEpochSecond()).subtract(new BigInteger(8, new SecureRandom())).toString(64);

            Validation val = new Validation();

            //determine if the user checked "keep me logged in" to know when the DSKey should expire
            Instant DSKeyExpiresOn;
            if (val.getKeepLogged()) {
                //add 36524 days (100 years) to the DSKeyCreatedOn to determine when the DSKey should expire
                DSKeyExpiresOn = DSKeyCreatedOn.plus(Duration.ofDays(36524));
            } else {
                DSKeyExpiresOn = DSKeyCreatedOn.plus(Duration.ofMinutes(CommonAPI.getSessionTimeoutMinutes()));
            }

            Logger.logMessage("instanceUserID " + instanceUserID, Logger.LEVEL.DEBUG);
            Logger.logMessage("gateway " + gatewayUserID, Logger.LEVEL.DEBUG);
            Logger.logMessage("user " + userID, Logger.LEVEL.DEBUG);
            Logger.logMessage("account " + accountID, Logger.LEVEL.DEBUG);
            Logger.logMessage("dskey " + DSKey, Logger.LEVEL.DEBUG);
            Logger.logMessage("otmID "+ otmLoginModel.getOtm().getId(), Logger.LEVEL.DEBUG);

            //prepare sql parameters and then execute insert SQL
            Integer insertResult = dm.parameterizedExecuteNonQuery("data.posapi30.createDSKeyForOtm",
                    new Object[]{
                            DSKey,
                            accountID,
                            userID,
                            gatewayUserID,
                            DSKeyCreatedOn.toString(),
                            DSKeyExpiresOn.toString(),
                            otmLoginModel.getOtm().getId()
                    }
            );

            //if the insert was a success
            if (insertResult != null && insertResult != -1) {
                //set the DSKey for this instance of the object
                return DSKey;
            } else {
                Logger.logMessage("Could not execute insert SQL", Logger.LEVEL.ERROR);
                return "ERROR";
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            String errorDetails = "Error in CommonAPI.createDSKey";
            return "ERROR";
        }

    }

    //creates a new DSKey for the specified terminal, expires all existing DSKeys
    public static String createDSKey(Integer terminalID) throws Exception {
        try {
            //if no terminalID is found, then error out
            if ( terminalID == null || terminalID == 0 ) {
                throw new MissingDataException();
            }

            //expire all DSKeys for this TerminalID - to prevent multiple terminals from using the same mac address
            dm.parameterizedExecuteNonQuery("data.kiosk.expireTerminalDSKeys",
                new Object[]{
                    terminalID
                }
            );

            //get the current timestamp and set to DSKeyCreatedOn
            Instant DSKeyCreatedOn = Instant.now();

            //create a secure random key
            String DSKey = new BigInteger(256, new SecureRandom()) + BigInteger.valueOf(DSKeyCreatedOn.getEpochSecond()).subtract(new BigInteger(8, new SecureRandom())).toString(64);

            //determine if the user checked "keep me logged in" to know when the DSKey should expire
            Instant DSKeyExpiresOn = DSKeyCreatedOn.plus(Duration.ofDays(36524));

            //prepare sql parameters and then execute insert SQL
            Integer insertResult = dm.parameterizedExecuteNonQuery("data.posapi30.createDSKey",
                    new Object[]{
                            DSKey,
                            null,
                            null,
                            null,
                            DSKeyCreatedOn.toString(),
                            DSKeyExpiresOn.toString(),
                            terminalID
                    }
            );

            //if the insert was a success
            if ( insertResult != -1 ) {
                //set the DSKey for this instance of the object
                return DSKey;
            }

            //if insert failed
            Logger.logMessage("Could not execute insert SQL", Logger.LEVEL.ERROR);
            return "ERROR";

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return "ERROR";
        }

    }

    public static String createStationDSKey(Integer stationID) throws Exception {
        try {
            //if no terminalID is found, then error out
            if ( stationID == null || stationID == 0 ) {
                throw new MissingDataException();
            }

            //expire all DSKeys for this TerminalID - to prevent multiple terminals from using the same mac address
            dm.parameterizedExecuteNonQuery("data.station.expireStationDSKeys",
                    new Object[]{
                            stationID
                    }
            );

            //get the current timestamp and set to DSKeyCreatedOn
            Instant DSKeyCreatedOn = Instant.now();

            //create a secure random key
            String DSKey = new BigInteger(256, new SecureRandom()) + BigInteger.valueOf(DSKeyCreatedOn.getEpochSecond()).subtract(new BigInteger(8, new SecureRandom())).toString(64);

            //determine if the user checked "keep me logged in" to know when the DSKey should expire
            Instant DSKeyExpiresOn = DSKeyCreatedOn.plus(Duration.ofDays(36524));

            //prepare sql parameters and then execute insert SQL
            Integer insertResult = dm.parameterizedExecuteNonQuery("data.station.createStationDSKey",
                    new Object[]{
                            DSKey,
                            stationID,
                            DSKeyCreatedOn.toString(),
                            DSKeyExpiresOn.toString(),

                    }
            );

            //if the insert was a success
            if ( insertResult != -1 ) {
                //set the DSKey for this instance of the object
                return DSKey;
            }

            //if insert failed
            Logger.logMessage("Could not execute insert SQL", Logger.LEVEL.ERROR);
            return "ERROR";

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return "ERROR";
        }

    }

    public static String updateStationDSKey(String DSKey, Integer stationID, Instant expirationDate) {
        try {

            //if no terminalID is found, the value will be it's default of 0, let's record it as NULL in the DeviceSessionKey table
            if (stationID == null || stationID == 0) {
                stationID = null;
            }

            Logger.logMessage("dskey: " + DSKey, Logger.LEVEL.DEBUG);
            Logger.logMessage("stationID: " + stationID, Logger.LEVEL.DEBUG);

            String expirationDateString = (expirationDate != null) ? expirationDate.toString() : "";
            Logger.logMessage("expirationDate: " + expirationDateString, Logger.LEVEL.DEBUG);

            //prepare sql parameters and then execute insert SQL
            Integer updateResult = dm.parameterizedExecuteNonQuery("data.station.updateStationDSKey",
                    new Object[]{
                            DSKey,
                            stationID,
                            (expirationDate != null) ? expirationDate.toString() : null
                    }
            );

            //if the insert was a success
            if (updateResult != null && updateResult != -1) {
                //set the DSKey for this instance of the object
                return DSKey;
            } else {
                Logger.logMessage("Could not execute update SQL", Logger.LEVEL.ERROR);
                throw new MissingDataException("Could not update DSKey");
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            String errorDetails = "Error in CommonAPI.updateDSKey";
            return "ERROR";
        }
    }

    public static String updateDSKey(String DSKey, Integer terminalId, Integer userId, Instant expirationDate) {
        try {

            //if no terminalID is found, the value will be it's default of 0, let's record it as NULL in the DeviceSessionKey table
            if (terminalId == null || terminalId == 0) {
                terminalId = null;
            }

            Logger.logMessage("user: " + userId, Logger.LEVEL.DEBUG);
            Logger.logMessage("dskey: " + DSKey, Logger.LEVEL.DEBUG);
            Logger.logMessage("terminalId: " + terminalId, Logger.LEVEL.DEBUG);

            String expirationDateString = (expirationDate != null) ? expirationDate.toString() : "";
            Logger.logMessage("expirationDate: " + expirationDateString, Logger.LEVEL.DEBUG);

            //prepare sql parameters and then execute insert SQL
            Integer updateResult = dm.parameterizedExecuteNonQuery("data.posapi30.updateDSKey",
                    new Object[]{
                            DSKey,
                            terminalId,
                            userId,
                            (expirationDate != null) ? expirationDate.toString() : null
                    }
            );

            //if the insert was a success
            if (updateResult != null && updateResult != -1) {
                //set the DSKey for this instance of the object
                return DSKey;
            } else {
                Logger.logMessage("Could not execute update SQL", Logger.LEVEL.ERROR);
                throw new MissingDataException("Could not update DSKey");
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            String errorDetails = "Error in CommonAPI.updateDSKey";
            return "ERROR";
        }
    }

    //adds the employeeID to the DSKey record in QC_DeviceSessionKey when an employee is selected on 'Select Student' page
    public static Integer updatePersonAccountDSKey(String employeeID, HttpServletRequest request) {
        Integer updateResult = null;
        try {

            String mmhayesAuthHeader = request.getHeader("X-MMHayes-Auth");

            //if the mmhayesAuthHeader is valid, verify the contents of it (Device Session Key)
            if (mmhayesAuthHeader == null) {
                Logger.logMessage("Could not find X-MMHayes-Auth in the request header CommonAPI.updatePersonAccountDSKey", Logger.LEVEL.ERROR);
                throw new InvalidAuthException();
            }

            if(employeeID.equals("")) {
                Logger.logMessage("Missing employeeID in CommonAPI.updatePersonAccountDSKey()", Logger.LEVEL.ERROR);
                throw new MissingDataException("Could not update person account dskey");
            }

            updateResult = dm.parameterizedExecuteNonQuery("data.common.updateDSKeyConsiderPerson",
                    new Object[]{
                            employeeID,
                            mmhayesAuthHeader
                    }
            );

            if (updateResult != 1) {
                Logger.logMessage("Could not add employeeID to DSKey for person account DSKey", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }

        return updateResult;
    }

    //extends the DSKey record in QC_DeviceSessionKey for fingerprint authentication with SSO
    public static Integer updateSSODSKeyForFingerprintAuth(HttpServletRequest request) {
        Integer updateResult = null;
        try {

            String mmhayesAuthHeader = request.getHeader("X-MMHayes-Auth");

            //if the mmhayesAuthHeader is valid, verify the contents of it (Device Session Key)
            if (mmhayesAuthHeader == null) {
                Logger.logMessage("Could not find X-MMHayes-Auth in the request header CommonAPI.updateSSODSKeyForFingerprintAuth", Logger.LEVEL.ERROR);
                throw new InvalidAuthException();
            }

            //check if the app.properties 'site.security.extendedsessiontimeoutminutes' exists, if not it will set expiration to 90 days instead of 100 years - 1/25/2018 gematuszyk
            Instant DSKeyExpiresOn;
            LoginModel loginModel = new LoginModel();
            loginModel.setExtendSession(true);  //if keepLoggedIn is true then we want to extend the session
            loginModel.checkForExtendedSessionTime();
            DSKeyExpiresOn = loginModel.getExtendedSessionExpirationDate();

            updateResult = dm.parameterizedExecuteNonQuery("data.common.updateDSKeyConsiderSSOFingerprint",
                    new Object[]{
                            DSKeyExpiresOn.toString(),
                            mmhayesAuthHeader
                    }
            );

            if (updateResult != 1) {
                Logger.logMessage("Could not extend DSKey for Fingerprint Authentication with SSO", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }

        return updateResult;
    }

    //get ImportAllInactive flag from QC_Globals
    public static Boolean getImportAllInactive() throws Exception {
        Boolean importAllInactive = false;
        try {
            // Do NOT run on Gateway
            if(!isGateway()){
                //determine what the importAllInactive flag
                importAllInactive = Boolean.parseBoolean(dm.parameterizedExecuteScalar("data.common.getImportAllInactiveFlag", new Object[]{}).toString());
            }
        } catch (Exception ex) {
            Logger.logMessage("Exception caught in CommonAPI.getImportAllInactive()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
        return importAllInactive;
    }

    //get backend version
    public static String getAPIVersion() throws Exception {
        String APIVersion = "1.0";
        try {
            // Do NOT run on Gateway
            if(!isGateway()){
                APIVersion = dm.parameterizedExecuteScalar("data.common.getAPIVersion", new Object[]{}).toString();
            }
        } catch (Exception ex) {
            Logger.logMessage("Could not determine API Version in CommonAPI.getAPIVersion", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
        return APIVersion;
    }

    // Get Global AuthenticationType Setting
    public static String getGlobalAuthType(){
        String GlobalAuthType="";
        try{
            // Do NOT run on Gateway
            if(!isGateway()){
                GlobalAuthType = dm.parameterizedExecuteScalar("data.common.getGlobalAuthType", new Object[]{}).toString();
            }
        } catch(Exception ex){
            Logger.logMessage("Error in Method: getGlobalAuthType()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return GlobalAuthType;
    }

    // Get Global SSO_ShowQCAuthLoginLinkMYQC Setting
    public static String getSSO_ShowQCAuthLoginLinkMYQC(){
        String ShowQCAuthLoginLinkMYQC="";
        try{
            // Do NOT run on Gateway
            if(!isGateway()){
                ShowQCAuthLoginLinkMYQC = dm.parameterizedExecuteScalar("data.common.getSSO_ShowQCAuthLoginLinkMYQC", new Object[]{}).toString();
            }
        } catch(Exception ex){
            Logger.logMessage("Error in Method: getSSO_ShowQCAuthLoginLinkMYQC()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return ShowQCAuthLoginLinkMYQC;
    }

    public static boolean isGateway() {
        return getSecurityType().equalsIgnoreCase(SECURITY_TYPE_GATEWAY);
    }

    public static HashMap verifyDSKey(HttpServletRequest request) throws Exception {
        return verifyDSKey(request, false);
    }

    public static HashMap verifyDSKey(HttpServletRequest request, boolean retrieveDetails) throws Exception {
        HashMap DSKeyHM = new HashMap();

        try {
            if ( !retrieveDetails ) {
                CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
            } else {
                DSKeyHM = CommonAuthResource.determineAuthDetailsFromAuthHeader(request);
            }
            DSKeyHM.put("result", "yes");
        } catch (Exception ex) {
            DSKeyHM.put("result", "no");
        }

        return DSKeyHM;
    }

    //checks if a given time is between two other times
    public static boolean checkTimeBetween(String fromTime, String toTime, String givenTime, String compareType) {
        Boolean isBetween = false;

        try {
            //set the starting time
            Date timeFrom =  new SimpleDateFormat("HH:mm").parse(fromTime);
            Calendar calendarFrom = Calendar.getInstance();
            calendarFrom.setTime(timeFrom);

            //set the ending time
            Date timeTo =  new SimpleDateFormat("HH:mm").parse(toTime);
            Calendar calendarTo = Calendar.getInstance();
            calendarTo.setTime(timeTo);

            //set the given time
            Date timeGiven = new SimpleDateFormat("HH:mm").parse(givenTime);
            Calendar calendarGiven = Calendar.getInstance();
            calendarGiven.setTime(timeGiven);

            Date date = calendarGiven.getTime();
            if(compareType.equals("between")) { //if the given time is after the start time and before end time
                if (date.after(calendarFrom.getTime()) && date.before(calendarTo.getTime())) {
                    isBetween = true;
                }
            } else if (compareType.equals("start")) { //if the given time equals the start time and if before the end time
                if ( date.compareTo(calendarFrom.getTime()) == 0 && date.before(calendarTo.getTime()) ) {
                    isBetween = true;
                }
            } else if (compareType.equals("end")) { //if the given time is after the start time and equals the end time
                if (date.after(calendarFrom.getTime()) &&  date.compareTo(calendarTo.getTime()) == 0) {
                    isBetween = true;
                }
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return isBetween;
    }

    public static String convertToMilitaryTime(String time) {
        String militaryTime = "";

        try {
            SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");
            SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");
            militaryTime = date24Format.format(date12Format.parse(time));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return militaryTime;
    }

    public static String convertFromMilitaryTime(String time) {
        String militaryTime = "";

        try {
            SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");
            SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");
            militaryTime = date12Format.format(date24Format.parse(time));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return militaryTime;
    }

    //determines if the minute is an even 15 minute interval
    public static Boolean isEvenInterval(LocalTime time) {
        if(time.getMinute() != 0 && time.getMinute() != 15 && time.getMinute() != 30 && time.getMinute() != 45) {
            return false;
        } else {
           return true;
        }
    }

    //rounds the time down to the nearest 15 minute interval
    public static String getRoundedTime(LocalTime time) {
        int minute = (time.getMinute()/15) * 15;
        String min = String.valueOf(minute);
        if(minute ==0) {
            min = "00";
        }
        String hour = String.valueOf(time.getHour());
        if(time.getHour() < 10) {
            hour = "0"+time.getHour();
        }
        return hour + ":" + min;
    }

    public static LocalDateTime formatLocalDateTime(LocalDateTime dateTimeReference, LocalTime localTimeReference) {
        LocalDateTime dateTime = dateTimeReference;
        dateTime = dateTime.withHour(localTimeReference.getHour());
        dateTime = dateTime.withMinute(localTimeReference.getMinute());
        dateTime = dateTime.withSecond(0);
        dateTime = dateTime.withNano(0);

        return dateTime;
    }

    public static LocalDateTime updateLocalDateTime(LocalDateTime dateTimeReference, LocalTime localTimeReference) {
        LocalDateTime dateTime = dateTimeReference;
        dateTime = dateTime.withHour(localTimeReference.getHour());
        dateTime = dateTime.withMinute(localTimeReference.getMinute());

        return dateTime;
    }

    public static HashMap getBrandingDetails() {
        ArrayList<HashMap> result;
        HashMap brandingHM = new HashMap();

        try {
            if(!isGateway()){
                result =dm.parameterizedExecuteQuery("data.common.getBrandingDetails", new Object[]{}, true);
                if (result != null && result.size() == 1) {
                    brandingHM = result.get(0);

                } else {
                    Logger.logMessage("Could not determine branding details in CommonAPI.getBrandingDetails method.", Logger.LEVEL.ERROR);
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("Error in CommonAPI.getBrandingDetails", Logger.LEVEL.ERROR);
        }

        return brandingHM;
    }

    //records what changes are made to an account in the QC_EmployeeChanges table
    public static boolean recordAccountChange(String accountID, String field, String before, String after, String loggedInUserID) {
        try {
            //if the before and after values are not the same
            if (before.compareTo(after) != 0) {
                if (field.equals("User Password")) { //remove passwords so they do not get logged in the DB as plain text
                    before = "********";
                    after = "********";
                }
                return dm.serializeUpdate("data.myqc.recordAccountChangeAsMyQCUser", new Object[] {accountID, field, before, after, loggedInUserID}) != -1;
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error recording account change in recordAccountChange for field: "+field, Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return false;
        }
        return false;
    }

    //checks if the inputted password is valid for account creation
    public static boolean validatePassword(String plainTextPassword){
        String passStrength = CommonAPI.getPassStrength();
        Boolean passCriteriaMet = false;
        commonMMHFunctions commFunc = new commonMMHFunctions();

        if (passStrength.equals("loose")) {
            passCriteriaMet = commFunc.validatePasswordLoose(plainTextPassword);
        } else {
            passCriteriaMet = commFunc.validatePasswordStrict(plainTextPassword);
        }

        if(!passCriteriaMet){
            Logger.logMessage("Password did not meet strength requirement", Logger.LEVEL.ERROR);
        }

        return passCriteriaMet;
    }

    //get global settings and account groups for account creation
    public static ArrayList<HashMap> getAccountCreationDetails() {
        ArrayList<HashMap> accountCreationDetails = new ArrayList<HashMap>();
        if(isGateway()){
            // Don't Run on Gateway
            return accountCreationDetails;
        }

        try {
            //get the global settings
            ArrayList<HashMap> result =dm.parameterizedExecuteQuery("data.common.getAccountCreationGlobals", new Object[]{}, true);

            //if result is not null, add it as the first index to accountCreationDetails list for the front end
            if (result != null && result.size() == 1) {
                HashMap accountDetailsHM = result.get(0);
                accountCreationDetails.add(accountDetailsHM);
                accountCreationDetails.addAll( getAccountCreationAccountGroups() );
            } else {
                Logger.logMessage("Could not determine Account Creation globals in CommonAPI.getAccountCreationDetails method.", Logger.LEVEL.WARNING);
            }

        } catch (Exception ex) {
            Logger.logMessage("Error in CommonAPI.getAccountCreationDetails", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

        return accountCreationDetails;
    }

    public static ArrayList<HashMap> getAccountCreationAccountGroups() throws Exception {
        ArrayList<HashMap> accountCreationDetails = new ArrayList<HashMap>();
        Boolean foundSpendingProfile = false, foundAccountType = false;

        //get the account groups, mapped spending profiles and account types
        ArrayList<HashMap> accountGroupsList =dm.parameterizedExecuteQuery("data.common.getAccountCreationAccountGroupsWithAccessCode", new Object[]{}, true);

        //if no account groups returned, log the error
        if (accountGroupsList == null || accountGroupsList.size() == 0) {
            Logger.logMessage("No Account Groups are available for Account Creation", Logger.LEVEL.WARNING);
            return accountCreationDetails;
        }

        Integer id = 0;

        //loop over each account group hashmap in the account group list
        for(HashMap accountGroup : accountGroupsList) {

            //add the index of the list as the id (for the front end)
            accountGroup.put("id",id);

            //if the account group has a spending profile ID and a spending profile Name format the spending profiles in the account group
            if(accountGroup.get("TERMINALPROFILEID") != null && accountGroup.get("TERMINALPROFILEID") != "" && accountGroup.get("TERMINALPROFILENAME") != "") {
                foundSpendingProfile = true;

                //spending profiles are returned as a comma separated list for the account group from the databse, format them appropriately for the front end
                formatSpendingProfiles(accountGroup);

                //otherwise log that there are no spending profiles, set the spendingprofiles key to "" for the front end to display error
            } else {
                accountGroup.remove("TERMINALPROFILEID");
                accountGroup.remove("TERMINALPROFILENAME");
                accountGroup.put("spendingProfiles", "");
                Logger.logMessage("No Spending Profiles were found for Account Group '"+accountGroup.get("ACCOUNTGROUPNAME").toString()+"' CommonAPI.getAccountCreationDetails", Logger.LEVEL.WARNING);
            }

            //check if there is a account type
            if(accountGroup.get("ACCOUNTTYPEID") != null && accountGroup.get("ACCOUNTTYPEID") != "") {
                foundAccountType = true;

                //otherwise log that there is not account type for the account group
            } else {
                Logger.logMessage("No Account Types were found for Account Group '"+accountGroup.get("ACCOUNTGROUPNAME").toString()+"' CommonAPI.getAccountCreationDetails", Logger.LEVEL.WARNING);
            }

            //add the account group HM with it's formatted spending profiles to the accountCreationDetails list
            accountCreationDetails.add(accountGroup);
            id++;
        }

        //if there are no spending profiles found for any account group log error
        if(!foundSpendingProfile) {
            Logger.logMessage("No Spending Profiles were found for any Account Group for Account Creation in CommonAPI.getAccountCreationDetails", Logger.LEVEL.WARNING);
        }

        //if there are not accountt types found for any account group log error
        if(!foundAccountType) {
            Logger.logMessage("No Account Type was found for any Account Group for Account Creation in CommonAPI.getAccountCreationDetails", Logger.LEVEL.WARNING);
        }

        // make sure we can still create person accounts if the access code is active
        // if it is, return the complete list of account creation settings (employee + person account) with person account at the front
        ArrayList<HashMap> completeAcctCreationDetails = getPersonAcctCreationDetails();
        if(completeAcctCreationDetails != null) {
            completeAcctCreationDetails.addAll(accountCreationDetails);
            return completeAcctCreationDetails;
        }

        return accountCreationDetails;
    }

    //formats the spending profiles into their appropriate Account Group
    public static void formatSpendingProfiles(HashMap accountGroup) {
        try{
            //get the spendingProfileID and spendingProfileName
            String spendingProfileIDs = accountGroup.get("TERMINALPROFILEID").toString();
            String spendingProfileNames = accountGroup.get("TERMINALPROFILENAME").toString();

            //spending profiles ids and names are returned in a comma separated list from the database, format them into a list
            List<String> spendingProfileIDList = Arrays.asList(spendingProfileIDs.split("\\s*,\\s*"));
            List<String> spendingProfileNameList = Arrays.asList(spendingProfileNames.split("\\s*,\\s*"));

            //if the list of id's matches the list of names (should happen otherwise something wrong with database values)
            if(spendingProfileIDList.size() == spendingProfileNameList.size()) {
                ArrayList<HashMap> spendingProfilesList = new ArrayList<HashMap>();

                //loop over each spending profile id
                for(int id=0;id<spendingProfileIDList.size();id++) {
                    HashMap spendingProfileHM = new HashMap();

                    //put the id and name into a spendingProfile HashMap
                    spendingProfileHM.put("spendingProfileID", spendingProfileIDList.get(id));
                    spendingProfileHM.put("spendingProfileName", spendingProfileNameList.get(id));

                    //add the Hashmap to the spendingProfilesList
                    spendingProfilesList.add(spendingProfileHM);
                }

                //remove id and name values from the account group Hashmap
                accountGroup.remove("TERMINALPROFILEID");
                accountGroup.remove("TERMINALPROFILENAME");

                //add the ArrayList or spending profiles HashMaps to the account group
                accountGroup.put("spendingProfiles", spendingProfilesList);

            } else {
                Logger.logMessage("Spending Profile ID's do not have an associated Spending Profile Name for Account Group: "+accountGroup.get("ACCOUNTGROUPNAME").toString()+" in CommonAPI.getAccountCreationDetails method.", Logger.LEVEL.WARNING);
            }
        } catch (Exception ex) {
            Logger.logMessage("Error in CommonAPI.getAccountCreationDetails", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

    }

    private static ArrayList<HashMap> getPersonAcctCreationDetails() {
        ArrayList<HashMap> detailList = dm.parameterizedExecuteQuery("data.common.getPersonAcctCreationDetails", new Object[]{}, true);
        if(detailList != null && detailList.size() != 0 && detailList.get(0).get("ACCESSCODEID") != null) {
            return detailList;
        }

        return null;
    }

    //takes a comma separated string and converts it to an arraylist<string>, optionally with unique values
    public static ArrayList<String> convertCommaSeparatedStringToArrayList(String str, boolean enforceUniqueness) {
        ArrayList<String> uniqueStrArr = new ArrayList<String>();

        try {
            if ( str == null || str.length() == 0 ) {
                return uniqueStrArr;
            }

            //the regex here handles the cases of spaces included in the separation - "," | " ," | " , " all work
            ArrayList<String> strArr = new ArrayList<String>(Arrays.asList(str.split("\\s*,\\s*")));

            if ( !enforceUniqueness ) {
                return strArr;
            }

            for ( String entry : strArr ) {
                if ( uniqueStrArr.contains(entry) ) {
                    continue;
                }

                uniqueStrArr.add(entry);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Error occurred in CommonAPI.convertCommaSeparatedStringToArrayList", Logger.LEVEL.DEBUG);
        }

        return uniqueStrArr;
    }

    //checks if the user's account type is Gift Card
    public static String checkForGiftCardAccountType(Integer instanceUserID) {
        try {
            String accountType = CommonAPI.convertModelDetailToString(dm.parameterizedExecuteScalar("data.accounts.getEmployeeAccountTypeByID",
                new Object[]{
                        instanceUserID
                }));

            if(accountType != null && !accountType.isEmpty()) {
                return accountType;
            }

        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Error occurred in CommonAPI.checkForGiftCardAccountType()", Logger.LEVEL.DEBUG);
        }

        return "";
    }

    //gets the Quickcharge Tender ID from the online ordering terminal
    public static Integer getQuickchargeTenderID(Integer storeID) {
        ArrayList<HashMap> result;
        Integer tenderID = 1;

        try {
            result = dm.parameterizedExecuteQuery("data.ordering.getTerminalsQCTenderID", new Object[]{storeID}, true);

            //should only be one result
            if (result != null && result.size() == 1) {
                HashMap resultHM = result.get(0);

                if (resultHM.containsKey("QCTENDERID") && resultHM.get("QCTENDERID") != null) {
                    tenderID = Integer.parseInt(resultHM.get("QCTENDERID").toString());
                } else {
                    Logger.logMessage("TenderID not found in CommonAPI.getTerminalsQuickchargeTenderID result.", Logger.LEVEL.ERROR);
                }
            } else {
                Logger.logMessage("Could not determine Quickcharge tenderID from My QC terminalID in CommonAPI.getTerminalsQuickchargeTenderID method.", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        return tenderID;
    }


    //gets the Credit Card Tender ID from the online ordering terminal
    public static Integer getCreditCardTenderID(Integer storeID, Integer employeeID) {
        ArrayList<HashMap> result;
        Integer tenderID = null;

        try {
            result = dm.parameterizedExecuteQuery("data.ordering.getTerminalsCCTenderID", new Object[]{storeID, employeeID}, true);

            //should only be one result
            if (result != null && result.size() == 1) {
                HashMap resultHM = result.get(0);

                if (resultHM.containsKey("CCTENDERID") && resultHM.get("CCTENDERID") != null) {
                    tenderID = Integer.parseInt(resultHM.get("CCTENDERID").toString());
                } else {
                    Logger.logMessage("TenderID not found in CommonAPI.getTerminalsCreditCardTenderID result.", Logger.LEVEL.ERROR);
                }
            } else {
                Logger.logMessage("Could not determine Credit Card tenderID from My QC terminalID in CommonAPI.getTerminalsCreditCardTenderID method.", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        return tenderID;
    }

    //gets the Credit Card Tender ID from the online ordering terminal
    public static Integer getKioskCreditCardTenderID(Integer storeID) {
        ArrayList<HashMap> result;
        Integer tenderID = null;

        try {
            result = dm.parameterizedExecuteQuery("data.ordering.getTerminalsKioskCCTenderID", new Object[]{storeID}, true);

            //should only be one result
            if (result != null && result.size() == 1) {
                HashMap resultHM = result.get(0);

                if (resultHM.containsKey("POSKIOSKCCTENDERID") && resultHM.get("POSKIOSKCCTENDERID") != null) {
                    tenderID = Integer.parseInt(resultHM.get("POSKIOSKCCTENDERID").toString());
                } else {
                    Logger.logMessage("TenderID not found in CommonAPI.getKioskCreditCardTenderID result.", Logger.LEVEL.ERROR);
                }
            } else {
                Logger.logMessage("Could not determine Kiosk Credit Card tenderID from My QC terminalID in CommonAPI.getKioskCreditCardTenderID method.", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        return tenderID;
    }

    //check if the store is able to use credit cards as a tender
    public static boolean isStoreUsingCreditCardAsTender(HttpServletRequest request) throws Exception {
        boolean storeUsesCreditCardAsTender = false;

        Integer storeID = CommonAPI.checkRequestHeaderForStoreID(request);
        Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

        ArrayList<HashMap> creditCardAsTenderList = dm.parameterizedExecuteQuery("data.myqc.checkIfStoreIsUsingCreditCardAsTender", new Object[]{
                storeID,
                employeeID
        }, true);

        if(creditCardAsTenderList != null && creditCardAsTenderList.size() > 0) {
            HashMap creditCardAsTenderHM = creditCardAsTenderList.get(0);
            storeUsesCreditCardAsTender = CommonAPI.convertModelDetailToBoolean(creditCardAsTenderHM.get("USESCREDITCARDASTENDER"));
        }

        return storeUsesCreditCardAsTender;
    }


    //determines if the employee can do account funding or can only setup a payment method
    public static boolean checkIfPayrollGroupAllowsFunding(HttpServletRequest request) throws Exception {
        Boolean paymentMethodAllowed = false;

        Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

        Object paymentMethod = dm.parameterizedExecuteScalar("data.myqc.checkIfPayrollGroupAllowsFunding", new Object[] {employeeID});

        if ( paymentMethod != null && !paymentMethod.toString().equals("")) {
            paymentMethodAllowed = Boolean.valueOf(paymentMethod.toString());
        }

        return paymentMethodAllowed;
    }

    public static HashMap getAliases() throws Exception {
        HashMap aliasHM = new HashMap();
        try {

            ArrayList<HashMap> aliasList = dm.parameterizedExecuteQuery("data.common.getGlobalAliasNames", new Object[]{}, true);

            if(aliasList != null && aliasList.size() > 0) {
                aliasHM = aliasList.get(0);
            }

        } catch (Exception ex) {
            Logger.logMessage("Could not determine Credit Card tenderID from My QC terminalID in CommonAPI.getTerminalsCreditCardTenderID method.", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
        return aliasHM;
    }

    //returns a badge number for the qr code generation
    public static long generateQRCodeBadgeNumber(HashMap badgeSettings, HttpServletRequest request) {
        String badgeNumber = "";
        String finalBadgeNumber = "";
        long badge = 0;

        try {
            Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

            String badgePrefix = ""; String badgeSuffix = ""; Integer qrCodeLength = 0;

            if(badgeSettings.get("badgePrefix") != null && !badgeSettings.get("badgePrefix").toString().equals("")) {
                badgePrefix = badgeSettings.get("badgePrefix").toString();
            }

            if(badgeSettings.get("badgeSuffix") != null && !badgeSettings.get("badgeSuffix").toString().equals("")) {
                badgeSuffix = badgeSettings.get("badgeSuffix").toString();
            }

            if(badgeSettings.get("qrCodeLength") != null && !badgeSettings.get("qrCodeLength").toString().equals("")) {
                qrCodeLength = Integer.parseInt(badgeSettings.get("qrCodeLength").toString());
            }

            //if qr code length is set to 0 then use the employee's badge
            if ( qrCodeLength.equals(0) ) {
                Object badgeObj = dm.parameterizedExecuteScalar("data.accounts.getEmployeeBadgeNumber",
                        new Object[]{
                                badgeNumber
                        });

                if ( badgeObj == null || badgeObj.toString() == null || badgeObj.toString().length() == 0 ) {
                    return badge;
                }

                return Long.parseLong( badgeObj.toString() );
            }

            Integer badgePrefixLength = badgePrefix.length();
            Integer badgeSuffixLength = badgeSuffix.length();

            //get badge length without prefix and suffix length
            Integer badgeLength = qrCodeLength - badgePrefixLength - badgeSuffixLength;

            if (badgeLength > 0 ) {
                Integer tryCount = 0;
                boolean keepTrying;

                do {

                    //generate random badge number
                    String badgeNum = createRandomNumber(badgeLength);

                    if(badgeNum.startsWith("0")) { //replace 0 if it is first in the string
                        badgeNum = "1" + badgeNum.substring(1, badgeNum.length());
                    }

                    //return the badge WITHOUT the prefix and suffix to the front end
                    finalBadgeNumber = badgeNum;

                    //add on prefix and/or suffix
                    if (badgePrefixLength > 0 && badgeSuffixLength > 0) {
                        badgeNumber = badgePrefix + badgeNum + badgeSuffix;
                    } else if (badgePrefixLength > 0) {
                        badgeNumber = badgePrefix + badgeNum;
                    } else if (badgeSuffixLength > 0) {
                        badgeNumber = badgeNum + badgeSuffix;
                    } else {
                        badgeNumber = badgeNum;
                    }

                    //check if badge number is already in use
                    String checkResult = CommonAPI.convertModelDetailToString(dm.parameterizedExecuteScalar("data.accounts.checkBadgeNumberInUse",
                            new Object[]{
                                    badgeNumber
                            }));

                    //duplicate badge number found, try again
                    if (checkResult != null && !checkResult.isEmpty() && Integer.parseInt(checkResult) < 0) {
                        keepTrying = true;

                        tryCount++;
                        if (tryCount > 100) {
                            Logger.logMessage("ERROR: Could NOT find valid account (badge) Number in AccountModel.checkDuplicateAcctNumber - tried 100 times.. giving up...", Logger.LEVEL.ERROR);
                            keepTrying = false;
                        }

                    } else {
                        keepTrying = false;
                    }

                } while (keepTrying);

                Logger.logMessage("Successfully generated random badge number for QC Code in CommonAPI.generateQRCodeBadgeNumber()", Logger.LEVEL.TRACE);

                //if successfully inserted records in QC_Badge and QC_BadgeAssignment, finally set badge as randomly generated badge number
                Integer insertResult = createBadgeNumberRecords(badgeNumber, employeeID);
                if (insertResult == 1) {
                    badge = Long.parseLong(finalBadgeNumber);  //return badge WITHOUT prefix and suffix to the front end
                }
            }

        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Error occurred in CommonAPI.generateQRCodeBadgeNumber()", Logger.LEVEL.DEBUG);
        }

        return badge;
    }

    //create records in QC_Badge and QC_BadgeAssignment for the user
    public static Integer createBadgeNumberRecords(String badgeNumber, Integer employeeID) {
        Integer insertResult = null;

        try {
            //create record in QC_Badge
            Integer badgeIDResult = dm.parameterizedExecuteNonQuery("data.accounts.createQRBadgeRecord",
                    new Object[]{
                        badgeNumber
                    }
            );

            if (badgeIDResult < 0) {
                Logger.logMessage("Could not execute insert SQL in CommonAPI.createBadgeNumberRecords", Logger.LEVEL.ERROR);
                return null;
            }

            //create record in QC_BadgeAssignment
            insertResult = dm.parameterizedExecuteNonQuery("data.accounts.createBadgeAssignmentRecord",
                    new Object[]{
                        badgeIDResult,
                        employeeID
                    }
            );

            if (insertResult != 1) {
                Logger.logMessage("Could not execute insert SQL in CommonAPI.createBadgeNumberRecords", Logger.LEVEL.ERROR);
                return insertResult;
            }

        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Error occurred in CommonAPI.createBadgeNumberRecords()", Logger.LEVEL.DEBUG);
        }

        return insertResult;
    }

    //inactivate all badge numbers in QC_Badge where the badge type is QR Code (2)
    public static Integer inactivateQRCodeBadgeNumber(HttpServletRequest request) throws Exception {
        Integer updateResult = null;
        Integer employeeID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

        try {
            ArrayList<HashMap> badgesToInactivate = dm.parameterizedExecuteQuery("data.accounts.getBadgesToInactivate", new Object[]{employeeID}, true);

            if(badgesToInactivate == null || badgesToInactivate.size() == 0) {
                updateResult = 1;

            } else {
                ArrayList<String> badgeList = new ArrayList<>();
                for(HashMap badge : badgesToInactivate) {
                    if(badge.containsKey("BADGEID") && !badge.get("BADGEID").toString().equals("")) {
                        badgeList.add(CommonAPI.convertModelDetailToString(badge.get("BADGEID")));
                    }
                }
                String badgeIDs = String.join(",", badgeList);

                updateResult = dm.parameterizedExecuteNonQuery("data.accounts.inactivateQRBadgeRecord", new Object[]{badgeIDs});

                if (updateResult < 1) {
                    Logger.logMessage("Could not execute inactivate Badge Numbers for Quick Pay in CommonAPI.inactivateQRCodeBadgeNumber()", Logger.LEVEL.ERROR);
                    return updateResult;
                }

                updateResult = dm.parameterizedExecuteNonQuery("data.accounts.inactivateQRBadgeAssignmentRecord", new Object[]{badgeIDs});

                if (updateResult < 1) {
                    Logger.logMessage("Could not execute inactivate Badge Assignment Records for employeeID: "+employeeID+" in Quick Pay in CommonAPI.inactivateQRCodeBadgeNumber()", Logger.LEVEL.ERROR);
                    return updateResult;
                }
            }

        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Error occurred in CommonAPI.inactivateQRCodeBadgeNumber()", Logger.LEVEL.DEBUG);
        }

        return updateResult;
    }

    public static String createRandomNumber(Integer numLength) {
        StringBuilder str = new StringBuilder();
        Random rand = new Random();
        for(int i=0;i<numLength;i++) {
            str.append(rand.nextInt(9));
        }
        return str.toString();
    }

    public static Integer getRestConnectionTimeout(){

        Integer timeout = 5000;

        try {
            ArrayList<HashMap> results = dm.parameterizedExecuteQuery("data.common.getRestConnectionTimeout",new Object[]{}, true);
            if(!results.isEmpty() && results.get(0).containsKey("CONNECTIONTIMEOUT")){
                timeout = Integer.parseInt(results.get(0).get("CONNECTIONTIMEOUT").toString());
            }
        } catch (Exception ex){
            Logger.logMessage("Error in method: getRestConnectionTimeout", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return timeout;
    }

    public static Integer getRestReadTimeout(){

        Integer timeout = 60000;

        try {
            ArrayList<HashMap> results = dm.parameterizedExecuteQuery("data.common.getRestReadTimeout",new Object[]{}, true);
            if(!results.isEmpty() && results.get(0).containsKey("READTIMEOUT")){
                timeout = Integer.parseInt(results.get(0).get("READTIMEOUT").toString());
            }
        } catch (Exception ex){
            Logger.logMessage("Error in method: getRestReadTimeout", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return timeout;
    }

    public static String getCreditCardTransInfo(AccountPaymentMethodModel accountPaymentMethodModel, boolean partialAuth) {
        String creditCardTransInfo = "PartialAuth=false;";

        if(partialAuth) {
            creditCardTransInfo = "PartialAuth=true;";
        }

        creditCardTransInfo += "ManualOrSwipe=Manual;";
        creditCardTransInfo += "TranType=Credit;";

        if(accountPaymentMethodModel != null) {
            Integer paymentMethodTypeID = accountPaymentMethodModel.getPaymentMethodTypeID();

            String cardType = "";
            if(paymentMethodTypeID == 1) {
                cardType = "VISA";
            } else if (paymentMethodTypeID == 2) {
                cardType = "AMEX";
            } else if (paymentMethodTypeID == 3) {
                cardType = "M/C";
            } else if (paymentMethodTypeID == 4) {
                cardType = "DCVR";
            } else if (paymentMethodTypeID == 5) {
                cardType = "JCB";
            } else if (paymentMethodTypeID == 6) {
                cardType = "DINERSCLUB";
            }

            creditCardTransInfo += "CardType=" + cardType + ";";
            String lastFour = accountPaymentMethodModel.getPaymentMethodLastFour();

            creditCardTransInfo += "AcctNo=XXXXXXXXXXXX"+lastFour+";";
        }

        return creditCardTransInfo;
    }

    //check if the MyQC products are active, have inventory, are not disabled, etc...
    public static ArrayList<HashMap> checkProductsInTransactionForMyQC(Integer terminalID, String transactionProducts) {

        //check if any of the products in any of the transactions are no longer active, have no inventory, etc
        return dm.parameterizedExecuteQuery("data.orders.checkProductsInTransactionByStore",
                new Object[]{
                        terminalID,
                        transactionProducts
                },
                true
        );
    }

    //check if the POS products are active, are not disabled, etc...
    public static ArrayList<HashMap> checkProductsInTransactionForPOS(String transactionProducts) {

        //check if any of the products in any of the transactions are no longer active etc
        return dm.parameterizedExecuteQuery("data.orders.checkProductsInTransactionByTerminal",
                new Object[]{
                        transactionProducts
                },
                true
        );
    }

    public static ArrayList<String> determineAllKeypadsInStore(Integer terminalID, Integer homeKeypadID) {
        ArrayList<String> keypadList = new ArrayList<>();
        try {

            keypadList.add( homeKeypadID.toString() );

            keypadList = getAllKeypadIDsInStore( homeKeypadID.toString(), keypadList, terminalID );

        } catch (Exception e) {
            Logger.logMessage("Could not retrieve valid keypads in CommonAPI.determineAllKeypadsInStore()", Logger.LEVEL.DEBUG);
        }

        return keypadList;
    }

    //get all the keypad ids that are active in the current store
    public static ArrayList<String> getAllKeypadIDsInStore(String keypadID, ArrayList<String> keypadList, Integer terminalID) {
        ArrayList<HashMap> innerKeypads = new ArrayList<>();
        try {
            innerKeypads = dm.serializeSqlWithColNames("data.orders.getAllKeypadsByKeypadID",
                    new Object[]{
                            keypadID,
                            terminalID
                    },
                    false,
                    false
            );

            if ( innerKeypads != null && innerKeypads.size() > 0 ) {
                for ( HashMap keypadHM : innerKeypads ) {
                    if ( keypadHM.get("KEYPADID") != null && !keypadHM.get("KEYPADID").toString().equals("") ) {
                        String innerKeypadID = keypadHM.get("KEYPADID").toString();

                        if ( keypadList.contains(innerKeypadID) ) {
                            continue;
                        }

                        keypadList.add(innerKeypadID);

                        getAllKeypadIDsInStore(innerKeypadID, keypadList, terminalID);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine all products in current keypad in CommonAPI.getAllKeypadIDsInStore", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return keypadList;
    }

    //go through each keypad root until all the products have been accounted for
    public static ArrayList<String> getAllProductsByKeypad(ArrayList<String> keypadList, Integer terminalID) {
        ArrayList<String> productList = new ArrayList<>();

        String keypadListStr = "";
        if(keypadList.size() > 0) {
            keypadListStr = String.join(",", keypadList);
        }

        try {
            ArrayList<HashMap> products = dm.serializeSqlWithColNames("data.orders.getAllProductsByKeypadID",
                    new Object[]{
                            keypadListStr,
                            terminalID
                    },
                    false,
                    false
            );

            if(products != null && products.size() > 0) {
                for(HashMap product : products) {
                    if(product.get("PRODUCTID") != null && !product.get("PRODUCTID").toString().equals("")) {
                        String productID = product.get("PRODUCTID").toString();

                        if( !productList.contains(productID) ) {
                            productList.add(productID);
                        }
                    }
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not determine all products in current keypad in CommonAPI.getAllProductsByKeypad", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return productList;
    }

    //checks against the value of LastUpdateDTM to see if the model has been updated since last being cached
    public static String getLastUpdateDTMForKeyapds(String keypadList) {
        String lastUpdateDTM = "";

        Object result = dm.parameterizedExecuteScalar("data.orders.getLastUpdateDTMForKeypads",
                new Object[]{
                        keypadList
                }
        );

        if(result != null) {
            lastUpdateDTM = result.toString();
        }

        return lastUpdateDTM;
    }

    //checks against the value of LastUpdateDTM to see if the model has been updated since last being cached
    public static boolean checkForUpdates(String keypadsInStore, LocalDateTime lastUpdateDTM, LocalDateTime lastUpdateCheck) {
        boolean needsUpdate = false;

        LocalDateTime now = LocalDateTime.now();

        if ( now.isAfter(lastUpdateCheck.plusSeconds(500)) && lastUpdateDTM != null ) {

            Object result = dm.parameterizedExecuteScalar("data.orders.checkStoreKeypadsForUpdates",
                    new Object[]{
                            keypadsInStore,
                            lastUpdateDTM.toLocalDate().toString()+" "+lastUpdateDTM.plusSeconds(1).toLocalTime().toString()
                    }
            );

            //keypads needs to be updated if there is no lastUpdateDTM or if the result is 1
            needsUpdate = ( result == null || result.toString().equals("1") );
        }

        return needsUpdate;
    }

    public boolean checkForUpdatesByTerminalID(Integer storeID, LocalDateTime lastUpdateDTM) throws Exception {
        boolean needsUpdate = false;
        LocalDateTime now = LocalDateTime.now();

        //if it's been more than 5 minutes since the last check for updates
        if ( lastUpdateDTM != null ) {

            Object result = dm.parameterizedExecuteScalar("data.rotation.checkRotationsForUpdatesByStoreID",
                    new Object[]{
                            storeID,
                            lastUpdateDTM.toLocalDate().toString()+" "+lastUpdateDTM.plusSeconds(1).toLocalTime().toString()
                    }
            );

            //keypads needs to be updated if there is no lastUpdateDTM or if the result is 1
            needsUpdate = ( result == null || result.toString().equals("1") );

        }

        return needsUpdate;
    }

    public static Integer getAssignedVersionForSoftwareUpdate(HttpServletRequest request) throws Exception {
        Integer terminalID = 0;
        Integer assignedVersionID = null;

        if ( CommonAuthResource.isKOA(request) ) {
            terminalID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
        }

        if (terminalID == null || terminalID == 0) {
            return assignedVersionID;
        }

        Object assignedVersionObj = dm.parameterizedExecuteScalar("data.userFunctions.getAssignedVersionIDByTerminal",
                new Object[] {
                        terminalID,
                });

        if(assignedVersionObj != null) {
            assignedVersionID = Integer.parseInt(assignedVersionObj.toString());
        }

        return assignedVersionID;
    }

    public static Boolean doesDsKeyContainEmpId(LoginModel loginModel) {
        Boolean accountLoggedIn = false;

        if (loginModel != null
                && loginModel.getEmployeeId() != null
                && !loginModel.getEmployeeId().toString().isEmpty()) {
            //This is an account login

            accountLoggedIn = true;
        }

        return accountLoggedIn;
    }

    /***
     * Verify that the terminal type is MMHayes POS.  This can be used to enforce authorization on resources by the terminal type.
     * @param terminal
     * @throws Exception
     */
    public static void verifyMMHayesPosAuthorization(TerminalModel terminal) throws Exception {
        verifyAuthorization(terminal, PosAPIHelper.TerminalType.MMHAYES_POS);
    }

    /***
     * Verify that the terminal model matches the type passed in
     * @param terminal
     * @param terminalType
     * @throws Exception
     */
    public static void verifyAuthorization(TerminalModel terminal, PosAPIHelper.TerminalType terminalType) throws Exception {
        if (terminal != null && terminal.getTypeId() != null) {

            if (terminalType.toInt() == terminal.getTypeId()){

            } else {
                throw new InvalidAuthException("Invalid authorization for this terminal type.");
            }
        }
    }

    /**
     * <p>Checks the webapps directory to get the names of all the WARs running on this machine.</p>
     *
     * @return An {@link ArrayList} of {@link String} corresponding to the names of WARs running on this machine.
     */
    public static ArrayList<String> getWARsOnThisMachine () {

        // get the MMHayes webapps directory
        File webappsDirectory = commonMMHFunctions.getDirectory("web", "webapps");
        if ((webappsDirectory != null) && (webappsDirectory.isDirectory())) {
            // get all WARs within the webapps directory
            FilenameFilter warFilter = (dir, name) -> name.toLowerCase().endsWith(".war");
            File[] warFiles = webappsDirectory.listFiles(warFilter);
            if (!DataFunctions.isEmptyGenericArr(warFiles)) {
                Logger.logMessage(String.format("CommonAPI.getWARsOnThisMachine found %s WARs on the machine with a hostname of %s.",
                        Objects.toString(warFiles.length, "N/A"),
                        Objects.toString(PeripheralsDataManager.getPHHostname(), "N/A")), Logger.LEVEL.TRACE);
                ArrayList<String> wars = new ArrayList<>();
                for (File war : warFiles) {
                    wars.add(FilenameUtils.removeExtension(war.getName()));
                }
                return wars;
            }
            else {
                Logger.logMessage(String.format("No WARs were found on this machine within the webapps directory at the path %s, now returning null.",
                        Objects.toString(webappsDirectory.getAbsolutePath(), "N/A")), Logger.LEVEL.ERROR);
                return new ArrayList<>();
            }
        }
        else {
            Logger.logMessage(String.format("Failed to obtain the webapps directory on this machine at the path %s, as a result the WARs within that directory can't be determined, now returning null!",
                    Objects.toString((webappsDirectory != null ? webappsDirectory.getAbsolutePath() : "N/A"), "N/A")), Logger.LEVEL.ERROR);
            return new ArrayList<>();
        }

    }

    /**
     * <p>Checks whether or not the given record can be found in the local print queue.</p>
     *
     * @param paPrinterQueueID The print queue ID to check.
     * @return Whether or not the given record can be found in the local print queue.
     */
    public static boolean isInLocalPrintQueue (int paPrinterQueueID) {

        // validate the print queue ID
        if (paPrinterQueueID <= 0) {
            Logger.logMessage("The print queue ID passed to CommonAPI.isInLocalPrintQueue must be greater than 0, unable to determine " +
                    "whether or not the record could be found within the local print queue, now returning false.", Logger.LEVEL.ERROR);
            return false;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.kms.GetIsInLocalPrintQueue").addIDList(1, paPrinterQueueID);

        return sql.getSingleBooleanField(dm);
    }

    /**
     * <p>Queries the database to get a lookup between a primary key ID and the name associated with the record.</p>
     *
     * @param idCol The name {@link String} of the primary key ID column.
     * @param nameCol The name {@link String} of the name ID column.
     * @param table The name {@link String} of the table to query.
     * @param conditions Any additional content to add to the query as a {@link String}, for example WHERE, ORDER BY, HAVING, etc...
     * @return A {@link HashMap} whose primary key is the primary key ID as a {@link String} and whose value {@link String} is the name associated with the record.
     */
    public static HashMap<String, String> getNameLookup (String idCol, String nameCol, String table, String conditions) {

        // validate the idCol
        if (!StringFunctions.stringHasContent(idCol)) {
            Logger.logMessage("The primary key ID column passed to CommonAPI.getNameLookup can't be null or empty, unable to get the lookup, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        // validate the nameCol
        if (!StringFunctions.stringHasContent(nameCol)) {
            Logger.logMessage("The name column passed to CommonAPI.getNameLookup can't be null or empty, unable to get the lookup, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        // validate the table
        if (!StringFunctions.stringHasContent(table)) {
            Logger.logMessage("The table name passed to CommonAPI.getNameLookup can't be null or empty, unable to get the lookup, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        DynamicSQL sql = new DynamicSQL("data.kms.GetNameLookup").addTableColumnList(1, new String[]{idCol, nameCol}).addTableORColumnName(2, table);
        if (StringFunctions.stringHasContent(conditions)) {
            sql.addDirectly(3, " " + conditions);
        }

        // query the database to get the lookup
        HashMap<String, String> lookup = new HashMap<>();
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dm));
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            for (HashMap hm : queryRes) {
                String id = HashMapDataFns.getStringVal(hm, idCol);
                String name = HashMapDataFns.getStringVal(hm, nameCol);
                lookup.put(id, name);
            }
        }

        return lookup;
    }

// <editor-fold desc="Get Email Settings">
    /**
     * <p>Queries the database to get the email settings.</p>
     *
     * @return A {@link HashMap} with a {@link String} key for the setting and {@link String} value for the setting.
     */
    @SuppressWarnings("Duplicates")
    public static HashMap<String, String> getEmailSettings () {
        HashMap<String, String> emailSettings = new HashMap<>();

        // query the database
        DynamicSQL sql = new DynamicSQL("data.ReceiptGen.EmailCfg");
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(new DataManager()));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
            HashMap queryResHM = queryRes.get(0);
            emailSettings.put("SMTPSERVER", HashMapDataFns.getStringVal(queryResHM, "SMTPSERVER"));
            emailSettings.put("SMTPUSER", HashMapDataFns.getStringVal(queryResHM, "SMTPUSER"));
            emailSettings.put("SMTPPASSWORD", StringFunctions.decodePassword(HashMapDataFns.getStringVal(queryResHM, "SMTPPASSWORD")));
            emailSettings.put("SMTPPORT", String.valueOf(HashMapDataFns.getIntVal(queryResHM, "SMTPPORT")));
            emailSettings.put("SMTPFROMADDRESS", HashMapDataFns.getStringVal(queryResHM, "SMTPFROMADDRESS"));
        }

        return emailSettings;
    }

    /**
     * <p>Queries the database to get the email settings.</p>
     *
     * @param log The file path {@link String} of the file to log information related to this method to.
     * @param dataManager The {@link DataManager} to use to query the database.
     * @return A {@link HashMap} with a {@link String} key for the setting and {@link String} value for the setting.
     */
    @SuppressWarnings("Duplicates")
    public static HashMap<String, String> getEmailSettings (String log, DataManager dataManager) {
        HashMap<String, String> emailSettings = new HashMap<>();

        // validate the log file
        if (!StringFunctions.stringHasContent(log)) {
            Logger.logMessage("The log file passed to CommonAPI.getEmailSettings can't be null or empty, unable to get the email settings from the database, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        // validate the DataManager
        if (dataManager == null) {
            Logger.logMessage("The DataManager passed to CommonAPI.getEmailSettings can't be null, unable to get the email settings from the database, now returning null.", log, Logger.LEVEL.ERROR);
            return null;
        }

        // query the database
        DynamicSQL sql = new DynamicSQL("data.ReceiptGen.EmailCfg").setLogFileName(log);
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(sql.serialize(dataManager));
        if ((!DataFunctions.isEmptyCollection(queryRes)) && (queryRes.size() == 1) && (!DataFunctions.isEmptyMap(queryRes.get(0)))) {
            HashMap queryResHM = queryRes.get(0);
            emailSettings.put("SMTPSERVER", HashMapDataFns.getStringVal(queryResHM, "SMTPSERVER"));
            emailSettings.put("SMTPUSER", HashMapDataFns.getStringVal(queryResHM, "SMTPUSER"));
            emailSettings.put("SMTPPASSWORD", HashMapDataFns.getStringVal(queryResHM, "SMTPPASSWORD"));
            emailSettings.put("SMTPPORT", String.valueOf(HashMapDataFns.getIntVal(queryResHM, "SMTPPORT")));
            emailSettings.put("SMTPFROMADDRESS", HashMapDataFns.getStringVal(queryResHM, "SMTPFROMADDRESS"));
        }

        return emailSettings;
    }
// </editor-fold>

// <editor-fold desc="Validate Email Address">
    /**
     * <p>Validates an email address.</p>
     *
     * @param email The email address {@link String} to validate.
     * @return Whether or not the email address is valid.
     */
    public static boolean isValidEmail (String email) {

        if (!StringFunctions.stringHasContent(email)) {
            Logger.logMessage("No email address was passed to CommonAPI.isValidEmail, the email can't be validated, now returning false.", Logger.LEVEL.ERROR);
            return false;
        }

        Pattern EMAIL_ADDRESS_REGEX =  Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = EMAIL_ADDRESS_REGEX.matcher(email);

        return matcher.find();
    }
// </editor-fold>

    // return whether or not to treat FP as test or prod - true if test, false if prod, default to prod
    public static boolean envUsesFreedomPayUAT() {
        try {
            boolean isUATEnv = Boolean.parseBoolean(MMHProperties.getAppSetting("site.freedompay.debug").trim());
            Logger.logMessage("Environment is Freedompay UAT: " + isUATEnv, Logger.LEVEL.DEBUG);
            return isUATEnv;
        } catch(Exception e) {
            Logger.logMessage("Could not read property site.freedompay.debug, treating this environment as production.", Logger.LEVEL.ERROR);
            Logger.logException(e);
        }
        return false;
    }

}
