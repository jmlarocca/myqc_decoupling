package com.mmhayes.common.api.resources;


/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */

import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.MMHError;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.datawire.DatawireLoggingHandler;
import com.mmhayes.common.datawire.DatawireRegistrationRequestModel;
import com.mmhayes.common.datawire.DatawireRegistrationResponseModel;
import com.mmhayes.common.login.LoginModel;
import com.mmhayes.common.starbucks.GiftCardRequestModel;
import com.mmhayes.common.starbucks.SVdot.SVdotTransactionSerializer;
import com.mmhayes.common.starbucks.StarbucksResponseModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.utils.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.datawire.vxn3.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Handler;

//REST Resource for /{instance}/api/starbucks/
@Path("starbucks")
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Bad Request", response = MMHError.class),
        @ApiResponse(code = 403, message = "Forbidden", response = MMHError.class),
        @ApiResponse(code = 404, message = "Not Found", response = MMHError.class),
        @ApiResponse(code = 405, message = "Bad Method", response = MMHError.class),
        @ApiResponse(code = 500, message = "Generic", response = MMHError.class),
        @ApiResponse(code = 501, message = "Unknown", response = MMHError.class),
        @ApiResponse(code = 502, message = "Bad Gateway", response = MMHError.class),
        @ApiResponse(code = 505, message = "Bad HTTP Verb", response = MMHError.class)
})
public class DatawireResource {
    private static final DatawireLoggingHandler loggingHandler = new DatawireLoggingHandler();
    private static final int TRANSACTION_TIMEOUT = 30000;//seconds for VXN transactions(not registration)
    @POST
    @Path("/register")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public DatawireRegistrationResponseModel registerTerminal(DatawireRegistrationRequestModel registrationModel, @Context HttpServletRequest request) throws Exception {
        LoginModel loginModel = LoginModel.createLoginModel(request);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);
        try{
            String responseMessage = "";
            String DID = null;
            int terminalID = terminalModel.getId();
            DataManager dm = new DataManager();
            //look these up using the terminals's mapped RC and the giftCardSetupRevCent mappings to get to the giftCardSetup
            Integer giftCardSetupID = registrationModel.getGiftCardSetup().getId();
            ArrayList<HashMap> firstDataSettings = dm.parameterizedExecuteQuery("data.posanywhere.getFistDataSettings", new Object[]{giftCardSetupID}, true);
//TODO: add the fields to the terminal model
            ArrayList<HashMap> terminalSettings = dm.parameterizedExecuteQuery("data.posanywhere.getStarbucksTerminalSettings", new Object[]{terminalID}, true);
            if(firstDataSettings.size() > 0 && terminalSettings.size() > 0){
                //application settings
                ArrayList<String> urls = new ArrayList();
                String[] URLArray = firstDataSettings.get(0).get("FIRSTDATASERVICEURLS").toString().split(",");
                for(String s: URLArray){urls.add(s);}
                String MID = firstDataSettings.get(0).get("FIRSTDATAMERCHANTID").toString();
                String serviceID = firstDataSettings.get(0).get("FIRSTDATASERVICEID").toString();
                String applicationID = firstDataSettings.get(0).get("FIRSTDATAAPPLICATIONID").toString();
                //terminal settings
                String altMID = terminalSettings.get(0).get("STARBUCKSSTORENUMBER").toString();
                String terminalNumber = terminalSettings.get(0).get("STARBUCKSTERMINALNUMBER").toString();
                String TID = altMID + terminalNumber;
                Object DIDObj = terminalSettings.get(0).get("DATAWIREID");
                if(DIDObj != null && DIDObj.toString().length() > 0){
                    DID = DIDObj.toString();
                    responseMessage = "Terminal was previously registered with Datawire";
                }

                if(DID == null){
                    responseMessage = "Terminal is not registered with Datawire";
                    //perform the registration
                    try {
                        VXN.setCipherSuite(
                                "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384," +
                                "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256," +
                                "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384," +
                                "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256");
                    } catch (VXNException e) {
                        Logger.logException(e);
                    }

                    SelfRegistration selfRegistration = new SelfRegistration(urls, MID, TID, serviceID, applicationID);
                    selfRegistration.setMaxRegisterAttempts(3);
                    selfRegistration.setRegisterAttemptsWaitMilliseconds(30000);
                    DID = selfRegistration.registerMerchant();
                    selfRegistration.activateMerchant();
                    Logger.logMessage("API returned DID of:" + DID, Logger.LEVEL.IMPORTANT);

                    if(DID != null){//we have a new DID
                        responseMessage = "Terminal is now registered with Datawire!";
                        dm.serializeUpdate("data.posanywhere.updateTerminalDID", new Object[]{terminalID, DID});
                        urls.clear();
                        urls.addAll(selfRegistration.getUrlList());
                        String newURLs = "";
                        for(int u = 0; u < urls.size(); u++){
                            newURLs += urls.get(u);
                            if(u < urls.size()-1)
                                newURLs += ",";
                        }
                        Logger.logMessage("Obtained new service URLs: " + newURLs);
                        dm.serializeUpdate("data.posanywhere.setFistDataURLs", new Object[]{newURLs, giftCardSetupID});
                    }
                }
            }
            DatawireRegistrationResponseModel response = new DatawireRegistrationResponseModel();
            response.setDID(DID);
            response.setResponseMessage(responseMessage);
            return response;
        } catch(Exception ex){
            Logger.logMessage("Error in method: registerTerminal()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            DatawireRegistrationResponseModel response = new DatawireRegistrationResponseModel();
            response.setResponseMessage(ex.getMessage());
            return response;
        }
    }

    @POST
    @Path("/inquire")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public StarbucksResponseModel balanceInquiry(GiftCardRequestModel giftCard, @Context HttpServletRequest request) throws Exception {
        String transactionTypeID = "0400";
        LoginModel loginModel = LoginModel.createLoginModel(request);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);
        giftCard.setTerminalID(terminalModel.getId());
        return createSimpleTransaction(giftCard, transactionTypeID, false);

    }

    @POST
    @Path("/connection-check")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public StarbucksResponseModel applicationCheck(GiftCardRequestModel giftCard,@Context HttpServletRequest request) throws Exception {

        //Prints the list of available Ciphers, only useful for verifying that
//        for (Provider provider: Security.getProviders()) {
//            System.out.println(provider.getName());
//            for (String key: provider.stringPropertyNames())
//                System.out.println("\t" + key + "\t" + provider.getProperty(key));
//        }
//        System.out.println("All Ciphers");
//        String[] ciphers = ((SSLServerSocketFactory)javax.net.ssl.SSLServerSocketFactory.getDefault()).getSupportedCipherSuites();
//        for(String s: ciphers){
//            System.out.println(s);
//        }
//        System.out.println("Default Ciphers");
//        String[] ciphers_def = ((SSLServerSocketFactory)javax.net.ssl.SSLServerSocketFactory.getDefault()).getDefaultCipherSuites();
//        for(String s: ciphers_def){
//            System.out.println(s);
//        }


        LoginModel loginModel = LoginModel.createLoginModel(request);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);
        giftCard.setTerminalID(terminalModel.getId());
        return createSimpleTransaction(giftCard, SVdotTransactionSerializer.TRANSACTION_TYPE_APP_CHECK, false);

    }

    @POST
    @Path("/activate")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public StarbucksResponseModel activate(GiftCardRequestModel giftCard,@Context HttpServletRequest request) throws Exception {
        String transactionTypeID = "0100";
        LoginModel loginModel = LoginModel.createLoginModel(request);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);
        giftCard.setTerminalID(terminalModel.getId());
        return createSimpleTransaction(giftCard, transactionTypeID, true);
    }

    @POST
    @Path("/void/activate")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public StarbucksResponseModel voidActivate(GiftCardRequestModel giftCard, @Context HttpServletRequest request) throws Exception {
        String transactionTypeID = "0802";
        LoginModel loginModel = LoginModel.createLoginModel(request);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);
        giftCard.setTerminalID(terminalModel.getId());
        return createSimpleTransaction(giftCard, transactionTypeID, true);
    }

    @POST
    @Path("/redeem")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public StarbucksResponseModel redeem(GiftCardRequestModel giftCard, @Context HttpServletRequest request) throws Exception {
        String transactionTypeID = "0202";
        LoginModel loginModel = LoginModel.createLoginModel(request);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);
        giftCard.setTerminalID(terminalModel.getId());
        return createSimpleTransaction(giftCard, transactionTypeID, true);
    }

    @POST
    @Path("/void/redeem")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public StarbucksResponseModel voidRedeem(GiftCardRequestModel giftCard, @Context HttpServletRequest request) throws Exception {
        String transactionTypeID = "0800";
        LoginModel loginModel = LoginModel.createLoginModel(request);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);
        giftCard.setTerminalID(terminalModel.getId());
        return createSimpleTransaction(giftCard, transactionTypeID, true);
    }

    @POST
    @Path("/reload")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public StarbucksResponseModel reload(GiftCardRequestModel giftCard, @Context HttpServletRequest request) throws Exception {
        String transactionTypeID = "0300";
        LoginModel loginModel = LoginModel.createLoginModel(request);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);
        giftCard.setTerminalID(terminalModel.getId());
        return createSimpleTransaction(giftCard, transactionTypeID, true);
    }

    @POST
    @Path("/void/reload")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public StarbucksResponseModel voidReload(GiftCardRequestModel giftCard, @Context HttpServletRequest request) throws Exception {
        String transactionTypeID = "0801";
        LoginModel loginModel = LoginModel.createLoginModel(request);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);
        giftCard.setTerminalID(terminalModel.getId());
        return createSimpleTransaction(giftCard, transactionTypeID, true);
    }

    @POST
    @Path("/cashout")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public StarbucksResponseModel cashout(GiftCardRequestModel giftCard, @Context HttpServletRequest request) throws Exception {
        String transactionTypeID = "0600";
        LoginModel loginModel = LoginModel.createLoginModel(request);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);
        giftCard.setTerminalID(terminalModel.getId());
        return createSimpleTransaction(giftCard, transactionTypeID, true);
    }

    @POST
    @Path("/void/cashout")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public StarbucksResponseModel voidCashout(GiftCardRequestModel giftCard, @Context HttpServletRequest request) throws Exception {
        String transactionTypeID = "0800";
        LoginModel loginModel = LoginModel.createLoginModel(request);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);
        giftCard.setTerminalID(terminalModel.getId());
        return createSimpleTransaction(giftCard, transactionTypeID, true);
    }

//    @POST
//    @Path("/reload-on-hold")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    // Creates a transaction
//    public StarbucksResponseModel reloadOnHold(GiftCardModel giftCard, @Context HttpServletRequest request){
//        String transactionTypeID = "0308";
//        return createSimpleTransaction(giftCard, transactionTypeID, true);
//    }
//
//    @DELETE
//    @Path("/reload-on-hold")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    // Creates a transaction
//    public StarbucksResponseModel voidReloadOnHold(GiftCardModel giftCard, @Context HttpServletRequest request){
//        String transactionTypeID = "0801";
//        return createSimpleTransaction(giftCard, transactionTypeID, true);
//    }

//    @POST
//    @Path("/cashout-active-status")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    // Creates a transaction
//    public StarbucksResponseModel cashoutActiveStatus(GiftCardModel giftCard, @Context HttpServletRequest request){
//        String transactionTypeID = "0605";
//        return createSimpleTransaction(giftCard, transactionTypeID, true);
//    }
//
//    @DELETE
//    @Path("/cashout-active-status")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    // Creates a transaction
//    public StarbucksResponseModel voidCashoutActiveStatus(GiftCardModel giftCard, @Context HttpServletRequest request){
//        String transactionTypeID = "0800";//weird, but this is not voided by the same as a cashout (0800)
//        return createSimpleTransaction(giftCard, transactionTypeID, true);
//    }

    /**
     * Integration with the Datawire API to send SVdot transaction payloads
     * @param giftCard the giftcard being used
     * @param transactionTypeID the type of transaction to send
     * @param reversable Toggle for performing transaction reversals when a response is not returned (this should be true for financial and locking transactions)
     * @return a parsed SVdot response
     */
    private StarbucksResponseModel createSimpleTransaction(GiftCardRequestModel giftCard, String transactionTypeID, boolean reversable){
        ConfigureLogging();//TODO: move this to a place it will only be called once.
        StarbucksResponseModel response = null;

        //set up how many retries / transaction reversals we should perform
        int MAX_ATTEMPTS;
        int MAX_REVERSALS;
        if(!reversable){
            MAX_ATTEMPTS = 1;
            MAX_REVERSALS = 0;
        }else{
            MAX_ATTEMPTS = 3;
            MAX_REVERSALS = 4;
        }

        DataManager dm = new DataManager();
        Integer giftCardSetupID = giftCard.getGiftCardSetup().getId();
        ArrayList<HashMap> firstDataSettings = dm.parameterizedExecuteQuery("data.posanywhere.getFistDataSettings", new Object[]{giftCardSetupID}, true);
        ArrayList<HashMap> terminalSettings = dm.parameterizedExecuteQuery("data.posanywhere.getStarbucksTerminalSettings", new Object[]{giftCard.getTerminalID()}, true);
        if(firstDataSettings.size() > 0 && terminalSettings.size() > 0){
            //validate we have a DID
            if(terminalSettings.get(0).get("DATAWIREID") == null || terminalSettings.get(0).get("DATAWIREID").toString().length() == 0){
                //the DID is not set
                response = new StarbucksResponseModel();
                response.responseCode = "-1";
                response.responseMessage = "Terminal is not registered or DatawireID has not been saved";
            }

            //application settings
            List urls = new ArrayList<String>();
            String[] URLArray = firstDataSettings.get(0).get("FIRSTDATASERVICEURLS").toString().split(",");
            for(String s: URLArray){urls.add(s);}
            String MID = firstDataSettings.get(0).get("FIRSTDATAMERCHANTID").toString();
            String serviceID = firstDataSettings.get(0).get("FIRSTDATASERVICEID").toString();
            String applicationID = firstDataSettings.get(0).get("FIRSTDATAAPPLICATIONID").toString();
            //terminal settings
            String altMID = terminalSettings.get(0).get("STARBUCKSSTORENUMBER").toString();//"25849";
            String terminalNumber = terminalSettings.get(0).get("STARBUCKSTERMINALNUMBER").toString();//"1001";
            String DID = terminalSettings.get(0).get("DATAWIREID").toString();//"00037302225892113089";//
            String TID = altMID + terminalNumber;
            //generate the payload
            char[] payload = null;
            char[] responsePayload = null;
            if(transactionTypeID.compareTo(SVdotTransactionSerializer.TRANSACTION_TYPE_APP_CHECK) == 0){
                payload = SVdotTransactionSerializer.serializeConnectionCheck(MID, altMID, terminalNumber).toCharArray();
            }else{
                payload = SVdotTransactionSerializer.serializeRequest(giftCard, transactionTypeID, MID, altMID, terminalNumber).toCharArray();
            }
            Logger.logMessage("Serialized request payload: " + new String(payload), Logger.LEVEL.TRACE);

            //set the ciphers
            try {
                VXN.setCipherSuite(
                        "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384," +
                        "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256," +
                        "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384," +
                        "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256");
            } catch (VXNException e) {
                Logger.logException(e);
            }

            VXN vxn = null;
            try{
                vxn = VXN.getInstance(urls, "dspFile", DID, MID, TID, serviceID, applicationID);
                vxn.setTimeout(TRANSACTION_TIMEOUT);
                vxn.startSavingUrls("dspFile");
            }catch (VXNException ex){
                Logger.logMessage("Error getting VXN Instance in method: createSimpleTransaction()", Logger.LEVEL.ERROR);
                Logger.logException(ex);
            }

            //Transaction Logic
            try{
                boolean successful = false;//break out of the loop if we get a resposnse
                boolean failedCompletely = false;//we attempted 4 transaction reversals and they all failed
                long lastRequestTime = 0;
                for(int attempts = 1; attempts <= MAX_ATTEMPTS && !successful && !failedCompletely; attempts++){//make up to 4 attempts to send the transaction
                    //make a new simple transaction
                    if(lastRequestTime > System.currentTimeMillis() - 30000){
                        //we need to wait until 30 seconds have passed before sending another reverse
                        long sleepTime = lastRequestTime - (System.currentTimeMillis() - 30000);
                        Logger.logMessage("Sleeping for  "+ sleepTime +" before sending transaction attempt " + attempts);
                        Thread.sleep(sleepTime);
                    }

                    try{
                        SetUniqueTransactionID(giftCard);
                        String clientRef = getClientRef(giftCard.getUniqueTransactionId(), giftCard.getTerminalID(), giftCard.getTerminalVersion());
                        SimpleTransaction transaction = vxn.newSimpleTransaction(clientRef);
                        transaction.setPayload(payload);
                        Logger.logMessage("Attempt "+ attempts +" to send trasnaction " + transactionTypeID + " with payload :" + new String(payload));
                        lastRequestTime = System.currentTimeMillis();
                        transaction.executeXmlRequest();
                        responsePayload = transaction.getPayload();

                    }catch (VXNException ex){
                        Logger.logMessage("Error Sending Transaction in method: createSimpleTransaction()", Logger.LEVEL.ERROR);
                        Logger.logException(ex);
                    }
                    //check if we got a response back
                    if(responsePayload != null && responsePayload.length > 0){
                        successful = true;//this will end the loop
                        //response had been obtained, parse it
                        try{
                            if(responsePayload != null && responsePayload.length > 0){
                                response = SVdotTransactionSerializer.deserializeResponse(responsePayload);
                                response.uniqueTransactionId = giftCard.getUniqueTransactionId();
                            }
                        }catch (Exception e){
                            Logger.logMessage("Failed to parse the transaction response payload", Logger.LEVEL.ERROR);
                            Logger.logException(e);
                        }
                    }else{
                        //we need to perform a timeout reversal
                        Logger.logMessage("Unsuccessful Datawire response...attempting reversal", Logger.LEVEL.TRACE);
                        //if the timeout reversals arent responded to, send up to 4,
                        boolean successfulReversal = false;

                        for(int reversalAttempts = 1; reversalAttempts <= MAX_REVERSALS && !successfulReversal; reversalAttempts++){//make up to 4 attempts to send the transaction
                            //make a new simple transaction
                            if(lastRequestTime > System.currentTimeMillis() - 30000){
                                //we need to wait until 30 seconds have passed before sending another reverse
                                long sleepTime = lastRequestTime - (System.currentTimeMillis() - 30000);
                                Logger.logMessage("Sleeping for  "+ sleepTime +" before sending reversal attempt " + reversalAttempts);
                                Thread.sleep(sleepTime);
                            }

                            char[] reversalResponsePayload = null;
                            try{
                                char[] reversedPayload = SVdotTransactionSerializer.serializeTimoutReversal(new String(payload));
                                SetUniqueTransactionID(giftCard);
                                String clientRef = getClientRef(giftCard.getUniqueTransactionId(), giftCard.getTerminalID(), giftCard.getTerminalVersion());//not sure if we need multiple clientRefs
                                SimpleTransaction transaction = vxn.newSimpleTransaction(clientRef);
                                transaction.setPayload(reversedPayload);
                                Logger.logMessage("Attempt " + reversalAttempts + " to send reversal of transaction " + transactionTypeID + " with payload :" + new String(reversedPayload));
                                lastRequestTime = System.currentTimeMillis();
                                transaction.executeXmlRequest();
                                reversalResponsePayload = transaction.getPayload();
                            }catch (VXNException ex){
                                Logger.logMessage("Error Sending Timeout Reversal in method: createSimpleTransaction()", Logger.LEVEL.ERROR);
                                Logger.logException(ex);
                            }
                            if(reversalResponsePayload != null)
                                Logger.logMessage("Datawire response payload:" + new String(reversalResponsePayload), Logger.LEVEL.TRACE);
                            else{
                                Logger.logMessage("Datawire response payload: null", Logger.LEVEL.TRACE);
                            }
                            //check if we got a response back
                            if(reversalResponsePayload != null && reversalResponsePayload.length > 0){
                                successfulReversal = true;//this will end the loop
                                StarbucksResponseModel reversalResponse = SVdotTransactionSerializer.deserializeResponse(reversalResponsePayload);
                                //create the appropriate log message
                                if(reversalResponse.responseCode == "00")
                                    Logger.logMessage("Successfully Reversed the " + transactionTypeID+ " transaction!", Logger.LEVEL.TRACE);
                                else if(reversalResponse.responseCode == "33")
                                    Logger.logMessage("The original " + transactionTypeID+ " transaction never made it to the Closed Loop system. Nothing more to reverse", Logger.LEVEL.TRACE);

                            }
                        }
                        if(!successfulReversal)
                            failedCompletely = true;
                    }
                    //at this point we know if the response was successful (stop trying), failed but was reversed (try again), or failed completely (stop trying)
                }

                if(successful){
                    //we sent a transaction and got a response payload back.
                    Logger.logMessage("Successful Datawire response. Payload:" + String.valueOf(responsePayload), Logger.LEVEL.TRACE);
                }else if(failedCompletely){
                    //we sent a transaction but do not know if it was received or reversed...
                    Logger.logMessage("A " + transactionTypeID + " transaction failed to be properly sent through Datawire, and failed to be reversed. " +
                            "The current status of the transaction is unknown.", Logger.LEVEL.IMPORTANT);
                    Logger.logMessage("Failed Payload: " + String.valueOf(payload), Logger.LEVEL.IMPORTANT);

                    response = new StarbucksResponseModel();
                    response.responseCode = "-1";
                    response.responseMessage = "The transaction failed to be properly sent through Datawire, and failed to be reversed. The current status of the transaction is unknown.";

                }else{
                    //we attempted to send the transaction 3 times, but failed 3 times and reversed each time
                    Logger.logMessage("A " + transactionTypeID + " transaction failed to be properly sent through Datawire within the maximum number of attempts" +
                            "The current status of the transaction is reversed/not sent.", Logger.LEVEL.TRACE);
                    Logger.logMessage("Failed Payload: " + String.valueOf(payload), Logger.LEVEL.TRACE);
                    response = new StarbucksResponseModel();
                    response.responseCode = "-1";
                    response.responseMessage = "The transaction failed to be properly sent through Datawire within the maximum number of attempts. The current status of the transaction is is reversed/not sent.";
                }
            //Modified from VXN Sample
            } catch (Throwable e) {//Is this really a good idea???
                // Print stack trace for the exception,
                // and all chained exceptions it brings in.
                if(e.getMessage() != null)
                    Logger.logMessage(e.getMessage(), Logger.LEVEL.ERROR);
                if (e instanceof net.datawire.vxn3.VXNException &&
                        e.getCause() instanceof net.datawire.vxn3.VXNStatusException) {
                    // The VXNStatusException is thrown when a request returns bad status code.
                    // In most cases this means that the transaction was sent to the
                    // Datawire Secure Transport Network but the error was detected before it
                    // reached the service provider.
                    VXNStatusException se = (net.datawire.vxn3.VXNStatusException)e.getCause();
                    Logger.logMessage("Status Code:"+se.getStatusCode()+" Reason:"+se.getStatusText(), Logger.LEVEL.ERROR);
                }
                if (e instanceof net.datawire.vxn3.VXNException &&
                        e.getCause() instanceof net.datawire.vxn3.VXNReturnCodeException) {
                    // The VXNReturnCodeException is thrown when a transaction reaches the
                    // service provider and an error is detected.
                    VXNReturnCodeException re = (net.datawire.vxn3.VXNReturnCodeException)e.getCause();
                    Logger.logMessage("Return Code:"+re.getReturnCode(), Logger.LEVEL.ERROR);
                }

                response = new StarbucksResponseModel();
                response.responseCode = "-1";
                response.responseMessage = e.getMessage();

            }
        }else{
            Logger.logMessage("Unable to obtain the Datawire and terminal settings from the database! TerminalID: " + giftCard.getTerminalID(), Logger.LEVEL.ERROR);
            response = new StarbucksResponseModel();
            response.responseCode = "-1";
            response.responseMessage = "Unable to obtain the Datawire and terminal settings from the database.";

        }

        return response;
    }

    private void SetUniqueTransactionID(GiftCardRequestModel model){
        long time = System.currentTimeMillis();
        time = (time % 86400000)/1000;//get the current seconds in the day max 86400
        //Terminal ID can be a max of 525232 before we start losing precision.
        //left pad with 0s
        String timeString = time + "";
        int len = timeString.length();
        for(int i = len; i < 5; i++){//should be 5 digits
            timeString = "0"+timeString;
        }
        String ID = model.getTerminalID() + "" + time; //gives a max representable long of 525232 86400 to fit in 7 base 34 digits
        ID = Long.toString(Long.parseLong(ID), Character.MAX_RADIX);
        if(ID.equals("0000000"))
            Logger.logMessage("Failed to generate Unique transactionID from " + model.getTerminalID() + " and " + time, Logger.LEVEL.ERROR);
        model.setUniqueTransactionId(ID);
    }
    private String getClientRef(String uniqueTransactionID, int terminalID, String version){

        String versionNumber = "";
        String revisionNumber = "";
        if(version != null && version.toString().length() != 0 && version.toString().compareTo("NO VERSION") != 0){
            //we have a version
            String[] parts = version.toString().split("\\.");
            for(int i = parts[0].length(); i < 3;i++){
                parts[0] = "0" + parts[0];
            }
            versionNumber = parts[0].substring(0,3);

            for(int i = 1; i < parts.length && revisionNumber.length() < 3; i++){
                revisionNumber += parts[i];
            }
            for(int i = revisionNumber.length(); i < 3;i++){
                revisionNumber = revisionNumber + "0";
            }
            revisionNumber = revisionNumber.substring(0,3);
        }else{
            //no version found...
            versionNumber="000";
            revisionNumber="000";
        }
        String tr = uniqueTransactionID;
        if(tr.length()  < 7){
            for(int i = tr.length(); i < 7; i++){
                tr = "0" + tr;
            }
        }else{
            tr = tr.substring(tr.length()-7, tr.length());
        }
        String versionString = versionNumber + revisionNumber;
        tr = tr + "V"  + versionString;
        return tr;
    }

    /**
     * This function hooks up our custom logging handler into the Datawire API
     */
    public static void ConfigureLogging(){
        java.util.logging.Logger datawireLogger = java.util.logging.Logger.getLogger("net.datawire.vxn3");
        Handler[] handlers = datawireLogger.getHandlers();
        boolean found = false;
        for(int i = 0; i < handlers.length; i++){
            if(handlers[i] == loggingHandler){
                found = true;
            }
        }
        if(!found)
            datawireLogger.addHandler(loggingHandler);
    }

}
