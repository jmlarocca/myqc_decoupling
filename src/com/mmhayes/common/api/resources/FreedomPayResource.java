package com.mmhayes.common.api.resources;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Public Common Resource to access FreedomPay EndPoints
 */

import com.mmhayes.common.api.clients.FreedomPayClient;
import com.mmhayes.common.funding.freedompay.FreedomPayCaptureTransactionResponse;
import com.mmhayes.common.funding.freedompay.FreedomPayCreateTransactionResponse;
import com.mmhayes.common.funding.freedompay.FreedomPayGetTransactionResponse;
import com.mmhayes.common.funding.freedompay.FreedomPayRequest;
import com.mmhayes.common.utils.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

//REST Resource for /{instance}/api/freedompay/
@Path("freedompay")
public class FreedomPayResource {

   FreedomPayClient client = new FreedomPayClient();

    @POST
    @Path("/createtransaction")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public FreedomPayCreateTransactionResponse createTransaction(FreedomPayRequest freedomPayRequest, @Context HttpServletRequest request) {
        try{
            return client.createTransaction(freedomPayRequest, request);
        } catch(Exception ex){
            Logger.logMessage("Error in method: createTransaction()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            return null;
        }
    }

    @POST
     @Path("/createverifcationtransaction")
     @Produces(MediaType.APPLICATION_JSON)
     @Consumes(MediaType.APPLICATION_JSON)
     // Creates a transaction
     public FreedomPayCreateTransactionResponse createVerificationTransaction(FreedomPayRequest freedomPayRequest, @Context HttpServletRequest request) {
        try{
            return client.createVerificationTransaction(freedomPayRequest, request);
        } catch(Exception ex){
            Logger.logMessage("Error in method: createTransaction()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            return null;
        }
    }

    @POST
    @Path("/createtokentransaction")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public FreedomPayCreateTransactionResponse createTokenTransaction(FreedomPayRequest freedomPayRequest, @Context HttpServletRequest request) {
        try{
            return client.createTokenTransaction(freedomPayRequest, request);
        } catch(Exception ex){
            Logger.logMessage("Error in method: createTransaction()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            return null;
        }
    }

    @POST
    @Path("/gettransaction")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public FreedomPayGetTransactionResponse getTransaction(String transaction, @Context HttpServletRequest request) {
        try{
            return client.getTransaction(transaction, request);
        } catch(Exception ex){
            Logger.logMessage("Error in method: createTransaction()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            return null;
        }
    }

    @POST
    @Path("/canceltransaction")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public boolean cancelTransaction(String transaction, @Context HttpServletRequest request) {
        try{
            return client.cancelTransaction(transaction, request);
        } catch(Exception ex){
            Logger.logMessage("Error in method: cancelTransaction()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            return false;
        }
    }

    @POST
    @Path("/capturetransaction")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    // Creates a transaction
    public FreedomPayCaptureTransactionResponse captureTransaction(String transaction, @Context HttpServletRequest request) {
        try{
            return client.captureTransaction(transaction, request);
        } catch(Exception ex){
            Logger.logMessage("Error in method: captureTransaction()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            return null;
        }
    }

}
