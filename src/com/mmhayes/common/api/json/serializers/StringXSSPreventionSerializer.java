package com.mmhayes.common.api.json.serializers;

//mmhayes dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.modules.*;
import com.mmhayes.common.dataaccess.*;

//other dependencies
import com.fasterxml.jackson.annotation.JsonTypeId;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;


/*
 JSON String Serializer

 Created with IntelliJ IDEA.
 User: jrmitaly
 Date: 7/13/15
 Time: 2:27 PM

 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-04-14 14:45:00 -0400 (Wed, 14 Apr 2021) $: Date of last commit
 $Rev: 13785 $: Revision of last commit

Notes: This is a customer serializer built to add XSS protection to our JSON serialization process
*/
public class StringXSSPreventionSerializer<T> extends JsonSerializer<String> {

    @Override
    public void serialize(String value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (value != null) {
            if (CommonAPI.HTML_ENCODE_API_RESPONSE_STRINGS){
                value = commonMMHFunctions.sanitizeStringFromXSSWithESAPI(value);
            } else {
                value = commonMMHFunctions.sanitizeStringFromXSS(value);
            }
            jsonGenerator.writeString(value);
        }
    }

    @Override
    public boolean isEmpty(SerializerProvider provider, String value) {
        return (value == null || value.isEmpty());
    }
}