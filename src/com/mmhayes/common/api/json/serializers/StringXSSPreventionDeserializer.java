package com.mmhayes.common.api.json.serializers;

//MMHayes Dependencies
import com.mmhayes.common.dataaccess.commonMMHFunctions;

//API Dependencies
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;

//Other Dependencies
import java.io.IOException;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-08-29 11:12:59 -0400 (Wed, 29 Aug 2018) $: Date of last commit
 $Rev: 7701 $: Revision of last commit
*/
public class StringXSSPreventionDeserializer<T> extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String str = jsonParser.getText();
        return commonMMHFunctions.sanitizeStringFromXSS(str, null);
    }
}
