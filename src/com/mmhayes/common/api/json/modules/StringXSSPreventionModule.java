package com.mmhayes.common.api.json.modules;

//mmhayes dependencies
import com.mmhayes.common.api.json.serializers.*;

//other dependencies
import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;

/*
 REST API Response Filter

 Created with IntelliJ IDEA.
 User: jrmitaly
 Date: 7/13/15
 Time: 2:27 PM

 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit

Notes: This is a filter for all REST API responses
*/
public class StringXSSPreventionModule extends SimpleModule {
    private static final String NAME = "StringModule";
    private static final VersionUtil VERSION_UTIL = new VersionUtil() {};

    public StringXSSPreventionModule() {
        super(NAME, VERSION_UTIL.version());
        addSerializer(StringXSSPreventionSerializer.class, new StringXSSPreventionSerializer());
        addDeserializer(StringXSSPreventionDeserializer.class, new StringXSSPreventionDeserializer());
    }
}