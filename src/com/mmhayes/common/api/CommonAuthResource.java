package com.mmhayes.common.api;

//mmhayes dependencies

import com.mmhayes.common.api.errorhandling.exceptions.InvalidAuthException;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.api.errorhandling.exceptions.SessionTimeoutException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.login.LoginModel;
import com.mmhayes.common.login.LoginSettingsModel;
import com.mmhayes.common.login.OtmLoginModel;
import com.mmhayes.common.utils.HttpSessionCollector;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.StringFunctions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

//other dependencies

/*
 Common Auth Resource
 
 Created with IntelliJ IDEA.
 User: jrmitaly
 Date: 11/29/14
 Time: 1:43 PM
 
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-09-24 15:39:24 -0400 (Fri, 24 Sep 2021) $: Date of last commit
 $Rev: 15440 $: Revision of last commit

 Notes: This class contains common methods used on the instance and gateway Auth Resources
*/


public class CommonAuthResource {

    private static DataManager dm = new DataManager(); //the usual data manager
    private static HashMap<String, HashMap> loginCache = new HashMap<>();

    //this method gets settings for the login page (method is safe, no data in, no sensitive data out)
    public static HashMap getLoginSettings() {
        HashMap loginSettings = new HashMap();
        try {
            //get the password strength requirement (for resetting passwords)
            loginSettings.put("passStrength", CommonAPI.getPassStrength()); //gets PasswordStrengthID from Globals

            //TODO:(low) Decide if this is the long term solution for deciding if we are in the cloud or not -jrmitaly
            //determine if this instance is on the ground or not
            loginSettings.put("onGround", Boolean.toString(CommonAPI.getOnGround())); //from app.property file

            // Check the database to make sure it's online
            loginSettings.put("online", Boolean.toString(CommonAPI.checkDBOnline())); //checks the count of Globals records

            // Check the setting for showing the keep me logged in checkbox
            loginSettings.put("showKeepLogged", Boolean.toString(CommonAPI.showKeepLogged())); //gets ShowKeepLogged from Globals

            //Check the database to get the importAllInactive flag
            loginSettings.put("importAllInactive", Boolean.toString(CommonAPI.getImportAllInactive())); //gets ImportAllInactive from Globals

            // Get the Server Version
            String APIVersion = CommonAPI.getAPIVersion();
            loginSettings.put("APIVersion", APIVersion ); //gets MyQCAPIVersion from Globals

            // Get the Global AuthenticationTypeID
            loginSettings.put("globalAuthType", CommonAPI.getGlobalAuthType()); //gets AuthenticationTypeID from Globals

            // Get the ShowQCAuthLoginLink setting from the Globals Settings
            loginSettings.put("showQCAuthLoginLinkMYQC", CommonAPI.getSSO_ShowQCAuthLoginLinkMYQC()); //gets SSO_ShowQCAuthLoginLinkMYQC from Globals

            //Get Branding Details
            loginSettings.put("brandingDetails", CommonAPI.getBrandingDetails()); //gets multiple branding fields from Globals

            //Get Account Creation Details
            loginSettings.put("accountCreation", CommonAPI.getAccountCreationDetails()); //gets multiple account creation fields from Globals, then gets account groups available for account creation (not from Globals)

            //Get Enable Fingerprint details
            if ( CommonAPI.isGateway() ) {
                loginSettings.put("enableFingerprint", "false");
            } else {
                Double apiVersion = Double.parseDouble( APIVersion.substring(0, 3) );
                loginSettings.put("enableFingerprint", Boolean.toString(CommonAPI.enableFingerprint(apiVersion))); //gets SSO_ShowQCAuthLoginLinkMYQC from Globals
            }

            if ( !CommonAPI.isGateway() ) {
                //Get the Alias names for Accounts and Person Accounts
                loginSettings.put("aliases", CommonAPI.getAliases());
            }

        } catch (Exception ex) {
            Logger.logMessage("An error occurred in login settings", Logger.LEVEL.ERROR); //log details
            Logger.logException(ex);
            loginSettings.put("errorDetails", "An error has occurred. Please contact MMHayes support if the error continues.");
        }
        return loginSettings;
    }

    public static LoginSettingsModel retrieveLoginSettings() {
        LoginSettingsModel loginSettingsModel = new LoginSettingsModel();
        String errorDetails = "An error has occurred. Please contact MMHayes support if the error continues.";

        //do not run on Gateway
        if ( CommonAPI.isGateway() ) {
            return loginSettingsModel;
        }

        try {
            ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.common.retrieveLoginSettings", new Object[]{}, true);

            //validate the response
            if ( result == null || result.size() != 1 || result.get(0) == null || !result.get(0).containsKey("APIVERSION") ) {
                loginSettingsModel.setErrorDetails( errorDetails );
                return loginSettingsModel;
            }

            loginSettingsModel.setModelProperties( result.get(0) );
            return loginSettingsModel;
        } catch ( Exception ex ) {
            Logger.logMessage("An error occurred loading login settings", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            loginSettingsModel.setErrorDetails(errorDetails);
            return loginSettingsModel;
        }
    }

    public static LoginSettingsModel retrieveKOALoginSettings() {
        LoginSettingsModel loginSettingsModel = new LoginSettingsModel();
        String errorDetails = "An error has occurred. Please contact MMHayes support if the error continues.";

        //do not run on Gateway
        if ( CommonAPI.isGateway() ) {
            return loginSettingsModel;
        }

        try {
            ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.common.retrieveKOALoginSettings", new Object[]{}, true);

            //validate the response
            if ( result == null || result.size() != 1 || result.get(0) == null || !result.get(0).containsKey("APIVERSION") ) {
                loginSettingsModel.setErrorDetails( errorDetails );
                return loginSettingsModel;
            }

            loginSettingsModel.setKOAModelProperties( result.get(0) );
            return loginSettingsModel;
        } catch ( Exception ex ) {
            Logger.logMessage("An error occurred loading login settings", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            loginSettingsModel.setErrorDetails(errorDetails);
            return loginSettingsModel;
        }
    }

    public static boolean checkKOAActiveStatus(HttpServletRequest request){

        boolean isActive = false;

        try{
            Integer POSKioskTerminalID = CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);
            ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.common.checkKOATerminalActiveStatus", new Object[]{ POSKioskTerminalID }, true);

            //validate the response
            if ( result == null || result.size() != 1 || result.get(0) == null || !result.get(0).containsKey("ENABLED") ) {
                isActive = CommonAPI.convertModelDetailToBoolean(result.get(0).get("ENABLED"));
            }

            return isActive;
        }
        catch(Exception e){
            return isActive;
        }
    }

    //gets the X-MMHayes-Auth header or throws an InvalidAuthException if it doesn't exist
    public static String getMMHayesAuthHeader(HttpServletRequest request) throws Exception  {
        //get custom header X-MMHayes-Auth from request object
        String mmhayesAuthHeader = request.getHeader("X-MMHayes-Auth");

        //if the mmhayesAuthHeader is valid, verify the contents of it (Device Session Key)
        if (mmhayesAuthHeader == null) {
            Logger.logMessage("Could not find X-MMHayes-Auth in the request header CommonAuthResource.checkRequestHeaderForMMHayesAuth", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        return mmhayesAuthHeader;
    }

    //gets the X-MMHayes-User-Auth header or throws an InvalidAuthException if it doesn't exist
    public static String getMMHayesUserAuthHeader(HttpServletRequest request) throws Exception  {
        //get custom header X-MMHayes-Auth from request object
        String mmhayesUserAuthHeader = request.getHeader("X-MMHayes-User-Auth");

        //if the mmhayesAuthHeader is valid, verify the contents of it (Device Session Key)
        if (mmhayesUserAuthHeader == null) {
            Logger.logMessage("Could not find X-MMHayes-User-Auth in the request header CommonAuthResource.getMMHayesUserAuthHeader", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        return mmhayesUserAuthHeader;
    }

    public static String getAppConfigHeader(HttpServletRequest request) throws Exception {
        return getAppConfigHeader(request, true);
    }

    //gets the X-MMHayes-AppConfig header
    public static String getAppConfigHeader(HttpServletRequest request, boolean useDefault) throws Exception {
        //get custom header X-MMHayes-Auth from request object
        String mmhayesAppConfigHeader = request.getHeader("X-MMHayes-AppConfig");

        //if the mmhayesAppConfigHeader is empty, then default to myqc
        if (useDefault && mmhayesAppConfigHeader == null) {
            return "myqc";
        }

        return mmhayesAppConfigHeader;
    }

    //gets the X-MMHayes-Account header
    public static Integer getAccountIDFromAccountHeader(HttpServletRequest request) throws Exception  {
        Integer accountID = null; //accountID is the same as employeeID (which was here before)

        //get custom header X-MMHayes-Auth from request object
        String accountHeader = getAccountHeader(request);

        Validation requestValidation = new Validation();
        requestValidation.setDSKey(accountHeader);

        String DSKeyStatus = requestValidation.checkDSKeyStatus();

        if (DSKeyStatus.equals("valid")) {
            accountID = requestValidation.getInstanceUserID(); //accountID is the same as employeeID (which was here before)
        }

        return accountID;
    }

    public static String getAccountHeader(HttpServletRequest request) throws Exception {
        return request.getHeader("X-MMHayes-Account") == null || request.getHeader("X-MMHayes-Account").equals("") ? null : request.getHeader("X-MMHayes-Account");
    }

    //determines if the request is from a kiosk ordering terminal via the X-MMHayes-AppConfig header
    public static boolean isKOA(HttpServletRequest request) throws  Exception {
        return getAppConfigHeader(request).equals("koa");
    }

    public static HashMap determineAuthDetailsFromAuthHeader(HttpServletRequest request) throws Exception {
        //get custom header X-MMHayes-Auth from request object
        String mmhayesAuthHeader = getMMHayesAuthHeader(request);

        Validation requestValidation = new Validation();
        requestValidation.setDSKey(mmhayesAuthHeader);

        HashMap AuthHM = requestValidation.checkDSKeyStatusWithDetails();

        if ( AuthHM == null || ( AuthHM.containsKey("result") && AuthHM.get("result").toString().equals("invalid") ) ) {
            throw new InvalidAuthException();
        }

        if ( AuthHM.containsKey("result") && AuthHM.get("result").toString().equals("expired") ) {
            throw new SessionTimeoutException();
        }

        return AuthHM;
    }

    //TODO: (HIGH) Replace the usage of this method in My QC with determineAccountIDFromAuthHeader -jrmitaly 7/14/2016
    public static Integer checkRequestHeaderForMMHayesAuth(HttpServletRequest request) throws Exception {
        String appConfig = getAppConfigHeader( request );

        if ( appConfig.equals("koa") ) {
            return determineTerminalIDFromAuthHeader( request );
        }

        return determineAccountIDFromAuthHeader( request );
    }

    //gets an accountID from the X-MMHayes-Auth header
    public static Integer determineAccountIDFromAuthHeader(HttpServletRequest request) throws Exception {

        Integer accountID = null; //accountID is the same as employeeID (which was here before)

        //get custom header X-MMHayes-Auth from request object
        String mmhayesAuthHeader = getMMHayesAuthHeader(request);

        Validation requestValidation = new Validation();
        requestValidation.setDSKey(mmhayesAuthHeader);

        String DSKeyStatus = requestValidation.checkDSKeyStatus();

        if (DSKeyStatus.equals("valid")) {
            String appConfig = getAppConfigHeader(request, false);
            if (appConfig != null && appConfig.equals("myqc")) {
                autoExtendSession(mmhayesAuthHeader);
            }
            accountID = requestValidation.getInstanceUserID(); //accountID is the same as employeeID (which was here before)
            return accountID; //accountID is the same as employeeID (which was here before)
        } else if (DSKeyStatus.equals("expired")) {
            Logger.logMessage("DSKey (" + mmhayesAuthHeader + ") is expired in CommonAuthResource.checkRequestHeaderForMMHayesAuth", Logger.LEVEL.ERROR);
            throw new SessionTimeoutException();
        }

        //TODO: (HIGH) Log this somehow and ban IP Address ? Auth Attempt ? Hmm...
        Logger.logMessage("Could not verify DSKey (" + mmhayesAuthHeader + ") in CommonAuthResource.checkRequestHeaderForMMHayesAuth", Logger.LEVEL.ERROR);
        throw new InvalidAuthException();
    }

    //gets an terminalID from the X-MMHayes-Auth header
    public static Integer determineTerminalIDFromAuthHeader(HttpServletRequest request) throws Exception {

        Integer terminalD = null; //terminalD is the same as employeeID (which was here before)

        //get custom header X-MMHayes-Auth from request object
        String mmhayesAuthHeader = getMMHayesAuthHeader(request);

        Validation requestValidation = new Validation();
        requestValidation.setDSKey(mmhayesAuthHeader);

        String DSKeyStatus = requestValidation.checkDSKeyStatus();

        if (DSKeyStatus.equals("valid")) {
            terminalD = requestValidation.getInstanceUserID();
            return terminalD;
        } else if (DSKeyStatus.equals("expired")) {
            Logger.logMessage("DSKey (" + mmhayesAuthHeader + ") is expired in CommonAuthResource.checkRequestHeaderForMMHayesAuth", Logger.LEVEL.ERROR);
            throw new SessionTimeoutException();
        }

        //TODO: (HIGH) Log this somehow and ban IP Address ? Auth Attempt ? Hmm...
        Logger.logMessage("Could not verify DSKey (" + mmhayesAuthHeader + ") in CommonAuthResource.checkRequestHeaderForMMHayesAuth", Logger.LEVEL.ERROR);
        throw new InvalidAuthException();
    }
    //TODO: All of these "Determine{X}IDFromAuthHeader do the exact same thing.....
    public static Integer determineKMSStationIDFromAuthHeader(String mmhayesAuthHeader) throws Exception {

        Integer terminalD = null; //terminalD is the same as employeeID (which was here before)

        //get custom header X-MMHayes-Auth from request object
//        String mmhayesAuthHeader = getMMHayesAuthHeader(request);

        Validation requestValidation = new Validation();
        requestValidation.setDSKey(mmhayesAuthHeader);

        String DSKeyStatus = requestValidation.checkDSKeyStatus();

        if (DSKeyStatus.equals("valid")) {
            terminalD = requestValidation.getInstanceStationID();
            return terminalD;
        } else if (DSKeyStatus.equals("expired")) {
            Logger.logMessage("DSKey (" + mmhayesAuthHeader + ") is expired in CommonAuthResource.checkRequestHeaderForMMHayesAuth", Logger.LEVEL.ERROR);
            throw new SessionTimeoutException();
        }

        //TODO: (HIGH) Log this somehow and ban IP Address ? Auth Attempt ? Hmm...
        Logger.logMessage("Could not verify DSKey (" + mmhayesAuthHeader + ") in CommonAuthResource.checkRequestHeaderForMMHayesAuth", Logger.LEVEL.ERROR);
        throw new InvalidAuthException();
    }

    //gets an userID from the X-MMHayes-Auth header
    public static Integer determineUserIDFromAuthHeader(HttpServletRequest request) throws Exception {

        Integer userID = null;

        //get custom header X-MMHayes-Auth from request object
        String mmhayesUserAuthHeader = getMMHayesUserAuthHeader(request);

        Validation requestValidation = new Validation();
        requestValidation.setDSKey(mmhayesUserAuthHeader);

        String DSKeyStatus = requestValidation.checkDSKeyStatus();

        if (DSKeyStatus.equals("valid")) {
            userID = requestValidation.getInstanceUserID();
            return userID;
        } else if (DSKeyStatus.equals("expired")) {
            Logger.logMessage("DSKey (" + mmhayesUserAuthHeader + ") is expired in CommonAuthResource.determineUserIDFromAuthHeader", Logger.LEVEL.ERROR);
            throw new SessionTimeoutException();
        }

        Logger.logMessage("Could not verify DSKey (" + mmhayesUserAuthHeader + ") in CommonAuthResource.determineUserIDFromAuthHeader", Logger.LEVEL.ERROR);
        throw new InvalidAuthException();
    }
    //gets an TerminalId from the X-MMHayes-Auth header
    //Authenticates the request header
    public static LoginModel determineUserIdFromAuthHeader(HttpServletRequest request) throws Exception {

        HashMap DSKeyHM = new HashMap();
        Integer terminalId = null;
        String userId = null;
        LoginModel loginModel = new LoginModel();

        //Second, check the request object for the DeviceSessionKey information
        if (request.getHeader("X-MMHayes-User-Auth") != null && !request.getHeader("X-MMHayes-User-Auth").isEmpty()) {
            String mmhayesAuthHeader = request.getHeader("X-MMHayes-User-Auth");
            if (mmhayesAuthHeader == null || mmhayesAuthHeader.isEmpty()) {
                Logger.logMessage("ERROR: Could not find X-MMHayes-User-Auth in the request header CommonAuthResource.determineUserIdFromAuthHeader", Logger.LEVEL.ERROR);
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            }

            //this overloaded dm.serializeWithColNames utilizes parameterized queries
            ArrayList DSKeyList = null;
            try {

                DSKeyList = dm.serializeSqlWithColNames("data.validation.verifyDSKey",
                        new Object[]{
                                mmhayesAuthHeader,
                                Instant.now().toString()
                        },
                        false,
                        true
                );

            } catch (Exception ex) {
                throw new MissingDataException("Could not connect to the database.  Please try again.");
            }

            if (DSKeyList == null || DSKeyList.isEmpty()) { //verified DSKeyList
                Logger.logMessage("ERROR: Could not find valid DSKey in database CommonAuthResource.determineUserIdFromAuthHeader", Logger.LEVEL.ERROR);
                //throw new InvalidAuthException("Could not find valid DSKey in the database");
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            } else {
                //add the instanceUserID to this instance
                DSKeyHM = (HashMap) DSKeyList.get(0);
                //TODO - add the hashmap into the cache once we are caching the logins
                //loginCache.put(mmhayesAuthHeader, DSKeyHM);

                terminalId =  CommonAuthResource.checkRequestHeaderForMMHayesAuth(request) ;

                if (DSKeyHM.get("USERID") != null && !DSKeyHM.get("USERID").toString().isEmpty()) {
                    userId = CommonAPI.convertModelDetailToString(DSKeyHM.get("USERID"));
                } else {
                    Logger.logMessage("ERROR: Could not find User ID in the database CommonAuthResource.determineUserIdFromAuthHeader");
                    throw new InvalidAuthException();
                }
            }

        } else {
            Logger.logMessage("ERROR: No authentication was found in the request header CommonAuthResource.determineUserIdFromAuthHeader", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        try {

            if (userId != null && !userId.isEmpty()) {
                loginModel.setUserId(Integer.parseInt(userId));
            }

            if (terminalId != null ) {
                loginModel.setTerminalId(terminalId);
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Terminal ID or User ID is invalid. CommonAuthResource.determineUserIdFromAuthHeader", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        return loginModel;

    }

    //Verifies the X-MMHayes-Auth is in the request, and return Mac Address, if it's null, throw Invalid Auth Exception
    public static String getMacAddressFromAuthHeader(HttpServletRequest request) throws Exception {

        //get custom header X-MMHayes-Auth from request object
        String macAddress = request.getHeader("X-MMHayes-Auth");
        return macAddress;
    }

    //validate the MAC address in the request header
    public static void validateMacAddressFromAuthHeader(HttpServletRequest request) throws Exception {

        //get custom header X-MMHayes-Auth from request object
        String macAddress = request.getHeader("X-MMHayes-Auth");
        checkForNullAndExistingMacAddress(macAddress);
    }

    //validate the MAC address in the MAC Address String that is passed in
    private static void checkForNullAndExistingMacAddress(String macAddress) throws Exception {
        checkForNullMacAddress(macAddress);
        checkForExistingMacAddress(macAddress);
    }

    //validate the MAC address in the MAC Address String that is passed in
    private static void checkForNullMacAddress(String macAddress) throws Exception {

        //if the mmhayesAuthHeader is valid, verify the contents of it
        if (macAddress == null) {
            Logger.logMessage("Could not find Mac Address in the request header CommonAuthResource.validateMacAddress", Logger.LEVEL.ERROR);
            throw new InvalidAuthException("MAC Address cannot be found");
        }
    }

	//Authenticates the request header
    public static boolean checkForValidSessionFromAuthHeader(HttpServletRequest request) throws Exception
    {
        boolean validSessionExists = false;
        HttpSession session = request.getSession();

        if (request.getHeader("X-MMHayes-JAuth") != null && !request.getHeader("X-MMHayes-JAuth").isEmpty())
        {
            String sessionId = request.getHeader("X-MMHayes-JAuth");

            if (sessionId == null || sessionId.isEmpty())
            {
                Logger.logMessage("ERROR: Could not find X-MMHayes-JAuth in the request header CommonAuthResource.checkForValidSessionFromAuthHeader", Logger.LEVEL.ERROR);
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            }

            HttpSession collectorSession = HttpSessionCollector.find(sessionId);

            if (collectorSession == null || collectorSession.getAttribute("QC_TERMINALID") == null || collectorSession.getAttribute("QC_TERMINALID").toString().isEmpty())
            {
                Logger.logMessage("ERROR: Could not find X-MMHayes-JAuth in the request header CommonAuthResource.checkForValidSessionFromAuthHeader", Logger.LEVEL.ERROR);
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            }

            validSessionExists = true;
        }
        //Second, check the request object for the DeviceSessionKey information
        else if (request.getHeader("X-MMHayes-Auth") != null && !request.getHeader("X-MMHayes-Auth").isEmpty())
        {
            String mmhayesAuthHeader = request.getHeader("X-MMHayes-Auth");
            if (mmhayesAuthHeader == null || mmhayesAuthHeader.isEmpty())
            {
                Logger.logMessage("ERROR: Could not find X-MMHayes-Auth in the request header CommonAuthResource.checkForValidSessionFromAuthHeader", Logger.LEVEL.ERROR);
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            }

            //this overloaded dm.serializeWithColNames utilizes parameterized queries
            ArrayList DSKeyList = null;
            try {

                DSKeyList = dm.serializeSqlWithColNames("data.validation.verifyDSKey",
                        new Object[]{
                                mmhayesAuthHeader,
                                Instant.now().toString()
                        },
                        false,
                        true
                );

            } catch (Exception ex) {
                throw new MissingDataException("Could not connect to the database.  Please try again.");
            }

            if (DSKeyList == null || DSKeyList.isEmpty()) //verified DSKeyList
            {
                Logger.logMessage("ERROR: Could not find valid DSKey in database CommonAuthResource.checkForValidSessionFromAuthHeader", Logger.LEVEL.ERROR);
                //throw new InvalidAuthException("Could not find valid DSKey in the database");
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            }

            validSessionExists = true;
        }
        else
        {
            Logger.logMessage("ERROR: No authentication was found in the request header CommonAuthResource.checkForValidSessionFromAuthHeader", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        return validSessionExists;

    }

    //validate the MAC address in the MAC Address String that is passed in
    private static void checkForExistingMacAddress(String macAddress) throws Exception {
        //Check database for MAC address
        Integer numberOfPrintControllersWithMacAddress = Integer.parseInt(dm.parameterizedExecuteScalar("data.labelPrinting.checkForMacAddressOnPrintController", new Object[]{macAddress}).toString());

        //if the MAC Address is not found in the database, throw an invalid auth error
        if (numberOfPrintControllersWithMacAddress < 1) {
            Logger.logMessage("Could not find Mac Address in the database CommonAuthResource.checkForExistingMacAddress", Logger.LEVEL.ERROR);
            throw new InvalidAuthException("MAC Address cannot be found");
        }
    }

    //gets an TerminalId from the X-MMHayes-Auth header
    //Authenticates the request header
    public static LoginModel determineTerminalIdFromAuthHeader(HttpServletRequest request, Boolean requireUserId) throws Exception {

        String employeeId = null;
        HashMap DSKeyHM = new HashMap();
        String terminalId = null;
        String userId = null;
        String dsKey = null;
        LoginModel loginModel = new LoginModel();
        //HttpSession session = request.getSession();

        if (request.getHeader("X-MMHayes-JAuth") != null && !request.getHeader("X-MMHayes-JAuth").isEmpty()) {
            String sessionId = request.getHeader("X-MMHayes-JAuth");

            if (sessionId == null || sessionId.isEmpty()) {
                Logger.logMessage("ERROR: Could not find X-MMHayes-JAuth in the request header CommonAuthResource.determineTerminalIdFromAuthHeader", Logger.LEVEL.ERROR);
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            }

            HttpSession collectorSession = HttpSessionCollector.find(sessionId);

            if (collectorSession == null || collectorSession.getAttribute("QC_TERMINALID") == null || collectorSession.getAttribute("QC_TERMINALID").toString().isEmpty()) {
                Logger.logMessage("ERROR: Could not find valid Terminal ID in the request header CommonAuthResource.determineTerminalIdFromAuthHeader", Logger.LEVEL.ERROR);
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            }

            terminalId = collectorSession.getAttribute("QC_TERMINALID").toString();
            userId = collectorSession.getAttribute("QC_USERID").toString();

        }
        //Second, check the request object for the DeviceSessionKey information
        else if (request.getHeader("X-MMHayes-Auth") != null && !request.getHeader("X-MMHayes-Auth").isEmpty()) {
            String mmhayesAuthHeader = request.getHeader("X-MMHayes-Auth");
            if (mmhayesAuthHeader == null || mmhayesAuthHeader.isEmpty()) {
                Logger.logMessage("ERROR: Could not find X-MMHayes-Auth in the request header CommonAuthResource.determineTerminalIdFromAuthHeader", Logger.LEVEL.ERROR);
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            }

            //this overloaded dm.serializeWithColNames utilizes parameterized queries
            ArrayList DSKeyList = null;
            try {

                DSKeyList = dm.serializeSqlWithColNames("data.validation.verifyDSKey",
                        new Object[]{
                                mmhayesAuthHeader,
                                Instant.now().toString()
                        },
                        false,
                        true
                );

            } catch (Exception ex) {
                throw new MissingDataException("Could not connect to the database.  Please try again.");
            }

            if (DSKeyList == null || DSKeyList.isEmpty()) { //verified DSKeyList
                Logger.logMessage("ERROR: Could not find valid DSKey in database CommonAuthResource.determineTerminalIdFromAuthHeader", Logger.LEVEL.ERROR);
                //throw new InvalidAuthException("Could not find valid DSKey in the database");
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            } else {
                //add the instanceUserID to this instance
                DSKeyHM = (HashMap) DSKeyList.get(0);
                //TODO - add the hashmap into the cache once we are caching the logins
                //loginCache.put(mmhayesAuthHeader, DSKeyHM);

                if (DSKeyHM.get("DEVICESESSIONKEY") != null && !DSKeyHM.get("DEVICESESSIONKEY").toString().isEmpty()) {
                    dsKey = CommonAPI.convertModelDetailToString(DSKeyHM.get("DEVICESESSIONKEY"));
                } else {
                    dsKey = null;
                }

                if (DSKeyHM.get("TERMINALID") != null && !DSKeyHM.get("TERMINALID").toString().isEmpty()) {
                    terminalId = CommonAPI.convertModelDetailToString(DSKeyHM.get("TERMINALID"));
                } else {
                    terminalId = null;
                }

                if (DSKeyHM.get("USERID") != null && !DSKeyHM.get("USERID").toString().isEmpty()) {
                    userId = CommonAPI.convertModelDetailToString(DSKeyHM.get("USERID"));
                } else {
                    if (requireUserId) {
                        Logger.logMessage("ERROR: Could not find User ID in the database CommonAuthResource.determineTerminalIdFromAuthHeader");
                        throw new InvalidAuthException();
                    }
                }

                //Only validate the Employee ID if it is on the DSKey
                if (DSKeyHM.get("EMPLOYEEID") != null && !DSKeyHM.get("EMPLOYEEID").toString().isEmpty()) {
                    employeeId = CommonAPI.convertModelDetailToString(DSKeyHM.get("EMPLOYEEID"));
                }
            }


        } else {
            Logger.logMessage("ERROR: No authentication was found in the request header CommonAuthResource.determineTerminalIdFromAuthHeader", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        try {

            if (userId != null && !userId.isEmpty()) {
                loginModel.setUserId(Integer.parseInt(userId));
            }

            if (terminalId != null && !terminalId.isEmpty()) {
                loginModel.setTerminalId(Integer.parseInt(terminalId));
            }

            if (employeeId != null && !employeeId.isEmpty()) {
                loginModel.setEmployeeId(Integer.parseInt(employeeId));
            }

            if (dsKey != null && !dsKey.isEmpty()){
                loginModel.setApiKey(dsKey);
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Terminal ID or User ID is invalid. CommonAuthResource.determineTerminalIdFromAuthHeader", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        return loginModel;

    }

    //gets an TerminalId from the X-MMHayes-Auth header
    //Authenticates the request header
    public static LoginModel determineTerminalIdFromAuthHeader(HttpServletRequest request) throws Exception {
        return determineTerminalIdFromAuthHeader(request, true); //pass in the request, and requireUserId = true
    }

    //gets an OtmId from the X-MMHayes-Auth header
    //Authenticates the request header
    public static OtmLoginModel determineOtmIdFromAuthHeader(HttpServletRequest request) throws Exception {

        String employeeId = null;
        HashMap DSKeyHM = new HashMap();
        String otmId = null;

        OtmLoginModel otmLoginModel = new OtmLoginModel();
        //HttpSession session = request.getSession();

        if (request.getHeader("X-MMHayes-JAuth") != null && !request.getHeader("X-MMHayes-JAuth").isEmpty()) {
            String sessionId = request.getHeader("X-MMHayes-JAuth");

            if (sessionId == null || sessionId.isEmpty()) {
                Logger.logMessage("ERROR: Could not find X-MMHayes-Auth in the request header CommonAuthResource.determineTerminalIdFromAuthHeader", Logger.LEVEL.ERROR);
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            }

            HttpSession collectorSession = HttpSessionCollector.find(sessionId);
            if (collectorSession != null) {
                if (collectorSession.getAttribute("QC_OTMID") != null && !collectorSession.getAttribute("QC_OTMID").toString().isEmpty()) {
                    otmId = collectorSession.getAttribute("QC_OTMID").toString(); //passed in by .Net Container
                } else if (collectorSession.getAttribute("OTM_ID") != null && !collectorSession.getAttribute("OTM_ID").toString().isEmpty()) {
                    otmId = collectorSession.getAttribute("OTM_ID").toString(); //passed in by Sync Service
                }
            }

            if (otmId == null) {
                Logger.logMessage("ERROR: Could not find X-MMHayes-Auth in the request header CommonAuthResource.determineTerminalIdFromAuthHeader", Logger.LEVEL.ERROR);
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            }

        } else if (request.getHeader("X-MMHayes-Auth") != null && !request.getHeader("X-MMHayes-Auth").isEmpty()) {
            String mmhayesAuthHeader = request.getHeader("X-MMHayes-Auth");
            if (mmhayesAuthHeader == null || mmhayesAuthHeader.isEmpty()) {
                Logger.logMessage("ERROR: Could not find X-MMHayes-Auth in the request header CommonAuthResource.determineOtmIdFromAuthHeader", Logger.LEVEL.ERROR);
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            }

            //this overloaded dm.serializeWithColNames utilizes parameterized queries
            ArrayList DSKeyList = null;
            try {

                DSKeyList = dm.serializeSqlWithColNames("data.validation.verifyDSKey",
                        new Object[]{
                                mmhayesAuthHeader,
                                Instant.now().toString()
                        },
                        false,
                        true
                );

            } catch (Exception ex) {
                throw new MissingDataException("Could not connect to the database.  Please try again.");
            }

            if (DSKeyList == null || DSKeyList.isEmpty()) { //verified DSKeyList
                Logger.logMessage("ERROR: Could not find valid DSKey in database CommonAuthResource.determineOtmIdFromAuthHeader", Logger.LEVEL.ERROR);
                //throw new InvalidAuthException("Could not find valid DSKey in the database");
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");
            } else {
                //add the instanceUserID to this instance
                DSKeyHM = (HashMap) DSKeyList.get(0);
                //TODO - add the hashmap into the cache once we are caching the logins
                //loginCache.put(mmhayesAuthHeader, DSKeyHM);

                if (DSKeyHM.get("OTMID") != null && !DSKeyHM.get("OTMID").toString().isEmpty()) {
                    otmId = CommonAPI.convertModelDetailToString(DSKeyHM.get("OTMID"));
                } else {
                    otmId = null;
                }
            }
        } else {
            Logger.logMessage("ERROR: No authentication was found in the request header CommonAuthResource.determineOtmIdFromAuthHeader", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        try {

            otmLoginModel.getOtm().setId(Integer.parseInt(otmId));

        } catch (Exception ex) {
            Logger.logMessage("ERROR: OTM ID is invalid. CommonAuthResource.determineOtmIdFromAuthHeader", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        return otmLoginModel;

    }

    //Check headers for both QCPOS, POS API, Sync Service, and OTM requests
    //this allows multiple clients to authenticate and hit the same endpoint
    public static LoginModel determineTerminalFromAuthHeader(HttpServletRequest request, boolean requireUserId) throws Exception {
        String employeeId = null;
        HashMap DSKeyHM = new HashMap();
        String terminalId = null;
        String userId = null;
        String otmId = null;
        String errorMessage = "";

        boolean headerFound = false;
        boolean invalidSession = false;
        boolean throwMissingDataException = false;
        LoginModel loginModel = new LoginModel();

        //Check JSession
        if (request.getHeader("X-MMHayes-JAuth") != null && !request.getHeader("X-MMHayes-JAuth").isEmpty()) {
            headerFound = true;
            String jSessionId = request.getHeader("X-MMHayes-JAuth");

            if (jSessionId != null && !jSessionId.isEmpty()) {
                HttpSession collectorSession = HttpSessionCollector.find(jSessionId);

                if (collectorSession != null) {

                    boolean foundTerminalId = collectorSession.getAttribute("QC_TERMINALID") != null && !collectorSession.getAttribute("QC_TERMINALID").toString().isEmpty();
                    boolean foundQcOtmId = collectorSession.getAttribute("QC_OTMID") != null && !collectorSession.getAttribute("QC_OTMID").toString().isEmpty();
                    boolean foundOtmId = collectorSession.getAttribute("OTM_ID") != null && !collectorSession.getAttribute("OTM_ID").toString().isEmpty();

                    if (foundTerminalId || //QCPOS, POS API
                            foundQcOtmId || //.Net Container
                            foundOtmId) { //Sync Service uses OTM_ID

                        if (collectorSession.getAttribute("QC_TERMINALID") != null && !collectorSession.getAttribute("QC_TERMINALID").toString().isEmpty()) {
                            terminalId = collectorSession.getAttribute("QC_TERMINALID").toString();
                            if (collectorSession.getAttribute("QC_USERID") != null && !collectorSession.getAttribute("QC_USERID").toString().isEmpty()) {
                                userId = collectorSession.getAttribute("QC_USERID").toString();
                            }
                        } else if (collectorSession.getAttribute("QC_OTMID") != null && !collectorSession.getAttribute("QC_OTMID").toString().isEmpty()) {
                            otmId = collectorSession.getAttribute("QC_OTMID").toString(); //passed in by .Net Container
                        } else if (collectorSession.getAttribute("OTM_ID") != null && !collectorSession.getAttribute("OTM_ID").toString().isEmpty()) {
                            otmId = collectorSession.getAttribute("OTM_ID").toString(); //passed in by Sync Service
                        }
                    } else {
                        errorMessage = "ERROR: Could not find valid Terminal or OTM ID attribute in the request header CommonAuthResource.determineTerminalIdFromAuthHeader";
                        invalidSession = true;
                    }
                } else {
                    errorMessage = "ERROR: Could not find valid Terminal or OTM ID attribute in the request header CommonAuthResource.determineTerminalIdFromAuthHeader";
                    invalidSession = true;
                }
            } else {
                errorMessage = "Could not find X-MMHayes-JAuth in the request header CommonAuthResource.determineTerminalIdFromAuthHeader";
                invalidSession = true;
            }
        } //end of checking for JSession

        //Check DSKey
        if (!headerFound && request.getHeader("X-MMHayes-Auth") != null && !request.getHeader("X-MMHayes-Auth").isEmpty()) {
            headerFound = true;
            String dsKeyValue = request.getHeader("X-MMHayes-Auth");

            if (dsKeyValue != null && !dsKeyValue.isEmpty()) {
                //this overloaded dm.serializeWithColNames utilizes parameterized queries
                ArrayList DSKeyList = null;
                try {

                    DSKeyList = dm.serializeSqlWithColNames("data.validation.verifyDSKey",
                            new Object[]{
                                    dsKeyValue,
                                    Instant.now().toString()
                            },
                            false,
                            true
                    );

                } catch (Exception ex) {
                    //throw new MissingDataException("Could not connect to the database.  Please try again.");
                    errorMessage = "Could not connect to the database.  Please try again.";
                    throwMissingDataException = true;
                }

                if (DSKeyList == null || DSKeyList.isEmpty()) { //verified DSKeyList
                    //Logger.logMessage("ERROR: Could not find valid DSKey in database CommonAuthResource.determineTerminalIdFromAuthHeader", Logger.LEVEL.ERROR);
                    //throw new InvalidAuthException("Could not find valid DSKey in the database");
                    errorMessage = "ERROR: Could not find valid DSKey in database CommonAuthResource.determineTerminalIdFromAuthHeader";
                    invalidSession = true;
                } else {
                    DSKeyHM = (HashMap) DSKeyList.get(0);

                    if (DSKeyHM.get("TERMINALID") != null && !DSKeyHM.get("TERMINALID").toString().isEmpty()) {
                        terminalId = CommonAPI.convertModelDetailToString(DSKeyHM.get("TERMINALID"));
                    }

                    if (DSKeyHM.get("OTMID") != null && !DSKeyHM.get("OTMID").toString().isEmpty()) {
                        otmId = CommonAPI.convertModelDetailToString(DSKeyHM.get("OTMID"));
                    }

                    if (DSKeyHM.get("USERID") != null && !DSKeyHM.get("USERID").toString().isEmpty()) {
                        userId = CommonAPI.convertModelDetailToString(DSKeyHM.get("USERID"));
                    } else {
                        //Only check User ID for QCPOS terminal
                        if (requireUserId) {
                            if (terminalId != null && !terminalId.isEmpty()) {
                                //Logger.logMessage("ERROR: Could not find User ID in the database CommonAuthResource.determineTerminalIdFromAuthHeader");
                                //throw new InvalidAuthException();
                                errorMessage = "ERROR: Could not find User ID in the database CommonAuthResource.determineClientIdFromAuthHeader";
                                invalidSession = true;
                            }
                        }
                    }

                    //Only validate the Employee ID if it is on the DSKey
                    if (DSKeyHM.get("EMPLOYEEID") != null && !DSKeyHM.get("EMPLOYEEID").toString().isEmpty()) {
                        employeeId = CommonAPI.convertModelDetailToString(DSKeyHM.get("EMPLOYEEID"));
                    }
                }
            } else {
                /*Logger.logMessage("ERROR: Could not find X-MMHayes-Auth in the request header CommonAuthResource.determineTerminalIdFromAuthHeader", Logger.LEVEL.ERROR);
                throw new InvalidAuthException("Missing or invalid authentication.  Please try again.");*/
                errorMessage = "Could not find X-MMHayes-Auth in the request header CommonAuthResource.determineTerminalIdFromAuthHeader";
                invalidSession = true;
            }
        }

        if (!headerFound){
            Logger.logMessage("ERROR: No authentication was found in the request header CommonAuthResource.determineTerminalIdFromAuthHeader", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        if (invalidSession) {
            Logger.logMessage(errorMessage, Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        if (throwMissingDataException) {
            Logger.logMessage(errorMessage, Logger.LEVEL.ERROR);
            throw new MissingDataException(errorMessage);
        }

        try {

            if (userId != null && !userId.isEmpty()) {
                loginModel.setUserId(Integer.parseInt(userId));
            }

            if (terminalId != null && !terminalId.isEmpty()) {
                loginModel.setTerminalId(Integer.parseInt(terminalId));
            }

            if (employeeId != null && !employeeId.isEmpty()) {
                loginModel.setEmployeeId(Integer.parseInt(employeeId));
            }

            if (otmId != null && !otmId.isEmpty()) {
                loginModel.setOtmId(Integer.parseInt(otmId));
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Terminal ID or User ID is invalid. CommonAuthResource.determineTerminalIdFromAuthHeader", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        return loginModel;
    }

    // Check the static Hashmap of hashmaps for saved login information
    // return a HashMap
    private static HashMap checkApiLoginCache(String mmhayesAuthHeader) throws Exception {
        HashMap hashMap = loginCache.get(mmhayesAuthHeader);
        return hashMap;
    }

    public static boolean checkSessionValidForExtenstion(String mmhayesAuthHeader) throws Exception {
        //get timer seconds for how long the session can be expired and the user can still extend it
        Integer extendTimerSeconds = CommonAPI.convertModelDetailToInteger(MMHProperties.getAppSetting("site.security.session.extendtimersecs"), 120, true);

        if (extendTimerSeconds == null) {
            Logger.logMessage("Could not find session.extendtimersecs property in CommonAuthResource.checkSessionValidForExtension", Logger.LEVEL.ERROR);
        }

        //create time object to check if the given DSKey would be valid at this time
        Instant extendTimer = Instant.now().minusSeconds(extendTimerSeconds);

        //use validation object
        Validation val = new Validation();
        val.setDSKey(mmhayesAuthHeader);
        return val.verifyDSKEY(extendTimer);
    }

    //extends the given session by the session timeout minutes
    public static HashMap extendSession(HttpServletRequest request) throws Exception {
        HashMap<String, String> responseHM = new HashMap<>();

        //get custom header X-MMHayes-Auth from request object
        String mmhayesAuthHeader = request.getHeader("X-MMHayes-Auth");

        //if the mmhayesAuthHeader is valid, verify the contents of it (Device Session Key)
        if (mmhayesAuthHeader == null) {
            Logger.logMessage("Could not find X-MMHayes-Auth in the request header CommonAuthResource.checkRequestHeaderForMMHayesAuth", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        if (!CommonAuthResource.checkSessionValidForExtenstion(mmhayesAuthHeader)) {
            Logger.logMessage("Session not valid for extension", Logger.LEVEL.DEBUG);
            responseHM.put("status", "failed");
            responseHM.put("details", "Session not valid for extension any more.");
            return responseHM;
        }

        //get timer seconds for how long the session can be expired and the user can still extend it
        Integer extendMinutes = CommonAPI.getSessionTimeoutMinutes();
        //ensure a valid amount to extend
        if (extendMinutes != null) {
            //expire the extendMinutes (DSKey) in the DB
            Integer updateResult = dm.parameterizedExecuteNonQuery("data.validation.extendSession",
                    new Object[]{
                            mmhayesAuthHeader,
                            extendMinutes
                    }
            );
            //record success in response
            if (updateResult == 1) {
                responseHM.put("status", "success");
                responseHM.put("details", "Successfully extended session.");
            } else {
                responseHM.put("status", "failed");
                responseHM.put("details", "Failed to extended session.");
            }
        } else {
            Logger.logMessage("Could not find sessiontimeoutminutes in CommonAuthResource.extendSession", Logger.LEVEL.ERROR);
            responseHM.put("status", "failed");
            responseHM.put("details", "Invalid Access.");
        }

        return responseHM;
    }

    // Called after verifying the given session to automatically extend it
    private static void autoExtendSession(String mmhayesAuthHeader) {
        Integer timeoutMinutes = CommonAPI.getSessionTimeoutMinutes();
        Integer extendedTimeoutMinutes = CommonAPI.convertModelDetailToInteger(MMHProperties.getAppSetting("site.security.extendedsessiontimeoutminutes"),
                129600, true);
        if (timeoutMinutes != null && extendedTimeoutMinutes != null) {
            Instant expiration = Instant.now().plus(Duration.ofMinutes(timeoutMinutes));
            Instant extendedExpiration = Instant.now().plus(Duration.ofMinutes(extendedTimeoutMinutes));
            // Use the current expiration to determine whether to extend by the "keep me logged in" amount or standard amount
            dm.parameterizedExecuteNonQuery("data.validation.autoExtendSession",
                    new Object[]{
                            mmhayesAuthHeader,
                            expiration.toString(),
                            extendedExpiration.toString()
                    }
            );
        } else {
            Logger.logMessage("Could not get timeouts in CommonAuthResource.autoExtendSession", Logger.LEVEL.ERROR);
        }
    }

    //checks the session for the X-MMHayes-Auth Header and confirms if it's valid
    public static HashMap expireDSKeyForMMHayesAuth(HttpServletRequest request) {
        HashMap<String, String> responseHM = new HashMap<>();
        try {

            //get custom header X-MMHayes-Auth from request object
            String mmhayesAuthHeader = request.getHeader("X-MMHayes-Auth");

            //if the mmhayesAuthHeader is valid, verify the contents of it (Device Session Key)
            if (mmhayesAuthHeader != null) {

                //expire the mmhayesAuthHeader (DSKey) in the DB
                Integer updateResult = dm.parameterizedExecuteNonQuery("data.validation.expireDSKey",
                        new Object[]{
                                mmhayesAuthHeader
                        }
                );
                //record success in response
                if (updateResult == 1) {
                    responseHM.put("status", "success");
                    responseHM.put("details", "Successfully expired DSKey.");
                } else {
                    responseHM.put("status", "failed");
                    responseHM.put("details", "Failed to expire DSDKey.");
                }
            } else {
                Logger.logMessage("Could not find X-MMHayes-Auth in the request header CommonAuthResource.expireDSKeyForMMHayesAuth", Logger.LEVEL.ERROR);
                responseHM.put("status", "failed");
                responseHM.put("details", "Invalid Access.");
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            responseHM.put("status", "error");
            responseHM.put("details", commonMMHFunctions.convertExceptionToString(ex));
        }
        return responseHM;
    }

    //checks that macAddress in the Validation object is set on at LEAST one Terminal
    public static Boolean checkForMacAddressOnAnyTerminal(Validation requestValidation) {
        Boolean validMacAddress = false;
        try {
            if (requestValidation.getMacAddress() != null) {
                Object checkMacAddressObj = dm.getSingleField("data.common.checkForMacAddressOnAnyTerminal",
                        new Object[]{
                                requestValidation.getMacAddress()
                        },
                        false,
                        true
                );
                if (checkMacAddressObj != null && !checkMacAddressObj.toString().isEmpty()) {
                    validMacAddress = true;
                } else {
                    Logger.logMessage("Mac Address is NOT VALID in CommonAuthResource.checkForMacAddressOnAnyTerminal()", Logger.LEVEL.ERROR);
                }
            } else {
                Logger.logMessage("Mac Address is NULL in CommonAuthResource.checkForMacAddressOnAnyTerminal()", Logger.LEVEL.ERROR);
            }
        } catch (Exception ex) {
            requestValidation.handleGenericException(ex, "CommonAuthResource.checkForMacAddressOnAnyTerminal");
        }
        return validMacAddress;
    }

    //turns off the flag for X-MMHayes-Auth Header ReAuthBeforePurchase
    public static HashMap unsetDSKeyReAuthFlag(HttpServletRequest request) {
        HashMap<String, String> responseHM = new HashMap<>();

        //get custom header X-MMHayes-Auth from request object
        String mmhayesAuthHeader = request.getHeader("X-MMHayes-Auth");

        //if the mmhayesAuthHeader is valid, verify the contents of it (Device Session Key)
        if (mmhayesAuthHeader != null) {

            //expire the mmhayesAuthHeader (DSKey) in the DB
            Integer updateResult = dm.parameterizedExecuteNonQuery("data.validation.unsetReAuthFlag",
                    new Object[]{
                            mmhayesAuthHeader
                    }
            );
            //record success in response
            if (updateResult == 1) {
                responseHM.put("status", "success");
                responseHM.put("details", "Successfully turned off ReAuth flag.");
            } else {
                responseHM.put("status", "failed");
                responseHM.put("details", "Failed to turn off ReAuth flag.");
            }
        } else {
            Logger.logMessage("Could not find X-MMHayes-Auth in the request header CommonAuthResource.unsetDSKeyReAuthFlag", Logger.LEVEL.ERROR);
            responseHM.put("status", "failed");
            responseHM.put("details", "Invalid Access.");
        }
        return responseHM;
    }

	// Checks if the request's mac address matches the mac address for the given terminal
    public static Boolean checkIfMacAddressMatchesTerminal(String terminalIdStr, HttpServletRequest request) throws Exception
    {
        boolean macAddressMatchesTerminal = false;
        HttpSession session = request.getSession();

        if (request.getHeader("X-MMHayes-JAuth") != null && !request.getHeader("X-MMHayes-JAuth").isEmpty())
        {
            String encodedMacAddress = request.getHeader("X-MMHayes-JAuth");
            String macAddress = StringFunctions.decodePassword(encodedMacAddress);

            // Get mac address of terminal
            Object terminalMacObj = dm.parameterizedExecuteScalar("data.common.getTerminalMacAddress", new Object[]{new Integer(terminalIdStr)});

            if (terminalMacObj != null && terminalMacObj.toString().equals(macAddress))
            {
                macAddressMatchesTerminal = true;
            }
            else
            {
                throw new InvalidAuthException("Missing or invalid authentication. Please try again.");
            }
        }
        else
        {
            Logger.logMessage("ERROR: No authentication was found in the request header CommonAuthResource.checkIfMacAddressMatchesTerminal", Logger.LEVEL.ERROR);
            throw new InvalidAuthException();
        }

        return macAddressMatchesTerminal;
    }

    public static HashMap saveLocalEmployeeDSKey(HttpServletRequest request, HashMap data) throws Exception {
        //auth check
        CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

        //delete all existing user ds keys
        deleteLocalEmployeeDSKeys();

        //save user's dskey
        String dskey = CommonAPI.convertModelDetailToString( data.get("DSKEY") );
        Integer employeeId = CommonAPI.convertModelDetailToInteger(data.get("EMPLOYEEID"));
        Integer sessionDuration = CommonAPI.convertModelDetailToInteger(MMHProperties.getAppSetting("site.security.sessiontimeoutminutes"), 30, true);

        int result = dm.parameterizedExecuteNonQuery("data.kiosk.saveLocalEmployeeDSKey",
            new Object[]{
                dskey,
                employeeId,
                sessionDuration
            }
        );

        HashMap resultHM = new HashMap();

        if ( result == -1 ) {
            Logger.logMessage("Employee dskey was not able to be saved to local database", Logger.LEVEL.ERROR);
            resultHM.put("result", false);
        } else {
            resultHM.put("result", true);
        }

        return resultHM;
    }

    public static HashMap deleteLocalEmployeeDSKeys(HttpServletRequest request) throws Exception {
        //auth check
        CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

        int deleteResult = deleteLocalEmployeeDSKeys();

        HashMap result = new HashMap();

        if ( deleteResult == -1 ) {
            Logger.logMessage("No employee dskeys were able to be deleted from the local database", Logger.LEVEL.WARNING);
            result.put("result", false);
            result.put("numDeleted", 0);
        } else {
            result.put("result", true);
            result.put("numDeleted", deleteResult);
        }

        return result;
    }

    private static int deleteLocalEmployeeDSKeys() {
        int result = dm.parameterizedExecuteNonQuery("data.kiosk.deleteLocalEmployeeDSKeys",
                new Object[]{}
        );

        return result;
    }

    private static int deleteLocalTerminalDSKeys() {
        int result = dm.parameterizedExecuteNonQuery("data.kiosk.deleteLocalTerminalDSKeys",
                new Object[]{}
        );

        return result;
    }

    public static HashMap saveLocalTerminalDSKey(HashMap data) throws Exception {
        deleteLocalTerminalDSKeys();

        //save terminal's dskey
        String dskey = CommonAPI.convertModelDetailToString( data.get("DSKEY") );
        Integer terminalId = CommonAPI.convertModelDetailToInteger(data.get("TERMINALID"));

        int result = dm.parameterizedExecuteNonQuery("data.kiosk.saveLocalTerminalDSKey",
                new Object[]{
                        dskey,
                        terminalId
                }
        );

        HashMap resultHM = new HashMap();

        if ( result == -1 ) {
            Logger.logMessage("Terminal dskey was not able to be saved to local database", Logger.LEVEL.ERROR);
            resultHM.put("result", false);
        } else {
            resultHM.put("result", true);
        }

        return resultHM;
    }

    public static HashMap saveLocalUserDSKey(HttpServletRequest request, HashMap data) throws Exception {
        //auth check
        CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

        //delete all existing user ds keys
        deleteLocalUserDSKeys();

        //save user's dskey
        String dskey = CommonAPI.convertModelDetailToString( data.get("DSKEY") );
        Integer userID = CommonAPI.convertModelDetailToInteger(data.get("USERID"));

        int result = dm.parameterizedExecuteNonQuery("data.kiosk.saveLocalUserDSKey",
            new Object[]{
                dskey,
                userID
            }
        );

        HashMap resultHM = new HashMap();

        if ( result == -1 ) {
            Logger.logMessage("User dskey was not able to be saved to local database", Logger.LEVEL.ERROR);
            resultHM.put("result", false);
        } else {
            resultHM.put("result", true);
        }

        return resultHM;
    }

    public static HashMap deleteLocalUserDSKeys(HttpServletRequest request) throws Exception {
        //auth check
        CommonAuthResource.checkRequestHeaderForMMHayesAuth(request);

        int deleteResult = deleteLocalUserDSKeys();

        HashMap result = new HashMap();

        if ( deleteResult == -1 ) {
            Logger.logMessage("No employee dskeys were able to be deleted from the local database", Logger.LEVEL.WARNING);
            result.put("result", false);
            result.put("numDeleted", 0);
        } else {
            result.put("result", true);
            result.put("numDeleted", deleteResult);
        }

        return result;
    }

    private static int deleteLocalUserDSKeys() {
        int result = dm.parameterizedExecuteNonQuery("data.kiosk.deleteLocalUserDSKeys",
                new Object[]{}
        );

        return result;
    }

    public static boolean isGuestAccount(int employeeID) {
        ArrayList<HashMap> fundingList = dm.parameterizedExecuteQuery("data.accounts.getEmployeeAccountTypeByID", new Object[] { employeeID }, true);
        if(fundingList != null && fundingList.size() != 0) {
            return CommonAPI.convertModelDetailToInteger(fundingList.get(0).get("ACCOUNTTYPEID")) == 5;
        }
        return false;
    }
}
