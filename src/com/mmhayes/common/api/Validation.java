package com.mmhayes.common.api;

//mmhayes dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.account.models.AccountPersonModel;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.login.LoginModel;
import com.mmhayes.common.login.OtmLoginModel;
import com.mmhayes.common.utils.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: mjbatko $: Author of last commit
 $Date: 2021-03-02 10:01:08 -0500 (Tue, 02 Mar 2021) $: Date of last commit
 $Rev: 13571 $: Revision of last commit

Notes: All notes for pages go here
*/

public class Validation {
    /**    _____   ____    _   _  ____ _______            _____  _____    _____  _____   ____  _____  ______ _____ _______ _____ ______  _____   _______ ____    _______ _    _ _____  _____    ____  ____       _ ______ _____ _______  __          _______ _______ _    _  ____  _    _ _______   ____  ______ _____ _   _  _____   __  ___   ___ _   __   _____ _    _ _____  ______  __     ______  _    _   _  ___   _  ______          __ __          ___    _       _______  __     ______  _    _            _____  ______   _____   ____ _____ _   _  _____
     //    |  __ \ / __ \  | \ | |/ __ \__   __|     /\   |  __ \|  __ \  |  __ \|  __ \ / __ \|  __ \|  ____|  __ \__   __|_   _|  ____|/ ____| |__   __/ __ \  |__   __| |  | |_   _|/ ____|  / __ \|  _ \     | |  ____/ ____|__   __| \ \        / /_   _|__   __| |  | |/ __ \| |  | |__   __| |  _ \|  ____|_   _| \ | |/ ____| /_ |/ _ \ / _ (_) / /  / ____| |  | |  __ \|  ____| \ \   / / __ \| |  | | | |/ / \ | |/ __ \ \        / / \ \        / / |  | |   /\|__   __| \ \   / / __ \| |  | |     /\   |  __ \|  ____| |  __ \ / __ \_   _| \ | |/ ____|
     //    | |  | | |  | | |  \| | |  | | | |       /  \  | |  | | |  | | | |__) | |__) | |  | | |__) | |__  | |__) | | |    | | | |__  | (___      | | | |  | |    | |  | |__| | | | | (___   | |  | | |_) |    | | |__ | |       | |     \ \  /\  / /  | |    | |  | |__| | |  | | |  | |  | |    | |_) | |__    | | |  \| | |  __   | | | | | | | | / /  | (___ | |  | | |__) | |__     \ \_/ / |  | | |  | | | ' /|  \| | |  | \ \  /\  / /   \ \  /\  / /| |__| |  /  \  | |     \ \_/ / |  | | |  | |    /  \  | |__) | |__    | |  | | |  | || | |  \| | |  __
     //    | |  | | |  | | | . ` | |  | | | |      / /\ \ | |  | | |  | | |  ___/|  _  /| |  | |  ___/|  __| |  _  /  | |    | | |  __|  \___ \     | | | |  | |    | |  |  __  | | |  \___ \  | |  | |  _ < _   | |  __|| |       | |      \ \/  \/ /   | |    | |  |  __  | |  | | |  | |  | |    |  _ <|  __|   | | | . ` | | |_ |  | | | | | | | |/ /    \___ \| |  | |  _  /|  __|     \   /| |  | | |  | | |  < | . ` | |  | |\ \/  \/ /     \ \/  \/ / |  __  | / /\ \ | |      \   /| |  | | |  | |   / /\ \ |  _  /|  __|   | |  | | |  | || | | . ` | | |_ |
     //    | |__| | |__| | | |\  | |__| | | |     / ____ \| |__| | |__| | | |    | | \ \| |__| | |    | |____| | \ \  | |   _| |_| |____ ____) |    | | | |__| |    | |  | |  | |_| |_ ____) | | |__| | |_) | |__| | |___| |____   | |       \  /\  /   _| |_   | |  | |  | | |__| | |__| |  | |    | |_) | |____ _| |_| |\  | |__| |  | | |_| | |_| / / _   ____) | |__| | | \ \| |____     | | | |__| | |__| | | . \| |\  | |__| | \  /\  /       \  /\  /  | |  | |/ ____ \| |       | | | |__| | |__| |  / ____ \| | \ \| |____  | |__| | |__| || |_| |\  | |__| |
     //    |_____/ \____/  |_| \_|\____/  |_|    /_/    \_\_____/|_____/  |_|    |_|  \_\\____/|_|    |______|_|  \_\ |_|  |_____|______|_____/     |_|  \____/     |_|  |_|  |_|_____|_____/   \____/|____/ \____/|______\_____|  |_|        \/  \/   |_____|  |_|  |_|  |_|\____/ \____/   |_|    |____/|______|_____|_| \_|\_____|  |_|\___/ \___/_/ (_) |_____/ \____/|_|  \_\______|    |_|  \____/ \____/  |_|\_\_| \_|\____/   \/  \/         \/  \/   |_|  |_/_/    \_\_|       |_|  \____/ \____/  /_/    \_\_|  \_\______| |_____/ \____/_____|_| \_|\_____|
     //      _____ _                 _                             _        _     _ _ _ _          __          _______ _      _        ____  _____  ______          _  __  _____  _    _ ______   _______ ____     _____       _______ ________          __ __     __
     //     / ____| |               | |                           | |      | |   (_) (_) |         \ \        / /_   _| |    | |      |  _ \|  __ \|  ____|   /\   | |/ / |  __ \| |  | |  ____| |__   __/ __ \   / ____|   /\|__   __|  ____\ \        / /\\ \   / /
     //    | |    | | ___  _   _  __| |   ___ ___  _ __ ___  _ __ | |_ __ _| |__  _| |_| |_ _   _   \ \  /\  / /  | | | |    | |      | |_) | |__) | |__     /  \  | ' /  | |  | | |  | | |__       | | | |  | | | |  __   /  \  | |  | |__   \ \  /\  / /  \\ \_/ /
     //    | |    | |/ _ \| | | |/ _` |  / __/ _ \| '_ ` _ \| '_ \| __/ _` | '_ \| | | | __| | | |   \ \/  \/ /   | | | |    | |      |  _ <|  _  /|  __|   / /\ \ |  <   | |  | | |  | |  __|      | | | |  | | | | |_ | / /\ \ | |  |  __|   \ \/  \/ / /\ \\   /
     //    | |____| | (_) | |_| | (_| | | (_| (_) | | | | | | |_) | || (_| | |_) | | | | |_| |_| |    \  /\  /   _| |_| |____| |____  | |_) | | \ \| |____ / ____ \| . \  | |__| | |__| | |____     | | | |__| | | |__| |/ ____ \| |  | |____   \  /\  / ____ \| |
     //    \_____|_|\___/ \__,_|\__,_|  \___\___/|_| |_| |_| .__/ \__\__,_|_.__/|_|_|_|\__|\__, |     \/  \/   |_____|______|______| |____/|_|  \_\______/_/    \_\_|\_\ |_____/ \____/|______|    |_|  \____/   \_____/_/    \_\_|  |______|   \/  \/_/    \_\_|
     //                                                    | |                              __/ |
     //                                                    |_|                             |___/
     */
    private static DataManager dm = new DataManager(); //the usual data manager
    boolean IsValid = false; //flag for if this object has passed validation
    String loginName = null; //plain text user name
    String loginPassword = null; //plain text user password
    String badgeNum = null; // user's badge number
    String macAddress = null; // user's Mac Address
    Boolean forcePassword = false; //flag to determine if the user has to change their password
    String GSKey = null; //gateway session key
    Integer instanceID = 0; //instance involved in communication
    Integer gatewayID = 0; //gateway involved in communication
    Integer instanceUserID = 0; //identifier on the instance for the user trying to authenticate
    String instanceAlias = ""; //alias of the instance
    Integer instanceTypeID = 0; //ID for the type of the instance - related to MMH_GatewayInstanceType
    Integer gatewayUserID = 0; //identifier on the gateway for the user trying to authenticate
    String hash = null; //communication hash that builds trust between any instance and any gateway
    String details = ""; //stores info messages during the authentication process
    String errorDetails = ""; //stores error messages during the authentication process
    String URL = ""; //URL to instance's login page (not used in hash intentionally)
    String IPAddress = ""; //the end-user's IPAddress
    String userAgent = ""; //the end-user's user agent string
    String gatewayHostName = ""; //the gateway's host name that is involved
    ArrayList<HashMap> data = null; //generic data holder
    ArrayList<HashMap> codes = null; //contains instance codes when a gateway user is authenticated
    List TOS = null; //collection of TOS models
    Integer failureCode = null; //reason code for failing validation
    String DSKey = null; //device session key
    Integer validationType = 1; //type of validation - JUST Quickcharge User (manager, QC_QuickchargeUsers - validationType of 2), OR JUST Quickcharge Account (employee, QC_Employees, - validationType of 3), OR "both" (either) Quickcharge User and Quickcharge Account - validationType of 1
    Boolean keepLogged = false; //determines if the system should extend the session for 100 years or use site.security.sessiontimeoutminutes
    //TODO: (HIGH) - DO NOT ADD PROPERTIES TO THIS OBJECT WITHOUT BEING 100% SURE YOU KNOW WHAT YOU ARE DOING - Cloud comptability WILL BREAK DUE TO GATEWAY -jrmitaly - 12/1/2016
    //Boolean showTour = false; //determines if the application should show a tour when the user logs in
    public static enum FAILURECODE {
        ERROR(2), PASSWORD(3), USERNAME(4), LOCKED(5), BLOCKEDIP(6), INACTIVE(7), DIRECTORY(10);
        private final int value;
        private FAILURECODE(int value) {
            this.value = value;
        }
        public int getCodeInt() {
            return value;
        }
    }

    //reset all class variables to their defaults
    public void reset() {
        this.IsValid = false; //flag for if this object has passed validation
        this.loginName = null; //plain text user name
        this.loginPassword = null; //plain text user password
        this.badgeNum = null;
        this.macAddress = null;
        this.GSKey = null; //gateway session key
        this.instanceID = 0; //instance involved in communication
        this.gatewayID = 0; //gateway involved in communication
        this.instanceUserID = 0; //identifier on the instance for the user trying to authenticate
        this.gatewayUserID = 0; //identifier on the gateway for the user trying to authenticate
        this.hash = null; //communication hash that builds trust between any instance and any gateway
        this.details = ""; //stores info messages during the authentication process
        this.errorDetails = ""; //stores error messages during the authentication process
        this.URL = ""; //URL to instance's login page (not used in hash intentionally)
        //this.IPAddress = null; //the end-user's IPAddress - DO NOT RESET, once set it's set as of 1/27/2015 -jrmitaly
        //this.userAgent = null; //the end-user's user agent string - DO NOT RESET, once set it's set as of 1/27/2015 -jrmitaly
        this.failureCode = null; //stores the reason code for validation failure
    }

    //creates a secure gateway session key and stores the key into the gateway DB
    public void createGSKey() {
        try {

            //create a secure random key
            String GSKey = new BigInteger(256, new SecureRandom()).toString(64);

            //get the current timestamp and set to GSKeyCreatedOnInstant
            Instant GSKeyCreatedOn = Instant.now();

            //add five minutes to the currentTime to determine when the GSKey should expire
            Instant GSKeyExpiresOn = GSKeyCreatedOn.plus(Duration.ofMinutes(5));

            //create a a new GSKey for this InstanceID and InstanceUserID
            String GSKeyCreateSQL=dm.getSQLProperty("data.validation.createGSKey", new Object[]{});
            ArrayList<String> GSKeyCreateValues = new ArrayList<>();
            GSKeyCreateValues.add(0, GSKey);
            GSKeyCreateValues.add(1, this.instanceID.toString());
            GSKeyCreateValues.add(2, this.instanceUserID.toString());
            GSKeyCreateValues.add(3, GSKeyCreatedOn.toString());
            GSKeyCreateValues.add(4, GSKeyExpiresOn.toString());
            dm.insertPreparedStatement(GSKeyCreateSQL, GSKeyCreateValues);
            //set the GSKey for this instance of the object
            this.GSKey = GSKey;
        } catch (Exception ex) {
            this.reset();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Validation.createGSKey - Exception thrown: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            this.setErrorDetails(errorDetails); //set error details
        }
    }

    //checks the gateway DB to determine if the GSKey on this instance is valid
    public boolean verifyGSKEY() {
         try {

            //this overloaded dm.serializeWithColNames utilizes parameterized queries
            ArrayList GSKeyList = dm.serializeSqlWithColNames("data.validation.verifyGSKey",
                    new Object[]{
                        this.GSKey,
                        this.instanceID.toString(),
                        this.instanceUserID.toString(),
                        Instant.now().toString()
                    },
                    false,
                    true
            );

            if (GSKeyList != null && GSKeyList.size() >= 1) { //verified GSKey
                return true;
            } else { //could not verify GSKey
                return false;
            }
        } catch (Exception ex) {
            this.reset();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Validation.verifyGSKEY - Exception thrown: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            this.setErrorDetails(errorDetails); //set error details
            return false;
        }
    }

    //creates a device session key and stores the key into the instance DB
    public void createDSKey() {
        try {

            String DSKey = CommonAPI.createDSKey(getInstanceUserID(), getGatewayUserID(), getKeepLogged(), null);

            this.setDSKey(DSKey);

        } catch (Exception ex) {
            Logger.logMessage("Error in Validation.createDSKey", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //creates a device session key including the SSO idpsessionkey and stores the key into the instance DB
    public void createDSKey(String idpSessionKey) {
        try {

            String DSKey = CommonAPI.createDSKey(getInstanceUserID(), getGatewayUserID(), null, getKeepLogged(), idpSessionKey);

            this.setDSKey(DSKey);

        } catch (Exception ex) {
            Logger.logMessage("Error in Validation.createDSKey", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //for MyQC 2.0 and up, creates a device session key considering the user type ID and stores the key into the instance DB
    public void createDSKey(Integer instanceUserTypeID) {
        try {

            String DSKey = CommonAPI.createDSKey(getInstanceUserID(), getGatewayUserID(), null, getKeepLogged(), "", instanceUserTypeID);

            this.setDSKey(DSKey);

        } catch (Exception ex) {
            Logger.logMessage("Error in Validation.createDSKey", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //creates a device session key and stores the key into the instance DB
    public void createDSKey(boolean isPosApi) {
        try {

            String DSKey = CommonAPI.createDSKey(getInstanceUserID(), getGatewayUserID(), null, isPosApi);

            this.setDSKey(DSKey);

        } catch (Exception ex) {
            Logger.logMessage("Error in Validation.createDSKey", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //creates a device session key and stores the key into the instance DB
    public void createDSKey(OtmLoginModel otmLoginModel) {
        try {

            String DSKey = CommonAPI.createDSKey(getInstanceUserID(), getGatewayUserID(), otmLoginModel);

            this.setDSKey(DSKey);

        } catch (Exception ex) {
            Logger.logMessage("Error in Validation.createDSKey", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
    }

    //verifies Device Session Key and sets the instanceUserID on this instance
    public boolean verifyDSKEY(Instant checkDateTime) {
        try {

            //this overloaded dm.serializeWithColNames utilizes parameterized queries
            ArrayList DSKeyList = dm.serializeSqlWithColNames("data.validation.verifyDSKey",
                    new Object[]{
                        this.getDSKey(),
                        checkDateTime.toString()
                    },
                    false,
                    true
            );

            if (DSKeyList != null && DSKeyList.size() >= 1) { //verified DSKeyList

                //add the instanceUserID to this instance
                HashMap DSKeyHM = (HashMap)DSKeyList.get(0);

                //set accountID or userID as instanceUserID
                setUserID(DSKeyHM);

                //DSKey successfully verified
                return true;
            } else { //could not verify DSKeyList
                return false;
            }
        } catch (Exception ex) {
            this.reset();
            Logger.logException(ex); //always log exceptions
            String errorDetails = "Error in Validation.verifyDSKEY";
            this.setErrorDetails(errorDetails); //set error details
            return false;
        }
    }

    //verifies Device Session Key and sets the instanceUserID on this instance
    public boolean verifyDSKEY() {
        return verifyDSKEY(Instant.now());
    }

    //verifies Device Session Key and sets the instanceUserID on this instance
    public LoginModel verifyDSKEY(LoginModel loginModel) {
        try {

            Instant checkDateTime = Instant.now();

            //this overloaded dm.serializeWithColNames utilizes parameterized queries
            ArrayList DSKeyList = dm.serializeSqlWithColNames("data.validation.verifyDSKey",
                    new Object[]{
                            this.getDSKey(),
                            checkDateTime.toString()
                    },
                    false,
                    true
            );

            //fetch all information from the valid DSKey
            if (DSKeyList != null && DSKeyList.size() >= 1) { //verified DSKeyList

                //add the instanceUserID to this instance
                HashMap DSKeyHM = (HashMap)DSKeyList.get(0);
                loginModel.setApiKeyExpirationDate(CommonAPI.convertModelDetailToString(DSKeyHM.get("EXPIRESON")));
                loginModel.setApiKeyCreateDate(CommonAPI.convertModelDetailToString(DSKeyHM.get("CREATEDON")));
                loginModel.setTerminalId(CommonAPI.convertModelDetailToInteger(DSKeyHM.get("TERMINALID")));
                loginModel.setMessage("Success");

                //set accountID or userID as instanceUserID
                setUserID(DSKeyHM);

                return loginModel;

            } else { //could not verify DSKeyList
                return null;
            }
        } catch (Exception ex) {
            this.reset();
            Logger.logException(ex); //always log exceptions
            String errorDetails = "Error in Validation.verifyDSKEY";
            this.setErrorDetails(errorDetails); //set error details
            return null;
        }
    }

    //verifies Device Session Key
    public OtmLoginModel verifyDSKEY(OtmLoginModel otmloginModel) {
        try {

            Instant checkDateTime = Instant.now();

            //this overloaded dm.serializeWithColNames utilizes parameterized queries
            ArrayList DSKeyList = dm.serializeSqlWithColNames("data.validation.verifyDSKey",
                    new Object[]{
                            this.getDSKey(),
                            checkDateTime.toString()
                    },
                    false,
                    true
            );

            //fetch all information from the valid DSKey
            if (DSKeyList != null && DSKeyList.size() >= 1) { //verified DSKeyList

                //add the instanceUserID to this instance
                HashMap DSKeyHM = (HashMap)DSKeyList.get(0);
                otmloginModel.setApiKeyExpirationDate(CommonAPI.convertModelDetailToString(DSKeyHM.get("EXPIRESON")));
                otmloginModel.setApiKeyCreateDate(CommonAPI.convertModelDetailToString(DSKeyHM.get("CREATEDON")));

                //set accountID or userID as instanceUserID
                setUserID(DSKeyHM);

                return otmloginModel;

            } else { //could not verify DSKeyList
                return null;
            }
        } catch (Exception ex) {
            this.reset();
            Logger.logException(ex); //always log exceptions
            String errorDetails = "Error in Validation.verifyDSKEY";
            this.setErrorDetails(errorDetails); //set error details
            return null;
        }
    }

    //returns a status of valid, invalid or expired for the given DSKey, for when the expiration datetime matters
    public String checkDSKeyStatus() {
        try {

            //this overloaded dm.serializeWithColNames utilizes parameterized queries
            ArrayList<HashMap> DSKeyList = dm.parameterizedExecuteQuery("data.validation.checkDSKeyStatus",
                    new Object[]{
                            this.getDSKey()
                    },
                    true
            );

            if (DSKeyList != null && DSKeyList.size() >= 1) { //verified DSKeyList

                //add the instanceUserID to this instance
                HashMap DSKeyHM = (HashMap)DSKeyList.get(0);

                //set accountID or userID as instanceUserID
                setUserID(DSKeyHM);

                String expiresOn = CommonAPI.convertModelDetailToString(DSKeyHM.get("EXPIRESON"));

                return checkDSKeyValidOrExpired(expiresOn);
            } else { //could not verify DSKeyList
                return "invalid";
            }
        } catch (Exception ex) {
            this.reset();
            Logger.logException(ex); //always log exceptions
            String errorDetails = "Error in Validation.checkDSKeyStatus";
            this.setErrorDetails(errorDetails); //set error details
            return "invalid";
        }
    }

    public HashMap checkDSKeyStatusWithDetails() {
        HashMap DSKeyStatusHM = new HashMap();

        //pull dskey record
        HashMap DSKeyHM = this.retrieveDSKeyDetails();

        //test that a valid record was returned
        if ( DSKeyHM == null ) {
            DSKeyStatusHM.put("result", "invalid");
            return DSKeyStatusHM;
        }

        //check dskey expiration
        String expiresOn = CommonAPI.convertModelDetailToString(DSKeyHM.get("EXPIRESON"));

        if ( this.checkDSKeyValidOrExpired(expiresOn).equals("expired") ) {
            DSKeyStatusHM.put("result", "expired");
            return DSKeyStatusHM;
        }

        //set the personID if one is set
        Integer personID = null;
        if ( DSKeyHM.containsKey("PERSONID") && DSKeyHM.get("PERSONID") != null && !DSKeyHM.get("PERSONID").toString().equals("")) {
            personID = CommonAPI.convertModelDetailToInteger(DSKeyHM.get("PERSONID"));
            DSKeyStatusHM.put("personID", personID);
        }

        //get the employeeID and create a detailed account model
        if ( DSKeyHM.containsKey("EMPLOYEEID") && DSKeyHM.get("EMPLOYEEID") != null && !DSKeyHM.get("EMPLOYEEID").toString().equals("")) {
            Integer accountID = CommonAPI.convertModelDetailToInteger(DSKeyHM.get("EMPLOYEEID"));
            AccountModel accountModel = new AccountModel(accountID);

            //do not set the account model if the account is inactive
            if ( accountModel.getStatus().equals("I") ) {
                accountModel = null;
            }

            //check if there is a personID and if so if this account is actively mapped to that personID
            if ( personID != null ) {
                AccountPersonModel accountPersonModel = new AccountPersonModel();
                String result = accountPersonModel.doesActiveMappingExist(accountID, personID);

                if ( !result.equals("true") ) {
                    accountModel = null;
                }
            }

            DSKeyStatusHM.put("accountModel", accountModel);
        }

        return DSKeyStatusHM;
    }

    private HashMap retrieveDSKeyDetails() {
        ArrayList<HashMap> DSKeyList = dm.parameterizedExecuteQuery("data.validation.checkDSKeyStatus",
            new Object[]{
                this.getDSKey()
            },
            true
        );

        if ( DSKeyList == null || DSKeyList.size() == 0 ) {
            return null;
        }

        return DSKeyList.get(0);
    }

    //takes the DSKey table field ExpiresOn as a String and returns whether it is valid or expired
    private String checkDSKeyValidOrExpired(String expiresOn) {
        String date = expiresOn.substring(0, 10);
        String time = expiresOn.substring(11, expiresOn.length());
        String utcTime = date.concat("T").concat(time).concat("Z");

        Instant expiration = Instant.parse(utcTime);

        //if not expired yet
        if ( expiration.isAfter(Instant.now())) {
            return "valid";
        }

        return "expired";
    }

    private void setUserID(HashMap DSKeyHM) {
        try {
            Integer instanceUserTypeID = null;
            if (DSKeyHM.get("EMPLOYEEID") != null && !DSKeyHM.get("EMPLOYEEID").toString().isEmpty()) { //if the DSKey was for an account (employee)
                this.setInstanceUserID(Integer.parseInt(DSKeyHM.get("EMPLOYEEID").toString()));
                instanceUserTypeID = 1;
            }
            if (DSKeyHM.get("USERID") != null && !DSKeyHM.get("USERID").toString().isEmpty()) { //if the DSKey was for a user (manager)
                this.setInstanceUserID(Integer.parseInt(DSKeyHM.get("USERID").toString()));
                instanceUserTypeID = 2;
            }
            if ( DSKeyHM.containsKey("PERSONID") && DSKeyHM.get("PERSONID") != null && !DSKeyHM.get("PERSONID").toString().isEmpty() ) {
                //should only ever set the instanceUserID to the PersonID if there is no AccountID (EmployeeID)
                if ( this.getInstanceUserID() == null || this.getInstanceUserID() == 0 ) {
                    this.setInstanceUserID(CommonAPI.convertModelDetailToInteger(DSKeyHM.get("PERSONID")));
                }
                //always set the instanceUserTypeID, even if the instanceUserID is an AccountID (EmployeeID), to denote this Validation object is for a Person
                instanceUserTypeID = 3;
            }
            if ( (this.getInstanceUserID() == null || this.getInstanceUserID() == 0) && DSKeyHM.get("TERMINALID") != null && !DSKeyHM.get("TERMINALID").toString().isEmpty() ) {
                this.setInstanceUserID( CommonAPI.convertModelDetailToInteger( DSKeyHM.get("TERMINALID") ) );
            }
            if ( (this.getInstanceUserID() == null || this.getInstanceUserID() == 0) && DSKeyHM.get("KMSSTATIONID") != null && !DSKeyHM.get("KMSSTATIONID").toString().isEmpty() ) {
                this.setInstanceUserID( CommonAPI.convertModelDetailToInteger( DSKeyHM.get("KMSSTATIONID") ) );
                this.setInstanceStationID( CommonAPI.convertModelDetailToInteger( DSKeyHM.get("KMSSTATIONID") ) );
            }
            if ( instanceUserTypeID != null ) {
                this.setInstanceUserTypeID(instanceUserTypeID);
            }
        } catch (Exception ex) {
            this.reset();
            Logger.logException(ex); //always log exceptions
            String errorDetails = "Error in Validation.setUserID";
            this.setErrorDetails(errorDetails); //set error details
        }
    }

    //no property for instanceUserTypeID, store in data
    public void setInstanceUserTypeID(Integer instanceUserTypeID) {
        ArrayList<HashMap> dataList = new ArrayList<HashMap>();
        HashMap<String, Integer> dataHM = new HashMap<String, Integer>();
        dataHM.put("instanceUserTypeID", instanceUserTypeID);

        //if data already exists, just add the new HM
        if ( this.getData() != null ) {
            dataList = this.getData();
        }

        dataList.add(dataHM);
        this.setData(dataList);
    }

    //no property for instanceUserTypeID, get from data
    public Integer fetchInstanceUserTypeID() {
        Integer instanceUserTypeID = null;

        //instanceUserTypeID is stored in data, if data is null or a size of 0, then it is not set
        if ( this.getData() == null || this.getData().size() == 0 ) {
            return null;
        }

        for ( int i = 0; i < this.getData().size(); i++ ) {
            HashMap dataHM = this.getData().get(i);
            if ( dataHM.containsKey("instanceUserTypeID") ) {
                instanceUserTypeID = CommonAPI.convertModelDetailToInteger(dataHM.get("instanceUserTypeID"));
                break;
            }
        }

        return instanceUserTypeID;
    }


    //no property for instanceUserTypeID, store in data
    @JsonIgnore
    public void setInstanceStationID(Integer instanceStationID) {
        ArrayList<HashMap> dataList = new ArrayList<HashMap>();
        HashMap<String, Integer> dataHM = new HashMap<String, Integer>();
        dataHM.put("instanceStationID", instanceStationID);

        //if data already exists, just add the new HM
        if ( this.getData() != null ) {
            dataList = this.getData();
        }

        dataList.add(dataHM);
        this.setData(dataList);
    }

    //no property for instanceStationID, get from data
    @JsonIgnore
    public Integer getInstanceStationID() {
        Integer instanceStationID = null;

        //instanceStationID is stored in data, if data is null or a size of 0, then it is not set
        if ( this.getData() == null || this.getData().size() == 0 ) {
            return null;
        }

        for ( int i = 0; i < this.getData().size(); i++ ) {
            HashMap dataHM = this.getData().get(i);
            if ( dataHM.containsKey("instanceStationID") ) {
                instanceStationID = CommonAPI.convertModelDetailToInteger(dataHM.get("instanceStationID"));
                break;
            }
        }

        return instanceStationID;
    }

    //this method creates a hash which builds trust between any gateway and any instance
    public void buildHash() {
        try {
            this.hash = "";
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            String salt = determineSalt(); //determine salt
            if (!salt.equals("invalid")) {
                md.update(salt.getBytes()); //apply salt
                this.hash = createValidationHash(md); //set hash
            } else {
                this.hash = "invalid";
            }
        } catch (NoSuchAlgorithmException e) {
            this.reset();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "(No such algorithm) in Validation.buildHash - Exception thrown: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            this.setErrorDetails(errorDetails); //set error details
        } catch (Exception ex) {
            this.reset();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Validation.buildHash - Exception thrown: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            this.setErrorDetails(errorDetails); //set error details
        }
    }

    //this method creates a hash to verify an existing hash which ensures the trust built between any
    public boolean verifyHash() {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            String salt = determineSalt(); //determine salt

            //if both the salt and the hash are valid then continue verification
            if (!salt.equals("invalid") && !this.hash.equals("invalid")) {
                md.update(salt.getBytes()); //apply salt
                String checkHash = createValidationHash(md); //create hash to check against
                boolean checkResult = checkHash.equals(this.hash);
                if (!checkResult) {
                    Logger.logMessage("Verifying hash...", Logger.LEVEL.ERROR);
                    Logger.logMessage("checkHash = "+checkHash, Logger.LEVEL.ERROR);
                    Logger.logMessage("this.hash = "+this.hash, Logger.LEVEL.ERROR);
                    this.hash = "invalid";
                    this.IsValid = false;
                    this.setDetails("Login Failure.");
                }
                return checkResult;
            } else {
                Logger.logMessage("Verifying hash...", Logger.LEVEL.ERROR);
                Logger.logMessage("checkHash = N/A", Logger.LEVEL.ERROR);
                Logger.logMessage("this.hash = "+this.hash, Logger.LEVEL.ERROR);
                return false;
            }
        } catch (NoSuchAlgorithmException e) {
            this.reset();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "(No such algorithm) in Validation.buildHash - Exception thrown: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            this.setErrorDetails(errorDetails); //set error details
        } catch (Exception ex) {
            this.reset();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Validation.buildHash - Exception thrown: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            this.setErrorDetails(errorDetails); //set error details
        }
        return false;
    }

    //this method prepares the Validation object for secure communication - starting at the gateway and ending at the instance
    public void prepareGatewayToInstanceComm() {
        try {

            //get this gateway's identifier and set it
            this.setGatewayID(CommonAPI.getGatewayID());

            //set this gateway's hostname
            this.setGatewayHostName(CommonAPI.getWindowsHostName());

            //set back to 0 for hashing purposes
            this.setInstanceID(0);

            //build the hash for secure communication
            this.buildHash();
        } catch (Exception ex) {
            this.reset();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Validation.prepareGatewayToInstanceComm - Exception thrown: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            this.setErrorDetails(errorDetails); //set error details
        }
    }

    //OVERLOAD - this method prepares the Validation object for secure communication - starting at the gateway and ending at the instance, takes in parameter to determine the manager login
    public void prepareInstanceToGatewayComm(HttpServletRequest request, Integer validationType) {
        try {

            //set the IPAddress and UserAgent from the request object
            this.setIPAddress(CommonAPI.getEndUserIPAddress(this, request));
            this.setUserAgent(request.getHeader("User-Agent"));

            //if manager login get the qc instance id and set it
            if(validationType != null && validationType == 2) {
                this.setInstanceID(CommonAPI.getQCInstanceID());
            }

            //if employee login or qc instance ID is not found, get this instances id and set it
            if(validationType == null || this.getInstanceID() == 0) {
                this.setInstanceID(CommonAPI.getInstanceID());
            }

            //set back to 0 for hashing purposes
            this.setGatewayID(0);

            //build the hash for secure communication
            this.buildHash();
        } catch (Exception ex) {
            this.reset();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Validation.prepareGatewayToInstanceComm - Exception thrown: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            this.setErrorDetails(errorDetails); //set error details
        }
    }

    //this method prepares the Validation object for secure communication - starting at the gateway and ending at the instance
    public void prepareInstanceToGatewayComm(HttpServletRequest request) {
        try {

            //set the IPAddress and UserAgent from the request object
            this.setIPAddress(CommonAPI.getEndUserIPAddress(this, request));
            this.setUserAgent(request.getHeader("User-Agent"));

            //get this instances identifier and set it
            this.setInstanceID(CommonAPI.getInstanceID());

            //set back to 0 for hashing purposes
            this.setGatewayID(0);

            //build the hash for secure communication
            this.buildHash();
        } catch (Exception ex) {
            this.reset();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Validation.prepareGatewayToInstanceComm - Exception thrown: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            this.setErrorDetails(errorDetails); //set error details
        }
    }

    //this method handles generic exceptions for this class
    public void handleGenericException(Exception ex, String classAndMethodName) {
        this.reset();
        this.setDetails("An error has occurred. Please contact MMHayes support if the error continues.");
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
        String errorDetails = "Error in "+classAndMethodName+" - Exception thrown: " + sw.toString();
        Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
        this.setErrorDetails("An error has occurred. Please see logs."); //set error details
    }

    //this method determines the salt that should be used (either a gateway salt or an instance salt)
    private String determineSalt() {
        String salt = "";
        try {
            //get this application's security type from the application properties file
            String securityType = CommonAPI.getSecurityType();

            //get and apply salt
            if (this.instanceID != 0 && (this.gatewayID == null || this.gatewayID == 0)) {
                if (securityType.equals("instance")) {
                    //get salt from application properties file
                    salt = CommonAPI.getSecurityKey();
                } else if (securityType.equals("gateway")) {
                    //get salt from database
                    salt = dm.getSingleField("data.validation.getInstanceKey",
                                new Object[]{
                                    this.instanceID
                                },
                                false,
                                true
                            ).toString();
                } else {
                    salt = "invalid";
                    String details = "Invalid gateway or instance.";
                    Logger.logMessage(details, Logger.LEVEL.ERROR); //log details
                    this.setErrorDetails(details); //set error details
                }
            } else if (this.gatewayID != 0 && this.instanceID == 0) {
                if (securityType.equals("gateway")) {
                    //get salt from application properties file
                    salt = CommonAPI.getSecurityKey();
                } else if (securityType.equals("instance")) {
                    //get salt from database
                    salt = dm.getSingleField("data.validation.getGatewayKey",
                                new Object[]{
                                    this.gatewayID
                                },
                                false,
                                true
                            ).toString();
                } else {
                    salt = "invalid";
                    String details = "Invalid gateway or instance.";
                    Logger.logMessage(details, Logger.LEVEL.ERROR); //log details
                    this.setErrorDetails(details); //set error details
                }
            } else {
                salt = "invalid";
                String details = "Invalid gateway or instance.";
                Logger.logMessage(details, Logger.LEVEL.ERROR);//log details
                this.setErrorDetails(details); //set error details
            }
        } catch (Exception ex) {
            this.reset();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Validation.determineSalt - Exception thrown: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log error details
            this.setErrorDetails(errorDetails); //set error details
        }
        return salt;
    }

    //this method creates a hash which includes the salt and many object properties
    private String createValidationHash(MessageDigest md) {
        try {
            //create string of the Validation Properties we wish to hash
            String instanceIDAsString = Integer.toString(this.instanceID);
            String gatewayIDAsString = Integer.toString(this.gatewayID);
            String instanceUserIDAsString = Integer.toString(this.instanceUserID);
            String gatewayUserIDAsString = Integer.toString(this.gatewayUserID);
            StringBuilder sb = new StringBuilder();
            sb.append(this.loginName);
            sb.append(this.loginPassword);
            sb.append(this.GSKey);
            sb.append(instanceIDAsString);
            sb.append(gatewayIDAsString);
            sb.append(instanceUserIDAsString);
            sb.append(gatewayUserIDAsString);
            String validationPropertiesAsString = sb.toString();

            //create a byte array containing the SALT + validationPropertiesAsString
            byte[] bytes = md.digest(validationPropertiesAsString.getBytes());

            //build hash
            StringBuilder hashbuilder = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                hashbuilder.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            //return hash
            return hashbuilder.toString();
        } catch (Exception ex) {
            this.reset();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            String errorDetails = "Validation.createValidationHash - Exception thrown: " + sw.toString();
            Logger.logMessage(errorDetails, Logger.LEVEL.ERROR); //log details
            this.setErrorDetails(errorDetails); //set error details
        }
        return "";
    }

//START - getters/setters

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = commonMMHFunctions.sanitizeStringFromXSS(loginName);
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = commonMMHFunctions.sanitizeStringFromXSS(loginPassword);
    }

    public String getBadgeNum() {
        return badgeNum;
    }

    public void setBadgeNum(String badgeNum) {
        this.badgeNum = badgeNum;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public Boolean getForcePassword() {
        return forcePassword;
    }

    public void setForcePassword(Boolean forcePassword) {
        this.forcePassword = forcePassword;
    }

    public Integer getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(Integer instanceID) {
        this.instanceID = instanceID;
    }

    public String getInstanceAlias() {
        return instanceAlias;
    }

    public void setInstanceAlias(String instanceAlias) {
        this.instanceAlias = commonMMHFunctions.sanitizeStringFromXSS(instanceAlias);
    }

    public Integer getInstanceTypeID() {
        return instanceTypeID;
    }

    public void setInstanceTypeID(Integer instanceTypeID) {
        this.instanceTypeID = instanceTypeID;
    }

    public Integer getGatewayID() {
        return gatewayID;
    }

    public void setGatewayID(Integer gatewayID) {
        this.gatewayID = gatewayID;
    }

    public Integer getInstanceUserID() {
        return instanceUserID;
    }

    public void setInstanceUserID(Integer instanceUserID) {
        this.instanceUserID = instanceUserID;
    }

    public Integer getGatewayUserID() {
        return gatewayUserID;
    }

    public void setGatewayUserID(Integer gatewayUserID) {
        this.gatewayUserID = gatewayUserID;
    }

    public String getHash() {
        return this.hash;
    }

    public void setHash(String hash) {
        this.hash = commonMMHFunctions.sanitizeStringFromXSS(hash);
    }

    public String getGSKey() {
        return GSKey;
    }

    public void setGSKey(String GSKey) {
        this.GSKey = commonMMHFunctions.sanitizeStringFromXSS(GSKey);
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = commonMMHFunctions.sanitizeStringFromXSS(URL);
    }

    public boolean getIsValid() {
        return IsValid;
    }

    public void setIsValid(boolean IsValid) {
        this.IsValid = IsValid;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = commonMMHFunctions.sanitizeStringFromXSS(details);
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = commonMMHFunctions.sanitizeStringFromXSS(errorDetails);
    }

    public ArrayList<HashMap> getData() {
        return this.data;
    }

    public void setData(ArrayList<HashMap> data) {
        this.data = data;
    }

    /*
    DO NOT NAME THIS METHOD getFailureCode - Jorah and Jeff found a very bizzare edge case on 1/15/2015
    We think there is something counting the number of "get" methods in this object and is hosing using the InstanceClient (possibly the jersey post method??)

    UPDATE (1/27/2015) by jrmitaly:
        Still don't know the cause of the bug but I am updating the Jackson libraries to 2.5.0..
        The Jackson libs DEFINITELY counts the number of getters/setters in this object when converting the object to JSON
    */
    public Integer fetchFailureCode() {
        return this.failureCode;
    }

    public void setFailureCode(int failureCode) {
        this.failureCode = failureCode;
    }

    public String getIPAddress() {
        return IPAddress;
    }

    public void setIPAddress(String IPAddress) {
        this.IPAddress = commonMMHFunctions.sanitizeStringFromXSS(IPAddress);
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = commonMMHFunctions.sanitizeStringFromXSS(userAgent);
    }

    public String getGatewayHostName() {
        return gatewayHostName;
    }

    public void setGatewayHostName(String gatewayHostName) {
        this.gatewayHostName = commonMMHFunctions.sanitizeStringFromXSS(gatewayHostName);
    }

    public ArrayList<HashMap> getCodes() {
        return this.codes;
    }

    public void setCodes(ArrayList<HashMap> codes) {
        this.codes = codes;
    }

    public List getTOS() {
        return this.TOS;
    }

    public void setTOS(List TOS) {
        this.TOS = TOS;
    }

    public String getDSKey() {
        return DSKey;
    }

    public void setDSKey(String DSKey) {
        this.DSKey = DSKey;
    }

    public Integer getValidationType() {
        return validationType;
    }

    public void setValidationType(Integer validationType) {
        this.validationType = validationType;
    }

    public Boolean getKeepLogged() {
        return keepLogged;
    }

    public void setKeepLogged(Boolean keepLogged) {
        this.keepLogged = keepLogged;
    }

//END - getters/setters

    @Override
    public String toString() {
        return "Validation{" +
                "IsValid=" + IsValid +
                ", loginName='" + loginName + '\'' +
                ", loginPassword='" + loginPassword + '\'' +
                ", badgeNum='" + badgeNum + '\'' +
                ", macAddress='" + macAddress + '\'' +
                ", forcePassword=" + forcePassword +
                ", GSKey='" + GSKey + '\'' +
                ", instanceID=" + instanceID +
                ", gatewayID=" + gatewayID +
                ", instanceUserID=" + instanceUserID +
                ", instanceAlias='" + instanceAlias + '\'' +
                ", instanceTypeID=" + instanceTypeID +
                ", gatewayUserID=" + gatewayUserID +
                ", hash='" + hash + '\'' +
                ", details='" + details + '\'' +
                ", errorDetails='" + errorDetails + '\'' +
                ", URL='" + URL + '\'' +
                ", IPAddress='" + IPAddress + '\'' +
                ", userAgent='" + userAgent + '\'' +
                ", gatewayHostName='" + gatewayHostName + '\'' +
                ", data=" + data +
                ", codes=" + codes +
                ", TOS=" + TOS +
                ", failureCode=" + failureCode +
                ", DSKey='" + DSKey + '\'' +
                ", validationType=" + validationType +
                ", keepLogged=" + keepLogged +
                '}';
    }

}