package com.mmhayes.common.api;

//mmhayes dependencies
import com.mmhayes.common.api.*;
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.*;
import org.apache.commons.collections.CollectionUtils;

//javax.ws.rs dependencies
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.container.*;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.*;

//other dependencies
import java.util.*;
import java.io.*;

/*
 REST API Response Filter
 
 Created with IntelliJ IDEA.
 User: jrmitaly
 Date: 7/13/15
 Time: 2:27 PM
 
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-07-26 14:13:38 -0400 (Wed, 26 Jul 2017) $: Date of last commit
 $Rev: 4732 $: Revision of last commit

Notes: This is a filter for all REST API responses
*/

/*

//Removed on 7/26/2017 - it doest not appear we are using this for anything at this point- jrmitaly
@Provider
public class APIResponseFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {

        //WARNING: THE FOLLOWING CODE ALLOWS CROSS DOMAIN REQUESTS FOR ALL JERSEY REST API END POINTS (IF SET IN WEB.XML) -jrmitaly 8/17/2015
        //WARNING: THE FOLLOWING CODE ALLOWS CROSS DOMAIN REQUESTS FOR ALL JERSEY REST API END POINTS (IF SET IN WEB.XML) -jrmitaly 8/17/2015
        //WARNING: THE FOLLOWING CODE ALLOWS CROSS DOMAIN REQUESTS FOR ALL JERSEY REST API END POINTS (IF SET IN WEB.XML) -jrmitaly 8/17/2015

        //allows ALL cross origin requests
        //enables the use of cookies by setting the exact origin rather than wildcard (for some reason wild card is not valid when using cookies)
        //String origin = "*";
        if (requestContext.getHeaders().get("Origin") != null) {
            String origin = requestContext.getHeaders().get("Origin").toString().replaceAll("\\[", "").replaceAll("\\]", "");
            responseContext.getHeaders().putSingle("Access-Control-Allow-Origin", origin); //allows ANY domain to call
        }

        responseContext.getHeaders().putSingle("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD"); //allow ALL HTTP VERBS
        responseContext.getHeaders().putSingle("ALLOW", "OPTIONS"); //allow OPTIONS (for pre-flight request)
        //responseContext.getHeaders().add("Access-Control-Allow-Credentials", "true"); //this has to do with allowing sessions
        //responseContext.getHeaders().add("Access-Control-Max-Age", "1209600"); //policy for renewing the pre-flight CORS check (14 days)

        //get the headers from the request
        List<String> requestHeaders = requestContext.getHeaders().get("Access-Control-Request-Headers");

        //allow ANY of the headers that the client sent us
        if(requestHeaders != null) {
            responseContext.getHeaders().put("Access-Control-Allow-Headers", new ArrayList<Object>(requestHeaders));
        } else {
            //allows ALL standard access control request headers that the client could send
            responseContext.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
        }

        //clean the URL parameters on the way out as well
        cleanURLParamaters(requestContext);

    }

    //clean up URL parameters to make sure there is no XSS threat
    private void cleanURLParamaters(ContainerRequestContext request) {
        UriBuilder builder = request.getUriInfo().getRequestUriBuilder();
        MultivaluedMap<String, String> queries = request.getUriInfo().getQueryParameters();

        for (Map.Entry<String, List<String>> query : queries.entrySet()) {
            String key = query.getKey();
            List<String> values = query.getValue();

            List<String> sanitizedValues = new ArrayList<String>();
            for (String value : values) {
                sanitizedValues.add(commonMMHFunctions.sanitizeStringFromXSSWithESAPI(value));
            }

            Integer size = CollectionUtils.size(sanitizedValues);
            builder.replaceQueryParam(key);

            if (size == 1) {
                builder.replaceQueryParam(key, sanitizedValues.get(0));
            } else if (size > 1) {
                builder.replaceQueryParam(key, sanitizedValues.toArray());
            }
        }

        request.setRequestUri(builder.build());
    }


}

*/