package com.mmhayes.common.api.logging;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.utils.Logger;
import org.apache.commons.collections.map.MultiValueMap;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.message.internal.ReaderWriter;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-24 10:54:30 -0400 (Wed, 24 Jul 2019) $: Date of last commit
 $Rev: 9144 $: Revision of last commit
*/

@Provider
public class CustomLoggingFilter extends LoggingFilter implements ContainerRequestFilter, ContainerResponseFilter {

    /**
     * This overidden filter handles the Request logging.  It formats the JSON body to remove all formatting for better performance.
     * @param requestContext
     * @throws IOException
     */
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        StringBuilder sb = new StringBuilder();

        String uriPath = requestContext.getUriInfo().getPath();
        sb.append("HTTP REQUEST : ").append(uriPath);
        sb.append(" ").append(requestContext.getMethod());

        String DSKey = getDSKey(requestContext.getHeaders());
        if (DSKey != null) {
            sb.append(" - DSKey: ").append(DSKey);
        }

        sb.append(" - Header: ").append(requestContext.getHeaders());

        //prevent logging request information when the request contains a plaintext password - login attempts should be recorded in the DB - added by jrmitaly on 11/2/2017
        if (uriPath.equals("auth/login") //standard login
                || uriPath.equals("auth/login/users") //standard login for QC Users (Managers)
                || uriPath.equals("auth/login/accounts") //standard login for QC Accounts (Employees)
                || uriPath.equals("auth/login/user") //standard login for QC Users (Managers)
                || uriPath.equals("auth/login/account") //standard login for QC Accounts (Employees)
                || uriPath.equals("auth/update") //force change password
                || uriPath.equals("auth/update/attempt") //force change password
                || uriPath.equals("auth/reauth") //My QC re-auth
                || uriPath.equals("auth/reauth/off")  //My QC re-auth
                || uriPath.equals("auth/enter") //cloud gateway enter
                || uriPath.equals("auth/reset") //cloud gateway pw reset
                || uriPath.equals("auth/reset/request") //cloud gateway pw reset
                || uriPath.equals("auth/reset/attempt") //cloud gateway pw reset
                || uriPath.equals("account/new") //new account creation
                || uriPath.equals("account/pass/change") //change password
                || uriPath.equals("account/pass/force") //force change password
                || uriPath.equals("auth/validate") //cloud gateway can take plaintext password in this case
            ) {
            sb.append(" - Entity: REDACTED");
        } else {
            sb.append(" - Entity: ").append(getEntityBody(requestContext));   //\r/n
        }


        //set the logfile to null, this will write to the POS_API_COMMON_T log
        Logger.logMessage(sb.toString(), PosAPIHelper.getLogFileName(), Logger.LEVEL.TRACE);
    }

    /**
     * Extract the JSON body request body from the ContainerRequestContext
     * @param requestContext
     * @return
     */
    private String getEntityBody(ContainerRequestContext requestContext) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InputStream in = requestContext.getEntityStream();
        String requestJsonText = "";

        final StringBuilder b = new StringBuilder();
        try {
            ReaderWriter.writeTo(in, out);

            byte[] requestEntity = out.toByteArray();
            if (requestEntity.length == 0) {
                //b.append("").append("\n");
            } else {
                b.append(new String(requestEntity));
                //b.append(new String(requestEntity)).append("\n");
            }
            requestContext.setEntityStream(new ByteArrayInputStream(requestEntity));

            requestJsonText = b.toString().replaceAll("\r\n", ""); //remove line breaks

        } catch (IOException ex) {
            //Handle logging error
            requestJsonText = " Error Logging Request Details.";
        }

        return requestJsonText;
    }

    /**
     * This overridden filter handles the Response
     * @param requestContext
     * @param responseContext
     * @throws IOException
     */
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        StringBuilder sb = new StringBuilder();
        ObjectMapper om = new ObjectMapper();
        boolean doResponseLengthCheck = false;

        sb.append("HTTP RESPONSE : ");
        sb.append("Header: ").append(responseContext.getHeaders());

        String jsonObject = om.writeValueAsString(responseContext.getEntity());

        //For certain endpoints, check the response length when writing to the log.
        //If length is > 50,000 chars only log in debug
        if (requestContext != null && requestContext.getUriInfo() != null){
            String uriPath = requestContext.getUriInfo().getPath();
            if (uriPath.startsWith("keypad/terminal")){
                doResponseLengthCheck = true;
            }
        }

        if (doResponseLengthCheck) {
            if (jsonObject != null && jsonObject.length() > 50_000) {
                if (Logger.currentLogLevel.getCodeInt() > Logger.LEVEL.TRACE.getCodeInt()){
                    Logger.LEVEL curLevel = Logger.currentLogLevel;
                    sb.append(" - Entity: ").append(jsonObject); //Log the full response for DEBUG and LUDICROUS
                    sb.append("\r\n");
                    Logger.logMessage(sb.toString(), PosAPIHelper.getLogFileName(), curLevel);
                } else if (Logger.currentLogLevel.getCodeInt() == Logger.LEVEL.TRACE.getCodeInt()){
                    sb.append(" - Entity: ").append("JSON Response is very large (>50,000 chars).  It will only be logged with DEBUG mode set.");
                    sb.append("\r\n");
                    Logger.logMessage(sb.toString(), PosAPIHelper.getLogFileName(), Logger.LEVEL.TRACE);
                }
            }
        } else {
            sb.append(" - Entity: ").append(jsonObject);
            sb.append("\r\n");
            Logger.logMessage(sb.toString(), PosAPIHelper.getLogFileName(), Logger.LEVEL.TRACE);
        }
    }

    /**
     * For Third Party requests, log the DSKey.  This can be used to get the Terminal ID
     * @param headers
     * @return
     */
    //For Third Parties, log the DSKey
    private String getDSKey(javax.ws.rs.core.MultivaluedMap<String, String> headers) {

        String DSKey = null;

        if (headers.containsKey("X-MMHayes-Auth")) {
            DSKey = headers.getFirst("X-MMHayes-Auth");
        }

        return DSKey;
    }

    //used to create the log file name
    private boolean isQcPosRequest(javax.ws.rs.core.MultivaluedMap<String, String> headers) {
        boolean isQcPos = false;

        if (headers.containsKey("X-MMHayes-JAuth")) {

            isQcPos = true;
        }
        return isQcPos;
    }
}

