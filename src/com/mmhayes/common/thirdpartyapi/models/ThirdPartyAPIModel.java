package com.mmhayes.common.thirdpartyapi.models;

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.utils.Logger;

import java.util.HashMap;

public abstract class ThirdPartyAPIModel {

    private final static String className = "ThirdPartyAPIModel";

    private int apiId = -1;

    private String name = "";
    private String authId = "";
    private String authToken = "";
    private Integer apiTypeId = -1;
    private String sendFromPhoneNumber = "";

    private DataManager dm = new DataManager();

    /**
     * Default constructor - does nothing, requires you to set any api details later
     */
    public ThirdPartyAPIModel() {
    }

    /**
     * Non default constructor - uses the given api id to retrieve any api details
     * from the database and sets those details accordingly
     *
     * @param apiId the api id to use for this api
     */
    public ThirdPartyAPIModel(Integer apiId) {
        setApiDetails(apiId);
    }

    /**
     * Sets the api id
     *
     * @param apiId the new api id
     */
    public void setApiId(Integer apiId) {
        this.apiId = apiId;
    }

    /**
     * Gets the api id
     *
     * @return the new api id
     */
    public Integer getApiId() {
        return apiId;
    }

    /**
     * Sets the name for the api model
     *
     * @param name the new name for the api model
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the name for the api model
     *
     * @return the name for the api model
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the auth id for the api model
     *
     * @param authId the new auth id for the api model
     */
    public void setAuthId(String authId) {
        this.authId = authId;
    }

    /**
     * Gets the auth id for the api model
     *
     * @return the auth id for the api model
     */
    public String getAuthId() {
        return authId;
    }

    /**
     * Sets the auth token for the api model
     *
     * @param authToken the new auth token for the api model
     */
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    /**
     * Gets the auth token for the api model
     *
     * @return the auth token for the api model
     */
    public String getAuthToken() {
        return authToken;
    }

    /**
     * Sets the origin phone number for any api request made
     *
     * @param sendFromPhoneNumber the new origin phone number for any api request made
     */
    public void setSendFromPhoneNumber(String sendFromPhoneNumber) {
        this.sendFromPhoneNumber = sendFromPhoneNumber;
    }

    /**
     * Gets the origin phone number for any api request made
     *
     * @return the origin phone number for any api request made
     */
    public String getSendFromPhoneNumber() {
        return sendFromPhoneNumber;
    }

    /**
     * Sets the api type id
     * 1 - Twilio
     *
     * @param apiTypeId the new api type id
     */
    public void setApiTypeId(Integer apiTypeId) {
        this.apiTypeId = apiTypeId;
    }

    /**
     * Gets the api type id
     *
     * @return the api type id
     */
    public Integer getApiTypeId() {
        return apiTypeId;
    }

    /**
     * Sets the api details using the given api id. Sets every detail available based on the third party
     * api database table.
     *
     * @param apiId
     */
    public void setApiDetails(Integer apiId) {

        String method = className + ".setApiDetails";

        try {
            setApiId(apiId);

            HashMap apiDetails = new DynamicSQL("data.thirdPartyApis.getApiDetails")
                    .addCount(1, 1)
                    .addConditionalClause(2, "WHERE", "ThirdPartyAPIID", "=", apiId)
                    .serializeSingleRecord(dm);

            // Get all of the fields which are required fields on the Third Party API Editor
            setName(apiDetails.get("NAME").toString());
            setApiTypeId(Integer.parseInt(apiDetails.get("THIRDPARTYAPITYPEID").toString()));
            setAuthId(apiDetails.get("APIKEY").toString());
            setAuthToken(apiDetails.get("APITOKEN").toString());

            // Get all fields which are not always required on the Third Party API Editor,
            // but default the value to empty/none in the event that it is not set for the api at hand
            setSendFromPhoneNumber(apiDetails.getOrDefault("SMSPHONENUMBER", "").toString());
        }
        catch (Exception ex) {
            Logger.logMessage("Error setting api details in " + method, Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

    }

    /**
     * Handles the necessary functionality to carry out the desired api call
     * Should be overridden in actual api models, as handling may differ per model
     */
    public abstract void handleApiCall();

    /**
     * Checks whether the required values have been set in order to successfully
     * handle an api
     * Should be overridden in actual api models, as it may differ per model
     *
     * @return true if all the required values are set, false otherwise
     */
    public abstract Boolean checkRequiredValuesSet();



}
