package com.mmhayes.common.thirdpartyapi.models;

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.DynamicSQL;
import com.mmhayes.common.utils.Logger;

import java.util.HashMap;

public abstract class ThirdPartyAPIFactory {

    private final String className = "ThirdPartyAPIFactory";

    /**
     * Gets the third party api based on the api id.
     *
     * @param apiId the id of the third party api to use
     * @return a third party api model based on the api id passed in
     */
    public abstract ThirdPartyAPIModel getThirdPartyApi(Integer apiId);

    /**
     * Gets the api type id of a given api id
     *
     * @param apiId the api id to get the api type id of
     * @return the api type id of the given api id, or -1 if none found
     */
    public Integer getApiTypeId(Integer apiId) {
        String method = className + ".getApiTypeId";
        Logger.logMessage("In method " + method, Logger.LEVEL.DEBUG);

        Integer apiTypeId = -1;

        try {
            apiTypeId = new DynamicSQL("data.thirdPartyAPis.getApiTypeId")
                    .addCount(1, 1)
                    .addConditionalClause(2, "WHERE", "ThirdPartyApiId", "=", apiId)
                    .getSingleIntField(new DataManager());
        }
        catch (Exception ex) {
            Logger.logMessage("Error getting api type id in " + method, Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return apiTypeId;
    }



}
