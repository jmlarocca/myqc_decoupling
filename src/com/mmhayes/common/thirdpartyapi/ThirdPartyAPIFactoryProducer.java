package com.mmhayes.common.thirdpartyapi;

import com.mmhayes.common.thirdpartyapi.models.ThirdPartyAPIFactory;
import com.mmhayes.common.thirdpartyapi.sms.SmsFactory;

public class ThirdPartyAPIFactoryProducer {

    /**
     *
     * Gets a factory for a third party api based on the function passed in.
     * Not strictly necessary, but will help maintain uniformity across
     * different third party api implementations.
     *
     * Functions include:
     * "SMS" for an sms factory, e.g. when using Twilio
     *
     * @param function the function of the api
     * @return the factory for the third party api based on the function
     */
    public static ThirdPartyAPIFactory getThirdPartyApiFactory(String function) {

        ThirdPartyAPIFactory factory = null;

        switch(function.toUpperCase()) {
            case "SMS":
                factory = new SmsFactory();
                break;
            default:
                break;
        }

        return factory;
    }

}
