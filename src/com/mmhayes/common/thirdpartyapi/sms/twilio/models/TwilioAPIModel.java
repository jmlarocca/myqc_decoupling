package com.mmhayes.common.thirdpartyapi.sms.twilio.models;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.thirdpartyapi.sms.models.SmsAPIModel;
import com.mmhayes.common.utils.Logger;

import com.mmhayes.common.utils.StringFunctions;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.Account;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

import java.util.HashMap;

import com.twilio.http.HttpClient;
import com.twilio.http.NetworkHttpClient;
import com.twilio.http.TwilioRestClient;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;


public class TwilioAPIModel extends SmsAPIModel {

    private final String className = "TwilioAPIModel";

    public TwilioAPIModel(Integer apiId) {
        super(apiId);
    }

    /**
     * Checks whether the required values are set
     * Required for this are: content, authId, and authToken
     *
     * @return true if the required fields are set, false otherwise
     */
    public Boolean checkRequiredValuesSet() {

        String method = className + ".checkRequiredValuesSet";
        Logger.logMessage("In method " + method, Logger.LEVEL.DEBUG);

        // default whether the required values are set to be true
        Boolean requiredValuesSet = true;

        try {

            if (getSendToPhoneNumber() == null || getSendToPhoneNumber().isEmpty()) {
                requiredValuesSet = false;
                Logger.logMessage("Send to phone number not set in " + method, Logger.LEVEL.WARNING);
            }

            if (getContent() == null || getContent().isEmpty()) {
                requiredValuesSet = false;
                Logger.logMessage("Content not set in " + method, Logger.LEVEL.WARNING);
            }

            if (getAuthId() == null || getAuthId().isEmpty()) {
                requiredValuesSet = false;
                Logger.logMessage("Auth Id not set in " + method, Logger.LEVEL.WARNING);
            }

            if (getAuthToken() == null || getAuthToken().isEmpty()) {
                requiredValuesSet = false;
                Logger.logMessage("Auth token not set in " + method, Logger.LEVEL.WARNING);
            }

            if (getSendFromPhoneNumber() == null || getSendFromPhoneNumber().isEmpty()) {
                requiredValuesSet = false;
                Logger.logMessage("Send from phone number not set in " + method, Logger.LEVEL.WARNING);
            }

        }
        catch (Exception ex) {
            Logger.logMessage("Error checking required values are set in " + method, Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        Logger.logMessage("All values checked in " + method, Logger.LEVEL.DEBUG);

        return requiredValuesSet;
    }

    /**
     * Handles the api call to Twilio -
     * First initializes the Twilio API, then creates a message and sends it
     */
    public void handleApiCall() {

        String method = className + ".handleApiCall";
        Logger.logMessage("In method " + method, Logger.LEVEL.DEBUG);

        try {

            Logger.logMessage("Checking whether required values set in " + method, Logger.LEVEL.DEBUG);
            if (checkRequiredValuesSet()) {

                Logger.logMessage("Authenticating API details with the Twilio API in " + method, Logger.LEVEL.DEBUG);

                DataManager dm = new DataManager();
                ClientConfig clientConfig = new ClientConfig();
                // SET TIMEOUTS
                clientConfig.property(ClientProperties.READ_TIMEOUT, CommonAPI.getRestReadTimeout());
                clientConfig.property(ClientProperties.CONNECT_TIMEOUT, CommonAPI.getRestConnectionTimeout());
                HashMap results = (HashMap) dm.parameterizedExecuteQuery("data.globalSettings.getProxySettings", new Object[]{}, true).get(0);
                String password = StringFunctions.decodePassword((String) results.get("PROXYPASSWORD"));
                String username = (String)results.get("PROXYUSERNAME");
                String uri = (String)results.get("PROXYURI");


                // Authenticates the set api with the Twilio API
                String auth = getAuthId();
                String token = getAuthToken();
                Logger.logMessage("Attempting connection to Twilio: " + auth + " " + token, Logger.LEVEL.IMPORTANT);
                Twilio.init(auth, token);

                if(uri != null && uri.length() > 0){
                    String[] parts = uri.split(":");
                    String host = parts[parts.length-2];
                    int port = 0;//443?
                    try{
                         port = new Integer(parts[parts.length-1]);
                    }catch(NumberFormatException e){
                        Logger.logException(e);
                    }

                    RequestConfig config = RequestConfig.custom()
                            .setConnectTimeout(10000)
                            .setSocketTimeout(30500)
                            .build();

                    PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
                    connectionManager.setDefaultMaxPerRoute(10);
                    connectionManager.setMaxTotal(10*2);

                    HttpHost proxy = new HttpHost(host, port);

                    HttpClientBuilder clientBuilder = HttpClientBuilder.create();
                    clientBuilder
                            .setConnectionManager(connectionManager)
                            .setProxy(proxy)
                            .setDefaultRequestConfig(config);

                    // Inclusion of Twilio headers and build() is executed under this constructor
                    HttpClient httpClient= new NetworkHttpClient(clientBuilder);

                    //get client
                    TwilioRestClient.Builder builder = new TwilioRestClient.Builder(getAuthId(), getAuthToken());

//                    builder.accountSid(auth);

                    Twilio.setRestClient(builder.httpClient(httpClient).build());

//                    Account account = Account.creator().create();
//                    Logger.logMessage("Account SID:" + account.getSid() + " " + account.getAuthToken(), Logger.LEVEL.IMPORTANT);

                }
                Logger.logMessage("Creating a message in " + method, Logger.LEVEL.DEBUG);
                Logger.logMessage("Message from "+ getSendFromPhoneNumber() + " to " + getSendToPhoneNumber(), Logger.LEVEL.IMPORTANT);
                // Creates a message with the set information, and sends it
                Message message = Message.creator(new PhoneNumber(getSendToPhoneNumber()),
                        new PhoneNumber(getSendFromPhoneNumber()),
                        getContent()).create();

                Logger.logMessage("Message sent in " + method + "; message sid is " + message.getSid(), Logger.LEVEL.DEBUG);
            }
        }
        catch (Exception ex) {
            Logger.logMessage("Error handling api call in " + method, Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }
}
