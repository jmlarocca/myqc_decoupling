package com.mmhayes.common.thirdpartyapi.sms.models;

import com.mmhayes.common.thirdpartyapi.models.ThirdPartyAPIModel;

public abstract class SmsAPIModel extends ThirdPartyAPIModel {

    private String sendToPhoneNumber = "";
    private String content = "";

    public SmsAPIModel() {
        super();
    }

    public SmsAPIModel(Integer apiId) {
        super(apiId);
    }

    /**
     * Sets the destination phone number for any api request made
     *
     * @param sendToPhoneNumber the new destination phone number for any api request made
     */
    public void setSendToPhoneNumber(String sendToPhoneNumber) {
        this.sendToPhoneNumber = sendToPhoneNumber;
    }

    /**
     * Gets the destination phone number for any api request made
     *
     * @return the destination phone number for any api request made
     */
    public String getSendToPhoneNumber() {
        return sendToPhoneNumber;
    }

    /**
     * Sets the content to be sent in the api request
     *
     * @param content the new content to be sent in the api request
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Gets the content to be sent in the api request
     *
     * @return the content to be sent in the api request
     */
    public String getContent() {
        return content;
    }

}
