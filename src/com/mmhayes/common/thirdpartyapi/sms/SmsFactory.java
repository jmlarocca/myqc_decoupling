package com.mmhayes.common.thirdpartyapi.sms;

import com.mmhayes.common.thirdpartyapi.models.ThirdPartyAPIModel;
import com.mmhayes.common.thirdpartyapi.models.ThirdPartyAPIFactory;
import com.mmhayes.common.thirdpartyapi.sms.models.SmsAPIModel;
import com.mmhayes.common.thirdpartyapi.sms.twilio.models.TwilioAPIModel;

public class SmsFactory extends ThirdPartyAPIFactory {

    public SmsFactory() {

    }

    public SmsAPIModel getThirdPartyApi(Integer apiId) {

        SmsAPIModel thirdPartyApi = null;

        switch (getApiTypeId(apiId)) {

            case 1:
                thirdPartyApi = new TwilioAPIModel(apiId);
                break;
            default:
                break;

        }

        return thirdPartyApi;
    }
}
