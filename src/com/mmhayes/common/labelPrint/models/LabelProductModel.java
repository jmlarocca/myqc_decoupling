package com.mmhayes.common.labelPrint.models;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import java.math.BigDecimal;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
*/
public class LabelProductModel {
    Integer id = null;
    String name = "";
    String description = "";
    BigDecimal price = null;
    String code = "";

    //default constructor
    public LabelProductModel() {
    }

    //Constructor- takes in a Hashmap
    public LabelProductModel(HashMap ProductHM) {
        setModelProperties(ProductHM);
    }

    //setter for all of this model's properties
    public LabelProductModel setModelProperties(HashMap modelDetailHM) {
        setID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
        setDescription(CommonAPI.convertModelDetailToString(modelDetailHM.get("DESCRIPTION")));
        setPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("PRICE")));
        setCode(CommonAPI.convertModelDetailToString(modelDetailHM.get("PLUCODE")));

        return this;
    }

    //getter
    public Integer getID() {
        return this.id;
    }

    //setter
    public void setID(Integer id) {
        this.id = id;
    }

    //getter
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return this.name;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    //getter
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getDescription() {
        return this.description;
    }

    //setter
    public void setDescription(String description) {
        this.description = description;
    }

    //getter
    public BigDecimal getPrice() {
        return this.price;
    }

    //setter
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    //getter
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getCode() {
        return this.code;
    }

    //setter
    public void setCode(String code) {
        this.code = code;
    }

}
