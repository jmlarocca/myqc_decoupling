package com.mmhayes.common.labelPrint.models;

//mmhayes dependencies
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.labelPrint.collections.LabelProductCollection;
import com.mmhayes.common.utils.Logger;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
*/
public class LabelPrintJobDetailModel {
    Integer id = null;
    LabelProductModel product = null;
    Integer quantity = null;
    String status = "N/A";
    private Integer productID = null;
    private Integer expirePrintJobMinutes = null;
    private static DataManager dm = new DataManager();
    private String authenticatedMacAddress = null;

    //default Constructor
    public LabelPrintJobDetailModel() {
    }

    //constructor- takes in a Hashmap
    public LabelPrintJobDetailModel(HashMap labelPrintJobDetailHM, HttpServletRequest request) throws Exception {
        setModelProperties(labelPrintJobDetailHM, request);
    }

    //constructor - takes in LabelPrintId, returns one LabelPrintJobModel
    public LabelPrintJobDetailModel(Integer labelPrintJobDetailId, HttpServletRequest request) throws Exception {

        //determine the authenticated Mac Address from the request
        setAuthenticatedMacAddress(CommonAuthResource.getMacAddressFromAuthHeader(request));
        CommonAPI.checkIsNullOrEmptyObject(labelPrintJobDetailId, "Label Print Job Detail Id cannot be found");   //If the labelPrintJobDetailId is null, reply with a Missing Data Exception

        setId(labelPrintJobDetailId);

        //get all models in an array list
        ArrayList<HashMap> labelPrintJobDetailList = dm.parameterizedExecuteQuery("data.labelPrinting.getOneLabelPrintJobDetail",
                new Object[]{
                        labelPrintJobDetailId
                },
                true
        );

        //if the list if null or empty, throw an exception
        CommonAPI.checkIsNullOrEmptyList(labelPrintJobDetailList, "Label Print Job Detail List is empty");
        HashMap modelDetailHM = labelPrintJobDetailList.get(0);
        CommonAPI.checkIsNullOrEmptyObject(modelDetailHM.get("ID"), "Label Print Job Detail Id List is empty");
        setModelProperties(modelDetailHM, request);
    }

    //setter for all of this model's properties
    public LabelPrintJobDetailModel setModelProperties(HashMap modelDetailHM, HttpServletRequest request) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setQuantity(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("QUANTITY")));
        setStatus(CommonAPI.convertModelDetailToString(modelDetailHM.get("STATUS")));
        setProductID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PRODUCTID")));
        setExpirePrintJobMinutes(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("EXPIREJOBMINS")));

        CommonAPI.checkIsNullOrEmptyObject(getProductID(), "Product Id cannot be found"); //Fetch detail records using Product ID, if not found, throw error
        //we are only expecting one product
        setProduct(new LabelProductCollection(getProductID(), request).getCollection().get(0));

        return this;
    }

    //update the Print Status on one Label Print Job Detail
    public void updateLabelPrintJobDetailStatus(HttpServletRequest request) throws Exception {

        Logger.logMessage("Entering method: LabelPrintJobDetailModel.updateLabelPrintJobDetailStatus", CommonAPI.getPosAPILogFileName(), Logger.LEVEL.DEBUG);
        //determine the authenticated Mac Address from the request
        setAuthenticatedMacAddress(CommonAuthResource.getMacAddressFromAuthHeader(request));
        CommonAPI.checkIsNullOrEmptyObject(this.getStatusId(), "Print Status cannot be found."); //Check for missing Print Job
        CommonAPI.checkIsNullOrEmptyObject(this.getId(), "Label Print Job Detail Id cannot be found."); //Check for a null labelPrintJobDetailId parameter

        //Update Label Print Job Detail, send Print Status ID and the detail ID
        Integer result = dm.parameterizedExecuteNonQuery("data.labelPrinting.updateLabelPrintJobDetail",
                new Object[]{
                        this.getStatusId(),
                        this.getId()
                }, CommonAPI.getPosAPILogFileName()
        );

        if (result < 1) {
            throw new MissingDataException("Could not Update Label Print Job Detail record");
        }
    }

    //getter
    public Integer getId() {
        return this.id;
    }

    //setter
    public void setId(Integer id) {
        this.id = id;
    }

    //getter
    public Object getProduct() {
        return this.product;
    }

    //setter
    public void setProduct(LabelProductModel product) {
        this.product = product;
    }

    //getter
    public Integer getQuantity() {
        return this.quantity;
    }

    //setter
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    //getter
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getStatus() {
        return this.status;
    }

    //setter
    public void setStatus(String status) {
        this.status = status;
    }

    private Integer getStatusId() {
        //Return the Integer value
        return LabelPrintAPIHelper.getPrintStatusId(this.getStatus());
    }

    //getter
    private Integer getProductID() {
        return this.productID;
    }

    //setter
    private void setProductID(Integer productID) {
        this.productID = productID;
    }

    //getter
    private String getAuthenticatedMacAddress() {
        return this.authenticatedMacAddress;
    }

    //setter
    private void setAuthenticatedMacAddress(String authenticatedMacAddress) {
        this.authenticatedMacAddress = authenticatedMacAddress;
    }

    //getter
    private Integer getExpirePrintJobMinutes() {
        return this.expirePrintJobMinutes;
    }

    //setter
    private void setExpirePrintJobMinutes(Integer expirePrintJobMinutes) {
        this.expirePrintJobMinutes = expirePrintJobMinutes;
    }

}
