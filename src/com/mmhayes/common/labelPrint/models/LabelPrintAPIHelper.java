package com.mmhayes.common.labelPrint.models;

//mmhayes dependencies
import com.mmhayes.common.dataaccess.*;
import com.mmhayes.common.utils.Logger;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-02-28 16:21:56 -0500 (Tue, 28 Feb 2017) $: Date of last commit
 $Rev: 3637 $: Revision of last commit
*/
public class LabelPrintAPIHelper {
    private static DataManager dm = new DataManager();

    public static final Integer PRINT_STATUS_QUEUED = 1;
    public static final Integer PRINT_STATUS_WAITING = 2;
    public static final Integer PRINT_STATUS_PRINTED = 3;
    public static final Integer PRINT_STATUS_SENT = 4;
    public static final Integer PRINT_STATUS_DONE = 5;
    public static final Integer PRINT_STATUS_ERROR = 6;
    public static final Integer PRINT_STATUS_IN_PROGRESS = 7;
    public static final Integer PRINT_STATUS_FAILED_LOCAL_PRINT = 8;

    //accept a Print Status string, return the integer value
    public static Integer getPrintStatusId(String printStatusString) {
        Integer iPrintStatus = null;

        if (printStatusString != null && !printStatusString.isEmpty()) {
            switch (printStatusString.toUpperCase()) {
                case "QUEUED":
                    iPrintStatus = 1;
                    break;
                case "WAITING":
                    iPrintStatus = 2;
                    break;
                case "PRINTED":
                    iPrintStatus = 3;
                    break;
                case "SENT":
                    iPrintStatus = 4;
                    break;
                case "DONE":
                    iPrintStatus = 5;
                    break;
                case "ERROR":
                    iPrintStatus = 6;
                    break;
                case "IN PROGRESS":
                    iPrintStatus = 7;
                    break;
                case "FAILED LOCAL PRINT":
                    iPrintStatus = 8;
                    break;
            }
        }

        return iPrintStatus;
    }

    //accept a Print Status integer, return the string value
    public static String getPrintStatusName(Integer iPrintStatus) {
        String PrintStatusString = "";

        if (iPrintStatus != null && !iPrintStatus.toString().isEmpty()) {
            switch (iPrintStatus) {
                case 1:
                    PrintStatusString = "Queued";
                    break;
                case 2:
                    PrintStatusString = "Waiting";
                    break;
                case 3:
                    PrintStatusString = "Printed";
                    break;
                case 4:
                    PrintStatusString = "Sent";
                    break;
                case 5:
                    PrintStatusString = "Done";
                    break;
                case 6:
                    PrintStatusString = "Error";
                    break;
                case 7:
                    PrintStatusString = "In Progress";
                    break;
                case 8:
                    PrintStatusString = "Failed Local Print";
                    break;
            }
        }

        return PrintStatusString;
    }

    //takes in LabelPrintId, returns one LabelPrintJobModel
    public static boolean doesPrintJobExist(LabelPrintJobModel labelPrintJob, HttpServletRequest request) throws Exception {

        String macAddress = CommonAuthResource.getMacAddressFromAuthHeader(request);   //throw InvalidAuthException if no authenticatedMacAddress is found
        CommonAPI.checkIsNullOrEmptyObject(labelPrintJob.getId(), "");  //If the labelPrintJobID is null, reply with a Missing Data Exception

        //get all models in an array list
        ArrayList<HashMap> labelPrintJobList = dm.parameterizedExecuteQuery("data.labelPrinting.getOneLabelPrintJob",
                new Object[]{
                        labelPrintJob.getPrintControllerId(),
                        labelPrintJob.getId(),
                },
                CommonAPI.getPosAPILogFileName(), true
        );

        if (labelPrintJobList == null || labelPrintJobList.isEmpty()) {
            Logger.logMessage("ERROR: Could not the Label Print Job id in table doesPrintJobExist", CommonAPI.getPosAPILogFileName(), Logger.LEVEL.TRACE);
            return false;
        } else {
            HashMap modelDetailHM = labelPrintJobList.get(0);
            if (modelDetailHM.get("ID") == null || modelDetailHM.get("ID").toString().isEmpty()) {
                Logger.logMessage("ERROR: Could not determine ID in doesPrintJobExist", CommonAPI.getPosAPILogFileName(), Logger.LEVEL.TRACE);
                return false;
            } else {
                return true;
            }
        }
    }

    //Fetch the Polling frequency set on the Print Controller.  Use the MAC address from the request object
    public static ArrayList<HashMap> getPrintControllerPollingFrequency(HttpServletRequest request) throws Exception {
        String macAddress = CommonAuthResource.getMacAddressFromAuthHeader(request);
        Integer pollingFrequency = null;

        //Fetch polling frequency depending on MAC Address
        //get all models in an array list
        ArrayList<HashMap> printControllerPollingFrequencyList = dm.parameterizedExecuteQuery("data.labelPrinting.getPrintControllerPollingFrequency",
                new Object[]{
                        macAddress},
                CommonAPI.getPosAPILogFileName(), true
        );

        //Fetch the Polling frequency from the Printer Controller table.
        if (printControllerPollingFrequencyList == null || printControllerPollingFrequencyList.isEmpty()) {
            Logger.logMessage("ERROR: Could not find any records getPrintControllerPollingFrequency()", Logger.LEVEL.DEBUG);
        /*} else {
            for (HashMap printControllerPollingFrequencyHM : printControllerPollingFrequencyList) {
                CommonAPI.checkIsNullOrEmptyObject(printControllerPollingFrequencyHM.get("POLLFREQUENCYSECS"), "Polling Frequency cannot be found");
                pollingFrequency = CommonAPI.convertModelDetailToInteger(printControllerPollingFrequencyHM.get("POLLFREQUENCYSECS"));   //assign the polling frequency
            }*/
        }

        return printControllerPollingFrequencyList;
    }

    //Label Print Job Collection
    //first check if there are any Print Jobs to expire. Then, expire them
    public static void checkForAndExpirePrintJobs(HttpServletRequest request) throws Exception {
        if (LabelPrintAPIHelper.hasExpiredPrintJobs()) {

            //expire the Label Print Job Details, then the Label Print Job
            LabelPrintAPIHelper.expirePrintJobDetails();
            LabelPrintAPIHelper.expirePrintJobs();
        }
    }

    //check if there are any Label Print Jobs to expire.  Log the IDs that will be expired
    public static boolean hasExpiredPrintJobs() throws Exception {
        Logger.logMessage("Entering method: LabelPrintAPIHelper.hasExpiredPrintJobs", Logger.LEVEL.DEBUG);

        //get all models in an array list
        ArrayList<HashMap> jobsToExpireListHM = dm.parameterizedExecuteQuery("data.labelPrinting.getPrintJobsToExpire",
                new Object[]{
                        LabelPrintAPIHelper.PRINT_STATUS_QUEUED
                },
                CommonAPI.getPosAPILogFileName(), true
        );

        StringBuilder sb = new StringBuilder();

        //CommonAPI.checkIsNullOrEmptyList(jobsToExpireList, "");
        for (HashMap labelPrintJobHM : jobsToExpireListHM) {
            CommonAPI.checkIsNullOrEmptyObject(labelPrintJobHM.get("ID"), "");
            //create a new model and add to the collection
            Integer tempId = CommonAPI.convertModelDetailToInteger(labelPrintJobHM.get("ID"));
            sb.append(tempId + ",");
        }

        Logger.logMessage(String.format("INFO: Label Print Job IDs: %1s set to expire. hasExpiredPrintJobs()", sb.toString()), CommonAPI.getPosAPILogFileName(), Logger.LEVEL.DEBUG);

        return jobsToExpireListHM.size() > 0;
    }

    //Expire Label Print Jobs - Set the print status to "Error"
    private static void expirePrintJobs() throws Exception {
        Logger.logMessage("Entering method: LabelPrintAPIHelper.expirePrintJobs", Logger.LEVEL.DEBUG);

        //Update Label Print Job Details
        Integer updatedPrintJobRows = dm.parameterizedExecuteNonQuery("data.labelPrinting.expirePrintJobs",
                new Object[]{
                        LabelPrintAPIHelper.PRINT_STATUS_ERROR,
                        LabelPrintAPIHelper.PRINT_STATUS_QUEUED
                }, CommonAPI.getPosAPILogFileName()
        );

        Logger.logMessage("INFO: " + updatedPrintJobRows.toString() + " Print Jobs have been expired. expirePrintJobs()", Logger.LEVEL.DEBUG);

        if (updatedPrintJobRows < 1) {
            Logger.logMessage("No Print Jobs were updated. LabelPrintAPIHelper.expirePrintJobs()", Logger.LEVEL.DEBUG);
            throw new MissingDataException("Could not update Print Jobs");
        }
    }

    //Expire Label Print Jobs Details - Set the print status to "Error"
    private static void expirePrintJobDetails() throws Exception {
        Logger.logMessage("Entering method: LabelPrintAPIHelper.expirePrintJobDetails", Logger.LEVEL.DEBUG);

        //Update Label Print Job Details
        Integer updatedPrintJobDetailRows = dm.parameterizedExecuteNonQuery("data.labelPrinting.expirePrintJobDetails",
                new Object[]{
                        LabelPrintAPIHelper.PRINT_STATUS_ERROR,
                        LabelPrintAPIHelper.PRINT_STATUS_QUEUED
                }, CommonAPI.getPosAPILogFileName()
        );

        Logger.logMessage("INFO: " + updatedPrintJobDetailRows.toString() + " Print Job Details have been expired. expirePrintJobDetails()", Logger.LEVEL.TRACE);

        if (updatedPrintJobDetailRows < 1) {
            Logger.logMessage("No Print Job Details were updated. LabelPrintAPIHelper.expirePrintJobDetail()", Logger.LEVEL.DEBUG);
            throw new MissingDataException("Could not update Print Job Details");
        }

    }

}
