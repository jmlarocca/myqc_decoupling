package com.mmhayes.common.labelPrint.models;

//mmhayes dependencies
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.labelPrint.collections.LabelPrintJobDetailCollection;
import com.mmhayes.common.utils.Logger;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.api.CommonAuthResource;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
*/
public class LabelPrintJobModel {
    String logicalPrinterName = "N/A";
    String template = "N/A";
    String status = "N/A";
    private Integer id = null;
    private Integer printControllerId = null;
    private Integer expirePrintJobMinutes = null;
    private String authenticatedMacAddress = null;
    private static DataManager dm = new DataManager();

    private List<LabelPrintJobDetailModel> details = new ArrayList<>();

    //default constructor
    public LabelPrintJobModel() {
    }

    //Constructor- takes in a Hashmap
    public LabelPrintJobModel(HashMap labelPrintJobHM, HttpServletRequest request) throws Exception {

        setAuthenticatedMacAddress(CommonAuthResource.getMacAddressFromAuthHeader(request));   //Verify the header has Custom Header and MAC Address Header

        setModelProperties(labelPrintJobHM, request);
    }

    //Constructor - takes in print Controller Id, LabelPrintId, returns one LabelPrintJobModel
    public LabelPrintJobModel(Integer printControllerId, Integer labelPrintJobID, HttpServletRequest request) throws Exception {

        setAuthenticatedMacAddress(CommonAuthResource.getMacAddressFromAuthHeader(request));   //throw InvalidAuthException if no authenticatedMacAddress is found
        CommonAPI.checkIsNullOrEmptyObject(printControllerId, "");  //If the printerHostId is null, reply with a Missing Data Exception
        CommonAPI.checkIsNullOrEmptyObject(labelPrintJobID, "");  //If the labelPrintJobID is null, reply with a Missing Data Exception


        //get all models in an array list
        ArrayList<HashMap> labelPrintJobList = dm.parameterizedExecuteQuery("data.labelPrinting.getOneLabelPrintJob",
                new Object[]{
                        printControllerId,
                        labelPrintJobID,
                },
                true
        );

        //populate this collection from an array list of hashmaps
        CommonAPI.checkIsNullOrEmptyList(labelPrintJobList, "Label Print Job cannot be found");
        HashMap modelDetailHM = labelPrintJobList.get(0);
        CommonAPI.checkIsNullOrEmptyObject(modelDetailHM.get("ID"), "Label Print Job Id cannot be found");
        setModelProperties(modelDetailHM, request);
    }

    //Constructor- LabelPrintJobModel
    public LabelPrintJobModel(LabelPrintJobModel labelPrintJobModel, HttpServletRequest request) throws Exception {
        //determine the authenticated Mac Address from the request
        setAuthenticatedMacAddress(CommonAuthResource.getMacAddressFromAuthHeader(request));
        CommonAPI.checkIsNullOrEmptyObject(labelPrintJobModel, ""); //Check for a null labelPrintJobModel parameter
    }

    //setter for all of this model's properties
    public LabelPrintJobModel setModelProperties(HashMap modelDetailHM, HttpServletRequest request) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setLogicalPrinterName(CommonAPI.convertModelDetailToString(modelDetailHM.get("LOGICALNAME")));
        setPrintControllerId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PRINTCONTROLLERID")));
        setTemplate(CommonAPI.convertModelDetailToString(modelDetailHM.get("TEMPLATE")));
        setStatus(CommonAPI.convertModelDetailToString(modelDetailHM.get("STATUS")));
        setExpirePrintJobMinutes(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("EXPIREJOBMINS")));

        CommonAPI.checkIsNullOrEmptyObject(getId(),""); //Fetch detail records using LabelPrintJob ID
        setDetails(new LabelPrintJobDetailCollection(this.getId(), request).getCollection());

        return this;
    }

    //Update the Label Print Job record
    public void updateLabelPrintJobStatus(HttpServletRequest request) throws Exception {
        Logger.logMessage("Entering method: LabelPrintJobModel.updateLabelPrintJobStatus", Logger.LEVEL.DEBUG);

        //determine the authenticated Mac Address from the request
        setAuthenticatedMacAddress(CommonAuthResource.getMacAddressFromAuthHeader(request));
        CommonAPI.checkIsNullOrEmptyObject(this.getId(), "Label Print Job Status Id cannot be found."); //Check for a null labelPrintJobId parameter
        CommonAPI.checkIsNullOrEmptyObject(this.getStatusId(), "Print Job Status cannot be found."); //Check for missing Print Job
        CommonAPI.checkIsNullOrEmptyList(this.getDetails(), "Label Print Job Details cannot be found"); //Check for missing Print Job Details

        //if Print Job doesn't exist in the database, return error
        if (!LabelPrintAPIHelper.doesPrintJobExist(this, request)) {
            throw new MissingDataException("Print Job Id could not be found");
        }

        Logger.logMessage("About to update Label Print Job Details", Logger.LEVEL.DEBUG);
        //For each of the Print Job Details in the Details collection, update the Print Status
        for (LabelPrintJobDetailModel labelPrintJobDetail : this.getDetails()) {
            labelPrintJobDetail.updateLabelPrintJobDetailStatus(request);
        }


        Logger.logMessage("About to update Label Print Job", Logger.LEVEL.DEBUG);
        //Update the Label Print Job - Print Status
        Integer result = dm.parameterizedExecuteNonQuery("data.labelPrinting.updateLabelPrintJob",
                new Object[]{
                        this.getStatusId(), //Print Status Id
                        this.getId()
                }, CommonAPI.getPosAPILogFileName()
        );

        //If nothing was updated, assume an error occurred
        if (result < 1) {
            throw new MissingDataException("Could not Update Label Print Job Detail record");
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getLogicalPrinterName() {
        return logicalPrinterName;
    }

    public void setLogicalPrinterName(String logicalPrinterName) {
        this.logicalPrinterName = logicalPrinterName;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private Integer getStatusId() {
        return LabelPrintAPIHelper.getPrintStatusId(this.getStatus());
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    private String getAuthenticatedMacAddress() {
        return authenticatedMacAddress;
    }

    private void setAuthenticatedMacAddress(String authenticatedMacAddress) {
        this.authenticatedMacAddress = authenticatedMacAddress;
    }

    public List<LabelPrintJobDetailModel> getDetails() {
        return details;
    }

    public void setDetails(List<LabelPrintJobDetailModel> details) {
        this.details = details;
    }

    //getter
    private Integer getExpirePrintJobMinutes() {
        return this.expirePrintJobMinutes;
    }

    //setter
    private void setExpirePrintJobMinutes(Integer expirePrintJobMinutes) {
        this.expirePrintJobMinutes = expirePrintJobMinutes;
    }

    public Integer getPrintControllerId() {
        return printControllerId;
    }

    public void setPrintControllerId(Integer printControllerId) {
        this.printControllerId = printControllerId;
    }

}

