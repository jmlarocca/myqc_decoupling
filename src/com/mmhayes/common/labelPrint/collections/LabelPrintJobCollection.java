package com.mmhayes.common.labelPrint.collections;

//mmhayes dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.labelPrint.models.*;
import com.mmhayes.common.dataaccess.DataManager;

//qcpos dependencies
import com.mmhayes.common.utils.Logger;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-02-28 16:21:56 -0500 (Tue, 28 Feb 2017) $: Date of last commit
 $Rev: 3637 $: Revision of last commit
*/
//List of LabelPrintJobModels
public class LabelPrintJobCollection {
    List<LabelPrintJobModel> collection = new ArrayList<LabelPrintJobModel>();
    private static DataManager dm = new DataManager();
    private String authenticatedMacAddress = null;

    //Constructors
    //constructor - default
    public LabelPrintJobCollection() throws Exception {
    }

    //constructor - populates the collection
    public LabelPrintJobCollection(Integer printControllerId, HttpServletRequest request) throws Exception {

        //Verify the header has Custom Header and MAC Address Header
        setAuthenticatedMacAddress(CommonAuthResource.getMacAddressFromAuthHeader(request));

        //populate this collection with models
        populateCollection(printControllerId, request);
    }

    //constructor - populates the collection
    //Used by PUT for List<LabelPrintJobModel>
    public LabelPrintJobCollection(HttpServletRequest request, List<LabelPrintJobModel> listLabelPrintJobModel) throws Exception {

        setAuthenticatedMacAddress(CommonAuthResource.getMacAddressFromAuthHeader(request));   //Verify the header has Custom Header and MAC Address Header
        CommonAPI.checkIsNullOrEmptyObject(listLabelPrintJobModel, "");   //If the List of Print Jobs is null, reply with a Missing Data Exception

        //set the collection to the Print Job Model List that is passed in.  This is the only list we want to updated
        this.setCollection(listLabelPrintJobModel);
    }

    //Instance Methods
    //populates this collection with models
    private LabelPrintJobCollection populateCollection(Integer printControllerId, HttpServletRequest request) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> LabelPrintJobList = dm.parameterizedExecuteQuery("data.labelPrinting.getLabelPrintJobs",
                new Object[]{
                        printControllerId,
                        LabelPrintAPIHelper.PRINT_STATUS_QUEUED
                },
                CommonAPI.getPosAPILogFileName(), true
        );

        //product list is null or empty, return empty list eglundin - 07-26-2016
        for (HashMap labelPrintJobHM : LabelPrintJobList) {
            CommonAPI.checkIsNullOrEmptyObject(labelPrintJobHM.get("ID"), "Label Print Job Id cannot be found.");
            //create a new model and add to the collection
            getCollection().add(new LabelPrintJobModel(labelPrintJobHM, request));
        }

        return this;
    }

    //Update Print Statuses in Label Print Job Collection
    public void updateLabelPrintJobStatusCollection(HttpServletRequest request) throws Exception {
        Logger.logMessage("Entering method: LabelPrintJobCollection.updateLabelPrintJobStatusCollection");

        setAuthenticatedMacAddress(CommonAuthResource.getMacAddressFromAuthHeader(request));   //Verify the header has Custom Header and MAC Address Header
        CommonAPI.checkIsNullOrEmptyList(this.getCollection(), "");

        //For each of the Print Jobs in the collection, update the Print Status on the Print Job
        for (LabelPrintJobModel labelPrintJob : this.getCollection()) {
            labelPrintJob.updateLabelPrintJobStatus(request);
        }
    }

    //re-fetch the Label Print Jobs from the database, only grab the jobs from the current list
    public List<LabelPrintJobModel> refreshCollection(HttpServletRequest request) throws Exception {
        //check for a null or empty list
        CommonAPI.checkIsNullOrEmptyList(this.getCollection(), "");
        List<LabelPrintJobModel> labelPrintJobListNew = new ArrayList<LabelPrintJobModel>();

        //re-fetch list depending on the Print Jobs in the current list
        for (LabelPrintJobModel labelPrintJob : this.getCollection()) {

            //fetch the Label Print Job Model from the DB
            LabelPrintJobModel newLabelPrintJobModel = new LabelPrintJobModel(labelPrintJob.getPrintControllerId(), labelPrintJob.getId(), request);
            labelPrintJobListNew.add(newLabelPrintJobModel);  //add the new Label Print Job to the new list
        }

        //return the updated list
        return labelPrintJobListNew;
    }

    //getter
    public List<LabelPrintJobModel> getCollection() {
        return this.collection;
    }

    //setter
    public void setCollection(List<LabelPrintJobModel> collection) {
        this.collection = collection;
    }

    //getter
    private String getAuthenticatedMacAddress() {
        return this.authenticatedMacAddress;
    }

    //setter
    private void setAuthenticatedMacAddress(String authenticatedMacAddress) {
        this.authenticatedMacAddress = authenticatedMacAddress;
    }
}




