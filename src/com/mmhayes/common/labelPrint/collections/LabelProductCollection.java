package com.mmhayes.common.labelPrint.collections;

//mmhayes dependencies
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.labelPrint.models.LabelProductModel;

//other dependencies
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-02-16 11:12:54 -0500 (Thu, 16 Feb 2017) $: Date of last commit
 $Rev: 3553 $: Revision of last commit
*/
public class LabelProductCollection {
    List<LabelProductModel> collection = new ArrayList<LabelProductModel>();
    private static DataManager dm = new DataManager();
    private String authenticatedMacAddress = null;

    //default constructor - used by the API
    public LabelProductCollection() {
    }

    //constructor - populates the collection
    public LabelProductCollection(Integer productID, HttpServletRequest request) throws Exception {

        setAuthenticatedMacAddress(CommonAuthResource.getMacAddressFromAuthHeader(request));   //Verify the header has Custom Header and MAC Address Header
        CommonAPI.checkIsNullOrEmptyObject(productID, "Product Id could not be found");   //If the productID parameter is null, reply with a Missing Data Exception

        //populate this collection with models
        populateCollection(productID, request);
    }

    //populates this collection with models
    private LabelProductCollection populateCollection(Integer productID, HttpServletRequest request) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> ProductList = dm.parameterizedExecuteQuery("data.labelPrinting.getProductDetail",
                new Object[]{
                        productID
                },
                CommonAPI.getPosAPILogFileName(), true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(ProductList, "Product List is empty");
        for (HashMap productHM : ProductList) {
            CommonAPI.checkIsNullOrEmptyObject(productHM.get("ID"), "Product Id could not be found");
            //create a new model and add to the collection
            getCollection().add(new LabelProductModel(productHM));
        }

        return this;
    }

    //getter
    public List<LabelProductModel> getCollection() {
        return this.collection;
    }

    //getter
    private String getAuthenticatedMacAddress() {
        return this.authenticatedMacAddress;
    }

    //setter
    private void setAuthenticatedMacAddress(String authenticatedMacAddress) {
        this.authenticatedMacAddress = authenticatedMacAddress;
    }
}
