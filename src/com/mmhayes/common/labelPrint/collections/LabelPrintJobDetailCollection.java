package com.mmhayes.common.labelPrint.collections;

//mmhayes dependencies
import com.mmhayes.common.dataaccess.DataManager;

//API dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.labelPrint.models.*;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-02-16 11:12:54 -0500 (Thu, 16 Feb 2017) $: Date of last commit
 $Rev: 3553 $: Revision of last commit
*/
public class LabelPrintJobDetailCollection {
    List<LabelPrintJobDetailModel> collection = new ArrayList<LabelPrintJobDetailModel>();
    private static DataManager dm = new DataManager();
    private String authenticatedMacAddress = null;

    //constructor - default
    public LabelPrintJobDetailCollection() {
    }

    //constructor - populates the collection
    public LabelPrintJobDetailCollection(Integer labelPrinterJobID, HttpServletRequest request) throws Exception {

        //Verify the header has Custom Header and MAC Address Header
        setAuthenticatedMacAddress(CommonAuthResource.getMacAddressFromAuthHeader(request));
        CommonAPI.checkIsNullOrEmptyObject(labelPrinterJobID, "Label Printer Job ID Cannot be found");   //If the labelPrinterJobID is null, reply with a Missing Data Exception

        //populate this collection with models
        populateCollection(labelPrinterJobID, request);
    }

    //populates this collection with models
    private LabelPrintJobDetailCollection populateCollection(Integer labelPrinterJobID, HttpServletRequest request) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> LabelPrintJobDetailList = dm.parameterizedExecuteQuery("data.labelPrinting.getLabelPrintJobDetails",
                new Object[]{
                        labelPrinterJobID
                },
                true
        );

        CommonAPI.checkIsNullOrEmptyList(LabelPrintJobDetailList, "");
        for (HashMap labelPrintJobDetailHM : LabelPrintJobDetailList) {
            CommonAPI.checkIsNullOrEmptyObject(labelPrintJobDetailHM.get("ID"),"");
            //create a new model and add to the collection
            getCollection().add(new LabelPrintJobDetailModel(labelPrintJobDetailHM, request));
        }

        return this;
    }

    //getter
    public List<LabelPrintJobDetailModel> getCollection() {
        return this.collection;
    }

    //getter
    private String getAuthenticatedMacAddress() {
        return this.authenticatedMacAddress;
    }

    //setter
    private void setAuthenticatedMacAddress(String authenticatedMacAddress) {
        this.authenticatedMacAddress = authenticatedMacAddress;
    }
}
