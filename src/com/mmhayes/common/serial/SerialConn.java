package com.mmhayes.common.serial;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-05-24 12:52:41 -0400 (Mon, 24 May 2021) $: Date of last commit
    $Rev: 14019 $: Revision of last commit
    Notes: Encapsulates a connection to a serial port.
*/

import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import javax.comm.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Objects;
import java.util.TooManyListenersException;

public class SerialConn {

    private String commPort;
    private int baudrate;
    private int databits;
    private int stopbits;
    private int parity;
    private SerialPort serialPort;

    /**
     * <p>Builds and returns a {@link SerialConn} built from the serial connection {@link String}.</p>
     *
     * @param connString The serial connection {@link String} to extract the connection parameters from.
     * @return The newly constructed {@link SerialConn} instance.
     * throws IllegalArgumentException
     */
    public static SerialConn buildSerialConnFromConnString (String connString) throws IllegalArgumentException {

        String[] connParams = extractSerialConnParamsFromConnString(connString);

        if (DataFunctions.isEmptyGenericArr(connParams)) {
            throw new IllegalArgumentException("No connection parameters were found in SerialConn buildSerialConnFromConnString!");
        }

        int baudrate;
        int databits;
        int stopbits;
        int parity;
        switch (connParams.length) {
            case 1:
                return new SerialConn(connParams[0]);
            case 2:
                baudrate = Integer.parseInt(connParams[1]);
                return new SerialConn(connParams[0], baudrate);
            case 3:
                baudrate = Integer.parseInt(connParams[1]);
                databits = Integer.parseInt(connParams[2]);
                return new SerialConn(connParams[0], baudrate, databits);
            case 4:
                baudrate = Integer.parseInt(connParams[1]);
                databits = Integer.parseInt(connParams[2]);
                stopbits = Integer.parseInt(connParams[3]);
                return new SerialConn(connParams[0], baudrate, databits, stopbits);
            case 5:
                baudrate = Integer.parseInt(connParams[1]);
                databits = Integer.parseInt(connParams[2]);
                stopbits = Integer.parseInt(connParams[3]);
                parity = Integer.parseInt(connParams[4]);
                return new SerialConn(connParams[0], baudrate, databits, stopbits, parity);
        }

        return null;
    }

    /**
     * <p>Constructor for a {@link SerialConn}</p>
     *
     * @param commPort The comm port {@link String} that was setup for serial communication.
     * @param baudrate The rate at which data will be transferred over the serial connection.
     * @param databits The data bit format.
     * @param stopbits The stop bit format.
     * @param parity The parity to be used in the serial communication.
     */
    private SerialConn (String commPort, int baudrate, int databits, int stopbits, int parity) {
        this.parity = parity;
        this.stopbits = stopbits;
        this.databits = databits;
        this.baudrate = baudrate;
        this.commPort = commPort;
    }

    /**
     * <p>Constructor for a {@link SerialConn}</p>
     *
     * @param commPort The comm port {@link String} that was setup for serial communication.
     * @param baudrate The rate at which data will be transferred over the serial connection.
     * @param databits The data bit format.
     * @param stopbits The stop bit format.
     */
    private SerialConn (String commPort, int baudrate, int databits, int stopbits) {
        this.parity = SerialPort.PARITY_NONE;
        this.stopbits = stopbits;
        this.databits = databits;
        this.baudrate = baudrate;
        this.commPort = commPort;
    }

    /**
     * <p>Constructor for a {@link SerialConn}</p>
     *
     * @param commPort The comm port {@link String} that was setup for serial communication.
     * @param baudrate The rate at which data will be transferred over the serial connection.
     * @param databits The data bit format.
     */
    private SerialConn (String commPort, int baudrate, int databits) {
        this.parity = SerialPort.PARITY_NONE;
        this.stopbits = SerialPort.STOPBITS_1;
        this.databits = databits;
        this.baudrate = baudrate;
        this.commPort = commPort;
    }

    /**
     * <p>Constructor for a {@link SerialConn}</p>
     *
     * @param commPort The comm port {@link String} that was setup for serial communication.
     * @param baudrate The rate at which data will be transferred over the serial connection.
     */
    private SerialConn (String commPort, int baudrate) {
        this.parity = SerialPort.PARITY_NONE;
        this.stopbits = SerialPort.STOPBITS_1;
        this.databits = SerialPort.DATABITS_8;
        this.baudrate = baudrate;
        this.commPort = commPort;
    }

    /**
     * <p>Constructor for a {@link SerialConn}</p>
     *
     * @param commPort The comm port {@link String} that was setup for serial communication.
     */
    private SerialConn (String commPort) {
        this.parity = SerialPort.PARITY_NONE;
        this.stopbits = SerialPort.STOPBITS_1;
        this.databits = SerialPort.DATABITS_8;
        this.baudrate = 9600;
        this.commPort = commPort;
    }

    /**
     * <p>Extracts the serial communication connection parameters from the connection {@link String}.</p>
     *
     * @param connString The serial connection {@link String} to extract the connection parameters from.
     * @return A {@link String} array containing the connection parameters extracted from the connection {@link String}.
     * @throws IllegalArgumentException
     */
    private static String[] extractSerialConnParamsFromConnString (String connString) throws IllegalArgumentException {

        if (!StringFunctions.stringHasContent(connString)) {
            throw new IllegalArgumentException("The serial parameter connection String passed the SerialConn.extractSerialConnParamsFromConnString can't be null or empty!");
        }

        Logger.logMessage(String.format("Attempting to extract the serial connection parameters from the connection String %s, " +
                "in SerialConn.extractSerialConnParamsFromConnString!",
                Objects.toString(connString, "N/A")), Logger.LEVEL.TRACE);

        ArrayList<String> connParams = new ArrayList<>();
        String[] tokens = connString.split("\\s*,\\s*");
        if ((tokens.length < 2) && (!tokens[1].toUpperCase().startsWith("COM"))) {
            throw new IllegalArgumentException("Unable to detect a COM port for serial serial communication within the serial connection parameters String!");
        }
        connParams.add(tokens[1]);

        if (tokens.length > 2) {
            connParams.add(tokens[2]);
        }

        if (tokens.length > 3) {
            connParams.add(tokens[3]);
        }

        if (tokens.length > 4) {
            connParams.add(tokens[4]);
        }

        if (tokens.length > 5) {
            connParams.add(tokens[5]);
        }

        return connParams.toArray(new String[connParams.size()]);
    }

    /**
     * <p>Opens a connection to the {@link SerialPort}.</p>
     *
     * @throws NoSuchPortException
     * @throws PortInUseException
     * @throws UnsupportedCommOperationException
     */
    public synchronized void open () throws NoSuchPortException, PortInUseException, UnsupportedCommOperationException {
        CommPortIdentifier commPortIdentifier = CommPortIdentifier.getPortIdentifier(commPort);
        serialPort = ((SerialPort) commPortIdentifier.open("PortReadWrite", 2000));
        serialPort.setSerialPortParams(baudrate, databits, stopbits, parity);
        Logger.logMessage(String.format("A connection has been established to the serial port %s in SerialConn.open!",
                Objects.toString(serialPort.getName(), "N/A")), Logger.LEVEL.IMPORTANT);
    }

    /**
     * <p>Closes the connection to the {@link SerialPort}.</p>
     *
     */
    public void close () {
        if (serialPort != null) {
            Logger.logMessage(String.format("Now closing the connection to the serial port %s in SerialConn.close!",
                    Objects.toString(serialPort.getName(), "N/A")), Logger.LEVEL.IMPORTANT);
            serialPort.close();
        }
    }

    /**
     * <p>Gets a handle to the {@link SerialPort} {@link OutputStream}.</p>
     * 
     * @return The {@link OutputStream} handle to the {@link SerialPort}.
     * @throws NullPointerException
     * @throws IOException
     */
    public OutputStream getOutputStream () throws NullPointerException, IOException {
        if (serialPort == null) {
            throw new NullPointerException("No SerialPort instance has been created, unable to obtain a handle to the OutputStream.");
        }
        return serialPort.getOutputStream();
    }

    /**
     * <p>Gets a handle to the {@link SerialPort} {@link InputStream}.</p>
     *
     * @return The {@link InputStream} handle to the {@link SerialPort}.
     * @throws NullPointerException
     * @throws IOException
     */
    public InputStream getInputStream () throws NullPointerException, IOException {
        if (serialPort == null) {
            throw new NullPointerException("No SerialPort instance has been created, unable to obtain a handle to the InputStream.");
        }
        return serialPort.getInputStream();
    }

    /**
     * <p>Adds a serial event listener to the {@link SerialPort}.</p>
     *
     * @param serialListener A {@link SerialListener} which will listen for and handle serial events.
     * @throws NullPointerException
     * @throws IllegalArgumentException
     * @throws TooManyListenersException
     */
    public void addListener (SerialListener serialListener) throws NullPointerException, IllegalArgumentException, TooManyListenersException {
        if (serialPort == null) {
            throw new NullPointerException("No SerialPort instance has been created, unable to add a serial event listener.");
        }
        if (serialListener == null) {
            throw new IllegalArgumentException("The SerialListener instance passed to SerialConn.addListener can't be null or empty!");
        }
        serialPort.notifyOnOutputEmpty(true);
        serialPort.notifyOnDataAvailable(true);
        // TODO listen for additional events
        serialPort.addEventListener(serialListener);
    }

    /**
     * <p>Removes the serial event listener from the {@link SerialPort}.</p>
     *
     * @throws NullPointerException
     */
    public void removeListener () throws NullPointerException {
        if (serialPort == null) {
            throw new NullPointerException("No SerialPort instance has been created, unable to remove a serial event listener.");
        }
        serialPort.removeEventListener();
    }

    /**
     * <p>Sends an array of bytes to the serial device.</p>
     *
     * @param message The byte array to send to the serial device.
     * @throws IllegalArgumentException
     * @throws IOException
     */
    public void write (byte[] message) throws IllegalArgumentException, IOException {
        if (DataFunctions.isEmptyByteArr(message)) {
            throw new IllegalArgumentException("The message passed to SerialConn.write can't be null or empty!");
        }
        Logger.logMessage(String.format("Writing the command: %s to the serial port: %s",
                Objects.toString(DataFunctions.convertByteArrToStr(message, StandardCharsets.UTF_8, true), "N/A"),
                Objects.toString(serialPort.getName(), "N/A")), Logger.LEVEL.TRACE);
        getOutputStream().write(message, 0, message.length);
    }

    /**
     * <p>Getter for the serialPort field of the {@link SerialConn}.</p>
     *
     * @return The serialPort field of the {@link SerialConn}.
     */
    public SerialPort getSerialPort () {
        return serialPort;
    }

}