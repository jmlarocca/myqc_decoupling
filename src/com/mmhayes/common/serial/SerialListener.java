package com.mmhayes.common.serial;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-05-24 12:52:41 -0400 (Mon, 24 May 2021) $: Date of last commit
    $Rev: 14019 $: Revision of last commit
    Notes: Listens for and handles serial events.
*/

import com.mmhayes.common.printing.api.ESC_POS;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import javax.comm.SerialPortEvent;
import javax.comm.SerialPortEventListener;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class SerialListener implements SerialPortEventListener {

    private SerialConn serialConn;

    /**
     * <p>Constructor for a {@link SerialListener}.</p>
     *
     * @param serialConn The {@link SerialConn} this {@link SerialListener} instance will be listening to serial events for.
     */
    public SerialListener (SerialConn serialConn) {
        this.serialConn = serialConn;
    }

    @Override
    public void serialEvent (SerialPortEvent serialPortEvent) {
        switch (serialPortEvent.getEventType()) {
            case SerialPortEvent.BI:
            case SerialPortEvent.OE:
            case SerialPortEvent.FE:
            case SerialPortEvent.PE:
            case SerialPortEvent.CD:
            case SerialPortEvent.CTS:
            case SerialPortEvent.DSR:
            case SerialPortEvent.RI:
            case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                break;
            case SerialPortEvent.DATA_AVAILABLE:
                try {
                    InputStream inputStream = (serialConn != null && serialConn.getInputStream() != null ? serialConn.getInputStream() : null);
                    if (inputStream != null) {
                        byte[] readBuffer = new byte[8];
                        int numBytes = 0;
                        while (serialConn.getInputStream().available() > 0) {
                            numBytes = serialConn.getInputStream().read(readBuffer);
                        }
                        Logger.logMessage(String.format("Read %s%s from the serial device %s which contained the following, %s.",
                                Objects.toString(numBytes, "N/A"),
                                Objects.toString((numBytes == 1 ? " byte" : " bytes"), "N/A"),
                                Objects.toString(serialConn.getSerialPort().getName(), "N/A"),
                                Objects.toString(DataFunctions.convertByteArrToStr(readBuffer, StandardCharsets.UTF_8, true), "N/A")), Logger.LEVEL.TRACE);
                        handleDataAvailableEvent(readBuffer);
                    }
                    else {
                        Logger.logMessage(String.format("The serial port %s has an invalid InputStream in SerialListener.serialEvent!",
                                Objects.toString((((serialConn != null) && (serialConn.getSerialPort() != null) && (StringFunctions.stringHasContent(serialConn.getSerialPort().getName()))) ? serialConn.getSerialPort().getName() : "N/A"), "N/A")), Logger.LEVEL.TRACE);
                    }
                }
                catch (IOException e) {
                    Logger.logException(e);
                }
                break;
        }
    }

    public void handleDataAvailableEvent (byte[] data) {

        Logger.logMessage("ESC_POS.isASBStatus? "+ESC_POS.isASBStatus(data), Logger.LEVEL.IMPORTANT);
        // TODO MORE HANDLING

    }

}