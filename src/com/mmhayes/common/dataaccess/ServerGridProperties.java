package com.mmhayes.common.dataaccess;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmhayes.common.utils.Logger;
import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;

/*
 $Author: crking $: Author of last commit
 $Date: 2019-11-06 15:27:05 -0500 (Wed, 06 Nov 2019) $: Date of last commit
 $Rev: 9876 $: Revision of last commit
 Notes: Properties of a server-search grid (paginated)
 THE DATA INSIDE HERE SHOULD NOT BE TRUSTED!!! It may contain SQLInjection
*/
public class ServerGridProperties {

    // Object variables
    private int currentPage;
    private int totalPages;
    private int totalRows;
    private int startingRecordNumber;
    private int endingRecordNumber;
    private Long queryId;
    private Long userId;
    private String defaultSortColumn;
    private String defaultSortOrder;
    private HashMap postParams;
    private HashMap<String, Object> filterHM;
    private HashMap<String, String> customFilters;
    private HashMap<String, String> detailFilters;

    // Flag variables
    private boolean filterOwnerShip;
    private boolean filterAccountGroups;
    private boolean filterHyperfind;
    private boolean filterRevenueCenters;

    // Constructors
    public ServerGridProperties() {
        this(null);
    }
    public ServerGridProperties(HashMap postParams) {
        this.currentPage = 0;
        this.totalPages = 0;
        this.totalRows = 0;
        this.startingRecordNumber = 0;
        this.endingRecordNumber = 0;
        this.queryId = null;
        this.userId = null;
        this.defaultSortColumn = "1";
        this.defaultSortOrder = "ASC";
        this.postParams = (postParams == null ? new HashMap() : postParams);
        this.filterOwnerShip = false;
        this.filterAccountGroups = false;
        this.filterHyperfind = false;
        this.filterRevenueCenters = false;
        this.customFilters = new HashMap<String, String>();
        this.detailFilters = new HashMap<String, String>();
        if (postParams != null) {
            parsePostParams(); // Initialize the filter HashMap based on the postParams
        } else {
            this.filterHM = null;
        }
    }

    // Grouped setters
    public void setGridFlags(boolean filterOwnerShip, boolean filterAccountGroups, boolean filterHyperfind) {
        this.filterOwnerShip = filterOwnerShip;
        this.filterAccountGroups = filterAccountGroups;
        this.filterHyperfind = filterHyperfind;
    }
    public void setDefaultSort(String defaultSortColumn, String defaultSortOrder) {
        setDefaultSortColumn(defaultSortColumn);
        setDefaultSortOrder(defaultSortOrder);
    }

    // Getters and setters
    public int getCurrentPage() {
        return this.currentPage;
    }
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }
    public int getTotalPages() {
        return this.totalPages;
    }
    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
    public int getTotalRows() {
        return this.totalRows;
    }
    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }
    public int getStartingRecordNumber() {
        return this.startingRecordNumber;
    }
    public void setStartingRecordNumber(int startingRecordNumber) {
        this.startingRecordNumber = startingRecordNumber;
    }
    public int getEndingRecordNumber() {
        return this.endingRecordNumber;
    }
    public void setEndingRecordNumber(int endingRecordNumber) {
        this.endingRecordNumber = endingRecordNumber;
    }
    public Long getQueryId() {
        return this.queryId;
    }
    public void setQueryId(Long queryId) {
        this.queryId = queryId;
    }
    public void setQueryId(String queryIdString) {
        if (queryIdString == null || queryIdString.trim().length() < 1 || !commonMMHFunctions.tryParseLong(queryIdString.trim())) {
            this.queryId = null;
        } else {
            this.queryId = Long.parseLong(queryIdString.trim());
        }
    }
    public Long getUserId() {
        return this.userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setUserId(String userIdString) {
        if (userIdString == null || userIdString.trim().length() < 1 || !commonMMHFunctions.tryParseLong(userIdString.trim())) {
            this.userId = null;
        } else {
            this.userId = Long.parseLong(userIdString.trim());
        }
    }
    public String getDefaultSortColumn() {
        return this.defaultSortColumn;
    }
    public void setDefaultSortColumn(String defaultSortColumn) {
        if (defaultSortColumn != null && defaultSortColumn.trim().length() > 0) {
            this.defaultSortColumn = defaultSortColumn.trim();
        }
    }
    public String getDefaultSortOrder() {
        return this.defaultSortOrder;
    }
    public void setDefaultSortOrder(String defaultSortOrder) {
        this.defaultSortOrder = defaultSortOrder.trim();
    }
    public HashMap getPostParams() {
        return this.postParams;
    }
    public void setPostParams(HashMap postParams) {
        this.postParams = postParams;
        parsePostParams();
    }
    public boolean getFilterOwnerShip() {
        return this.filterOwnerShip;
    }
    public void setFilterOwnerShip(boolean filterOwnerShip) {
        this.filterOwnerShip = filterOwnerShip;
    }
    public boolean getFilterAccountGroups() {
        return this.filterAccountGroups;
    }
    public void setFilterAccountGroups(boolean filterAccountGroups) {
        this.filterAccountGroups = filterAccountGroups;
    }
    public boolean getFilterHyperfind() {
        return this.filterHyperfind;
    }
    public void setFilterHyperfind(boolean filterHyperfind) {
        this.filterHyperfind = filterHyperfind;
    }
    public boolean getFilterRevenueCenters() {
        return this.filterRevenueCenters;
    }
    public void setFilterRevenueCenters(boolean filterRevenueCenters) {
        this.filterRevenueCenters = filterRevenueCenters;
    }

    /**
     * The data within this hashmap should be considered DANGEROUS!
     * @return
     */
    public HashMap getFilterHM() {
        return this.filterHM;
    }
    public HashMap<String, String> getCustomFilters() {
        return this.customFilters;
    }

    /**
     * WARNING: Do NOT use any user input in the filterSQL param. If you do, you may be introducing an SQLI vulnerability.
     * @param filterKey
     * @param filterSQL
     */
    public void addCustomFilter(String filterKey, String filterSQL) {
        this.customFilters.put(filterKey, filterSQL);
    }
    public void deleteCustomFilter(String filterKey) {
        Iterator it = this.customFilters.keySet().iterator();
        while (it.hasNext()) {
            it.next();
            if (it.equals(filterKey)) {
                it.remove();
            }
        }
    }
    public HashMap<String, String> getDetailFilters() {
        return this.detailFilters;
    }
    public void addDetailFilter(String filterKey, String filterSQL) {
        this.detailFilters.put(filterKey, filterSQL);
    }
    public void deleteDetailFilter(String filterKey) {
        Iterator it = this.detailFilters.keySet().iterator();
        while (it.hasNext()) {
            it.next();
            if (it.equals(filterKey)) {
                it.remove();
            }
        }
    }

    // Transformation methods
    private void parsePostParams() {
        if (this.postParams.get("hyperFindFilterID") != null) {
            setQueryId(this.postParams.get("hyperFindFilterID").toString());
        }
        this.filterHM = new HashMap<String, Object>();
        if (this.postParams.get("filters") != null) {
            addFilterHM(this.postParams.get("filters").toString());
        } else {
            this.filterHM = null;
        }
        if (this.postParams.get("initSearchFilters") != null) {
            addFilterHM(this.postParams.get("initSearchFilters").toString(), "rules");
        }
    }

    /**
     * The data contained within this should be considered DANGEROUS!
     * @param filterJsonString
     */
    public void addFilterHM(String filterJsonString) { // Public method
        addFilterHM(filterJsonString, null);
    }

    /**
     * The data contained within this should be considered DANGEROUS!
     * @param filterJsonString
     */
    private void addFilterHM(String filterJsonString, String segmentKey) {
        if (filterJsonString != null && filterJsonString.length() > 0) {
            try {
                ObjectMapper jsonMapper = new ObjectMapper();
                if (segmentKey == null) {
                    this.filterHM = jsonMapper.readValue(filterJsonString, HashMap.class);
                } else {
                    HashMap unsegmentedFilters = jsonMapper.readValue(filterJsonString, HashMap.class);
                    ArrayList segments = new ArrayList();
                    if (this.filterHM != null && this.filterHM.containsKey(segmentKey)) {
                        segments = commonMMHFunctions.toArrayList(this.filterHM.get(segmentKey));
                    } else {
                        this.filterHM = new HashMap<String, Object>();
                        if (unsegmentedFilters.get("groupOp") != null && unsegmentedFilters.get("groupOp") instanceof String) {
                            this.filterHM.put("groupOp", unsegmentedFilters.get("groupOp"));
                        } else {
                            this.filterHM.put("groupOp", "AND"); // Default to additional operations
                        }
                    }
                    for (Object segmentData : commonMMHFunctions.toArrayList(unsegmentedFilters.get(segmentKey))) {
                        segments.add(segmentData);
                    }
                    this.filterHM.put(segmentKey, segments);
                }
            } catch (IOException ioe) {
                Logger.logMessage("IO Exception when building ServerGridProperties filter HashMap (filterHM): " + ioe.getMessage());
            } catch (Exception e) {
                Logger.logMessage("Exception when building ServerGridProperties filter HashMap (filterHM): " + e.getMessage());
            }
        }
    }

    // Public output methods
    public ArrayList<HashMap> returnRecords(ArrayList<HashMap> data) {
        HashMap returnParams = new HashMap();
        returnParams.put("currentPage", this.currentPage);
        returnParams.put("totalPages", this.totalPages);
        returnParams.put("totalRows", this.totalRows);
        returnParams.put("startingRecordNumber", this.startingRecordNumber);
        returnParams.put("endingRecordNumber", this.endingRecordNumber);
        data.add(returnParams);
        return data;
    }

}
