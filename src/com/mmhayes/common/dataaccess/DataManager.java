package com.mmhayes.common.dataaccess;

import com.mmhayes.common.conn_pool.JDCConnection;
import com.mmhayes.common.conn_pool.JDCConnectionPool;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.MMHResultSet;
import com.mmhayes.common.utils.StringFunctions;
import redstone.xmlrpc.handlers.ReflectiveInvocationHandler;

import java.io.ByteArrayInputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.Date;

//mmhayes dependencies

/*
 $Author: ecdyer $: Author of last commit
 $Date: 2020-05-05 13:04:47 -0400 (Tue, 05 May 2020) $: Date of last commit
 $Rev: 11626 $: Revision of last commit
 Notes: Data Manager containing common functionality other data managers use
*/
public class DataManager extends ReflectiveInvocationHandler {

    public static final int IDENTITY_COLUMN = 0;
    public String RemoteHost = "";
    public static JDCConnectionPool pool = null;
    public final static String PARAM_DELIM_START = "{";
    public final static String PARAM_DELIM_END = "}";
    private static String _driver;
    private static String _url;
    private static String _username;

    public DataManager() {
        if (pool == null) {
            GetPool();
        }
    }

    private boolean GetPool() {
        try {
            Logger.setLogLevel();
            //SC2P: Only supporting one database type at a time, the driver will be part of the application
            _driver = MMHProperties.getAppSetting("database.connection.driver"); //"com.microsoft.sqlserver.jdbc.SQLServerDriver";
            _url = MMHProperties.getAppSetting("database.connection.url");
            _username = MMHProperties.getAppSetting("database.connection.username");
            String password = MMHProperties.getAppSetting("database.connection.password");
            password = StringFunctions.decodePassword(password);
            Logger.logMessage("Creating new pool: " + _driver + ", " + _url + ", " + _username + ".","", Logger.LEVEL.TRACE);
            Class.forName(_driver);
            pool = new JDCConnectionPool(_url, _username, password);
            return true;
        } catch (ClassNotFoundException ce) {
            Logger.logMessage("Could not load DB driver " + ce.getMessage(),"", Logger.LEVEL.ERROR);
            Logger.logException(ce, "");
            return false;
        }
    }

//BEGIN getSQLProperty with overloads

    public String getSQLProperty(String sqlPropName, Object[] params) {
        return getSQLProperty(sqlPropName, params, "", true);
    }

    public String getSQLProperty(String sqlPropName, Object[] params, String logFileName) {
        return getSQLProperty(sqlPropName, params, logFileName, true);
    }

    public String getSQLProperty(String sqlPropName, Object[] params, boolean quoteParams) {
        return getSQLProperty(sqlPropName, params, "", quoteParams);
    }

    public String getSQLProperty(String sqlPropName, Object[] params, String logFileName, boolean quoteParams) {
        String sql = MMHProperties.getSqlString(sqlPropName, logFileName);
        if (params != null) {
            Logger.logMessage("About to insert parameters into '" + sql + "'", logFileName, Logger.LEVEL.LUDICROUS);
            sql = expandSqlParams(sql, params, quoteParams);
        }
        Logger.logMessage("About to return '" + sql + "'", logFileName, Logger.LEVEL.LUDICROUS);
        return sql;
    }

//END getSQLProperty with overloads

//BEGIN expandSqlParams with overloads

    /**
     * Quotes and fixes params to prevent against SQL Injection.
     * @param theString
     * @param params
     * @return
     */
    public String expandSqlParams(String theString, Object[] params) {
        return expandSqlParams(theString, params, true);
    }

    public String expandSqlParams(String theString, Object[] params, boolean quoteParams) {
        return expandSqlParams(theString, params, "", quoteParams);
    }

    //Loops over the params and calls expandSqlParam with quoteParams arg
    public String expandSqlParams(String theString, Object[] params, String logFileName, boolean quoteParams) {
        String newString = theString;
        if (params != null) {
            ArrayList<String> paramList = new ArrayList<>();
            for (int i = 1; i <= params.length; i++) {
                Object item = params[i - 1];
                paramList.add(item != null ? item.toString() : "NULL");
                newString = expandSqlParam(i, newString, item, quoteParams, logFileName);
            }
            if(!paramList.isEmpty()){
                Logger.logMessage("PARAMS: "+String.join(",",paramList), Logger.LEVEL.TRACE);
            }
        }
        return newString;
    }

    //returns a hashmap with the sql string being parameterized and an arraylist of the parameters in order
    //works for parameters out of order and multiple of the same parameter
    public HashMap expandParameterizedSQLParams(String sql, Object[] args) {
        ArrayList<Integer> indexes = new ArrayList<>();
        ArrayList values = new ArrayList();
        String theString = sql;
        try {
            for (int i = 0; i < args.length; i++) {
                String argNumIdentifier = "{" + Integer.toString(i + 1) + "}";
                String value = "temp";
                boolean tokensExist = (theString.indexOf(argNumIdentifier, 0) > 0);
                while (tokensExist) {
                    Logger.logMessage("replacing " + argNumIdentifier + " with " + args[i].toString(), "", Logger.LEVEL.LUDICROUS);
                    int pos = theString.indexOf(argNumIdentifier);
                    if (pos >= 0) {
                        indexes.add(pos);
                        Collections.sort(indexes);
                        int sortedIndex = indexes.indexOf(pos);
                        values.add(sortedIndex, args[i]);
                        theString = theString.substring(0, pos) + "?" + theString.substring(pos + argNumIdentifier.length(), theString.length());
                    }
                    tokensExist = (theString.indexOf(argNumIdentifier, 0) > 0);
                }
            }
        } catch (Exception e) {
            Logger.logException(e); //always log exceptions
            return null;
        }
        HashMap retObj = new HashMap();
        retObj.put("PARAMETERIZEDSQL", theString);
        retObj.put("ARGUMENTLIST", values);
        return retObj;
    }

//END expandSqlParams with overloads

//BEGIN expandSqlParam with overloads

    /**
     * TODO: We should no longer be using this method
     * @param paramId
     * @param theString
     * @param value
     * @param quoteAndFixParam
     * @return
     */
    @Deprecated
    public String expandSqlParam(int paramId, String theString, Object value, boolean quoteAndFixParam) {
        return expandSqlParam(paramId, theString, value, quoteAndFixParam, "");
    }

    public String expandSqlParams(String theString, Object[] params, String logFileName) {
        return expandSqlParams(theString, params, logFileName, true);//called with true -> not a problem
    }

    //formats the value based on data type and sends to replace
    //TODO: calling this method with false, or calling it with sql that is dynamically generated with parameters already inserted is insecure
    @Deprecated
    public String expandSqlParam(int paramId, String theString, Object value, boolean quoteAndFixParam, String logFileName) {
        //we are really only vulnerable when this is set to false
        if(quoteAndFixParam == false){
            //log potentially dangerous uses of this
            String loggingLevel = MMHProperties.getAppSetting("site.logging.level").toUpperCase();
            if(loggingLevel.equals("LUDICROUS")){
                //we should only have to be conserned when its a string
                if(value instanceof String){
                    Exception e = new RuntimeException("DataManager.expandSqlParam used insecurely");
                    Logger.logMessage("POSSIBLE SQLI VULNERABILITY FOUND", Logger.LEVEL.ERROR);
                    Logger.logMessage("STRING     : " + theString, Logger.LEVEL.ERROR);
                    Logger.logMessage("VALUE      : " +value.toString(), Logger.LEVEL.ERROR);
                    String dangerous = value.toString();
                    value = commonMMHFunctions.sanitizeStringFromSQLInjection(value.toString(), "");
                    Logger.logMessage("SANITIZES TO: " + value, Logger.LEVEL.ERROR);
                    if(dangerous.compareTo(value.toString()) != 0)
                        Logger.logException(new RuntimeException("Possible SQLInjection attack"));
                    value = dangerous;//set it back so we are only logging places that would break if we did this
                }
            }
        }
        // allow values to be null, substitute--this is great for SQL, but other things?
        /** @todo check for Oracle and SQL Server compatability */
//		if (value instanceof String) {
//			String val = value.toString();
//			if (val.compareToIgnoreCase("[NULL]") == 0)
//				value = null;
//		}

// put dates in proper format
/** @todo check for Oracle and SQL Server compatability */
        if (value instanceof Date) {
            value = fixDate((Date) value);
            quoteAndFixParam = false;
        } else if (value instanceof Boolean) {
            value = fixBoolean((Boolean) value);
        } else if (value instanceof Double) {
            value = fixDouble((Double) value);
        }

        //If we quote, then we have to fix and vice-versa, otherwise you can inject it,
        //and if we dont do either we are vulnerable to sqlInjection...hmm
        if (quoteAndFixParam) {
            if (value != null) {
                //TODO: (HIGH) Prevents a single quote for being an actual value - should we stop this check if there are two? -jrmitaly 12/2/2014
                value = value.toString().replaceAll("'", "''"); // replace all ' with ''
            }
            value = quoteSqlParam(value, logFileName);
        }

        return replace(theString, PARAM_DELIM_START + String.valueOf(paramId) + PARAM_DELIM_END, value.toString(), logFileName);
    }

    /**
     * THIS METHOD IS VULNERABLE TO SQL INJECTION WHEN USED IMPROPERLY! USE WISELY!
     * This method should ONLY be used for PARAMETERIZING dynamic SQL. Your SQL should NOT have parameters pre-populated
     *      at the time of calling this method. For correct usage, see commonMMHFunctions.addRecord.
     * @param paramId the {#} to replace with an sql fragment
     * @param rootSQL the SQL pulled from the SQL properties file
     * @param value part of a SQL Fragment.
     * @return the rootSQL with the fragement replaced into {paramID}
     */
    public String expandDynamicSqlFragment(int paramId, String rootSQL, Object value) {
        // put dates in proper format
        if (value instanceof Date) {
            value = fixDate((Date) value);
        } else if (value instanceof Boolean) {
            value = fixBoolean((Boolean) value);
        } else if (value instanceof Double) {
            value = fixDouble((Double) value);
        }

        return replace(rootSQL, PARAM_DELIM_START + String.valueOf(paramId) + PARAM_DELIM_END, value.toString(), "");
    }

    //END expandSqlParam with overloads

    //replace is used by expandSqlParams and expandSqlParam
    //finds all instances of the replacement paramter in sql string and replaces with replaceWith
    private String replace(String original, String toReplace, String replaceWith, String logFileName) {
        String ret = "";
        String theString = original;
        boolean tokensExist = (theString.indexOf(toReplace, 0) > 0);
        while (tokensExist) {
            Logger.logMessage("replace " + toReplace + " with: " + replaceWith, logFileName, Logger.LEVEL.DEBUG);
            int pos = theString.indexOf(toReplace);
            if (pos >= 0) {
                ret = ret + theString.substring(0, pos) + replaceWith;
                theString = theString.substring(pos + toReplace.length(), theString.length());
            } else {
                ret = ret + theString;
                theString = "";
            }
            tokensExist = (theString.indexOf(toReplace, 0) > 0);
        }
        ret = ret + theString;
        return ret;
    }

    private String quoteSqlParam(Object param, String logFileName) {
        String ret;
        if (param instanceof String) {
            String s = (String) param;
            if (s.toUpperCase().compareTo("NULL") == 0) {
                ret = String.valueOf(param);
            } else {
                //s = replace(s, "'", "''", logFileName);
                ret = "'" + s + "'";
            }
        } else {
            ret = String.valueOf(param);
        }
        return ret;
    }

//BEGIN getSingleField with overloads

    //BEGIN RAW SQL CALLS , THESE DO NOT READ SQL FILE TO GET SQL STRING
    //safe from SQLI WHEN
    public Object getSingleField(String sqlStatement, ArrayList<String> recordValues) {
        return getSingleField(recordValues.toArray(new Object[recordValues.size()]),sqlStatement);
    }
    //safe from SQLI
    public Object getSingleField(Object[] params, String sqlStatement) {
        return getSingleField(params, sqlStatement,"");
    }
    //safe from SQLI
    public Object getSingleField(Object[] params, String sqlStatement, String logFileName) {
        Object ret = "";
        try {
            String sql = sqlStatement;
            if (params != null) sql = expandSqlParams(sql, params, logFileName);
            MMHResultSet rs = runSql(sql,null,logFileName);
            if (rs.resultSet.next()) {
                ret = rs.resultSet.getObject(1);
            } else {
                ret = null;
            }
            rs.close();
        } catch (SQLException se) {
            Logger.logMessage("Could not get Single Field " + sqlStatement + " (SQLException).",logFileName, Logger.LEVEL.ERROR);
            Logger.logException(se, logFileName);
        } catch (Exception e) {
            Logger.logMessage("Could not get Single Field " + sqlStatement + " (Exception).",logFileName, Logger.LEVEL.ERROR);
            Logger.logException(e, logFileName);
        }
        return ret;
    }

    //END RAW SQL CALLS

    //FOLLOWING METHODS READ SQL PROPERTY FILE TO GET SQL STRING
    //Safe from SQLI
    public Object getSingleField(String sqlPropName, Object[] params) {
        return getSingleField(sqlPropName, params, null, "");
    }

    @Deprecated
    public Object getSingleField(String sqlPropName,Object[] params, boolean quoteParams){
        return getSingleField(sqlPropName, params,null,"",quoteParams);
    }

    //This is functionally never run without parameterization
    public Object getSingleField(String sqlPropName,Object[] params, boolean quoteParams,boolean parameterize){
        return getSingleField(sqlPropName, params,null,"",quoteParams,parameterize);
    }
    //Safe from SQLI
    public Object getSingleField(String sqlPropName,Object[] params, JDCConnection conn){
        return getSingleField(sqlPropName,params,conn,"");
    }

    //Safe from SQLI
    public Object getSingleField(String sqlPropName, Object[] params, JDCConnection conn, String logFileName) {
        return getSingleField(sqlPropName, params, conn, logFileName, true);
    }

    //TODO: this is only used by other getSingleField functions
    public Object getSingleField(String sqlPropName, Object[] params, JDCConnection conn, String logFileName, boolean quoteParams) {
        return getSingleField(sqlPropName, params, conn, logFileName, quoteParams, false);
    }

    //This is functionally only used in correct ways(other than the above method, parameterize is always true when this method is called directly(not through overloaded)
    public Object getSingleField(String sqlPropName, Object[] params, JDCConnection conn, String logFileName, boolean quoteParams, boolean parameterize) {
        if (parameterize) {
            return parameterizedGetSingleField(sqlPropName, params, conn, logFileName);
        }
       //TODO: The following getSingleField methods take raw SQL statements - do we really want this? -jrmitaly
        else {
            Object ret = "";
            try {
                String sql = MMHProperties.getSqlString(sqlPropName, logFileName);
                if (params != null) sql = expandSqlParams(sql, params, quoteParams);
                MMHResultSet rs = runSql(sql, conn, logFileName);
                if (rs.resultSet.next()) {
                    ret = rs.resultSet.getObject(1);
                } else {
                    ret = null;
                }
                rs.close();
            } catch (SQLException se) {
                Logger.logMessage("Could not get Single Field " + sqlPropName + " (SQLException).", logFileName, Logger.LEVEL.ERROR);
                Logger.logException(se, logFileName);
            } catch (Exception e) {
                Logger.logMessage("Could not get Single Field " + sqlPropName + " (Exception).", logFileName, Logger.LEVEL.ERROR);
                Logger.logException(e, logFileName);
            }
            return ret;
        }
    }

    public Object parameterizedGetSingleField(String sqlPropName, Object[] params, JDCConnection conn, String logFileName) {
        Object ret = "";
        try {

            String sql = MMHProperties.getSqlString(sqlPropName, logFileName);
            ArrayList args = new ArrayList();
            boolean parameterizeSuccessful = true;

            if (params != null && params.length > 0) {
                HashMap properties = expandParameterizedSQLParams(sql, params);
                if (properties != null) {
                    sql = properties.get("PARAMETERIZEDSQL").toString();
                    args = (ArrayList) properties.get("ARGUMENTLIST");
                } else {
                    parameterizeSuccessful = false;
                }
            }

            MMHResultSet rs= null;

            if (parameterizeSuccessful) {
                try{
                    rs = runSql(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, null, "", args);
                    if (rs.resultSet.next()) {
                        ret = rs.resultSet.getObject(1);
                    } else {
                        ret = null;
                    }
                } finally {
                    if(rs != null){
                        rs.close();
                    }
                }
            }
        } catch (SQLException se) {
            Logger.logMessage("Could not get Single Field " + sqlPropName + " (SQLException).", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(se, logFileName);
        } catch (Exception e) {
            Logger.logMessage("Could not get Single Field " + sqlPropName + " (Exception).", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(e, logFileName);
        }
        return ret;
    }

    /**
     * This method can be dangerous! Taking in a raw SQL query with parameters generated from user input is how we become
     * vulnerable to SQLInjection. Use this method wisely
     * @param rawQuery
     * @param recordValues
     * @return
     */
    public Object parameterizedGetSingleField(String rawQuery, ArrayList<String> recordValues) {
        Object ret = "";
        MMHResultSet rs = null;
        try {
            rs = runSql(rawQuery, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, null, "", recordValues);
            if (rs.resultSet.next()) {
                ret = rs.resultSet.getObject(1);
            } else {
                ret = null;
            }
        } catch (SQLException se) {
            Logger.logMessage("Could not get Single Field (SQLException), query: " + rawQuery,"", Logger.LEVEL.ERROR);
            Logger.logException(se, "");
        } catch (Exception e) {
            Logger.logMessage("Could not get Single Field (Exception), query: " + rawQuery,"", Logger.LEVEL.ERROR);
            Logger.logException(e, "");
        } finally {
            if(rs != null){
                rs.close();
            }
        }
        return ret;
    }


    /**
     * Designed to be tightly coupled with DynamicSQL instead of an SQL string and param list to prevent abuse.
     * @param dynamicSQL
     * @param logFileName
     * @return
     */
    public Object dynamicGetSingleField(DynamicSQL dynamicSQL, String logFileName) {
        Object ret = "";
        try {

            String sql = dynamicSQL.getSqlStatement();
            Object[] params = dynamicSQL.getParameters().toArray();
            ArrayList args = new ArrayList();
            boolean parameterizeSuccessful = true;

            if (params != null && params.length > 0) {
                HashMap properties = expandParameterizedSQLParams(sql, params);
                if (properties != null) {
                    sql = properties.get("PARAMETERIZEDSQL").toString();
                    args = (ArrayList) properties.get("ARGUMENTLIST");
                } else {
                    parameterizeSuccessful = false;
                }
            }

            MMHResultSet rs= null;

            if (parameterizeSuccessful) {
                try{
                    rs = runSql(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, null, "", args);
                    if (rs.resultSet.next()) {
                        ret = rs.resultSet.getObject(1);
                    } else {
                        ret = null;
                    }
                } finally {
                    if(rs != null){
                        rs.close();
                    }
                }
            }
        } catch (SQLException se) {
            Logger.logMessage("Could not get Single Field " + dynamicSQL.getSqlProperty() + " (SQLException).", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(se, logFileName);
        } catch (Exception e) {
            Logger.logMessage("Could not get Single Field " + dynamicSQL.getSqlProperty() + " (Exception).", logFileName, Logger.LEVEL.ERROR);
            Logger.logException(e, logFileName);
        }
        return ret;
    }


    //END READING SQL PROPERTY

//END getSingleField with overloads

//BEGIN runSql with overloads
    @Deprecated
    public MMHResultSet runSql(String sqlString) {
        return runSql(sqlString, null, "");
    }
    @Deprecated
    public MMHResultSet runSql(String sqlString, JDCConnection conn, String logFileName) {
        return runSql(sqlString, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, conn, logFileName);
    }
    @Deprecated //TODO: See why the parameterized version of this is so much slower...
    public MMHResultSet runSql(String sqlString, int cursorType, int readWriteAccess, JDCConnection conn, String logFileName) {
        // run sql string, return results
        MMHResultSet rs = new MMHResultSet();
        if (conn == null) {
            try {
                conn = pool.getConnection(false, logFileName);
            } catch (SQLException qe) {
                Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe.getMessage(), logFileName, Logger.LEVEL.ERROR);
                try {
                    // wait between 0.5 and 2.5 seconds and try again
                    int Min = 500;
                    int Max = 2500;
                    Thread.sleep(Min + (int) (Math.random() * ((Max - Min) + 1)));
                } catch (Exception ex) {
                    Logger.logException(ex);
                }
                try {
                    Logger.logMessage("Try again to get connection.", logFileName, Logger.LEVEL.ERROR);
                    conn = pool.getConnection(false, logFileName);
                } catch (SQLException qe2) {
                    Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe2.getMessage(), logFileName, Logger.LEVEL.ERROR);
                }
            }
        }
        if (conn != null) {
            try {
                String SPID = "";
                if(JDCConnectionPool.isConnectionLoggingEnabled){
                    SPID =  "SPID: " + conn.getSPID()+" ";
                    Logger.logMessage(SPID+"Creating Statement", logFileName, Logger.LEVEL.TRACE);
                }
                rs.statement = conn.createStatement(cursorType, readWriteAccess);
                Logger.logMessage(SPID+"(runSQL) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);
                Instant before = Instant.now(); // Capture time before
                rs.resultSet = rs.statement.executeQuery(sqlString);
                Instant after = Instant.now(); // Capture time after
                long delta = Duration.between(before, after).toMillis();
                Logger.logMessage(SPID+"RESULTS=" + (rs.resultSet.isBeforeFirst()?"T":"F")+" "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
            } catch (SQLException ex) {
                returnConnection(conn, logFileName);
                Logger.logMessage("Could not execute query: " + sqlString + " \r\nmessage: " + ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
                Logger.logMessage("ERROR: '" + ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
                if ((ex.getMessage().toLowerCase().indexOf("connection reset by peer") >= 0)
                        || (ex.getMessage().toLowerCase().indexOf("connection aborted by peer") >= 0)
                        || (ex.getMessage().toLowerCase().indexOf("no more data to read from socket") >= 0)
                        || (ex.getMessage().toLowerCase().indexOf("connection is closed") >= 0)
                        || (ex.getMessage().toLowerCase().indexOf("socket write error") >= 0)) {
                    try {
                        conn = pool.getConnection(true, logFileName);
                    } catch (SQLException qe) {
                        Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe.getMessage(), logFileName, Logger.LEVEL.ERROR);
                    }
                    if (conn != null) {
                        try {
                            String SPID = "";
                            if(JDCConnectionPool.isConnectionLoggingEnabled){
                                SPID = "SPID: " + conn.getSPID()+" ";
                                Logger.logMessage(SPID+"Creating Statement", logFileName, Logger.LEVEL.TRACE);
                            }
                            rs.statement = conn.createStatement(cursorType, readWriteAccess);
                            Logger.logMessage(SPID+"(runSQL) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);
                            Instant before = Instant.now(); // Capture time before
                            rs.resultSet = rs.statement.executeQuery(sqlString);
                            Instant after = Instant.now(); // Capture time after
                            long delta = Duration.between(before, after).toMillis();
                            Logger.logMessage(SPID+"RESULTS=" + (rs.resultSet.isBeforeFirst()?"T":"F")+" "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
                        } catch (SQLException exSub) {
                            Logger.logMessage("Could not Re-Running SQL statement: " + sqlString + " \r\nmessage: " + exSub.getMessage(), logFileName, Logger.LEVEL.ERROR);
                            Logger.logException(exSub, logFileName);
                        } finally {
                            returnConnection(conn, logFileName);
                        }
                    }

                } else {
                    Logger.logException(ex, logFileName);
                }
            } finally {
                returnConnection(conn, logFileName);
            }
        }
        return rs;
    }

    //uses a prepared statement, the arraylist is effectively a flag that it needs to be parameterized
    @Deprecated
    public MMHResultSet runSql(String sqlString, int cursorType, int readWriteAccess, JDCConnection conn, String logFileName, ArrayList<String> params) {
        // run sql string, return results
        MMHResultSet rs = new MMHResultSet();
        if (conn == null) {
            try {
                conn = pool.getConnection(false, logFileName);
            } catch (SQLException qe) {
                Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe.getMessage(), logFileName, Logger.LEVEL.ERROR);
                try {
                    // wait between 0.5 and 2.5 seconds and try again
                    int Min = 500;
                    int Max = 2500;
                    Thread.sleep(Min + (int) (Math.random() * ((Max - Min) + 1)));
                } catch (Exception ex) {
                    Logger.logException(ex);
                }
                try {
                    if(JDCConnectionPool.isConnectionLoggingEnabled){
                        Logger.logMessage("Try again to get connection.", logFileName, Logger.LEVEL.TRACE);
                    }
                    conn = pool.getConnection(false, logFileName);
                } catch (SQLException qe2) {
                    Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe2.getMessage(), logFileName, Logger.LEVEL.ERROR);
                }
            }
        }
        if (conn != null) {
            try {
                String SPID = "";
                if(JDCConnectionPool.isConnectionLoggingEnabled){
                    SPID =  "SPID: " + conn.getSPID()+" ";
                    Logger.logMessage(SPID+"Creating PreparedStatement", logFileName, Logger.LEVEL.TRACE);
                }
                rs.preparedStatement = conn.prepareStatement(sqlString);
                int i = 1;
                ArrayList<String> paramList = new ArrayList<>();
                for (Object param : params) {
                    paramList.add(param != null ? param.toString() : "NULL");
                    rs.preparedStatement.setObject(i, param);
                    i++;
                }
                if(!paramList.isEmpty()){Logger.logMessage("PARAMS: "+String.join(",",paramList), Logger.LEVEL.TRACE);}
                Logger.logMessage(SPID + "(runSQL) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);
                Instant before = Instant.now(); // Capture time before
                rs.resultSet = rs.preparedStatement.executeQuery();
                Instant after = Instant.now(); // Capture time after
                long delta = Duration.between(before, after).toMillis();
                Logger.logMessage(SPID+"RESULTS=" + (rs.resultSet.isBeforeFirst()?"T":"F")+" "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
            } catch (SQLException ex) {
                returnConnection(conn, logFileName);
                Logger.logMessage("Could not execute query: " + sqlString + " \r\nmessage: " + ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
                Logger.logMessage(ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
                if ((ex.getMessage().toLowerCase().indexOf("connection reset by peer") >= 0)
                        || (ex.getMessage().toLowerCase().indexOf("connection aborted by peer") >= 0)
                        || (ex.getMessage().toLowerCase().indexOf("no more data to read from socket") >= 0)
                        || (ex.getMessage().toLowerCase().indexOf("connection is closed") >= 0)
                        || (ex.getMessage().toLowerCase().indexOf("socket write error") >= 0)) {
                    try {
                        conn = pool.getConnection(true, logFileName);
                    } catch (SQLException qe) {
                        Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe.getMessage(), logFileName, Logger.LEVEL.ERROR);
                    }
                    if (conn != null) {
                        try {
                            String SPID = "";
                            if(JDCConnectionPool.isConnectionLoggingEnabled){
                                SPID =  "SPID: " + conn.getSPID()+" ";
                                Logger.logMessage(SPID+"Creating Statement ", logFileName, Logger.LEVEL.TRACE);
                            }
                            rs.statement = conn.createStatement(cursorType, readWriteAccess);
                            Logger.logMessage(SPID + "(runSQL) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);
                            Instant before = Instant.now(); // Capture time before
                            rs.resultSet = rs.statement.executeQuery(sqlString);
                            Instant after = Instant.now(); // Capture time after
                            long delta = Duration.between(before, after).toMillis();
                            Logger.logMessage(SPID + "RESULTS="+(rs.resultSet.isBeforeFirst()?"T":"F")+" "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
                        } catch (SQLException exSub) {
                            Logger.logMessage("Could not Re-Running SQL statement: " + sqlString + " \r\nmessage: " + exSub.getMessage(), logFileName, Logger.LEVEL.ERROR);
                            Logger.logException(exSub, logFileName);
                        } finally {
                            returnConnection(conn, logFileName);
                        }
                    }
                } else {
                    Logger.logException(ex, logFileName);
                }
            } finally {
                returnConnection(conn, logFileName);
            }
        }
        return rs;
    }


//    public MMHResultSet runSqlSpecificTypesTEST(String sqlString, int cursorType, int readWriteAccess, JDCConnection conn, String logFileName, ArrayList<String> params) {
//        // run sql string, return results
//        MMHResultSet rs = new MMHResultSet();
//        if (conn == null) {
//            try {
//                conn = pool.getConnection(false, logFileName);
//            } catch (SQLException qe) {
//                Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe.getMessage(), logFileName, Logger.LEVEL.ERROR);
//                try {
//                    // wait between 0.5 and 2.5 seconds and try again
//                    int Min = 500;
//                    int Max = 2500;
//                    Thread.sleep(Min + (int) (Math.random() * ((Max - Min) + 1)));
//                } catch (Exception ex) {
//                    Logger.logException(ex);
//                }
//                try {
//                    if(JDCConnectionPool.isConnectionLoggingEnabled){
//                        Logger.logMessage("Try again to get connection.", logFileName, Logger.LEVEL.TRACE);
//                    }
//                    conn = pool.getConnection(false, logFileName);
//                } catch (SQLException qe2) {
//                    Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe2.getMessage(), logFileName, Logger.LEVEL.ERROR);
//                }
//            }
//        }
//        if (conn != null) {
//            try {
//                String SPID = "";
//                if(JDCConnectionPool.isConnectionLoggingEnabled){
//                    SPID =  "SPID: " + conn.getSPID()+" ";
//                    Logger.logMessage(SPID+"Creating PreparedStatement", logFileName, Logger.LEVEL.TRACE);
//                }
//                rs.preparedStatement = conn.prepareStatement(sqlString);
//                int i = 1;
//                ArrayList<String> paramList = new ArrayList<>();
//                for (String param : params) {
//                    paramList.add(param != null ? param.toString() : "NULL");
//                    rs.preparedStatement.setString(i, param);
//                    i++;
//                }
//                if(!paramList.isEmpty()){Logger.logMessage("PARAMS: "+String.join(",",paramList), Logger.LEVEL.TRACE);}
//                Logger.logMessage(SPID + "(runSQL) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);
//                Instant before = Instant.now(); // Capture time before
//                rs.resultSet = rs.preparedStatement.executeQuery();
//                Instant after = Instant.now(); // Capture time after
//                long delta = Duration.between(before, after).toMillis();
//                Logger.logMessage(SPID+"RESULTS=" + (rs.resultSet.isBeforeFirst()?"T":"F")+" "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
//            } catch (SQLException ex) {
//                returnConnection(conn, logFileName);
//                Logger.logMessage("Could not execute query: " + sqlString + " \r\nmessage: " + ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
//                Logger.logMessage(ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
//                if ((ex.getMessage().toLowerCase().indexOf("connection reset by peer") >= 0)
//                        || (ex.getMessage().toLowerCase().indexOf("connection aborted by peer") >= 0)
//                        || (ex.getMessage().toLowerCase().indexOf("no more data to read from socket") >= 0)
//                        || (ex.getMessage().toLowerCase().indexOf("connection is closed") >= 0)
//                        || (ex.getMessage().toLowerCase().indexOf("socket write error") >= 0)) {
//                    try {
//                        conn = pool.getConnection(true, logFileName);
//                    } catch (SQLException qe) {
//                        Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe.getMessage(), logFileName, Logger.LEVEL.ERROR);
//                    }
//                    if (conn != null) {
//                        try {
//                            String SPID = "";
//                            if(JDCConnectionPool.isConnectionLoggingEnabled){
//                                SPID =  "SPID: " + conn.getSPID()+" ";
//                                Logger.logMessage(SPID+"Creating Statement ", logFileName, Logger.LEVEL.TRACE);
//                            }
//                            rs.statement = conn.createStatement(cursorType, readWriteAccess);
//                            Logger.logMessage(SPID + "(runSQL) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);
//                            Instant before = Instant.now(); // Capture time before
//                            rs.resultSet = rs.statement.executeQuery(sqlString);
//                            Instant after = Instant.now(); // Capture time after
//                            long delta = Duration.between(before, after).toMillis();
//                            Logger.logMessage(SPID + "RESULTS="+(rs.resultSet.isBeforeFirst()?"T":"F")+" "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
//                        } catch (SQLException exSub) {
//                            Logger.logMessage("Could not Re-Running SQL statement: " + sqlString + " \r\nmessage: " + exSub.getMessage(), logFileName, Logger.LEVEL.ERROR);
//                            Logger.logException(exSub, logFileName);
//                        } finally {
//                            returnConnection(conn, logFileName);
//                        }
//                    }
//                } else {
//                    Logger.logException(ex, logFileName);
//                }
//            } finally {
//                returnConnection(conn, logFileName);
//            }
//        }
//        return rs;
//    }
//    public MMHResultSet runSqlSpecificTypesTEST2(String sqlString, int cursorType, int readWriteAccess, JDCConnection conn, String logFileName, ArrayList<String> params) {
//        // run sql string, return results
//        MMHResultSet rs = new MMHResultSet();
//        if (conn == null) {
//            try {
//                conn = pool.getConnection(false, logFileName);
//            } catch (SQLException qe) {
//                Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe.getMessage(), logFileName, Logger.LEVEL.ERROR);
//                try {
//                    // wait between 0.5 and 2.5 seconds and try again
//                    int Min = 500;
//                    int Max = 2500;
//                    Thread.sleep(Min + (int) (Math.random() * ((Max - Min) + 1)));
//                } catch (Exception ex) {
//                    Logger.logException(ex);
//                }
//                try {
//                    if(JDCConnectionPool.isConnectionLoggingEnabled){
//                        Logger.logMessage("Try again to get connection.", logFileName, Logger.LEVEL.TRACE);
//                    }
//                    conn = pool.getConnection(false, logFileName);
//                } catch (SQLException qe2) {
//                    Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe2.getMessage(), logFileName, Logger.LEVEL.ERROR);
//                }
//            }
//        }
//        if (conn != null) {
//            try {
//                String SPID = "";
//                if(JDCConnectionPool.isConnectionLoggingEnabled){
//                    SPID =  "SPID: " + conn.getSPID()+" ";
//                    Logger.logMessage(SPID+"Creating PreparedStatement", logFileName, Logger.LEVEL.TRACE);
//                }
//                rs.preparedStatement = conn.prepareStatement(sqlString);
//                int i = 1;
//                ArrayList<String> paramList = new ArrayList<>();
//                for (String param : params) {
//                    paramList.add(param != null ? param.toString() : "NULL");
//                    rs.preparedStatement.setNString(i, param);
//                    i++;
//                }
//                if(!paramList.isEmpty()){Logger.logMessage("PARAMS: "+String.join(",",paramList), Logger.LEVEL.TRACE);}
//                Logger.logMessage(SPID + "(runSQL) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);
//                Instant before = Instant.now(); // Capture time before
//                rs.resultSet = rs.preparedStatement.executeQuery();
//                Instant after = Instant.now(); // Capture time after
//                long delta = Duration.between(before, after).toMillis();
//                Logger.logMessage(SPID+"RESULTS=" + (rs.resultSet.isBeforeFirst()?"T":"F")+" "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
//            } catch (SQLException ex) {
//                returnConnection(conn, logFileName);
//                Logger.logMessage("Could not execute query: " + sqlString + " \r\nmessage: " + ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
//                Logger.logMessage(ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
//                if ((ex.getMessage().toLowerCase().indexOf("connection reset by peer") >= 0)
//                        || (ex.getMessage().toLowerCase().indexOf("connection aborted by peer") >= 0)
//                        || (ex.getMessage().toLowerCase().indexOf("no more data to read from socket") >= 0)
//                        || (ex.getMessage().toLowerCase().indexOf("connection is closed") >= 0)
//                        || (ex.getMessage().toLowerCase().indexOf("socket write error") >= 0)) {
//                    try {
//                        conn = pool.getConnection(true, logFileName);
//                    } catch (SQLException qe) {
//                        Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe.getMessage(), logFileName, Logger.LEVEL.ERROR);
//                    }
//                    if (conn != null) {
//                        try {
//                            String SPID = "";
//                            if(JDCConnectionPool.isConnectionLoggingEnabled){
//                                SPID =  "SPID: " + conn.getSPID()+" ";
//                                Logger.logMessage(SPID+"Creating Statement ", logFileName, Logger.LEVEL.TRACE);
//                            }
//                            rs.statement = conn.createStatement(cursorType, readWriteAccess);
//                            Logger.logMessage(SPID + "(runSQL) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);
//                            Instant before = Instant.now(); // Capture time before
//                            rs.resultSet = rs.statement.executeQuery(sqlString);
//                            Instant after = Instant.now(); // Capture time after
//                            long delta = Duration.between(before, after).toMillis();
//                            Logger.logMessage(SPID + "RESULTS="+(rs.resultSet.isBeforeFirst()?"T":"F")+" "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
//                        } catch (SQLException exSub) {
//                            Logger.logMessage("Could not Re-Running SQL statement: " + sqlString + " \r\nmessage: " + exSub.getMessage(), logFileName, Logger.LEVEL.ERROR);
//                            Logger.logException(exSub, logFileName);
//                        } finally {
//                            returnConnection(conn, logFileName);
//                        }
//                    }
//                } else {
//                    Logger.logException(ex, logFileName);
//                }
//            } finally {
//                returnConnection(conn, logFileName);
//            }
//        }
//        return rs;
//    }

//END runSql with overloads

//BEGIN INSERT AND UPDATE METHODS WITH PREPARED STATEMENTS

    public ArrayList<HashMap> insertPreparedStatement(String sqlString, ArrayList<String> recordValues) {
        JDCConnection conn = null;
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        try {
            conn = pool.getConnection(false, "");
        } catch (SQLException qe) {
            Logger.logMessage("Could not run Insert: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username,"", Logger.LEVEL.ERROR);
            Logger.logException(qe);
            errorCodeList.add(new HashMap() {{
                put("errorMsg", "Could not get connection to database");
            }});
        }
        if (conn != null) {
            String SPID = "";
            if(JDCConnectionPool.isConnectionLoggingEnabled){
                SPID =  "SPID: " + conn.getSPID()+" ";
            }
            Logger.logMessage(SPID+"(insertPreparedStatement) SQL: " + sqlString,"", Logger.LEVEL.TRACE);
            PreparedStatement ps = null;
            ResultSet rs = null;
            String lastRecordID = "";
            CallableStatement stm = null;
            try {
                if (MMHProperties.getAppSetting("database.type").compareToIgnoreCase("ORACLE") == 0) {
                    stm = conn.prepareCall(sqlString);
                    int i = 1;
                    for (String insertVal : recordValues) {
                        stm.setObject(i, insertVal);
                        i++;
                    }
                    stm.registerOutParameter(i, Types.VARCHAR);
                    stm.execute();
                    int id = stm.getInt(i);
                    lastRecordID = String.valueOf(id);
                } else {
                    ps = conn.prepareStatement(sqlString);
                    int i = 1;
                    for (String insertVal : recordValues) {

                        //remove non valid ASCII characters from value strings - added by jrmitaly on 3/24/2017
                        if (insertVal != null && !insertVal.isEmpty()) {

                            //ASCII Chars 32-126 are GOOD Chars (Regex says: find characters that are NOT between ASCII Char 32 (x20) through ASCII Char 126 (x7E)) -jrmitaly 3/24/2017
                            insertVal = insertVal.replaceAll("[^\\x20-\\x7e]", "");

                        }

                        ps.setObject(i, insertVal);
                        i++;
                    }
                    Instant before = Instant.now(); // Capture time before
                    int affectedRows = PreparedStatementExecuteUpdateWithDeadlockRetries(ps,"");
                    Instant after = Instant.now(); // Capture time after
                    long delta = Duration.between(before, after).toMillis();
                    Logger.logMessage(SPID+"COUNT="+affectedRows+" "+delta+"ms", Logger.LEVEL.TRACE);
                    rs = ps.getGeneratedKeys();
                    while (rs.next()) {
                        lastRecordID = rs.getString("RETURNEDVAL");
                    }
                }
                HashMap temp = new HashMap();
                temp.put("LASTRECORDID", lastRecordID);
                errorCodeList.add(temp);

            } catch (final Exception e) {
                Logger.logMessage("Could not insert record with Exception :" + e.toString(),"", Logger.LEVEL.ERROR);
                Logger.logException(e);
                errorCodeList.add(new HashMap() {{
                    put("errorMsg", "Could not insert record with Exception : " + e.toString());
                }});
            } finally {
                //release resources
                try {
                    if (ps != null) {
                        ps.close();
                        if( rs != null ){
                            rs.close();
                        }
                    }

                    if (stm != null) {
                        stm.close();
                    }
                } catch (SQLException e) {
                    Logger.logMessage("Closing ps or stm raised " + e.getMessage(),"", Logger.LEVEL.ERROR);
                    Logger.logException(e);
                    errorCodeList.add(new HashMap() {{
                        put("errorMsg", "Could not release resources");
                    }});
                } finally {
                    returnConnection(conn, "");
                }

            }
        }
        return errorCodeList;
    }

    //overload for setAndUpdatePreparedStatement(String sqlString, String primaryIdentifier, ArrayList<String> recordValues)
    public ArrayList<HashMap> setAndUpdatePreparedStatement(String sqlString, ArrayList<String> recordValues) {
        return setAndUpdatePreparedStatement(sqlString, recordValues, "N/A");
    }

    public ArrayList<HashMap> setAndUpdatePreparedStatement(String sqlString, ArrayList<String> recordValues, String recordIdentifier) {
        JDCConnection conn = null;
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        try {
            conn = pool.getConnection(false, "");
        } catch (SQLException qe) {
            Logger.logMessage("Could not run Update: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username, "", Logger.LEVEL.ERROR);
            Logger.logException(qe);
            errorCodeList.add(new HashMap() {{
                put("errorMsg", "Could not get connection to database");
            }});
        }
        if (conn != null) {
            String SPID = "";
            if(JDCConnectionPool.isConnectionLoggingEnabled){
                SPID =  "SPID: " + conn.getSPID()+" ";
            }
            Logger.logMessage(SPID+"(setAndUpdatePreparedStatement) SQL: " + sqlString ,"", Logger.LEVEL.TRACE);
            PreparedStatement ps = null;

            try {
                ps = conn.prepareStatement(sqlString);
                int i = 1;
                //insert the values in order
                for (String insertVal : recordValues) {
                    if (insertVal == null || insertVal.equals("NULL")) {
                        ps.setNull(i, Types.NULL);
                    } else {

                        //remove non valid ASCII characters from value strings - added by jrmitaly on 3/24/2017
                        if (insertVal != null && !insertVal.isEmpty()) {

                            //ASCII Chars 32-126 are GOOD Chars (Regex says: find characters that are NOT between ASCII Char 32 (x20) through ASCII Char 126 (x7E)) -jrmitaly 3/24/2017
                            insertVal = insertVal.replaceAll("[^\\x20-\\x7e]", "");

                        }

                        ps.setObject(i, insertVal);
                    }

                    i++;
                }
                Instant before = Instant.now(); // Capture time before
                int result = PreparedStatementExecuteUpdateWithDeadlockRetries(ps,"");
                Instant after = Instant.now(); // Capture time after
                long delta = Duration.between(before, after).toMillis();
                Logger.logMessage(SPID+"COUNT="+result+" "+delta+"ms", Logger.LEVEL.TRACE);
                if (result != 1) {
                    Logger.logMessage("Query returned a value of -1","", Logger.LEVEL.DEBUG);
                    Logger.logMessage("Return value of -1 might indicate that the NOCOUNT property for the database was turned on and never turned off by a previous SQL call","", Logger.LEVEL.DEBUG);
                }

            } catch (final Exception e) {
                Logger.logMessage("Update for record identifier: '" + recordIdentifier + "' failed due to exception: " + e.toString(),"", Logger.LEVEL.ERROR);
                Logger.logException(e);
                errorCodeList.add(new HashMap() {{
                    put("errorMsg", "Could not update record with Exception : " + e.toString());
                }});
            } finally {
                //release resources
                try {
                    ps.close();
                } catch (final SQLException e) {
                    Logger.logMessage("Closing ps raised " + e.getMessage(),"", Logger.LEVEL.ERROR);
                    Logger.logException(e);
                    errorCodeList.add(new HashMap() {{
                        put("errorMsg", "Could not release system resources to database with Exception : " + e.toString());
                    }});
                } finally {
                    returnConnection(conn, "");
                }
            }
        }
        return errorCodeList;
    }

//END INSERT AND UPDATE METHODS WITH PREPARED STATEMENTS


//BEGIN RESULT SET TRANSFORMATION METHODS

    public ArrayList resultSetToArrayList(MMHResultSet rs, boolean destroyResultSet) {
        return resultSetToArrayList(rs, destroyResultSet, "");
    }

    //TODO: (HIGH) This method does not address all types - (see other methods as well) - only integers and booleans and incorrectly creates integers when they should be big ints -jrmitaly on 9/14/2015
    public ArrayList resultSetToArrayList(MMHResultSet rs, boolean destroyResultSet, String logFileName) {
        ArrayList ret = new ArrayList();

        try {
            ResultSetMetaData rsInfo = rs.resultSet.getMetaData();

            int numCols = rsInfo.getColumnCount();
            if (numCols > 0) {
                // Fill up the rows
                while (rs.resultSet.next()) {
                    ArrayList row = new ArrayList();
                    for (int i = 1; i <= numCols; i++) {
                        int type = rsInfo.getColumnType(i);
                        String typeName = rsInfo.getColumnTypeName(i);
                        String colName = rsInfo.getColumnName(i);
                        Object value = null;
                        if ((type == Types.DECIMAL) || (type == Types.DOUBLE) || (type == Types.NUMERIC) || (type == Types.FLOAT) || (type == Types.INTEGER)) {
                            String typesName;
                            if (type == Types.DECIMAL) {
                                typesName = "DECIMAL";
                            } else if (type == Types.DOUBLE) {
                                typesName = "DOUBLE";
                            } else if (type == Types.NUMERIC) {
                                typesName = "NUMERIC";
                            } else if (type == Types.FLOAT) {
                                typesName = "FLOAT";
                            } else {
                                typesName = "INTEGER";
                            }

                            int prec = rsInfo.getPrecision(i);
                            int scale = rsInfo.getScale(i); // number of digits to the right of decimal place

                            if (rs.resultSet.getObject(i) == null) {
                                Logger.logMessage(colName + ": value=null; typeName=" + typeName + "; type=" + type + "; prec = " + prec + "; scale = " + scale, logFileName, Logger.LEVEL.LUDICROUS);
                            } else {
                                Logger.logMessage(colName + ": value=" + rs.resultSet.getObject(i).toString() + "; typeName=" + typeName + "; type=" + type + "; prec = " + prec + "; scale = " + scale, logFileName, Logger.LEVEL.LUDICROUS);
                            }

                            // EXAMPLES:
                            //   ORACLE NUMBER(18,0): Precision = 18, Scale = 0
                            //   ORACLE NUMBER(8,2): Precision = 8, Scale = 2, biggest number = 999999.99

                            // NOTE: CAN NOT CHECK FOR PRECISION = 0 BECAUSE THIS HAPPENS WITH VARIOUS NUMERIC TYPES WHEN USING AGGREGATE FUNCTIONS LIKE SUM OR WHEN USING A CASE STATEMENT
                            if ((rs.resultSet.getObject(i) != null) && (((prec == 1) && (scale == 0)) || (((colName.length() >= 2) && (colName.substring(0, 2).compareToIgnoreCase("B_") == 0)) && (prec == 0) && (scale == 0)))) {
                                // going to assume this is a boolean value
                                value = rs.resultSet.getBoolean(i);
                            } else if ((rs.resultSet.getObject(i) != null) && (prec > 1) && (scale == 0) && (rs.resultSet.getObject(i).toString().indexOf(".") < 0) && ((rs.resultSet.getLong(i) >= Integer.MIN_VALUE) && (rs.resultSet.getLong(i) <= Integer.MAX_VALUE))) {

                                //TODO: (HIGH) Two things wrong below... (see other methods as well) -jrmitaly on 9/14/2015
                                //1. Why would you just "assume" it's an integer? What if it's bigger than integer?
                                //2. What's the point of adding the INTEGER type to the return list if we aren't going to account for the basic STRING type?
                                //Why not just treat everything as generic objects if we are not going to determine the common ones???

                                // assume this is an integer
                                value = rs.resultSet.getInt(i);
                            } else {
                                value = rs.resultSet.getObject(i);
                            }
                        } else if ((type == Types.DATE) || (type == Types.TIMESTAMP)) {
                            try {
                                if (rs.resultSet.getObject(i) == null) {
                                    Logger.logMessage(colName + ": value=null; typeName=" + typeName + "; type=" + type, logFileName, Logger.LEVEL.LUDICROUS);
                                } else {
                                    Logger.logMessage(colName + ": value=" + rs.resultSet.getTimestamp(i).toString() + "; typeName=" + typeName + "; type=" + type, logFileName, Logger.LEVEL.LUDICROUS);
                                }
                            } catch (Exception e) {
                                Logger.logException(e, logFileName);
                            }
                            value = rs.resultSet.getTimestamp(i);
                        } else {
                            try {
                                if (rs.resultSet.getObject(i) == null) {
                                    Logger.logMessage(colName + ": value=null; typeName=" + typeName + "; type=" + type, logFileName, Logger.LEVEL.LUDICROUS);
                                } else {
                                    Logger.logMessage(colName + ": value=" + rs.resultSet.getObject(i).toString() + "; typeName=" + typeName + "; type=" + type, logFileName, Logger.LEVEL.LUDICROUS);
                                }
                            } catch (Exception e) {
                                Logger.logException(e, logFileName);
                            }
                            value = rs.resultSet.getObject(i);
                        }
                        if (value == null) value = "";
                        row.add(value);
                    }
                    ret.add(row);
                }
            }
        } catch (SQLException se) {
            // LOG Error, etc.
            Logger.logMessage("Error in DataManager: " + se.getMessage(), logFileName, Logger.LEVEL.ERROR);
            Logger.logException(se, logFileName);
        }
        if (destroyResultSet) {
            rs.close();
        }
        return ret;
    }

    public ArrayList resultSetToHashMap(MMHResultSet rs, boolean destroyResultSet) {
        return resultSetToHashMap(rs, destroyResultSet, "");
    }

    //TODO: (HIGH) This method does not address all types - - (see other methods as well) - only integers and booleans and incorrectly creates integers when they should be big ints -jrmitaly on 9/14/2015
    public ArrayList resultSetToHashMap(MMHResultSet rs, boolean destroyResultSet, String logFileName) {
        Logger.logMessage("Entering resultSetToHashMap",Logger.LEVEL.LUDICROUS);
        ArrayList ret = new ArrayList();
        try {
            ResultSetMetaData rsInfo = rs.resultSet.getMetaData();

            int numCols = rsInfo.getColumnCount();
            if (numCols > 0) {
                // Fill up the rows
                while (rs.resultSet.next()) {
                    HashMap row = new HashMap();
                    for (int i = 1; i <= numCols; i++) {
                        int type = rsInfo.getColumnType(i);
                        String typeName = rsInfo.getColumnTypeName(i);
                        Object value;
                        String colName = rsInfo.getColumnName(i).toUpperCase();
                        if (type == Types.LONGVARCHAR) {
                            try {
                                if (rs.resultSet.getObject(i) == null) {
                                    Logger.logMessage(colName + ": value=null; typeName=" + typeName + "; type=" + type, logFileName, Logger.LEVEL.LUDICROUS);
                                } else {
                                    Logger.logMessage(colName + ": value=" + rs.resultSet.getObject(i).toString() + "; typeName=" + typeName + "; type=" + type, logFileName, Logger.LEVEL.LUDICROUS);
                                }
                            } catch (Exception e) {
                                Logger.logException(e, logFileName);
                            }
                            //value = null; //commented out by jrmitaly on 7/28/2015
                            value = rs.resultSet.getObject(i); //we should handle the the TEXT type properly.. -added by jrmitaly on 7/28/2015
                        } else if ((type == Types.DECIMAL) || (type == Types.DOUBLE) || (type == Types.NUMERIC)) {
                            Logger.logMessage("In resultSetToHashMap column " + colName + " (type == Types.DECIMAL) || (type == Types.DOUBLE) || (type == Types.NUMERIC)", logFileName, Logger.LEVEL.LUDICROUS);
                            String typesName;
                            if (type == Types.DECIMAL)
                                typesName = "DECIMAL";
                            else if (type == Types.DOUBLE)
                                typesName = "DOUBLE";
                            else //if (type == Types.NUMERIC)
                                typesName = "NUMERIC";
                            int prec = rsInfo.getPrecision(i);
                            int scale = rsInfo.getScale(i);

                            try {
                                if (rs.resultSet.getObject(i) == null) {
                                    Logger.logMessage(colName + ": value=null; typeName=" + typeName + "; typesName=" + typesName + "; type=" + type + "; prec = " + prec + "; scale = " + scale, logFileName, Logger.LEVEL.LUDICROUS);
                                } else {
                                    Logger.logMessage(colName + ": value=" + rs.resultSet.getObject(i).toString() + "; typeName=" + typeName + "; typesName=" + typesName + "; type=" + type + "; prec = " + prec + "; scale = " + scale, logFileName, Logger.LEVEL.LUDICROUS);
                                }
                            } catch (Exception e) {
                                Logger.logException(e, logFileName);
                            }

                            // NOTE: CAN NOT CHECK FOR PRECISION = 0 BECAUSE THIS HAPPENS WITH VARIOUS NUMERIC TYPES WHEN USING AGGREGATE FUNCTIONS LIKE SUM OR WHEN USING A CASE STATEMENT
                            if ((rs.resultSet.getObject(i) != null) && (((prec == 1) && (scale == 0)) || (((colName.length() >= 2) && (colName.substring(0, 2).compareToIgnoreCase("B_") == 0)) && (prec == 0) && (scale == 0)))) {
                                // going to assume this is a boolean value
                                value = rs.resultSet.getBoolean(i);
                            } else if ((rs.resultSet.getObject(i) != null) && (prec > 1) && (scale == 0) && (rs.resultSet.getObject(i).toString().indexOf(".") < 0) && ((rs.resultSet.getLong(i) >= Integer.MIN_VALUE) && (rs.resultSet.getLong(i) <= Integer.MAX_VALUE))) {

                                //TODO: (HIGH) Two things wrong below... -jrmitaly on 9/14/2015
                                //1. Why would you just "assume" it's an integer? What if it's bigger than integer?
                                //2. What's the point of adding the INTEGER type to the return list if we aren't going to account for the basic STRING type?
                                //Why not just treat everything as generic objects if we are not going to determine the common ones???

                                // assume this is an integer
                                value = rs.resultSet.getInt(i);
                            } else {
                                value = rs.resultSet.getObject(i);
                            }

                        } else if ((type == Types.DATE) || (type == Types.TIMESTAMP)) {
                            try {
                                if (rs.resultSet.getObject(i) == null) {
                                    Logger.logMessage(colName + ": value=null; typeName=" + typeName + "; type=" + type, logFileName, Logger.LEVEL.LUDICROUS);
                                } else {
                                    Logger.logMessage(colName + ": value=" + rs.resultSet.getTimestamp(i).toString() + "; typeName=" + typeName + "; type=" + type, logFileName, Logger.LEVEL.LUDICROUS);
                                }
                            } catch (Exception e) {
                                Logger.logException(e, logFileName);
                            }
                            value = rs.resultSet.getTimestamp(i);
                        } else {
                            try {
                                if (rs.resultSet.getObject(i) == null) {
                                    Logger.logMessage(colName + ": value=null; typeName=" + typeName + "; type=" + type, logFileName, Logger.LEVEL.LUDICROUS);
                                } else {
                                    Logger.logMessage(colName + ": value=" + rs.resultSet.getObject(i).toString() + "; typeName=" + typeName + "; type=" + type, logFileName, Logger.LEVEL.LUDICROUS);
                                }
                            } catch (Exception e) {
                                Logger.logException(e, logFileName);
                            }
                            value = rs.resultSet.getObject(i);
                        }
                        if (value == null) value = "";
                        row.put(rsInfo.getColumnName(i).toUpperCase(), value);
                    }
                    ret.add(row);
                    if ((ret.size() % 1000) == 0)
                    {
                        Logger.logMessage("Processed " + ret.size() + " rows so far", logFileName, Logger.LEVEL.TRACE);
                    }
                }
                Logger.logMessage("ROWS: " + ret.size(), logFileName, Logger.LEVEL.TRACE);
            }
        } catch (SQLException se) {
            // LOG Error, etc.
            Logger.logMessage("Error in DataManager = " + se.getMessage(), logFileName, Logger.LEVEL.ERROR);
            Logger.logException(se, logFileName);
        }
        if (destroyResultSet) {
            rs.close();
        }
        return ret;
    }

    //TODO: (HIGH) This method does not address all types - (see other methods as well) - only integers and booleans and incorrectly creates integers when they should be big ints -jrmitaly on 9/14/2015
    public ArrayList resultSetToLinkedHashmap(MMHResultSet rs, boolean destroyResultSet) {
        ArrayList ret = new ArrayList();

        try {
            ResultSetMetaData rsInfo = rs.resultSet.getMetaData();

            int numCols = rsInfo.getColumnCount();
            if (numCols > 0) {
                // Fill up the rows
                while (rs.resultSet.next()) {
                    LinkedHashMap row = new LinkedHashMap();
                    for (int i = 1; i <= numCols; i++) {
                        int type = rsInfo.getColumnType(i);
                        String typeName = rsInfo.getColumnTypeName(i);
                        String colName = rsInfo.getColumnName(i);
                        if (type == Types.LONGVARCHAR) {
                            try {
                                if (rs.resultSet.getObject(i) == null) {
                                    Logger.logMessage(colName + ": value=null; typeName=" + typeName + "; type=" + type,"", Logger.LEVEL.LUDICROUS);
                                } else {
                                    Logger.logMessage(colName + ": value=" + rs.resultSet.getObject(i).toString() + "; typeName=" + typeName + "; type=" + type,"", Logger.LEVEL.LUDICROUS);
                                }
                            } catch (Exception e) {
                                Logger.logException(e);
                            }
                            row.put(rsInfo.getColumnName(i).toUpperCase(), "");
                        } else if ((type == Types.DECIMAL) || (type == Types.DOUBLE) || (type == Types.NUMERIC)) {
                            Logger.logMessage("In resultSetToHashMap column " + colName + " (type == Types.DECIMAL) || (type == Types.DOUBLE) || (type == Types.NUMERIC)","", Logger.LEVEL.LUDICROUS);
                            String typesName;
                            if (type == Types.DECIMAL)
                                typesName = "DECIMAL";
                            else if (type == Types.DOUBLE)
                                typesName = "DOUBLE";
                            else //if (type == Types.NUMERIC)
                                typesName = "NUMERIC";
                            int prec = rsInfo.getPrecision(i);
                            int scale = rsInfo.getScale(i);

                            try {
                                if (rs.resultSet.getObject(i) == null) {
                                    Logger.logMessage(colName + ": value=null; typeName=" + typeName + "; typesName=" + typesName + "; type=" + type + "; prec = " + prec + "; scale = " + scale,"", Logger.LEVEL.LUDICROUS);
                                } else {
                                    Logger.logMessage(colName + ": value=" + rs.resultSet.getObject(i).toString() + "; typeName=" + typeName + "; typesName=" + typesName + "; type=" + type + "; prec = " + prec + "; scale = " + scale,"", Logger.LEVEL.LUDICROUS);
                                }
                            } catch (Exception e) {
                                Logger.logException(e);
                            }


                            // NOTE: CAN NOT CHECK FOR PRECISION = 0 BECAUSE THIS HAPPENS WITH VARIOUS NUMERIC TYPES WHEN USING AGGREGATE FUNCTIONS LIKE SUM OR WHEN USING A CASE STATEMENT
                            if ((rs.resultSet.getObject(i) != null) && (((prec == 1) && (scale == 0)) || (((colName.length() >= 2) && (colName.substring(0, 2).compareToIgnoreCase("B_") == 0)) && (prec == 0) && (scale == 0)))) {
                                // going to assume this is a boolean value
                                row.put(rsInfo.getColumnName(i).toUpperCase(), fixNulls(rs.resultSet.getBoolean(i)));

                            } else if ((rs.resultSet.getObject(i) != null) && (prec > 1) && (scale == 0) && (rs.resultSet.getObject(i).toString().indexOf(".") < 0) && ((rs.resultSet.getLong(i) >= Integer.MIN_VALUE) && (rs.resultSet.getLong(i) <= Integer.MAX_VALUE))) {

                                //TODO: (HIGH) Two things wrong below... -jrmitaly on 9/14/2015
                                //1. Why would you just "assume" it's an integer? What if it's bigger than integer?
                                //2. What's the point of adding the INTEGER type to the return list if we aren't going to account for the basic STRING type?
                                //Why not just treat everything as generic objects if we are not going to determine the common ones???

                                // assume this is an integer
                                row.put(rsInfo.getColumnName(i).toUpperCase(), fixNulls(rs.resultSet.getInt(i)));
                            } else {
                                row.put(rsInfo.getColumnName(i).toUpperCase(), fixNulls(rs.resultSet.getObject(i)));
                            }

                        } else if ((type == Types.DATE) || (type == Types.TIMESTAMP)) {
                            try {
                                if (rs.resultSet.getObject(i) == null) {
                                    Logger.logMessage(colName + ": value=null; typeName=" + typeName + "; type=" + type,"", Logger.LEVEL.LUDICROUS);
                                } else {
                                    Logger.logMessage(colName + ": value=" + rs.resultSet.getTimestamp(i).toString() + "; typeName=" + typeName + "; type=" + type,"", Logger.LEVEL.LUDICROUS);
                                }
                            } catch (Exception e) {
                                Logger.logException(e);
                            }
                            row.put(rsInfo.getColumnName(i).toUpperCase(), fixNulls(rs.resultSet.getTimestamp(i)));
                        } else {
                            try {
                                if (rs.resultSet.getObject(i) == null) {
                                    Logger.logMessage(colName + ": value=null; typeName=" + typeName + "; type=" + type,"", Logger.LEVEL.LUDICROUS);
                                } else {
                                    Logger.logMessage(colName + ": value=" + rs.resultSet.getObject(i).toString() + "; typeName=" + typeName + "; type=" + type,"", Logger.LEVEL.LUDICROUS);
                                }
                            } catch (Exception e) {
                                Logger.logException(e);
                            }
                            row.put(rsInfo.getColumnName(i).toUpperCase(), fixNulls(rs.resultSet.getObject(i)));
                        }
                    }
                    ret.add(row);
                }
            }
        } catch (SQLException se) {
            // LOG Error, etc.
            Logger.logMessage("Error in DataManager = " + se.getMessage(),"", Logger.LEVEL.ERROR);
            Logger.logException(se);
        }
        if (destroyResultSet) {
            rs.close();
        }
        return ret;
    }

//END RESULT SET TRANSFORMATION METHODS

    private int StatementExecuteUpdateWithDeadlockRetries(Statement statement, String sqlString, String logFileName) throws Exception {
        int retries = 10;
        boolean successfulExecution = false;
        int ret = 0;
        while (!successfulExecution) {
            try {
                Logger.logMessage("Calling Statement: attempt("+(11-retries)+")", Logger.LEVEL.TRACE);
                ret = statement.executeUpdate(sqlString);
                successfulExecution = true;
            } catch (SQLException ex) {
                retries--;
                if ((retries > 0) && (ex.getMessage().contains("deadlock"))) {
                    Logger.logMessage(ex.getMessage(),logFileName, Logger.LEVEL.IMPORTANT);
                    Logger.logMessage("WARNING DEADLOCK DETECTED RETRYING SQL: " + sqlString,logFileName, Logger.LEVEL.IMPORTANT);
                    Thread.sleep(500);
                } else {
                    Logger.logException(ex);
                    throw ex; // this will cause us to exit the while loop in the event that a non-deadlock error happens
                }
            }
        }
        return ret;
    }

    private int PreparedStatementExecuteUpdateWithDeadlockRetries(PreparedStatement statement, String logFileName) throws Exception {
        int retries = 10;
        boolean successfulExecution = false;
        int ret = 0;
        while (!successfulExecution) {
            try {
                Logger.logMessage("Calling Prepared Statement: attempt("+(11-retries)+")", Logger.LEVEL.TRACE);
                ret = statement.executeUpdate();
                successfulExecution = true;
            } catch (SQLException ex) {
                retries--;
                if ((retries > 0) && (ex.getMessage().contains("deadlock"))) {
                    Logger.logMessage(ex.getMessage(),logFileName, Logger.LEVEL.IMPORTANT);
                    Logger.logMessage("WARNING DEADLOCK DETECTED RETRYING SQL",logFileName, Logger.LEVEL.IMPORTANT);
                    Thread.sleep(500);
                } else {
                    Logger.logException(ex);
                    throw ex; // this will cause us to exit the while loop in the event that a non-deadlock error happens
                }
            }
        }
        return ret;
    }

//BEGIN RUNUPDATE METHODS

    //USED IN QC KIOS DATA MANAGER
    public int runUpdateByteArray(String sql, byte[] b) {
        int ret = -1;
        JDCConnection conn = null;
        try {
            conn = pool.getConnection(false, "");
        } catch (SQLException qe) {
            Logger.logMessage("Could not run Update: " + sql + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username,"", Logger.LEVEL.ERROR);
            Logger.logException(qe);
        }
        if (conn != null) {
            try {
                //Statement statement = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                String SPID = "";
                if(JDCConnectionPool.isConnectionLoggingEnabled){
                    SPID =  "SPID: " + conn.getSPID()+" ";
                    Logger.logMessage(SPID+"Creating PreparedStatement","", Logger.LEVEL.TRACE);
                }
                PreparedStatement ps = conn.prepareStatement(sql);
                Logger.logMessage(SPID +"(runUpdateByteArray) SQL: " + sql,"", Logger.LEVEL.TRACE);
                ByteArrayInputStream bais = new ByteArrayInputStream(b);
                ps.setBinaryStream(1, bais, b.length);
                Instant before = Instant.now(); // Capture time before
                ret = PreparedStatementExecuteUpdateWithDeadlockRetries(ps,"");
                Instant after = Instant.now(); // Capture time after
                long delta = Duration.between(before, after).toMillis();
                Logger.logMessage(SPID+" Executed query (ret=" + ret + ")"+" "+delta+"ms", Logger.LEVEL.TRACE);
                ps.close();
            } catch (SQLException ex) {
                Logger.logMessage("Could not execute query: " + sql + " \r\nmessage: " + ex.getMessage(),"", Logger.LEVEL.ERROR);
                Logger.logException(ex);
            } catch (Exception ex) {
                Logger.logMessage("Could not execute query: " + sql + " \r\nmessage: " + ex.getMessage(), Logger.LEVEL.ERROR);
                Logger.logException(ex);
            } finally {
                returnConnection(conn, "");
            }
        }
        return ret;
    }

    public int runUpdate(String sqlString) {
        return runUpdate(sqlString, null, "");
    }

    public int runUpdate(String sqlString, JDCConnection conn, String logFileName) {
        return runUpdate(sqlString, conn, logFileName, 0);
    }

    public int runUpdate(String sqlString, JDCConnection conn, String logFileName, int timeoutInSeconds) {
        int ret = -1;

        if (conn == null) {
            try {
                conn = pool.getConnection(false, logFileName);
            } catch (SQLException qe) {
                Logger.logMessage("Could not run Update: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username, logFileName, Logger.LEVEL.ERROR);
                Logger.logException(qe, logFileName);
            }
        }
        if (conn != null) {
            try {
                String SPID = "";
                if(JDCConnectionPool.isConnectionLoggingEnabled){
                    SPID = "SPID: "+conn.getSPID()+" ";
                    Logger.logMessage(SPID+"Creating Statement", logFileName, Logger.LEVEL.TRACE);
                }
                Statement statement = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                if (timeoutInSeconds != 0) {
                    statement.setQueryTimeout(timeoutInSeconds);
                    Logger.logMessage("QueryTimeout: " + timeoutInSeconds, logFileName, Logger.LEVEL.DEBUG);
                }
                Logger.logMessage(SPID+"(runUpdate) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);
                // TODO Add Duration Logging
                Instant before = Instant.now(); // Capture time before
                ret = StatementExecuteUpdateWithDeadlockRetries(statement,sqlString,logFileName);
                Instant after = Instant.now(); // Capture time after
                long delta = Duration.between(before, after).toMillis();
                Logger.logMessage(SPID + "Executed query (ret=" + ret + ") "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
                if (sqlString.toUpperCase().indexOf("OUTPUT INSERTED.") > 0) {
                    Logger.logMessage("calling getGeneratedKeys", logFileName, Logger.LEVEL.DEBUG);
                    ResultSet rs = statement.getGeneratedKeys();
                    if (rs.next()) {
                        ret = Integer.parseInt(rs.getObject(1).toString());
                        Logger.logMessage("getGeneratedKeys returned " + ret, logFileName, Logger.LEVEL.TRACE);
                    }
                    rs.close();
                }

                statement.close();
            } catch (SQLException ex) {
                Logger.logMessage("Could not execute query: " + sqlString + " \r\nmessage: " + ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
                Logger.logException(ex,logFileName);
            } catch (Exception ex) {
                Logger.logMessage("Could not execute query: " + sqlString + " \r\nmessage: " + ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
                Logger.logException(ex,logFileName);
            } finally {
                if(JDCConnectionPool.isConnectionLoggingEnabled){
                    Logger.logMessage("About to check conn.getAutoCommit() in finally block of runUpdate SPID: "+conn.getSPID(),logFileName, Logger.LEVEL.DEBUG);
                }
                returnConnection(conn, logFileName);
            }
        }
        return ret;
    }

    public int runUpdate(String sqlString, ArrayList<String> args, JDCConnection conn, String logFileName, int timeoutInSeconds) {
        int ret = -1;

        if (conn == null) {
            try {
                conn = pool.getConnection(false, logFileName);
            } catch (SQLException qe) {
                Logger.logMessage("Could not run Update: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username, logFileName, Logger.LEVEL.ERROR);
                Logger.logException(qe, logFileName);
            }
        }
        if (conn != null) {
            try {
                String SPID = "";
                if(JDCConnectionPool.isConnectionLoggingEnabled){
                    SPID = "SPID: " + conn.getSPID()+" ";
                    Logger.logMessage(SPID+"Creating Statement", logFileName, Logger.LEVEL.TRACE);
                }
                PreparedStatement statement = conn.prepareStatement(sqlString);
                if (timeoutInSeconds != 0) {
                    statement.setQueryTimeout(timeoutInSeconds);
                    Logger.logMessage("QueryTimeout: " + timeoutInSeconds, logFileName, Logger.LEVEL.DEBUG);
                }
                Logger.logMessage(SPID+"(runUpdate) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);
                statement = conn.prepareStatement(sqlString);
                int i = 1;
                //insert the values in order
                ArrayList<String> paramList = new ArrayList<>();
                for (Object insertVal : args) {
                    paramList.add(insertVal != null ? insertVal.toString() : "NULL");
                    statement.setObject(i, insertVal);
                    i++;
                }
                if(!paramList.isEmpty()){Logger.logMessage("PARAMS: "+String.join(",",paramList), Logger.LEVEL.TRACE);}
                Instant before = Instant.now(); // Capture time before
                ret = PreparedStatementExecuteUpdateWithDeadlockRetries(statement,logFileName);
                Instant after = Instant.now(); // Capture time after
                long delta = Duration.between(before, after).toMillis();
                Logger.logMessage(SPID+"Executed query (ret=" + ret + ") "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
                statement.close();
            } catch (SQLException ex) {
                Logger.logMessage("Could not execute query: " + sqlString + " \r\nmessage: " + ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
                Logger.logException(ex,logFileName);
            } catch (Exception ex) {
                Logger.logMessage("Could not execute query: " + sqlString + " \r\nmessage: " + ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
                Logger.logException(ex,logFileName);
            } finally {
                returnConnection(conn, logFileName);
            }
        }
        return ret;
    }

//END RUNUPDATE METHODS

//BEGIN FIX VALUE METHODS

    private Object fixNulls(Object o) {
        if (o == null) {
            return "";
        } else {
            return o;
        }
    }

    public String fixDouble(Double amount) {
        String ret;
        ret = String.format("%,.9f", amount);
        return ret;
    }

    public String fixDate(Date date) {
        SimpleDateFormat df = (SimpleDateFormat) SimpleDateFormat.getDateInstance();
        df.applyPattern("MM/dd/yyyy hh:mm:ss a");
        String ret;
        if (MMHProperties.getAppSetting("database.type").compareToIgnoreCase("ORACLE") == 0) {
            ret = "TO_DATE('" + df.format(date) + "','MM/DD/YYYY HH:MI:SS AM')";
        } else {
            ret = "'" + df.format(date) + "'";
        }
        return ret;
    }

    public Object fixBoolean(Boolean value) {
       /* if (value.booleanValue()) return new Integer(1);
        else return new Integer(0);*/
       return value ? 1 : 0;
    }

//END FIX VALUE METHODS

//BEGIN SERIALIZESQL METHODS

    //Safe from SQLI
    public ArrayList serializeSql(String sqlPropName, Object[] params) {
        String sql = MMHProperties.getSqlString(sqlPropName, "");
        if (params != null) sql = expandSqlParams(sql, params);
        MMHResultSet rs = runSql(sql);
        return resultSetToArrayList(rs, true);
    }

    //Safe from SQLI
    public ArrayList serializeSqlWithColNames(String sqlPropName) {
        return serializeSqlWithColNames(sqlPropName, new Object[]{});
    }

    //Safe from SQLI
    public ArrayList serializeSqlWithColNames(String sqlPropName, Object[] params) {
        return serializeSqlWithColNames(sqlPropName, params, true);
    }

    //modified this method to call an overload w/ quote params -jrmitaly
    @Deprecated
    public ArrayList serializeSqlWithColNames(String sqlPropName, Object[] params, JDCConnection conn, String logFileName) {
        return serializeSqlWithColNames(sqlPropName, params, conn, logFileName, false);
    }

    //re-added? this overload in for quote params -jrmitaly
    @Deprecated
    public ArrayList serializeSqlWithColNames(String sqlPropName, Object[] params, JDCConnection conn, String logFileName, boolean quoteParams) {
        String sql = MMHProperties.getSqlString(sqlPropName, logFileName);
        if (params != null) sql = expandSqlParams(sql, params, quoteParams);
        MMHResultSet rs = runSql(sql, conn, logFileName);
        return resultSetToHashMap(rs, true, logFileName);
    }

    //TODO: The following are different then the top 5, they do the same thing but the arguments don't chain the same
    //itd be nice to have the arguments cascade
    @Deprecated
    public ArrayList serializeSqlWithColNames(String sqlPropName, Object[] params, boolean quoteParams) {
        return serializeSqlWithColNames(sqlPropName, params, quoteParams, false);
    }
    // this has the potential for abuse
    public ArrayList serializeSqlWithColNames(String sqlPropName, Object[] params, boolean quoteParams, boolean parameterize) {
        ArrayList retArrList = new ArrayList();
        if (parameterize) {
            String sql = MMHProperties.getSqlString(sqlPropName, "");
            HashMap properties = expandParameterizedSQLParams(sql, params);
            if (properties != null) {
                sql = properties.get("PARAMETERIZEDSQL").toString();
                ArrayList args = (ArrayList) properties.get("ARGUMENTLIST");
                if (args != null && sql != null && !sql.isEmpty()) {
                    MMHResultSet rs = runSql(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, null, "", args);
                    retArrList = resultSetToHashMap(rs, true);
                }
            }
        } else {
            //TODO: If this is being used, it is likely a vulnerability
            retArrList = serializeSqlWithColNames(sqlPropName, params, null, "",quoteParams);
        }
        return retArrList;
    }
    /**
     * WARNING: this method takes RAW possibly dynamically generated sql. This is DANGEROUS! Use wisely...
     * This method is designed only for use with paginated servergrid or the DynamicSQL class, which properly parameterizes it's input.
     * @param sqlString This MUST NEVER contain ANY unvalidated user input, to do so is to introduce an sql vulnerability.
     * @param params arguments to parameterize into the query
     * @return
     */
    //TODO: we should probably require a DynamicSQL object for this to prevent abuse
    public ArrayList dynamicSerializeSqlWithColNames(String sqlString, Object[] params) {
        return dynamicSerializeSqlWithColNames(sqlString, params, "");
    }
    /**
     * WARNING: this method takes RAW possibly dynamically generated sql. This is DANGEROUS! Use wisely...
     * This method is designed only for use with paginated servergrid or the DynamicSQL class, which properly parameterizes it's input.
     * @param sqlString This MUST NEVER contain ANY unvalidated user input, to do so is to introduce an sql vulnerability.
     * @param params arguments to parameterize into the query
     * @return
     */
    //TODO: we should probably require a DynamicSQL object for this to prevent abuse
    public ArrayList dynamicSerializeSqlWithColNames(String sqlString, Object[] params, String logFileName) {
        if(logFileName == null){
            logFileName = "";
        }
        long startTime = System.currentTimeMillis();
        long queryTime = 0;
        ArrayList retArrList = new ArrayList();
            String sql = sqlString;
            HashMap properties = expandParameterizedSQLParams(sql, params);
            if (properties != null) {
                sql = properties.get("PARAMETERIZEDSQL").toString();
                ArrayList args = (ArrayList) properties.get("ARGUMENTLIST");
                if (args != null && sql != null && !sql.isEmpty()) {
                    queryTime = System.currentTimeMillis();
                    MMHResultSet rs = runSql(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, null, logFileName, args);
                    queryTime = System.currentTimeMillis() - queryTime;
                    retArrList = resultSetToHashMap(rs, true);
                }
            }
        Logger.logMessage("dynamicSerializeSqlWithColNames run time: " + (System.currentTimeMillis()-startTime) + " query time:" + queryTime, Logger.LEVEL.DEBUG);
        return retArrList;
    }

    //This is only used in the getLabelForRecord in commonMMHFunctions
    //TODO: refactor common methods to not use this overload
    public ArrayList serializeSqlWithColNames(String sqlPropName, String sqlString, ArrayList<String> recordValues, boolean quoteParams) {
        Object[] args = recordValues.toArray(new Object[recordValues.size()]);
        String sql = (sqlPropName.length() > 0 ? MMHProperties.getSqlString(sqlPropName) : sqlString);
        return serializeSqlWithColNames(sql, args, null, "", quoteParams);
    }

    public ArrayList<HashMap> serializeSqlLinkedHashmap(String sqlPropName, Object[] params){
        String sql = MMHProperties.getSqlString(sqlPropName, "");
        if (params != null) sql = expandSqlParams(sql, params, true);
        MMHResultSet rs = runSql(sql, null, "");
        return resultSetToLinkedHashmap(rs,true);
    }


//END SERIALIZESQL METHODS

    private String dateToStringStandardSqlFormat(java.util.Date javaDate) {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return df.format(javaDate);
    }

//BEGIN PARAMETERIZED EXECUTE, EXECUTENONQUERY & EXECUTESCALAR METHODS

    // NOTES:
    //      THESE METHODS USE SQL FROM SQLSTRINGS FILES THAT IS ALREADY PROPERLY PARAMETERIZED (?)
    //      THEY ACCEPT PARAMETERS IN THE FOR OF OBJECT ARRAYS
    //      OVERLOADS OF THESE ACCEPTING PARAMETERS AS STRING LIST (FOR JAVASCRIPT) COULD BE MADE IF NEEDED
    //      NAMES WERE DERIVED USING C# SQL FUNCTION NAMING CONVENTIONS TO DIFFERENTIATE FROM OLDER NON-PARAMETERIZED METHODS

    //returns a hashmap with the sql string being parameterized and an arraylist of the parameters in order
    //works for parameters out of order and multiple of the same parameter

    public int parameterizedExecuteNonQuery(String sqlPropName, Object[] params) {
        return parameterizedExecuteNonQuery(sqlPropName, params, "");
    }

    public int parameterizedExecuteNonQuery(String sqlPropName, Object[] params, String logFileName) {
        return parameterizedExecuteNonQuery(sqlPropName, params, logFileName, null);
    }

    public int parameterizedExecuteNonQuery(String sqlPropName, Object[] params, String logFileName, JDCConnection conn) {
        return parameterizedExecuteNonQuery(sqlPropName, params, logFileName, conn, 0);
    }

    public int parameterizedExecuteNonQuery(String sqlPropName, Object[] params, String logFileName, JDCConnection conn, int timeoutInSeconds) {
        int result = 0;
        if (conn == null) {
            try {
                conn = pool.getConnection(false, logFileName);
            } catch (SQLException qe) {
                Logger.logMessage("Could not run Update: " + sqlPropName + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username, logFileName, Logger.LEVEL.ERROR);
                Logger.logException(qe, logFileName);
                return -1;
            }
        }

        PreparedStatement ps = null;

        try {
            String SPID="";
            if(JDCConnectionPool.isConnectionLoggingEnabled){
                SPID =  "SPID: " + conn.getSPID()+" ";
            }
            String sqlString = getSQLProperty(sqlPropName, null, logFileName);
            Logger.logMessage(SPID+"(parameterizedExecuteNonQuery) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);

            ArrayList convertedList = convertSquigglyParams(sqlString, params, logFileName);
            sqlString = convertedList.get(0).toString();
            params = (Object[]) convertedList.get(1);

            ps = conn.prepareStatement(sqlString);

            //insert the values in order
            if (params != null) {
                String paramList = "";
                for (int i = 0; i < params.length; i++) {
                    if (params[i] instanceof java.util.Date) {
                        //ps.setDate(i+1,new java.sql.Date(((java.util.Date)params[i]).getTime()));
                        ps.setObject(i+1, dateToStringStandardSqlFormat((java.util.Date)params[i]));
                    } else if (params[i] instanceof String) {
                        String paramAsStr = params[i].toString();

                        //remove non valid ASCII characters from value strings - added by jrmitaly on 3/24/2017
                        if (paramAsStr != null && !paramAsStr.isEmpty()) {

                            //ASCII Chars 32-126 are GOOD Chars (Regex says: find characters that are NOT between ASCII Char 32 (x20) through ASCII Char 126 (x7E)) -jrmitaly 3/24/2017
                            paramAsStr = paramAsStr.replaceAll("[^\\x20-\\x7e]", "");

                        }

                        ps.setObject(i+1, paramAsStr);
                    } else {
                        ps.setObject(i+1, params[i]);
                    }
                    paramList += (i > 0 ? "," : "") + params[i];
                }
                if(!paramList.isEmpty()){
                    Logger.logMessage(SPID+"PARAMS: " + paramList, logFileName, Logger.LEVEL.TRACE);
                }
            }

            //TODO not sure if fail on execute will throw an exception(in which case it's covered) or if we should do something with the bool
            ps.setQueryTimeout(timeoutInSeconds); // NOTE: 0 means no limit, which is the default value of timeoutInSeconds
            Instant before = Instant.now(); // Capture time before
            result = PreparedStatementExecuteUpdateWithDeadlockRetries(ps,logFileName);
            Instant after = Instant.now(); // Capture time after
            long delta = Duration.between(before, after).toMillis();
            if (sqlString.toUpperCase().indexOf("OUTPUT INSERTED.") > 0) {
                Logger.logMessage("calling getGeneratedKeys", logFileName, Logger.LEVEL.LUDICROUS);
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    result = Integer.parseInt(rs.getObject(1).toString());
                    Logger.logMessage(SPID+"getGeneratedKeys returned " + result+" "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
                }
                rs.close();
            }

        } catch (final Exception e) {
            Logger.logMessage("Update for : " + sqlPropName + " failed : " + e.getMessage(), logFileName, Logger.LEVEL.ERROR);
            Logger.logException(e,logFileName);
            result = -1;
        } finally {
            //release resources
            try {
                ps.close();
            } catch (final SQLException e) {
                Logger.logMessage("Releasing resources for : " + sqlPropName + " failed : " + e.getMessage(), logFileName, Logger.LEVEL.ERROR);
                Logger.logException(e,logFileName);
            } finally {
                returnConnection(conn, "");
            }
        }
        return result;
    }

    public ArrayList parameterizedExecuteQuery(String sqlPropName, Object[] params, boolean columnNames) {
        return parameterizedExecuteQuery(sqlPropName, params, "", columnNames, null);
    }

    public ArrayList parameterizedExecuteQuery(String sqlPropName, Object[] params, String logFileName, boolean columnNames) {
        return parameterizedExecuteQuery(sqlPropName, params, logFileName, columnNames, null);
    }

    public ArrayList parameterizedExecuteQuery(String sqlPropName, Object[] params, String logFileName, boolean columnNames, JDCConnection conn) {
        return parameterizedExecuteQuery(sqlPropName, params, logFileName, columnNames, conn, 0);
    }

    public ArrayList parameterizedExecuteQuery(String sqlPropName, Object[] params, String logFileName, boolean columnNames, JDCConnection conn, int timeoutInSeconds) {
        MMHResultSet rs = parameterizedExecuteQuery(sqlPropName, params,logFileName, conn, timeoutInSeconds);
        if (columnNames)
        {
            return resultSetToHashMap(rs, true, logFileName);
        }
        else
        {
            return resultSetToArrayList(rs, true, logFileName);
        }
    }

    public MMHResultSet parameterizedExecuteQuery(String sqlPropName, Object[] params, String logFileName) {
        return parameterizedExecuteQuery(sqlPropName, params, logFileName, null);
    }

    public MMHResultSet parameterizedExecuteQuery(String sqlPropName, Object[] params, String logFileName, JDCConnection conn) {
        return parameterizedExecuteQuery(sqlPropName, params, logFileName, conn, 0);
    }

    public MMHResultSet parameterizedExecuteQuery(String sqlPropName, Object[] params, String logFileName, JDCConnection conn, int timeoutInSeconds) {
        String sql = MMHProperties.getSqlString(sqlPropName, logFileName);
        return parameterizedRunSql(sql, params, logFileName, conn);
    }

    public Object parameterizedExecuteScalar(String sqlPropName, Object[] params) {
        return parameterizedExecuteScalar(sqlPropName, params, "", null);
    }

    public Object parameterizedExecuteScalar(String sqlPropName, Object[] params, String logFileName) {
        return parameterizedExecuteScalar(sqlPropName, params, logFileName, null);
    }

    public Object parameterizedExecuteScalar(String sqlPropName, Object[] params, String logFileName, JDCConnection conn) {
        return parameterizedExecuteScalar(sqlPropName, params, logFileName, conn, 0);
    }

    public Object parameterizedExecuteScalar(String sqlPropName, Object[] params, String logFileName, JDCConnection conn, int timeoutInSeconds) {
        // NOTE: THIS IS SIMILAR TO GetSingleField
        MMHResultSet rs = null; //new MMHResultSet();
        Object ret = null;
        try{
            rs = parameterizedExecuteQuery(sqlPropName, params, logFileName, conn, timeoutInSeconds);
            if (rs.resultSet.next()) {
                ret = rs.resultSet.getObject(1);
            }
        } catch (SQLException ex) {
            Logger.logMessage("QUERY: " + sqlPropName + " raised error " + ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
            Logger.logException(ex,logFileName);
        }finally {
            try {
                if(rs != null ){
                    rs.close();
                }
            } catch (Exception ex) {
                Logger.logException(ex);
            }
        }
        return ret;
    }

    public MMHResultSet parameterizedRunSql(String sqlString, Object[] params, String logFileName, JDCConnection conn) {
        // run sql string, return results
        MMHResultSet rs = new MMHResultSet();
        if (conn == null) {
            try {
                conn = pool.getConnection(false, logFileName);
            } catch (SQLException qe) {
                Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe.getMessage(), logFileName, Logger.LEVEL.ERROR);
                Logger.logException(qe,logFileName);
                try {
                    // wait between 0.5 and 2.5 seconds and try again
                    int Min = 500;
                    int Max = 2500;
                    Thread.sleep(Min + (int) (Math.random() * ((Max - Min) + 1)));
                } catch (Exception ex) {
                    Logger.logException(ex);
                }
                try {
                    if(JDCConnectionPool.isConnectionLoggingEnabled){
                        Logger.logMessage("Try again to get connection.", logFileName, Logger.LEVEL.TRACE);
                    }
                    conn = pool.getConnection(false, logFileName);
                } catch (SQLException qe2) {
                    Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe2.getMessage(), logFileName, Logger.LEVEL.ERROR);
                    Logger.logException(qe2,logFileName);
                }
            }
        }
        if (conn != null) {
            try {
                String SPID = "";
                if(JDCConnectionPool.isConnectionLoggingEnabled){
                    SPID =  "SPID: " + conn.getSPID()+" ";
                }
                ArrayList convertedList = convertSquigglyParams(sqlString, params, logFileName);
                sqlString = convertedList.get(0).toString();
                params = (Object[]) convertedList.get(1);

                rs.preparedStatement = conn.prepareStatement(sqlString);
                // Add the parameters in order
                if (params != null) {
                    Logger.logMessage(SPID+"About to add parameters to SQL statement", logFileName, Logger.LEVEL.LUDICROUS);
                    String paramList = "";
                    for (int i = 0; i < params.length; i++) {
                        if (params[i] instanceof java.util.Date) {
                            //rs.preparedStatement.setDate(i+1, new java.sql.Date(((java.util.Date)params[i]).getTime()));
                            rs.preparedStatement.setObject(i+1, dateToStringStandardSqlFormat((java.util.Date)params[i]));
                        } else {
                            rs.preparedStatement.setObject(i+1, params[i]);
                        }
                        paramList += (i > 0 ? "," : "") + params[i];
                    }
                    if(!paramList.isEmpty()){
                        Logger.logMessage("PARAMS: " + paramList, logFileName, Logger.LEVEL.TRACE);
                    }
                }
                Logger.logMessage(SPID+"(parameterizedRunSql) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);
                Instant before = Instant.now(); // Capture time before
                rs.resultSet = rs.preparedStatement.executeQuery();
                Instant after = Instant.now(); // Capture time after
                long delta = Duration.between(before, after).toMillis();
                Logger.logMessage(SPID+"RESULTS=" + (rs.resultSet.isBeforeFirst()?"T":"F")+" "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
            } catch (SQLException ex) {
                returnConnection(conn, logFileName);
                Logger.logMessage("Could not execute query: " + sqlString + " \r\nmessage: " + ex.getMessage(), logFileName, Logger.LEVEL.ERROR);
                Logger.logException(ex, logFileName);
                if ((ex.getMessage().toLowerCase().indexOf("connection reset by peer") >= 0)
                        || (ex.getMessage().toLowerCase().indexOf("connection aborted by peer") >= 0)
                        || (ex.getMessage().toLowerCase().indexOf("no more data to read from socket") >= 0)
                        || (ex.getMessage().toLowerCase().indexOf("connection is closed") >= 0)
                        || (ex.getMessage().toLowerCase().indexOf("socket write error") >= 0)) {
                    try {
                        conn = pool.getConnection(true, logFileName);
                    } catch (SQLException qe) {
                        Logger.logMessage("Could not get conn to run Select: " + sqlString + ". No connection to DB. (Driver:" + _driver + "; url: " + _url + "; username: " + _username + " RAISED ERROR: " + qe.getMessage(), logFileName, Logger.LEVEL.ERROR);
                        Logger.logException(qe,logFileName);
                    }
                    if (conn != null) {
                        try {
                            String SPID = "";
                            if(JDCConnectionPool.isConnectionLoggingEnabled){
                                SPID = "SPID: "+conn.getSPID()+" ";
                            }
                            rs.preparedStatement = conn.prepareStatement(sqlString);
                            if (params != null) {
                                Logger.logMessage(SPID+"About to add parameters to statement:", logFileName, Logger.LEVEL.LUDICROUS);
                                String paramList = "";
                                for (int i = 0; i < params.length; i++) {
                                    if (params[i] instanceof java.util.Date) {
                                        rs.preparedStatement.setObject(i+1, dateToStringStandardSqlFormat((java.util.Date)params[i]));
                                    } else {
                                        rs.preparedStatement.setObject(i+1, params[i]);
                                    }
                                    paramList += (i > 0 ? "," : "") + params[i];
                                }
                                if(!paramList.isEmpty()){
                                    Logger.logMessage("PARAMS: " + paramList, logFileName, Logger.LEVEL.TRACE);
                                }
                            }
                            Logger.logMessage(SPID+"(parameterizedRunSql) SQL: " + sqlString, logFileName, Logger.LEVEL.TRACE);
                            Instant before = Instant.now(); // Capture time before
                            rs.resultSet = rs.preparedStatement.executeQuery();
                            Instant after = Instant.now(); // Capture time after
                            long delta = Duration.between(before, after).toMillis();
                            Logger.logMessage(SPID+"RESULTS=" + (rs.resultSet.isBeforeFirst()?"T":"F")+" "+delta+"ms", logFileName, Logger.LEVEL.TRACE);
                        } catch (SQLException exSub) {
                            Logger.logMessage("Could not Re-Running SQL statement: " + sqlString + " \r\nmessage: " + exSub.getMessage(), logFileName, Logger.LEVEL.ERROR);
                            Logger.logException(exSub, logFileName);
                        } finally {
                            returnConnection(conn, logFileName);
                        }
                    }

                } else {
                    Logger.logException(ex, logFileName);
                }
            } finally {
                returnConnection(conn, logFileName);
            }
        }
        return rs;
    }


//END PARAMETERIZED EXECUTE, EXECUTENONQUERY & EXECUTESCALAR METHODS

    //convert squiggly parameters (ex. {1}) to question marks while maintaining parameter order for proper parameterization
    public ArrayList convertSquigglyParams(String sqlString, Object[] args, String logFileName) {
        //first check to see if we should be attempting this at all
        ArrayList returnList = new ArrayList();
        try {

            //if this sqlString contains at least one ? then don't both with the conversion at all
            if ((sqlString.indexOf("?", 0) > 0) || (args == null)) {
                returnList.add(sqlString);
                returnList.add(args);
                return returnList;
            }

            //now we can do the conversion so initialize the objects we will need and start
            ArrayList<Integer> squigglyPositionList = new ArrayList<>();
            ArrayList sortedArgs = new ArrayList();
            ArrayList<String> paramList = new ArrayList<>();

            //iterate through every argument
            for (int i = 0; i < args.length; i++) {

                //this identifier will be used to find this arguments squiggly in the sqlString ("{1}" or "{5}" for example)
                String squigglyIdentifier = "{" + Integer.toString(i + 1) + "}";

                //keep going until this squigglyIdentifier does not exist in the sqlString
                while ((sqlString.indexOf(squigglyIdentifier, 0) > 0)) {

                    //get the position in the sql of this squiggly
                    int squigglyPosition = sqlString.indexOf(squigglyIdentifier);

                    //add this squiggly position to the squigglyPositionList
                    squigglyPositionList.add(squigglyPosition);

                    //sort the squigglyPositionList
                    Collections.sort(squigglyPositionList);

                    //add this argument to sortedArgs at the position it is currently in in the squigglyPositionList
                    sortedArgs.add(squigglyPositionList.indexOf(squigglyPosition), args[i]);
                    paramList.add(squigglyPositionList.indexOf(squigglyPosition), (args[i] != null ? args[i].toString() : "NULL"));

                    //replace this squiggly with a question mark
                    sqlString = sqlString.substring(0, squigglyPosition) + "?" + sqlString.substring(squigglyPosition + squigglyIdentifier.length(), sqlString.length());
                }
            }

            //add the newly formatted sqlString and convert sortedArgs to an object array and log the success
            returnList.add(sqlString);
            returnList.add(sortedArgs.toArray());
            if(sortedArgs.size()>0){
                Logger.logMessage("PARAMS: "+String.join(",",paramList), logFileName, Logger.LEVEL.LUDICROUS);
                Logger.logMessage("UPDATED SQL: "+sqlString, logFileName, Logger.LEVEL.LUDICROUS);
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            returnList.add(null); //set to null, if this fails we don't want the sql to run
            returnList.add(null); //set to null, if this fails we don't want the sql to run
        }
        return returnList;
    }

//BEGIN SERIALIZEUPDATE WITH OVERLOADS
    //Safe from SQLI
    public int serializeUpdate(String sqlPropName) {
        return serializeUpdate(sqlPropName, null);
    }

    //Safe from SQLI
    public int serializeUpdate(String sqlPropName, Object[] params) {
        return serializeUpdate(sqlPropName, params, null, "");
    }

    //This should not be called with false
    @Deprecated
    public int serializeUpdate(String sqlPropName, Object[] params, boolean quoteParams) {
        return serializeUpdate(sqlPropName, params, null, "", 0, quoteParams);
    }

    //Safe from SQLI
    public int serializeUpdate(String sqlPropName, Object[] params, JDCConnection conn, String logFileName) {
        return serializeUpdate(sqlPropName, params, conn, logFileName, 0, true);
    }

    //Safe from SQLI
    public int serializeUpdate(String sqlPropName, Object[] params, JDCConnection conn, String logFileName, int timeoutInSeconds) {
        return serializeUpdate(sqlPropName, params, conn, logFileName, timeoutInSeconds, true);
    }

    //TODO: This should never be called with false quoteparams
    public int serializeUpdate(String sqlPropName, Object[] params, JDCConnection conn, String logFileName, int timeoutInSeconds, boolean quoteParams) {
        return serializeUpdate(sqlPropName, params, conn, logFileName, timeoutInSeconds, quoteParams, false);
    }

    //TODO: This should never be called with false quoteparams AND false parameterize
    public int serializeUpdate(String sqlPropName, Object[] params, JDCConnection conn, String logFileName, int timeoutInSeconds, boolean quoteParams, boolean parameterize) {
        String sql = MMHProperties.getSqlString(sqlPropName, logFileName);
        int ret = -1;
        ArrayList args = new ArrayList();
        if (params != null) {
            Logger.logMessage("About to insert parameters into '" + sql + "'", logFileName, Logger.LEVEL.LUDICROUS);
            if (parameterize) {
                HashMap properties = expandParameterizedSQLParams(sql, params);
                if (properties != null) {
                    sql = properties.get("PARAMETERIZEDSQL").toString();
                    args = (ArrayList) properties.get("ARGUMENTLIST");
                }
            } else {
                sql = expandSqlParams(sql, params, quoteParams);
            }

        }

        if (parameterize) {
            if (args != null && sql != null && !sql.isEmpty()) {
                ret = runUpdate(sql, args, conn, logFileName, timeoutInSeconds);
            }
        } else {
            Logger.logMessage("About to execute '" + sql + "'", logFileName, Logger.LEVEL.LUDICROUS); // NOTE: DON't LOG SPID HERE BECAUSE COULD BE NULL
            ret = runUpdate(sql, conn, logFileName, timeoutInSeconds);
            Logger.logMessage("runUpdate returned: " + ret, logFileName, Logger.LEVEL.DEBUG);
        }

        return ret;
    }

    /**
     * This is only meant to be used with the DynamicSQL class.
     * @param sql the rawsql statement with {X}'s
     * @param params a list of params to replace the {X}s
     * @param conn a connection. can be null
     * @param logFileName what file to log too.
     * @param timeoutInSeconds how long to set the query timeout
     * @return
     */
    public int dynamicSerializeUpdate(String sql, Object[] params, JDCConnection conn, String logFileName, int timeoutInSeconds) {
        int ret = -1;
        ArrayList args = new ArrayList();
        if (params != null) {
            Logger.logMessage("About to insert parameters into '" + sql + "'", logFileName, Logger.LEVEL.LUDICROUS);
                HashMap properties = expandParameterizedSQLParams(sql, params);
                if (properties != null) {
                    sql = properties.get("PARAMETERIZEDSQL").toString();
                    args = (ArrayList) properties.get("ARGUMENTLIST");
                }

        }
        if (args != null && sql != null && !sql.isEmpty()) {
            ret = runUpdate(sql, args, conn, logFileName, timeoutInSeconds);
        }

        return ret;
    }


    //END SERIALIZEUPDATE WITH OVERLOADS

    private void returnConnection(JDCConnection conn, String logFileName) {
        try {
            if (conn.getAutoCommit()) {
                if(JDCConnectionPool.isConnectionLoggingEnabled){
                    Logger.logMessage("Calling pool.returnConnection() SPID: "+conn.getSPID(),logFileName, Logger.LEVEL.DEBUG);
                }
                pool.returnConnection(conn, logFileName);
            }
        } catch (SQLException ex){
            Logger.logException("DataManager.returnConnection(" + conn.getSPID() + ") raised an error",logFileName, ex);
        }
    }

}