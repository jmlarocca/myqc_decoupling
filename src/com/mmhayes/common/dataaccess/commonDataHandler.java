package com.mmhayes.common.dataaccess;

//mmhayes dependencies

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import io.swagger.util.Json;
import jdk.nashorn.internal.parser.JSONParser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;


/*
 $Author: wjlesniak $: Author of last commit
 $Date: 2021-09-02 12:13:15 -0400 (Thu, 02 Sep 2021) $: Date of last commit
 $Rev: 15258 $: Revision of last commit
 Notes: Data Handler containing common functionality other data managers use
*/
public abstract class commonDataHandler {
    //using old DataManager in 7.0.2
    protected DataManager dm = new DataManager();
    protected commonMMHFunctions commDM = new commonMMHFunctions();

    //BEGIN getRecordsFilterLocation method + related methods
    //-----------------------------------------------------------------------------------

    // Get the records which the users has access to based on view permission for the location
    public ArrayList<HashMap> getRecordsFilterLocation(HttpServletRequest request, DataHandlerProperties prop) {
        // Check for page view permissions first
        if (!commDM.getPagePermissionByType(request, "VIEWPERMISSION")) {
            ArrayList<HashMap> errorCodeList = new ArrayList<HashMap>();
            errorCodeList.add(commDM.commonErrorMessage("Access Denied.\nNo view permissions for this page."));
            Logger.logMessage("ERROR: No view permissions for user " + commDM.checkSession(request) + ". Rejecting get records request.", Logger.LEVEL.ERROR);
            return errorCodeList;
        }
        ArrayList<HashMap> records = new ArrayList<HashMap>();
        String permissionSql = "data.common.checkUsePermission";
        // Assign different SQL to inject into the query based on the permission type specified (char because switch on strings isn't implemented until java 1.7)
        switch (prop.getPartitionPermissionType()) {
            case 'V': permissionSql = "data.common.checkRecordViewPermission";
                break;
            case 'E': permissionSql = "data.common.checkRecordEditPermission";
                break;
            // Case 'U' is the default
            default: permissionSql = "data.common.checkRecordUsePermission";
                break;
        }
        // Inject the permission check as the first argument to the query that will be run, but add in the user ID from the session first
        prop.addGetArguments(dm.getSQLProperty(permissionSql, commonMMHFunctions.prependObjectArray(new ArrayList<Object>(), commDM.checkSession(request)), false), 0);
        try {
            // Get the records (including any children)
            records = getRecords(request, prop);
        } catch (Exception e){
            Logger.logMessage("Error retrieving records with SQL Method : " + prop.getGetSQL(), Logger.LEVEL.ERROR);
            commonMMHFunctions.logStackTrace(e);
            return commDM.commonErrorMessageList("There was an unexpected error could not retrieve records");
        }
        // Return the full record set
        return records;
    }

    // Get the records which the users has access to based on the company
    public ArrayList<HashMap> getRecordsFilterCompany(HttpServletRequest request, DataHandlerProperties prop) {
        // Check for page view permissions first
        if (!commDM.getPagePermissionByType(request, "VIEWPERMISSION")) {
            ArrayList<HashMap> errorCodeList = new ArrayList<HashMap>();
            errorCodeList.add(commDM.commonErrorMessage("Access Denied.\nNo view permissions for this page."));
            Logger.logMessage("ERROR: No view permissions for user " + commDM.checkSession(request) + ". Rejecting get records request.", Logger.LEVEL.ERROR);
            return errorCodeList;
        }
        ArrayList<HashMap> records = new ArrayList<HashMap>();
        // Inject the permission check as the first argument to the query that will be run, but add in the user ID from the session first
        prop.addGetArguments(dm.getSQLProperty("data.common.checkRecordCompanyPermission", commonMMHFunctions.prependObjectArray(new ArrayList<Object>(), commDM.checkSession(request)), false), 0);
        try {
            // Get the records (including any children)
            records = getRecords(request, prop);
        } catch (Exception e){
            Logger.logMessage("Error retrieving records with SQL Method : " + prop.getGetSQL(), Logger.LEVEL.ERROR);
            commonMMHFunctions.logStackTrace(e);
            return commDM.commonErrorMessageList("There was an unexpected error could not retrieve records");
        }
        // Return the full record set
        return records;
    }

    //-----------------------------------------------------------------------------------
    //END getRecordsFilterLocation method + overloaded methods

    public ArrayList<HashMap> getRecordsFilterAccountGroup(HttpServletRequest request, DataHandlerProperties prop){
        ArrayList<HashMap>  records = new ArrayList<HashMap>();
        String getSQL = prop.getGetSQL();
        ArrayList arguments = prop.getGetArguments();
        Boolean useQuotes = prop.getGetRecordUseSingleQuotes();
        Boolean afterGetRecordsHook = prop.getUseAfterGetHook();
        try{
            String userID = commDM.checkSession(request);
            if(!userID.isEmpty()){
                Logger.logMessage("Running method "+getSQL+"......", Logger.LEVEL.TRACE);
                HttpSession session = request.getSession();

                String payRollGroups = commDM.getUserPayRollGroupsString(request); //add ownership group filtering to where clause
                try{
                    DynamicSQL dynamicSQL = new DynamicSQL(getSQL);
                    if(payRollGroups.equals("-1")){
                        dynamicSQL.addConditionalClause(1, "", "", "<>", "0");
                    }else if(payRollGroups.isEmpty()){
                        return new ArrayList<HashMap>();
                    }else{
                        dynamicSQL.addConditionalClause(1, "", "", "IN", payRollGroups);
                    }
                    int i = 2;
                    Object[] arr = arguments.toArray();
                    for(Object o : arr){
                        dynamicSQL.addValue(i, o);
                        i++;
                    }
                    records = dynamicSQL.serialize(dm);
                }catch (Exception e){
                    Logger.logMessage("Error running sql : " + getSQL, Logger.LEVEL.ERROR);
                    return commDM.commonErrorMessageList("Could not retrieve records with Exception : " + e.toString());
                }
            }

            //defined on the individual DM levels for if additional work needs to be done after retrieving records
            if(afterGetRecordsHook){
                afterGetRecordsFilterOwnership(records, prop,request);
            }

            try {
                // Check the properties object for children
                ArrayList<DataHandlerProperties> children = prop.getChildren();
                if (children.size() > 0) {
                    // Loop through the children
                    for (DataHandlerProperties child : children) {
                        if (child.getPreventAttachRecords()) {
                            continue;
                        }

                        // Attach data for any children
                        records = attachRelatedRecords(request, records, child);
                    }
                }
            } catch (Exception e) {
                Logger.logMessage("Error retrieving child records.", Logger.LEVEL.ERROR);
                commonMMHFunctions.logStackTrace(e);
                return commDM.commonErrorMessageList("There was an unexpected error could not retrieve child records");
            }

        }catch (Exception e){
            return commDM.commonErrorMessageList("Unexpected error Exception : " + e.toString());
        }
        return records;
    }

    //BEGIN getRecordsFilterOwnership method + overloaded methods
    //-----------------------------------------------------------------------------------

    public ArrayList<HashMap> getRecordsFilterOwnership(HttpServletRequest request, DataHandlerProperties prop){
        ArrayList<HashMap>  records = new ArrayList<HashMap>();
        String getSQL = prop.getGetSQL();
        ArrayList arguments = prop.getGetArguments();
        Boolean useQuotes = prop.getGetRecordUseSingleQuotes();
        Boolean afterGetRecordsHook = prop.getUseAfterGetHook();
        try{
            String userID = commDM.checkSession(request);
            if(!userID.isEmpty()){
                Logger.logMessage("Running method "+getSQL+"......", Logger.LEVEL.TRACE);
                HttpSession session = request.getSession();
                String ownershipGroups;
                if(session.getAttribute("QC_USEROWNERSHIPGROUPS") != null){
                    ownershipGroups = session.getAttribute("QC_USEROWNERSHIPGROUPS").toString().replace("[","").replace("]","");
                }else{
                    ownershipGroups =commDM.userOwnershipSuperStringList(userID);
                }

                try{
                    DynamicSQL dynamicSQL = new DynamicSQL(getSQL);
                    if(ownershipGroups.equals("-1")){
                        dynamicSQL.addConditionalClause(1, "", "", "<>", "0");
                    }else if(ownershipGroups.isEmpty()){
                        return new ArrayList<HashMap>();
                    }else{
                        dynamicSQL.addConditionalClause(1, "", "", "IN", ownershipGroups);
                    }
                    int i = 2;
                    Object[] arr = arguments.toArray();
                    for(Object o : arr){
                        dynamicSQL.addValue(i, o);
                        i++;
                    }

                    if (prop.getUseBeforeGetHook()) {
                        hookBeforeGet(request, prop);
                    }

                    records = dynamicSQL.serialize(dm);
                }catch (Exception e){
                    Logger.logMessage("Error running sql : " + getSQL, Logger.LEVEL.ERROR);
                    return commDM.commonErrorMessageList("Could not retrieve records with Exception : " + e.toString());
                }
            }

            //defined on the individual DM levels for if additional work needs to be done after retrieving records
            if(afterGetRecordsHook){
                afterGetRecordsFilterOwnership(records, prop,request);
            }

            try {
                // Check the properties object for children
                ArrayList<DataHandlerProperties> children = prop.getChildren();
                if (children.size() > 0) {
                    // Loop through the children
                    for (DataHandlerProperties child : children) {
                        if (child.getPreventAttachRecords()) {
                            continue;
                        }

                        // Attach data for any children
                        records = attachRelatedRecords(request, records, child);
                    }
                }
            } catch (Exception e) {
                Logger.logMessage("Error retrieving child records.", Logger.LEVEL.ERROR);
                commonMMHFunctions.logStackTrace(e);
                return commDM.commonErrorMessageList("There was an unexpected error could not retrieve child records");
            }

        }catch (Exception e){
            return commDM.commonErrorMessageList("Unexpected error Exception : " + e.toString());
        }
        return records;
    }

    //-----------------------------------------------------------------------------------
    //END getRecordsFilterOwnership method + overloaded methods



    //BEGIN getRecords method + overloaded methods
    //------------------------------------------------------------------------------------

    /**
     * This function allows for filtering out records that a user does not have access to through their RC mappings
     * @param request
     * @param objectID
     * @param prop this only allows for extra value type parameters. DynamicSQL is generated based off of this prop
     * @return
     */
    public ArrayList<HashMap> getRecordsFilterRevenueCenters(HttpServletRequest request,final String objectID, DataHandlerProperties prop){
        ArrayList<HashMap> records = new ArrayList<HashMap>();
        String userID = commDM.checkSession(request);
        String sqlString = prop.getGetSQL();
//        if(prop.isDynamicSQL()){//DynamicSQL needs to already have the filterprops set because we cant add them to the beginnning here
//            prop.isDynamicSQL()
//        }else{
            if(!userID.isEmpty()){
                String revCenters = commDM.ArrayListToCSV(commDM.getUserRevenueCentersStringList(request));
                if(revCenters.isEmpty()){
                    revCenters = "''";
                }
                final String revcenterArg = revCenters;
                try {

                    //create the newArgList with the objectTypeID and user's revenue centers
                    ArrayList<Object> newArgList = new ArrayList<Object>(){{add(objectID);add(revcenterArg);}};
                    DynamicSQL dynamicSQL = new DynamicSQL(sqlString);
                    dynamicSQL.addValue(1, objectID);
                    dynamicSQL.addIDList(2, revcenterArg);

//                    //add all the currentArgs in the currentArgList to the end of the newArgList
                    ArrayList<Object> currentArgList = prop.getGetArguments();
                    if (currentArgList != null) {
                        int i = 3;
                        for (Object currentArg : currentArgList) {
                            if (currentArg != null) {
                                dynamicSQL.addValue(i, currentArg);
                                i++;
                            }
                        }
                    }
                    prop.setDynamicSQL(dynamicSQL);//NOTE: this overwrites the old getParams

                    records = getRecords(request,prop);
                }catch (Exception e){
                    Logger.logMessage("Error retrieiving records filtered on revenue centers with SQL String : " + sqlString+" and Object Type ID : " + objectID, Logger.LEVEL.ERROR);
                    commDM.logStackTrace(e);
                    return commDM.commonErrorMessageList("There was an unexpected error could not retrieve records");

                }
            }else{
                return  commDM.commonErrorMessageList("Unable to determine user credentials, cannot retrieve records");
            }
//        }
        //defined on the individual DM levels for if additional work needs to be done after retrieving records
        if(prop.getUseAfterGetHook()){
            hookAfterGet(request,prop,records);
        }
        return records;
    }
    /**
     * This function places the arguments that filter recrods based on revenue center access into a DynamicSQL object.
     * The arguments will be placed at the next 2 consecutive locations within dynamic sql.
     * @param dynamicSQL
     * @param request
     * @param objectID
     */
    public void addDynamicRevenueFilter(DynamicSQL dynamicSQL, HttpServletRequest request, String objectID){
        ArrayList<HashMap> records = new ArrayList<HashMap>();
        String userID = commDM.checkSession(request);
        int last = dynamicSQL.getLastParameterSet();
        last++;
        dynamicSQL.addValue(last, objectID);
        last++;
        if(!userID.isEmpty()){
            ArrayList<String> revCenters = commDM.getUserRevenueCentersStringList(request);

            dynamicSQL.addIDList(last, revCenters);
        }else{
            dynamicSQL.addIDList(last, "");
        }
    }
    //BEGIN getRecordsFilterTerminals method + overloaded methods
    //-----------------------------------------------------------------------------------

    public ArrayList<HashMap> getRecordsFilterTerminals(HttpServletRequest request, DataHandlerProperties prop){
        ArrayList<HashMap>  records = new ArrayList<HashMap>();
        String getSQL = prop.getGetSQL();
        ArrayList arguments = prop.getGetArguments();
        Boolean useQuotes = prop.getGetRecordUseSingleQuotes();
        Boolean afterGetRecordsHook = prop.getUseAfterGetHook();
        try{
            String userID = commDM.checkSession(request);
            if(!userID.isEmpty()){
                Logger.logMessage("Running method "+getSQL+"......", Logger.LEVEL.TRACE);
                String terminalIDs = "";
                if (userID.equals("1")) {
                    terminalIDs = commDM.getUserTerminalCSV(request,true);
                } else {
                    terminalIDs = commDM.getUserTerminalCSV(request,false);
                }
                try{
                    String sql;
                    if(terminalIDs.equals("-1")){
                        sql="<> 0";
                    }else if(terminalIDs.isEmpty()){
                        return new ArrayList<HashMap>();
                    }else{
                        sql = "IN("+terminalIDs+")";
                    }
                    arguments.add(0,sql);
                    Object[] arr = arguments.toArray();
                    records = dm.serializeSqlWithColNames(getSQL, arr,useQuotes);
                }catch (Exception ex){
                    Logger.logMessage("Error running sql : " + getSQL, Logger.LEVEL.ERROR);
                    Logger.logException(ex); //always log exceptions
                    return commDM.commonErrorMessageList("Could not retrieve records with Exception : " + ex.toString());
                }
            }

            //defined on the individual DM levels for if additional work needs to be done after retrieving records
            if(afterGetRecordsHook){
                hookAfterGet(request,prop,records);
            }

            try {
                // Check the properties object for children
                ArrayList<DataHandlerProperties> children = prop.getChildren();
                if (children.size() > 0) {
                    // Loop through the children
                    for (DataHandlerProperties c : children) {
                        // Attach data for any children
                        records = attachRelatedRecords(request, records, c);
                    }
                }
            } catch (Exception ex) {
                Logger.logMessage("Error retrieving child records in getRecordsFilterTerminals.", Logger.LEVEL.ERROR);
                Logger.logException(ex); //always log exceptions
                return commDM.commonErrorMessageList("There was an unexpected error could not retrieve child records");
            }

          }catch (Exception ex){
            Logger.logException(ex); //always log exceptions
            return commDM.commonErrorMessageList("Unexpected error Exception : " + ex.toString());
        }
        return records;
    }

    //-----------------------------------------------------------------------------------
    //END getRecordsFilterTerminals method + overloaded methods


    // Get records without filtering based on a properties object instead of class-level variables (includes children, if any)
    //SQLI Vulnerability this can be unsafe when dynamically adding sql fragments with user input into DHProps and NOT setting quote or parameterize to true
    //TODO: It would be nice to more tightly couple this(or a version of this) with DynamicSQL to prevent abuse
    public final ArrayList<HashMap> getRecords(HttpServletRequest request, DataHandlerProperties prop) {
        ArrayList<HashMap> records = new ArrayList<HashMap>();
        // Set user object permissions
        prop.setPermission(commonMMHFunctions.checkSession(request));
        if (!prop.getPermission('V')) { // Check object permissions
            Logger.logMessage("Warning: Requested records without the appropriate permission " + (prop.getComponentId() == null ? "because there is no component" : "from component ID " + prop.getComponentId().toString()), Logger.LEVEL.ERROR);
            return commDM.commonErrorMessageList("Could not retrieve records due to insufficient privileges.");
        }
        // Allow for a hook to execute before getting the records
        if (prop.getUseBeforeGetHook()) {
            hookBeforeGet(request, prop);
        }
        Logger.logMessage("Running SQL Method : " + prop.getGetSQL(), Logger.LEVEL.TRACE);
        try {
            if(prop.isDynamicSQL()){//the new proper way to set dynamic sql
                records = dm.dynamicSerializeSqlWithColNames(prop.getDynamicSQL(), prop.getGetArguments().toArray());
            }else{
                records = dm.serializeSqlWithColNames(prop.getGetSQL(), prop.getGetArguments().toArray(), prop.getGetRecordUseSingleQuotes(), prop.getParameterizeGetValues());
            }
        } catch (Exception e) {
            Logger.logMessage("Error retrieving records with SQL Method : " + prop.getGetSQL(), Logger.LEVEL.ERROR);
            commDM.logStackTrace(e);
            return commDM.commonErrorMessageList("There was an unexpected error could not retrieve records");
        }
        // Allow for a hook to execute after getting the records, but before any children are processed
        if (prop.getUseAfterGetHook()) {
            records = hookAfterGet(request, prop, records);
        }
        try {
            // Check the properties object for children
            ArrayList<DataHandlerProperties> children = prop.getChildren();
            if (children.size() > 0) {
                // Loop through the children
                for (DataHandlerProperties child : children) {
                    // Attach data for any children if the get SQL is defined
                    if (child.getGetSQL().trim().length() > 0 && !child.getPreventAttachRecords()) {
                        records = attachRelatedRecords(request, records, child);
                    }
                }
            }
        } catch (Exception e) {
            Logger.logMessage("Error retrieving child records.", Logger.LEVEL.ERROR);
            commonMMHFunctions.logStackTrace(e);
            return commDM.commonErrorMessageList("There was an unexpected error could not retrieve child records");
        }
        // Return the full record set
        return records;
    }


    /**
     * WARNING: DO NOT USE THIS METHOD WITH DYNAMIC QUERIES GENERATED FROM USER INPUT. That is how we become vulnerable to SQLInjection.
     * This method is intended only for server grids, which are properly generating a dynamic query WITHOUT user input.
     * There is no other way to dynamically add complex query clauses without dynamically generating the sql.
     * No part of the query can have un-whitelisted user input.
     * @param sqlQuery a dynamic query with custom filters added.
     * @param request
     * @param prop this method MUST FORCE parameterization of this prop. The param list should not be empty. If it is, you are likely introducing a vulnerability
     * @return
     */
    public final ArrayList<HashMap> getServerGridRecords(String sqlQuery, HttpServletRequest request, DataHandlerProperties prop) {
        ArrayList<HashMap> records = new ArrayList<HashMap>();
        // Set user object permissions
        prop.setPermission(commonMMHFunctions.checkSession(request));
        //FORCE parameterization, otherwise we are vulnerable
        prop.setParameterizeGetValues(true);
        if(prop.getGetArguments().size() == 0){
            Logger.logMessage("Error retrieving child records. This method is not designed for queries with no parameters", Logger.LEVEL.ERROR);
            return commDM.commonErrorMessageList("There was an unexpected error could not retrieve records");
        }

        if (!prop.getPermission('V')) { // Check object permissions
            Logger.logMessage("Warning: Requested records without the appropriate permission " + (prop.getComponentId() == null ? "because there is no component" : "from component ID " + prop.getComponentId().toString()), Logger.LEVEL.ERROR);
            return commDM.commonErrorMessageList("Could not retrieve records due to insufficient privileges.");
        }
        // Allow for a hook to execute before getting the records
        if (prop.getUseBeforeGetHook()) {
            hookBeforeGet(request, prop);
        }
        Logger.logMessage("Running SQL Method : " + prop.getGetSQL() + "with overwritten query: " + sqlQuery, Logger.LEVEL.TRACE);
        try {
            //Custom implementation of dm.serializeSqlWithColNames that uses the query itself, not the prop name.
            //Its done this way insead of creating another function override because passing in a dynamic query really is a BAD idea, and we want to do it little as possible
            records = dm.dynamicSerializeSqlWithColNames(sqlQuery, prop.getGetArguments().toArray());
        } catch (Exception e) {
            Logger.logMessage("Error retrieving records with SQL Method : " + prop.getGetSQL(), Logger.LEVEL.ERROR);
            commDM.logStackTrace(e);
            return commDM.commonErrorMessageList("There was an unexpected error could not retrieve records");
        }
        // Allow for a hook to execute after getting the records, but before any children are processed
        if (prop.getUseAfterGetHook()) {
            records = hookAfterGet(request, prop, records);
        }
        try {
            // Check the properties object for children
            ArrayList<DataHandlerProperties> children = prop.getChildren();
            if (children.size() > 0) {
                // Loop through the children
                for (DataHandlerProperties child : children) {
                    // Attach data for any children if the get SQL is defined
                    if (child.getGetSQL().trim().length() > 0 && !child.getPreventAttachRecords()) {
                        records = attachRelatedRecords(request, records, child);
                    }
                }
            }
        } catch (Exception e) {
            Logger.logMessage("Error retrieving child records.", Logger.LEVEL.ERROR);
            commonMMHFunctions.logStackTrace(e);
            return commDM.commonErrorMessageList("There was an unexpected error could not retrieve child records");
        }
        // Return the full record set
        return records;
    }


    public final HashMap getRecord(HttpServletRequest request, DataHandlerProperties prop) {
        ArrayList<HashMap> records = getRecords(request, prop);
        HashMap retHM = new HashMap();
        if (records.size() > 0) {
            if (records.get(0).getClass().equals(HashMap.class)) {
                retHM = records.get(0);
            }
        }
        return retHM;
    }

    public ArrayList findSelectObject(Integer objectID, HttpServletRequest request,String classMethodName,String getSQL, String objectName){
        try
        {
            Logger.logMessage("Running method "+classMethodName+"..", Logger.LEVEL.DEBUG);
            if (!commDM.checkSession(request).equals("")) { //check that user is logged in
                return dm.serializeSqlWithColNames(getSQL, new Object[]{new Integer(objectID)});
            } else {
                return commDM.commonErrorMessageList("Invalid Access.");
            }
        }
        catch (Exception ex)
        {
            Logger.logMessage("Error within method "+classMethodName+":"+ex.toString(), Logger.LEVEL.ERROR);
            return commDM.commonErrorMessageList("Error trying to find "+objectName+" ID#:"+objectID);
        }
    }

    // Attach records as an ArrayList<Hashmap> when the primary records have matches based on the query
    public ArrayList<HashMap> attachRelatedRecords(HttpServletRequest request, ArrayList<HashMap> inputArray, DataHandlerProperties prop) {
        // Loop through all the records
        for (HashMap record : inputArray) {
            // Check to see if a child record belongs based on matches to the field that links to the parent
            if (record.containsKey(prop.getParentIdentifier())) {
                // Clear any existing get arguments
                prop.clearGetArguments();
                // Add in arguments from the child property object
                String primaryIdentifier = record.get(prop.getParentIdentifier()).toString();
                prop.addGetArguments(primaryIdentifier, 0);
                if (prop.getLanguageId() != null) {
                    prop.addGetArguments(prop.getLanguageId().toString(), 1);
                }
                try {
                    boolean getChildRecords = true;
                    ArrayList<HashMap> childRecordList = new ArrayList<HashMap>();
                    //check if a max child size is set
                    if ( prop.getMaxChildSize() != null && !prop.getCountSQL().equals("") ) {
                        Object[] params = new Object[] {
                            primaryIdentifier
                        };
                        Integer childCount = Integer.parseInt(dm.parameterizedExecuteScalar(prop.getCountSQL(),params).toString());

                        if ( childCount > prop.getMaxChildSize() ) {
                            getChildRecords = false;
                            record.put(prop.getNameField().concat("_COUNT"), childCount);
                        }
                    }

                    if ( getChildRecords ) {
                        if (prop.getUseBeforeGetHook()) {
                            hookBeforeGet(request, prop);
                        }

                        childRecordList = getRecords(request, prop);
                    }

                    record.put(prop.getNameField(), childRecordList);
                } catch (Exception e) {
                    Logger.logMessage("Error retrieving related records to " + prop.getParentIdentifier() + ": " + record.get(prop.getParentIdentifier()) + " with SQL Method : " + prop.getGetSQL(), Logger.LEVEL.ERROR);
                    commonMMHFunctions.logStackTrace(e);
                    continue;
                }
            }
        }
        // Return the original data with the child data attached
        return inputArray;
    }
    //-----------------------------------------------------------------------------------------
    //END getRecords method + overloaded methods

    protected final ArrayList<HashMap> saveRecordsFilterOwnership(ArrayList<HashMap> records,HttpServletRequest request, DataHandlerProperties prop){
        // @param records : the records to save
        // @param request : the request that initiated this function call (and therefore the request to used
        //                  to filter the records based on what the user owns)
        // @param prop : the properties containing the relevant sql information
        //
        // Saves the given list of records after filtering them to ensure the user has access to everything in
        // the records they are trying to save

        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        ArrayList<HashMap> saveableRecords = new ArrayList<>();
        ArrayList<HashMap> userOwnershipGroups = commDM.getUserOwnershipGroupHashMap(request);

        // Filter the locations
        try {
            for (HashMap record : records) {
                String recordID = record.get(prop.getPrimaryIdentifier()).toString(); //this may contain SQLI
                boolean saveable = false;
                String recordOwnershipID;
                if(record.get("OWNERSHIPGROUPID") != null){
                    //TODO: I wonder if one could modify the data coming from the front end and change this to save things they dont have access to?
                    recordOwnershipID = record.get("OWNERSHIPGROUPID").toString();
                }
                //otherwise we have to ping the database
                else{
                    try{
                        if(prop.getGetOwnershipSQL() != null && !prop.getGetOwnershipSQL().isEmpty()){
                            recordOwnershipID = new DynamicSQL(prop.getGetOwnershipSQL())
                                    .addValue(1,recordID)
                                    .getSingleField(dm).toString();
                        }else{
                            recordOwnershipID = new DynamicSQL("data.generic.getOwnershipID")
                                    .addTableORColumnName(1, prop.getTableName())
                                    .addTableORColumnName(2, prop.getPrimaryIdentifier())
                                    .addValue(3,recordID)
                                    .getSingleField(dm).toString();
                        }
                    }catch (final Exception ex){
                        Logger.logException(ex);
                        errorCodeList.add(commDM.commonErrorMessage("ERROR: Unable to validate ownership group access to be saved."));
                        return errorCodeList;
                    }

                }
                for(int i =0;i<userOwnershipGroups.size();i++){
                    if(!recordOwnershipID.isEmpty() && recordOwnershipID.equals(userOwnershipGroups.get(i).get("OWNERSHIPGROUPID").toString())){
                        saveable = true;
                        break;
                    }
                }
                if(saveable){
                   saveableRecords.add(record);
                }else{
                    errorCodeList.add(commDM.commonErrorMessage("Could not save record changes, user does not have appropriate ownership group privileges"));
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex);
            errorCodeList.add(commDM.commonErrorMessage("Unable to validate ownership group access to be saved."));
            return errorCodeList;
        }

        // Call saveRecords unless there are location validation errors
        if (errorCodeList.isEmpty()) {
            return saveRecords(request, prop, saveableRecords);
        } else {
            return errorCodeList;
        }
    }

    protected final ArrayList<HashMap> saveRecords(HttpServletRequest request, DataHandlerProperties prop, ArrayList<HashMap> records) {
        // BEFORE YOU USE THIS - do you really want to use this? Why not saveRecordsFilterOwnership()?
        // You may have a use case where you want to use this, but just be aware that there is absolutely no filtering
        // based on what the user should actually have access to change vs what they are trying to change
        //
        // @param request : the request that initiated this function call
        // @param prop : the properties containing the relevant sql data
        // @param records : the records to save
        //
        // Saves a given list of records

        // Testing functionality
        if (prop.getTestingApplication()) {
            Date date = new Date();
            writeMessage(date.toString() + " - working with new batch of records");
        }
        // Log progress
        Logger.logMessage("Begin processing save record for table " + prop.getTableName() + ", session ID: " + request.getSession().getId(), Logger.LEVEL.DEBUG);
        // Set permissions for later object permissions check
        prop.setPermission(commonMMHFunctions.checkSession(request));
        // Add in the company ID for the record
        prop.setCacheCompanyId(commDM.getCompanyId(request));
        // Run the data validation, fields will be shared between all actions
        prop.setDataValidation();
        // Declare variables
        ArrayList<HashMap> errorCodeList = new ArrayList<HashMap>();
        HashMap<String, HashMap> newlySavedHM = new HashMap();

        ArrayList<HashMap> hardDeletions = new ArrayList<>();
        ArrayList<HashMap> insertedRecords = new ArrayList<>();

        if (prop.getUseBeforeCompleteSaveHook()) {
            hookBeforeCompleteSave(request, prop, records);
        }

        // Iterate through the records
        for (HashMap r : records) {
            // Allow for a hook to execute before saving the records
            if (prop.getUseBeforeSaveHook()) {
                r = hookBeforeSave(request, prop, r);
            }
            // Set up variable for the saved record
            HashMap saveRecord = new HashMap();
            // Check for, remove and log an undefined field
            if (!prop.getValidatedColumns().containsKey(commDM.UNDEFINED) && r.containsKey(commDM.UNDEFINED)) {
                Logger.logMessage("ERROR: field \"" + commDM.UNDEFINED + "\" found in the record with data: " + r.get(commDM.UNDEFINED).toString(), Logger.LEVEL.ERROR);
                Logger.logMessage("Removing the \"" + commDM.UNDEFINED + "\" field...", Logger.LEVEL.ERROR);
                r.remove(commDM.UNDEFINED);
            }
            // Check for deleted flags
            boolean isDeleted = (r.containsKey(commDM.DELETED) && r.get(commDM.DELETED).toString().compareToIgnoreCase("1") == 0);
            // Check for edited flag (all child records for a single parent record are sent when any of the children are edited)
            boolean isEditedFlag = (r.containsKey(commDM.CHILD_EDITED_FLAG) && r.get(commDM.CHILD_EDITED_FLAG).toString().compareToIgnoreCase("1") == 0);
            // Remove automatically determined fields if they somehow made it into the request
            r.remove(commDM.COMPANYID);
            r.remove(commDM.RECORDACTIONTIMESTAMP);
            r.remove(commDM.RECORDACTIONUSER);
            r.remove(commDM.RECORDACTION);
            // Remove fields that could appear in the data, but only if they do not
            if (!prop.getValidatedColumns().containsKey(commDM.ARRAYLISTINDEX)) r.remove(commDM.ARRAYLISTINDEX);
            if (!prop.getValidatedColumns().containsKey(commDM.DELETED) && !prop.getHardDelete()) r.remove(commDM.DELETED);
            if (!prop.getValidatedColumns().containsKey(commDM.CHILD_EDITED_FLAG)) r.remove(commDM.CHILD_EDITED_FLAG);
            // Add in the logging columns that the user would never have the opportunity to set
            if (prop.getValidatedColumns().containsKey(commDM.RECORDACTIONUSER)) r.put(commDM.RECORDACTIONUSER, commDM.getUserId(request));
            if (prop.getValidatedColumns().containsKey(commDM.RECORDACTIONTIMESTAMP)) r.put(commDM.RECORDACTIONTIMESTAMP, commDM.getCurrentUTCDateTime());
            // Find and cache the child records (treated differently with updates/inserts/deletes
            r = isolateChildren(r, prop);
            // Reserve certain fields for specific logging
            String recordId = (r.containsKey(prop.getPrimaryIdentifier()) ? r.get(prop.getPrimaryIdentifier()).toString() : "");
            String recordName = (r.containsKey(prop.getNameField()) ? r.get(prop.getNameField()).toString() : "");
            boolean recordExists = false;
            // Determine if record exists prior to attempting to update/insert
            if (prop.getNegativeNewRecordLookup()) {
                try {
                    recordExists= Integer.parseInt(recordId) > 0;
                } catch (Exception e) {
                   errorCodeList.add(commDM.commonErrorMessage("Unable to determine if insert or update for Record ID : " + recordId));
                }
            } else {
                recordExists=commDM.checkRecordExists(prop, r);
            }
            if (recordExists) { // Record exists - update (or delete)
                ArrayList<HashMap> updateErrorCodeList = new ArrayList<HashMap>();
                // For each primary key, cache to key/value pair in the properties object and remove from the main parameters
                prop.clearKeyValues();
                for (String primaryKey : prop.getPrimaryKeys()) {
                    if (r.containsKey(primaryKey)) {
                        // Add the key and value to the stuff
                        prop.addKeyValues(primaryKey, r.get(primaryKey));
                        // Remove the key from the main record
                        r.remove(primaryKey);
                    } else {
                        Logger.logMessage("Record missing a primary key!", Logger.LEVEL.ERROR);
                    }
                }
                if (isDeleted) { // Check if the record is deleted, if so soft-delete
                    if (!prop.getPermission('D')) { // Check object permissions
                        Logger.logMessage("Warning: Deleting record without the appropriate object permission " + (prop.getComponentId() == null ? "because there is no component" : "on component ID " + prop.getComponentId().toString()), Logger.LEVEL.ERROR);
                        errorCodeList.add(commDM.commonErrorMessage("Could not delete record due to insufficient privileges."));
                        continue;
                    }
                    if (prop.getHardDelete()) {
                        // Hard delete the records without validating, assumes no child records exist

                        // add detail information for the hardDeletions ArrayList
                        HashMap rCopy = new HashMap(r);
                        rCopy.put(prop.getPrimaryIdentifier(), prop.getPrimaryIdentifierValue().toString());
                        hardDeletions.add(rCopy);

                        errorCodeList.addAll(commDM.updateRecord(prop, r));
                        continue;
                    } else {
                        if (prop.getCheckPermissions() && prop.getPermissionCache().containsKey(commDM.DELETEPERMISSION) && !(Boolean)prop.getPermissionCache().get(commDM.DELETEPERMISSION)) {
                            // The user does not have delete permissions so this record will be skipped
                            errorCodeList.add(commDM.commonErrorMessage("Attempted to delete record without delete permissions.\nDeletion rejected for ID: " + recordId + (recordName.length() > 0 ? " Name: " + recordName : "")));
                            Logger.logMessage("ERROR: No delete permissions for user " + commDM.checkSession(request) + ". Rejecting save records request.", Logger.LEVEL.ERROR);
                            Logger.logMessage(r.toString(), Logger.LEVEL.ERROR);
                            continue; // No further processing on this record in the loop
                        }
                        r.clear(); // Remove all elements (keys have already been removed)
                        if (prop.getValidatedColumns().containsKey(commDM.RECORDACTIONUSER)) r.put(commDM.RECORDACTIONUSER, commDM.getUserId(request)); // Re-add
                        if (prop.getValidatedColumns().containsKey(commDM.RECORDACTIONTIMESTAMP)) r.put(commDM.RECORDACTIONTIMESTAMP, commDM.getCurrentUTCDateTime()); // Re-add
                        if (prop.getValidatedColumns().containsKey(commDM.RECORDACTION)) r.put(commDM.RECORDACTION, commDM.LOG_DELETE_FLAG); // Assign the record action of D for delete
                        Logger.logMessage("Processing record for soft-delete...", Logger.LEVEL.DEBUG);
                    }
                } else if (prop.getParentIdentifier().length() > 0 && !isEditedFlag) { // This is a child record that was not edited
                    r.clear(); // Remove all the elements so that we don't process as having data to update
                } else { // Update the record (do not delete)
                    if (!prop.getPermission('E')) { // Check object permissions
                        Logger.logMessage("Warning: Editing record without the appropriate object permission " + (prop.getComponentId() == null ? "because there is no component" : "on component ID " + prop.getComponentId().toString()), Logger.LEVEL.ERROR);
                        errorCodeList.add(commDM.commonErrorMessage("Could not edit record due to insufficient privileges."));
                        continue;
                    }
                    if (prop.getValidatedColumns().containsKey(commDM.RECORDACTION)) r.put(commDM.RECORDACTION, commDM.LOG_UPDATE_FLAG); // Assign the record action of U for update
                    Logger.logMessage("Processing record for update...", Logger.LEVEL.DEBUG);
                }
                if (!isDeleted && prop.getCheckPermissions() && prop.getPermissionCache().containsKey(commDM.EDITPERMISSION) && !(Boolean)prop.getPermissionCache().get(commDM.EDITPERMISSION)) {
                    // The user does not have edit permissions so this record will be skipped
                    errorCodeList.add(commDM.commonErrorMessage("Attempted to edit record without edit permissions.\nEdit rejected for ID: " + recordId + (recordName.length() > 0 ? " Name: " + recordName : "")));
                    Logger.logMessage("ERROR: No edit permissions for user " + commDM.checkSession(request) + ". Rejecting save records request.", Logger.LEVEL.ERROR);
                    Logger.logMessage(r.toString(), Logger.LEVEL.ERROR);
                    continue; // No further processing on this record in the loop
                }
                // Additional logging when bulk testing
                if (prop.getTestingApplication() && prop.getTestingOutputAll()) {
                    writeMessage("---------------------------------------------------------------------");
                    writeMessage("Trying to update record");
                    writeRecordToFile(r);
                }
                // Allow for a hook to execute before updating the records
                if (prop.getUseBeforeUpdateHook()) {
                    errorCodeList.addAll(hookBeforeUpdate(request, prop, r));
                }
                // Validate the record only once all of the data has been manipulated
                HashMap returnObj = commDM.validateRecord(prop.getDataValidation(), prop.getValidatedColumns(), r);
                // Include the validation information in the record to be saved
                if (returnObj.containsKey("validatedRecord")) {
                    saveRecord = (HashMap)returnObj.get("validatedRecord");
                }
                // Include any validation errors
                if (returnObj.containsKey("errorCodeList")) {
                    updateErrorCodeList.addAll((ArrayList<HashMap>)returnObj.get("errorCodeList"));
                }
                // Only if all the validation passed and there's data to actually change, save the record
                if (updateErrorCodeList.isEmpty() && (isDeleted || prop.getIsMappingObject() || commDM.recordHasDataColumns(saveRecord))) {
                    updateErrorCodeList.addAll(commDM.updateRecord(prop, saveRecord));
                } else if (!updateErrorCodeList.isEmpty() && prop.getTestingApplication()) {
                    writeMessage("Unable to update previous entry, errors in data validation");
                    writeMessage("Failed fields : " + updateErrorCodeList.toString());
                    writeMessage("On record : " + saveRecord );
                    writeMessage("---------------------------------------------------------------------\n");
                }
                // Run through any child records after the main record, regardless if the update was successful
                if ( !prop.getChildren().isEmpty() ) {
                    ArrayList<HashMap> saveChildrenResult = saveChildren(request, prop, prop.getPrimaryIdentifierValue());

                    if ( prop.isAsyncSave() && saveChildrenResult.size() == 1 && saveChildrenResult.get(0).containsKey("newlySaved") ) {
                        HashMap<String, HashMap> newlySavedChildResult = saveChildrenResult.get(0);
                        HashMap<String, HashMap> newlySavedChildResultValueHM = newlySavedChildResult.get("newlySaved");

                        HashMap savedRecordHM = new HashMap();
                        for ( HashMap.Entry<String, HashMap> entry: newlySavedChildResultValueHM.entrySet()) {
                            savedRecordHM.put(entry.getKey(), entry.getValue());
                        }

                        savedRecordHM.put(prop.getPrimaryIdentifier(), recordId);
                        newlySavedHM.put(recordId, savedRecordHM);

                        saveChildrenResult.remove(0);
                    }

                    updateErrorCodeList.addAll(saveChildrenResult);
                }
                // Allow for a hook to execute after the records have been inserted
                if (prop.getUseAfterUpdateHook() && updateErrorCodeList.isEmpty()) {
                    updateErrorCodeList.addAll(hookAfterUpdate(request, prop, r));
                }

                errorCodeList.addAll(updateErrorCodeList);
            } else { // Record doesn't exist - insert
                if (!isDeleted) { // Verify that the record isn't set to be deleted (if it is it will be ignored)
                    if (!prop.getPermission('A')) { // Check object permissions
                        Logger.logMessage("Warning: Adding record without the appropriate object permission " + (prop.getComponentId() == null ? "because there is no component" : "on component ID " + prop.getComponentId().toString()), Logger.LEVEL.ERROR);
                        errorCodeList.add(commDM.commonErrorMessage("Could not add record due to insufficient privileges."));
                        continue;
                    }
                    if (prop.getCheckPermissions() && prop.getPermissionCache().containsKey(commDM.ADDPERMISSION) && !(Boolean)prop.getPermissionCache().get(commDM.ADDPERMISSION)) {
                        // The user does not have add permissions so this record will be skipped
                        errorCodeList.add(commDM.commonErrorMessage("Attempted to add record without add permissions.\nAdd rejected for ID: " + recordId + (recordName.length() > 0 ? " Name: " + recordName : "")));
                        Logger.logMessage("ERROR: No add permissions for user " + commDM.checkSession(request) + ". Rejecting save records request.", Logger.LEVEL.ERROR);
                        Logger.logMessage(r.toString(), Logger.LEVEL.ERROR);
                        continue; // No further processing on this record in the loop
                    }
                    if (prop.getValidatedColumns().containsKey(commDM.RECORDACTION) && !r.isEmpty()) r.put(commDM.RECORDACTION, commDM.LOG_ADD_FLAG); // Assign the record action of A for add
                    Logger.logMessage("Processing record for insert...", Logger.LEVEL.DEBUG);
                    // Insert the record
                    ArrayList<HashMap> insertErrorCodeList = new ArrayList<HashMap>();
                    // Additional logging when bulk testing
                    if (prop.getTestingApplication() && prop.getTestingOutputAll()) {
                        writeMessage("---------------------------------------------------------------------");
                        writeMessage("Trying to insert record");
                        writeRecordToFile(r);
                    }
                    // Add in the company ID if applicable
                    if (prop.getValidatedColumns().containsKey(commDM.COMPANYID)) r.put(commDM.COMPANYID, prop.getCacheCompanyId());
                    // Allow for a hook to execute before inserting the records
                    if (prop.getUseBeforeInsertHook()) {
                        insertErrorCodeList.addAll(hookBeforeInsert(request, prop, r));
                    }
                    // Remove the primary key value if it's the only key (assuming a single key will be determined by the database for inserts)
                    if (prop.getPrimaryKeys().size() == 1) {
                        r.remove(prop.getPrimaryIdentifier());
                    }
                    // Validate the record only once all of the data has been manipulated
                    HashMap returnObj = commDM.validateRecord(prop.getDataValidation(), prop.getValidatedColumns(), r);
                    // Include the validation information in the record to be saved
                    if (returnObj.containsKey("validatedRecord")) {
                        saveRecord = (HashMap)returnObj.get("validatedRecord");
                    }
                    // Include any validation errors
                    if (returnObj.containsKey("errorCodeList")) {
                        insertErrorCodeList.addAll((ArrayList<HashMap>)returnObj.get("errorCodeList"));
                    }
                    // Only if all the validation passed, save the record
                    if (insertErrorCodeList.isEmpty() && !saveRecord.isEmpty()) {
                        insertErrorCodeList.addAll(commDM.addRecord(prop, saveRecord));
                    } else if (!insertErrorCodeList.isEmpty() && prop.getTestingApplication()) {
                        writeMessage("Unable to insert previous entry, errors in data validation");
                        writeMessage("Failed fields : " + insertErrorCodeList.toString());
                        writeMessage("On record : " + saveRecord );
                        writeMessage("---------------------------------------------------------------------\n");
                    }
                    // Get the ID of the inserted record from the return of the insert record method, and remove it from the error code list
                    String lastRecordID = "";
                    for (int i = 0; i < insertErrorCodeList.size(); i++) {
                        HashMap h = insertErrorCodeList.get(i);
                        if (h.get("LASTRECORDID") != null) {
                            lastRecordID = h.get("LASTRECORDID").toString();
                            h.remove("LASTRECORDID");
                            if (prop.getTestingApplication() && prop.getTestingOutputAll()) {
                                writeMessage("Previous was correctly inserted");
                                writeMessage("---------------------------------------------------------------------\n");
                            }
                            if (h.size() == 0) {
                                insertErrorCodeList.remove(i);
                            }
                        }
                    }
                    // More logging if the testing application is enabled
                    if (lastRecordID.length() == 0 && prop.getTestingApplication()) {
                        writeMessage("Unable to insert previous entry , errors interacting with database");
                        writeMessage(saveRecord.toString());
                        writeMessage("---------------------------------------------------------------------\n");
                    }
                    // Run through any child records after the main record, only if the insert was successful
                    if (lastRecordID.length() > 0) {
                        ArrayList<HashMap> saveChildrenResult = saveChildren(request, prop, lastRecordID);

                        if ( prop.isAsyncSave() && saveChildrenResult.size() == 1 && saveChildrenResult.get(0).containsKey("newlySaved") ) {
                            HashMap<String, HashMap> newlySavedChildResult = saveChildrenResult.get(0);
                            HashMap<String, HashMap> newlySavedChildResultValueHM = newlySavedChildResult.get("newlySaved");

                            HashMap savedRecordHM = new HashMap();
                            for ( HashMap.Entry<String, HashMap> entry: newlySavedChildResultValueHM.entrySet()) {
                                savedRecordHM.put(entry.getKey(), entry.getValue());
                            }

                            newlySavedHM.put(recordId, savedRecordHM);

                            saveChildrenResult.remove(0);
                        }

                        insertErrorCodeList.addAll(saveChildrenResult);
                    }

                    // Allow for a hook to execute after the records have been inserted
                    if (prop.getUseAfterInsertHook() && insertErrorCodeList.isEmpty()) {
                        // add inserted record to ArrayList of inserted records
                        HashMap rCopy = new HashMap(r);
                        rCopy.put("INSERTEDID", lastRecordID);
                        insertedRecords.add(rCopy);

                        insertErrorCodeList.addAll(hookAfterInsert(request, prop, r, lastRecordID));
                    }

                    if ( prop.isAsyncSave() && insertErrorCodeList.isEmpty() ) {
                        HashMap savedRecordHM = new HashMap();

                        //for datahandlers that contain children
                        if ( newlySavedHM.containsKey(recordId) ) {
                            savedRecordHM = newlySavedHM.get(recordId);
                        }
                        savedRecordHM.put(prop.getPrimaryIdentifier(), lastRecordID);
                        newlySavedHM.put(recordId,savedRecordHM);
                    }

                    errorCodeList.addAll(insertErrorCodeList);
                }
            }
        }
        // Allow for a hook to execute after saving the records
        if (prop.getUseAfterSaveHook()) {
            errorCodeList.addAll(hookAfterSave(request, prop, errorCodeList));
        }
        // allow hook to execute to capture removed detail records
        if (prop.getUseGetDeletionsHook()) {
            errorCodeList.addAll(hookForDeletions(request, prop, hardDeletions));
        }
        // allow for hook to execute after inserting the records
        if (prop.getUseAfterCompleteInsertHook()) {
            errorCodeList.addAll(hookAfterCompleteInsert(request, prop, insertedRecords));
        }
        // Testing functionality
        if (prop.getTestingApplication()) {
            writeMessage("Finished working with batch\n");
        }

        //put new recordIds hashmap into errorCodeList for async saves
        if ( prop.isAsyncSave() && errorCodeList.isEmpty() ) {
            HashMap asyncReturn = new HashMap();
            asyncReturn.put("newlySaved", newlySavedHM);
            errorCodeList.add(asyncReturn);
        }

        Logger.logMessage("Completed processing save record for table " + prop.getTableName() + ", session ID: " + request.getSession().getId(), Logger.LEVEL.DEBUG);
        // Return the results of the save operation
        return errorCodeList;
    }

    protected final ArrayList<HashMap> saveChildren(HttpServletRequest request, DataHandlerProperties prop, Object parentValue) {
        ArrayList<HashMap> errorCodeList = new ArrayList<HashMap>();
        HashMap<String, HashMap> AsyncReturn = new HashMap();
        boolean doAsyncReturn = false;
        // Check for any children (removed from the main record)
        if (prop.getMappingArrayLists().size() > 0) {
            Iterator iter = prop.getMappingArrayLists().entrySet().iterator();
            // Loop through the cached records
            while (iter.hasNext()) {
                Map.Entry child = (Map.Entry)iter.next();
                // Find the matching child properties object
                DataHandlerProperties childProp = new DataHandlerProperties();
                for (DataHandlerProperties cp : prop.getChildren()) {
                    if (child.getKey().toString().compareToIgnoreCase(cp.getNameField()) == 0 && cp.getParentIdentifier().toString().compareToIgnoreCase(prop.getPrimaryIdentifier()) == 0) {
                        childProp = cp;
                    }
                }
                if (childProp.getParentIdentifier().toString().length() > 0) {
                    // Only add in parent record ID if there was a generated or existing value (i.e. greater than 0)
                    if (commonMMHFunctions.tryParseLong(parentValue.toString()) && Long.parseLong(parentValue.toString()) > 0) {
                        // Add record ID from parent
                        for (HashMap c : (ArrayList<HashMap>)child.getValue()) {
                            c.put(childProp.getParentIdentifier().toString(), parentValue.toString());
                        }
                    }
                    // Process child records
                    Logger.logMessage("Begin processing child records for table " + prop.getTableName() + ", session ID: " + request.getSession().getId(), Logger.LEVEL.DEBUG);
                    errorCodeList.addAll(saveRecords(request, childProp, (ArrayList<HashMap>) child.getValue()));

                    //adjust for async saving - returning new IDs
                    if ( childProp.isAsyncSave() && errorCodeList.size() == 1 && errorCodeList.get(0).containsKey("newlySaved") ) {
                        doAsyncReturn = true;

                        HashMap<String, String> newlySaved = (HashMap<String, String>) errorCodeList.get(0).get("newlySaved");
                        AsyncReturn.put(child.getKey().toString(), newlySaved);

                        errorCodeList.remove(0);
                    }
                }
            }
        }

        if ( doAsyncReturn && AsyncReturn.size() > 0) {
            HashMap<String, HashMap> returnHM = new HashMap();
            returnHM.put("newlySaved", AsyncReturn);
            errorCodeList.add(returnHM);
        }
        // Return an empty ArrayList if there aren't any records to process
        return errorCodeList;
    }

    protected final ArrayList<HashMap> saveRecordsFilterLocation(HttpServletRequest request, DataHandlerProperties prop, ArrayList<HashMap> records) {
        ArrayList<HashMap> errorCodeList = new ArrayList<HashMap>();
        ArrayList<String> validLocations = new ArrayList<String>();

        // Cache the page permissions
        prop.setPermissionCache(commDM.getPageSavePermissionHashMap(request));

        // Get the user's valid locations
        try {
            ArrayList<Object> params = new ArrayList<Object>();
            params.add(commDM.checkSession(request));
            ArrayList<HashMap> tempLocations = dm.serializeSqlWithColNames("data.common.getSaveLocations", params.toArray(), false);
            for (HashMap m : tempLocations) {
                if (m.containsKey(commDM.LOCATIONID) && m.get(commDM.LOCATIONID) != null) {
                    validLocations.add(m.get(commDM.LOCATIONID).toString());
                }
            }
        } catch (Exception e) {
            Logger.logMessage("ERROR: Unable to retrieve user locations: " + e.getMessage(), Logger.LEVEL.ERROR);
            errorCodeList.add(commDM.commonErrorMessage("Unable to determine valid locations."));
            return errorCodeList;
        }

        // Filter the locations
        try {
            for (HashMap m : records) {
                if (m.containsKey(commDM.LOCATIONID)) {
                    if (m.get(commDM.LOCATIONID) != null && m.get(commDM.LOCATIONID).toString().replaceAll("\\s","").length() > 0) {
                        String loc = m.get(commDM.LOCATIONID).toString();
                        boolean isValid = false;
                        for (String validLoc : validLocations) {
                            if (validLoc.compareToIgnoreCase(loc) == 0) {
                                isValid = true;
                            }
                        }
                        if (!isValid) {
                            Logger.logMessage("Invalid location attempted to be saved: " + loc, Logger.LEVEL.ERROR);
                            errorCodeList.add(commDM.commonErrorMessage("Attempting to save a record with a location that you do not have access to. Save skipped."));
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.logMessage("ERROR: Unable to validate user locations: " + e.getMessage(), Logger.LEVEL.ERROR);
            errorCodeList.add(commDM.commonErrorMessage("Unable to validate locations to be saved."));
            return errorCodeList;
        }

        // Call saveRecords unless there are location validation errors
        if (errorCodeList.isEmpty()) {
            return saveRecords(request, prop, records);
        } else {
            return errorCodeList;
        }
    }

    protected final ArrayList<HashMap> insertRecords(HttpServletRequest request, DataHandlerProperties prop, ArrayList<HashMap> records) {
        // Set up the return value, which is a list of errors
        ArrayList<HashMap> errorCodeList = new ArrayList<HashMap>();
        // Validate that the data to be inserted matches columns on this table
        DataValidation dv = new DataValidation(prop.getTableName());
        HashMap<String,HashMap> columns = dv.getValidationData();
        boolean includeCompanyId = (columns.containsKey(commDM.COMPANYID) ? true : false);
        try {
            // Iterate through all the records, inserts are processed one at a time
            for (HashMap cur : records) {
                // Set up the error code list for the single record
                ArrayList<HashMap> localErrorCodeList = new ArrayList<HashMap>();
                // Additional logging when bulk testing
                if (prop.getTestingApplication() && prop.getTestingOutputAll()) {
                    writeMessage("---------------------------------------------------------------------");
                    writeMessage("Trying to insert record");
                    writeRecordToFile(cur);
                }
                // Find and cache the child records
                cur = isolateChildren(cur, prop);
                // Remove the company ID if somehow it made it through the request
                cur.remove(commDM.COMPANYID);
                // Add in the company ID if applicable
                if (includeCompanyId) {
                    cur.put(commDM.COMPANYID, commDM.getCompanyId(request));
                }
                /*// Add in the logging columns that the user would never have the opportunity to set, prior to data validation
                if (prop.getLoggedObject()) {
                    cur.put(commDM.RECORDACTIONTIMESTAMP, commDM.getCurrentUTCDateTime());
                    cur.put(commDM.RECORDACTIONUSER, commDM.getUserId(request));
                    cur.put(commDM.RECORDACTION, "A");
                }*/
                // Allow for a hook to execute before inserting the records
                if (prop.getUseBeforeInsertHook()) {
                    hookBeforeInsert(request, prop, cur);
                }
                // Always remove the primary key value which is determined by the database for inserts
                cur.remove(prop.getPrimaryIdentifier());
                // Set up variables for the saved record and the object returned
                HashMap saveRecord = new HashMap();
                HashMap returnObj = new HashMap();
                // Validate per specific database
                if (MMHProperties.getAppSetting("database.type").compareToIgnoreCase("ORACLE") == 0) {
                    returnObj = dv.validateDataOracle(cur, columns);
                } else {
                    returnObj = dv.validateDataSQL(cur, columns);
                }
                // Include the validation information
                if (returnObj.containsKey("validatedRecord")) {
                    saveRecord = (HashMap)returnObj.get("validatedRecord");
                }
                // Include any validation errors
                if (returnObj.containsKey("errorCodeList")) {
                    localErrorCodeList.addAll((ArrayList<HashMap>)returnObj.get("errorCodeList"));
                }
                // Only if all the validation passed, save the record
                if (localErrorCodeList.isEmpty()) {
                    //localErrorCodeList.addAll(commDM.addRecord(prop.getInsertSQL(), saveRecord, prop.getPrimaryIdentifier(), ""));
                    localErrorCodeList.addAll(commDM.addRecord(prop, saveRecord));
                } else if (!localErrorCodeList.isEmpty() && prop.getTestingApplication()) {
                    writeMessage("Unable to insert previous entry, errors in data validation");
                    writeMessage("Failed fields : " + localErrorCodeList.toString());
                    writeMessage("On record : " + saveRecord );
                    writeMessage("---------------------------------------------------------------------");
                    writeMessage("\n");
                }
                // Get the ID of the inserted record from the return of the insert record method, and remove it from the error code list
                String lastRecordID = "";
                for (int i = 0; i < localErrorCodeList.size(); i++) {
                    HashMap h = localErrorCodeList.get(i);
                    if (h.get("LASTRECORDID") != null) {
                        lastRecordID = h.get("LASTRECORDID").toString();
                        h.remove("LASTRECORDID");
                        if (prop.getTestingApplication() && prop.getTestingOutputAll()) {
                            writeMessage("Previous was correctly inserted");
                            writeMessage("---------------------------------------------------------------------");
                            writeMessage("\n");
                        }
                        if (h.size() == 0) {
                            localErrorCodeList.remove(i);
                        }
                    }
                }
                // More logging if the testing application is enabled
                if (lastRecordID.isEmpty() && prop.getTestingApplication()) {
                    writeMessage("Unable to insert previous entry , errors interacting with database");
                    writeMessage(saveRecord.toString());
                    writeMessage("---------------------------------------------------------------------");
                    writeMessage("\n");
                }
                // Run through any child records after the main record insert to process
                if (prop.getMappingArrayLists().size() > 0) {
                    Iterator iter = prop.getMappingArrayLists().entrySet().iterator();
                    while (iter.hasNext()) {
                        Map.Entry child = (Map.Entry)iter.next();
                        // Find the matching child properties object
                        DataHandlerProperties childProp = new DataHandlerProperties();
                        for (DataHandlerProperties cp : prop.getChildren()) {
                            if (child.getKey() == cp.getNameField() && cp.getParentIdentifier() == prop.getPrimaryIdentifier()) {
                                childProp = cp;
                            }
                        }
                        if (childProp.getParentIdentifier().length() > 0) {
                            // Add record ID from parent
                            for (HashMap c : (ArrayList<HashMap>)child.getValue()) {
                                c.put(childProp.getParentIdentifier().toString(), lastRecordID);
                            }
                            // Process child records
                            //localErrorCodeList.addAll(insertRecords(request, childProp, (ArrayList<HashMap>)child.getValue()));
                        }
                    }
                }
                // Allow for a hook to execute after the records have been inserted
                if (prop.getUseAfterInsertHook() && localErrorCodeList.isEmpty()) {
                    localErrorCodeList.addAll(hookAfterInsert(request, prop, cur, lastRecordID));
                }
                errorCodeList.addAll(localErrorCodeList);
            }
        } catch (Exception e) {
            Logger.logMessage("Unexpected error on one of these records : " + records.toString(), Logger.LEVEL.ERROR);
            Logger.logMessage("Insert exception : " + e.toString(), Logger.LEVEL.ERROR);
            errorCodeList.add(commDM.commonErrorMessage("Unexpected error on inserting record"));
        }
        return errorCodeList;
    }

    protected final ArrayList<HashMap> updateRecords(HttpServletRequest request, DataHandlerProperties prop, ArrayList<HashMap> records) {
        // Set up the return value, which is a list of errors
        ArrayList<HashMap> errorCodeList = new ArrayList<HashMap>();
        // Validate that the data to be inserted matches columns on this table
        DataValidation dv = new DataValidation(prop.getTableName());
        HashMap<String,HashMap> columns = dv.getValidationData();
        try {
            // Iterate through all the records, inserts are processed one at a time
            for (HashMap cur : records) {
                // Set up the error code list for the single record
                ArrayList<HashMap> localErrorCodeList = new ArrayList<HashMap>();
                // Get the key to update on for this record
                String recordID = cur.get(prop.getPrimaryIdentifier()).toString();
                // Additional logging when bulk testing
                if (prop.getTestingApplication() && prop.getTestingOutputAll()) {
                    writeMessage("---------------------------------------------------------------------");
                    writeMessage("Trying to update record");
                    writeRecordToFile(cur);
                }
                // Find and cache the child records
                cur = isolateChildren(cur, prop);
                // Remove the company ID if somehow it made it through the request
                cur.remove(commDM.COMPANYID);
                // Process any child records prior to the main record update
                // TODO: Process any child records prior to the main record update
                // Add in the logging columns that the user would never have the opportunity to set, prior to data validation
                /*if (prop.getLoggedObject()) {
                    cur.put(commDM.RECORDACTIONTIMESTAMP, commDM.getCurrentUTCDateTime());
                    cur.put(commDM.RECORDACTIONUSER, commDM.getUserId(request));
                    cur.put(commDM.RECORDACTION, "U");
                }*/
                // Allow for a hook to execute before inserting the records
                if (prop.getUseBeforeUpdateHook()) {
                    hookBeforeUpdate(request, prop, cur);
                }

                // Remove the primary key value because it will be used to key the update
                cur.remove(prop.getPrimaryIdentifier());
                // Set up variables for the saved record and the object returned
                HashMap saveRecord = new HashMap();
                HashMap returnObj = new HashMap();
                // Validate per specific database
                if (MMHProperties.getAppSetting("database.type").compareToIgnoreCase("ORACLE") == 0) {
                    returnObj = dv.validateDataOracle(cur, columns);
                } else {
                    returnObj = dv.validateDataSQL(cur, columns);
                }
                // Include the validation information
                if (returnObj.containsKey("validatedRecord")) {
                    saveRecord = (HashMap)returnObj.get("validatedRecord");
                }
                // Include any validation errors
                if (returnObj.containsKey("errorCodeList")) {
                    localErrorCodeList.addAll((ArrayList<HashMap>)returnObj.get("errorCodeList"));
                }
                // Only if all the validation passed, save the record
                if (localErrorCodeList.isEmpty()) {
                    localErrorCodeList.addAll(commDM.updateRecord(prop, saveRecord));
                }
                // Output information to the log
                if (localErrorCodeList.isEmpty() && prop.getTestingApplication() && prop.getTestingOutputAll()) {
                    writeMessage("Previous was correctly updated");
                    writeMessage("---------------------------------------------------------------------");
                    writeMessage("\n");
                } else if (!localErrorCodeList.isEmpty() && prop.getTestingApplication()) {
                    writeMessage("Failed fields : " + localErrorCodeList.toString());
                    writeMessage("On record : " + saveRecord.toString() );
                    writeMessage("Previous was not updated, with errors");
                    writeMessage("---------------------------------------------------------------------");
                    writeMessage("\n");
                }
                // Allow for a hook to execute after the records have been updated
                if (prop.getUseAfterUpdateHook() && localErrorCodeList.isEmpty()) {
                    localErrorCodeList.addAll(hookAfterUpdate(request, prop, cur));
                }
                errorCodeList.addAll(localErrorCodeList);
            }
        } catch (Exception e) {
            Logger.logMessage("Unexpected error on one of these records : " + records.toString(), Logger.LEVEL.ERROR);
            Logger.logMessage("Update exception : " + e.toString(), Logger.LEVEL.ERROR);
            errorCodeList.add(commDM.commonErrorMessage("Unexpected error on updating record"));
        }
        return errorCodeList;
    }

    // Utility method to find child records and return them (to be cached)
    protected final HashMap isolateChildren(HashMap record, DataHandlerProperties prop) {
        // Remove any existing mapping (old cache)
        prop.clearMappingArrayLists();
        // Loop through the items in the HashMap to see if any values are an ArrayList
        Iterator iter = record.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry e = (Map.Entry)iter.next();
            if (e.getValue() != null && e.getValue().getClass().equals(ArrayList.class)) {
                // When an ArrayList is present, add it to the mapping array and remove it from the values to be inserted
                prop.setMappingArrayLists(e.getKey().toString(), e.getValue());
                iter.remove();
            }
        }
        return record;
    }

    //Defined on the individual DM level, for doing tasks after retrieving records
    public ArrayList<HashMap> afterGetRecordsFilterOwnership(ArrayList<HashMap> records,DataHandlerProperties prop, HttpServletRequest request){
        return new ArrayList<HashMap>();
    };

    public boolean scrubDetail(HashMap cur,String detailID){
        return true;
    }

    private boolean writeRecordToFile(HashMap cur){
         PrintWriter out = null;
         try {
             out = new PrintWriter(new BufferedWriter(new FileWriter("C:\\IdeaProjects\\Fire Agate 2\\src\\com\\mmhayes\\quickcharge\\server\\dataaccess\\ROBTESTDATA.txt", true)));
             out.println("[");
             Iterator entries = cur.entrySet().iterator();
             while(entries.hasNext()){
                 Map.Entry thisEntry = (Map.Entry)entries.next();
                 String key = thisEntry.getKey().toString();
                 String value = thisEntry.getValue().toString();
                 out.println(key + " : " + value);
             }
             out.println("]");
         }catch (IOException e) {
             System.err.println(e);
             return false;
         }finally{
             if(out != null){
                 out.close();
             }
         }
         return true;
     }

    private boolean writeMessage(String message){
        PrintWriter out = null;
        try {
            out = new PrintWriter(new BufferedWriter(new FileWriter("C:\\IdeaProjects\\Fire Agate 2\\src\\com\\mmhayes\\quickcharge\\server\\dataaccess\\ROBTESTDATA.txt", true)));
            out.println(message);
        }catch (IOException e) {
            System.err.println(e);
            return false;
        }finally{
            if(out != null){
                out.close();
            }
        }
        return true;
    }

    /**
     * THIS METHOD IS EXTREAMLY VULNERABLE TO SQLI..
     * Basically everywhere that uses this is dynamically creating SQL statements that are unvalidated and not parameterized
     * @param loadData
     * @return
     */
    @Deprecated
    protected ArrayList<Object> getDynamicLoadingArgs(HashMap loadData){
        String likeArg = "";
        String IDsArg = "";
        String sizeArg;
        if(loadData.containsKey("IDS")){
            String ids = loadData.get("IDS").toString();
            if(ids.isEmpty()){
                IDsArg = "('')";
            }else{
                IDsArg = "("+ids+")";
            }
        }
        if(loadData.containsKey("LIKE")){
            likeArg = "'%"+loadData.get("LIKE").toString()+"%'";
        }
        if(loadData.containsKey("SIZE") && loadData.get("SIZE") != null){
            sizeArg = loadData.get("SIZE").toString();
        }else{
            sizeArg="200";
        }
        ArrayList<Object> args = new ArrayList<Object>();
        args.add(IDsArg);
        args.add(likeArg);
        args.add(sizeArg);
        return args;
    }

    /**
     * Adds the first 3 parameters for server side selector boxes
     * Secure version that goes through DynamicSQL.
     */
    public DynamicSQL getDynamicLoadingArgs(String property, HashMap loadData_DANGEROUS){
        //create a dynamic SQL statment
        DynamicSQL dynamicSQL = new DynamicSQL(property);
        //replace param {1}
        //TODO: check how ID param affects the indexes
        if(loadData_DANGEROUS.containsKey("IDS")) {
            dynamicSQL.addIDList(1, loadData_DANGEROUS.get("IDS").toString());
        }else{
            dynamicSQL.addIDList(1, "");
        }
        //replace param{2}
        if(loadData_DANGEROUS.containsKey("LIKE")){
            dynamicSQL.addLikeCondition(2, loadData_DANGEROUS.get("LIKE").toString());
        }else{
            dynamicSQL.addLikeCondition(2, "");//anything
        }
        //replace param{3}
        if(loadData_DANGEROUS.containsKey("SIZE") && loadData_DANGEROUS.get("SIZE") != null){
            dynamicSQL.addCount(3, loadData_DANGEROUS.get("SIZE").toString());
        }else{
            dynamicSQL.addCount(3, "200");
        }
        return dynamicSQL;
    }


//    /**
//     * This function is the secure version of ArrayList<Object> getDynamicLoadingArgs(HashMap loadData)
//     *
//     * @param loadData_DANGEROUS the data loaded from the frontend
//     * @param baseNum a starting number for the newly generated param arg list = the length of the parameter list
//     *      EX: existing query contains {1}{2} so base num would be 2
//     * @return This function generates 2 list, defined as follows:
//     * -----1----- @ 0
//     * It generates a set of dynamic SQL Fragements that should
//     * be inserted into the main sql, similarly to how it was previously being done.
//     * The generated param numbers will begin with baseNum
//     *
//     * -----2----- @ 1
//     * It generates a list of arguments that mirrors the results of parameterizeDynamicLoadingFragments
//     * Params are entered like so: LIKE @ 0th, SIZE @ 1st, IDs @ 2nd position and up.
//     */
//    //TODO: test that this is working!!!
//    protected ArrayList<ArrayList<Object>> parameterizeDynamicLoadingArgs(HashMap loadData_DANGEROUS, int baseNum){
//        baseNum++;//put into {X} column space
//        ArrayList<Object> sqlFragments = new ArrayList<>();
//        ArrayList<Object> args = new ArrayList<>();
//        StringBuilder likeFrag = new StringBuilder();
//        String IDsFrag = "";// = new StringBuilder();
//        StringBuilder sizeFrag = new StringBuilder();
//
//        //like
//        if(loadData_DANGEROUS.containsKey("LIKE")){
//            likeFrag.append("'%' + ").append('{').append(baseNum).append('}').append(" + '%'");
//            args.add(loadData_DANGEROUS.get("LIKE").toString());
//        }else{
//            likeFrag.append("'%%'");
//            args.add("");
//        }
//        sqlFragments.add(likeFrag);
//        //size (val or default)
//        sizeFrag.append('{').append(baseNum + 1).append('}');
//        if(loadData_DANGEROUS.containsKey("SIZE") && loadData_DANGEROUS.get("SIZE") != null){
//            args.add(loadData_DANGEROUS.get("SIZE").toString());
//        }else{
//            args.add("200");
//        }
//        sqlFragments.add(sizeFrag);
//
//        //ID list
//        if(loadData_DANGEROUS.containsKey("IDS")){//TODO: this
//            String ids = loadData_DANGEROUS.get("IDS").toString();
//            if(ids.isEmpty()){
//                IDsFrag = "('')";
//            }else{
//                //updates args in helper method and generates {X},{X+1},...
//                IDsFrag = "("+IDListToParameterizedList(ids, baseNum+2,args).toString()+")";
//            }
//        }
//        sqlFragments.add(IDsFrag);
//
//        ArrayList<ArrayList<Object>> parameterized = new ArrayList<ArrayList<Object>>();
//        parameterized.add(sqlFragments);
//        parameterized.add(args);
//        return parameterized;
//    }
//
//    /**
//     *
//     * @param ids input IDlist with just numbers and ","s
//     * @param startNumberingAt start creating sql params at {startNumberingAt}
//     * @param outParamList a param list to append to. Creates a new one if the reference is null.
//     *       You should NOT just pass in NULL, all the params will be lost...
//     * @return SQL Fragment string builder
//     */
//    //TOOD: use dynamicSQL class instead
//    private StringBuilder IDListToParameterizedList(String ids, int startNumberingAt, ArrayList<Object> outParamList){
//        String[] idList = ids.split(",");
//        StringBuilder sqlFrag = new StringBuilder();
//        if(outParamList == null)
//            outParamList = new ArrayList();
//        int i;
//        for(i = startNumberingAt; i < idList.length-2 + startNumberingAt; i++){//-2 to not include the last element
//            sqlFrag.append('{').append(i).append('}').append(',');
//            outParamList.add(idList[i-startNumberingAt]);
//        }
//        sqlFrag.append('{').append(i).append('}');//the last
//        outParamList.add(idList[i-startNumberingAt]);
//
//        return sqlFrag;
//    }


    public void StringArrayToString(HashMap record, String fieldName, String mapTo){
        StringBuilder builder = new StringBuilder();
        try{
            String[] arr = (String[])record.get(fieldName);
            String prefix="";
            for(int i=0;i<arr.length;i++){
                builder.append(prefix);
                prefix=",";
                builder.append(arr[i]);
            }
            record.remove(fieldName);
            String empIDs = "("+builder.toString()+")";
            record.put(mapTo,empIDs);
        }catch (Exception e){
            record.put(mapTo,"('')");
        }

    }

    //BEGIN object-based hooks which are overridden on the individual data handler level
    //-----------------------------------------------------------------------------------
    public void hookBeforeGet(HttpServletRequest request, DataHandlerProperties prop) {
        return;
    }
    public ArrayList<HashMap> hookAfterGet(HttpServletRequest request, DataHandlerProperties prop, ArrayList<HashMap> inArray) {
        return inArray;
    }

    public HashMap hookBeforeSave(HttpServletRequest request, DataHandlerProperties prop, HashMap record) {
        return record;
    }

    public void hookBeforeCompleteSave (HttpServletRequest request, DataHandlerProperties prop, ArrayList<HashMap> records) {}

    /**
     * This function allows for handeling after save
     * @param request
     * @param prop
     * @param errors existing error code list
     * @return any aditional errors created.
     */
    public ArrayList<HashMap> hookAfterSave(HttpServletRequest request, DataHandlerProperties prop, ArrayList<HashMap> errors) {
        return new ArrayList<HashMap>();
    }
    public ArrayList<HashMap> hookForDeletions (HttpServletRequest request, DataHandlerProperties prop, ArrayList<HashMap> records) {
        return new ArrayList<HashMap>();
    }
    public ArrayList<HashMap> hookBeforeInsert(HttpServletRequest request, DataHandlerProperties prop, HashMap record) {
        return new ArrayList<HashMap>();
    }
    public ArrayList<HashMap> hookAfterInsert(HttpServletRequest request, DataHandlerProperties prop, HashMap record, String insertedId) {
        return new ArrayList<HashMap>();
    }
    public ArrayList<HashMap> hookAfterCompleteInsert(HttpServletRequest request, DataHandlerProperties prop, ArrayList<HashMap> records) {
        return new ArrayList<HashMap>();
    }
    public ArrayList<HashMap> hookBeforeUpdate(HttpServletRequest request, DataHandlerProperties prop, HashMap record) {
        return new ArrayList<HashMap>();
    }
    public ArrayList<HashMap> hookAfterUpdate(HttpServletRequest request, DataHandlerProperties prop, HashMap record) {
        return new ArrayList<HashMap>();
    }
    //-----------------------------------------------------------------------------------
    //END object-based hooks which are overridden on the individual data handler level

//START - Paginate Record Methods

    public ArrayList paginateRecords(HttpServletRequest request, HashMap postParams, String countSQLStatement, String recordsSQLStatement, String defaultSortCol, String defaultSortOrder, boolean filterOwnerShip, boolean filterAccountGroups, String QueryID) {
        return paginateRecords(request, postParams, countSQLStatement, recordsSQLStatement, defaultSortCol, defaultSortOrder, filterOwnerShip, filterAccountGroups, false, QueryID);
    }

    public ArrayList paginateRecords(HttpServletRequest request, HashMap postParams, String countSQLStatement, String recordsSQLStatement, String defaultSortCol, String defaultSortOrder, boolean filterOwnerShip, boolean filterAccountGroups, boolean filterHyperfind, String QueryID) {
        DataHandlerProperties dataProp = new DataHandlerProperties();
        dataProp.setGetSQL(recordsSQLStatement);
        dataProp.setCountSQL(countSQLStatement);
        ServerGridProperties gridProp = new ServerGridProperties(postParams);
        gridProp.setGridFlags(filterOwnerShip, filterAccountGroups, filterHyperfind);
        gridProp.setDefaultSort(defaultSortCol, defaultSortOrder);
        if (filterAccountGroups || filterOwnerShip) {
            gridProp.addCustomFilter("REVENUECENTERIDS", "searchString IN (SELECT * FROM fnQC_StringArrayConvert(RevenueCenterIDs))");
            gridProp.addCustomFilter("VENDORIDS", "searchString IN (SELECT * FROM fnQC_StringArrayConvert(VendorIDs))");
            gridProp.addCustomFilter("PRINTERIDS", "searchString IN (SELECT * FROM fnQC_StringArrayConvert(PrinterIDs))");
            gridProp.addCustomFilter("QC_SYNC_STATUS", "QC_SYNC_STATUS = searchString");
            gridProp.addCustomFilter("POS_SYNC_STATUS", "POS_SYNC_STATUS = searchString");
        }
        return paginateRecords(request, dataProp, gridProp);
    }

    /**
     * WARNING: Custom Filters are a powerful tool for creating dynamic sql statements, but with great power comes great responsibility.
     * If you use ANY user input from post params in directly in your custom filters, you have introduced a SQLI vulnerablity
     * @param request
     * @param dataProp
     * @param gridProp
     * @return
     */
    public ArrayList paginateRecords(HttpServletRequest request, DataHandlerProperties dataProp, ServerGridProperties gridProp) {
        return paginateParameterizedRecordsWithKnownCount(request, dataProp, gridProp, -1);
    }

    /**
     * This function retrieves a page of records for a grid. This uses proper parameterization.
     * @param request
     * @param dataProp
     * @param gridProp
     * @param knownTotal the count sql will be ignored if this is positive. There is no point in recounting multiple times.
     * @return
     */
    public ArrayList paginateParameterizedRecordsWithKnownCount(HttpServletRequest request, DataHandlerProperties dataProp, ServerGridProperties gridProp, int knownTotal) {
        ArrayList<HashMap> paginatedRecords = new ArrayList<HashMap>();
        try {
            // Ensure that the user ID makes it into the grid object, overriding anything from the front-end
            gridProp.setUserId(commonMMHFunctions.checkSession(request));
            //declare pagination vars
            int rowsPerPage = Integer.parseInt(gridProp.getPostParams().get("rows").toString()); //how many rows per page is the user requesting?
            int requestedPage = Integer.parseInt(gridProp.getPostParams().get("page").toString()); //what page # is the user requesting?
            String sortCol_DANGEROUS = gridProp.getPostParams().get("sidx").toString(); //which column should we sort by
            String sortCol = null;//validated against SQLI
            String sortOrder_DANGEROUS = gridProp.getPostParams().get("sord").toString(); //what order should we sort by
            String sortOrder = null;//validated against SQLI
            if(sortCol_DANGEROUS != null){
                sortCol = commonMMHFunctions.sanitizeStringFromSQLInjection(sortCol_DANGEROUS, "");
            }
            if(sortOrder_DANGEROUS != null){
                sortOrder = commonMMHFunctions.sanitizeStringFromSQLInjection(sortOrder_DANGEROUS, "");
            }

            if (sortCol == null || sortCol.equals("")) { //if no user sortCol is passed, then use defaultSortCol
                sortCol = gridProp.getDefaultSortColumn();
            }
            if (sortOrder == null || sortCol.equals("")) { //if no user sortCol is passed, then use defaultSortOrder
                sortOrder = gridProp.getDefaultSortOrder();
            }
            double totalPages = 0; //how many pages will there be after running pagination query?
            //get where clause arguments
            ArrayList<Object> sqlArgsList = new ArrayList<Object>();
            //get where clause arguments that all must always be true (ignores groupOp)
//            ArrayList<Object> constraintArgsList = new ArrayList<>();
//            constraintArgsList = commDM.addConstraintArguments(request, gridProp);
            // Add the SQL parameters to argument list
            sqlArgsList.add(0, "");//placeholder for parameterized where clause
            sqlArgsList.add(1, "");//placeholder for parameterized where clause.
            sqlArgsList.add(2, sortCol);//placeholder for dynamically adding them to the query because we cant parameterize this
            sqlArgsList.add(3, sortOrder);//placeholder for dynamically adding them to the query because we cant parameterize this
            sqlArgsList.add(4, 0); // startingRecordNumber before it's determined
            sqlArgsList.add(5, 0); // endingRecordNumber before it's determined
            sqlArgsList.add(6, commDM.checkSession(request)); // User ID
            sqlArgsList.add(7, (gridProp.getQueryId() != null ? gridProp.getQueryId() : "-1")); // Hyperfind Query ID
            sqlArgsList.add(8, "NULL"); //sqlArgsList.add(hyperfindDate); // Hyperfind Apply Date
            //add the constraint args to the param list
            sqlArgsList.add(9, "");//placeholder for parameterized constraint where clause

            ArrayList<String> dynamicSQLArray = new ArrayList<>();
            int curIndex = 10;
            if (gridProp.getPostParams().get("dynamicSQL") != null) {
                try {
                    ObjectMapper jsonMapper = new ObjectMapper();
                    dynamicSQLArray = jsonMapper.readValue(gridProp.getPostParams().get("dynamicSQL").toString(), ArrayList.class);
                    for (String dynamicSQL : dynamicSQLArray) {
                        sqlArgsList.add(curIndex++, dynamicSQL);
                    }
                    curIndex = curIndex - dynamicSQLArray.size(); //reset index for future dynamicSQL shenanigans
                } catch (IOException ioe) {
                    Logger.logMessage("IO Exception when building dynamicSQL Array: " + ioe.getMessage());
                } catch (Exception e) {
                    Logger.logMessage("Exception when building dynamicSQL Array: " + e.getMessage());
                }
            }

            commDM.buildServerGridFilterArguments(request, gridProp, dataProp, sqlArgsList);
            String paramSQL = sqlArgsList.get(0).toString();
            String clauseSQL = sqlArgsList.get(1).toString();
            String constraintSQL = sqlArgsList.get(9).toString();
            //clear where the SQL was held so we dont try to paramaterize that
            sqlArgsList.set(0, "");
            sqlArgsList.set(1, "");
            //we now should have some clause SQL with param numbers matching the sqlArgList.

            //START - Pagination Logic
            //get the count of all the rows that would be returned by this query
            if(knownTotal < 0){
                String rawSQL = MMHProperties.getSqlString(dataProp.getCountSQL());
                //append our custom clause sql
                //this is basically the same thing as expandSQLParams(false)...
                rawSQL = rawSQL.replaceAll("\\{1\\}", paramSQL);
                rawSQL = rawSQL.replaceAll("\\{2\\}", clauseSQL);
                rawSQL = rawSQL.replaceAll("\\{10\\}", constraintSQL);
                //we cant parameterize column names, so manually enter sanitized params
                rawSQL = rawSQL.replaceAll("\\{3\\}", sortCol);
                rawSQL = rawSQL.replaceAll("\\{4\\}", sortOrder);
                // custom dynamic sql parameters - these need to be added  to the list of parameterized SQL args above; and ALSO replaced here
                for (String dynamicSQL : dynamicSQLArray) { // yeah the sqlArgs start at 0 and this replace stuff starts at 1 so we have to add 1 while incrementing
                    rawSQL = rawSQL.replaceAll("\\{" + String.valueOf(curIndex++ + 1) + "\\}", dynamicSQL);
                }
                curIndex = curIndex - dynamicSQLArray.size(); //reset index for future dynamicSQL shenanigans

                Object[] params = sqlArgsList.toArray();

                //perform the parameterization
                String paramaterizedSQL = "";
                ArrayList paramaterizedArgs = new ArrayList();
                HashMap properties = dm.expandParameterizedSQLParams(rawSQL, params);
                if (properties != null) {
                    paramaterizedSQL = properties.get("PARAMETERIZEDSQL").toString();
                    paramaterizedArgs = (ArrayList) properties.get("ARGUMENTLIST");
                }
                Logger.logMessage("Server Grid Counting: " + dataProp.getCountSQL());
                gridProp.setTotalRows(Integer.parseInt(dm.parameterizedGetSingleField(paramaterizedSQL, paramaterizedArgs).toString()));
            }else{
                gridProp.setTotalRows(knownTotal);
            }
            //determine the total pages
            if (gridProp.getTotalRows() > 0) {
                if (gridProp.getTotalRows() % rowsPerPage == 0) {
                    totalPages = gridProp.getTotalRows()/rowsPerPage;
                } else {  //round up and add page if totalRows is not divisible by rowsPerPage
                    totalPages = Math.ceil(gridProp.getTotalRows() / rowsPerPage);
                    totalPages = totalPages + 1;
                }
            }
            //if the total rows is greater than 0, we should have at least one page
            if (totalPages < 1 && gridProp.getTotalRows() > 0) {
                totalPages = 1;
            }

            //if the total rows is greater than 0, we should have at least one page
            gridProp.setTotalPages((int)totalPages);
            //determine starting and ending record indexes for pagination
            if (totalPages > 0) {
                gridProp.setEndingRecordNumber(rowsPerPage * requestedPage);
                gridProp.setStartingRecordNumber(gridProp.getEndingRecordNumber() - rowsPerPage);
            }
            //END - Pagination Logic
            // Re-add the record number parameters to the argument list
            sqlArgsList.set(4, gridProp.getStartingRecordNumber());
            sqlArgsList.set(5, gridProp.getEndingRecordNumber());
            dataProp.setGetArguments(sqlArgsList);
            dataProp.setParameterizeGetValues(true);//kindof redundant as getServerGridRecords forces this...

            String rawSQL = MMHProperties.getSqlString(dataProp.getGetSQL());
            //append our custom clause sql
            rawSQL = rawSQL.replaceAll("\\{1\\}", paramSQL);
            rawSQL = rawSQL.replaceAll("\\{2\\}", clauseSQL);
            rawSQL = rawSQL.replaceAll("\\{10\\}", constraintSQL);
            //we cant parameterize column names, so manually enter sanitized params
            rawSQL = rawSQL.replaceAll("\\{3\\}", sortCol);
            rawSQL = rawSQL.replaceAll("\\{4\\}", sortOrder);
            //append custom dynamic SQL - anything can go here
            for (String dynamicSQL : dynamicSQLArray) {
                rawSQL = rawSQL.replaceAll("\\{" + String.valueOf(curIndex++ + 1) + "\\}", dynamicSQL);
            }
            //retrieve the records for this page. This function parameterizes the call for us
            Logger.logMessage("Server Grid Fetching Results: " + dataProp.getGetSQL());
            paginatedRecords = getServerGridRecords(rawSQL, request, dataProp);
            //add return parameters for the jQgrid
            gridProp.setCurrentPage(requestedPage);
            paginatedRecords = gridProp.returnRecords(paginatedRecords);
        } catch (Exception ex) {
            Logger.logMessage("Error paginating records: " + ex, Logger.LEVEL.ERROR);
        }
        return paginatedRecords;
    }

    public String getLastRecordID(ArrayList<HashMap> errorCodeList) {
        String lastRecordID = "";
        for (int i = 0; i < errorCodeList.size(); i++) {
            HashMap h = errorCodeList.get(i);
            if (h.get("LASTRECORDID") != null) {
                lastRecordID = h.get("LASTRECORDID").toString();
                h.remove("LASTRECORDID");
                if (h.size() == 0) {
                    errorCodeList.remove(i);
                }
            }
        }
        return lastRecordID;
    }
    // Get Paginated Audit History
    // This method is responsible for getting three types of history
    // 1. Object level history
    //    all edits made to a particular object.
    //    specified with an object ID, date range, and a table name.
    //    requests for this type of history do not retrieve the table name.
    // 2. User level history
    //    all edits a single user has made throughout the application.
    //    specified with a user ID and date range only.
    //    when getting this type of history, query also sends table name.
    // 3. Application level history
    //    all edits made by all users accross the entire application.
    //    specified with a date range only
    //    when getting this type of history, query also sends the table name.
    public ArrayList<HashMap> getAuditHistory(HttpServletRequest request, HashMap postParams, ArrayList<String> tableNames){
        if(!commonMMHFunctions.checkSession(request).equals("")) {
            // If the request contains the key, 'initialCheck', just checking to see if any history exists for a particular object.
            Object checkOnly = postParams.get("initialCheck");
            if(checkOnly != null){
                String rowids = commDM.ArrayListToCSV((ArrayList) postParams.get("rowid"));
                return commDM.checkForAuditHistory(request,rowids,commDM.getSchemaTableID(tableNames));
            }
            // Properties setup
            DataHandlerProperties auditHistoryProperties = new DataHandlerProperties("data.common.getGlobalAuditHistory");
            auditHistoryProperties.setCountSQL("data.common.getGlobalAuditHistoryCount");
            ServerGridProperties gridProp = new ServerGridProperties(postParams);
            gridProp.setGridFlags(false, false, false);
            gridProp.setDefaultSort("innerq.AUDITDATE", "DESC");
            // Initial Variables
            ArrayList<HashMap> rules = (ArrayList<HashMap>)gridProp.getFilterHM().get("rules");
            String tableIDs;
            ArrayList<HashMap> auditHistory = new ArrayList<>();
            String sortCol = gridProp.getPostParams().get("sidx").toString();
            String ObjectSQL;
            boolean hasTableNames = false;

            if(sortCol == null || sortCol.equals("")) {
                gridProp.getPostParams().put("sord", "desc");
            }
            else if(sortCol.equalsIgnoreCase("SC.USERLABEL")){
                  gridProp.getPostParams().put("sidx","FIELDNAME");
            }
            else if(sortCol.equalsIgnoreCase("ST.USERLABEL")){
                  gridProp.getPostParams().put("sidx","TABLENAME");
            }

            tableIDs = commDM.getSchemaTableID(tableNames);
            String rowids = "";

            for(HashMap rule : new ArrayList<HashMap>(rules)){
                if(rule.get("field").equals("TABLEID")){ rule.put("data",tableIDs); }
                if(rule.get("field").equals("TABLENAMESREQUEST")){
                    auditHistoryProperties.setGetSQL("data.common.getGlobalAuditHistoryWithTableNames");
                    rules.remove(rule);
                    hasTableNames = true;
                }
                if(rule.get("field").equals("ACTIVEONLY")){ rules.remove(rule); }
                if((rule.get("field").equals("RECORDID"))){
                    rowids = commDM.ArrayListToCSV((ArrayList) rule.get("data"));
                }
                if(rule.get("field").equals("FIELDNAME")){ rule.put("field","SC.USERLABEL");}
                if(rule.get("field").equals("AUDITDATE")){ rule.put("field","CD.CHANGEDTM");}
                if(rule.get("field").equals("USERNAME")){ rule.put("field","QU.NAME");}
            }


            if(postParams.containsKey("CUSTOMOBJECTID") && !postParams.get("CUSTOMOBJECTID").equals("")){
                String IDs = DynamicSQL.sanitizeIDs(postParams.get("CUSTOMOBJECTID").toString());
                ObjectSQL = "CD.OBJECTID IN(" + IDs + ")";
            }
            else if(rowids != ""){
                String IDs = DynamicSQL.sanitizeIDs(rowids);
                ObjectSQL = "CD.OBJECTID IN(" +IDs+ ")";
            }
            else{
                ObjectSQL = "CD.OBJECTID IN(searchString)";
            }

            // special cases for Purchase Orders and Nutrition
            if(tableNames.size() == 1 && tableNames.get(0).equalsIgnoreCase("QC_PANutritionCategory") || tableNames.get(0).equalsIgnoreCase("QC_PurchaseOrder")) {
                auditHistoryProperties.setGetSQL("data.common.getGlobalAuditHistoryWithTableNames");
                hasTableNames = true;
            }

            // Required for all types of history
            gridProp.addCustomFilter("TODATE", "CD.ChangeDTM <= searchString");
            gridProp.addCustomFilter("FROMDATE", "CD.ChangeDTM >= searchString");
            // Required for object level history
            gridProp.addCustomFilter("RECORDID", ObjectSQL);
            gridProp.addCustomFilter("TABLEID", "ST.SchemaTableID IN(searchString)");
            //Required for user level history
            gridProp.addCustomFilter("USERID","QU.UserID = searchString");
            try{
                auditHistory = paginateRecords(request, auditHistoryProperties, gridProp);
                // Password fields throught the application
                ArrayList<String> passwordFields = new ArrayList<String>(){{
                    add("Temp Login Password"); add("Kronos Password"); add("Photo Data Password");
                    add("SMTPPassword"); add("Kronos Xml Password"); add("QCDB_Web DBPassword");
                    add("Password"); add("CCPassword"); add("Data Source Password");
                    add("Yellow Dog APIPassword"); add("Proxy Password"); add("My QCPerson Acct Password");
                    add("Wfd Password");
                }};

                // Format values to be user friendly
                if(!auditHistory.isEmpty()){

                    boolean finalHasTableNames = hasTableNames;
                    // used to get the record's name, only used if hasTableNames == true
                    HashMap<String, String[]> tableInfo = new HashMap<String, String[]>();
                    if(finalHasTableNames) {
                        // built from quickcharge/server/AuditLogging/Framework/AuditLoggerDataType.java
                        tableInfo.put("QC_DebitProfiles", new String[]{"DebitProfileID", "Name"});
                        tableInfo.put("QC_RestrictionProfile", new String[]{"RestrictionProfileID", "Name"});
                        tableInfo.put("QC_RestrictionProfileDetail", new String[]{"RestrictionProfileDetailID", "NO_NAME"});
                        tableInfo.put("QC_OrgLevelQuaternary", new String[]{"OrgLevelQuaternaryID", "Name"});
                        tableInfo.put("QC_OrgLevelTertiary", new String[]{"OrgLevelTertiaryID", "Name"});
                        tableInfo.put("QC_OrgLevelSecondary", new String[]{"OrgLevelSecondaryID", "Name"});
                        tableInfo.put("QC_DEDUCTIONPLATEAU", new String[]{"DeductionPlateauID", "Name"});
                        tableInfo.put("QC_DEDUCTIONPROFILE", new String[]{"DeductionProfileID", "Name"});
                        tableInfo.put("QC_DeductionAmountMethod", new String[]{"DeductionAmtMthID", "Name"});
                        tableInfo.put("QC_DeductionGroupingMethod", new String[]{"DeductionGrpMthID", "Name"});
                        tableInfo.put("QC_DeletionThreshold", new String[]{"DeletionThresholdID", "Name"});
                        tableInfo.put("MMH_COMMUNICATIONGROUP", new String[]{"CommunicationGroupID", "Name"});
                        tableInfo.put("QC_BACKGROUNDTASK", new String[]{"BackgroundTaskID", "Name"});
                        tableInfo.put("QC_PAYCODES", new String[]{"PayCodeID", "Name"});
                        tableInfo.put("QC_PAKeypad", new String[]{"PAKeypadID", "Name"});
                        tableInfo.put("QC_PAPlu", new String[]{"PAPluID", "Name"});
                        tableInfo.put("QC_PAPluAlias", new String[]{"PAPluAliasID", "PluCode"});
                        tableInfo.put("QC_PurchaseOrder", new String[]{"PurchaseOrderID", "PurchaseOrderNumber"}); // different than the AuditLoggerDataType of "NO_NAME"
                        tableInfo.put("QC_PurchaseOrderDetail", new String[]{"PurchaseOrderDetailID", "NO_NAME"});
                        tableInfo.put("QC_PATender", new String[]{"PATenderID", "Name"});
                        tableInfo.put("QC_PASubdepartment", new String[]{"PASubDeptID", "Name"});
                        tableInfo.put("QC_PADepartment", new String[]{"PADepartmentID", "Name"});
                        tableInfo.put("QC_PADiscounts", new String[]{"PADiscountID", "Name"});
                        tableInfo.put("QC_Discount", new String[]{"DiscountID", "Name"});
                        tableInfo.put("QC_TimeUnit", new String[]{"TimeUnitID", "Name"});
                        tableInfo.put("QC_DiscountFrequency", new String[]{"DiscountFrequencyID", "Name"});
                        tableInfo.put("QC_DiscountProfileMaster", new String[]{"DiscountProfileID", "Name"});
                        tableInfo.put("QC_DiscountProfileDetail", new String[]{"DiscountProfileDetailID", "NO_NAME"});
                        tableInfo.put("QC_FundingAmount", new String[]{"FUNDINGAMOUNTID", "Name"});
                        tableInfo.put("QC_FundingAmountType", new String[]{"FUNDINGAMOUNTTYPEID", "Name"});
                        tableInfo.put("QC_FundingAmountTypeDefault", new String[]{"FUNDINGAMOUNTTYPEDEFAULTID", "NO_NAME"});
                        tableInfo.put("QC_PAFunctions", new String[]{"PAFunctionID", "Name"});
                        tableInfo.put("QC_PAPriceLevel", new String[]{"PAPriceLevelID", "Name"});
                        tableInfo.put("QC_PAPaidOut", new String[]{"PAPaidOutID", "Name"});
                        tableInfo.put("QC_PAPrepOption", new String[]{"PAPrepOptionID", "Name"});
                        tableInfo.put("QC_PAPrepOptionSet", new String[]{"PAPrepOptionSetID", "Name"});
                        tableInfo.put("QC_PAPrepOptionSetDetail", new String[]{"PAPrepOptionSetDetailID", "NO_NAME"});
                        tableInfo.put("QC_PAReceivedAcct", new String[]{"PAReceivedAcctID", "Name"});
                        tableInfo.put("QC_PATareProfile", new String[]{"PATareProfileID", "Name"});
                        tableInfo.put("QC_PATareProfileDetail", new String[]{"PATareProfileDetailID", "NO_NAME"});
                        tableInfo.put("QC_PATares", new String[]{"PATareID", "Name"});
                        tableInfo.put("QC_ITFieldString", new String[]{"ITFieldStringID", "Name"});
                        tableInfo.put("QC_ITForm", new String[]{"ITFormID", "Name"});
                        tableInfo.put("QC_ITFieldBadge", new String[]{"ITFieldBadgeID", "Name"});
                        tableInfo.put("QC_ITFieldDate", new String[]{"ITFieldDateID", "Name"});
                        tableInfo.put("QC_ITFieldDuration", new String[]{"ITFieldDurationID", "Name"});
                        tableInfo.put("QC_ITFieldFloat", new String[]{"ITFieldFloatID", "Name"});
                        tableInfo.put("QC_ITFieldHidden", new String[]{"ITFieldHiddenID", "Name"});
                        tableInfo.put("QC_ITFieldInteger", new String[]{"ITFieldIntegerID", "Name"});
                        tableInfo.put("QC_ITFieldList", new String[]{"ITFieldListID", "Name"});
                        tableInfo.put("QC_ReasonCode", new String[]{"ReasonCodeID", "Name"});
                        tableInfo.put("QC_PAMacro", new String[]{"PAMacroID", "Name"});
                        tableInfo.put("QC_PANumber", new String[]{"PANumberID", "Name"});
                        tableInfo.put("QC_Printer", new String[]{"PrinterID", "LogicalName"});
                        tableInfo.put("QC_PrinterType", new String[]{"PrinterTypeID", "Name"});
                        tableInfo.put("QC_PrinterHardwareType", new String[]{"PrinterHardwareTypeID", "Name"});
                        tableInfo.put("QC_PAVendors", new String[]{"VendorID", "Name"});
                        tableInfo.put("QC_PAVendorItemMapping", new String[]{"VendorItemMappingID", "NO_NAME"});
                        tableInfo.put("QC_ITFieldTime", new String[]{"ITFieldTimeID", "Name"});
                        tableInfo.put("QC_PARotation", new String[]{"PARotationID", "Name"});
                        tableInfo.put("QC_LoyaltyReward", new String[]{"LoyaltyRewardId", "Name"});
                        tableInfo.put("QC_TaxRate", new String[]{"TaxRateID", "Name"});
                        tableInfo.put("QC_LoyaltyRewardPLUMapping", new String[]{"LoyaltyRewardPLUMappingID", "NO_NAME"});
                        tableInfo.put("QC_LoyaltyRewardType", new String[]{"LoyaltyRewardTypeID", "Name"});
                        tableInfo.put("QC_LoyaltyRewardCalcMethod", new String[]{"LoyaltyRewardCalcMethodID", "Name"});
                        tableInfo.put("QC_LoyaltyRoundingType", new String[]{"LoyaltyRoundingTypeID", "Name"});
                        tableInfo.put("QC_LoyaltyRoundingPrecision", new String[]{"LoyaltyRoundingPrecisionID", "Name"});
                        tableInfo.put("QC_Interfaces", new String[]{"InterfaceID", "Name"});
                        tableInfo.put("QC_TaskType", new String[]{"TaskTypeID", "Name"});
                        tableInfo.put("QC_EmployeeInterfaceType", new String[]{"EmployeeInterfaceTypeID", "Name"});
                        tableInfo.put("QC_ImportType", new String[]{"ImportTypeID", "Name"});
                        tableInfo.put("QC_DataSourceType", new String[]{"DataSourceTypeID", "Name"});
                        tableInfo.put("QC_ImportInterfaceFieldMap", new String[]{"ImportInterfaceFieldMapID", "StagingFieldName"});
                        tableInfo.put("QC_InterfaceWFDFieldFilters", new String[]{"InterfaceWfdFieldFilterID", "WfdFieldName"});
                        tableInfo.put("QC_PASurcharge", new String[]{"PASurchargeID", "Name"});
                        tableInfo.put("QC_DSDisplay", new String[]{"DSDisplayID", "Name"});
                        tableInfo.put("QC_DSSignGroup", new String[]{"DSSignGroupID", "Name"});
                        tableInfo.put("QC_DSSign", new String[]{"DSSignID", "Name"});
                        tableInfo.put("QC_PACombo", new String[]{"PAComboID", "Name"});
                        tableInfo.put("QC_RevenueCenter", new String[]{"RevenueCenterID", "Name"});
                        tableInfo.put("QC_RevenueCenterInv", new String[]{"RevenueCenterInvID", "NO_NAME"});
                        tableInfo.put("QC_RevenueCenterMappings", new String[]{"RevCentMapID", "NO_NAME"});
                        tableInfo.put("QC_PAInvBatchType", new String[]{"PAInvBatchTypeID", "Name"});
                        tableInfo.put("QC_PAInvBatchStatus", new String[]{"PAInvBatchStatusID", "Name"});
                        tableInfo.put("QC_UserType", new String[]{"UserTypeID", "Name"});
                        tableInfo.put("QC_PAIconPositions", new String[]{"PAIconPositionID", "Name"});
                        tableInfo.put("QC_PAColor", new String[]{"PAColorID", "Name"});
                        tableInfo.put("QC_Location", new String[]{"LocationID", "LocationName"});
                        tableInfo.put("QC_LocationInv", new String[]{"LocationInvID", "NO_NAME"});
                        tableInfo.put("QC_PAInvBatch", new String[]{"PAInvBatchID", "NO_NAME"});
                        tableInfo.put("QC_PAInvBatchDetail", new String[]{"PAInvBatchDetailID", "NO_NAME"});
                        tableInfo.put("QC_OwnershipGroup", new String[]{"OwnershipGroupID", "Name"});
                        tableInfo.put("QC_VenueCategories", new String[]{"VenueCategoryID", "TypeName"});
                        tableInfo.put("QC_InventoryCostMethod", new String[]{"InventoryCostMethodID", "Name"});
                        tableInfo.put("QC_PALabelPrintMethod", new String[]{"PALabelPrintMethodID", "Name"});
                        tableInfo.put("QC_OrgLevelPrimary", new String[]{"OrgLevelPrimaryID", "Name"});
                        tableInfo.put("QC_PrintController", new String[]{"PrintControllerID", "HostName"});
                        tableInfo.put("QC_KDSDataSource", new String[]{"KDSDataSourceID", "Name"});
                        tableInfo.put("QC_TerminalType", new String[]{"TERMINALTYPEID", "NAME"});
                        tableInfo.put("QC_POSItemProfiles", new String[]{"POSItemProfileID", "Name"});
                        tableInfo.put("QC_TransitionEffect", new String[]{"TransitionEffectID", "Name"});
                        tableInfo.put("QC_TerminalModel", new String[]{"TERMINALMODELID", "NAME"});
                        tableInfo.put("QC_TerminalPOSLiteProfile", new String[]{"TerminalPOSLiteProfileID", "Name"});
                        tableInfo.put("QC_TOS", new String[]{"TOSID", "Title"});
                        tableInfo.put("QC_TOSMapping", new String[]{"TOSMappingID", "NO_NAME"});
                        tableInfo.put("QC_PATerminalMode", new String[]{"PATerminalModeID", "Name"});
                        tableInfo.put("MMH_Template", new String[]{"TemplateID", "Name"});
                        tableInfo.put("MMH_TemplateType", new String[]{"TemplateTypeID", "Name"});
                        tableInfo.put("QC_PAFontSize", new String[]{"PAFontSizeID", "Name"});
                        tableInfo.put("QC_OTMDetail", new String[]{"OTMDetailID", "Label"});
                        tableInfo.put("QC_ExpediterRcptType", new String[]{"ExpediterRcptTypeID", "Name"});
                        tableInfo.put("QC_PaymentProcessor", new String[]{"PaymentProcessorID", "Name"});
                        tableInfo.put("QC_PACCProcessor", new String[]{"PACCProcessorID", "DisplayName"});
                        tableInfo.put("QC_ReAuthType", new String[]{"ReAuthTypeID", "Name"});
                        tableInfo.put("QC_Schedule", new String[]{"ScheduleID", "Name"});
                        tableInfo.put("QC_ScheduleDetail", new String[]{"ScheduleDetailID", "Name"});
                        tableInfo.put("QC_ScheduleType", new String[]{"ScheduleTypeID", "Name"});
                        tableInfo.put("MMH_SystemText", new String[]{"SystemTextID", "Description"});
                        tableInfo.put("MMH_SystemTextType", new String[]{"SystemTextTypeID", "Name"});
                        tableInfo.put("QC_FundingFeeCalculationMethod", new String[]{"FundingFeeCalculationMethodID", "Name"});
                        tableInfo.put("QC_Terminals", new String[]{"TerminalID", "Name"});
                        tableInfo.put("QC_MyQCTerminal", new String[]{"MyQCTerminalID", "NO_NAME"});
                        tableInfo.put("QC_PADeploymentPackage", new String[]{"PADeploymentPackageID", "PackageName"});
                        tableInfo.put("QC_Source", new String[]{"SourceID", "Name"});
                        tableInfo.put("QC_PAInvCombinedBatch", new String[]{"PAInvCombinedBatchID", "Name"});
                        tableInfo.put("QC_PAModifierSet", new String[]{"PAModifierSetID", "Name"});
                        tableInfo.put("QC_PAModifierSetDetail", new String[]{"PAModifierSetDetailID", "NO_NAME"});
                        tableInfo.put("QC_QuickChargeUsers", new String[]{"UserID", "Name"});
                        tableInfo.put("QC_UserProfile", new String[]{"UserProfileID", "Name"});
                        tableInfo.put("MMH_InviteStatus", new String[]{"InviteStatusID", "InternalDescription"});
                        tableInfo.put("QC_PAKeypadDetail", new String[]{"PAKeypadDetailID", "NO_NAME"});
                        tableInfo.put("QC_PAResolutions", new String[]{"PAResolutionID", "Name"});
                        tableInfo.put("QC_Employees", new String[]{"EmployeeID", "Name"});
                        tableInfo.put("QC_Person", new String[]{"PersonID", "Name"});
                        tableInfo.put("QC_EmployeePersonMapping", new String[]{"EmployeePersonMappingID", "NO_NAME"});
                        tableInfo.put("QC_Transactions", new String[]{"TransactionID", "NO_NAME"});
                        tableInfo.put("QC_TransactionPOSItems", new String[]{"TransactionPOSItemID", "NO_NAME"});
                        tableInfo.put("QC_PayrollGroupings", new String[]{"PayrollGroupingID", "Name"});
                        tableInfo.put("QC_TerminalProfilesMaster", new String[]{"TerminalProfileID", "Name"});
                        tableInfo.put("QC_TerminalProfilesDeduction", new String[]{"TermProfDedID", "NO_NAME"});
                        tableInfo.put("QC_TerminalProfilesDetail", new String[]{"TerminalProfileIndex", "NO_NAME"});
                        tableInfo.put("QC_PayrollGroupTerminalProfile", new String[]{"PayrollGroupTerminalProfileID", "NO_NAME"});
                        tableInfo.put("QC_TerminalProfileAccountOptions", new String[]{"TerminalProfileAccountOptionID", "NO_NAME"});
                        tableInfo.put("QC_ReceiptBalanceType", new String[]{"ReceiptBalanceTypeID", "Name"});
                        tableInfo.put("QC_OTM", new String[]{"OTMID", "Name"});
                        tableInfo.put("QC_OTMType", new String[]{"OTMTypeID", "Name"});
                        tableInfo.put("QC_AccountType", new String[]{"AccountTypeID", "Name"});
                        tableInfo.put("QC_PersonRelationship", new String[]{"PersonRelationshipID", "Name"});
                        tableInfo.put("QC_BadgeType", new String[]{"BadgeTypeID", "Name"});
                        tableInfo.put("QC_POSItems", new String[]{"POSItemID", "Name"});
                        tableInfo.put("QC_Badge", new String[]{"BadgeID", "BadgeNumber"});
                        tableInfo.put("QC_ApplicationMenu", new String[]{"ApplicationMenuId", "ApplicationMenuName"});
                        tableInfo.put("QC_DayPart", new String[]{"DayPartID", "Name"});
                        tableInfo.put("MMH_Language", new String[]{"LanguageID", "Name"});
                        tableInfo.put("QC_PASurchargeType", new String[]{"PASurchargeTypeID", "Name"});
                        tableInfo.put("QC_PASurchargeApplicationType", new String[]{"PASurchargeApplicationTypeID", "Name"});
                        tableInfo.put("QC_PASurchargeAmountType", new String[]{"PASurchargeAmountTypeID", "Name"});
                        tableInfo.put("QC_DSSignGroupDetail", new String[]{"DSSignGroupDetailID", "NO_NAME"});
                        tableInfo.put("QC_DSSignDetail", new String[]{"DSSignDetailID", "Title"});
                        tableInfo.put("QC_DSContextType", new String[]{"DSContextTypeID", "Name"});
                        tableInfo.put("QC_LoyaltyProgram", new String[]{"LoyaltyProgramId", "Name"});
                        tableInfo.put("QC_LoyaltyAccrualPolicy", new String[]{"LoyaltyAccrualPolicyID", "Name"});
                        tableInfo.put("QC_LoyaltyAccrualPolicyType", new String[]{"LoyaltyAccrualPolicyTypeID", "Name"});
                        tableInfo.put("QC_LoyaltyEarningType", new String[]{"LoyaltyEarningTypeId", "Name"});
                        tableInfo.put("QC_POSWar", new String[]{"POSWARID", "Name"});
                        tableInfo.put("QC_Globals", new String[]{"GlobalID", "NO_NAME"});
                        tableInfo.put("QC_AuthenticationTypes", new String[]{"AuthenticationTypeID", "Name"});
                        tableInfo.put("QC_SSOLoginNameType", new String[]{"SSOLoginNameTypeID", "Name"});
                        tableInfo.put("QC_UploadedFileSizeLimit", new String[]{"UploadedFileSizeLimitID", "NO_NAME"});
                        tableInfo.put("QC_UploadedFileSource", new String[]{"FileSourceID", "FileSourceName"});
                        tableInfo.put("QC_MYQCFeatureSetting", new String[]{"MYQCFeatureSettingID", "OverrideDisplayName"});
                        tableInfo.put("QC_PANutritionCategory", new String[]{"PANutritionCategoryID", "Name"});
                        tableInfo.put("QC_PANutritionTotals", new String[]{"PANutritionTotalsID", "DVCalorieValLbl"});
                        tableInfo.put("QC_DiscountType", new String[]{"DiscountTypeID", "Name"});
                        tableInfo.put("QC_Query", new String[]{"QueryID", "Name"});
                        tableInfo.put("QC_LaborLevels", new String[]{"LaborLevelID", "Entry"});
                        tableInfo.put("QC_PAItemPOSItemMap", new String[]{"PAItemPOSItemMapID", "NO_NAME"});
                        tableInfo.put("QC_POSItemMapping", new String[]{"POSItemMappingID", "NO_NAME"});
                        tableInfo.put("QC_SystemMonitorCheck", new String[]{"SystemMonitorCheckID", "Name"});
                        tableInfo.put("QC_SystemMonitorCheckTimeRange", new String[]{"SystemMonitorCheckTimeRangeID", "NO_NAME"});
                        tableInfo.put("QC_PAUpsellProfile", new String[]{"PAUpsellProfileID", "Name"});
                        tableInfo.put("QC_PAUpsellProfileDetail", new String[]{"PAUpsellProfileDetailID", "NO_NAME"});
                        tableInfo.put("QC_PurchaseOrderStatus", new String[]{"PurchaseOrderStatusID", "Name"});
                        tableInfo.put("QC_FAQ", new String[]{"FAQID", "Name"});
                        tableInfo.put("QC_FAQSet", new String[]{"FAQSetID", "Name"});
                        tableInfo.put("QC_FAQSetDetail", new String[]{"FAQSetDetailID", "NO_NAME"});
                        tableInfo.put("QC_PrinterModel", new String[]{"PrinterModelID", "Name"});
                        tableInfo.put("QC_PrinterComType", new String[]{"PrinterComTypeID", "Name"});
                        tableInfo.put("QC_Donation", new String[]{"DonationID", "Name"});
                        tableInfo.put("QC_AccessCode", new String[]{"AccessCodeID", "Name"});
                        tableInfo.put("QC_SSOConfig", new String[]{"SSOConfigID", "Name"});
                        tableInfo.put("QC_SSOAttributeMapping", new String[]{"SSOAttributeMappingID", "FIELDNAME"});
                    }

                    auditHistory.forEach(record ->{
                       if(record.containsKey("COLUMNNAME")){
                           if(passwordFields.contains(record.get("COLUMNNAME"))){
                               record.put("BEFOREVALUE","***ENCRYPTED PASSWORD***");
                               record.put("AFTERVALUE","***ENCRYPTED PASSWORD***");
                           }
                       }
                       if(record.containsKey("DATATYPE")){
                          if(record.get("DATATYPE").equals("BIT")){
                             if(!record.get("BEFOREVALUE").equals("")){
                                 Object before = record.get("BEFOREVALUE");
                                record.put("BEFOREVALUE",((before.equals("1") || before.equals("true")) ? "true" : "false"));
                             }
                             if(!record.get("AFTERVALUE").equals("")){
                                 Object after = record.get("AFTERVALUE");
                                 record.put("AFTERVALUE",((after.equals("1") || after.equals("true")) ? "true": "false"));
                             }

                          }
                          record.remove("DATATYPE");
                       }
                        // check that we should get the 'Name' of the record; OBJECTID will not be available on empty data sets & will throw an error
                        if(finalHasTableNames && record.keySet().contains("OBJECTID") && record.get("RECORDNAME").equals("")) {
                            // get the ObjectID and ActualTableName
                            String objectID = "";
                            String tableName = "";

                           // default in case of some issue
                           String recordValue = "Could Not Get Record Name";

                            try {
                                objectID = record.get("OBJECTID").toString();
                                tableName = record.get("ACTUALTABLENAME").toString();
                               recordValue = objectID;

                                // a little error checking...
                                if(!objectID.equals("") && !tableName.equals("")) {

                                    try {
                                        String PKeyField = tableInfo.get(tableName)[0];
                                        String fieldName = tableInfo.get(tableName)[1];
                                        if(fieldName.equals(null)) {
                                            fieldName = "NAME";
                                        }
                                        // build query to get the name of the record
                                       DynamicSQL getRecordName = new DynamicSQL("data.common.getChangedDataRecordName")
                                               .addValue(1, objectID)
                                               .addTableORColumnName(2, tableName)
                                               .addTableORColumnName(3, fieldName)
                                               .addTableORColumnName(4, PKeyField);
                                       recordValue = getRecordName.getSingleField(dm).toString();
                                    }
                                    catch(Exception e) {
                                        Logger.logMessage("Could not get the record name in commonDataHandler.getAuditHistory() - " + e, Logger.LEVEL.ERROR);
                                        Logger.logException(e);
                                    }
                                }

                            }
                            catch(Exception ex) {
                                Logger.logMessage("Could not get params necessary in commonDataHandler.getAuditHistory() - " + ex, Logger.LEVEL.ERROR);
                                Logger.logException(ex);
                            }
                            finally {
                               // remove these once we save the values locally so we do not expose them to the client
                        if(record.get("OBJECTID") != null) {
                            record.remove("OBJECTID");
                        }
                        if(record.get("ACTUALTABLENAME") != null) {
                            record.remove("ACTUALTABLENAME");
                        }
                               record.put("RECORDNAME", recordValue);

                           }
                       }
                    });
                }
            }
            catch(Exception e){
                Logger.logException(e);
            }
            return auditHistory;
        }else{
            return new ArrayList<HashMap>();
        }
    }


//END - Paginate Record Methods
}



