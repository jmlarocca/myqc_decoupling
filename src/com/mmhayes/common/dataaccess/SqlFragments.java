package com.mmhayes.common.dataaccess;

/*
 $Author: jrmitaly $: Author of last commit
 $Date: 2015-04-30 09:16:16 -0400 (Thu, 30 Apr 2015) $: Date of last commit
 $Rev: 1538 $: Revision of last commit
 Notes: Class to build and keep track of pieces of SQL
*/

import com.mmhayes.common.utils.MMHProperties;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class SqlFragments {

    private String insertKeySql;
    private String insertValSql;
    private String updateSql;
    private String whereSql;
    private ArrayList<String> parameters;
    public SqlFragments() {
        this.insertKeySql = "";
        this.insertValSql = "";
        this.updateSql = "";
        this.whereSql = "";
        this.parameters = new ArrayList<String>();
    }
    public SqlFragments(String insertKeySql, String insertValSql, String updateSql, String whereSql) {
        this.insertKeySql = insertKeySql;
        this.insertValSql = insertValSql;
        this.updateSql = updateSql;
        this.whereSql = whereSql;
        this.parameters = new ArrayList<String>();
    }
    public String getInsertKeySql() {
        return insertKeySql;
    }
    public void setInsertKeySql(String insertKeySql) {
        this.insertKeySql = (insertKeySql == null ? "" : insertKeySql);
    }
    public void appendInsertKeySql(String insertKeySql) {
        this.insertKeySql += insertKeySql;
    }
    public void prependInsertKeySql(String insertKeySql) {
        if (this.insertKeySql.substring(0,1).compareToIgnoreCase("(") == 0) {
            if (this.insertKeySql.replaceAll("\\s","").substring(1,this.insertKeySql.replaceAll("\\s","").indexOf(")")).length() > 1) {
                this.insertKeySql = "(" + insertKeySql + ", " + this.insertKeySql.substring(1);
            } else {
                this.insertKeySql = "(" + insertKeySql + " " + this.insertKeySql.substring(1);
            }
        } else {
            this.insertKeySql = insertKeySql + ", " + this.insertKeySql.substring(1);
        }
    }
    public String getInsertValSql() {
        return insertValSql;
    }
    public void setInsertValSql(String insertValSql) {
        this.insertValSql = (insertValSql == null ? "" : insertValSql);
    }
    public void appendInsertValSql(String insertValSql) {
        this.insertValSql += insertValSql;
    }
    public void prependInsertValSql(String insertValSql) {
        if (this.insertValSql.substring(0,1).compareToIgnoreCase("(") == 0) {
            if (this.insertValSql.replaceAll("\\s","").substring(1,this.insertValSql.replaceAll("\\s","").indexOf(")")).length() > 1) {
                this.insertValSql = "(" + insertValSql + ", " + this.insertValSql.substring(1);
            } else {
                this.insertValSql = "(" + insertValSql + " " + this.insertValSql.substring(1);
            }
        } else {
            this.insertValSql = insertValSql + ", " + this.insertValSql.substring(1);
        }
    }
    public String getUpdateSql() {
        return updateSql;
    }
    public void setUpdateSql(String updateSql) {
        this.updateSql = (updateSql == null ? "" : updateSql);
    }
    public void appendUpdateSql(String updateSql) {
        this.updateSql += updateSql;
    }
    public String getWhereSql() {
        return whereSql;
    }
    public void setWhereSql(String whereSql) {
        this.whereSql = (whereSql == null ? "" : whereSql);
    }
    public void appendWhereSql(String whereSql) {
        this.whereSql += whereSql;
    }
    public ArrayList<String> getParameters() {
        return this.parameters;
    }
    public void addParameter(Object o) {
        if (o != null) {
            this.parameters.add(String.valueOf(o));
        } else {
            this.parameters.add(null);
        }
    }
    public void addParameter(Object o, int i) {
        if (o != null) {
            this.parameters.add(i, String.valueOf(o));
        } else {
            this.parameters.add(i, null);
        }
    }
    public void removeParameter(int index) {
        this.parameters.remove(index);
    }
    public void removeParameter(Object o) {
        Iterator it = this.parameters.iterator();
        while (it.hasNext()) {
            String s = (String)it.next();
            if (s == String.valueOf(o)) {
                it.remove();
            }
        }
    }
    public void prependNextVal(String insertKey, String sequenceNext) {
        if (this.insertKeySql.substring(0,1).compareToIgnoreCase("(") == 0) {
            if (this.insertKeySql.replaceAll("\\s","").substring(1,this.insertKeySql.replaceAll("\\s","").indexOf(")")).length() > 1) {
                this.insertKeySql = "(" + insertKey + ", " + this.insertKeySql.substring(1);
            } else {
                this.insertKeySql = "(" + insertKey + " " + this.insertKeySql.substring(1);
            }
        } else {
            this.insertKeySql = insertKey + ", " + this.insertKeySql.substring(1);
        }
        if (this.insertValSql.substring(0,1).compareToIgnoreCase("(") == 0) {
            if (this.insertValSql.replaceAll("\\s","").substring(1,this.insertValSql.replaceAll("\\s","").indexOf(")")).length() > 1) {
                this.insertValSql = "(" + sequenceNext + ", " + this.insertValSql.substring(1);
            } else {
                this.insertValSql = "(" + sequenceNext + " " + this.insertValSql.substring(1);
            }
        } else {
            this.insertValSql = sequenceNext + ", " + this.insertValSql.substring(1);
        }
    }

    // Utility method to change date formatting
    private Object formatOracleDate(Object o) {
        SimpleDateFormat df = (SimpleDateFormat)SimpleDateFormat.getDateInstance();
        df.applyPattern("MM/dd/yyyy HH:mm:ss");
        return df.format(o);
    }

    // Utility method to build SQL strings out of a HashMap of parameters for dynamic SQL
    public void prepareSqlFragments(HashMap params, char type) {
        // Check and store if the database is Oracle
        boolean isOracle = MMHProperties.getAppSetting("database.type").compareToIgnoreCase("ORACLE") == 0;
        // Different execution based on the type, which is a char
        if (params != null && params.size() > 0) {
            switch (type) {
                case 'I': // 'I' is for insert
                    // Inserts must be built all-at-once, so set the SQL strings to empty
                    this.setInsertKeySql("");
                    this.setInsertValSql("");
                    // Loop over the given key-value pairs (found in the HashMap) using the key string
                    for (Object key : params.keySet()) {
                        if (params.get(key) != null) {
                            Object o = params.get(key); // Get the value for that key
                            this.appendInsertKeySql(key.toString() + ", "); // Add the key to the first part of the insert string: INSERT INTO {table} ({this}) VALUES ({value_string})
                            // Add the ? for parameterized queries to the second part of the insert string: INSERT INTO {table} ({column_string}) VALUES ({this})
                            if (o != null && o instanceof Date && isOracle) {
                                o = formatOracleDate(o);
                                this.appendInsertValSql("TO_DATE(?, 'mm/dd/yyyy hh24:mi:ss'), ");
                            } else {
                                this.appendInsertValSql("?, ");
                            }
                            this.addParameter(o); // Add the value to the parameter list, will replace the ? when executed
                        }
                    }
                    if (this.getInsertKeySql().length() > 2) { // Trim the trailing ', ' and enclose in parenthesis where there's populated values
                        this.setInsertKeySql("(" + this.getInsertKeySql().substring(0, this.getInsertKeySql().length() - 2) + ")");
                    }
                    if (this.getInsertValSql().length() > 2) { // Trim the trailing ', ' and enclose in parenthesis where there's populated values
                        this.setInsertValSql("(" + this.getInsertValSql().substring(0, this.getInsertValSql().length() - 2) + ")");
                    }
                    break;
                case 'U': // 'U' is for update
                    // If this isn't the first time running the function, add to the existing field
                    if (this.getUpdateSql().length() > 0) {
                        this.appendUpdateSql(", ");
                    }
                    // Loop over the given key-value pairs (found in the HashMap) using the key string
                    for (Object key : params.keySet()) {
                        //if (params.get(key) != null) {
                        Object o = params.get(key); // Get the value for that key
                        // Add the column and ? for parameterized queries to the set part of the update string: UPDATE {table} SET {this_column = this_value} WHERE {where}
                        if (o != null && o instanceof Date && isOracle) {
                            o = formatOracleDate(o);
                            this.appendUpdateSql(key.toString() + " = TO_DATE(?, 'mm/dd/yyyy hh24:mi:ss'), ");
                        } else {
                            this.appendUpdateSql(key.toString() + " = ?, ");
                        }
                        this.addParameter(o); // Add the value to the parameter list, will replace the ? when executed
                        //}
                    }
                    if (this.getUpdateSql().length() > 2) { // Trim the trailing ', ' where there's populated values
                        this.setUpdateSql(this.getUpdateSql().substring(0, this.getUpdateSql().length() - 2));
                    }
                    break;
                case 'W': // 'W' is for where, which allows you to build on the where clause
                    // If this isn't the first time running the function, add to the existing field
                    if (this.getWhereSql().length() > 0) {
                        this.appendWhereSql(" AND ");
                    }
                    // Loop over the given key-value pairs (found in the HashMap) using the key string
                    for (Object key : params.keySet()) {
                        if (params.get(key) != null) {
                            Object o = params.get(key); // Get the value for that key
                            // Add the column and ? for parameterized queries to the where part of the update string: UPDATE {table} SET {update} WHERE {this_column = this_value}
                            if (o instanceof Date && isOracle) {
                                o = formatOracleDate(o);
                                this.appendWhereSql(key.toString() + " = TO_DATE(?, 'mm/dd/yyyy hh24:mi:ss') AND ");
                            } else {
                                this.appendWhereSql(key.toString() + " = ? AND ");
                            }
                            this.addParameter(o); // Add the value to the parameter list, will replace the ? when executed
                        }
                    }
                    if (this.getWhereSql().length() > 5) { // Trim the trailing ', ' where there's populated values
                        this.setWhereSql(this.getWhereSql().substring(0, this.getWhereSql().length() - 5));
                    }
                    break;
            }
        }
        return;
    }
}
