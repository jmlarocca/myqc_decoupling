package com.mmhayes.common.dataaccess;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.utils.*;
import org.owasp.esapi.ESAPI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//other dependencies

/*
 $Author: nyu $: Author of last commit
 $Date: 2021-08-17 15:42:27 -0400 (Tue, 17 Aug 2021) $: Date of last commit
 $Rev: 14943 $: Revision of last commit
 Notes: Data Manager containing common functionality other data managers use
*/
public class commonMMHFunctions implements Serializable {

    DataManager dm;
    private static String className;

    public static final String COMPANYID = "COMPANYID";
    public static final String RECORDACTION = "RECORDACTION";
    public static final String RECORDACTIONTIMESTAMP = "RECORDACTIONTIMESTAMP";
    public static final String RECORDACTIONUSER = "RECORDACTIONUSER";
    public static final String ARRAYLISTINDEX = "ARRLISTINDEX";
    public static final String UNDEFINED = "undefined";
    public static final String DELETED = "FEDELETED";
    public static final String CHILD_EDITED_FLAG = "FEEDITED";
    public static final String INTERNAL_ERROR = "INTERNAL_ERROR_HAS_OCCURRED";
    public static final String LOG_ADD_FLAG = "A";
    public static final String LOG_UPDATE_FLAG = "U";
    public static final String LOG_DELETE_FLAG = "D";
    public static final String DYNAMIC_UPDATE_SQL = "data.generic.dynamicUpdate";
    public static final String DYNAMIC_INSERT_SQL_WITH_RETURN = "data.generic.dynamicInsertWithReturn";
    public static final String DYNAMIC_INSERT_SQL_NO_RETURN = "data.generic.dynamicInsertNoReturn";
    public static final String DYNAMIC_DELETE_SQL = "data.generic.dynamicHardDelete";
    public static final String DYNAMIC_LOOKUP_SQL = "data.generic.dynamicLookup";
    public static final String VIEWPERMISSION = "VIEWPERMISSION";
    public static final String EDITPERMISSION = "EDITPERMISSION";
    public static final String ADDPERMISSION = "ADDPERMISSION";
    public static final String DELETEPERMISSION = "DELETEPERMISSION";
    public static final String LOCATIONID = "LOCATIONID";
    public static final String PATH_INTERFACES = "Interfaces";
    public static final String PATH_APPLICATION = "application";
    public static final String ERR_DIR_NOT_EXIST = "Directory does not exist.";
    public static final String ERR_OBJ_NOT_DIR = "Object is not a directory.";
    public static final String ERR_FILE_NOT_EXIST = "File does not exist.";
    public static final String ERR_OBJ_NOT_FILE = "Object is not a file.";
    public static final int MAX_PASSWORD_LENGTH = 112; // Max field length = 224
    public static final long MILLISECONDS_PER_MINUTE = 60000; // Milliseconds
    public static final int ONLINE_ORDERING_TERMINAL_MODEL = 105;
    public static final int MY_QC_FUNDING_TERMINAL_MODEL = 106;
    public static final int QC_POS_KIOSK_TERMINAL_MODEL = 107;
    private boolean employeeLicenseBasedOnUse = false;
    private boolean employeeLicenseBasedOnSignUp = false;
    // keep track of whether the first page has been loaded or not - used for the onLoginMessage
    private boolean loadedFirstPage = false;

    public commonMMHFunctions() {
        className = this.getClass().getSimpleName();
        this.dm = new DataManager();
    }

/* START - Session Handlers */

    //checks the session for the logged in user's ID and returns the sessions userID
    public static String checkSession(HttpServletRequest request) {
        String userID = "";
        try {
            Logger.logMessage("Running method commonMMHFunctions.checkSession...", Logger.LEVEL.DEBUG);
            HttpSession session = request.getSession();

            if (session.getAttribute("MMH_USERID") != null) {
                userID = session.getAttribute("MMH_USERID").toString();
                return userID;
            } else if (session.getAttribute("QC_USERID") != null) {
                userID = session.getAttribute("QC_USERID").toString();
                return userID;
            } else {
                return userID;
            }
        } catch (Exception ex) {
            logStackTrace(ex, className, "checkSession");
            Logger.logMessage("Checking the user's session " + ex.toString(), Logger.LEVEL.ERROR);
            return userID;
        }
    }

    //returns the user ID as a long
    public static long getUserId(HttpServletRequest request) {
        try {
            String sId = "";
            HttpSession session = request.getSession();
            if (session.getAttribute("MMH_USERID") != null) {
                sId = session.getAttribute("MMH_USERID").toString();
            } else if (session.getAttribute("QC_USERID") != null) {
                sId = session.getAttribute("QC_USERID").toString();
            }
            return (sId.length() > 0 ? Long.parseLong(sId) : 0);
        } catch (Exception e) {
            return 0;
        }
    }

    //returns a boolean indicating if the user is valid in the database
    public boolean validateUser(HttpServletRequest request) {
        long Id = getUserId(request);
        //TODO: validate against the database
        return (Id != 0);
    }

    //checks the session for the logged in user's username
    public String getUserName(HttpServletRequest request) {
        try
        {
            Logger.logMessage("Running method commonMMHFunctions.getUserName...", Logger.LEVEL.DEBUG);
            String userName;
            HttpSession session = request.getSession();
            if (session.getAttribute("MMH_USERFULLNAME") != null) {
                userName = session.getAttribute("MMH_USERFULLNAME").toString();
                return userName;
            } else if (session.getAttribute("QC_USERFULLNAME") != null) {
                userName = session.getAttribute("QC_USERFULLNAME").toString();
                return userName;
            } else {
                return "";
            }
        }
        catch(Exception ex)
        {
            Logger.logMessage("Error getting user name... "+ex.toString(), Logger.LEVEL.ERROR);
            return "";
        }
    }

    //checks if the logged in user is an MMHayes account
    public boolean isMMHUser(HttpServletRequest request){
        String userName = getUserName(request);
        if(userName != null)
            if(userName.compareTo("MMHayes User") == 0)
                return true;
        return false;
    }

    public ArrayList<HashMap> isValidUser(HttpServletRequest request){
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        ArrayList<HashMap> userInfo;
        try {

            String userID = checkSession(request);
            userInfo = dm.serializeSqlWithColNames("data.common.isUserValid", new Object[]{userID});
            if (!userInfo.isEmpty()){
                HashMap user = userInfo.get(0);
                if (!user.containsKey("ACTIVE") || (user.get("ACTIVE").toString().equals("false") || user.get("ACTIVE").toString().equals("0"))){
                    errorCodeList.add(commonErrorMessage("User ID : " + userID + " is currently not active"));
                }
                if (!user.containsKey("LOCKED") || (user.get("LOCKED").toString().equals("true") || user.get("LOCKED").toString().equals("1"))){
                    errorCodeList.add(commonErrorMessage("User ID : " + userID + " is currently not locked"));
                }
            } else {
                errorCodeList.add(commonErrorMessage("Unable to verify user credentials"));
            }
        } catch (Exception e) {
            errorCodeList.add(commonErrorMessage("Unable to verify user credentials"));
        }
        return errorCodeList;
    }

    public boolean isUsingKMS(HttpServletRequest request){
        String usingKMS = "false";
        try{
            usingKMS = dm.getSingleField("data.kms.GetIsUsingKMS", new Object[]{PeripheralsDataManager.getTerminalMacAddress()}).toString();
        }
        catch(Exception e){
            Logger.logException(e);
        }

        return Boolean.valueOf(usingKMS);
    }

    public boolean showKMSMenus(HttpServletRequest request){

            String usingKMS = "false";

            try {
                if(commonMMHFunctions.checkSession(request).equalsIgnoreCase("")){
                    throw new Exception("Invalid Access");
                }
                String loggedInUser = request.getSession().getAttribute("QC_USERID").toString();
                usingKMS = dm.getSingleField("data.kms.ShowKMSMenus", new Object[]{loggedInUser}).toString();
            }
            catch(Exception e){
                Logger.logMessage("Error in method: showKMSMenus: "+e.getMessage(), Logger.LEVEL.ERROR);
                Logger.logException(e);
            }

        return Boolean.valueOf(usingKMS);
    }

    // Returns a user's company ID from the database, given a user ID
    public long getCompanyId(Long userId) {
        long companyId = 0;
        try {
            Object companyIdObject = new DynamicSQL("common.getCompanyId").addValue(1, userId).getSingleField(dm);
            if (tryParseLong(companyIdObject.toString())) {
                companyId = Long.parseLong(companyIdObject.toString());
            }
        } catch (Exception e) {
            companyId = 0;
        }
        return companyId;
    }

    // Returns a user's company ID from the session
    public static long getCompanyId(HttpServletRequest request) {
        long companyId = 0;
        try {
            String companyIdString = "";
            HttpSession session = request.getSession();
            if (session.getAttribute("MMH_COMPANYID") != null) {
                companyIdString = session.getAttribute("MMH_COMPANYID").toString();
            } else if (session.getAttribute("QC_COMPANYID") != null) {
                companyIdString = session.getAttribute("QC_COMPANYID").toString();
            }
            if (tryParseLong(companyIdString)) {
                companyId = Long.parseLong(companyIdString);
            }
        } catch (Exception e) {
            companyId = 0;
        }
        return companyId;
    }

    // Returns a user's currently selected language ID
    public static int getLanguageId(HttpServletRequest request) {
        int langId = 0;
        try {
            String langIdString = "";
            HttpSession session = request.getSession();
            if (session.getAttribute("MMH_LANGUAGE") != null) {
                langIdString = session.getAttribute("MMH_LANGUAGE").toString();
            }
            if (tryParseInt(langIdString)) {
                langId = Integer.parseInt(langIdString);
            }
        } catch (Exception e) {
            langId = 0;
        }
        return langId;
    }

    public static String getMMHUploadedFilePath(){
        //determine MMH directory for storing uploaded files
        return getMMH_Home() + File.separator + PATH_APPLICATION + File.separator + getInstanceName();
    }

    public static String getMMH_Home(){
        // Try to get the home path, which should be in the environmental variable
        String envVar = System.getenv("MMH_HOME");

        if (envVar != null && envVar.length() > 0) {
            return envVar;
        }
        else { // otherwise default to c drive
            return "C:"+File.separator+"MMHayes"+File.separator;
        }
    }

    public static String getInstancePath(){
        // RETURNS as String >> {MMH_HOME}\application\{WAR}
        String instanceName;
        if (MMHServletStarter.isDevEnv()) {
            instanceName = "qc"; //default to QC for dev environments
        } else {
            instanceName = com.mmhayes.common.utils.MMHServletStarter.getInstanceIdentifier();
        }
        return getMMH_Home() + PATH_APPLICATION + File.separator + instanceName + File.separator;
    }

    public static String getInstanceName(){
        //get instance name
        String instanceName;
        if (MMHServletStarter.isDevEnv()) {
            instanceName = "qc"; //default to QC for dev environments
        } else {
            instanceName = com.mmhayes.common.utils.MMHServletStarter.getInstanceIdentifier();
        }
        return instanceName;
    }

    //determine MMH directory for storing uploaded files
    public static String getInterfacesPath(){

        final String PATH_INTERFACES = "Interfaces";
        return getInstancePath() + PATH_INTERFACES + File.separator;
    }

    /**
     * <p>Queries the database to get the QC base URL.</p>
     *
     * @return The QC base URL.
     */
    public String getQCBaseURL () {

        DynamicSQL sql = new DynamicSQL("data.kitchenPrinter.GetQCBaseURL");
        Object queryRes = sql.getSingleField(dm);
        if ((queryRes != null) && (StringFunctions.stringHasContent(queryRes.toString()))) {
            return queryRes.toString();
        }

        return null;
    }

    public HashMap getUploadedFile(Integer uploadedFileID){
        ArrayList<HashMap> temp;
        try {
            temp=dm.parameterizedExecuteQuery("data.common.getFile", new Object[]{uploadedFileID},true);
            if(temp.size() > 0){
                return temp.get(0);
            }
        }catch (Exception e){
            Logger.logMessage("Unable to retrieve uploaded file information.", Logger.LEVEL.ERROR);
            logStackTrace(e);
        }
        return null;
    }

    //returns the user ID as a long
    public static int getBrowserTimezoneOffset(HttpServletRequest request) {
        try {
            HttpSession session = request.getSession();
            return (session.getAttribute("MMH_TZOFFSET") != null && tryParseInt(session.getAttribute("MMH_TZOFFSET").toString()) ? Integer.parseInt(session.getAttribute("MMH_TZOFFSET").toString()) : 0);
        } catch (Exception e) {
            return 0;
        }
    }

    //expires all ds keys for a given employee, essentially logging them out from myQuickcharge
    public void expireDSKeys(String employeeID) {
        Logger.logMessage("Attempting to expire DS keys for Employee: " + employeeID, Logger.LEVEL.DEBUG);
        try {
            //expire the DSKey in the DB
            dm.parameterizedExecuteNonQuery("data.validation.expireDSKeys",
                    new Object[]{
                            employeeID
                    }
            );
        } catch (Exception e) {
            Logger.logException(e);
        }
    }

    //expires all ds keys for a given user, essentially logging them out from myQuickcharge
    public void expireDSKeysUsers(String userID) {
        Logger.logMessage("Attempting to expire DS keys for User: " + userID, Logger.LEVEL.DEBUG);
        try {
            //expire the DSKey in the DB
            dm.parameterizedExecuteNonQuery("data.validation.expireDSKeysUsers",
                    new Object[]{
                            userID
                    }
            );
        } catch (Exception e) {
            Logger.logException(e);
        }
    }

    //resets failed login attempts for a manager (QC User)
    public void setManagerFailedAttempts(Integer failedAttempts, String qcUserID) {
        try {
            dm.parameterizedExecuteNonQuery("data.common.setManagerFailedAttempts",
                new Object[]{
                    failedAttempts.toString(),
                    qcUserID
                }
            );
        } catch (Exception ex) {
            Logger.logException(ex);
        }
    }

    //resets failed login attempts for an employee (QC account)
    public void setEmployeeFailedAttempts(Integer failedAttempts, String employeeID) {
        try {
            dm.parameterizedExecuteNonQuery("data.common.setEmployeeFailedAttempts",
                new Object[]{
                    failedAttempts.toString(),
                    employeeID
                }
            );
        } catch (Exception ex) {
            Logger.logException(ex);
        }
    }

/* END - Session Handlers */

/* START - Page Permission Handlers */

    // Get the last part of the permission map
    private static boolean checkPermissionEntry(HashMap permissionMap, String permissionType) {
        try {
            if (permissionMap.containsKey(permissionType)) {
                return (boolean)permissionMap.get(permissionType);
            }
        } catch (Exception e) {
            Logger.logMessage("No permission defined for " + permissionType + ". Rejecting permission.", Logger.LEVEL.ERROR);
            return false;
        }
        return false;
    }

    // Return a HashMap that has all the save-type permissions (edit/add/delete) for the current page
    public HashMap getPageSavePermissionHashMap(HttpServletRequest request) {
        HashMap<String,Boolean> permissionSet = new HashMap<String, Boolean>();
        permissionSet.put(EDITPERMISSION, getPagePermissionByType(request, EDITPERMISSION));
        permissionSet.put(ADDPERMISSION, getPagePermissionByType(request, ADDPERMISSION));
        permissionSet.put(DELETEPERMISSION, getPagePermissionByType(request, DELETEPERMISSION));
        return permissionSet;
    }

    // Returns a true or false (as a string) that indicates if the user has view permissions on the current page
    public boolean getPagePermissionByType(HttpServletRequest request, String permissionType) {
        // Check for valid input first
        if (!(permissionType.equals(VIEWPERMISSION) || permissionType.equals(EDITPERMISSION)  || permissionType.equals(ADDPERMISSION) || permissionType.equals(DELETEPERMISSION))) {
            return false;
        }
        int pageId;
        HashMap permissionMap;
        HttpSession session = request.getSession();
        // Get the actual page ID
        try {
            pageId = Integer.parseInt(session.getAttribute("currentPageId").toString());
        } catch (Exception e) {
            Logger.logMessage("Unknown page, unable check specific permissions.", Logger.LEVEL.ERROR);
            pageId = -1;
        }
        // Get the array
        if (session.getAttribute("pagePermissions") != null) {
            permissionMap = (HashMap)session.getAttribute("pagePermissions");
            // Go to the element with the corresponding pageId
            if (permissionMap.containsKey(pageId)) {
                return checkPermissionEntry((HashMap)permissionMap.get(pageId),permissionType);
            } else if (permissionMap.containsKey(-1)) {
                // Get the default permissions
                return checkPermissionEntry((HashMap)permissionMap.get(-1),permissionType);
            } else {
                Logger.logMessage("No permissions available. Rejecting permission authorization.", Logger.LEVEL.ERROR);
            }
        }
        return false;
    }

    // Provides a HashMap of the page permission (View/Edit/Add/Delete)
    public HashMap getPagePermission(HttpServletRequest request) {
        try {
            ArrayList<String> params = new ArrayList<>();
            params.add(request.getAttribute("pageId").toString());
            params.add(checkSession(request));
            //TODO: switch over to prepared statement when functionality is available
            ArrayList<HashMap> permArray = dm.serializeSqlWithColNames("data.common.checkPagePermissions", params.toArray());
            for (HashMap m : permArray) {
                if (m.containsKey("VIEWPERMISSION") && m.containsKey("EDITPERMISSION") && m.containsKey("ADDPERMISSION") && m.containsKey("DELETEPERMISSION")) {
                    return m;
                }
            }
        } catch (Exception e) {
            Logger.logMessage("Error determining page permissions: " + e.getMessage(), Logger.LEVEL.ERROR);
        }
        return new HashMap();
    }

    //Provide a way to check if we can access a page that we are not currently on, (ex: Product Modal)
    public HashMap hasPageAccess(HttpServletRequest request, String page){
        HashMap accessHM = new HashMap();
        accessHM.put("allowed", false);
        HttpSession session = request.getSession();
        String access = ((HashMap)session.getAttribute("QC_USERPROFILE")).getOrDefault(page, "false").toString();
        if(access.compareTo("true") == 0){
            accessHM.put("allowed", true);
        }
        return accessHM;
    }


/* END - Page Permission Handlers */

/* START - Ownership Group Handlers */

    public ArrayList getSessionOwnershipGroupIDList(HttpSession session){
        if(session.getAttribute("QC_USEROWNERSHIPGROUPS") != null){ //&& session.getAttribute("QC_USERREVCENTERS").getClass().toString()){
            try{
                return (ArrayList)session.getAttribute("QC_USEROWNERSHIPGROUPS");
            }catch (Exception e){
                Logger.logMessage("Unable to retrieve user's session ownership group ids", Logger.LEVEL.ERROR);
                logStackTrace(e);
            }
        }
        return null;
    }

    public ArrayList<HashMap> getAllUserOwnershipGroupHashMap(HttpServletRequest request){
        String userID = checkSession(request);
        ArrayList<HashMap> ownershipGroups;
        if(!userID.isEmpty()){
            ownershipGroups = getUserOwnershipGroupHashMap(userID);
        }else{
            Logger.logMessage("Unable to retrieve User's Ownership Group IDs, user ID is blank", Logger.LEVEL.ERROR);
            return new ArrayList<>();
        }
        return ownershipGroups;
    }

    //get the logged in user's revenue centers as an array list of strings
    public ArrayList<String> getUserOwnershipGroupStringList(HttpServletRequest request){
        String userID = checkSession(request);
        ArrayList<String> ownershipGroupIDs = new ArrayList<>();
        ArrayList sessionOwnershipGroupIDs = getSessionOwnershipGroupIDList(request.getSession());
        if(sessionOwnershipGroupIDs != null && !userID.isEmpty()){
            ArrayList<HashMap> ownershipGroups = getUserOwnershipGroupHashMap(userID,sessionOwnershipGroupIDs);
            for(HashMap record : ownershipGroups){
                if(containsFullKey(record.get("OWNERSHIPGROUPID"))){
                    ownershipGroupIDs.add(record.get("OWNERSHIPGROUPID").toString());
                }
            }
        }else if(!userID.isEmpty()){
            ownershipGroupIDs = getUserOwnershipGroupStringList(userID);
        }else{
            Logger.logMessage("In method: "+className+"."+"getUserOwnershipGroupStringList", Logger.LEVEL.ERROR);
            Logger.logMessage("Unable to determine user ID", Logger.LEVEL.ERROR);
            return new ArrayList<>();
        }
        return ownershipGroupIDs;
    }

    //overloaded protected method for getting the logged in user's ownership groups as an array list of strings
    public ArrayList<String> getUserOwnershipGroupStringList(String userID){
        ArrayList<HashMap> ownershipGroups = getUserOwnershipGroupHashMap(userID);
        ArrayList<String> ownershipList = new ArrayList<>();
        for(HashMap record : ownershipGroups){
            if(containsFullKey(record.get("OWNERSHIPGROUPID"))){
                ownershipList.add(record.get("OWNERSHIPGROUPID").toString());
            }
        }
        return ownershipList;
    }

    public ArrayList<HashMap> getUserOwnershipGroupHashMap(HttpServletRequest request){
        String userID = checkSession(request);
        ArrayList sessionOwnershipIDs = getSessionOwnershipGroupIDList(request.getSession());
        ArrayList<HashMap> ownershipGroupIDs = new ArrayList<>();
        if(sessionOwnershipIDs != null && !userID.isEmpty()){
            ownershipGroupIDs = getUserOwnershipGroupHashMap(userID,sessionOwnershipIDs);
        }else if(!userID.isEmpty()){
            ownershipGroupIDs = getUserOwnershipGroupHashMap(userID);
        }else{
            Logger.logMessage("In method: "+className+"."+"getUserOwnershipGroupHashMap(HttpServletRequest)", Logger.LEVEL.ERROR);
            Logger.logMessage("Unable to determine user ID", Logger.LEVEL.ERROR);
        }
        return ownershipGroupIDs;
    }

    public ArrayList<HashMap> getUserOwnershipGroupHashMap(String userID){
        return getUserOwnershipGroupHashMap(userID,null);
    }

    public ArrayList<HashMap> getUserOwnershipGroupHashMap(String userID, ArrayList sessionOwnershipIDs){
        ArrayList<HashMap> ownershipGroups;
        if(sessionOwnershipIDs != null){
            String ownershipArg = ArrayListToCSV(sessionOwnershipIDs);
            //if sessions ids are empty no need to ping db
            if(ownershipArg.isEmpty()){
                return new ArrayList<>();
            }
            try{
                ownershipGroups = new DynamicSQL("data.common.getSelectedOwnershipGroups").addIDList(1, ownershipArg).serialize(dm);
            }catch (Exception e){
                Logger.logMessage("Unable to retrieve user ID : " + userID+ " session ownership groups", Logger.LEVEL.ERROR);
                logStackTrace(e);
                return commonErrorMessageList("Unable to retrieve user's ownership groups");
            }
        }else{
            try{
                ownershipGroups = new DynamicSQL("data.common.getUserOwnershipGroups").addIDList(1, userID).serialize(dm);
            }catch (Exception e){
                Logger.logMessage("Unable to retrieve user ID : " + userID+ " ownership groups", Logger.LEVEL.ERROR);
                logStackTrace(e);
                return commonErrorMessageList("Unable to retrieve user's ownership groups");
            }
        }
        return ownershipGroups;
    }

    public String userOwnershipSuperStringList(String userID){
        ArrayList<HashMap> ownershipGroups = getUserOwnershipGroupHashMap(userID);
        if(ownershipGroups.size() > 0){
            HashMap temp = ownershipGroups.get(0);
            if(containsFullKey(temp.get("SUPERACCESS"))){
                if(temp.get("SUPERACCESS").equals("1")){
                    return "-1";
                }
            }
        }
        ArrayList<String> ownershipList = new ArrayList<>();
        for(HashMap record : ownershipGroups){
            if(containsFullKey(record.get("OWNERSHIPGROUPID"))){
                ownershipList.add(record.get("OWNERSHIPGROUPID").toString());
            }
        }
        return ArrayListToCSV(ownershipList);
    }

/* END - Ownership Group Handlers */

/* START - Revenue Center Handlers */

    public ArrayList getSessionRevCenterIDList(HttpSession session){
        if(session.getAttribute("QC_USERREVCENTERS") != null){ //&& session.getAttribute("QC_USERREVCENTERS").getClass().toString()){
            try{
                return (ArrayList)session.getAttribute("QC_USERREVCENTERS");
            }catch (Exception e){
                Logger.logMessage("Unable to retrieve user's session revenue center ids.", Logger.LEVEL.ERROR);
                logStackTrace(e);
            }
        }
        return null;
    }

    //get all the revenue centers in the system
    public ArrayList getAllRevenueCenters(HttpServletRequest request){
        try
        {
            Logger.logMessage("Running method commonMMHFunctions.getAllRevenueCenters...", Logger.LEVEL.DEBUG);
            if (!checkSession(request).equals("")) {
                ArrayList<HashMap> revCenters = dm.serializeSqlWithColNames("data.common.getRevenueCenters");
                return revCenters;
            } else {
                Logger.logMessage("Invalid Access in getAllRevenueCenters().", Logger.LEVEL.ERROR);
                return commonErrorMessageList("Invalid Access.");
            }
        }
        catch (Exception ex)
        {
            Logger.logMessage("Error obtaining all revenue centers - Exception thrown:"+ex.toString(), Logger.LEVEL.ERROR);
            return commonErrorMessageList("Error obtaining all revenue centers.");
        }
    }

    public ArrayList<HashMap> getAllUserRevenueCentersHashMap(HttpServletRequest request){
        String userID = checkSession(request);
        ArrayList<HashMap> revenueCenters;
        if(!userID.isEmpty()){
            revenueCenters = getUserRevenueCentersHashMap(userID);
        }else{
            Logger.logMessage("Unable to retrieve User's Revenue Center ID, user ID is blank", Logger.LEVEL.ERROR);
            return new ArrayList<>();
        }
        return revenueCenters;
    }

    //get the logged in user's revenue centers as an array list of strings
    public ArrayList<String> getUserRevenueCentersStringList(HttpServletRequest request){
        String userID = checkSession(request);
        ArrayList<String> revenueCenterIDs = new ArrayList<>();
        ArrayList sessionRevCenterIDs = getSessionRevCenterIDList(request.getSession());
        if(sessionRevCenterIDs != null && !userID.isEmpty()){
            ArrayList<HashMap> revenueCenters = getUserRevenueCentersHashMap(userID,sessionRevCenterIDs);
            for(HashMap record : revenueCenters){
                if(containsFullKey(record.get("REVENUECENTERID"))){
                    revenueCenterIDs.add(record.get("REVENUECENTERID").toString());
                }
            }
        }else if(!userID.isEmpty()){
            revenueCenterIDs = getUserRevenueCentersStringList(userID);
        }else{
            Logger.logMessage("In method: "+className+"."+"getUserRevenueCentersStringList", Logger.LEVEL.ERROR);
            Logger.logMessage("Unable to determine user ID", Logger.LEVEL.ERROR);
            return new ArrayList<>();
        }
        return revenueCenterIDs;
    }

    public String getUserRevenueCentersString(HttpServletRequest request) {
        StringBuilder revCenterStringBuilder = new StringBuilder();
        revCenterStringBuilder.append("(");
        ArrayList<String> revCenterList = getUserRevenueCentersStringList(request);

//        for (String revCenter : revCenterList) {
        int listSize = revCenterList.size();
        for (int i = 0; i < listSize; i++) {
            revCenterStringBuilder.append(revCenterList.get(i));

            // only add a comma and a space if this isn't the last item
            if (i < listSize - 1) {
                revCenterStringBuilder.append(", ");
            }
        }

        revCenterStringBuilder.append(")");

        return revCenterStringBuilder.toString();
//        return revCenterList.get(0);
    }

    //overloaded protected method for getting the logged in user's ownership groups as an array list of strings
    public ArrayList<String> getUserRevenueCentersStringList(String userID){
        ArrayList<HashMap> revenueCenters = getUserRevenueCentersHashMap(userID);
        ArrayList<String> revenueCenterList = new ArrayList<>();
        for(HashMap record : revenueCenters){
            if(containsFullKey(record.get("REVENUECENTERID"))){
                revenueCenterList.add(record.get("REVENUECENTERID").toString());
            }
        }
        return revenueCenterList;
    }

    public ArrayList<HashMap> getUserRevenueCentersHashMap(HttpServletRequest request){
        String userID = checkSession(request);
        ArrayList sessionRevenueCenterIDs = getSessionRevCenterIDList(request.getSession());
        ArrayList<HashMap> revenueCenters = new ArrayList<>();
        if(sessionRevenueCenterIDs != null && !userID.isEmpty()){
            revenueCenters = getUserRevenueCentersHashMap(userID, sessionRevenueCenterIDs);
        }else if(!userID.isEmpty()){
            revenueCenters = getUserRevenueCentersHashMap(userID);
        }else{
            Logger.logMessage("In method: "+className+"."+"getUserRevenueCenterHashMap(HttpServletRequest)", Logger.LEVEL.ERROR);
            Logger.logMessage("Unable to determine user ID", Logger.LEVEL.ERROR);
        }
        return revenueCenters;
    }

    public ArrayList<HashMap> getUserRevenueCentersHashMap(String userID){
        return getUserRevenueCentersHashMap(userID, null);
    }

    public ArrayList<HashMap> getUserRevenueCentersHashMap(String userID, ArrayList sessionRevenueCenterIDs){
        ArrayList<HashMap> revenueCenters;
        if(sessionRevenueCenterIDs != null){
            String revCenterArg = ArrayListToCSV(sessionRevenueCenterIDs);
            //if session ids are empty no reason to ping db
            if(revCenterArg.isEmpty()){
                return new ArrayList<>();
            }
            try{
                revenueCenters = new DynamicSQL("data.common.getSelectedRevenueCenters").addIDList(1, sessionRevenueCenterIDs).serialize(dm);//dm.serializeSqlWithColNames("data.common.getSelectedRevenueCenters", new Object[]{revCenterArg},false);
            }catch (Exception e){
                Logger.logMessage("Unable to retrieve user ID : " + userID+ " session revenue centers", Logger.LEVEL.ERROR);
                logStackTrace(e);
                return commonErrorMessageList("Unable to retrieve user's revenue centers");
            }
        }else{
            try{
                revenueCenters = dm.serializeSqlWithColNames("data.common.getUserRevenueCenters", new Object[]{userID});
            }catch (Exception e){
                Logger.logMessage("Unable to retrieve user ID : " + userID+ " revenue centers", Logger.LEVEL.ERROR);
                logStackTrace(e);
                return commonErrorMessageList("Unable to retrieve user's revenue centers");
            }
        }
        return revenueCenters;
    }

    public String userRevenueSuperStringList(String userID){
        ArrayList<HashMap> revenueCenters = getUserRevenueCentersHashMap(userID);
        if(revenueCenters.size() > 0){
            HashMap temp = revenueCenters.get(0);
            if(containsFullKey(temp.get("SUPERACCESS"))){
                if(temp.get("SUPERACCESS").equals("1")){
                    return "-1";
                }
            }
        }
        ArrayList<String> revCenterList = new ArrayList<>();
        for(HashMap record : revenueCenters){
            if(containsFullKey(record.get("REVENUECENTERID"))){
                revCenterList.add(record.get("REVENUECENTERID").toString());
            }
        }
        return ArrayListToCSV(revCenterList);
    }

    public ArrayList<ArrayList> getSelectedRevenueCenters(String recordID, String objectID) {
        // @param recordID : the recordID to get the selected revenue centers for
        // @param objectID : the objectID to get the selected revenue centers for
        //
        // Gets an arraylist of all the revenue centers that are mapped to the given
        // record id and object id combination

        Logger.logMessage("In function: com.mmhayes.common.dataaccess.commonMMHFunctions.getSelectedRevenueCenters", Logger.LEVEL.DEBUG);

        try {
            // get the selected revenue centers
            ArrayList<ArrayList> selectedRevCenters =
                    dm.serializeSql("data.common.getSelectedRevenueCenterMappings",
                            new Object[]{recordID, objectID});

            return selectedRevCenters;
        }
        catch (Exception e) {
            Logger.logMessage("Unable to get selected revenue centers with record id: " + recordID +
                    " and object id: " + objectID + ".", Logger.LEVEL.ERROR);
            logStackTrace(e);
            return new ArrayList<>();
        }
    }

    public ArrayList<String> getSelectedRevenueCentersStringList(String recordID, String objectID) {
        // @param recordID : the recordID to get the selected revenue centers for
        // @param objectID : the objectID to get the selected revenue centers for
        //
        // Gets an arraylist of strings of all the revenue centers that are mapped to the given
        // record id and object id combination
        Logger.logMessage("In function: com.mmhayes.common.dataaccess.commonMMHFunctions.getSelectedRevenueCentersStringList", Logger.LEVEL.DEBUG);

        ArrayList<ArrayList> selectedRevCenters = getSelectedRevenueCenters(recordID, objectID);
        ArrayList<String> selectedRevCenterStrings = new ArrayList<>();

        try {
            // Loop through every outer loop
            for (int i = 0; i < selectedRevCenters.size(); i++) {
                ArrayList innerList = selectedRevCenters.get(i);
                // loop through every inner loop (should only be one item, but just in case)
                for (int j = 0; j < innerList.size(); j++) {
                    // get the strings inside the inner loop (the revenue center ids) and add them
                    // to the cumulative list of revenue center ids
                    selectedRevCenterStrings.add(innerList.get(j).toString());
                }
            }
            return selectedRevCenterStrings;
        }
        catch (Exception e) {
            Logger.logMessage("Unable to get selected revenue center string list with record id: " + recordID +
                    " and object id: " + objectID + ".", Logger.LEVEL.ERROR);
            logStackTrace(e);
            return new ArrayList<>();
        }
    }

    public ArrayList<HashMap> saveRevenueCenters(HttpServletRequest request, ArrayList revCenters, String recordID, String objectID){
        // @param request : the request that initiated this function call
        // @param revCenters : the list of revenue centers to ensure are mapped
        // @param record id: the record id the revenue center mappings are for
        // @param object id: the object id the revenue center mappings are for
        //
        // Saves a set of revenue centers as the selected revenue centers for a given object

        Logger.logMessage("In function: com.mmhayes.common.dataaccess.commonMMHFunctions.saveRevenueCenters", Logger.LEVEL.DEBUG);
        DataHandlerProperties temp = new DataHandlerProperties();
        temp.setMappingArrayLists("REVENUECENTERIDS",revCenters);
        return saveRevenueCenters(request, temp,recordID,objectID);
    }

    public ArrayList<HashMap> saveRevenueCenters(HttpServletRequest request, DataHandlerProperties prop, String recordID, String objectID){
        // @param request : the request that initiated this function call
        // @param prop : the properties associated with the sql parameters
        // @param record id: the record id the revenue center mappings are for
        // @param object id: the object id the revenue center mappings are for
        //
        // Saves a set of revenue centers as the selected revenue centers for a given object

        Logger.logMessage("In function: com.mmhayes.common.dataaccess.commonMMHFunctions.saveRevenueCenters", Logger.LEVEL.DEBUG);

        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        if(prop.checkMappingArrayLists("REVENUECENTERIDS")){  //if any changes to revenue centers


            try{
                // get the list of revenue centers the user had selected at the point this function was called
                ArrayList<String> selectedRevCenters = new ArrayList(new LinkedHashSet(toArrayList(prop.getMappingArrayListProp("REVENUECENTERIDS"))));
                // go through each selected revenue center, and set the value to the raw revenue center id
                for (int i = 0; i < selectedRevCenters.size(); i++) {
                    selectedRevCenters.set(i,
                            selectedRevCenters.get(i).toString()
                                    .replace("[","")
                                    .replace("]",""));
                }

                // get the raw object id
                objectID=objectID.replace("[","").replace("]","");

                // get a list of all the users revenue centers
                ArrayList<String> userRevCenters = getUserRevenueCentersStringList(request);

                // get an arraylist of the revenue centers that were selected (before this function was called)
                ArrayList<String> prevSelectedRevCenters = getSelectedRevenueCentersStringList(recordID, objectID);

                // create an arraylist to keep track of revenue centers we add
                ArrayList<String> addedRevCenters = new ArrayList<>();

                // go through each of the user's revenue centers
                for (String revCenterID : userRevCenters) {

                    // if this revenue center was selected
                    if (selectedRevCenters.contains(revCenterID)) {
                        // if it was not previously selected (if it was selected, just leave it selected)
                        if (!prevSelectedRevCenters.contains(revCenterID)) {
                            // if the revenue center is not already added, is not equal to 0, is not an empty value,
                            // and is a valid integer then add a mapping for that revenue center
                            if (!addedRevCenters.contains(revCenterID) && !revCenterID.equals("0") &&
                                    !revCenterID.isEmpty() && tryParseInt(revCenterID)) {
                                dm.serializeUpdate("data.common.createRevenueCenterMapping",
                                        new Object[]{recordID, revCenterID, objectID});
                            }
                        }

                    }
                    // else, if this revenue center was previously selected, deselect it
                    else if (prevSelectedRevCenters.contains(revCenterID)) {
                        dm.serializeUpdate("data.common.deleteRevenueCenterMapping",
                                new Object[]{revCenterID, recordID, objectID});
                    }

                }
            } catch (final Exception e){
                Logger.logMessage("Unable to save revenue centers with record id: " + recordID +
                        " and object id: " + objectID + ".", Logger.LEVEL.ERROR);
                logStackTrace(e);

                errorCodeList.add(commonErrorMessage("Unable to map revenue centers"));
            }
        }
        return errorCodeList;
    }

    @Deprecated
    public ArrayList<HashMap> saveRevenueCenters(ArrayList revCenters, String recordID, String objectID){
        // DEPRECATED due to lack of ownership filtering - introduces bugs where users can modify the
        // revenue center mappings of revenue centers they do not control
        //
        // @param revCenters : the list of revenue centers to ensure are mapped
        // @param record id: the record id the revenue center mappings are for
        // @param object id: the object id the revenue center mappings are for
        //
        // Saves a list of revenue center mappings according to what is passed in in the revCenters list, recordID,
        // and objectID
        // Does not filter deleting or creating based on what the current user owns
        DataHandlerProperties temp = new DataHandlerProperties();
        temp.setMappingArrayLists("REVENUECENTERIDS",revCenters);
        return saveRevenueCenters(temp,recordID,objectID);
    }

    @Deprecated
    public ArrayList<HashMap> saveRevenueCenters(DataHandlerProperties prop, String recordID, String objectID){
        // DEPRECATED due to lack of ownership filtering - introduces bugs where users can modify the
        // revenue center mappings of revenue centers they do not control
        //
        // @param prop : the properties associated with the sql parameters
        // @param record id: the record id the revenue center mappings are for
        // @param object id: the object id the revenue center mappings are for
        //
        // Saves revenue center mappings according to what is passed in in the prop, recordID, and objectID
        // Does not filter deleting or creating based on what the current user owns

        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        if(prop.checkMappingArrayLists("REVENUECENTERIDS")){  //if any changes to revenue centers
            ArrayList<String> duplicates = new ArrayList<>();

            ArrayList revCenters = new ArrayList(new LinkedHashSet(toArrayList(prop.getMappingArrayListProp("REVENUECENTERIDS"))));

            try{
                objectID=objectID.replace("[","").replace("]","");
                dm.serializeUpdate("data.common.deleteRevenueCenterMappings", new Object[]{recordID,objectID});  //delete all of the currents
                for(int i=0;i<revCenters.size();i++){
                    String revCenterID = revCenters.get(i).toString().replace("[","").replace("]","");
                    if(!duplicates.contains(revCenterID) && !revCenterID.equals("0") && !revCenterID.isEmpty() && tryParseInt(revCenterID)){  //dont save rev centers with ids of 0 or duplicate rev centers
                        duplicates.add(revCenterID);
                        dm.serializeUpdate("data.common.createRevenueCenterMapping", new Object[]{recordID,revCenterID,objectID});
                    }
                }
            }catch (final Exception e){
                errorCodeList.add(commonErrorMessage("Unable to map revenue centers"));
            }
        }
        return errorCodeList;
    }

    /* Used for Find Method on any revenue center select box with commonMMHFunctions as the selectLoadObject
       @params - revCenterID - this is the ID the select can't find, it will auto-populate based on the db data
       @returns - revenue center name to match the ID and add as option to select
     */
    public ArrayList<HashMap> findRevCenter(final Integer revCenterID, HttpServletRequest request) {
        DataManager dmgr = new DataManager();
        Object[] params = prependObjectArray(new ArrayList<>(), revCenterID);
        return dmgr.serializeSqlWithColNames("data.inventoryEditor.findRevCenter", params, false);
    }
/* END - Revenue Center Handlers */

/* BEGIN - Session Priviledge Handlers */

    public HashMap getSessionPriviledges(HttpServletRequest request){
        HttpSession session = request.getSession();
        HashMap ret = new HashMap();
        if(session.getAttribute("QC_USERREVCENTERS") != null){
            ret.put("QC_USERREVCENTERS",session.getAttribute("QC_USERREVCENTERS"));
        }else{
            ret.put("QC_USERREVCENTERS","");
        }

        if(session.getAttribute("QC_USEROWNERSHIPGROUPS") != null){
            ret.put("QC_USEROWNERSHIPGROUPS",session.getAttribute("QC_USEROWNERSHIPGROUPS"));
        }else{
            ret.put("QC_USEROWNERSHIPGROUPS","");
        }
        return ret;
    }

    public ArrayList<HashMap> saveSessionPriviledges(HashMap priviledges, HttpServletRequest request){
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        try{
            HttpSession session = request.getSession();
            if(priviledges.get("QC_USEROWNERSHIPGROUPS") != null){
                session.setAttribute("QC_USEROWNERSHIPGROUPS",priviledges.get("QC_USEROWNERSHIPGROUPS"));
            }
            if(priviledges.get("QC_USERREVCENTERS") != null){
                session.setAttribute("QC_USERREVCENTERS",priviledges.get("QC_USERREVCENTERS"));
            }
        }catch (Exception e){
            errorCodeList.add(commonErrorMessage("Unable to set session privileges."));
        }

        return errorCodeList;
    }

/* END - Session Priviledge Handlers */

/* START - Location Handlers */

    //get the logged in user's locations as an array list of hash maps
    public ArrayList<HashMap> getUserLocationHashMap(HttpServletRequest request) {
        return getUserLocationHashMap(request, null);
    }
    public ArrayList<HashMap> getUserLocationHashMap(HttpServletRequest request, Long Id) {
        try {
            DataManager dmgr = new DataManager();
            if (Id != null) {
                Object[] params = prependObjectArray(new ArrayList<>(), Id);
                return dmgr.serializeSqlWithColNames("data.common.getLocationLookup", params, false);
            } else {
                Object[] params = prependObjectArray(new ArrayList<>(), checkSession(request));
                return dmgr.serializeSqlWithColNames("data.common.getUserLocationList", params, false);
            }
        } catch (Exception e) {
            Logger.logMessage("Error obtaining user's locations (getUserLocationHashMap)" + e.toString(), Logger.LEVEL.ERROR);
            return commonErrorMessageList("Error with the user's locations");
        }
    }

/* END - Location Handlers */

/* START - Pay Roll Groups Handling*/

    //get the logged in user's pay roll groups as a string
    public String getUserPayRollGroupsString(HttpServletRequest request){
        try
        {
            Logger.logMessage("Running method commonMMHFunctions.getUserPayRollGroups..", Logger.LEVEL.DEBUG);
            String userID = checkSession(request);
            ArrayList userPayRollList;
            String userPayRollGroupString;
            //The issue here is the rev centers are being stored in the db as comma delimited string
            userPayRollList = dm.serializeSql("data.common.getUserPayRollGroups",  new Object[] {userID});
            //create a string with the users ownership groups
            userPayRollGroupString = userPayRollList.get(0).toString().replace("[","").replace("]","");
            return userPayRollGroupString;
        }
        catch (Exception ex)
        {
            Logger.logMessage("Error within method commonMMHFunctions.getUserPayRollGroups:"+ex.toString(), Logger.LEVEL.ERROR);
            return "";
        }
    }

    //overloaded protected method for getting logged in user's pay roll groups as a string
    public String getUserPayRollGroupsString(String userID){
        try
        {
            Logger.logMessage("Running method commonMMHFunctions.getUserPayRollGroupsString..", Logger.LEVEL.DEBUG);
            ArrayList userPayRollList;
            String userPayRollGroupString;
            //The issue here is the rev centers are being stored in the db as comma delimited string
            userPayRollList = dm.serializeSql("data.common.getUserPayRollGroups", new Object[] {userID});
            //create a string with the users ownership groups
            userPayRollGroupString = userPayRollList.get(0).toString().replace("[","").replace("]","");
            return userPayRollGroupString;
        }
        catch (Exception ex)
        {
            Logger.logMessage("Error within method commonMMHFunctions.getUserPayRollGroupsString:"+ex.toString(), Logger.LEVEL.ERROR);
            return "";
        }
    }

    public ArrayList getAccessCodes(HttpServletRequest request, String ownershipGroupID) {
        Logger.logMessage("Getting the Access Codes in commonMMHFunctions.getAccessCodes", Logger.LEVEL.TRACE);
        if(ownershipGroupID == null) {
            Logger.logMessage("No ownershipGroupID passed to the method");
            return new ArrayList();
        }
        try {
            ArrayList accessCodes = null;
            int ogid = Integer.parseInt(ownershipGroupID);
            if(ogid != -1) {
                accessCodes = dm.parameterizedExecuteQuery("data.accountGroupEditor.getAccessCodesByOwnershipGroupID", new Object[]{ ogid }, true);
            } else {
                accessCodes = dm.parameterizedExecuteQuery("data.accountGroupEditor.getAllAccessCodes", new Object[]{}, true);
            }
            if(accessCodes.size() > 0) {
                return accessCodes;
            }
        } catch(Exception e) {
            Logger.logMessage("Error getting the access codes for the accountGroupEditor");
            Logger.logException(e);
        }
        return new ArrayList();
    }

/* END - Pay Roll Groups Handling */

/* START - QC Terminal Handling */

    //get the logged in user's revenue centers as an array list of hash maps

    //refactored this method, was trying to pull user's terminal ids from
    //user profile, which is the old way of doing it(column has been removed from table) - rkb
    public ArrayList<HashMap> getUserTerminals(HttpServletRequest request) {
        return getUserTerminals(request,false);
    }

    public ArrayList<HashMap> getUserTerminalsWithDetails(HttpServletRequest request, final String terminalProfileID) {
        return getUserTerminalsWithDetails(request, false, terminalProfileID);
    }

    public ArrayList<HashMap> getUserTerminals(HttpServletRequest request, boolean negativeOneFlag){
        try{
            Logger.logMessage("Running method commonMMHFunctions.getUserTerminals...", Logger.LEVEL.DEBUG);
            String userID = checkSession(request);
            ArrayList<HashMap> terminals;
            if(!userID.isEmpty()){
                String sqlString;
                if(negativeOneFlag){
                    sqlString="data.common.getSuperUserTerminals";
                }else{
                    sqlString= "data.common.getUserTerminals";
                }
                terminals = dm.serializeSqlWithColNames(sqlString, new Object[]{userID},false,true);
            }else{
                //TODO: user not logged in
                Logger.logMessage("Could not confirm user identity.", Logger.LEVEL.ERROR);
                return commonErrorMessageList("Could not confirm user's identity.");
            }
            return terminals;
        }catch (Exception ex)
        {
            Logger.logMessage("Error obtaining user's terminals (getUserTerminals)"+ex.toString(), Logger.LEVEL.ERROR);
            return commonErrorMessageList("Error retrieving user's terminals");
        }

    }

    public ArrayList<HashMap> getUserTerminalsWithDetails(HttpServletRequest request, boolean negativeOneFlag, String terminalProfileID){
        try{
            Logger.logMessage("Running method commonMMHFunctions.getUserTerminals...", Logger.LEVEL.DEBUG);
            String userID = checkSession(request);
            ArrayList<HashMap> terminals;
            if(!userID.isEmpty()){
                String sqlString;
                if(negativeOneFlag){
                    sqlString="data.common.getSuperUserTerminalsWithDetails";
                }else{
                    sqlString= "data.common.getUserTerminalsWithDetails";
                }
                terminals = dm.serializeSqlWithColNames(sqlString, new Object[]{userID, terminalProfileID},false,true);
            }else{
                //TODO: user not logged in
                Logger.logMessage("Could not confirm user identity", Logger.LEVEL.ERROR);
                return commonErrorMessageList("Could not confirm user's identity");
            }
            return terminals;
        }catch (Exception ex)
        {
            Logger.logMessage("Error obtaining user's terminals (getUserTerminals)"+ex.toString(), Logger.LEVEL.ERROR);
            return commonErrorMessageList("Error retrieving user's terminals");
        }
    }

    public String getUserTerminalCSV(HttpServletRequest request,boolean negativeOneFlag){
        ArrayList<HashMap> terminals = getUserTerminals(request,negativeOneFlag);
        String terminalsIDs = "";
        if (terminals != null && !terminals.isEmpty()) {
            for(HashMap terminal : terminals){
                if(containsFullKey(terminal.get("TERMINALID"))){
                    terminalsIDs+=terminal.get("TERMINALID").toString()+",";
                }
            }
            terminalsIDs=terminalsIDs.substring(0,terminalsIDs.length()-1);
        }
        return terminalsIDs;
    }

    //get the QC items for any valid terminal passed
    public ArrayList<HashMap> getTerminalQCItems(String terminalID, HttpServletRequest request) {
        ArrayList<HashMap> terminalQCItems = new ArrayList<>();
        try
        {
            Logger.logMessage("Running method commonMMHFunctions.getTerminalQCItems...", Logger.LEVEL.DEBUG);

            if (!checkSession(request).equals("")) { //check that user is logged in
                //TODO: Possible SQL Injection! (Should be safe due to java typing) but serializeUpdate does not paramertize queries! -jrmitaly
                if (!terminalID.equals("-1")) {
                    Integer terminalID_int = Integer.parseInt(terminalID);
                    terminalQCItems = dm.serializeSqlWithColNames("data.common.getTerminalQCItems", new Object[]{terminalID_int});
                }
            } else {
                terminalQCItems.add(commonErrorMessage("Invalid Access."));
            }
        }
        catch (Exception ex)
        {
            logStackTrace(ex, className, "getTerminalQCItems");
            terminalQCItems.add(commonErrorMessage("Error retrieving the terminal's QC Items."));
        }
        return terminalQCItems;
    }

/* END - QC Terminal Handling */

/* START - Password Helpers */

    // Encrypt a string as a password
    public static String commonStringToPassword(String plainTextPassword) {
        try {
            if (plainTextPassword.length() <= 0) { // A password must be supplied
                Logger.logMessage("No password supplied in commonStringToPassword", Logger.LEVEL.ERROR);
                return "";
            } else if (plainTextPassword.length() > MAX_PASSWORD_LENGTH) { // A max password length is defined so that we can know how large to make the database fields
                Logger.logMessage("Supplied password is too long (" + plainTextPassword.length() + " > " + MAX_PASSWORD_LENGTH + ") in commonStringToPassword", Logger.LEVEL.ERROR);
                return "";
            } else if (plainTextPassword.matches("^[\\*]+$")) { // Someone has a password that's really all asterisks? Well, we can't let that be valid...
                Logger.logMessage("Supplied password is invalid (" + plainTextPassword.length() + ") in commonStringToPassword", Logger.LEVEL.ERROR);
                return "";
            }
            return MMHPassword.EncryptTwoWayPassword(plainTextPassword);
        } catch (Exception e) {
            Logger.logMessage("In method commonStringToPassword(): " + e.getMessage(), Logger.LEVEL.ERROR);
            return "";
        }
    }

    // Decrypt a password to a normal string
    public static String commonPasswordToString(String encryptedPassword) {
        try {
            if ((encryptedPassword.length() % 2) != 0) {
                Logger.logMessage("Invalid encrypted string supplied in commonPasswordToString", Logger.LEVEL.ERROR);
                return "";
            }
            return MMHPassword.DecryptTwoWayPassword(encryptedPassword);
        } catch (Exception e) {
            Logger.logMessage(e.getMessage(), Logger.LEVEL.ERROR);
            return "";
        }
    }


    public boolean validatePassword(String plainTextPassword){
        String strength;
        try{
            strength = dm.getSingleField("data.passwordEditor.getPasswordStrength", new Object[]{}).toString();
        }catch (Exception e){
            Logger.logMessage("Unable to retrieve password validation strength, defaulting to strongest method of password validation", Logger.LEVEL.ERROR);
            //default to stronger authentication
            strength="2";
        }


        //only two strengths of password validation for now
        boolean validPassword;
        if(strength.equals("1")){
            validPassword=validatePasswordLoose(plainTextPassword);
        }else{
            validPassword=validatePasswordStrict(plainTextPassword);
        }

        if(!validPassword){
            Logger.logMessage("Password did not meet strength requirement", Logger.LEVEL.ERROR);
        }

        return validPassword;
    }

    //"Loose" passwords contain at least two character types and are at least six characters long
    public boolean validatePasswordLoose(String plainTextPassword) {
        int hasSpecial  = plainTextPassword.matches(".*[!@#$%^&*()_+=-?<>|].*")?1:0;
        int hasLowerCase = plainTextPassword.matches(".*[a-z].*") ? 1 : 0;
        int hasUpperCase = plainTextPassword.matches(".*[A-Z].*") ? 1: 0;
        int hasNumeric = plainTextPassword.matches(".*[0-9].*")?1:0;
        int verificationFields = hasSpecial + hasLowerCase+hasUpperCase+hasNumeric;
        int length = plainTextPassword.length();
        return length >= 6 &&  verificationFields >= 2;
    }

    //"Strict" passwords contain at least three character types and are at least eight characters long
    public boolean validatePasswordStrict(String plainTextPassword) {
        int hasSpecial  = plainTextPassword.matches(".*[!@#$%^&*()_+=-?<>|].*")?1:0;
        int hasLowerCase = plainTextPassword.matches(".*[a-z].*") ? 1 : 0;
        int hasUpperCase = plainTextPassword.matches(".*[A-Z].*") ? 1: 0;
        int hasNumeric = plainTextPassword.matches(".*[0-9].*")?1:0;
        int verificationFields = hasSpecial + hasLowerCase+hasUpperCase+hasNumeric;
        int length = plainTextPassword.length();
        return length >= 8 &&  verificationFields >= 3;
    }

    public static String hashPassword(String plainTextPassword){
        // COMMON utility method to hash Passwords
        // NOTE: WILL use BCRYPT one way hashing if minimum version requirements are met!!!
        // IF PASSWORD MUST BE DECRYPTABLE FOR INTEGRATION PURPOSES THEN DO ***NOT*** Use this method!
        Logger.logMessage("In Method: hashPassword()...", Logger.LEVEL.DEBUG);
        String hashedPassword = "";
        try{
            // Check for bcrypt version support
            if(MMHHashingHelper.checkCanUseBCrypt()){
                Logger.logMessage("Hashing password with BCrypt ...", Logger.LEVEL.DEBUG);
                hashedPassword = BCrypt.HashPassword(plainTextPassword, BCrypt.GenerateSalt(CommonAPI.getWorkFactor()));
            } else {
                Logger.logMessage("Hashing password with MMH Encryption ...", Logger.LEVEL.DEBUG);
                hashedPassword = StringFunctions.encodePassword(plainTextPassword);
            }
        } catch(Exception ex){
            Logger.logMessage("Error in method: encodePassword()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return hashedPassword;
    }

    public static boolean verifyPassword(String plainTextPassword, String hashedPassword){
        // COMMON utility method to verify passwords
        // NOTE: WILL verify password via bcrypt or StringFunctions depending on prefix
        Logger.logMessage("In Method: verifyPassword() ...", Logger.LEVEL.DEBUG);
        boolean isVerified = false;
        String PREFIX_BCRYPT = "$";
        try{
            // Check for bcrypt prefix
            if(hashedPassword.startsWith(PREFIX_BCRYPT)){
                Logger.logMessage("Verifying password with BCrypt ...", Logger.LEVEL.DEBUG);
                isVerified = BCrypt.VerifyPassword(plainTextPassword, hashedPassword);
            } else {
                Logger.logMessage("Verifying password with MMH Encryption ...", Logger.LEVEL.DEBUG);
                isVerified = StringFunctions.decodePassword(hashedPassword).equals(plainTextPassword);
            }
        } catch(Exception ex){
            Logger.logMessage("Error in method: verifyPassword()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        Logger.logMessage("commonMMHFunctions.verifyPassword: "+isVerified, Logger.LEVEL.DEBUG);
        return isVerified;
    }

/* END - Password Helpers */

/* START - Common Error Handling */

    //handles error messages
    public HashMap<String,Object> commonErrorMessage(final String errorMessage){
        return commonErrorMessage(errorMessage,"errorMsg");
    }

    //handles error messages
    public HashMap commonErrorMessage(final String errorMessage,String messageType){
        HashMap<String,Object> temp = new HashMap<>();
        temp.put(messageType, errorMessage);
        Logger.logMessage("Common error! Msg: "+errorMessage, Logger.LEVEL.ERROR);
        return temp;
    }

    public ArrayList<String> commonErrorMessageStringList(final String errorMessage){
        ArrayList<String> temp = new ArrayList<>();
        temp.add(errorMessage);
        Logger.logMessage("Common error! Msg: "+errorMessage, Logger.LEVEL.ERROR);
        return temp;
    }

    public static ArrayList<HashMap> commonErrorMessageList(final String errorMessage){
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        errorCodeList.add(new HashMap(){{put("errorMsg",errorMessage);}});
        Logger.logMessage("Common error! Msg: "+errorMessage, Logger.LEVEL.ERROR);
        return errorCodeList;
    }

/* END - Common Error Handling */

/* START - Common Record Handling */

    //overloaded addRecord which defaults to "QC_" as the tablePrepend string
    public ArrayList<HashMap> addRecord(String sqlName, HashMap record, String tableIdentifier) {
        return addRecord(sqlName,record,tableIdentifier, "QC_");
    }

    //overloaded addRecord which doesn't use the properties object
    public ArrayList<HashMap> addRecord(String sqlName, HashMap record, String tableIdentifier, String tablePrepend) {
        DataHandlerProperties prop = new DataHandlerProperties();
        prop.setInsertSQL(sqlName);
        prop.setPrimaryIdentifier(tableIdentifier);
        prop.setInsertSequence(tablePrepend + tableIdentifier);
        return addRecord(prop, record);
    }

    // Uses properties object to do the add
    //TODO: Add information about ID and Name to error message -jrmitaly
    public ArrayList<HashMap> addRecord(DataHandlerProperties prop, HashMap record) {
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        boolean useGenerics = false;
        boolean useReturn = true;
        if (record != null && !record.isEmpty()) {
            String sql = "";
            SqlFragments parts = new SqlFragments();
            parts.prepareSqlFragments(record, 'I');
            String loggingString = "";
            boolean isOracle = prop.getDbType().compareToIgnoreCase("ORACLE") == 0;
            try {
                if (prop.getInsertSQL().length() > 0) {
                    sql = dm.getSQLProperty(prop.getInsertSQL(), new Object[] {}); // get sql string for creating a batch
                } else {
                    useGenerics = true;
                    if (prop.getPrimaryKeys().size() == 1) {
                        sql = dm.getSQLProperty(DYNAMIC_INSERT_SQL_WITH_RETURN, new Object[] {}); // Use the generic insert that will return a value
                    } else {
                        useReturn = false;
                        sql = dm.getSQLProperty(DYNAMIC_INSERT_SQL_NO_RETURN, new Object[] {}); // Use the generic insert that will return -1
                    }
                }
                //add .NEXTVAL for any auto incrementing primary keys in Oracle
                if (isOracle && prop.getInsertSequence() != null && !prop.getPrimaryIdentifier().equals("")) {
                    parts.prependNextVal(prop.getPrimaryIdentifier(), dm.expandDynamicSqlFragment(1, prop.getDbNextVal(), prop.getInsertSequence()));
                }
            } catch (final Exception ex) {
                Logger.logException(ex); //always log exceptions
                errorCodeList.add(new HashMap() {{put("errorMsg","Could not save record with Exception : " + ex.toString());}});
            }

            try {
                sql = dm.expandDynamicSqlFragment(1, sql, parts.getInsertKeySql()); // Expand the column parameters
                sql = dm.expandDynamicSqlFragment(2, sql, parts.getInsertValSql()); // Expand the value parameters
                if (useGenerics) {
                    sql = dm.expandDynamicSqlFragment(3, sql, DynamicSQL.sanitizeTableColumn(prop.getTableName())); // Add the table name to the generic query
                    if (useReturn) {
                        sql = dm.expandDynamicSqlFragment(4, sql, DynamicSQL.sanitizeTableColumn(prop.getPrimaryIdentifier())); // Add the returned value to the generic query
                    }
                }
                Logger.logMessage("Inserting values", Logger.LEVEL.DEBUG);
                Logger.logMessage(sql, Logger.LEVEL.DEBUG);
                Logger.logMessage(loggingString, Logger.LEVEL.DEBUG);
                errorCodeList.addAll(dm.insertPreparedStatement(sql, parts.getParameters()));
            } catch (final Exception ex) {
                Logger.logException(ex); //always log exceptions
                errorCodeList.add(new HashMap() {{ put("errorMsg","Could not save record with Exception : " + ex.toString()); }});
            }
        }
        return errorCodeList;
    }

    //handles all records to be updated
    //TODO: Add information about ID and Name to error message -jrmitaly	
    public ArrayList<HashMap> updateRecord(String sqlName, HashMap record, String recordIdentifier){
        DataHandlerProperties prop = new DataHandlerProperties();
        prop.setUpdateSQL(sqlName);
        prop.setUpdateKeySingleValue(recordIdentifier);
        return updateRecord(prop, record);
    }

    public ArrayList<HashMap> updateRecord(DataHandlerProperties prop, HashMap record) {
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        boolean useGenerics = false;
        if (record != null && !record.isEmpty()) {
            String sqlString = "";
            boolean hardDeleteFlag = containsFullKey(record.get("FEDELETED")) && record.get("FEDELETED").toString().equals("1");
            try {
                if(hardDeleteFlag){
                    sqlString = dm.getSQLProperty(DYNAMIC_DELETE_SQL, new Object[]{}); // use generic hard delete
                    useGenerics=true;
                }
                else if (prop.getUpdateSQL().length() > 0) {
                    sqlString = dm.getSQLProperty(prop.getUpdateSQL(), new Object[] {}); //get sql string for updating a batch
                }else {
                    useGenerics = true;
                    sqlString = dm.getSQLProperty(DYNAMIC_UPDATE_SQL, new Object[] {}); // Use the generic update
                }
            } catch (final Exception e){
                errorCodeList.add(new HashMap(){{ put("errorMsg","Could not update record with Exception : " + e.toString()); }});
            }
            SqlFragments parts = new SqlFragments();
            if(!hardDeleteFlag){
                parts.prepareSqlFragments(record, 'U');
            }
            try {
                if (prop.getUpdateKeySingleValue().length() > 0 && !useGenerics) {
                    sqlString = dm.expandDynamicSqlFragment(2, sqlString, prop.getUpdateKeySingleValue());
                } else {
                    parts.prepareSqlFragments(prop.getKeyValues(), 'W');
                    parts.prepareSqlFragments(prop.getUpdateKeys(), 'W');
                    sqlString = dm.expandDynamicSqlFragment(2, sqlString, parts.getWhereSql());
                    if (useGenerics) {
                        sqlString = dm.expandDynamicSqlFragment(3, sqlString, DynamicSQL.sanitizeTableColumn(prop.getTableName())); // Add the table name to the generic query
                    }
                }
                sqlString = dm.expandDynamicSqlFragment(1, sqlString, parts.getUpdateSql());
                Logger.logMessage("Running update SQL : " + sqlString, Logger.LEVEL.DEBUG);
                Logger.logMessage("For values " + parts.getParameters().toString(), Logger.LEVEL.DEBUG);
                Logger.logMessage(generateSQLString(sqlString,parts.getParameters()), Logger.LEVEL.DEBUG);
                errorCodeList.addAll(dm.setAndUpdatePreparedStatement(sqlString, parts.getParameters(), prop.getUpdateKeySingleValue()));
            } catch (final Exception e){
                errorCodeList.add(new HashMap() {{ put("errorMsg","Could not record with Exception : " + e.toString()); }});
            }
        }
        return errorCodeList;
    }

    // Utility method to adjust end effective dates when changes are made
    public HashMap effectiveDateAdjustment(DataHandlerProperties prop, HashMap record, String getRecordDetailSql, String parentKeyField, String startDateField, String endDateField) {
        try {
            boolean deleted = false, edited = false;
            if (record.containsKey(DELETED) && record.get(DELETED) != null && record.get(DELETED).toString().length() > 0) deleted = true;
            if (record.containsKey(CHILD_EDITED_FLAG) && record.get(CHILD_EDITED_FLAG) != null && record.get(CHILD_EDITED_FLAG).toString().length() > 0) edited = true;
            if ((deleted || edited) && record.containsKey(parentKeyField) && record.get(parentKeyField) != null && record.containsKey(startDateField) && record.get(startDateField) != null) { // Don't bother processing junk fields
                ArrayList<String> partitionElements = new ArrayList<>();
                Long parentId = Long.parseLong(record.get(parentKeyField).toString());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date recordStartDate = sdf.parse(record.get(startDateField).toString());
                // Only operate on records that are not deleted and are edited (additional records may be passed back)
                if (parentId != null && recordStartDate != null) {
                    ArrayList<HashMap> allListItems = dm.serializeSqlWithColNames(getRecordDetailSql, new Object[] {parentId}, false, true);
                    for (HashMap li : allListItems) {
                        boolean isQualified = true;
                        for (String partitionElement : partitionElements) {
                            if (li.get(partitionElement).toString().compareToIgnoreCase(record.get(partitionElement).toString()) != 0) {
                                isQualified = false;
                            }
                        }
                        if (deleted && isQualified) {
                            // When the deleted record is found we want to alter the end date to equal the start date
                            if (recordStartDate.equals(sdf.parse(li.get(startDateField).toString()))) {
                                HashMap up = new HashMap();
                                up.put(endDateField, sdf.format(sdf.parse(li.get(startDateField).toString())));
                                li.remove(endDateField);
                                SqlFragments sql = new SqlFragments();
                                sql.prepareSqlFragments(up, 'U');
                                sql.prepareSqlFragments(li, 'W');
                                dm.setAndUpdatePreparedStatement(dm.getSQLProperty("data.generic.dynamicUpdate", new String[] {sql.getUpdateSql(), sql.getWhereSql(), prop.getTableName()}, false), sql.getParameters());
                            }
                        } else if (isQualified && li.containsKey(endDateField) && (li.get(endDateField) == null || li.get(endDateField).toString().length() == 0 || recordStartDate.before(sdf.parse(li.get(endDateField).toString())))) {
                            HashMap up = new HashMap();
                            up.put(endDateField, sdf.format(recordStartDate));
                            li.remove(endDateField);
                            SqlFragments sql = new SqlFragments();
                            sql.prepareSqlFragments(up, 'U');
                            sql.prepareSqlFragments(li, 'W');
                            dm.setAndUpdatePreparedStatement(dm.getSQLProperty("data.generic.dynamicUpdate", new String[] {sql.getUpdateSql(), sql.getWhereSql(), prop.getTableName()}, false), sql.getParameters());
                        }
                    }
                }
            }
        } catch (Exception e) {
            record.put(INTERNAL_ERROR, "1");
        }
        return record;
    }

    //TODO : idea to create string that replaces all of the question marks with their actual parameters
    //so techs can see close to the exact query being run
    //TODO: NEVER USE THIS AS THE SQL THAT GETS RUN!!!
    public String generateSQLString(String sqlString, ArrayList<String> params){
        try{
            int index = sqlString.indexOf("?");
            int i = 0;
            while (index >= 0) {
                String param = "''";
                if(params.get(i) != null){
                    param = "'"+params.get(i)+"'";
                }
                sqlString=sqlString.substring(0,index)+param+sqlString.substring(index+1,sqlString.length());
                index = sqlString.indexOf("?", index + 1);
                i++;
            }
            return sqlString;
        }catch (Exception e){
            return "";
        }
    }

    // Utility method to check if a record exists, so that a differentiation between updates and inserts can be discerned
    public boolean checkRecordExists(DataHandlerProperties prop, HashMap record) {
        // Ensure that the record has data
        if (record != null && !record.isEmpty()) {
            // Start with standard SQL statement
            String sqlString;
            try {
                sqlString = dm.getSQLProperty("data.common.checkRecordExistance", new Object[] {});
                if (prop.getTableName().length() > 0) {
                    // Add in the table name, only continue if it exists
                    sqlString = dm.expandDynamicSqlFragment(1, sqlString, DynamicSQL.sanitizeTableColumn(prop.getTableName()));
                    // Get all the record's keys
                    HashMap<String,Object> keyMap = new HashMap<>();
                    Iterator iter = record.entrySet().iterator();
                    while (iter.hasNext()) {
                        Map.Entry field = (Map.Entry)iter.next();
                        for (String key : prop.getPrimaryKeys()) {
                            if (key.compareToIgnoreCase((String)field.getKey()) == 0) {
                                keyMap.put((String)field.getKey(), field.getValue());
                            }
                        }
                    }
                    // If a company ID needs to be taken into account, it's done here
                    if (prop.getValidatedColumns().containsKey(COMPANYID)) keyMap.put(COMPANYID, prop.getCacheCompanyId());
                    // Put the record's keys into the SQL fragments
                    SqlFragments sql = new SqlFragments();
                    sql.prepareSqlFragments(keyMap, 'W');
                    // Finish building the SQL
                    sqlString = dm.expandDynamicSqlFragment(2, sqlString, sql.getWhereSql());
                    // Run the SQL
                    Object o = dm.parameterizedGetSingleField(sqlString, sql.getParameters());
                    if (tryParseLong(o.toString())) {
                        if (Long.parseLong(o.toString()) > 0) {
                            // If the returned result is a valid integer and greater than zero, then the record exists
                            return true;
                        }
                    }
                }
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }

    // Utility method to get a label (name field) for a specific record
    public ArrayList<HashMap> getLabelForRecord(HttpServletRequest request, DataHandlerProperties prop) {
        try {
            if (!checkSession(request).equals("")) { //check that user is logged in
                if (prop.getGetSQL().length() > 0) {
                    // If get SQL is defined, then it will use the get arguments as the parameters
                    String sql = dm.getSQLProperty(prop.getGetSQL(), new Object[]{}); // use given name lookup (select)
                    for (int i = 0; i < prop.getGetArguments().size(); i++) {
                        sql = dm.expandDynamicSqlFragment(i + 1, sql, prop.getGetArguments().get(i));
                    }
                    return dm.serializeSqlWithColNames("", sql, new ArrayList<>(), false);
                } else {
                    // When the get SQL is not defined, then it will use generic and specify the where clause using the key values
                    String sql = dm.getSQLProperty(DYNAMIC_LOOKUP_SQL, new Object[]{}); // use generic name lookup (select)
                    sql = dm.expandDynamicSqlFragment(1, sql, prop.getNameField());
                    SqlFragments parts = new SqlFragments();
                    parts.prepareSqlFragments(prop.getKeyValues(), 'W');
                    sql = dm.expandDynamicSqlFragment(2, sql, parts.getWhereSql());
                    sql = dm.expandDynamicSqlFragment(3, sql, prop.getTableName());
                    return dm.serializeSqlWithColNames("", sql, parts.getParameters(), false);
                }
            }
            return commonErrorMessageList("Invalid Access.");
        } catch (Exception e) {
            return commonErrorMessageList("Invalid Access.");
        }
    }

    // Utility method to add a deleted flag to all child records
    public HashMap addDeleteFlagToChildren(HashMap record) {
        // Loop through all the elements of the record
        for (Object o : record.values()) {
            // Check for ArrayList objects, those are the children
            if (o.getClass().equals(ArrayList.class)) {
                // Loop through the ArrayList
                for (HashMap m : (ArrayList<HashMap>)o) {
                    // Add the deleted flag
                    m.put(DELETED, "1");
                }
            }
        }
        // Return the entire record
        return record;
    }

    // Utility method to generate an incrementing key
    public long generateNextValue(DataHandlerProperties prop, HashMap record, String incrementColumn) {
        return generateNextValue(prop, record, incrementColumn, true);
    }
    public long generateNextValue(DataHandlerProperties prop, HashMap record, String incrementColumn, boolean perParent) {
        return generateNextValue(prop, record, incrementColumn, perParent, 1);
    }
    public long generateNextValue(DataHandlerProperties prop, HashMap record, String incrementColumn, boolean perParent, long startingIndex) {
        // Ensure that the record has data
        if (record != null && !record.isEmpty()) {
            // Start with standard SQL statement
            String sqlString;
            try {
                sqlString = dm.getSQLProperty("data.common.findKeyMaxValue", new Object[] {});
                if (prop.getTableName().length() > 0) {
                    // Add in the table name, only continue if it exists
                    sqlString = dm.expandDynamicSqlFragment(1, sqlString, DynamicSQL.sanitizeTableColumn(prop.getTableName()));
                    // Add in the column to increment
                    sqlString = dm.expandDynamicSqlFragment(3, sqlString, DynamicSQL.sanitizeTableColumn(incrementColumn));
                    // Get all the record's keys
                    HashMap<String,Object> keyMap = new HashMap<>();
                    Iterator iter = record.entrySet().iterator();
                    while (iter.hasNext()) {
                        Map.Entry field = (Map.Entry)iter.next();
                        // Loop through all of the primary keys
                        for (String key : prop.getPrimaryKeys()) {
                            // Only look at keys that match the record and are not the column we are looking to increment
                            if (key.compareToIgnoreCase((String)field.getKey()) == 0 && key.compareToIgnoreCase(incrementColumn) != 0) {
                                // Do nothing unless we are restricting by parent (include the parent key) or it is not the parent key
                                if (perParent || key.compareToIgnoreCase(prop.getParentIdentifier()) != 0) {
                                    keyMap.put((String)field.getKey(), field.getValue()); // Add the key to the record
                                }
                            }
                        }
                    }
                    // If a company ID needs to be taken into account, it's done here
                    if (prop.getValidatedColumns().containsKey(COMPANYID) && Long.toString(prop.getCacheCompanyId()).length() > 0) {
                        keyMap.put(COMPANYID, prop.getCacheCompanyId());
                    }
                    // Put the record's keys into the SQL fragments
                    SqlFragments sql = new SqlFragments();
                    sql.prepareSqlFragments(keyMap, 'W');
                    // Finish building the SQL
                    sqlString = dm.expandDynamicSqlFragment(2, sqlString, sql.getWhereSql());
                    // Run the SQL
                    Object o = dm.parameterizedGetSingleField(sqlString, sql.getParameters());
                    if (tryParseLong(o.toString())) {
                        long i = Long.parseLong(o.toString());
                        if (i > 0) {
                            // If the returned result is a valid integer and greater than zero, then increment the result
                            return ++i;
                        } else {
                            // If the result is less than zero it's because there are no records with the same criteria
                            return startingIndex;
                        }
                    }
                }
            } catch (Exception e) {
                return -1;
            }
        }
        return -1;
    }

    // Utility method to ensure that records pass data validation
    public HashMap validateRecord(DataValidation dv, HashMap columns, HashMap record) {
        // Validate per specific database
        if (MMHProperties.getAppSetting("database.type").compareToIgnoreCase("ORACLE") == 0) {
            return dv.validateDataOracle(record, columns);
        } else {
            return dv.validateDataSQL(record, columns);
        }
    }

    // Utility method to check for data columns
    public boolean recordHasDataColumns(HashMap record) {
        int counter = 0;
        for (Object field : record.keySet()) {
            String fieldName = field.toString();
            if (fieldName.compareToIgnoreCase(RECORDACTION) != 0 &&
                    fieldName.compareToIgnoreCase(RECORDACTIONTIMESTAMP) != 0 &&
                    fieldName.compareToIgnoreCase(RECORDACTIONUSER) != 0) {
                counter++;
            }
        }
        return counter > 0;
    }

/* END - Common Record Handling */

/* START - User Selected List Handling */

    public ArrayList<HashMap> getUserList(String userTypeID, HttpServletRequest request) {
        ArrayList<HashMap> userList = new ArrayList<>();
        try {
            String userID = checkSession(request);
            if (!userID.isEmpty()) {
                //if the user has a negative session id, then reset list to just that user
                if (Integer.parseInt(userID) < 0) {
                    resetUserListToLoggedInUser(request);
                }
                //retrieve all records for this userID
                userList = dm.serializeSqlWithColNames("data.selectedlist.getUserList", new Object[] {userID, userTypeID});
            }
        } catch(Exception ex) {
            Logger.logMessage("Error in method commonMMHFunctions.getUserList:" + ex.toString(), Logger.LEVEL.ERROR);
        }
        return userList;
    }

    public String getUserListAsString(HttpServletRequest request) {
        ArrayList<HashMap> userList;
        String userListAsString = "";
        try {
            String userID = checkSession(request);
            if (!userID.isEmpty()) {
                //retrieve all records for this userID - the logged in user
                userList = dm.serializeSql("data.selectedlist.getUserList", new Object[] {userID,1});
                userListAsString = userList.toString().replace("[","").replace("]","");
            }
        } catch(Exception ex) {
            Logger.logMessage("Error in method commonMMHFunctions.getUserListAsString:" + ex.toString(), Logger.LEVEL.ERROR);
        }
        return userListAsString;
    }

    //TODO: (low) start actually using the hyperfind query
    public boolean touchUserList(String userId, String queryId) {
        try {
            if (queryId == null || queryId.trim().length() < 1 || !tryParseLong(queryId)) {
                queryId = "NULL";
            }
            dm.serializeUpdate("data.selectedlist.touchUserList", new Object[] {userId, queryId});
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public ArrayList<HashMap> resetUserList(String recordID, String userTypeID, HttpServletRequest request) {
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        try {
            HashMap recordHashMap = new HashMap();
            String userID = checkSession(request);
            if (!userID.isEmpty() && touchUserList(userID, null)) {
                try {
                    //delete all records with this userID
                    dm.serializeUpdate("data.selectedlist.resetUserList", new Object[] {userID, userTypeID});
                } catch(Exception deleteEx) {
                    Logger.logMessage("Error in method commonMMHFunctions.resetUserList trying to reset: " + deleteEx.toString(), Logger.LEVEL.ERROR);
                    errorCodeList = commonErrorMessageList("Error in method commonMMHFunctions.resetUserList trying to reset: " + deleteEx.toString());
                } finally {
                    recordHashMap.put("USERID", userID);
                    recordHashMap.put("PERSONID", recordID);
                    recordHashMap.put("USERTYPEID", userTypeID);
                    errorCodeList = addRecord("data.selectedlist.addToUserList", recordHashMap, "SelectedListDetail", "MMH_");

                }
            }
        } catch(Exception ex) {
            Logger.logMessage("Error in method commonMMHFunctions.resetUserList:" + ex.toString(), Logger.LEVEL.ERROR);
            errorCodeList = commonErrorMessageList("Error in method commonMMHFunctions.resetUserList trying to reset: " + ex.toString());
        }
        return errorCodeList;
    }

    /**
     * VULNERABLE TO SQLI when using Oracle
     * @param recordID
     * @param userTypeID
     * @param request
     * @return
     */
    public ArrayList<HashMap> addToUserList(String recordID, String userTypeID, HttpServletRequest request) {
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        try {
            HashMap recordHashMap = new HashMap();
            String userID = checkSession(request);
            if (!userID.isEmpty() && touchUserList(userID, null)) {
                recordHashMap.put("USERID", userID);
                recordHashMap.put("PERSONID", recordID);
                recordHashMap.put("USERTYPEID", userTypeID);
                boolean isOracle = MMHProperties.getAppSetting("database.type").compareToIgnoreCase("ORACLE") == 0;
                if (isOracle) {
                    //TODO: (high) possible SQL injection -jrmitaly
                    dm.serializeUpdate("data.selectedlist.addToUserList", new Object[] {userID, recordID});
                } else {
                    errorCodeList = addRecord("data.selectedlist.addToUserList", recordHashMap, "SelectedListDetail", "MMH_");
                }
            }
        } catch(Exception ex) {
            Logger.logMessage("Error in method commonMMHFunctions.addToUserList: " + ex.toString(), Logger.LEVEL.ERROR);
            errorCodeList = commonErrorMessageList("Error in method commonMMHFunctions.addToUserList: " +ex.toString());
        }
        return errorCodeList;
    }

    public ArrayList<HashMap> addArrayToUserList(String[] recordIDs, String userTypeID, HttpServletRequest request) {
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        try {
            String userID = checkSession(request);
            if (!userID.isEmpty()) {
                //iterate over each record and add to user list
                for (String recordID: recordIDs) {
                    addToUserList(recordID, userTypeID, request);
                }
            }
        } catch(Exception ex) {
            Logger.logMessage("Error in method commonMMHFunctions.addArrayToUserList: " + ex.toString(), Logger.LEVEL.ERROR);
            errorCodeList = commonErrorMessageList("Error in method commonMMHFunctions.addArrayToUserList: " + ex.toString());
        }
        return errorCodeList;
    }

    public ArrayList<HashMap> deleteFromUserList(String recordID, String userTypeID, HttpServletRequest request) {
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        try {
            String userID = checkSession(request);
            if (!userID.isEmpty() && touchUserList(userID, null)) {
                //delete all records with this userID and recordID
                dm.serializeUpdate("data.selectedlist.deleteFromUserList", new Object[] {userID, recordID, userTypeID});
            }
        } catch(Exception ex) {
            Logger.logMessage("Error in method commonMMHFunctions.deleteFromUserList:" + ex.toString(), Logger.LEVEL.ERROR);
            errorCodeList = commonErrorMessageList("Error in method commonMMHFunctions.deleteFromUserList: " + ex.toString());
        }
        return errorCodeList;
    }

    public ArrayList<HashMap> allRecordsToUserList(String filterString, String QueryID, String userTypeID, HttpServletRequest request) {
        return allRecordsToUserList(filterString, QueryID, request, null, userTypeID);
    }

    /**
     * Parameters to this method are coming directly from the client, and thus may contain SQLI
     * @param filterString_DANGEROUS
     * @param QueryID_DANGEROUS
     * @param request
     * @param filterOptions_DANGEROUS
     * @param userTypeID_DANGEROUS
     * @return
     */
    public ArrayList<HashMap> allRecordsToUserList(String filterString_DANGEROUS, String QueryID_DANGEROUS, HttpServletRequest request, HashMap filterOptions_DANGEROUS, String userTypeID_DANGEROUS) {
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        try {
            String userID = checkSession(request);
            if (!userID.isEmpty() && touchUserList(userID, null)) {
                try {
                    //delete all records with this userID
                    new DynamicSQL("data.selectedlist.resetUserList")
                            .addValue(1, userID).addValue(2, userTypeID_DANGEROUS)
                            .runUpdate(dm);
                } catch(Exception resetEx) {
                    Logger.logMessage("Error in method " + className + ".allRecordsToUserList trying to resetUserList: " + resetEx.toString(), Logger.LEVEL.ERROR);
                    errorCodeList = commonErrorMessageList("Error in method " + className + ".allRecordsToUserList trying to resetUserList: " + resetEx.toString());
                    return errorCodeList;
                }
                ServerGridProperties gridProp = new ServerGridProperties();
                for (Map.Entry<String, Object> filterOption : ((HashMap<String, Object>)filterOptions_DANGEROUS).entrySet()) {
                    if (filterOption.getKey().compareToIgnoreCase("FILTERHYPERFIND") == 0) {
                        gridProp.setFilterHyperfind(Boolean.parseBoolean(filterOption.getValue().toString()));
                    } else if (filterOption.getKey().compareToIgnoreCase("FILTERACCOUNTGROUPS") == 0) {
                        gridProp.setFilterAccountGroups(Boolean.parseBoolean(filterOption.getValue().toString()));
                    } else if (filterOption.getKey().compareToIgnoreCase("FILTEROWNERSHIP") == 0) {
                        gridProp.setFilterOwnerShip(Boolean.parseBoolean(filterOption.getValue().toString()));
                    }
                }
                //this does almost the same thing as ServerGridProperties(postParams)
                gridProp.addFilterHM(filterString_DANGEROUS); //filters by jQgrid filters
                gridProp.setQueryId(QueryID_DANGEROUS); //filters by hyperfind (QC)
                gridProp.setUserId(userID);



                //TODO: (HIGH) Needs abstraction - for QC Account Manager - Account Summary - but would apply to ANY custom filter passed for select all -jrmitaly
                if(userTypeID_DANGEROUS.equals("3")) {
                    gridProp.addCustomFilter("QUICKFIND", " ( P.NAME LIKE '%' + searchString + '%' OR P.EMAIL LIKE '%' + searchString + '%' )");
                    gridProp.addCustomFilter("ACTIVEONLY", " ( P.ACTIVE = searchString )");
                } else {
                    gridProp.addCustomFilter("QUICKFIND", " ( E.NAME LIKE '%' + searchString + '%' OR E.EMPLOYEENUMBER LIKE '%' + searchString + '%' OR E.BADGE LIKE '%' + searchString + '%' )");
                    gridProp.addCustomFilter("ACTIVEONLY", " ( E.AIP = searchString )");
                }

                ArrayList<Object> sqlArgsList = new ArrayList<>();
//                sqlArgsList = addFilterArguments(request, gridProp);
                sqlArgsList.add("");//placeholder
                sqlArgsList.add("");//placeholder
                sqlArgsList.add((gridProp.getQueryId() != null ? gridProp.getQueryId() : "-1")); // Add in the HyperFind query
                sqlArgsList.add("");//the true place that the 0th param should be
                sqlArgsList.add(userTypeID_DANGEROUS);//add the user typeID

                buildServerGridFilterArguments(request,gridProp, null, sqlArgsList);

                //for handling Quickcharge hyperfinds
                if (!gridProp.getFilterHyperfind() && gridProp.getQueryId() != null) { //update where clause for hyperfind filtering for accounts
                    sqlArgsList.set(1, sqlArgsList.get(1).toString() + qcHyperfindWhereText(gridProp.getQueryId().toString()));
                }
                //move 0 to 3 and set user ID
                sqlArgsList.set(3, sqlArgsList.get(0));
                sqlArgsList.set(0, gridProp.getUserId()); //reserved for userID

                String paramSQL = sqlArgsList.get(3).toString();
                String clauseSQL = sqlArgsList.get(1).toString();
                //clear where the SQL was held so we dont try to paramaterize that
                sqlArgsList.set(3, "");//TODO: (Low) this could be refactored to use param numbers like all the others paginated grids if we change the query
                sqlArgsList.set(1, "");

                String rawSQL;
                if(userTypeID_DANGEROUS.equals("3")) {
                    rawSQL= MMHProperties.getSqlString("data.selectedlist.allRecordsToUserListPerson");
                } else {
                    rawSQL= MMHProperties.getSqlString("data.selectedlist.allRecordsToUserListEmp");
                }

                //append our custom clause sql because we cant parameterize this
                rawSQL = rawSQL.replaceAll("\\{4\\}", paramSQL);
                rawSQL = rawSQL.replaceAll("\\{2\\}", clauseSQL);

                Object[] sqlArgsArr = sqlArgsList.toArray();
                // Add all entries into the selected list
                //TODO: there must be a better way than using dynamicSerializeUpdate directly, it should only be used with DynamicSQL
                //TODO:     but DynamicSQL shouldnt allow for custom filters to be directly inserted
                dm.dynamicSerializeUpdate(rawSQL, sqlArgsArr, null, "", 0);
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            errorCodeList = commonErrorMessageList("Error in method " + className + ".allRecordsToUserList: " + ex.toString());
        }
        return errorCodeList;
    }

    public ArrayList<HashMap> resetUserListToLoggedInUser(HttpServletRequest request) {
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        try {
            HashMap recordHashMap = new HashMap();
            String userID = checkSession(request);
            if (!userID.isEmpty() && touchUserList(userID, null)) {
                try {
                    //delete all records with this userID
                    //TODO: there could be a bug here, if we dont want a user type of 1. Added the "1" just to stop the query from failing to run.
                    dm.serializeUpdate("data.selectedlist.resetUserList", new Object[] {userID, "1"});
                } catch(Exception deleteEx) {
                    Logger.logMessage("Error in method commonMMHFunctions.resetUserListToLoggedInUser trying to reset: " + deleteEx.toString(), Logger.LEVEL.ERROR);
                    errorCodeList = commonErrorMessageList("Error in method commonMMHFunctions.resetUserListToLoggedInUser trying to reset: " + deleteEx.toString());
                } finally {
                    recordHashMap.put("USERID", userID);
                    Integer personID = (Integer.parseInt(userID) * -1);
                    recordHashMap.put("PERSONID", personID);
                    errorCodeList = addRecord("data.selectedlist.addToUserList", recordHashMap, "SelectedListDetail", "MMH_");

                }
            }
        } catch(Exception ex) {
            Logger.logMessage("Error in method commonMMHFunctions.resetUserList:" + ex.toString(), Logger.LEVEL.ERROR);
            errorCodeList = commonErrorMessageList("Error in method commonMMHFunctions.resetUserListToLoggedInUser trying to reset: " + ex.toString());
        }
        return errorCodeList;
    }

/* END - User Selected List Handling */

/* START - QC User Profile Methods */

    public ArrayList getUserProfile(HttpServletRequest request) {
        ArrayList userProfile = new ArrayList();
        try
        {
            Logger.logMessage("Running method commonMMHFunctions.getUserProfile...", Logger.LEVEL.DEBUG);
            String userID = checkSession(request);
            if(!userID.isEmpty()){
                userProfile = dm.serializeSqlWithColNames("data.common.getUserProfile",
                        new Object[]{
                                userID
                        },
                        false,
                        true
                );
            }
        }
        catch(Exception ex)
        {
            Logger.logMessage("Error in method commonMMHFunctions.getUserProfile:" + ex.toString(), Logger.LEVEL.ERROR);
        }
        return userProfile;
    }

    public String getUserProfileID(HttpServletRequest request) {
        ArrayList userProfile = new ArrayList();
        try
        {
            Logger.logMessage("Running method commonMMHFunctions.getUserProfile...", Logger.LEVEL.DEBUG);
            String userID = checkSession(request);
            if(!userID.isEmpty()){
                userProfile = dm.serializeSqlWithColNames("data.common.getUserProfile",
                        new Object[]{
                                userID
                        },
                        false,
                        true
                );
            }
        }
        catch(Exception ex)
        {
            Logger.logMessage("Error in method commonMMHFunctions.getUserProfile:" + ex.toString(), Logger.LEVEL.ERROR);
        }

        HashMap hm = (HashMap)userProfile.get(0);
        String userProfileID = hm.get("USERPROFILEID").toString();
        return userProfileID;
    }

    public String getUserProfileColumn(String profileColumnName, HttpServletRequest request) {
        try
        {
            Logger.logMessage("Running method commonMMHFunctions.getUserProfileColumn...", Logger.LEVEL.DEBUG);
            String userID = checkSession(request);
            if(!userID.isEmpty()){
                return new DynamicSQL("data.common.getUserProfileColumn")
                        .addTableORColumnName(1, profileColumnName).addValue(2, userID)
                        .getSingleField(dm).toString();
            }
        }
        catch(Exception ex)
        {
            Logger.logMessage("Error checking user profile "+ex.toString(), Logger.LEVEL.ERROR);
            return "";
        }
        return "";
    }

    public boolean checkUserProfileBitColumn_qc(String profileColumnName, HttpServletRequest request) {
        try
        {
            Logger.logMessage("Running method commonMMHFunctions.checkUserProfileBitColumn_qc...", Logger.LEVEL.DEBUG);
            String userID = checkSession(request);
            if(!userID.isEmpty()){
                Object columnAccess = new DynamicSQL("data.common.getUserProfileColumn")
                        .addTableORColumnName(1, profileColumnName).addValue(2, userID)
                        .getSingleField(dm);
                if (columnAccess != null && columnAccess.toString().equals("true")) {
                    return true;
                }
            }
        }
        catch(Exception ex)
        {
            Logger.logMessage("Error checking user profile in commonMMHFunctions.checkUserProfileBitColumn_qc..", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return false;
        }
        return false;
    }

    public boolean checkUserProfileBitColumnByID_qc(String profileColumnName, String userID) {
        try
        {
            Logger.logMessage("Running method commonMMHFunctions.checkUserProfileBitColumnByID_qc...", Logger.LEVEL.DEBUG);
            if(!userID.isEmpty()){
                Object columnAccess = new DynamicSQL("data.common.getUserProfileColumn")
                        .addTableORColumnName(1, profileColumnName).addValue(2, userID)
                        .getSingleField(dm);
                if (columnAccess != null && columnAccess.toString().equals("true")) {
                    return true;
                }
            }
        }
        catch(Exception ex)
        {
            Logger.logMessage("Error checking user profile in commonMMHFunctions.checkUserProfileBitColumnByID_qc..", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return false;
        }
        return false;
    }

/* END - QC User Profile Methods */

/* START - MMH User Methods */

    public HashMap getUserDetails(HttpServletRequest request) {
        HashMap hm = new HashMap();
        try {
            Long userId = getUserId(request);
            if (userId != 0) {
                ArrayList<HashMap> ret = dm.serializeSqlWithColNames("data.session.getUserDetails", new Object[] {userId}, false, true);
                if (ret != null && ret.size() == 1) {
                    hm = ret.get(0);
                    if (!hm.containsKey("FULLNAME") && hm.get("FIRSTNAME") != null && hm.get("LASTNAME") != null) {
                        hm.put("FULLNAME", makeFullName(hm.get("FIRSTNAME").toString(), hm.get("LASTNAME").toString()));
                    }
                }
            }
        } catch (Exception e) {
            Logger.logMessage("Error in commonMMHFunctions.getUserDetails: " + e.getMessage(), Logger.LEVEL.ERROR);
        }
        return hm;
    }

    public static String makeFullName(String firstName, String lastName) {
        String fullName = (lastName != null ? lastName.trim() : "");
        if (firstName != null && firstName.trim().length() > 0) {
            firstName = firstName.replaceAll("[^A-Za-z]", "").trim();
            if (firstName.length() > 1 && fullName.length() > 0) {
                fullName = firstName + " " + fullName;
            } else if (firstName.length() == 1 && fullName.length() > 0) {
                fullName = firstName + ". " + fullName;
            }
        }
        return fullName;
    }

/* END - MMH User Methods */

/* START - Other common methods */

    public HashMap getColors (HttpServletRequest request){
        ArrayList<HashMap> colors = new ArrayList<>();
        try{
            colors = dm.serializeSqlWithColNames("data.common.getColors");
        }catch (Exception e){
            Logger.logMessage("Unable to retrieve supported colors", Logger.LEVEL.ERROR);
        }
        HashMap colorMap = new HashMap();
        for(HashMap color : colors){
            if(containsFullKey(color.get("COLOR")) && containsFullKey(color.get("PACOLORID"))){
                colorMap.put(color.get("PACOLORID").toString(),color.get("COLOR").toString());
            }
        }
        return colorMap;
    }

    //maps objects to other objects in database
    public boolean mapObject(ArrayList mapObjects, String removeSQL , String createSQL, String recordID, String objectName ){
        try
        {
            Logger.logMessage("Running method commonMMHFunctions.mapObject...", Logger.LEVEL.DEBUG);
            if(mapObjects!=null && !mapObjects.isEmpty()){
                if(dm.serializeUpdate(removeSQL, new Object[]{recordID})==-1){
                    return false;
                }
                ArrayList<String> mappedObjects = new ArrayList<>();
                for(Object mapObject : mapObjects){
                    if(!mapObject.toString().equals("0") && !mapObject.toString().equals("") && !mappedObjects.contains(mapObject.toString())){
                        Logger.logMessage("mapping "+ objectName + " to identifier " + recordID, Logger.LEVEL.DEBUG);
                        if(dm.serializeUpdate(createSQL,new Object[]{recordID,mapObject.toString()})==-1){
                            return false;
                        }
                        mappedObjects.add(mapObject.toString());
                    }
                }
            }
            return true;
        }
        catch(Exception ex)
        {
            Logger.logMessage("Error in method commonMMHFunctions.mapObject:" + ex.toString(), Logger.LEVEL.ERROR);
            return false;
        }
    }

    //TODO: Add note here -jrmitaly
    public boolean activePage(int targetID, String identifier, HttpServletRequest request){
        try
        {
            Logger.logMessage("Running method commonMMHFunctions.activePage...", Logger.LEVEL.DEBUG);
            if (!checkSession(request).equals("")) { //check that user is logged in
                HttpSession session = request.getSession();
                session.setAttribute("activeParentMenu",targetID);
                session.setAttribute("activeChildMenu",identifier);
                return true;
            } else {
                Logger.logMessage("Invalid access in commonMMHFunctions.activePage.", Logger.LEVEL.ERROR);
                return false;
            }
        }
        catch(Exception ex)
        {
            Logger.logMessage("Error in method commonMMHFunctions.activePage:" + ex.toString(), Logger.LEVEL.ERROR);
            return false;
        }
    }

    //Creates date objects from strings via SimpleDateFormat
    public Date formatDate(String simpleDateFormat, String dateToFormat) {
        try
        {
            Date formattedDate;
            SimpleDateFormat formatter=new SimpleDateFormat(simpleDateFormat); //"MM/dd/yyyy"
            formattedDate = formatter.parse(dateToFormat); //06/01/2013
            return formattedDate;
        }
        catch(Exception ex)
        {
            Logger.logMessage("Error in method commonMMHFunctions.formatDate:" + ex.toString(), Logger.LEVEL.ERROR);
            return new Date();
        }
    }

    //creates an array list from a comma delimited list then placed back inside of the passed hashmap
    public void createArrayListInHashMap(HashMap record,String identifier,String mapTo){
        try
        {
            if(record.get(identifier) != null){
                String ids = record.get(identifier).toString();
                if(!ids.isEmpty()){
                    String[] idList = ids.split("\\s*,\\s*");
                    ArrayList idArrayList =  new ArrayList(Arrays.asList(idList));
                    record.put(mapTo, idArrayList);
                }else {
                    record.put(mapTo, new ArrayList());
                }
                if(!identifier.equals(mapTo)){
                    record.remove(identifier);
                }
            }else{
                record.put(identifier,new ArrayList<HashMap>());
            }
        }
        catch(Exception ex)
        {
            Logger.logMessage("Error in method commonMMHFunctions.createArrayListInHashMap:" + ex.toString(), Logger.LEVEL.ERROR);
        }
    }

    public static ArrayList toArrayList(Object record) {
        ArrayList returnRecord;
        try {
            returnRecord = (ArrayList)record;
        } catch (Exception e) {
            return new ArrayList();
        }
        return returnRecord;
    }

    public static boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean tryParseLong(String value) {
        try {
            Long.parseLong(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean tryParseBoolean(String value) {
        try {
            Boolean.parseBoolean(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean containsFullKey(Object prop) {
        if (prop != null && !prop.toString().isEmpty() && !prop.toString().replace(" ","").isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static void logStackTrace(final Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
        //   Logger.logMessag e("Error in "+className+"."+methodName+" - Exception thrown: "+sw.toString());
    }

    public static void logStackTrace(final Exception e, String className, String methodName) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
        Logger.logMessage("Error in " + className + "." + methodName + " - Exception thrown: " + sw.toString(), Logger.LEVEL.ERROR);
    }

    public static String uppercaseFirstLetters(String sentence) {
        final StringBuilder result = new StringBuilder(sentence.length());
        String[] words = sentence.split("\\s");
        for (int i = 0, l = words.length; i < l; ++i) {
            if (i > 0) result.append(" ");
            result.append(Character.toUpperCase(words[i].charAt(0))).append(words[i].substring(1));
        }
        return result.toString();
    }

    public String formatTime(String time, String inFormat, String outFormat) {
        if (time.equals("NULL")) {
            return time;
        } else {
            SimpleDateFormat inFormatter = new SimpleDateFormat(inFormat);
            SimpleDateFormat outFormatter = new SimpleDateFormat(outFormat);
            String militaryTimeString = "";
            try {
                //convert AM/PM time string to date object then format the date object to military time string
                militaryTimeString = outFormatter.format(inFormatter.parse(time));
            } catch (Exception ex) {
                logStackTrace(ex, className, "formatTime");
                return militaryTimeString;
            }
            return militaryTimeString;
        }
    }

    public String ternaryGetString(HashMap cur, String propertyName) {
        return containsFullKey(cur.get(propertyName)) ? cur.get(propertyName).toString() : "";
    }

    public String convertToCurrencyString(Object hashmapField) {
        double money = Double.parseDouble(hashmapField.toString());
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        return formatter.format(money).replace("$", "").replace(",", "");
    }

    public String convertToVarDecimal(Object hashmapField, String decimalPlaces) {
        try {
            double percentage = Double.parseDouble(hashmapField.toString());
            return String.format("%." + decimalPlaces + "f", percentage);
        } catch (Exception e) {
            return "";
        }

    }

    public static String ArrayListToCSV(ArrayList cur) {
        if (cur != null && cur.size() > 0) {
            String csv = "";
            for (int i =0; i < cur.size(); i++) {
                if (cur.get(i) != null) {
                    String index = cur.get(i).toString();
                    if (!index.isEmpty()) {
                        csv += cur.get(i).toString() + ",";
                    }
                }
            }
            if (csv.isEmpty()) {
                return csv;
            } else {
                return csv.substring(0, csv.length() - 1);
            }
        } else {
            return "";
        }
    }

    public static String ArrayListToCSVRemoveDupes(ArrayList cur) {
        ArrayList<String> dupeVales = new ArrayList<>();
        if (cur != null && cur.size() > 0) {
            String csv = "";
            for (int i = 0; i < cur.size(); i++) {
                if (cur.get(i) != null) {
                    String index = cur.get(i).toString();
                    if (!index.isEmpty()) {
                        if (!dupeVales.contains(cur.get(i))) {
                            csv += cur.get(i).toString() + ",";
                            dupeVales.add(cur.get(i).toString());
                        }
                    }
                }
            }
            if (csv.isEmpty()) {
                return csv;
            } else {
                return csv.substring(0, csv.length() - 1);
            }
        } else {
            return "";
        }
    }

    public ArrayList<HashMap> loadSelectBox(String params) {
        ArrayList<HashMap> selectOptions = new ArrayList<HashMap>();
        String selectBoxID = "N/A";
        try {
            String[] splitArray = params.split("-");
            selectBoxID = splitArray[0];
            String recordKey = splitArray[1];
            selectOptions = new DynamicSQL("data.common.loadSelectBox")
                    .addTableORColumnName(1, recordKey)
                    .addValue(2, selectBoxID)
                    .serialize(dm);
        } catch (Exception e) {
            logStackTrace(e);
            selectOptions.add(this.commonErrorMessage("Could not retrieve Select Options for Select Element ID : " + selectBoxID));
            return selectOptions;
        }
        return selectOptions;
    }

    public ArrayList<HashMap> findSelectBoxTextValue(String primaryKeyName, String primaryKeyValue, String textKeyName) {
        ArrayList<HashMap> tableNames;
        try {
            tableNames = dm.serializeSqlWithColNames("data.common.findTableName", new Object[]{primaryKeyName});
        } catch (Exception e) {
            Logger.logMessage("Unable to find table name derived from primary key name", Logger.LEVEL.ERROR);
            logStackTrace(e);
            return commonErrorMessageList("Unable to find select box value");
        }

        if (tableNames.size() > 1) {
            //We have a problem because there were two tables with the same primary key
        } else {
            String tableName = "";
            for (HashMap temp : tableNames) {

        }
            String textValue = dm.getSingleField("data.common.findSelectOptionText", new Object[] {textKeyName,}).toString();
    }
        return new ArrayList<>();
    }

    public String getPasswordValidationStrength() {
        String strength;
        try {
            strength = dm.getSingleField("data.passwordEditor.getPasswordStrength", new Object[] {}).toString();
        } catch (Exception e) {
            strength = "2";
        }
        return strength;
    }

    public String getCompanyPasswordStrength(HttpServletRequest request) {
        String strength = "";
        try {
            long userId = getUserId(request);
            if (userId != 0) {
                strength = dm.getSingleField("data.company.getSingleParameter", new Object[] {"1", Long.toString(userId)}, false, true).toString();
                if (strength == null) strength = "2";
            }
        } catch (Exception e) {
            strength = "2";
        }
        return strength;
    }

    //TODO: (high) add notes -jrmitaly
    //TODO: (HIGH) Need to refactor the way this all works - it would be nice if SQL Args weren't tied to a specific index and if custom filtering was cleaned up etc etc. Standard for how server grids can filter! It's almost there.. just needs some work.. -jrmitaly 9/17/2015
    @Deprecated
    public ArrayList addFilterArguments(HttpServletRequest request, ServerGridProperties gridProp) {
        return addFilterArguments(request, gridProp, false);
    }

    /**
     * Populate a new array with only the constraint rules, mainly so we can be sure to add them
     * in a separate param to the sql query. This is useful for server grids that are dependant on some other value,
     * like another grid's row ID or the currently selected client.
     * This will prevent the user from clearing this filter/changing it to an OR clause
     * @param request
     * @param gridProp
     * @return
     */
    public ArrayList addConstraintArguments(HttpServletRequest request, ServerGridProperties gridProp) {
        return addFilterArguments(request, gridProp, true);
    }

    /**
     * This is what actually transforms the filter rules into the SQL where clause parameters
     * TODO: add more comments
     * @param request
     * @param gridProp
     * @param processConstraints if this is true, we are only going to add arguments for the
     * @return
     */
    public ArrayList addFilterArguments(HttpServletRequest request, ServerGridProperties gridProp, boolean processConstraints) {
        ArrayList<Object> sqlArgsList = new ArrayList<>();
        try {
            //this is needed because we may add special filters
            sqlArgsList.add(""); //add filler to array index 0 (sql arg {1}
            //declare strings needed for dynamic filters
            String whereClause = "";
            String clauseLogic = "";
            String searchField = "";
            String searchOperator = "";
            String searchString = "";
            //declare hashmap to translate the filter operators
            HashMap<String, String> filterOperators = new HashMap<>();
            //default filters
            filterOperators.put("eq", "="); //equals
            filterOperators.put("ne", "<>"); //not equals
            filterOperators.put("bw", "LIKE"); //begins with
            filterOperators.put("bn", "NOT LIKE"); //does not being with
            filterOperators.put("ew", "LIKE"); //ends with
            filterOperators.put("en", "NOT LIKE"); //does not end with
            filterOperators.put("cn", "LIKE"); //contains
            filterOperators.put("nc", "NOT LIKE"); //does not contain
            //non default filters
            filterOperators.put("lt", "<"); //less than
            filterOperators.put("le", "<="); //less than or equal to
            filterOperators.put("gt", ">"); //greater than
            filterOperators.put("ge", ">="); //greater than or equal to
            //begin building where clause
            StringBuilder whereClauseBuilder = new StringBuilder();
            //START - Ownership Group Filtering
            if (gridProp.getFilterOwnerShip()) { //if we are filtering by ownership group
                String ownershipGroups = ArrayListToCSV(getUserOwnershipGroupStringList(request)); //add ownership group filtering to where clause
                if (ownershipGroups.equals("-1")) {
                    whereClauseBuilder.append("<> 0");
                } else {
                    whereClauseBuilder.append("IN(");
                    whereClauseBuilder.append(ownershipGroups);
                    whereClauseBuilder.append(")");
                }
            }
            //END - Ownership Group Filtering
            //START - Account Group Filtering
            if (gridProp.getFilterAccountGroups()) { //if we are filtering by account group
                if (Integer.parseInt(checkSession(request)) < 0) { //TODO: (HIGH) review - for My Quickcharge
                    whereClauseBuilder.append("<> 0");
                } else {
                    String payRollGroups = getUserPayRollGroupsString(request); //add ownership group filtering to where clause
                    if (payRollGroups.equals("-1")) {
                        whereClauseBuilder.append("<> 0");
                    } else {
                        whereClauseBuilder.append("IN(");
                        whereClauseBuilder.append(payRollGroups);
                        whereClauseBuilder.append(")");
                    }
                }
            }

            boolean firstWhereClauseParameter=true;

            //END - Ownership Group Filtering
            //START - jQgrid Search Filtering
            HashMap<String, Object> filterHM = gridProp.getFilterHM(); //if the user did a jQgrid search we will have filters in our postParams HashMap
            if (filterHM != null) {
                boolean firstSearchParam = true;


                if (containsFullKey(filterHM.get("groupOp"))) {
                    clauseLogic = filterHM.get("groupOp").toString();
                }


                ArrayList<Object> valueArrList = new ArrayList<>();

                if(containsFullKey(filterHM.get("rules"))){
                    valueArrList = toArrayList(filterHM.get("rules"));
                }

                if(processConstraints){//By definition we ALWAYS want to require ALL constraints
                    clauseLogic = "AND";
                }

                //iterate through array list of filterHashmaps
                for (int searchIndex = 0; searchIndex < valueArrList.size(); searchIndex++) {

                    boolean specialParamUsed = false;
                    boolean constraintRule = false;
                    Map<String, Object> innerValue = (HashMap<String, Object>) valueArrList.get(searchIndex);

                    if(containsFullKey(innerValue.get("field"))){
                        searchField = innerValue.get("field").toString().toUpperCase();
                    }

                    if(containsFullKey(innerValue.get("op"))){
                        searchOperator = innerValue.get("op").toString();
                    }

                    if(containsFullKey(innerValue.get("data"))){
                        searchString = innerValue.get("data").toString().toUpperCase();
                    }
                    if(containsFullKey(innerValue.get("constraint"))){
                        constraintRule = true;
                    }
                    //if there is a missmatch, ignore this rule
                    if(constraintRule != processConstraints)
                        continue;
                    //START - Dynamic and Custom Parameter Handling
                    if( gridProp.getCustomFilters() != null && gridProp.getCustomFilters().containsKey(searchField)){

                        /* if (searchField.equals(filter.getKey())) {
                                //TODO: (HIGH) Needs abstraction in ServerGridProperties class - need to be able to desiginate where the custom filter should go - jrmitaly 3/2/2015
                                if (searchField.equals("REVENUECENTERIDS") || searchField.equals("VENDORIDS") || searchField.equals("PRINTERIDS")) {
                                    //TODO: (HIGH) Needs abstraction in ServerGridProperties class -jrmitaly 3/2/2015
                                    specialParamUsed = true;
                                    whereClauseBuilder.append(" "+clauseLogic+" "+filter.getValue().replaceAll("searchString", searchString));
                                } else {
                                    specialParamUsed = true;
                                    //TODO: Does "AND" have to be replaced with clauseLogic here? What about "OR" ? -jrmitaly 3/2/2015
                                    sqlArgsList.set(0, sqlArgsList.get(0).toString() + " AND " + filter.getValue().replaceAll("searchString", searchString));
                                }
                        }*/

                        specialParamUsed = true;

                        if (clauseLogic == null || clauseLogic.isEmpty()) {
                            clauseLogic = "AND";
                        }


                        //HARDCODE - hardcoded switch for custom filters for the Inventory Editor - done for 8.2.1 Henry Ford - jrmitaly 3/8/2017
                        String customFilterSQL = gridProp.getCustomFilters().get(searchField).toString().replace("searchString", searchString).replace("searchOperatorString", filterOperators.get(searchOperator));
                        if (searchField.equals("$_INVREVENUECENTERID") || searchField.equals("$_INVLOCATIONID")) {
                            if ( sqlArgsList.size() == 1 ) {
                                sqlArgsList.add("");
                            }
                            sqlArgsList.set(1, sqlArgsList.get(1).toString() + " AND " + customFilterSQL);
                        } else {
                            if ( sqlArgsList.get(0).toString().equals("") ) {
                                sqlArgsList.set(0, " AND ("+customFilterSQL+")");
                            } else {
                                String customFilterParamSQL = sqlArgsList.get(0).toString();
                                if ( searchField.equals("ACTIVEONLY") ) {
                                    sqlArgsList.set(0, " AND " + customFilterSQL + customFilterParamSQL);
                                } else {
                                    customFilterParamSQL = customFilterParamSQL.substring(0, customFilterParamSQL.length() - 1);
                                    sqlArgsList.set(0, customFilterParamSQL + " " + clauseLogic + " " + customFilterSQL + ")");
                                }
                            }
                        }

                    }
                    if (gridProp.getDetailFilters() != null) {
                        for (Map.Entry<String, String> filter : gridProp.getDetailFilters().entrySet()) {
                            if (searchField.equals(filter.getKey())) {
                                specialParamUsed = true;
                                if (firstSearchParam) {
                                    if (gridProp.getFilterOwnerShip() || gridProp.getFilterAccountGroups()) {
                                        whereClauseBuilder.append(" AND ");
                                    } else {
                                        whereClauseBuilder.append("WHERE ");
                                    }
                                    firstSearchParam = false;
                                }
                                whereClauseBuilder.append(filter.getValue().replaceAll("searchString", searchString));
                            }
                        }
                    }

                    //END - Dynamic and Custom Parameter Handling

                    if (!specialParamUsed) { // Don't bother with normal parameters if a dynamic one has already been used for this parameter
                        //prepare where clause for filters
                        if (firstSearchParam) {
                            if (gridProp.getFilterOwnerShip() || gridProp.getFilterAccountGroups() || gridProp.getFilterRevenueCenters()) {
                                whereClauseBuilder.append(" AND ");
                            } else {
                                whereClauseBuilder.append("WHERE ");
                            }
                            firstSearchParam = false;
                        }
                        //for handling multiple searches (with or without special parameters)
                        if (specialParamUsed) {
                            //do not append clause logic if a special parameter was used last iteration
                            specialParamUsed = false;
                        } else {
                            if (!firstWhereClauseParameter) {
                                whereClauseBuilder.append(" ");
                                whereClauseBuilder.append(clauseLogic);
                                whereClauseBuilder.append(" ");
                            }
                        }
                        //format searchString based on searchOperator
                        if (searchOperator.equals("bw") || searchOperator.equals("bn")) {
                            searchString = searchString + '%';
                        }
                        if (searchOperator.equals("ew") || searchOperator.equals("en")) {
                            searchString = '%' + searchString;
                        }
                        if (searchOperator.equals("cn") || searchOperator.equals("nc") || searchOperator.equals("in") || searchOperator.equals("ni")) {
                            searchString = '%' + searchString + '%';
                        }
                        //add searchField to where clause
                        whereClauseBuilder.append(searchField);
                        whereClauseBuilder.append(" ");
                        //add search operator to where clause
                        whereClauseBuilder.append(filterOperators.get(searchOperator));
                        whereClauseBuilder.append(" ");
                        //add searchString to where clause
                        whereClauseBuilder.append(" '");
                        whereClauseBuilder.append(searchString);
                        whereClauseBuilder.append("' ");
                        firstWhereClauseParameter=false;
                    }
                }
            }

            //END - jQgrid Search Filtering
            whereClause = whereClauseBuilder.toString(); //convert whereClauseBuilder to a string object
            Logger.logMessage("Successfully built whereClause: " + whereClause, Logger.LEVEL.DEBUG);
            sqlArgsList.add(whereClause);
            return sqlArgsList;
        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Error building where clause: " + ex, Logger.LEVEL.ERROR);
            return sqlArgsList;
        }
    }

    /**
     * WARNING - ONLY USE THE RESULTS OF THIS IN A PARAMETERIZED DB call. Any search params are not validated against SQLInjection
     * this method appends the custom filters to the main sql query.
     * Note: Constraints must use customfilters to work properly
     * @param request
     * @param gridProp
     * @param dataProp TODO: i suppose we dont need this after all. Should get removed
     * @param sqlArgsList Requires a holder postion at 0 and 1 for the filterSQL.
     *                    this array WILL CONTAIN DANGEROUS user input(IT MAY BE MALITIOUS DATA. DO NOT TRUST IT, IT MUST BE PARAMETERIZED)
     *                    a prepopulated sqlarg list using the old method. This will be appended to with the added searchparams
     * @return nothing. sqlArgsList popuated. where clause SQL at 0 and 1 new filter params appended.
     */
    public void buildServerGridFilterArguments(HttpServletRequest request, ServerGridProperties gridProp, DataHandlerProperties dataProp, ArrayList<Object> sqlArgsList) {
        try {
            int initialSQLParamCount = sqlArgsList.size();
            //this is needed because we may add special filters
            //declare strings needed for dynamic filters
            String clauseLogic = "";
            String searchField = "";
            String searchOperator = "";
            String searchString_DANGEROUS = "";//THIS FIELD MAY CONTAIN SQLINJECTION!!! Do not use it directly in ANY sql
            //declare hashmap to translate the filter operators
            HashMap<String, String> filterOperators = new HashMap<>();
            //default filters
            filterOperators.put("eq", "="); //equals
            filterOperators.put("ne", "<>"); //not equals
            filterOperators.put("bw", "LIKE"); //begins with
            filterOperators.put("bn", "NOT LIKE"); //does not being with
            filterOperators.put("ew", "LIKE"); //ends with
            filterOperators.put("en", "NOT LIKE"); //does not end with
            filterOperators.put("cn", "LIKE"); //contains
            filterOperators.put("nc", "NOT LIKE"); //does not contain
            //non default filters
            filterOperators.put("lt", "<"); //less than
            filterOperators.put("le", "<="); //less than or equal to
            filterOperators.put("gt", ">"); //greater than
            filterOperators.put("ge", ">="); //greater than or equal to
            //special handling for numeric values.
            filterOperators.put("isn","IS NULL"); // is null
            filterOperators.put("inn","IS NOT NULL"); // is not null
            //begin building where clause
            StringBuilder whereClauseBuilder_2 = new StringBuilder();//builder for the {2} param
            StringBuilder whereClauseBuilder_1 = new StringBuilder(); //builder for the {1} param
            StringBuilder whereClauseBuilder_10 = new StringBuilder(); //builder for the {10} param aka constraints

            //START - Ownership Group Filtering
            if (gridProp.getFilterOwnerShip()) { //if we are filtering by ownership group
                String ownershipGroups = ArrayListToCSV(getUserOwnershipGroupStringList(request)); //add ownership group filtering to where clause
                if (ownershipGroups.equals("-1")) {
                    whereClauseBuilder_2.append("<> 0");
                }else if(ownershipGroups.equals("")){
                    whereClauseBuilder_2.append("IN(-1)");//nothing
                } else {
                    whereClauseBuilder_2.append("IN(");
                    //TODO: parameterize this
                    whereClauseBuilder_2.append(ownershipGroups);
                    whereClauseBuilder_2.append(")");
                }
            }
            //END - Ownership Group Filtering
            //START - Account Group Filtering
            if (gridProp.getFilterAccountGroups()) { //if we are filtering by account group
                if (Integer.parseInt(checkSession(request)) < 0) { //TODO: (HIGH) review - for My Quickcharge
                    whereClauseBuilder_2.append("<> 0");
                } else {
                    String payRollGroups = getUserPayRollGroupsString(request); //add ownership group filtering to where clause
                    if (payRollGroups.equals("-1")) {
                        whereClauseBuilder_2.append("<> 0");
                    } else if(payRollGroups.equals("")){
                        whereClauseBuilder_2.append("IN(-1)");//nothing
                    } else {
                        whereClauseBuilder_2.append("IN(");
                        //TODO: parameterize this
                        whereClauseBuilder_2.append(payRollGroups);
                        whereClauseBuilder_2.append(")");
                    }
                }
            }

            boolean firstWhereClauseParameter=true;

            //END - Ownership Group Filtering
            //START - jQgrid Search Filtering
            HashMap<String, Object> filterHM = gridProp.getFilterHM(); //if the user did a jQgrid search we will have filters in our postParams HashMap
            if (filterHM != null) {
                boolean firstSearchParam = true;
                ArrayList<Object> valueArrList = new ArrayList<>();

                if (containsFullKey(filterHM.get("groupOp"))) {
                    //validate the clause logic
                    String clauseLogic_DANGEROUS = filterHM.get("groupOp").toString();
                    if(clauseLogic_DANGEROUS.equals("AND"))
                        clauseLogic = "AND";
                    else if(clauseLogic_DANGEROUS.equals("OR"))
                        clauseLogic = "OR";
                    else
                        clauseLogic = "AND";
                }

                if(containsFullKey(filterHM.get("rules"))){
                    valueArrList = toArrayList(filterHM.get("rules"));
                }

                //iterate through array list of filterHashmaps
                for (int searchIndex = 0; searchIndex < valueArrList.size(); searchIndex++) {
                    StringBuilder currentBuilder = null;//{1} and {10} generate in the same way, but should be generated separately
                    boolean specialParamUsed = false;
                    boolean constraintRule = false;
                    boolean usingConnectingClause = false;
                    Map<String, Object> innerValue = (HashMap<String, Object>) valueArrList.get(searchIndex);

                    if(containsFullKey(innerValue.get("field"))){
                        searchField = innerValue.get("field").toString().toUpperCase();
                        //VALIDATE the field
                        searchField = sanitizeStringFromSQLInjection(searchField, "");
                    }

                    if(containsFullKey(innerValue.get("op"))){
                        //This param is transated and not put into the sql directly, validate anyway
                        searchOperator = innerValue.get("op").toString();
                        searchOperator = sanitizeStringFromSQLInjection(searchOperator, "");
                    }

                    if(containsFullKey(innerValue.get("data"))){
                        searchString_DANGEROUS = innerValue.get("data").toString().toUpperCase();
                    }
                    //Should this part of the filter always be defined with an AND?
                    if(containsFullKey(innerValue.get("constraint"))){
                        constraintRule = true;
                    }
                    //Value for connecting clause logic.
                    if(containsFullKey(innerValue.get("connector"))){
                        usingConnectingClause = true;
                    }
                    //Switch which builder we are using based on if it is a constraint
                    if(constraintRule)
                        currentBuilder = whereClauseBuilder_10;
                    else
                        currentBuilder = whereClauseBuilder_1;


                    //START - Dynamic and Custom Parameter Handling
                    if( gridProp.getCustomFilters() != null && gridProp.getCustomFilters().containsKey(searchField)){

                        /* if (searchField.equals(filter.getKey())) {
                                //TODO: (HIGH) Needs abstraction in ServerGridProperties class - need to be able to desiginate where the custom filter should go - jrmitaly 3/2/2015
                                if (searchField.equals("REVENUECENTERIDS") || searchField.equals("VENDORIDS") || searchField.equals("PRINTERIDS")) {
                                    //TODO: (HIGH) Needs abstraction in ServerGridProperties class -jrmitaly 3/2/2015
                                    specialParamUsed = true;
                                    whereClauseBuilder_2.append(" "+clauseLogic+" "+filter.getValue().replaceAll("searchString", searchString));
                                } else {
                                    specialParamUsed = true;
                                    //TODO: Does "AND" have to be replaced with clauseLogic here? What about "OR" ? -jrmitaly 3/2/2015
                                    sqlArgsList.set(0, sqlArgsList.get(0).toString() + " AND " + filter.getValue().replaceAll("searchString", searchString));
                                }
                        }*/

                        specialParamUsed = true;

                        if (clauseLogic == null || clauseLogic.isEmpty()) {
                            clauseLogic = "AND";
                        }
                        //We want to force constraint type params to ALWAYS use AND
                        String useClauseLogic = clauseLogic;
                        if(constraintRule)
                            useClauseLogic = "AND";


                        //HARDCODE - hardcoded switch for custom filters for the Inventory Editor - done for 8.2.1 Henry Ford - jrmitaly 3/8/2017
                        String customFilterSQL = gridProp.getCustomFilters().get(searchField).toString().replace("searchString", "{" + (sqlArgsList.size() + 1) +  "}").replace("searchOperatorString", filterOperators.get(searchOperator));
                        sqlArgsList.add(searchString_DANGEROUS);
                        if (searchField.equals("$_INVREVENUECENTERID") || searchField.equals("$_INVLOCATIONID")) {
                            whereClauseBuilder_10.append(" AND ").append(customFilterSQL);//parameterized

                        } else {
                            if (currentBuilder.length() == 0) {
                                currentBuilder.append(" AND (").append(customFilterSQL).append(")");
                            } else {
                                if ( searchField.equals("ACTIVEONLY") ) {
                                    //prepend the custom filter:  AND, FILTER, EXISTING
                                    currentBuilder.insert(0, customFilterSQL);
                                    currentBuilder.insert(0, " AND ");

                                } else {
                                    // Custom Operator Handling
                                    if(usingConnectingClause){

                                        if(innerValue.get("connector").equals("1")){
                                            useClauseLogic = "AND";
                                        }
                                        else if(innerValue.get("connector").equals("2")){
                                            useClauseLogic = "OR";
                                        }
                                    }
                                    //remove the last ")"
                                    currentBuilder.setLength(currentBuilder.length()-1);
                                    //append the custom filter
                                    currentBuilder.append(" ").append(useClauseLogic).append(" ").append(customFilterSQL).append(")");
                                }
                            }
                        }

                    }
                    if (gridProp.getDetailFilters() != null) {
                        for (Map.Entry<String, String> filter : gridProp.getDetailFilters().entrySet()) {
                            if (searchField.equals(filter.getKey())) {
                                specialParamUsed = true;
                                if (firstSearchParam) {
                                    if (gridProp.getFilterOwnerShip() || gridProp.getFilterAccountGroups()) {
                                        whereClauseBuilder_2.append(" AND ");
                                    } else {
                                        whereClauseBuilder_2.append("WHERE ");
                                    }
                                    firstSearchParam = false;
                                }
                                //replace the param in the filter with the correct param number, and add that param to the arg list
                                whereClauseBuilder_2.append(filter.getValue().replaceAll("searchString", "{"+ sqlArgsList.size() + "}"));
                                sqlArgsList.add(searchString_DANGEROUS);
                            }
                        }
                    }

                    //END - Dynamic and Custom Parameter Handling

                    if (!specialParamUsed) { // Don't bother with normal parameters if a dynamic one has already been used for this parameter
                        //prepare where clause for filters
                        if (firstSearchParam) {
                            if (gridProp.getFilterOwnerShip() || gridProp.getFilterAccountGroups() || gridProp.getFilterRevenueCenters()) {
                                whereClauseBuilder_2.append(" AND ");
                            } else {
                                whereClauseBuilder_2.append("WHERE ");
                            }
                            firstSearchParam = false;
                        }
                        //for handling multiple searches (with or without special parameters)
                        if (specialParamUsed) {
                            //do not append clause logic if a special parameter was used last iteration
                            specialParamUsed = false;
                        } else {
                            if (!firstWhereClauseParameter) {
                                whereClauseBuilder_2.append(" ");
                                whereClauseBuilder_2.append(clauseLogic);
                                whereClauseBuilder_2.append(" ");
                            }
                        }

                        //TODO: Don't hardcode these fields.
                        // serverGrid searchable numeric/money columns
                        ArrayList<String> knownNumericFields = new ArrayList<String>(){{
                            // account manager fields
                            add("E.BADGE"); add("E.RELOADAMOUNT");
                            add("E.RELOADTHRESHOLD"); add("TRANSAMOUNT");
                            add("TRANSBALANCE"); add("SPLITS");
                            // person account manager fields
                            add("P.LOWBALANCETHRESHOLD");
                            // product editor fields
                            add("NETWEIGHT"); add("GROSSWEIGHT");
                            add("MAXOPENPRICE"); add("MINOPENPRICE");
                            add("PRICE"); add("MAXQUANTITY");
                            add("MINQUANTITY");
                            // pos transaction viewer fields
                            add("PATRANS.PATRANSACTIONID"); add("PATRANSIDOFFLINE");
                            add("DIRECTVOIDAMT"); add("DIRECTVOIDQTY");
                            add("INDIRECTVOIDAMT"); add("INDIRECTVOIDQTY");
                        }};

                        // Handle non-numeric searchStrings being passed for numeric fields.
                        if(knownNumericFields.contains(searchField)){

                            if(searchString_DANGEROUS.equals("")){
                                if(searchOperator.equals("eq")){
                                    searchOperator = "isn"; // is null
                                }
                                else if(searchOperator.equals("ne")){
                                    searchOperator = "inn"; // is not null
                                }
                                else{
                                    // not a valid query.
                                    searchOperator = "cn";
                                }
                            }
                            else{

                                String numericSearch = searchString_DANGEROUS.replaceAll("[^0-9^\\.]", "X");
                                if(numericSearch.contains("X")){
                                    // if any invalid characters, not a valid query.
                                    searchOperator = "cn";
                                }
                            }

                        }

                        //format searchString based on searchOperator
                        String formattedSearch = "{" + (sqlArgsList.size() + 1) +"}";

                        if (searchOperator.equals("bw") || searchOperator.equals("bn")) {
                            formattedSearch = formattedSearch + " + '%'";
                        }
                        if (searchOperator.equals("ew") || searchOperator.equals("en")) {
                            formattedSearch = "'%' + " + formattedSearch;
                        }
                        if (searchOperator.equals("cn") || searchOperator.equals("nc") || searchOperator.equals("in") || searchOperator.equals("ni")) {
                            formattedSearch = "'%' + " + formattedSearch + " + '%'";
                        }
                        if(searchOperator.equals("isn") || searchOperator.equals("inn")){
                           formattedSearch = "";
                        }
                        //add searchField to where clause
                        whereClauseBuilder_2.append(searchField);//this has been validated as safe
                        whereClauseBuilder_2.append(" ");
                        //add search operator to where clause
                        whereClauseBuilder_2.append(filterOperators.get(searchOperator));//this is translated, therefor safe
                        whereClauseBuilder_2.append(" ");
                        //add searchString to where clause and parameterize it
                        whereClauseBuilder_2.append(formattedSearch);
                        sqlArgsList.add(searchString_DANGEROUS);
                        firstWhereClauseParameter=false;
                    }
                }
            }


            //TODO: the sqlArgs shouldnt have the queries inside should be appending to a where clause
            //for handling Quickcharge hyperfinds
            if (!gridProp.getFilterHyperfind() && gridProp.getQueryId() != null) { //update where clause for hyperfind filtering for accounts
                whereClauseBuilder_2.append(qcHyperfindWhereText(gridProp.getQueryId().toString()));
            }

            //filter records to the only the records in the user's selected list by adding them to the where clause (param 1)
            if (gridProp.getPostParams().get("userSelectedListFilter") != null) {
                if(gridProp.getPostParams().get("userTypeID") != null && gridProp.getPostParams().get("personID") != null) {
                    //add the proper type of clause
                    if(whereClauseBuilder_2.length() > 0)
                        whereClauseBuilder_2.append(" AND");
                    else
                        whereClauseBuilder_2.append(" WHERE");
                    //add the user's selected list
                    whereClauseBuilder_2.append(" EXISTS (SELECT 1 FROM MMH_SelectedListDetail sld WHERE sld.UserID = {" + (sqlArgsList.size() + 1) + "} AND ");
                    sqlArgsList.add(gridProp.getUserId());
                    //add the correct type of person ID
                    if(gridProp.getFilterHyperfind())
                        whereClauseBuilder_2.append("v.PersonId");
                    else{
                        //We should parameterize this too, but we cant add it because it specifies table/column names
                        whereClauseBuilder_2.append(sanitizeStringFromSQLInjection(gridProp.getPostParams().get("personID").toString(), ""));
                    }
                    whereClauseBuilder_2.append(" = sld.PersonID AND sld.UserTypeID = {" + (sqlArgsList.size() + 1) + "})");
                    sqlArgsList.add(gridProp.getPostParams().get("userTypeID").toString());
                } else {
                    //add the proper type of clause
                    if(whereClauseBuilder_2.length() > 0)
                        whereClauseBuilder_2.append(" AND");
                    else
                        whereClauseBuilder_2.append(" WHERE");
                    //add the user's selected list
                    whereClauseBuilder_2.append(" EXISTS (SELECT 1 FROM MMH_SelectedListDetail sld WHERE sld.UserID = {" + (sqlArgsList.size() + 1) + "} AND ");
                    sqlArgsList.add(gridProp.getUserId());

                    if(gridProp.getFilterHyperfind())
                        whereClauseBuilder_2.append("v.PersonId");
                    else{
                        whereClauseBuilder_2.append("E.EmployeeID");
                    }
                    whereClauseBuilder_2.append(" = sld.PersonID)");
                }
            } else if (gridProp.getPostParams().get("myQCUserSelectedListFilter") != null && gridProp.getPostParams().get("userID") != null) { //TODO: (HIGH) review - for My Quickcharge
                //we have to reset the sqlArgsList to what it was before the function and clear the builder
                while(sqlArgsList.size() > initialSQLParamCount){
                    sqlArgsList.remove(sqlArgsList.size()-1);
                }
                whereClauseBuilder_2.setLength(0);
                whereClauseBuilder_2.append(" != 0 AND EXISTS (SELECT 1 FROM MMH_SelectedListDetail sld WHERE sld.UserID = " + checkSession(request)
                        + " AND E.EmployeeID = sld.PersonID)");
            }

            //END - jQgrid Search Filtering
            //convert whereClauseBuilder_2 to a string object, and place them in the holder positions
            String whereClause_1 = whereClauseBuilder_1.toString();
            String whereClause_2 = whereClauseBuilder_2.toString();
            String whereClause_10 = whereClauseBuilder_10.toString();
            Logger.logMessage("Successfully built whereClause for {1}: " + whereClause_1, Logger.LEVEL.DEBUG);
            Logger.logMessage("Successfully built whereClause for {2}: " + whereClause_2, Logger.LEVEL.DEBUG);
            Logger.logMessage("Successfully built whereClause for {10}: " + whereClause_10, Logger.LEVEL.DEBUG);
            sqlArgsList.set(0,whereClause_1);
            sqlArgsList.set(1,whereClause_2);
            sqlArgsList.set(9,whereClause_10);
            //these args should then be used to modify the query, then use parameterization
        } catch (Exception ex) {
            Logger.logException(ex);
            Logger.logMessage("Error building where clause: " + ex, Logger.LEVEL.ERROR);
        }
    }


    //TODO: this should really be using true parameterization, but at least it shouldn't be vulnerable as of 10/7/2019
    public static String qcHyperfindWhereText(String queryId_DANGEROUS) {
        //we HAVE to validate the query ID before it touches any sql. It could be injection
        Integer queryID_VALID = Integer.parseInt(queryId_DANGEROUS);//will throw an error if its not a number
        String queryId = queryID_VALID.toString();

        StringBuilder returnText = new StringBuilder();
        if (MMHProperties.getAppSetting("database.type").compareToIgnoreCase("ORACLE") == 0) {
            // Oracle text for filtering by spending profiles in hyperfind filters
            returnText.append(" AND (E.TerminalProfileID IN (SELECT COLUMN_VALUE FROM table(FNQC_STRINGARRAYCONVERT((SELECT TerminalProfileIDS FROM QC_Query WHERE QueryID = " + queryId + ")))) OR (SELECT TerminalProfileIDS FROM QC_Query WHERE QueryID = " + queryId + ") IS NULL)");
            // Oracle text for filtering by account groups in hyperfind filters
            returnText.append(" AND (E.PayRollGroupingID IN (SELECT COLUMN_VALUE FROM table(FNQC_STRINGARRAYCONVERT((SELECT PayRollGroupingIDs FROM QC_Query WHERE QueryID = " + queryId + ")))) OR (SELECT PayRollGroupingIDs FROM QC_Query WHERE QueryID = " + queryId + ") IS NULL)");
            // Oracle text for filtering by employee ids in hyperfind filters
            returnText.append(" AND (E.EmployeeID IN (SELECT COLUMN_VALUE FROM table(FNQC_STRINGARRAYCONVERT((SELECT EmployeeIDs FROM QC_Query WHERE QueryID = " + queryId + ")))) OR (SELECT EmployeeIDs FROM QC_Query WHERE QueryID = " + queryId + ") IS NULL)");
        } else {
            // SQL Server text for filtering by spending profiles in hyperfind filters
            returnText.append(" AND (E.TerminalProfileID IN (SELECT col FROM fnQC_StringArrayConvert((SELECT TerminalProfileIDS FROM QC_Query WHERE QueryID = " + queryId + "))) OR (SELECT TerminalProfileIDS FROM QC_Query WHERE QueryID = " + queryId + ") = '' OR (SELECT TerminalProfileIDS FROM QC_Query WHERE QueryID="+queryId+") IS NULL)");
            // SQL Server text for filtering by account groups in hyperfind filters
            returnText.append(" AND (E.PayRollGroupingID IN (SELECT col FROM fnQC_StringArrayConvert((SELECT PayRollGroupingIDs FROM QC_Query WHERE QueryID = " + queryId + "))) OR (SELECT PayRollGroupingIDs FROM QC_Query WHERE QueryID = " + queryId + ") = '' OR (SELECT PayRollGroupingIDs FROM QC_Query WHERE QueryID="+queryId+") IS NULL)");
            // SQL Server text for filtering by employee ids in hyperfind filters
            returnText.append(" AND (E.EmployeeID IN(SELECT col FROM fnQC_StringArrayConvert((SELECT EmployeeIDs FROM QC_Query WHERE QueryID = " + queryId + "))) OR (SELECT EmployeeIDs FROM QC_Query WHERE QueryID = " + queryId + ") = '' OR (SELECT EmployeeIDs FROM QC_Query WHERE QueryID="+queryId+") IS NULL)");
        }
        return returnText.toString();
    }

    //gets all of the globally defined filter limits in array list of hashmaps
    public ArrayList<HashMap> getFilterLimits(HttpServletRequest request) {
        ArrayList<HashMap> filterLimits = new ArrayList<>();
        try {
            String userID = checkSession(request);
            if (!userID.isEmpty()) {
                filterLimits = dm.serializeSqlWithColNames("data.common.getFilterLimits", new Object[] {userID});
            }
        } catch (Exception ex) {
            Logger.logMessage("Error in method commonMMHFunctions.getFilterLimits: " + ex.toString(), Logger.LEVEL.ERROR);
        }
        return filterLimits;
    }

    //keeps session alive
    public String maintainSession(HttpServletRequest request) {
        if (!checkSession(request).equals("")) {
            HttpSession thisSession = request.getSession();
            Date lastAccessedDate = new Date(thisSession.getLastAccessedTime());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return formatter.format(lastAccessedDate);
        } else {
            return "invalid session found";
        }
    }

    public ArrayList<HashMap> generatePLUCode(HttpServletRequest request){
        ArrayList<HashMap> returnList = new ArrayList<>();
        try{
            // NOTE: THE NEW SP_QC_GetNextPLUCode GETS THE CURRENT CODE, CHECKS FOR USAGE AND INCREMENTS IN ONE STEP
            String uniquePLUCode = dm.getSingleField("data.common.generateNextPLUCode", new Object[]{}).toString();
            returnList.add(commonErrorMessage(uniquePLUCode,"PLUCODE"));
        }catch (Exception ex){
            Logger.logException(ex); //always log exceptions
            returnList.add(commonErrorMessage("Unable to generate new PLU Code","displayErrorMsg"));
        }
        return returnList;
    }

    public ArrayList<HashMap> isPLUCodeInUse(HttpServletRequest request, String pluID){
        ArrayList<HashMap> results = new ArrayList<>();
        try{
            String count = dm.getSingleField("data.common.isPLUCodeInUse", new Object[]{pluID}).toString();
            results.add(commonErrorMessage(count,"INUSE"));
        }catch (Exception e){
            results.add(commonErrorMessage("Unable to determine if Item Code is in use","displayErrorMsg"));
            Logger.logMessage("Unable to determine if Item Code is in use", Logger.LEVEL.ERROR);
            logStackTrace(e);
        }
        return results;
    }

    public boolean isProductActive(HttpServletRequest request, final String PAPLUID) {
        boolean isActive = true;
        try {
            String active = dm.getSingleField("data.pluEditor.isProductActive", new Object[]{PAPLUID}).toString();
            isActive = Boolean.valueOf(active);
        } catch (Exception e){
            Logger.logMessage("Unable to determine if product is active", Logger.LEVEL.ERROR);
        }
        return isActive;
    }

    public String verifyProductCode(HttpServletRequest request, final String PAPLUID) {
        String PLUCode = "";
        try {
            PLUCode = dm.getSingleField("data.pluEditor.verifyProductCode", new Object[]{PAPLUID}).toString();
        } catch (Exception e){
            Logger.logMessage("Unable to verify product's PLU code", Logger.LEVEL.ERROR);
        }
        return PLUCode;
    }

    public String printPOs(String taskTypeIDStr, String PurchaseOrderIDsStr, HttpServletRequest request) {
        try {
            String userIDStr = checkSession(request);
            if (!userIDStr.equals("")) { //check that user is logged in
                Logger.logMessage("Running method PurchaseOrderDataHandler:printPOs...", Logger.LEVEL.DEBUG);

                //convert strings to integers to prevent sql injection
                Integer taskTypeID = Integer.parseInt(taskTypeIDStr);
                Integer userID = Integer.parseInt(userIDStr);

                //create a task to print the pos
                dm.serializeUpdate("data.purchaseOrderEditor.printPOs", new Object[]{taskTypeID, userID, PurchaseOrderIDsStr});

            } else {
                Logger.logMessage("Error within method PurchaseOrderDataHandler:printPOs Invalid Access", Logger.LEVEL.ERROR);
                return "Invalid Access";
            }
        } catch (Exception ex) {
            Logger.logMessage("Error within method PurchaseOrderDataHandler:printPOs:" + ex.toString(), Logger.LEVEL.ERROR);
            return "error";
        }
        return "success";
    }

    //Determines if a keypad is eligible for being a rotating keypad
    //Checks if the keypad is a detail of another keypad, if it is a keypad controller, or if it is a modifier keypad
    //Returns false if any condition is true
    public Boolean isKeypadEligibileForRotation(HttpServletRequest request, String keypadID) {
        String count;

        try{
            //check if keypad is a detail of another keypad - returns a count of rows
            count = dm.getSingleField("data.common.isKeypadDetail", new Object[]{keypadID}).toString();

            //if zero returned, keypad is not a detail of another keypad
            if ( count.equals("0") ) {

                //check if keypad is a controller keypad on the rotations table - returns a count of rows
                count = dm.getSingleField("data.common.isKeypadController", new Object[]{keypadID}).toString();

                //if zero returned, keypad is not a controller keypad
                if ( !count.equals("0") ) {
                    // if count is not zero then is a controller keypad
                    return false;
                }
            } else {
                return false;
            }
        }catch (Exception e){
            Logger.logMessage("Unable to determine if keypad is eligible for rotation", Logger.LEVEL.ERROR);
            logStackTrace(e);
        }

        return true;
    }

    //Determines if a keypad is currently a rotation detail keypad
    //returns true if keypadID is found as a rotation detail PAKeypadID
    public Boolean isKeypadARotationDetail(HttpServletRequest request, String keypadID) {
        String count;

        try{
            //check if keypad is a detail of another keypad - returns a count of rows
            count = dm.getSingleField("data.common.isRotationDetail", new Object[]{keypadID}).toString();

            if ( !count.equals("0") ) {
                return true;
            }

        }catch (Exception e){
            Logger.logMessage("Unable to determine if keypad is eligible for removal from rotation keypad (flag)", Logger.LEVEL.ERROR);
            logStackTrace(e);
        }

        return false;
    }

    //Determines if a keypad is currently a rotation detail keypad
    //returns true if keypadID is found as a rotation detail PAKeypadID
    public Boolean isKeypadAModifierDetail(HttpServletRequest request, String keypadID) {
        String count;

        try{
            //check if keypad is a detail of another keypad - returns a count of rows
            count = dm.getSingleField("data.common.isModifierDetail", new Object[]{keypadID}).toString();

            if ( !count.equals("0") ) {
                return true;
            }

        }catch (Exception e){
            Logger.logMessage("Unable to determine if keypad is eligible for removal from modifier keypad (flag)", Logger.LEVEL.ERROR);
            Logger.logException(e);
        }

        return false;
    }

    public ArrayList<HashMap> getMyQCConfigurableFeatures(HttpServletRequest request){
        ArrayList<HashMap> featureList = new ArrayList<>();

        try {
            featureList = dm.parameterizedExecuteQuery("data.globalSettings.getMyQCConfigurableFeatures", new Object[]{}, true);
        } catch (Exception ex) {
            Logger.logException(ex);
        }

        return featureList;
    }
/* END - Other common methods */

/* BEGIN - additional common methods */

    // Utility method to add an Object to the beginning of an ArrayList of Objects while changing null Objects to blank Strings
    public static ArrayList<Object> prependObjectArrayList(ArrayList<Object> original, Object additional) {
        ArrayList<Object> outputList = new ArrayList<>();
        outputList.add(additional);
        for (Object o : original) {
            if (o == null) {
                outputList.add("");
            } else {
                outputList.add(o);
            }
        }
        return outputList;
    }

    // Utility method to add an Object to the beginning of an ArrayList of Objects and output as an array
    public static Object[] prependObjectArray(ArrayList<Object> original, Object additional) {
        ArrayList<Object> a = prependObjectArrayList(original, additional);
        return a.toArray();
    }

    // Utility method to convert specific items of datetime type in an ArrayList of HashMaps to string equivalents, with a format
    public static ArrayList<HashMap> changeListDatetimeToString(ArrayList<HashMap> orig, String key, String dateFormat) {
        return changeListDatetimeToString(orig, key, dateFormat, 0);
    }
    public static ArrayList<HashMap> changeListDatetimeToString(ArrayList<HashMap> orig, String key, String dateFormat, int utcOffsetMinutes) {
        return changeListDatetimeToString(orig, key, dateFormat, 0, "");
    }
    public static ArrayList<HashMap> changeListDatetimeToString(ArrayList<HashMap> orig, String key, String dateFormat, int utcOffsetMinutes, String futurePlaceholder) {
        try {
            for (HashMap m : orig) {
                Iterator iter = m.entrySet().iterator();
                while (iter.hasNext()) {
                    Entry e = (Entry)iter.next();
                    if (e.getValue() != null && e.getValue().getClass().equals(ArrayList.class)) {
                        m.put(e.getKey().toString(), changeListDatetimeToString((ArrayList)e.getValue(), key, dateFormat, utcOffsetMinutes, futurePlaceholder));
                    } else if ((e.getValue() != null) && (e.getValue().getClass().equals(Timestamp.class) || e.getValue().getClass().equals(Date.class)) && (e.getKey().toString().compareToIgnoreCase(key) == 0)) {
                        String st = "";
                        Date unformattedDate = null;
                        if (e.getValue().getClass().equals(Timestamp.class)) {
                            Timestamp ts = (Timestamp)m.get(e.getKey().toString());
                            unformattedDate = new Date(ts.getTime());
                        } else if (e.getValue().getClass().equals(Date.class)) {
                            unformattedDate = (Date)m.get(e.getKey().toString());
                        }
                        if (unformattedDate != null) {
                            boolean dateMatchesPlaceholder = false;
                            if (futurePlaceholder.length() > 0) {
                                try {
                                    Date futureDate = new SimpleDateFormat(dateFormat).parse(futurePlaceholder);
                                    if (futureDate.compareTo(unformattedDate) <= 0) {
                                        dateMatchesPlaceholder = true;
                                    }
                                } catch (Exception ex) {
                                    dateMatchesPlaceholder = false;
                                }
                            }
                            if (utcOffsetMinutes != 0 && !dateMatchesPlaceholder) {
                                unformattedDate = dateFromUTC(unformattedDate, utcOffsetMinutes);
                            }
                            st = new SimpleDateFormat(dateFormat).format(unformattedDate);
                        }
                        if (st.length() > 0) {
                            m.put(e.getKey().toString(), st);
                        }
                    } else if (e.getKey().toString().compareToIgnoreCase(key) == 0 && e.getValue() == null) {
                        m.put(e.getKey().toString(), "");
                    }
                }
            }
        } catch (Exception e) {
            return orig;
        }
        return orig;
    }

    // Utility method to convert specific items of various time formats in an ArrayList of HashMaps to a full datetime object
    public static ArrayList<HashMap> changeListTimeStringToDatetime(ArrayList<HashMap> orig, String key, String startTimeFormat) {
        return changeListTimeStringToDatetime(orig, key, startTimeFormat, "1900-01-01");
    }
    public static ArrayList<HashMap> changeListTimeStringToDatetime(ArrayList<HashMap> orig, String key, String startTimeFormat, String baseDate) {
        try {
            for (HashMap m : orig) {
                Iterator iter = m.entrySet().iterator();
                while (iter.hasNext()) {
                    Entry e = (Entry)iter.next();
                    if (e.getValue() != null && e.getValue().getClass().equals(ArrayList.class)) {
                        m.put(e.getKey().toString(), changeListTimeStringToDatetime((ArrayList)e.getValue(), key, startTimeFormat, baseDate));
                    } else if ((e.getValue() != null) && e.getValue().getClass().equals(String.class) && (e.getKey().toString().compareToIgnoreCase(key) == 0)) {
                        m.put(key, new SimpleDateFormat("yyyy-MM-dd " + startTimeFormat).parse(baseDate + " " + m.get(key)));
                    }
                }
            }
        } catch (Exception e) {
            return orig;
        }
        return orig;
    }

    // Utility method to convert specific items of various datetime formats in an ArrayList of HashMaps to a full datetime object
    public static ArrayList<HashMap> changeListDatetimeStringToFormattedString(ArrayList<HashMap> orig, String key, String datetimeFormatString) {
        return changeListDatetimeStringToFormattedString(orig, key, datetimeFormatString, 0);
    }
    public static ArrayList<HashMap> changeListDatetimeStringToFormattedString(ArrayList<HashMap> orig, String key, String datetimeFormatString, int utcOffsetMinutes) {
        try {
            for (HashMap m : orig) {
                Iterator iter = m.entrySet().iterator();
                while (iter.hasNext()) {
                    Entry e = (Entry)iter.next();
                    if (e.getValue() != null && e.getValue().getClass().equals(ArrayList.class)) {
                        m.put(e.getKey().toString(), changeListDatetimeStringToFormattedString((ArrayList) e.getValue(), key, datetimeFormatString, utcOffsetMinutes));
                    } else if ((e.getValue() != null) && e.getValue().getClass().equals(String.class) && (e.getKey().toString().compareToIgnoreCase(key) == 0)) {
                        SimpleDateFormat givenFormat = new SimpleDateFormat(datetimeFormatString);
                        Date dt = givenFormat.parse(m.get(key).toString());
                        if (utcOffsetMinutes != 0) {
                            dt = dateToUTC(dt, utcOffsetMinutes);
                        }
                        m.put(key, new SimpleDateFormat(getSystemDatetimeFormat()).format(dt));
                    }
                }
            }
        } catch (Exception e) {
            return orig;
        }
        return orig;
    }

    // Utility method to encrypt specific items of string type in an ArrayList of HashMaps (decryptable format)
    public static ArrayList<HashMap> changeListTextToPassword(ArrayList<HashMap> orig, String key) {
        try {
            for (HashMap m : orig) {
                Iterator iter = m.entrySet().iterator();
                while (iter.hasNext()) {
                    Entry e = (Entry)iter.next();
                    if (e.getValue() != null && e.getValue().getClass().equals(ArrayList.class)) {
                        m.put(e.getKey().toString(), changeListTextToPassword((ArrayList) e.getValue(), key));
                    } else if ((e.getValue() != null) && (e.getValue().getClass().equals(String.class)) && (e.getKey().toString().compareToIgnoreCase(key) == 0)) {
                        String pass = commonStringToPassword(m.get(e.getKey().toString()).toString());
                        if (pass.length() > 0) {
                            m.put(e.getKey().toString(), pass);
                        } else {
                            Logger.logMessage("Removing non-parsable password field from record", Logger.LEVEL.ERROR);
                            iter.remove();
                        }
                    }
                }
            }
        } catch (Exception e) {
            return orig;
        }
        return orig;
    }

    // Utility method to get the current UTC date and time
    public static String getCurrentUTCDateTime() {
        SimpleDateFormat df = new SimpleDateFormat(getSystemDatetimeFormat());
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(new Date());
    }

    // Utility method to get the system date-time format string
    public static String getSystemDatetimeFormat() {
        try {
            return MMHProperties.getAppSetting("database.dateformat");
        } catch (Exception e) {
            return "MM/dd/yyyy HH:mm:ss";
        }
    }

    // Utility method to adjust a given Date to UTC
    public static Date dateToUTC(Date date, int offsetMinutes) {
        long currentTime = date.getTime();
        return new Date(currentTime + (offsetMinutes * MILLISECONDS_PER_MINUTE));
    }

    // Utility method to adjust a given Date from UTC
    public static Date dateFromUTC(Date date, int offsetMinutes) {
        long currentTime = date.getTime();
        return new Date(currentTime - (offsetMinutes * MILLISECONDS_PER_MINUTE));
    }

    // Utility method to remove a field from a record set
    public static HashMap removeFieldFromRecord(HashMap record, String fieldLabel) {
        try {
            Iterator iter = record.entrySet().iterator();
            while (iter.hasNext()) {
                Entry e = (Entry)iter.next();
                if (e.getKey().toString().compareToIgnoreCase(fieldLabel) == 0) {
                    record.remove(fieldLabel);
                }
            }
        } catch (Exception e) {
            return record;
        }
        return record;
    }

    // Utility method to retrieve the "LASTRECORDID" field from a record set
    public String removeReturnIdentifierValue(ArrayList<HashMap> errorCodeList) {
        Iterator<HashMap> it = errorCodeList.iterator();
        String recordID = "";
        while (it.hasNext()) {
            HashMap temp = it.next();
            if (temp.containsKey("LASTRECORDID")) {
                recordID = temp.get("LASTRECORDID").toString();
                if (!temp.containsKey("errorMsg")) {
                    it.remove();
                }
                break;
            }
        }
        return recordID;
    }

    // Utility method to validate and format an IP version 4 address - 0.0.0.0 through 255.255.255.255
    public static String cleanIPv4Address(String inputIP) {
        String errorMessage = "error";
        String ip = "";
        try {
            // Basic check on the length
            if (inputIP.length() > 15 || inputIP.length() < 7) { return errorMessage; }
            // Check against the regular expression
            String regex = "^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$";
            if (!inputIP.matches(regex)) { return errorMessage; }
            // Split up the input with a regular expression
            Pattern pat = Pattern.compile(regex);
            Matcher mat = pat.matcher(inputIP);
            if (mat.find()) {
                for (int i = 1; i <= 4; i++) {
                    if (mat.group(i) != null && tryParseInt(mat.group(i).toString())) {
                        int octet = Integer.parseInt(mat.group(i).toString());
                        if (octet >= 0 && octet <= 255) {
                            ip += (ip.length() > 0 ? "." : "") + Integer.toString(octet);
                        } else {
                            return errorMessage;
                        }
                    } else {
                        return errorMessage;
                    }
                }
            } else {
                return errorMessage;
            }
        } catch (Exception e) {
            Logger.logMessage("Error in cleanIPv4Address: " + e.getMessage(), Logger.LEVEL.ERROR);
            return errorMessage;
        }
        return ip;
    }

    public static boolean filePathValidation(HashMap argsObj) {
        boolean retValue = false;
        if (containsFullKey(argsObj.get("FILEPATH"))) {
            String path = argsObj.get("FILEPATH").toString();
            File f = new File(path);
            retValue = f.exists();
            //check if its a directory
            if (containsFullKey(argsObj.get("ISDIR")) && argsObj.get("ISDIR").equals("TRUE")) {
                retValue = retValue && f.isDirectory();
            }
            //check if file has certain extension
            if (containsFullKey(argsObj.get("EXT"))) {
                String ext = argsObj.get("EXT").toString().toLowerCase();
                String extension = f.getName();
                extension =  extension.substring(extension.lastIndexOf(".") + 1, extension.length()).toLowerCase();
                retValue = retValue && (ext.equals(extension));
            }
        } else {
            Logger.logMessage("Unable to determine file path for argument object", Logger.LEVEL.ERROR);
            Logger.logMessage(argsObj.toString(), Logger.LEVEL.ERROR);
        }
        return retValue;
    }

    //Validates the file and directory based on a given path
    //The containing div should have a data-pathType of 'both'
    //
    //@params argsObj - HM must contain FILEPATH
    //
    //@return response - Arraylist<Hashmap> indicating the responseMsg to the user
    public static ArrayList<HashMap> fileAndPathValidation(HashMap argsObj) {
        String responseMsg;
        HashMap responseHM = new HashMap();
        ArrayList<HashMap> response = new ArrayList<>();
        boolean fileExists;
        boolean directoryExists;
        boolean fullyVerified = false;
        boolean correctExt = false;
        boolean checkExt = false;
        String extension = "";

        //must have full file path
        if (containsFullKey(argsObj.get("FILEPATH"))) {

            //grab the path
            String path = argsObj.get("FILEPATH").toString();

            //check the path length for a valid path
            if ( path.length() > 1 && path.contains("/") ) {
                File f = new File(path);

                //checks the file
                fileExists = f.isFile();
                responseHM.put("FILEEXISTS", fileExists);

                if (fileExists && containsFullKey(argsObj.get("EXT"))){
                    checkExt = true;
                    String ext = argsObj.get("EXT").toString().toLowerCase();
                    extension = f.getName();
                    extension = extension.substring(extension.lastIndexOf(".") + 1, extension.length()).toLowerCase();
                    correctExt = ext.equals(extension);
                    responseHM.put("CORRECTEXT", correctExt);
                }

                //out of the path grab the beginning character to the last "/"
                String dir = path.substring(0, path.lastIndexOf("/"));

                //make new file object
                File d = new File(dir);

                //check if it is a directory
                directoryExists = d.isDirectory();
                responseHM.put("DIREXISTS", directoryExists);

                //determine which response is appropriate - 8 possible outcomes
                //if no extension to check
                if ( !checkExt || (checkExt && correctExt) )  {
                    if (directoryExists && fileExists) {
                        responseMsg = "File and directory confirmed for indicated path.";
                        fullyVerified = true;
                    } else if (directoryExists && !fileExists) {
                        responseMsg = "Directory confirmed, but file could not be found.";
                    } else if (!directoryExists && fileExists) {
                        //case when user enters trailing "/" like C:/test/test.exe/
                        responseMsg = "File confirmed, but directory could not be found. Check for slashes after the file name.";
                    } else {
                        responseMsg = "Neither directory nor file were found. Please enter a valid path (ex: C:/mydirectory/myfile.ext ).";
                    }
                } else { //checkExt && !correctExt
                    if (directoryExists && fileExists) {
                        responseMsg = "File and directory exist, but the file has the wrong extension. File should be of type " + extension;
                    } else if (directoryExists && !fileExists) {
                        responseMsg = "Directory confirmed, but file could not be found, check the extension.";
                    } else if (!directoryExists && fileExists) {
                        //case when user enters trailing "/" like C:/test/test.exe/
                        responseMsg = "File exists, but has the wrong extension. Directory could not be found - check for slashes after the file name.";
                    } else {
                        responseMsg = "Neither directory nor file were found. Please enter a valid path (ex: C:/mydirectory/myfile.ext ).";
                    }
                }

                responseHM.put("VERIFIED", fullyVerified);

            } else {
                responseMsg = "Please enter a valid path (ex: \"C:/mydirectory/myfile.ext\" ).";
            }
        } else {
            responseMsg = "Please enter a valid path (ex: \"C:/mydirectory/myfile.ext\" ).";
            Logger.logMessage("Unable to determine file path for argument object", Logger.LEVEL.ERROR);
            Logger.logMessage(argsObj.toString(), Logger.LEVEL.ERROR);
        }
        responseHM.put("MESSAGE", responseMsg);
        response.add(responseHM);
        return response;
    }

    public static boolean checkDirectoryPath(String path) {
        File f = new File(path);
        return f.exists() && f.isDirectory();
    }

    public static boolean checkFilePath(String path) {
        File f = new File(path);
        return f.exists();
    }

    public String getGobalNegativeFlag(HttpServletRequest request) {
        return dm.getSingleField("data.common.getGlobalNegativeFlag", new Object[] {}).toString();
    }

    // Utility method to return ArrayList<Object> from Array[Object]
    public static ArrayList<Object> ArrayToArrayList(Object[] params) {
        return new ArrayList<>(Arrays.asList(params));
    }

    // Utility method to transform a delimited list into an ArrayList of strings
    public static ArrayList<String> splitStringToArrayList(String inputString) {
        return splitStringToArrayList(inputString, ","); // Default delimiter of a comma
    }
    public static ArrayList<String> splitStringToArrayList(String inputString, String delimiter) {
        try {
            delimiter = "\\s*" + (delimiter == null || delimiter.length() < 0 ? delimiter : ",") + "\\s*";
            return new ArrayList<>(Arrays.asList(inputString.split(delimiter)));
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            return new ArrayList<>();
        }
    }

    //checks if the instance is on the ground
    public Boolean checkGround() {
        try {
            return CommonAPI.getOnGround();
        } catch (Exception ex) { // Just in case the value does not parse as a boolean
            Logger.logException(ex); //always log exceptions
            return true;
        }
    }

    //checks if the AutoCloudInvite is set in the Globals table
    public Boolean checkAutoCloudInvitesEnabled() {
        // Init return variable
        boolean isEnabled = false;
        String GLOBALS_AUTOCLOUDINVITE = "AUTOCLOUDINVITE";
        try {
            // Check setting in the Globals Table
            ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.invitations.global.autocloudinvite", new Object[]{}, true);
            if(!result.isEmpty() && result.get(0).containsKey(GLOBALS_AUTOCLOUDINVITE)){
                isEnabled = Boolean.parseBoolean(result.get(0).get(GLOBALS_AUTOCLOUDINVITE).toString());
            }
        } catch (Exception ex){
            Logger.logMessage("Error in method: checkAutoCloudInvitesEnabled()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        // Return result
        return isEnabled;
    }

    //determines the My QuickCharge user ID in any QC system
    public Integer determineMyQCUserID() {
        Integer MyQCUserID = null;
        try
        {
            //first determine the user profile assigned with the "MyQC" bit
            String myQCUserProfileID = dm.getSingleField("data.common.getMyQCUserProfile",
                    new Object[]{},
                    false,
                    true
            ).toString();

            if (myQCUserProfileID != null && !myQCUserProfileID.equals("")) {
                //then determine which QuickChargeUser is assigned this user profile
                MyQCUserID = Integer.parseInt(dm.getSingleField("data.common.getMyQCUserID",
                        new Object[]{
                                myQCUserProfileID
                        },
                        false,
                        true
                ).toString());
            } else {
                Logger.logMessage("Determining myQCUserProfileID in method commonMMHFunctions.determineMyQCUserID !!", Logger.LEVEL.ERROR);
            }
        }
        catch(Exception ex)
        {
            Logger.logMessage("Exception caught in method commonMMHFunctions.determineMyQCUserID:" + ex.toString(), Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
        if (MyQCUserID == null) {
            Logger.logMessage("Determining MyQCUserID in method commonMMHFunctions.determineMyQCUserID !!", Logger.LEVEL.ERROR);
        }
        return MyQCUserID;
    }

    //checks if a qcUserID has an employeeID associated with it (manager with my quick charge account)
    public String qcUserIDHasEmployeeID(String qcUserID) {
        String employeeID = null;
        try
        {
            if (MMHProperties.getAppSetting("site.application.offline") != null && !MMHProperties.getAppSetting("site.application.offline").equals("yes")) {
                //do the check
                Object employeeIDObj = dm.getSingleField("data.common.getEmployeeIDFromQCUserID",
                        new Object[]{
                                qcUserID
                        },
                        false,
                        true
                );
    
                //if we found an employeeID, make it negative
                if (employeeIDObj != null) {
                    employeeID = employeeIDObj.toString();
                    if (!employeeID.isEmpty()) {
                        Integer tmpEmployeeID = Integer.parseInt(employeeID);
                        tmpEmployeeID = tmpEmployeeID * -1;
                        employeeID = tmpEmployeeID.toString();
                    }
                }
            }
        }
        catch(Exception ex)
        {
            logStackTrace(ex); //always log exceptions
            employeeID = null;
            Logger.logException(ex); //always log exceptions
        }
        return employeeID; //return null if no match was found, no need to log
    }

    //gets product details by providing an array list of hashmaps of RevenueCenterIDs and ProductCodes
    public ArrayList<HashMap> getProductDetailsByCode(ArrayList<HashMap> revProductCodeList, HttpServletRequest request) {
        ArrayList<HashMap> productDetailList = new ArrayList<>();
        try {
            if (!checkSession(request).isEmpty()) { //if the user is logged in

                //iterate through revProductCodeList and build the productDetailList
                for(HashMap revProductHM : revProductCodeList){
                    String revCenterID = revProductHM.get("revCenterID").toString();
                    String productCode = revProductHM.get("productCode").toString();

                    //this overloaded dm.serializeWithColNames utilizes parameterized queries
                    ArrayList singleProductDetailList = dm.serializeSqlWithColNames("data.common.getProductDetailsByCode",
                            new Object[]{
                                productCode,
                                revCenterID
                            },
                            false,
                            true
                    );

                    //get the product details and add them to the productDetailList
                    if (singleProductDetailList != null && singleProductDetailList.size() == 1) {
                        HashMap singleProductDetailHM = (HashMap)singleProductDetailList.get(0);
                        productDetailList.add(singleProductDetailHM);
                    } else {
                        Logger.logMessage("Could not find PRODUCTCODE: "+productCode+ " in commonMMHFunctions.getProductDetailsByCode.", Logger.LEVEL.WARNING);
                        HashMap singleProductDetailHM = new HashMap();
                        singleProductDetailHM.put("PRODUCTCODE", "N/A");
                        productDetailList.add(singleProductDetailHM);
                    }

                }

            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return productDetailList;
    }

    //checks if there are any QCPOS terminals in the system that are running a version equal to or greater than the passed in terminalVersionToCheck (checks internal versions, not display versions)
    public Boolean checkForExistingTerminalVersion(String terminalVersionToCheck) {
        try {
            //split apart the terminalVersionToCheck string and determine the integer values for each piece
            String[] terminalVersionToCheckSplit = terminalVersionToCheck.split("\\.");
            Integer majorVersionToCheck =  Integer.parseInt(terminalVersionToCheckSplit[0]);
            Integer minorVersionToCheck =  Integer.parseInt(terminalVersionToCheckSplit[1]);
            Integer servicePackToCheck =  Integer.parseInt(terminalVersionToCheckSplit[2]);
            Integer buildToCheck =  Integer.parseInt(terminalVersionToCheckSplit[3]);

            //grab all the terminal versions running on this system and iterate through them
            ArrayList<HashMap> terminalVersionList = dm.serializeSqlWithColNames("data.common.getTerminalVersions",
                    new Object[]{},
                    false,
                    true
            );
            if (terminalVersionList != null) {
                for (HashMap terminalVersionHM : terminalVersionList) {
                    if (terminalVersionHM != null && terminalVersionHM.get("QCPOSINTERNALVERSION") != null) {
                        //split apart the internalTerminalVersion string and determine the integer values for each piece
                        String internalTerminalVersion = terminalVersionHM.get("QCPOSINTERNALVERSION").toString();
                        String[] internalTerminalVersionSplit = internalTerminalVersion.split("\\.");
                        Integer majorVersion =  Integer.parseInt(internalTerminalVersionSplit[0]);
                        Integer minorVersion =  Integer.parseInt(internalTerminalVersionSplit[1]);
                        Integer servicePack =  Integer.parseInt(internalTerminalVersionSplit[2]);
                        Integer build =  Integer.parseInt(internalTerminalVersionSplit[3]);

                        //if any of version in terminalVersionList is greater than or equal to the terminalVersionToCheck then return true
                        if (majorVersion <= majorVersionToCheck) { //major version is lesser or equal, continue checking
                            if (majorVersion.equals(majorVersionToCheck)) { //major version is equal, continue checking
                                if (minorVersion <= minorVersionToCheck) { //minor version is lesser or equal, continue checking
                                    if (minorVersion.equals(minorVersionToCheck)) { //minor version is equal, continue checking
                                        if (servicePack <= servicePackToCheck) { //service pack is lesser or equal, continue checking
                                            if (servicePack.equals(servicePackToCheck)) { //service pack is equal, continue checking
                                                if (build >= buildToCheck) { //major and minor versions and service packs are equal, build is greater OR equal, return true
                                                    return true;
                                                }
                                            }
                                        } else { //major and minor versions are equal, service pack is greater, return true
                                            return true;
                                        }
                                    }
                                } else {
                                    return true; //major version is equal, minor version is greater, return true
                                }
                            }
                        } else { //major version is greater, return true
                            return true;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return false;
    }

    //calls formatTime()
    public String formatDateTime(String dateTime, String inFormat, String outFormat) {
        return formatTime(dateTime, inFormat, outFormat);
    }

    //converts an exception to a well formatted string
    public static String convertExceptionToString(Exception ex) {
        String exceptionString = "N/A";
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            exceptionString = "ERROR: Caught exception: "+sw.toString();
        } catch (Exception innerEx) {
            Logger.logException(innerEx); //always log exceptions
        }
        return exceptionString;
    }

    //gets the last sync date time for the given accountID
    public String getLastSyncDateTimeForAccount(Integer accountID) { //BACKPORTED FROM 8.2 - COMMON WEB TRUNK
        String lastSynced;
        try {
            //determine the last time the logged in accounts's information was synced
            Object lastSyncedObj = dm.parameterizedExecuteScalar("data.accounts.getAccountLastSyncDateTime", new Object[] {accountID});

            if (lastSyncedObj != null) {
                lastSynced = lastSyncedObj.toString();

                //specify in and out formats for formatting lastSynced
                String dateTimeInFormat = "yyyy-MM-dd HH:mm:ss.S";
                String dateTimeOutFormat = "M/dd/yyyy h:mm:ss a"; //old way was - M/dd/yyyy H:mm:ss - jrmitaly 6/23/2015
                lastSynced = formatDateTime(lastSynced, dateTimeInFormat, dateTimeOutFormat);
            } else {
                lastSynced = "N/A";
                Logger.logMessage("Could not determine last sync date for account ID:"+accountID, Logger.LEVEL.WARNING);
            }

        } catch (Exception ex) {
            lastSynced = "N/A";
            Logger.logException(ex); //always log exceptions
        }
        return lastSynced;
    }

    //checks the accounts (employees) passed to see if there are enough account (employee) licenses and gift card licenses in the system for them
    public Boolean checkAccountLicenseAvailability(ArrayList<HashMap> accountsWithInfo) {
        Boolean licensesAvailable = false;
        try {
            Integer employeeLicensesRequested = 0;
            Integer giftCardLicensesRequested = 0;

            //determine how many totalAvailableEmployeeLicenses there are
            Integer totalAvailableEmployeeLicenses = Integer.parseInt(dm.parameterizedExecuteScalar("data.common.totalAvailEmployeeLicenses", new Object[] {}).toString());

            //determine how many totalEmployeeLicensesInUse there are
            Integer totalEmployeeLicensesInUse = Integer.parseInt(dm.parameterizedExecuteScalar("data.common.totalEmployeeLicensesInUse", new Object[] {}).toString());

            //check employee licenses based on sign up (returns -1 if this setting (IMPORTALLINACTIVE) is not turned in QC_Globals
            Integer employeeLicensesBasedOnSignUp = checkEmployeeLicensesBasedOnSignUp();
            if(employeeLicensesBasedOnSignUp != -1) {
                totalEmployeeLicensesInUse  = employeeLicensesBasedOnSignUp;
            }

            //check employee licenses based on use (returns -1 if this setting (EMPLICBSDONUSE) is not turned in QC_Globals
            Integer employeeLicensesBasedOnUse = checkEmployeeLicensesBasedOnUse();
            if (employeeLicensesBasedOnUse != -1) {
                totalEmployeeLicensesInUse = employeeLicensesBasedOnUse;
            }

            //determine how many totalAvailableGiftCardLicenses there are
            Integer totalAvailableGiftCardLicenses = Integer.parseInt(dm.parameterizedExecuteScalar("data.common.totalAvailGiftCardLicenses", new Object[] {}).toString());

            //determine how many totalGiftCardLicensesInUse there are
            Integer totalGiftCardLicensesInUse = Integer.parseInt(dm.parameterizedExecuteScalar("data.common.totalGiftCardLicensesInUse", new Object[] {}).toString());

            //iterate through all the accounts
            for (HashMap accountWithInfo : accountsWithInfo) {

                Integer accountID = null;
                Integer accountType = null;
                String accountStatus = null;

                if (accountWithInfo.get("ACCOUNTID") != null) {
                    accountID = Integer.parseInt(accountWithInfo.get("ACCOUNTID").toString());
                }
                if (accountWithInfo.get("ACCOUNTTYPEID") != null) {
                    if (accountWithInfo.get("ACCOUNTTYPEID").toString().isEmpty()) { //if empty just set it to 0
                        accountType = 0;
                    } else {
                        accountType = Integer.parseInt(accountWithInfo.get("ACCOUNTTYPEID").toString());
                    }
                }
                if (accountWithInfo.get("ACCOUNTSTATUS") != null) {
                    accountStatus = accountWithInfo.get("ACCOUNTSTATUS").toString();
                }

                //if all the data is valid for this account
                if (accountID != null && accountType != null && accountType > 0 && accountStatus != null && !accountStatus.isEmpty()) {
                    if (checkAccountStatusAndLicenseFlags(accountStatus)) {
                        if (!accountType.equals(4)) { //all other account types besides gift cards
                            employeeLicensesRequested++;
                            if ((employeeLicensesRequested + totalEmployeeLicensesInUse) > totalAvailableEmployeeLicenses) {
                                Logger.logMessage("Employee licenses requested: "+employeeLicensesRequested, Logger.LEVEL.DEBUG);
                                Logger.logMessage("Employee licenses in use: "+totalEmployeeLicensesInUse, Logger.LEVEL.DEBUG);
                                Logger.logMessage("Employee licenses available: "+totalAvailableEmployeeLicenses, Logger.LEVEL.DEBUG);
                                Logger.logMessage("Account (employee) license requested and was denied in commonMMHFunctions.checkLicenseAvailability() for accountID: "+accountID, Logger.LEVEL.WARNING);
                                return false;
                            }
                        } else {
                            giftCardLicensesRequested++;
                            if ((giftCardLicensesRequested + totalGiftCardLicensesInUse) > totalAvailableGiftCardLicenses) {
                                Logger.logMessage("Gift card licenses requested: "+giftCardLicensesRequested, Logger.LEVEL.DEBUG);
                                Logger.logMessage("Gift card licenses in use: "+totalGiftCardLicensesInUse, Logger.LEVEL.DEBUG);
                                Logger.logMessage("Gift card licenses available: "+totalAvailableGiftCardLicenses, Logger.LEVEL.DEBUG);
                                Logger.logMessage("Gift card license requested and was denied in commonMMHFunctions.checkLicenseAvailability() for accountID: "+accountID, Logger.LEVEL.WARNING);
                                return false;
                            }
                        }
                    }
                } else {
                    Logger.logMessage("Invalid account data found while checking licenses in commonMMHFunctions.checkLicenseAvailability()", Logger.LEVEL.ERROR);
                    return licensesAvailable;
                }
            }

            //double check
            for (HashMap accountWithInfo : accountsWithInfo) {
                Integer accountType = null;

                if (accountWithInfo.get("ACCOUNTTYPEID") != null) {
                    if (accountWithInfo.get("ACCOUNTTYPEID").toString().isEmpty()) { //if empty just set it to 0
                        accountType = 0;
                    } else {
                        accountType = Integer.parseInt(accountWithInfo.get("ACCOUNTTYPEID").toString());
                    }
                }
                if (!accountType.equals(4)) {
                    //double check - if the licenses requested plus all the existing licenses are greater than the total available licenses, then return false
                    if ((employeeLicensesRequested + totalEmployeeLicensesInUse) > totalAvailableEmployeeLicenses) {
                        Logger.logMessage("Employee licenses requested: "+employeeLicensesRequested, Logger.LEVEL.DEBUG);
                        Logger.logMessage("Employee licenses in use: "+totalEmployeeLicensesInUse, Logger.LEVEL.DEBUG);
                        Logger.logMessage("Employee licenses available: "+totalAvailableEmployeeLicenses, Logger.LEVEL.DEBUG);
                        Logger.logMessage("Account (employee) license requested and was denied in commonMMHFunctions.checkLicenseAvailability()", Logger.LEVEL.WARNING);
                        return false;
                    }
                }  else {
                    //double check - if the licenses requested plus all the existing licenses are greater than the total available licenses, then return false
                    if ((giftCardLicensesRequested + totalGiftCardLicensesInUse) > totalAvailableGiftCardLicenses) {
                        Logger.logMessage("Gift card licenses requested: "+giftCardLicensesRequested, Logger.LEVEL.DEBUG);
                        Logger.logMessage("Gift card licenses in use: "+totalGiftCardLicensesInUse, Logger.LEVEL.DEBUG);
                        Logger.logMessage("Gift card licenses available: "+totalAvailableGiftCardLicenses, Logger.LEVEL.DEBUG);
                        Logger.logMessage("Gift card license requested and was denied in commonMMHFunctions.checkLicenseAvailability()", Logger.LEVEL.WARNING);
                        return false;
                    }
                }
            }

            //all checks have passed, there are available licenses for the accounts passed
            return true;

        } catch (Exception ex) {
            licensesAvailable = false;
            Logger.logException(ex); //always log exceptions
        }
        return licensesAvailable;
    }

    //generates a random password for a person account in the cloud
    public String generateRandomPassword() {
        String password = "";

        try {
            int randomPasswordLength = (int) (Math.random() * (14 - 8)) + 8;

            String capitalChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            String lowerChars = "abcdefghijklmnopqrstuvwxyz";
            String digits = "0123456789";
            String specialChars = "!@#";

            for(int i=0; i<randomPasswordLength; i++) {
                int chooseChar = (int) (Math.random() * 4) + 1;
                Random random = new Random();

                switch (chooseChar) {
                    case 1:
                        password += capitalChars.charAt(random.nextInt(capitalChars.length()));
                        break;
                    case 2:
                        password += lowerChars.charAt(random.nextInt(lowerChars.length()));
                        break;
                    case 3:
                        password += digits.charAt(random.nextInt(digits.length()));
                        break;
                    case 4:
                        password += specialChars.charAt(random.nextInt(specialChars.length()));
                        break;
                    default:
                        password += capitalChars.charAt(random.nextInt(capitalChars.length()));
                        break;
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("ERROR: Could not create random password in PersonAccountDataHandler.generateRandomPassword()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return password;
    }

    private boolean checkAccountStatusAndLicenseFlags(String accountStatus){
        boolean result;
        // Method returns boolean based on accountStatus and Global License flags
        if(employeeLicenseBasedOnSignUp && !employeeLicenseBasedOnUse){
            // If ImportAllInactive = true ( ie employeeLicenseBasedOnSignUp = true)
            result = !accountStatus.equals("I") && !accountStatus.equals("W");
        } else {
            // Standard licensing
            result = !accountStatus.equals("I");
        }
        return result;
    }

    //check employee licenses based on use (returns -1 if this setting (EMPLICBSDONUSE) is not turned in QC_Globals
    public Integer checkEmployeeLicensesBasedOnUse() {
        Integer checkEmployeeLicensesBasedOnUse = -1; //means that licenses based on use is not configured
        try {
            Boolean empLicBasedOnUseFlag = Boolean.parseBoolean(dm.parameterizedExecuteScalar("data.common.getEmpLicBasedOnUseFlag", new Object[]{}).toString());
            employeeLicenseBasedOnUse = empLicBasedOnUseFlag;
            if (empLicBasedOnUseFlag) {
                checkEmployeeLicensesBasedOnUse = Integer.parseInt(dm.parameterizedExecuteScalar("data.common.totalEmployeeLicensesBasedOnUse", new Object[] {}).toString());
            }
        } catch (Exception ex) {
            checkEmployeeLicensesBasedOnUse = -1;
            Logger.logMessage("Exception caught in commonMMHFunctions.checkEmployeeLicensesBasedOnUse()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
        return checkEmployeeLicensesBasedOnUse;
    }

    //check employee licenses based on sign up (returns -1 if this setting (EMPLICBSDONUSE) is not turned in QC_Globals
    public Integer checkEmployeeLicensesBasedOnSignUp() {
        Integer checkEmployeeLicensesBasedOnSignUp = -1; //means that licenses based on sign up is not configured
        try {
            //determine what the importAllInactive flag is from QC_Globals
            Boolean importAllInactiveFlag = Boolean.parseBoolean(dm.parameterizedExecuteScalar("data.common.getImportAllInactiveFlag", new Object[]{}).toString());
            employeeLicenseBasedOnSignUp = importAllInactiveFlag;
            if(importAllInactiveFlag) {
                checkEmployeeLicensesBasedOnSignUp = Integer.parseInt(dm.parameterizedExecuteScalar("data.common.totalEmployeeLicensesBasedOnSignUp", new Object[] {}).toString());
            }
        } catch (Exception ex) {
            checkEmployeeLicensesBasedOnSignUp = -1;
            Logger.logMessage("Exception caught in commonMMHFunctions.checkEmployeeLicensesBasedOnUse()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
        return checkEmployeeLicensesBasedOnSignUp;
    }

    //check available licenses before activating the given userID
    public boolean getEmployeeLicenseAvailabilityByUserID(Integer userID) {
        HashMap accountWithInfo = new HashMap();
        Boolean validLicenses = false;

        try {
            ArrayList<HashMap> result = dm.parameterizedExecuteQuery("data.common.getAccountWithInfo",
                new Object[]{
                        userID
                },
                true
            );

            if (result != null && result.size() == 1) {
                HashMap resultHM = result.get(0);

                if (resultHM.get("AIP").toString().equals("I")) {
                    accountWithInfo.put("ACCOUNTSTATUS", 0);
                } else {
                    accountWithInfo.put("ACCOUNTSTATUS", resultHM.get("USERACCTACTIVE").toString());
                }
                accountWithInfo.put("ACCOUNTID", userID*-1);
                accountWithInfo.put("ACCOUNTTYPEID", resultHM.get("ACCOUNTTYPEID"));
            } else {
                Logger.logMessage("Could not determine user's account info AuthResourse.getLicenseAvailability.", Logger.LEVEL.ERROR);
            }

            //populate accountsWithInfoForLicenseAvailability to check if there are available licenses
            ArrayList<HashMap> accountsWithInfoForLicenseAvailability = new ArrayList<HashMap>();
            accountsWithInfoForLicenseAvailability.add(accountWithInfo);

            if(checkAccountLicenseAvailability(accountsWithInfoForLicenseAvailability)) {
                validLicenses = true;
            } else {
                Logger.logMessage("WARNING: Insufficient licenses AuthResource.getLicenseAvailability", Logger.LEVEL.WARNING);
            }
        }catch (Exception ex) {
            Logger.logMessage("Error in AuthResource.getLicenseAvailability", Logger.LEVEL.ERROR);
        }

        return validLicenses;
    }

    //checks the accounts (employees) spending profile to see if there is an advanced payment terminal associated with it
    public Boolean checkAccountsSpendingProfileForAdvancedTerminal(String accountID) {
        Boolean hasAdvancedTerminal = false;
        try {

            //grab the count of advanced terminals that this account's spending profile has associated with it
            Integer advancedTerminalCount = Integer.parseInt(dm.parameterizedExecuteScalar("data.common.checkAccountsSpendingProfileForAdvancedTerminal", new Object[]{accountID}).toString());

            //if there is at least one valid advanced terminal...
            if (advancedTerminalCount > 0) {
                hasAdvancedTerminal = true;
            }

        } catch (Exception ex) {
            hasAdvancedTerminal = false;
            Logger.logException(ex); //always log exceptions
        }
        return hasAdvancedTerminal;
    }

    //get the Account and Person Account alias names set in the Global Settings
    public ArrayList<HashMap> getAccountAliases() {
        ArrayList<HashMap> aliases = new ArrayList<>();
        try {
            aliases = dm.parameterizedExecuteQuery("data.common.getAccountAliases", new Object[]{}, true);

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return aliases;
    }

    //checks the QC POS terminals passed to see if there are enough QC POS licenses (POSAnyTermsLicensed) in the system for them
    public ArrayList<HashMap> checkMMHayesPOSTerminalLicenseAvailability(ArrayList<HashMap> terminalsWithInfo) {
        ArrayList<HashMap> licensesAvailable = new ArrayList<>();
        HashMap results = new HashMap();
        try {

            Integer terminalLicensesRequested = 0;

            //determine how many totalAvailableMMHayesPOSTerminalLicenses there are
            Integer totalAvailableMMHayesPOSTerminalLicenses = Integer.parseInt(dm.parameterizedExecuteScalar("data.common.totalAvailMMHayesPOSTerminalLicenses", new Object[]{}).toString());

            //determine how many totalTerminalMMHayesLicensesInUse there are
            Integer totalTerminalMMHayesLicensesInUse = Integer.parseInt(dm.parameterizedExecuteScalar("data.common.totalMMHayesPOSTerminalLicensesInUse", new Object[] {}).toString());

            //iterate through all the terminals
            for (HashMap terminalWithInfo : terminalsWithInfo) {

                Integer terminalID = null;
                Integer terminalModelID = null;
                Integer terminalType = null;
                String terminalStatus = null;

                if (terminalWithInfo.get("TERMINALID") != null) {
                    terminalID = Integer.parseInt(terminalWithInfo.get("TERMINALID").toString());
                }
                if (terminalWithInfo.get("TERMINALTYPEID") != null) {
                    if (terminalWithInfo.get("TERMINALTYPEID").toString().isEmpty()) { //if empty just set it to 0
                        terminalType = 0;
                    } else {
                        terminalType = Integer.parseInt(terminalWithInfo.get("TERMINALTYPEID").toString());
                    }
                }
                if (terminalWithInfo.get("TERMINALSTATUS") != null) {
                    terminalStatus = terminalWithInfo.get("TERMINALSTATUS").toString();
                }
                if (terminalWithInfo.get("TERMINALMODELID") != null) {
                    terminalModelID = Integer.parseInt(terminalWithInfo.get("TERMINALMODELID").toString());
                }

                //if all the data is valid for this terminal
                if (terminalID != null && terminalType != null && terminalType > 0 && terminalStatus != null && !terminalStatus.isEmpty()) {
                    if ((terminalStatus.equals("1") || terminalStatus.equals("true")) && terminalType.equals(5) && !terminalModelID.equals(107)) { //terminal type 5 is "MMHayes POS" (QC POS) and terminal model 107 is "QC POS Kiosk"
                        terminalLicensesRequested++;
                        if ((terminalLicensesRequested + totalTerminalMMHayesLicensesInUse) > totalAvailableMMHayesPOSTerminalLicenses) {
                            Logger.logMessage("MMHayes POS Terminal license requested and was denied in commonMMHFunctions.checkMMHayesPOSTerminalLicenseAvailability() for terminalID: "+terminalID, Logger.LEVEL.WARNING);
                            results.put("status","failure");
                            results.put("message","Cannot complete the requested operation. Exceeded MMHayes POS Terminal license count.");
                            licensesAvailable.add(results);
                            return licensesAvailable;
                        }
                    }
                } else {
                    Logger.logMessage("Invalid terminal data found while checking licenses in commonMMHFunctions.checkMMHayesPOSTerminalLicenseAvailability()", Logger.LEVEL.ERROR);
                    results.put("status", "failure");
                    results.put("message", "Invalid terminal configuration.  Please complete required fields for terminal.");
                    licensesAvailable.add(results);
                    return licensesAvailable;
                }
            }

            //double check - if the licenses requested plus all the existing licenses are greater than the total available licenses, then return false
            if ((terminalLicensesRequested + totalTerminalMMHayesLicensesInUse) > totalAvailableMMHayesPOSTerminalLicenses) {
                Logger.logMessage("MMHayes POS Terminal license requested and was denied in commonMMHFunctions.checkMMHayesPOSTerminalLicenseAvailability()", Logger.LEVEL.WARNING);
                results.put("status","failure");
                results.put("message", "Cannot complete the requested operation. Exceeded MMHayes POS terminal license count.");
                licensesAvailable.add(results);
                return licensesAvailable;
            }

            //all checks have passed, there are available licenses for the terminals passed
            results.put("status","success");
            licensesAvailable.add(results);

        } catch (Exception ex) {
            //licensesAvailable = false;
            results.put("status", "failure");
            results.put("message", "Cannot complete the requested operation. Error encountered while checking MMHayes POS terminal licensing.");
            licensesAvailable.add(results);
            Logger.logException(ex); //always log exceptions
        }
        return licensesAvailable;
    }

    //checks for validity in changes to terminals terminalModelIDs against available licenses
    public ArrayList<HashMap> checkTerminalLicenses(ArrayList<HashMap> changedTerminalList) {
        ArrayList<HashMap> licensesAvailable = new ArrayList<>();
        HashMap results = new HashMap();

        if ( changedTerminalList == null || changedTerminalList.size() == 0 ) {
            results.put("status", "success");
            return licensesAvailable;
        }

        try {
            //SQL - get licenseList with info TerminalModelID, NumberAvailable
            ArrayList<HashMap> licenseList = dm.parameterizedExecuteQuery("data.common.getAllLicenseLimits", new Object[] {}, true);

            //convert licenseList to licenseHM HashMap<TerminalModelID, LicenseInfoHM> w/ LicenseInfoHM having keys currentlyInUse (0) and totalAvailable
            HashMap<Integer, HashMap> licenseHM = new HashMap<>();
            for ( HashMap license : licenseList ) {
                if ( license != null && license.containsKey("TERMINALMODELID") && license.containsKey("VALIDATIONSTATUSID") && CommonAPI.convertModelDetailToInteger(license.get("VALIDATIONSTATUSID")) == 1 ) {
                    license.put("CURRENTLYINUSE", 0);
                    licenseHM.put(CommonAPI.convertModelDetailToInteger(license.get("TERMINALMODELID")), license);
                }
            }

            //SQL - get terminalList of all terminals with info TerminalID, Model, Active
            ArrayList<HashMap> terminalList = dm.parameterizedExecuteQuery("data.common.getAllTerminalInfo", new Object[] {}, true);

            //convert changedTerminalList to changedTerminalHM HashMap<TerminalID,TerminalInfoHM>
            HashMap<Integer, HashMap> changedTerminalsHM = new HashMap<>();
            for ( HashMap changedTerminalHM : changedTerminalList ) {
                if ( changedTerminalHM != null && changedTerminalHM.containsKey("TERMINALID") ) {
                    Integer terminalID = CommonAPI.convertModelDetailToInteger(changedTerminalHM.get("TERMINALID"));

                    if ( terminalID < 0 ) {
                        terminalList.add(changedTerminalHM);
                        continue;
                    }

                    changedTerminalsHM.put(terminalID, changedTerminalHM);
                }
            }

            //combine db terminal list with changed data
            for ( HashMap terminalHM : terminalList ) {
                //if terminalID exists in changedTerminalHM
                Integer terminalID = CommonAPI.convertModelDetailToInteger(terminalHM.get("TERMINALID"));

                //if this terminal exists in the set of changed data
                if (terminalID > 0 && changedTerminalsHM.containsKey(terminalID)) {
                    //update terminalHM accordingly
                    HashMap changedTerminalHM = changedTerminalsHM.get(terminalID);
                    terminalHM.putAll(changedTerminalHM);
                }
            }

            //get and check the final counts for the new quantities of terminal licenses requested
            for ( HashMap terminalHM : terminalList ) {
                //if enabled
                if ( (terminalHM.containsKey("ENABLED") && CommonAPI.convertModelDetailToBoolean(terminalHM.get("ENABLED"))) || (terminalHM.containsKey("TERMINALSTATUS") && CommonAPI.convertModelDetailToInteger(terminalHM.get("TERMINALSTATUS")) == 1) ) {
                    Integer terminalModelID = CommonAPI.convertModelDetailToInteger(terminalHM.get("TERMINALMODELID"));

                    HashMap modelLicenseHM = licenseHM.get(terminalModelID);

                    //no mmh_license record, check if we must check this model or not
                    if ( modelLicenseHM == null ) {
                        //TODO: all models that cannot be missing need to go here OR all models need a mmh_license record
                        if ( terminalModelID != null && terminalModelID == ONLINE_ORDERING_TERMINAL_MODEL && isTerminalModelInChangedList(terminalModelID, changedTerminalList)) {
                            results.put("status","failure");
                            results.put("message","Cannot complete the requested operation. Missing license for Online Ordering terminals.");
                            licensesAvailable.add(results);
                            return licensesAvailable;

                        } else if ( terminalModelID != null && terminalModelID == MY_QC_FUNDING_TERMINAL_MODEL && isTerminalModelInChangedList(terminalModelID, changedTerminalList)) {
                            results.put("status","failure");
                            results.put("message","Cannot complete the requested operation. Missing license for My QC Funding terminals.");
                            licensesAvailable.add(results);
                            return licensesAvailable;

                        } else if ( terminalModelID != null && terminalModelID == QC_POS_KIOSK_TERMINAL_MODEL && isTerminalModelInChangedList(terminalModelID, changedTerminalList)) {
                            results.put("status","failure");
                            results.put("message","Cannot complete the requested operation. Missing license for QC POS Kiosk terminals.");
                            licensesAvailable.add(results);
                            return licensesAvailable;
                        }
                        //no licenses being considered for this terminalModelID
                        continue;
                    }

                    Integer inUse = CommonAPI.convertModelDetailToInteger(modelLicenseHM.get("CURRENTLYINUSE"));
                    Integer totalAvailable = CommonAPI.convertModelDetailToInteger(modelLicenseHM.get("NUMBERVALUE"));

                    ++inUse;

                    //fail if currentlyInUse > totalAvailable for this terminalModelID
                    //TODO: take out check for specific terminalModelID for generic use
                    if ( terminalModelID != null && totalAvailable < inUse && terminalModelID == ONLINE_ORDERING_TERMINAL_MODEL ) {
                        results.put("status", "failure");
                        results.put("message", "Cannot complete the requested operation. Exceeded license count for Online Ordering terminals.");
                        licensesAvailable.add(results);
                        return licensesAvailable;

                    } else if ( terminalModelID != null && totalAvailable < inUse && terminalModelID == MY_QC_FUNDING_TERMINAL_MODEL ) {
                        results.put("status", "failure");
                        results.put("message", "Cannot complete the requested operation. Exceeded license count for My QC Funding terminals.");
                        licensesAvailable.add(results);
                        return licensesAvailable;

                    } else if ( terminalModelID != null && totalAvailable < inUse && terminalModelID == QC_POS_KIOSK_TERMINAL_MODEL ) {
                        results.put("status", "failure");
                        results.put("message", "Cannot complete the requested operation. Exceeded license count for QC POS Kiosk terminals.");
                        licensesAvailable.add(results);
                        return licensesAvailable;
                    }


                    //update currentlyInUse for this terminalModelID
                    modelLicenseHM.put("CURRENTLYINUSE", inUse);
                }
            }

            //no failures
            results.put("status", "success");
            licensesAvailable.add(results);

        } catch (Exception ex) {
            Logger.logException(ex);
            results.put("status","failure");
            results.put("message", "Error checking terminal licenses.");
            licensesAvailable.add(results);
        }

        return licensesAvailable;
    }

    public Boolean isTerminalModelInChangedList(Integer terminalModelID, ArrayList<HashMap> changedTerminalList) {
        for ( HashMap changedTerminalHM : changedTerminalList ) {
            Integer changedTerminalModelID = CommonAPI.convertModelDetailToInteger(changedTerminalHM.get("TERMINALMODELID"));
            if(changedTerminalModelID != null && changedTerminalModelID.equals(terminalModelID)) {
                return true;
            }
        }

        return false;
    }

    //checks if the given deductionProfileID has a grouping method of "Individual Transactions" and deduction amount method of "Level Payments"
    public Boolean checkDeductionProfileForIndivTranLvlPay(String deductionProfileID) {
        Boolean deductionProfileHasIndivTranLvlPay;
        try {

            //determine if the given deductionProfileID has a grouping method of "Individual Transactions" and deduction amount method of "Level Payments"
            deductionProfileHasIndivTranLvlPay = (Integer.parseInt(
                    dm.parameterizedExecuteScalar("data.common.checkDeductionProfileForIndivTranLvlPay",
                    new Object[] {
                        deductionProfileID
                    }
             ).toString()) == 1);

        } catch (Exception ex) {
            deductionProfileHasIndivTranLvlPay = false;
            Logger.logException(ex); //always log exceptions
        }
        return deductionProfileHasIndivTranLvlPay;
    }

//START - COMMON TOS (Terms of Service) Methods

    //checks if the system is configured to require Terms of Service for the given account group and spending profile
    public Boolean checkIfTOSIsRequired(Integer accountGroupID, Integer spendingProfileID) {
        Boolean isTOSRequired;
        try {
            //determine if either the accountGroupID or spendingProfileID has an associated TOS
            isTOSRequired = (Integer.parseInt(dm.parameterizedExecuteScalar("data.common.checkIfTOSIsRequired",
                new Object[] {
                    accountGroupID,
                    spendingProfileID
                }
            ).toString()) >= 1);
        } catch (Exception ex) {
            isTOSRequired = false;
            Logger.logException(ex); //always log exceptions
        }
        return isTOSRequired;
    }

    //checks if the user already accepted the TOS for this account group and spending profile
    public Boolean checkIfUserAlreadyAcceptedTOS(String employeeID, Integer accountGroupID, Integer spendingProfileID) {
        Boolean userAcceptTOS;
        try {
            //determine if either the accountGroupID or spendingProfileID has an associated TOS
            userAcceptTOS = (Integer.parseInt(dm.parameterizedExecuteScalar("data.common.checkIfUserAlreadyAcceptedTOS",
                    new Object[] {
                            employeeID,
                            accountGroupID,
                            spendingProfileID
                    }
            ).toString()) >= 1);
        } catch (Exception ex) {
            userAcceptTOS = false;
            Logger.logException(ex); //always log exceptions
        }
        return userAcceptTOS;
    }

    //checks if the user already accepted the TOS for this account group and spending profile
    public Boolean lastTOSAcceptedWasNotThisTOS(String employeeID, Integer accountGroupID, Integer spendingProfileID) {
        Boolean lastTOSWasNotThis;
        try {
            //determine if either the accountGroupID or spendingProfileID has an associated TOS
            lastTOSWasNotThis = (Integer.parseInt(dm.parameterizedExecuteScalar("data.common.lastTOSAcceptedWasNotThisTOS",
                    new Object[] {
                            employeeID,
                            accountGroupID,
                            spendingProfileID
                    }
            ).toString()) >= 1);
        } catch (Exception ex) {
            lastTOSWasNotThis = false;
            Logger.logException(ex); //always log exceptions
        }
        return lastTOSWasNotThis;
    }

    //builds an array list of hashmaps of TOS(s) the user needs to accept for the given account (employee) - this is replacing determineAccountTOS. See defect #1696 - gematuszyk 5/10/18
    public ArrayList<HashMap> getRequiredTOS(String accountGroupID, String spendingProfileID) {
        ArrayList<HashMap> tosList = null;
        try {
            tosList = dm.parameterizedExecuteQuery("data.common.getRequiredTOS",
                    new Object[]{
                            accountGroupID,
                            spendingProfileID
                    },
                    true
            );

            //if the size of the tosList is 0, then just set it to null because we didn't find anything
            if (tosList != null && tosList.size() == 0) {
                tosList = null;
            }

        } catch (Exception ex) {
            Logger.logMessage("Could not find TOS for account  group and spending profile BUT account has an account status of W in commonMMHFunctions.getRequiredTOS()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }
        return tosList;
    }

    //builds an array list of hashmaps of TOS(s) the user needs to accept for the given account (employee)
    public ArrayList<HashMap> determineAccountTOS(Integer accountID) {
        ArrayList<HashMap> tosList = null;
        try {
            if (accountID != null) {
                //only find TOS if the user's account status is "W" (for waiting for approvals)
                if (checkAccountStatus(accountID).equals("W")) {

                    Logger.logMessage("Determined that accountID ("+accountID+") needs a TOS. Attempting to find appropriate TOS.", Logger.LEVEL.DEBUG);

                    //first check if there is a match for both spending profile and account group
                    tosList = checkTOSSpendingProfileAndAccountGroup(accountID);

                    if (tosList == null) { //no match there, try the next

                        //second check if there is a match for JUST the spending profile
                        tosList = checkTOSSpendingProfile(accountID);

                        if (tosList == null) { //no match there, try the next

                            //lastly check if there is a match for JUST the account group
                            tosList = checkTOSPayrollGroup(accountID);

                        }
                    }

                    if (tosList == null || tosList.size() <= 0) {//check one more time now that we have tried "everything" to find a TOS for this account (employee)
                        Logger.logMessage("Could not find TOS for account BUT account has an account status of W in commonMMHFunctions.determineAccountTOS()", Logger.LEVEL.ERROR);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return tosList;
    }

    //get the account status of the currently logged in account (employee)
    public String checkAccountStatus(Integer accountID) {
        String accountStatus = null;
        try {
            Object employeeAccountStatusObj = dm.parameterizedExecuteScalar("data.common.checkAccountStatus", new Object[] {accountID});
            accountStatus = employeeAccountStatusObj.toString();
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return accountStatus;
    }

    //check if there is a TOS match for both this account's (employee's) account group and spending profile
    public ArrayList<HashMap> checkTOSSpendingProfileAndAccountGroup(Integer accountID) {
        ArrayList<HashMap> tosList = null;
        try {
            tosList = dm.parameterizedExecuteQuery("data.common.checkTOSSpendingProfileAndAccountGroup",
                    new Object[]{
                        accountID.toString()
                    },
                    true
            );

            //if the size of the tosList is 0, then just set it to null because we didn't find anything
            if (tosList != null && tosList.size() == 0) {
                tosList = null;
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return tosList;
    }

    //check if there is a TOS match for JUST this account's (employee's) spending profile (terminal profile)
    public ArrayList<HashMap> checkTOSSpendingProfile(Integer accountID) {
        ArrayList<HashMap> tosList = null;
        try {
            tosList = dm.parameterizedExecuteQuery("data.common.checkTOSSpendingProfile",
                    new Object[]{
                            accountID.toString()
                    },
                    true
            );

            //if the size of the tosList is 0, then just set it to null because we didn't find anything
            if (tosList != null && tosList.size() == 0) {
                tosList = null;
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return tosList;
    }

    //check if there is a TOS match for JUST this account's (employee's) account group (payroll group)
    public ArrayList<HashMap> checkTOSPayrollGroup(Integer accountID) {
        ArrayList<HashMap> tosList = null;
        try {
            tosList = dm.parameterizedExecuteQuery("data.common.checkTOSAccountGroup",
                    new Object[]{
                        accountID.toString()
                    },
                    true
            );

            //if the size of the tosList is 0, then just set it to null because we didn't find anything
            if (tosList != null && tosList.size() == 0) {
                tosList = null;
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return tosList;
    }

//END - COMMON TOS (Terms of Service) Methods

    //determines the IP address of the end user (use this instead of simply checking getRemoteAddr() !!)
    public String getEndUserIPAddress(HttpServletRequest request) {

        Logger.logMessage("Attempting to determine end user IP Address in commonMMHFunctions.getEndUserIPAddress()", Logger.LEVEL.DEBUG);

        //string array of various HTTP Headers that *may* have the end user's IP Address
        String[] variousIPAddressHTTPHeaders = {
                "X-Forwarded-For",
                "Proxy-Client-IP",
                "WL-Proxy-Client-IP",
                "HTTP_X_FORWARDED_FOR",
                "HTTP_X_FORWARDED",
                "HTTP_X_CLUSTER_CLIENT_IP",
                "HTTP_CLIENT_IP",
                "HTTP_FORWARDED_FOR",
                "HTTP_FORWARDED",
                "HTTP_VIA",
                "REMOTE_ADDR" };

        //iterate through the various HTTP headers to see if we find anything useful
        for (String header : variousIPAddressHTTPHeaders) {
            String ipFromHeader = request.getHeader(header);
            if (ipFromHeader != null && ipFromHeader.length() != 0 && !"unknown".equalsIgnoreCase(ipFromHeader)) {

                if (header.equals("X-Forwarded-For") || header.equals("HTTP_X_FORWARDED_FOR") || header.equals("HTTP_FORWARDED_FOR")) {
                    //parse on "," if it exists
                    if (ipFromHeader.contains(",")) {
                        String[] ipFromHeaderSplit = ipFromHeader.split(",");
                        ipFromHeader = ipFromHeaderSplit[0]; //the client's IP should be the first in the list
                    }
                }

                Logger.logMessage("Determined end user IP Address("+ipFromHeader+") from HTTP Header: "+header, Logger.LEVEL.DEBUG);
                return ipFromHeader;
            }
        }

        //all else failed - return last known IP address (could be wrong!)
        String ipFromRemoteAddr = request.getRemoteAddr();
        Logger.logMessage("Determined end user IP Address("+ipFromRemoteAddr+") from request.getRemoteAddr()", Logger.LEVEL.WARNING);
        return ipFromRemoteAddr;
    }

    //this retrieves VALID authentication types -jrmitaly 5/20/2016
    public ArrayList<HashMap> getAccountImportDefaultAuthTypes() {
        return getAccountImportDefaultAuthTypes("AUTHENTICATIONTYPEID");
    }

    //this retrieves VALID authentication types -jrmitaly 5/20/2016
    public ArrayList<HashMap> getAccountImportDefaultAuthTypes(String globalAuthTypeAlias) {
        ArrayList<HashMap> validAuthTypes = new ArrayList<>();
        try {
            validAuthTypes = dm.serializeSqlWithColNames("data.common.getValidAuthTypes", new Object[]{globalAuthTypeAlias});
        } catch (Exception ex) {
            Logger.logException(ex);
        }
        return validAuthTypes;
    }

    public void setLoadedFirstPage(boolean loadedFirstPage) {
        // Sets whether or not the first page has been loaded
        this.loadedFirstPage = loadedFirstPage;
    }

    public boolean getLoadedFirstPage() {
        // Returns true if the first page has already been loaded, and false otherwise
        return loadedFirstPage;
    }

    public String getLoginMessage() {
        // Retrieve the login message from the globals table
        // If there is no login message set, an empty string will be retrieved

        // default the login message to an empty message
        String loginMessage = "";

        try {
            // retrieve the login message from the globals table
            ArrayList<HashMap> loginMessageHms = new DynamicSQL("data.common.getLoginMessage").serialize(dm);
            if (loginMessageHms.size() > 0) {
                HashMap loginMessageHm = loginMessageHms.get(0);
                loginMessage = loginMessageHm.getOrDefault("LOGINMESSAGE", "").toString();
                loginMessage =  ESAPI.encoder().decodeForHTML(loginMessage);
                loginMessage = loginMessage.replaceAll("_lt_", "<");
                loginMessage = loginMessage.replaceAll("_gt_", ">");
            }
        }
        catch (Exception ex) {
            Logger.logMessage("Error in method commonMMHFunctions.getLoginMessage getting the login message", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return loginMessage;
    }

/* END - additional common methods */

/* START - security and sanitization methods */

    //sanitize string value with a myriad of replaces with regular expressions - jrmitaly 7/25/2017
    public static String sanitizeStringFromXSS(String value) {

        try {
            if (value != null) {

                // Avoid anything between greater than and less than
                Pattern scriptPattern = Pattern.compile("/<(.*)>/", Pattern.CASE_INSENSITIVE);
                value = scriptPattern.matcher(value).replaceAll("");

                // Avoid anything between script tags
                scriptPattern = Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE);
                value = scriptPattern.matcher(value).replaceAll("");

                // Avoid anything in a src='...' type of expression
                scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll("");

                scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll("");

                // Remove any lonesome </script> tag
                scriptPattern = Pattern.compile("</script>", Pattern.CASE_INSENSITIVE);
                value = scriptPattern.matcher(value).replaceAll("");

                // Remove any lonesome <script ...> tag
                scriptPattern = Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll("");

                // Avoid eval(...) expressions
                scriptPattern = Pattern.compile("eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll("");

                // Avoid expression(...) expressions
                scriptPattern = Pattern.compile("expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll("");

                // Avoid javascript:... expressions
                scriptPattern = Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE);
                value = scriptPattern.matcher(value).replaceAll("");

                // Avoid vbscript:... expressions
                scriptPattern = Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE);
                value = scriptPattern.matcher(value).replaceAll("");

                // Avoid onload= expressions
                scriptPattern = Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll("");

                // Avoid onerror= expressions
                scriptPattern = Pattern.compile("onerror(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll("");

                //added additional measure to replace greater than and less than chars - 7/25/2017 - jrmitaly
                value = value.replaceAll("<", "_lt_");
                value = value.replaceAll(">", "_gt_");

            }

        } catch (Exception ex) {
            Logger.logMessage("Could not sanitize string from XSS due to some exception", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

        return value;
    }

    //sanitize string value with a myriad of replaces with regular expressions - jrmitaly 7/25/2017
    public static String sanitizeStringFromXSSWithESAPI(String value) {
        try {

            //sanitize string value with a myriad of replaces with regular expressions - jrmitaly 7/25/2017
            value = sanitizeStringFromXSS(value);

            //finally encode anything left over for HTML
            value = ESAPI.encoder().encodeForHTML(value);

        } catch (Exception ex) {
            Logger.logMessage("Could not sanitize string from XSS with ESAPI due to some exception", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

        return value;
    }

    //sanitize string value with a number of replaces with regular expressions.  Replace the malicious string with a replacement value.  Pass in a replacement value or a default will be assigned - eglundin 07/01/2019
    public static String sanitizeStringFromXSS(String value, String replacementValue) {

        try {
            if (value != null && !value.isEmpty()) {

                if (replacementValue == null){
                    replacementValue = "POTENTIALLY MALICIOUS DATA REMOVED";
                }

                // Avoid anything between greater than and less than
                Pattern scriptPattern = Pattern.compile("/<(.*)>/", Pattern.CASE_INSENSITIVE);
                value = scriptPattern.matcher(value).replaceAll(replacementValue);

                // Avoid anything between script tags
                scriptPattern = Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE);
                value = scriptPattern.matcher(value).replaceAll(replacementValue);

                // Avoid anything in a src='...' type of expression
                scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll(replacementValue);

                scriptPattern = Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll(replacementValue);

                // Remove any lonesome </script> tag
                scriptPattern = Pattern.compile("</script>", Pattern.CASE_INSENSITIVE);
                value = scriptPattern.matcher(value).replaceAll(replacementValue);

                // Remove any lonesome <script ...> tag
                scriptPattern = Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll(replacementValue);

                // Avoid eval(...) expressions
                scriptPattern = Pattern.compile("eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll(replacementValue);

                // Avoid expression(...) expressions
                scriptPattern = Pattern.compile("expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll(replacementValue);

                // Avoid javascript:... expressions
                scriptPattern = Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE);
                value = scriptPattern.matcher(value).replaceAll(replacementValue);

                // Avoid vbscript:... expressions
                scriptPattern = Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE);
                value = scriptPattern.matcher(value).replaceAll(replacementValue);

                // Avoid onload= expressions
                scriptPattern = Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll(replacementValue);

                // Avoid onerror= expressions
                scriptPattern = Pattern.compile("onerror(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
                value = scriptPattern.matcher(value).replaceAll(replacementValue);

                //added additional measure to replace greater than and less than chars - 7/25/2017 - jrmitaly
                value = value.replaceAll("<", "_lt_");
                value = value.replaceAll(">", "_gt_");

            }

        } catch (Exception ex) {
            Logger.logMessage("Could not sanitize string from XSS due to some exception", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

        return value;
    }

    /**
     * Replaces all occurences of potentially harmful strings with a sanitized replacement value
     * CAUTION: this method is very strict. If you need to validate a string to that you know
     *      will not pass this sanitization, YOU SHOULD PARAMETERIZE THE QUERY!
     *      This ONLY allows alphanumeric characters and the ability to specify a TableName.ColumnName for
     *      use in a dynamic query.
     * @param value a raw, potentially dangerous user supplied string
     * @param replacementValue the string to be replaced in the raw value when malitious items are found
     * @return a string that is SAFE to be inserted into a dynamic sql statement
     */
    public static String sanitizeStringFromSQLInjection(String value, String replacementValue){
        try {
            if (value != null && !value.isEmpty()) {

                if (replacementValue == null){
                    replacementValue = "POTENTIALLY MALICIOUS DATA REMOVED";
                }

                //BEGIN--------------------Application Specific WhiteList-------------------------------

                //We will only allow alphanumeric characters, spaces, and . _ $
                //This will allow us to still specify table/column names, but not escape syntax
                value = value.replaceAll("[^a-z^A-Z^0-9^\\.^_^\\$^\\s]", replacementValue);

                //END--------------------Application Specific WhiteList-------------------------------

                //BEGIN--------------------Specific recommended blacklist-------------------------------
                //the blacklist is less strict than the whitelist, so there is no point
//                //Avoid the query delimiter ';'
//                value = value.replaceAll(";", replacementValue);
//
//                //Avoid the character data string delimiter '--'
//                value = value.replaceAll("--", replacementValue);
//
//                //Avoid the character data string delimiter "'"
//                value = value.replaceAll("'", "''"); //TODO: this might break searching for certain items
//
//                //Avoid the character comment delimiters "/* ... */"
//                Pattern scriptPattern = Pattern.compile("/\\*.*\\*/|/\\*|\\*/", Pattern.CASE_INSENSITIVE);
//                value = scriptPattern.matcher(value).replaceAll(replacementValue);
//
//                //Avoid catalog-extended stored procedures
//                Pattern scriptPattern = Pattern.compile("\\s+xp_", Pattern.CASE_INSENSITIVE);
//                value = scriptPattern.matcher(value).replaceAll(replacementValue);
//
//                //Escape wildcards for LIKE clauses
//                value = value.replaceAll("\\[", "[[]");
//                value = value.replaceAll("%", "[%]");
//                value = value.replaceAll("_", "[_]");

                //END--------------------Specific recommended blacklist-------------------------------
            }

        } catch (Exception ex) {
            Logger.logMessage("Could not sanitize string from SQLI due to some exception", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            value = "";
        }

        return value;
    }
/* END - security and sanitization methods */

/* START - Create "Label Printing Jobs" (for Direct to Label Printer) Methods */

    //creates a label print job from an existing inventory batch
    public String createLabelPrintJobsFromBatch(Integer printerID, Integer batchID, HttpServletRequest request) {
        Logger.logMessage("Formatting batch ID# '"+batchID+" to label print job format...", Logger.LEVEL.DEBUG);
        ArrayList<HashMap> productList = new ArrayList<>();
        Integer userID = null;
        try {

            //get logged in userID
            userID = Integer.parseInt(checkSession(request));

            if (batchID != null) {

                //grab getBatchDetailProductIDs for the passed in batchID
                productList = dm.parameterizedExecuteQuery("data.common.getBatchProductsForLabelPrinting",
                        new Object[]{
                                batchID
                        },
                        true
                );

            } else {
                Logger.logMessage("ERROR: Could not find valid batchID in createLabelPrintJobsFromBatch()...", Logger.LEVEL.ERROR);
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            productList = null;
        }
        return createLabelPrintJob(printerID, productList, userID);
    }

    //creates a label print job from a comma separated list pf products
    public String createLabelPrintJobFromList(Integer printerID, String productCSList, Integer labelsPerProduct, HttpServletRequest request) {
        ArrayList<HashMap> productList = new ArrayList<>();
        Integer userID = null;
        try {
            if (productCSList != null) {

                //get logged in userID
                userID = Integer.parseInt(checkSession(request));

                //split comma separated list into a list of strings
                List<String> productListOfStr = Arrays.asList(productCSList.split("\\s*,\\s*"));

                //iterate through the array list of strings and convert them to integers
                for (String productAsStr : productListOfStr) {
                    HashMap productHM = new HashMap();
                    productHM.put("PRODUCTID", Integer.parseInt(productAsStr));
                    productHM.put("LABELSPERPRODUCT", labelsPerProduct);
                    productList.add(productHM);
                }

            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            productList = null;
        }
        return createLabelPrintJob(printerID, productList, userID);
    }

    //creates a label print job for a single product
    public String createLabelPrintJobForSingleProduct(Integer printerID, Integer productID, Integer labelsPerProduct, Integer userID) {
        ArrayList<HashMap> productList = new ArrayList<>();
        try {
            HashMap productHM = new HashMap();
            productHM.put("PRODUCTID", productID);
            productHM.put("LABELSPERPRODUCT", labelsPerProduct);
            productList.add(productHM);
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            productList = null;
        }
        return createLabelPrintJob(printerID, productList, userID);
    }

    //creates label print job from an array list of product identifiers
    public String createLabelPrintJob(Integer printerID, ArrayList<HashMap> productList, Integer userID) {
        Logger.logMessage("Attempting to create label print job...", Logger.LEVEL.DEBUG);
        String jobCreated = "failed";
        try {

            //ensure we have the data we need to create a label print job
            if (printerID != null && productList != null && productList.size() > 0 && userID != null) {

                //create label printer job header
                String labelPrintJobSQL = dm.getSQLProperty("data.common.createLabelPrintJob", new Object[]{});
                ArrayList<String> labelPrintJobValues = new ArrayList<>();
                labelPrintJobValues.add(0, printerID.toString());
                labelPrintJobValues.add(1, userID.toString());
                ArrayList<HashMap> labelPrintJobResult  = dm.insertPreparedStatement(labelPrintJobSQL, labelPrintJobValues);
                Integer labelPrintJobID = Integer.parseInt(labelPrintJobResult.get(0).get("LASTRECORDID").toString());

                //error check
                if (labelPrintJobID <= 0 ) {
                    Logger.logMessage("Could not determine a label print job header ID...", Logger.LEVEL.ERROR);
                    return jobCreated;
                }

                //iterate through product identifiers and create label print job details
                for (HashMap productHM : productList) {

                    //create label print job detail
                    String labelPrintJobDetailSQL = dm.getSQLProperty("data.common.createLabelPrintJobDetail", new Object[]{});
                    ArrayList<String> labelPrintJobDetailValues = new ArrayList<>();
                    labelPrintJobDetailValues.add(0, labelPrintJobID.toString());
                    labelPrintJobDetailValues.add(1, productHM.get("PRODUCTID").toString());
                    labelPrintJobDetailValues.add(2, productHM.get("LABELSPERPRODUCT").toString());
                    ArrayList<HashMap> labelPrintJobDetailResult  = dm.insertPreparedStatement(labelPrintJobDetailSQL, labelPrintJobDetailValues);
                    Integer labelPrintJobDetailID = Integer.parseInt(labelPrintJobDetailResult.get(0).get("LASTRECORDID").toString());

                    //error check
                    if ( labelPrintJobID <= 0 ) {
                        Logger.logMessage("Could not determine a label print job detail ID...", Logger.LEVEL.ERROR);
                        return jobCreated;
                    }

                }

                //print job successfully created
                Logger.logMessage("Successfully created to create label print job... ID# "+labelPrintJobID, Logger.LEVEL.DEBUG);
                jobCreated = "success";

            } else {
                Logger.logMessage("Could not create label print job due to missing data...", Logger.LEVEL.ERROR);
                jobCreated = "failed";
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            jobCreated = "failed";
        }
        return jobCreated;
    }

    //get label printers that the logged in user has access too
    public ArrayList<HashMap> getLabelPrinters(HttpServletRequest request) {
        ArrayList<HashMap> labelPrinters = new ArrayList<>();
        try {
            //get logged in userID
            String userID = checkSession(request);

            //get label printers filtered by this user's revenue center
            labelPrinters = dm.parameterizedExecuteQuery("data.common.getLabelPrinters",
                new Object[]{
                    userID
                },
                true
            );
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return labelPrinters;
    }

    //get all the label print methods from the QC_PALabelPrintMethod system table
    public ArrayList<HashMap> getLabelPrintMethods(HttpServletRequest request) {
        ArrayList<HashMap> labelPrintMethods = new ArrayList<>();
        try {
            //get label printers filtered by this user's revenue center
            labelPrintMethods = dm.parameterizedExecuteQuery("data.common.getLabelPrintMethods",
                new Object[]{},
                true
            );
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return labelPrintMethods;
    }

/* END - Create "Label Printing Jobs" (for Direct to Label Printer) Methods */

    // method for verifying if revCenter has a valid Expeditor Printer
    public Boolean checkForValidExpeditorPrinter(HttpServletRequest request, Integer revCenterID) {
        Boolean isValidPrinter = false; // Init return variable

        // Check Here if rev center has Valid Expeditor printer
        try {
            //get label printers filtered by this user's revenue center
            ArrayList<HashMap> hasPrinter;
            hasPrinter = dm.parameterizedExecuteQuery(
                    "data.common.checkForValidExpeditorPrinter",
                    new Object[]{revCenterID},
                    true
            );

            // CHECK for ExpeditorPrinter == true and Name !="No Printer"
            if(!hasPrinter.isEmpty()){
                // check printer Name
                String name = hasPrinter.get(0).get("NAME").toString();
                if(!name.isEmpty() && !name.equalsIgnoreCase("[ No Printer ]")){
                    isValidPrinter = true;
                }
            }

        } catch (Exception ex) {
            Logger.logMessage("Error in method: checkForValidExpeditorPrinter()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

        return isValidPrinter;
    }

    // method for verifying if revCenter has a valid Expeditor Printer
    public Boolean verifyRevenueCenterPrinter(HttpServletRequest request, Integer revCenterID) {
        Boolean isValid = false; // Init return variable

        // Check Here if rev center has Valid Expeditor printer
        try {
            //get label printers filtered by this user's revenue center
            ArrayList<HashMap> result;
            result = dm.parameterizedExecuteQuery(
                    "data.common.verifyRevenueCenterPrinter",
                    new Object[]{revCenterID},
                    true
            );

            // CHECK for ExpeditorPrinter == true and Name !="No Printer"
            Integer hardwareType = CommonAPI.convertModelDetailToInteger( result.get(0).get("PRINTERTYPEID") );

            //if using KDS, return true
            if ( hardwareType == 1 ) {
                isValid = true;
            }

            // check printer Name
            String name = result.get(0).get("NAME").toString();
            if ( !name.isEmpty() && !name.equalsIgnoreCase("[ No Printer ]") ) {
                isValid = true;
            }

        } catch (Exception ex) {
            Logger.logMessage("Error in method: verifyRevenueCenterPrinter()", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
        }

        return isValid;
    }

    public static HashMap<String,String> validateFile(String filePath, String name){
        // INIT RETURN VAL
        HashMap<String, String> result = new HashMap<>();

        try{
            // Create FILE
            File f = new File(filePath);
            // TEST EXISTENCE
            if(!f.exists()){
                result.put("RESULT", "FAILURE");
                result.put("MESSAGE", name+" does not exist.");
                Logger.logMessage(ERR_FILE_NOT_EXIST+" "+filePath, Logger.LEVEL.TRACE);
            } else if(!f.isFile()){
                // Test if an object is a 'File' and not dir etc
                result.put("RESULT", "FAILURE");
                result.put("MESSAGE", "Object is not a valid "+name+".");
                Logger.logMessage(ERR_OBJ_NOT_FILE+" "+filePath, Logger.LEVEL.TRACE);
            } else {
                // SUCCESS
                result.put("RESULT", "SUCCESS");
                result.put("MESSAGE", name+" is valid.");
            }
        } catch(Exception ex){
            // LOG EXCEPTION AND set failure
            Logger.logMessage("Exception in method: validateFile()", Logger.LEVEL.TRACE);
            Logger.logException(ex);
            result.put("RESULT", "FAILURE");
            result.put("MESSAGE",ex.getMessage());
        }

        // DONE
        return result;
    }

    public static HashMap<String,String> validateFile(String filePath) {

        String name ="File";

        return validateFile(filePath, name);

    }

    public static HashMap<String,String> validateDirectory(String path) {
        // INIT RETURN VAL
        HashMap<String, String> result = new HashMap<>();
        try{
            // Create FILE
            File f = new File(path);
            // TEST EXISTENCE
            if(!f.exists()){
                result.put("RESULT", "FAILURE");
                result.put("MESSAGE", ERR_DIR_NOT_EXIST);
                Logger.logMessage(ERR_DIR_NOT_EXIST + " " + path, Logger.LEVEL.TRACE);
            } else if(!f.isDirectory()){
                // TEST is a DIR and not something else
                result.put("RESULT", "FAILURE");
                result.put("MESSAGE", ERR_OBJ_NOT_DIR);
                Logger.logMessage(ERR_OBJ_NOT_DIR + " " + path, Logger.LEVEL.TRACE);
            } else {
                // SUCCESS
                result.put("RESULT", "SUCCESS");
                result.put("MESSAGE", "Directory is valid.");
            }
        } catch(Exception ex){
            // LOG EXCEPTION AND set failure
            Logger.logMessage("Exception in method: validateDirectory()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
            result.put("RESULT", "FAILURE");
            result.put("MESSAGE",ex.getMessage());
        }

        // DONE
        return result;
    }

    public static ArrayList<HashMap> getInterfaceDirectories(String valueName){

        // INIT Return var
        ArrayList<HashMap> results = new ArrayList<>();
        final String PATH_INTERFACES = "Interfaces";

        try{
            if(valueName.isEmpty()){
                throw new Exception("Parameter is missing or blank.");
            }

            // Get DIRECTORIES WITHIN {MMH_HOME}\applications\{instance}\Interfaces
            String interfacePath = commonMMHFunctions.getMMHUploadedFilePath() + File.separator + PATH_INTERFACES;

            File file = new File(interfacePath);
            String[] directories = file.list(new FilenameFilter() {
                @Override
                public boolean accept(File current, String name) {
                    return new File(current, name).isDirectory();
                }
            });
            if(directories != null){
                for(String dir : directories){
                    HashMap<String, String> d = new HashMap<>();
                    d.put("NAME", dir);
                    d.put(valueName, dir);
                    results.add(d);
                }
            } else {
               Logger.logMessage("Unable to reading Interfaces directory. Interfaces directory is missing.", Logger.LEVEL.ERROR);
            }

        }catch(Exception ex){
            Logger.logMessage("Exception in method: getInterfaceDirectories() ", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return results;
    }

    public static ArrayList<HashMap> findInterfaceDirectory(String param,String key){
        final String INVALID_PATH = "Invalid Path";
        ArrayList<HashMap> result = new ArrayList<>();
        try{
            if(param != null && !param.isEmpty()){
                HashMap<String, String> responseMap = new HashMap<>();
                responseMap.put("NAME", INVALID_PATH);
                responseMap.put(key, INVALID_PATH);
                result.add(responseMap);
            }
        } catch (Exception ex){
            Logger.logMessage("Exception in method: findInterfaceDirectory()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return result;
    }

    public ArrayList<HashMap> checkFileUploadInterfaceDataSource(int interfaceID){
        ArrayList<HashMap> results = new ArrayList<>();
        try{
            results = dm.parameterizedExecuteQuery("data.common.checkFileUploadInterfaceDataSource", new Object[]{interfaceID}, true);
        } catch(Exception ex){
            Logger.logMessage("Error in method: checkFileUploadInterfaceDataSource.", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return results;
    }

    public static String buildRefPageParam(String uri){
        // This method parses the requestURI and builds the refPage param
        String refPage = "";
        try{
            int posSlash = uri.lastIndexOf("/");
            if(posSlash > -1){
                refPage = uri.substring(posSlash+1);
                refPage = "?refpage="+refPage.replace(".jsp","");
            }
        } catch(Exception ex){
            Logger.logMessage("Error in method: buildRefPageParam()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return refPage;
    }

    public boolean sendTemplateEmail(int UserID, int LanguageID, int EmailTypeID, HashMap params){
        boolean sendSuccess = false;
        try{
            // Lookup QuickChargeUser email address by ID
            ArrayList<HashMap> userList = dm.parameterizedExecuteQuery(MMHEmailBuilder.QUERY_USEREMAIL, new Object[]{UserID}, true);
            if(!userList.isEmpty()){
                if(userList.get(0).containsKey("EMAILADDRESS1")){
                    String emailAddress = userList.get(0).get("EMAILADDRESS1").toString();
                    sendSuccess = sendTemplateEmail(emailAddress, LanguageID, EmailTypeID, "", params);
                }
            } else {
                Logger.logMessage("Unable to find email address for UserID: "+UserID, Logger.LEVEL.DEBUG);
            }
        } catch(Exception ex){
            Logger.logMessage("Error in method: sendTemplateEmail(UserID): ", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return sendSuccess;
    }

    public boolean sendTemplateEmail(String emailAddress, int EmailTypeID, String templateText, HashMap params){
        // Override - use default Language ID of English / 1
        return sendTemplateEmail(emailAddress, 1, EmailTypeID, templateText, params);
    }

    public boolean sendTemplateEmail(String emailAddress, int languageID, int emailTypeID, String templateText, HashMap params ){

        // Init return variable
        boolean sendResult = false;
        // Init SecureAuth flag
        boolean sendSecure = false;

        // check params validity
        if(params == null){
            params = new HashMap();
        }

        // EMAIL PROPERTIES
        String smtpUser = "";
        String smtpPassword = "";
        String smtpServer = "";
        String smtpPort = "";
        String smtpReplyToAddress = "";
        String smtpReplyToName = "";
        String smtpFromAddress = "";
        String smtpFromName = "";

        // FIELDS

        String FIELD_LANGUAGE_ID = "LANGUAGEID";
        String FIELD_INVITEEMAILTEMPLATEID = "INVITEEMAILTEMPLATEID";
        String FIELD_MYQCEMAILTEMPLATEID = "MYQCEMAILTEMPLATEID";
        String FIELD_QCPOSEMAILTEMPLATEID = "QCPOSEMAILTEMPLATEID";

        // DEFAULT to 1
        int communicationGroupID = 1;

        try {
            // Validate Params
            if(emailAddress.isEmpty()){
                Logger.logMessage("Email Address parameter is missing.", Logger.LEVEL.ERROR);
                return sendResult;
            } else {
                // Add email address to parameters if not already in params
                if(!params.containsKey("Email")){
                    params.put("Email", emailAddress);
                }
            }

            // Determine whether to send via Secure SMTP or not
            ArrayList<HashMap> sendSecureArrayList = dm.parameterizedExecuteQuery(MMHEmailBuilder.QUERY_GLOBALSETTINGS, new Object[]{}, true);
            if(!sendSecureArrayList.isEmpty()){
                if(sendSecureArrayList.get(0).containsKey(MMHEmailBuilder.FIELD_SMTPUSER)){
                    smtpUser = sendSecureArrayList.get(0).get(MMHEmailBuilder.FIELD_SMTPUSER).toString();
                    if(!smtpUser.isEmpty()){MMHEmail.setUser(smtpUser);}
                }
                if(sendSecureArrayList.get(0).containsKey(MMHEmailBuilder.FIELD_SMTPPASSWORD)){
                    smtpPassword = sendSecureArrayList.get(0).get(MMHEmailBuilder.FIELD_SMTPPASSWORD).toString();
                    if(!smtpPassword.isEmpty()){MMHEmail.setPass(StringFunctions.decodePassword(smtpPassword));}
                }
                if(sendSecureArrayList.get(0).containsKey(MMHEmailBuilder.FIELD_SMTPPORT)){
                    smtpPort = sendSecureArrayList.get(0).get(MMHEmailBuilder.FIELD_SMTPPORT).toString();
                    if(!smtpPort.isEmpty()){MMHEmail.setPort(smtpPort);}
                }
                if(sendSecureArrayList.get(0).containsKey(MMHEmailBuilder.FIELD_SMTPSERVER)){
                    smtpServer = sendSecureArrayList.get(0).get(MMHEmailBuilder.FIELD_SMTPSERVER).toString();
                    if(!smtpServer.isEmpty()){MMHEmail.setHost(smtpServer);}
                }
                if(sendSecureArrayList.get(0).containsKey(MMHEmailBuilder.FIELD_SMTPREPLYTOADDRESS)){
                    smtpReplyToAddress = sendSecureArrayList.get(0).get(MMHEmailBuilder.FIELD_SMTPREPLYTOADDRESS).toString();
                    if(!smtpReplyToAddress.isEmpty()){MMHEmail.setReplyToAddress(smtpReplyToAddress);}
                }
                if(sendSecureArrayList.get(0).containsKey(MMHEmailBuilder.FIELD_SMTPREPLYTONAME)){
                    smtpReplyToName = sendSecureArrayList.get(0).get(MMHEmailBuilder.FIELD_SMTPREPLYTONAME).toString();
                    if(!smtpReplyToName.isEmpty()){MMHEmail.setReplyToName(smtpReplyToName);}
                }
                if(sendSecureArrayList.get(0).containsKey(MMHEmailBuilder.FIELD_SMTPFROMADDRESS)){
                    smtpFromAddress = sendSecureArrayList.get(0).get(MMHEmailBuilder.FIELD_SMTPFROMADDRESS).toString();
                    if(!smtpFromAddress.isEmpty()){MMHEmail.setFromAddress(smtpFromAddress);}
                }
                if(sendSecureArrayList.get(0).containsKey(MMHEmailBuilder.FIELD_SMTPFROMNAME)){
                    smtpFromName = sendSecureArrayList.get(0).get(MMHEmailBuilder.FIELD_SMTPFROMNAME).toString();
                    if(!smtpFromName.isEmpty()){MMHEmail.setFromName(smtpFromName);}
                }
                // TEST FOR ABILITY TO SEND WITH SECURE AUTH
                if((!smtpUser.isEmpty() && !smtpUser.equals("N/A")) && (!smtpPassword.isEmpty() && !smtpPassword.equals("N/A"))){
                    sendSecure = true;
                }
            }

            // Get EmailType record
            int subjectID = 0;
            int messageID = 0;
            int templateTypeID = 0;
            ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(MMHEmailBuilder.QUERY_EMAILTYPEID, new Object[]{emailTypeID}, true);
            if(!arrayList.isEmpty()){
                if(arrayList.get(0).containsKey("EMAILTYPEID")){
                    subjectID = Integer.parseInt(arrayList.get(0).get("SUBJECTSYSTEMTEXTTYPEID").toString());
                    messageID = Integer.parseInt(arrayList.get(0).get("MESSAGESYSTEMTEXTTYPEID").toString());
                    templateTypeID = Integer.parseInt(arrayList.get(0).get("TEMPLATETYPEID").toString());
                }
            } else {
                Logger.logMessage("Unable to find EmailTypeID for: " + emailTypeID, Logger.LEVEL.ERROR);
                return sendResult;
            }

            // Lookup EmployeeID by EmailAddress if both UserID and EmployeeID are not provided
            if(!params.containsKey("EmployeeID") && !params.containsKey("UserID") && !emailAddress.isEmpty()){
                // Lookup EmployeeID by Email
                arrayList = dm.parameterizedExecuteQuery(MMHEmailBuilder.QUERY_EMPLOYEEID, new Object[]{emailAddress}, true);
                if(!arrayList.isEmpty() && arrayList.get(0).containsKey("EMPLOYEEID")){
                    int empID = Integer.parseInt(arrayList.get(0).get("EMPLOYEEID").toString());
                    params.put("EmployeeID",empID);
                }

                arrayList = new DynamicSQL(MMHEmailBuilder.QUERY_USERID)
                        .addConditionalClause(1, "WHERE", "EMAILADDRESS1", "=", emailAddress)
                        .serialize(dm);
                if (!arrayList.isEmpty() && arrayList.get(0).containsKey("USERID")) {
                    int userID = Integer.parseInt(arrayList.get(0).get("USERID").toString());
                    params.put("UserID", userID);
                }
            }

            // Is CommunicationGroupID Available
            if(params.containsKey("CommunicationGroupID")){
                // Lookup Template Text
                communicationGroupID = Integer.parseInt(params.get("CommunicationGroupID").toString());
            } else {
                // Get CommunicationGroup from EmployeeID
                if(params.containsKey("EmployeeID")){
                    int employeeID = Integer.parseInt(params.get("EmployeeID").toString());
                    arrayList = dm.parameterizedExecuteQuery(MMHEmailBuilder.QUERY_COMMGROUPBYEMP, new Object[]{employeeID}, true);
                    if(!arrayList.isEmpty() && arrayList.get(0).containsKey("COMMUNICATIONGROUPID")){
                        communicationGroupID = Integer.parseInt(arrayList.get(0).get("COMMUNICATIONGROUPID").toString());
                    }
                }
                else if (params.containsKey("UserID")) {
                    int userID = Integer.parseInt(params.get("UserID").toString());
                    arrayList = new DynamicSQL(MMHEmailBuilder.QUERY_COMMGROUPBYUSER)
                            .addConditionalClause(1, "WHERE", "USERID", "=", userID)
                            .serialize(dm);
                    if (!arrayList.isEmpty() && arrayList.get(0).containsKey("COMMUNICATIONGROUPID")) {
                        communicationGroupID = Integer.parseInt(arrayList.get(0).get("COMMUNICATIONGROUPID").toString());
                    }
                }
            }

            // Lookup TemplateID from the Communication Group
            int templateID = 0;
            if(!params.containsKey("TEMPLATEID")){
                arrayList = dm.parameterizedExecuteQuery(MMHEmailBuilder.QUERY_TEMPLATEID, new Object[]{communicationGroupID}, true);
                if(!arrayList.isEmpty()){
                    if(templateTypeID == 1){
                        templateID = Integer.parseInt(arrayList.get(0).get(FIELD_INVITEEMAILTEMPLATEID).toString());
                    } else if(templateTypeID == 2){
                        templateID = Integer.parseInt(arrayList.get(0).get(FIELD_MYQCEMAILTEMPLATEID).toString());
                    } else if(templateTypeID == 5){
                        templateID = Integer.parseInt(arrayList.get(0).get(FIELD_QCPOSEMAILTEMPLATEID).toString());
                    } else {
                        Logger.logMessage("Template Type ID is invalid.", Logger.LEVEL.ERROR);
                        return sendResult;
                    }

                    // ADD TemplateID to params for later reference when building [[HeaderImage]]
                    params.put("TEMPLATEID",templateID);
                }
            } else {
                templateID = Integer.parseInt(params.get("TEMPLATEID").toString());
            }

            // if templateText is not provided then retrieve from the MMH_TEMPLATE
            if(templateText == null || templateText.isEmpty()){

                // Lookup templateText by Communication Type
                arrayList = dm.parameterizedExecuteQuery(MMHEmailBuilder.QUERY_EMAILTEMPLATE, new Object[]{templateID, templateTypeID}, true);
                if(!arrayList.isEmpty() && arrayList.get(0).containsKey(MMHEmailBuilder.FIELD_HTML)) {
                    templateText = arrayList.get(0).get(MMHEmailBuilder.FIELD_HTML).toString();
                    templateText = MMHEmailBuilder.decode(templateText);
                }

                // If cannot find template text then throw error and exit
                if(templateText.isEmpty()){
                    Logger.logMessage("Unable to find email template.", Logger.LEVEL.ERROR);
                    return sendResult;
                }
            }

            // Lookup Language ID if missing
            if(languageID == 0){

                if(params.containsKey("EmployeeID")){
                    // Lookup Employee Language
                    Integer employeeID = Integer.parseInt(params.get("EmployeeID").toString());
                    arrayList = dm.parameterizedExecuteQuery(MMHEmailBuilder.QUERY_EMP_LANGUAGE, new Object[]{employeeID}, true);
                    if(!arrayList.isEmpty() && arrayList.get(0).containsKey(FIELD_LANGUAGE_ID)){
                        languageID = Integer.parseInt(arrayList.get(0).get(FIELD_LANGUAGE_ID).toString());
                    }
                } else if(params.containsKey("UserID")){
                    // Lookup User Language
                    Integer userID = Integer.parseInt(params.get("UserID").toString());
                    arrayList = dm.parameterizedExecuteQuery(MMHEmailBuilder.QUERY_USER_LANGUAGE, new Object[]{userID}, true);
                    if(!arrayList.isEmpty() && arrayList.get(0).containsKey(FIELD_LANGUAGE_ID)){
                        languageID = Integer.parseInt(arrayList.get(0).get(FIELD_LANGUAGE_ID).toString());
                    }
                } else {
                    // default languageID to 1
                    languageID = 1;
                    Logger.logMessage("Setting LanguageID to DEFAULT - 1 - ENGLISH", Logger.LEVEL.DEBUG);
                }
            }

            MMHEmailBuilder emailBuilder = new MMHEmailBuilder();
            emailBuilder.setParameterMap(params);
            emailBuilder.createSubject(subjectID, communicationGroupID, languageID);
            emailBuilder.createBody(templateText, messageID, communicationGroupID, languageID);

            // Send Email
            String subject = emailBuilder.buildSubject();
            if(subject.trim().isEmpty()){
                throw new Exception("Unable to build Subject for SystemTextID: "+subjectID+
                        " CommunicationGroupID: "+communicationGroupID+
                        " LanguageID: "+languageID+" System Text is blank or missing!");
            }
            String body = emailBuilder.buildBody();
            if(body.trim().isEmpty()){
                throw new Exception("Unable to build MessageText for SystemTextID: "+messageID+
                        " CommunicationGroupID: "+communicationGroupID+
                        " LanguageID: "+languageID+" System Text is blank or missing!");
            }
            sendResult = MMHEmail.sendEmail(emailAddress, smtpFromAddress, subject, body, sendSecure);

        } catch(Exception ex){
            Logger.logMessage("Error in method: sendTemplateEmail(EmailAddress): ", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return sendResult;
    }

    public int getEmployeeIDFromFundingEmailAddress(String emailAddress){
        // This method retrieves the EmployeeID from the provided funding Email AddressLogger.logMessage("Error in method: getEmployeeIDFromFundingEmailAddress()", Logger.LEVEL.ERROR);
        Logger.logMessage("Entering method: getEmployeeIDFromFundingEmailAddress() ...", Logger.LEVEL.DEBUG);
        int employeeID = -1;
        try {
            if(!emailAddress.trim().isEmpty()) {
                ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(MMHEmailBuilder.QUERY_FUNDING_EMPID, new Object[]{emailAddress}, true);
                if (!arrayList.isEmpty()) {
                    if (arrayList.get(0).containsKey(MMHEmailBuilder.FIELD_EMPLOYEE_ID)) {
                        employeeID = Integer.parseInt(arrayList.get(0).get(MMHEmailBuilder.FIELD_EMPLOYEE_ID).toString());
                        Logger.logMessage("Found EmployeeID: " + employeeID + " for funding email: " + emailAddress, Logger.LEVEL.DEBUG);
                    }
                }
            }
        } catch(Exception ex){
            Logger.logMessage("Error in method: getEmployeeIDFromFundingEmailAddress()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return employeeID;
    }

    public static void deregisterJDBCDriver(){
        // Deregister JDBC drivers in this context's ClassLoader:
        // Get the webapp's ClassLoader
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        // Loop through all drivers
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            if (driver.getClass().getClassLoader() == cl) {
                // This driver was registered by the webapp's ClassLoader, so deregister it:
                try {
                    String driverName = driver.getClass().getCanonicalName()+"("+driver.getMajorVersion()+"."+driver.getMinorVersion()+")";
                    Logger.logMessage("Deregistering JDBC driver: "+driverName, Logger.LEVEL.IMPORTANT);
                    DriverManager.deregisterDriver(driver);
                } catch (SQLException ex) {
                    Logger.logMessage("Error deregistering JDBC driver: "+driver.toString(), Logger.LEVEL.ERROR);
                    Logger.logException(ex);
                }
            } else {
                // driver was not registered by the webapp's ClassLoader and may be in use elsewhere
                Logger.logMessage("Not deregistering JDBC driver:"+driver.toString()+" as it does not belong to this webapp's ClassLoader", Logger.LEVEL.IMPORTANT);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public HashMap getKDSRouterInfo(int terminalID, String log){

        // Method retrieves KDS/Bematech Router information from RC via TerminalID

        HashMap routerInfo = new HashMap();
        String KDS_DSID = "KDSDATASOURCEID";
        String KDS_RA_PRIM = "KDSROUTERADDRESSPRIMARY";
        String KDS_RPN_PRIM = "KDSROUTERPORTNUMBERPRIMARY";
        String KDS_RA_BACK = "KDSROUTERADDRESSBACKUP";
        String KDS_RPN_BACK = "KDSROUTERPORTNUMBERBACKUP";

        try{
            ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery("data.newKitchenPrinter.GetKDSRouterInfo", new Object[]{terminalID}, true);
            if (!DataFunctions.isEmptyCollection(arrayList) && !DataFunctions.isEmptyMap(arrayList.get(0))) {
                String dsID = HashMapDataFns.getStringVal(arrayList.get(0), KDS_DSID);
                String routerPrimaryIPAddr = HashMapDataFns.getStringVal(arrayList.get(0), KDS_RA_PRIM);
                String routerPrimaryPortNum = HashMapDataFns.getStringVal(arrayList.get(0), KDS_RPN_PRIM);
                String routerBackupIPAddr = HashMapDataFns.getStringVal(arrayList.get(0), KDS_RA_BACK);
                String routerBackupPortNum = HashMapDataFns.getStringVal(arrayList.get(0), KDS_RPN_BACK);

                routerInfo.put(KDS_DSID, dsID);
                routerInfo.put(KDS_RA_PRIM, routerPrimaryIPAddr);
                routerInfo.put(KDS_RPN_PRIM, routerPrimaryPortNum);
                routerInfo.put(KDS_RA_BACK, routerBackupIPAddr);
                routerInfo.put(KDS_RPN_BACK, routerBackupPortNum);
            }
            else {
                Logger.logMessage("No router information has been found in the local database! Please check configuraiton.", log, Logger.LEVEL.ERROR);
            }
        } catch(Exception ex){
            Logger.logMessage("(getKDSRouterInfo) "+ex.getMessage());
            Logger.logException(ex);
        }

        return routerInfo;
    }

    public ArrayList<HashMap> addActiveStatus(ArrayList<HashMap> records, String nameField, String activeField) {
        try{

           for(HashMap record: records) {

               if(containsFullKey(record.get(nameField)) && containsFullKey(record.get(activeField))) {
                   String activeStatus = "Invalid Status";

                   if(record.get(activeField).toString().equals("1") || record.get(activeField).toString().equals("true")) {
                       activeStatus = "Active";
                   } else if(record.get(activeField).toString().equals("0") || record.get(activeField).toString().equals("false")) {
                       activeStatus = "Inactive";
                   }

                   record.put(nameField, record.get(nameField).toString().concat(" | ").concat(activeStatus));
                   record.remove(activeField);
               }
           }

        } catch(Exception ex){
            Logger.logMessage("ERROR: Could not add active status to name record in commonMMH.addActiveStatus()", Logger.LEVEL.DEBUG);
            Logger.logException(ex);
        }
        return records;
    }

    /*START GET SCHEMA TABLES AND COLUMNS FOR GLOBAL AUDIT LOGGING*/

    /**
     * Get all the tables and columns from the database schema.
     *
     * @return {@link ArrayList<HashMap>}
     */
    public static ArrayList<HashMap> getSchemaTablesAndColumns () {
        ArrayList<HashMap> schemaTablesAndColumns = new ArrayList<>();
        try {
            schemaTablesAndColumns =
                    new DataManager().parameterizedExecuteQuery("data.globalAuditLogger.getSchemaTablesAndColumns",
                            new Object[]{}, true);
        }
        catch (Exception e) {
            Logger.logException(e);
        }
        return schemaTablesAndColumns;
    }
    /*END GET SCHEMA TABLES AND COLUMNS FOR GLOBAL AUDIT LOGGING*/

    /* START - helper methods for getting global audit history */

    // Uses passed in String table names to retrieve Table ID from QC_SchemaTable
    public String getSchemaTableID(ArrayList<String> tableNames){
        ArrayList<Long> tableIds = new ArrayList<>();
        try{
            for(int i = 0; i < tableNames.size(); i++){
                tableIds.add((Long) dm.parameterizedExecuteScalar("data.common.getSchemaTableID", new Object[]{tableNames.get(i)}));
            }
        }
        catch(Exception e){
            Logger.logException(e);
        }
        return ArrayListToCSV(tableIds);
    }
    // Does initial check to see if any history exists for a certain object in a specific table
    public ArrayList<HashMap> checkForAuditHistory(HttpServletRequest request, String rowID, String tableID){
        try{
            return new DynamicSQL("data.common.checkAuditHistoryExists").addIDList(1, rowID).addIDList(2,tableID).serialize(dm);
        }
        catch(Exception e){
            Logger.logException(e);
            return new ArrayList<>();
        }
    }
    /* END - helper methods for getting global audit history */

    // TODO try to improve time complexity of this method it's O(n^3)
    /**
     * <p>Utility method to simulate a SQL LIKE search in Java.</p>
     *
     * @param records The {@link ArrayList} of {@link HashMap} corresponding to the records search through.
     * @param searchTerm The {@link String} that is being searched for.
     * @param searchFields An array of field {@link String} to include in the search.
     * @return The {@link ArrayList} of {@link HashMap} corresponding to records which satisfy the search.
     */
    @SuppressWarnings("Convert2streamapi")
    public static ArrayList<HashMap> simulateSQLLikeSearch (ArrayList<HashMap> records, String searchTerm, String[] searchFields) {
        ArrayList<HashMap> matches = new ArrayList<>();

        try {
            if ((!DataFunctions.isEmptyCollection(records)) && (StringFunctions.stringHasContent(searchTerm)) && (!DataFunctions.isEmptyGenericArr(searchFields))) {
                for (HashMap record : records) {
                    if (!DataFunctions.isEmptyMap(record)) {
                        // check if the record contains the search term
                        for (String searchField : searchFields) {
                            if ((record.containsKey(searchField.toUpperCase())) && (StringFunctions.containsIgnoreCase(HashMapDataFns.getStringVal(record, searchField.toUpperCase()), searchTerm))) {
                                matches.add(record);
                                // don't need to check the next search term a match was already found
                                break;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to perform a SQL LIKE search on the given " +
                    "records and given fields within those records of %s in commonMMHFunctions.simulateSQLLikeSearch",
                    Objects.toString(StringFunctions.buildStringFromList(((!DataFunctions.isEmptyGenericArr(searchFields)) ? Stream.of(searchFields).collect(Collectors.toCollection(ArrayList::new)) : new ArrayList<>()), ","),"NULL")), Logger.LEVEL.ERROR);
        }

        return matches;
    }

    /**
     * <p>Iterates through the records and finds any record whose value at the key contains the search term.</p>
     *
     * @param records An {@link ArrayList} of {@link HashMap} corresponding to the records to search through.
     * @param searchTerm The {@link String} search term.
     * @param key The {@link String} key to search for within each record.
     * @return An {@link ArrayList} of {@link HashMap} corresponding to the records whose value at the key contains the search term.
     */
    @SuppressWarnings("Convert2streamapi")
    public static ArrayList<HashMap> simulateSQLLikeSearch (ArrayList<HashMap> records, String searchTerm, String key) {

        // make sure there are records to search through
        if (DataFunctions.isEmptyCollection(records)) {
            Logger.logMessage("The records passed to commonMMHFunctions.simulateSQLLikeSearch can't be null or empty, unable to " +
                    "find any records, now returning null!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure there is a search term
        if (!StringFunctions.stringHasContent(searchTerm)) {
            Logger.logMessage("The search term String passed to commonMMHFunctions.simulateSQLLikeSearch can't be null or empty, unable to " +
                    "find any records, now returning null!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure there is a key
        if (!StringFunctions.stringHasContent(key)) {
            Logger.logMessage("The key String passed to commonMMHFunctions.simulateSQLLikeSearch can't be null or empty, unable to " +
                    "find any records, now returning null!", Logger.LEVEL.ERROR);
            return null;
        }

        // search for relevant records
        ArrayList<HashMap> retRecords = new ArrayList<>();
        for (HashMap hm : records) {
            if ((!DataFunctions.isEmptyMap(hm)) && (StringFunctions.caseInsensitiveContains(searchTerm, HashMapDataFns.getStringVal(hm, key, true)))) {
                retRecords.add(hm);
            }
        }

        return retRecords;
    }

    /**
     * <p>Gets the logging level as defined in the app properties.</p>
     *
     * @return The logging level as a {@link String}.
     */
    public static String getLoggingLevel () {

        if (StringFunctions.stringHasContent(MMHProperties.getAppSetting("site.logging.level").trim())) {
            return MMHProperties.getAppSetting("site.logging.level").trim();
        }

        return "NONE";
    }

    /**
     * <p>Gets the directory at the specified paths.</p>
     *
     * @param paths A variable number of {@link String} corresponding to a folder within the path.
     * @return The {@link File} directory.
     */
    public static File getDirectory (String ... paths) {
        File directory = null;

        String mmhHomeDirectory = getMMH_Home();

        if (!mmhHomeDirectory.substring(mmhHomeDirectory.length() - 1).equalsIgnoreCase(File.separator)) {
            mmhHomeDirectory += File.separator;
        }

        if (paths.length > 0) {
            for (String path : paths) {
                mmhHomeDirectory += path + File.separator;
            }
        }

        Path path = null;
        try {
            path = Files.createDirectories(Paths.get(mmhHomeDirectory));
        }
        catch (IOException e) {
            Logger.logException(e);
            Logger.logMessage(String.format("A problem occurred while trying to create the directory at the path %s!",
                    Objects.toString(mmhHomeDirectory, "N/A")), Logger.LEVEL.ERROR);
        }
        if ((path != null) && (path.toUri() != null)) {
            directory = new File(path.toUri().getPath());
        }

        if (directory != null) {
            Logger.logMessage(String.format("A directory may be found at the path %s!",
                    Objects.toString(directory.getAbsolutePath(), "N/A")), Logger.LEVEL.TRACE);
        }

        return directory;
    }

    /**
     * <p>Utility method to copy the source {@link File} to a destination {@link File}.</p>
     *
     * @param src The source {@link File} to copy.
     * @param dest The destination {@link File} thet the source {@link File} will be copied into.
     */
    public static void copyFile (File src, File dest) throws Exception {

        // make sure the source File is valid
        if (src == null) {
            Logger.logMessage("The source File passed to commonMMHFunctions.copyFile can't be null!", Logger.LEVEL.ERROR);
            throw new Exception("The source File passed to commonMMHFunctions.copyFile can't be null!");
        }

        // make sure the destination File is valid
        if (dest == null) {
            Logger.logMessage("The destination File passed to commonMMHFunctions.copyFile can't be null!", Logger.LEVEL.ERROR);
            throw new Exception("The destination File passed to commonMMHFunctions.copyFile can't be null!");
        }

        // convert the source file to a Path
        Path srcPath = src.toPath();

        // convert the destination file to a Path
        Path destPath = dest.toPath();

        Logger.logMessage(String.format("Now attempting to copy the File \"%s\" into \"%s\" in commonMMHFunctions.copyFile!",
                Objects.toString(src.getAbsolutePath(), "N/A"),
                Objects.toString(dest.getAbsolutePath(), "N/A")), Logger.LEVEL.TRACE);

        // try to copy the file
        try {
            Files.copy(srcPath, destPath, StandardCopyOption.REPLACE_EXISTING);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("A problem occurred while attempting to copy the File \"%s\" into \"%s\" in commonMMHFunctions.copyFile!",
                    Objects.toString(src.getAbsolutePath(), "N/A"),
                    Objects.toString(dest.getAbsolutePath(), "N/A")), Logger.LEVEL.ERROR);
            throw e;
        }

        Logger.logMessage(String.format("The File \"%s\" has been successfully copied into \"%s\" in commonMMHFunctions.copyFile!",
                Objects.toString(src.getAbsolutePath(), "N/A"),
                Objects.toString(dest.getAbsolutePath(), "N/A")), Logger.LEVEL.TRACE);
    }

    /**
     * <p>Deletes a file.</p>
     *
     * @param f The {@link File} to delete.
     * @return Whether or not the {@link File} was deleted.
     */
    public static boolean deleteFile (File f) {

        if (f == null) {
            Logger.logMessage("The File passed to commonMMHFunctions.deleteFile can't be null!", Logger.LEVEL.ERROR);
            return false;
        }

        if (!f.exists()) {
            Logger.logMessage("The File passed to commonMMHFunctions.deleteFile must exist!", Logger.LEVEL.ERROR);
            return false;
        }

        return f.delete();
    }

	  /**
     * <p>Turns an object (that is itself an array/list of strings) into an actual ArrayList<String>
     * This is used because when javascript sends an array/list to the java side through JSONRPC,
     * declaring that variable on the Java side as ArrayList or ArrayList<String> (or List, etc.) causes an error ('arg could not unmarshal')
     * i.e. - public void updateThing(ArrayList<String> ids) <--will fail
     *      - public void updateThing(Object ids) <--will not fail, and can be turned into a proper ArrayList by this function
     * </p>
     * @param f Object that holds a list of strings
     * @return ArrayList of Strings
     */
    public static ArrayList<String> objectToArrayList(Object ids) {
        Object[] objects = (Object[]) ids;
        ArrayList<String> stringList = new ArrayList<String>();
        for (Object object : objects) {
            stringList.add(object.toString());
        }
        return stringList;
    }
}
