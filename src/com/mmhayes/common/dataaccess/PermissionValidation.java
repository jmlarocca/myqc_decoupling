package com.mmhayes.common.dataaccess;

import com.mmhayes.common.utils.Logger;
import java.util.ArrayList;
import java.util.HashMap;

/*
 $Author: jrmitaly $: Author of last commit
 $Date: 2015-04-30 09:16:16 -0400 (Thu, 30 Apr 2015) $: Date of last commit
 $Rev: 1538 $: Revision of last commit
 Notes: Object-level permission checking
*/

public class PermissionValidation {

    // Static variables
    public final String VIEW = "VIEWPERMISSION";
    public final String EDIT = "EDITPERMISSION";
    public final String ADD = "ADDPERMISSION";
    public final String DELETE = "DELETEPERMISSION";

    // Class variables
    private Integer componentId;
    private Long userId;
    private boolean hasCheckedPermissions;
    private boolean hasError;
    private String specialPermissionSqlStatement;
    private boolean view;
    private boolean edit;
    private boolean add;
    private boolean delete;
    private HashMap<String, Boolean> special;

    // Constructors
    public PermissionValidation() {
        this((Integer)null);
    }
    public PermissionValidation(Integer componentId) {
        this(componentId, null);
    }
    public PermissionValidation(String componentId) {
        this(componentId, null);
    }
    public PermissionValidation(String stringComponentId, String specialPermissionSqlStatement) {
        this((commonMMHFunctions.tryParseInt(stringComponentId) ? Integer.parseInt(stringComponentId) : 0), specialPermissionSqlStatement);
    }
    public PermissionValidation(Integer componentId, String specialPermissionSqlStatement) {
        this.hasCheckedPermissions = false;
        this.componentId = componentId;
        setSpecialPermission(specialPermissionSqlStatement);
    }

    // Getters
    public Long getUserIdLong() {
        return this.userId;
    }
    public String getUserIdString() {
        return (this.userId == null ? "" : this.userId.toString());
    }
    public boolean getHasCheckedPermissions() {
        return this.hasCheckedPermissions;
    }
    public boolean getHasError() {
        return this.hasError;
    }
    public boolean getViewPermission() {
        return this.view;
    }
    public boolean getEditPermission() {
        return this.edit;
    }
    public boolean getAddPermission() {
        return this.add;
    }
    public boolean getDeletePermission() {
        return this.delete;
    }
    public HashMap<String, Boolean> getSpecialPermission() {
        return this.special;
    }
    public void setSpecialPermission(String specialPermissionSqlStatement) {
        if (specialPermissionSqlStatement != null && specialPermissionSqlStatement.trim().length() > 0) {
            this.specialPermissionSqlStatement = specialPermissionSqlStatement;
        } else {
            this.specialPermissionSqlStatement = null;
        }
    }

    // Set the permissions
    public void setPermissions(String userId) {
        setPermissions((commonMMHFunctions.tryParseLong(userId) ? Long.parseLong(userId) : null), this.componentId, this.specialPermissionSqlStatement);
    }
    public void setPermissions(Long userId) {
        setPermissions(userId, this.componentId, this.specialPermissionSqlStatement);
    }
    public void setPermissions(Long userId, Integer componentId, String specialPermissionSqlStatement) {
        this.componentId = componentId;
        this.userId = userId;
        this.specialPermissionSqlStatement = specialPermissionSqlStatement;
        this.hasError = determinePermissions();
        if (!this.hasError) {
            this.hasCheckedPermissions = true;
        }
    }
    private boolean determinePermissions() {
        if (componentId == null) {
            this.view = true;
            this.edit = true;
            this.add = true;
            this.delete = true;
            this.special = new HashMap<String, Boolean>();
            return false;
        } else {
            try {
                if (userId == null) {
                    throw new Exception("No user provided!");
                }
                DataManager dm = new DataManager();
                ArrayList<HashMap> standardPermissionResultSet = dm.serializeSqlWithColNames("data.PermissionValidation.getStandardPermissions", new Object[] {this.userId, this.componentId}, false, true);
                if (standardPermissionResultSet.size() != 1) {
                    throw new Exception("Invalid query results when checking standard permissions.");
                }
                for (HashMap permission : standardPermissionResultSet) {
                    if (permission.containsKey(VIEW)) {
                        this.view = Boolean.parseBoolean(permission.get(VIEW).toString());
                    } else {
                        this.view = false;
                    }
                    if (permission.containsKey(EDIT)) {
                        this.edit = Boolean.parseBoolean(permission.get(EDIT).toString());
                    } else {
                        this.edit = false;
                    }
                    if (permission.containsKey(ADD)) {
                        this.add = Boolean.parseBoolean(permission.get(ADD).toString());
                    } else {
                        this.add = false;
                    }
                    if (permission.containsKey(DELETE)) {
                        this.delete = Boolean.parseBoolean(permission.get(DELETE).toString());
                    } else {
                        this.delete = false;
                    }
                }
                determineSpecialPermissions();
                return false;
            } catch (Exception e) {
                // Set all the permissions to false because there is a serious issue
                this.view = false;
                this.edit = false;
                this.add = false;
                this.delete = false;
                this.special = new HashMap<String, Boolean>();
                Logger.logMessage("Error in PermissionValidation.determinePermissions: " + e.getMessage());
                return true;
            }
        }
    }
    private void determineSpecialPermissions() {
        if (this.specialPermissionSqlStatement != null) {
            try {
                if (userId == null) {
                    throw new Exception("No user provided!");
                }
                DataManager dm = new DataManager();
                ArrayList<HashMap> specialPermissionResultSet = dm.serializeSqlWithColNames(this.specialPermissionSqlStatement, new Object[] {this.userId, this.componentId}, false, true);
                if (specialPermissionResultSet.size() != 1) {
                    throw new Exception("Invalid query results when checking special permissions.");
                }
                this.special = specialPermissionResultSet.get(0);
            } catch (Exception e) {
                this.special = new HashMap<String, Boolean>();
                Logger.logMessage("Error in PermissionValidation.determineSpecialPermissions: " + e.getMessage());
            }
        } else {
            this.special = new HashMap<String, Boolean>();
        }
        return;
    }
}
