package com.mmhayes.common.dataaccess;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;
import com.mmhayes.common.utils.*;

import javax.servlet.http.HttpServletRequest;

/*
 $Author: jkflanagan $: Author of last commit
 $Date: 2020-02-27 07:53:23 -0500 (Thu, 27 Feb 2020) $: Date of last commit
 $Rev: 10912 $: Revision of last commit
 Notes: Properties of a data element used by Data Handlers
*/
public class DataHandlerProperties {

    // Identifier strings
    private String tableName;
    private String parentIdentifier;
    private String nameField;
    private String getSQL;
    private String countSQL;
    private String insertSQL;
    private String updateSQL;
    private String insertSequence;
    private String getOwnershipSQL;
    private String eventStatusField;
    private ArrayList<String> primaryKeys; // Replaces primaryIdentifier
    private String updateKeySingleValue; // Cache value
    private String dynamicSQL;//this field is meant to be used with the DynamicSQL class to generate SAFE dynamic sql

    // Static Strings
    private String dbType;
    private String dbTimestamp;
    private String dbNextVal;
    private String dbDateFormat;

    // Booleans
    private boolean isDynamicSQL;
    private boolean getRecordUseSingleQuotes;
    private boolean parameterizeGetValues;
    private boolean loggedObject;
    private boolean testingApplication;
    private boolean testingOutputAll;
    private boolean useBeforeGetHook;
    private boolean useAfterGetHook;
    private boolean useBeforeSaveHook;
    private boolean useBeforeCompleteSaveHook;
    private boolean useAfterSaveHook;
    private boolean useGetDeletionsHook;
    private boolean useBeforeInsertHook;
    private boolean useAfterInsertHook;
    private boolean useAfterCompleteInsertHook;
    private boolean useBeforeUpdateHook;
    private boolean useAfterUpdateHook;
    private boolean checkPermissions;
    private boolean hardDelete;
    private boolean negativeNewRecordLookup;
    private boolean isMappingObject;
    private boolean AsyncSave;
    private boolean preventAttachRecords;

    // Integers
    private Integer componentId;
    private Integer languageId;
    private Integer maxChildSize;

    // Characters
    private char partitionPermissionType; // Used for record-level permissions

    // Cache Objects
    private HashMap mappingArrayLists;
    private HashMap<String, Object> keyValues;
    private long cacheCompanyId;

    // List of arguments for SQL statements
    private ArrayList<Object> getArguments;
    private HashMap<String, String> updateKeys;

    // Permission Cache
    private HashMap<String, Object> permissionCache;

    // Data validation
    private DataValidation dataValidation;
    private HashMap<String, HashMap> validatedColumns;

    // Permission validation
    private PermissionValidation perms;

    // List of child objects
    private ArrayList<DataHandlerProperties> children;

    // Constructors
    public DataHandlerProperties() {
        this("");
    }
    public DataHandlerProperties(String getSQL) {
       this(getSQL, new ArrayList<Object>());
    }
    public DataHandlerProperties(String getSQL, ArrayList<Object> getArguments) {
        this.tableName = "";
        this.nameField = "";
        this.getSQL = getSQL;
        this.countSQL = "";
        this.insertSQL = "";
        this.updateSQL = "";
        this.insertSequence = "";
        this.getOwnershipSQL = "";
        this.eventStatusField = "";
        this.primaryKeys = new ArrayList<String>();
        this.dbType = MMHProperties.getAppSetting("database.type");
        this.isDynamicSQL = false;
        this.getRecordUseSingleQuotes = false;
        this.parameterizeGetValues = true;
        this.loggedObject = false;
        this.testingApplication = false;
        this.testingOutputAll = false;
        this.useBeforeGetHook = false;
        this.useAfterGetHook = false;
        this.useBeforeSaveHook = false;
        this.useBeforeCompleteSaveHook = true;
        this.useAfterSaveHook = false;
        this.useGetDeletionsHook = false;
        this.useBeforeInsertHook = false;
        this.useAfterInsertHook = false;
        this.useAfterCompleteInsertHook = false;
        this.useBeforeUpdateHook = false;
        this.useAfterUpdateHook = false;
        this.checkPermissions = true;
        this.negativeNewRecordLookup = false;
        this.partitionPermissionType = 'U';
        this.hardDelete = false;
        this.isMappingObject = false;
        this.mappingArrayLists = new HashMap();
        this.getArguments = getArguments;
        this.keyValues = new HashMap<String, Object>();
        this.updateKeys = new HashMap<String, String>();
        this.cacheCompanyId = 0;
        this.componentId = null;
        this.languageId = null;
        this.maxChildSize = null;
        this.perms = null;
        this.permissionCache = new HashMap<String, Object>();
        this.children = new ArrayList<DataHandlerProperties>();
        try {
            this.dbDateFormat = MMHProperties.getAppSetting("database.dateformat"); // Get the date-time format from the properties
            SimpleDateFormat testFormat = new SimpleDateFormat(this.dbDateFormat); // Validate that the given format works
        } catch (Exception e) {
            this.dbDateFormat = "MM/dd/yyyy hh:mm:ss"; // If no property is given or the test is not successful apply a default
        }
        if (this.dbType.compareToIgnoreCase("ORACLE") == 0) {
            // Set the timestamp to Oracle format
            this.dbTimestamp = "SYS_EXTRACT_UTC(SYSTIMESTAMP)";
            this.dbNextVal = " {1}.NEXTVAL ";
        } else {
            // Assume Microsoft SQL Server if not Oracle (for now)
            this.dbTimestamp = "GETUTCDATE()";
            this.dbNextVal = " NEXT VALUE FOR {1} ";
        }
        //PostgreSQL
        //this.dbTimestamp = "CAST(LOCALTIMESTAMP AT TIME ZONE 'UTC' AS TIMESTAMP)";
        //this.dbNextVal = "nextval('{1}')";
    }

    // Internal method to return strings
    private void setString(String inString, int toSet) {
        if (inString == null) {
            inString = "";
        }
        switch (toSet) {
            case 1:
                this.tableName = inString;
                break;
            case 3:
                this.parentIdentifier = inString;
                break;
            case 4:
                this.nameField = inString;
                break;
            case 5:
                this.getSQL = inString;
                break;
            case 6:
                this.insertSQL = inString;
                break;
            case 7:
                this.updateSQL = inString;
                break;
            case 8:
                this.insertSequence = inString;
                break;
            case 9:
                this.getOwnershipSQL = inString;
                break;
            case 10:
                this.updateKeySingleValue = inString;
                break;
            case 11:
                this.eventStatusField = inString;
                break;
            case 12:
                this.countSQL = inString;
                break;
        }
        return;
    }


    /*
       String inTableName - Table name where child records are being pulled
       String inPrimaryIdentifier - Primary key property for the records
       String inParentIdentifier - If records are children then primary key property for the parent, otherwise empty strings
       String inNameField - If child records where in the parent hashmap they will be stored
       String inGetSQL - Population / Get SQL property name
       String inInsertSQL - If new record run this sql to insert, otherwise will default to a generic insert statement
       String inUpdateSQL - If update scenario run this sql, otherwise will default to a generic update statement
       String insertSequence - If Oracle the sequence name for the table
       String getOwnershipSQL - how to determine ownership priviledges for records, otherwise runs generic get ownership sql statement

     */


    // Utility method to set most of the strings
    public void setSqlStrings(String inTableName, String inPrimaryIdentifier, String inParentIdentifier, String inNameField, String inGetSQL, String inInsertSQL, String inUpdateSQL, String inInsertSequence, String inGetOwnershipSQL) {
        this.tableName = (inTableName == null ? "" : inTableName);
        if (inPrimaryIdentifier != null & inPrimaryIdentifier.length() > 0) {
            this.primaryKeys.add(0, inPrimaryIdentifier);
        }
        this.parentIdentifier = (inParentIdentifier == null ? "" : inParentIdentifier);
        this.nameField = (inNameField == null ? "" : inNameField);
        this.getSQL = (inGetSQL == null ? "" : inGetSQL);
        this.insertSQL = (inInsertSQL == null ? "" : inInsertSQL);
        this.updateSQL = (inUpdateSQL == null ? "" : inUpdateSQL);
        this.insertSequence = (inInsertSequence == null ? "" : inInsertSequence);
        this.getOwnershipSQL = (inGetOwnershipSQL == null ? "" : inGetOwnershipSQL);
        return;
    }

    // Utility method to set the most commonly used items
    public void setDataProperties(String inComponentId, char inPartitionPermissionType, String inTableName, String inPrimaryIdentifier, String inParentIdentifier, String inNameField, String inGetSQL) {
        this.perms = null;
        this.perms = new PermissionValidation(inComponentId);
        this.partitionPermissionType = inPartitionPermissionType;
        this.tableName = (inTableName == null ? "" : inTableName);
        if (inPrimaryIdentifier != null & inPrimaryIdentifier.length() > 0) {
            this.primaryKeys.add(0, inPrimaryIdentifier);
        }
        this.parentIdentifier = (inParentIdentifier == null ? "" : inParentIdentifier);
        this.nameField = (inNameField == null ? "" : inNameField);
        this.getSQL = (inGetSQL == null ? "" : inGetSQL);
        return;
    }

    // BEGIN Identifier string get and set methods
    public String getTableName() {
        return (this.tableName == null ? "" : this.tableName);
    }
    public void setTableName(String inTableName) {
        setString(inTableName, 1);
        return;
    }

    public String getPrimaryIdentifier() {
        if (this.primaryKeys.size() > 0) {
            if (this.primaryKeys.get(0) == null) {
                return "";
            } else {
                return this.primaryKeys.get(0);
            }
        }
        return "";
    }
    public void setPrimaryIdentifier(String inPrimaryIdentifier) {
        if (inPrimaryIdentifier != null & inPrimaryIdentifier.length() > 0) {
            this.primaryKeys.add(0, inPrimaryIdentifier);
        }
        return;
    }

    public String getParentIdentifier() {
        return (this.parentIdentifier == null ? "" : this.parentIdentifier);
    }
    public void setParentIdentifier(String inParentIdentifier) {
        setString(inParentIdentifier, 3);
        return;
    }

    public String getNameField() {
        return (this.nameField == null ? "" : this.nameField);
    }
    public void setNameField(String inNameField) {
        setString(inNameField, 4);
        return;
    }

    public String getGetSQL() {
        return (this.getSQL == null ? "" : this.getSQL);
    }
    public void setGetSQL(String inGetSQL) {
        setString(inGetSQL, 5);
        return;
    }

    public String getCountSQL() {
        return (this.countSQL == null ? "" : this.countSQL);
    }
    public void setCountSQL(String inCountSQL) {
        setString(inCountSQL, 12);
        return;
    }

    public String getInsertSQL() {
        return (this.insertSQL == null ? "" : this.insertSQL);
    }
    public void setInsertSQL(String inInsertSQL) {
        setString(inInsertSQL, 6);
        return;
    }

    public String getUpdateSQL() {
        return (this.updateSQL == null ? "" : this.updateSQL);
    }
    public void setUpdateSQL(String inUpdateSQL) {
        setString(inUpdateSQL, 7);
        return;
    }

    public String getInsertSequence() {
        return (this.insertSequence == null ? "" : this.insertSequence);
    }
    public void setInsertSequence(String inInsertSequence) {
        setString(inInsertSequence, 8);
        return;
    }

    public String getGetOwnershipSQL() {
        return (this.getOwnershipSQL == null ? "" : this.getOwnershipSQL);
    }
    public void setGetOwnershipSQL(String inGetOwnershipSQL) {
        setString(inGetOwnershipSQL, 9);
        return;
    }

    public String getEventStatusField() {
        return (this.eventStatusField == null ? "" : this.eventStatusField);
    }
    public void setEventStatusField(String inEventStatusField) {
        setString(inEventStatusField, 11);
        return;
    }

    public ArrayList<String> getPrimaryKeys() {
        return this.primaryKeys;
    }
    public void addPrimaryKey(String inPrimaryKey) {
        if (inPrimaryKey != null & inPrimaryKey.length() > 0) {
            this.primaryKeys.add(inPrimaryKey);
        }
    }
    public void clearPrimaryKeys() {
        this.primaryKeys.clear();
    }

    public String getUpdateKeySingleValue() {
        return (this.updateKeySingleValue == null ? "" : this.updateKeySingleValue);
    }
    public void setUpdateKeySingleValue(String inUpdateKeySingleValue) {
        setString(inUpdateKeySingleValue, 10);
        return;
    }
    // END Identifier string get and set methods

    // BEGIN Static string get methods
    public String getDbType() {
        return this.dbType;
    }
    public String getDbTimestamp() {
        return this.dbTimestamp;
    }
    public String getDbNextVal() {
        return this.dbNextVal;
    }
    public String getDbDateFormat() {
        return this.dbDateFormat;
    }
    // END Static string get methods

    // BEGIN Boolean get and set methods
    public boolean getGetRecordUseSingleQuotes() {
        return this.getRecordUseSingleQuotes;
    }
    public void setGetRecordUseSingleQuotes(boolean inGetRecordUseSingleQuotes) {
        this.getRecordUseSingleQuotes = inGetRecordUseSingleQuotes;
    }

    public boolean getParameterizeGetValues() {
        return this.parameterizeGetValues;
    }
    public void setParameterizeGetValues(boolean val) {
        this.parameterizeGetValues = val;
    }

    public boolean getLoggedObject() {
        return this.loggedObject;
    }
    public void setLoggedObject(boolean inLoggedObject) {
        this.loggedObject = inLoggedObject;
    }

    public boolean getTestingApplication() {
        return this.testingApplication;
    }
    public void setTestingApplication(boolean inTestingApplication) {
        this.testingApplication = inTestingApplication;
    }

    public boolean getTestingOutputAll() {
        return this.testingOutputAll;
    }
    public void setTestingOutputAll(boolean inTestingOutputAll) {
        this.testingOutputAll = inTestingOutputAll;
    }

    public boolean getUseBeforeGetHook() {
        return useBeforeGetHook;
    }
    public void setUseBeforeGetHook(boolean inUseBeforeGetHook) {
        this.useBeforeGetHook = inUseBeforeGetHook;
    }

    public boolean getUseAfterGetHook() {
        return useAfterGetHook;
    }
    public void setUseAfterGetHook(boolean inUseAfterGetHook) {
        this.useAfterGetHook = inUseAfterGetHook;
    }

    public boolean getUseBeforeSaveHook() {
        return useBeforeSaveHook;
    }
    public void setUseBeforeSaveHook(boolean inUseBeforeSaveHook) {
        this.useBeforeSaveHook = inUseBeforeSaveHook;
    }

    public boolean getUseBeforeCompleteSaveHook () {
        return useBeforeCompleteSaveHook;
    }
    public void setUseBeforeCompleteSaveHook (boolean useBeforeCompleteSaveHook) {
        this.useBeforeCompleteSaveHook = useBeforeCompleteSaveHook;
    }

    public boolean getUseAfterSaveHook() {
        return useAfterSaveHook;
    }
    public void setUseAfterSaveHook(boolean inUseAfterSaveHook) {
        this.useAfterSaveHook = inUseAfterSaveHook;
    }

    public boolean getUseGetDeletionsHook() {
        return useGetDeletionsHook;
    }
    public void setUseGetDeletionsHook(boolean inUseGetDeletionsHook) {
        this.useGetDeletionsHook = inUseGetDeletionsHook;
    }

    public boolean getUseBeforeInsertHook() {
        return useBeforeInsertHook;
    }
    public void setUseBeforeInsertHook(boolean inUseBeforeInsertHook) {
        this.useBeforeInsertHook = inUseBeforeInsertHook;
    }

    public boolean getUseAfterInsertHook() {
        return useAfterInsertHook;
    }
    public void setUseAfterInsertHook(boolean inUseAfterInsertHook) {
        this.useAfterInsertHook = inUseAfterInsertHook;
    }

    public boolean getUseAfterCompleteInsertHook() {
        return useAfterCompleteInsertHook;
    }
    public void setUseAfterCompleteInsertHook(boolean inUseAfterCompleteInsertHook) { this.useAfterCompleteInsertHook = inUseAfterCompleteInsertHook; }

    public boolean getUseBeforeUpdateHook() {
        return useBeforeUpdateHook;
    }
    public void setUseBeforeUpdateHook(boolean inUseBeforeUpdateHook) {
        this.useBeforeUpdateHook = inUseBeforeUpdateHook;
    }

    public boolean getUseAfterUpdateHook() {
        return useAfterUpdateHook;
    }
    public void setUseAfterUpdateHook(boolean inUseAfterUpdateHook) {
        this.useAfterUpdateHook = inUseAfterUpdateHook;
    }

    public boolean getCheckPermissions() {
        return this.checkPermissions;
    }
    public void setCheckPermissions(boolean inCheckPermissions) {
        this.checkPermissions = inCheckPermissions;
    }

    public boolean getHardDelete() {
        return this.hardDelete;
    }
    public void setHardDelete(boolean value) {
        this.hardDelete = value;
    }

    public boolean getIsMappingObject() {
        return this.isMappingObject;
    }
    public void setIsMappingObject(boolean value) {
        this.isMappingObject = value;
    }

    public boolean getPreventAttachRecords() {return preventAttachRecords;}
    public void setPreventAttachRecords(boolean preventAttachRecords) {this.preventAttachRecords = preventAttachRecords;}
    // END Boolean get and set methods

    // BEGIN Integer get and set methods
    public Integer getComponentId() {
        return this.componentId;
    }
    private void setComponentId(Integer inComponentId) {
        this.componentId = inComponentId;
    }

    public Integer getLanguageId() {
        return this.languageId;
    }
    public void setLanguageId(Integer inLanguageId) {
        this.languageId = inLanguageId;
    }

    public Integer getMaxChildSize() {
        return this.maxChildSize;
    }
    public void setMaxChildSize(Integer maxChildSize) {
        this.maxChildSize = maxChildSize;
    }
    // END Integer get and set methods

    // BEGIN char get and set methods
    public char getPartitionPermissionType() {
        return this.partitionPermissionType;
    }
    public void setPartitionPermissionType(char inPermissionType) {
        this.partitionPermissionType = inPermissionType;
    }
    // END char get and set methods

    // BEGIN HashMap entry get and set methods
    public HashMap getMappingArrayLists() {
        return this.mappingArrayLists;
    }
    public void setMappingArrayLists(String identifier, Object value) {
        this.mappingArrayLists.put(identifier, (value == null ? new ArrayList<HashMap>() : value));
    }

    public boolean checkMappingArrayLists(String identifier){
        return mappingArrayLists.containsKey(identifier);
    }

    public Object getMappingArrayListProp(String identifier){
        if (mappingArrayLists.containsKey(identifier)) {
            return mappingArrayLists.get(identifier);
        } else {
            return new Object();
        }
    }
    public void deleteMappingArrayLists(String identifier) {
        /*Iterator<HashMap> it = this.mappingArrayLists.keySet().iterator();
        while (it.hasNext()) {
            it.next();
            if (it.equals(identifier)) {
                it.remove();
            }
        }*/
        this.mappingArrayLists.remove(identifier);
    }
    public void clearMappingArrayLists() {
        this.mappingArrayLists.clear();
    }

    public HashMap<String,Object> getKeyValues() {
        return this.keyValues;
    }
    public void addKeyValues(String key, Object value) {
        this.keyValues.put(key, (value == null ? "" : value));
    }
    public void deleteKeyValues(String key) {
        Iterator it = this.keyValues.keySet().iterator();
        while (it.hasNext()) {
            it.next();
            if (it.equals(key)) {
                it.remove();
            }
        }
    }
    public void clearKeyValues() {
        this.keyValues.clear();
    }
    public Object getPrimaryIdentifierValue() {
        if (this.keyValues.size() > 0) {
            if (this.keyValues.get(this.getPrimaryIdentifier()) == null) {
                if (this.updateKeySingleValue.length() > 0) {
                    return this.updateKeySingleValue;
                }
            } else {
                return this.keyValues.get(this.getPrimaryIdentifier());
            }
        }
        return "";
    }

    public HashMap<String,String> getUpdateKeys() {
        return this.updateKeys;
    }
    public void setUpdateKeys(String identifier, String value) {
        this.updateKeys.put(identifier, (value == null ? "" : value));
    }
    public void deleteUpdateKeys(String identifier) {
        Iterator<String> it = this.updateKeys.keySet().iterator();
        while (it.hasNext()) {
            it.next();
            if (it.equals(identifier)) {
                it.remove();
            }
        }
    }
    public void clearUpdateKeys() {
        this.updateKeys.clear();
    }

    public long getCacheCompanyId() {
        return this.cacheCompanyId;
    }
    public void setCacheCompanyId(long inCacheCompanyId) {
        this.cacheCompanyId = inCacheCompanyId;
    }

    public HashMap getPermissionCache() {
        return this.permissionCache;
    }
    public void setPermissionCache(HashMap inPermissionCache) {
        this.permissionCache = inPermissionCache;
    }

    // END HashMap entry get and set methods

    // BEGIN DataValidation get and set methods
    public DataValidation getDataValidation() {
        return this.dataValidation;
    }
    public void setDataValidation() {
        if (this.tableName.length() > 0) {
            this.dataValidation = new DataValidation(this.tableName, this.dbDateFormat);
            this.validatedColumns = this.dataValidation.getValidationData();
        }
    }
    public HashMap<String, HashMap> getValidatedColumns() {
        return this.validatedColumns;
    }
    // END DataValidation get and set methods

    // BEGIN PermissionValidation methods
    public void setPermission(String userId) {
        if (this.perms == null) {
            this.perms = new PermissionValidation();
        }
        if (!this.perms.getHasCheckedPermissions() && !userId.trim().equals(this.perms.getUserIdString())) {
            this.perms.setPermissions(userId);
        }
    }
    public void setPermission(Long userId) {
        if (this.perms == null) {
            this.perms = new PermissionValidation();
        }
        if (!this.perms.getHasCheckedPermissions() && !userId.equals(this.perms.getUserIdLong())) {
            this.perms.setPermissions(userId);
        }
    }
    public boolean getPermission(char permissionType) {
        if (this.perms == null) {
            this.perms = new PermissionValidation();
        }
        switch (permissionType) {
            case 'V':
                return this.perms.getViewPermission();
            case 'E':
                return this.perms.getEditPermission();
            case 'A':
                return this.perms.getAddPermission();
            case 'D':
                return this.perms.getDeletePermission();
            default:
                return false;
        }
    }

    public boolean isAsyncSave() {
        return AsyncSave;
    }

    public void setAsyncSave(boolean asyncSave) {
        AsyncSave = asyncSave;
    }
    // END PermissionValidation methods

    // BEGIN ArrayList entry get and set methods
    public ArrayList getGetArguments() {
        return this.getArguments;
    }
    public void setGetArguments(ArrayList getArguments){
       this.getArguments = getArguments;
    }
    public void addGetArguments(Object o) {
        if (o != null) {
            this.getArguments.add(o);
        }
    }
    public void addGetArguments(Object o, int i) {
        if (o != null) {
            this.getArguments.add(i, o);
        }
    }
    public void removeGetArguments(int index) {
        this.getArguments.remove(index);
    }
    public void removeGetArguments(Object obj) {
        Iterator it = this.getArguments.iterator();
        while (it.hasNext()) {
            Object o = it.next();
            if (o == obj) {
                it.remove();
            }
        }
    }
    public void clearGetArguments() {
        this.getArguments.clear();
    }
    public ArrayList<DataHandlerProperties> getChildren() {
        return this.children;
    }
    public void addChildren(DataHandlerProperties c) {
        if (c != null) {
            this.children.add(c);
        }
    }
    public void removeChildren(int index) {
        this.children.remove(index);
    }
    public void removeChildren(DataHandlerProperties child) {
        Iterator it = this.getArguments.iterator();
        while (it.hasNext()) {
            Object c = it.next();
            if (c == child) {
                it.remove();
            }
        }
    }
    public boolean getNegativeNewRecordLookup() {
        return negativeNewRecordLookup;
    }
    public void setNegativeNewRecordLookup(boolean negativeNewRecordLookup) {
        this.negativeNewRecordLookup = negativeNewRecordLookup;
    }

    public boolean isDynamicSQL(){
        return this.isDynamicSQL;
    }
    //TODO: we might have to save the sql inside of this, and wrap the property methods around DynamicSQL or provide a stepping stone to the object itself???
    public void setDynamicSQL(DynamicSQL dynamic){
        this.isDynamicSQL = true;
        this.dynamicSQL = dynamic.getSqlStatement();
        this.setGetArguments(dynamic.getParameters());
        this.setParameterizeGetValues(true);
    }
    public String getDynamicSQL(){
        return this.dynamicSQL;
    }
    // END ArrayList entry get and set methods
}
