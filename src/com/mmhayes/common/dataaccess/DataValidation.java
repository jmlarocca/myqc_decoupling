package com.mmhayes.common.dataaccess;

//mmhayes dependencies
import com.mmhayes.common.utils.Logger;
import org.owasp.esapi.ESAPI;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/*
 $Author: ejwhitton $: Author of last commit
 $Date: 2021-09-22 13:42:52 -0400 (Wed, 22 Sep 2021) $: Date of last commit
 $Rev: 15427 $: Revision of last commit
 Notes: Data Manager containing common functionality other data managers use
*/
public class DataValidation {
    DataManager dm = new DataManager();
    String tableName = "";
    commonMMHFunctions commDM = new commonMMHFunctions();
    String dateTimeFormatterString = "";

    public DataValidation(String tableName){
        this(tableName,"MM/dd/yyyy hh:mm:ss");
    }

    public DataValidation(String tableName,String dateTimeFormatterString){
        this.tableName = tableName;
        this.dateTimeFormatterString = dateTimeFormatterString;
    }


    public static void main(String args[]){
      //  DataValidation dv = new DataValidation("QC_PAVendors");
      //  HashMap<String,HashMap> columnData = dv.getValidationData();
    }

    public HashMap<String,HashMap> getValidationData(){
        ArrayList<HashMap> listColumns;
        HashMap<String,HashMap> mapColumns = new HashMap<>();
        try {

            listColumns = dm.serializeSqlWithColNames("data.DataValidation.getColumnData",new Object[]{tableName.toUpperCase()},true);
            for(HashMap column : listColumns){
                String columnName = column.get("COLUMN_NAME").toString().toUpperCase();
                mapColumns.put(columnName,column);
            }
        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            ArrayList<HashMap> errorCodeList = new ArrayList<>();
            errorCodeList.add(commDM.commonErrorMessage("Could not validate record, unable to save"));
            //return errorCodeList;
        }
        return mapColumns;
    }

    public HashMap validateDataSQL (HashMap cur, HashMap<String,HashMap> columns){
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        HashMap returnObj = new HashMap();
        HashMap returnRecord = new HashMap();
        boolean saveable = true;

        try {
            for(Object key : cur.keySet()){
                if(columns.containsKey(key.toString())){
                    HashMap validationData = columns.get(key);
                    String value = cur.get(key).toString();

                    //It appears sometimes array lists, even though they are passed over as empty from the client, still add [].. -jrmitaly
                    //should handle this on the individual DH level, "[" and "]" might be valid characters in a string - rkb

                    //  value = value.toString().replace("[","").replace("]","");




                    String dataType = validationData.get("DATA_TYPE").toString();
                    String isNullable = validationData.get("IS_NULLABLE").toString();

                    if(value.equals(" ") && (dataType.equals("bigint") || dataType.equals("money") || dataType.equals("int") || dataType.equals("numeric"))){
                        if (isNullable.equals("NO")) {
                            value = "";
                        } else {
                            value = null;
                        }
                    }

                    if(value != null && value.isEmpty() && isNullable.equals("YES")) {
                        value = null;
                    } else if (value != null && value.isEmpty() && isNullable.equals("NO")) {
                        Logger.logMessage("ERROR: Column "+key+" has an empty value and is not nullable.");
                    }


                    /*
                    // Change empty strings to null values for consistency
                    if (value == "" && dataType.equals("varchar") && isNullable.equals("YES")) {
                        value = null;
                    }
                    */

                    // Only check type if the value is not null
                    if (value != null) {

                        //remove non valid ASCII characters from value strings AND sanitize strings from potential XSS
                        if ( !value.isEmpty() ) {

                            String beforeVal = new StringBuffer(value).toString();

                            //sanitize strings from potential XSS
                            value = commonMMHFunctions.sanitizeStringFromXSS(value);

                            //decode to fix issues where encoded values are too large for field size - ecd 08/11/17
                            value =  ESAPI.encoder().decodeForHTML(value);

                            //ASCII Chars 32-126 are GOOD Chars (Regex says: find characters that are NOT between ASCII Char 32 (x20) through ASCII Char 126 (x7E)) -jrmitaly 3/32/2017
                            value = value.replaceAll("[^\\x20-\\x7e]", "");

                            if(value.trim().isEmpty() && !beforeVal.trim().isEmpty()) {
                                errorCodeList.add(commDM.commonErrorMessage("Field : " + key.toString()+ " is now empty after sanitizing. Changes will not be saved."));
                                saveable = false;
                            }

                            if(key.equals("EXPRESSION")) {
                                value = value.replaceAll("_lt_", "<");
                                value = value.replaceAll("_gt_", ">");
                            }
                        }

                        //if empty value but column is not nullable
                        if(isNullable.equals("NO") && !value.isEmpty() && value.equals("")){
                            //if not default value then need to take out field
                            if(validationData.get("COLUMN_DEFAULT").toString().isEmpty()){
                                saveable=false;
                                errorCodeList.add(commDM.commonErrorMessage("Field : " + key.toString()+ " does not accept a blank value"));
                            }
                            //if theres a default value put it into value
                            else{
                                value=validationData.get("DATA_DEFAULT").toString();
                            }
                        }

                        //anything with string
                        if(dataType.equals("varchar")){
                            if(Integer.parseInt(validationData.get("CHARACTER_MAXIMUM_LENGTH").toString() ) < value.length()){
                                saveable=false;
                                errorCodeList.add(commDM.commonErrorMessage("Field "+key.toString()+" with value : " + value + "\nLength : "+value.length()+" when the maximum length is " +validationData.get("CHARACTER_MAXIMUM_LENGTH").toString()));
                            }
                        }

                        //a char needs to be exactly the same length
                        if(dataType.equals("char")){
                            if(Integer.parseInt(validationData.get("CHARACTER_MAXIMUM_LENGTH").toString() ) != value.length()){
                                saveable=false;
                                errorCodeList.add(commDM.commonErrorMessage("Field "+key.toString()+" with value : " + value + "\nLength : "+value.length()+" when the maximum length is " +validationData.get("CHARACTER_MAXIMUM_LENGTH").toString()));
                            }
                        }

                        if(dataType.equals("bit")){
                            //if the value true gets through from front end change it to a 1
                            if(value.equals("true") || value.equals("TRUE") || value.equals("True")){
                                value="1";
                            }
                            //if the value false gets through from front end change to 0
                            else if(value.equals("false") || value.equals("FALSE") || value.equals("False")){
                                value="0";
                            }
                            //if not a 1 or 0 at this point it fails
                            if(!value.equals("1") && !value.equals("0")){
                                saveable=false;
                                errorCodeList.add(commDM.commonErrorMessage("Field "+key.toString()+" with value : " + value + " can only accept true or false values"));
                            }
                        }

                        if(dataType.equals("bigint")){
                            try{
                                value=value.replace(",","");
                                if(value.isEmpty() && isNullable.equals("YES")){
                                    //  continue;
                                }else{
                                    long temp = Long.valueOf(value);
                                }

                            }catch (Exception e){
                                saveable=false;
                                errorCodeList.add(commDM.commonErrorMessage("Field "+key.toString()+" with value : " + value + " could not be converted to a float number to be saved"));
                            }
                        }

                        if(dataType.equals("int")){
                            try{
                                value=value.replace(",","");
                                if(value.isEmpty() && isNullable.equals("YES")){
                                    //  continue;
                                }else{
                                    int temp = Integer.valueOf(value);
                                }

                            }catch (Exception e){
                                saveable=false;
                                errorCodeList.add(commDM.commonErrorMessage("Field "+key.toString()+" with value : " + value + " could not be converted to an integer number to be saved"));
                            }
                        }


                        if(dataType.equals("money") || (dataType.equals("numeric") && Integer.parseInt(validationData.get("NUMERIC_SCALE").toString()) > 0)){
                            try{
                                value=value.replace(",","");
                                if(value.isEmpty() && isNullable.equals("YES")){
                                    //  continue;
                                }else{
                                    double temp = Double.valueOf(value);
                                }

                            }catch (Exception e){
                                saveable=false;
                                errorCodeList.add(commDM.commonErrorMessage("Field "+key.toString()+" with value : " + value + " could not be converted to a decimal number to be saved"));
                            }
                        }
                        if(dataType.equals("DATE")){
                            try{
                                if(!value.equals("NULL")){
                                    SimpleDateFormat df = (SimpleDateFormat)SimpleDateFormat.getDateInstance();
                                    df.applyPattern(dateTimeFormatterString);
                                    value = df.format(value);
                                }
                            }catch (Exception e){
                                errorCodeList.add(commDM.commonErrorMessage("Field : " + key.toString()+" could not be converted to the date format"));
                                saveable=false;
                            }

                        }
                    }

                    if(saveable){
                        returnRecord.put(key.toString().toUpperCase(),value);
                    }
                    else{
                        returnObj.put("errorCodeList",errorCodeList);
                        return returnObj;
                    }
                }else{
                    errorCodeList.add(commDM.commonErrorMessage("Field : " + key.toString()+" is not recognized and will not be saved"));
                }
            }

            returnObj.put("validatedRecord",returnRecord);

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            errorCodeList.add(commDM.commonErrorMessage("Fatal error during data validation."));
        }

        returnObj.put("errorCodeList",errorCodeList);
        return returnObj;
    }

    public HashMap validateDataOracle(HashMap cur, HashMap<String,HashMap> columns){
        ArrayList<HashMap> errorCodeList = new ArrayList<>();
        HashMap returnObj = new HashMap();
        HashMap returnRecord = new HashMap();
        boolean saveable=true;
        for(Object key : cur.keySet()){
            if(columns.containsKey(key.toString())){
                HashMap validationData = columns.get(key);
                String value = cur.get(key).toString();
                boolean isDate = false;
                String dataType = validationData.get("DATA_TYPE").toString();
                value = value.replace("[","").replace("]","");
                value = value.replaceAll("'","''"); // replace all ' with ''

                if(dataType.equals("NUMBER") && value.equals(" ")){
                    if (validationData.get("NULLABLE").toString().equals("N")) {
                        value = "";
                    } else {
                        value = null;
                    }
                }

                /*
                // Change empty strings to null values for consistency
                if (value == "" && dataType.equals("varchar") && isNullable.equals("YES")) {
                    value = null;
                }
                */

                //if empty value but column is not nullable and doesnt have a default value
                if(value.isEmpty() && validationData.get("NULLABLE").toString().equals("N")){
                    continue;
                }

                // Only check type if the value is not null
                if (value != null) {

                    if(!value.isEmpty() && validationData.get("NULLABLE").toString().equals("N") && value.equals("")){
                        //if not default value then need to take out field
                        if(validationData.get("DATA_DEFAULT").toString().isEmpty()){
                            saveable=false;
                            errorCodeList.add(commDM.commonErrorMessage("Field : " + key.toString()+ " does not accept a blank value"));
                        }
                        //if theres a default value put it into value
                        else{
                            value=validationData.get("DATA_DEFAULT").toString();
                        }
                    }
                    //anything with string
                    if(dataType.equals("VARCHAR2")){
                        if(Integer.parseInt(validationData.get("DATA_LENGTH").toString() ) < value.length()){
                            saveable=false;
                            errorCodeList.add(commDM.commonErrorMessage("Field "+key.toString()+" with value : " + value + "\n Length : "+value.length()+" when the maximum length is " +validationData.get("DATA_LENGTH").toString()));
                        }
                    }

                    if(dataType.equals("NUMBER")){
                        //boolean need to make sure it fits
                        if(validationData.get("DATA_SCALE").toString().equals("0") && validationData.get("DATA_PRECISION").equals("1")){
                            //if the value true gets through from front end change it to a 1
                            if(value.equals("true") || value.equals("TRUE") || value.equals("True")){
                                value="1";
                            }
                            //if the value false gets through from front end change to 0
                            else if(value.equals("false") || value.equals("FALSE") || value.equals("False")){
                                value="0";
                            }
                            //if not a 1 or 0 at this point it fails
                            if(!value.equals("1") && !value.equals("0")){
                                saveable=false;
                                errorCodeList.add(commDM.commonErrorMessage("Field "+key.toString()+" with value : " + value + " can only accept true or false values"));
                            }
                        }

                        //is an integer data type in the database, why would we use this when everything else is
                        //number(18,0)
                        if(!value.isEmpty() && validationData.get("DATA_PRECISION").toString().isEmpty() && validationData.get("DATA_SCALE").toString().isEmpty()){
                            try{
                                value=value.replace(",","");
                                double temp = Double.valueOf(value);
                            }catch (Exception e){
                                saveable=false;
                                errorCodeList.add(commDM.commonErrorMessage("Field "+key.toString()+" with value : " + value + " could not be converted to a integer number to be saved"));
                            }
                        }

                        //if an integer or no decimal place
                        else if(!value.isEmpty() && Integer.parseInt(validationData.get("DATA_PRECISION").toString()) > 1 && validationData.get("DATA_SCALE").toString().equals("0")){
                            try{
                                value=value.replace(",","");
                                long temp = Long.valueOf(value);
                            }catch (Exception e){
                                saveable=false;
                                errorCodeList.add(commDM.commonErrorMessage("Field "+key.toString()+" with value : " + value + " could not be converted to a integer number to be saved"));
                            }
                        }

                        //if a double
                        //if an integer or no decimal place
                        else if(!value.isEmpty() && Integer.parseInt(validationData.get("DATA_PRECISION").toString()) > 1 && Integer.parseInt(validationData.get("DATA_SCALE").toString()) > 0){
                            try{
                                value=value.replace(",","");
                                double temp = Double.valueOf(value);
                            }catch (Exception e){
                                saveable=false;
                                errorCodeList.add(commDM.commonErrorMessage("Field "+key.toString()+" with value : " + value + " could not be converted to a decimal number to be saved"));
                            }
                        }
                    }
                    if(dataType.equals("DATE")){
                        try{
                            Date now = commDM.formatDate(dateTimeFormatterString, value);
                            isDate=true;
                            returnRecord.put(key.toString().toUpperCase(),now);
                        }catch (Exception e){
                            errorCodeList.add(commDM.commonErrorMessage("Field "+key.toString()+" with value : " + value + " could not be converted to the date format"));
                            saveable=false;
                        }
                    }
                }
                if(saveable && !isDate){
                    returnRecord.put(key.toString().toUpperCase(),value);
                }
                if(!saveable){
                    returnObj.put("errorCodeList",errorCodeList);
                    return returnObj;
                }

            }else{
                errorCodeList.add(commDM.commonErrorMessage("Field : " + key.toString()+" is not recognized and will not be saved"));
            }
        }
        returnObj.put("validatedRecord",returnRecord);
        returnObj.put("errorCodeList",errorCodeList);

        return returnObj;
    }


//START OTHER METHODS

    //prevents duplication of quotes (looks for single quotes before and after each single quote)
    public Boolean checkSingleQuote (String value) {
        String valueAsString = value.toString();
        Boolean escapeQuotes = false;
        for(Integer i=0; i < valueAsString.length(); i++) {
            if (valueAsString.charAt(i) == '\'' ) {
                if (i-1 >= 0) {
                    if (valueAsString.charAt(i-1) == '\'' ) {
                        //leave as this quote has already been escaped
                    } else { //this quote is either escaping or has not been escaped
                        if (i+1 <= valueAsString.length()) {
                            if (valueAsString.charAt(i+1) == '\'' ) {
                                //this quote is escaping, so leave it alone
                            } else {
                                //this quote needs to be escaped
                                escapeQuotes = true;
                            }
                        } else { //this quote is at the end of the string, so escape it
                            escapeQuotes = true;
                        }
                    }
                }
            }
        }
        return escapeQuotes;
    }

//END OTHER METHODS

}
