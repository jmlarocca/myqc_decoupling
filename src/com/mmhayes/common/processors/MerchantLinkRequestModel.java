package com.mmhayes.common.processors;

import com.google.gson.JsonObject;
import com.mmhayes.common.utils.Logger;

/**
 * Created by nyu on 8/7/2017.
 */
public abstract class MerchantLinkRequestModel
{
    private String command = "";
    private String clientMac = "";
    private String lane = "";
    private String id = "";
    private String clientID = "";
    private String companyCode = "";
    private String siteCode = "";
    private String terminalID = "";
    private String transLinkMac = "";               // Last 4 characters of TransactionLink mac address
    private String qcposTerminalID = "";

    //region Basic Json Obj Generation

    /**
     * @return JsonObject with Command and ClientMac properties
     */
    public JsonObject getBasicJsonObj()
    {
        JsonObject plRequestJSONObj = new JsonObject();
        try
        {
            plRequestJSONObj.addProperty("Command", command);
            plRequestJSONObj.addProperty("ClientMAC", clientMac);
        }
        catch (Exception ex)
        {
            Logger.logMessage("ERROR: MerchantLinkRequestModel.getBasicJsonObj");
            Logger.logException(ex);
        }

        return plRequestJSONObj;
    }

    /**
     * @return JsonObject with Command, ClientMac, and Lane properties
     */
    public JsonObject getBasicJsonObjWithLane()
    {
        JsonObject plRequestJSONObj = new JsonObject();
        try
        {
            plRequestJSONObj.addProperty("Command", command);
            plRequestJSONObj.addProperty("ClientMAC", clientMac);
            plRequestJSONObj.addProperty("Lane", lane);
        }
        catch (Exception ex)
        {
            Logger.logMessage("ERROR: MerchantLinkRequestModel.getBasicJsonObjWithLane");
            Logger.logException(ex);
        }

        return plRequestJSONObj;
    }

    /**
     * @return JsonObject with all properties
     */
    public JsonObject getJsonObj()
    {
        JsonObject plRequestJSONObj = getBasicJsonObjWithLane();
        try
        {
            plRequestJSONObj.addProperty("Id", id);
            plRequestJSONObj.addProperty("ClientId", clientID);
            plRequestJSONObj.addProperty("CompanyCode", companyCode);
            plRequestJSONObj.addProperty("SiteCode", siteCode);
            plRequestJSONObj.addProperty("TerminalId", terminalID);
        }
        catch (Exception ex)
        {
            Logger.logMessage("ERROR: MerchantLinkRequestModel.getJsonObj");
            Logger.logException(ex);
        }

        return plRequestJSONObj;
    }
    
    /**
     * @return JsonObject with PLRequest object: Command and ClientMAC
     */
    public JsonObject getEnclosedBasicJsonObj()
    {
        JsonObject basicJSONObj = new JsonObject();

        try
        {
            JsonObject plRequestJSONObj = getBasicJsonObj();
            basicJSONObj.add("PLRequest", plRequestJSONObj);
        }
        catch (Exception ex)
        {
            Logger.logMessage("ERROR: MerchantLinkRequestModel.getEnclosedBasicJsonObj");
            Logger.logException(ex);
        }

        return basicJSONObj;
    }

    /**
     * @return JsonObject with PLRequest object: Command, ClientMAC, and Lane
     */
    public JsonObject getEnclosedBasicJsonObjWithLane()
    {
        JsonObject basicJSONObj = new JsonObject();

        try
        {
            JsonObject plRequestJSONObj = getBasicJsonObjWithLane();

            basicJSONObj.add("PLRequest", plRequestJSONObj);
        }
        catch (Exception ex)
        {
            Logger.logMessage("ERROR: MerchantLinkRequestModel.getEnclosedBasicJsonObjWithLane");
            Logger.logException(ex);
        }

        return basicJSONObj;
    }
    //endregion
    
    //region Getters and Setters
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getClientMac() {
        return clientMac;
    }

    public void setClientMac(String clientMac) {
        this.clientMac = clientMac;
    }

    public String getLane() {
        return lane;
    }

    public void setLane(String lane) {
        this.lane = lane;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getTransLinkMac() {
        return transLinkMac;
    }

    public void setTransLinkMac(String transLinkMac) {
        this.transLinkMac = transLinkMac;
    }

    public String getQcposTerminalID()
    {
        return qcposTerminalID;
    }

    public void setQcposTerminalID(String qcposTerminalID)
    {
        this.qcposTerminalID = qcposTerminalID;
    }

    public String getLogFileName()
    {
        if (Integer.parseInt(qcposTerminalID) <= 0)
        {
            return "QCPOS_COMMON_T.log";
        }
        else
        {
            return "QCPOS_" + qcposTerminalID + "_T.log";
        }
    }

    //endregion
}
