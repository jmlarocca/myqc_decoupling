package com.mmhayes.common.processors.simplifyfusebox.models;

//MMHayes Dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.conn_pool.JDCConnection;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.processors.simplifyfusebox.collections.PaymentGatewayInquiryTenderCollection;
import com.mmhayes.common.transaction.models.OtmModel;
import com.mmhayes.common.utils.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//API Dependencies
//Other Dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-12-09 14:12:04 -0500 (Mon, 09 Dec 2019) $: Date of last commit
 $Rev: 10252 $: Revision of last commit
*/

public class PaymentGatewayInquiryTransaction {
    DataManager dm = new DataManager();
    private Integer id = null;
    private Integer transactionStatusId = null;
    private Integer linkedPATransactionId = null;
    private Integer terminalId = null;
    private String logFileName = "";
    private List<PaymentGatewayInquiryTender> tenders = new ArrayList();
    private OtmModel otmModel = null;

    public PaymentGatewayInquiryTransaction() {

    }

    public PaymentGatewayInquiryTransaction(HashMap paymentGatewayInqiryTransactionHM, String logFileName) throws Exception {
        setModelProperties(paymentGatewayInqiryTransactionHM);
        setLogFileName(logFileName);
    }

    //setter for all of this model's properties
    public PaymentGatewayInquiryTransaction setModelProperties(HashMap modelDetailHM) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setTransactionStatusId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PATRANSSTATUSID")));
        setLinkedPATransactionId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("LINKEDTOPATRANSACTIONID")));
        setTerminalId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALID")));

        PaymentGatewayInquiryTenderCollection paymentGatewayInquiryTenderCollection = new PaymentGatewayInquiryTenderCollection().getAllPaymentGatewayInquiryTendersByTransId(this.getId(), this.getLogFileName());
        setTenders(paymentGatewayInquiryTenderCollection.getCollection());

        return this;
    }

    public void validateForUpdate() throws Exception {
        boolean isValid = true;

        if (this.getId() == null || this.getId().toString().isEmpty()) {
            isValid = false;
        }

        if (!isValid) {
            throw new MissingDataException("Cannot update transaction, missing data.", this.getTerminalId());
        }

        for (PaymentGatewayInquiryTender paymentGatewayInquiryTender : this.getTenders()) {
            if ((paymentGatewayInquiryTender.getCreditCardTransInfo() != null && !paymentGatewayInquiryTender.getCreditCardTransInfo().isEmpty()) ||
                    (paymentGatewayInquiryTender.getSaleAuthDetails() != null && !paymentGatewayInquiryTender.getSaleAuthDetails().isEmpty())) {

                //If CreditCardTransInfo or ItemComment is populated, paTransLineItemId must be populated
                if (paymentGatewayInquiryTender.getId() == null || paymentGatewayInquiryTender.getId().toString().isEmpty()) {
                    isValid = false;
                }
            }
        }

        if (!isValid) {
            throw new MissingDataException("Cannot update transaction line item, missing data.", logFileName);
        }
    }

    public void update() throws Exception {

        String logFileName = this.getLogFileName();

        int saveStatus = 0;
        Exception mainEx = null;

        JDCConnection conn = null;
        try {
            conn = dm.pool.getConnection(false, logFileName);
        } catch (SQLException qe) {
            Logger.logMessage("PaymentGatewayInquiryTransaction.updateTransaction no connection to DB.", logFileName);
            Logger.logMessage(qe.getMessage(), logFileName);
            throw new MissingDataException("Could not establish a connection to the database.", logFileName);
        }

        try {

            conn.setAutoCommit(false); //make sure the save is atomic

            //save transaction and transLineItems
            this.updatePaTransaction(conn, logFileName);

            for (PaymentGatewayInquiryTender paymentGatewayInquiryTender : this.getTenders()) {
                updatePaTransLine(conn, logFileName, paymentGatewayInquiryTender);
            }

            saveStatus = 1;
            conn.commit();
            Logger.logMessage("Transaction Saved Successfully (TransactionID=" + this.getId() + ")", logFileName, Logger.LEVEL.DEBUG);
        } catch (Exception ex) {
            Logger.logMessage("Transaction Failed (TransactionID=" + this.getId() + ")", logFileName, Logger.LEVEL.ERROR);
            mainEx = ex;

            try {
                conn.rollback();
                //conn.setAutoCommit(true);  // THIS WAS ALREADY COMMENTED OUT ... SHOULD IT HAVE BEEN?
                Logger.logMessage("Rollback Successfully (Transaction ID=" + this.getId() + ")", logFileName, Logger.LEVEL.ERROR);
            } catch (SQLException e) {
                Logger.logException("PaymentGatewayInquiryTransaction.updateTransaction. SQL exception", logFileName, e);
            }
            saveStatus = -1;
            Logger.logException(ex, logFileName);
        } finally {
            try {
                conn.setAutoCommit(true);
                dm.pool.returnConnection(conn, logFileName);
                Logger.logMessage("Database Transaction Committed Successfully (TransactionID=" + this.getId() + ")", logFileName, Logger.LEVEL.DEBUG);
            } catch (SQLException ex) {
                Logger.logException(ex, logFileName);
            }
        }

        //Now the the "finally" is done and the database should be cleaned up
        //if the save did not occur properly, return the error to the API Client
        if (saveStatus < 1) {
            if (mainEx != null) {
                throw mainEx;
            } else {
                throw new MissingDataException("Error Saving Payment Gateway Inquiry Transaction.  Check logs for more details.", logFileName);
            }
        }
    }

    /**
     * Update PATransaction data.
     * The Transaction Status ID
     * If the Linked
     *
     * @param conn
     * @param logFileName
     * @throws Exception
     */
    private void updatePaTransaction(JDCConnection conn, String logFileName) throws Exception {

        Logger.logMessage("Updating Transaction Data. PaymentGatewayInquiryTransaction.updatePaTransaction.", logFileName, Logger.LEVEL.DEBUG);


        Integer numUpdatedRecords = 0;
        if (this.getTransactionStatusId() == null) {

            numUpdatedRecords = dm.parameterizedExecuteNonQuery("data.posapi30.updatePaymentGatewayInquiryTransactionLinkedTransId", new Object[]{
                    this.getId(),
                    this.getLinkedPATransactionId()

            }, logFileName, conn);
        } else {

            //Integer numUpdatedRecords = 0;
            numUpdatedRecords = dm.parameterizedExecuteNonQuery("data.posapi30.updatePaymentGatewayInquiryTransaction", new Object[]{

                    this.getId(),
                    this.getTransactionStatusId(),
                    this.getLinkedPATransactionId()

            }, logFileName, conn);
        }

        if (numUpdatedRecords <= 0) {
            throw new MissingDataException("Payment Gateway Transaction could not be updated.", logFileName);
        }
    }

    /**
     * Update the PATransLineItem for the Credit Card Tender
     * Only update the CreditCardTransInfo if it is populated
     * Only update the Item Comment if it is populated
     *
     * @param conn
     * @param logFileName
     * @param paymentGatewayInquiryTender
     * @throws Exception
     */
    private void updatePaTransLine(JDCConnection conn, String logFileName, PaymentGatewayInquiryTender paymentGatewayInquiryTender) throws Exception {
        Logger.logMessage("Updating Transaction Data. PaymentGatewayInquiryTransaction.updateTransLine.", logFileName, Logger.LEVEL.DEBUG);

        boolean updateItemComment = false;
        boolean updateCreditCardTransInfo = false;

        if (paymentGatewayInquiryTender.getCreditCardTransInfo() != null &&
                !paymentGatewayInquiryTender.getCreditCardTransInfo().isEmpty()) {
            updateCreditCardTransInfo = true;
        }

        if (paymentGatewayInquiryTender.getSaleAuthDetails() != null &&
                !paymentGatewayInquiryTender.getSaleAuthDetails().isEmpty()) {
            updateItemComment = true;
        }

        if (updateCreditCardTransInfo && updateItemComment) {

            Integer numUpdatedRecords = 0;
            numUpdatedRecords = dm.parameterizedExecuteNonQuery("data.posapi30.updateTransLineCCInfoAndItemComment", new Object[]{

                    paymentGatewayInquiryTender.getId(), //paTransLineItemId
                    paymentGatewayInquiryTender.getCreditCardTransInfo(),
                    paymentGatewayInquiryTender.getSaleAuthDetails()
            }, logFileName, conn);

            if (numUpdatedRecords <= 0) {
                throw new MissingDataException("Trans Line Item could not be updated.", logFileName);
            }

        } else if (updateCreditCardTransInfo && !updateItemComment) {

            Integer numUpdatedRecords = 0;
            numUpdatedRecords = dm.parameterizedExecuteNonQuery("data.posapi30.updateTransLineCCInfo", new Object[]{
                    paymentGatewayInquiryTender.getId(), //paTransLineItemId
                    paymentGatewayInquiryTender.getCreditCardTransInfo()

            }, logFileName, conn);

            if (numUpdatedRecords <= 0) {
                throw new MissingDataException("Trans Line Item could not be updated.", logFileName);
            }

        } else if (!updateCreditCardTransInfo && updateItemComment) {

            Integer numUpdatedRecords = 0;
            numUpdatedRecords = dm.parameterizedExecuteNonQuery("data.posapi30.updateTransLineItemComment", new Object[]{
                    paymentGatewayInquiryTender.getId(), //paTransLineItemId
                    paymentGatewayInquiryTender.getSaleAuthDetails(),

            }, logFileName, conn);

            if (numUpdatedRecords <= 0) {
                throw new MissingDataException("Trans Line Item could not be updated.", logFileName);
            }
        }
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTransactionStatusId() {
        return transactionStatusId;
    }

    public void setTransactionStatusId(Integer transactionStatusId) {
        this.transactionStatusId = transactionStatusId;
    }

    public Integer getLinkedPATransactionId() {
        return linkedPATransactionId;
    }

    public void setLinkedPATransactionId(Integer linkedPATransactionId) {
        this.linkedPATransactionId = linkedPATransactionId;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    @JsonIgnore
    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }

    @JsonIgnore
    public OtmModel getOtmModel() {
        return otmModel;
    }

    public void setOtmModel(OtmModel otmModel) {
        this.otmModel = otmModel;
    }

    public List<PaymentGatewayInquiryTender> getTenders() {
        return tenders;
    }

    public void setTenders(List<PaymentGatewayInquiryTender> tenders) {
        this.tenders = tenders;
    }
}
