package com.mmhayes.common.processors.simplifyfusebox.models;


//MMHayes Dependencies
import com.mmhayes.common.api.CommonAPI;

//API Dependencies
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.annotation.*;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

//other dependencies
import java.math.BigDecimal;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-11-14 15:15:49 -0500 (Thu, 14 Nov 2019) $: Date of last commit
 $Rev: 10021 $: Revision of last commit
*/

public class PaymentGatewayInquiryTender {
    private Integer id = null;
    private BigDecimal amount = null;
    private String saleAuthDetails = null;
    private String creditCardTransInfo = null;
    private String logFileName = "";

    public PaymentGatewayInquiryTender(){

    }

    public PaymentGatewayInquiryTender(HashMap paymentGatewayInquiryTenderHM, String logFileName){
        setModelProperties(paymentGatewayInquiryTenderHM);
        setLogFileName(logFileName);
    }

    //setter for all of this model's properties
    public PaymentGatewayInquiryTender setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setAmount(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("EXTENDEDAMOUNT")));
        setSaleAuthDetails(CommonAPI.convertModelDetailToString(modelDetailHM.get("ITEMCOMMENT")));
        setCreditCardTransInfo(CommonAPI.convertModelDetailToString(modelDetailHM.get("CREDITCARDTRANSINFO")));

        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getSaleAuthDetails() {
        return saleAuthDetails;
    }

    public void setSaleAuthDetails(String saleAuthDetails) {
        this.saleAuthDetails = saleAuthDetails;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getCreditCardTransInfo() {
        return creditCardTransInfo;
    }

    public void setCreditCardTransInfo(String creditCardTransInfo) {
        this.creditCardTransInfo = creditCardTransInfo;
    }

    @JsonIgnore
    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }
}
