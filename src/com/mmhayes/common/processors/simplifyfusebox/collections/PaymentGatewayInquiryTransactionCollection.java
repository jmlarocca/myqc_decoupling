package com.mmhayes.common.processors.simplifyfusebox.collections;

//MMHayes Dependencies
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.processors.simplifyfusebox.models.*;
import com.mmhayes.common.transaction.models.OtmModel;
import com.mmhayes.common.api.PosApi30.PosAPIHelper.*;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-11-14 15:15:49 -0500 (Thu, 14 Nov 2019) $: Date of last commit
 $Rev: 10021 $: Revision of last commit
*/

public class PaymentGatewayInquiryTransactionCollection {
    DataManager dm = new DataManager();
    private List<PaymentGatewayInquiryTransaction> collection = new ArrayList<>();
    private String logFileName = "";

    public PaymentGatewayInquiryTransactionCollection getAllInquiryTransByOtmId(OtmModel otmModel, Integer paymentProcessorId, String logFileName) throws Exception {
        setLogFileName(logFileName);

        //get all models in an array list
        ArrayList<HashMap> gatewayInquiryTransHMList = dm.parameterizedExecuteQuery("data.posapi30.getAllPaymentGatewayInquiryTransactions",
                new Object[]{otmModel.getId(), paymentProcessorId, TransactionType.PAYMENT_GATEWAY_INQUIRE.toInt(), TransactionStatus.PAYMENT_GATEWAY_INQUIRE_NEEDED.toInt() },
                logFileName,
                true
        );

        PaymentGatewayInquiryTransactionCollection paymentGatewayInquiryTransactions = new PaymentGatewayInquiryTransactionCollection();

        for (HashMap paymentGatewayInquiryTransHM : gatewayInquiryTransHMList) {

            //create a new model and add to the collection
            paymentGatewayInquiryTransactions.getCollection().add(new PaymentGatewayInquiryTransaction(paymentGatewayInquiryTransHM, getLogFileName()));
        }

        return paymentGatewayInquiryTransactions;

    }

    public List<PaymentGatewayInquiryTransaction> getCollection() {
        return collection;
    }

    public void setCollection(List<PaymentGatewayInquiryTransaction> collection) {
        this.collection = collection;
    }

    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }
}
