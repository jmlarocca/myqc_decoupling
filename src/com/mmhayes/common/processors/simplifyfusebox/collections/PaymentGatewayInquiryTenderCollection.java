package com.mmhayes.common.processors.simplifyfusebox.collections;

//MMHayes Dependencies
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.processors.simplifyfusebox.models.*;

//API Dependencies
import com.fasterxml.jackson.annotation.*;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-11-14 15:15:49 -0500 (Thu, 14 Nov 2019) $: Date of last commit
 $Rev: 10021 $: Revision of last commit
*/

public class PaymentGatewayInquiryTenderCollection {
    DataManager dm = new DataManager();
    private List<PaymentGatewayInquiryTender> collection = new ArrayList<>();
    private String logFileName = "";

    public PaymentGatewayInquiryTenderCollection getAllPaymentGatewayInquiryTendersByTransId(Integer paTransId, String logFileName) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> paymentGatewayInquiryTenderHMList = dm.parameterizedExecuteQuery("data.posapi30.getAllPaymentGatewayInquiryTendersByTransId",
                new Object[]{ paTransId },
                logFileName,
                true
        );

        PaymentGatewayInquiryTenderCollection paymentGatewayInquiryTenderCollection = new PaymentGatewayInquiryTenderCollection();
        for (HashMap paymentGatewayInquiryTenderHM : paymentGatewayInquiryTenderHMList) {

            //create a new model and add to the collection
            paymentGatewayInquiryTenderCollection.getCollection().add(new PaymentGatewayInquiryTender(paymentGatewayInquiryTenderHM, getLogFileName()));
        }

        return paymentGatewayInquiryTenderCollection;
    }

    public List<PaymentGatewayInquiryTender> getCollection() {
        return collection;
    }

    public void setCollection(List<PaymentGatewayInquiryTender> collection) {
        this.collection = collection;
    }

    @JsonIgnore
    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }
}