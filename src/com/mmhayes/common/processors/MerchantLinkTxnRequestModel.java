package com.mmhayes.common.processors;

import com.google.gson.JsonObject;
import com.mmhayes.common.utils.Logger;

/**
 * Created by nyu on 8/7/2017.
 */
public class MerchantLinkTxnRequestModel extends MerchantLinkRequestModel
{
    private String transactionDate = "";
    private String transactionTime = "";

    // For CCSALE
    private String amount = "";
    private String input = "";
    private String token = "";
    private String safEnable = "";
    private String expiryDate = "";

    // For CCVOID
    private String gwTranId = "";
    private String cardNumber = "";
    private String recNum = "";

    // For CCPREAUTH
    private String authAmt = "";
    
    public MerchantLinkTxnRequestModel()
    {
    }
    
    //region getters and setters
    /**
     * @return All properties on MerchantLinkRequestLink, plus TransactionDate and TransactionTime
     */
    public JsonObject getTxnJsonObj()
    {
        JsonObject plRequestJSONObj = getJsonObj();
        try
        {
            plRequestJSONObj.addProperty("TransactionDate", transactionDate);
            plRequestJSONObj.addProperty("TransactionTime", transactionTime);
        }
        catch (Exception ex)
        {
            Logger.logMessage("ERROR: MerchantLinkRequestModel.getTxnJsonObj");
            Logger.logException(ex);
        }

        return plRequestJSONObj;
    }
    
    public String getAuthAmt() {
        return authAmt;
    }

    public void setAuthAmt(String authAmt) {
        this.authAmt = authAmt;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSafEnable() {
        return safEnable;
    }

    public void setSafEnable(String safEnable) {
        this.safEnable = safEnable;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getGwTranId() {
        return gwTranId;
    }

    public void setGwTranId(String gwTranId) {
        this.gwTranId = gwTranId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getRecNum() {
        return recNum;
    }

    public void setRecNum(String recNum) {
        this.recNum = recNum;
    }
    //endregion
}
