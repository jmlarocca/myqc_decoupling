
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HotelData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HotelData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="expectedDuration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folioNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noShow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="programCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="checkinDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="checkoutDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="extraChargeTypes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="roomRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="roomRateUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="roomTax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="extraChargeTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="renterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelData", propOrder = {

})
public class HotelData {

    protected String expectedDuration;
    protected String folioNumber;
    protected String noShow;
    protected String programCode;
    protected String checkinDate;
    protected String checkoutDate;
    protected String extraChargeTypes;
    protected String roomRate;
    protected String roomRateUnit;
    protected String roomTax;
    protected String extraChargeTotal;
    protected String renterName;

    /**
     * Gets the value of the expectedDuration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedDuration() {
        return expectedDuration;
    }

    /**
     * Sets the value of the expectedDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedDuration(String value) {
        this.expectedDuration = value;
    }

    /**
     * Gets the value of the folioNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioNumber() {
        return folioNumber;
    }

    /**
     * Sets the value of the folioNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioNumber(String value) {
        this.folioNumber = value;
    }

    /**
     * Gets the value of the noShow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoShow() {
        return noShow;
    }

    /**
     * Sets the value of the noShow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoShow(String value) {
        this.noShow = value;
    }

    /**
     * Gets the value of the programCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramCode() {
        return programCode;
    }

    /**
     * Sets the value of the programCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramCode(String value) {
        this.programCode = value;
    }

    /**
     * Gets the value of the checkinDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckinDate() {
        return checkinDate;
    }

    /**
     * Sets the value of the checkinDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckinDate(String value) {
        this.checkinDate = value;
    }

    /**
     * Gets the value of the checkoutDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckoutDate() {
        return checkoutDate;
    }

    /**
     * Sets the value of the checkoutDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckoutDate(String value) {
        this.checkoutDate = value;
    }

    /**
     * Gets the value of the extraChargeTypes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraChargeTypes() {
        return extraChargeTypes;
    }

    /**
     * Sets the value of the extraChargeTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraChargeTypes(String value) {
        this.extraChargeTypes = value;
    }

    /**
     * Gets the value of the roomRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomRate() {
        return roomRate;
    }

    /**
     * Sets the value of the roomRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomRate(String value) {
        this.roomRate = value;
    }

    /**
     * Gets the value of the roomRateUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomRateUnit() {
        return roomRateUnit;
    }

    /**
     * Sets the value of the roomRateUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomRateUnit(String value) {
        this.roomRateUnit = value;
    }

    /**
     * Gets the value of the roomTax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomTax() {
        return roomTax;
    }

    /**
     * Sets the value of the roomTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomTax(String value) {
        this.roomTax = value;
    }

    /**
     * Gets the value of the extraChargeTotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraChargeTotal() {
        return extraChargeTotal;
    }

    /**
     * Sets the value of the extraChargeTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraChargeTotal(String value) {
        this.extraChargeTotal = value;
    }

    /**
     * Gets the value of the renterName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRenterName() {
        return renterName;
    }

    /**
     * Sets the value of the renterName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRenterName(String value) {
        this.renterName = value;
    }

}
