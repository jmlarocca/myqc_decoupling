
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Pos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Pos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="registerNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="entryMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="track1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="track2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="track3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trackKsn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardPresent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="track1e" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="track2e" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="track1len" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="track2len" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tracke" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="encMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="msrType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chipData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="issuerScriptResults" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="caps" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fallbackReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sigData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Pos", propOrder = {

})
public class Pos {

    protected String registerNumber;
    protected String entryMode;
    protected String track1;
    protected String track2;
    protected String track3;
    protected String trackKsn;
    protected String sequenceNumber;
    protected String cardPresent;
    @XmlElement(name = "track1e")
    protected String track1E;
    @XmlElement(name = "track2e")
    protected String track2E;
    @XmlElement(name = "track1len")
    protected String track1Len;
    @XmlElement(name = "track2len")
    protected String track2Len;
    protected String tracke;
    protected String encMode;
    protected String msrType;
    protected String paymentDate;
    protected String chipData;
    protected String issuerScriptResults;
    protected String caps;
    protected String fallbackReason;
    protected String sigData;

    /**
     * Gets the value of the registerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterNumber() {
        return registerNumber;
    }

    /**
     * Sets the value of the registerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterNumber(String value) {
        this.registerNumber = value;
    }

    /**
     * Gets the value of the entryMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntryMode() {
        return entryMode;
    }

    /**
     * Sets the value of the entryMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntryMode(String value) {
        this.entryMode = value;
    }

    /**
     * Gets the value of the track1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrack1() {
        return track1;
    }

    /**
     * Sets the value of the track1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrack1(String value) {
        this.track1 = value;
    }

    /**
     * Gets the value of the track2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrack2() {
        return track2;
    }

    /**
     * Sets the value of the track2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrack2(String value) {
        this.track2 = value;
    }

    /**
     * Gets the value of the track3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrack3() {
        return track3;
    }

    /**
     * Sets the value of the track3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrack3(String value) {
        this.track3 = value;
    }

    /**
     * Gets the value of the trackKsn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrackKsn() {
        return trackKsn;
    }

    /**
     * Sets the value of the trackKsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrackKsn(String value) {
        this.trackKsn = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the cardPresent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardPresent() {
        return cardPresent;
    }

    /**
     * Sets the value of the cardPresent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardPresent(String value) {
        this.cardPresent = value;
    }

    /**
     * Gets the value of the track1E property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrack1E() {
        return track1E;
    }

    /**
     * Sets the value of the track1E property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrack1E(String value) {
        this.track1E = value;
    }

    /**
     * Gets the value of the track2E property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrack2E() {
        return track2E;
    }

    /**
     * Sets the value of the track2E property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrack2E(String value) {
        this.track2E = value;
    }

    /**
     * Gets the value of the track1Len property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrack1Len() {
        return track1Len;
    }

    /**
     * Sets the value of the track1Len property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrack1Len(String value) {
        this.track1Len = value;
    }

    /**
     * Gets the value of the track2Len property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrack2Len() {
        return track2Len;
    }

    /**
     * Sets the value of the track2Len property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrack2Len(String value) {
        this.track2Len = value;
    }

    /**
     * Gets the value of the tracke property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTracke() {
        return tracke;
    }

    /**
     * Sets the value of the tracke property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTracke(String value) {
        this.tracke = value;
    }

    /**
     * Gets the value of the encMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncMode() {
        return encMode;
    }

    /**
     * Sets the value of the encMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncMode(String value) {
        this.encMode = value;
    }

    /**
     * Gets the value of the msrType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsrType() {
        return msrType;
    }

    /**
     * Sets the value of the msrType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsrType(String value) {
        this.msrType = value;
    }

    /**
     * Gets the value of the paymentDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * Sets the value of the paymentDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentDate(String value) {
        this.paymentDate = value;
    }

    /**
     * Gets the value of the chipData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChipData() {
        return chipData;
    }

    /**
     * Sets the value of the chipData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChipData(String value) {
        this.chipData = value;
    }

    /**
     * Gets the value of the issuerScriptResults property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerScriptResults() {
        return issuerScriptResults;
    }

    /**
     * Sets the value of the issuerScriptResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerScriptResults(String value) {
        this.issuerScriptResults = value;
    }

    /**
     * Gets the value of the caps property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaps() {
        return caps;
    }

    /**
     * Sets the value of the caps property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaps(String value) {
        this.caps = value;
    }

    /**
     * Gets the value of the fallbackReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFallbackReason() {
        return fallbackReason;
    }

    /**
     * Sets the value of the fallbackReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFallbackReason(String value) {
        this.fallbackReason = value;
    }

    /**
     * Gets the value of the sigData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSigData() {
        return sigData;
    }

    /**
     * Sets the value of the sigData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSigData(String value) {
        this.sigData = value;
    }

}
