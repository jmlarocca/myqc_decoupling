
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PurchaseTotals complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaseTotals">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="taxDetail" type="{http://freeway.freedompay.com/}TaxDetail" minOccurs="0"/>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="discountTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dutyTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="freightTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="debitSurcharge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cashBackAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="overtender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="invoiceTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eidAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chargeAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseTotals", propOrder = {

})
public class PurchaseTotals {

    protected TaxDetail taxDetail;
    protected String currency;
    protected String discountTotal;
    protected String dutyTotal;
    protected String freightTotal;
    protected String taxTotal;
    protected String tipAmount;
    protected String debitSurcharge;
    protected String cashBackAmount;
    protected String overtender;
    protected String invoiceTotal;
    protected String eidAmount;
    protected String chargeAmount;

    /**
     * Gets the value of the taxDetail property.
     * 
     * @return
     *     possible object is
     *     {@link TaxDetail }
     *     
     */
    public TaxDetail getTaxDetail() {
        return taxDetail;
    }

    /**
     * Sets the value of the taxDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxDetail }
     *     
     */
    public void setTaxDetail(TaxDetail value) {
        this.taxDetail = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the discountTotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountTotal() {
        return discountTotal;
    }

    /**
     * Sets the value of the discountTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountTotal(String value) {
        this.discountTotal = value;
    }

    /**
     * Gets the value of the dutyTotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutyTotal() {
        return dutyTotal;
    }

    /**
     * Sets the value of the dutyTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutyTotal(String value) {
        this.dutyTotal = value;
    }

    /**
     * Gets the value of the freightTotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightTotal() {
        return freightTotal;
    }

    /**
     * Sets the value of the freightTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightTotal(String value) {
        this.freightTotal = value;
    }

    /**
     * Gets the value of the taxTotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxTotal() {
        return taxTotal;
    }

    /**
     * Sets the value of the taxTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxTotal(String value) {
        this.taxTotal = value;
    }

    /**
     * Gets the value of the tipAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipAmount() {
        return tipAmount;
    }

    /**
     * Sets the value of the tipAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipAmount(String value) {
        this.tipAmount = value;
    }

    /**
     * Gets the value of the debitSurcharge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitSurcharge() {
        return debitSurcharge;
    }

    /**
     * Sets the value of the debitSurcharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitSurcharge(String value) {
        this.debitSurcharge = value;
    }

    /**
     * Gets the value of the cashBackAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashBackAmount() {
        return cashBackAmount;
    }

    /**
     * Sets the value of the cashBackAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashBackAmount(String value) {
        this.cashBackAmount = value;
    }

    /**
     * Gets the value of the overtender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOvertender() {
        return overtender;
    }

    /**
     * Sets the value of the overtender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOvertender(String value) {
        this.overtender = value;
    }

    /**
     * Gets the value of the invoiceTotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceTotal() {
        return invoiceTotal;
    }

    /**
     * Sets the value of the invoiceTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceTotal(String value) {
        this.invoiceTotal = value;
    }

    /**
     * Gets the value of the eidAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEidAmount() {
        return eidAmount;
    }

    /**
     * Sets the value of the eidAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEidAmount(String value) {
        this.eidAmount = value;
    }

    /**
     * Gets the value of the chargeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeAmount() {
        return chargeAmount;
    }

    /**
     * Sets the value of the chargeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeAmount(String value) {
        this.chargeAmount = value;
    }

}
