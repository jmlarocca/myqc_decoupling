
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigInteger;


/**
 * <p>Java class for ReplyMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReplyMessage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="adminReply" type="{http://freeway.freedompay.com/}AdminReply" minOccurs="0"/>
 *         &lt;element name="ccAuthReply" type="{http://freeway.freedompay.com/}CCAuthReply" minOccurs="0"/>
 *         &lt;element name="ccCaptureReply" type="{http://freeway.freedompay.com/}CCCaptureReply" minOccurs="0"/>
 *         &lt;element name="ccCreditReply" type="{http://freeway.freedompay.com/}CCCreditReply" minOccurs="0"/>
 *         &lt;element name="ccFollowupReply" type="{http://freeway.freedompay.com/}CCFollowupReply" minOccurs="0"/>
 *         &lt;element name="dccReply" type="{http://freeway.freedompay.com/}DccReply" minOccurs="0"/>
 *         &lt;element name="decision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="discountReply" type="{http://freeway.freedompay.com/}DiscountReply" minOccurs="0"/>
 *         &lt;element name="fraudCheckReply" type="{http://freeway.freedompay.com/}FraudCheckReply" minOccurs="0"/>
 *         &lt;element name="inquiryReply" type="{http://freeway.freedompay.com/}InquiryReply" minOccurs="0"/>
 *         &lt;element name="invalidFields" type="{http://freeway.freedompay.com/}ArrayOfString1" minOccurs="0"/>
 *         &lt;element name="loyaltyReply" type="{http://freeway.freedompay.com/}LoyaltyReply" minOccurs="0"/>
 *         &lt;element name="magicCookie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="merchantReferenceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="messageReply" type="{http://freeway.freedompay.com/}MessageReply" minOccurs="0"/>
 *         &lt;element name="missingFields" type="{http://freeway.freedompay.com/}ArrayOfString2" minOccurs="0"/>
 *         &lt;element name="mobileReply" type="{http://freeway.freedompay.com/}MobileReply" minOccurs="0"/>
 *         &lt;element name="networkData" type="{http://freeway.freedompay.com/}NetworkData" minOccurs="0"/>
 *         &lt;element name="promoReply" type="{http://freeway.freedompay.com/}PromoReply" minOccurs="0"/>
 *         &lt;element name="reasonCode" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="receiptData" type="{http://freeway.freedompay.com/}ReceiptData" minOccurs="0"/>
 *         &lt;element name="requestID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requestToken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sessionReply" type="{http://freeway.freedompay.com/}SessionReply" minOccurs="0"/>
 *         &lt;element name="tokenCreateReply" type="{http://freeway.freedompay.com/}TokenCreateReply" minOccurs="0"/>
 *         &lt;element name="tokenInformation" type="{http://freeway.freedompay.com/}TokenInformation" minOccurs="0"/>
 *         &lt;element name="vendControlReply" type="{http://freeway.freedompay.com/}VendControlReply" minOccurs="0"/>
 *         &lt;element name="voidReply" type="{http://freeway.freedompay.com/}VoidReply" minOccurs="0"/>
 *         &lt;element name="torReply" type="{http://freeway.freedompay.com/}TORReply" minOccurs="0"/>
 *         &lt;element name="eodReply" type="{http://freeway.freedompay.com/}EodReply" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReplyMessage", propOrder = {

})
public class ReplyMessage {

    protected AdminReply adminReply;
    protected CCAuthReply ccAuthReply;
    protected CCCaptureReply ccCaptureReply;
    protected CCCreditReply ccCreditReply;
    protected CCFollowupReply ccFollowupReply;
    protected DccReply dccReply;
    protected String decision;
    protected DiscountReply discountReply;
    protected FraudCheckReply fraudCheckReply;
    protected InquiryReply inquiryReply;
    protected ArrayOfString1 invalidFields;
    protected LoyaltyReply loyaltyReply;
    protected String magicCookie;
    protected String merchantReferenceCode;
    protected MessageReply messageReply;
    protected ArrayOfString2 missingFields;
    protected MobileReply mobileReply;
    protected NetworkData networkData;
    protected PromoReply promoReply;
    protected BigInteger reasonCode;
    protected ReceiptData receiptData;
    protected String requestID;
    protected String requestToken;
    protected SessionReply sessionReply;
    protected TokenCreateReply tokenCreateReply;
    protected TokenInformation tokenInformation;
    protected VendControlReply vendControlReply;
    protected VoidReply voidReply;
    protected TORReply torReply;
    protected EodReply eodReply;

    /**
     * Gets the value of the adminReply property.
     * 
     * @return
     *     possible object is
     *     {@link AdminReply }
     *     
     */
    public AdminReply getAdminReply() {
        return adminReply;
    }

    /**
     * Sets the value of the adminReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdminReply }
     *     
     */
    public void setAdminReply(AdminReply value) {
        this.adminReply = value;
    }

    /**
     * Gets the value of the ccAuthReply property.
     * 
     * @return
     *     possible object is
     *     {@link CCAuthReply }
     *     
     */
    public CCAuthReply getCcAuthReply() {
        return ccAuthReply;
    }

    /**
     * Sets the value of the ccAuthReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link CCAuthReply }
     *     
     */
    public void setCcAuthReply(CCAuthReply value) {
        this.ccAuthReply = value;
    }

    /**
     * Gets the value of the ccCaptureReply property.
     * 
     * @return
     *     possible object is
     *     {@link CCCaptureReply }
     *     
     */
    public CCCaptureReply getCcCaptureReply() {
        return ccCaptureReply;
    }

    /**
     * Sets the value of the ccCaptureReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link CCCaptureReply }
     *     
     */
    public void setCcCaptureReply(CCCaptureReply value) {
        this.ccCaptureReply = value;
    }

    /**
     * Gets the value of the ccCreditReply property.
     * 
     * @return
     *     possible object is
     *     {@link CCCreditReply }
     *     
     */
    public CCCreditReply getCcCreditReply() {
        return ccCreditReply;
    }

    /**
     * Sets the value of the ccCreditReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link CCCreditReply }
     *     
     */
    public void setCcCreditReply(CCCreditReply value) {
        this.ccCreditReply = value;
    }

    /**
     * Gets the value of the ccFollowupReply property.
     * 
     * @return
     *     possible object is
     *     {@link CCFollowupReply }
     *     
     */
    public CCFollowupReply getCcFollowupReply() {
        return ccFollowupReply;
    }

    /**
     * Sets the value of the ccFollowupReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link CCFollowupReply }
     *     
     */
    public void setCcFollowupReply(CCFollowupReply value) {
        this.ccFollowupReply = value;
    }

    /**
     * Gets the value of the dccReply property.
     * 
     * @return
     *     possible object is
     *     {@link DccReply }
     *     
     */
    public DccReply getDccReply() {
        return dccReply;
    }

    /**
     * Sets the value of the dccReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link DccReply }
     *     
     */
    public void setDccReply(DccReply value) {
        this.dccReply = value;
    }

    /**
     * Gets the value of the decision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecision() {
        return decision;
    }

    /**
     * Sets the value of the decision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecision(String value) {
        this.decision = value;
    }

    /**
     * Gets the value of the discountReply property.
     * 
     * @return
     *     possible object is
     *     {@link DiscountReply }
     *     
     */
    public DiscountReply getDiscountReply() {
        return discountReply;
    }

    /**
     * Sets the value of the discountReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link DiscountReply }
     *     
     */
    public void setDiscountReply(DiscountReply value) {
        this.discountReply = value;
    }

    /**
     * Gets the value of the fraudCheckReply property.
     * 
     * @return
     *     possible object is
     *     {@link FraudCheckReply }
     *     
     */
    public FraudCheckReply getFraudCheckReply() {
        return fraudCheckReply;
    }

    /**
     * Sets the value of the fraudCheckReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link FraudCheckReply }
     *     
     */
    public void setFraudCheckReply(FraudCheckReply value) {
        this.fraudCheckReply = value;
    }

    /**
     * Gets the value of the inquiryReply property.
     * 
     * @return
     *     possible object is
     *     {@link InquiryReply }
     *     
     */
    public InquiryReply getInquiryReply() {
        return inquiryReply;
    }

    /**
     * Sets the value of the inquiryReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link InquiryReply }
     *     
     */
    public void setInquiryReply(InquiryReply value) {
        this.inquiryReply = value;
    }

    /**
     * Gets the value of the invalidFields property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString1 }
     *     
     */
    public ArrayOfString1 getInvalidFields() {
        return invalidFields;
    }

    /**
     * Sets the value of the invalidFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString1 }
     *     
     */
    public void setInvalidFields(ArrayOfString1 value) {
        this.invalidFields = value;
    }

    /**
     * Gets the value of the loyaltyReply property.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyReply }
     *     
     */
    public LoyaltyReply getLoyaltyReply() {
        return loyaltyReply;
    }

    /**
     * Sets the value of the loyaltyReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyReply }
     *     
     */
    public void setLoyaltyReply(LoyaltyReply value) {
        this.loyaltyReply = value;
    }

    /**
     * Gets the value of the magicCookie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMagicCookie() {
        return magicCookie;
    }

    /**
     * Sets the value of the magicCookie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMagicCookie(String value) {
        this.magicCookie = value;
    }

    /**
     * Gets the value of the merchantReferenceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantReferenceCode() {
        return merchantReferenceCode;
    }

    /**
     * Sets the value of the merchantReferenceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantReferenceCode(String value) {
        this.merchantReferenceCode = value;
    }

    /**
     * Gets the value of the messageReply property.
     * 
     * @return
     *     possible object is
     *     {@link MessageReply }
     *     
     */
    public MessageReply getMessageReply() {
        return messageReply;
    }

    /**
     * Sets the value of the messageReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageReply }
     *     
     */
    public void setMessageReply(MessageReply value) {
        this.messageReply = value;
    }

    /**
     * Gets the value of the missingFields property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString2 }
     *     
     */
    public ArrayOfString2 getMissingFields() {
        return missingFields;
    }

    /**
     * Sets the value of the missingFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString2 }
     *     
     */
    public void setMissingFields(ArrayOfString2 value) {
        this.missingFields = value;
    }

    /**
     * Gets the value of the mobileReply property.
     * 
     * @return
     *     possible object is
     *     {@link MobileReply }
     *     
     */
    public MobileReply getMobileReply() {
        return mobileReply;
    }

    /**
     * Sets the value of the mobileReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link MobileReply }
     *     
     */
    public void setMobileReply(MobileReply value) {
        this.mobileReply = value;
    }

    /**
     * Gets the value of the networkData property.
     * 
     * @return
     *     possible object is
     *     {@link NetworkData }
     *     
     */
    public NetworkData getNetworkData() {
        return networkData;
    }

    /**
     * Sets the value of the networkData property.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkData }
     *     
     */
    public void setNetworkData(NetworkData value) {
        this.networkData = value;
    }

    /**
     * Gets the value of the promoReply property.
     * 
     * @return
     *     possible object is
     *     {@link PromoReply }
     *     
     */
    public PromoReply getPromoReply() {
        return promoReply;
    }

    /**
     * Sets the value of the promoReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link PromoReply }
     *     
     */
    public void setPromoReply(PromoReply value) {
        this.promoReply = value;
    }

    /**
     * Gets the value of the reasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getReasonCode() {
        return reasonCode;
    }

    /**
     * Sets the value of the reasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setReasonCode(BigInteger value) {
        this.reasonCode = value;
    }

    /**
     * Gets the value of the receiptData property.
     * 
     * @return
     *     possible object is
     *     {@link ReceiptData }
     *     
     */
    public ReceiptData getReceiptData() {
        return receiptData;
    }

    /**
     * Sets the value of the receiptData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceiptData }
     *     
     */
    public void setReceiptData(ReceiptData value) {
        this.receiptData = value;
    }

    /**
     * Gets the value of the requestID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Gets the value of the requestToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestToken() {
        return requestToken;
    }

    /**
     * Sets the value of the requestToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestToken(String value) {
        this.requestToken = value;
    }

    /**
     * Gets the value of the sessionReply property.
     * 
     * @return
     *     possible object is
     *     {@link SessionReply }
     *     
     */
    public SessionReply getSessionReply() {
        return sessionReply;
    }

    /**
     * Sets the value of the sessionReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionReply }
     *     
     */
    public void setSessionReply(SessionReply value) {
        this.sessionReply = value;
    }

    /**
     * Gets the value of the tokenCreateReply property.
     * 
     * @return
     *     possible object is
     *     {@link TokenCreateReply }
     *     
     */
    public TokenCreateReply getTokenCreateReply() {
        return tokenCreateReply;
    }

    /**
     * Sets the value of the tokenCreateReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link TokenCreateReply }
     *     
     */
    public void setTokenCreateReply(TokenCreateReply value) {
        this.tokenCreateReply = value;
    }

    /**
     * Gets the value of the tokenInformation property.
     * 
     * @return
     *     possible object is
     *     {@link TokenInformation }
     *     
     */
    public TokenInformation getTokenInformation() {
        return tokenInformation;
    }

    /**
     * Sets the value of the tokenInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link TokenInformation }
     *     
     */
    public void setTokenInformation(TokenInformation value) {
        this.tokenInformation = value;
    }

    /**
     * Gets the value of the vendControlReply property.
     * 
     * @return
     *     possible object is
     *     {@link VendControlReply }
     *     
     */
    public VendControlReply getVendControlReply() {
        return vendControlReply;
    }

    /**
     * Sets the value of the vendControlReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link VendControlReply }
     *     
     */
    public void setVendControlReply(VendControlReply value) {
        this.vendControlReply = value;
    }

    /**
     * Gets the value of the voidReply property.
     * 
     * @return
     *     possible object is
     *     {@link VoidReply }
     *     
     */
    public VoidReply getVoidReply() {
        return voidReply;
    }

    /**
     * Sets the value of the voidReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoidReply }
     *     
     */
    public void setVoidReply(VoidReply value) {
        this.voidReply = value;
    }

    /**
     * Gets the value of the torReply property.
     * 
     * @return
     *     possible object is
     *     {@link TORReply }
     *     
     */
    public TORReply getTorReply() {
        return torReply;
    }

    /**
     * Sets the value of the torReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link TORReply }
     *     
     */
    public void setTorReply(TORReply value) {
        this.torReply = value;
    }

    /**
     * Gets the value of the eodReply property.
     * 
     * @return
     *     possible object is
     *     {@link EodReply }
     *     
     */
    public EodReply getEodReply() {
        return eodReply;
    }

    /**
     * Sets the value of the eodReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link EodReply }
     *     
     */
    public void setEodReply(EodReply value) {
        this.eodReply = value;
    }

}
