
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestMessage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="clientId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="storeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="terminalId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esToken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="adminService" type="{http://freeway.freedompay.com/}AdminService" minOccurs="0"/>
 *         &lt;element name="autoRentalData" type="{http://freeway.freedompay.com/}AutoRentalData" minOccurs="0"/>
 *         &lt;element name="healthcareData" type="{http://freeway.freedompay.com/}HealthcareDataWeb" minOccurs="0"/>
 *         &lt;element name="billTo" type="{http://freeway.freedompay.com/}BillTo" minOccurs="0"/>
 *         &lt;element name="card" type="{http://freeway.freedompay.com/}Card" minOccurs="0"/>
 *         &lt;element name="ccAuthService" type="{http://freeway.freedompay.com/}CCAuthService" minOccurs="0"/>
 *         &lt;element name="ccCaptureService" type="{http://freeway.freedompay.com/}CCCaptureService" minOccurs="0"/>
 *         &lt;element name="ccCreditService" type="{http://freeway.freedompay.com/}CCCreditService" minOccurs="0"/>
 *         &lt;element name="ccFollowupService" type="{http://freeway.freedompay.com/}CCFollowupService" minOccurs="0"/>
 *         &lt;element name="dccService" type="{http://freeway.freedompay.com/}DccService" minOccurs="0"/>
 *         &lt;element name="dcc" type="{http://freeway.freedompay.com/}DccInfo" minOccurs="0"/>
 *         &lt;element name="clerkId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientMetadata" type="{http://freeway.freedompay.com/}ClientMetadata" minOccurs="0"/>
 *         &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="discountService" type="{http://freeway.freedompay.com/}DiscountService" minOccurs="0"/>
 *         &lt;element name="efvOptions" type="{http://freeway.freedompay.com/}EfvOptions" minOccurs="0"/>
 *         &lt;element name="fleetData" type="{http://freeway.freedompay.com/}FleetData" minOccurs="0"/>
 *         &lt;element name="fraudCheckService" type="{http://freeway.freedompay.com/}FraudCheckService" minOccurs="0"/>
 *         &lt;element name="inquiryService" type="{http://freeway.freedompay.com/}InquiryService" minOccurs="0"/>
 *         &lt;element name="hotelData" type="{http://freeway.freedompay.com/}HotelData" minOccurs="0"/>
 *         &lt;element name="invoiceDiscountDetail" type="{http://freeway.freedompay.com/}InvoiceDiscountDetail" minOccurs="0"/>
 *         &lt;element name="invoiceHeader" type="{http://freeway.freedompay.com/}InvoiceHeader" minOccurs="0"/>
 *         &lt;element name="items" type="{http://freeway.freedompay.com/}ArrayOfItem" minOccurs="0"/>
 *         &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loyaltyService" type="{http://freeway.freedompay.com/}LoyaltyService" minOccurs="0"/>
 *         &lt;element name="merchantDefinedData" type="{http://freeway.freedompay.com/}MerchantDefinedData" minOccurs="0"/>
 *         &lt;element name="magicCookie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="merchantReferenceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="merchantBatchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="messageService" type="{http://freeway.freedompay.com/}MessageService" minOccurs="0"/>
 *         &lt;element name="mobileService" type="{http://freeway.freedompay.com/}MobileService" minOccurs="0"/>
 *         &lt;element name="networkData" type="{http://freeway.freedompay.com/}NetworkData" minOccurs="0"/>
 *         &lt;element name="orderRequestID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderRequestToken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pos" type="{http://freeway.freedompay.com/}Pos" minOccurs="0"/>
 *         &lt;element name="promoService" type="{http://freeway.freedompay.com/}PromoService" minOccurs="0"/>
 *         &lt;element name="purchaseTotals" type="{http://freeway.freedompay.com/}PurchaseTotals" minOccurs="0"/>
 *         &lt;element name="restaurantData" type="{http://freeway.freedompay.com/}RestaurantData" minOccurs="0"/>
 *         &lt;element name="responseFlags" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sessionService" type="{http://freeway.freedompay.com/}SessionService" minOccurs="0"/>
 *         &lt;element name="shipFrom" type="{http://freeway.freedompay.com/}ShipFrom" minOccurs="0"/>
 *         &lt;element name="shipTo" type="{http://freeway.freedompay.com/}ShipTo" minOccurs="0"/>
 *         &lt;element name="tokenCreateService" type="{http://freeway.freedompay.com/}TokenCreateService" minOccurs="0"/>
 *         &lt;element name="vendControlService" type="{http://freeway.freedompay.com/}VendControlService" minOccurs="0"/>
 *         &lt;element name="voidService" type="{http://freeway.freedompay.com/}VoidService" minOccurs="0"/>
 *         &lt;element name="torService" type="{http://freeway.freedompay.com/}TORService" minOccurs="0"/>
 *         &lt;element name="eodService" type="{http://freeway.freedompay.com/}EodService" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestMessage", propOrder = {

})
public class RequestMessage {

    protected String clientId;
    protected String storeId;
    protected String terminalId;
    protected String esToken;
    protected AdminService adminService;
    protected AutoRentalData autoRentalData;
    protected HealthcareDataWeb healthcareData;
    protected BillTo billTo;
    protected Card card;
    protected CCAuthService ccAuthService;
    protected CCCaptureService ccCaptureService;
    protected CCCreditService ccCreditService;
    protected CCFollowupService ccFollowupService;
    protected DccService dccService;
    protected DccInfo dcc;
    protected String clerkId;
    protected ClientMetadata clientMetadata;
    protected String comments;
    protected DiscountService discountService;
    protected EfvOptions efvOptions;
    protected FleetData fleetData;
    protected FraudCheckService fraudCheckService;
    protected InquiryService inquiryService;
    protected HotelData hotelData;
    protected InvoiceDiscountDetail invoiceDiscountDetail;
    protected InvoiceHeader invoiceHeader;
    protected ArrayOfItem items;
    protected String language;
    protected LoyaltyService loyaltyService;
    protected MerchantDefinedData merchantDefinedData;
    protected String magicCookie;
    protected String merchantReferenceCode;
    protected String merchantBatchId;
    protected MessageService messageService;
    protected MobileService mobileService;
    protected NetworkData networkData;
    protected String orderRequestID;
    protected String orderRequestToken;
    protected Pos pos;
    protected PromoService promoService;
    protected PurchaseTotals purchaseTotals;
    protected RestaurantData restaurantData;
    protected String responseFlags;
    protected String sessionKey;
    protected SessionService sessionService;
    protected ShipFrom shipFrom;
    protected ShipTo shipTo;
    protected TokenCreateService tokenCreateService;
    protected VendControlService vendControlService;
    protected VoidService voidService;
    protected TORService torService;
    protected EodService eodService;

    /**
     * Gets the value of the clientId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Sets the value of the clientId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Gets the value of the storeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     * Sets the value of the storeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStoreId(String value) {
        this.storeId = value;
    }

    /**
     * Gets the value of the terminalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * Sets the value of the terminalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminalId(String value) {
        this.terminalId = value;
    }

    /**
     * Gets the value of the esToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsToken() {
        return esToken;
    }

    /**
     * Sets the value of the esToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsToken(String value) {
        this.esToken = value;
    }

    /**
     * Gets the value of the adminService property.
     * 
     * @return
     *     possible object is
     *     {@link AdminService }
     *     
     */
    public AdminService getAdminService() {
        return adminService;
    }

    /**
     * Sets the value of the adminService property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdminService }
     *     
     */
    public void setAdminService(AdminService value) {
        this.adminService = value;
    }

    /**
     * Gets the value of the autoRentalData property.
     * 
     * @return
     *     possible object is
     *     {@link AutoRentalData }
     *     
     */
    public AutoRentalData getAutoRentalData() {
        return autoRentalData;
    }

    /**
     * Sets the value of the autoRentalData property.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoRentalData }
     *     
     */
    public void setAutoRentalData(AutoRentalData value) {
        this.autoRentalData = value;
    }

    /**
     * Gets the value of the healthcareData property.
     * 
     * @return
     *     possible object is
     *     {@link HealthcareDataWeb }
     *     
     */
    public HealthcareDataWeb getHealthcareData() {
        return healthcareData;
    }

    /**
     * Sets the value of the healthcareData property.
     * 
     * @param value
     *     allowed object is
     *     {@link HealthcareDataWeb }
     *     
     */
    public void setHealthcareData(HealthcareDataWeb value) {
        this.healthcareData = value;
    }

    /**
     * Gets the value of the billTo property.
     * 
     * @return
     *     possible object is
     *     {@link BillTo }
     *     
     */
    public BillTo getBillTo() {
        return billTo;
    }

    /**
     * Sets the value of the billTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillTo }
     *     
     */
    public void setBillTo(BillTo value) {
        this.billTo = value;
    }

    /**
     * Gets the value of the card property.
     * 
     * @return
     *     possible object is
     *     {@link Card }
     *     
     */
    public Card getCard() {
        return card;
    }

    /**
     * Sets the value of the card property.
     * 
     * @param value
     *     allowed object is
     *     {@link Card }
     *     
     */
    public void setCard(Card value) {
        this.card = value;
    }

    /**
     * Gets the value of the ccAuthService property.
     * 
     * @return
     *     possible object is
     *     {@link CCAuthService }
     *     
     */
    public CCAuthService getCcAuthService() {
        return ccAuthService;
    }

    /**
     * Sets the value of the ccAuthService property.
     * 
     * @param value
     *     allowed object is
     *     {@link CCAuthService }
     *     
     */
    public void setCcAuthService(CCAuthService value) {
        this.ccAuthService = value;
    }

    /**
     * Gets the value of the ccCaptureService property.
     * 
     * @return
     *     possible object is
     *     {@link CCCaptureService }
     *     
     */
    public CCCaptureService getCcCaptureService() {
        return ccCaptureService;
    }

    /**
     * Sets the value of the ccCaptureService property.
     * 
     * @param value
     *     allowed object is
     *     {@link CCCaptureService }
     *     
     */
    public void setCcCaptureService(CCCaptureService value) {
        this.ccCaptureService = value;
    }

    /**
     * Gets the value of the ccCreditService property.
     * 
     * @return
     *     possible object is
     *     {@link CCCreditService }
     *     
     */
    public CCCreditService getCcCreditService() {
        return ccCreditService;
    }

    /**
     * Sets the value of the ccCreditService property.
     * 
     * @param value
     *     allowed object is
     *     {@link CCCreditService }
     *     
     */
    public void setCcCreditService(CCCreditService value) {
        this.ccCreditService = value;
    }

    /**
     * Gets the value of the ccFollowupService property.
     * 
     * @return
     *     possible object is
     *     {@link CCFollowupService }
     *     
     */
    public CCFollowupService getCcFollowupService() {
        return ccFollowupService;
    }

    /**
     * Sets the value of the ccFollowupService property.
     * 
     * @param value
     *     allowed object is
     *     {@link CCFollowupService }
     *     
     */
    public void setCcFollowupService(CCFollowupService value) {
        this.ccFollowupService = value;
    }

    /**
     * Gets the value of the dccService property.
     * 
     * @return
     *     possible object is
     *     {@link DccService }
     *     
     */
    public DccService getDccService() {
        return dccService;
    }

    /**
     * Sets the value of the dccService property.
     * 
     * @param value
     *     allowed object is
     *     {@link DccService }
     *     
     */
    public void setDccService(DccService value) {
        this.dccService = value;
    }

    /**
     * Gets the value of the dcc property.
     * 
     * @return
     *     possible object is
     *     {@link DccInfo }
     *     
     */
    public DccInfo getDcc() {
        return dcc;
    }

    /**
     * Sets the value of the dcc property.
     * 
     * @param value
     *     allowed object is
     *     {@link DccInfo }
     *     
     */
    public void setDcc(DccInfo value) {
        this.dcc = value;
    }

    /**
     * Gets the value of the clerkId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClerkId() {
        return clerkId;
    }

    /**
     * Sets the value of the clerkId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClerkId(String value) {
        this.clerkId = value;
    }

    /**
     * Gets the value of the clientMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link ClientMetadata }
     *     
     */
    public ClientMetadata getClientMetadata() {
        return clientMetadata;
    }

    /**
     * Sets the value of the clientMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClientMetadata }
     *     
     */
    public void setClientMetadata(ClientMetadata value) {
        this.clientMetadata = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the discountService property.
     * 
     * @return
     *     possible object is
     *     {@link DiscountService }
     *     
     */
    public DiscountService getDiscountService() {
        return discountService;
    }

    /**
     * Sets the value of the discountService property.
     * 
     * @param value
     *     allowed object is
     *     {@link DiscountService }
     *     
     */
    public void setDiscountService(DiscountService value) {
        this.discountService = value;
    }

    /**
     * Gets the value of the efvOptions property.
     * 
     * @return
     *     possible object is
     *     {@link EfvOptions }
     *     
     */
    public EfvOptions getEfvOptions() {
        return efvOptions;
    }

    /**
     * Sets the value of the efvOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link EfvOptions }
     *     
     */
    public void setEfvOptions(EfvOptions value) {
        this.efvOptions = value;
    }

    /**
     * Gets the value of the fleetData property.
     * 
     * @return
     *     possible object is
     *     {@link FleetData }
     *     
     */
    public FleetData getFleetData() {
        return fleetData;
    }

    /**
     * Sets the value of the fleetData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FleetData }
     *     
     */
    public void setFleetData(FleetData value) {
        this.fleetData = value;
    }

    /**
     * Gets the value of the fraudCheckService property.
     * 
     * @return
     *     possible object is
     *     {@link FraudCheckService }
     *     
     */
    public FraudCheckService getFraudCheckService() {
        return fraudCheckService;
    }

    /**
     * Sets the value of the fraudCheckService property.
     * 
     * @param value
     *     allowed object is
     *     {@link FraudCheckService }
     *     
     */
    public void setFraudCheckService(FraudCheckService value) {
        this.fraudCheckService = value;
    }

    /**
     * Gets the value of the inquiryService property.
     * 
     * @return
     *     possible object is
     *     {@link InquiryService }
     *     
     */
    public InquiryService getInquiryService() {
        return inquiryService;
    }

    /**
     * Sets the value of the inquiryService property.
     * 
     * @param value
     *     allowed object is
     *     {@link InquiryService }
     *     
     */
    public void setInquiryService(InquiryService value) {
        this.inquiryService = value;
    }

    /**
     * Gets the value of the hotelData property.
     * 
     * @return
     *     possible object is
     *     {@link HotelData }
     *     
     */
    public HotelData getHotelData() {
        return hotelData;
    }

    /**
     * Sets the value of the hotelData property.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelData }
     *     
     */
    public void setHotelData(HotelData value) {
        this.hotelData = value;
    }

    /**
     * Gets the value of the invoiceDiscountDetail property.
     * 
     * @return
     *     possible object is
     *     {@link InvoiceDiscountDetail }
     *     
     */
    public InvoiceDiscountDetail getInvoiceDiscountDetail() {
        return invoiceDiscountDetail;
    }

    /**
     * Sets the value of the invoiceDiscountDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoiceDiscountDetail }
     *     
     */
    public void setInvoiceDiscountDetail(InvoiceDiscountDetail value) {
        this.invoiceDiscountDetail = value;
    }

    /**
     * Gets the value of the invoiceHeader property.
     * 
     * @return
     *     possible object is
     *     {@link InvoiceHeader }
     *     
     */
    public InvoiceHeader getInvoiceHeader() {
        return invoiceHeader;
    }

    /**
     * Sets the value of the invoiceHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoiceHeader }
     *     
     */
    public void setInvoiceHeader(InvoiceHeader value) {
        this.invoiceHeader = value;
    }

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfItem }
     *     
     */
    public ArrayOfItem getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfItem }
     *     
     */
    public void setItems(ArrayOfItem value) {
        this.items = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the loyaltyService property.
     * 
     * @return
     *     possible object is
     *     {@link LoyaltyService }
     *     
     */
    public LoyaltyService getLoyaltyService() {
        return loyaltyService;
    }

    /**
     * Sets the value of the loyaltyService property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoyaltyService }
     *     
     */
    public void setLoyaltyService(LoyaltyService value) {
        this.loyaltyService = value;
    }

    /**
     * Gets the value of the merchantDefinedData property.
     * 
     * @return
     *     possible object is
     *     {@link MerchantDefinedData }
     *     
     */
    public MerchantDefinedData getMerchantDefinedData() {
        return merchantDefinedData;
    }

    /**
     * Sets the value of the merchantDefinedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link MerchantDefinedData }
     *     
     */
    public void setMerchantDefinedData(MerchantDefinedData value) {
        this.merchantDefinedData = value;
    }

    /**
     * Gets the value of the magicCookie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMagicCookie() {
        return magicCookie;
    }

    /**
     * Sets the value of the magicCookie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMagicCookie(String value) {
        this.magicCookie = value;
    }

    /**
     * Gets the value of the merchantReferenceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantReferenceCode() {
        return merchantReferenceCode;
    }

    /**
     * Sets the value of the merchantReferenceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantReferenceCode(String value) {
        this.merchantReferenceCode = value;
    }

    /**
     * Gets the value of the merchantBatchId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantBatchId() {
        return merchantBatchId;
    }

    /**
     * Sets the value of the merchantBatchId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantBatchId(String value) {
        this.merchantBatchId = value;
    }

    /**
     * Gets the value of the messageService property.
     * 
     * @return
     *     possible object is
     *     {@link MessageService }
     *     
     */
    public MessageService getMessageService() {
        return messageService;
    }

    /**
     * Sets the value of the messageService property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageService }
     *     
     */
    public void setMessageService(MessageService value) {
        this.messageService = value;
    }

    /**
     * Gets the value of the mobileService property.
     * 
     * @return
     *     possible object is
     *     {@link MobileService }
     *     
     */
    public MobileService getMobileService() {
        return mobileService;
    }

    /**
     * Sets the value of the mobileService property.
     * 
     * @param value
     *     allowed object is
     *     {@link MobileService }
     *     
     */
    public void setMobileService(MobileService value) {
        this.mobileService = value;
    }

    /**
     * Gets the value of the networkData property.
     * 
     * @return
     *     possible object is
     *     {@link NetworkData }
     *     
     */
    public NetworkData getNetworkData() {
        return networkData;
    }

    /**
     * Sets the value of the networkData property.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkData }
     *     
     */
    public void setNetworkData(NetworkData value) {
        this.networkData = value;
    }

    /**
     * Gets the value of the orderRequestID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderRequestID() {
        return orderRequestID;
    }

    /**
     * Sets the value of the orderRequestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderRequestID(String value) {
        this.orderRequestID = value;
    }

    /**
     * Gets the value of the orderRequestToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderRequestToken() {
        return orderRequestToken;
    }

    /**
     * Sets the value of the orderRequestToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderRequestToken(String value) {
        this.orderRequestToken = value;
    }

    /**
     * Gets the value of the pos property.
     * 
     * @return
     *     possible object is
     *     {@link Pos }
     *     
     */
    public Pos getPos() {
        return pos;
    }

    /**
     * Sets the value of the pos property.
     * 
     * @param value
     *     allowed object is
     *     {@link Pos }
     *     
     */
    public void setPos(Pos value) {
        this.pos = value;
    }

    /**
     * Gets the value of the promoService property.
     * 
     * @return
     *     possible object is
     *     {@link PromoService }
     *     
     */
    public PromoService getPromoService() {
        return promoService;
    }

    /**
     * Sets the value of the promoService property.
     * 
     * @param value
     *     allowed object is
     *     {@link PromoService }
     *     
     */
    public void setPromoService(PromoService value) {
        this.promoService = value;
    }

    /**
     * Gets the value of the purchaseTotals property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseTotals }
     *     
     */
    public PurchaseTotals getPurchaseTotals() {
        return purchaseTotals;
    }

    /**
     * Sets the value of the purchaseTotals property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseTotals }
     *     
     */
    public void setPurchaseTotals(PurchaseTotals value) {
        this.purchaseTotals = value;
    }

    /**
     * Gets the value of the restaurantData property.
     * 
     * @return
     *     possible object is
     *     {@link RestaurantData }
     *     
     */
    public RestaurantData getRestaurantData() {
        return restaurantData;
    }

    /**
     * Sets the value of the restaurantData property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestaurantData }
     *     
     */
    public void setRestaurantData(RestaurantData value) {
        this.restaurantData = value;
    }

    /**
     * Gets the value of the responseFlags property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseFlags() {
        return responseFlags;
    }

    /**
     * Sets the value of the responseFlags property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseFlags(String value) {
        this.responseFlags = value;
    }

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

    /**
     * Gets the value of the sessionService property.
     * 
     * @return
     *     possible object is
     *     {@link SessionService }
     *     
     */
    public SessionService getSessionService() {
        return sessionService;
    }

    /**
     * Sets the value of the sessionService property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionService }
     *     
     */
    public void setSessionService(SessionService value) {
        this.sessionService = value;
    }

    /**
     * Gets the value of the shipFrom property.
     * 
     * @return
     *     possible object is
     *     {@link ShipFrom }
     *     
     */
    public ShipFrom getShipFrom() {
        return shipFrom;
    }

    /**
     * Sets the value of the shipFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipFrom }
     *     
     */
    public void setShipFrom(ShipFrom value) {
        this.shipFrom = value;
    }

    /**
     * Gets the value of the shipTo property.
     * 
     * @return
     *     possible object is
     *     {@link ShipTo }
     *     
     */
    public ShipTo getShipTo() {
        return shipTo;
    }

    /**
     * Sets the value of the shipTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipTo }
     *     
     */
    public void setShipTo(ShipTo value) {
        this.shipTo = value;
    }

    /**
     * Gets the value of the tokenCreateService property.
     * 
     * @return
     *     possible object is
     *     {@link TokenCreateService }
     *     
     */
    public TokenCreateService getTokenCreateService() {
        return tokenCreateService;
    }

    /**
     * Sets the value of the tokenCreateService property.
     * 
     * @param value
     *     allowed object is
     *     {@link TokenCreateService }
     *     
     */
    public void setTokenCreateService(TokenCreateService value) {
        this.tokenCreateService = value;
    }

    /**
     * Gets the value of the vendControlService property.
     * 
     * @return
     *     possible object is
     *     {@link VendControlService }
     *     
     */
    public VendControlService getVendControlService() {
        return vendControlService;
    }

    /**
     * Sets the value of the vendControlService property.
     * 
     * @param value
     *     allowed object is
     *     {@link VendControlService }
     *     
     */
    public void setVendControlService(VendControlService value) {
        this.vendControlService = value;
    }

    /**
     * Gets the value of the voidService property.
     * 
     * @return
     *     possible object is
     *     {@link VoidService }
     *     
     */
    public VoidService getVoidService() {
        return voidService;
    }

    /**
     * Sets the value of the voidService property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoidService }
     *     
     */
    public void setVoidService(VoidService value) {
        this.voidService = value;
    }

    /**
     * Gets the value of the torService property.
     * 
     * @return
     *     possible object is
     *     {@link TORService }
     *     
     */
    public TORService getTorService() {
        return torService;
    }

    /**
     * Sets the value of the torService property.
     * 
     * @param value
     *     allowed object is
     *     {@link TORService }
     *     
     */
    public void setTorService(TORService value) {
        this.torService = value;
    }

    /**
     * Gets the value of the eodService property.
     * 
     * @return
     *     possible object is
     *     {@link EodService }
     *     
     */
    public EodService getEodService() {
        return eodService;
    }

    /**
     * Sets the value of the eodService property.
     * 
     * @param value
     *     allowed object is
     *     {@link EodService }
     *     
     */
    public void setEodService(EodService value) {
        this.eodService = value;
    }

}
