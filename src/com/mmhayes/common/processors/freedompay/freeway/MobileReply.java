
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MobileReply complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MobileReply">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="tipProvided" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentSessionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paySeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nextPaySeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="barData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliedDiscountAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MobileReply", propOrder = {

})
public class MobileReply {

    protected String tipProvided;
    protected String tipAmount;
    protected String paymentSessionId;
    protected String paySeq;
    protected String nextPaySeq;
    protected String barData;
    protected String appliedDiscountAmount;

    /**
     * Gets the value of the tipProvided property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipProvided() {
        return tipProvided;
    }

    /**
     * Sets the value of the tipProvided property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipProvided(String value) {
        this.tipProvided = value;
    }

    /**
     * Gets the value of the tipAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipAmount() {
        return tipAmount;
    }

    /**
     * Sets the value of the tipAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipAmount(String value) {
        this.tipAmount = value;
    }

    /**
     * Gets the value of the paymentSessionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentSessionId() {
        return paymentSessionId;
    }

    /**
     * Sets the value of the paymentSessionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentSessionId(String value) {
        this.paymentSessionId = value;
    }

    /**
     * Gets the value of the paySeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaySeq() {
        return paySeq;
    }

    /**
     * Sets the value of the paySeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaySeq(String value) {
        this.paySeq = value;
    }

    /**
     * Gets the value of the nextPaySeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextPaySeq() {
        return nextPaySeq;
    }

    /**
     * Sets the value of the nextPaySeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextPaySeq(String value) {
        this.nextPaySeq = value;
    }

    /**
     * Gets the value of the barData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarData() {
        return barData;
    }

    /**
     * Sets the value of the barData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarData(String value) {
        this.barData = value;
    }

    /**
     * Gets the value of the appliedDiscountAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliedDiscountAmount() {
        return appliedDiscountAmount;
    }

    /**
     * Sets the value of the appliedDiscountAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliedDiscountAmount(String value) {
        this.appliedDiscountAmount = value;
    }

}
