
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigInteger;


/**
 * <p>Java class for DiscountReply complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DiscountReply">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="reasonCode" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="requestDateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="discountKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="discounts" type="{http://freeway.freedompay.com/}ArrayOfDiscountMeta" minOccurs="0"/>
 *         &lt;element name="invoiceDiscounts" type="{http://freeway.freedompay.com/}ArrayOfDiscountInfo" minOccurs="0"/>
 *         &lt;element name="items" type="{http://freeway.freedompay.com/}ArrayOfItemDiscounts" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiscountReply", propOrder = {

})
public class DiscountReply {

    protected BigInteger reasonCode;
    protected String requestDateTime;
    protected String discountKey;
    protected ArrayOfDiscountMeta discounts;
    protected ArrayOfDiscountInfo invoiceDiscounts;
    protected ArrayOfItemDiscounts items;

    /**
     * Gets the value of the reasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getReasonCode() {
        return reasonCode;
    }

    /**
     * Sets the value of the reasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setReasonCode(BigInteger value) {
        this.reasonCode = value;
    }

    /**
     * Gets the value of the requestDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestDateTime() {
        return requestDateTime;
    }

    /**
     * Sets the value of the requestDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestDateTime(String value) {
        this.requestDateTime = value;
    }

    /**
     * Gets the value of the discountKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountKey() {
        return discountKey;
    }

    /**
     * Sets the value of the discountKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountKey(String value) {
        this.discountKey = value;
    }

    /**
     * Gets the value of the discounts property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDiscountMeta }
     *     
     */
    public ArrayOfDiscountMeta getDiscounts() {
        return discounts;
    }

    /**
     * Sets the value of the discounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDiscountMeta }
     *     
     */
    public void setDiscounts(ArrayOfDiscountMeta value) {
        this.discounts = value;
    }

    /**
     * Gets the value of the invoiceDiscounts property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDiscountInfo }
     *     
     */
    public ArrayOfDiscountInfo getInvoiceDiscounts() {
        return invoiceDiscounts;
    }

    /**
     * Sets the value of the invoiceDiscounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDiscountInfo }
     *     
     */
    public void setInvoiceDiscounts(ArrayOfDiscountInfo value) {
        this.invoiceDiscounts = value;
    }

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfItemDiscounts }
     *     
     */
    public ArrayOfItemDiscounts getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfItemDiscounts }
     *     
     */
    public void setItems(ArrayOfItemDiscounts value) {
        this.items = value;
    }

}
