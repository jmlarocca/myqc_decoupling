
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HealthcareDataWeb complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HealthcareDataWeb">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="totalAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rx" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clinic" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dental" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HealthcareDataWeb", propOrder = {

})
public class HealthcareDataWeb {

    protected String totalAmount;
    protected String rx;
    protected String vision;
    protected String clinic;
    protected String dental;

    /**
     * Gets the value of the totalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the value of the totalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalAmount(String value) {
        this.totalAmount = value;
    }

    /**
     * Gets the value of the rx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRx() {
        return rx;
    }

    /**
     * Sets the value of the rx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRx(String value) {
        this.rx = value;
    }

    /**
     * Gets the value of the vision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVision() {
        return vision;
    }

    /**
     * Sets the value of the vision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVision(String value) {
        this.vision = value;
    }

    /**
     * Gets the value of the clinic property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClinic() {
        return clinic;
    }

    /**
     * Sets the value of the clinic property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClinic(String value) {
        this.clinic = value;
    }

    /**
     * Gets the value of the dental property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDental() {
        return dental;
    }

    /**
     * Sets the value of the dental property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDental(String value) {
        this.dental = value;
    }

}
