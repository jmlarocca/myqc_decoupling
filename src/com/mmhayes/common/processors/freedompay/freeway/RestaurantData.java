
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RestaurantData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RestaurantData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="foodSubtotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="beverageSubtotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RestaurantData", propOrder = {

})
public class RestaurantData {

    protected String foodSubtotal;
    protected String beverageSubtotal;

    /**
     * Gets the value of the foodSubtotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFoodSubtotal() {
        return foodSubtotal;
    }

    /**
     * Sets the value of the foodSubtotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFoodSubtotal(String value) {
        this.foodSubtotal = value;
    }

    /**
     * Gets the value of the beverageSubtotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeverageSubtotal() {
        return beverageSubtotal;
    }

    /**
     * Sets the value of the beverageSubtotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeverageSubtotal(String value) {
        this.beverageSubtotal = value;
    }

}
