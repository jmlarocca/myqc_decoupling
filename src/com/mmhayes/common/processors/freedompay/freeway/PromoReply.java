
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigInteger;


/**
 * <p>Java class for PromoReply complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PromoReply">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="reasonCode" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="mixedPromosAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requestDateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="validationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="promos" type="{http://freeway.freedompay.com/}ArrayOfPromoInfo" minOccurs="0"/>
 *         &lt;element name="invoice" type="{http://freeway.freedompay.com/}InvoicePromos" minOccurs="0"/>
 *         &lt;element name="items" type="{http://freeway.freedompay.com/}ArrayOfItemPromos" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PromoReply", propOrder = {

})
public class PromoReply {

    protected BigInteger reasonCode;
    protected String mixedPromosAllowed;
    protected String requestDateTime;
    protected String validationCode;
    protected ArrayOfPromoInfo promos;
    protected InvoicePromos invoice;
    protected ArrayOfItemPromos items;

    /**
     * Gets the value of the reasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getReasonCode() {
        return reasonCode;
    }

    /**
     * Sets the value of the reasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setReasonCode(BigInteger value) {
        this.reasonCode = value;
    }

    /**
     * Gets the value of the mixedPromosAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMixedPromosAllowed() {
        return mixedPromosAllowed;
    }

    /**
     * Sets the value of the mixedPromosAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMixedPromosAllowed(String value) {
        this.mixedPromosAllowed = value;
    }

    /**
     * Gets the value of the requestDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestDateTime() {
        return requestDateTime;
    }

    /**
     * Sets the value of the requestDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestDateTime(String value) {
        this.requestDateTime = value;
    }

    /**
     * Gets the value of the validationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidationCode() {
        return validationCode;
    }

    /**
     * Sets the value of the validationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidationCode(String value) {
        this.validationCode = value;
    }

    /**
     * Gets the value of the promos property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPromoInfo }
     *     
     */
    public ArrayOfPromoInfo getPromos() {
        return promos;
    }

    /**
     * Sets the value of the promos property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPromoInfo }
     *     
     */
    public void setPromos(ArrayOfPromoInfo value) {
        this.promos = value;
    }

    /**
     * Gets the value of the invoice property.
     * 
     * @return
     *     possible object is
     *     {@link InvoicePromos }
     *     
     */
    public InvoicePromos getInvoice() {
        return invoice;
    }

    /**
     * Sets the value of the invoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoicePromos }
     *     
     */
    public void setInvoice(InvoicePromos value) {
        this.invoice = value;
    }

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfItemPromos }
     *     
     */
    public ArrayOfItemPromos getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfItemPromos }
     *     
     */
    public void setItems(ArrayOfItemPromos value) {
        this.items = value;
    }

}
