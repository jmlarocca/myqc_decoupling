
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DiscountInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DiscountInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *       &lt;/all>
 *       &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="chainGroup" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="qtyLimit" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="qtyOffset" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="forceUnitPrice" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="discfix" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="discpct" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="disclimit" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="scale" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="each" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="total" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiscountInfo", propOrder = {

})
public class DiscountInfo {

    @XmlAttribute(name = "code")
    protected String code;
    @XmlAttribute(name = "chainGroup")
    protected String chainGroup;
    @XmlAttribute(name = "qtyLimit")
    protected String qtyLimit;
    @XmlAttribute(name = "qtyOffset")
    protected String qtyOffset;
    @XmlAttribute(name = "forceUnitPrice")
    protected String forceUnitPrice;
    @XmlAttribute(name = "discfix")
    protected String discfix;
    @XmlAttribute(name = "discpct")
    protected String discpct;
    @XmlAttribute(name = "disclimit")
    protected String disclimit;
    @XmlAttribute(name = "scale")
    protected String scale;
    @XmlAttribute(name = "each")
    protected String each;
    @XmlAttribute(name = "total")
    protected String total;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the chainGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChainGroup() {
        return chainGroup;
    }

    /**
     * Sets the value of the chainGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChainGroup(String value) {
        this.chainGroup = value;
    }

    /**
     * Gets the value of the qtyLimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQtyLimit() {
        return qtyLimit;
    }

    /**
     * Sets the value of the qtyLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQtyLimit(String value) {
        this.qtyLimit = value;
    }

    /**
     * Gets the value of the qtyOffset property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQtyOffset() {
        return qtyOffset;
    }

    /**
     * Sets the value of the qtyOffset property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQtyOffset(String value) {
        this.qtyOffset = value;
    }

    /**
     * Gets the value of the forceUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForceUnitPrice() {
        return forceUnitPrice;
    }

    /**
     * Sets the value of the forceUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForceUnitPrice(String value) {
        this.forceUnitPrice = value;
    }

    /**
     * Gets the value of the discfix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscfix() {
        return discfix;
    }

    /**
     * Sets the value of the discfix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscfix(String value) {
        this.discfix = value;
    }

    /**
     * Gets the value of the discpct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscpct() {
        return discpct;
    }

    /**
     * Sets the value of the discpct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscpct(String value) {
        this.discpct = value;
    }

    /**
     * Gets the value of the disclimit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisclimit() {
        return disclimit;
    }

    /**
     * Sets the value of the disclimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisclimit(String value) {
        this.disclimit = value;
    }

    /**
     * Gets the value of the scale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScale() {
        return scale;
    }

    /**
     * Sets the value of the scale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScale(String value) {
        this.scale = value;
    }

    /**
     * Gets the value of the each property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEach() {
        return each;
    }

    /**
     * Sets the value of the each property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEach(String value) {
        this.each = value;
    }

    /**
     * Gets the value of the total property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotal() {
        return total;
    }

    /**
     * Sets the value of the total property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotal(String value) {
        this.total = value;
    }

}
