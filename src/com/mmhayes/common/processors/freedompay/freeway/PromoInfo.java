
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PromoInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PromoInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *       &lt;/all>
 *       &lt;attribute name="code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="desc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="hints" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="buydown" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="moreInfo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="disclaim" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="apr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="payments" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="days" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="untilDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PromoInfo", propOrder = {

})
public class PromoInfo {

    @XmlAttribute(name = "code")
    protected String code;
    @XmlAttribute(name = "desc")
    protected String desc;
    @XmlAttribute(name = "hints")
    protected String hints;
    @XmlAttribute(name = "buydown")
    protected String buydown;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "moreInfo")
    protected String moreInfo;
    @XmlAttribute(name = "disclaim")
    protected String disclaim;
    @XmlAttribute(name = "apr")
    protected String apr;
    @XmlAttribute(name = "payments")
    protected String payments;
    @XmlAttribute(name = "days")
    protected String days;
    @XmlAttribute(name = "untilDate")
    protected String untilDate;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the desc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Sets the value of the desc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesc(String value) {
        this.desc = value;
    }

    /**
     * Gets the value of the hints property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHints() {
        return hints;
    }

    /**
     * Sets the value of the hints property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHints(String value) {
        this.hints = value;
    }

    /**
     * Gets the value of the buydown property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuydown() {
        return buydown;
    }

    /**
     * Sets the value of the buydown property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuydown(String value) {
        this.buydown = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the moreInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoreInfo() {
        return moreInfo;
    }

    /**
     * Sets the value of the moreInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoreInfo(String value) {
        this.moreInfo = value;
    }

    /**
     * Gets the value of the disclaim property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisclaim() {
        return disclaim;
    }

    /**
     * Sets the value of the disclaim property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisclaim(String value) {
        this.disclaim = value;
    }

    /**
     * Gets the value of the apr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApr() {
        return apr;
    }

    /**
     * Sets the value of the apr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApr(String value) {
        this.apr = value;
    }

    /**
     * Gets the value of the payments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayments() {
        return payments;
    }

    /**
     * Sets the value of the payments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayments(String value) {
        this.payments = value;
    }

    /**
     * Gets the value of the days property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDays() {
        return days;
    }

    /**
     * Sets the value of the days property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDays(String value) {
        this.days = value;
    }

    /**
     * Gets the value of the untilDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUntilDate() {
        return untilDate;
    }

    /**
     * Sets the value of the untilDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUntilDate(String value) {
        this.untilDate = value;
    }

}
