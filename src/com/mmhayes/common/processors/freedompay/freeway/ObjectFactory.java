
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.freedompay.freeway package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.freedompay.freeway
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SubmitResponse }
     * 
     */
    public SubmitResponse createSubmitResponse() {
        return new SubmitResponse();
    }

    /**
     * Create an instance of {@link ReplyMessage }
     * 
     */
    public ReplyMessage createReplyMessage() {
        return new ReplyMessage();
    }

    /**
     * Create an instance of {@link Submit }
     * 
     */
    public Submit createSubmit() {
        return new Submit();
    }

    /**
     * Create an instance of {@link RequestMessage }
     * 
     */
    public RequestMessage createRequestMessage() {
        return new RequestMessage();
    }

    /**
     * Create an instance of {@link LoyaltyService }
     * 
     */
    public LoyaltyService createLoyaltyService() {
        return new LoyaltyService();
    }

    /**
     * Create an instance of {@link AdminService }
     * 
     */
    public AdminService createAdminService() {
        return new AdminService();
    }

    /**
     * Create an instance of {@link PromoInfo }
     * 
     */
    public PromoInfo createPromoInfo() {
        return new PromoInfo();
    }

    /**
     * Create an instance of {@link PromoReply }
     * 
     */
    public PromoReply createPromoReply() {
        return new PromoReply();
    }

    /**
     * Create an instance of {@link ArrayOfItemDiscounts }
     * 
     */
    public ArrayOfItemDiscounts createArrayOfItemDiscounts() {
        return new ArrayOfItemDiscounts();
    }

    /**
     * Create an instance of {@link CCAuthService }
     * 
     */
    public CCAuthService createCCAuthService() {
        return new CCAuthService();
    }

    /**
     * Create an instance of {@link CCCaptureService }
     * 
     */
    public CCCaptureService createCCCaptureService() {
        return new CCCaptureService();
    }

    /**
     * Create an instance of {@link EIDDetail }
     * 
     */
    public EIDDetail createEIDDetail() {
        return new EIDDetail();
    }

    /**
     * Create an instance of {@link DiscountMeta }
     * 
     */
    public DiscountMeta createDiscountMeta() {
        return new DiscountMeta();
    }

    /**
     * Create an instance of {@link AutoRentalData }
     * 
     */
    public AutoRentalData createAutoRentalData() {
        return new AutoRentalData();
    }

    /**
     * Create an instance of {@link EfvOptions }
     * 
     */
    public EfvOptions createEfvOptions() {
        return new EfvOptions();
    }

    /**
     * Create an instance of {@link CCCreditReply }
     * 
     */
    public CCCreditReply createCCCreditReply() {
        return new CCCreditReply();
    }

    /**
     * Create an instance of {@link Item }
     * 
     */
    public Item createItem() {
        return new Item();
    }

    /**
     * Create an instance of {@link EodService }
     * 
     */
    public EodService createEodService() {
        return new EodService();
    }

    /**
     * Create an instance of {@link CCFollowupReply }
     * 
     */
    public CCFollowupReply createCCFollowupReply() {
        return new CCFollowupReply();
    }

    /**
     * Create an instance of {@link MobileReply }
     * 
     */
    public MobileReply createMobileReply() {
        return new MobileReply();
    }

    /**
     * Create an instance of {@link PurchaseTotals }
     * 
     */
    public PurchaseTotals createPurchaseTotals() {
        return new PurchaseTotals();
    }

    /**
     * Create an instance of {@link BillTo }
     * 
     */
    public BillTo createBillTo() {
        return new BillTo();
    }

    /**
     * Create an instance of {@link ItemPromos }
     * 
     */
    public ItemPromos createItemPromos() {
        return new ItemPromos();
    }

    /**
     * Create an instance of {@link HealthcareDataWeb }
     * 
     */
    public HealthcareDataWeb createHealthcareDataWeb() {
        return new HealthcareDataWeb();
    }

    /**
     * Create an instance of {@link ArrayOfDiscountInfo }
     * 
     */
    public ArrayOfDiscountInfo createArrayOfDiscountInfo() {
        return new ArrayOfDiscountInfo();
    }

    /**
     * Create an instance of {@link VendProduct }
     * 
     */
    public VendProduct createVendProduct() {
        return new VendProduct();
    }

    /**
     * Create an instance of {@link CCFollowupService }
     * 
     */
    public CCFollowupService createCCFollowupService() {
        return new CCFollowupService();
    }

    /**
     * Create an instance of {@link FleetData }
     * 
     */
    public FleetData createFleetData() {
        return new FleetData();
    }

    /**
     * Create an instance of {@link InvoiceDiscountDetail }
     * 
     */
    public InvoiceDiscountDetail createInvoiceDiscountDetail() {
        return new InvoiceDiscountDetail();
    }

    /**
     * Create an instance of {@link CCCreditService }
     * 
     */
    public CCCreditService createCCCreditService() {
        return new CCCreditService();
    }

    /**
     * Create an instance of {@link ShipFrom }
     * 
     */
    public ShipFrom createShipFrom() {
        return new ShipFrom();
    }

    /**
     * Create an instance of {@link MessageReply }
     * 
     */
    public MessageReply createMessageReply() {
        return new MessageReply();
    }

    /**
     * Create an instance of {@link DiscountInfo }
     * 
     */
    public DiscountInfo createDiscountInfo() {
        return new DiscountInfo();
    }

    /**
     * Create an instance of {@link TORService }
     * 
     */
    public TORService createTORService() {
        return new TORService();
    }

    /**
     * Create an instance of {@link VendControlService }
     * 
     */
    public VendControlService createVendControlService() {
        return new VendControlService();
    }

    /**
     * Create an instance of {@link InquiryReply }
     * 
     */
    public InquiryReply createInquiryReply() {
        return new InquiryReply();
    }

    /**
     * Create an instance of {@link Card }
     * 
     */
    public Card createCard() {
        return new Card();
    }

    /**
     * Create an instance of {@link VendControlReply }
     * 
     */
    public VendControlReply createVendControlReply() {
        return new VendControlReply();
    }

    /**
     * Create an instance of {@link NetworkCustomData }
     * 
     */
    public NetworkCustomData createNetworkCustomData() {
        return new NetworkCustomData();
    }

    /**
     * Create an instance of {@link ArrayOfEIDDetail }
     * 
     */
    public ArrayOfEIDDetail createArrayOfEIDDetail() {
        return new ArrayOfEIDDetail();
    }

    /**
     * Create an instance of {@link SessionReply }
     * 
     */
    public SessionReply createSessionReply() {
        return new SessionReply();
    }

    /**
     * Create an instance of {@link PromoService }
     * 
     */
    public PromoService createPromoService() {
        return new PromoService();
    }

    /**
     * Create an instance of {@link Pos }
     * 
     */
    public Pos createPos() {
        return new Pos();
    }

    /**
     * Create an instance of {@link ClientMetadata }
     * 
     */
    public ClientMetadata createClientMetadata() {
        return new ClientMetadata();
    }

    /**
     * Create an instance of {@link ArrayOfString1 }
     * 
     */
    public ArrayOfString1 createArrayOfString1() {
        return new ArrayOfString1();
    }

    /**
     * Create an instance of {@link ArrayOfString2 }
     * 
     */
    public ArrayOfString2 createArrayOfString2() {
        return new ArrayOfString2();
    }

    /**
     * Create an instance of {@link ArrayOfString3 }
     * 
     */
    public ArrayOfString3 createArrayOfString3() {
        return new ArrayOfString3();
    }

    /**
     * Create an instance of {@link FraudCheckReply }
     * 
     */
    public FraudCheckReply createFraudCheckReply() {
        return new FraudCheckReply();
    }

    /**
     * Create an instance of {@link TaxDetail }
     * 
     */
    public TaxDetail createTaxDetail() {
        return new TaxDetail();
    }

    /**
     * Create an instance of {@link ArrayOfItemPromos }
     * 
     */
    public ArrayOfItemPromos createArrayOfItemPromos() {
        return new ArrayOfItemPromos();
    }

    /**
     * Create an instance of {@link ItemDiscounts }
     * 
     */
    public ItemDiscounts createItemDiscounts() {
        return new ItemDiscounts();
    }

    /**
     * Create an instance of {@link VendProductList }
     * 
     */
    public VendProductList createVendProductList() {
        return new VendProductList();
    }

    /**
     * Create an instance of {@link VoidService }
     * 
     */
    public VoidService createVoidService() {
        return new VoidService();
    }

    /**
     * Create an instance of {@link TokenCreateReply }
     * 
     */
    public TokenCreateReply createTokenCreateReply() {
        return new TokenCreateReply();
    }

    /**
     * Create an instance of {@link ArrayOfLoyaltyProgramData }
     * 
     */
    public ArrayOfLoyaltyProgramData createArrayOfLoyaltyProgramData() {
        return new ArrayOfLoyaltyProgramData();
    }

    /**
     * Create an instance of {@link ArrayOfItem }
     * 
     */
    public ArrayOfItem createArrayOfItem() {
        return new ArrayOfItem();
    }

    /**
     * Create an instance of {@link RestaurantData }
     * 
     */
    public RestaurantData createRestaurantData() {
        return new RestaurantData();
    }

    /**
     * Create an instance of {@link CCCaptureReply }
     * 
     */
    public CCCaptureReply createCCCaptureReply() {
        return new CCCaptureReply();
    }

    /**
     * Create an instance of {@link LoyaltyProgramData }
     * 
     */
    public LoyaltyProgramData createLoyaltyProgramData() {
        return new LoyaltyProgramData();
    }

    /**
     * Create an instance of {@link InvoicePromos }
     * 
     */
    public InvoicePromos createInvoicePromos() {
        return new InvoicePromos();
    }

    /**
     * Create an instance of {@link DccReply }
     * 
     */
    public DccReply createDccReply() {
        return new DccReply();
    }

    /**
     * Create an instance of {@link MobileService }
     * 
     */
    public MobileService createMobileService() {
        return new MobileService();
    }

    /**
     * Create an instance of {@link DccInfo }
     * 
     */
    public DccInfo createDccInfo() {
        return new DccInfo();
    }

    /**
     * Create an instance of {@link MerchantDefinedData }
     * 
     */
    public MerchantDefinedData createMerchantDefinedData() {
        return new MerchantDefinedData();
    }

    /**
     * Create an instance of {@link DiscountService }
     * 
     */
    public DiscountService createDiscountService() {
        return new DiscountService();
    }

    /**
     * Create an instance of {@link TokenCreateService }
     * 
     */
    public TokenCreateService createTokenCreateService() {
        return new TokenCreateService();
    }

    /**
     * Create an instance of {@link TaxDetailItem }
     * 
     */
    public TaxDetailItem createTaxDetailItem() {
        return new TaxDetailItem();
    }

    /**
     * Create an instance of {@link ArrayOfMessageInfo }
     * 
     */
    public ArrayOfMessageInfo createArrayOfMessageInfo() {
        return new ArrayOfMessageInfo();
    }

    /**
     * Create an instance of {@link TokenInformation }
     * 
     */
    public TokenInformation createTokenInformation() {
        return new TokenInformation();
    }

    /**
     * Create an instance of {@link AdminReply }
     * 
     */
    public AdminReply createAdminReply() {
        return new AdminReply();
    }

    /**
     * Create an instance of {@link EodReply }
     * 
     */
    public EodReply createEodReply() {
        return new EodReply();
    }

    /**
     * Create an instance of {@link MessageService }
     * 
     */
    public MessageService createMessageService() {
        return new MessageService();
    }

    /**
     * Create an instance of {@link NetworkData }
     * 
     */
    public NetworkData createNetworkData() {
        return new NetworkData();
    }

    /**
     * Create an instance of {@link LoyaltyReply }
     * 
     */
    public LoyaltyReply createLoyaltyReply() {
        return new LoyaltyReply();
    }

    /**
     * Create an instance of {@link MessageInfo }
     * 
     */
    public MessageInfo createMessageInfo() {
        return new MessageInfo();
    }

    /**
     * Create an instance of {@link InquiryService }
     * 
     */
    public InquiryService createInquiryService() {
        return new InquiryService();
    }

    /**
     * Create an instance of {@link ArrayOfNetworkCustomData }
     * 
     */
    public ArrayOfNetworkCustomData createArrayOfNetworkCustomData() {
        return new ArrayOfNetworkCustomData();
    }

    /**
     * Create an instance of {@link InvoiceHeader }
     * 
     */
    public InvoiceHeader createInvoiceHeader() {
        return new InvoiceHeader();
    }

    /**
     * Create an instance of {@link SessionService }
     * 
     */
    public SessionService createSessionService() {
        return new SessionService();
    }

    /**
     * Create an instance of {@link VoidReply }
     * 
     */
    public VoidReply createVoidReply() {
        return new VoidReply();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link DccService }
     * 
     */
    public DccService createDccService() {
        return new DccService();
    }

    /**
     * Create an instance of {@link FraudCheckService }
     * 
     */
    public FraudCheckService createFraudCheckService() {
        return new FraudCheckService();
    }

    /**
     * Create an instance of {@link CCAuthReply }
     * 
     */
    public CCAuthReply createCCAuthReply() {
        return new CCAuthReply();
    }

    /**
     * Create an instance of {@link ShipTo }
     * 
     */
    public ShipTo createShipTo() {
        return new ShipTo();
    }

    /**
     * Create an instance of {@link ReceiptData }
     * 
     */
    public ReceiptData createReceiptData() {
        return new ReceiptData();
    }

    /**
     * Create an instance of {@link DiscountReply }
     * 
     */
    public DiscountReply createDiscountReply() {
        return new DiscountReply();
    }

    /**
     * Create an instance of {@link TORReply }
     * 
     */
    public TORReply createTORReply() {
        return new TORReply();
    }

    /**
     * Create an instance of {@link ArrayOfPromoInfo }
     * 
     */
    public ArrayOfPromoInfo createArrayOfPromoInfo() {
        return new ArrayOfPromoInfo();
    }

    /**
     * Create an instance of {@link HotelData }
     * 
     */
    public HotelData createHotelData() {
        return new HotelData();
    }

    /**
     * Create an instance of {@link ArrayOfDiscountMeta }
     * 
     */
    public ArrayOfDiscountMeta createArrayOfDiscountMeta() {
        return new ArrayOfDiscountMeta();
    }

}
