
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NetworkData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NetworkData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="network" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="stan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aci" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tdcc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="downgradeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cvcError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="authSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="responseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="productId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="programId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="merchantAdvice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fleetPrompts" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customData" type="{http://freeway.freedompay.com/}ArrayOfNetworkCustomData" minOccurs="0"/>
 *         &lt;element name="mac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="posSeqNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="macKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pinKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fieldKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hostControlData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NetworkData", propOrder = {

})
public class NetworkData {

    protected String network;
    protected String stan;
    protected String aci;
    protected String tdcc;
    protected String pcode;
    protected String vcode;
    protected String downgradeCode;
    protected String cvcError;
    protected String dataError;
    protected String authSource;
    protected String responseCode;
    protected String productId;
    protected String programId;
    protected String merchantAdvice;
    protected String serviceOption;
    protected String fleetPrompts;
    protected ArrayOfNetworkCustomData customData;
    protected String mac;
    protected String posSeqNum;
    protected String macKey;
    protected String pinKey;
    protected String fieldKey;
    protected String transDate;
    protected String transTime;
    protected String hostControlData;

    /**
     * Gets the value of the network property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetwork() {
        return network;
    }

    /**
     * Sets the value of the network property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetwork(String value) {
        this.network = value;
    }

    /**
     * Gets the value of the stan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStan() {
        return stan;
    }

    /**
     * Sets the value of the stan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStan(String value) {
        this.stan = value;
    }

    /**
     * Gets the value of the aci property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAci() {
        return aci;
    }

    /**
     * Sets the value of the aci property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAci(String value) {
        this.aci = value;
    }

    /**
     * Gets the value of the tdcc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTdcc() {
        return tdcc;
    }

    /**
     * Sets the value of the tdcc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTdcc(String value) {
        this.tdcc = value;
    }

    /**
     * Gets the value of the pcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPcode() {
        return pcode;
    }

    /**
     * Sets the value of the pcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPcode(String value) {
        this.pcode = value;
    }

    /**
     * Gets the value of the vcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVcode() {
        return vcode;
    }

    /**
     * Sets the value of the vcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVcode(String value) {
        this.vcode = value;
    }

    /**
     * Gets the value of the downgradeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDowngradeCode() {
        return downgradeCode;
    }

    /**
     * Sets the value of the downgradeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDowngradeCode(String value) {
        this.downgradeCode = value;
    }

    /**
     * Gets the value of the cvcError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCvcError() {
        return cvcError;
    }

    /**
     * Sets the value of the cvcError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCvcError(String value) {
        this.cvcError = value;
    }

    /**
     * Gets the value of the dataError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataError() {
        return dataError;
    }

    /**
     * Sets the value of the dataError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataError(String value) {
        this.dataError = value;
    }

    /**
     * Gets the value of the authSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthSource() {
        return authSource;
    }

    /**
     * Sets the value of the authSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthSource(String value) {
        this.authSource = value;
    }

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseCode(String value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the productId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductId(String value) {
        this.productId = value;
    }

    /**
     * Gets the value of the programId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramId() {
        return programId;
    }

    /**
     * Sets the value of the programId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramId(String value) {
        this.programId = value;
    }

    /**
     * Gets the value of the merchantAdvice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantAdvice() {
        return merchantAdvice;
    }

    /**
     * Sets the value of the merchantAdvice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantAdvice(String value) {
        this.merchantAdvice = value;
    }

    /**
     * Gets the value of the serviceOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOption() {
        return serviceOption;
    }

    /**
     * Sets the value of the serviceOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOption(String value) {
        this.serviceOption = value;
    }

    /**
     * Gets the value of the fleetPrompts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFleetPrompts() {
        return fleetPrompts;
    }

    /**
     * Sets the value of the fleetPrompts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFleetPrompts(String value) {
        this.fleetPrompts = value;
    }

    /**
     * Gets the value of the customData property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfNetworkCustomData }
     *     
     */
    public ArrayOfNetworkCustomData getCustomData() {
        return customData;
    }

    /**
     * Sets the value of the customData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfNetworkCustomData }
     *     
     */
    public void setCustomData(ArrayOfNetworkCustomData value) {
        this.customData = value;
    }

    /**
     * Gets the value of the mac property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMac() {
        return mac;
    }

    /**
     * Sets the value of the mac property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMac(String value) {
        this.mac = value;
    }

    /**
     * Gets the value of the posSeqNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosSeqNum() {
        return posSeqNum;
    }

    /**
     * Sets the value of the posSeqNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosSeqNum(String value) {
        this.posSeqNum = value;
    }

    /**
     * Gets the value of the macKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacKey() {
        return macKey;
    }

    /**
     * Sets the value of the macKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacKey(String value) {
        this.macKey = value;
    }

    /**
     * Gets the value of the pinKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPinKey() {
        return pinKey;
    }

    /**
     * Sets the value of the pinKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPinKey(String value) {
        this.pinKey = value;
    }

    /**
     * Gets the value of the fieldKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldKey() {
        return fieldKey;
    }

    /**
     * Sets the value of the fieldKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldKey(String value) {
        this.fieldKey = value;
    }

    /**
     * Gets the value of the transDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransDate() {
        return transDate;
    }

    /**
     * Sets the value of the transDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransDate(String value) {
        this.transDate = value;
    }

    /**
     * Gets the value of the transTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransTime() {
        return transTime;
    }

    /**
     * Sets the value of the transTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransTime(String value) {
        this.transTime = value;
    }

    /**
     * Gets the value of the hostControlData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHostControlData() {
        return hostControlData;
    }

    /**
     * Sets the value of the hostControlData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHostControlData(String value) {
        this.hostControlData = value;
    }

}
