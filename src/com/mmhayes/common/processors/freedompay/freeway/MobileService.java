
package com.mmhayes.common.processors.freedompay.freeway;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MobileService complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MobileService">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="featureFlags1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="featureFlags2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="functionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="checkId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="acceptTip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="openTab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentSessionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mhaCallbackKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="qrseq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="barData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="barType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="confirmPaySeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paySeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliedDiscountAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *       &lt;attribute name="run" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MobileService", propOrder = {

})
public class MobileService {

    protected String featureFlags1;
    protected String featureFlags2;
    protected String functionCode;
    protected String checkId;
    protected String acceptTip;
    protected String openTab;
    protected String paymentSessionId;
    protected String mhaCallbackKey;
    protected String qrseq;
    protected String barData;
    protected String barType;
    protected String confirmPaySeq;
    protected String paySeq;
    protected String appliedDiscountAmount;
    @XmlAttribute(name = "run")
    protected String run;

    /**
     * Gets the value of the featureFlags1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeatureFlags1() {
        return featureFlags1;
    }

    /**
     * Sets the value of the featureFlags1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureFlags1(String value) {
        this.featureFlags1 = value;
    }

    /**
     * Gets the value of the featureFlags2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeatureFlags2() {
        return featureFlags2;
    }

    /**
     * Sets the value of the featureFlags2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureFlags2(String value) {
        this.featureFlags2 = value;
    }

    /**
     * Gets the value of the functionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFunctionCode() {
        return functionCode;
    }

    /**
     * Sets the value of the functionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFunctionCode(String value) {
        this.functionCode = value;
    }

    /**
     * Gets the value of the checkId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckId() {
        return checkId;
    }

    /**
     * Sets the value of the checkId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckId(String value) {
        this.checkId = value;
    }

    /**
     * Gets the value of the acceptTip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcceptTip() {
        return acceptTip;
    }

    /**
     * Sets the value of the acceptTip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcceptTip(String value) {
        this.acceptTip = value;
    }

    /**
     * Gets the value of the openTab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpenTab() {
        return openTab;
    }

    /**
     * Sets the value of the openTab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpenTab(String value) {
        this.openTab = value;
    }

    /**
     * Gets the value of the paymentSessionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentSessionId() {
        return paymentSessionId;
    }

    /**
     * Sets the value of the paymentSessionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentSessionId(String value) {
        this.paymentSessionId = value;
    }

    /**
     * Gets the value of the mhaCallbackKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMhaCallbackKey() {
        return mhaCallbackKey;
    }

    /**
     * Sets the value of the mhaCallbackKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMhaCallbackKey(String value) {
        this.mhaCallbackKey = value;
    }

    /**
     * Gets the value of the qrseq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQrseq() {
        return qrseq;
    }

    /**
     * Sets the value of the qrseq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQrseq(String value) {
        this.qrseq = value;
    }

    /**
     * Gets the value of the barData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarData() {
        return barData;
    }

    /**
     * Sets the value of the barData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarData(String value) {
        this.barData = value;
    }

    /**
     * Gets the value of the barType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBarType() {
        return barType;
    }

    /**
     * Sets the value of the barType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBarType(String value) {
        this.barType = value;
    }

    /**
     * Gets the value of the confirmPaySeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmPaySeq() {
        return confirmPaySeq;
    }

    /**
     * Sets the value of the confirmPaySeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmPaySeq(String value) {
        this.confirmPaySeq = value;
    }

    /**
     * Gets the value of the paySeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaySeq() {
        return paySeq;
    }

    /**
     * Sets the value of the paySeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaySeq(String value) {
        this.paySeq = value;
    }

    /**
     * Gets the value of the appliedDiscountAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliedDiscountAmount() {
        return appliedDiscountAmount;
    }

    /**
     * Sets the value of the appliedDiscountAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliedDiscountAmount(String value) {
        this.appliedDiscountAmount = value;
    }

    /**
     * Gets the value of the run property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRun() {
        return run;
    }

    /**
     * Sets the value of the run property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRun(String value) {
        this.run = value;
    }

}
