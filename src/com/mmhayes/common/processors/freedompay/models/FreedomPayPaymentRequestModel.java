package com.mmhayes.common.processors.freedompay.models;

//MMHayes Dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.FundingException;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.processors.freedompay.FreedomPayHelper;
import com.mmhayes.common.funding.FundingHelper;
import com.mmhayes.common.processors.freedompay.freeway.ReplyMessage;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.utils.Logger;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonIgnore;

//Other Dependencies
import java.math.BigDecimal;
import java.util.HashMap;

public class FreedomPayPaymentRequestModel {

    private BigDecimal amount = null;
    private TerminalModel terminal = null; //terminal where the transaction took place
    private TerminalModel posTerminal = null;

    private AccountModel account = null;
    private String requestId = null;
    private FreedomPayPaymentResponseModel freedomPayPaymentResponseModel = null;

    private String freedomPayStoreNum = null;
    private String freedomPayTerminalNum = null;

    public FreedomPayPaymentRequestModel() {

    }

    public void validate() throws Exception {
        this.setFreedomPayPaymentResponseModel(new FreedomPayPaymentResponseModel());
        this.getFreedomPayPaymentResponseModel().setRequestId(this.getRequestId());
        this.getFreedomPayPaymentResponseModel().setAuthorizedAmount(BigDecimal.ZERO);
        this.getFreedomPayPaymentResponseModel().setRequestedAmount(this.getAmount());

        //validate terminal
        this.setTerminal(new TerminalModel().createTerminalModel(getTerminal().getId()));

        //validate account
        if (this.getAccount() != null && this.getAccount().getId() != null && !this.getAccount().getId().toString().isEmpty()) {
            this.setAccount(AccountModel.getAccountModelById(this.getAccount().getId(), this.getPosTerminal()));
        }

        //validate processor info
        if (this.getTerminal().getTypeId().equals(PosAPIHelper.TerminalType.ONLINE_ORDERING.toInt())) {
            if (this.getTerminal().getUseFundingTerminal()) {
                HashMap accountPaymentMethodHM = new HashMap();

                //if for person, then the employeeID will not be set, so set it
                accountPaymentMethodHM.put("EMPLOYEEID", this.getAccount().getId());

                //the api keys will not be set, retrieve and set them
                accountPaymentMethodHM = FundingHelper.setPaymentProcessorCredentials(accountPaymentMethodHM);

                if (accountPaymentMethodHM == null || accountPaymentMethodHM.isEmpty()) {
                    throw new MissingDataException("Could not properly set Payment Processor Info");
                }

                //HashMap modelDetailHM = paymentProcessorList.get(0);
                Integer paymentProcessorID = CommonAPI.convertModelDetailToInteger(accountPaymentMethodHM.get("PAYMENTPROCESSORID"));
                if (paymentProcessorID == 6) {
                    this.setFreedomPayStoreNum(CommonAPI.convertModelDetailToString(accountPaymentMethodHM.get("PAYMENTPROCESSORSTORENUM")));
                    this.setFreedomPayTerminalNum(CommonAPI.convertModelDetailToString(accountPaymentMethodHM.get("PAYMENTPROCESSORTERMINALNUM")));
                } else {
                    throw new MissingDataException("Invalid Payment Processor: Refunds not available");
                }
            } else {
                //pull configuration from the MyQC terminal
                this.setFreedomPayStoreNum(this.getTerminal().getPaymentProcessorStoreNum());
                this.setFreedomPayTerminalNum(this.getTerminal().getPaymentProcessorTerminalNum());
            }

            if (this.getFreedomPayStoreNum() == null || this.getFreedomPayStoreNum().isEmpty()) {
                throw new MissingDataException("Invalid Processor Store Num");
            }

            if (this.getFreedomPayTerminalNum() == null || this.getFreedomPayTerminalNum().isEmpty()) {
                throw new MissingDataException("Invalid Processor Terminal Num");
            }

        } else {
            throw new FundingException("Invalid Terminal Type on original transaction.");
        }
    }

    public void refund() throws Exception {
        FreedomPayHelper freedomPayHelper = new FreedomPayHelper();
        freedomPayHelper.setStoreID(this.getFreedomPayStoreNum());
        freedomPayHelper.setTerminalID(this.getFreedomPayTerminalNum());

        // Execute Refund Call
        ReplyMessage replyMessage = freedomPayHelper.refund(this.getRequestId(), this.getAmount(), this.getPosTerminal().getTerminalLogFileName());

        try {
            this.getFreedomPayPaymentResponseModel().setAuthorizedAmount(new BigDecimal(replyMessage.getCcCreditReply().getAmount()));
            this.getFreedomPayPaymentResponseModel().setProcessorResponseCode(replyMessage.getCcCreditReply().getProcessorResponseCode());
            this.getFreedomPayPaymentResponseModel().setProcessorResponseMessage(replyMessage.getCcCreditReply().getProcessorResponseMessage());
            this.getFreedomPayPaymentResponseModel().setAuthorizationCode(replyMessage.getCcCreditReply().getAuthorizationCode());
            this.getFreedomPayPaymentResponseModel().setAuthorizationCode(replyMessage.getCcCreditReply().getAuthorizationCode());
            this.getFreedomPayPaymentResponseModel().setDecision(replyMessage.getDecision());
            this.getFreedomPayPaymentResponseModel().setMerchantReferenceCode(replyMessage.getMerchantReferenceCode());
            this.getFreedomPayPaymentResponseModel().setReasonCode(replyMessage.getReasonCode().toString());
            this.getFreedomPayPaymentResponseModel().setRequestId(replyMessage.getRequestID());
            this.getFreedomPayPaymentResponseModel().setProcessorTransactionId(replyMessage.getCcCreditReply().getProcessorTransactionID());
            this.getPosTerminal().logTransactionTime();

        } catch (Exception ex) {
            Logger.logMessage("Error setting FreedomPayPaymentResponse fields: " + ex.getMessage(), Logger.LEVEL.ERROR);
        }
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public TerminalModel getTerminal() {
        return terminal;
    }

    public void setTerminal(TerminalModel terminal) {
        this.terminal = terminal;
    }

    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public FreedomPayPaymentResponseModel getFreedomPayPaymentResponseModel() {
        return freedomPayPaymentResponseModel;
    }

    public void setFreedomPayPaymentResponseModel(FreedomPayPaymentResponseModel freedomPayPaymentResponseModel) {
        this.freedomPayPaymentResponseModel = freedomPayPaymentResponseModel;
    }

    public TerminalModel getPosTerminal() {
        return posTerminal;
    }

    public void setPosTerminal(TerminalModel posTerminal) {
        this.posTerminal = posTerminal;
    }

    @JsonIgnore
    public String getFreedomPayStoreNum() {
        return freedomPayStoreNum;
    }

    public void setFreedomPayStoreNum(String freedomPayStoreNum) {
        this.freedomPayStoreNum = freedomPayStoreNum;
    }

    @JsonIgnore
    public String getFreedomPayTerminalNum() {
        return freedomPayTerminalNum;
    }

    public void setFreedomPayTerminalNum(String freedomPayTerminalNum) {
        this.freedomPayTerminalNum = freedomPayTerminalNum;
    }
}
