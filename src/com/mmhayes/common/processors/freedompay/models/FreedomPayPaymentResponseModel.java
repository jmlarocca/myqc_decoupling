package com.mmhayes.common.processors.freedompay.models;

import java.math.BigDecimal;

public class FreedomPayPaymentResponseModel {
    private BigDecimal requestedAmount = null;
    private BigDecimal authorizedAmount = null;
    private String requestId = null;
    private String processorResponseCode = null;
    private String processorResponseMessage = null;
    private String authorizationCode = null;
    private String decision = null;
    private String merchantReferenceCode = null;
    private String reasonCode = null;
    private String processorTransactionId = null;

    public FreedomPayPaymentResponseModel(){

    }

    public BigDecimal getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(BigDecimal requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    public BigDecimal getAuthorizedAmount() {
        return authorizedAmount;
    }

    public void setAuthorizedAmount(BigDecimal authorizedAmount) {
        this.authorizedAmount = authorizedAmount;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getProcessorResponseCode() {
        return processorResponseCode;
    }

    public void setProcessorResponseCode(String processorResponseCode) {
        this.processorResponseCode = processorResponseCode;
    }

    public String getProcessorResponseMessage() {
        return processorResponseMessage;
    }

    public void setProcessorResponseMessage(String processorResponseMessage) {
        this.processorResponseMessage = processorResponseMessage;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public String getMerchantReferenceCode() {
        return merchantReferenceCode;
    }

    public void setMerchantReferenceCode(String merchantReferenceCode) {
        this.merchantReferenceCode = merchantReferenceCode;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getProcessorTransactionId() {
        return processorTransactionId;
    }

    public void setProcessorTransactionId(String processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
}
