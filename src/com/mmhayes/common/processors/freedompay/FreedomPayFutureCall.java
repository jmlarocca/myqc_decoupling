package com.mmhayes.common.processors.freedompay;

import com.mmhayes.common.processors.freedompay.freeway.FreewayService;
import com.mmhayes.common.processors.freedompay.freeway.RequestMessage;

import java.util.concurrent.Callable;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Callable object that wraps the SOAP Freeway Call to allow timeout
 */
public class FreedomPayFutureCall implements Callable {

    private FreewayService freewayService;
    private RequestMessage requestMessage;

    @Override
    public Object call() throws Exception {
        // Execute Soap Call
        return getFreewayService().getFreewayServiceSoap().submit(getRequestMessage());
    }

    public FreewayService getFreewayService() {
        return freewayService;
    }

    public void setFreewayService(FreewayService freewayService) {
        this.freewayService = freewayService;
    }

    public RequestMessage getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(RequestMessage requestMessage) {
        this.requestMessage = requestMessage;
    }

}
