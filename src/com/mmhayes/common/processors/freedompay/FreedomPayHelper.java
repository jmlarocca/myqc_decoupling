package com.mmhayes.common.processors.freedompay;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.FundingCardException;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.processors.freedompay.freeway.*;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.*;

/*
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Helper class to support calls to the FreedomPay Soap API
 */
public class FreedomPayHelper {

    private String storeID = "-1";
    private String terminalID = "-1";
    private String status = "";
    private String paymentToken = "";
    private String requestID = "";
    private Integer TIMEOUT_SECONDS;
    private String version = "";
    private BigInteger responseNumber;
    private String responseID;

    // Static vars
    private static final String CARD_TYPE_TOKEN = "token";
    private static final String DECISION_ACCEPT = "ACCEPT";

    public FreedomPayHelper(){
        try{
            TIMEOUT_SECONDS = CommonAPI.getFreedomPayTimeoutSeconds();
            setVersion(CommonAPI.getAPIVersion());
        } catch(Exception ex){
            Logger.logMessage("Error in FreedomPayHelper constructor...", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    public BigDecimal charge(BigDecimal amount) throws Exception {
        return charge(amount, false);
    }

    public BigDecimal charge(BigDecimal amount, boolean allowPartialAuth) throws Exception {

        Logger.logMessage("STARTING method FreedomPay.charge()", Logger.LEVEL.DEBUG);

        // init return VAR
        BigDecimal chargeResult;
        String DECISION_ACCEPT = "ACCEPT";

        // Build Card
        Card card = new Card();
        card.setCardType(CARD_TYPE_TOKEN);
        card.setAccountNumber(getPaymentToken());

        // create purchaseTotals ie Amount
        PurchaseTotals purchaseTotals = new PurchaseTotals();
        purchaseTotals.setChargeAmount(amount.toString());

        // Create Auth
        CCAuthService ccAuthService = new CCAuthService();
        ccAuthService.setRun("true");

        if( allowPartialAuth ) {
            ccAuthService.setAllowPartial("Y");
        }

        // Create Capture
        CCCaptureService ccCaptureService = new CCCaptureService();
        ccCaptureService.setRun("true");

        // extra data
        ClientMetadata clientMetadata = new ClientMetadata();
        clientMetadata.setApplicationName(commonMMHFunctions.getInstanceName());
        clientMetadata.setApplicationVersion(getVersion());

        // Add invoice per FP certification req
        InvoiceHeader invoiceHeader = new InvoiceHeader();
        invoiceHeader.setInvoiceNumber(createMerchantReferenceCode());

        // Build Request
        RequestMessage requestMessage = new RequestMessage();
        // Add API creds
        requestMessage.setStoreId(getStoreID());
        requestMessage.setTerminalId(getTerminalID());
        // Add Card
        requestMessage.setCard(card);
        // ADD Purchase Totals
        requestMessage.setPurchaseTotals(purchaseTotals);
        // ADD CCAuthService
        requestMessage.setCcAuthService(ccAuthService);
        // ADD CCCaptureService
        requestMessage.setCcCaptureService(ccCaptureService);
        // Add MerchantReferenceCode
        requestMessage.setMerchantReferenceCode(invoiceHeader.getInvoiceNumber());
        // Add Metadata
        requestMessage.setClientMetadata(clientMetadata);
        // Add invoice object
        requestMessage.setInvoiceHeader(invoiceHeader);

        // Create Service object
        FreewayService freewayService = new FreewayService();

        // CALL SERVICE
        ExecutorService executor = Executors.newCachedThreadPool();
        FreedomPayFutureCall task = new FreedomPayFutureCall();
        task.setFreewayService(freewayService);
        task.setRequestMessage(requestMessage);

        // Make future Call with timeout
        Logger.logMessage("FreedomPay Calling Charge Service...", Logger.LEVEL.DEBUG);
        ReplyMessage reply;
        Future<Object> future = executor.submit(task);
        try {
            Object result = future.get(TIMEOUT_SECONDS, TimeUnit.SECONDS);
            reply = (ReplyMessage)result;

            // Inspect Reply
            Logger.logMessage("FreedomPay Reply: Decision:"+reply.getDecision(), Logger.LEVEL.DEBUG);
            Logger.logMessage("FreedomPay Reply: ReasonCode:"+reply.getReasonCode().toString(), Logger.LEVEL.DEBUG);
            Logger.logMessage("FreedomPay Reply: RequestID:"+reply.getRequestID(), Logger.LEVEL.DEBUG);
            this.setResponseNumber(reply.getReasonCode());
            this.setResponseID(reply.getRequestID());

            if(reply.getDecision().equals(DECISION_ACCEPT)){
                this.setRequestID(reply.getRequestID());
                this.setStatus(reply.getCcCaptureReply().getProcessorResponseMessage());
                chargeResult = new BigDecimal(reply.getCcCaptureReply().getAmount());
                Logger.logMessage("ENDING method FreedomPay.charge()", Logger.LEVEL.DEBUG);

                if( allowPartialAuth && reply.getCcAuthReply().getPartialAmount().equals("Y")) {
                    chargeResult = new BigDecimal(reply.getCcAuthReply().getAmount());
                }

                return chargeResult;
            } else {
                Logger.logMessage("Error processing FreedomPay Charge...", Logger.LEVEL.ERROR);
                String decision = reply.getDecision() != null?reply.getDecision() : "";
                String reasonCode = reply.getReasonCode()!=null ? reply.getReasonCode().toString() : "";
                throw new FundingCardException("Unable to process charge: "+decision+" ("+reasonCode+")");
            }
        } catch (TimeoutException ex) {
            // handle the timeout
            Logger.logMessage("Attempting Timeout Reversal...", Logger.LEVEL.ERROR);
            TORService torService = new TORService();
            torService.setRun("true");
            requestMessage.setTorService(torService);
            ReplyMessage replyTimeout = freewayService.getFreewayServiceSoap().submit(requestMessage);
            Logger.logMessage("Timeout Reversal: Decision:"+replyTimeout.getDecision()+" ReasonCode: "+replyTimeout.getReasonCode(), Logger.LEVEL.ERROR);
            if(replyTimeout.getRequestID() != null){
                Logger.logMessage("Timeout Reversal: RequestID:"+replyTimeout.getRequestID(), Logger.LEVEL.ERROR);
            }
            throw new FundingCardException("Unable to process charge: "+replyTimeout.getDecision()+" ("+replyTimeout.getReasonCode()+")");
        } catch (InterruptedException e) {
            // handle the interrupts
            Logger.logMessage("FreedomPay encountered an Interrupted Exception.");
            Logger.logException(e);
            throw new FundingCardException("Unable to process charge due to interruption exception.");
        } catch (ExecutionException e) {
            // handle other exceptions
            Logger.logMessage("FreedomPay encountered an Execution Exception.");
            Logger.logException(e);
            throw new FundingCardException("Unable to process charge due to execution exception.");
        }
    }

    public BigDecimal refund(String requestID, BigDecimal amount) throws Exception {

        Logger.logMessage("STARTING method FreedomPay.refund()", Logger.LEVEL.DEBUG);

        // init return VAR
        BigDecimal refundResult;

        // create purchaseTotals ie Amount
        PurchaseTotals purchaseTotals = new PurchaseTotals();
        purchaseTotals.setChargeAmount(amount.toString());

        // Create CreditService
        CCCreditService ccCreditService = new CCCreditService();
        ccCreditService.setRun("true");

        // Create Client data
        ClientMetadata clientMetadata = new ClientMetadata();
        clientMetadata.setApplicationName(commonMMHFunctions.getInstanceName());
        clientMetadata.setApplicationVersion(getVersion());

        // Add invoice per FP certification req
        InvoiceHeader invoiceHeader = new InvoiceHeader();
        invoiceHeader.setInvoiceNumber(createMerchantReferenceCode());

        // Build Request
        RequestMessage requestMessage = new RequestMessage();
        requestMessage.setStoreId(getStoreID());
        requestMessage.setTerminalId(getTerminalID());
        requestMessage.setOrderRequestID(requestID);
        requestMessage.setPurchaseTotals(purchaseTotals);

        // ADD CCCreditService
        requestMessage.setCcCreditService(ccCreditService);
        // Add MerchantReferenceCode
        requestMessage.setMerchantReferenceCode(invoiceHeader.getInvoiceNumber());
        // Add Metadata
        requestMessage.setClientMetadata(clientMetadata);
        // Add invoice object
        requestMessage.setInvoiceHeader(invoiceHeader);

        // Create service object
        FreewayService freewayService = new FreewayService();

        // CALL SERVICE
        ExecutorService executor = Executors.newCachedThreadPool();
        FreedomPayFutureCall task = new FreedomPayFutureCall();
        task.setFreewayService(freewayService);
        task.setRequestMessage(requestMessage);

        // Make future Call with timeout
        Logger.logMessage("FreedomPay Calling Refund Service...", Logger.LEVEL.DEBUG);
        ReplyMessage reply;
        Future<Object> future = executor.submit(task);
        try {
            Object result = future.get(TIMEOUT_SECONDS, TimeUnit.SECONDS);
            reply = (ReplyMessage)result;

            // Inspect Reply
            if(reply.getDecision() !=null) {Logger.logMessage("FreedomPay Reply: Decision:"+reply.getDecision(), Logger.LEVEL.TRACE);}
            if(reply.getReasonCode() != null){Logger.logMessage("FreedomPay Reply: ReasonCode:"+reply.getReasonCode(), Logger.LEVEL.TRACE);}
            if(reply.getRequestID() != null){Logger.logMessage("FreedomPay Reply: RequestID:"+reply.getRequestID(), Logger.LEVEL.TRACE);}
            this.setResponseNumber(reply.getReasonCode());
            this.setResponseID(reply.getRequestID());

            if(reply.getDecision().equals(DECISION_ACCEPT)){
                this.setRequestID(reply.getRequestID());
                this.setStatus(reply.getCcCreditReply().getProcessorResponseMessage());
                refundResult = new BigDecimal(reply.getCcCreditReply().getAmount());
                Logger.logMessage("ENDING method FreedomPay.refund()", Logger.LEVEL.DEBUG);
                return refundResult;
            } else {
                Logger.logMessage("Error processing FreedomPay Refund...", Logger.LEVEL.ERROR);
                String decision = reply.getDecision() != null ? reply.getDecision(): "" ;
                String reasonCode = reply.getReasonCode() != null ? reply.getReasonCode().toString() : "";
                throw new FundingCardException("Unable to process refund: "+decision+" ("+reasonCode+")");
            }
        } catch (TimeoutException ex) {
            // handle the timeout
            Logger.logMessage("Attempting Timeout Reversal...", Logger.LEVEL.ERROR);
            TORService torService = new TORService();
            torService.setRun("true");
            requestMessage.setTorService(torService);
            ReplyMessage replyTimeout = freewayService.getFreewayServiceSoap().submit(requestMessage);
            Logger.logMessage("Timeout Reversal: Decision:"+replyTimeout.getDecision()+" ReasonCode: "+replyTimeout.getReasonCode(), Logger.LEVEL.ERROR);
            if(replyTimeout.getRequestID() != null){
                Logger.logMessage("Timeout Reversal: RequestID:"+replyTimeout.getRequestID(), Logger.LEVEL.ERROR);
            }
            throw new FundingCardException("Unable to process charge: "+replyTimeout.getDecision()+" ("+replyTimeout.getReasonCode()+")");
        } catch (InterruptedException e) {
            // handle the interrupts
            Logger.logMessage("FreedomPay encountered an Interrupted Exception.");
            Logger.logException(e);
            throw new FundingCardException("Unable to process charge due to interruption exception.");
        } catch (ExecutionException e) {
            // handle other exceptions
            Logger.logMessage("FreedomPay encountered an Execution Exception.");
            Logger.logException(e);
            throw new FundingCardException("Unable to process charge due to execution exception.");
        }
    }

    public ReplyMessage refund(String requestID, BigDecimal amount, String logFile) throws Exception {

        Logger.logMessage("STARTING method FreedomPay.refund()", logFile, Logger.LEVEL.TRACE);

        // create purchaseTotals ie Amount
        PurchaseTotals purchaseTotals = new PurchaseTotals();
        purchaseTotals.setChargeAmount(amount.toString());

        // Create CreditService
        CCCreditService ccCreditService = new CCCreditService();
        ccCreditService.setRun("true");

        // Create Client data
        ClientMetadata clientMetadata = new ClientMetadata();
        clientMetadata.setApplicationName(commonMMHFunctions.getInstanceName());
        clientMetadata.setApplicationVersion(getVersion());

        // Add invoice per FP certification req
        InvoiceHeader invoiceHeader = new InvoiceHeader();
        invoiceHeader.setInvoiceNumber(createMerchantReferenceCode());

        // Build Request
        RequestMessage requestMessage = new RequestMessage();
        requestMessage.setStoreId(getStoreID());
        requestMessage.setTerminalId(getTerminalID());
        requestMessage.setOrderRequestID(requestID);
        requestMessage.setPurchaseTotals(purchaseTotals);

        // ADD CCCreditService
        requestMessage.setCcCreditService(ccCreditService);
        // Add MerchantReferenceCode
        requestMessage.setMerchantReferenceCode(invoiceHeader.getInvoiceNumber());
        // Add Metadata
        requestMessage.setClientMetadata(clientMetadata);
        // Add invoice object
        requestMessage.setInvoiceHeader(invoiceHeader);

        // Create service object
        FreewayService freewayService = new FreewayService();

        // CALL SERVICE
        ExecutorService executor = Executors.newCachedThreadPool();
        FreedomPayFutureCall task = new FreedomPayFutureCall();
        task.setFreewayService(freewayService);
        task.setRequestMessage(requestMessage);

        // Make future Call with timeout
        Logger.logMessage("FreedomPay Calling Refund Service...", logFile, Logger.LEVEL.TRACE);
        ReplyMessage reply;
        Future<Object> future = executor.submit(task);
        try {
            Object result = future.get(TIMEOUT_SECONDS, TimeUnit.SECONDS);
            reply = (ReplyMessage)result;

            // Inspect Reply
            if(reply.getDecision() !=null) {Logger.logMessage("FreedomPay Reply: Decision:"+reply.getDecision(), logFile, Logger.LEVEL.TRACE);}
            if(reply.getReasonCode() != null){Logger.logMessage("FreedomPay Reply: ReasonCode:"+reply.getReasonCode(), logFile,  Logger.LEVEL.TRACE);}
            if(reply.getRequestID() != null){Logger.logMessage("FreedomPay Reply: RequestID:"+reply.getRequestID(), logFile,  Logger.LEVEL.TRACE);}
            this.setResponseNumber(reply.getReasonCode());
            this.setResponseID(reply.getRequestID());

            if(reply.getDecision().equals(DECISION_ACCEPT)){
                this.setRequestID(reply.getRequestID());
                this.setStatus(reply.getCcCreditReply().getProcessorResponseMessage());
                Logger.logMessage("ENDING method FreedomPay.refund()", logFile,  Logger.LEVEL.TRACE);
                return reply;
            } else {
                Logger.logMessage("Error processing FreedomPay Refund...", logFile,  Logger.LEVEL.ERROR);
                String decision = reply.getDecision() != null ? reply.getDecision(): "" ;
                String reasonCode = reply.getReasonCode() != null ? reply.getReasonCode().toString() : "";
                throw new FundingCardException("Unable to process refund: "+decision+" ("+reasonCode+")");
            }
        } catch (TimeoutException ex) {
            // handle the timeout
            Logger.logMessage("Attempting Timeout Reversal...", logFile,  Logger.LEVEL.ERROR);
            TORService torService = new TORService();
            torService.setRun("true");
            requestMessage.setTorService(torService);
            ReplyMessage replyTimeout = freewayService.getFreewayServiceSoap().submit(requestMessage);
            Logger.logMessage("Timeout Reversal: Decision:"+replyTimeout.getDecision()+" ReasonCode: "+replyTimeout.getReasonCode(), logFile,  Logger.LEVEL.ERROR);
            if(replyTimeout.getRequestID() != null){
                Logger.logMessage("Timeout Reversal: RequestID:"+replyTimeout.getRequestID(), logFile, Logger.LEVEL.ERROR);
            }
            throw new FundingCardException("Unable to process charge: "+replyTimeout.getDecision()+" ("+replyTimeout.getReasonCode()+")");
        } catch (InterruptedException e) {
            // handle the interrupts
            Logger.logMessage("FreedomPay encountered an Interrupted Exception.", logFile);
            Logger.logException(e, logFile);
            throw new FundingCardException("Unable to process charge due to interruption exception.");
        } catch (ExecutionException e) {
            // handle other exceptions
            Logger.logMessage("FreedomPay encountered an Execution Exception.", logFile);
            Logger.logException(e, logFile);
            throw new FundingCardException("Unable to process charge due to execution exception.");
        }
    }

    public Boolean voidFPTransaction(String requestID) throws Exception {

        Logger.logMessage("STARTING method FreedomPay.fpVoid()", Logger.LEVEL.DEBUG);

        VoidService voidService = new VoidService();
        voidService.setRun("true");

        // Create Client data
        ClientMetadata clientMetadata = new ClientMetadata();
        clientMetadata.setApplicationName(commonMMHFunctions.getInstanceName());
        clientMetadata.setApplicationVersion(getVersion());

        // Add invoice per FP certification req
        InvoiceHeader invoiceHeader = new InvoiceHeader();
        invoiceHeader.setInvoiceNumber(createMerchantReferenceCode());

        // Build Request
        RequestMessage requestMessage = new RequestMessage();
        requestMessage.setStoreId(getStoreID());
        requestMessage.setTerminalId(getTerminalID());
        requestMessage.setOrderRequestID(requestID);

        // ADD VoidService
        requestMessage.setVoidService(voidService);
        // Add MerchantReferenceCode
        requestMessage.setMerchantReferenceCode(invoiceHeader.getInvoiceNumber());
        // Add Metadata
        requestMessage.setClientMetadata(clientMetadata);
        // Add invoice object
        requestMessage.setInvoiceHeader(invoiceHeader);

        // Create service object
        FreewayService freewayService = new FreewayService();

        // CALL SERVICE
        ExecutorService executor = Executors.newCachedThreadPool();
        FreedomPayFutureCall task = new FreedomPayFutureCall();
        task.setFreewayService(freewayService);
        task.setRequestMessage(requestMessage);

        // Make future Call with timeout
        Logger.logMessage("FreedomPay Calling Void Service...", Logger.LEVEL.DEBUG);
        ReplyMessage reply;
        Future<Object> future = executor.submit(task);
        try {
            Object result = future.get(TIMEOUT_SECONDS, TimeUnit.SECONDS);
            reply = (ReplyMessage)result;

            // Inspect Reply
            if(reply.getDecision() !=null) {Logger.logMessage("FreedomPay Reply: Decision:"+reply.getDecision(), Logger.LEVEL.TRACE);}
            if(reply.getReasonCode() != null){Logger.logMessage("FreedomPay Reply: ReasonCode:"+reply.getReasonCode(), Logger.LEVEL.TRACE);}
            if(reply.getRequestID() != null){Logger.logMessage("FreedomPay Reply: RequestID:"+reply.getRequestID(), Logger.LEVEL.TRACE);}
            this.setResponseNumber(reply.getReasonCode());
            this.setResponseID(reply.getRequestID());

            if(reply.getDecision().equals(DECISION_ACCEPT)){
                this.setRequestID(reply.getRequestID());
                this.setStatus(reply.getVoidReply().getProcessorResponseMessage());
                Logger.logMessage("Transaction Void Amount: "+reply.getVoidReply().getAmount(), Logger.LEVEL.TRACE);
                Logger.logMessage("ENDING method FreedomPay.void()", Logger.LEVEL.DEBUG);
                return true;
            } else {
                Logger.logMessage("Error processing FreedomPay Void...", Logger.LEVEL.ERROR);
                String decision = reply.getDecision() != null ? reply.getDecision(): "" ;
                String reasonCode = reply.getReasonCode() != null ? reply.getReasonCode().toString() : "";
                throw new FundingCardException("Unable to process void: "+decision+" ("+reasonCode+")");
            }
        } catch (TimeoutException ex) {
            // handle the timeout
            Logger.logMessage("Attempting Timeout Reversal...", Logger.LEVEL.ERROR);
            TORService torService = new TORService();
            torService.setRun("true");
            requestMessage.setTorService(torService);
            ReplyMessage replyTimeout = freewayService.getFreewayServiceSoap().submit(requestMessage);
            Logger.logMessage("Timeout Reversal: Decision:"+replyTimeout.getDecision()+" ReasonCode: "+replyTimeout.getReasonCode(), Logger.LEVEL.ERROR);
            if(replyTimeout.getRequestID() != null){
                Logger.logMessage("Timeout Reversal: RequestID:"+replyTimeout.getRequestID(), Logger.LEVEL.ERROR);
            }
            throw new FundingCardException("Unable to process void: "+replyTimeout.getDecision()+" ("+replyTimeout.getReasonCode()+")");
        } catch (InterruptedException e) {
            // handle the interrupts
            Logger.logMessage("FreedomPay encountered an Interrupted Exception.");
            Logger.logException(e);
            throw new FundingCardException("Unable to process void due to interruption exception.");
        } catch (ExecutionException e) {
            // handle other exceptions
            Logger.logMessage("FreedomPay encountered an Execution Exception.");
            Logger.logException(e);
            throw new FundingCardException("Unable to process void due to execution exception.");
        }
    }

    private String getStoreID() {
        return storeID;
    }

    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    private String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getPaymentToken() {
        return paymentToken;
    }

    public void setPaymentToken(String paymentToken) {
        this.paymentToken = paymentToken;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String currentVersion){
        version = currentVersion;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getResponseID() {
        return responseID;
    }

    public void setResponseID(String responseID) {
        this.responseID = responseID;
    }

    public BigInteger getResponseNumber() {
        return responseNumber;
    }

    public void setResponseNumber(BigInteger responseNumber) {
        this.responseNumber = responseNumber;
    }

    private String createMerchantReferenceCode(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        return simpleDateFormat.format(date);
    }
}
