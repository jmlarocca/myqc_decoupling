package com.mmhayes.common.processors;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiModel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by nyu on 8/1/2017.
 */
@ApiModel(value = "MerchantLinkRequestModel", description = "MerchantLinkRequestModel is the basis for all MerchantLink admin requests")
public class MerchantLinkAdminPPRequestModel extends MerchantLinkRequestModel
{
    // FOR CONNECTIONTEST
    private String local = "";

    // FOR RESETPINPAD
    private boolean generateEMVConfigReport = false;

    // FOR FIRMWAREDOWNLOAD
    private String serverIP = "";
    private String serverPort = "";
    private String firmwareVersion = "";
    private boolean downloadMode = false;

    // FOR CHIP TXN REPORT
    private boolean generateChipTxnReport = false;

    // FOR ITEMDETAIL SAF TXNS
    private String recNum = "";

    public MerchantLinkAdminPPRequestModel()
    {
    }

    //region getters and setters
    public boolean getGenerateEMVConfigReport()
    {
        return generateEMVConfigReport;
    }

    public void setGenerateEMVConfigReport(boolean generateEMVConfigReport)
    {
        this.generateEMVConfigReport = generateEMVConfigReport;
    }

    public String getServerIP()
    {
        return serverIP;
    }

    public void setServerIP(String serverIP)
    {
        this.serverIP = serverIP;
    }

    public String getServerPort()
    {
        return serverPort;
    }

    public void setServerPort(String serverPort)
    {
        this.serverPort = serverPort;
    }

    public String getFirmwareVersion()
    {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion)
    {
        this.firmwareVersion = firmwareVersion;
    }

    public boolean getDownloadMode()
    {
        return downloadMode;
    }

    public void setDownloadMode(boolean downloadMode)
    {
        this.downloadMode = downloadMode;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public boolean getGenerateChipTxnReport()
    {
        return generateChipTxnReport;
    }

    public void setGenerateChipTxnReport(boolean generateChipTxnReport)
    {
        this.generateChipTxnReport = generateChipTxnReport;
    }

    public String getRecNum() {
        return recNum;
    }

    public void setRecNum(String recNum) {
        this.recNum = recNum;
    }

    //endregion
}
