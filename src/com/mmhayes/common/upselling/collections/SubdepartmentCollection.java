package com.mmhayes.common.upselling.collections;

//MMHayes Dependencies

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.upselling.models.KeypadDisplayModel;
import com.mmhayes.common.upselling.models.SubdepartmentModel;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//API Dependencies
//Other Dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-07-09 12:39:31 -0400 (Tue, 09 Jul 2019) $: Date of last commit
 $Rev: 9056 $: Revision of last commit
*/
public class SubdepartmentCollection {
    List<SubdepartmentModel> collection = new ArrayList<>();
    DataManager dm = new DataManager();

    public SubdepartmentCollection(TerminalModel terminalModel) throws Exception {

        populateCollection(terminalModel);
    }

    //populates this collection with models
    private SubdepartmentCollection populateCollection(TerminalModel terminalModel) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> subdepartmentList = dm.parameterizedExecuteQuery("data.posapi30.getAllActiveSubdepartmentsByRevCenter",
                new Object[]{
                        terminalModel.getRevenueCenterId()
                },
                true
        );

        //subdepartment list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(subdepartmentList, "Sub Department List is empty", terminalModel.getId());
        for (HashMap subdepartmentHM : subdepartmentList) {
            CommonAPI.checkIsNullOrEmptyObject(subdepartmentHM.get("ID"), "Sub Department Id could not be found", terminalModel.getId());
            //create a new model and add to the collection
            getCollection().add(SubdepartmentModel.createSubdepartmentModel(subdepartmentHM, terminalModel));
        }

        return this;
    }

    //region Getters/Setters

    public List<SubdepartmentModel> getCollection() {
        return collection;
    }

    public void setCollection(List<SubdepartmentModel> collection) {
        this.collection = collection;
    }
    //endregion
}
