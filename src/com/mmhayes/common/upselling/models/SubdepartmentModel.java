package com.mmhayes.common.upselling.models;

//MMHayes Dependencies

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;

import java.util.HashMap;

//API Dependencies
//Other Dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-07-09 12:39:31 -0400 (Tue, 09 Jul 2019) $: Date of last commit
 $Rev: 9056 $: Revision of last commit
*/
public class SubdepartmentModel {
    private Integer id = null;
    private String name = null;
    private KeypadDisplayModel similarProductsKeypad = null;
    private KeypadDisplayModel complementaryProductsKeypad = null;

    private static DataManager dm = new DataManager();

    public SubdepartmentModel( HashMap subdepartmentHM, TerminalModel terminalModel ) throws Exception {
        setModelProperties(subdepartmentHM, terminalModel);
    }


    public static SubdepartmentModel createSubdepartmentModel(HashMap subdepartmentHM, TerminalModel terminalModel) throws Exception {
        return new SubdepartmentModel(subdepartmentHM, terminalModel);
    }

    public SubdepartmentModel setModelProperties(HashMap modelDetailHM, TerminalModel terminalModel) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        //set similar keypad display model
        if( CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAUPSELLSIMILARPAKEYPADID")) != null ) {
            Integer similarPAKeypadID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAUPSELLSIMILARPAKEYPADID"));

            KeypadDisplayModel keypadDisplayModel = new KeypadDisplayModel(similarPAKeypadID, terminalModel);
            setSimilarProductsKeypad(keypadDisplayModel);
        }

        //set complementary keypad display model
        if( CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAUPSELLCOMPLEMENTARYPAKEYPADID")) != null ) {
            Integer complementaryPAKeypadID = CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAUPSELLCOMPLEMENTARYPAKEYPADID"));

            KeypadDisplayModel keypadDisplayModel = new KeypadDisplayModel(complementaryPAKeypadID, terminalModel);
            setComplementaryProductsKeypad(keypadDisplayModel);

        }

        return this;
    }

    //region Getters/Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public KeypadDisplayModel getSimilarProductsKeypad() {
        return similarProductsKeypad;
    }

    public void setSimilarProductsKeypad(KeypadDisplayModel similarProductsKeypad) {
        this.similarProductsKeypad = similarProductsKeypad;
    }

    public KeypadDisplayModel getComplementaryProductsKeypad() {
        return complementaryProductsKeypad;
    }

    public void setComplementaryProductsKeypad(KeypadDisplayModel complementaryProductsKeypad) {
        this.complementaryProductsKeypad = complementaryProductsKeypad;
    }

    //endregion
}
