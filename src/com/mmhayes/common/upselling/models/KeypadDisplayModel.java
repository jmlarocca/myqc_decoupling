package com.mmhayes.common.upselling.models;

//MMHayes Dependencies

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;

import java.util.ArrayList;
import java.util.HashMap;

//API Dependencies
//Other Dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-07-09 12:39:31 -0400 (Tue, 09 Jul 2019) $: Date of last commit
 $Rev: 9056 $: Revision of last commit
*/
public class KeypadDisplayModel {
    private Integer id = null;
    private String name = null;

    private static DataManager dm = new DataManager();

    public KeypadDisplayModel() {

    }

    public KeypadDisplayModel(Integer keypadID, TerminalModel terminalModel) throws Exception {
        createKeypadDisplayModel(keypadID, terminalModel);
    }

    public KeypadDisplayModel setModelProperties(HashMap modelDetailHM) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        return this;
    }

    //populates this collection with models
    public KeypadDisplayModel createKeypadDisplayModel(Integer keypadID, TerminalModel terminalModel) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> keypadList = dm.parameterizedExecuteQuery("data.posapi30.getKeypadByKeypadID",
                new Object[]{
                        keypadID,
                        terminalModel.getRevenueCenterId()
                },
                true
        );

        //keypad list is null or empty, throw exception
        if(keypadList != null && !keypadList.isEmpty()) {
            HashMap keypadHM = keypadList.get(0);
            CommonAPI.checkIsNullOrEmptyObject(keypadHM.get("ID"), "Keypad Id could not be found");

            setModelProperties(keypadHM);
        }

        return this;
    }

    //region Getters/Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //endregion
}
