package com.mmhayes.common.datawire;

/**
 * Created by mmhayes on 4/20/2020.
 */
public class DatawireConstants {
    public static enum DatawireTransType {
        ACTIVATION ("0100", "Activation"),
        REDEMPTION ("0202", "Redemption"),
        RELOAD ("0300", "Reload"),
        BALANCE_INQUIRY ("0400", "Balance Inquiry"),
        CASH_OUT ("0600", "Cash Out"),
        REDEMPTION_VOID ("0800", "Redemption/Cash Out Void"),
        RELOAD_VOID ("0801", "Reload Void"),
        ACTIVATION_VOID ("0802", "Activation Void");

        private String txnCode;
        private String name;

        private DatawireTransType(String txnCode, String name) {
            this.txnCode = txnCode;
            this.name = name;
        }

        public String getTxnCode() {
            return txnCode;
        }

        public String getName() {
            return name;
        }

        public static String convertTxnCodeToName(String code) {
            switch (code) {
                case "0100":
                    return ACTIVATION.getName();
                case "0202":
                    return REDEMPTION.getName();
                case "0300":
                    return RELOAD.getName();
                case "0400":
                    return BALANCE_INQUIRY.getName();
                case "0600":
                    return CASH_OUT.getName();
                case "0800":
                    return REDEMPTION_VOID.getName();
                case "0801":
                    return RELOAD_VOID.getName();
                case "0802":
                    return ACTIVATION_VOID.getName();
                default:
                    return "";
            }
        }

        public static String convertTxnCodeToReceiptName(String code) {
            switch (code) {
                case "0100":
                case "0802":
                    return ACTIVATION.getName();
                case "0202":
                case "0800":
                    return REDEMPTION.getName();
                case "0300":
                case "0801":
                    return RELOAD.getName();
                case "0400":
                    return BALANCE_INQUIRY.getName();
                case "0600":
                    //return CASH_OUT.getName();
                    return "";
                default:
                    return "";
            }
        }
    }

    public static enum DatawireDetailPrefix {
        TRANSACTION_TYPE ("TransTypeNum"),
        CARD_NUM ("CardNum"),
        UNIQUE_TRANS_ID ("UniqueTransID"),
        CLERK_ID ("ClerkID"),
        NEW_BALANCE ("NewBalance"),
        PREV_BALANCE ("PrevBalance");

        private String prefixName;

        private DatawireDetailPrefix(String prefixName) {
            this.prefixName = prefixName;
        }

        public String getPrefixName() {
            return prefixName;
        }
    }
}
