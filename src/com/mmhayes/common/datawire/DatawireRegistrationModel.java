package com.mmhayes.common.datawire;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class DatawireRegistrationModel {
    private int terminalID;

    public int getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(int terminalID) {
        this.terminalID = terminalID;
    }
}
