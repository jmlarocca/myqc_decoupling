package com.mmhayes.common.datawire;

import com.mmhayes.common.utils.Logger;


import java.util.logging.Handler;
//import java.util.logging.Level;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class DatawireLoggingHandler  extends Handler {
    /**
     * Publish a <tt>LogRecord</tt>.
     * <p/>
     * The logging request was made initially to a <tt>Logger</tt> object,
     * which initialized the <tt>LogRecord</tt> and forwarded it here.
     * <p/>
     * The <tt>Handler</tt>  is responsible for formatting the message, when and
     * if necessary.  The formatting should include localization.
     *
     * @param record description of the log event. A null record is
     *               silently ignored and is not published
     */
    @Override
    public void publish(LogRecord record) {
        java.util.logging.Level recordLevel = record.getLevel();
        Logger.LEVEL level = Logger.LEVEL.TRACE;
        if(recordLevel == Level.ALL)
            level = Logger.LEVEL.TRACE;
        if(recordLevel == Level.SEVERE)
            level = Logger.LEVEL.IMPORTANT;
        if(recordLevel == java.util.logging.Level.WARNING)
            level = Logger.LEVEL.WARNING;
        Logger.logMessage(record.getMessage(), level);
    }

    /**
     * Flush any buffered output.
     */
    @Override
    public void flush() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Close the <tt>Handler</tt> and free all associated resources.
     * <p/>
     * The close method will perform a <tt>flush</tt> and then close the
     * <tt>Handler</tt>.   After close has been called this <tt>Handler</tt>
     * should no longer be used.  Method calls may either be silently
     * ignored or may throw runtime exceptions.
     *
     * @throws SecurityException if a security manager exists and if
     *                           the caller does not have <tt>LoggingPermission("control")</tt>.
     */
    @Override
    public void close() throws SecurityException {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
