package com.mmhayes.common.onlineordering.Error;

public enum OrderingError {

    /*** STORE MODEL ERRORS ***/
    STORE_NOT_FOUND("Could not determine any stores with the provided data"),
    PRINTER_HOST_OFFLINE("Printer host is unavailable at this time."),
    FUTURE_ORDER_MAXIMUM_DAYS_EXCEEDED("The order has exceeded the number of days allowed for future orders."),
    NO_PICKUP_DELIVERY("Both pickup and delivery are unavailable at this time");

    public String errorMessage;

    OrderingError(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
