package com.mmhayes.common.onlineordering.Ordering;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.TransactionBuilder;
import com.mmhayes.common.login.LoginModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.transaction.models.TransactionQueryParams;
import com.mmhayes.common.utils.Logger;

import javax.servlet.http.HttpServletRequest;

public class OrderingHelper {

    /*** START VOID TRANSACTION ***/

    public static TransactionModel voidTransactionById(String transactionIdString, HttpServletRequest request) throws Exception {
        //create terminal model, validate the incoming request
        LoginModel loginModel = LoginModel.createLoginModel(request, true);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);  //create Terminal Model, grab terminal information out of the loginModel
        Integer transactionId = PosAPIHelper.cleanIdentifier(transactionIdString, CommonAPI.PosModelType.TRANSACTION, terminalModel.getId());

        return voidTransactionById(transactionId, terminalModel);
    }

    public static TransactionModel voidTransactionById(int transactionId, HttpServletRequest request) throws Exception {
        //create terminal model, validate the incoming request
        LoginModel loginModel = LoginModel.createLoginModel(request, true);
        TerminalModel terminalModel = TerminalModel.createTerminalModel(loginModel, true);  //create Terminal Model, grab terminal information out of the loginModel

        return voidTransactionById(transactionId, terminalModel);
    }

    public static TransactionModel voidTransactionById(int transactionId, TerminalModel terminalModel) throws Exception {
        TransactionModel validTransaction = new TransactionModel().getOneTransactionByIdAndSetOriginalTransaction(transactionId, terminalModel, new TransactionQueryParams());
        TransactionBuilder transactionBuilder = TransactionBuilder.createTransactionBuilder(terminalModel, validTransaction, PosAPIHelper.ApiActionType.VOID, null);
        transactionBuilder.buildTransaction();
        transactionBuilder.saveTransaction();

        Logger.logMessage("Successfully voided transaction with id " + transactionId, Logger.LEVEL.DEBUG);

        return transactionBuilder.getTransactionModel();
    }

    /*** END VOID TRANSACTION ***/

}
