package com.mmhayes.common.deploymentpackage.models;

//MMHayes Dependencies
import com.mmhayes.common.utils.Logger;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-02-11 11:09:51 -0500 (Tue, 11 Feb 2020) $: Date of last commit
 $Rev: 10797 $: Revision of last commit
*/
public class DeploymentPackageUpdateModel {

    private boolean approvedForUpdate = false;
    private boolean errorOccurred = false;
    private String deploymentManagerUrl = "";
    private DeploymentPackageModel currentDeploymentPackage = null;
    private DeploymentPackageModel assignedDeploymentPackage = null;

    public DeploymentPackageUpdateModel(){

    }

    public void validateAssignedDeploymentPackage(String deploymentPackageName, Integer terminalId) {

        DeploymentPackageModel currentPackage = new DeploymentPackageModel().getOneDeploymentPackageByName(deploymentPackageName, terminalId);

        if (currentPackage == null || currentPackage.getId() == null || currentPackage.getId().toString().isEmpty()) {
            Logger.logMessage("Current Package was not found, nothing will be done.", PosAPIHelper.getLogFileName(terminalId));
            setApprovedForUpdate(false);
            setErrorOccurred(true);
        } else {
            Integer terminalAssignedPackageId = DeploymentPackageHelper.getAssignedVersionIdByTerminalId(terminalId);

            if (terminalAssignedPackageId == null || terminalAssignedPackageId.toString().isEmpty()) {
                Logger.logMessage("Terminal does not have an AssignedVersionID, nothing will be done.", PosAPIHelper.getLogFileName(terminalId));
                setApprovedForUpdate(false);
            } else if (terminalAssignedPackageId.equals(currentPackage.getId())) {
                setApprovedForUpdate(false);
            } else if (!terminalAssignedPackageId.equals(currentPackage.getId())) {
                //proceed with the update
                String deploymentManagerUrl = DeploymentPackageHelper.getDeploymentManagerUrl(terminalId);

                if (deploymentManagerUrl == null || deploymentManagerUrl.isEmpty()) {
                    Logger.logMessage("Deployment Manager URL is not set properly, nothing will be done.", PosAPIHelper.getLogFileName(terminalId));
                    setApprovedForUpdate(false);
                    setErrorOccurred(true);
                    return;
                }

                DeploymentPackageModel assignedDeploymenPackage = new DeploymentPackageModel().getOneDeploymentPackageById(terminalAssignedPackageId, terminalId);
                this.setDeploymentManagerUrl(deploymentManagerUrl);

                setAssignedDeploymentPackage(assignedDeploymenPackage);
                setApprovedForUpdate(true);
                //update is approved and assigned Deployment Package is set
            }
        }
    }

    public boolean isApprovedForUpdate() {
        return approvedForUpdate;
    }

    public void setApprovedForUpdate(boolean approvedForUpdate) {
        this.approvedForUpdate = approvedForUpdate;
    }

    public boolean isErrorOccurred() {
        return errorOccurred;
    }

    public void setErrorOccurred(boolean errorOccurred) {
        this.errorOccurred = errorOccurred;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getDeploymentManagerUrl() {
        return deploymentManagerUrl;
    }

    public void setDeploymentManagerUrl(String deploymentManagerUrl) {
        this.deploymentManagerUrl = deploymentManagerUrl;
    }

    public DeploymentPackageModel getCurrentDeploymentPackage() {
        return currentDeploymentPackage;
    }

    public void setCurrentDeploymentPackage(DeploymentPackageModel currentDeploymentPackage) {
        this.currentDeploymentPackage = currentDeploymentPackage;
    }

    public DeploymentPackageModel getAssignedDeploymentPackage() {
        return assignedDeploymentPackage;
    }

    public void setAssignedDeploymentPackage(DeploymentPackageModel assignedDeploymentPackage) {
        this.assignedDeploymentPackage = assignedDeploymentPackage;
    }
}
