package com.mmhayes.common.deploymentpackage.models;

//MMHayes Dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;

//API Dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-02-11 11:09:51 -0500 (Tue, 11 Feb 2020) $: Date of last commit
 $Rev: 10797 $: Revision of last commit
*/
public class DeploymentPackageHelper {



    public static String getDeploymentManagerUrl(Integer terminalId){
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> deploymentManagerUrlList = dm.parameterizedExecuteQuery("data.posapi30.getDeploymentManagerURL",
                new Object[]{ },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        if (deploymentManagerUrlList != null && !deploymentManagerUrlList.isEmpty()){
            HashMap modelDetailHM = deploymentManagerUrlList.get(0);
            return CommonAPI.convertModelDetailToString(modelDetailHM.get("DEPLOYMENTMANAGERURL"));
        } else {
            return "";
        }
    }

    public static Integer getAssignedVersionIdByTerminalId(Integer terminalId){
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> assignedVersionIdList = dm.parameterizedExecuteQuery("data.posapi30.getAssignedVersionIdByTerminalId",
                new Object[]{ terminalId },
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        if (assignedVersionIdList != null && !assignedVersionIdList.isEmpty()){
            HashMap modelDetailHM = assignedVersionIdList.get(0);
            return CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ASSIGNEDVERSIONID"));
        } else {
            return null;
        }
    }

}
