package com.mmhayes.common.deploymentpackage.models;

//MMHayes Dependencies

import com.mmhayes.common.dataaccess.DataManager;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-05-15 08:59:52 -0400 (Fri, 15 May 2020) $: Date of last commit
 $Rev: 11750 $: Revision of last commit
*/
public class DeploymentPackageModel {

    private Integer id = null;
    private String packageName = "";
    private String packageVersion = "";
    private String hardwarePlatform = "";
    private String serverName = "";

    private Boolean active = false;
    private Boolean approvedForUpdate = false;
    private Integer PosWarId = null;
    DataManager dm = new DataManager();

    public DeploymentPackageModel() {

    }

    public DeploymentPackageModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADEPLOYMENTPACKAGEID")));
        setPackageName(CommonAPI.convertModelDetailToString(modelDetailHM.get("PACKAGENAME")));
        setPackageVersion(CommonAPI.convertModelDetailToString(modelDetailHM.get("PACKAGEVERSION")));
        setHardwarePlatform(CommonAPI.convertModelDetailToString(modelDetailHM.get("HARDWAREPLATFORM")));
        setActive(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("HARDWAREPLATFORM"), false));
        setPosWarId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("POSWARID")));
        return this;
    }

    public DeploymentPackageModel getOneDeploymentPackageById(Integer deploymentPackageId, Integer terminalId) {

        //get all models in an array list
        ArrayList<HashMap> deploymentPackageList = dm.parameterizedExecuteQuery("data.posapi30.getOneDeploymentPackageById",
                new Object[]{deploymentPackageId},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        if (deploymentPackageList != null && !deploymentPackageList.isEmpty()) {
            return this.setModelProperties(deploymentPackageList.get(0));
        } else {
            return null;
        }
    }

    public DeploymentPackageModel getOneDeploymentPackageByName(String deploymentPackageName, Integer terminalId) {

        //get all models in an array list
        ArrayList<HashMap> deploymentPackageList = dm.parameterizedExecuteQuery("data.posapi30.getOneDeploymentPackageByName",
                new Object[]{deploymentPackageName},
                PosAPIHelper.getLogFileName(terminalId),
                true
        );

        if (deploymentPackageList != null && !deploymentPackageList.isEmpty()) {
            return this.setModelProperties(deploymentPackageList.get(0));
        } else {
            return null;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPackageVersion() {
        return packageVersion;
    }

    public void setPackageVersion(String packageVersion) {
        this.packageVersion = packageVersion;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getHardwarePlatform() {
        return hardwarePlatform;
    }

    public void setHardwarePlatform(String hardwarePlatform) {
        this.hardwarePlatform = hardwarePlatform;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getPosWarId() {
        return PosWarId;
    }

    public void setPosWarId(Integer posWarId) {
        PosWarId = posWarId;
    }

    public Boolean getApprovedForUpdate() {
        return approvedForUpdate;
    }

    public void setApprovedForUpdate(Boolean approvedForUpdate) {
        this.approvedForUpdate = approvedForUpdate;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }
}
