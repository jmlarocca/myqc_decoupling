package com.mmhayes.common.deploymentpackage.models;

//MMHayes dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.utils.Logger;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-02-26 11:20:10 -0500 (Wed, 26 Feb 2020) $: Date of last commit
 $Rev: 10907 $: Revision of last commit
*/
public class DeploymentTerminalInfoModel {

    private Integer terminalErrorState = 0;
    private Integer installedDeploymentPackageId = null;
    private String installedDeploymentPackageName = "";
    private Integer assignedDeploymentPackageId = null;
    private String softwareVersion = "";
    private String macAddress = "";
    private String errorStatus = "";

    private TerminalModel terminal = null;
    private DataManager dm = new DataManager();

    public DeploymentTerminalInfoModel() {

    }

    public void validate() throws Exception {

        if (getTerminal() == null) {
            throw new MissingDataException("Terminal Not Found: Unable to validate terminal");
        }
    }

    public void updateTerminalInfo() {
        if (getTerminal() != null && getTerminal().getId() != null) {

            try {

                updateTerminalErrorState();
                updateTerminalSoftwareVersion();
                updateTerminalInstalledPackageId();

            } catch (Exception ex) {
                this.setErrorStatus("Exception occurred.  Terminal Info was not saved.");
            }

        } else {
            Logger.logMessage("Could not determine terminalID DeploymentTerminalInfoModel.updateTerminalInfo", Logger.LEVEL.ERROR);
        }
    }

    public void updateTerminalErrorState() {
        Integer updateResult = dm.parameterizedExecuteNonQuery("data.posapi30.setDeploymentPackageErrState",
                new Object[]{
                        getTerminalErrorState(),
                        getTerminal().getId()
                },
                PosAPIHelper.getLogFileName(getTerminal().getId())
        );

        //record success in response
        if (updateResult >= 0) {
            //setId(insertResult);

        } else {
            Logger.logMessage("Could not update Terminal info ErrState DeploymentTerminalInfoModel.updateTerminalErrorState.", PosAPIHelper.getLogFileName(getTerminal().getId()), Logger.LEVEL.ERROR);
        }
    }

    public void updateTerminalSoftwareVersion() {
        Integer insertResult = dm.parameterizedExecuteNonQuery("data.posapi30.setTerminalVersion",
                new Object[]{
                        getSoftwareVersion(),
                        getMacAddress()
                },
                PosAPIHelper.getLogFileName(getTerminal().getId())
        );

        //record success in response
        if (insertResult > 0) {
            //setId(insertResult);

        } else {
            Logger.logMessage("Could not update Terminal info ErrState DeploymentTerminalInfoModel.updateTerminalSoftwareVersion.", PosAPIHelper.getLogFileName(getTerminal().getId()), Logger.LEVEL.ERROR);
        }
    }

    // Sets Installed Package version (id) for any (enabled) terminals with a given mac address
    public void updateTerminalInstalledPackageId()
    {
        Integer installedPackageId = null;
        //get all models in an array list
        ArrayList<HashMap> deploymentPackageList = dm.parameterizedExecuteQuery("data.posapi30.getOneDeploymentPackageByName",
                new Object[]{getInstalledDeploymentPackageName()},
                PosAPIHelper.getLogFileName(getTerminal().getId()),
                true
        );

        if (deploymentPackageList != null && !deploymentPackageList.isEmpty()) {
            installedPackageId = CommonAPI.convertModelDetailToInteger(deploymentPackageList.get(0).get("PADEPLOYMENTPACKAGEID"));
            this.setInstalledDeploymentPackageId(installedPackageId);
        }

        Integer updateResult = dm.parameterizedExecuteNonQuery("data.posapi30.setDeploymentPackageIDNum",
                new Object[]{
                        getInstalledDeploymentPackageId(),
                        getMacAddress()
                },
                PosAPIHelper.getLogFileName(getTerminal().getId())
        );

        if (updateResult < 0) {
            Logger.logMessage("Could not update Terminal Installed Package Version info ErrState DeploymentTerminalInfoModel.updateTerminalInstalledPackageId.", PosAPIHelper.getLogFileName(getTerminal().getId()), Logger.LEVEL.ERROR);
        }

        Integer updateResult1 = dm.parameterizedExecuteNonQuery("data.posapi30.removeAssignedVersionMatchingDeployedVersion",
                new Object[]{
                        getMacAddress()
                },
                PosAPIHelper.getLogFileName(getTerminal().getId())
        );

        if (updateResult1 < 0) {
            Logger.logMessage("Could not remove Terminal Assigned Package Version. DeploymentTerminalInfoModel.updateTerminalInstalledPackageId.", PosAPIHelper.getLogFileName(getTerminal().getId()), Logger.LEVEL.ERROR);
        }
    }

    public Integer getTerminalErrorState() {
        return terminalErrorState;
    }

    public void setTerminalErrorState(Integer terminalErrorState) {
        this.terminalErrorState = terminalErrorState;
    }

    public Integer getInstalledDeploymentPackageId() {
        return installedDeploymentPackageId;
    }

    public void setInstalledDeploymentPackageId(Integer installedDeploymentPackageId) {
        this.installedDeploymentPackageId = installedDeploymentPackageId;
    }

    public String getInstalledDeploymentPackageName() {
        return installedDeploymentPackageName;
    }

    public void setInstalledDeploymentPackageName(String installedDeploymentPackageName) {
        this.installedDeploymentPackageName = installedDeploymentPackageName;
    }

    public Integer getAssignedDeploymentPackageId() {
        return assignedDeploymentPackageId;
    }

    public void setAssignedDeploymentPackageId(Integer assignedDeploymentPackageId) {
        this.assignedDeploymentPackageId = assignedDeploymentPackageId;
    }

    public String getSoftwareVersion() {
        return softwareVersion;
    }

    public void setSoftwareVersion(String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public TerminalModel getTerminal() {
        return terminal;
    }

    public void setTerminal(TerminalModel terminal) {
        this.terminal = terminal;
    }

    public String getErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(String errorStatus) {
        this.errorStatus = errorStatus;
    }
}
