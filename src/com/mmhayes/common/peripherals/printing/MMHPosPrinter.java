package com.mmhayes.common.peripherals.printing;

import com.mmhayes.common.printing.api.ESC_POS;
import com.mmhayes.common.serial.SerialConn;
import com.mmhayes.common.serial.SerialListener;
import com.mmhayes.common.utils.Logger;
import jpos.JposConst;
import jpos.JposException;
import jpos.POSPrinter;

import javax.comm.*;
import java.io.IOException;
import java.io.OutputStream;
import java.util.TooManyListenersException;

/**
 * Created by MEBrown on 10/7/2015.
 *     This class allows us to substitute a Serial ESC/Pos printer for jpos.POSPrinter
 */


public class MMHPosPrinter extends POSPrinter {
    boolean bIsSerial = false;
    private OutputStream outputStream;
    private int countCharPrinted = 0;
    private boolean bRightAlign = false;
    private SerialConn serialConn;

    private final static char ESC_CHAR = 0x1B;
    private final static char GS = 0x1D;
    private final static byte[] LINE_FEED = new byte[]{0x0A};

    private final static byte[] CUT_PAPER = new byte[]{GS, 0x56, 0x00};

    private final static byte[] ALIGN_LEFT = new byte[]{ESC_CHAR, 0x61, 0x30};
    private final static byte[] ALIGN_CENTER = new byte[]{ESC_CHAR, 0x61, 0x31};
    private final static byte[] ALIGN_RIGHT = new byte[]{ESC_CHAR, 0x61, 0x32};

    private final static byte[] KICK_DRAWER_1 = new byte[]{ESC_CHAR, 0x70, 0x00, 0x19, (byte)0xFA};
    private final static byte[] KICK_DRAWER_2 = new byte[]{ESC_CHAR, 0x70, 0x01, 0x19, (byte)0xFA};

    private final static byte[] LARGE_FONT = new byte[]{ESC_CHAR, 0x21, 0x38};
    private final static byte[] NORMAL_FONT = new byte[]{ESC_CHAR, 0x21, 0x00};

    // I was not able to emulate transaction printing using page mode so ..
    //private final static byte[] ENTER_PG_MODE = new byte[]{ESC_CHAR, 0x4C};
    //private final static byte[] EXIT_PG_MODE = new byte[]{ESC_CHAR, 0x53};

    // this is the text for the command that jpos uses
    // we are translating this to Esc/Pos
    private final static String JPOS_ALIGN_CENTER = "\u001b|cA";
    private final static String JPOS_ALIGN_RIGHT  = "\u001b|rA";
    private final static String JPOS_LARGE_FNT  = "\u001b|4C";

    // padding for right alignment done in raw() method
    private final static String PAD_STR  = "                                                ";
    private final static int PAD_LEN    = 48;

    private static String logFileName = "QCPOS_Periph.log";

    public MMHPosPrinter(String logFile)
    {
        logFileName = logFile;
    }

    // region Getters and Setters
    @Override
    public String getDeviceServiceDescription() throws JposException
    {
        if(bIsSerial)
        {
            return "ESC/POS";
        }
        return super.getDeviceServiceDescription();    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public int getState()
    {
        if(bIsSerial)
        {
            return JposConst.JPOS_SUCCESS; // this should either return success or idle. I think
        }
        else
        {
            return super.getState();
        }
    }

    @Override
    public void setDeviceEnabled(boolean b) throws JposException
    {
        if(!bIsSerial)
        {
            super.setDeviceEnabled(b);
        }
    }

    @Override
    public void setMapMode(int i) throws JposException
    {
        if(!bIsSerial)
        {
            super.setMapMode(i);
        }
    }

    @Override
    public void setRecLetterQuality(boolean b) throws JposException
    {
        if(!bIsSerial)
        {
            super.setRecLetterQuality(b);
        }
    }
    // endregion

    // region Open and Close Methods
    @Override
    public synchronized void open(String s) throws JposException
    {
        if(s.toUpperCase().contains("COM"))
        {
            bIsSerial = true;
        }

        String methodName = "MMHPosPrinter.open('" + s + "')";
        if(bIsSerial)
        {
            serialConn = SerialConn.buildSerialConnFromConnString(s);
            if (serialConn != null) {
                try {
                    serialConn.open();
                }
                catch (NoSuchPortException | PortInUseException | UnsupportedCommOperationException e) {
                    Logger.logException(e);
                }
                try {
                    serialConn.addListener(new SerialListener(serialConn));
                }
                catch (TooManyListenersException e) {
                    Logger.logException(e);
                }
                try {
                    outputStream = serialConn.getOutputStream();
                }
                catch (IOException e) {
                    Logger.logException(e);
                }

                try {
                    serialConn.write(ESC_POS.ENABLE_AUTO_STATUS_BACK);
                }
                catch (IOException e) {
                    Logger.logException(e);
                }
            }
        }
        else
        {
            super.open(s);
        }
    }

    @Override
    public void claim(int i) throws JposException
    {
        if(!bIsSerial)
        {
            super.claim(i);
        }
    }

    @Override
    public void release() throws JposException
    {
        if(!bIsSerial)
        {
            super.release();
        }
    }

    @Override
    public synchronized void close() throws JposException
    {
        if(bIsSerial)
        {
            if (serialConn != null) {
                serialConn.removeListener();
                serialConn.close();
            }
        }
        else
        {
            super.close();
        }
    }
    // endregion

    // region Cash Drawer Open Methods
    public void openCashDrawerA() throws IOException
    {
        if(bIsSerial)
        {
            outputStream.write(KICK_DRAWER_1);
        }
    }

    public void openCashDrawerB() throws IOException
    {
        if(bIsSerial)
        {
            outputStream.write(KICK_DRAWER_2);
        }
    }
    // endregion

    // region Print Methods
    @Override
    public void directIO(int i, int[] ints, Object o) throws JposException
    {
        if(!bIsSerial)
        {
            super.directIO(i, ints, o);
        }
    }

    @Override
    public void printBitmap(int i, String s, int i1, int i2) throws JposException
    {
        if(!bIsSerial)
        {
            super.printBitmap(i, s, i1, i2);
        }
    }

    @Override
    public void transactionPrint(int i, int i1) throws JposException {
        if(bIsSerial)
        {
/*
            try {
                if(POSPrinterConst.PTR_TP_TRANSACTION == i1)
                    os.write(ENTER_PG_MODE);
                else {
                    os.write(FF);
                    os.write(EXIT_PG_MODE);
                }
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
*/

        }
        else
        {
            super.transactionPrint(i, i1);
        }
    }

    /**
     * raw() handles right alignment and tracking how many characters printed so far
     * @param s
     * @throws jpos.JposException
     * @throws java.io.IOException
     */
    private void raw(String s) throws JposException, IOException
    {
        boolean bTestNewMethod = false;
        int carriageReturnPosition = s.indexOf("\n");
        //if(bRightAlign && countCharPrinted >0){

        if(bRightAlign)
        {
            carriageReturnPosition = Math.max(s.length(), carriageReturnPosition);
            // int padCount = PAD_LEN - (countCharPrinted + carriageReturnPosition) - 6;
            int padCount = PAD_LEN - (countCharPrinted + carriageReturnPosition);
            outputStream.write(PAD_STR.substring(0, padCount).getBytes());
            bRightAlign=false;
        }

        if(s.contains("\n"))
        {
            countCharPrinted=0;
            outputStream.write(s.getBytes());
        }
        else
        {
            countCharPrinted += s.length();
            outputStream.write(s.getBytes());
        }
    }

    @Override
    public void printNormal(int i, String s) throws JposException
    {
        if(bIsSerial)
        {
            // Logger.logMessage("MMHPosPrinter.printNormal - serial", logFileName);
            if(serialConn != null)
            {
                try {
                    if(containsIgnoreCode(s))
                    {
                        Logger.logMessage("Contains Ignore Code = true", logFileName, Logger.LEVEL.TRACE);
                        return;
                    }

                    if(s.startsWith("\n"))
                    {
                        raw("\n");
                        s = s.substring(1);
                    }

                    if(s.startsWith(JPOS_ALIGN_RIGHT))
                    {
                        s = s.substring(JPOS_ALIGN_RIGHT.length());
                        bRightAlign = true;
                        outputStream.write(ALIGN_RIGHT);
                    }
                    else if(s.startsWith(JPOS_ALIGN_CENTER))
                    {
                        s = s.substring(JPOS_ALIGN_CENTER.length());
                        outputStream.write(ALIGN_CENTER);
                    }
                    else
                    {
                        outputStream.write(ALIGN_LEFT);
                    }

                    if(s.contains(JPOS_ALIGN_RIGHT))
                    {
                        int pos = s.indexOf(JPOS_ALIGN_RIGHT);
                        if(-1 != pos)
                        {
                            raw(s.substring(0, pos)); // next chunk will need to be right aligned
                            bRightAlign=true;

                            outputStream.write(ALIGN_RIGHT);
                            pos += JPOS_ALIGN_RIGHT.length();
                            s = s.substring(pos);
                        }
                    }

                    if(s.contains(JPOS_LARGE_FNT))
                    {
                        int pos = s.indexOf(JPOS_LARGE_FNT);
                        if(pos > 0)
                        {
                            String t = s.substring(0, pos);
                            raw(t);
                            s = s.substring(pos + JPOS_LARGE_FNT.length());
                        }
                        s = s.replace(JPOS_LARGE_FNT, "");
                        outputStream.write(LARGE_FONT);
                    }

                    s = s.replace(JPOS_ALIGN_RIGHT, "");
                    s = s.replace(JPOS_ALIGN_CENTER, "");

                    raw(s);
                    outputStream.write(ALIGN_LEFT);
                    outputStream.write(NORMAL_FONT);
                }
                catch (IOException ioe)
                {
                    Logger.logException("MMHPosPrinter.printNormal ioexception", logFileName, ioe);
                }
                catch (Exception e)
                {
                    Logger.logException("MMHPosPrinter.printNormal ioexception", logFileName, e);
                }
            }
            else
            {
                Logger.logMessage("MMHPosPrinter.printNormal - sPort is null!", logFileName, Logger.LEVEL.ERROR);
            }
        }
        else
        {
            super.printNormal(i, s);
        }
    }

    @Override
    public void cutPaper(int i) throws JposException
    {
        if(bIsSerial)
        {
            try {
                outputStream.write(LINE_FEED);
                outputStream.write(LINE_FEED);
                outputStream.write(LINE_FEED);
                outputStream.write(LINE_FEED);
                outputStream.write(CUT_PAPER);
            } catch (IOException e) {
                Logger.logException("MMHPosPrinter.cutPaper", "", e);
            }
        }
        else
        {
            super.cutPaper(i);
        }
    }
    // endregion

    // region Other
    @Override
    public void clearOutput() throws JposException
    {
        if(bIsSerial)
        {

        }
        else
        {
            super.clearOutput();
        }
    }

    private boolean containsIgnoreCode(String s)
    {
        return s.startsWith("\u001b|5lF");
    }

    /**
     * <p>Getter for the serialConn field of the {@link MMHPosPrinter}.</p>
     *
     * @return The serialConn field of the {@link MMHPosPrinter}.
     */
    public SerialConn getSerialConn () {
        return serialConn;
    }

    // We expect the COM params string to be
    // DeviceName,ComPort,BaudRate,DataBits,StopBits,Parity
    // Everything after ComPort is optional and defaults to
    // Name,ComPort,BaudRate=128000,DataBits=8,StopBits=1,Parity=None
    private COMParams extractCOMParameters(String s)
    {
        COMParams p = new COMParams();
        String params[] = s.split(",");
        if(params.length < 2  || !params[1].toUpperCase().startsWith("COM")) // COM port is mandatory
        {
            throw new IllegalArgumentException("COM port was expected to be the 2nd parameter. params='" + s + "'");
        }
        p.COMPort = params[1].toUpperCase();

        if(params.length>2)
        {
            p.baudrate = Integer.parseInt(params[2]);
        }

        if(params.length>3)
        {
            p.dataBits = Integer.parseInt(params[3]);
        }

        if(params.length>4)
        {
            p.stopBits = Integer.parseInt(params[4]);
        }

        if(params.length>5)
        {
            String parity = params[5].toUpperCase();
            if(parity.startsWith("O"))
            {
                p.parity = SerialPort.PARITY_ODD;
            }
            else if (parity.startsWith("E"))
            {
                p.parity = SerialPort.PARITY_EVEN;
            }
            else
            {
                p.parity = SerialPort.PARITY_NONE; // default
            }
        }

        return p;
    }

    class COMParams
    {
        public String COMPort="";
        //int baudrate = 128000;
        int baudrate = 9600;
        int dataBits = SerialPort.DATABITS_8;
        int stopBits = SerialPort.STOPBITS_1;
        int parity = SerialPort.PARITY_NONE;
    }
    // endregion
}
