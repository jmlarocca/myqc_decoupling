package com.mmhayes.common.kmsapi;

/*
    Last Updated (automatically updated by SVN)
    $Author: jmkimber $: Author of last commit
    $Date: 2021-04-26 10:59:25 -0400 (Mon, 26 Apr 2021) $: Date of last commit
    $Rev: 13847 $: Revision of last commit
    Notes: Contains endpoints for KMS functionality.
*/

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.Validation;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.kitchenPrinters.KitchenPrinterCommon;
import com.mmhayes.common.kitchenPrinters.KitchenPrinterTerminal;
import com.mmhayes.common.kitchenPrinters.LocalPrintJob;
import com.mmhayes.common.kitchenPrinters.LocalPrintJobAdapter;
import com.mmhayes.common.kms.PeripheralsDataManager;
import com.mmhayes.common.kms.models.KMSEventLogLine;
import com.mmhayes.common.kms.models.KMSStatusUpdateModel;
import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceipt;
import com.mmhayes.common.kms.receiptGen.customerReceipt.CustomerReceiptData;
import com.mmhayes.common.kms.receiptGen.errorReceipt.EmailedErrorReceipt;
import com.mmhayes.common.receiptGen.Formatter.GiftReceiptFormatter;
import com.mmhayes.common.receiptGen.Formatter.KitchenPrinterReceiptFormatter;
import com.mmhayes.common.receiptGen.HashMapDataFns;
import com.mmhayes.common.receiptGen.ReceiptData.ReceiptData;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TypeData;
import com.mmhayes.common.thirdpartyapi.ThirdPartyAPIFactoryProducer;
import com.mmhayes.common.thirdpartyapi.sms.SmsFactory;
import com.mmhayes.common.thirdpartyapi.sms.models.SmsAPIModel;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.utils.StringFunctions;
import com.mmhayes.common.xmlapi.XmlRpcUtil;
import redstone.xmlrpc.XmlRpcStruct;
import redstone.xmlrpc.handlers.ReflectiveInvocationHandler;

import java.io.IOException;
import java.util.*;

/**
 * <p>Contains endpoints for KMS functionality.</p>
 *
 */
public class KMSDataManager extends ReflectiveInvocationHandler {

    // private member variables of a KMSDataManager
    private static final String KMS_DATA_MANAGER_LOG = "KMSDataManager.log";
    private DataManager dm;
    private KitchenPrinterTerminal kitchenPrinterTerminal;

    /**
     * <p>Constructor for a {@link KMSDataManager}.</p>
     *
     */
    public KMSDataManager () {
        this.dm = new DataManager();
    }

    /**
     * <p>Gets called by a terminal in online mode to get transaction information from the server and use it to
     * create print jobs that will be added to the local print queue.</p>
     *
     * @param paTransactionID The ID of the transaction to get information for.
     * @return Whether or not print jobs could be create in the local database for the given transaction.
     */
    public boolean printOrderUsingKMSInOnlineMode (int paTransactionID) {
        Logger.logMessage("KMSDataManager.printOrderUsingKMSInOnlineMode called on transaction " + paTransactionID,KMS_DATA_MANAGER_LOG, Logger.LEVEL.DEBUG);
        boolean success = false;

        try {
            String url = MMHProperties.getAppSetting("site.application.serverXmlRpc.path");
            String macAddress = PeripheralsDataManager.getTerminalMacAddress();
            String hostname = PeripheralsDataManager.getPHHostname();
            Object xmlRpcResponse = XmlRpcUtil.getInstance().xmlRpcPost(url, macAddress, hostname, "KMSDataManager.getTransactionData",new Object[]{(paTransactionID + "")});
            if(xmlRpcResponse != null){
                try{
                    ArrayList result;
                    if(xmlRpcResponse instanceof ArrayList){
                        result =  (ArrayList) xmlRpcResponse;
                    }else{
                        result = XmlRpcUtil.getInstance().parseArrayListXmlRpcResponse(xmlRpcResponse, "KMSDataManager.getTransactionData");
                    }
                    //Logger.logMessage(result.toString());

                    ReceiptGen receiptGen = new ReceiptGen();
                    receiptGen.setReceiptFormatter(new KitchenPrinterReceiptFormatter(dm));
                    IRenderer htmlRenderer = receiptGen.createBasicHTMLRenderer();
                    receiptGen.setReceiptData(receiptGen.createReceiptData());
                    if (htmlRenderer != null) {
                        if (receiptGen.buildFromServerData(paTransactionID, TypeData.ReceiptType.KITCHENPTRRECEIPT, htmlRenderer, result)) {
                            ArrayList<HashMap> details =  receiptGen.getReceiptData().getReceiptDetails();
                            if(details.size() > 0){
                                int printerQueueID = -1;
                                Logger.logMessage("Saving PrintQueue Header..", KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);
                                Logger.logMessage("KMSDataManager.printOrderUsingKMSInOnlineMode Estimated order time: " + receiptGen.getReceiptData().getTransactionData().getTransactionHeader().getEstimatedOrderTime(), KMS_DATA_MANAGER_LOG, Logger.LEVEL.DEBUG);
                                printerQueueID = dm.parameterizedExecuteNonQuery(receiptGen.getReceiptData().getTransactionData().getTransactionHeader().getSQLInsertString(),
                                        receiptGen.getReceiptData().getTransactionData().getTransactionHeader().toQCPrintQueueRecord(true), KMS_DATA_MANAGER_LOG);
                                if(printerQueueID > 0) {
                                    Logger.logMessage("Saved PrintQueue Header!", KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);
                                    success = true;
                                }
                                // Add all of the details to the QC_PAPrinterQueueDetail table
                                for (HashMap detail :receiptGen.getReceiptData().getReceiptDetails())
                                {
                                    // Insert detail
                                    int resultCount = dm.parameterizedExecuteNonQuery("data.newKitchenPrinter.InsertKMSDetailIntoPAPrinterQueueDetail", toQCPrintQueueDetailRecord(printerQueueID, detail), KMS_DATA_MANAGER_LOG);
                                    if(resultCount <= 0)
                                        success = false;
                                }
                            }

                        }
                        else {
                            Logger.logMessage(String.format("Unable to generate a receipt for the transaction with an ID of %s in KMSDataManager.printOrderUsingKMSInOnlineMode",
                                    Objects.toString(paTransactionID, "N/A")), KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
                        }
                    }
                    else {
                        Logger.logMessage("Encountered an invalid HTML renderer, the HTML renderer can't be null in KMSDataManager.printOrderUsingKMSInOnlineMode", KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
                    }

                }catch(NumberFormatException e){
                    Logger.logMessage("Failed to parse KMSChangePollFrequencyMinutes in KMSChangeUpdater.init. Defaulting to 15");
                }
            }else{
                Logger.logMessage("Failed to get an XMLRPC response from KMSDataManager.getTransactionData");
            }
        }
        catch (Exception e) {
            Logger.logException(e, KMS_DATA_MANAGER_LOG);
            Logger.logMessage(String.format("There was a problem trying to add print jobs to the local print queue for the " +
                    "transaction with an ID of %s in KMSDataManager.printOrderUsingKMSInOnlineMode",
                    Objects.toString(paTransactionID, "N/A")), KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * <p>Gets called by a terminal in offline mode to get transaction information from the local database and use it to
     * create print jobs that will be added to the local print queue.</p>
     *
     * @param paTransactionID The ID of the transaction to get information for.
     * @return Whether or not print jobs could be created in the local database for the given transaction.
     */
    public boolean printOrderUsingKMSInOfflineMode (int paTransactionID) {
        Logger.logMessage("KMSDataManager.printOrderUsingKMSInOfflineMode called on transaction " + paTransactionID,KMS_DATA_MANAGER_LOG, Logger.LEVEL.DEBUG);
        boolean success = false;

        try {
            ReceiptGen receiptGen = new ReceiptGen();
            receiptGen.setReceiptFormatter(new KitchenPrinterReceiptFormatter(dm));
            IRenderer htmlRenderer = receiptGen.createBasicHTMLRenderer();
            receiptGen.setReceiptData(receiptGen.createReceiptData());
            if (htmlRenderer != null) {
                if (receiptGen.buildFromDBOrderNum(paTransactionID, TypeData.ReceiptType.KITCHENPTRRECEIPT, htmlRenderer)) {
                    ArrayList<HashMap> details =  receiptGen.getReceiptData().getReceiptDetails();
                    if(details.size() > 0){
                        int printerQueueID = -1;
                        printerQueueID = dm.parameterizedExecuteNonQuery(receiptGen.getReceiptData().getTransactionData().getTransactionHeader().getSQLInsertString(),
                                receiptGen.getReceiptData().getTransactionData().getTransactionHeader().toQCPrintQueueRecord(false), KMS_DATA_MANAGER_LOG);
                        // Add all of the details to the QC_PAPrinterQueueDetail table
                        for (HashMap detail :receiptGen.getReceiptData().getReceiptDetails())
                        {
                            // Insert detail
                            dm.parameterizedExecuteNonQuery("data.newKitchenPrinter.InsertKMSDetailIntoPAPrinterQueueDetail", toQCPrintQueueDetailRecord(printerQueueID, detail), KMS_DATA_MANAGER_LOG);
                        }
                    }
                    success = true;
                }
                else {
                    Logger.logMessage(String.format("Unable to generate a receipt for the transaction with an ID of %s in KMSDataManager.printOrderUsingKMSInOfflineMode",
                            Objects.toString(paTransactionID, "N/A")), KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
                }
            }
            else {
                Logger.logMessage("Encountered an invalid HTML renderer, the HTML renderer can't be null in KMSDataManager.printOrderUsingKMSInOfflineMode", KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
            }
        }
        catch (Exception e) {
            Logger.logException(e, KMS_DATA_MANAGER_LOG);
            Logger.logMessage(String.format("There was a problem trying to add print jobs to the local print queue for the " +
                    "transaction with an ID of %s in KMSDataManager.printOrderUsingKMSInOfflineMode",
                    Objects.toString(paTransactionID, "N/A")), KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
        }

        return success;
    }

    /**
     * converts the receipt detail HM into QC_PAPrinterQueueDetail records to insert into the DB
     * TODO: there is probably a better place to put this than in the DataManager
     * @param parentPAPrintQueueID
     * @param detail
     * @return
     */
    @SuppressWarnings("unchecked")
    private Object[] toQCPrintQueueDetailRecord (long parentPAPrintQueueID, HashMap detail) {

        Logger.logMessage(String.format("Now converting the HashMap: %s, to a print queue detail in KMSDataManager.toQCPrintQueueDetailRecord!",
                Objects.toString(Collections.singletonList(detail), "N/A")), KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);

        String printerID = detail.getOrDefault("PRINTERID", "").toString();
        String printStatusID =  detail.getOrDefault("PRINTSTATUSID", "").toString();
        String printControllerID =  detail.getOrDefault("PRINTCONTROLLERID", "").toString();
        String paPluID =  detail.getOrDefault("PRODUCTID", "").toString();
        String quantity = detail.getOrDefault("QUANTITY", "").toString();
        String isModifier = detail.getOrDefault("ISMODIFIER", false).toString();
        String lineDetail = detail.getOrDefault("LINEDETAIL", "").toString();
        String hideStation = detail.getOrDefault("HIDDENSTATION", "").toString();
        Integer kmsStationID = null;
        Long transLineID = null;
        String linkedFrom = null;

        //remove any -1s where the data should just be null instead
        if(printerID.compareTo("-1") == 0) printerID = null;
        if(printStatusID.compareTo("-1") == 0) printStatusID = null;
        if(printControllerID.compareTo("-1") == 0) printControllerID = null;
        if(lineDetail.compareTo("-1") == 0) lineDetail = null;
        if(hideStation.compareTo("-1") == 0) hideStation = null;


        if(detail.containsKey("KMSSTATIONID") && detail.get("KMSSTATIONID") != null && detail.get("KMSSTATIONID").toString().length() > 0 && detail.get("KMSSTATIONID").toString().compareTo("-1") != 0){
            kmsStationID = Integer.parseInt(detail.get("KMSSTATIONID").toString());
        }
        if(detail.containsKey("TRANSLINEITEMID") && detail.get("TRANSLINEITEMID") != null && detail.get("TRANSLINEITEMID").toString().length() > 0 && detail.get("TRANSLINEITEMID").toString().compareTo("-1") != 0){
            transLineID = Long.parseLong(detail.get("TRANSLINEITEMID").toString());
        }
        if(detail.containsKey("LINKEDFROMIDS") && detail.get("LINKEDFROMIDS") != null && detail.get("LINKEDFROMIDS").toString().length() > 0 && detail.get("LINKEDFROMIDS").toString().compareTo("-1") != 0){
            linkedFrom = detail.getOrDefault("LINKEDFROMIDS", "").toString();
        }

        return new Object[]{ parentPAPrintQueueID, printerID, printStatusID,
                printControllerID, paPluID, quantity, isModifier, lineDetail, hideStation, kmsStationID, transLineID, linkedFrom};
    }

    public String validateStationDSKey(String dskey){
        Validation requestValidation = new Validation();
        requestValidation.setDSKey(dskey);
        String DSKeyStatus = requestValidation.checkDSKeyStatus();
        if (DSKeyStatus.equals("valid")) {
            Logger.logMessage("Found DSKey to be valid for " + requestValidation.getInstanceStationID(), KMS_DATA_MANAGER_LOG, Logger.LEVEL.DEBUG);
            return requestValidation.getInstanceStationID() + "";
        }else{
            Logger.logMessage("Found DSKey to be invalid", KMS_DATA_MANAGER_LOG, Logger.LEVEL.DEBUG);
        }
        return null;
    }

    //    private static ConcurrentLinkedQueue<KMSStatusUpdateModel> failedStatusUpdates= new ConcurrentLinkedQueue();
    private static List<KMSStatusUpdateModel> failedStatusUpdates = Collections.synchronizedList(new ArrayList<KMSStatusUpdateModel>());
    /**
     * Events should always log, we wont run into a case where we need to log, but cant because a transaction is not up to date yet.
     */
    public boolean logEvent(XmlRpcStruct logStruct){
        Logger.logMessage("RPC call to KMSManager.logEvent", Logger.LEVEL.IMPORTANT);
        try{
            KMSEventLogLine logLine = new KMSEventLogLine(logStruct);

            //check if the transactionID has a T in it, if it does, we need to look up if there is an online version of it
            //attempt to get the online ID
            if(logLine.getPATransactionID() != null && logLine.getPATransactionID().contains("T")){
                String offlineTransID = logLine.getPATransactionID().split("T")[0];
                Object onlineID = dm.parameterizedExecuteScalar("data.KMSManager.getOnlineTransID", new Object[]{offlineTransID});
                if(onlineID == null || onlineID.toString().length() == 0){
                    return false;
                }else{
                    logLine.setPATransactionID(onlineID.toString());
                }
            }

            //TODO (HIGH): what about a transLineID, there is no offlineTransID on the transLines...
            if(logLine.getPATransLineItemID() != null && logLine.getPATransLineItemID().contains("T")){
                String offlineTransLineID = logLine.getPATransLineItemID().split("T")[0];
                //TODO: there is no field for offline ID.... so just use the offline as the online for now?
                Object onlineID = offlineTransLineID;//dm.parameterizedExecuteScalar("data.KMSManager.getOnlineTransID", new Object[]{offlineTransID});
                if(onlineID == null || onlineID.toString().length() == 0){
                    return false;
                }else{
                    logLine.setPATransLineItemID(onlineID.toString());
                }
            }
            int inserted = dm.parameterizedExecuteNonQuery("data.KMSManager.logEvent", new Object[]{
                    logLine.getEventDTM(),
                    logLine.getKMSStationID(),
                    logLine.getKMSEventTypeID(),
                    logLine.getPATransactionID(),
                    logLine.getPATransLineItemID(),
                    logLine.getNOTE(),
                    logLine.getKMSOrderStatusID()});
            if(inserted > 0)
                return true;
        }catch (Exception e){
            Logger.logException(e);
        }
        return false;
    }

//    /**
//     * Attempt to update an order status
//     * @param PATransactionID
//     * @param KMSOrderStatusID
//     * @return
//     */
//    public boolean updateOrderStatus(Long PATransactionID, Integer KMSOrderStatusID){
//        int updated = dm.parameterizedExecuteNonQuery("data.KMSManager.updateTransactionStatus", new Object[]{ KMSOrderStatusID, PATransactionID});
//        if(updated > 0){
//            //the save went through
//            failedStatusUpdates.remove(PATransactionID + "_" + KMSOrderStatusID);
//        }else{
//            //the save failed
//            if(!failedStatusUpdates.contains(PATransactionID + "_" + KMSOrderStatusID))
//                failedStatusUpdates.add(PATransactionID + "_" + KMSOrderStatusID);
//        }
//        return updated > 0;
//    }
//    public boolean updateProductOrderStatus(Long PATransactionID, Long PATransLineItemID, Integer KMSOrderStatusID){
//        int updated = dm.parameterizedExecuteNonQuery("data.KMSManager.updateLineItemStatus", new Object[]{ KMSOrderStatusID, PATransLineItemID});
//        if(updated > 0){
//            //the save went through
//            failedStatusUpdates.remove(PATransactionID + "_" + KMSOrderStatusID + "_" + PATransLineItemID);
//        }else{
//            //the save failed
//            if(!failedStatusUpdates.contains(PATransactionID + "_" + KMSOrderStatusID + "_" + PATransLineItemID))
//                failedStatusUpdates.add(PATransactionID + "_" + KMSOrderStatusID + "_" + PATransLineItemID);
//        }
//        return updated > 0;
//    }


    /**
     * To be called when the host gets a new order update.
     * @param updateStruct
     * @return
     */
    public boolean notifyOfOrderUpdate(XmlRpcStruct updateStruct){
        Logger.logMessage("RPC call to KMSManager.notifyOfOrderUpdate", Logger.LEVEL.DEBUG);
        KMSStatusUpdateModel statusUpdate = new KMSStatusUpdateModel(updateStruct);
        if(!failedStatusUpdates.contains(statusUpdate)){//Note: this must ignore the previouslyFailed field
            statusUpdate.currentUpdateLocation = KMSStatusUpdateModel.SENT_TO_SERVER;
            failedStatusUpdates.add(statusUpdate);
            Logger.logMessage("Added failedStatusUpdate in KMSManager.notifyOfOrderUpdate", Logger.LEVEL.DEBUG);
        }
        return true;//either we already have the update, or we didnt. Either way, we have received this order update now.
    }

    /**
     * When attempting to update a order status, we may run into the case where the transaction is not on the server yet.
     * This means we have to hold onto failed status updates until we can successfully update their status. This involves 2 queues for redundancy
     * @param updateStruct
     * @return
     */
    public boolean updateOrderStatus(XmlRpcStruct updateStruct){
        Logger.logMessage("RPC call to KMSManager.updateOrderStatus", Logger.LEVEL.DEBUG);
        KMSStatusUpdateModel statusUpdate = new KMSStatusUpdateModel(updateStruct);
        //Note: in the case where incoming has previously failed, but its not in the list, that item must be the most up to date status from the perspective of the server.
        //it will attempt to update like normal and add it to list if it fails again.
        //This is why after a new printer host grabs the list, they MUST be added to the host's queue in chronological order.

        //handles the case where a future status update has been successfully saved(meaning the previous ones were set to passed, and should NOT be updated, just removed)
        //and we have an incoming update that the host has attempted to save before, but failed to do so.
        if((statusUpdate.currentUpdateLocation < 2) && failedStatusUpdates.contains(statusUpdate) && (failedStatusUpdates.get(failedStatusUpdates.indexOf(statusUpdate)).currentUpdateLocation == KMSStatusUpdateModel.SAVED_IN_SERVER_DB)){
            //this status updated was queued, but was set to passed on the server by a later update.
            failedStatusUpdates.remove(failedStatusUpdates.indexOf(statusUpdate));
            return true;
        }
        int updated = 0;
        if(statusUpdate.status.PATransLineItemID != null){
            if(statusUpdate.status.PATransLineItemID.contains("T")){
                //Format: {OfflineTransLineID}T{TerminalID}
                updated = 1; //TODO: Currently there is no way to map from an offline transline Item ID to the online trans line ID, therefor if the lineID contains T, we need to ignore it
            }else{
                updated = dm.parameterizedExecuteNonQuery("data.KMSManager.updateLineItemStatus", new Object[]{ statusUpdate.status.KMSOrderStatusID, statusUpdate.status.PATransactionID, statusUpdate.status.PATransLineItemID});
            }
        }else{
            if(statusUpdate.status.PATransactionID != null && statusUpdate.status.PATransactionID.contains("T")){
                String offlineID = statusUpdate.status.PATransactionID.split("T")[0];
                String terminalID = statusUpdate.status.PATransactionID.split("T")[1];
                updated = dm.parameterizedExecuteNonQuery("data.KMSManager.updateOfflineTransactionStatus", new Object[]{ statusUpdate.status.KMSOrderStatusID, offlineID, terminalID});
            }else{
                updated = dm.parameterizedExecuteNonQuery("data.KMSManager.updateTransactionStatus", new Object[]{ statusUpdate.status.KMSOrderStatusID, statusUpdate.status.PATransactionID});
            }
        }
        if(updated > 0){
            //the save went through
            //go through the list and set all previous order updates for this item to passed.
            synchronized (failedStatusUpdates){
                for(int i = failedStatusUpdates.size()-1; i >= 0; i--){
                    //TODO: we can go back to removing the old ones, because of the assumption that we are splitting the sending and updating, and that the host is running the updates chronologically
                    if(failedStatusUpdates.get(i).updateIsForSameItem(statusUpdate.status) && failedStatusUpdates.get(i).updateTime < statusUpdate.updateTime){
                        failedStatusUpdates.get(i).currentUpdateLocation = KMSStatusUpdateModel.SAVED_IN_SERVER_DB;
                        Logger.logMessage("Saved StatusUpdate in KMSManager.updateOrderStatus", Logger.LEVEL.DEBUG);
                    }
                }
            }
        }else{
            //the save failed
            if(System.currentTimeMillis() - 86400000L > statusUpdate.updateTime){
                //the update is more than a day old, and should be removed
                if(failedStatusUpdates.contains(statusUpdate)){
                    failedStatusUpdates.remove(statusUpdate);
                    Logger.logMessage("Removed day old failedStatusUpdate in KMSManager.updateOrderStatus", Logger.LEVEL.DEBUG);
                }
            }else if(!failedStatusUpdates.contains(statusUpdate)){//Note: this must ignore the previouslyFailed field
                statusUpdate.currentUpdateLocation = KMSStatusUpdateModel.SENT_TO_SERVER;
                failedStatusUpdates.add(statusUpdate);
                Logger.logMessage("Added failedStatusUpdate in KMSManager.updateOrderStatus", Logger.LEVEL.DEBUG);
            }
        }
        return updated > 0;
    }

    /**
     * Returns a list of updates that failed to go through in order of oldest first.
     * Note: this MUST only return ones that have failed. When a successful future update comes through and toggles previously failed updates to passed.
     *      Those MUST NOT be in the list in case the server goes down and cant recognize that they are old
     * @return
     */
    public ArrayList getUnsavedOrderUpdates(){
        Logger.logMessage("RPC call to KMSManager.getUnsavedOrderUpdates", Logger.LEVEL.DEBUG);
        ArrayList<KMSStatusUpdateModel> statuses = new ArrayList<>();
        synchronized (failedStatusUpdates){
            for(int i = 0; i < failedStatusUpdates.size(); i++){
                if(failedStatusUpdates.get(i).currentUpdateLocation == KMSStatusUpdateModel.SENT_TO_SERVER)
                    statuses.add(failedStatusUpdates.get(i));
            }
        }
        return statuses;
    }

    public ArrayList getPrepExpMappings(){
        Logger.logMessage("RPC call to KMSManager.getPrepExpMappings", Logger.LEVEL.DEBUG);
        return dm.parameterizedExecuteQuery("data.KMSManager.getPrepExpMappings", new Object[]{}, true);
    }

    public ArrayList getDisplayStations(){
        Logger.logMessage("RPC call to KMSManager.getDisplayStations", Logger.LEVEL.DEBUG);
        return dm.parameterizedExecuteQuery("data.KMSManager.getDisplayStations", new Object[]{}, true);
    }

    public ArrayList getBackupAndMirrors(){
        Logger.logMessage("RPC call to KMSManager.getBackupAndMirrors", Logger.LEVEL.DEBUG);
        return dm.parameterizedExecuteQuery("data.KMSManager.getBackupAndMirrors", new Object[]{}, true);
    }

    public ArrayList getConfigUpdates(){
        Logger.logMessage("RPC call to KMSManager.getConfigUpdates", Logger.LEVEL.DEBUG);
        return dm.parameterizedExecuteQuery("data.KMSManager.getStationUpdateTimes", new Object[]{}, true);
    }

    public Object getKMSChangePollingRate(){
        return dm.parameterizedExecuteScalar("data.KMSManager.getPollingRate", new Object[]{});
    }

    public ArrayList getTransactionData(String transactionID){
        Logger.logMessage("RPC call to KMSManager.getTransactionData for transaction " + transactionID, Logger.LEVEL.DEBUG);
        return dm.parameterizedExecuteQuery("data.kms.GetTransactionData", new Object[]{transactionID}, true);
    }

    public ArrayList getWebsocketConfig(String printerHostMAC){
        Logger.logMessage("RPC call to KMSManager.getWebsocketTimeouts for printer host " + printerHostMAC, Logger.LEVEL.DEBUG);
        return dm.parameterizedExecuteQuery("data.KMSManager.getWebsocketConfig", new Object[]{printerHostMAC}, true);
    }

    public ArrayList getOrderUpdateMessages(String transactionID, Integer kmsOrderStatus) {

        Logger.logMessage("RPC call to KMSManager.getOrderUpdateMessage (GOUM) for transaction " +
                transactionID + ", order status " + kmsOrderStatus, Logger.LEVEL.TRACE);

        ArrayList<HashMap> messageData = new ArrayList<>();

        try {
            // if the order is finalized, retrieve the appropriate information
            if (kmsOrderStatus == 6) { // finalized
                Logger.logMessage("Order status is finalized, attempting to retrieve message data (GOUM)", Logger.LEVEL.TRACE);
                messageData = dm.parameterizedExecuteQuery("data.KMSManager.getOrderUpdateMessageDetails", new Object[]{transactionID}, true);
            }

            Logger.logMessage(messageData.size() + " message details have been found (GOUM)", Logger.LEVEL.TRACE);

            // HashMap to map from the regex for the tag to replace to the key into the messagedata for what replaces it
            HashMap<String, String> messageReplacements = new HashMap<>();
            messageReplacements.put("\\[\\[OrderNumber\\]\\]", "ORDERNUMBER");
            messageReplacements.put("\\[\\[CustomerName\\]\\]", "CUSTOMERNAME");
            messageReplacements.put("\\[\\[RevenueCenter\\]\\]", "REVENUECENTERNAME");
            messageReplacements.put("\\[\\[OnlineOrderingStoreName\\]\\]", "ONLINEORDERINGSTORENAME");

            Logger.logMessage("About to make message replacements (GOUM)", Logger.LEVEL.TRACE);

            // Iterate through each value to replace and replace it
            for (HashMap eaMessageData : messageData) {
                Logger.logMessage("About to iterate through each regex  - existing message is " + eaMessageData.getOrDefault("MESSAGE", "{No message}").toString() + " (GOUM)", Logger.LEVEL.TRACE);
                for (String eaRegex : messageReplacements.keySet()) {
                    String eaKey = messageReplacements.get(eaRegex);
                    if (eaKey != null) {
                        eaMessageData.put("MESSAGE", eaMessageData.get("MESSAGE").toString().replaceAll(eaRegex, eaMessageData.get(eaKey).toString()));
                    }
                }
                Logger.logMessage("Made replacements to message - final message is " + eaMessageData.getOrDefault("MESSAGE", "{No message}").toString() + " (GOUM)", Logger.LEVEL.TRACE);
            }

            Logger.logMessage("Made all message replacements (GOUM)", Logger.LEVEL.TRACE);
        }
        catch (Exception ex) {
            Logger.logMessage("Error getting order update messages in KMSDataManager.getOrderUpdateMessages", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return messageData;
    }

    public Boolean sendOrderUpdateMessage(Integer thirdPartyApiId, String sendToNumber, String messageContent) {

        Logger.logMessage("RPC call to KMSManager.sendOrderUpdateMessage for sms with content '" +
                messageContent + "', third party api id " + thirdPartyApiId + ", and to the number" + sendToNumber, Logger.LEVEL.DEBUG);

        Boolean callHandled = false;

        try {
            SmsFactory apiFactory = (SmsFactory) ThirdPartyAPIFactoryProducer.getThirdPartyApiFactory("SMS");
            SmsAPIModel smsModel = apiFactory.getThirdPartyApi(thirdPartyApiId);

            if (smsModel != null) {
                Logger.logMessage("Setting phone number to send sms to: " + sendToNumber, Logger.LEVEL.DEBUG);
                smsModel.setSendToPhoneNumber(sendToNumber);

                Logger.logMessage("Setting message content: " + messageContent, Logger.LEVEL.DEBUG);
                smsModel.setContent(messageContent);

                Logger.logMessage("Handling the api call", Logger.LEVEL.DEBUG);
                smsModel.handleApiCall();
                Logger.logMessage("Handled the api call", Logger.LEVEL.DEBUG);
                callHandled = true;
            }
        }
        catch (Exception ex) {
            Logger.logMessage("Error sending order update message in KMSManager.sendOrderUpdateMessage", Logger.LEVEL.ERROR);
        }

        return callHandled;
    }

    /**
     * <p>Invoked through XML-RPC to get any previous order numbers within the transaction with the given ID.</p>
     *
     * @param paTransactionID The ID of the transaction.
     * @return A comma delimited {@link String} containing previous order numbers within the transaction with the given ID.
     */
    @SuppressWarnings("unchecked")
    public String getPreviousOrderNumbersInTransaction (int paTransactionID) {

        // make sure the transaction ID is valid
        if (paTransactionID <= 0) {
            Logger.logMessage("The transaction ID passed to KMSDataManager.getPreviousOrderNumbersInTransaction must be greater than 0!", KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
            return "";
        }

        // query the database
        ArrayList<HashMap> queryRes = DataFunctions.purgeAlOfHm(dm.parameterizedExecuteQuery("data.kms.GetPreviousOrderNumbersInTransaction", new Object[]{paTransactionID}, KMS_DATA_MANAGER_LOG, true));
        if (!DataFunctions.isEmptyCollection(queryRes)) {
            ArrayList<String> previousOrderNumbers = new ArrayList<>();
            for (HashMap hm : queryRes) {
                String previousOrderNumber = HashMapDataFns.getStringVal(hm, "ORDERNUM");
                if (StringFunctions.stringHasContent(previousOrderNumber)) {
                    previousOrderNumbers.add(previousOrderNumber);
                }
            }

            if (!DataFunctions.isEmptyCollection(previousOrderNumbers)) {
                Logger.logMessage(String.format("Found the previous order numbers %s, within the transaction with an ID of %s in KMSDataManager.getPreviousOrderNumbersInTransaction.",
                        Objects.toString(StringFunctions.buildStringFromList(previousOrderNumbers, ",") , "N/A"),
                        Objects.toString(paTransactionID, "N/A")), KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);
                return StringFunctions.buildStringFromList(previousOrderNumbers, ",");
            }
            else {
                Logger.logMessage(String.format("No previous order numbers were found within the transaction with an ID of %s in KMSDataManager.getPreviousOrderNumbersInTransaction.",
                        Objects.toString(paTransactionID, "N/A")), KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);
                return "";
            }
        }
        else {
            Logger.logMessage(String.format("No previous order numbers were found within the transaction with an ID of %s in KMSDataManager.getPreviousOrderNumbersInTransaction.",
                    Objects.toString(paTransactionID, "N/A")), KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);
            return "";
        }

    }

    /**
     * <p>Called through XML RPC to print a customer or merchant receipt.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param customerReceiptDataStruct The {@link XmlRpcStruct} that will be used to create a {@link CustomerReceiptData}.
     * @return Whether or not the consolidated receipt could be printed properly.
     */
    @SuppressWarnings({"TypeMayBeWeakened", "ThrowableResultOfMethodCallIgnored"})
    public boolean printConsolidatedReceipt (String callerHostname, XmlRpcStruct customerReceiptDataStruct) throws Exception {

        if (StringFunctions.stringHasContent(callerHostname)) {
            Logger.logMessage(String.format("The method KMSDataManager.printConsolidatedReceipt has been invoked from the terminal with a hostname of %s.",
                    Objects.toString(callerHostname, "N/A")), KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);
        }

        // make sure the customer data struct is valid
        if (DataFunctions.isEmptyMap(customerReceiptDataStruct)) {
            Logger.logMessage("The customer receipt data struct passed to KMSDataManager.printConsolidatedReceipt can't be null or empty!", KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // convert the XmlRpcStruct to a CustomerReceiptData
        CustomerReceiptData customerReceiptData = CustomerReceiptData.buildFromXmlRpcStruct(customerReceiptDataStruct);

        // make sure the customer receipt data is valid
        if (customerReceiptData == null) {
            Logger.logMessage("A problem occurred while trying to convert the XmlRpcStruct into CustomerReceiptData in KMSDataManager.printConsolidatedReceipt!", KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        Logger.logMessage("Now logging the CustomerReceiptData received in KMSDataManager.printConsolidatedReceipt...", KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);
        Logger.logMessage(customerReceiptData.toString(), KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);

        ReceiptGen receiptGen = new ReceiptGen();

        if (customerReceiptData.getIsGiftReceipt()) {
            receiptGen.setReceiptFormatter(new GiftReceiptFormatter());
        }

        ReceiptData receiptData = receiptGen.createReceiptData();
        receiptData.setPrintGrossWeight(customerReceiptData.getPrintGrossWeight());
        receiptData.setPrintNetWeight(customerReceiptData.getPrintNetWeight());
        receiptGen.setReceiptData(receiptData);

        // if there's an email address then email the receipt
        if (StringFunctions.stringHasContent(customerReceiptData.getEmailAddress())) {
            Logger.logMessage(String.format("Now attempting to email the consolidated receipt to %s in KMSDataManager.printConsolidateReceipt!",
                    Objects.toString(customerReceiptData.getEmailAddress(), "N/A")), KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);
            return CustomerReceipt.emailConsolidatedReceipt(KMS_DATA_MANAGER_LOG, receiptGen, customerReceiptData);
        }
        else {
            Logger.logMessage("Now attempting to print the consolidated receipt at the local receipt printer in KMSDataManager.printConsolidateReceipt!", KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);
            boolean printReceiptResult = false;
            try {
                printReceiptResult = CustomerReceipt.printConsolidatedReceipt(KMS_DATA_MANAGER_LOG, receiptGen, customerReceiptData);
            }
            catch (Exception e) {
                Logger.logException(e, KMS_DATA_MANAGER_LOG);
                Logger.logMessage("A problem occurred while trying to print the consolidated receipt in KMSDataManager.printConsolidatedReceipt!", KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
            }
            return printReceiptResult;
        }

    }

    /**
     * <p>Called through XML RPC to send error receipts through email from the server.</p>
     *
     * @param callerHostname The hostname {@link String} of the terminal that made the XML RPC call.
     * @param printJobJSON The errored print job to send in the emails in a JSON {@link String} format.
     * @return Whether or not the emails could be sent successfully.
     */
    public boolean sendErrorReceipt (String callerHostname, String printJobJSON) {

        // validate the caller hostname
        if (!StringFunctions.stringHasContent(callerHostname)) {
            Logger.logMessage("Unable to determine the hostname of the terminal that invoked KMSDataManager.sendErrorReceipt on the server.", KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
        }

        // validate the print job JSON
        if (!StringFunctions.stringHasContent(printJobJSON)) {
            Logger.logMessage("The print job JSON passed to KMSDataManager.sendErrorReceipt can't be null or empty, unable to send " +
                    "emails for the errored print job from the server.", KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        // convert the print job JSON to a LocalPrintJob
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LocalPrintJob.class, new LocalPrintJobAdapter());
        Gson gson = builder.create();
        LocalPrintJob localPrintJob;
        try {
            localPrintJob = gson.getAdapter(LocalPrintJob.class).fromJson(printJobJSON);
        }
        catch (IOException e) {
            Logger.logException(e, KMS_DATA_MANAGER_LOG);
            Logger.logMessage("A problem occurred when trying to convert the errored print job JSON to a LocalPrintJob in KMSDataManager.sendErrorReceipt, unable to send " +
                    "emails for the errored print job from the server.", KMS_DATA_MANAGER_LOG, Logger.LEVEL.ERROR);
            return false;
        }

        Logger.logMessage(String.format("KMSDataManager.sendErrorReceipt has been invoked on the server from the terminal %s!",
                Objects.toString(callerHostname, "N/A")), KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);

        // get the email settings
        HashMap<String, String> emailSettings = CommonAPI.getEmailSettings();

        // get the email recipients
        int terminalID;
        if (KitchenPrinterCommon.isOOTerminal(KMS_DATA_MANAGER_LOG, dm, localPrintJob.getTerminalID())) {
            terminalID = KitchenPrinterCommon.getTerminalIDOfInServicePHForOOTerminal(KMS_DATA_MANAGER_LOG, dm, localPrintJob.getTerminalID());
            Logger.logMessage(String.format("The terminal ID %s is for an online ordering terminal that will be sending orders to " +
                    "the printer host running on the terminal with an ID of %s in KMSDataManager.sendEmailReceipt!",
                    Objects.toString(localPrintJob.getTerminalID(), "N/A"),
                    Objects.toString(terminalID, "N/A")), KMS_DATA_MANAGER_LOG, Logger.LEVEL.TRACE);
        }
        else {
            terminalID = localPrintJob.getTerminalID();
        }
        ArrayList<String> emailRecipients = EmailedErrorReceipt.getEmailRecipients(terminalID, KMS_DATA_MANAGER_LOG, dm);

        // build and send out the emails
        return EmailedErrorReceipt.sendEmailsForLocalPrintJob(callerHostname, localPrintJob, emailSettings, emailRecipients, KMS_DATA_MANAGER_LOG, dm);
    }

}