package com.mmhayes.common.kmsapi;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-11-03 10:09:01 -0500 (Tue, 03 Nov 2020) $: Date of last commit
    $Rev: 13030 $: Revision of last commit
    Notes: Contains the names of the methods available in the KMSDataManager.
*/

/**
 * <p>Contains the names of the methods available in the {@link KMSDataManager}</p>
 *
 */
public class KMSDataManagerMethod {

    public static final String KMS_PRINT_ORDER_ONLINE_MODE = "KMSDataManager.printOrderUsingKMSInOnlineMode";
    public static final String KMS_PRINT_ORDER_OFFLINE_MODE = "KMSDataManager.printOrderUsingKMSInOfflineMode";
    public static final String GET_PREV_ORDER_NUMS_IN_TXN = "KMSDataManager.getPreviousOrderNumbersInTransaction";
    public static final String PRINT_CONSOL_RCPT = "KMSDataManager.printConsolidatedReceipt";
    public static final String SEND_ERROR_RECEIPT = "KMSDataManager.sendErrorReceipt";

}