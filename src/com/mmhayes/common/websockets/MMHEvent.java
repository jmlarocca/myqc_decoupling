package com.mmhayes.common.websockets;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class MMHEvent {
    public static final String AUTHENTICATE = "authenticate";
    public static final String CONFIRMATION = "confirmation";

    public static final String ORDER_STATUS_UPDATE = "order-status-update";
    public static final String ORDER_UPLOAD = "order-status-upload";
    public static final String ORDER_ITEM_STATUS_UPDATE = "item-status-update";
    public static final String CONFIGURATION_UPDATE = "config-update";
    public static final String ORPHANED_ORDER = "orphaned-order";
    public static final String INQUIRY = "query";
    public static final String PRINT_ORDER = "print-order";
    public static final String REQUEST_DOWNLOAD = "request-download";
    public static final String TRIGGER_CONFIG_UPDATE = "trigger-config-update";

    //TODO: these arent websocket events, these are KMSEventTypes
    public class KMSEventType{
        public static final String BUMPED = "bumped";
        public static final String UNBUMPED = "unbumped";
        public static final String SUSPENDED = "suspended";
        public static final String CANCELED = "canceled";
        public static final String TRANSFER = "transfer";
        public static final String ITEM_UPDATE = "item-update";
        public static final String ERROR = "error";
        public static final String ORDER_RECEIVED = "order-received";
        public static final String MANUAL_BACKUP = "manual-backup";
        public static final String AUTOMATIC_BACKUP = "automatic-backup";
        public static final String STATION_ONLINE = "station-online";
        public static final String STATION_OFFLINE = "station-offline";
        public static final String LOG_MESSAGE = "log-message";
    }

    //TODO: not sure if we really need these...
    public class Status{
        public static final String OKAY = "okay";
        public static final String ERROR = "error";
    }



}
