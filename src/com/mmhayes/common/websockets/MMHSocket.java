/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.mmhayes.common.websockets;

import com.google.gson.Gson;
import com.mmhayes.common.kms.KMSCloseReason;
import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;

import javax.websocket.*;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

//import util.HTMLFilter;

/**the base class does not have an endpoint. Child classes should include the following line uncommented:
 * //@ServerEndpoint(value = "/websocket/test", configurator = MMHSocketConfig.class)
 */
public abstract class MMHSocket {
    public static long PING_PONG_FREQUENCY = 3000;
    public static long MISSED_PING_PONGS_FOR_TIMEOUT = 5;
    private static final String CONNECTION_PREFIX = "Connection ";
    private static final AtomicInteger connectionIds = new AtomicInteger(0);
    //This set of connections is global to ALL classes that extend MMHSocket
    private static final Set<MMHSocket> connections = Collections.synchronizedSet(new HashSet<>());

    private long connectedAtTime;
    protected long lastCommTime;
    private boolean authorized = false;

    protected static final String DEFAULT_MESSAGE_KEY = "message";
    protected final String connectionID;
    protected Session session;

    public MMHSocket() {
        connectionID = CONNECTION_PREFIX + connectionIds.getAndIncrement();
    }

    public Session getSession(){
        return session;
    }

    @OnOpen
    public void start(Session session) {
        this.session = session;
        this.connectedAtTime = System.currentTimeMillis();
        this.lastCommTime = connectedAtTime;
    }

    @OnClose
    public void end() {
        removeFromConnectionSet(this);
        if(this.authorized){
            handleConnectionClosing();
        }
    }

    @OnMessage
    public void onPong(PongMessage pong){
        try {
            Logger.logMessage("PONG for KMSM's WS" + getSession().getId(), "KMS_PPWC.log", Logger.LEVEL.LUDICROUS);
            lastCommTime = System.currentTimeMillis();
            String timeSent = new String(pong.getApplicationData().array());
            long lag = lastCommTime - Long.parseLong(timeSent);

            if (lag > 200) {
                Logger.logMessage("WebSocket High Latency: " + lag + "ms");
            }

        } catch (Exception ex) {
            Logger.logMessage("Invalid Pong message", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

//    @OnWebSocketFrame
//    @SuppressWarnings("unused")
//    public void onFrame(Frame pong) {
//        if (pong instanceof PongFrame) {
//            lastPong = Instant.now();
//        }
//    }

    /**
     * This function should be run before ANY messages are processed. This will handle authenticating the connection
     * @param message the raw message from the client.
     * @return true iff the message should be processed. Note: This will return false on a successful validation message
     */
    protected boolean validateIncomingMessage(String message) {
        message = message.trim();
        if(message.isEmpty())
            return false;

        MMHSocketMessage parsedMessage = new Gson().fromJson(message, MMHSocketMessage.class);
        if(!authorized){
            //the first message from the client needs to be an authorization message containing a valid ticket
            // TODO:(currently this is using the DSKey as a token. but this is not a one time connection ticket...)
            //check the authorization message
            String ID = null;
            try {
                if(parsedMessage != null && parsedMessage.token != null){
                    ID = validateToken(parsedMessage.token);
                }
            } catch (Exception e) {
                Logger.logException(e);
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            Logger.logMessage("MMHSocket.validateIncomingMessage: ID found to be" + ID, "KMS.log", Logger.LEVEL.TRACE);
            if(ID != null){
                authorized = true;//the connection has passed auth
                handleConnectionApproved(ID);
                addToConnectionSet(this);//add it to the list so that it will receive messages from other connected clients.
                return false;//the initial auth message should need no further handling because its only a token
            }else{
                //Do not accept the handshake when the DSKey is null
                try {
                    this.getSession().close(KMSCloseReason.FAILED_VALIDATION.getCloseReason());
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }else{
            //the incoming message is from an authorized connection.
//            handleIncomingMessage(parsedMessage);
            return true;
        }
        Logger.logMessage("MMHSocket.validateIncomingMessage: FAILED TO VALIDATE MESSAGE SOURCE", "KMS.log", Logger.LEVEL.TRACE);
        return false;
    }

    @OnError
    public void onError(Throwable t) throws Throwable {
        if(t != null && t.getMessage() != null)
            Logger.logMessage(t.getMessage(), Logger.LEVEL.ERROR);
    }

    /**
     * This function is used to determine if the connections for this class should be added/sent/removed from a global connection collection
     * or if the connections should only be applicable to the connections for that class.
     * @param currentConnection the instance of MMHSocket that is requesting to get the connection list.
     * @return a connection set. Defaults to global connection collection.
     */
    protected Set<MMHSocket> getConnectionSet(MMHSocket currentConnection){
        return connections;
    }

    protected void addToConnectionSet(MMHSocket currentConnection){
        connections.add(currentConnection);
    }

    protected void removeFromConnectionSet(MMHSocket currentConnection){
        connections.remove(currentConnection);
    }

    /**
     * Checks that the token is valid and obtains the object ID associated with it.
     * @param token
     * @return
     */
    protected abstract String validateToken(String token);

    /**
     * This function is called when the connection to the client has been authorized.
     * This can be used to notify other clients of the connection, update data, etc.
     * @param stationID
     */
    public abstract void handleConnectionApproved(String stationID);

    /**
     * This function is used to implement logic specific to handling the message event and data of AUTHORIZED clients.
     * This will not be called for unauthorized clients.
     * @param rawMessage the message coming in from the client. Note: TODO:this may contain SQLInjection...
     */
    public abstract void handleIncomingMessage(String rawMessage);

    /**
     * This function is used to implement custom logic when an AUTHORIZED client is closing,
     *      and can be used to send messages to other connected clients
     * This will not be called for unauthorized clients.
     * The current client will already be removed from the list of connections by the time this function is called.
     */
    public abstract void handleConnectionClosing();

    /**
     * This function handles the flow of closing a connection that has experienced an error.
     * @param e
     */
    private void handleError(Exception e){
        Logger.logException(e);
        removeFromConnectionSet(this);
        try {
            this.session.close();
        } catch (IOException e1) {
            // Ignore
        }
        if(this.authorized){
            this.handleConnectionClosing();
        }
    }


    protected void sendToClient(String status, String event, Object message) {
        MMHSocketMessage mmhSocketMessage = new MMHSocketMessage();
        mmhSocketMessage.status = status;
        mmhSocketMessage.event = event;
        mmhSocketMessage.model = message;
        sendToClient(mmhSocketMessage);
    }
    protected void sendToClient(MMHSocketMessage message) {
        forwardTo(this, message);
    }


    /**
     * This function sends a message to every connected client in the list.
     * Usages of this method can be from any piece of code on the WAR.
     * @param message
     */
    public void broadcast(MMHSocketMessage message) {
        broadcast(null, message, true);
    }
    /**
     * This function will send a message to connected clients, including the client initializing the call.
     * Usages of this method originate with a client sending a message to other clients.
     * @param message
     * @param sendToSelf if true, the msg
     */
    public void broadcast(MMHSocket from, String status, String event, Object message, boolean sendToSelf) {
        MMHSocketMessage mmhSocketMessage = new MMHSocketMessage();
        mmhSocketMessage.status = status;
        mmhSocketMessage.event = event;
        mmhSocketMessage.model = message;
        broadcast(from, mmhSocketMessage, sendToSelf);
    }

    /**
     * A static method that mirrors the flow of this could be used to allow an extending class to provide methods to send
     * messages without needing an instance of the class.
     * @param from
     * @param message
     * @param sendToSelf
     */
    public void broadcast(MMHSocket from, MMHSocketMessage message, boolean sendToSelf) {
        Set<MMHSocket> clients = getConnectionSet(from);
        if (DataFunctions.isEmptyCollection(clients)) return;
        Iterator<MMHSocket> connectionItr = clients.iterator();
        synchronized (this) {
            while (connectionItr.hasNext()) {
                MMHSocket connection = connectionItr.next();
                if ((from != null) && (connection.equals(from)) && (sendToSelf) && (connection.authorized)) {
                    // send the message to the originator
                    LogWebsocketMessage(true,true, Objects.toString(connection.getSession().getUserProperties().get("STATION"), "N/A"), Objects.toString(new Gson().toJson(message), "N/A"));
                    try {
                        connection.session.getBasicRemote().sendText(new Gson().toJson(message));
                    }
                    catch (IOException e) {
                        connection.handleError(e);
                    }
                }
                else if (connection.authorized) {
                    // send the message to every connection (excludes the originator)
                    LogWebsocketMessage(true,true, Objects.toString(connection.getSession().getUserProperties().get("STATION"), "N/A"), Objects.toString(new Gson().toJson(message), "N/A"));
                    try {
                        connection.session.getBasicRemote().sendText(new Gson().toJson(message));
                    }
                    catch (IOException e) {
                        connection.handleError(e);
                    }
                }
            }
        }
    }


    //-----These functions will allow messages to be forwarded to specific clients

    /**
     * Send a message to a specific connection
     * @param to the connection to send this to
     * @param message the message to send
     * @return true if the message has been sent
     */
    public static boolean forwardTo(MMHSocket to, MMHSocketMessage message){
        if(to != null){
            if(to.authorized){
                LogWebsocketMessage(true,false, Objects.toString(to.getSession().getUserProperties().get("STATION"), "N/A"), Objects.toString(new Gson().toJson(message), "N/A"));
                try {
                    to.session.getBasicRemote().sendText(new Gson().toJson(message));
                    return true;
                } catch (IOException e) {
                    to.handleError(e);
                }
            }
        }
        Logger.logMessage("Attempted to send message to session: " + to.getSession().getId() + " whom is unauthorized to receive message " + new Gson().toJson(message) + " in regards to " + message.reference, "KMS.log", Logger.LEVEL.ERROR);
        return false;
    }

    public static void PingConnections(){
        String data = System.currentTimeMillis() +"";
        ByteBuffer payload = ByteBuffer.wrap(data.getBytes());
        for(MMHSocket connection : connections){
            try {
                if(connection != null && connection.getSession() != null){
                    Session s = connection.getSession();
                    if(s.isOpen()){
                        s.getBasicRemote().sendPing(payload);
                        Logger.logMessage("PING for KMSM's WS " + connection.getSession().getId(), "KMS_PPWC.log", Logger.LEVEL.LUDICROUS);
                    }
                }
            } catch (IOException e) {
                Logger.logException(e, "KMS.log");
            }
        }
    }

    /**
     * <p>Cleans up websocket connections which are no longer valid.</p>
     *
     */
    public static void ReapUnPonged(){

        if (!DataFunctions.isEmptyCollection(connections)) {
            Iterator<MMHSocket> connItr = connections.iterator();
            while (connItr.hasNext()) {
                MMHSocket socket = connItr.next();
                if (socket != null && socket.getSession() != null && socket.getSession().isOpen()) {
                    if (System.currentTimeMillis() - socket.lastCommTime > PING_PONG_FREQUENCY * MISSED_PING_PONGS_FOR_TIMEOUT) {
                        try {
                            Logger.logMessage("LOST " + socket.getSession().getId(), "KMS_PPWC.log", Logger.LEVEL.LUDICROUS);
                            socket.getSession().close(KMSCloseReason.MISSED_HEARTBEATS.getCloseReason());
                        }
                        catch (IOException e) {
                            socket.end();
                            Logger.logException(e, "KMS.log");
                        }
                        finally {
                            socket.end();
                            // remove the connection from the Set of connections
                            connItr.remove();
                        }
                    }
                }
            }
        }
        else {
            Logger.logMessage("There or no web socket connections to remove in MMHSocket.ReapUnPonged!", "KMS.log", Logger.LEVEL.TRACE);
        }

    }

    /**
     * Logs websocket messages in a parsable format for use by the websocket debug tool
     */
    public static void LogWebsocketMessage(boolean sending, boolean broadcast, String station, String message){
        Logger.logMessage(String.format("%-8s %-10s %-6s %-4s %s",
                sending ? "Sending" : "Received",
                broadcast ? "broadcast" : "message",
                sending ? "to" : "from", station,message), "Websocket.log", Logger.LEVEL.TRACE);
    }
}