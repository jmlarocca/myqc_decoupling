package com.mmhayes.common.websockets;

import com.mmhayes.common.api.CommonAuthResource;
import com.mmhayes.common.utils.Logger;

import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class MMHSocketConfig extends ServerEndpointConfig.Configurator {
    Map<String, String> dsKeys;

    public MMHSocketConfig(){
        dsKeys = new ConcurrentHashMap<>();
    }
    public Map<String, String> getConnectionKeys(){
        return Collections.unmodifiableMap(dsKeys);
    }

    /**
     * Inspect the DSKey and prevent connection without proper X-MMHayes-Auth
     * @param sec
     * @param request
     * @param response
     */
    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        super.modifyHandshake(sec, request, response);
        //get the DSKey and
        //NOTE: the below doesnt actually work because the SEC_WEBSOCKET_ACCEPT is set AFTER this code is called...
//        String dsKey = null;
//        try {
//            if(request.getHeaders().get("X-MMHayes-Auth") != null){
//                String token = request.getHeaders().get("X-MMHayes-Auth").get(0);
//                dsKey = CommonAuthResource.determineKMSStationIDFromAuthHeader(token) + "";
//            }
//        } catch (Exception e) {
//            Logger.logException(e);
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
//        if(dsKey != null){
//            String name = request.getUserPrincipal().getName();
//            dsKeys.put(name, dsKey);
//        }else{
//            //Do not accept the handshake when the DSKey is null
//            response.getHeaders().put(HandshakeResponse.SEC_WEBSOCKET_ACCEPT, new ArrayList<String>());
//        }
    }
}
