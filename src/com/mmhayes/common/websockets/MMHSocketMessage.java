package com.mmhayes.common.websockets;

import java.util.HashMap;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class MMHSocketMessage <T> {
//    public static final String STANDARD_MODEL_KEY = "model";
    public String token;
    public String event;
    public String status;
    public String reference; //TODO (low): If this is included, reference should be present in response conformations/errors
//    public HashMap<String, Object> data;
    public T model;


    public MMHSocketMessage(){
//        data = new HashMap<>();
    }

}
