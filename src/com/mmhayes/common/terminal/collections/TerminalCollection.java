package com.mmhayes.common.terminal.collections;

//mmhayes dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.ModelPaginator;

//API dependencies
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.utils.Logger;

//other dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-08-17 17:00:31 -0400 (Fri, 17 Aug 2018) $: Date of last commit
 $Rev: 7650 $: Revision of last commit
*/
public class TerminalCollection {

    List<TerminalModel> collection = new ArrayList<TerminalModel>();

    //constructor
    public TerminalCollection() {

    }

    public static List<TerminalModel> getAllTerminalsForRevenueCenter(ModelPaginator modelPaginator, TerminalModel requestTerminalModel) throws Exception {
        DataManager dm = new DataManager();
        List<TerminalModel> terminalModels = new ArrayList<TerminalModel>();
        modelPaginator.finalizePaginationProperties();

        String sortByColumn = ""; //what column should this result set be sorted by
        if (modelPaginator.getSort().toUpperCase().equals("ID")) {
            sortByColumn = "t.TerminalID";
        } else if (modelPaginator.getSort().toUpperCase().equals("DATE")) { //this is the default, there is no date field on the terminal, so default it to Name
            sortByColumn = "t.Name";
        } else if (modelPaginator.getSort().toUpperCase().equals("NAME")) {
            sortByColumn = "t.Name";
        } else {
            Logger.logMessage("ERROR: Invalid sort property in TerminalCollection.getAllTerminalsForRevenueCenter(). sort property = " + modelPaginator.getSort(), Logger.LEVEL.ERROR);
            throw new MissingDataException("Invalid sort parameter for Transaction Endpoint", requestTerminalModel.getId());
        }

        //get a single page of models in an array list
        ArrayList<HashMap> terminalList = dm.serializeSqlWithColNames("data.posapi30.getTerminalsByRevenueCenterPaginate",
                new Object[]{
                        requestTerminalModel.getRevenueCenterId(),
                        modelPaginator.getStartingModelIndex().toString(),
                        modelPaginator.getEndingModelIndex().toString(),
                        sortByColumn,
                        modelPaginator.getOrder().toUpperCase()
                }, null,
                PosAPIHelper.getLogFileName(requestTerminalModel.getId()),
                false

        );

        if (terminalList.size() > 0) {
            for (HashMap terminalHM : terminalList) {
                CommonAPI.checkIsNullOrEmptyObject(terminalHM.get("ID"), "Terminal Id cannot be found.", requestTerminalModel.getId());

                TerminalModel terminalModel = new TerminalModel().setModelProperties(terminalHM);
                terminalModels.add(terminalModel);
            }
        }

        return terminalModels;
    }

    //getter
    public List<TerminalModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(List<TerminalModel> collection) {
        this.collection = collection;
    }
}
