package com.mmhayes.common.terminal.models;

//MMHayes Dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

//API Dependencies
//Other Dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-07-22 15:48:57 -0400 (Wed, 22 Jul 2020) $: Date of last commit
 $Rev: 12218 $: Revision of last commit
*/
public class TerminalConnectionInfoModel {
    private Integer id = null;
    private Integer reasonCode = null;
    private String offlineDateTime = null;
    private String onlineDateTime = null;
    private String methodName = "";
    private String errorText = "";
    private TerminalModel terminal = null;

    private static DataManager dm = new DataManager();

    public TerminalConnectionInfoModel(){

    }

    public TerminalConnectionInfoModel setModelProperties(HashMap modelDetailHM) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setReasonCode(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALOFFLINEREASONID")));
        setMethodName(CommonAPI.convertModelDetailToString(modelDetailHM.get("METHODNAME")));
        setErrorText(CommonAPI.convertModelDetailToString(modelDetailHM.get("ERRORTEXT")));
        setOfflineDateTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("OFFLINEDATETIME")));
        setOnlineDateTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("ONLINEDATETIME")));

        return this;
    }

    /**
     * Insert the Terminal Connection Info Model
     *
     * @return a Terminal Connection Info Model with the inserted database record set as the ID
     */
    public TerminalConnectionInfoModel insertTerminalConnectionInfo() throws Exception {

        if (getTerminal() != null && getTerminal().getId() != null ) {

            if(getReasonCode() == null || getOfflineDateTime() == null || getOnlineDateTime() == null) {
                Logger.logMessage("Invalid Reason Code, Offline Date Time or Online Date Time in TerminalConnectionInfoModel.insertTerminalConnectionInfo", PosAPIHelper.getLogFileName(getTerminal().getId()), Logger.LEVEL.ERROR);
                return this;
            }

            //Check for dupe Terminal Connection Info
            ArrayList<HashMap> terminalOfflineRecordList = dm.parameterizedExecuteQuery("data.posapi30.getOneTerminalConnectionInfo",
                    new Object[]{ getTerminal().getId(), getOfflineDateTime()},
                    PosAPIHelper.getLogFileName(getTerminal().getId()),
                    true
            );

            if (terminalOfflineRecordList != null && !terminalOfflineRecordList.isEmpty()){
                //this request was already saved to the database.  Return the record
                this.setModelProperties(terminalOfflineRecordList.get(0));
                return this;
            }

            //insert Connection Info record into QC_TerminalOffline
            Integer insertResult = dm.parameterizedExecuteNonQuery("data.posapi30.InsertTerminalConnectionInfo",
                    new Object[]{
                            getTerminal().getId(),
                            getReasonCode(),
                            getOfflineDateTime(),
                            getOnlineDateTime(),
                            getMethodName(),
                            getErrorText()
                    },
                    PosAPIHelper.getLogFileName(getTerminal().getId())
            );

            //record success in response
            if (insertResult > 0) {
                setId(insertResult);

            } else {
                Logger.logMessage("Could not insert Terminal Connection record in TerminalConnectionInfoModel.insertTerminalConnectionInfo.", PosAPIHelper.getLogFileName(getTerminal().getId()), Logger.LEVEL.ERROR);
            }

        } else {
            Logger.logMessage("Could not determine terminalID TerminalConnectionInfoModel.insertTerminalConnectionInfo", Logger.LEVEL.ERROR);
        }

        return this;
    }

    //region Getters/Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        this.reasonCode = reasonCode;
    }


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getOfflineDateTime() {
        return offlineDateTime;
    }

    public void setOfflineDateTime(String offlineDateTime) {
        this.offlineDateTime = offlineDateTime;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getOnlineDateTime() {
        return onlineDateTime;
    }

    public void setOnlineDateTime(String onlineDateTime) {
        this.onlineDateTime = onlineDateTime;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    @JsonIgnore
    public TerminalModel getTerminal() {
        return terminal;
    }

    public void setTerminal(TerminalModel terminal) {
        this.terminal = terminal;
    }

    //endregion
}
