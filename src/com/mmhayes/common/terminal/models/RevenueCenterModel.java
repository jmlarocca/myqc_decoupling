package com.mmhayes.common.terminal.models;

//MMHayes Dependencies
import com.mmhayes.common.api.CommonAPI;

//API Dependencies
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//Other Dependencies
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
*/
public class RevenueCenterModel {
    private Integer id = null;
    private String name = null;
    private boolean allowCompoundSubTotalDiscounts = false;
    private boolean allowCompoundItemDiscounts = false;

    public RevenueCenterModel(){

    }

    public RevenueCenterModel(Integer id, String name){
        this.setId(id);
        this.setName(name);
    }

    public RevenueCenterModel setModelProperties(HashMap modelDetailHM) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        return this;
    }

    //region Getters/Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAllowCompoundSubTotalDiscounts() {
        return allowCompoundSubTotalDiscounts;
    }

    public void setAllowCompoundSubTotalDiscounts(boolean allowCompoundSubTotalDiscounts) {
        this.allowCompoundSubTotalDiscounts = allowCompoundSubTotalDiscounts;
    }

    public boolean isAllowCompoundItemDiscounts() {
        return allowCompoundItemDiscounts;
    }

    public void setAllowCompoundItemDiscounts(boolean allowCompoundItemDiscounts) {
        this.allowCompoundItemDiscounts = allowCompoundItemDiscounts;
    }


    //endregion
}
