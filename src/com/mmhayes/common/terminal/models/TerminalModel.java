package com.mmhayes.common.terminal.models;

//mmhayes dependencies

import com.mmhayes.common.account.models.AccountQueryParams;
import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.conn_pool.JDCConnection;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.login.LoginModel;
import com.mmhayes.common.product.models.ItemProfileMapping;
import com.mmhayes.common.transaction.collections.TaxCollection;
import com.mmhayes.common.transaction.models.TransactionModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;

//api dependencies
import com.fasterxml.jackson.annotation.*;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import io.swagger.annotations.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-09-10 14:46:42 -0400 (Fri, 10 Sep 2021) $: Date of last commit
 $Rev: 15347 $: Revision of last commit
*/
@JsonPropertyOrder({"id", "name", "typeId", "typeName", "defaultPurchaseCategoryId"})
public class TerminalModel {
    private Integer Id = null;
    private String name = "";
    private Integer typeId = null;
    private String typeName = "";
    private Integer defaultPurchaseCategoryId = null;
    private Integer revenueCenterId = null;
    private Integer userId = null;
    private Integer numSecondsSubmitTransactionTimeout = null;
    private Integer paSurchargeID = null;
    private Integer terminalModelId = null;

    private String terminalModelName = "";
    private String terminalLogFileName = "";
    private String nextAutoRefreshDateTime = null;
    private String transactionNameLabel = "";

    private Boolean sandBoxMode = false;
    private Boolean allowQcCouponDiscountOffline = false;
    private boolean disableSmartCombos = false;
    private Boolean useFundingTerminal = false;
    private Boolean useOnlineOrderingFunding = false;
    private boolean autoApplyQCCouponDiscounts = false; // PAAutoApplyQCCouponDsct
    private boolean allowOfflineOpenTxns = false;
    private boolean chargeAtTimeOfPurchase = true;
    private boolean allowOfflineVouchers = false;
    private boolean autoApplyQCDiscountOnSaleRequest = false;

    //receipt information
    private String receiptAcctInfoLine1 = "";
    private String receiptAcctInfoLine2 = "";
    private String receiptAcctInfoLine3 = "";
    private String receiptAcctInfoLine4 = "";
    private String receiptAcctInfoLine5 = "";

    //header
    private String receiptHeader1 = "";
    private String receiptHeader2 = "";
    private String receiptHeader3 = "";
    private String receiptHeader4 = "";
    private String receiptHeader5 = "";

    //footer
    private String receiptFooter1 = "";
    private String receiptFooter2 = "";
    private String receiptFooter3 = "";
    private String receiptFooter4 = "";
    private String receiptFooter5 = "";

    private LoginModel loginModel = null;
    private RevenueCenterModel revenueCenter = null;
    private List<ItemProfileMapping> itemProfileMappings = new ArrayList<>();

    private DataManager dm = new DataManager();

    private long startTime;
    private long endTime;
    private long transactionDuration;

    private String datawireId = "";
    private String paymentProcessorStoreNum = null;
    private String paymentProcessorTerminalNum = null;


    //default constructor
    public TerminalModel() {
        this.setStartTime(System.nanoTime());
        this.getSandBoxModeSetting();
    }

    //Constructor- takes in a Hashmap
    public TerminalModel(HashMap TerminalHM) {
        setModelProperties(TerminalHM);
        this.setStartTime(System.nanoTime());
        this.getSandBoxModeSetting();
    }

    //Constructor- takes in a LoginModel
    public TerminalModel(LoginModel loginModel) {
        setLoginModel(loginModel);
        this.setStartTime(System.nanoTime());
        this.getSandBoxModeSetting();
    }

    public TerminalModel(Integer terminalId) throws Exception {
        this.setStartTime(System.nanoTime());
        this.getSandBoxModeSetting();
        this.createTerminalModel(terminalId);
    }

    public static TerminalModel createTerminalModel(HashMap terminalHM, boolean setLogFileName, boolean fetchCache) throws Exception {
        TerminalModel terminalModel = new TerminalModel(terminalHM);

        if (setLogFileName) {
            if (terminalModel.getId() != null && !terminalModel.getId().toString().isEmpty()) {
                PosAPIHelper.setLogFileName(terminalModel);
            }
        }

        if (fetchCache) {
            if (PosAPIModelCache.useTerminalCache) {

                TerminalModel.initTerminalCache(terminalModel);
            }
        }

        return terminalModel;
    }

    public static TerminalModel createTerminalModel(Integer terminalId, boolean setLogFileName) throws Exception {

        TerminalModel terminalModel = new TerminalModel().createTerminalModel(terminalId);

        Logger.logMessage("Terminal Created.  TerminalModel.createTerminalModel.", PosAPIHelper.getLogFileName(terminalModel.getId()), Logger.LEVEL.DEBUG);

        if (setLogFileName) {
            if (terminalId != null && !terminalId.toString().isEmpty()) {
                PosAPIHelper.setLogFileName(terminalModel);
            }
        }

        if (PosAPIModelCache.useTerminalCache) {
            TerminalModel.initTerminalCache(terminalModel);
        }

        return terminalModel;
    }

    public static TerminalModel createTerminalModel(LoginModel loginModel, boolean setLogFileName) throws Exception {
        return createTerminalModel(loginModel, setLogFileName, true);
    }

    /**
     * The loginModel may include the Terminal ID but it may not.
     * If the Terminal ID exists, validate it
     * If the Terminal ID does not exist, create a new Terminal Model object with a null ID
     *
     * @param loginModel
     * @return
     * @throws Exception
     */
    public static TerminalModel createOptionalTerminalModel(LoginModel loginModel) throws Exception {
        TerminalModel terminalModel = null;
        if (loginModel.getTerminalId() != null && !loginModel.getTerminalId().toString().isEmpty()) {
            terminalModel = TerminalModel.createTerminalModel(loginModel, true);
        } else {
            terminalModel = new TerminalModel(loginModel);
        }

        return terminalModel;
    }

    /**
     * The loginModel may include the Terminal ID but it may not.
     * If the Terminal ID was sent in on the AccountQueryParams, validate that
     * If the Terminal's Client Identifier was sent in on the AccountQueryParams, validate that
     * If the Terminal ID exists on the DSKey, validate it
     * If the Terminal ID does not exist, create a new Terminal Model object with a null ID
     *
     * @param loginModel
     * @return
     * @throws Exception
     */
    public static TerminalModel createOptionalTerminalModel(LoginModel loginModel, AccountQueryParams accountQueryParams) throws Exception {
        TerminalModel terminalModel = null;

        //Check the AccountQueryParams first
        if (accountQueryParams != null) {
            if (accountQueryParams.getTerminalId() != null && !accountQueryParams.getTerminalId().toString().isEmpty()) {
                terminalModel = TerminalModel.createTerminalModel(accountQueryParams.getTerminalId(), false);
            } else if (accountQueryParams.getClientIdentifier() != null && !accountQueryParams.getClientIdentifier().isEmpty()) {
                terminalModel = TerminalModel.getOneTerminalModelByTerminalIdentifier(accountQueryParams.getClientIdentifier());
            }
        }

        if (terminalModel == null) {
            if (loginModel.getTerminalId() != null && !loginModel.getTerminalId().toString().isEmpty()) {
                terminalModel = TerminalModel.createTerminalModel(loginModel, true);
            } else {
                terminalModel = new TerminalModel(loginModel);
            }
        }

        return terminalModel;
    }

    /**
     * Create the Terminal Model
     *
     * @param loginModel     - contains terminal Id and User Id
     * @param setLogFileName -
     * @param fetchCache     - Do we need to fetch the cache.  In some situations we don't, like a No Sale transaction
     * @return a Terminal Model with the error log name set
     * @throws Exception
     */
    public static TerminalModel createTerminalModel(LoginModel loginModel, boolean setLogFileName, boolean fetchCache) throws Exception {
        TerminalModel terminalModel = new TerminalModel().createTerminalModel(loginModel.getTerminalId());
        terminalModel.setLoginModel(loginModel);
        Logger.logMessage("Terminal Created.  TerminalModel.createTerminalModel.", PosAPIHelper.getLogFileName(terminalModel.getId()), Logger.LEVEL.DEBUG);

        if (setLogFileName) {
            if (loginModel.getTerminalId() != null && !loginModel.getTerminalId().toString().isEmpty()) {
                PosAPIHelper.setLogFileName(terminalModel);
            }
        }

        if (fetchCache) { //used by commCheck, we don't need to fetch Programs on the comm check
            if (PosAPIModelCache.useTerminalCache) {
                TerminalModel.initTerminalCache(terminalModel);
            }
        }

        return terminalModel;
    }

    /**
     * Create the Terminal Model
     *
     * @param loginModel     - contains terminal Id and User Id
     * @param setLogFileName -
     * @param fetchCache     - Do we need to fetch the cache.  In some situations we don't, like a No Sale transaction
     * @return a Terminal Model with the error log name set
     * @throws Exception
     */
    public static TerminalModel createTerminalModelNoMappings(LoginModel loginModel, boolean setLogFileName, boolean fetchCache) throws Exception {
        TerminalModel terminalModel = new TerminalModel().createTerminalModelNoMappings(loginModel.getTerminalId());
        terminalModel.setLoginModel(loginModel);
        Logger.logMessage("Terminal Created.  TerminalModel.createTerminalModel.", PosAPIHelper.getLogFileName(terminalModel.getId()), Logger.LEVEL.DEBUG);

        if (setLogFileName) {
            if (loginModel.getTerminalId() != null && !loginModel.getTerminalId().toString().isEmpty()) {
                PosAPIHelper.setLogFileName(terminalModel);
            }
        }

        if (fetchCache) { //used by commCheck, we don't need to fetch Programs on the comm check
            if (PosAPIModelCache.useTerminalCache) {
                TerminalModel.initTerminalCache(terminalModel);
            }
        }

        return terminalModel;
    }

    //setter for all of this model's properties
    public TerminalModel setModelProperties(HashMap modelDetailHM) {

        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("TERMINALNAME")));
        setTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALTYPEID")));
        setDefaultPurchaseCategoryId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PROFILEDEFAULTPOSITEM")));
        setRevenueCenterId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("REVENUECENTERID")));

        setReceiptAcctInfoLine1(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTACCTINFOLINE1")));
        setReceiptAcctInfoLine2(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTACCTINFOLINE2")));
        setReceiptAcctInfoLine3(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTACCTINFOLINE3")));
        setReceiptAcctInfoLine4(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTACCTINFOLINE4")));
        setReceiptAcctInfoLine5(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTACCTINFOLINE5")));

        setReceiptHeader1(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTHEADER1")));
        setReceiptHeader2(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTHEADER2")));
        setReceiptHeader3(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTHEADER3")));
        setReceiptHeader4(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTHEADER4")));
        setReceiptHeader5(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTHEADER5")));

        setReceiptFooter1(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTFOOTER1")));
        setReceiptFooter2(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTFOOTER2")));
        setReceiptFooter3(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTFOOTER3")));
        setReceiptFooter4(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTFOOTER4")));
        setReceiptFooter5(CommonAPI.convertModelDetailToString(modelDetailHM.get("PARECEIPTFOOTER5")));

        setTerminalModelId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALMODELID")));
        setTerminalModelName(CommonAPI.convertModelDetailToString(modelDetailHM.get("TERMINALMODELNAME")));
        setAllowQcCouponDiscountOffline(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWQCCOUPONDISCOUNTOFFLINE"), false));
        setNextAutoRefreshDateTime(CommonAPI.convertModelDetailToString(modelDetailHM.get("NEXTAUTOREFRESHDATETIME")));
        setNumSecondsSubmitTransactionTimeout(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PANUMSECONDSSUBMITTXNTIMEOUT")));
        setTransactionNameLabel(CommonAPI.convertModelDetailToString(modelDetailHM.get("PATRANSNAMELABEL")));
        setDisableSmartCombos(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("DISABLESMARTCOMBOS"), false));
        setUseOnlineOrderingFunding(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("USEONLINEORDERINGFUNDING"), false));
        setUseFundingTerminal(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("USEFUNDINGTERMINAL"), false));
        setAutoApplyQCCouponDiscounts(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("AUTOAPPLYQCCOUPONDISCOUNTS"), false));
        setPASurchargeID(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PASURCHARGEID")));
        setAllowOfflineOpenTxns(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWOFFLINEOPENTXNS"), false));
        setDatawireId(CommonAPI.convertModelDetailToString(modelDetailHM.get("DATAWIREID")));
        setPaymentProcessorStoreNum(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTPROCESSORSTORENUM")));
        setPaymentProcessorTerminalNum(CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYMENTPROCESSORTERMINALNUM")));
        setAllowOfflineVouchers(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWOFFLINEVOUCHERS"), false));
        setAutoApplyQCDiscountOnSaleRequest(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("PAAUTOAPPLYQCDSCTONSALEREQUEST"), false));

        this.cleanAndSetLogFileName(CommonAPI.convertModelDetailToString(modelDetailHM.get("TERMLOGFILENAME")));

        RevenueCenterModel revenueCenterModel = new RevenueCenterModel();
        revenueCenterModel.setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("REVENUECENTERID")));
        revenueCenterModel.setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("REVENUECENTERNAME")));
        revenueCenterModel.setAllowCompoundItemDiscounts(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWCOMPOUNDITEMDISC"), false));
        revenueCenterModel.setAllowCompoundSubTotalDiscounts(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("ALLOWCOMPOUNDSUBTOTALDISC"), false));
        this.setRevenueCenter(revenueCenterModel);

        return this;
    }

    public TerminalModel createTerminalModel(Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        this.setId(terminalId);
        ArrayList<HashMap> terminalList = null;

        this.getSandBoxModeSetting();

        if ((MMHProperties.getAppSetting("site.application.offline").length() > 0) && (MMHProperties.getAppSetting("site.application.offline").compareToIgnoreCase("yes") == 0)) {
            //if the terminal is offline, we don't fetch all the Item Profile Mappings.  We don't need them, and the table QC_PAItemPOSItemMap doesn't exist in the local DB
            terminalList = dm.parameterizedExecuteQuery("data.posapi30.getOneTerminalByTerminalTypeOffline",
                    new Object[]{
                            this.getId()
                    },
                    PosAPIHelper.getLogFileName(terminalId),
                    true
            );
        } else {
            terminalList = dm.parameterizedExecuteQuery("data.posapi30.getOneTerminalByTerminalType",
                    new Object[]{
                            this.getId()
                    },
                    PosAPIHelper.getLogFileName(terminalId),
                    true
            );
        }

        //populate this collection from an array list of hashmaps
        CommonAPI.checkIsNullOrEmptyList(terminalList, CommonAPI.PosModelType.TERMINAL, "", terminalId);
        HashMap modelDetailHM = terminalList.get(0);

        CommonAPI.checkIsNullOrEmptyObject(modelDetailHM.get("ID"), PosAPIHelper.getLogFileName(0));
        setModelProperties(modelDetailHM);

        //map Item profiles for 3rd party transactions
        //Set the list of Item Profile Mappings to be used during the transaction inquire and save for QC_Transactions
        List<ItemProfileMapping> itemProfileMappings = new ArrayList<>();
        for (HashMap terminalHM : terminalList) {
            if (terminalHM.get("PAITEMTYPEID") != null && !terminalHM.get("PAITEMTYPEID").toString().isEmpty()) {
                itemProfileMappings.add(new ItemProfileMapping(terminalHM));
            }
        }
        this.setItemProfileMappings(itemProfileMappings);

        return this;
    }

    public TerminalModel createTerminalModelNoMappings(Integer terminalId) throws Exception {
        DataManager dm = new DataManager();

        this.setId(terminalId);
        ArrayList<HashMap> terminalList = null;

        this.getSandBoxModeSetting();

        if ((MMHProperties.getAppSetting("site.application.offline").length() > 0) && (MMHProperties.getAppSetting("site.application.offline").compareToIgnoreCase("yes") == 0)) {
            //if the terminal is offline, we don't fetch all the Item Profile Mappings.  We don't need them, and the table QC_PAItemPOSItemMap doesn't exist in the local DB
            terminalList = dm.parameterizedExecuteQuery("data.posapi30.getOneTerminalByTerminalTypeOffline",
                    new Object[]{
                            this.getId()
                    },
                    PosAPIHelper.getLogFileName(terminalId),
                    true
            );
        } else {
            terminalList = dm.parameterizedExecuteQuery("data.posapi30.getOneTerminalByIdNoItemProfileMappings",
                    new Object[]{
                            this.getId()
                    },
                    PosAPIHelper.getLogFileName(terminalId),
                    true
            );
        }

        //populate this collection from an array list of hashmaps
        CommonAPI.checkIsNullOrEmptyList(terminalList, CommonAPI.PosModelType.TERMINAL, "", terminalId);
        HashMap modelDetailHM = terminalList.get(0);

        CommonAPI.checkIsNullOrEmptyObject(modelDetailHM.get("ID"), PosAPIHelper.getLogFileName(0));
        setModelProperties(modelDetailHM);

        return this;
    }

    private void getSandBoxModeSetting() {

        if ((MMHProperties.getAppSetting("site.application.apiSandboxMode").length() > 0) && (MMHProperties.getAppSetting("site.application.apiSandboxMode").compareToIgnoreCase("yes") == 0)) {
            this.setSandBoxMode(true);
        }
    }

    public BigDecimal logTransactionTime() {
        this.setEndTime(System.nanoTime());
        this.setTransactionDuration(this.getEndTime() - this.getStartTime());

        BigDecimal duration = new BigDecimal(this.getTransactionDuration());
        BigDecimal billion = new BigDecimal(1000000000);
        BigDecimal durationInSeconds = duration.divide(billion).setScale(4, RoundingMode.HALF_UP);
        Logger.logMessage("Transaction Duration: " + durationInSeconds + " seconds", PosAPIHelper.getLogFileName());
        return durationInSeconds;
    }

    /**
     * This logOperationDuration is designed to be used for Transactions
     *
     * @param transactionModel
     * @param conn
     * @param logFileName
     * @throws Exception
     */
    public void logOperationDuration(TransactionModel transactionModel, JDCConnection conn, String logFileName) throws Exception {

        Integer operationType = 2;
        Integer userId = null;

        BigDecimal transDuration = this.logTransactionTime(); //write the transaction duration to the logs

        if (transactionModel.getTransactionTypeEnum() != null) {
            switch (transactionModel.getTransactionTypeEnum()) {

                case SALE:
                    operationType = 2;
                    break;
                case VOID:
                    operationType = 3;
                    break;
                case REFUND:
                    operationType = 4;
                    break;
                case TRAINING:
                    operationType = 5;
                    break;
                case CANCEL:
                    operationType = 6;
                    break;
                case CASHIERSHIFTEND:
                    operationType = 7;
                    break;
                case NOSALE:
                    operationType = 8;
                    break;
                case BROWSERCLOSE:
                    operationType = 9;
                    break;
                case LOGIN:
                    operationType = 10;
                    break;
                case LOGOUT:
                    operationType = 11;
                    break;
                case ROA:
                    operationType = 12;
                    break;
                case PO:
                    operationType = 13;
                    break;
                case BATCHCLOSE:
                    operationType = 14;
                    break;
            }
        }

        if (transactionModel.getUserId() != null && !transactionModel.getUserId().toString().isEmpty()) {
            userId = transactionModel.getUserId();
        }

        logOperationDuration(operationType, transDuration, this.getId(), transactionModel.getId(), userId, conn, logFileName);
    }

    /**
     * This Operation Duration is abstracted so you could call it for Transactions, Accounts, etc.
     *
     * @param operationType
     * @param transDuration
     * @param terminalId
     * @param transactionId
     * @param userId
     * @param conn
     * @param logFileName
     * @throws Exception
     */
    private void logOperationDuration(Integer operationType, BigDecimal transDuration, Integer terminalId, Integer transactionId, Integer userId, JDCConnection conn, String logFileName) throws Exception {
        Integer result = 0;

        result = dm.parameterizedExecuteNonQuery("data.posapi30.InsertOperationDuration", new Object[]{
                operationType,
                transDuration,
                terminalId,
                transactionId,
                userId

        }, logFileName, conn);

        if (result == 0) {
            Logger.logMessage("Inserted 0 records.  Duration was less than time threshold.", logFileName, Logger.LEVEL.TRACE);
        } else if (result <= 0) {
            Logger.logMessage("Error Recording Operation Duration", logFileName, Logger.LEVEL.ERROR);
        }
    }

    /**
     * Get a TerminalModel from the database based on the Terminal name
     *
     * @param terminalName
     * @return
     * @throws Exception
     */
    public static TerminalModel getOneTerminalModelByName(String terminalName) throws Exception {
        DataManager dm = new DataManager();

        try {

            ArrayList terminalList = dm.parameterizedExecuteQuery("data.posapi30.getOneTerminalByName", new Object[]{terminalName}, true);

            if (terminalList.size() > 1) {
                throw new MissingDataException("Multiple Terminals found for Client Identifier");
            }

            //product list is null or empty, throw exception
            CommonAPI.checkIsNullOrEmptyList(terminalList, "Terminal Not Found", null);

            TerminalModel terminalModel = new TerminalModel((HashMap) terminalList.get(0));

            if (terminalModel == null) {
                throw new TerminalNotFoundException("Error getting Terminal from request");
            } else {
                return terminalModel;
            }
        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }
    }

    /**
     * Get a TerminalModel from the database based on the Terminal name
     *
     * @param terminalIdentifier
     * @return
     * @throws Exception
     */
    public static TerminalModel getOneTerminalModelByTerminalIdentifier(String terminalIdentifier) throws Exception {
        DataManager dm = new DataManager();

        try {

            ArrayList terminalList = dm.parameterizedExecuteQuery(
                    "data.posapi30.getOneTerminalByTerminalIdentifier",
                    new Object[]{terminalIdentifier}, PosAPIHelper.getLogFileName(), true);

            if (terminalList.size() > 1) {
                throw new MissingDataException("Multiple Terminals found for Client Identifier");
            }

            TerminalModel terminalModel = null;

            if (!terminalList.isEmpty()) {
                terminalModel = TerminalModel.createTerminalModel((HashMap) terminalList.get(0), true, true);
            } else {
                throw new MissingDataException("Invalid Client Identifier Submitted.");
            }

            return terminalModel;

        } catch (Exception ex) {
            Logger.logException(ex);
            throw ex;
        }
    }

    public static void initTerminalCache(TerminalModel terminalModel) throws Exception {
        if (PosAPIModelCache.useTerminalCache) {
            PosAPIModelCache.buildGlobalCache(terminalModel);
        }
    }

    /**
     * If there are special characters in the Terminal Model Name, make sure to strip them out before making the logfile name
     *
     * @param terminalLogFileName
     */
    private void cleanAndSetLogFileName(String terminalLogFileName) {

        if (!terminalLogFileName.isEmpty()) {
            //Define the characters that are not allowed in the log file name
            //replaceAll uses regex so the regex code for backslash needs to be passed in
            String[] badStrings = new String[]{"/", "\\\\", "%", "#", "*", "!", "@", "&", "^", "(", ")", "[", "]", "{", "}", "?", "|", "<", ">", ":"};

            for (String badString : badStrings) {
                if (terminalLogFileName.contains(badString)) {
                    terminalLogFileName = terminalLogFileName.replaceAll(badString, "_");
                }
            }
        }

        setTerminalLogFileName(terminalLogFileName);
    }

    public void validateTerminalIdParam(String terminalIdParam) throws Exception {
        if (!this.paramEqualsTerminalId(terminalIdParam)) {
            throw new MissingDataException("Invalid Terminal ID submitted");
        }
    }

    public boolean paramEqualsTerminalId(String terminalId) throws Exception {
        boolean isValid = true;

        try {
            if (!StringUtils.isNumeric(terminalId)) {
                return false;
            }

            Integer iTerminalId = Integer.parseInt(terminalId);
            if (!iTerminalId.equals(this.getId())) {
                isValid = false;
            }
        } catch (Exception e) {
            isValid = false;
        }

        return isValid;
    }

    public boolean autoRefreshDateIsValid(TerminalModel updatedTerminalModel) throws Exception {
        boolean isValid = false;

        if (this.getNextAutoRefreshDateTime() == null && updatedTerminalModel.getNextAutoRefreshDateTime() == null) {
            return false;
        } else if (this.getNextAutoRefreshDateTime().isEmpty() && updatedTerminalModel.getNextAutoRefreshDateTime() == null) {
            // Empty string and null are the same thing
            return false;
        } else if (this.getNextAutoRefreshDateTime() == null && updatedTerminalModel.getNextAutoRefreshDateTime().isEmpty()) {
            // Empty string and null are the same thing
            return false;
        }

        if (this.getNextAutoRefreshDateTime().equals(updatedTerminalModel.getNextAutoRefreshDateTime()) == false) {
            this.setNextAutoRefreshDateTime(updatedTerminalModel.getNextAutoRefreshDateTime());
        } else {
           return false; //the values equal each other, nothing to do
        }

        if (this.getNextAutoRefreshDateTime() == null) {
            isValid = true;
        } else if (this.getNextAutoRefreshDateTime().isEmpty()) {
            this.setNextAutoRefreshDateTime(null);
            isValid = true;
        } else { //the date was populated


            boolean dateIsValid = false;
            Date validDate = null;

            try {
                //example we are looking for: 5/25/2017 9:22:07
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                validDate = dateFormat.parse(this.getNextAutoRefreshDateTime());
                dateIsValid = true;
                isValid = true;

            } catch (Exception ex) {
                //throw new MissingDataException("Invalid Birthday Submitted");
            }

            try {
                //example we are looking for: 5/25/2017
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                validDate = dateFormat.parse(this.getNextAutoRefreshDateTime());
                dateIsValid = true;
                isValid = true;

            } catch (Exception ex) {

            }

            if (!dateIsValid) {
                throw new MissingDataException("Invalid Auto Refresh Date Submitted");
            }
        }

        return isValid;
    }

    public boolean dataWireIdIsValid(TerminalModel updatedTerminalModel) throws Exception {
        boolean isValid = false;

        if (updatedTerminalModel.getDatawireId() == null || updatedTerminalModel.getDatawireId().isEmpty())
        {
            isValid = false;
        }
        else if (this.getDatawireId() != null && this.getDatawireId().equals(updatedTerminalModel.getDatawireId()))
        {
            isValid = false; //Date Wire ID is already updated
        }
        else
        {
            this.setDatawireId(updatedTerminalModel.getDatawireId());
            isValid = true;
        }

        return isValid;
    }

    public void update(TerminalModel newTerminalModel) throws Exception {
        String logFileName = this.getTerminalLogFileName();
        Logger.logMessage("Saving Terminal. TerminalModel.update.", logFileName, Logger.LEVEL.DEBUG);

        boolean saveAutoRefreshDate = this.autoRefreshDateIsValid(newTerminalModel);
        boolean saveDataWireId = this.dataWireIdIsValid(newTerminalModel);

        Integer iTerminalId = 0;

        if (saveAutoRefreshDate && saveDataWireId) {

            iTerminalId = dm.parameterizedExecuteNonQuery("data.posapi30.updateTerminal", new Object[]{
                    this.getNextAutoRefreshDateTime(),
                    this.getDatawireId(),
                    this.getId()
            }, logFileName);

        } else if (saveAutoRefreshDate && !saveDataWireId) {
            iTerminalId = dm.parameterizedExecuteNonQuery("data.posapi30.updateTerminalAutoRefreshDate", new Object[]{
                    this.getNextAutoRefreshDateTime(),
                    this.getId()
            }, logFileName);
        } else if (!saveAutoRefreshDate && saveDataWireId) {
            iTerminalId = dm.parameterizedExecuteNonQuery("data.posapi30.updateTerminalDataWireId", new Object[]{
                    this.getDatawireId(),
                    this.getId()
            }, logFileName);
        }

        if (iTerminalId <= 0) {
            throw new MissingDataException("Terminal could not be updated in QC_Terminals", this.getId());
        }
    }

    //getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getId() {
        return Id;
    }

    //setter
    public void setId(Integer Id) {
        this.Id = Id;
    }

    @ApiModelProperty(value = "Name of the Terminal.", dataType = "String", required = false)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    //setter
    public void setName(String name) {
        this.name = name;
    }

    //getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getTypeId() {
        return typeId;
    }

    //setter
    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    //getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTypeName() {
        return typeName;
    }

    //setter
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    //getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getDefaultPurchaseCategoryId() {
        return defaultPurchaseCategoryId;
    }

    //setter
    public void setDefaultPurchaseCategoryId(Integer defaultPurchaseCategoryId) {
        this.defaultPurchaseCategoryId = defaultPurchaseCategoryId;
    }

    //@JsonIgnore
    public Integer getRevenueCenterId() {
        return revenueCenterId;
    }

    public void setRevenueCenterId(Integer revenueCenterId) {
        this.revenueCenterId = revenueCenterId;
    }

    @JsonIgnore
    public List<ItemProfileMapping> getItemProfileMappings() {
        return itemProfileMappings;
    }

    public void setItemProfileMappings(List<ItemProfileMapping> itemProfileMappings) {
        this.itemProfileMappings = itemProfileMappings;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptAcctInfoLine1() {
        return receiptAcctInfoLine1;
    }

    public void setReceiptAcctInfoLine1(String receiptAcctInfoLine1) {
        this.receiptAcctInfoLine1 = receiptAcctInfoLine1;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptAcctInfoLine2() {
        return receiptAcctInfoLine2;
    }

    public void setReceiptAcctInfoLine2(String receiptAcctInfoLine2) {
        this.receiptAcctInfoLine2 = receiptAcctInfoLine2;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptAcctInfoLine3() {
        return receiptAcctInfoLine3;
    }

    public void setReceiptAcctInfoLine3(String receiptAcctInfoLine3) {
        this.receiptAcctInfoLine3 = receiptAcctInfoLine3;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptAcctInfoLine4() {
        return receiptAcctInfoLine4;
    }

    public void setReceiptAcctInfoLine4(String receiptAcctInfoLine4) {
        this.receiptAcctInfoLine4 = receiptAcctInfoLine4;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptAcctInfoLine5() {
        return receiptAcctInfoLine5;
    }

    public void setReceiptAcctInfoLine5(String receiptAcctInfoLine5) {
        this.receiptAcctInfoLine5 = receiptAcctInfoLine5;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptHeader1() {
        return receiptHeader1;
    }

    public void setReceiptHeader1(String receiptHeader1) {
        this.receiptHeader1 = receiptHeader1;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptHeader2() {
        return receiptHeader2;
    }

    public void setReceiptHeader2(String receiptHeader2) {
        this.receiptHeader2 = receiptHeader2;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptHeader3() {
        return receiptHeader3;
    }

    public void setReceiptHeader3(String receiptHeader3) {
        this.receiptHeader3 = receiptHeader3;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptHeader4() {
        return receiptHeader4;
    }

    public void setReceiptHeader4(String receiptHeader4) {
        this.receiptHeader4 = receiptHeader4;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptHeader5() {
        return receiptHeader5;
    }

    public void setReceiptHeader5(String receiptHeader5) {
        this.receiptHeader5 = receiptHeader5;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptFooter1() {
        return receiptFooter1;
    }

    public void setReceiptFooter1(String receiptFooter1) {
        this.receiptFooter1 = receiptFooter1;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptFooter2() {
        return receiptFooter2;
    }

    public void setReceiptFooter2(String receiptFooter2) {
        this.receiptFooter2 = receiptFooter2;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptFooter3() {
        return receiptFooter3;
    }

    public void setReceiptFooter3(String receiptFooter3) {
        this.receiptFooter3 = receiptFooter3;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptFooter4() {
        return receiptFooter4;
    }

    public void setReceiptFooter4(String receiptFooter4) {
        this.receiptFooter4 = receiptFooter4;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptFooter5() {
        return receiptFooter5;
    }

    public void setReceiptFooter5(String receiptFooter5) {
        this.receiptFooter5 = receiptFooter5;
    }

    @JsonIgnore
    public String getTerminalModelName() {
        return terminalModelName;
    }

    public void setTerminalModelName(String terminalModelName) {
        this.terminalModelName = terminalModelName;
    }

    @JsonIgnore
    public String getTerminalLogFileName() {
        return terminalLogFileName;
    }

    public void setTerminalLogFileName(String terminalLogFileName) {
        this.terminalLogFileName = terminalLogFileName;
    }

    @JsonIgnore
    public LoginModel getLoginModel() {
        return loginModel;
    }

    public void setLoginModel(LoginModel loginModel) {
        this.loginModel = loginModel;
    }

    @JsonIgnore
    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    @JsonIgnore
    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    @JsonIgnore
    public long getTransactionDuration() {
        return transactionDuration;
    }

    public void setTransactionDuration(long transactionDuration) {
        this.transactionDuration = transactionDuration;
    }

    @JsonIgnore
    public Boolean isSandBoxMode() {
        return sandBoxMode;
    }

    public void setSandBoxMode(Boolean sandBoxMode) {
        this.sandBoxMode = sandBoxMode;
    }

    @JsonIgnore
    public Boolean getAllowQcCouponDiscountOffline() {
        return allowQcCouponDiscountOffline;
    }

    public void setAllowQcCouponDiscountOffline(Boolean allowQcCouponDiscountOffline) {
        this.allowQcCouponDiscountOffline = allowQcCouponDiscountOffline;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getNextAutoRefreshDateTime() {
        return nextAutoRefreshDateTime;
    }

    public void setNextAutoRefreshDateTime(String nextAutoRefreshDateTime) {
        this.nextAutoRefreshDateTime = nextAutoRefreshDateTime;
    }

    @JsonIgnore
    public Integer getNumSecondsSubmitTransactionTimeout() {
        return numSecondsSubmitTransactionTimeout;
    }

    public void setNumSecondsSubmitTransactionTimeout(Integer numSecondsSubmitTransactionTimeout) {
        this.numSecondsSubmitTransactionTimeout = numSecondsSubmitTransactionTimeout;
    }

    public boolean getDisableSmartCombos() {
        return disableSmartCombos;
    }

    public void setDisableSmartCombos(boolean disableSmartCombos) {
        this.disableSmartCombos = disableSmartCombos;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getTransactionNameLabel() {
        return transactionNameLabel;
    }

    public void setTransactionNameLabel(String transactionNameLabel) {
        this.transactionNameLabel = transactionNameLabel;
    }

    public Boolean getUseFundingTerminal() {
        return useFundingTerminal;
    }

    public void setUseFundingTerminal(Boolean useFundingTerminal) {
        this.useFundingTerminal = useFundingTerminal;
    }

    public Boolean getUseOnlineOrderingFunding() {
        return useOnlineOrderingFunding;
    }

    public void setUseOnlineOrderingFunding(Boolean useOnlineOrderingFunding) {
        this.useOnlineOrderingFunding = useOnlineOrderingFunding;
    }

    @JsonIgnore
    public RevenueCenterModel getRevenueCenter() {
        return revenueCenter;
    }

    public void setRevenueCenter(RevenueCenterModel revenueCenter) {
        this.revenueCenter = revenueCenter;
    }

    @JsonIgnore
    public boolean isAutoApplyQCCouponDiscounts() {
        return autoApplyQCCouponDiscounts;
    }

    public void setAutoApplyQCCouponDiscounts(boolean autoApplyQCCouponDiscounts) {
        this.autoApplyQCCouponDiscounts = autoApplyQCCouponDiscounts;
    }

    public Integer getPASurchargeID() {
        return paSurchargeID;
    }

    public void setPASurchargeID(Integer paSurchargeID) {
        this.paSurchargeID = paSurchargeID;
    }

    public Integer getTerminalModelId() {
        return terminalModelId;
    }

    public void setTerminalModelId(Integer terminalModelId) {
        this.terminalModelId = terminalModelId;
    }

    public boolean isAllowOfflineOpenTxns() {
        return allowOfflineOpenTxns;
    }

    public void setAllowOfflineOpenTxns(boolean allowOfflineOpenTxns) {
        this.allowOfflineOpenTxns = allowOfflineOpenTxns;
    }

    @JsonIgnore
    public boolean isChargeAtTimeOfPurchase() {
        return chargeAtTimeOfPurchase;
    }

    public void setChargeAtTimeOfPurchase(boolean chargeAtTimeOfPurchase) {
        this.chargeAtTimeOfPurchase = chargeAtTimeOfPurchase;
    }

    @JsonIgnore
    public boolean checkAllowOfflineVouchers() { return allowOfflineVouchers; }
    public void setAllowOfflineVouchers(boolean val) { this.allowOfflineVouchers = val; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getDatawireId() {
        return datawireId;
    }

    public void setDatawireId(String datawireId) {
        this.datawireId = datawireId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPaymentProcessorStoreNum() {
        return paymentProcessorStoreNum;
    }

    public void setPaymentProcessorStoreNum(String paymentProcessorStoreNum) {
        this.paymentProcessorStoreNum = paymentProcessorStoreNum;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPaymentProcessorTerminalNum() {
        return paymentProcessorTerminalNum;
    }

    public void setPaymentProcessorTerminalNum(String paymentProcessorTerminalNum) {
        this.paymentProcessorTerminalNum = paymentProcessorTerminalNum;
    }

    @JsonIgnore
    public boolean isAutoApplyQCDiscountOnSaleRequest() {
        return autoApplyQCDiscountOnSaleRequest;
    }

    public void setAutoApplyQCDiscountOnSaleRequest(boolean autoApplyQCDiscountOnSaleRequest) {
        this.autoApplyQCDiscountOnSaleRequest = autoApplyQCDiscountOnSaleRequest;
    }
}
