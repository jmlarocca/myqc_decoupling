package com.mmhayes.common.account.models;

//mmhayes dependencies

import com.mmhayes.common.account.collections.*;
import com.mmhayes.common.api.PosApi30.TransactionBuilder;
import com.mmhayes.common.conn_pool.JDCConnection;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.login.LoginModel;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.collections.QcDiscountCollection;
import com.mmhayes.common.transaction.collections.TenderCollection;
import com.mmhayes.common.transaction.models.*;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.loyalty.models.LoyaltyAccountModel;
import com.mmhayes.common.user.models.UserModel;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.MMHProperties;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

//API dependencies
import com.mmhayes.common.api.errorhandling.exceptions.*;
import com.mmhayes.common.api.CommonAPI;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//other dependencies
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 Last Updated (automatically updated by SVN)
 $Author: mjbatko $: Author of last commit
 $Date: 2021-06-28 08:23:25 -0400 (Mon, 28 Jun 2021) $: Date of last commit
 $Rev: 14230 $: Revision of last commit
*/
@JsonPropertyOrder({"id", "name", "badge", "badgeAlias", "badgeAssignmentId", "number", "status", "accountType", "accountTypeId", "contact", "address1",
        "address2", "city", "state", "zipCode", "phone", "email", "available", "singleChargeLimit", "terminalGroupBalance",
        "terminalGroupLimit", "globalBalance", "globalLimit", "eligiblePayments", "receiptBalanceLabel", "receiptBalance", "receiptBalanceTypeId", "payrollGroupingId",
        "birthday", "spendingProfileId", "autoCloudInvite", "mobilePhone", "notes", "terminalMappedToSpendingProfile",
        "restrictionProfileId", "redacted", "qcDiscountsAvailable", "qcDiscountsApplied", "accountGroup"})
public class AccountModel {

    //region private fields

    private Integer id = null;
    private Integer badgeAssignmentId = null;
    private Integer eligiblePayments = null; //splits

    private String name = null;
    private String email = null;
    private String badge = null;
    private String number = null;
    private String status = null; //"Active", "Inactive", etc.
    private String accountType = null; //Payroll Deduction, Prepay Employee, Billing Account, Gift Card
    private String address1 = null;
    private String address2 = null;
    private String city = null;
    private String state = null;
    private String zipCode = null;
    private String phone = null;
    private String contact = null;
    private String receiptBalanceLabel = null;
    private String birthday = null;
    private String mobilePhone = null;
    private String badgeAlias = null;
    private String employeeLowBalanceThreshold = null;

    //Balance Information
    private BigDecimal singleChargeLimit = null;
    private BigDecimal terminalGroupBalance = null;
    private BigDecimal terminalGroupLimit = null;
    private BigDecimal globalBalance = null;
    private BigDecimal globalLimit = null;
    private BigDecimal available = null;
    private BigDecimal receiptBalance = null;

    private Integer accountTypeId = null;
    private Integer payrollGroupingId = null;
    private Integer receiptBalanceTypeId = null;
    private Integer spendingProfileId = null;
    private Integer spendingProfileMappedForTerminal = null;
    private Integer restrictionProfileId = null;

    private Boolean autoCloudInvite = null;
    private Boolean isNewAccount = false;
    private Boolean nonNoteUpdateRequested = false;
    private Boolean tosCheckAlreadyDone = false;
    private Boolean terminalMappedToSpendingProfile = false;
    private Boolean redacted = false;

    private TerminalModel terminal = null;
    private UserModel userModel = null;
    private AccountModel originalAccountModel = null;
    private HashMap changedFieldsHM = null;
    private LoginModel loginModel = null;
    private AccountGroupModel accountGroup = null;

    private List<AccountNoteModel> notes = new ArrayList<>();
    private List<QcDiscountModel> qcDiscountsAvailable = new ArrayList<>();
    private List<QcDiscountLineItemModel> qcDiscountsApplied = new ArrayList<>();

    private List<SpendingProfileModel> spendingProfileModelList = new ArrayList<>();
    private List<AccountGroupModel> accountGroupModelList = new ArrayList<>(); //need to rename this, this is not a list of the account's account group models. We believe this list is used to determine the account group name for logging for customer tracking. Used in AccountModel.getAccountGroupName()

    //Used by Third Parties to get Account Balances
    private List<TerminalGroupBalanceModel> terminalGroupBalances = new ArrayList<>();

    private static DataManager dataManager = new DataManager();

    //endregion

    //default constructor
    public AccountModel() {

    }

    //basic account constructor by employeeID
    public AccountModel(Integer accountID) {
        this.setId(accountID);

        //get account info
        ArrayList<HashMap> accountInfoList = dataManager.parameterizedExecuteQuery("data.posapi30.getSingleAccountInfo",
            new Object[]{
                this.getId()
            },
            true
        );

        if ( accountInfoList == null || accountInfoList.size() == 0 ) {
            //error out
            return;
        }

        HashMap accountInfo = accountInfoList.get(0);

        this.setName(CommonAPI.convertModelDetailToString(accountInfo.get("NAME")));
        this.setNumber(CommonAPI.convertModelDetailToString(accountInfo.get("EMPLOYEENUMBER")));
        this.setBadge(CommonAPI.convertModelDetailToString(accountInfo.get("BADGE")));
        this.setStatus(CommonAPI.convertModelDetailToString(accountInfo.get("ACCOUNTSTATUS")));
        this.setAccountTypeId(CommonAPI.convertModelDetailToInteger(accountInfo.get("ACCOUNTTYPEID")));
        this.setPhone(CommonAPI.convertModelDetailToString(accountInfo.get("PHONE")));
        this.setMobilePhone(CommonAPI.convertModelDetailToString(accountInfo.get("MOBILEPHONE")));
        this.setEmail(CommonAPI.convertModelDetailToString(accountInfo.get("EMAILADDRESS1")));
        this.setEmployeeLowBalanceThreshold(CommonAPI.convertModelDetailToString(accountInfo.get("EMPLOYEELOWBALANCETHRESHOLD")));
    }

    //Constructor- takes in a Hashmap
    private AccountModel(HashMap AccountHM, TerminalModel terminalModel) throws Exception {
        setModelProperties(AccountHM, terminalModel);
    }

    public AccountModel(LoyaltyAccountModel loyaltyAccountModel, TerminalModel terminalModel) throws Exception {
        this.setId(loyaltyAccountModel.getId());
        this.setBadgeAlias(loyaltyAccountModel.getBadgeAlias());
        this.setBadge(loyaltyAccountModel.getBadge());
        this.setBadgeAssignmentId(loyaltyAccountModel.getBadgeAssignmentId());
        this.setAccountTypeId(loyaltyAccountModel.getAccountTypeId());
        this.setAccountType(loyaltyAccountModel.getAccountType());

        this.setEmail(loyaltyAccountModel.getEmail());

        this.setName(loyaltyAccountModel.getName());
        this.setNumber(loyaltyAccountModel.getNumber());
        this.setStatus(loyaltyAccountModel.getStatus());
        this.setEligiblePayments(loyaltyAccountModel.getEligiblePayments());

        this.setSingleChargeLimit(loyaltyAccountModel.getSingleChargeLimit());
        this.setAvailable(loyaltyAccountModel.getAvailable());

        this.setTerminalGroupBalance(loyaltyAccountModel.getTerminalGroupBalance());
        this.setTerminalGroupLimit(loyaltyAccountModel.getTerminalGroupLimit());

        this.setGlobalBalance(loyaltyAccountModel.getGlobalBalance());
        this.setGlobalLimit(loyaltyAccountModel.getGlobalLimit());

        this.setAddress1(loyaltyAccountModel.getAddress1());
        this.setAddress2(loyaltyAccountModel.getAddress2());
        this.setCity(loyaltyAccountModel.getCity());
        this.setState(loyaltyAccountModel.getState());
        this.setZipCode(loyaltyAccountModel.getZipCode());
        this.setPhone(loyaltyAccountModel.getPhone());
        this.setContact(loyaltyAccountModel.getContact());

        this.setPayrollGroupingId(loyaltyAccountModel.getPayrollGroupingId());
        this.setReceiptBalanceTypeId(loyaltyAccountModel.getReceiptBalanceTypeId());
        this.setReceiptBalanceLabel(loyaltyAccountModel.getReceiptBalanceLabel());

        this.setBirthday(loyaltyAccountModel.getBirthday());
        this.setSpendingProfileId(loyaltyAccountModel.getSpendingProfileId());
        this.setSpendingProfileMappedForTerminal(loyaltyAccountModel.getSpendingProfileMappedForTerminal());
        this.setAutoCloudInvite(loyaltyAccountModel.getAutoCloudInvite());
        this.setMobilePhone(loyaltyAccountModel.getMobilePhone());
        this.setRedacted(loyaltyAccountModel.isRedacted());

        this.initAvailable();
        this.initReceiptBalance();
        this.setNotes(loyaltyAccountModel.getNotes());

        this.setAccountGroup(loyaltyAccountModel.getAccountGroup());
    }

    private AccountModel mapAccountModel(ArrayList<HashMap> accountList, TerminalModel terminalModel, AccountQueryParams accountQueryParams) throws Exception {
        //populate this collection from an array list of hashmaps
        CommonAPI.checkIsNullOrEmptyList(accountList, CommonAPI.PosModelType.ACCOUNT, "Account Not Found", terminalModel.getId());
        HashMap modelDetailHM = getFirstRecord(accountList);
        CommonAPI.checkIsNullOrEmptyObject(modelDetailHM.get("ID"), "Account Id cannot be found", terminalModel.getId());

        if (modelDetailHM.get("ACCOUNTSTATUS") != null && !modelDetailHM.get("ACCOUNTSTATUS").toString().isEmpty()) {
            String accountStatus = modelDetailHM.get("ACCOUNTSTATUS").toString().toUpperCase();

            if (!accountQueryParams.isIgnoreAccountStatus()) {
                switch (accountStatus) {
                    case "I": //Inactive
                        throw new AccountInactiveException("Account is Inactive", terminalModel.getId());

                    case "S": //Suspended
                        throw new AccountInactiveException("Account is Suspended", terminalModel.getId());

                    case "P": //Pending Inactivation
                        throw new AccountInactiveException("Account is Pending Inactivation", terminalModel.getId());

                    case "F": //Pending Inactivation
                        throw new AccountInactiveException("Account is Frozen", terminalModel.getId());

                    case "W": //Waiting For Signup
                        throw new AccountInactiveException("Account is Waiting For Signup", terminalModel.getId());
                }
            }
        }

        //if ignoreAccountStatus=yes, and the account is redacted, do not return it
        if (modelDetailHM.get("REDACTED") != null && !modelDetailHM.get("REDACTED").toString().isEmpty()) {
            Boolean isRedacted = CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("REDACTED").toString());
            if (!accountQueryParams.isIgnoreRedacted()) {
                if (isRedacted) {
                    throw new AccountNotFoundException("Account is Redacted");
                }
            }
        }

        setModelProperties(modelDetailHM, terminalModel);
        return this;
    }

    /**
     * Try and grab the first active record from a list of accounts.
     * If no active records exist, just grab the first one
     * @param accountList
     * @return
     */
    private HashMap getFirstRecord(ArrayList<HashMap> accountList){
        HashMap modelDetailHM = null;

        //First, check for an active account
        for (HashMap accountHM : accountList) {
            if (accountHM.get("ACCOUNTSTATUS") != null
                    && !accountHM.get("ACCOUNTSTATUS").toString().isEmpty()
                    && accountHM.get("ACCOUNTSTATUS").toString().equals("A")) {

                modelDetailHM = accountHM;
                break;
            }
        }

        //If none found, grab the first record
        if (modelDetailHM == null) {
            modelDetailHM = accountList.get(0); //grab the first record
        }

        return modelDetailHM;
    }

    //setter for all of this model's properties
    public AccountModel setModelProperties(HashMap modelDetailHM, TerminalModel terminalModel) throws Exception {
        this.setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        this.setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        this.setEmail(CommonAPI.convertModelDetailToString(modelDetailHM.get("EMAILADDRESS1")));
        this.setBadge(CommonAPI.convertModelDetailToString(modelDetailHM.get("BADGE")));
        this.setNumber(CommonAPI.convertModelDetailToString(modelDetailHM.get("NUMBER")));
        this.setStatus(CommonAPI.convertModelDetailToString(modelDetailHM.get("ACCOUNTSTATUSNAME")));
        this.setAccountTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ACCOUNTTYPEID")));
        this.setAccountType(CommonAPI.convertModelDetailToString(modelDetailHM.get("ACCOUNTTYPENAME")));

        this.setTerminalGroupBalance(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("STOREBALANCE")));
        this.setGlobalBalance(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("GLOBALBALANCE")));
        this.setEligiblePayments(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("NUMPAYMENTS")));

        this.setSingleChargeLimit(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("SINGLECHARGELIMIT")));
        this.setGlobalLimit(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("GLOBALLIMIT")));
        this.setTerminalGroupLimit(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("STORELIMIT")));

        this.setAddress1(CommonAPI.convertModelDetailToString(modelDetailHM.get("ADDRESS1")));
        this.setAddress2(CommonAPI.convertModelDetailToString(modelDetailHM.get("ADDRESS2")));
        this.setCity(CommonAPI.convertModelDetailToString(modelDetailHM.get("CITY")));
        this.setState(CommonAPI.convertModelDetailToString(modelDetailHM.get("STATE")));
        this.setZipCode(CommonAPI.convertModelDetailToString(modelDetailHM.get("ZIPCODE")));
        this.setPhone(CommonAPI.convertModelDetailToString(modelDetailHM.get("PHONE")));
        this.setContact(CommonAPI.convertModelDetailToString(modelDetailHM.get("CONTACT")));
        this.setBadgeAlias(CommonAPI.convertModelDetailToString(modelDetailHM.get("ALTBADGENUMBER")));
        this.setBadgeAssignmentId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("BADGEASSIGNMENTID")));

        this.setPayrollGroupingId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAYROLLGROUPINGID")));
        this.setReceiptBalanceTypeId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("RECEIPTBALANCETYPEID")));
        this.setReceiptBalanceLabel(CommonAPI.convertModelDetailToString(modelDetailHM.get("RECEIPTBALANCEPROMPT")));
        this.setBirthday(CommonAPI.convertModelDetailToString(modelDetailHM.get("BIRTHDAY")));
        this.setSpendingProfileId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SPENDINGPROFILEID")));
        this.setSpendingProfileMappedForTerminal(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SPENDINGPROFILEDETAILID")));  //will return a the spending profile if mapped
        this.setAutoCloudInvite(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("AUTOCLOUDINVITE")));
        this.setMobilePhone(CommonAPI.convertModelDetailToString(modelDetailHM.get("MOBILEPHONE")));
        this.setRestrictionProfileId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("RESTRICTIONPROFILEID")));
        this.setRedacted(CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("REDACTED"), false));

        this.initAvailable();
        this.initReceiptBalance();
        this.initAccountNote(terminalModel);
        this.verifyAccountAccess(terminalModel);

        if (terminalModel == null || terminalModel.getTypeId() == null){
          //We don't know what terminal type we have.
        } else if (PosAPIHelper.checkPosType(terminalModel.getTypeId()) == PosAPIHelper.PosType.THIRD_PARTY){
            this.setQcDiscountsAvailable(QcDiscountCollection.getAvailableQcDiscounts(this.getId(), BigDecimal.ZERO, terminalModel, true).getCollection());
        } else {
            this.buildAccountGroupModel(terminalModel);
        }

        return this;
    }

    //get employee information and account data by Employee Id and Terminal Id
    //Splits are calculated in this query
    public static AccountModel getAccountModelByIdForTender(TerminalModel terminalModel, BigDecimal tenderAmount, Integer employeeId, AccountQueryParams accountQueryParams) throws Exception {
        DataManager dm = new DataManager();
        AccountModel accountModel = new AccountModel();
        ArrayList<HashMap> accountList = null;

        if ((MMHProperties.getAppSetting("site.application.offline").length() > 0) && (MMHProperties.getAppSetting("site.application.offline").compareToIgnoreCase("yes") == 0)) {

            //get all models in an array list, QCPos is offline
            accountList = dm.parameterizedExecuteQuery("data.posapi30.PersonTerminalLookupByIdOffline",
                    new Object[]{
                            employeeId,
                            terminalModel.getId(),
                            tenderAmount,
                            accountQueryParams.getBadgeAssignmentId()
                    },
                    PosAPIHelper.getLogFileName(terminalModel.getId()), true
            );

        } else {
            //get all models in an array list
            accountList = dm.parameterizedExecuteQuery("data.posapi30.PersonTerminalLookupByIdBP",
                    new Object[]{
                            employeeId,
                            terminalModel.getId(),
                            tenderAmount,
                            accountQueryParams.getBadgeAssignmentId()
                    },
                    PosAPIHelper.getLogFileName(terminalModel.getId()), true
            );
        }

        accountModel = accountModel.mapAccountModel(accountList, terminalModel, accountQueryParams);

        //populate this collection from an array list of hashmaps
        return accountModel;
    }

    //get employee information and account data by Employee Id and Terminal Id
    //Splits are calculated in this query
    public static AccountModel getAccountModelByNumberForTender(TerminalModel terminalModel, BigDecimal tenderAmount, String employeeNumber, AccountQueryParams accountQueryParams) throws Exception {
        DataManager dm = new DataManager();
        AccountModel accountModel = new AccountModel();
        ArrayList<HashMap> accountList = null;

        if ((MMHProperties.getAppSetting("site.application.offline").length() > 0) && (MMHProperties.getAppSetting("site.application.offline").compareToIgnoreCase("yes") == 0)) {
            //get all models in an array list, QCPos is offline
            accountList = dm.parameterizedExecuteQuery("data.posapi30.PersonTerminalLookupByNumberOffline",
                    new Object[]{
                            employeeNumber,
                            terminalModel.getId(),
                            tenderAmount
                    },
                    PosAPIHelper.getLogFileName(terminalModel.getId()), true
            );

        } else {
            //get all models in an array list
            accountList = dm.parameterizedExecuteQuery("data.posapi30.PersonTerminalLookupByNumberBP",
                    new Object[]{
                            employeeNumber,
                            terminalModel.getId(),
                            tenderAmount
                    },
                    PosAPIHelper.getLogFileName(terminalModel.getId()), true
            );
        }

        accountModel = accountModel.mapAccountModel(accountList, terminalModel, accountQueryParams);

        //populate this collection from an array list of hashmaps
        return accountModel;
    }

    //get employee information and account data by Employee Id and Terminal Id
    //Splits are calculated in this query
    public static AccountModel getAccountModelByBadgeForTender(TerminalModel terminalModel, BigDecimal tenderAmount, String employeeBadge, AccountQueryParams accountQueryParams) throws Exception {
        DataManager dm = new DataManager();
        AccountModel accountModel = new AccountModel();
        ArrayList<HashMap> accountList = null;

        //check for sandbox mode
        if (terminalModel.isSandBoxMode()) {
            sandboxModeCheck(employeeBadge);
        }

        if ((MMHProperties.getAppSetting("site.application.offline").length() > 0) && (MMHProperties.getAppSetting("site.application.offline").compareToIgnoreCase("yes") == 0)) {
            //get all models in an array list, QCPos is offline
            accountList = dm.parameterizedExecuteQuery("data.posapi30.PersonTerminalLookupByBadgeOffline",
                    new Object[]{
                            employeeBadge,
                            terminalModel.getId(),
                            tenderAmount,
                            accountQueryParams.isIgnoreAccountStatus() ? 1 : 0
                    },
                    PosAPIHelper.getLogFileName(terminalModel.getId()), true
            );
        } else {
            //get all models in an array list
            accountList = dm.parameterizedExecuteQuery("data.posapi30.PersonTerminalLookupByBadgeBP",
                    new Object[]{
                            employeeBadge,
                            terminalModel.getId(),
                            tenderAmount,
                            accountQueryParams.isIgnoreAccountStatus() ? 1 : 0
                    },
                    PosAPIHelper.getLogFileName(terminalModel.getId()), true
            );
        }

        accountModel = accountModel.mapAccountModel(accountList, terminalModel, accountQueryParams);

        //populate this collection from an array list of hashmaps
        return accountModel;
    }

    public static AccountModel createNewAccount(HashMap accountHM, TerminalModel terminalModel) throws Exception {
        return new AccountModel(accountHM, terminalModel);
    }

    public static AccountModel createNewAccount(LoyaltyAccountModel loyaltyAccountModel, TerminalModel terminalModel) throws Exception {
        return new AccountModel(loyaltyAccountModel, terminalModel);
    }

    public static AccountModel getAccountModelById(Integer accountId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();
        AccountModel accountModel = new AccountModel();
        ArrayList<HashMap> accountList = null;

        //get all models in an array list, QCPos is offline
        accountList = dm.parameterizedExecuteQuery("data.posapi30.getOneAccountById",
                new Object[]{
                        accountId,
                },
                PosAPIHelper.getLogFileName(terminalModel.getId()), true
        );

        //If no Account Info was found, error out
        CommonAPI.checkIsNullOrEmptyList(accountList, CommonAPI.PosModelType.ACCOUNT, "Account Not Found", terminalModel.getId());
        accountModel = accountModel.setModelProperties(accountList.get(0), terminalModel);

        //populate this collection from an array list of hashmaps
        return accountModel;
    }

    public static AccountModel getAccountModelByBadge(String badgeNumber, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();
        AccountModel accountModel = new AccountModel();
        ArrayList<HashMap> accountList = null;

        //get all models in an array list, QCPos is offline
        accountList = dm.parameterizedExecuteQuery("data.posapi30.getOneAccountByBadge",
                new Object[]{
                        badgeNumber,
                },
                PosAPIHelper.getLogFileName(terminalModel.getId()), true
        );

        //If no Account Info was found, error out
        CommonAPI.checkIsNullOrEmptyList(accountList, CommonAPI.PosModelType.ACCOUNT, "Account Not Found", terminalModel.getId());
        accountModel = accountModel.setModelProperties(accountList.get(0), terminalModel);

        //populate this collection from an array list of hashmaps
        return accountModel;
    }

    public static AccountModel getAccountModelByDynamicBadge(String badgeNumber, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();
        AccountModel accountModel = new AccountModel();
        ArrayList<HashMap> accountList = null;

        //get all models in an array list, QCPos is offline
        accountList = dm.parameterizedExecuteQuery("data.kiosk.getOneAccountByDynamicBadge",
                new Object[]{
                        badgeNumber,
                },
                PosAPIHelper.getLogFileName(terminalModel.getId()), true
        );

        //If no Account Info was found, error out
        CommonAPI.checkIsNullOrEmptyList(accountList, CommonAPI.PosModelType.ACCOUNT, "Account Not Found", terminalModel.getId());
        accountModel = accountModel.setModelProperties(accountList.get(0), terminalModel);

        //populate this collection from an array list of hashmaps
        return accountModel;
    }

    private static void sandboxModeCheck(String employeeBadge) throws Exception {

        switch (employeeBadge) {
            case "10009999": //E0000	500	Invalid Data.
                throw new Exception();
            case "50009999": //E5000	500	Invalid Data.	This exception means the application was expecting something and it was not there. Likely a problem with configuration or data.
                throw new MissingDataException("Invalid Data.");
            case "70009999": //E7000	500	Transaction Not Found	This Exception means that the referenced Transaction was not found.
                throw new TransactionNotFoundException();
            case "70019999": //E7001	500	Transaction exceeds Individual Transaction Limit.	This Exception means that the referenced Transaction exceeds Individual Transaction limit.
                throw new TransactionIndividualLimitException();
            case "70029999": //E7002	500	Transaction exceeds Store Limit.	This Exception means that the referenced Transaction exceeds Store Transaction limit.
                throw new TransactionStoreLimitException();
            case "70039999": //E7003	500	Transaction exceeds Global Limit.	This Exception means that the referenced Transaction exceeds the Global Transaction limit.
                throw new TransactionGlobalLimitException();
            case "70049999": //E7004	500	Duplicate Transaction	This Exception means that the submitted transaction is a duplicate.
                throw new DuplicateTransactionException();
            case "71009999": //E7100	500	Account Not Found	This Exception means that the referenced Account was not found in the database.
                throw new AccountNotFoundException();
            case "71019999": //E7101	500	Account Inactive	This Exception means that the referenced Account was found to be inactive.
                throw new AccountInactiveException();
            case "72009999": //E7200	500	Discount Not Found	This Exception means that the referenced Discount was not found in the request.
                throw new DiscountNotFoundException();
            case "73009999": //E7300	500	Loyalty Program Not Found	This Exception means that the referenced Loyalty Program was not found in the request.
                throw new LoyaltyProgramNotFoundException();
            case "73019999": //E7301	500	Loyalty Reward Program Not Found	This Exception means that the referenced Loyalty Reward Program was not found in the request.
                throw new LoyaltyRewardNotFoundException();
            case "74009999": //E7400	500	Product Not Found	This Exception means that the referenced Product was not found in the request.
                throw new ProductNotFoundException();
            case "75009999":  //E7500	500	Terminal Not Found	This Exception means that the referenced Terminal was not found in the request.
                throw new TerminalNotFoundException();
            case "76009999": //E7600	500	Tender Not Found	This Exception means that the referenced Tender was not found in the request.
                throw new TenderNotFoundException();
            case "76019999": //E7601	500	Payment Method Type Not Found	This Exception means that the referenced Payment Method Type was not found in the request.
                throw new PaymentMethodTypeNotFoundException();
            case "76020000": //E7005	500	Transaction Timeout.	This Exception means that the referenced Payment Method Type was not found in the request.
            case "76100000":
                Thread.sleep(1000 * 60);
                throw new TransactionTimeoutException();
        }
    }

    //region Validation Methods

    /**
     * Validate the AccountID in the URL vs. the AccountID in the model
     *
     * @param accountId
     * @throws Exception
     */
    public void validateAccountId(Integer accountId) throws Exception {
        if (accountId != null && !accountId.toString().isEmpty()) {
            //If the Id on the model is missing, fill it in
            if (this.getId() == null || this.getId().toString().isEmpty()) {
                this.setId(accountId);
            }

            //If the Id on the model is present, compare the 2 ID's.  If they are different error out
            if (this.getId() != null && !this.getId().toString().isEmpty()) {

                if (!this.getId().equals(accountId)) {
                    throw new MissingDataException("Invalid Account ID submitted");
                }
            }
        }
    }

    /**
     * Validate the Account before save
     */
    public void validateForUpdate() throws Exception {

        if (this.checkForOfflineMode()) {
            throw new MissingDataException("Account changes not allowed in offline mode");
        }

        //validate the Account ID
        if (this.getId() == null || this.getId().toString().isEmpty()) {
            throw new MissingDataException("Missing Account ID");
        }

        UserModel userModel = UserModel.getOneUserModelById(this.getTerminal().getLoginModel().getUserId(), this.getTerminal());
        this.setUserModel(userModel);

        /*
        B-06076: remove permissions for now
        //validate the User ID can Edit
        if (!userModel.getCanViewEdit()) {
            throw new MissingDataException("Invalid rights for updating Account");
        }*/

        //Fetch original from the database
        AccountModel originalAccountModel = AccountModel.getAccountModelByIdForTender(this.getTerminal(), BigDecimal.ZERO, this.getId(), new AccountQueryParams(true));
        this.setOriginalAccountModel(originalAccountModel);
        this.getOriginalAccountModel().setUserModel(userModel); //user model is used for validation
        this.getOriginalAccountModel().setTerminal(this.getTerminal());

        this.validateAccountStatus(originalAccountModel, userModel);
        this.validateFields(userModel); //check date, badge, number
        this.checkTosForChangedAccountGroup(); //if just the Account Group was changed
        this.checkForChangedFields();
        this.setFieldsOnOriginalForUpdate();
    }

    public void validateForInsert() throws Exception {

        if (this.checkForOfflineMode()) {
            throw new MissingDataException("Account changes not allowed in offline mode");
        }

        //if the ID is populated, throw an error
        if (this.getId() != null
                && !this.getId().toString().isEmpty() && !this.getId().equals(-1)) {
            throw new MissingDataException("Invalid Account ID Submitted");
        }
        this.setId(-1);

        UserModel userModel = UserModel.getOneUserModelById(this.getTerminal().getLoginModel().getUserId(), this.getTerminal());
        this.setUserModel(userModel);

        this.validateAccountStatus(userModel);
        this.validateFields(userModel);
        this.checkForChangedFields();
        this.setFieldsForInsert();

        //In the Globals, is the Auto Cloud Invite set to true
        Boolean autoCloudInviteInGlobals = AccountModel.autoCloudInviteInGlobals(this.getTerminal());
        this.setAutoCloudInvite(autoCloudInviteInGlobals);
    }

    /**
     * Used by the Update
     *
     * @param originalAccountModel
     * @param userModel
     * @throws Exception
     */
    private void validateAccountStatus(AccountModel originalAccountModel, UserModel userModel) throws Exception {

        PosAPIHelper.EmployeeStatus originalEmployeeStatus = PosAPIHelper.EmployeeStatus.convertStringNameToEnum(originalAccountModel.getStatus());

        //Account Notes are only valid for these Statuses: Waiting, Suspended, Frozen, Pending
        if (this.getStatus() != null && !this.getStatus().isEmpty()) {

            PosAPIHelper.EmployeeStatus updatedEmployeeStatus = PosAPIHelper.EmployeeStatus.convertStringNameToEnum(this.getStatus());

            switch (originalEmployeeStatus) {
                case ACTIVE:
                    switch (updatedEmployeeStatus) {
                        case INACTIVE:
                            //this is the only combination we allow
                            break;
                        default:
                            throw new MissingDataException("Invalid Account Status submitted");
                            //break;
                    }
                    break;
                case INACTIVE:
                    switch (updatedEmployeeStatus) {
                        case ACTIVE:
                        case WAITING_FOR_SIGNUP:

                            //set the account status on the original
                            //this.getOriginalAccountModel().setStatus(this.getStatus());

                            //do a license check for a new account
                            List<AccountModel> accountList = new ArrayList<>();
                            accountList.add(this.getOriginalAccountModel());

                            Boolean hasEnoughLicenses = AccountCollection.checkAccountLicenses(accountList, this.getTerminal());
                            if (!hasEnoughLicenses) {
                                throw new MissingDataException("No Licenses exist for new Account");
                            }

                           /* //check the view or edit Account User
                            B-06076: remove permissions for now
                            if (!userModel.getCanViewEdit()) {
                                Logger.logMessage("User doesn't have rights to view or edit Account", Logger.LEVEL.TRACE);
                                throw new MissingDataException("Invalid rights for viewing or editing Account");
                            }*/

                            //If the TOS needs to be signed, set the Account Status to Waiting
                            Boolean needsTermsOfService = AccountModel.checkOneAccountForTOS(this, this.getTerminal());
                            if (needsTermsOfService) {
                                Logger.logMessage("Account needs to sign Terms Of Service.  Status will be changed to Waiting for Signup", Logger.LEVEL.TRACE);
                                this.setStatus(PosAPIHelper.EmployeeStatus.WAITING_FOR_SIGNUP.toStringName());
                            }
                            this.setTosCheckAlreadyDone(true);

                            break;
                        default:
                            throw new MissingDataException("Invalid Account Status submitted");
                            //break;

                    }
                    break;

                case WAITING_FOR_SIGNUP:
                case SUSPENDED:
                case FROZEN:
                case PENDING_INACTIVATION:
                    switch (updatedEmployeeStatus) {
                        case ACTIVE:
                            //this is allowed
                            break;
                        default:
                            if (!originalEmployeeStatus.equals(updatedEmployeeStatus)) {
                                throw new MissingDataException("Invalid Account Status submitted");
                            }
                    }
            }
        }
    }

    /**
     * Used by Account Creation
     *
     * @param userModel
     * @throws Exception
     */
    private void validateAccountStatus(UserModel userModel) throws Exception {

        //Account Notes are only valid for these Statuses: Waiting, Suspended, Frozen, Pending
        if (this.getStatus() != null && !this.getStatus().isEmpty()) {

            PosAPIHelper.EmployeeStatus newEmployeeStatus = PosAPIHelper.EmployeeStatus.convertStringNameToEnum(this.getStatus());

            switch (newEmployeeStatus) {
                //case ACTIVE:
                //Not sure if this needs to validate anything else

                default:
                    //do a license check for a new account
                    List<AccountModel> accountList = new ArrayList<>();
                    accountList.add(this);

                    Boolean hasEnoughLicenses = AccountCollection.checkAccountLicenses(accountList, this.getTerminal());
                    if (!hasEnoughLicenses) {
                        throw new MissingDataException("No Licenses exist for new Account");
                    }

                    /*
                    B-06076: remove permissions for now
                    //check the Create Account User
                    if (!userModel.getCanCreate()) {
                        Logger.logMessage("User doesn't have rights to create Account", Logger.LEVEL.TRACE);
                        throw new MissingDataException("Invalid rights for creating Account");
                    }*/

                    //If the TOS needs to be signed, set the Account Status to Waiting
                    Boolean needsTermsOfService = AccountModel.checkOneAccountForTOS(this, this.getTerminal());
                    if (needsTermsOfService) {
                        Logger.logMessage("Account needs to sign Terms Of Service.  Status will be changed to Waiting for Signup", Logger.LEVEL.TRACE);
                        this.setStatus(PosAPIHelper.EmployeeStatus.WAITING_FOR_SIGNUP.toStringName());
                    }
                    this.setTosCheckAlreadyDone(true);

                    break;
            }
        }
    }

    private void validateFields(UserModel userModel) throws Exception {

        if (this.getBirthday() != null && !this.getBirthday().isEmpty()) {

            boolean dateIsValid = false;
            Date validDate = null;

            try {
                //example we are looking for: 5/25/2017 9:22:07
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                validDate = dateFormat.parse(this.getBirthday());
                dateIsValid = true;

            } catch (Exception ex) {
                //throw new MissingDataException("Invalid Birthday Submitted");
            }

            try {
                //example we are looking for: 5/25/2017
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                validDate = dateFormat.parse(this.getBirthday());
                dateIsValid = true;

            } catch (Exception ex) {

            }

            if (!dateIsValid) {
                throw new MissingDataException("Invalid Birthday Submitted");
            }
        }

        //Only one note can be updated or inserted for an Account
        if (this.getNotes() != null && !this.getNotes().isEmpty()) {

            if (this.getNotes().size() > 1) {
                throw new MissingDataException("Invalid Number of Notes submitted.");
            }

            //Don't allow notes outside of the Terminal's revenue center
            for (AccountNoteModel accountNoteModel : this.getNotes()) {
                if (accountNoteModel.getRevenueCenterId() != null && !accountNoteModel.getRevenueCenterId().toString().isEmpty()) {
                    if (!accountNoteModel.getRevenueCenterId().equals(this.getTerminal().getRevenueCenterId())) {
                        throw new MissingDataException("Invalid Revenue Center on Note.");
                    }
                }
            }
        }

        if (this.isNewAccount()) {
            //Name
            if (this.getName() == null || this.getName().isEmpty()) {
                throw new MissingDataException("Name is Required");
            }

            //Account #
            if (this.getNumber() == null || this.getNumber().isEmpty()) {
                throw new MissingDataException("Number is Required");
            } else {
                //validate that the Number is alphanumeric

                Pattern pattern = Pattern.compile("^[a-zA-Z0-9_-]*$");
                Matcher matcher = pattern.matcher(this.getNumber());
                Boolean isValid = matcher.matches();

                if (!isValid) {
                    throw new MissingDataException("Number is invalid.  Please try again.");
                }
            }

            //Badge
            if (this.getBadge() == null || this.getBadge().isEmpty()) {
                throw new MissingDataException("Badge is Required");
            }

            //Account Group (Payroll Grouping ID)
            if (this.getPayrollGroupingId() == null || getPayrollGroupingId().toString().isEmpty()) {
                throw new MissingDataException("Account Group is Required");
            } else {

                String payrollGroupingId = this.fetchOnePayrollGroupingId();
                if (payrollGroupingId == null || payrollGroupingId.isEmpty()) {
                    throw new MissingDataException("Invalid Account Group is submitted");
                }

                //check that the current User can update based on the account's Account Group (Payroll Grouping)
                if (!userModel.canUserIdAccessAccountGroup(this.getTerminal(), this)) {
                    Logger.logMessage("User doesn't have access to this account's Account Group", Logger.LEVEL.TRACE);
                    throw new MissingDataException("Invalid rights for updating Account");
                }
            }

            //Spending Profile
            if (this.getSpendingProfileId() == null || getSpendingProfileId().toString().isEmpty()) {
                throw new MissingDataException("Spending Profile is Required");
            }

            //Account Type
            if (this.getAccountTypeId() == null || getAccountTypeId().toString().isEmpty()) {
                throw new MissingDataException("Account Type is Required");
            }

            //Account Status
            if (this.getStatus() == null || this.getStatus().isEmpty()) {
                throw new MissingDataException("Account Status is Required");
            }

            if (this.getBadge() != null) {
                Boolean isBadgeValid = true;

                if (this.getBadge().isEmpty()) {
                    isBadgeValid = false;
                }

                String badgeNumberFromDb = this.getOneBadgeNumber();
                if (badgeNumberFromDb != null && !badgeNumberFromDb.isEmpty()) {
                    isBadgeValid = false;
                }

                if (!isBadgeValid) {
                    throw new MissingDataException("Invalid Badge Submitted");
                }
            }

            if (this.getAccountTypeId() != null) {
                if (!PosAPIHelper.AccountType.isValid(this.getAccountTypeId())) {
                    throw new MissingDataException("Invalid Account Type Submitted", this.getTerminal().getId());
                }
            }

            if (this.getEmail() != null && !this.getEmail().isEmpty()) {

                ArrayList<HashMap> accountListHM = this.getOneEmailAddress();

                if (accountListHM != null && !accountListHM.isEmpty()) {
                    throw new AccountDuplicateEmailException(this.getTerminal().getId());
                }
            }

        } else {
            //This badge validation checks against the original badge
            if (this.getBadge() != null
                    && (this.getOriginalAccountModel().getBadge() == null || !this.getOriginalAccountModel().getBadge().equals(this.getBadge()))) {

                Boolean isBadgeValid = true;

                if (this.getBadge().isEmpty()) {
                    isBadgeValid = false;
                }

                String badgeNumberFromDb = this.getOneBadgeNumber();
                if (badgeNumberFromDb != null && !badgeNumberFromDb.isEmpty()) {
                    isBadgeValid = false;
                }

                if (!isBadgeValid) {
                    throw new MissingDataException("Invalid Badge Submitted", this.getTerminal().getId());
                }
            }

            //This number validation checks against the original number
            if (this.getNumber() != null
                    && (this.getOriginalAccountModel().getNumber() == null || !this.getOriginalAccountModel().getNumber().equals(this.getNumber()))) {

                Boolean isNumberValid = true;

                if (this.getNumber().isEmpty()) {
                    isNumberValid = false;
                }

                String acctNumberFromDb = this.getOneAccountNumber();
                if (acctNumberFromDb != null && !acctNumberFromDb.isEmpty()) {
                    isNumberValid = false;
                }

                Pattern pattern = Pattern.compile("^[a-zA-Z0-9_-]*$");
                Matcher matcher = pattern.matcher(this.getNumber());
                Boolean isValid = matcher.matches();

                if (!isValid) {
                    throw new MissingDataException("Number is invalid.  Please try again.", this.getTerminal().getId());
                }

                if (!isNumberValid) {
                    throw new MissingDataException("Invalid Number Submitted", this.getTerminal().getId());
                }
            }

            if (this.getAccountTypeId() != null
                    && (this.getOriginalAccountModel().getAccountTypeId() == null || !this.getOriginalAccountModel().getAccountTypeId().equals(this.getAccountTypeId()))) {

                if (!PosAPIHelper.AccountType.isValid(this.getAccountTypeId())) {
                    throw new MissingDataException("Invalid Account Type Submitted", this.getTerminal().getId());
                }
            }

            //validate Payroll Grouping Id (Account Group)
            if (this.getPayrollGroupingId() != null
                    && (this.getOriginalAccountModel().getPayrollGroupingId() == null || !this.getOriginalAccountModel().getPayrollGroupingId().equals(this.getPayrollGroupingId()))) {

                String payrollGroupingId = this.fetchOnePayrollGroupingId();
                if (payrollGroupingId == null || payrollGroupingId.isEmpty()) {
                    throw new MissingDataException("Invalid Account Group is submitted", this.getTerminal().getId());
                }

                //check that the current User can update based on the account's Account Group (Payroll Grouping)
                if (!userModel.canUserIdAccessAccountGroup(this.getTerminal(), this.getOriginalAccountModel())) {
                    Logger.logMessage("User doesn't have access to this account's Account Group", Logger.LEVEL.TRACE);
                    throw new MissingDataException("Invalid rights for updating Account", this.getTerminal().getId());
                }
            }

            if (this.getEmail() != null
                    && (this.getOriginalAccountModel().getEmail() == null || !this.getOriginalAccountModel().getEmail().equals(this.getEmail()))) {

                String employeeId = "";
                ArrayList<HashMap> accountListHM = this.getOneEmailAddress();
                Boolean emailIsValid = true;

                if (accountListHM != null && !accountListHM.isEmpty()) {
                    for (HashMap accountHM : accountListHM) {
                        employeeId = CommonAPI.convertModelDetailToString(accountHM.get("EMPLOYEEID"));

                        if (!this.getId().equals(employeeId)) {
                            //if this employeeIds aren't equal, that means we are trying to save a duplicate
                            emailIsValid = false;
                        }
                    }
                }

                if (!emailIsValid) {
                    throw new AccountDuplicateEmailException(this.getTerminal().getId());
                }
            }
        }
    }

    private void setFieldsOnOriginalForUpdate() throws Exception {

        //Account Status
        if (this.getStatus() != null) {
            this.getOriginalAccountModel().setStatus(this.getStatus());
        }

        //Account Badge Number
        if (this.getBadge() != null) {
            this.getOriginalAccountModel().setBadge(this.getBadge());
        }

        //Account Number
        if (this.getNumber() != null) {
            this.getOriginalAccountModel().setNumber(this.getNumber());
        }

        //Name
        if (this.getName() != null) {
            this.getOriginalAccountModel().setName(this.getName());
        }

        //Account Group
        if (this.getPayrollGroupingId() != null) {
            this.getOriginalAccountModel().setPayrollGroupingId(this.getPayrollGroupingId());
        }

        //Spending Profile
        if (this.getSpendingProfileId() != null) {
            this.getOriginalAccountModel().setSpendingProfileId(this.getSpendingProfileId());
        }

        //Address1
        if (this.getAddress1() != null) {
            this.getOriginalAccountModel().setAddress1(this.getAddress1());
        }

        //Address2
        if (this.getAddress2() != null) {
            this.getOriginalAccountModel().setAddress2(this.getAddress2());
        }

        //City
        if (this.getCity() != null) {
            this.getOriginalAccountModel().setCity(this.getCity());
        }

        //State
        if (this.getState() != null) {
            this.getOriginalAccountModel().setState(this.getState());
        }

        //ZipCode
        if (this.getZipCode() != null) {
            this.getOriginalAccountModel().setZipCode(this.getZipCode());
        }

        //Phone
        if (this.getPhone() != null) {
            this.getOriginalAccountModel().setPhone(this.getPhone());
        }

        //Contact
        if (this.getContact() != null) {
            this.getOriginalAccountModel().setContact(this.getContact());
        }

        //Email
        if (this.getEmail() != null) {
            this.getOriginalAccountModel().setEmail(this.getEmail());
        }

        //Account Type
        if (this.getAccountTypeId() != null) {
            this.getOriginalAccountModel().setAccountTypeId(this.getAccountTypeId());
        }

        //Auto Cloud Invite
        if (this.getAutoCloudInvite() != null) {
            this.getOriginalAccountModel().setAutoCloudInvite(this.getAutoCloudInvite());
        }

        //Mobile Phone
        if (this.getMobilePhone() != null) {
            this.getOriginalAccountModel().setMobilePhone(this.getMobilePhone());
        }

        //new Birthday
        if (this.getBirthday() != null) {
            if (!this.getBirthday().isEmpty()) {
                this.getOriginalAccountModel().setBirthday(this.getBirthday());
            } else { //If empty string is passed in, clear out the birthday
                this.getOriginalAccountModel().setBirthday(null);
            }
        } else { //birthday is null and original birthday is empty, this was causing 1900/01/01 to be saved to db
            if (this.getOriginalAccountModel().getBirthday().isEmpty()) {
                this.getOriginalAccountModel().setBirthday(null);
            }
        }

        //Note
        if (this.getNoteString() != null) {

            String noteToSave = this.getNoteString();

            if (!this.isNewAccount()) {
                this.setOrigAccountNote(noteToSave);
            } else {

            }
        }

        //Account Group Model
        if (this.getAccountGroup() != null) {
            this.getOriginalAccountModel().setAccountGroup(this.getAccountGroup());
        }
    }

    private void setFieldsForInsert() throws Exception {

        if (this.getBirthday() != null && this.getBirthday().isEmpty()) {

            //if an empty string is passed in, set the field to null.  This will null out the Birthday in the database
            this.setBirthday(null);
        }
    }

    /**
     * Compare the current Account against the original account
     * Keep track of these changes in a hashmap that will be saved later
     */
    private void checkForChangedFields() throws Exception {
        changedFieldsHM = new HashMap();

        this.checkAccountDetails();
        this.checkContactInfo();
        this.checkAccountNote();

        //for updates, if any changes were made that are not notes, check the
        if (!this.isNewAccount()) {

            PosAPIHelper.EmployeeStatus statusEnum = PosAPIHelper.EmployeeStatus.convertStringNameToEnum(this.getOriginalAccountModel().getStatus());
            switch (statusEnum) {
                case WAITING_FOR_SIGNUP:
                case SUSPENDED:
                case FROZEN:
                case PENDING_INACTIVATION:

                    if (this.getStatus() != null) {

                        PosAPIHelper.EmployeeStatus curStatusEnum = PosAPIHelper.EmployeeStatus.convertStringNameToEnum(this.getStatus());
                        switch (curStatusEnum) {
                            case ACTIVE:
                                //Only Account Notes are allowed
                                String dbFieldName = "ACCOUNTNOTE";
                                String dbFieldName2 = "AIP";

                                if (this.getChangedFieldsHM() != null && !this.getChangedFieldsHM().isEmpty() &&
                                        (this.getChangedFieldsHM().size() > 2)) {
                                    //If a non note change is submitted, throw an error
                                    throw new MissingDataException("Only notes can be updated for this account status", this.getTerminal().getId());
                                }

                                //Only account status and Note can be updated here
                                if (this.getChangedFieldsHM() != null && !this.getChangedFieldsHM().isEmpty() &&
                                        (this.getChangedFieldsHM().size() == 2)
                                        && (!getChangedFieldsHM().containsKey(dbFieldName) && !getChangedFieldsHM().containsKey(dbFieldName2))) {

                                    //If a non note change is submitted, throw an error
                                    throw new MissingDataException("Only notes can be updated for this account status", this.getTerminal().getId());
                                }
                                break;

                            default:
                                //If a non note change is submitted, throw an error
                                throw new MissingDataException("Only notes can be updated for this account status", this.getTerminal().getId());
                        }
                    }

                default:
                    //do nothing
                    break;
            }
        }
    }

    /**
     * Keep track of the original values in a Hash Map
     *
     * @return
     */
    private void checkAccountDetails() {
        String dbFieldName = "";

        if (this.getName() != null) {
            dbFieldName = "NAME";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getName() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getName().equals(this.getName())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getName());
                }
            }
        }

        if (this.getAccountTypeId() != null) {
            dbFieldName = "ACCOUNTTYPEID";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getAccountTypeId() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getAccountTypeId().equals(this.getAccountTypeId())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getAccountTypeId());
                }
            }
        }

        if (this.getSpendingProfileId() != null) {
            dbFieldName = "TERMINALPROFILEID";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getSpendingProfileId() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getSpendingProfileId().equals(this.getSpendingProfileId())) {
                    String spendingProfileName = getSpendingProfileName(this.getOriginalAccountModel().getSpendingProfileId());
                    changedFieldsHM.put(dbFieldName, spendingProfileName);
                }
            }
        }

        if (this.getStatus() != null) {
            dbFieldName = "AIP";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getStatus() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getStatus().equals(this.getStatus())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getStatus());
                }
            }
        }

        //Account Group
        if (this.getPayrollGroupingId() != null) {
            dbFieldName = "PAYROLLGROUPINGID";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getPayrollGroupingId() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getPayrollGroupingId().equals(this.getPayrollGroupingId())) {
                    String accountGroupName = getAccountGroupName(this.getOriginalAccountModel().getPayrollGroupingId());
                    changedFieldsHM.put(dbFieldName, accountGroupName);
                }
            }
        }

        if (this.getBadge() != null) {
            dbFieldName = "BADGE";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getBadge() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getBadge().equals(this.getBadge())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getBadge());
                }
            }
        }

        //Acount Number
        if (this.getNumber() != null) {
            dbFieldName = "EMPLOYEENUMBER";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getNumber() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getNumber().equals(this.getNumber())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getNumber());
                }
            }
        }
    }

    private void checkContactInfo() {
        String dbFieldName = "";

        if (this.getAddress1() != null) {
            dbFieldName = "ADDRESS1";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getAddress1() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getAddress1().equals(this.getAddress1())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getAddress1());
                }
            }
        }

        if (this.getAddress2() != null) {
            dbFieldName = "ADDRESS2";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getAddress2() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getAddress2().equals(this.getAddress2())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getAddress2());
                }
            }
        }

        if (this.getCity() != null) {
            dbFieldName = "CITY";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getCity() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getCity().equals(this.getCity())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getCity());
                }
            }
        }

        if (this.getState() != null) {
            dbFieldName = "STATE";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getState() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getState().equals(this.getState())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getState());
                }
            }
        }

        if (this.getZipCode() != null) {
            dbFieldName = "ZIPCODE";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getZipCode() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getZipCode().equals(this.getZipCode())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getZipCode());
                }
            }
        }

        if (this.getContact() != null) {
            dbFieldName = "CONTACT";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getContact() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getContact().equals(this.getContact())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getContact());
                }
            }
        }

        if (this.getPhone() != null) {
            dbFieldName = "PHONE";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getPhone() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getPhone().equals(this.getPhone())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getPhone());
                }
            }
        }

        if (this.getEmail() != null) {
            dbFieldName = "EMAILADDRESS1";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getEmail() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getEmail().equals(this.getEmail())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getEmail());
                }
            }
        }

        if (this.getBirthday() != null) {
            dbFieldName = "BIRTHDAY";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getBirthday() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getBirthday().equals(this.getBirthday())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getBirthday());
                }
            }
        }

        if (this.getAutoCloudInvite() != null) {
            dbFieldName = "AUTOCLOUDINVITE";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getAutoCloudInvite() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getAutoCloudInvite().equals(this.getAutoCloudInvite())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getAutoCloudInvite());
                }
            }
        }

        if (this.getMobilePhone() != null) {
            dbFieldName = "MOBILEPHONE";

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, "New Activation");
            } else {
                if (this.getOriginalAccountModel().getMobilePhone() == null) {
                    changedFieldsHM.put(dbFieldName, "");
                } else if (!this.getOriginalAccountModel().getMobilePhone().equals(this.getMobilePhone())) {
                    changedFieldsHM.put(dbFieldName, this.getOriginalAccountModel().getMobilePhone());
                }
            }
        }
    }

    private void checkAccountNote() {
        //Check Notes
        int revCenterId = this.getTerminal().getRevenueCenterId();

        if (this.getNoteString() != null) {
            String dbFieldName = "ACCOUNTNOTE";

            String revCenterNameString = "";
            if (this.getTerminal().getRevenueCenter().getName() != null && !this.getTerminal().getRevenueCenter().getName().isEmpty()) {
                revCenterNameString = "(" + this.getTerminal().getRevenueCenter().getName() + ") ";
            }

            if (this.isNewAccount()) {
                changedFieldsHM.put(dbFieldName, revCenterNameString + "New Activation");
            } else {
                if (this.getOriginalAccountModel().getNoteString() == null) {
                    changedFieldsHM.put(dbFieldName, revCenterNameString);
                } else if (!this.getOriginalAccountModel().getNoteString().equals(getNoteString())) {
                    changedFieldsHM.put(dbFieldName, revCenterNameString + this.getOriginalAccountModel().getNoteString());
                }
            }
        }
    }

    //endregion

    //region Save Methods

    //save QCPOS transaction to database
    public void saveAccount() throws Exception {
        DataManager dm = new DataManager();
        String logFileName = PosAPIHelper.getLogFileName(this.getTerminal().getId());

        int saveStatus = 0;
        Exception mainEx = null;

        JDCConnection conn = null;
        try {
            conn = dm.pool.getConnection(false, logFileName);
        } catch (SQLException qe) {
            Logger.logMessage("AccountModel.saveAccount no connection to DB.", logFileName);
            Logger.logMessage(qe.getMessage(), logFileName);
            throw new MissingDataException("Could not establish a connection to the database.", this.getTerminal().getId());
        }

        int iAccountID = -1;

        try {

            conn.setAutoCommit(false); //make sure the save is atomic

            if (this.isNewAccount()) { //update original account
                this.saveNewAccount(conn, logFileName); //save Account Model
                this.saveAccountNotes(conn, logFileName); //save notes
                AccountModel.setLastEmployeeId(conn, logFileName, this.getId().toString(), this.getTerminal()); //Set the last Inserted EmployeeID in the Globals
            } else {
                this.saveOriginalAccount(conn, logFileName); //save Account Model
                this.saveOriginalAccountNotes(conn, logFileName); //save notes
            }

            this.updateEmployeeChanges(conn, logFileName); //save "RecordAccountChanges"

            saveStatus = 1;
            conn.commit();
            Logger.logMessage("Account Saved Successfully (AccountID=" + iAccountID + ")", logFileName, Logger.LEVEL.DEBUG);
        } catch (Exception ex) {
            Logger.logMessage("Account Failed (AccountID=" + iAccountID + ")", logFileName, Logger.LEVEL.ERROR);
            mainEx = ex;

            try {
                conn.rollback();
                //conn.setAutoCommit(true);  // THIS WAS ALREADY COMMENTED OUT ... SHOULD IT HAVE BEEN?
                Logger.logMessage("Rollback Successfully (AccountID=" + iAccountID + ")", logFileName, Logger.LEVEL.ERROR);
            } catch (SQLException e) {
                Logger.logException("AccountModel.saveAccount. SQL exception", logFileName, e);
            }
            saveStatus = -1;
            Logger.logException(ex, logFileName);
        } finally {
            try {
                conn.setAutoCommit(true);
                dm.pool.returnConnection(conn, logFileName);
                Logger.logMessage("Database Transaction Committed Successfully (AccountID=" + iAccountID + ")", logFileName, Logger.LEVEL.DEBUG);
            } catch (SQLException ex) {
                Logger.logException(ex, logFileName);
            }
        }

        //Now the the "finally" is done and the database should be cleaned up
        //if the save did not occur properly, return the error to the API Client
        if (saveStatus < 1) {
            if (mainEx != null) {
                throw mainEx;
            } else {
                throw new MissingDataException("Error Saving Account.  Check logs for more details.", this.getTerminal().getId());
            }
        }

        this.getTerminal().logTransactionTime();
    }

    private void saveAccountNotes(JDCConnection conn, String logFileName) throws Exception {
        DataManager dm = new DataManager();
        String dbFieldName = "ACCOUNTNOTE";
        if (this.changedFieldsHM.get(dbFieldName) == null) {
            return; //if this field wasn't changed, return
        }

        //Get the note for the correct revenue center
        String noteToSave = this.getNoteString();

        int updatedAccountNoteID = 0;

        updatedAccountNoteID = dm.parameterizedExecuteNonQuery("data.posapi30.UpdateAccountNote", new Object[]{
                noteToSave,
                this.getId(),
                this.getTerminal().getRevenueCenterId()

        }, logFileName, conn);

        //if update fails, try the insert
        if (updatedAccountNoteID <= 0) {
            int accountNoteId = 0;

            accountNoteId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertAccountNote", new Object[]{
                    noteToSave,
                    this.getId(),
                    this.getTerminal().getRevenueCenterId()
            }, logFileName, conn);

            if (accountNoteId <= 0) {
                throw new MissingDataException("Error inserting Account Note", this.getTerminal().getId());
            }

            AccountNoteModel accountNoteModel = this.getNoteForRevenueCenterId(this.getTerminal().getRevenueCenterId());
            accountNoteModel.setId(accountNoteId);
        }
    }

    private void saveOriginalAccountNotes(JDCConnection conn, String logFileName) throws Exception {

        String dbFieldName = "ACCOUNTNOTE";
        if (this.changedFieldsHM.get(dbFieldName) == null) {
            return; //if this field wasn't changed, return
        }

        DataManager dm = new DataManager();

        //Get the note for the correct revenue center
        String noteToSave = this.getOriginalAccountModel().getNoteString();

        if (noteToSave == null) {
            return;
        }

        int updatedAccountNoteID = 0;


        updatedAccountNoteID = dm.parameterizedExecuteNonQuery("data.posapi30.UpdateAccountNote", new Object[]{
                noteToSave,
                this.getOriginalAccountModel().getId(),
                this.getTerminal().getRevenueCenterId()

        }, logFileName, conn);

        //if update fails, try the insert
        if (updatedAccountNoteID <= 0) {
            int accountNoteId = 0;

            accountNoteId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertAccountNote", new Object[]{
                    noteToSave,
                    this.getOriginalAccountModel().getId(),
                    this.getTerminal().getRevenueCenterId()
            }, logFileName, conn);

            if (accountNoteId <= 0) {
                throw new MissingDataException("Error inserting Account Note", this.getTerminal().getId());
            }

            AccountNoteModel accountNoteModel = this.getOriginalAccountModel().getNoteForRevenueCenterId(this.getTerminal().getRevenueCenterId());
            accountNoteModel.setId(accountNoteId);
        }
    }

    private void updateEmployeeChanges(JDCConnection conn, String logFileName) {

        //records all the account changes in the QC_EmployeesChanges table
        if (!changedFieldsHM.isEmpty()) {
            //recordUpdatedAccountChanges(record);
            recordUpdatedAccountDetails(conn, logFileName);
            recordUpdatedContactInfo(conn, logFileName);
            recordUpdatedNotes(conn, logFileName);
        }
    }

    /**
     * Save Account Details to the QC_EmployeeChanges table
     *
     * @param conn
     * @param logFileName
     */
    private void recordUpdatedAccountDetails(JDCConnection conn, String logFileName) {
        String dbFieldName = "";
        String recordAs = "";
        String loggedInUserID = this.getUserModel().getId().toString();
        String accountId = this.getId().toString();

        dbFieldName = "NAME";
        recordAs = "Name";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getName(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getName(), loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "ACCOUNTTYPEID";
        recordAs = "Account Type";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getAccountTypeId().toString(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getAccountTypeId().toString(), loggedInUserID, conn, logFileName);
            }
        }

        //Spending Profile
        dbFieldName = "TERMINALPROFILEID";
        recordAs = "Spending Profile";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field

            if (this.isNewAccount()) {
                String spendingProfileName = getSpendingProfileName(this.getSpendingProfileId());
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), spendingProfileName, loggedInUserID, conn, logFileName);
            } else {
                String spendingProfileName = getSpendingProfileName(this.getOriginalAccountModel().getSpendingProfileId());
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), spendingProfileName, loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "AIP";
        recordAs = "Status";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getStatus(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getStatus(), loggedInUserID, conn, logFileName);
            }
        }

        //Account Group
        dbFieldName = "PAYROLLGROUPINGID";
        recordAs = "Account Group";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                String accountGroupName = getAccountGroupName(this.getPayrollGroupingId());
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), accountGroupName, loggedInUserID, conn, logFileName);
            } else {
                String accountGroupName = getAccountGroupName(this.getOriginalAccountModel().getPayrollGroupingId());
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), accountGroupName, loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "BADGE";
        recordAs = "Badge";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getBadge(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getBadge(), loggedInUserID, conn, logFileName);
            }
        }

        //Acount Number
        dbFieldName = "EMPLOYEENUMBER";
        recordAs = "Employee ID";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getNumber(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getNumber(), loggedInUserID, conn, logFileName);
            }
        }
    }

    /**
     * Save Contact Info to the QC_EmployeeChanges table
     *
     * @return
     */
    private void recordUpdatedContactInfo(JDCConnection conn, String logFileName) {
        String dbFieldName = "";
        String recordAs = "";
        String loggedInUserID = this.getUserModel().getId().toString();
        String accountId = this.getId().toString();

        dbFieldName = "ADDRESS1";
        recordAs = "Address1";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getAddress1(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getAddress1(), loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "ADDRESS2";
        recordAs = "Address2";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getAddress2(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getAddress2(), loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "CITY";
        recordAs = "City";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getCity(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getCity(), loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "STATE";
        recordAs = "State";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getState(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getState(), loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "ZIPCODE";
        recordAs = "ZipCode";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getZipCode(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getZipCode(), loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "CONTACT";
        recordAs = "Contact";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getContact(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getContact(), loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "PHONE";
        recordAs = "Phone";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getPhone(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getPhone(), loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "EMAILADDRESS1";
        recordAs = "EmailAddress";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getEmail(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getEmail(), loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "BIRTHDAY";
        recordAs = "Birthday";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getBirthday(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), (this.getOriginalAccountModel().getBirthday() == null) ? "" : this.getOriginalAccountModel().getBirthday(), loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "AUTOCLOUDINVITE";
        recordAs = "Auto Cloud Invite";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getAutoCloudInvite().toString(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getAutoCloudInvite().toString(), loggedInUserID, conn, logFileName);
            }
        }

        dbFieldName = "MOBILEPHONE";
        recordAs = "Mobile Phone";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field
            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getMobilePhone(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), this.getOriginalAccountModel().getMobilePhone(), loggedInUserID, conn, logFileName);
            }
        }
    }

    /**
     * Save Note Info to the QC_EmployeeChanges table
     *
     * @return
     */
    private void recordUpdatedNotes(JDCConnection conn, String logFileName) {
        String dbFieldName = "";
        String recordAs = "";
        String loggedInUserID = this.getUserModel().getId().toString();
        String accountId = this.getId().toString();

        dbFieldName = "ACCOUNTNOTE";
        recordAs = "Account Note";
        if (changedFieldsHM.get(dbFieldName) != null) { //don't bother trying if we didn't update this field

            String revCenterNameString = "";
            if (this.getTerminal().getRevenueCenter().getName() != null && !this.getTerminal().getRevenueCenter().getName().isEmpty()) {
                revCenterNameString = "(" + this.getTerminal().getRevenueCenter().getName() + ") ";
            }

            if (this.isNewAccount()) {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), revCenterNameString + this.getNoteString(), loggedInUserID, conn, logFileName);
            } else {
                recordAccountChange(accountId, recordAs, changedFieldsHM.get(dbFieldName).toString(), revCenterNameString + this.getOriginalAccountModel().getNoteString(), loggedInUserID, conn, logFileName);
            }
        }
    }

    //records what changes are made to an account in the QC_EmployeeChanges table
    public boolean recordAccountChange(String accountID, String field, String before, String after, String loggedInUserID, JDCConnection conn, String logFileName) {
        DataManager dm = new DataManager();

        try {
            //if the before and after values are not the same
            if (before.compareTo(after) != 0) {
                if (field.equals("User Password")) { //remove passwords so they do not get logged in the DB as plain text
                    before = "********";
                    after = "********";
                }
                return dm.serializeUpdate("data.posapi30.recordAccountChange", new Object[]{accountID, field, before, after, loggedInUserID}, conn, logFileName) != -1;
            }
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error recording account change in recordAccountChange for field: " + field, Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return false;
        }
        return false;
    }

    /**
     * Save the account
     * First check if it is a new account
     *
     * @param conn
     * @param logFileName
     * @throws Exception
     */
    private void saveNewAccount(JDCConnection conn, String logFileName) throws Exception {
        DataManager dm = new DataManager();
        Logger.logMessage("Saving Account. AccountModel.save.", logFileName, Logger.LEVEL.DEBUG);

        String accountStatusLetter = PosAPIHelper.EmployeeStatus.convertStringNameToEnum(this.getStatus()).toLetter();

        //Set the new Employee ID
        Integer lastEmployeeId = AccountModel.getLastEmployeeId(this.getTerminal());
        Integer newEmployeeId = lastEmployeeId + 1;

        Integer iAccountId = 0;
        iAccountId = dm.parameterizedExecuteNonQuery("data.posapi30.InsertNewAccount", new Object[]{
                newEmployeeId,
                accountStatusLetter,
                this.getBadge(),
                this.getNumber(),
                this.getName(),
                this.getPayrollGroupingId(), //account group
                this.getSpendingProfileId(),
                this.getAddress1(),
                this.getAddress2(),
                this.getCity(),
                this.getState(),
                this.getZipCode(),
                this.getPhone(),
                this.getContact(),
                this.getEmail(),
                this.getAccountTypeId(),
                this.getAutoCloudInvite(),
                this.getBirthday(),
                this.getMobilePhone()
        }, logFileName, conn);

        if (iAccountId <= 0) {
            throw new MissingDataException("Account could not be recorded in QC_Employees", this.getTerminal().getId());
        }

        this.setId(iAccountId);
    }

    /**
     * Save the original account
     * This is used in the case of an update
     * For the Update, use the AccountModel.getOriginalAccountModel for the save.  That way all the info is included
     *
     * @param conn
     * @param logFileName
     * @throws Exception
     */
    private void saveOriginalAccount(JDCConnection conn, String logFileName) throws Exception {
        DataManager dm = new DataManager();
        Logger.logMessage("Saving Account. AccountModel.saveOriginalAccount.", logFileName, Logger.LEVEL.DEBUG);

        String accountStatusLetter = PosAPIHelper.EmployeeStatus.convertStringNameToEnum(this.getOriginalAccountModel().getStatus()).toLetter();
        Integer iAccountId = 0;

        iAccountId = dm.parameterizedExecuteNonQuery("data.posapi30.UpdateAccount", new Object[]{

                this.getOriginalAccountModel().getId(),
                accountStatusLetter,
                this.getOriginalAccountModel().getBadge(),
                this.getOriginalAccountModel().getNumber(),
                this.getOriginalAccountModel().getName(),
                this.getOriginalAccountModel().getPayrollGroupingId(), //account group
                this.getOriginalAccountModel().getSpendingProfileId(),
                this.getOriginalAccountModel().getAddress1(),
                this.getOriginalAccountModel().getAddress2(),
                this.getOriginalAccountModel().getCity(),
                this.getOriginalAccountModel().getState(),
                this.getOriginalAccountModel().getZipCode(),
                this.getOriginalAccountModel().getPhone(),
                this.getOriginalAccountModel().getContact(),
                this.getOriginalAccountModel().getEmail(),
                this.getOriginalAccountModel().getAccountTypeId(),
                this.getOriginalAccountModel().getAutoCloudInvite(),
                this.getOriginalAccountModel().getBirthday(),
                this.getOriginalAccountModel().getMobilePhone()

        }, logFileName, conn);

        if (iAccountId <= 0) {
            throw new MissingDataException("Account could not be updated in QC_Employees", this.getTerminal().getId());
        }
    }

    //endregion

    /**
     * B-06068: If just the Account group is changed on an Account, we need to check if they need a Terms of Service (TOS) check
     * If it was already checked, don't do it again
     *
     * @throws Exception
     */
    private void checkTosForChangedAccountGroup() throws Exception {
        Boolean accountGroupChanged = false;

        //Check if Account Group was changed
        if (this.getPayrollGroupingId() != null) {
            if (this.getOriginalAccountModel().getPayrollGroupingId() == null) {
                accountGroupChanged = true;
            } else if (!this.getOriginalAccountModel().getPayrollGroupingId().equals(this.getPayrollGroupingId())) {
                accountGroupChanged = true;
            }
        }

        if (accountGroupChanged && !tosCheckAlreadyDone()) {

            //If the TOS needs to be signed, set the Account Status to Waiting
            Boolean needsTermsOfService = AccountModel.checkOneAccountForTOS(this, this.getTerminal());
            if (needsTermsOfService) {
                Logger.logMessage("Account needs to sign Terms Of Service.  Status will be changed to Waiting for Signup", Logger.LEVEL.TRACE);
                this.setStatus(PosAPIHelper.EmployeeStatus.WAITING_FOR_SIGNUP.toStringName());
            }

            this.setTosCheckAlreadyDone(true);
        }
    }

    public static Boolean autoCloudInviteInGlobals(TerminalModel terminalModel) {
        Boolean autoCloudInviteInGlobals = false;
        DataManager dm = new DataManager();

        //get all models in an array list, QCPos is offline
        ArrayList<HashMap> globalsList = dm.parameterizedExecuteQuery("data.posapi30.getGlobals",
                new Object[]{},
                PosAPIHelper.getLogFileName(terminalModel.getId()), true
        );

        if (globalsList == null || globalsList.isEmpty()) {
            return false;
        }

        HashMap modelDetailHM = globalsList.get(0);
        autoCloudInviteInGlobals = CommonAPI.convertModelDetailToBoolean(modelDetailHM.get("AUTOCLOUDINVITE"), false);

        return autoCloudInviteInGlobals;
    }

    public static Boolean checkOneAccountForTOS(AccountModel accountModel, TerminalModel terminalModel) throws Exception {
        Boolean doesAccountNeedTos = false;
        AccountQueryParams accountQueryParams = new AccountQueryParams(true);

        //Query the Account Model
        //The AccountModel needs at least an EmployeeID, Employee Number, or Badge
        if (accountModel != null) {

            if (accountModel.getId() != null
                    && !accountModel.getId().toString().isEmpty()
                    && !accountModel.getId().equals(-1)) {

                if (accountModel.getId() != null && !accountModel.getId().toString().isEmpty()) {
                    accountModel = AccountModel.getAccountModelByIdForTender(terminalModel, BigDecimal.ONE, accountModel.getId(), accountQueryParams);
                } else if (accountModel.getBadge() != null && !accountModel.getBadge().toString().isEmpty()) {
                    accountModel = AccountModel.getAccountModelByBadgeForTender(terminalModel, BigDecimal.ONE, accountModel.getBadge(), accountQueryParams);

                } else if (accountModel.getNumber() != null && !accountModel.getNumber().toString().isEmpty()) {
                    accountModel = AccountModel.getAccountModelByNumberForTender(terminalModel, BigDecimal.ONE, accountModel.getNumber(), accountQueryParams);
                }
            }
        }

        //Check if the Account needs a TOS
        commonMMHFunctions commonMMHFunctions = new commonMMHFunctions();
        doesAccountNeedTos = commonMMHFunctions.checkIfTOSIsRequired(accountModel.getPayrollGroupingId(), accountModel.getSpendingProfileId());

        return doesAccountNeedTos;
    }

    private void buildAccountGroupModel(TerminalModel terminalModel) throws Exception {

        if(this.getPayrollGroupingId() != null) {

            AccountGroupModel accountGroup = AccountGroupModel.getOneAccountGroupModelById(this.getPayrollGroupingId(), terminalModel);
            this.setAccountGroup(accountGroup);
        }
    }

    @JsonIgnore
    private AccountNoteModel getNoteForRevenueCenterId(int revenueCenterId) {

        AccountNoteModel curAccttNoteModel = null;

        String noteToSave = null;
        if (this.getNotes() != null && !this.getNotes().isEmpty()) {
            for (AccountNoteModel accountNoteModel : this.getNotes()) {
                curAccttNoteModel = accountNoteModel;
            }
        }

        return curAccttNoteModel;
    }

    private void setOrigAccountNote(String newNote) {
        if (newNote != null) {
            if (this.isNewAccount()) {
                AccountNoteModel accountNoteModel = new AccountNoteModel();
                accountNoteModel.setRevenueCenterId(this.getTerminal().getRevenueCenterId());
                accountNoteModel.setNote(newNote);
                this.getOriginalAccountModel().getNotes().add(accountNoteModel);
            } else {
                String noteToUpdate = null;
                for (AccountNoteModel accountNoteModel : this.getOriginalAccountModel().getNotes()) {
                    noteToUpdate = accountNoteModel.getNote();
                }

                if (noteToUpdate == null) { //no note existed
                    AccountNoteModel accountNoteModel = new AccountNoteModel();
                    accountNoteModel.setRevenueCenterId(this.getTerminal().getRevenueCenterId());
                    accountNoteModel.setNote(newNote);
                    this.getOriginalAccountModel().getNotes().add(accountNoteModel);
                } else {  //update the note that is already there
                    for (AccountNoteModel accountNoteModel : this.getOriginalAccountModel().getNotes()) {
                        accountNoteModel.setNote(newNote);
                    }
                }
            }
        }
    }

    @JsonIgnore
    private String getNoteString() {

        String noteToSave = null;
        if (this.getNotes() != null && !this.getNotes().isEmpty()) {
            for (AccountNoteModel accountNoteModel : this.getNotes()) {
                noteToSave = accountNoteModel.getNote();
            }
        }

        return noteToSave;
    }

    @JsonIgnore
    private String getOriginalNoteForRevenueCenterId(int revenueCenterId) {

        String noteToSave = null;
        if (this.getOriginalAccountModel().getNotes() != null && !this.getOriginalAccountModel().getNotes().isEmpty()) {
            for (AccountNoteModel accountNoteModel : this.getOriginalAccountModel().getNotes()) {
                if (accountNoteModel.getRevenueCenterId().equals(revenueCenterId)) {
                    noteToSave = accountNoteModel.getNote();
                }
            }
        }

        return noteToSave;
    }

    public void setAccountModelCache() throws Exception {

        this.setSpendingProfileModelList(SpendingProfileCollection.getAllSpendingProfiles(this.getTerminal()).getCollection());
        this.setAccountGroupModelList(AccountGroupCollection.getAllActiveAccountGroups(this.getTerminal()).getCollection());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountModel that = (AccountModel) o;

        if (badge != null ? !badge.equals(that.badge) : that.badge != null) return false;
        if (!id.equals(that.id)) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (number != null ? !number.equals(that.number) : that.number != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (badge != null ? badge.hashCode() : 0);
        result = 31 * result + (number != null ? number.hashCode() : 0);
        return result;
    }

    @JsonIgnore
    private String getSpendingProfileName(Integer spendingProfileId) {

        String spendingProfileName = "";

        for (SpendingProfileModel spendingProfileModel : this.getSpendingProfileModelList()) {
            if (spendingProfileId.equals(spendingProfileModel.getId())) {
                spendingProfileName = spendingProfileModel.getName();
            }
        }

        return spendingProfileName;
    }

    @JsonIgnore
    private String getAccountGroupName(Integer accountGroupId) {

        String accountGroupName = "";

        for (AccountGroupModel accountGroupModel : this.getAccountGroupModelList()) {
            if (accountGroupId.equals(accountGroupModel.getId())) {
                accountGroupName = accountGroupModel.getName();
            }
        }

        return accountGroupName;
    }

    @JsonIgnore
    public String getOneBadgeNumber() {
        DataManager dm = new DataManager();
        String badgeNumber = "";

        ArrayList<HashMap> accountListHM = dm.parameterizedExecuteQuery("data.posapi30.getOneAccountBadge",
                new Object[]{
                        this.getBadge()
                },
                PosAPIHelper.getLogFileName(this.getTerminal().getId()), true
        );

        if (accountListHM != null && !accountListHM.isEmpty()) {
            HashMap modelDetailHM = accountListHM.get(0);
            badgeNumber = CommonAPI.convertModelDetailToString(modelDetailHM.get("BADGEID"));
        }

        return badgeNumber;
    }

    @JsonIgnore
    public String getOneAccountNumber() {
        DataManager dm = new DataManager();
        String accountNumber = "";

        ArrayList<HashMap> accountListHM = dm.parameterizedExecuteQuery("data.posapi30.getOneAccountNumber",
                new Object[]{
                        this.getNumber()
                },
                PosAPIHelper.getLogFileName(this.getTerminal().getId()), true
        );

        if (accountListHM != null && !accountListHM.isEmpty()) {

            HashMap modelDetailHM = accountListHM.get(0);
            accountNumber = CommonAPI.convertModelDetailToString(modelDetailHM.get("EMPLOYEENUMBER"));

        }

        return accountNumber;
    }

    @JsonIgnore
    public ArrayList<HashMap> getOneEmailAddress() {
        DataManager dm = new DataManager();

        ArrayList<HashMap> accountListHM = dm.parameterizedExecuteQuery("data.posapi30.getOneAccountEmail",
                new Object[]{
                        this.getEmail()
                },
                PosAPIHelper.getLogFileName(this.getTerminal().getId()), true
        );

        return accountListHM;
    }

    @JsonIgnore
    public String fetchOnePayrollGroupingId() {
        DataManager dm = new DataManager();
        String payrollGroupingId = "";

        ArrayList<HashMap> accountListHM = dm.parameterizedExecuteQuery("data.posapi30.fetchOnePayrollGroupingId",
                new Object[]{
                        this.getPayrollGroupingId()
                },
                PosAPIHelper.getLogFileName(this.getTerminal().getId()), true
        );

        if (accountListHM != null && !accountListHM.isEmpty()) {

            HashMap modelDetailHM = accountListHM.get(0);
            payrollGroupingId = CommonAPI.convertModelDetailToString(modelDetailHM.get("PAYROLLGROUPINGID"));

        }

        return payrollGroupingId;
    }

    @JsonIgnore
    public static Integer getLastEmployeeId(TerminalModel terminalModel) {
        DataManager dm = new DataManager();
        Integer lastId = -1;

        ArrayList<HashMap> accountListHM = dm.parameterizedExecuteQuery("data.posapi30.getLastEmpID",
                new Object[]{

                },
                PosAPIHelper.getLogFileName(terminalModel.getId()), true
        );

        if (accountListHM != null && !accountListHM.isEmpty()) {
            HashMap accountHM = accountListHM.get(0);
            lastId = CommonAPI.convertModelDetailToInteger(accountHM.get("LASTEMPID"));
        }

        return lastId;
    }

    @JsonIgnore
    public static Integer setLastEmployeeId(JDCConnection conn, String logFileName, String employeeId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();
        Integer lastId = -1;


        lastId = dm.parameterizedExecuteNonQuery("data.posapi30.setGlobalEmpID",
                new Object[]{employeeId

                },
                logFileName, conn
        );

        if (lastId <= 0) {
            throw new MissingDataException("Last Employee ID could not be updated", terminalModel.getId());
        }

        return lastId;
    }

    public Boolean checkForOfflineMode() {
        Boolean isOffline = false;

        //check if POS is in offline mode
        if ((MMHProperties.getAppSetting("site.application.offline").length() > 0) && (MMHProperties.getAppSetting("site.application.offline").compareToIgnoreCase("yes") == 0)) {
            isOffline = true;
        }

        return isOffline;
    }

    public void freezeAccount() throws Exception {

        if (this.getStatus() != null && !this.getStatus().isEmpty()) {
            if (!this.getStatus().equalsIgnoreCase(PosAPIHelper.EmployeeStatus.ACTIVE.toString())) {
                throw new MissingDataException("Account status is not Active.");
            }
        }

        if (this.getId() != null && this.getId() != null && !this.getId().toString().isEmpty()) {
            //update AIP
            this.updateAccountStatus(PosAPIHelper.EmployeeStatus.FROZEN.toLetter());
        }
    }

    public void unFreezeAccount() throws Exception {

        if (this.getStatus() != null && !this.getStatus().isEmpty()) {
            if (!this.getStatus().equalsIgnoreCase(PosAPIHelper.EmployeeStatus.FROZEN.toString())) {
                throw new MissingDataException("Account status is not Frozen.");
            }
        }

        if (this.getId() != null && this.getId() != null && !this.getId().toString().isEmpty()) {

            //update AIP
            this.updateAccountStatus(PosAPIHelper.EmployeeStatus.ACTIVE.toLetter());
        }
    }

    public void updateAccountStatus(String accountStatusLetter) throws Exception {
        DataManager dm = new DataManager();
        String logFileName = PosAPIHelper.getLogFileName(this.getTerminal().getId());

        int saveStatus = 0;
        Exception mainEx = null;

        JDCConnection conn = null;
        try {
            conn = dm.pool.getConnection(false, logFileName);
        } catch (SQLException qe) {
            Logger.logMessage("AccountModel.saveAccount no connection to DB.", logFileName);
            Logger.logMessage(qe.getMessage(), logFileName);
            throw new MissingDataException("Could not establish a connection to the database.", this.getTerminal().getId());
        }

        int iAccountID = -1;

        try {


            conn.setAutoCommit(false); //make sure the save is atomic

            Integer iAccountId = 0;
            iAccountId = dm.parameterizedExecuteNonQuery("data.posapi30.UpdateAccountStatus", new Object[]{

                    this.getId(),
                    accountStatusLetter

            }, logFileName, conn);

            if (iAccountId <= 0) {
                throw new MissingDataException("Account could not be updated in QC_Employees", this.getTerminal().getId());
            }

            //log the original status and the new status
            PosAPIHelper.EmployeeStatus originalEmployeeStatusEnum = PosAPIHelper.EmployeeStatus.convertStringNameToEnum(this.getStatus());
            String originalEmployeeStatusLetter = originalEmployeeStatusEnum.toLetter();
            this.recordAccountChange(this.getId().toString(), "Status", originalEmployeeStatusLetter, accountStatusLetter, this.getTerminal().getLoginModel().getUserId().toString(), conn, logFileName);

            //new status -update the model to return back to the client.
            PosAPIHelper.EmployeeStatus newEmployeeStatusEnum = PosAPIHelper.EmployeeStatus.convertLetterToEnum(accountStatusLetter);
            String newEmployeeStatusLetter = newEmployeeStatusEnum.toLetter();
            String newEmployeeStatusName = newEmployeeStatusEnum.toStringName();
            this.setStatus(newEmployeeStatusName);


            saveStatus = 1;
            conn.commit();
            Logger.logMessage("Account Saved Successfully (AccountID=" + iAccountID + ")", logFileName, Logger.LEVEL.DEBUG);
        } catch (Exception ex) {
            Logger.logMessage("Account Failed (AccountID=" + iAccountID + ")", logFileName, Logger.LEVEL.ERROR);
            mainEx = ex;

            try {
                conn.rollback();
                //conn.setAutoCommit(true);  // THIS WAS ALREADY COMMENTED OUT ... SHOULD IT HAVE BEEN?
                Logger.logMessage("Rollback Successfully (AccountID=" + iAccountID + ")", logFileName, Logger.LEVEL.ERROR);
            } catch (SQLException e) {
                Logger.logException("AccountModel.saveAccount. SQL exception", logFileName, e);
            }
            saveStatus = -1;
            Logger.logException(ex, logFileName);
        } finally {
            try {
                conn.setAutoCommit(true);
                dm.pool.returnConnection(conn, logFileName);
                Logger.logMessage("Database Transaction Committed Successfully (AccountID=" + iAccountID + ")", logFileName, Logger.LEVEL.DEBUG);
            } catch (SQLException ex) {
                Logger.logException(ex, logFileName);
            }
        }

        //Now the the "finally" is done and the database should be cleaned up
        //if the save did not occur properly, return the error to the API Client
        if (saveStatus < 1) {
            if (mainEx != null) {
                throw mainEx;
            } else {
                throw new MissingDataException("Error Saving Account.  Check logs for more details.", this.getTerminal().getId());
            }
        }

        this.getTerminal().logTransactionTime();

    }

    public static Boolean checkIfAccountCanSetSplitPayments(Integer accountId, TerminalModel terminalModel) {

        Logger.logMessage("Checking whether an account can manually elect the number of split payments in AccountModel.checkIfAccountCanSetSplitPayments", Logger.LEVEL.DEBUG);

        // default to faulse
        Boolean canSetSplitPayments = false;

        if (accountId != null && terminalModel != null && terminalModel.getId() != null) {
            ArrayList<HashMap> accountList = null;

            try {
                DataManager dm = new DataManager();

                accountList = dm.parameterizedExecuteQuery("data.posapi30.checkIfAccountCanSetSplitPayments",
                        new Object[]{
                                accountId,
                                terminalModel.getId()
                        },
                        PosAPIHelper.getLogFileName(terminalModel.getId()), true
                );
            }
            catch (Exception ex) {
                Logger.logMessage("Error trying to check whether an account manually elect the number of split payments in AccountModel.checkIfAccountCanSetSplitPayments", Logger.LEVEL.ERROR);
                Logger.logException(ex);
            }

            if (accountList != null && !accountList.isEmpty()) {
                HashMap accountHM = accountList.get(0);
                canSetSplitPayments = CommonAPI.convertModelDetailToBoolean(accountHM.get("SPLITPAYMENTSENABLED"), false);
            }
        }


        return canSetSplitPayments;
    }

    /**
     * Verify that if the account logged in to get their session, that the requested employeeId matches the employeeId that is on the session.
     * @param terminalModel
     * @throws Exception
     */
    private void verifyAccountAccess(TerminalModel terminalModel) throws Exception {
        if (terminalModel != null){
            if (CommonAPI.doesDsKeyContainEmpId(terminalModel.getLoginModel())){

                if (terminalModel.getLoginModel().getEmployeeId().compareTo(this.getId()) != 0) {
                    Logger.logMessage("Account is not authorized to complete this function.  The DSKey has an employee ID associated with it.  That account logged in and cannot affect a different account.", Logger.LEVEL.ERROR);
                    throw new InvalidAuthException("Account is not authorized to complete this function.");
                }
            }
        }
    }

    //region getters and setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getBadgeAlias() {
        return badgeAlias;
    }

    public void setBadgeAlias(String badgeAlias) {
        this.badgeAlias = badgeAlias;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getTerminalGroupBalance() {
        return terminalGroupBalance;
    }

    public void setTerminalGroupBalance(BigDecimal terminalGroupBalance) {
        this.terminalGroupBalance = terminalGroupBalance;
    }

    @JsonIgnore
    public Boolean isAccountPrePay() {
        Boolean isAccountPrePay = false;

        if (this.getAccountTypeId() != null) {
            switch (this.getAccountTypeId()) {
                //Inclining Balance
                //case 1: //Payroll deduction
                //case 3: //Billing Account
                //Declining Balance
                case 2: //Prepay Employee
                case 4: //Gift Card
                case 5: //Guest Account

                    //if the account is a prepaid type, return true
                    isAccountPrePay = true;
            }
        }

        return isAccountPrePay;
    }

    @JsonIgnore
    public Boolean hasGlobalCreditBalance() {
        Boolean hasGlobalCreditBal = false;

        if (this.getGlobalBalance() != null
                && this.getGlobalBalance().compareTo(BigDecimal.ZERO) == -1) {
            hasGlobalCreditBal = true;
        }

        return hasGlobalCreditBal;
    }

    public BigDecimal getGlobalBalance() {
        return globalBalance;
    }

    public void setGlobalBalance(BigDecimal globalBalance) {
        this.globalBalance = globalBalance;
    }

    public Integer getEligiblePayments() {
        return eligiblePayments;
    }

    public void setEligiblePayments(Integer eligiblePayments) {
        this.eligiblePayments = eligiblePayments;
    }

    public BigDecimal getGlobalLimit() {
        return globalLimit;
    }

    public void setGlobalLimit(BigDecimal globalLimit) {
        this.globalLimit = globalLimit;
    }

    public BigDecimal getSingleChargeLimit() {
        return singleChargeLimit;
    }

    public void setSingleChargeLimit(BigDecimal singleChargeLimit) {
        this.singleChargeLimit = singleChargeLimit;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public BigDecimal getTerminalGroupLimit() {
        return terminalGroupLimit;
    }

    public void setTerminalGroupLimit(BigDecimal terminalGroupLimit) {
        this.terminalGroupLimit = terminalGroupLimit;
    }

    /**
     * get the lowest available amount for the QC transaction
     * check:
     * Global Limit - Global Balance
     * Store Limit - Store Balance
     * Single Charge Limit
     * Available should always return a positive
     */
    public BigDecimal getAvailable() {
        return available;
    }

    public void setAvailable(BigDecimal available) {
        this.available = available;
    }

    @JsonIgnore
    public void initAvailable() {

        //For Third Party POS API account look-ups with no Terminal, we won't have any of the Terminal Group information
        //return all the values as null
        if (this.getSingleChargeLimit() == null &&
                this.getTerminalGroupBalance() == null &&
                this.getTerminalGroupLimit() == null &&
                this.getGlobalBalance() == null &&
                this.getGlobalLimit() == null) {

            setAvailable(null);
            return;
        }

        ArrayList<BigDecimal> listOfValues = new ArrayList<>();
        BigDecimal globalAvailable = BigDecimal.ZERO;
        BigDecimal storeAvailable = BigDecimal.ZERO;

        if (this.getAccountTypeId() != null) {
            //check for null values on each of these values
            globalAvailable = (this.getGlobalLimit() == null ? BigDecimal.ZERO : this.getGlobalLimit()).subtract(this.getGlobalBalance() == null ? BigDecimal.ZERO : this.getGlobalBalance());
            storeAvailable = (this.getTerminalGroupLimit() == null ? BigDecimal.ZERO : this.getTerminalGroupLimit()).subtract(this.getTerminalGroupBalance() == null ? BigDecimal.ZERO : this.getTerminalGroupBalance());
            listOfValues.add((this.getSingleChargeLimit()) == null ? BigDecimal.ZERO : this.getSingleChargeLimit());
            listOfValues.add(globalAvailable);
            listOfValues.add(storeAvailable);

            //sort in ascending order
            Collections.sort(listOfValues);

            //get the first value, which will be the lowest
            available = listOfValues.get(0);
            if (available.compareTo(BigDecimal.ZERO) == -1) {
                //if available is less than 1, override available to 0
                available = BigDecimal.ZERO;
            }
        }
    }

    /**
     * B-04416- Addition of Receipt Fields
     * For Pre-Paid accounts, flip the sign for the response.
     * ex - if the balances is $-25, then return $25 indicating that $25 is available
     * This method should be run after the account or transaction work is done.  So that the balances aren't affected during the transaction building processes
     */
    public void flipBalanceSignsForPrePayAcct() {

        if (this.isAccountPrePay()) {
            if (this.getGlobalBalance() != null) {
                this.setGlobalBalance(this.getGlobalBalance().negate());
            }
            if (this.getTerminalGroupBalance() != null) {
                this.setTerminalGroupBalance(this.getTerminalGroupBalance().negate());
            }
            if (this.getReceiptBalanceTypeId() != null) {
                PosAPIHelper.ReceiptBalanceType receiptBalanceType = PosAPIHelper.ReceiptBalanceType.convertIntToEnum(this.getReceiptBalanceTypeId());

                switch (receiptBalanceType) {
                    case STORE_BALANCE:
                    case GLOBAL_BALANCE:
                        this.setReceiptBalance(this.getReceiptBalance().negate());
                        break;
                }
            }
        }
    }

    public void initAccountNote(TerminalModel terminalModel) throws Exception {

        AccountNoteModel accountNoteModel = AccountNoteModel.getOneAccountNote(this.getId(), terminalModel);
        if (accountNoteModel != null) {
            this.getNotes().add(accountNoteModel);
        }
    }

    /**
     * Get Global balances for a QC Account
     *
     * @param terminalModel
     * @throws Exception
     */
    @JsonIgnore
    public void getBalancesForEmployee(TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> balanceModelList = dm.parameterizedExecuteQuery("data.posapi30.getEmployeeGlobalBalance",
                new Object[]{this.getId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        //returns GlobalLimit, GlobalBalance
        if (balanceModelList != null && balanceModelList.size() > 0) {
            HashMap modelDetailHM = balanceModelList.get(0);
            setGlobalLimit(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("GLOBALLIMIT")));
            setGlobalBalance(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("GLOBALBALANCE")));
        }

        if (!this.isAccountPrePay()) {
            TerminalGroupBalanceCollection terminalGroupBalanceCollection = TerminalGroupBalanceCollection.getAllTerminalGroupBalances(this.getId(), terminalModel);
            this.setTerminalGroupBalances(terminalGroupBalanceCollection.getCollection());
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<QcDiscountModel> getQcDiscountsAvailable() {
        return qcDiscountsAvailable;
    }

    public void setQcDiscountsAvailable(List<QcDiscountModel> qcDiscountsAvailable) {
        this.qcDiscountsAvailable = qcDiscountsAvailable;
    }

    public List<QcDiscountLineItemModel> getQcDiscountsApplied() {
        return qcDiscountsApplied;
    }

    public void setQcDiscountsApplied(List<QcDiscountLineItemModel> qcDiscountsApplied) {
        this.qcDiscountsApplied = qcDiscountsApplied;
    }

    @JsonIgnore
    public TerminalModel getTerminal() {
        return terminal;
    }

    public void setTerminal(TerminalModel terminal) {
        this.terminal = terminal;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getPayrollGroupingId() {
        return payrollGroupingId;
    }

    public void setPayrollGroupingId(Integer payrollGroupingId) {
        this.payrollGroupingId = payrollGroupingId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getReceiptBalanceTypeId() {
        return receiptBalanceTypeId;
    }

    public void setReceiptBalanceTypeId(Integer receiptBalanceTypeId) {
        this.receiptBalanceTypeId = receiptBalanceTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getReceiptBalanceLabel() {
        return receiptBalanceLabel;
    }

    public void setReceiptBalanceLabel(String receiptBalanceLabel) {
        this.receiptBalanceLabel = receiptBalanceLabel;
    }

    //Determines what balance to show on the receipt
    public BigDecimal getReceiptBalance() {
        return receiptBalance;
    }

    public void setReceiptBalance(BigDecimal receiptBalance) {
        this.receiptBalance = receiptBalance;
    }

    public void initReceiptBalance() {
        if (this.getReceiptBalanceTypeId() != null) {
            PosAPIHelper.ReceiptBalanceType receiptBalanceType = PosAPIHelper.ReceiptBalanceType.convertIntToEnum(this.getReceiptBalanceTypeId());

            switch (receiptBalanceType) {
                case STORE_BALANCE:
                    receiptBalance = this.getTerminalGroupBalance();
                    break;
                case STORE_AVAILABLE:
                    receiptBalance = (this.getTerminalGroupLimit() == null ? BigDecimal.ZERO : this.getTerminalGroupLimit()).subtract(this.getTerminalGroupBalance() == null ? BigDecimal.ZERO : this.getTerminalGroupBalance());
                    break;
                case GLOBAL_BALANCE:
                    receiptBalance = this.getGlobalBalance();
                    break;
                case GLOBAL_AVAILABLE:
                    receiptBalance = (this.getGlobalLimit() == null ? BigDecimal.ZERO : this.getGlobalLimit()).subtract(this.getGlobalBalance() == null ? BigDecimal.ZERO : this.getGlobalBalance());
                    break;
            }
        }
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getBadgeAssignmentId() {
        return badgeAssignmentId;
    }

    public void setBadgeAssignmentId(Integer badgeAssignmentId) {
        this.badgeAssignmentId = badgeAssignmentId;
    }

    //@JsonIgnore
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Integer getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(Integer accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    //QC_Employees.TerminalProfileID
    public Integer getSpendingProfileId() {
        return spendingProfileId;
    }

    public void setSpendingProfileId(Integer spendingProfileId) {
        this.spendingProfileId = spendingProfileId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Boolean getAutoCloudInvite() {
        return autoCloudInvite;
    }

    public void setAutoCloudInvite(Boolean autoCloudInvite) {
        this.autoCloudInvite = autoCloudInvite;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<AccountNoteModel> getNotes() {
        return notes;
    }

    public void setNotes(List<AccountNoteModel> notes) {
        this.notes = notes;
    }

    @JsonIgnore
    public Boolean isNewAccount() {
        return isNewAccount;
    }

    public void setIsNewAccount(Boolean newAccount) {
        isNewAccount = newAccount;
    }

    @JsonIgnore
    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    @JsonIgnore
    public AccountModel getOriginalAccountModel() {
        return originalAccountModel;
    }

    public void setOriginalAccountModel(AccountModel originalAccountModel) {
        this.originalAccountModel = originalAccountModel;
    }

    @JsonIgnore
    public HashMap getChangedFieldsHM() {
        return changedFieldsHM;
    }

    public void setChangedFields(HashMap changedFieldsHM) {
        this.changedFieldsHM = changedFieldsHM;
    }

    @JsonIgnore
    public List<SpendingProfileModel> getSpendingProfileModelList() {
        return spendingProfileModelList;
    }

    public void setSpendingProfileModelList(List<SpendingProfileModel> spendingProfileModelList) {
        this.spendingProfileModelList = spendingProfileModelList;
    }

    @JsonIgnore
    public List<AccountGroupModel> getAccountGroupModelList() {
        return accountGroupModelList;
    }

    public void setAccountGroupModelList(List<AccountGroupModel> accountGroupModelList) {
        this.accountGroupModelList = accountGroupModelList;
    }

    @JsonIgnore
    public Boolean getNonNoteUpdateRequested() {
        return nonNoteUpdateRequested;
    }

    public void setNonNoteUpdateRequested(Boolean nonNoteUpdateRequested) {
        this.nonNoteUpdateRequested = nonNoteUpdateRequested;
    }

    @JsonIgnore
    public Integer getSpendingProfileMappedForTerminal() {
        return spendingProfileMappedForTerminal;
    }

    public void setSpendingProfileMappedForTerminal(Integer spendingProfileMappedForTerminal) {
        this.spendingProfileMappedForTerminal = spendingProfileMappedForTerminal;
    }

    @JsonIgnore
    public Boolean tosCheckAlreadyDone() {
        return tosCheckAlreadyDone;
    }

    public void setTosCheckAlreadyDone(Boolean tosCheckAlreadyDone) {
        this.tosCheckAlreadyDone = tosCheckAlreadyDone;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @JsonIgnore
    public LoginModel getLoginModel() {
        return loginModel;
    }

    public void setLoginModel(LoginModel loginModel) {
        this.loginModel = loginModel;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public List<TerminalGroupBalanceModel> getTerminalGroupBalances() {
        return terminalGroupBalances;
    }

    public void setTerminalGroupBalances(List<TerminalGroupBalanceModel> terminalGroupBalances) {
        this.terminalGroupBalances = terminalGroupBalances;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public AccountGroupModel getAccountGroup() {
        return accountGroup;
    }

    public void setAccountGroup(AccountGroupModel accountGroup) {
        this.accountGroup = accountGroup;
    }

    public Boolean getTerminalMappedToSpendingProfile() {
        if (this.getSpendingProfileMappedForTerminal() == null){
            terminalMappedToSpendingProfile = false;
        } else {
            terminalMappedToSpendingProfile = true;
        }

        return terminalMappedToSpendingProfile;
    }

    @JsonInclude
    public Integer getRestrictionProfileId() {
        return restrictionProfileId;
    }

    public void setRestrictionProfileId(Integer restrictionProfileId) {
        this.restrictionProfileId = restrictionProfileId;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getEmployeeLowBalanceThreshold() {
        return employeeLowBalanceThreshold;
    }

    public void setEmployeeLowBalanceThreshold(String employeeLowBalanceThreshold) {
        this.employeeLowBalanceThreshold = employeeLowBalanceThreshold;
    }

    public Boolean isRedacted() {
        return redacted;
    }

    public void setRedacted(Boolean redacted) {
        this.redacted = redacted;
    }

    //endregion

}
