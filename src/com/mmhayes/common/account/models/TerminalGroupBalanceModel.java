package com.mmhayes.common.account.models;

//MMHayes Dependencies
import com.mmhayes.common.api.CommonAPI;

//Other Dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-08-29 11:12:59 -0400 (Wed, 29 Aug 2018) $: Date of last commit
 $Rev: 7701 $: Revision of last commit
*/
public class TerminalGroupBalanceModel {

    private Integer id = null;
    private String name = "";
    private BigDecimal limit = null;
    private BigDecimal balance = null;
    private BigDecimal available = null;
    private BigDecimal singleChargeLimit = null;

    private ArrayList<RevenueCenterBalanceModel> revenueCenterBalances = new ArrayList<>();

    public TerminalGroupBalanceModel(){

    }

    public TerminalGroupBalanceModel(HashMap modelDetailHM) throws Exception {
        setModelProperties(modelDetailHM);
    }

    public TerminalGroupBalanceModel setModelProperties(HashMap modelDetailHM) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("TERMINALGROUPID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("TERMINALGROUPNAME")));
        setLimit(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("TERMINALGROUPLIMIT")));
        setBalance(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("TERMINALGROUPBALANCE")));
        setAvailable(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("TERMINALGROUPAVAILABLE")));
        setSingleChargeLimit(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("SINGLECHARGELIMIT")));

        return this;
    }

    //region Getters/Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getLimit() {
        return limit;
    }

    public void setLimit(BigDecimal limit) {
        this.limit = limit;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getAvailable() {
        return available;
    }

    public void setAvailable(BigDecimal available) {
        this.available = available;
    }

    public BigDecimal getSingleChargeLimit() {
        return singleChargeLimit;
    }

    public void setSingleChargeLimit(BigDecimal singleChargeLimit) {
        this.singleChargeLimit = singleChargeLimit;
    }

    public ArrayList<RevenueCenterBalanceModel> getRevenueCenterBalances() {
        return revenueCenterBalances;
    }

    public void setRevenueCenterBalances(ArrayList<RevenueCenterBalanceModel> revenueCenterBalances) {
        this.revenueCenterBalances = revenueCenterBalances;
    }

    //endregion
}
