package com.mmhayes.common.account.models;

//API Dependencies

import com.mmhayes.common.api.CommonAPI;

//Other Dependencies
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-09-14 09:08:08 -0400 (Fri, 14 Sep 2018) $: Date of last commit
 $Rev: 7783 $: Revision of last commit
*/

public class SpendingProfileModel {

    private Integer id = null;
    private String name = "";
    private String accountType = "";
    private Integer accountTypeId = null;

    public SpendingProfileModel() {

    }

    public static SpendingProfileModel createSpendingProfileModel(HashMap spendingProfileHM) {
        SpendingProfileModel spendingProfileModel = new SpendingProfileModel();
        spendingProfileModel.setModelDetails(spendingProfileHM);
        return spendingProfileModel;
    }

    private SpendingProfileModel setModelDetails(HashMap spendingProfileHM) {
        setId(CommonAPI.convertModelDetailToInteger(spendingProfileHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(spendingProfileHM.get("NAME")));
        setAccountType(CommonAPI.convertModelDetailToString(spendingProfileHM.get("ACCOUNTTYPE")));
        setAccountTypeId(CommonAPI.convertModelDetailToInteger(spendingProfileHM.get("ACCOUNTTYPEID")));

        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Integer getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(Integer accountTypeId) {
        this.accountTypeId = accountTypeId;
    }
}
