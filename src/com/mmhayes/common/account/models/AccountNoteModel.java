package com.mmhayes.common.account.models;

//MMHayes Dependencies

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;

//API Dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;

 /*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2019-07-02 08:12:14 -0400 (Tue, 02 Jul 2019) $: Date of last commit
 $Rev: 9024 $: Revision of last commit
*/

public class AccountNoteModel {
    private Integer id = null;
    private String note = "";
    private Integer revenueCenterId = null;

    public AccountNoteModel(){

    }

    public static AccountNoteModel getOneAccountNote(Integer employeeId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();
        AccountNoteModel accountNoteModel = null;

        //get all models in an array list
        ArrayList<HashMap> accountNoteList = dm.parameterizedExecuteQuery("data.posapi30.getOneAccountNote",
                new Object[]{employeeId, terminalModel.getRevenueCenterId()},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        if (accountNoteList != null && accountNoteList.size() > 0){
            accountNoteModel = new AccountNoteModel();
            accountNoteModel = accountNoteModel.setModelProperties(accountNoteList.get(0));
        }

        return accountNoteModel;
    }

    //setter for all of this model's properties
    public AccountNoteModel setModelProperties(HashMap modelDetailHM) {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setNote(CommonAPI.convertModelDetailToString(modelDetailHM.get("NOTES")));
        setRevenueCenterId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("REVENUECENTERID")));

        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getRevenueCenterId() {
        return revenueCenterId;
    }

    public void setRevenueCenterId(Integer revenueCenterId) {
        this.revenueCenterId = revenueCenterId;
    }
}
