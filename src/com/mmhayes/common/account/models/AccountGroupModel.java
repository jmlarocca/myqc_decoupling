package com.mmhayes.common.account.models;

//API Dependencies
import com.fasterxml.jackson.annotation.JsonInclude;import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionSerializer;
import com.mmhayes.common.api.json.serializers.StringXSSPreventionDeserializer;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.transaction.models.TaxModel;
import com.mmhayes.common.utils.Logger;

//Other Dependencies
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2021-04-15 12:28:51 -0400 (Thu, 15 Apr 2021) $: Date of last commit
 $Rev: 13795 $: Revision of last commit
*/

public class AccountGroupModel {

    private Integer id = null;
    private String name = "";
    private String accountType = "";
    private Integer accountTypeId = null;
    private Boolean requiresSecondaryAcct = false;
    private String secondaryAcctPromptText = "";

    private List<SpendingProfileModel> spendingProfiles = new ArrayList();
    private List<TaxModel> taxDeletes = new ArrayList<>();

    public AccountGroupModel(){

    }

    public AccountGroupModel(HashMap accountGroupHM){
        setModelDetails(accountGroupHM);
    }

    private AccountGroupModel setModelDetails(HashMap accountGroupHM){
        setId(CommonAPI.convertModelDetailToInteger(accountGroupHM.get("PAYROLLGROUPINGID")));
        setName(CommonAPI.convertModelDetailToString(accountGroupHM.get("PAYROLLGROUPINGNAME")));
        setRequiresSecondaryAcct(CommonAPI.convertModelDetailToBoolean(accountGroupHM.get("REQUIRESSECONDARYACCT")));
        setSecondaryAcctPromptText(CommonAPI.convertModelDetailToString(accountGroupHM.get("SECONDARYACCTPROMPTTEXT")));
        return this;
    }

    public static AccountGroupModel getOneAccountGroupModelById(Integer accountGroupId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> accountGroupList = dm.parameterizedExecuteQuery("data.posapi30.getOneAccountGroupById",
                new Object[]{accountGroupId},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(accountGroupList, "Account Group Not Found", terminalModel.getId());

        CommonAPI.checkIsNullOrEmptyObject(accountGroupList.get(0).get("PAYROLLGROUPINGID"), "Account Group could not be found", terminalModel.getId());

        Integer accountGroupID = CommonAPI.convertModelDetailToInteger(accountGroupList.get(0).get("PAYROLLGROUPINGID"));

        int tempId = -1;
        AccountGroupModel accountGroupModel = null;
        for (HashMap accountGroupHM : accountGroupList) {
            CommonAPI.checkIsNullOrEmptyObject(accountGroupHM.get("PAYROLLGROUPINGID"), "Account Group could not be found", terminalModel.getId());

            //create a new model and add to the collection
            if (accountGroupID.equals(CommonAPI.convertModelDetailToInteger(accountGroupHM.get("PAYROLLGROUPINGID")))) {

                if(tempId != CommonAPI.convertModelDetailToInteger(accountGroupHM.get("PAYROLLGROUPINGID"))) {
                    accountGroupModel = new AccountGroupModel(accountGroupHM);

                    accountGroupModel.setAccountType(CommonAPI.convertModelDetailToString(accountGroupHM.get("PAYROLLGROUPACCTTYPE")));
                    accountGroupModel.setAccountTypeId(CommonAPI.convertModelDetailToInteger(accountGroupHM.get("PAYROLLGROUPACCTTYPEID"))); //These are fields that identify the Account Type

                    //create a new spending profile object
                    accountGroupModel.setSpendingProfileModel(accountGroupHM, terminalModel);

                    //create tax delete rate models
                    accountGroupModel.setTaxDeleteRateModels(accountGroupHM, terminalModel);

                    tempId = CommonAPI.convertModelDetailToInteger(accountGroupHM.get("PAYROLLGROUPINGID"));
                } else {

                    //create a new spending profile object
                    if (accountGroupModel  != null) {
                        accountGroupModel.setSpendingProfileModel(accountGroupHM, terminalModel);
                    }
                }
            }
        }

        return accountGroupModel;
    }

    //create the spending profile model, check if it is already on the account group before adding it
    public void setSpendingProfileModel(HashMap accountGroupHM, TerminalModel terminalModel) throws Exception {

        //create a new spending profile object
        if (CommonAPI.convertModelDetailToInteger(accountGroupHM.get("TERMINALPROFILEID")) != null
                && !CommonAPI.convertModelDetailToInteger(accountGroupHM.get("TERMINALPROFILEID")).toString().isEmpty()) {
            SpendingProfileModel spendingProfileModel = new SpendingProfileModel();
            spendingProfileModel.setId(CommonAPI.convertModelDetailToInteger(accountGroupHM.get("TERMINALPROFILEID")));
            spendingProfileModel.setName(CommonAPI.convertModelDetailToString(accountGroupHM.get("SPENDINGPROFILENAME")));
            spendingProfileModel.setAccountType(CommonAPI.convertModelDetailToString(accountGroupHM.get("TERMINALPROFILEACCTTYPE")));
            spendingProfileModel.setAccountTypeId(CommonAPI.convertModelDetailToInteger(accountGroupHM.get("TERMINALPROFILEACCTTYPEID")));

            //only add the spending profile if it is not already in the account group model
            if( !this.containsSpendingProfile(spendingProfileModel) ) {
                this.getSpendingProfiles().add(spendingProfileModel);
            }
        }
    }

    //get all the tax delete rate ids as a list of string, build tax delete models based on ID
    public void setTaxDeleteRateModels(HashMap accountGroupHM, TerminalModel terminalModel) throws Exception {

        String taxDeleteRateIDStr = CommonAPI.convertModelDetailToString(accountGroupHM.get("TAXDELETERATEIDS"));

        if(taxDeleteRateIDStr.equals("")) {
            return;
        }

        List<String> taxDeleteRateIDs = new ArrayList<String>(Arrays.asList(taxDeleteRateIDStr.split(",")));

        for(String taxDeleteRateID : taxDeleteRateIDs) {
            Integer taxDeleteRateId = Integer.parseInt(taxDeleteRateID);
            TaxModel taxDeleteModel = new TaxModel();

            try {
                taxDeleteModel = TaxModel.getTaxModelFromCache(taxDeleteRateId, terminalModel.getId(), false);
            } catch (Exception e) {
                taxDeleteModel = new TaxModel();
                taxDeleteModel.setId(taxDeleteRateId);
                Logger.logMessage("Tax was not found in Tax Cache.  Only the ID will be populated", PosAPIHelper.getLogFileName(terminalModel.getId()));
            }
            this.getTaxDeletes().add(taxDeleteModel);
        }
    }

    public boolean containsSpendingProfile(SpendingProfileModel spendingProfileModel) {
        for(SpendingProfileModel spendingProfile : this.getSpendingProfiles()) {
            if(spendingProfile.getId().equals(spendingProfileModel.getId())) {
                return true;
            }
        }

        return false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public List<SpendingProfileModel> getSpendingProfiles() {
        return spendingProfiles;
    }

    public void setSpendingProfiles(List<SpendingProfileModel> spendingProfiles) {
        this.spendingProfiles = spendingProfiles;
    }

    public Integer getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(Integer accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    public Boolean getRequiresSecondaryAcct() {
        return requiresSecondaryAcct;
    }

    public void setRequiresSecondaryAcct(Boolean requiresSecondaryAcct) {
        this.requiresSecondaryAcct = requiresSecondaryAcct;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonSerialize(using = StringXSSPreventionSerializer.class)
    @JsonDeserialize(using = StringXSSPreventionDeserializer.class)
    public String getSecondaryAcctPromptText() {
        return secondaryAcctPromptText;
    }

    public void setSecondaryAcctPromptText(String secondaryAcctPromptText) {
        this.secondaryAcctPromptText = secondaryAcctPromptText;
    }

    public List<TaxModel> getTaxDeletes() {
        return taxDeletes;
    }

    public void setTaxDeletes(List<TaxModel> taxDeletes) {
        this.taxDeletes = taxDeletes;
    }

}
