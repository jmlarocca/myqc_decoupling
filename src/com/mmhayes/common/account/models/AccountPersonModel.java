package com.mmhayes.common.account.models;

//Other Dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2018-10-30 08:58:30 -0400 (Tue, 30 Oct 2018) $: Date of last commit
 $Rev: 7985 $: Revision of last commit
*/

public class AccountPersonModel {

    private Integer id = null;
    private Integer personID = null;
    private Integer employeeID = null;
    private Integer personRelationshipID = null;
    private String name = "";
    private boolean active = false;
    private AccountModel accountModel = new AccountModel();
    private String result = "";
    private static DataManager dm = new DataManager();

    public AccountPersonModel(){

    }

    public AccountPersonModel(HashMap accountPersonHM){

        setModelProperties(accountPersonHM);

    }

    private AccountPersonModel setModelProperties(HashMap accountPersonHM){

        setId(CommonAPI.convertModelDetailToInteger(accountPersonHM.get("ID")));
        setName(CommonAPI.convertModelDetailToString(accountPersonHM.get("NAME")));
        setPersonID(CommonAPI.convertModelDetailToInteger(accountPersonHM.get("PERSONID")));
        setEmployeeID(CommonAPI.convertModelDetailToInteger(accountPersonHM.get("EMPLOYEEID")));
        setPersonRelationshipID(CommonAPI.convertModelDetailToInteger(accountPersonHM.get("PERSONRELATIONSHIPID")));
        setActive(CommonAPI.convertModelDetailToBoolean(accountPersonHM.get("ACTIVE")));

        if(getEmployeeID() != null && getEmployeeID() > 0) {
            AccountModel accountModel = new AccountModel(getEmployeeID());
            setAccountModel(accountModel);
        }

        return this;
    }

    public AccountPersonModel findEmployee(HashMap accountPersonHM){
        String firstName = CommonAPI.convertModelDetailToString(accountPersonHM.get("firstName"));
        firstName = firstName.replace( (char)8217, '\'' ).replace((char) 8216, '\'');
        String lastName = CommonAPI.convertModelDetailToString(accountPersonHM.get("lastName"));
        lastName = lastName.replace( (char)8217, '\'' ).replace( (char)8216, '\'' );
        String middleInitial = CommonAPI.convertModelDetailToString(accountPersonHM.get("middleInitial"));
        String studentID = CommonAPI.convertModelDetailToString(accountPersonHM.get("studentID"));  //employee number
        boolean hasMiddle = (middleInitial != null && middleInitial.length() > 0);

        String name = firstName + ' ' + lastName;
        String nameWithComma = lastName + ", " + firstName;
        String nameWithInitial = firstName + ' ' + middleInitial + ' ' + lastName;
        String nameWithCommaAndInitial = lastName + ", " + firstName + ' ' + middleInitial;
        String nameWithInitialPeriod = firstName + ' ' + middleInitial + "." + ' ' + lastName;
        String nameWithCommaAndInitialPeriod = lastName + ", " + firstName + ' ' + middleInitial + ".";
        String nameWithFullMiddle = firstName + ' ' + middleInitial + "% " + lastName;
        String nameWithCommaAndFullMiddle = lastName + ", " + firstName + ' ' + middleInitial + "%";

        setId(1);
        //get all models in an array list
        ArrayList<HashMap> foundEmployee = new ArrayList<HashMap>();

        if ( hasMiddle ) {
            foundEmployee = dm.parameterizedExecuteQuery("data.posapi30.findEmployeeWithMiddleName",
                    new Object[]{
                            name,
                            nameWithComma,
                            nameWithInitial,
                            nameWithCommaAndInitial,
                            nameWithInitialPeriod,
                            nameWithCommaAndInitialPeriod,
                            nameWithFullMiddle,
                            nameWithCommaAndFullMiddle,
                            studentID
                    },
                    true
            );
        } else {
            foundEmployee = dm.parameterizedExecuteQuery("data.posapi30.findEmployee",
                    new Object[]{
                            name,
                            nameWithComma,
                            studentID
                    },
                    true
            );
        }

        if (foundEmployee != null && foundEmployee.size() > 0) {
            //if employee is found, get out details and set employeeID
            HashMap foundEmployeeHM = foundEmployee.get(0);
            Integer employeeID = CommonAPI.convertModelDetailToInteger(foundEmployeeHM.get("EMPLOYEEID"));
            this.setEmployeeID(employeeID);

            //get personID to verify mapping
            Integer personID = CommonAPI.convertModelDetailToInteger(accountPersonHM.get("personID"));

            //check the accountPerson mapping
            String isMappingActive = doesActiveMappingExist(employeeID, personID);

            if ( isMappingActive.equals("true") ) { //active mapping exists
                this.setResult("activeMapping");
            } else if ( isMappingActive.equals("inactive") ) { //mapping exists but is inactive
                this.setResult("inactiveMapping");
            } else { //found employee but mapping does not exist
                this.setResult("true");
            }

            //checks if the account type is a gift card account, can't add account if its gift card
            String accountType = CommonAPI.checkForGiftCardAccountType(employeeID);
            if(!accountType.equals("") && accountType.equals("4")) {
                this.setResult("gift-card");
            }

        //QC_Employee does not exist
        } else {
            this.setResult("false");
        }

        return this;
    }

    //returns "false" if no mapping exists, "inactive" if a mapping exists but is inactive, and "true" if an active mapping exists
    public String doesActiveMappingExist(Integer employeeID, Integer personID) {
        //get all models in an array list
        ArrayList<HashMap> foundEmployeeMapping = dm.parameterizedExecuteQuery("data.posapi30.findActiveEmployeePersonMapping",
            new Object[]{
                employeeID,
                personID
            },
            true
        );

        //no mapping found, return false
        if (foundEmployeeMapping == null || foundEmployeeMapping.size() == 0) {
            return "false";
        }

        //mapping found, get out details
        HashMap foundMappingHM = foundEmployeeMapping.get(0);

        //grab and set the relationship
        if(foundMappingHM.get("PERSONRELATIONSHIPID") != null && !foundMappingHM.get("PERSONRELATIONSHIPID").toString().equals("")) {
            setPersonRelationshipID(Integer.parseInt(foundMappingHM.get("PERSONRELATIONSHIPID").toString()));
        }

        //mapping found and is active
        if(foundMappingHM.get("ACTIVE") != null && foundMappingHM.get("ACTIVE").toString().equals("true")) {
            return "true";
        }

        //mapping found, but inactive
        return "inactive";
    }

    //updates employee to person mapping record or creates a new one
    public Integer employeePersonMapping(HashMap accountPersonHM) {
        Integer updateResult = null;

        String name = CommonAPI.convertModelDetailToString(accountPersonHM.get("name"));
        String employeeID = CommonAPI.convertModelDetailToString(accountPersonHM.get("employeeID"));
        String personID = CommonAPI.convertModelDetailToString(accountPersonHM.get("personID"));  //employee number
        String personRelationshipID = CommonAPI.convertModelDetailToString(accountPersonHM.get("personRelationshipID"));
        boolean inactiveMapping = CommonAPI.convertModelDetailToBoolean(accountPersonHM.get("inactiveMapping"));
        boolean newMapping = CommonAPI.convertModelDetailToBoolean(accountPersonHM.get("newMapping"));


        if(inactiveMapping) {
            updateResult = updateExistingMapping(employeeID, personID, personRelationshipID);
        } else if (newMapping) {
            updateResult = createNewPersonMapping(employeeID, personID, personRelationshipID, name);
        }
        return updateResult;
    }

    //reactivate employee to person mapping and reset the relationship status
    public Integer updateExistingMapping(String employeeID, String personID, String personRelationshipID) {

        Integer updateResult = dm.parameterizedExecuteNonQuery("data.ordering.updateExistingMapping",
                new Object[]{
                        employeeID,
                        personID,
                        personRelationshipID
                }
        );

        if (updateResult != 1) {
            Logger.logMessage("Could not update existing mapping in AccountPersonModel.updateExistingMapping()", Logger.LEVEL.ERROR);
        }

        return updateResult;
    }

    //create new employee to person mapping, record change in QC_EmployeeChanges
    public Integer createNewPersonMapping(String employeeID, String personID, String personRelationshipID, String name) {

        //prepare sql parameters and then execute insert SQL
        Integer insertResult = dm.parameterizedExecuteNonQuery("data.posapi30.createNewMapping",
                new Object[]{
                        employeeID,
                        personID,
                        personRelationshipID
                }
        );

        //if the insert was a success
        if (insertResult != 1) {
            Logger.logMessage("Could not execute insert SQL in AccountPersonModel.createNewMapping", Logger.LEVEL.ERROR);
            //throw new MissingDataException("Could not execute insert SQL for employee person mapping");
        }  else {

            //create new account record in QC_EmployeeChanges
            if (!recordPersonAccountEmployeeChanges(employeeID, name)) {
                Logger.logMessage("ERROR: Error recording account change in createNewPersonMapping for field: Person Account", Logger.LEVEL.ERROR);
            }

        }
        return insertResult;
    }

    //record changes to account group for an account in the QC_EmployeeChanges table
    public boolean recordPersonAccountEmployeeChanges(String employeeID, String name) {
        try {
            return dm.serializeUpdate("data.myqc.recordPersonAccountChangeAsMyQCUser", new Object[] {employeeID, name}) != -1;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: Error recording person account change in QC_EmployeeChanges in AccountPersonModel.recordPersonAccountEmployeeChanges", Logger.LEVEL.ERROR);
            Logger.logException(ex); //always log exceptions
            return false;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    public Integer getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(Integer employeeID) {
        this.employeeID = employeeID;
    }

    public Integer getPersonRelationshipID() {
        return personRelationshipID;
    }

    public void setPersonRelationshipID(Integer personRelationshipID) {
        this.personRelationshipID = personRelationshipID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public AccountModel getAccountModel() {
        return accountModel;
    }

    public void setAccountModel(AccountModel accountModel) {
        this.accountModel = accountModel;
    }


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
