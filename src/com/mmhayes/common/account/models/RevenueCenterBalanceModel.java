package com.mmhayes.common.account.models;

//MMHayes Dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.terminal.models.RevenueCenterModel;

//Other Dependencies
import java.math.BigDecimal;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-08-29 11:12:59 -0400 (Wed, 29 Aug 2018) $: Date of last commit
 $Rev: 7701 $: Revision of last commit
*/
public class RevenueCenterBalanceModel {

    private BigDecimal balance = null;
    private RevenueCenterModel revenueCenter = null;

    public RevenueCenterBalanceModel(){

    }

    public RevenueCenterBalanceModel(RevenueCenterModel revenueCenter, BigDecimal balance){
        this.setRevenueCenter(revenueCenter);
        this.setBalance(balance);
    }

    public RevenueCenterBalanceModel setModelProperties(HashMap modelDetailHM) throws Exception {
        setBalance(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("BALANCE")));

        setRevenueCenter(new RevenueCenterModel().setModelProperties(modelDetailHM));

        return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public RevenueCenterModel getRevenueCenter() {
        return revenueCenter;
    }

    public void setRevenueCenter(RevenueCenterModel revenueCenter) {
        this.revenueCenter = revenueCenter;
    }
}
