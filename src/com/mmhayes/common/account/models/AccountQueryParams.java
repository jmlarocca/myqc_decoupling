package com.mmhayes.common.account.models;

//mmhayes dependencies

import com.mmhayes.common.utils.Logger;

//API dependencies
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

//Other Dependencies
import org.apache.commons.lang3.StringUtils;

/*
 Last Updated (automatically updated by SVN)
 $Author: mjbatko $: Author of last commit
 $Date: 2021-06-28 08:23:25 -0400 (Mon, 28 Jun 2021) $: Date of last commit
 $Rev: 14230 $: Revision of last commit
*/
public class AccountQueryParams {

    private boolean ignoreAccountStatus = false;
    private boolean ignoreRedacted = false;
    private Integer terminalId = null;
    private Integer badgeAssignmentId = null;
    private String clientIdentifier = null;

    public AccountQueryParams() {

    }

    public AccountQueryParams(Boolean ignoreAccountStatus) {
        this(ignoreAccountStatus, false, null);
    }

    public AccountQueryParams(Boolean ignoreAccountStatus, Boolean ignoreRedacted) {
        this(ignoreAccountStatus, ignoreRedacted, null);
    }

    public AccountQueryParams(Boolean ignoreAccountStatus, Boolean ignoreRedacted, Integer badgeAssignmentId){
        this.setIgnoreAccountStatus(ignoreAccountStatus);
        this.setIgnoreRedacted(ignoreRedacted);
        this.setBadgeAssignmentId(badgeAssignmentId);
    }

    //populates the passed in paginationParamsHM with HTTP GET parameters in the uriInfo
    public AccountQueryParams populateInactiveParamFromURI(@Context UriInfo uriInfo) {
        AccountQueryParams accountQueryParams = new AccountQueryParams();

        try {
            MultivaluedMap<String, String> httpGetParams = uriInfo.getQueryParameters();
            if (httpGetParams != null
                    && !httpGetParams.isEmpty()
                    && httpGetParams.get("ignoreaccountstatus") != null
                    && httpGetParams.get("ignoreaccountstatus").get(0) != null
                    && !httpGetParams.get("ignoreaccountstatus").get(0).toString().isEmpty()
                    && httpGetParams.get("ignoreaccountstatus").get(0).toString().equalsIgnoreCase("yes")) {

                accountQueryParams.setIgnoreAccountStatus(true);
            }

            if (httpGetParams != null
                    && !httpGetParams.isEmpty()
                    && httpGetParams.get("clientidentifier") != null
                    && httpGetParams.get("clientidentifier").get(0) != null
                    && !httpGetParams.get("clientidentifier").get(0).toString().isEmpty()
                    ) {

                accountQueryParams.setClientIdentifier(httpGetParams.get("clientidentifier").get(0).toString());
            }

            if (httpGetParams != null
                    && !httpGetParams.isEmpty()
                    && httpGetParams.get("terminalid") != null
                    && httpGetParams.get("terminalid").get(0) != null
                    && !httpGetParams.get("terminalid").get(0).toString().isEmpty()
                    && StringUtils.isNumeric(httpGetParams.get("terminalid").get(0).toString())) //must be a number
            {

                accountQueryParams.setTerminalId(Integer.parseInt(httpGetParams.get("terminalid").get(0).toString()));
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
        }
        return accountQueryParams;
    }

    public boolean isIgnoreAccountStatus() {
        return ignoreAccountStatus;
    }

    public void setIgnoreAccountStatus(boolean ignoreAccountStatus) {
        this.ignoreAccountStatus = ignoreAccountStatus;
    }

    public Integer getBadgeAssignmentId() {
            return badgeAssignmentId;
    }
    
    public void setBadgeAssignmentId(Integer badgeAssignmentId) {
        this.badgeAssignmentId = badgeAssignmentId;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public String getClientIdentifier() {
        return clientIdentifier;
    }

    public void setClientIdentifier(String clientIdentifier) {
        this.clientIdentifier = clientIdentifier;
    }

    public boolean isIgnoreRedacted() {
        return ignoreRedacted;
    }

    public void setIgnoreRedacted(boolean ignoreRedacted) {
        this.ignoreRedacted = ignoreRedacted;
    }
}
