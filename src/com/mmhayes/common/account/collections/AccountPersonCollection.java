package com.mmhayes.common.account.collections;

//MMHayes Dependencies

import com.mmhayes.common.account.models.AccountPersonModel;
import com.mmhayes.common.dataaccess.DataManager;

import java.util.ArrayList;
import java.util.HashMap;

//API Dependencies
//Other Dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-06-16 12:44:46 -0400 (Fri, 16 Jun 2017) $: Date of last commit
 $Rev: 7566 $: Revision of last commit
*/
public class AccountPersonCollection {

    private ArrayList<AccountPersonModel> collection = new ArrayList<>();
    private ArrayList<HashMap> relationships = new ArrayList<>();
    private static DataManager dm = new DataManager();

    //constructor
    public AccountPersonCollection() {

        populateRelationships();
    }

    //constructor
    public AccountPersonCollection(String personID) {

        populateAccountPersonCollection(personID);
    }

    public AccountPersonCollection populateAccountPersonCollection(String personID) {

        //get all models in an array list
        ArrayList<HashMap> accountPersonList = dm.parameterizedExecuteQuery("data.posapi30.getPersonAccounts",
                new Object[]{
                        personID
                },
                true
        );


        if (accountPersonList != null && accountPersonList.size() > 0) {
            for (HashMap accountPersonHM : accountPersonList) {
                //create a new model and add to the collection
                getCollection().add(new AccountPersonModel(accountPersonHM));
            }
        }

        return this;
    }

    //populates arraylist with relationship types
    public AccountPersonCollection populateRelationships() {

        //get all models in an array list
        ArrayList<HashMap> relationshipList = dm.parameterizedExecuteQuery("data.posapi30.getRelationshipTypes", new Object[]{}, true);

        if (relationshipList != null && relationshipList.size() > 0) {
            for (HashMap relationshipHM : relationshipList) {
                getRelationships().add(relationshipHM);
            }
        }

        return this;
    }


    //getter
    public ArrayList<AccountPersonModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(ArrayList<AccountPersonModel> collection) {
        this.collection = collection;
    }

    public ArrayList<HashMap> getRelationships() {
        return relationships;
    }

    public void setRelationships(ArrayList<HashMap> relationships) {
        this.relationships = relationships;
    }
}
