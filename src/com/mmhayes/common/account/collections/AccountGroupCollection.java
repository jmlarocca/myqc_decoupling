package com.mmhayes.common.account.collections;

//MMHayes Dependencies

import com.mmhayes.common.account.models.*;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;

//API Dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2017-06-16 12:44:46 -0400 (Fri, 16 Jun 2017) $: Date of last commit
 $Rev: 9536 $: Revision of last commit
*/
public class AccountGroupCollection {
    private ArrayList<AccountGroupModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    //constructor
    public AccountGroupCollection() {

    }

    public static AccountGroupCollection getAllActiveAccountGroups(TerminalModel terminalModel) throws Exception {
        AccountGroupCollection accountGroupCollection = new AccountGroupCollection();


        //get all models in an array list
        ArrayList<HashMap> accountGroupList = dm.parameterizedExecuteQuery("data.posapi30.getAllActiveAccountGroups",
                new Object[]{},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        for (HashMap accountGroupHM : accountGroupList) {
            CommonAPI.checkIsNullOrEmptyObject(accountGroupHM.get("ID"), "Account Group could not be found", terminalModel.getId());
            //create a new model and add to the collection
            accountGroupCollection.getCollection().add(new AccountGroupModel(accountGroupHM));
        }

        return accountGroupCollection;
    }

    /**
     * Get all Active Account Groups with their mapped Spending Profiles
     * Fetch all the data with one query, then map the Account Group and their Spending Profiles
     *
     * @param terminalModel
     * @return
     * @throws Exception
     */
    public static AccountGroupCollection getAllActiveAccountGroupsWithSpendingProfiles(TerminalModel terminalModel) throws Exception {
        AccountGroupCollection accountGroupCollection = new AccountGroupCollection();

        //get all models in an array list
        ArrayList<HashMap> accountGroupList = dm.parameterizedExecuteQuery("data.posapi30.getAllActiveAccountGroupsWithSpendingProfiles",
                new Object[]{},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        int tempId = -1;
        AccountGroupModel accountGroupModel = null;
        for (HashMap accountGroupHM : accountGroupList) {
            CommonAPI.checkIsNullOrEmptyObject(accountGroupHM.get("PAYROLLGROUPINGID"), "Account Group could not be found", terminalModel.getId());
            //create a new model and add to the collection

            if (tempId != CommonAPI.convertModelDetailToInteger(accountGroupHM.get("PAYROLLGROUPINGID"))) {
                accountGroupModel = new AccountGroupModel(accountGroupHM);
                accountGroupModel.setAccountType(CommonAPI.convertModelDetailToString(accountGroupHM.get("PAYROLLGROUPACCTTYPE")));
                accountGroupModel.setAccountTypeId(CommonAPI.convertModelDetailToInteger(accountGroupHM.get("PAYROLLGROUPACCTTYPEID"))); //These are fields that identify the Account Type

                //create spending profile model
                accountGroupModel.setSpendingProfileModel(accountGroupHM, terminalModel);

                //create tax delete rate models
                accountGroupModel.setTaxDeleteRateModels(accountGroupHM, terminalModel);

                tempId = CommonAPI.convertModelDetailToInteger(accountGroupHM.get("PAYROLLGROUPINGID"));
                accountGroupCollection.getCollection().add(accountGroupModel);
            } else {

                //create spending profile model
                if (accountGroupModel  != null) {
                    accountGroupModel.setSpendingProfileModel(accountGroupHM, terminalModel);
                }
            }
        }

        return accountGroupCollection;
    }

    /**
     * Get the User ID of the session information (DSKey), and get all the Mapped and Active account groups for the User ID's User Profile
     *
     * @param terminalModel
     * @param allAccountGroups
     * @return
     * @throws Exception
     */
    public static List<AccountGroupModel> getAllActiveAccountGroupsForOneUser(TerminalModel terminalModel, List<AccountGroupModel> allAccountGroups) throws Exception {
        Integer userId = terminalModel.getLoginModel().getUserId();
        AccountGroupCollection accountGroupCollection = new AccountGroupCollection();

        //get all models in an array list
        ArrayList<HashMap> userAccessProfileList = dm.parameterizedExecuteQuery("data.posapi30.getUserProfileForOneUserId",
                new Object[]{userId},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        HashMap userAccessProfileHM = null;
        if (!userAccessProfileList.isEmpty()) {
            userAccessProfileHM = userAccessProfileList.get(0);
        }

        String payrollGroupIds = "";
        if (userAccessProfileHM != null) {

            payrollGroupIds = CommonAPI.convertModelDetailToString(userAccessProfileHM.get("PAYROLLGROUPIDS"));

            if (payrollGroupIds != null && !payrollGroupIds.isEmpty()) {

                if (payrollGroupIds.equalsIgnoreCase("-1")) {
                    return allAccountGroups;

                } else if (!payrollGroupIds.contains(",")) { //the case where there is just one mapped Account Group
                    Integer tempPayrollGroupingId = Integer.parseInt(payrollGroupIds);

                    for (AccountGroupModel accountGroupModel : allAccountGroups) {
                        if (accountGroupModel.getId().equals(tempPayrollGroupingId)) {
                            accountGroupCollection.getCollection().add(accountGroupModel);
                        }
                    }
                }
                else if (payrollGroupIds.contains(",")) {

                    String[] payrollGroupIdsArray = payrollGroupIds.split(",");

                    //Loop through the mapped Account Groups for the Account Profile
                    for (int i = 0; i < payrollGroupIdsArray.length; i++) {
                        String temp = payrollGroupIdsArray[i];
                        Integer tempPayrollGroupingId = Integer.parseInt(temp);

                        for (AccountGroupModel accountGroupModel : allAccountGroups) {
                            if (accountGroupModel.getId().equals(tempPayrollGroupingId)) {
                                accountGroupCollection.getCollection().add(accountGroupModel);
                            }
                        }
                    }
                }
            }
        }

        return accountGroupCollection.getCollection();
    }

    //getter
    public ArrayList<AccountGroupModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(ArrayList<AccountGroupModel> collection) {
        this.collection = collection;
    }
}
