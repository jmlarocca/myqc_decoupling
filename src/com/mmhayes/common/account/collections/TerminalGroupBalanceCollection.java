package com.mmhayes.common.account.collections;

//MMHayes Dependencies

import com.mmhayes.common.account.models.*;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.*;

//Other Dependencies
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-10-01 10:09:58 -0400 (Thu, 01 Oct 2020) $: Date of last commit
 $Rev: 12782 $: Revision of last commit
*/
public class TerminalGroupBalanceCollection {
    private ArrayList<TerminalGroupBalanceModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    //constructor
    public TerminalGroupBalanceCollection() {

    }

    public static TerminalGroupBalanceCollection getAllTerminalGroupBalances(Integer employeeId, TerminalModel terminalModel) throws Exception {
        TerminalGroupBalanceCollection terminalGroupBalanceCollection = new TerminalGroupBalanceCollection();

        Integer revenueCenterId = (terminalModel.getRevenueCenterId() == null) ? null : terminalModel.getRevenueCenterId();

        //get all models in an array list
        ArrayList<HashMap> terminalGroupBalanceList = dm.parameterizedExecuteQuery("data.posapi30.getEmployeeBalancesByTerminalGroup",
                new Object[]{employeeId, revenueCenterId },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        Integer tempTermGroupId = -1;

        for (HashMap terminalGroupBalanceHM : terminalGroupBalanceList) {
            //create a new model and add to the collection

            Integer curTermGroupId = CommonAPI.convertModelDetailToInteger(terminalGroupBalanceHM.get("TERMINALGROUPID"));
            TerminalGroupBalanceModel terminalGroupBalanceModel = new TerminalGroupBalanceModel();

            if (curTermGroupId != null) {

                if (!curTermGroupId.equals(tempTermGroupId)) {
                    terminalGroupBalanceModel = new TerminalGroupBalanceModel(terminalGroupBalanceHM);
                    terminalGroupBalanceCollection.getCollection().add(terminalGroupBalanceModel);
                    tempTermGroupId = curTermGroupId;

                    for (HashMap terminalGroupRevenueCenterHM : terminalGroupBalanceList) {
                        Integer curTermGroupRevCenter = CommonAPI.convertModelDetailToInteger(terminalGroupRevenueCenterHM.get("TERMINALGROUPID"));

                        //Match the Revenue Center Record by Terminal Group ID
                        if (curTermGroupRevCenter != null && curTermGroupRevCenter.equals(curTermGroupId)){
                            //Initialize the Revenue Center
                            Integer revCenterId = CommonAPI.convertModelDetailToInteger(terminalGroupRevenueCenterHM.get("REVCENTERID"));
                            String revCenterName = CommonAPI.convertModelDetailToString(terminalGroupRevenueCenterHM.get("REVCENTERNAME"));
                            BigDecimal revCenterBalance = CommonAPI.convertModelDetailToBigDecimal(terminalGroupRevenueCenterHM.get("REVCENTERBALANCE"));
                            RevenueCenterModel revenueCenterModel = new RevenueCenterModel(revCenterId, revCenterName);

                            //Initialize Revenue Center Balance Model
                            RevenueCenterBalanceModel revenueCenterBalanceModel = new RevenueCenterBalanceModel(revenueCenterModel, revCenterBalance);
                            terminalGroupBalanceModel.getRevenueCenterBalances().add(revenueCenterBalanceModel);
                        }
                    }
                }
            }
        }

        return terminalGroupBalanceCollection;
    }

    //getter
    public ArrayList<TerminalGroupBalanceModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(ArrayList<TerminalGroupBalanceModel> collection) {
        this.collection = collection;
    }
}


