package com.mmhayes.common.account.collections;

//MMHayes Dependencies
import com.mmhayes.common.account.models.*;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;

//API Dependencies
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;

//Other Dependencies
import java.util.ArrayList;
import java.util.HashMap;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-06-16 12:44:46 -0400 (Fri, 16 Jun 2017) $: Date of last commit
 $Rev: 7701 $: Revision of last commit
*/
public class SpendingProfileCollection {

    private ArrayList<SpendingProfileModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    //constructor
    public SpendingProfileCollection() {

    }

    public static SpendingProfileCollection getAllSpendingProfiles(TerminalModel terminalModel) throws Exception {
        SpendingProfileCollection spendingProfileCollection = new SpendingProfileCollection();

        //get all models in an array list
        ArrayList<HashMap> spendingProfileList = dm.parameterizedExecuteQuery("data.posapi30.getAllActiveSpendingProfiles",
                new Object[]{},
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );


        for (HashMap spendingProfileHM : spendingProfileList) {
            CommonAPI.checkIsNullOrEmptyObject(spendingProfileHM.get("ID"), "Spending Profile could not be found", terminalModel.getId());
            //create a new model and add to the collection
            spendingProfileCollection.getCollection().add(SpendingProfileModel.createSpendingProfileModel(spendingProfileHM));
        }

        return spendingProfileCollection;

    }

    //getter
    public ArrayList<SpendingProfileModel> getCollection() {
        return collection;
    }

    //setter
    public void setCollection(ArrayList<SpendingProfileModel> collection) {
        this.collection = collection;
    }
}
