package com.mmhayes.common.account.collections;

//mmhayes dependencies

import com.mmhayes.common.account.models.AccountModel;
import com.mmhayes.common.account.models.AccountQueryParams;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.ModelPaginator;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.utils.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: mjbatko $: Author of last commit
 $Date: 2021-06-25 17:17:22 -0400 (Fri, 25 Jun 2021) $: Date of last commit
 $Rev: 14224 $: Revision of last commit
*/
public class AccountCollection {
    private List<AccountModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();
    private String authenticatedMacAddress = null;

    //default constructor
    public AccountCollection() {
    }

    //constructor - populates the collection
    public AccountCollection(HashMap<String, LinkedList> paginationParamsHM, TerminalModel terminalModel, String accountName) throws Exception {

        //populate this collection with models
        populateCollection(new ModelPaginator(paginationParamsHM), terminalModel, accountName);

    }

    //constructor - populates the collection
    public AccountCollection(HashMap<String, LinkedList> paginationParamsHM, TerminalModel terminalModel, Integer accountNumber) throws Exception {

        //populate this collection with models
        populateCollection(new ModelPaginator(paginationParamsHM), terminalModel, accountNumber);

    }

    //populates this collection with models
    private AccountCollection populateCollection(Integer accountID, HttpServletRequest request, TerminalModel terminalModel) throws Exception {

        //get all models in an array list
        ArrayList<HashMap> accountList = dm.parameterizedExecuteQuery("data.posapi30.getOneAccountById",
                new Object[]{
                        accountID
                },
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(accountList, "Account List is empty", terminalModel.getId());
        for (HashMap accountHM : accountList) {
            CommonAPI.checkIsNullOrEmptyObject(accountHM.get("ID"), "Account Id could not be found", terminalModel.getId());
            //create a new model and add to the collection
            getCollection().add(AccountModel.createNewAccount(accountHM, terminalModel));
        }

        return this;
    }

    /**
     * Search for an account by Account name
     */
    public List<AccountModel> populateCollection(ModelPaginator modelPaginator, TerminalModel terminalModel, String accountName) throws Exception {

        //set the modelCount so we can finalize the pagination properties
        //determines, sets and validates pagination properties
        modelPaginator.finalizePaginationProperties();

        String sortByColumn = ""; //what column should this result set be sorted by
        if (modelPaginator.getSort().toUpperCase().equals("NAME")) {
            sortByColumn = "EMP.Name";
        } else if (modelPaginator.getSort().toUpperCase().equals("ID")) {
            sortByColumn = "EMP.ID";
        } else if (modelPaginator.getSort().toUpperCase().equals("NUMBER")) {
            sortByColumn = "EMP.Number";
        } else if (modelPaginator.getSort().toUpperCase().equals("BADGE")) {
            sortByColumn = "EMP.Badge";
        } else if (modelPaginator.getSort().toUpperCase().equals("DATE")) { //this is the default
            sortByColumn = "EMP.Name";

        } else {
            Logger.logMessage("ERROR: Invalid sort property in TransactionCollection.populateCollection(). sort property = " + modelPaginator.getSort(), Logger.LEVEL.ERROR);
            throw new MissingDataException("Invalid sort parameter for Account Endpoint", terminalModel.getId());
        }

        //get a single page of models in an array list
        ArrayList<HashMap> accountList = dm.serializeSqlWithColNames("data.posapi30.searchForAccountByNamePaginate",
                new Object[]{
                        modelPaginator.getStartingModelIndex().toString(),
                        modelPaginator.getEndingModelIndex().toString(),
                        sortByColumn,
                        modelPaginator.getOrder().toUpperCase(),
                        accountName
                },
                null,
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                false
        );

        //populate this collection from an array list of hashmaps
        if (accountList != null && accountList.size() > 0) {
            for (HashMap accountHM : accountList) {

                AccountModel accountModel = AccountModel.createNewAccount(accountHM, terminalModel);
                accountModel.setTerminal(terminalModel);

                getCollection().add(accountModel);
            }
        }

        return this.getCollection();
    }

    /**
     * Search for an account by Account number
     */
    public List<AccountModel> populateCollection(ModelPaginator modelPaginator, TerminalModel terminalModel, Integer accountNumber) throws Exception {

        //set the modelCount so we can finalize the pagination properties

        //determines, sets and validates pagination properties
        modelPaginator.finalizePaginationProperties();

        String sortByColumn = ""; //what column should this result set be sorted by
        if (modelPaginator.getSort().toUpperCase().equals("NAME")) {
            sortByColumn = "EMP.Name";
        } else if (modelPaginator.getSort().toUpperCase().equals("ID")) {
            sortByColumn = "EMP.ID";
        } else if (modelPaginator.getSort().toUpperCase().equals("NUMBER")) {
            sortByColumn = "EMP.Number";
        } else if (modelPaginator.getSort().toUpperCase().equals("BADGE")) {
            sortByColumn = "EMP.Badge";
        } else if (modelPaginator.getSort().toUpperCase().equals("DATE")) { //this is the default
            sortByColumn = "EMP.Number";

        } else {
            Logger.logMessage("ERROR: Invalid sort property in TransactionCollection.populateCollection(). sort property = " + modelPaginator.getSort(), Logger.LEVEL.ERROR);
            throw new MissingDataException("Invalid sort parameter for Account Endpoint", terminalModel.getId());
        }

        //get a single page of models in an array list
        ArrayList<HashMap> accountList = dm.serializeSqlWithColNames("data.posapi30.searchForAccountByNumberPaginate",
                new Object[]{
                        modelPaginator.getStartingModelIndex().toString(),
                        modelPaginator.getEndingModelIndex().toString(),
                        sortByColumn,
                        modelPaginator.getOrder().toUpperCase(),
                        accountNumber
                },
                false,
                false
        );

        //populate this collection from an array list of hashmaps
        if (accountList != null && accountList.size() > 0) {
            for (HashMap accountHM : accountList) {

                AccountModel accountModel = AccountModel.createNewAccount(accountHM, terminalModel);
                accountModel.setTerminal(terminalModel);

                getCollection().add(accountModel);
            }
        }

        return this.getCollection();
    }

    //gets all accounts from the database
    public static AccountCollection getAllAccounts(TerminalModel terminalModel) throws Exception {
        CommonAPI.checkIsNullOrEmptyObject(terminalModel.getId(), CommonAPI.PosModelType.TERMINAL, terminalModel.getId());
        AccountCollection accountCollection = new AccountCollection();

        //get all models in an array list
        ArrayList<HashMap> accountList = dm.parameterizedExecuteQuery("data.posapi30.getAllAccounts",
                new Object[]{terminalModel.getId()},
                true
        );

        //product list is null or empty, throw exception
        CommonAPI.checkIsNullOrEmptyList(accountList, CommonAPI.PosModelType.ACCOUNT, "Cannot find Account for this Terminal", terminalModel.getId());
        for (HashMap accountHM : accountList) {
            //CommonAPI.checkIsNullOrEmptyObject(accountHM.get("ID"), "Product Id could not be found");
            //create a new model and add to the collection
            accountCollection.getCollection().add(AccountModel.createNewAccount(accountHM, terminalModel));
        }

        return accountCollection;
    }

    public static Boolean checkAccountLicenses(List<AccountModel> accountList, TerminalModel terminalModel) throws Exception {
        Boolean hasEnoughLicenses = false;

        //convert AccountModelList to arraylist of HashMaps
        ArrayList<HashMap> accountListHM = new ArrayList();

        for (AccountModel accountModel : accountList) {
            HashMap accountHM = new HashMap();

            if (accountModel.getId() == null) {
                throw new MissingDataException("Invalid Account Submitted");
            } else {
                //this is a new Account
                if (accountModel.getId().equals(-1)) {
                    accountHM.put("ACCOUNTID", -1);
                } else {
                    //this must be an existing account, let's validate it
                    //This will query the database for the account, if it's not found, it will throw an exception
                    AccountModel tempAccountModel = AccountModel.getAccountModelByIdForTender(terminalModel, BigDecimal.ZERO, accountModel.getId(), new AccountQueryParams(true));

                    if (tempAccountModel == null) {
                        throw new MissingDataException("Invalid Account ID Submitted");
                    }

                    accountHM.put("ACCOUNTID", accountModel.getId());
                }

                //validate account type
                if (accountModel.getAccountTypeId() == null || accountModel.getAccountTypeId().toString().isEmpty()) {
                    throw new MissingDataException("Invalid Account Type Submitted");
                } else {

                    if (!PosAPIHelper.AccountType.isValid(accountModel.getAccountTypeId())) {
                        throw new MissingDataException("Invalid Account Type Submitted");
                    }

                    //Account Types:
                    //1 - Payroll Deduct
                    //2 - Prepay
                    //3 - Billing Account
                    //4 - Gift Card
                    //5 - Guest

                    accountHM.put("ACCOUNTTYPEID", accountModel.getAccountTypeId());
                }

                //validate account status
                if (accountModel.getStatus() == null || accountModel.getStatus().isEmpty()) {
                    throw new MissingDataException("Invalid Account Status Submitted");
                } else {

                    String accountStatusLetter = "";

                    switch (accountModel.getStatus().toUpperCase()) {
                        case "A": //Active
                        case "ACTIVE":
                            accountStatusLetter = "A";
                            break;
                        case "I": //Inactive
                        case "INACTIVE":
                            accountStatusLetter = "I";
                            break;
                        case "S": //Suspended
                        case "SUSPENDED":
                            accountStatusLetter = "S";
                            break;
                        case "P": //Pending Inactivation
                        case "PENDING":
                        case "PENDING INACTIVATION":
                            accountStatusLetter = "P";
                            break;
                        case "F": //Pending Inactivation
                        case "FROZEN":
                            accountStatusLetter = "F";
                            break;
                        case "W": //Waiting For Signup
                        case "WAITING FOR SIGNUP":
                        case "WAITING":
                            accountStatusLetter = "W";
                            break;
                        default:
                            throw new MissingDataException("Invalid Account Status Submitted");
                    }

                    accountHM.put("ACCOUNTSTATUS", accountStatusLetter);
                }
            }

            accountListHM.add(accountHM);

        } //end of for loop

        if (accountListHM.size() > 0) {
            //Now do the actual check if there are licenses available
            commonMMHFunctions commonMMHFunctions = new commonMMHFunctions();
            hasEnoughLicenses = commonMMHFunctions.checkAccountLicenseAvailability(accountListHM);
        }

        return hasEnoughLicenses;
    }

    //getter
    public List<AccountModel> getCollection() {
        return this.collection;
    }

    //getter
    private String getAuthenticatedMacAddress() {
        return this.authenticatedMacAddress;
    }

    //setter
    private void setAuthenticatedMacAddress(String authenticatedMacAddress) {
        this.authenticatedMacAddress = authenticatedMacAddress;
    }
}