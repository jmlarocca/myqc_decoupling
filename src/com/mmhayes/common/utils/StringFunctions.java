package com.mmhayes.common.utils;

import com.sun.jna.Native;
import com.google.common.base.Splitter;
import com.mmhayes.common.utils.stringMatchingUtils.RabinKarp;
import org.apache.commons.lang.math.NumberUtils;

import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Pattern;

/**
 * <p>Title: QuickCharge 4</p>
 * <p>Description: QuickCharge</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: MMHayes</p>
 * @author MMHayes
 * @version 4.0
 */

public class StringFunctions {
  public static final String[] AMPM = new String[] {"AM", "PM"};
  public static final int SHORT_DATE = 1;
  public static final int LONG_DATE = 2;
  public static final int DEFAULT_DATE = 3;

  public static boolean stringHasContent(String s) {
      return (s != null && !s.isEmpty());
  }

  public static String[] splitString(String stringToSplit, String stringToSplitOn) {
    String[] ret = null;
    StringTokenizer t = new StringTokenizer(stringToSplit, stringToSplitOn, false);
    int tokens = t.countTokens();
    if (tokens > 0) {
      ret = new String[tokens];
      int i = 0;
      while (t.hasMoreTokens()) {
        ret[i] = t.nextToken();
        i++;
      }
    }
    return ret;
  }

  public static void printRow(ArrayList l) {
    Iterator iter = l.iterator();
    while (iter.hasNext()) {
      Object item = iter.next();
      System.out.print("[" + item + "]");
    }
    System.out.println();
  }

  public static String rightPadNumber(int number) {
    String ret = String.valueOf(number);
    if (number < 10 && number > -10) {
      ret = "0" + ret;
    }
    return ret;
  }

  public static String decodePassword(String passwordCoded) {

      String decodedPassword = "";

      try{
          // Check if password is all numeric
          if(passwordCoded.matches("^[0-9]*$")){
              Logger.logMessage("Attempting to decode password via legacy algorithm.", Logger.LEVEL.DEBUG);
              decodedPassword = decodePasswordLegacy(passwordCoded);
          } else {
              try {
                  Logger.logMessage("Attempting to decode password with: "+MMHCommonItemsLibrary.getDLL(), Logger.LEVEL.DEBUG);
                  MMHCommonItemsLibrary.MMHCommonItems mmhPass = Native.load(MMHCommonItemsLibrary.getDLL(), MMHCommonItemsLibrary.MMHCommonItems.class);
                  decodedPassword = mmhPass.Decrypt(passwordCoded);
                  Logger.logMessage("Decode successful.", Logger.LEVEL.DEBUG);
              } catch(Exception ex){
                  Logger.logMessage(ex.getLocalizedMessage(), Logger.LEVEL.ERROR);
                  Logger.logException(ex);
                  throw new Exception("Error decrypting password with: "+MMHCommonItemsLibrary.getDLL());
              }
          }
      } catch (Exception ex){
        Logger.logException(ex);
      }

      return decodedPassword;
  }



  public static String encodePassword(String password) {

      String encodedPassword = "";

      if(password.isEmpty()){return password;} //Don't encode blanks

      try {
          if(MMHCommonItemsLibrary.useEnhancedEncryption){
              Logger.logMessage("EnhancedEncryption is enabled.", Logger.LEVEL.DEBUG);
              MMHCommonItemsLibrary.MMHCommonItems mmhPass = Native.load(MMHCommonItemsLibrary.getDLL(), MMHCommonItemsLibrary.MMHCommonItems.class);
              encodedPassword = mmhPass.Encrypt(password);
          } else {
              Logger.logMessage("EnhancedEncryption is disabled.", Logger.LEVEL.DEBUG);
              encodedPassword = encodePasswordLegacy(password);
          }

      } catch (Exception ex){
          Logger.logMessage("Error in Method: encodePasssword: "+ex.getMessage());
          Logger.logException(ex);
      }

      return encodedPassword;

  }

    public static String encodePasswordLegacy(String password) {
        int temp;
        char c;
        String buffer = "";
        String result = "";
        for (int i = 0; i < password.length(); i++) {
            c = password.charAt(i);
            temp = (int) c;
            if ((temp >= 33) & (temp <= 122)) {
                //ord('0')..ord('9'), ord('a')..ord('z'), ord('A')..ord('Z'): begin
                temp = temp - 23;
                buffer = buffer + stringOfLength("" + temp,'0',"right",2);
            } else {
                return "error";
            }
        }
        while (buffer.length() > 0) {
            if (buffer.length() <= 6) {
                temp = Integer.parseInt(buffer);
                buffer = "";
            } else {
                temp = Integer.parseInt(buffer.substring(0,6));
                buffer = buffer.substring(6);
            }
            temp = (temp + 1466187) * 6; // OLD SALT
            //temp = (temp + 1386546) * 6; //NEW SALT
            result = result + stringOfLength("" + temp,'0',"right",8);
        }
        return result;
    }

  private static String decodePasswordLegacy(String passwordCoded) {
        String sBuffer = "";
        String sTemp;
        String result = "";
        int iTemp;
        while (passwordCoded.length() > 0) {
            if (passwordCoded.length() > 8) {
                sTemp = passwordCoded.substring(0,8);
                passwordCoded = passwordCoded.substring(8);
            } else {
                sTemp = passwordCoded;
                passwordCoded = "";
            }
            iTemp = Math.round(Integer.parseInt(sTemp) / 6) - 1466187; //OLD SALT
            //iTemp = Math.round(Integer.parseInt(sTemp) / 6) - 1386546; // NEW SALT 2021
            sTemp = "" + iTemp;
            if (sTemp.length()/2 != Math.rint(sTemp.length()/2)) {
                // ODD LENGTH
                sBuffer = sBuffer + "0" + sTemp;
            } else {
                // EVEN LENGTH
                sBuffer = sBuffer + sTemp;
            }
        }
        while (sBuffer.length() > 0) {
            if (sBuffer.length() > 2) {
                sTemp = sBuffer.substring(0,2);
                sBuffer = sBuffer.substring(2);
            } else {
                sTemp = sBuffer;
                sBuffer = "";
            }
            result = result + Character.toString((char)(Integer.parseInt(sTemp) + 23));
        }
        return result;
  }

  public static String stringOfLength(String original, char padding, String justification, int length) {
    if (original.length() > length) {
      return original.substring(0,length - 1);
    } else {
      String ret = original;
      while (ret.length() < length) {
        if (justification.compareToIgnoreCase("right") == 0) {
          ret = padding + ret;
        } else {
          ret = ret + padding;
        }
      }
      return ret;
    }
  }

  public static String dateToTimeString(Date time) {
    GregorianCalendar c = new GregorianCalendar();
    c.setTime(time);
    String ret;
    int hour = c.get(c.HOUR);
    if (hour == 0) {
      hour = 12;
    }
    ret	= hour + ":" + rightPadNumber(c.get(c.MINUTE)) + ":" + rightPadNumber(c.get(c.SECOND)) + " " + StringFunctions.AMPM[c.get(c.AM_PM)];
    return ret;
  }

  public static int csvListIndexOf(String item, String list) {
    if ((list.compareTo(item) == 0) || (list.indexOf(item + ",") == 0)) {
        // only item in list or first item in list
        return 0;
    } else if (list.indexOf("," + item + ",") >= 0) {
        // middle item in list
        return list.indexOf("," + item + ",") + 1;
    } else if ((list.lastIndexOf("," + item) > 0) && (list.lastIndexOf("," + item) == list.length() - ("," + item).length())) {
        // last item in list
        return list.lastIndexOf("," + item) + 1;
    } else {
        return -1;
    }
  }

  public static String csvListAddTo(String item, String list) {
      return list + (list.length() > 0 ? "," : "") + item;
  }

  public static String csvListRemoveFrom(String item, String list) {
      if (csvListIndexOf(item,list) < 0) {
        return list;
      } else if (csvListIndexOf(item,list) == 0) {
          if (item.length() == list.length()) {
              // ITEM IS ONLY THING IN LIST
              return "";
          } else {
              // ITEM IS FIRST IN LIST
              return list.substring(list.indexOf(",") + 1);
          }
      } else if (list.indexOf("," + item + ",") >= 0) {
          // ITEM IS IN MIDDLE OF LIST
          return list.replaceFirst("," + item + "," ,",");
      } else {
          // ITEM IS AT END OF LIST
          return list.substring(0,list.lastIndexOf("," + item) - 1);
      }
  }

  public static String csvListCreateStringListFromJavaSet(Set<String> valueSet, boolean sort) {
      if (valueSet == null) {
          return "";
      }

      List<String> valueSortedList = new ArrayList(valueSet);
      if (sort) {
          Collections.sort(valueSortedList);
      }
      return csvListCreateStringListFromJavaList(valueSortedList);
  }

  public static String csvListCreateStringListFromJavaList(List<String> valueList) {
      StringBuilder sb = new StringBuilder();
      boolean isFirst = true;
      for (String value : valueList) {
          if (isFirst) {
              isFirst = false;
          }
          else {
              sb.append(",");
          }
          sb.append(value);
      }
      return sb.toString();
  }

    /**
     * Utility method to convert to given String to an int.
     *
     * @param s {@link String} The String to convert.
     * @return The converted String.
     */
    public static int convertToInt (String s) {
        int i = 0;

        try {
            if ((stringHasContent(s)) && (NumberUtils.isNumber(s))) {
                i = Integer.parseInt(s);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("Unable convert the String %s to an int in StringFunctions.convertToInt", Objects.toString(s, "NULL")), Logger.LEVEL.ERROR);
        }

        return i;
    }

    /**
     * Utility method to convert to given String to a double.
     *
     * @param s {@link String} The String to convert.
     * @return The converted String.
     */
    public static double convertToDouble (String s) {
        double d = 0.0d;

        try {
            if ((stringHasContent(s)) && (NumberUtils.isNumber(s))) {
                d = Double.parseDouble(s);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("Unable convert the String %s to a double in StringFunctions.convertToDouble", Objects.toString(s, "NULL")), Logger.LEVEL.ERROR);
        }

        return d;
    }

    /**
     * Utility method to convert to given String to a boolean.
     *
     * @param s {@link String} The String to convert.
     * @return The converted String.
     */
    public static boolean convertToBoolean (String s) {
        boolean b = false;

        try {
            if (stringHasContent(s)) {
                if ((s.equalsIgnoreCase("1")) || (s.equalsIgnoreCase("true"))) {
                    b = true;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("Unable convert the String %s to a boolean in StringFunctions.convertToBoolean", Objects.toString(s, "NULL")), Logger.LEVEL.ERROR);
        }

        return b;
    }

    /**
     * Converts list into a delimited String.
     *
     * @param list {@link List<T>} Generic List to concatenate.
     * @param delimiter {@link String} The delimiter to use.
     * @return {@link String} The delimited list as a String.
     */
    @SuppressWarnings("Duplicates")
    public static <T> String buildStringFromList (List<T> list, String delimiter) {
        String s = "";

        if ((!DataFunctions.isEmptyCollection(list)) && (stringHasContent(delimiter))) {
            for (T t : list) {
                if ((t instanceof String) && (StringFunctions.stringHasContent(t.toString()))) {
                    s += t+delimiter;
                }
                else {
                    if (StringFunctions.stringHasContent(t.toString())) {
                        s += t.toString()+delimiter;
                    }
                }
            }
            // remove trailing delimiter
            if (stringHasContent(s)) {
                s = s.substring(0, s.length()-delimiter.length());
            }
        }

        return s;
    }

    /**
     * <p>Builds a String using the getter for a property from a list of objects.</p>
     *
     * @param list The {@link List} of objects to concatenate a property for.
     * @param c The {@link Class} for the type of objects to concatenate a property for.
     * @param methodName The getter method name {@link String} for the property of the object.
     * @param params An array of {@link Object} corresponding to any arguments to pass to the getter method.
     * @return A built {@link String} using the getter for a property from a {@link List} of objects.
     */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    public static <T> String buildStringFromListOfObjProp (List<T> list, Class c, String methodName, Object[] params) {

        // make sure we have a valid List
        if (DataFunctions.isEmptyCollection(list)) {
            Logger.logMessage("The List passed to StringFunctions.buildStringFromListOfObjProp can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid Class
        if (c == null) {
            Logger.logMessage("The Class passed to StringFunctions.buildStringFromListOfObjProp can't be null!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid method name
        if (!StringFunctions.stringHasContent(methodName)) {
            Logger.logMessage("The method name passed to StringFunctions.buildStringFromListOfObjProp can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // no parameters to pass
        if (DataFunctions.isEmptyGenericArr(params)) {
            params = new Object[]{};
        }

        // get methods for the class
        Method[] methods = c.getMethods();
        if (DataFunctions.isEmptyGenericArr(methods)) {
            Logger.logMessage("No methods may be invoked for the class in StringFunctions.buildStringFromListOfObjProp!", Logger.LEVEL.ERROR);
            return null;
        }

        // get the method to invoke
        Method m = null;
        for (int i = 0; i < methods.length; i++) {
            if (methods[i].getName().contains(methodName)) {
                m = methods[i];
                break;
            }
        }
        if (m == null) {
            Logger.logMessage(String.format("Unable to find the method %s as a method that may be invoked in StringFunctions.buildStringFromListOfObjProp!",
                    Objects.toString(methodName, "N/A")), Logger.LEVEL.ERROR);
            return null;
        }

        // attempt to invoke the method for each object
        String s = "";
        for (T item : list) {
            try {
                Object result = m.invoke(item, params);
                if ((result != null) && (StringFunctions.stringHasContent(result.toString()))) {
                    s += result.toString() + ",";
                }
            }
            catch (Exception e) {
                Logger.logException(e);
                Logger.logMessage(String.format("Failed to invoke the method %s for the class %s in StringFunctions.buildStringFromListOfObjProp!",
                        Objects.toString(methodName, "N/A"),
                        Objects.toString(c.getName(), "N/A")), Logger.LEVEL.ERROR);
            }
        }

        // remove the last comma
        if ((StringFunctions.stringHasContent(s)) && (s.endsWith(","))) {
            s = s.substring(0, s.length() - 1);
        }

        return s;
    }

    /**
     * <p>Converts a generic array into a delimited String.</p>
     *
     * @param arr Generic array to concatenate.
     * @param delimiter {@link String} The delimiter to use.
     * @return The delimited generic array as a {@link String}.
     */
    @SuppressWarnings("Duplicates")
    public static <T> String buildStringFromGenericArr (T[] arr, String delimiter) {
        String s = "";

        if ((!DataFunctions.isEmptyGenericArr(arr)) && (stringHasContent(delimiter))) {
            for (T t : arr) {
                if ((t instanceof String) && (StringFunctions.stringHasContent(t.toString()))) {
                    s += t + delimiter;
                }
                else {
                    if (StringFunctions.stringHasContent(t.toString())) {
                        s += t.toString() + delimiter;
                    }
                }
            }
            // remove trailing delimiter
            if (stringHasContent(s)) {
                s = s.substring(0, s.length()-delimiter.length());
            }
        }

        return s;
    }

    /**
     * <p>Converts an int array into a delimited String.</p>
     *
     * @param arr Int array to concatenate.
     * @param delimiter {@link String} The delimiter to use.
     * @return The delimited int array as a {@link String}.
     */
    public static String buildStringFromIntArr (int[] arr, String delimiter) {
        String s = "";

        if ((!DataFunctions.isEmptyIntArr(arr)) && (stringHasContent(delimiter))) {
            for (int i : arr) {
                s += String.valueOf(i) + delimiter;
            }
            // remove trailing delimiter
            if (stringHasContent(s)) {
                s = s.substring(0, s.length() - delimiter.length());
            }
        }

        return s;
    }

    /**
     * <p>Utility method to remove the last character in the {@link String}</p>
     *
     * @param s The {@link String} to remove the last character from.
     * @return The {@link String} with the last character removed.
     */
    public static String removeLastChar (String s) {

        if (stringHasContent(s)) {
            s = s.substring(0, s.length() - 1);
        }

        return s;
    }

    /**
     * <p>Returns the indicies in which the word to find is found within the text.</p>
     *
     * @param searchText The {@link String} in which the word may be found.
     * @param wordToFind The {@link String} to find within the gicen text.
     * @return An {@link ArrayList} of {@link Integer} indicating the indicies at which the word was found within the text.
     */
    public static ArrayList<Integer> findWordIndices (String searchText, String wordToFind) {
        ArrayList<Integer> indices = new ArrayList<>();

        try {
            // check that the search test contains the word at least once otherwise return -1
            if ((!StringFunctions.stringHasContent(searchText)) || (!StringFunctions.stringHasContent(wordToFind)) || (!searchText.contains(wordToFind))) {
                indices.add(-1);
            }
            else {
                int length = 0;
                int index = 0;
                while (index != -1) {
                    index = searchText.indexOf(wordToFind, index + length);
                    if (index != -1) {
                        indices.add(index);
                    }
                    length = wordToFind.length();
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("Unable to find the indices at which the String %s may be found within " +
                    "the String %s in StringFunctions.findWordIndices",
                    Objects.toString(wordToFind, "NULL"),
                    Objects.toString(searchText, "NULL")), Logger.LEVEL.ERROR);
        }

        return indices;
    }

    /**
     * <p>Splits a {@link String} at the given indices into smaller substrings.</p>
     *
     * @param s The {@link String} to split up at the given indices.
     * @param indices The {@link ArrayList} of {@link Integer} indices to split up the {@link String} at.
     * @return An {@link ArrayList} of {@link String} created from the indices to split the given String at.
     */
    public static ArrayList<String> splitStrByIndices (String s, ArrayList<Integer> indices) {
        ArrayList<String> substrings = new ArrayList<>();

        try {
            if ((indices.size() == 1) && (indices.get(0) == -1)) {
                Logger.logMessage(String.format("No valid indices on which to split the String %s in " +
                        "StringFunctions.splitStrByIndices",
                        Objects.toString(s, "NULL")), Logger.LEVEL.ERROR);
            }
            else {
                if (!DataFunctions.isEmptyCollection(indices)) {
                    for (int i = 0; i < indices.size(); i++) {
                        String substring;
                        // check if it's the last index
                        if (i == indices.size() - 1) {
                            substring = s.substring(indices.get(i), s.length());
                            substrings.add(substring);
                        }
                        else {
                            substring = s.substring(indices.get(i), indices.get(i + 1));
                            substrings.add(substring);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to split the String %s at the indices %s in " +
                    "StringFunctions.splitStrByIndices",
                    Objects.toString(s, "NULL"),
                    Objects.toString(DataFunctions.convertIntArrayListToStr(indices), "NULL")), Logger.LEVEL.ERROR);
        }

        return substrings;
    }

    /**
     * <p>Utility method to check if a {@link String} contains the given substring without accounting for case.</p>
     *
     * @param s The {@link String} to check for the substring.
     * @param sub The substring to search for in the {@link String}.
     * @return Whether or not the substring is in the {@link String}.
     */
    public static boolean containsIgnoreCase (String s, String sub) {
        if ((stringHasContent(s)) && (stringHasContent(sub))) {
            return s.toLowerCase().contains(sub.toLowerCase());
        }
        return false;
    }

    /**
     * <p>Utility method to convert a {@link Collection} of a generic type into a bracket delimited {@link String}.</p>
     *
     * @param c The {@link Collection} to convert.
     * @return The given {@link Collection} of a generic type as a bracket delimited {@link String}.
     */
    public static <T> String collectionToBracketedString (Collection<T> c) {
        String s = "";

        if (!DataFunctions.isEmptyCollection(c)) {
            s += "[";
            for (T item : c) {
                s += "["+item.toString()+"]";
            }
            s+= "]";
        }

        return s;
    }

    /**
     * <p>
     *     Creates a {@link String} using only the given character which will be within the {@link String} the number of rep times.
     *     ex: repeat(a, 4) will produce thw String "aaaa"
     * </p>
     *
     * @param c The character to use to populate the {@link String}.
     * @param numberOfReps The number of times the given character should be within the {@link String}.
     * @return The {@link String} created using only the given character which is within the {@link String} the number of rep times.
     */
    public static String repeat (char c, int numberOfReps) {

        // make sure we have a valid char
        if (c == '\u0000') {
            Logger.logMessage("The character argument passed to StringFunctions.repeat is invalid!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the number of repetitions is valid
        if (numberOfReps <= 0) {
            Logger.logMessage("The number of repetitions passed to StringFunctions.repeat must be greater than 0!", Logger.LEVEL.ERROR);
            return null;
        }

        // fill a char array with the character
        char[] arr = new char[numberOfReps];
        Arrays.fill(arr, c);

        // return the String built from the char array
        return new String(arr);
    }

    /**
     * <p>Checks whether or not a string contains a substring using the RabinKarp algorithm.</p>
     *
     * @param pattern The pattern {@link String} to search for.
     * @param text The text {@link String} to search for the pattern within.
     * @param primeNumber A prime number that may used to calculate the hashed value.
     * @return Whether or not a string contains a substring using the RabinKarp algorithm.
     */
    public static boolean rkContains (String pattern, String text, int primeNumber) {
        return !DataFunctions.isEmptyCollection(RabinKarp.search(pattern, text, primeNumber));
    }

    /**
     * <p>Checks whether or not a string contains a substring using the RabinKarp algorithm.</p>
     *
     * @param patterns An array of pattern {@link String} to search for.
     * @param text The text {@link String} to search for the patterns within.
     * @param primeNumber A prime number that may used to calculate the hashed value.
     * @return Whether or not a string contains any of the given substrings using the RabinKarp algorithm.
     */
    public static boolean rkMultiContains (String[] patterns, String text, int primeNumber) {

        if (!DataFunctions.isEmptyGenericArr(patterns)) {
            for (String pattern : patterns) {
                if (rkContains(pattern, text, primeNumber)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * <p>Checks whether or not the expression {@link String} is within the source {@link String} in a case insensitive manner.</p>
     *
     * @param expr The expression {@link String} to check for within the source.
     * @param src The source {@link String} to search for the expression {@link String} within.
     * @return Whether or not the expression {@link String} is within the source {@link String} in a case insensitive manner.
     */
    public static boolean caseInsensitiveContains (String expr, String src) {

        // make sure the expression String is valid
        if (!stringHasContent(expr)) {
            Logger.logMessage("The expression String passed to StringFunctions.caseInsensitiveContains can't be null or empty, " +
                    "unable to determine whether or not the expression String is contained within the source String, now returning false!", Logger.LEVEL.ERROR);
            return false;
        }

        // make sure the source String is valid
        if (!stringHasContent(src)) {
            Logger.logMessage("The source String passed to StringFunctions.caseInsensitiveContains can't be null or empty, " +
                    "unable to determine whether or not the expression String is contained within the source String, now returning false!", Logger.LEVEL.ERROR);
            return false;
        }

        return Pattern.compile(Pattern.quote(expr), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE).matcher(src).find();
    }

    /**
     * <p>Uses the Guava splitter to split a String based on a delimiter.</p>
     *
     * @param s The {@link String} to split.
     * @param delimiter The delimiter {@link String} to use to split the given {@link String}.
     * @return An {@link ArrayList} of {@link String} corresponding to the tokens acquired from splitting the given {@link String} based on the delimiter {@link String}.
     */
    public static ArrayList<String> split (String s, String delimiter) {

        // validate the String
        if (!stringHasContent(s)) {
            Logger.logMessage("The String to split passed to StringFunctions.split can't be null or empty, unable to split the String, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        // validate the delimiter
        if (delimiter == null) {
            Logger.logMessage("The delimiter passed to StringFunctions.split can't be null, unable to split the String, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        return new ArrayList<>(Splitter.on(delimiter).splitToList(s));
    }

}
