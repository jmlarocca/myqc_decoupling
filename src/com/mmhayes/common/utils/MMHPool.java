package com.mmhayes.common.utils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-20 13:10:34 -0400 (Tue, 20 Apr 2021) $: Date of last commit
    $Rev: 13819 $: Revision of last commit
    Notes: Interface for a pool containing objects of a generic type.
*/

public interface MMHPool<T> {

    /**
     * <p>Returns an instance from the {@link MMHPool}.</p>
     *
     * @return The instance retrieved from the {@link MMHPool}.
     */
    T get();

    /**
     * <p>Releases the object and returns it to the {@link MMHPool}.</p>
     *
     * @param t The Object to release and return to the pool.
     */
    void release(T t);

    /**
     * <p>Shuts down the {@link MMHPool} and prevents subsequent requests from accessing resources within the {@link MMHPool}.</p>
     *
     */
    void shutdown();

    /**
     * <p>Interface that will validate objects of the generic type within the {@link MMHPool}.</p>
     *
     */
    interface Validator<T> {

        /** <p>Checks whether or not objects of the generic type are valid or not.</p>
         *
         * @param t The object to validate.
         * @return Whether or not objects of the generic type are valid or not.
         */
        boolean isValid(T t);

        /** <p>Performs any cleanup tasks associated with an object of the generic type which has been deemed invalid.</p>
         *
         * @param t The object to invalidate.
         */
        void invalidate(T t);
    }

}