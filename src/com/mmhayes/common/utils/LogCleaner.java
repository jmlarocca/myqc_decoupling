package com.mmhayes.common.utils;

/* Log Cleaner
 $Author: jrmitaly $: Author of last commit
 $Date: 2015-04-30 09:16:16 -0400 (Thu, 30 Apr 2015) $: Date of last commit
 $Rev: 1538 $: Revision of last commit
 Notes: is kicked off as a new thread, just cleans up the log files
 */

public class LogCleaner implements Runnable {
    public void run() {
        //Logger.cleanupLogFiles(30); //removed due to recent changes in the new logger class -jrmitaly 11/13/2014
    }
}
