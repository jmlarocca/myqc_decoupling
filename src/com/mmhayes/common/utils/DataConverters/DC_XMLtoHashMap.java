package com.mmhayes.common.utils.DataConverters;

/*
        Last Updated (automatically updated by SVN)
        $Author: jkflanagan $: Author of last commit
        $Date: 2019-08-29 11:50:40 -0400 (Thu, 29 Aug 2019) $: Date of last commit
        $Rev: 41215 $: Revision of last commit

        Notes: Converts XML into a HashMap while maintaing the hierarchy.
*/

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.utils.Logger;
import org.json.JSONObject;
import org.json.XML;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Objects;

/**
 * Converts XML into a HashMap while maintaing the hierarchy.
 * NOTE: This class uses the builder pattern.
 *
 */
public class DC_XMLtoHashMap {

    // private member variables of a DC_XMLtoHashMap
    private final DataManager dataManager; // required
    // NOTE: while xmlStr and xmlFile are both optional one if these options must be supplied
    private final String xmlStr; // optional
    private final File xmlFile; // optional

    /**
     * Constructor for a DC_XMLtoHashMap.
     *
     * @param builder {@link DC_BLDR_XMLtoHashMap} Builder class for a DC_XMLtoHashMap.
     */
    public DC_XMLtoHashMap (DC_BLDR_XMLtoHashMap builder) {
        this.dataManager = builder.dataManager;
        this.xmlStr = builder.xmlStr;
        this.xmlFile = builder.xmlFile;
    }

    /**
     * Converts the supplied XML String or File into a HashMap.
     *
     * @return {@link HashMap} The converted XML.
     */
    public HashMap convertXMLtoHashMap () {
        HashMap res = new HashMap();

        BufferedReader bufferedReader = null;

        try {
            String xml;
            if (this.xmlFile != null) {
                // convert the File to a xml String
                bufferedReader = new BufferedReader(new FileReader(this.xmlFile));
                StringBuilder stringBuilder = new StringBuilder();
                String line = bufferedReader.readLine();
                while (line != null) {
                    stringBuilder.append(line);
                    line = bufferedReader.readLine();
                }
                xml = stringBuilder.toString();
                bufferedReader.close();
            }
            else {
                xml = this.xmlStr;
            }
            // convert the xml to json
            JSONObject json = XML.toJSONObject(xml);
            String jsonStr = json.toString();
            // convert the json to a HashMap
            ObjectMapper jsonMapper = new ObjectMapper();
            res = jsonMapper.readValue(jsonStr, HashMap.class);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to convert the xml " +
                    Objects.toString((this.xmlFile == null ? "String" : "File"), "NULL")+" into a HashMap in " +
                    "DC_XMLtoHashMap.convertXMLtoHashMap", Logger.LEVEL.ERROR);
        }
        finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            }
            catch (Exception e) {
                Logger.logException(e);
                Logger.logMessage("Unable top close the readers in DC_XMLtoHashMap.convertXMLtoHashMap", Logger.LEVEL.ERROR);
            }
        }

        return res;
    }

    /**
     * Builder class for a DC_XMLtoHashMap.
     *
     */
    public static class DC_BLDR_XMLtoHashMap {

        // private member variables of a DC_BLDR_XMLtoHashMap
        private final DataManager dataManager;
        private String xmlStr;
        private File xmlFile;

        /**
         * Constructor for a DC_BLDR_XMLtoHashMap.
         *
         * @param dataManager {@link DataManager} DataManager to be used by the DC_BLDR_XMLtoHashMap.
         */
        public DC_BLDR_XMLtoHashMap (DataManager dataManager) {
            this.dataManager = dataManager;
        }

        /**
         * Adds an optional xmlStr to the DC_BLDR_XMLtoHashMap.
         *
         * @param xmlStr {@link String} XML to convert to a HashMap in String form.
         * @return {@link DC_BLDR_XMLtoHashMap} The DC_BLDR_XMLtoHashMap with the xmlStr field included.
         */
        public DC_BLDR_XMLtoHashMap xmlStr (String xmlStr) {
            this.xmlStr = xmlStr;
            return this;
        }

        /**
         * Adds an optional xmlFile to the DC_BLDR_XMLtoHashMap.
         *
         * @param xmlFile {@link File} XML to convert to a HashMap in File form.
         * @return {@link DC_BLDR_XMLtoHashMap} The DC_BLDR_XMLtoHashMap with the xmlFile field included.
         */
        public DC_BLDR_XMLtoHashMap xmlFile (File xmlFile) {
            this.xmlFile = xmlFile;
            return this;
        }

        /**
         * Builds and returns the DC_XMLtoHashMap.
         *
         * @return {@link DC_XMLtoHashMap} The newly constructed DC_XMLtoHashMap.
         */
        public DC_XMLtoHashMap build () {
            return (new DC_XMLtoHashMap(this));
        }

    }

}
