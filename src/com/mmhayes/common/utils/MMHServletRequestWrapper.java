package com.mmhayes.common.utils;

//mmhayes dependencies
import com.mmhayes.common.dataaccess.*;

//other dependencies
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/*
 MMHayes Common Filter

 Created with IntelliJ IDEA.
 User: jrmitaly
 Date: 12/15/15
 Time: 8:47 AM

 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2017-09-29 10:37:07 -0400 (Fri, 29 Sep 2017) $: Date of last commit
 $Rev: 5314 $: Revision of last commit

 Notes: Handles wrapping ALL Servlet Requests
*/
public class MMHServletRequestWrapper extends HttpServletRequestWrapper {

    //wrap the servletRequest
    public MMHServletRequestWrapper(HttpServletRequest servletRequest) {
        super(servletRequest);
    }

    @Override
    //anytime all HTTP parameters are grabbed - sanitize them
    public String[] getParameterValues(String parameter) {
        String[] values = super.getParameterValues(parameter);

        if (values == null) {
            return null;
        }

        int count = values.length;
        String[] encodedValues = new String[count];
        for (int i = 0; i < count; i++) {
            encodedValues[i] = sanitizeXSS(values[i]);
        }

        return encodedValues;
    }

    @Override
    //anytime an HTTP parameter is grabbed - sanitize it
    public String getParameter(String parameter) {
        String value = super.getParameter(parameter);
        return sanitizeXSS(value);
    }

    @Override
    //anytime an HTTP header is grabbed - sanitize it
    public String getHeader(String name) {
        String value = super.getHeader(name);
        if (name.equalsIgnoreCase("Origin")) {
            return value;
        }  else {
            return sanitizeXSS(value);
        }
    }

    //sanitize string from common XSS attacks using various REGEX
    private String sanitizeXSS(String value) {
        return (commonMMHFunctions.sanitizeStringFromXSSWithESAPI(value));
    }

}
