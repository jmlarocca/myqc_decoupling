package com.mmhayes.common.utils;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: samcgarvey
 * Date: 5/19/14
 * Time: 6:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class HttpSessionCollector implements HttpSessionListener {
    private static final Map<String, HttpSession> sessions = new HashMap<String, HttpSession>();

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        HttpSession session = event.getSession();
        sessions.put(session.getId(), session);
    }


    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        Logger.logMessage("sessionDestroyed: " + event.getSession().getId(), Logger.LEVEL.DEBUG);
        sessions.remove(event.getSession().getId());
    }

    public static HttpSession find(String sessionId) {
        return sessions.get(sessionId);
    }

    /**
     * Sets the terminal with the given terminal id as in use by the session with the given session id
     * @param sessionId
     * @param terminalId
     * @return
     */
    public static boolean setSessionTerminalInUse(String sessionId, String terminalId) {

        boolean successfullySetSessionsTerminal = false;

        if (sessionId != null && !sessionId.isEmpty()) {
            if (terminalId != null && !terminalId.isEmpty()) {
                HttpSession session = find(sessionId);
                if (session != null) {
                    session.setAttribute("TerminalID", terminalId);
                    Logger.logMessage("Successfully attached terminal ID " + terminalId + " to session " + sessionId, Logger.LEVEL.DEBUG);
                    successfullySetSessionsTerminal = true;
                }
                else {
                    Logger.logMessage("Failed to attach terminal ID " + terminalId + " to session - session id " + sessionId + " not found", Logger.LEVEL.ERROR);
                }
            }
            else {
                Logger.logMessage("Failed to attach terminal ID " + terminalId + " to session - invalid terminal id given", Logger.LEVEL.ERROR);
            }
        }
        else {
            Logger.logMessage("Failed to attach terminal ID " + terminalId + " to session - invalid session id given", Logger.LEVEL.ERROR);
        }

        return successfullySetSessionsTerminal;
    }

    /**
     * Resets the terminals set as in use by the session with the given session id
     * @param sessionId
     */
    public static void resetSessionTerminalsInUse(String sessionId) {
        if (sessionId != null && !sessionId.isEmpty()) {
            HttpSession session = find(sessionId);
            if (session != null) {
                session.removeAttribute("TerminalID");
            }
        }
    }

    // Used by POS Anywhere
    // Iterates through all sessions and creates a list of terminal IDs
    // that have been assigned to a session
    public static ArrayList getTerminalsInUse() {
        ArrayList ret = new ArrayList();

        Iterator it = sessions.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            HttpSession session = find(pair.getKey().toString());

            if (session.getAttribute("TerminalID") != null) {
                String terminalId = session.getAttribute("TerminalID").toString();
                ret.add(terminalId);
            }
        }

        return ret;
    }

    public static int invalidateSessionsByAttribute(String attribute, String value){
        // This method invalidates all sessions which match a specified attribute and value
        int count = 0;
        ArrayList<HttpSession> sessionsToInvalidate = new ArrayList<>();
        try {
            Iterator it = sessions.entrySet().iterator();

            // Add matching sessions to arraylist
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                HttpSession session = find(pair.getKey().toString());

                if (session.getAttribute(attribute) != null) {
                    if(session.getAttribute(attribute).toString().equalsIgnoreCase(value)){
                        sessionsToInvalidate.add(session);
                    }
                }
            }

            // Log and Invalidate Sessions
            if(!sessionsToInvalidate.isEmpty()){
                count = sessionsToInvalidate.size();
                Logger.logMessage("Found "+count+" sessions matching "+attribute +" equal to "+value, Logger.LEVEL.TRACE);
                for(HttpSession session : sessionsToInvalidate){
                    session.invalidate();
                }
                Logger.logMessage("Successfully invalidated "+count+" session(s).", Logger.LEVEL.TRACE);
            }

        } catch(Exception ex){
            Logger.logMessage("Error in method invalidateSessionsByAttribute()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        // return # of sessions which were invalidated
        return count;
    }
}
