package com.mmhayes.common.utils;

import com.mmhayes.common.api.CommonAPI;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

/* MMHEmail
 
 Created with IntelliJ IDEA.
 User: jrmitaly
 Date: 10/27/14
 Time: 9:30 AM
 
 Last Updated (automatically updated by SVN)
 $Author: ecdyer $: Author of last commit
 $Date: 2020-08-24 14:00:45 -0400 (Mon, 24 Aug 2020) $: Date of last commit
 $Rev: 12420 $: Revision of last commit

 Notes: For handling e-mails
*/


public class MMHEmail {
    public static String fromAddress = null;
    public static String fromName = null;
    public static String replyToAddress = null;
    public static String replyToName = null;
    public static String host = null;
    public static String port = null;
    public static String user = null;
    public static String pass = null;

    //generic send email method - UNDER LOAD
    public static Boolean sendEmail(String toAddress, String subject, String htmlBody) {
        return sendEmail(toAddress, null, subject, htmlBody);
    }

    //generic send email method - UNDER LOAD
    public static Boolean sendEmail(String toAddress, String fromAddress, String subject, String htmlBody) {
        //for when called directly, and fromAddress is not set or is different from what is set, set to new fromAddress
        if (fromAddress != null && !fromAddress.equals(getFromAddress())) {
            setFromAddress(fromAddress);
        }

        return sendEmail(toAddress, fromAddress, subject, htmlBody, true);
        //return sendEmail(toAddress, fromAddress, subject, htmlBody, false); //uncomment for dev testing
    }

    //generic send email method - UNDER LOAD
    public static Boolean sendEmail(String toAddress, String fromAddress, String subject, String htmlBody, Boolean useSMTPAuth) {
        //for when called directly, and fromAddress is not set or is different from what is set, set to new fromAddress
        if (fromAddress != null && !fromAddress.equals(getFromAddress())) {
            setFromAddress(fromAddress);
        }

        Integer port = null;
        try {
            port = (getPort() == null || getPort().isEmpty()) ? null : Integer.parseInt(getPort());
        } catch(Exception ex){
            Logger.logMessage("Error reading SMTP PORT. Blank or invalid value.", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        if (useSMTPAuth) {
            //sendEmail with SMTP authentication
            return sendAuthEmail(port, getHost(), getUser(), getPass(), toAddress, getFromAddress(), subject, htmlBody, "text/html");
        } else {
            //sendEmail with NO authentication
            return sendEmail(getHost(), toAddress, getFromAddress(), subject, htmlBody, "text/html");
        }
    }

    //generic send email method - does not use SMTP authentication  (This is
    public static Boolean sendEmail(String host, String toAddress, String fromAddress, String subject, String body, String bodyContentType) {
        //for when called directly, and fromAddress is not set or is different from what is set, set to new fromAddress
        if (fromAddress != null && !fromAddress.equals(getFromAddress())) {
            setFromAddress(fromAddress);
        }

        //for when called directly, and host is not set or is different from what is set, set to new host
        if (host != null && !host.equals(getHost())) {
            setHost(host);
        }

        Integer port = null;
        try {
            port = (getPort() == null || getPort().isEmpty()) ? null : Integer.parseInt(getPort());
        } catch(Exception ex){
            Logger.logMessage("Error reading SMTP PORT. Blank or invalid value.", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return sendEmail(port, getHost(), toAddress, getFromAddress(), subject, body, bodyContentType);
    }

    public static boolean finalizeProperties() {
        try {
            //verify fromAddress is set
            if (getFromAddress() == null || getFromAddress().isEmpty()) {
                setFromAddress(CommonAPI.getFromAddress());
            }

            //verify replyTo Address is set
            if (getReplyToAddress() == null || getReplyToAddress().isEmpty()) {
                setReplyToAddress(CommonAPI.getReplyToAddress());
            }

            //verify host is set
            if (getHost() == null || getHost().isEmpty()) {
                setHost(CommonAPI.getEmailSMTPHost());
            }

            //verify port is set
            if (getPort() == null || getPort().isEmpty()) {
                setPort(CommonAPI.getEmailSMTPPort());
            }

            //verify user is set
            if (getUser() == null || getUser().isEmpty()) {
                String user = CommonAPI.getEmailSMTPUser();
                setUser(user);
            }

            //verify pass is set
            if (getPass() == null || getPass().isEmpty()) {
                String pass = CommonAPI.getEmailSMTPPassword();
                setPass(pass);
            }
        } catch (Exception ex) {
            Logger.logException(ex);
            return false;
        }

        return true;
    }

    //generic send email method - does not use SMTP authentication
    public static Boolean sendEmail(Integer port, String host, String toAddress, String fromAddress, String subject, String body, String bodyContentType) {
        try {
            //for when called directly, and fromAddress is not set or is different from what is set, set to new fromAddress
            if (fromAddress != null && !fromAddress.equals(getFromAddress())) {
                setFromAddress(fromAddress);
            }

            //for when called directly, and host is not set or is different from what is set, set to new host
            if (host != null && !host.equals(getHost())) {
                setHost(host);
            }

            //for when called directly, and port is not set or is different from what is set, set to new port
            if (port != null && !port.toString().equals(getPort())) {
                setPort(port.toString());
            }

            if ( !finalizeProperties() ) {
                return false;
            }

            port = Integer.parseInt(getPort());

            //get java properties in a Properties object
            Properties properties = System.getProperties();

            //define host for mail
            properties.setProperty("mail.smtp.host", getHost());

            //this type of mail server doesn't require authentication, so tell java mail that
            properties.put("mail.smtp.auth", "false");
            properties.put("mail.smtp.starttls.enable", "false");
            properties.put("mail.smtp.starttls.required", "false");

            if(port != -1) {
                properties.put("mail.smtp.port", port);
            }

            //create a Session object to represent a mail session with the specified properties.
            Session session = Session.getDefaultInstance(properties);

            //create a default message object
            MimeMessage message = new MimeMessage(session);

            //set the From: header field
            message.setFrom(getFromInternetAddress());

            // set the Reply To : header field
            if(getReplyToAddress()!= null && !getReplyToAddress().isEmpty()){
                message.setReplyTo(new javax.mail.Address[]{getReplyToInternetAddress()});
            }

            //set the To: header field
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));

            //set the Subject: header field
            message.setSubject(subject);

            //set the Date the email was sent
            message.setSentDate(new Date());

            //set the body and the body content type for the email
            message.setContent(body, bodyContentType);

            //send the e-mail
            Transport.send(message);

            //return true because the email was successful
            return true;
        } catch (MessagingException mex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            mex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            Logger.logMessage("ERROR: In MMHEmail.sendEmail: Could not send e-mail to "+toAddress+", from host "+getHost()+". Due to MessagingException:" + sw.toString(), Logger.LEVEL.ERROR);
            return false;
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            Logger.logMessage("ERROR: In MMHEmail.sendEmail: Could not send e-mail to "+toAddress+", from host "+getHost()+". Due to Exception:" + sw.toString(), Logger.LEVEL.ERROR);
            return false;
        }
    }

    //sends an email using SMTP Authentication
    public static Boolean sendAuthEmail(Integer port, String host, String smtpUser, String smtpPass, String toAddress, String fromAddress, String subject, String body, String bodyContentType) {
        Transport transport = null; //initialize a transport object
        try {
            //for when called directly, and fromAddress is not set or is different from what is set, set to new fromAddress
            if (fromAddress != null && !fromAddress.equals(getFromAddress())) {
                setFromAddress(fromAddress);
            }

            //for when called directly, and host is not set or is different from what is set, set to new host
            if (host != null && !host.equals(getHost())) {
                setHost(host);
            }

            //for when called directly, and smtpUser is not set or is different from what is set, set to new smtpUser
            if (smtpUser != null && !smtpUser.equals(getUser())) {
                setUser(smtpUser);
            }

            //for when called directly, and smtpPass is not set or is different from what is set, set to new smtpPass
            if (smtpPass != null && !smtpPass.equals(getPass())) {
                setPass(smtpPass);
            }

            //for when called directly, and port is not set or is different from what is set, set to new port
            if (port != null && !port.toString().equals(getPort())) {
                setPort(port.toString());
            }

            if ( !finalizeProperties() ) {
                return false;
            }

            if(getPort().isEmpty() || getPort() == null){
                throw new Exception("SMTP PORT is missing or blank. SMTP PORT must be a valid port number.");
            } else {
                port = Integer.parseInt(getPort());
            }

            //create a Properties object to contain connection configuration information
            Properties props = System.getProperties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.port", port);

            //set properties indicating that we want to use STARTTLS to encrypt the connection
            //the SMTP session will begin on an unencrypted connection, and then the client will issue a STARTTLS command to upgrade to an encrypted connection
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.starttls.required", "true");

            //create a Session object to represent a mail session with the specified properties
            Session session = Session.getDefaultInstance(props);

            //create a default message object
            MimeMessage msg = new MimeMessage(session);

            //set the From: header field
            msg.setFrom(getFromInternetAddress());

            // set the Reply To : header field
            if(getReplyToAddress()!= null && !getReplyToAddress().isEmpty()){
                msg.setReplyTo(new javax.mail.Address[]{getReplyToInternetAddress()});
            }

            //set the To: header field
            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));

            //set the Subject: header field
            msg.setSubject(subject);

            //set the Date the email was sent
            msg.setSentDate(new Date());

            //set the body and the body content type for the email
            msg.setContent(body, bodyContentType);

            //set the default transport object
            transport = session.getTransport();

            //connect to host using the SMTP username and password
            transport.connect(getHost(), getUser(), getPass());

            //send the email
            transport.sendMessage(msg, msg.getAllRecipients());

            //close the transport object
            transport.close();

            //return true because the email was successful
            return true;
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            Logger.logMessage("ERROR: In MMHEmail.sendAuthEmail: Could not send e-mail to "+toAddress+", from host "+getHost()+", on port "+getPort()+", using SMTP Auth User "+getUser()+". Due to Exception:" + sw.toString(), Logger.LEVEL.ERROR);
            if (transport != null) { //attempt to close the transport object
                try {
                    transport.close();
                } catch (Exception transEx) {
                    StringWriter transSW = new StringWriter();
                    PrintWriter transPW = new PrintWriter(transSW);
                    transEx.printStackTrace(transPW); //allows stack trace as a string -> sw.toString()
                    Logger.logMessage("ERROR: In MMHEmail.sendAuthEmail: Could not close transport object. Due to Exception:" + transSW.toString(), Logger.LEVEL.ERROR);
                }
            }
            return false;
        }
    }

    public static String getReplyToAddress() {
        return replyToAddress;
    }

    public static void setReplyToAddress(String replyToAddress) {
        MMHEmail.replyToAddress = replyToAddress;
    }

    public static String getFromAddress() {
        return fromAddress;
    }

    public static void setFromAddress(String fromAddress) {
        MMHEmail.fromAddress = fromAddress;
    }

    public static String getFromName() {
        return fromName;
    }

    public static void setFromName(String fromName) {
        MMHEmail.fromName = fromName;
    }

    public static String getReplyToName() {
        return replyToName;
    }

    public static void setReplyToName(String replyToName) {
        MMHEmail.replyToName = replyToName;
    }

    public static String getHost() {
        return host;
    }

    public static void setHost(String host) {
        MMHEmail.host = host;
    }

    public static String getPort() {
        return port;
    }

    public static void setPort(String port) {
        MMHEmail.port = port;
    }

    public static String getUser() {
        return user;
    }

    public static void setUser(String user) {
        MMHEmail.user = user;
    }

    public static String getPass() {
        return pass;
    }

    public static void setPass(String pass) {
        MMHEmail.pass = pass;
    }

    public static InternetAddress getReplyToInternetAddress() throws UnsupportedEncodingException, AddressException {
        if(getReplyToAddress() != null && getReplyToName() != null){
            return new InternetAddress(getReplyToAddress(), getReplyToName());
        } else {
            return new InternetAddress(getReplyToAddress());
        }
    }

    public static InternetAddress getFromInternetAddress() throws UnsupportedEncodingException, AddressException {
        if(getFromAddress() != null && getFromName() != null){
            return new InternetAddress(getFromAddress(), getFromName());
        } else {
            return new InternetAddress(getFromAddress());
        }
    }
}
