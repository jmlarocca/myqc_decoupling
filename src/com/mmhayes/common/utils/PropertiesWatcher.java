package com.mmhayes.common.utils;

//SC2P: removing the properties file entirely, this entire class should be removed
/* PropertiesWatcher
 $Author: jrmitaly $: Author of last commit
 $Date: 2015-04-30 09:16:16 -0400 (Thu, 30 Apr 2015) $: Date of last commit
 $Rev: 1538 $: Revision of last commit
 Notes: File Watcher that monitors app.properties changes
 */

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Properties;


public class PropertiesWatcher {

    private static MMHFileWatchDog watcher;
    private static final String DEBUG_MODE = "site.debugmode";
    private static final String LOGGING_LEVEL = "site.logging.level";
    private static boolean changed;

    public static void start(String filepath){

        try {

            Logger.logMessage("Starting PropertiesWatcher: "+filepath, Logger.LEVEL.DEBUG);

            // Validate the filepath
            File file;
            file = new File(filepath);
            if(!file.exists()){
                throw new Exception("Invalid file path for PropertiesWatcher: "+filepath);
            }

            watcher = new MMHFileWatchDog(filepath) {
                @Override
                protected void doOnChange() {

                    Logger.logMessage("FILE CHANGE DETECTED :"+file.getName(), Logger.LEVEL.IMPORTANT);

                    try {
                        // TEST FOR FILE
                        Properties props;
                        if(file.exists()){
                            props = new Properties();
                            FileReader fileReader = new FileReader(file);
                            props.load(fileReader);
                        } else {
                            throw new Exception("Invalid properties file.");
                        }

                        // Set changed flag to false
                        PropertiesWatcher.changed = false;

                        // Build list of settings to check
                        ArrayList<String> settings = new ArrayList<>();
                        settings.add(DEBUG_MODE);
                        settings.add(LOGGING_LEVEL);

                        // Check settings for changes
                        checkProperty(props, settings);

                        // RESET LOG LEVEL
                        if(changed){
                            Logger.setLogLevel();
                            Logger.logMessage("Updated Logging Level to: "+ Logger.currentLogLevel.toString(), Logger.LEVEL.IMPORTANT);
                        }

                    } catch(Exception ex){
                        Logger.logException(ex);
                    }
                }
            };
            watcher.start();

        } catch(Exception ex){
            Logger.logException(ex);
        }
    }

     public static void stop(){
        try {
            Logger.logMessage("Stopping PropertiesWatcher", Logger.LEVEL.DEBUG);
            watcher.interrupt();
            watcher = null;
        } catch (Exception ex){
            System.out.println("Exception in PropertiesWatcher.stop(): "+ex.getMessage());
        }
     }

    @SuppressWarnings("unchecked")
    private static void checkProperty(Properties props,  ArrayList<String> settings) {
         try {
             // Iterate through all properties
             for(String key : props.stringPropertyNames()){

                 // Get current and prior setting
                 String currentProperty = props.getProperty(key);
                 String cachedProperty;
                 if ( key.equals("site.networkConnCode") || key.equals("site.networkConnectionName") )
                 {
                     cachedProperty = MMHProperties.getCommonAppSetting(key);
                 }
                 else
                 {
                     cachedProperty = MMHProperties.getAppSetting(key);
                 }

                 // Check if this is a logging setting we are monitoring
                 if(settings.contains(key)){
                     if(!cachedProperty.equalsIgnoreCase(currentProperty)){
                         Logger.logMessage("Logging Level has changed: " + key +" : "+currentProperty, Logger.LEVEL.IMPORTANT);
                         changed = true;
                     }
                 }

                 if ( key.equals("site.networkConnCode") || key.equals("site.networkConnectionName") )
                 {
                     // Update properties if different
                     if(!cachedProperty.equalsIgnoreCase(currentProperty)){
                         MMHProperties.setCommonAppSetting(key, currentProperty);
                         Logger.logMessage("Updating Common App Property: "+key+" from "+cachedProperty+" to "+currentProperty, Logger.LEVEL.DEBUG);
                         MMHProperties.setCommonAppSetting(key, currentProperty);
                     }
                 }
                 else
                 {
                     // Update properties if different
                     if(!cachedProperty.equalsIgnoreCase(currentProperty)){
                         MMHProperties.setAppSetting(key, currentProperty);
                         Logger.logMessage("Updating App Property: "+key+" from "+cachedProperty+" to "+currentProperty, Logger.LEVEL.DEBUG);
                         MMHProperties.setAppSetting(key, currentProperty);
                     }
                 }
             }
         } catch (Exception ex){
            Logger.logMessage("Error in method: checkProperty()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
         }

    }
}
