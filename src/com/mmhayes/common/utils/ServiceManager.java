package com.mmhayes.common.utils;

//other dependencies
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

/*
 $Author: jrmitaly $: Author of last commit
 $Date: 2016-09-02 10:53:21 -0400 (Fri, 02 Sep 2016) $: Date of last commit
 $Rev: 2841 $: Revision of last commit
 Notes:
*/
public class ServiceManager{

    private final static int SERVICE_STATE_STOPPED = 1;
    private final static int SERVICE_STATE_START_PENDING = 2;
    private final static int SERVICE_STATE_STOP_PENDING = 3;
    private final static int SERVICE_STATE_RUNNING = 4;

    public int getServiceState(String serviceName, String logFileName) {
        //String[] queryScript = {"cmd.exe", "/c", "sc", "query", serviceName, "|", "find", "/C", "\"RUNNING\""};
        String[] queryScript = {"cmd.exe", "/c", "sc", "query", serviceName};
        Runtime runtime = Runtime.getRuntime();
        int ret = 0;
        try
        {
            Process p = runtime.exec(queryScript);
            p.waitFor();
            BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            String buffer = "";
            do
            {
                line = reader.readLine();
                buffer = buffer + line;
            } while (line!=null);
            Logger.logMessage(buffer,logFileName, Logger.LEVEL.DEBUG);
            if (buffer.indexOf("RUNNING") >= 0) {
                ret = SERVICE_STATE_RUNNING;
            }
            else if (buffer.indexOf("STOPPED") >= 0) {
                ret = SERVICE_STATE_STOPPED;
            }
            else if (buffer.indexOf("STOP_PENDING") >= 0) {
                ret = SERVICE_STATE_STOP_PENDING;
            }
            else if (buffer.indexOf("START_PENDING") >= 0) {
                ret = SERVICE_STATE_START_PENDING;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        Logger.logMessage("Service " + serviceName + " Status: " + ret,logFileName, Logger.LEVEL.TRACE);
        return ret;
    }

    private String getQueryValue(String line) {
        return line.substring(line.indexOf(":") + 1).trim();
    }

    public ArrayList<HashMap> getQCServices() {
        ArrayList<HashMap> ret = new ArrayList<HashMap>();

        String[] queryScript = {"cmd.exe", "/c", "sc", "query", "state=", "all"};
        Runtime runtime = Runtime.getRuntime();
        try
        {
            Process p = runtime.exec(queryScript);
            //p.waitFor();
            BufferedReader reader=new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            String buffer = "";
            int i = 1;
            do
            {
                line = reader.readLine();
                if (line!=null) {
                    if (line.indexOf("SERVICE_NAME") >= 0) {
                        if ((getQueryValue(line).compareToIgnoreCase("MySQL") == 0)
                                || (getQueryValue(line).compareToIgnoreCase("QCBckPrcr") == 0)
                                || (getQueryValue(line).toUpperCase().indexOf("QuickCharge".toUpperCase()) >= 0)
                                || (getQueryValue(line).toUpperCase().indexOf("Service_XMLUpdate".toUpperCase()) >= 0)
                                || (getQueryValue(line).toUpperCase().indexOf("srvMain".toUpperCase()) >= 0)
                                || (getQueryValue(line).toUpperCase().indexOf("srvTZMain".toUpperCase()) >= 0)
                            //|| (getQueryValue(line).compareToIgnoreCase("Spooler") == 0)
                                ) {
                            HashMap temp = new HashMap();
                        //    row.add(getQueryValue(line)); // ADD SERVICE_NAME
                            temp.put("SERVICENAME",getQueryValue(line));
                            buffer = buffer + line;

                            line = reader.readLine();
                         //   row.add(getQueryValue(line)); // ADD DISPLAY_NAME
                            temp.put("DISPLAYNAME",getQueryValue(line));
                            buffer = buffer + line;

                            line = reader.readLine(); // SKIP THIS LINE
                            buffer = buffer + line;

                            line = reader.readLine();
                       //     row.add(getQueryValue(line).substring(2)); // ADD STATUS
                            String[] status = getQueryValue(line).split(" ");
                            if(status.length > 2){
                                temp.put("STATUSID",status[0]);
                                String displayStatus = status[2].toLowerCase();
                                displayStatus = displayStatus.substring(0,1).toUpperCase() + displayStatus.substring(1);
                                temp.put("DISPLAYSTATUS",displayStatus);
                            }
                            buffer = buffer + line;
                          //  temp.put("SERVICEID",i);
                            i++;
                            ret.add(temp);
                        }
                    }
                    buffer = buffer + line;
                }
            } while (line!=null);
            Logger.logMessage(buffer,"", Logger.LEVEL.DEBUG);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return ret;
    }

    public boolean startService(String serviceName, String logFileName) {
        String[] startScript = {"cmd.exe", "/c", "sc", "start", serviceName};
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec(startScript);
        } catch (Exception e) {
            Logger.logMessage(e.getMessage(),logFileName, Logger.LEVEL.ERROR);
        }
        int waitSecs = 0;
        boolean isRunning = false;
        Thread thisThread = Thread.currentThread();
        do {
            try {
                thisThread.sleep(1000); // 1000 = 1 second
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            waitSecs = waitSecs + 1;
            isRunning = getServiceState(serviceName,logFileName) == this.SERVICE_STATE_RUNNING;
        } while (!isRunning && (waitSecs < 60));
        return isRunning;
    }

    public void restartTomcat(String logFileName) {
        Logger.logMessage("QCRESTART.BAT",logFileName, Logger.LEVEL.TRACE);
        Runtime runtime = Runtime.getRuntime();
        try {
            String[] stopScript = {"cmd.exe", "/c", "QCRESTART.BAT"}; // NOTE: THIS RUNS THE BAT, STOPS TOMCAT/JBOSS BUT FAILS TO START BACK UP
            runtime.exec(stopScript);
        } catch (Exception e) {
            Logger.logMessage(e.getMessage(),logFileName, Logger.LEVEL.ERROR);
        }
    }

    public boolean stopService(String serviceName, String logFileName) {
        String[] stopScript = {"cmd.exe", "/c", "sc", "stop", serviceName};
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec(stopScript);
        } catch (Exception e) {
            Logger.logMessage(e.getMessage(),logFileName, Logger.LEVEL.ERROR);
        }
        int waitSecs = 0;
        boolean isStopped = false;
        Thread thisThread = Thread.currentThread();
        do {
            try {
                thisThread.sleep(1000); // 1000 = 1 second
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            waitSecs = waitSecs + 1;
            isStopped = getServiceState(serviceName,logFileName) == this.SERVICE_STATE_STOPPED;
        } while (!isStopped && (waitSecs < 60));
        return isStopped;
    }

    public boolean restartService(String serviceName, String logFileName) {
        if (getServiceState(serviceName,logFileName) != this.SERVICE_STATE_STOPPED) {
            stopService(serviceName,logFileName);
        }
        return startService(serviceName,logFileName);
    }


}