package com.mmhayes.common.utils;

/*
 * $Author: jkflanagan $: Author of last commit
 * $Date: 2020-07-27 10:47:41 -0400 (Mon, 27 Jul 2020) $: Date of last commit
 * $Rev: 50462 $: Revision of last commit
 * Notes: Contains methods for converting between various data types.
*/

import org.apache.commons.lang.math.NumberUtils;

public class Convert {

    // convert a String to a boolean
    public static boolean strToBool (String s) {
        if (StringFunctions.stringHasContent(s)) {
            if ((s.equalsIgnoreCase("true")) || ((NumberUtils.isNumber(s)) && (s.equalsIgnoreCase("1")))) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    // convert a boolean to a String
    public static String boolToStr (boolean b) {
        return String.valueOf(b);
    }

    // convert a String into an int
    public static int strToInt (String s) {
        if (StringFunctions.stringHasContent(s)) {
            if (NumberUtils.isNumber(s)) {
                return Integer.parseInt(s);
            }
            else {
                return 0;
            }
        }
        return 0;
    }

    // convert an int into a String
    public static String intToStr (int i) {
        return String.valueOf(i);
    }

    // convert a String into a double
    public static double strToDbl (String s) {
        if (StringFunctions.stringHasContent(s)) {
            if (NumberUtils.isNumber(s)) {
                return Double.parseDouble(s);
            }
            else {
                return 0.0d;
            }
        }
        return 0.0d;
    }

    // convert a double into a String
    public static String dblToStr (double d) {
        return String.valueOf(d);
    }

    // convert Object to an Object of the given type
    public static <T> T objToType (Object o, Class<T> clazz) {
        try {
            return clazz.cast(o);
        }
        catch (ClassCastException e) {
            return null;
        }
    }

}