package com.mmhayes.common.utils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-29 16:39:47 -0400 (Fri, 29 May 2020) $: Date of last commit
    $Rev: 11836 $: Revision of last commit
    Notes: Implementation of a circular or ring buffer.
*/

public class MMHCircularBuffer {

    private byte[] buffer; // internal buffer which holds the data
    private int bufferSize; // size of the internal buffer
    private int start; // pointer to the beginning of the data within the buffer
    private int end; // pointer to the end of the data within the buffer

    /**
     * <p>Constructs a new {@link MMHCircularBuffer} instance with a default 1k buffer.</p>
     */
    public MMHCircularBuffer () {
        this(1024);
    }

    /**
     * <p>Constructs a new {@link MMHCircularBuffer} instance with buffer of a specified size.</p>
     * @param bufferSize The size of the internal buffer.
     */
    public MMHCircularBuffer (int bufferSize) {
        this.bufferSize = bufferSize;
        buffer = new byte[bufferSize];
        clear();
    }

    /**
     * <p>Resets the start and end data pointers, any data the was still within the buffer will be overwritten at some point.</p>
     */
    public void clear () {
        start = 0;
        end = 0;
    }

    /**
     * <p>Calculates how much space any data within the buffer is using.</p>
     * @return How much space any data within the buffer is using.
     */
    public int getUsedSpace () {
        return (start <= end ? end - start : end - start + bufferSize);
    }

    /**
     * <p>Calculates how much space any within the buffer isn't being used by any data.</p>
     * @return How much space any within the buffer isn't being used by any data.
     */
    public int getFreeSpace () {
        return (start <= end ? bufferSize + start - end : start - end);
    }

    /**
     * <p>Writes as much data as possible to the buffer.</p>
     * @param data A byte array containing the data to write to the buffer.
     * @return How much data was actually written to the buffer.
     */
    public int write (byte[] data) {
        return write(data, 0, data.length);
    }

    /**
     * <p>Writes as much data as possible to the buffer.</p>
     * @param data A byte array containing the data to write to the buffer.
     * @param offset Offset of the data we are interested in within the data byte array.
     * @param length The amount of data that should be written to the buffer.
     * @return How much data was actually written to the buffer.
     */
    public int write (byte[] data, int offset, int length) {
        if (length <= 0) {
            return 0;
        }

        int remainingNumBytesToWrite = length;

        int i = Math.min(remainingNumBytesToWrite, (end < start ? start : buffer.length) - end);
        if (i > 0) {
            System.arraycopy(data, offset, buffer, end, i);
            offset += i;
            remainingNumBytesToWrite -= i;
            end += i;
        }

        i = Math.min(remainingNumBytesToWrite, end >= start ? start : 0);
        if (i > 0) {
            System.arraycopy(data, offset, buffer, 0, i);
            remainingNumBytesToWrite -= i;
            end = i;
        }

        return length - remainingNumBytesToWrite;
    }

    /**
     * <p>Reads as much data as possible from the buffer.</p>
     * @param data A byte array where data from the buffer will be stored.
     * @return How much data was actually read from the buffer.
     */
    public int read (byte[] data) {
        return read(data, 0, data.length);
    }

    /**
     * <p>Reads as much data as possible from the buffer.</p>
     * @param data A byte array where data from the buffer will be stored.
     * @param offset Offset of the data we are interested in within the data byte array.
     * @param length The amount of data that should be read from the buffer.
     * @return How much data was actually read from the buffer.
     */
    public int read (byte[] data, int offset, int length) {
        if (length <= 0) {
            return 0;
        }

        int remainingNumBytesToRead = length;

        int i = Math.min(remainingNumBytesToRead, (end < start ? buffer.length : end) - start);
        if (i > 0) {
            System.arraycopy(buffer, start, data, offset, i);
            offset += i;
            remainingNumBytesToRead -= i;
            start  += i;
            if (start >= buffer.length) {
                start = 0;
            }
        }

        i = Math.min(remainingNumBytesToRead, end >= start ? 0 : end);
        if (i > 0 ) {
            System.arraycopy(buffer, 0, data, offset, i);
            remainingNumBytesToRead -= i;
            start = i;
        }

        return length - remainingNumBytesToRead;
    }

}