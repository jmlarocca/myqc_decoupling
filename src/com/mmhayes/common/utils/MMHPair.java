package com.mmhayes.common.utils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-08 14:45:35 -0400 (Thu, 08 Apr 2021) $: Date of last commit
    $Rev: 13771 $: Revision of last commit
    Notes: Implementation of an immutable pair.
*/

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>Implementation of an immutable pair.</p>
 *
 */
public class MMHPair<K, V> {

    // private member variables of a MMHPair
    private final K key;
    private final V value;

    /**
     * <p>Private constructor for a {@link MMHPair}.</p>
     *
     * @param key The key for the {@link MMHPair}.
     * @param value The value for the {@link MMHPair}.
     */
    private MMHPair (K key, V value) {
        this.key = key;
        this.value = value;
    }

    /**
     * <p>Calls the private constructor and returns an instance of a {@link MMHPair}.</p>
     *
     * @param key The key for the {@link MMHPair}.
     * @param value The value for the {@link MMHPair}.
     * @return An instance of a {@link MMHPair}.
     */
    public static <K, V> MMHPair <K, V> of (K key, V value) {
        return new MMHPair<>(key, value);
    }

    /**
     * <p>Calls the private constructor and returns an instance of a {@link MMHPair} created from the given {@link HashMap}.</p>
     *
     * @param hm The {@link HashMap} to convert to a {@link MMHPair}.
     * @return An instance of a {@link MMHPair}.
     */
    public static <K, V> MMHPair <K, V> fromHashMap (HashMap<K, V> hm) {

        // can't create a MMHPair from a null or empty HashMap
        if (DataFunctions.isEmptyMap(hm)) {
            return null;
        }

        Map.Entry<K, V> firstEntry = hm.entrySet().iterator().next();

        return new MMHPair<>(firstEntry.getKey(), firstEntry.getValue());
    }

    /**
     * <p>Gets the key stored in this {@link MMHPair}.</p>
     *
     * @return The key {@link K} stored in this {@link MMHPair}.
     */
    public K getKey () {
        return key;
    }

    /**
     * <p>Gets the value stored in this {@link MMHPair}.</p>
     *
     * @return The value {@link V} stored in this {@link MMHPair}.
     */
    public V getValue () {
        return value;
    }

    /**
     * <p>Checks whether or not the {@link MMHPair} has any content.</p>
     *
     * @return Whether or not the {@link MMHPair} has any content.
     */
    public boolean isEmpty () {
        return (key == null);
    }

    /**
     * <p>Overridden toString() method for a {@link MMHPair}.</p>
     *
     * @return A {@link String} representation of this {@link MMHPair}.
     */
    @Override
    public String toString () {
        return String.format("KEY: %s, VALUE: %s",
                Objects.toString(key, "N/A"),
                Objects.toString(value, "N/A"));
    }

    /**
     * <p>Overridden equals() method for a {@link MMHPair}.
     * Two {@link MMHPair} are defined as being equal if they have the same key and value.</p>
     *
     * @param obj The {@link Object} to compare against this {@link MMHPair}.
     * @return Whether or not the {@link Object} is equal to this {@link MMHPair}.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast obj to a MMHPair
        MMHPair<?, ?> mmhPair = (MMHPair<?, ?>) obj;
        return ((Objects.equals(mmhPair.key, key)) && (Objects.equals(mmhPair.value, value)));
    }

    /**
     * <p>Overridden hashCode() method for a {@link MMHPair}.</p>
     *
     * @return The unique hash code for this {@link MMHPair}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(key, value);
    }

}