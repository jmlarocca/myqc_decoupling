package com.mmhayes.common.utils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-20 13:10:34 -0400 (Tue, 20 Apr 2021) $: Date of last commit
    $Rev: 13819 $: Revision of last commit
    Notes: Abstract implementation of the pool interface containing objects of a generic type.
*/

abstract class MMHAbstractPool<T> implements MMHPool<T> {

    /**
     * <p>Releases the object and returns it to the {@link MMHPool}.</p>
     *
     * @param t The Object to release and return to the pool.
     */
    @Override
    public void release (T t) {
        if (t == null) {
            return;
        }

        if (isValid(t)) {
            returnToMMHPool(t);
        }
        else {
            handleInvalidReturn(t);
        }
    }

    /**
     * <p>Checks whether or not objects of the generic type are valid or not.</p>
     *
     * @param t The object to validate.
     * @return Whether or not objects of the generic type are valid or not.
     */
    protected abstract boolean isValid(T t);

    /**
     * <p>Returns the object of the generic type back into the {@link MMHPool}.</p>
     *
     * @param t The generic object to return to the pool.
     */
    protected abstract void returnToMMHPool(T t);

    /**
     * <p>Handles the scenario where the object to return to the {@link MMHPool} is invalid.</p>
     *
     * @param t The generic job which is invalid and is unable to be returned to the {@link MMHPool}.
     */
    protected abstract void handleInvalidReturn(T t);

}