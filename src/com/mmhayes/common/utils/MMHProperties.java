package com.mmhayes.common.utils;

/*
 $Author: rawithers $: Author of last commit
 $Date: 2021-03-08 16:10:58 -0500 (Mon, 08 Mar 2021) $: Date of last commit
 $Rev: 13597 $: Revision of last commit
 Notes:
*/

import org.slf4j.event.KeyValuePair;
import javax.servlet.ServletContext;
import java.io.*;
import java.nio.file.Files;
import java.util.*;

public class MMHProperties {
    private static HashMap appSettings;
    //SC2P: "common app properties" are no longer used, why did they ever exist?
    private static HashMap commonAppSettings;
    private static HashMap<String,String> sqlStrings;
    private static HashMap peripheralStrings;
    public static String projectSQLFile = "";
    public static String SQL_STRINGS_FILE = "";
    private static String COMMON_SQL_STRINGS_FILE = "";
    private static String QC_POS_SQL_STRINGS_FILE = "";
    public static String APP_SETTINGS_FILE = "";
    public static String COMMON_APP_SETTINGS_FILE = "";
    private static String PERIPHERALS_FILE = "Peripherals.properties";
    public static String APP_PROP_PATH = "";
    public static String COMMON_APP_PROP_PATH = "";
    public static String SYNC_APP_PROP_PATH = "";
    public static String PROP_FILES_PATH = "";
    public static String applicationPropertyDirName = "properties";

    // Allows for loading of instance property files
    public static void loadInstancePropertyFiles(ServletContext sc) throws IOException {
        //SC2P: load the properties from environmental variables because these are easily accessible in containers
        loadApplicationProperties();

        //SC2P: do not try to load the properties from filesystem
        // /-*
        // Use common paths and names for the application files
        APP_PROP_PATH = MMHServletStarter.getInstancePropertyLocation();
        COMMON_APP_PROP_PATH = MMHServletStarter.getInstanceCommonPropertyLocation();
        //APP_SETTINGS_FILE = MMHServletStarter.getInstanceIdentifier() + ".properties";  //removed 11/14/2014 now so all files are app.properties
        //APP_SETTINGS_FILE = "app.properties";
        COMMON_APP_SETTINGS_FILE = "common_app.properties";

        // /-*
        // Load the application properties with a dynamic name based on the instance
        String appFileFullPath = (MMHServletStarter.getInstancePropertyLocation() + "/" + APP_SETTINGS_FILE).replace("//", "/");
        Logger.logMessage("LOADING APPLICATION PROPERTIES FILE FROM: " + appFileFullPath, Logger.LEVEL.DEBUG);
        appSettings = new HashMap();
        try {
            FileInputStream propFile = new FileInputStream(appFileFullPath);
            PropertyResourceBundle appRes = new PropertyResourceBundle(propFile);
            saveAppProperties(appRes);
            Logger.logMessage("LOADED APPLICATION PROPERTIES FILE FROM: " + appFileFullPath, Logger.LEVEL.TRACE);
        } catch (Exception ex) {
            StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            Logger.logMessage("FAILED TO LOAD APPLICATION PROPERTIES - Due to exception:"+sw.toString(), Logger.LEVEL.ERROR);
        }
        // *-/

        if (appSettings != null) {
            // Load the SQL files with a static name and path within the war
            sqlStrings = new HashMap<>();
            try {
                SQL_STRINGS_FILE = MMHServletStarter.getApplicationIdentifier() + "_SQLStrings" + getAppSetting("database.type") + ".properties";
                COMMON_SQL_STRINGS_FILE = "COMMON_SQLStrings" + getAppSetting("database.type") + ".properties";
                PROP_FILES_PATH = MMHServletStarter.getApplicationPropertyLocation();
                String commonSqlFileFullPath = "/WEB-INF/classes/" + COMMON_SQL_STRINGS_FILE;
                String sqlFileFullPath = "/WEB-INF/classes/" + SQL_STRINGS_FILE;

                // Load Common SQL
                loadSQLPropertiesFilesAsResource(commonSqlFileFullPath,sc);

                // Load Instance SQL
                loadSQLPropertiesFilesAsResource(sqlFileFullPath,sc);

            } catch (Exception ex) {
                StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw);
                ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
                Logger.logMessage("ERROR: FAILED TO LOAD SQL FILE - Due to exception:"+sw.toString(), Logger.LEVEL.ERROR);
            }
        }

        //SC2P: "common app properties" are no longer used, why did they ever exist?
        // /-*
        // Load the common application properties with a dynamic name based on the instance
        String commonAppFileFullPath = (MMHServletStarter.getInstanceCommonPropertyLocation() + "/" + COMMON_APP_SETTINGS_FILE).replace("//", "/");
        Logger.logMessage("LOADING COMMON APPLICATION PROPERTIES FILE FROM: " + commonAppFileFullPath, Logger.LEVEL.DEBUG);
        commonAppSettings = new HashMap();
        try {
            FileInputStream commonPropFile = new FileInputStream(commonAppFileFullPath);
            PropertyResourceBundle commonAppRes = new PropertyResourceBundle(commonPropFile);
            saveCommonAppProperties(commonAppRes);
            Logger.logMessage("LOADED COMMON APPLICATION PROPERTIES FILE FROM: " + commonAppFileFullPath, Logger.LEVEL.TRACE);
        } catch (Exception ex) {
            Logger.logMessage("FAILED TO LOAD COMMON APPLICATION PROPERTIES; This application may not need a common applications file.", Logger.LEVEL.ERROR);
        }
        // *-/

        // No attempt to load additional properties files, this is intentional
        // - If additional files are loaded it should be per war (i.e. QC POS can load whatever properties it needs when it's broken out into a separate war)
    }

    // Load the environmental variables to the appSettings based on prefix
    public static void loadPrefixedEnvVariables(Map<String, String> allEnvVars, String prefix) {
        prefix = prefix.toLowerCase();
        if (!prefix.endsWith(".")) {
            prefix += ".";
        }
        for (Map.Entry<String, String> v : allEnvVars.entrySet()) {
            String k = v.getKey().toLowerCase();
            if (k.startsWith(prefix)) {
                String n = k.substring(prefix.length());
                //appSettings.put(v.getKey().substring(prefix.length()), v.getValue());
                appSettings.put(n, v.getValue());
            }
        }
    }

    //allows loading (or re-loading) of ALL property files
    public static void loadAllPropertyFiles(String appPropPath, String commonAppPropPath, String propFilesPath, String appSettingsFile, String commonAppSettingsFile, String sqlFile) throws IOException {
        //SC2P: switch overload
        loadAllPropertyFiles(propFilesPath, sqlFile, true);
        //loadAllPropertyFiles(appPropPath, commonAppPropPath, propFilesPath, appSettingsFile, commonAppSettingsFile, sqlFile, true);
    }

    //allows loading (or re-loading) of ALL property files
    public static void loadAllPropertyFiles(String appPropPath, String commonAppPropPath, String propFilesPath, String appSettingsFile, String commonAppSettingsFile, String sqlFile, Boolean loadQCPOS) throws IOException {
        //SC2P: switch overload
        loadAllPropertyFiles(propFilesPath, sqlFile, loadQCPOS);
        // /-*
        //reset paths
        APP_PROP_PATH = appPropPath;
        Logger.logMessage("Reset APP_PROP_PATH to: "+appPropPath, Logger.LEVEL.DEBUG);
        COMMON_APP_PROP_PATH = commonAppPropPath;
        Logger.logMessage("Reset COMMON_APP_PROP_PATH to: "+commonAppPropPath, Logger.LEVEL.DEBUG);
        PROP_FILES_PATH = propFilesPath;
        Logger.logMessage("Reset PROP_FILES_PATH to: "+propFilesPath, Logger.LEVEL.DEBUG);

        //reset other application file settings
        appSettings = null;
        APP_SETTINGS_FILE = appSettingsFile;
        COMMON_APP_SETTINGS_FILE = commonAppSettingsFile;
        //reset sql file settings
        sqlStrings = null;
        projectSQLFile = sqlFile;
        SQL_STRINGS_FILE = "";

        //load application properties file - APP_SETTINGS_FILE
        loadApplicationPropertiesFile(appPropPath, commonAppPropPath, propFilesPath);

        if (appSettings != null) {
            loadSQLPropertyFiles(propFilesPath);
            if (loadQCPOS) {
                //reset QC POS settings
                QC_POS_SQL_STRINGS_FILE = "";
                peripheralStrings = null;
                loadQCPOSSQLPropertyFiles(propFilesPath); //load both QC and QC POS SQL files
                //loadPeripheralPropertiesFile(propFilesPath); //load QC POS Peripheral file - PERIPHERALS_FILE
                //loadPeripheralPropertiesFile(MMHServletStarter.getInstanceCommonPropertyLocation()); //load QC POS Peripheral file - PERIPHERALS_FILE
            }
        }
        // *-/
    }

    //SC2P: new version with far fewer parameters
    // Allows loading (or re-loading) of ALL properties
    public static void loadAllPropertyFiles(String propFilesPath, String sqlFile, Boolean loadQCPOS) throws IOException {
        projectSQLFile = sqlFile;
        SQL_STRINGS_FILE = "";
        loadApplicationProperties();
        if (appSettings != null) {
            loadSQLPropertyFiles(propFilesPath);
            if (loadQCPOS) {
                //reset QC POS settings
                QC_POS_SQL_STRINGS_FILE = "";
                peripheralStrings = null;
                loadQCPOSSQLPropertyFiles(propFilesPath); //load both QC and QC POS SQL files
            }
        }
    }

    // Load application properties from environment variables
    public static void loadApplicationProperties() {
        Logger.logMessage("Attempting to load application properties from environment", Logger.LEVEL.DEBUG);
        appSettings = new HashMap();
        String mmhPrefix = "mmh";
        String qcPrefix = "qc";
        String contextPrefix = MMHServletStarter.getInstanceIdentifier();
        Map<String, String> allEnvVars = new HashMap<String, String>();
        // Hard-code some properties, will eventually not need them
        appSettings.put("database.type", "SQLSVR");
        appSettings.put("database.connection.driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver");
        // Get all the environmental variables to sift through
        Map<String, String> sysEnvVars = System.getenv();
        // For linux there may be underscores instead of periods, replace them and only include variables that start with one of the prefixes
        for (Map.Entry<String, String> envVar : sysEnvVars.entrySet()) {
            String envName = envVar.getKey().replaceAll("_", ".");
            String envValue = envVar.getValue();
            if (envName.startsWith(mmhPrefix) || envName.startsWith(qcPrefix) || envName.startsWith(contextPrefix)) {
                allEnvVars.put(envName, envValue);
            }
        }
        // Get all the environmental variables with the "mmh" prefix
        loadPrefixedEnvVariables(allEnvVars, mmhPrefix);
        // Get all the environmental variables with the "qc" prefix, overwrite mmh prefixed variables
        loadPrefixedEnvVariables(allEnvVars, qcPrefix);
        // Get all the environmental variables with a prefix that matches the war, overwrite mmh and qc prefixed variables
        loadPrefixedEnvVariables(allEnvVars, contextPrefix);
        // Order of overwriting is such that the most specific variable will prevail
    }

    //SC2P: don't use properties from files
    // /-*
    //load application properties file
    public static void loadApplicationPropertiesFile(String appPropPath, String commonAppPropPath, String propFilesPath) {
        Logger.logMessage("Attempting to load application properties from path: " + appPropPath, Logger.LEVEL.DEBUG);
        Logger.logMessage("Attempting to load common application properties from path: " + commonAppPropPath, Logger.LEVEL.DEBUG);
        appSettings = new HashMap();
        try {
            FileInputStream propFile = new FileInputStream(appPropPath + "\\" + APP_SETTINGS_FILE);
            PropertyResourceBundle appRes = new PropertyResourceBundle(propFile);
            saveAppProperties(appRes);
            Logger.logMessage("Successfully loaded application properties file '"+APP_SETTINGS_FILE+"'  from " + appPropPath + "\\", Logger.LEVEL.TRACE);
        } catch (IOException ioe) {
            Logger.logMessage("Could not find "+APP_SETTINGS_FILE+" @ path: " + appPropPath, Logger.LEVEL.WARNING);
            Logger.logMessage("Re-attempting to load application properties from path: " + propFilesPath, Logger.LEVEL.WARNING);
            try {
                FileInputStream propFile = new FileInputStream(propFilesPath + "\\" + APP_SETTINGS_FILE);
                PropertyResourceBundle appRes = new PropertyResourceBundle(propFile);
                saveAppProperties(appRes);
                Logger.logMessage("Successfully loaded application properties file '"+APP_SETTINGS_FILE+"'  from " + propFilesPath + "\\", Logger.LEVEL.TRACE);
            } catch (IOException ioe2) {
                Logger.logMessage("ERROR: Could not load application properties file '"+APP_SETTINGS_FILE+"'  from " + propFilesPath + "\\", Logger.LEVEL.ERROR);
            }
        }
        commonAppSettings = new HashMap();
        try {
            FileInputStream commonPropFile = new FileInputStream(commonAppPropPath + "\\" + COMMON_APP_SETTINGS_FILE);
            PropertyResourceBundle commonAppRes = new PropertyResourceBundle(commonPropFile);
            saveCommonAppProperties(commonAppRes);
            Logger.logMessage("Successfully loaded common application properties file '"+COMMON_APP_SETTINGS_FILE+"'  from " + commonAppPropPath + "\\", Logger.LEVEL.TRACE);
        } catch (IOException ioe) {
            Logger.logMessage("Could not find "+COMMON_APP_SETTINGS_FILE+" @ path: " + commonAppPropPath, Logger.LEVEL.WARNING);
            Logger.logMessage("Re-attempting to load common application properties from path: " + propFilesPath, Logger.LEVEL.WARNING);
            try {
                FileInputStream commonPropFile = new FileInputStream(propFilesPath + "\\" + COMMON_APP_SETTINGS_FILE);
                PropertyResourceBundle commonAppRes = new PropertyResourceBundle(commonPropFile);
                saveCommonAppProperties(commonAppRes);
                Logger.logMessage("Successfully loaded common application properties file '"+COMMON_APP_SETTINGS_FILE+"'  from " + propFilesPath + "\\", Logger.LEVEL.TRACE);
            } catch (IOException ioe2) {
                Logger.logMessage("Could not load common application properties file '"+COMMON_APP_SETTINGS_FILE+"'  from " + propFilesPath + "\\; This application may not need a common applications file.", Logger.LEVEL.ERROR);
            }
        }
    }
    // *-/

    //load sql
    public static void loadSQLPropertyFiles(String propFilesPath) {
        loadSQLPropertyFiles(propFilesPath, projectSQLFile);
    }

    //load sql
    public static void loadSQLPropertyFiles(String propFilesPath, String sqlFile) {
        sqlStrings = new HashMap<>();
        try {
            SQL_STRINGS_FILE = sqlFile + getAppSetting("database.type") + ".properties";
            Logger.logMessage("Attempting to load SQL File properties from path: " + propFilesPath, Logger.LEVEL.DEBUG);
            FileInputStream propFile = new FileInputStream(propFilesPath + "\\" + SQL_STRINGS_FILE);
            PropertyResourceBundle sqlRes = new PropertyResourceBundle(propFile);
            saveSqlStrings(sqlRes);
            Logger.logMessage("Successfully loaded SQL File properties file '"+SQL_STRINGS_FILE+"'  from " + propFilesPath + "\\", Logger.LEVEL.TRACE);
        } catch (Exception ex) {
            StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            Logger.logMessage("ERROR: Could not load SQL File properties file Due to exception:"+sw.toString(), Logger.LEVEL.ERROR);
        }
    }

    private static void loadSQLPropertiesFilesAsResource(String sqlFile, ServletContext sc) throws Exception{
        try {
            Logger.logMessage("LOADING SQL FILE: " + sqlFile, Logger.LEVEL.TRACE);
            InputStream propFile = sc.getResourceAsStream(sqlFile);
            PropertyResourceBundle sqlRes = new PropertyResourceBundle(propFile);
            saveSqlStrings(sqlRes);
            Logger.logMessage("LOADED SQL FILE: " + sqlFile, Logger.LEVEL.TRACE);
        } catch (Exception ex) {
            Logger.logMessage("ERROR: FAILED TO LOAD SQL FILE:"+sqlFile+ " Exception: "+ex.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    //load qc pos sql
    public static void loadQCPOSSQLPropertyFiles(String propFilesPath) {
        //try to load QC POS properties file
        try {
            //Load in another properties file for QC_POS and save into the sqlStrings property
            QC_POS_SQL_STRINGS_FILE = "QC_POS_SQLStrings" + getAppSetting("database.type") + ".properties";
            FileInputStream pos_propFile = new FileInputStream(propFilesPath + "\\" + QC_POS_SQL_STRINGS_FILE);
            PropertyResourceBundle pos_sqlRes = new PropertyResourceBundle(pos_propFile);
            saveSqlStrings(pos_sqlRes);
            Logger.logMessage("Successfully loaded QC POS SQL File properties file '"+QC_POS_SQL_STRINGS_FILE+"'  from " + propFilesPath + "\\", Logger.LEVEL.TRACE);
        } catch (IOException ioe) {
            Logger.logMessage("ERROR: Could not load QC POS SQL File properties file '"+QC_POS_SQL_STRINGS_FILE+"'  from " + propFilesPath + "\\", Logger.LEVEL.ERROR);
        }
    }

    //load qc pos peripherals
    public static void loadPeripheralPropertiesFile(String propFilesPath) {
        //try to load QC POS Peripheral Properties File
        peripheralStrings = new HashMap();
        try {
            FileInputStream propFile = new FileInputStream(propFilesPath + "\\" + PERIPHERALS_FILE);
            PropertyResourceBundle perphRes = new PropertyResourceBundle(propFile);
            peripheralStrings = new HashMap();
            savePeripheralStrings(perphRes);
            Logger.logMessage("Successfully loaded QC POS Peripherals properties file '"+PERIPHERALS_FILE+"'  from " + propFilesPath + "\\", Logger.LEVEL.TRACE);
        } catch (IOException ioe) {
            Logger.logMessage("ERROR: Could not load QC POS Peripherals properties file '"+PERIPHERALS_FILE+"'  from " + propFilesPath + "\\", Logger.LEVEL.ERROR);
        }
    }

    public static void PrintProperties(PropertyResourceBundle res) {
        Enumeration enumer = res.getKeys();
        while (enumer.hasMoreElements()) {
            String key = enumer.nextElement().toString();
            System.out.println(key + " = " + res.getString(key));
        }
    }

    public static void saveAppProperties(PropertyResourceBundle res) {
        Enumeration enumer = res.getKeys();
        while (enumer.hasMoreElements()) {
            String key = enumer.nextElement().toString();
            appSettings.put(key, res.getString(key));
        }
    }

    //SC2P: "common app properties" are no longer used, why did they ever exist?
    // /-*
    public static void saveCommonAppProperties(PropertyResourceBundle res) {
        Enumeration enumer = res.getKeys();
        while (enumer.hasMoreElements()) {
            String key = enumer.nextElement().toString();
            commonAppSettings.put(key, res.getString(key));
        }
    }
    // *-/

    public static void saveSqlStrings(PropertyResourceBundle res) {
        Enumeration enumer = res.getKeys();
        ArrayList<String> duplicateKeys = new ArrayList<String>();
        while (enumer.hasMoreElements()) {
            String isSame = "";
            String key = enumer.nextElement().toString();
            String loadedQuery;
            String newQuery = res.getString(key);
            if(sqlStrings.containsKey(key)) {
                loadedQuery = sqlStrings.get(key);
                isSame = loadedQuery.equalsIgnoreCase(newQuery) ? " [IDENTICAL]" : " [DIFFERENT]";
                Logger.logMessage("FOUND DUPLICATE QUERY: "+key+isSame, Logger.LEVEL.IMPORTANT);

                if(isSame.equalsIgnoreCase(" [IDENTICAL]"))
                    duplicateKeys.add(key);
            }

            sqlStrings.put(key, newQuery);
        }

        //removeProperties(duplicateKeys);
    }

    public static void savePeripheralStrings(PropertyResourceBundle res) {
        Enumeration enumer = res.getKeys();
        while (enumer.hasMoreElements()) {
            String key = enumer.nextElement().toString();
            peripheralStrings.put(key, res.getString(key));
        }
    }

    public static String getPeripheralsSetting(String key) {
        try {
            if (peripheralStrings == null) {
                //loadPeripheralPropertiesFile(MMHServletStarter.getInstanceCommonPropertyLocation());
            }
            String ret = peripheralStrings.get(key).toString();
            return ret == null ? "" : ret;
        } catch (Exception e) {
            Logger.logMessage("MMHProperties.getPeripheralsSetting('" + key + "'). This may be because the property key is simply not in the properties file.", Logger.LEVEL.WARNING);
            return "";
        }
    }

    public static String getAppSetting(String key) {
        try {
            if (appSettings == null || appSettings.isEmpty()) {
                //SC2P: don't use files
                loadApplicationProperties();
                //loadApplicationPropertiesFile(APP_PROP_PATH, COMMON_APP_PROP_PATH, PROP_FILES_PATH);
            }
            String ret = appSettings.get(key).toString();

            return ret == null ? "" : ret;
        } catch (Exception e) {
            Logger.logMessage("MMHProperties.getAppSetting('" + key + "'). This may be because the property key is simply not in the properties file.", Logger.LEVEL.WARNING);
            return "";
        }
    }

    public static String getCommonAppSetting(String key) {
        //SC2P: "common app properties" are no longer used, why did they ever exist?
        return getAppSetting(key);
        /*
        try {
            if (commonAppSettings == null) {
                loadApplicationPropertiesFile(APP_PROP_PATH, COMMON_APP_PROP_PATH, PROP_FILES_PATH);
            }
            String ret = commonAppSettings.get(key).toString();
            return ret == null ? "" : ret;
        } catch (Exception e) {
            Logger.logMessage("MMHProperties.getCommonAppSetting('" + key + "'). This may be because the property key is simply not in the common properties file.", Logger.LEVEL.WARNING);
            return "";
        }
        */
    }

    public static void setAppSetting(String key, String value) {
        appSettings.put(key, value);
    }
    public static void setCommonAppSetting(String key, String value) {
        //SC2P: "common app properties" are no longer used, why did they ever exist?
        setAppSetting(key, value);
        //commonAppSettings.put(key, value);
    }
    public static String getSqlString(String key) {
        return getSqlString(key,"");
    }

    public static String getSqlString(String key, String logFileName) {
        Logger.logMessage("QUERY: " + key, logFileName, Logger.LEVEL.TRACE);
        String ret = "";
        if (sqlStrings == null) {
            loadSQLPropertyFiles(PROP_FILES_PATH, "");
        }
        if (sqlStrings != null) {
            try {
                ret = sqlStrings.get(key);
            } catch (Exception e) {
                //Logger.logMessage("Error in PropertyReader = " + e.getMessage(), logFileName);
                StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
                Logger.logMessage("Error in PropertyReader. Could not load SQL string for '" + key + "' . Exception: "+sw.toString(), logFileName, Logger.LEVEL.ERROR);
            }
        }
        ret = ret == null ? "" : ret;
        Logger.logMessage("SQL: " + ret, logFileName, Logger.LEVEL.LUDICROUS);
        return ret;
    }

    public static String getSqlStringForSQLRunner(String sqlProperty) {
        return getSqlStringForSQLRunner(sqlProperty,"");
    }

    public static String getSqlStringForSQLRunner(String sqlProperty, String logFileName) {
        String sqlString = "";
        try {
            Logger.logMessage("INFO: (SQLRunner) - About to retrieve SQL for '" + sqlProperty + "'", logFileName, Logger.LEVEL.DEBUG);
            if (sqlStrings == null) {
                loadSQLPropertyFiles(PROP_FILES_PATH, "");
            }
            if (sqlStrings != null) {
                sqlString = sqlStrings.get(sqlProperty);
            }
            sqlString = sqlString == null ? "" : sqlString;
        } catch (Exception ex) {
            Logger.logMessage("ERROR: (SQLRunner) - Error in PropertyReader. Could not load SQL string for '" + sqlProperty, Logger.LEVEL.ERROR);
            Logger.logException(ex);
            return sqlString;
        }
        Logger.logMessage("INFO: (SQLRunner) - Retrieved SQL for '" + sqlProperty + "' - SQL: " + sqlString, Logger.LEVEL.TRACE);
        return sqlString;
    }

    //SC2P: This is crazy, don't allow it
    /*
    *    treat the app.properties file as an ordinary text file
    *   the Properties class loads the text from a FileInputStream and removes the `\`
    *   `keys` is an ArrayList of Strings of the identical keys to be removed
    */
    /*
  public static void removeProperties(ArrayList<String> keys) {
        if(keys.size() == 0) return;
        File propertiesFile = null;
        File newFile = null;
        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            String newFilePath ="C:\\IdeaProjects\\NewPropsFiles\\" +
                    MMHServletStarter.getApplicationIdentifier();
            newFile = new File(newFilePath + "\\new_app.properties");
            // create path if needed
            new File(newFilePath).mkdirs();
            newFile.createNewFile();

            String filePath = APP_PROP_PATH + "/" + SQL_STRINGS_FILE;
            propertiesFile = new File(filePath);
            br = new BufferedReader(new FileReader(propertiesFile));
            bw = new BufferedWriter(new FileWriter(newFile, false));
            List<String> lines = Files.readAllLines(propertiesFile.toPath());
            String line;

            for( int i = 0; i < lines.size(); i++ ) {
                line = lines.get(i);
                try {
                    // check the line is a key, just write if not
                    if (line.startsWith("data.")) {
                        // get the substring up to the '='
                        int equalsIndex = line.indexOf('=');
                        String substring = line.substring(0, equalsIndex);
                        // if the keys AL contains the found substring
                        if (keys.contains(substring.trim())) {
                            // skip all lines until we get to an empty line or line with just whitespace
                            while (!(lines.get(i).trim().isEmpty())) i++;
                        // the key was unique or different and will need to be checked manually
                        } else bw.write(line + "\n");
                    // not a key nor a query following a duplicate key
                    } else bw.write(line + "\n");
                } catch (Exception e) {
                    Logger.logMessage("Out of Range in MMHProperties.removeProperties() - " + e.getMessage(), Logger.LEVEL.ERROR);
                }
            }
        }
        catch(IOException ioe) {
            Logger.logMessage("ERROR: IOException in MMHProperties.removeProperties() - " + ioe.getMessage(), Logger.LEVEL.ERROR);
        }
        catch (Exception e) {
            Logger.logMessage("ERROR: Exception in MMHProperties.removeProperties() - "  + e.getMessage(), Logger.LEVEL.ERROR);
        }
        // try to close our buffered reader and buffered writer
        finally {
            try {
                if(br!= null) br.close();
                if(bw != null) bw.close();
            }
            catch (IOException ioe) {
                Logger.logMessage("ERROR: Could not close reader, writer, or file in MMHProperties.removeProperties() - " + ioe.getMessage(), Logger.LEVEL.ERROR);
            }

        }
    }
    */
}