package com.mmhayes.common.utils;

import java.util.HashMap;
import com.mmhayes.common.dataaccess.*;
import java.util.ArrayList;
import java.sql.SQLException;
import java.util.regex.*;

/**
 * <p>Title: QuickCharge 4</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: MMHayes</p>
 * @author unascribed
 * @version 4.0
 */

public class TimekeeperLink {

  private boolean activeLink;
  private String timekeeperUrl;
  private DataManager dm;

  public TimekeeperLink() {
    dm = new DataManager();
    ArrayList alKronosParameters = dm.resultSetToHashMap(dm.runSql("SELECT INTEGRATEKRONOS, KRONWEBSERVER FROM MMHGGLOBALS"),true);
    HashMap kronosParameters = (HashMap)alKronosParameters.get(0);
    if (kronosParameters.get("INTEGRATEKRONOS").toString().compareTo("true") == 0) {
      activeLink = true;
      String tmpUrl = kronosParameters.get("KRONWEBSERVER").toString();
      String urlReplacement = "$1logon";
      Pattern urlPattern = Pattern.compile("^(http[s]?\\:\\/\\/[A-Za-z0-9-.]+\\/[A-Za-z0-9-.]+\\/).*$");
      Matcher matchedUrl = urlPattern.matcher(tmpUrl);
      timekeeperUrl = matchedUrl.replaceAll(urlReplacement);
    } else {
      activeLink = false;
      timekeeperUrl = "";
    }
  }

  public boolean IsTimeKeeperActive() {
    return activeLink;
  }

  public String GetTimekeeperUrl() {
    return timekeeperUrl;
  }

  public boolean CanChangePassword(String userId) {
    dm = new DataManager();
    ArrayList alKronosParameters = dm.resultSetToHashMap(dm.runSql("SELECT AUTHENTICATIONTYPEID FROM MMHGPERSON WHERE PERSONID = '" + userId + "'"),true);
    HashMap kronosParameters = (HashMap)alKronosParameters.get(0);
    if (kronosParameters.get("AUTHENTICATIONTYPEID").toString().compareTo("1") == 0) {
      return true;
    } else {
      return false;
    }
  }

}
