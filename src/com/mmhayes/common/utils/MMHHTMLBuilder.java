package com.mmhayes.common.utils;

/*
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-11-06 09:59:21 -0500 (Fri, 06 Nov 2020) $: Date of last commit
    $Rev: 13059 $: Revision of last commit
    Notes: This may be used in place of manually building html using a StringBuilder.
*/

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;

/**
 * <p>This may be used in place of manually building html using a StringBuilder.</p>
 *
 */
public class MMHHTMLBuilder {

// <editor-fold desc="Private Member Variables">
    // private member variables of a MMHHTMLBuilder
    private StringBuilder sb;
// </editor-fold>

// <editor-fold desc="MMHHTMLBuilder Constructors">
    /**
     * <p>Constructor for a {@link MMHHTMLBuilder}.</p>
     *
     */
    public MMHHTMLBuilder () {sb = new StringBuilder();}

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "[CONTENT]" appended.</p>
     *
     *
     * @param content The content {@link String} to add.
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addContent (String content) {
        sb.append(content);
        return this;
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "[HTML DOCTYPE]" appended.</p>
     *
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addHTMLDOCTYPE () {
        sb.append("<!DOCTYPE html>");
        return this;
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "[START HTML COMMENT]" appended.</p>
     *
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addStartHTMLComment () {
        sb.append("<!--");
        return this;
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "[END HTML COMMENT]" appended.</p>
     *
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addEndHTMLCommment () {
        sb.append("-->");
        return this;
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "[START HTML COMMENT][COMMENT][END HTML COMMENT]" appended.</p>
     *
     * @param comment The comment {@link String} to add.
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addCompleteHTMLCommment (String comment) {
        return this.addStartHTMLComment().addContent(comment).addEndHTMLCommment();
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "<" appended.</p>
     *
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addOpenStartTag () {
        sb.append("<");
        return this;
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with the tag appended.</p>
     *
     * @param tag The HTML tag {@link String} to add.
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addTag (String tag) {
        sb.append(tag);
        return this;
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with ">" appended.</p>
     *
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addCloseStartTag () {
        sb.append(">");
        return this;
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "/>" appended.</p>
     *
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addSingleElementCloseStartTag () {
        sb.append("/>");
        return this;
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with " [ATTRIBUTE]=[VALUE]" appended.</p>
     *
     * @param attr The attrubute {@link String} to append.
     * @param val The attrubute's value {@link String} to append.
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addAttrToTag (String attr, String val) {
        sb.append(" ");
        sb.append(attr);
        sb.append("=");
        sb.append("\"");
        sb.append(val);
        sb.append("\"");
        return this;
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "<[TAG]>" appended.</p>
     *
     * @param tag The HTML tag {@link String} to add.
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addCompleteStartTagNoAttrs (String tag) {
        return this.addOpenStartTag().addTag(tag).addCloseStartTag();
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "<[TAG]>[CONTENT]</[TAG]>" appended.</p>
     *
     * @param tag The HTML tag {@link String} to add.
     * @param content The content {@link String} to add.
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addCompleteTagNoAttrs (String tag, String content) {
        return this.addCompleteStartTagNoAttrs(tag).addContent(content).addCompleteEndTag(tag);
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "<[TAG] [ATTRIBUTE]=[VALUE] ...>" appended.</p>
     *
     * @param tag The HTML tag {@link String} to add.
     * @param attrsAndVals A {@link HashMap} containing the attribute {@link String} as a key and the attribute's value {@link String}.
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addCompleteStartTag (String tag, HashMap<String, String> attrsAndVals) {

        if (DataFunctions.isEmptyMap(attrsAndVals)) {
            return this.addCompleteStartTagNoAttrs(tag);
        }
        else {
            this.addOpenStartTag().addTag(tag);
            attrsAndVals.forEach(this::addAttrToTag);
            return this.addCloseStartTag();
        }

    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "<[TAG] [ATTRIBUTE]=[VALUE] ...>[CONTENT]</[TAG]>" appended.</p>
     *
     * @param tag The HTML tag {@link String} to add.
     * @param content The content {@link String} to add.
     * @param attrsAndVals A {@link HashMap} containing the attribute {@link String} as a key and the attribute's value {@link String}.
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addCompleteTag (String tag, String content, HashMap<String, String> attrsAndVals) {

        if (DataFunctions.isEmptyMap(attrsAndVals)) {
            return addCompleteTagNoAttrs(tag, content);
        }
        else {
            return this.addCompleteStartTag(tag, attrsAndVals).addContent(content).addCompleteEndTag(tag);
        }

    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "<[TAG]/>" appended.</p>
     *
     * @param tag The HTML tag {@link String} to add.
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addCompleteSingleElementStartTagNoAttrs (String tag) {
        return this.addOpenStartTag().addTag(tag).addSingleElementCloseStartTag();
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "<[TAG] [ATTRIBUTE]=[VALUE] .../>" appended.</p>
     *
     * @param tag The HTML tag {@link String} to add.
     * @param attrsAndVals A {@link HashMap} containing the attribute {@link String} as a key and the attribute's value {@link String}.
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addCompleteSingleElementStartTag (String tag, HashMap<String, String> attrsAndVals) {

        if (DataFunctions.isEmptyMap(attrsAndVals)) {
            return this.addCompleteStartTagNoAttrs(tag);
        }
        else {
            this.addOpenStartTag().addTag(tag);
            attrsAndVals.forEach(this::addAttrToTag);
            return this.addSingleElementCloseStartTag();
        }
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "</" appended.</p>
     *
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addCloseEndTag () {
        sb.append("</");
        return this;
    }

    /**
     * <p>Constructor for a {@link MMHHTMLBuilder} with "</[TAG]>" appended.</p>
     *
     * @param tag The HTML tag {@link String} to add.
     * @return This {@link MMHHTMLBuilder} instance.
     */
    public MMHHTMLBuilder addCompleteEndTag (String tag) {
        return this.addCloseEndTag().addTag(tag).addCloseStartTag();
    }
// </editor-fold>

// <editor-fold desc="HTML Tags">
    /**
     * <p>Contains all valid HTML 5 tags.</p>
     *
     */
    public static class HTMLTag {
        public static final String A = "a";
        public static final String ABBR = "abbr";
        public static final String ADDRESS = "address";
        public static final String AREA = "area";
        public static final String ARTICLE = "article";
        public static final String ASIDE = "aside";
        public static final String AUDIO = "audio";
        public static final String B = "b";
        public static final String BASE = "base";
        public static final String BDI = "bdi";
        public static final String BDO = "bdo";
        public static final String BLOCKQUOTE = "blockquote";
        public static final String BODY = "body";
        public static final String BR = "br";
        public static final String BUTTON = "button";
        public static final String CANVAS = "canvas";
        public static final String CAPTION = "caption";
        public static final String CITE = "cite";
        public static final String CODE = "code";
        public static final String COL = "col";
        public static final String COLGROUP = "colgroup";
        public static final String DATA = "data";
        public static final String DATALIST = "datalist";
        public static final String DD = "dd";
        public static final String DEL = "del";
        public static final String DETAILS = "details";
        public static final String DFN = "dfn";
        public static final String DIALOG = "dialog";
        public static final String DIV = "div";
        public static final String DL = "dl";
        public static final String DT = "dt";
        public static final String EM = "em";
        public static final String EMBED = "embed";
        public static final String FIELDSET = "fieldset";
        public static final String FIGCAPTION = "figcaption";
        public static final String FIGURE = "figure";
        public static final String FOOTER = "footer";
        public static final String FORM = "form";
        public static final String H1 = "h1";
        public static final String H2 = "h2";
        public static final String H3 = "h3";
        public static final String H4 = "h4";
        public static final String H5 = "h5";
        public static final String H6 = "h6";
        public static final String HEAD = "head";
        public static final String HEADER = "header";
        public static final String HR = "hr";
        public static final String HTML = "html";
        public static final String I = "i";
        public static final String IFRAME = "iframe";
        public static final String IMG = "img";
        public static final String INPUT = "input";
        public static final String INS = "ins";
        public static final String KBD = "kbd";
        public static final String LABEL = "label";
        public static final String LEGEND = "legend";
        public static final String LI = "li";
        public static final String LINK = "link";
        public static final String MAIN = "main";
        public static final String MAP = "map";
        public static final String MARK = "mark";
        public static final String META = "meta";
        public static final String METER = "meter";
        public static final String NAV = "nav";
        public static final String NOSCRIPT = "noscript";
        public static final String OBJECT = "object";
        public static final String OL = "ol";
        public static final String OPTGROUP = "optgroup";
        public static final String OPTION = "option";
        public static final String OUTPUT = "output";
        public static final String P = "p";
        public static final String PARAM = "param";
        public static final String PICTURE = "picture";
        public static final String PRE = "pre";
        public static final String PROGRESS = "progress";
        public static final String Q = "q";
        public static final String RP = "rp";
        public static final String RT = "rt";
        public static final String RUBY = "ruby";
        public static final String S = "s";
        public static final String SAMP = "samp";
        public static final String SCRIPT = "script";
        public static final String SECTION = "section";
        public static final String SELECT = "select";
        public static final String SMALL = "small";
        public static final String SOURCE = "source";
        public static final String SPAN = "span";
        public static final String STRONG = "strong";
        public static final String STYLE = "style";
        public static final String SUB = "sub";
        public static final String SUMMARY = "summary";
        public static final String SUP = "sup";
        public static final String SVG = "svg";
        public static final String TABLE = "table";
        public static final String TBODY = "tbody";
        public static final String TD = "td";
        public static final String TEMPLATE = "template";
        public static final String TEXTAREA = "textarea";
        public static final String TFOOT = "tfoot";
        public static final String TH = "th";
        public static final String THEAD = "thead";
        public static final String TIME = "time";
        public static final String TITLE = "title";
        public static final String TR = "tr";
        public static final String TRACK = "track";
        public static final String U = "u";
        public static final String UL = "ul";
        public static final String VAR = "var";
        public static final String VIDEO = "video";
        public static final String WBR = "wbr";
    }
// </editor-fold>

// <editor-fold desc="Getters and Setters">
    /**
     * <p>Getter for content within the sb field of the {@link MMHHTMLBuilder}.</p>
     *
     * @return The sb field of the {@link MMHHTMLBuilder}.
     */
    public String getHTML () {
        return sb.toString();
    }
// </editor-fold>

// <editor-fold desc="Pretty Print the HTML">
//    /**
//     * <p>Gets the pretty printed HTML.</p>
//     *
//     * @return The pretty printed HTML {@link String}.
//     */
//    @Override
//    public String toString () {
//
//        try {
//            Source html = new StreamSource(new StringReader(sb.toString()));
//            StringWriter writer = new StringWriter();
//            StreamResult result = new StreamResult(writer);
//            TransformerFactory factory = TransformerFactory.newInstance();
//            Transformer transformer = factory.newTransformer();
//            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
//            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
//            transformer.transform(html, result);
//            return result.getWriter().toString();
//        }
//        catch (Exception e) {
//            Logger.logException(e);
//            Logger.logMessage("A problem occurred in MMHHTMLBuilder.toString while trying to format the HTML.", Logger.LEVEL.ERROR);
//        }
//
//        return "";
//    }
// </editor-fold>

}