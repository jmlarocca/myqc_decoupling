package com.mmhayes.common.utils;

import java.lang.reflect.Field;
import java.util.LinkedHashSet;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Data manager for discount data manager
 */
public class DeleteOnExitHookCacheManager implements Runnable{

    private void cleanCache(){

        Class<?> c;
        Field files;
        LinkedHashSet filesValue;

        try {
            // Get Package Private class via reflection
            c = Class.forName("java.io.DeleteOnExitHook");
            // Get private static field
            files = c.getDeclaredField("files");
            // Make public via reflection voodoo
            files.setAccessible(true);
            // Read files
            filesValue = (LinkedHashSet) files.get(null);
            Logger.logMessage("DeleteOnExitHook file cache count before: "+filesValue.size(), Logger.LEVEL.TRACE);
            // Clear LinkedHashSet
            filesValue.clear();
            Logger.logMessage("DeleteOnExitHook file cache count after: "+filesValue.size(), Logger.LEVEL.TRACE);
        }
        catch (Exception ex) {
            Logger.logMessage("Error in DeleteOnExitHookCacheManager.cleanCache()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
    }

    @Override
    public void run() {
        cleanCache();
    }

}