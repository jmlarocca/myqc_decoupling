package com.mmhayes.common.utils;

/*
 * $Author: bdyoung $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: TaskScheduler class used to run the Scheduled Tasks
 */

import com.mmhayes.common.dataaccess.DataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The TaskScheduler class is used to maintain all of the Scheduled Tasks on the server.
 */
@SuppressWarnings("unchecked")
public class TaskScheduler {

    private ArrayList<HashMap> tasks = new ArrayList<>();
    private ArrayList<ScheduledExecutorService> services = new ArrayList<>();
    private HashMap<Object, String> nameMap = new HashMap<>();

    private ArrayList<MMHRunnable> runnables = new ArrayList<>();

    public TaskScheduler(DataManager dm) {
        try {
            this.tasks = dm.parameterizedExecuteQuery("data.scheduledTasks.getBackgroundTasks", new Object[]{}, true);
        } catch(Exception e) {
            Logger.logException(e);
            Logger.logMessage("Error retrieving Scheduled Tasks in TaskScheduler");
        }
    }

    private void createTask(MMHRunnable task) {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        HashMap details = tasks.get(task.getBackgroundTaskId() - 1);
        try {
            String logMessage = "Starting Background Task " + details.get("NAME") + " Initial Delay:Repeat Interval:Time Unit " + details.get("INITIALDELAY") + ":" + details.get("REPEATINTERVAL") + ":" + details.get("TIMEUNIT");
            Logger.logMessage(logMessage, Logger.LEVEL.TRACE);
            executor.scheduleWithFixedDelay(
                    task,
                    (Integer) details.get("INITIALDELAY"),
                    (Integer) details.get("REPEATINTERVAL"),
                    TimeUnit.valueOf( ((String) details.get("TIMEUNIT")).toUpperCase() )
            );
            nameMap.put(executor, details.get("NAME").toString());
            services.add(executor);
        } catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("Error creating " + details.get("NAME") + " executor...", Logger.LEVEL.ERROR);
        }
    }

    public void addRunnable(MMHRunnable runnable) {
        runnables.add(runnable);
    }

    public void initiateScheduledTasks() {
        try {
            this.runnables.forEach(task -> createTask(task));
        } catch (Exception e) {
            Logger.logMessage("Error Starting Background Tasks", Logger.LEVEL.ERROR);
        }
    }

    public void shutdownScheduledTasks(SimpleDateFormat sdf) {
        services.forEach(service -> {
            service.shutdown();
            while(!service.isTerminated()) {
                Logger.logMessage("Waiting for " + nameMap.getOrDefault(service, service.toString()) + " termination", Logger.LEVEL.IMPORTANT);
                try {
                    service.awaitTermination(1, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    Logger.logMessage("Error in MMHServletListener.contextDestroyed method.", Logger.LEVEL.IMPORTANT);
                }
            }

        });
        nameMap.clear();
    }

}
