package com.mmhayes.common.utils;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.clients.EmailBuilderClient;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.mmhayes.common.receiptGen.ReceiptGen;
import com.mmhayes.common.receiptGen.Renderer.IRenderer;
import com.mmhayes.common.receiptGen.TypeData;
import org.owasp.esapi.ESAPI;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

/*
 $Author: ecdyer $: Author of last commit
 $Date: 2017-12-19 17:02:09 -0500 (Tue, 19 Dec 2017) $: Date of last commit
 $Rev: 5887 $: Revision of last commit
 Notes: Class contains methods to support Configurable Email Process
*/
public class MMHEmailBuilder {

    // TAGS
    private static final String TAG_HEADERIMAGE =       "[[HeaderImage]]";
    private static final String TAG_HEADERIMAGESRC =    "[[HeaderImageSRC]]";
    private static final String TAG_CUSTOMIMAGE =       "[[Image:";
    private static final String TAG_CUSTOMIMAGESRC =    "[[ImageSrc:";
    private static final String TAG_MMH_LOGO =          "[[MMHLogo]]";
    private static final String TAG_QC_LOGO =           "[[QCLogo]]";
    private static final String TAG_QC_LOGO_SMALL =     "[[QCLogoSmall]]";
    private static final String TAG_MYQC_LOGO =         "[[MyQCLogo]]";
    private static final String TAG_COMPANYNAME =       "[[CompanyName]]";
    private static final String TAG_RECIPIENTNAME =     "[[RecipientName]]";
    private static final String TAG_MESSAGETEXT =       "[[MessageText]]";
    private static final String TAG_PROGRAM_NAME =      "[[ProgramName]]";
    private static final String TAG_BRANDING_LOGO =     "[[BrandingLogo]]";
    private static final String TAG_BRANDING_BGC1 =     "[[BrandingBackgroundColor1]]";
    private static final String TAG_BRANDING_BGC2 =     "[[BrandingBackgroundColor2]]";
    private static final String TAG_BRANDING_BGC3 =     "[[BrandingBackgroundColor3]]";
    private static final String TAG_BRANDING_FC1 =      "[[BrandingFontColor1]]";
    private static final String TAG_BRANDING_FC2 =      "[[BrandingFontColor2]]";
    private static final String TAG_BRANDING_FC3 =      "[[BrandingFontColor3]]";
    private static final String TAG_BRANDING_AC1 =      "[[BrandingAccentColor1]]";
    private static final String TAG_BRANDING_AC2 =      "[[BrandingAccentColor2]]";
    private static final String TAG_APPCODE =           "[[AppCode]]";
    private static final String TAG_MYQC_WEBLINK =      "[[MyQCWebLink]]";
    private static final String TAG_APPLE_LINK =        "[[AppleLink]]";
    private static final String TAG_ANDROID_LINK =      "[[AndroidLink]]";
    private static final String TAG_CLOUD_ACCOUNT =     "[[CloudAccount]]";
    private static final String TAG_ERR_MSG =           "[[ErrorMessage]]";
    private static final String TAG_EMAIL =             "[[Email]]";
    private static final String TAG_INSTANCE_NAME =     "[[InstanceName]]";
    private static final String TAG_INSTANCE_URL =      "[[InstanceURL]]";
    private static final String TAG_INSTANCE_TYPE =     "[[InstanceType]]";
    private static final String TAG_BUTTON_LINK =       "[[ButtonLink]]";
    private static final String TAG_BUTTON_TEXT =       "[[ButtonText]]";
    private static final String TAG_AMOUNT =            "[[Amount]]";
    private static final String TAG_STORE_NAME =        "[[StoreName]]";
    private static final String TAG_TOS =               "[[TOS]]";
    private static final String TAG_TOS_NAME =          "[[TOSName]]";
    private static final String TAG_ICON_DESKTOP =      "[[DesktopIcon]]";
    private static final String TAG_ICON_APPLE =        "[[AppleIcon]]";
    private static final String TAG_ICON_ANDROID =      "[[AndroidIcon]]";
    private static final String TAG_EMPLOYEE_NAME =     "[[EmployeeName]]";
    private static final String TAG_LOW_BALANCE_THRESHOLD = "[[LowBalanceThreshold]]";
    private static final String TAG_GLOBALBALANCE =     "[[GlobalBalance]]";
    private static final String TAG_CONTACT_NAME =      "[[ContactName]]";
    private static final String TAG_CONTACT_URL =       "[[ContactURL]]";
    private static final String TAG_CONTACT_PHONE =     "[[ContactPhone]]";
    private static final String TAG_ADDRESS =           "[[ContactAddress]]";
    private static final String TAG_PROCESSED_AMOUNT =  "[[ProcessedAmount]]";
    private static final String TAG_FEE_AMOUNT =        "[[FeeAmount]]";
    private static final String TAG_FEE_LABEL =         "[[FeeLabel]]";
    private static final String TAG_FEE_TEXT =          "[[FeeText]]";
    private static final String TAG_APP_NAME =          "[[AppName]]";
    private static final String TAG_TRANSACTION_TYPE =  "[[TransactionType]]";
    private static final String TAG_RECEIPT =           "[[Receipt]]";
    private static final String TAG_ORDER_READY_DETAILS = "[[OrderReadyDetails]]";
    private static final String TAG_VOUCHERNAME =       "[[VoucherName]]";
    private static final String TAG_VOUCHERCODEDESC =   "[[VoucherCodeDesc]]";
    private static final String TAG_VOUCHERCODEHEADER = "[[VoucherCodeHeader]]";
    private static final String TAG_VOUCHERCODEEXP =    "[[VoucherExpDate]]";
    private static final String TAG_VOUCHERAMOUNT =     "[[VoucherAmount]]";

    // Invite Image MERGE TAGS
    private static final String TAG_LOGO_BROWSE_MENUS =         "[[LogoBrowseMenus]]";
    private static final String TAG_LOGO_BROWSE_MENUS_SRC =     "[[LogoBrowseMenusSRC]]";
    private static final String TAG_LOGO_FREEZE_ACCOUNT =       "[[LogoFreezeAccount]]";
    private static final String TAG_LOGO_FREEZE_ACCOUNT_SRC =   "[[LogoFreezeAccountSRC]]";
    private static final String TAG_LOGO_FUND_ACCOUNT =         "[[LogoFundAccount]]";
    private static final String TAG_LOGO_FUND_ACCOUNT_SRC =     "[[LogoFundAccountSRC]]";
    private static final String TAG_LOGO_GIFT_BOX =             "[[LogoGiftBox]]";
    private static final String TAG_LOGO_GIFT_BOX_SRC =         "[[LogoGiftBoxSRC]]";
    private static final String TAG_LOGO_HEALTHY_EATING =       "[[LogoHealthyEating]]";
    private static final String TAG_LOGO_HEALTHY_EATING_SRC =   "[[LogoHealthyEatingSRC]]";
    private static final String TAG_LOGO_MANAGE_ACCOUNT =       "[[LogoManageAccount]]";
    private static final String TAG_LOGO_MANAGE_ACCOUNT_SRC =   "[[LogoManageAccountSRC]]";
    private static final String TAG_LOGO_ONLINE_ORDERING =      "[[LogoOnlineOrdering]]";
    private static final String TAG_LOGO_ONLINE_ORDERING_SRC =  "[[LogoOnlineOrderingSRC]]";
    private static final String TAG_LOGO_PAYROLL_DEDUCT =       "[[LogoPayrollDeduct]]";
    private static final String TAG_LOGO_PAYROLL_DEDUCT_SRC =   "[[LogoPayrollDeductSRC]]";
    private static final String TAG_LOGO_PAY_WITH_BADGE =       "[[LogoPayWithBadge]]";
    private static final String TAG_LOGO_PAY_WITH_BADGE_SRC =   "[[LogoPayWithBadgeSRC]]";
    private static final String TAG_LOGO_PERSONALIZE =          "[[LogoPersonalize]]";
    private static final String TAG_LOGO_PERSONALIZE_SRC =      "[[LogoPersonalizeSRC]]";
    private static final String TAG_LOGO_QR_CODE =              "[[LogoQRCode]]";
    private static final String TAG_LOGO_QR_CODE_SRC =          "[[LogoQRCodeSRC]]";
    private static final String TAG_LOGO_TRACK_CASHLESS =       "[[LogoTrackCashless]]";
    private static final String TAG_LOGO_TRACK_CASHLESS_SRC =   "[[LogoTrackCashlessSRC]]";
    private static final String TAG_LOGO_APP_ON_PHONE =         "[[LogoAppOnPhone]]";
    private static final String TAG_LOGO_APP_ON_PHONE_SRC =     "[[LogoAppOnPhoneSRC]]";
    private static final String TAG_LOGO_PHONE_GARDEN_CAFE =    "[[LogoPhoneGardenCafe]]";
    private static final String TAG_LOGO_PHONE_GARDEN_CAFE_SRC = "[[LogoPhoneGardenCafeSRC]]";

    // SQL FIELDS
    public static final String FIELD_SMTPUSER =             "SMTPUSER";
    public static final String FIELD_SMTPPASSWORD =         "SMTPPASSWORD";
    public static final String FIELD_SMTPPORT =             "SMTPPORT";
    public static final String FIELD_SMTPSERVER =           "SMTPSERVER";
    public static final String FIELD_SMTPREPLYTOADDRESS =   "SMTPREPLYTOADDRESS";
    public static final String FIELD_SMTPREPLYTONAME =      "SMTPREPLYTONAME";
    public static final String FIELD_SMTPFROMADDRESS =      "SMTPFROMADDRESS";
    public static final String FIELD_SMTPFROMNAME =         "SMTPFROMNAME";
    public static final String FIELD_HTML =                 "HTML";
    public static final String FIELD_EMPLOYEE_ID =          "EMPLOYEEID";
    private static final String FIELD_FILENAME =            "HEADERIMAGETOKENIZEDFILENAME";
    private static final String FIELD_COMPANYNAME =         "REGCOMPANY";
    private static final String FIELD_TOS =                 "HTMLCONTENT";
    private static final String FIELD_TOS_NAME =            "TITLE";
    private static final String FIELD_BRANDING_COLOR_BG1 =  "MYQCPRIMARYCOLOR";
    private static final String FIELD_BRANDING_COLOR_BG2 =  "MYQCSECONDARYCOLOR";
    private static final String FIELD_BRANDING_COLOR_BG3 =  "MYQCTERTIARYCOLOR";
    private static final String FIELD_BRANDING_COLOR_F1 =   "MYQCFONTCOLORONPRIMARY";
    private static final String FIELD_BRANDING_COLOR_F2 =   "MYQCFONTCOLORONSECONDARY";
    private static final String FIELD_BRANDING_COLOR_F3 =   "MYQCFONTCOLORONTERTIARY";
    private static final String FIELD_BRANDING_COLOR_A1 =   "MYQCACCENTCOLORONE";
    private static final String FIELD_BRANDING_COLOR_A2 =   "MYQCACCENTCOLORTWO";
    private static final String FIELD_BRANDING_IMAGE =      "MYQCTOKENIZEDPROGRAMLOGO";
    private static final String FIELD_BRANDING_NAME =       "MYQCPROGRAMNAME";
    private static final String FIELD_CONTACT_NAME =        "CONTACTNAME";
    private static final String FIELD_CONTACT_PHONE =       "CONTACTPHONE";
    private static final String FIELD_CONTACT_URL =         "CONTACTURL";
    private static final String FIELD_CONTACT_ADDRESS1 =    "CONTACTADDRESS1";
    private static final String FIELD_CONTACT_ADDRESS2 =    "CONTACTADDRESS2";
    private static final String FIELD_VOUCHERNAME =         "NAME";
    private static final String FIELD_VOUCHERCODEDESC =     "DESCRIPTION";
    private static final String FIELD_VOUCHERCODEHEADER =   "HEADERTEXT";
    private static final String FIELD_VOUCHERCODEEXP =      "EXPIRATIONDATE";
    private static final String FIELD_VOUCHERAMOUNT =       "AMOUNT";

    // QUERIES
    private static final String QUERY_COMPANYNAME =     "data.common.emailbuilder.getCompanyName";
    private static final String QUERY_TOS =             "data.common.emailbuilder.getTOS";
    private static final String QUERY_RECIPIENTNAME =   "data.common.emailbuilder.getRecipientName";
    private static final String QUERY_EMPLOYEENAME =    "data.common.emailbuilder.getEmployeeName";
    private static final String QUERY_USERNAME =        "data.common.emailbuilder.getUserName";
    private static final String QUERY_SYSTEMTEXT =      "data.common.emailbuilder.getSystemText";
    private static final String QUERY_CONTACT_INFO =    "data.common.emailbuilder.getContactInfo";
    private static final String QUERY_VOUCHERNAME =     "data.common.emailbuilder.getVoucherName";
    private static final String QUERY_VOUCHERCODEDESC = "data.common.emailbuilder.getVoucherCodeDescription";
    private static final String QUERY_VOUCHERCODEHEADER = "data.common.emailbuilder.getVoucherCodeHeader";
    private static final String QUERY_VOUCHERCODEEXP =  "data.common.emailbuilder.getVoucherCodeExpiration";
    private static final String QUERY_VOUCHERAMOUNT =   "data.common.emailbuilder.getVoucherAmount";
    public static final String QUERY_GLOBALSETTINGS =   "data.common.emailbuilder.getGlobals";
    public static final String QUERY_HEADERIMAGE =      "data.common.emailbuilder.getHeaderImage";
    public static final String QUERY_BRANDING_IMAGE =   "data.common.emailbuilder.getBrandingImage";
    public static final String QUERY_BRANDING_INFO =    "data.common.emailbuilder.getBranding";
    public static final String QUERY_EMAILTYPEID =      "data.common.emailbuilder.getEmailByEmailTypeID";
    public static final String QUERY_USEREMAIL =        "data.common.emailbuilder.getUserEmailByUserID";
    public static final String QUERY_COMMGROUPBYEMP =   "data.common.emailbuilder.getCommunicationGroupByEmpID";
    public static final String QUERY_COMMGROUPBYUSER =  "data.common.emailbuilder.getCommunicationGroupByUserID";
    public static final String QUERY_TEMPLATEID =       "data.common.emailbuilder.getTemplateID";
    public static final String QUERY_EMAILTEMPLATE =    "data.common.emailbuilder.getEmailTemplate";
    public static final String QUERY_EMP_LANGUAGE =     "data.common.emailbuilder.getEmployeeLanguage";
    public static final String QUERY_USER_LANGUAGE =    "data.common.emailbuilder.getUserLanguage";
    public static final String QUERY_EMPLOYEEID =       "data.common.emailbuilder.getEmpIDbyEmail";
    public static final String QUERY_USERID =           "data.common.emailbuilder.getUserIDbyEmail";
    public static final String QUERY_FUNDING_EMPID =    "data.common.emailbuilder.getFundingEmpID";

    // Image Paths
    private static final String IMAGE_MMH_LOGO =        "/images/email/mmhlogo.png";
    private static final String IMAGE_QC_LOGO =         "/images/email/qclogo.png";
    private static final String IMAGE_QC_LOGO_SMALL =   "/images/email/qclogo100.png";
    private static final String IMAGE_MYQC_LOGO =       "/images/email/myqclogo.png";
    private static final String IMAGE_ICON_DESKTOP =    "/images/email/browser.png";
    private static final String IMAGE_ICON_APPLE =      "/images/email/apple.png";
    private static final String IMAGE_ICON_ANDROID =    "/images/email/android.png";

    // Refactored Invite/Verification images
    private static final String IMAGE_INVITE_APPONPHONE =       "/images/email/invite/AppOnPhone.png";
    private static final String IMAGE_INVITE_BROWSEMENUS =      "/images/email/invite/BrowseMenus.png";
    private static final String IMAGE_INVITE_FREEZEACCOUNT =    "/images/email/invite/FreezeAccount.png";
    private static final String IMAGE_INVITE_FUNDACCOUNT =      "/images/email/invite/FundAccount.png";
    private static final String IMAGE_INVITE_GIFTBOX =          "/images/email/invite/GiftBox.png";
    private static final String IMAGE_INVITE_HEALTHYEATING =    "/images/email/invite/HealthyEating.png";
    private static final String IMAGE_INVITE_MANAGEACCOUNT =    "/images/email/invite/ManageAccount.png";
    private static final String IMAGE_INVITE_ONLINEORDERING =   "/images/email/invite/OnlineOrdering.png";
    private static final String IMAGE_INVITE_PAYROLLDEDUCT =    "/images/email/invite/PayrollDeduct.png";
    private static final String IMAGE_INVITE_PAYWITHBADGE =     "/images/email/invite/PayWithBadge.png";
    private static final String IMAGE_INVITE_PERSONALIZE =      "/images/email/invite/Personalize.png";
    private static final String IMAGE_INVITE_PHONEGARDENCAFE =  "/images/email/invite/PhoneGardenCafe.png";
    private static final String IMAGE_INVITE_QRCODE =           "/images/email/invite/QRCode.png";
    private static final String IMAGE_INVITE_TRACKCASHLESS =    "/images/email/invite/TrackCashless.png";


    // Data Manager
    private static final DataManager dm = new DataManager();

    // Internal data fields
    private HashMap parameterMap;
    private String subject;
    private String body;
    private String companyName = "";
    private String recipientName = "";

    private String brandingProgramName = "";
    private String brandingColorBG1 = "";
    private String brandingColorBG2 = "";
    private String brandingColorBG3 = "";
    private String brandingColorFont1 = "";
    private String brandingColorFont2 = "";
    private String brandingColorFont3 = "";
    private String brandingColorAccent1 = "";
    private String brandingColorAccent2 = "";

    // APPCODE Static var - rarely changes
    private static String appCode="";

    private ArrayList<MMHEmailLogo> logoList = new ArrayList<>();

    //Constructor
    public MMHEmailBuilder(){
        initLogoList();
    }

    private void initLogoList() {

        String w170 = "w170";
        String h80 = "80";
        String w80 = "80";
        String w90 = "90";

        logoList.add(new MMHEmailLogo(TAG_LOGO_APP_ON_PHONE, IMAGE_INVITE_APPONPHONE,"", w170, w90));
        logoList.add(new MMHEmailLogo(TAG_LOGO_APP_ON_PHONE_SRC, IMAGE_INVITE_APPONPHONE));
        logoList.add(new MMHEmailLogo(TAG_LOGO_BROWSE_MENUS, IMAGE_INVITE_BROWSEMENUS,"","", w80, h80));
        logoList.add(new MMHEmailLogo(TAG_LOGO_BROWSE_MENUS_SRC, IMAGE_INVITE_BROWSEMENUS));
        logoList.add(new MMHEmailLogo(TAG_LOGO_FREEZE_ACCOUNT, IMAGE_INVITE_FREEZEACCOUNT,"","", w80, h80));
        logoList.add(new MMHEmailLogo(TAG_LOGO_FREEZE_ACCOUNT_SRC, IMAGE_INVITE_FREEZEACCOUNT));
        logoList.add(new MMHEmailLogo(TAG_LOGO_FUND_ACCOUNT, IMAGE_INVITE_FUNDACCOUNT,"","", w80, h80));
        logoList.add(new MMHEmailLogo(TAG_LOGO_FUND_ACCOUNT_SRC, IMAGE_INVITE_FUNDACCOUNT));
        logoList.add(new MMHEmailLogo(TAG_LOGO_GIFT_BOX, IMAGE_INVITE_GIFTBOX,"","", w80, h80));
        logoList.add(new MMHEmailLogo(TAG_LOGO_GIFT_BOX_SRC, IMAGE_INVITE_GIFTBOX));
        logoList.add(new MMHEmailLogo(TAG_LOGO_HEALTHY_EATING, IMAGE_INVITE_HEALTHYEATING,"","", w80, h80));
        logoList.add(new MMHEmailLogo(TAG_LOGO_HEALTHY_EATING_SRC, IMAGE_INVITE_HEALTHYEATING));
        logoList.add(new MMHEmailLogo(TAG_LOGO_MANAGE_ACCOUNT, IMAGE_INVITE_MANAGEACCOUNT,"","", w80, h80));
        logoList.add(new MMHEmailLogo(TAG_LOGO_MANAGE_ACCOUNT_SRC, IMAGE_INVITE_MANAGEACCOUNT));
        logoList.add(new MMHEmailLogo(TAG_LOGO_ONLINE_ORDERING, IMAGE_INVITE_ONLINEORDERING,"","", w80, h80));
        logoList.add(new MMHEmailLogo(TAG_LOGO_ONLINE_ORDERING_SRC, IMAGE_INVITE_ONLINEORDERING));
        logoList.add(new MMHEmailLogo(TAG_LOGO_PAYROLL_DEDUCT, IMAGE_INVITE_PAYROLLDEDUCT,"","", w80, h80));
        logoList.add(new MMHEmailLogo(TAG_LOGO_PAYROLL_DEDUCT_SRC, IMAGE_INVITE_PAYROLLDEDUCT));
        logoList.add(new MMHEmailLogo(TAG_LOGO_PAY_WITH_BADGE, IMAGE_INVITE_PAYWITHBADGE,"","", w80, h80));
        logoList.add(new MMHEmailLogo(TAG_LOGO_PAY_WITH_BADGE_SRC, IMAGE_INVITE_PAYWITHBADGE));
        logoList.add(new MMHEmailLogo(TAG_LOGO_PERSONALIZE, IMAGE_INVITE_PERSONALIZE,"","", w80, h80));
        logoList.add(new MMHEmailLogo(TAG_LOGO_PERSONALIZE_SRC, IMAGE_INVITE_PERSONALIZE));
        logoList.add(new MMHEmailLogo(TAG_LOGO_PHONE_GARDEN_CAFE, IMAGE_INVITE_PHONEGARDENCAFE,"", w170, w90));
        logoList.add(new MMHEmailLogo(TAG_LOGO_PHONE_GARDEN_CAFE_SRC, IMAGE_INVITE_PHONEGARDENCAFE));
        logoList.add(new MMHEmailLogo(TAG_LOGO_QR_CODE, IMAGE_INVITE_QRCODE,"","", w80, h80));
        logoList.add(new MMHEmailLogo(TAG_LOGO_QR_CODE_SRC, IMAGE_INVITE_QRCODE));
        logoList.add(new MMHEmailLogo(TAG_LOGO_TRACK_CASHLESS, IMAGE_INVITE_TRACKCASHLESS,"","", w80, h80));
        logoList.add(new MMHEmailLogo(TAG_LOGO_TRACK_CASHLESS_SRC, IMAGE_INVITE_TRACKCASHLESS));
    }

    private String systemTextBuilder(int emailTypeID, int communicationGroupID, int languageID){

        // Init return string
        String text = "";

        // Get System Text Template
        ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(QUERY_SYSTEMTEXT, new Object[]{emailTypeID, communicationGroupID, languageID}, true);
        if(!arrayList.isEmpty()){
            if(arrayList.get(0).containsKey("TEXT")){
                text = arrayList.get(0).get("TEXT").toString();
                text = decode(text);
            }
        }

        return text;
    }

    private String dynamicTextBuilder(String text){

        // Called for subject where there is no [[MessageText]]
        return dynamicTextBuilder(text, "");

    }

    private String dynamicTextBuilder(String text, String messageText, int communicationGroupId) {
        text = dynamicTextBuilder(text, messageText);
        try {
            text = processContactInformation(text, communicationGroupId);
        } catch(Exception ex){
            Logger.logMessage("Error occured in method: dynamicTextBuilder()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return text;
    }

    private String dynamicTextBuilder(String text, String messageText){

        Logger.logMessage("Building dynamic text ...", Logger.LEVEL.DEBUG);

        try {
            HashMap params = getParameterMap();

            // Replace [[MessageText]]
            if(!messageText.isEmpty()){
                text = replaceText(text, TAG_MESSAGETEXT, messageText);
            }

            // Replace Header Image tag
            // [[HeaderImage]]
            text = processHeaderImage(text);

            // [[HeaderImageSRC]]
            text = processHeaderImageSRC(text);

            // Replace Custom Image tag
            // [[Image:file.jpg]]
            text = processCustomImage(text);

            // Replace Custom Image tag
            // [[ImageSrc:file.jpg]]
            text = processCustomImageSrc(text);

            // Replace [[MMHLogo]]
            text = processMMHLogo(text);

            // Replace [[QCLogo]]
            text = processQCLogo(text);

            // Replace [[QCLogoSmall]]
            text = processQCLogoSmall(text);

            // Replace [[MyQCLogo]]
            text = processMyQCLogo(text);

            // Replace [[BrandingLogo]]
            text = processBrandingLogo(text);

            // Replace [[DesktopIcon]]
            text = processDesktopIcon(text);

            // Replace [[AppleIcon]]
            text = processAppleIcon(text);

            // Replace [[AndroidIcon]]
            text = processAndroidIcon(text);

            // [[CompanyName]]
            text = processCompanyName(text);

            // [[RecipientName]]
            text = processRecipientName(text);

            // [[AndroidLink]]
            text = processAndroidLink(text);

            // [[AppleLink]]
            text = processAppleLink(text);

            // [[TOS]]
            text = processTOS(text);

            // [[MyQCWEbLink]]
            text = processMyQCWebLink(text);

            // [[CloudAccount]]
            text = processCloudAccount(text);

            // [[AppCode]]
            text = processAppCode(text);

            // All Global Branding fields
            text = processBranding(text);

            // [[Amount]]
            text = processAmount(text);

            // [[FeeText]]
            text = processFeeText(text);

            // Other replacement field in Parameter Map
            text = processParameter(text);

            // [[Receipt]]
            text = processReceipt(text);

            // Invite Logos
            text = processLogo(text);

            // Voucher replacements
            text = processParameterQuery(text, TAG_VOUCHERNAME, FIELD_VOUCHERNAME, "PAVOUCHERID", QUERY_VOUCHERNAME);
            text = processParameterQuery(text, TAG_VOUCHERCODEDESC, FIELD_VOUCHERCODEDESC, "PAVOUCHERCODEBATCHID", QUERY_VOUCHERCODEDESC);
            text = processParameterQuery(text, TAG_VOUCHERCODEHEADER, FIELD_VOUCHERCODEHEADER, "PAVOUCHERCODEBATCHID", QUERY_VOUCHERCODEHEADER);
            text = processParameterQuery(text, TAG_VOUCHERCODEEXP, FIELD_VOUCHERCODEEXP, "PAVOUCHERCODEID", QUERY_VOUCHERCODEEXP);
            text = processParameterQuery(text, TAG_VOUCHERAMOUNT, FIELD_VOUCHERAMOUNT, "PAVOUCHERID", QUERY_VOUCHERAMOUNT);

            // Voucher Vode HTML (not able to be queried, so this has to be processed on its own)
            if (params.containsKey("[[VoucherCodeHREF]]")) {
                text = replaceText(text, "[[VoucherCodeHREF]]", params.get("[[VoucherCodeHREF]]").toString());
            }

        } catch(Exception ex){
            Logger.logMessage("Error occured in method: dynamicTextBuilder()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return text;
    }

    private String processHeaderImageSRC(String text){

        Logger.logMessage("Processing Header Image SRC ...", Logger.LEVEL.DEBUG);

        try {
            if(text.contains(TAG_HEADERIMAGESRC) && parameterMap.containsKey("TEMPLATEID")){
                // Lookup Tokenized FileName
                ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(QUERY_HEADERIMAGE, new Object[]{parameterMap.get("TEMPLATEID")}, true);
                if(arrayList.isEmpty()){
                    return text; // No results so exit
                }

                if(!arrayList.get(0).containsKey(FIELD_FILENAME)){
                    return text; // Column Not found in ResultSet therefore exit
                }

                String headerFileName = arrayList.get(0).get(FIELD_FILENAME).toString();
                if(headerFileName.isEmpty()){
                    // No FileName therefore exit
                    Logger.logMessage("No Header Image found.", Logger.LEVEL.DEBUG);
                    return text;
                }

                // Get Hostname
                String hostName = getHostName();

                // Build image path
                String imagePath = hostName+"/webimages/"+headerFileName;

                // Build Image tag
                String imageTag = "src=\""+imagePath+"\"";

                // Replace all
                text = replaceText(text, TAG_HEADERIMAGESRC, imageTag);

            }
        } catch(Exception ex){
            Logger.logMessage("Exception occurred in method: processHeaderImageSRC()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return text;
    }

    private String processHeaderImage(String text){

        Logger.logMessage("Processing Header Images ...", Logger.LEVEL.DEBUG);

        try {
            if(text.contains(TAG_HEADERIMAGE) && parameterMap.containsKey("TEMPLATEID")){
                // Lookup Tokenized FileName
                ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(QUERY_HEADERIMAGE, new Object[]{parameterMap.get("TEMPLATEID")}, true);
                if(arrayList.isEmpty()){
                    return text; // No results so exit
                }

                if(!arrayList.get(0).containsKey(FIELD_FILENAME)){
                    return text; // Column Not found in ResultSet therefore exit
                }

                String headerFileName = arrayList.get(0).get(FIELD_FILENAME).toString();
                if(headerFileName.isEmpty()){
                    // No FileName therefore exit
                    Logger.logMessage("No Header Image found.", Logger.LEVEL.DEBUG);
                    return text;
                }

                // Get Hostname
                String hostName = getHostName();

                // Build image path
                String imagePath = hostName+"/webimages/"+headerFileName;

                // Build Image tag
                String imageTag = "<img src=\""+imagePath+"\" border=\"0\" style=\" outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; border:none; display:block;\" />";

                // Replace all
                text = replaceText(text, TAG_HEADERIMAGE, imageTag);

            }
        } catch(Exception ex){
            Logger.logMessage("Exception occurred in method: processHeaderImage()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return text;
    }

    private String processCustomImageSrc(String email){

        Logger.logMessage("Processing Custom ImageSrc ...", Logger.LEVEL.DEBUG);

        try {
            if(email.contains(TAG_CUSTOMIMAGESRC)){
                while(email.contains(TAG_CUSTOMIMAGESRC)){
                    // Parse text to get image name
                    int start = email.indexOf(TAG_CUSTOMIMAGESRC);
                    int end = email.indexOf("]]",start);
                    if(end == -1){
                        Logger.logMessage("Unable to find custom closing tag: ']]' in method processCustomImageSrc", Logger.LEVEL.DEBUG);
                        return email;
                    }
                    String imageFileNameTag = email.substring(start,end+2);
                    String imageFileName = imageFileNameTag.replace(TAG_CUSTOMIMAGESRC,"").replace("]]","").trim();
                    Logger.logMessage("Parsed custom image filename: "+imageFileName);

                    // validate custom image name, May only contain alphanumeric, "_" or "."
                    String validatedFileName = imageFileName.replaceAll("[^\\w.]",""); // Remove invalid chars
                    if(!validatedFileName.equals(imageFileName)){
                        // if validated string does not match imageFileName then it contains invalid character
                        Logger.logMessage("Invalid custom image filename found: "+imageFileName, Logger.LEVEL.ERROR);
                        return email;
                    }

                    String hostName = getHostName();

                    // build image path
                    String imagePath = hostName+"/webimages/"+imageFileName;

                    // build image tag
                    String imgageTag = "src=\""+imagePath+"\"";

                    // Replace
                    email = email.replace(imageFileNameTag, imgageTag);
                }

            }
        } catch(Exception ex){
            Logger.logMessage("Exception occured in method: processHeaderImage()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return email;
    }

    private String processCustomImage(String email){

        Logger.logMessage("Processing Custom Images ...", Logger.LEVEL.DEBUG);

        try {
            if(email.contains(TAG_CUSTOMIMAGE)){
                while(email.contains(TAG_CUSTOMIMAGE)){
                    // Parse text to get image name
                    int start = email.indexOf(TAG_CUSTOMIMAGE);
                    int end = email.indexOf("]]",start);
                    if(end == -1){
                        Logger.logMessage("Unable to find custom closing tag: ']]' in method processCustomImage", Logger.LEVEL.DEBUG);
                        return email;
                    }
                    String imageFileNameTag = email.substring(start,end+2);
                    String imageFileName = imageFileNameTag.replace(TAG_CUSTOMIMAGE,"").replace("]]","").trim();
                    Logger.logMessage("Parsed custom image filename: "+imageFileName);

                    // validate custom image name, May only contain alphanumeric, "_" or "."
                    String validatedFileName = imageFileName.replaceAll("[^\\w.]",""); // Remove invalid chars
                    if(!validatedFileName.equals(imageFileName)){
                        // if validated string does not match imageFileName then it contains invalid character
                        Logger.logMessage("Invalid custom image filename found: "+imageFileName, Logger.LEVEL.ERROR);
                        return email;
                    }

                    String hostName = getHostName();

                    // build image path
                    String imagePath = hostName+"/webimages/"+imageFileName;

                    // build image tag
                    String imgageTag = "<img src=\""+imagePath+"\" border=\"0\" style=\" outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; border:none; display:block;\" />";

                    // Replace
                    email = email.replace(imageFileNameTag, imgageTag);
                }

            }
        } catch(Exception ex){
            Logger.logMessage("Exception occured in method: processHeaderImage()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return email;
    }

    private String processMMHLogo(String email){

        Logger.logMessage("Processing MMHLogo ...", Logger.LEVEL.DEBUG);

        try {
            if(email.contains(TAG_MMH_LOGO)){

                // Get Instance Name
                String instance = "/"+commonMMHFunctions.getInstanceName();

                // Build image path
                String imagePath = getHostName() + instance + IMAGE_MMH_LOGO;

                // Build Image tag
                String imageTag = "<img alt='MM Hayes Co., Inc.' src='"+imagePath+"' border='0' style='outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;border:none;display:block;'/>";

                // Replace all
                email = replaceText(email, TAG_MMH_LOGO, imageTag);

            }
        } catch(Exception ex){
            Logger.logMessage("Exception occurred in method: processMMHLogo()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return email;
    }

    private String processQCLogo(String email){

        Logger.logMessage("Processing QCLogo ...", Logger.LEVEL.DEBUG);

        try {
            if(email.contains(TAG_QC_LOGO)){

                // Get Instance Name
                String instance = "/"+commonMMHFunctions.getInstanceName();

                // Build image path
                String imagePath = getHostName() + instance + IMAGE_QC_LOGO;

                // Build Image tag
                String imageTag = "<img alt='Quickcharge' src=\""+imagePath+"\" border=\"0\" style=\"outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;border:none;display:block;\"/>";

                // Replace all
                email = replaceText(email, TAG_QC_LOGO, imageTag);

            }
        } catch(Exception ex){
            Logger.logMessage("Exception occurred in method: processQCLogo()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return email;
    }

    private String processQCLogoSmall(String email){

        Logger.logMessage("Processing QCLogoSmall ...", Logger.LEVEL.DEBUG);

        try {
            if(email.contains(TAG_QC_LOGO_SMALL)){

                // Get Instance Name
                String instance = "/"+commonMMHFunctions.getInstanceName();

                // Build image path
                String imagePath = getHostName() + instance + IMAGE_QC_LOGO_SMALL;

                // Build Image tag
                String imageTag = "<img alt='Quickcharge' src=\""+imagePath+"\" border=\"0\" style=\"outline:none;text-decoration:none;border:none;display:block;\"/>";

                // Replace all
                email = replaceText(email, TAG_QC_LOGO_SMALL, imageTag);

            }
        } catch(Exception ex){
            Logger.logMessage("Exception occurred in method: processQCLogoSmall()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return email;
    }

    private String processLogo(String email){

        Logger.logMessage("Processing Logo ...", Logger.LEVEL.DEBUG);

        for (int x=0; x <logoList.size(); x++ ){
            email = processInviteLogo(logoList.get(x), email);
        }

        return email;
    }

    private String processInviteLogo(MMHEmailLogo logo, String email){

        Logger.logMessage("Processing "+logo.getMergeField(), Logger.LEVEL.DEBUG);

        try {
            if(email.contains(logo.getMergeField())){

                // Get Instance Name
                String instance = "/"+commonMMHFunctions.getInstanceName();

                // Build image path
                String imagePath = getHostName() + instance + logo.getImage();

                // Build className if it exists
                String className = "";
                if(!logo.getClassName().isEmpty()){
                    className = " class=\""+logo.getClassName()+"\" ";
                }

                // Build className if it exists
                String style = " style=\"outline:none;text-decoration:none;border:none;display:block;\" ";
                if(!logo.getStyle().isEmpty()){
                    style = " style=\"outline:none;text-decoration:none;border:none;display:block;"+logo.getStyle()+"\" ";
                }

                // Add width
                String width="";
                if(!logo.getWidth().isEmpty()){
                    width = " width=\""+logo.getWidth()+"\" ";
                }

                // Add height
                String height="";
                if(!logo.getHeight().isEmpty()){
                    height = " height=\""+logo.getHeight()+"\" ";
                }

                // Build Image tag
                String imageTag;
                if(!logo.getMergeField().endsWith("SRC]]")){
                    imageTag = "<img src=\""+imagePath+"\" "+className+style+width+height+"/>";
                } else {
                    // build image tag
                    imageTag = "src=\""+imagePath+"\"";
                }

                // Replace all
                email = replaceText(email, logo.getMergeField(), imageTag);

            }
        } catch(Exception ex){
            Logger.logMessage("Exception occurred in method: processInviteLogo()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return email;

    }

    private String processMyQCLogo(String email){

        Logger.logMessage("Processing MyQCLogo ...", Logger.LEVEL.DEBUG);

        try {
            if(email.contains(TAG_MYQC_LOGO)){

                // Get Instance Name
                String instance = "/"+commonMMHFunctions.getInstanceName();

                // Build image path
                String imagePath = getHostName() + instance + IMAGE_MYQC_LOGO;

                // Build Image tag
                String imageTag = "<img alt='My Quickcharge' src='"+imagePath+"' border='0' style='outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;border:none;display:block;'/>";

                // Replace all
                email = replaceText(email, TAG_MYQC_LOGO, imageTag);

            }
        } catch(Exception ex){
            Logger.logMessage("Exception occurred in method: processQCLogo()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return email;
    }

    private String processBrandingLogo(String email){

        Logger.logMessage("Processing BrandingLogo ...", Logger.LEVEL.DEBUG);

        try {
            if(email.contains(TAG_BRANDING_LOGO)){

                // Lookup Tokenized FileName
                ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(QUERY_BRANDING_IMAGE, new Object[]{}, true);
                if(arrayList.isEmpty()){
                    return email; // No results so exit
                }

                if(!arrayList.get(0).containsKey(FIELD_BRANDING_IMAGE)){
                    return email; // Column Not found in ResultSet therefore exit
                }

                String fileName = arrayList.get(0).get(FIELD_BRANDING_IMAGE).toString();
                if(fileName.isEmpty()){
                    // No FileName therefore exit
                    Logger.logMessage("No Branding Image found.", Logger.LEVEL.DEBUG);
                    return email;
                }

                // Get Hostname
                String hostName = getHostName();

                // Image FilePath
                String imageFilePath = "/webimages/"+fileName;

                // Build image path
                String imagePath = hostName + imageFilePath;

                // Build Image tag
                String imageTag = "<img src='"+imagePath+"' border='0' style='outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;border:none;display:block;'/>";

                // Replace all
                email = replaceText(email, TAG_BRANDING_LOGO, imageTag);

            }
        } catch(Exception ex){
            Logger.logMessage("Exception occurred in method: processQCLogo()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return email;
    }

    private String processRecipientName(String text) {

        Logger.logMessage("Processing Recipient Name ...", Logger.LEVEL.DEBUG);

        if(text.contains(TAG_RECIPIENTNAME)){

            // Initialize default value
            String name = "Friend";

            // check for cached value
            if(this.getRecipientName().isEmpty()){
                if(getParameterMap().containsKey("RecipientName")){
                    name = getParameterMap().get("RecipientName").toString();
                    // cache name for later use
                    if(name.trim().isEmpty()){name = "Friend";}
                    this.setRecipientName(name);
                }
                if(getParameterMap().containsKey("EmployeeID")){
                    // Lookup Name by EmployeeID
                    int employeeID = Integer.parseInt(getParameterMap().get("EmployeeID").toString());
                    // Lookup from QC_EMPLOYEES
                    ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(QUERY_EMPLOYEENAME, new Object[]{employeeID},true);
                    if(!arrayList.isEmpty()){
                        if(arrayList.get(0).containsKey("NAME")){
                            name = arrayList.get(0).get("NAME").toString();
                            // cache name for later use
                            if(name.trim().isEmpty()){name = "Friend";}
                            this.setRecipientName(name);
                        }
                    }
                } else if(getParameterMap().containsKey("UserID")){
                    // Lookup Name by UserID
                    int userID = Integer.parseInt(getParameterMap().get("UserID").toString());

                    // Lookup from QC_QUICKCHARGEUSERS
                    ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(QUERY_USERNAME, new Object[]{userID},true);
                    if(!arrayList.isEmpty()){
                        if(arrayList.get(0).containsKey("NAME")){
                            name = arrayList.get(0).get("NAME").toString();
                            // cache name for later use
                            if(name.trim().isEmpty()){name = "Friend";}
                            this.setRecipientName(name);
                        }
                    }
                } else if(getParameterMap().containsKey("EMAILADDRESS1")){
                    String emailAddress = getParameterMap().get("EMAILADDRESS1").toString();
                    if(!emailAddress.isEmpty()){
                        // Lookup from QC_EMPLOYEES
                        ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(QUERY_RECIPIENTNAME, new Object[]{emailAddress},true);
                        if(!arrayList.isEmpty()){
                            if(arrayList.get(0).containsKey("NAME")){
                                name = arrayList.get(0).get("NAME").toString();
                                // cache name for later use
                                if(name.trim().isEmpty()){name = "Friend";}
                                this.setRecipientName(name);
                            }
                        }
                    }
                }
            } else {
                // Use cached value
                name = getRecipientName();
            }
            // Replace
            text = replaceText(text, TAG_RECIPIENTNAME, name);
        }
        return text;
    }

    private String processAndroidLink(String text){

        Logger.logMessage("Processing AndroidLink ...", Logger.LEVEL.DEBUG);

        // HARD CODED FOR NOW
        if(text.contains(TAG_ANDROID_LINK)){

            // Build link
            //String androidLink = "<a href='https://play.google.com/store/apps/details?id=com.mmhayes.myqc.alpha' target='_blank' >My Quickcharge - Google Play Store</a>";
            String androidLink = "https://play.google.com/store/apps/details?id=com.mmhayes.myqc.alpha";
            // replace tag with link
            text = replaceText(text, TAG_ANDROID_LINK, androidLink);
        }

        return text;
    }

    private String processAppleLink(String text){

        Logger.logMessage("Processing AppleLink ...", Logger.LEVEL.DEBUG);

        // HARD CODED FOR NOW
        if(text.contains(TAG_APPLE_LINK)){
            // Build link html
            //String appleLink = "<a href='https://itunes.apple.com/us/app/my-quickcharge/id1059849685?mt=8' target='_blank' >My Quickcharge - iTunes App Store</a>";
            String appleLink = "https://itunes.apple.com/us/app/my-quickcharge/id1059849685";
            // replace tag with link
            text = replaceText(text, TAG_APPLE_LINK, appleLink);
        }

        return text;
    }

    private String processDesktopIcon(String text){

        Logger.logMessage("Processing DesktopIcon ...", Logger.LEVEL.DEBUG);

        // HARD CODED FOR NOW
        if(text.contains(TAG_ICON_DESKTOP)){

            // Get Instance Name
            String instance = "/"+commonMMHFunctions.getInstanceName();

            // Build image path
            String imagePath = getHostName() + instance + IMAGE_ICON_DESKTOP;

            // Build link html
            String image = "<img src='"+imagePath+"' alt='Computer Icon'>";
            // replace tag with link
            text = replaceText(text, TAG_ICON_DESKTOP, image);
        }

        return text;
    }

    private String processAppleIcon(String text){

        Logger.logMessage("Processing AppleIcon ...", Logger.LEVEL.DEBUG);

        // HARD CODED FOR NOW
        if(text.contains(TAG_ICON_APPLE)){

            // Get Instance Name
            String instance = "/"+commonMMHFunctions.getInstanceName();

            // Build image path
            String imagePath = getHostName() + instance + IMAGE_ICON_APPLE;

            // Build link html
            String image = "<img src='"+imagePath+"' alt='Apple Logo'>";
            // replace tag with link
            text = replaceText(text, TAG_ICON_APPLE, image);
        }

        return text;
    }

    private String processContactInformation(String text, int communicationGroupId) {
        Logger.logMessage("Pocessing Contact Information ...", Logger.LEVEL.DEBUG);

        ArrayList<HashMap> contactInfo;
        contactInfo = dm.parameterizedExecuteQuery(QUERY_CONTACT_INFO, new Object[]{communicationGroupId}, true);
        if (!contactInfo.isEmpty()) {
            String contactName = contactInfo.get(0).get(FIELD_CONTACT_NAME).toString();
            String contactUrl = contactInfo.get(0).get(FIELD_CONTACT_URL).toString();
            String contactPhone = contactInfo.get(0).get(FIELD_CONTACT_PHONE).toString();
            String contactAddress1 = contactInfo.get(0).get(FIELD_CONTACT_ADDRESS1).toString();
            String contactAddress2 = contactInfo.get(0).get(FIELD_CONTACT_ADDRESS2).toString();
            if (text.contains(TAG_CONTACT_NAME)) {
                if (contactName != null && !contactName.equals("")) {
                    text = text.replace(TAG_CONTACT_NAME, contactName);
                } else {
                    String companyName = getCompanyName();
                    if (companyName != null) {
                        text = text.replace(TAG_CONTACT_NAME, companyName);
                    } else {
                        text = text.replace(TAG_CONTACT_NAME, TAG_COMPANYNAME);
                        text = processCompanyName(text);
                    }
                }
            }
            if (text.contains(TAG_CONTACT_URL)) {
                if (contactUrl != null) {
                    text = text.replace(TAG_CONTACT_URL, contactUrl);
                } else {
                    text = text.replace(TAG_CONTACT_URL, "");
                }
            }
            if (text.contains(TAG_CONTACT_PHONE)) {
                if (contactPhone != null) {
                    text = text.replace(TAG_CONTACT_PHONE, contactPhone);
                } else {
                    text = text.replace(TAG_CONTACT_PHONE, "");
                }
            }
            if (text.contains(TAG_ADDRESS)) {
                if (contactAddress1 != null) {
                    String address = contactAddress2 != null ? contactAddress1 + "\n" + contactAddress2 : contactAddress1;
                    text = text.replace(TAG_ADDRESS, address);
                } else {
                    text = text.replace(TAG_ADDRESS, "");
                }
            }
        } else {
            // If the database doesn't return a commmunication group
            text = text.replace(TAG_CONTACT_NAME, TAG_COMPANYNAME);
            text = processCompanyName(text);
            text = text.replace(TAG_CONTACT_URL, "");
            text = text.replace(TAG_CONTACT_PHONE, "");
            text = text.replace(TAG_ADDRESS, "");
        }

        return text;
    }

    private String processAndroidIcon(String text){

        Logger.logMessage("Processing AndroidIcon ...", Logger.LEVEL.DEBUG);

        // HARD CODED FOR NOW
        if(text.contains(TAG_ICON_ANDROID)){

            // Get Instance Name
            String instance = "/"+commonMMHFunctions.getInstanceName();

            // Build image path
            String imagePath = getHostName() + instance + IMAGE_ICON_ANDROID;

            // Build link html
            String image = "<img src='"+imagePath+"' alt='Android Logo'>";
            // replace tag with link
            text = replaceText(text, TAG_ICON_ANDROID, image);
        }

        return text;
    }

    private String processMyQCWebLink(String text){

        Logger.logMessage("Processing MyQCWebLink ...", Logger.LEVEL.DEBUG);

        // HARD CODED FOR NOW
        if(text.contains(TAG_MYQC_WEBLINK)){
            // Build link html
            String link = getHostName()+"/myqc";
            // replace tag with link
            text = replaceText(text, TAG_MYQC_WEBLINK, link);
        }

        return text;
    }

    private String processParameter(String text) {

        text = processParameter(text, TAG_ERR_MSG, "ErrorMessage");
        text = processParameter(text, TAG_INSTANCE_NAME, "InstanceName");
        text = processParameter(text, TAG_INSTANCE_URL, "InstanceURL");
        text = processParameter(text, TAG_INSTANCE_TYPE, "InstanceType");
        text = processParameter(text, TAG_BUTTON_LINK, "ButtonLink");
        text = processParameter(text, TAG_BUTTON_TEXT, "ButtonText");
        text = processParameter(text, TAG_AMOUNT, "Amount");
        text = processParameter(text, TAG_PROCESSED_AMOUNT, "ProcessedAmount");
        text = processParameter(text, TAG_FEE_AMOUNT, "FeeAmount");
        text = processParameter(text, TAG_FEE_LABEL, "FeeLabel");
        text = processParameter(text, TAG_STORE_NAME, "StoreName");
        text = processParameter(text, TAG_EMPLOYEE_NAME, "EmployeeName");
        text = processParameter(text, TAG_GLOBALBALANCE, "GlobalBalance");
        text = processParameter(text, TAG_LOW_BALANCE_THRESHOLD, "LowBalanceThreshold");
        text = processParameter(text, TAG_EMAIL, "Email");
        text = processParameter(text, TAG_APP_NAME, "AppName");
        text = processParameter(text, TAG_TRANSACTION_TYPE, "TransactionType");
        text = processParameter(text, TAG_ORDER_READY_DETAILS, "OrderReadyDetails");

        return text;
    }

    private String processParameter(String text, String tag, String key) {

        // Utility method to processes TAGS passed in via parameterMap
        Logger.logMessage("Processing "+tag+" ...", Logger.LEVEL.DEBUG);

        if(text.contains(tag)){

            // Initialize default value - use key so no blank
            String value = (key.equalsIgnoreCase("ErrorMessage") ? "" : key);

            // Lookup key / value in parameterMap
            if(getParameterMap().containsKey(key)){
                value = parameterMap.get(key).toString();
            }

            // Replace tag with value
            text = replaceText(text, tag, value);
        }
        return text;
    }

    private String processCompanyName(String text) {

        Logger.logMessage("Processing Company Name ...", Logger.LEVEL.DEBUG);

        if(text.contains(TAG_COMPANYNAME)){

            // check for cached value
            if(this.getCompanyName().isEmpty()){
                // Lookup CompanyName fom globals
                ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(QUERY_COMPANYNAME, new Object[]{}, true);
                if(!arrayList.isEmpty()){
                    if(arrayList.get(0).containsKey(FIELD_COMPANYNAME)){
                        companyName = arrayList.get(0).get(FIELD_COMPANYNAME).toString();
                        // cache company name for later use
                        this.setCompanyName(companyName);
                    }
                }
            }

            // Replace Company Name
            text = replaceText(text, TAG_COMPANYNAME, getCompanyName());
        }
        return text;
    }

    private String processTOS(String text) {

        Logger.logMessage("Processing TOS ...", Logger.LEVEL.DEBUG);

        if(text.contains(TAG_TOS) || text.contains(TAG_TOS_NAME)){

            // Initialize default value
            String tos = "Terms of Service";
            String tos_name = "Terms of Service Name";

            if(getParameterMap().containsKey("TOS_ID")){
                int tosID = Integer.parseInt(parameterMap.get("TOS_ID").toString());

                // Lookup CompanyName fom globals
                ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(QUERY_TOS, new Object[]{tosID}, true);
                if(!arrayList.isEmpty()){
                    if(arrayList.get(0).containsKey(FIELD_TOS)){
                        tos = arrayList.get(0).get(FIELD_TOS).toString();
                        // custom parse and replacement code
                        text = parseTOS(text,tos);
                    }
                    if(arrayList.get(0).containsKey(FIELD_TOS_NAME)){
                        tos_name = arrayList.get(0).get(FIELD_TOS_NAME).toString();
                    }
                }
            }

            // Replace TOS
            text = replaceText(text, TAG_TOS, tos);
            // Replace TOS Name
            text = replaceText(text, TAG_TOS_NAME, tos_name);
        }

        return text;
    }

    private String processAppCode(String text){

        Logger.logMessage("Processing AppCode ...", Logger.LEVEL.DEBUG);

        if(text.contains(TAG_APPCODE)){

            // Initialize default value
            String appCode = getAppCode().isEmpty() ? "No App Code found." : getAppCode();

            // Replace AppCode
            text = replaceText(text, TAG_APPCODE, appCode);
        }
        return text;
    }

    private String parseTOS(String text, String tos){

        String tosStyleTag = "";
        String tosBodyTag = "";
        String tmpTOS = tos.toLowerCase();

        // parse tos style tag
        int posStartStyle = tmpTOS.indexOf("<style");
        int posEndStyle = tmpTOS.indexOf("</style");
        if(posStartStyle > -1 && posEndStyle > -1){
            posStartStyle = tmpTOS.indexOf(">", posStartStyle)+1;
            if(posStartStyle>-1){
                tosStyleTag = tos.substring(posStartStyle, (posEndStyle-1));
            }
        }
        // if Style Tag is found then insert before </head>
        if(!tosStyleTag.isEmpty()){
            int posInsertionPoint = text.toLowerCase().indexOf("</head");
            if(posInsertionPoint > -1){
                text = text.substring(0,posInsertionPoint-1) +
                        tosStyleTag +
                        text.substring(posInsertionPoint);
            }
        }

        // parse tos body
        int posStartBody = tmpTOS.indexOf("<body");
        int posEndBody = tmpTOS.indexOf("</body");
        if(posStartBody > -1 && posEndBody > -1){
            posStartBody = tmpTOS.indexOf(">", posStartBody)+1;
            if(posStartBody>-1){
                tosBodyTag = tos.substring(posStartBody, (posEndBody-1));
            }
        }
        text = replaceText(text, TAG_TOS, tosBodyTag);

        return text;
    }

    private String processCloudAccount(String text){

        Logger.logMessage("Processing CloudAccount ...", Logger.LEVEL.DEBUG);
        try{
            if(text.contains(TAG_CLOUD_ACCOUNT)){

                String cloudAccount = "No Cloud Account found.";

                // Does the parameter map contain an ID
                if(parameterMap.containsKey("EmployeeID") ||
                        parameterMap.containsKey("UserID") ||
                        parameterMap.containsKey("PersonID")){

                    // Get ID
                    String userID;
                    Integer instanceUserTypeID;
                    if(parameterMap.containsKey("PersonID")){
                        userID = parameterMap.get("PersonID").toString();
                        instanceUserTypeID = 3;
                    } else if(parameterMap.containsKey("EmployeeID")){
                        userID = "-"+parameterMap.get("EmployeeID").toString();
                        instanceUserTypeID = 1;
                    } else {
                        userID = parameterMap.get("UserID").toString();
                        instanceUserTypeID = 2;
                    }

                    //set parameters for rest client
                    int gatewayID = CommonAPI.getGatewayID();
                    if(gatewayID == 0){
                        // No gateway configured so not using MMHCloud
                        return text;
                    }
                    String validationTarget = dm.getSingleField("data.validation.getGatewayLocation", new Object[]{gatewayID}).toString();
                    String PARAM_TARGET = validationTarget;
                    String PARAM_PATH = "/account/name";

                    //creates a file transfer client
                    EmailBuilderClient emailBuilderClient= new EmailBuilderClient(PARAM_TARGET, PARAM_PATH);
                    //sends a request to the rest client asking for the cloud account name
                    cloudAccount = emailBuilderClient.getCloudAccount(Integer.parseInt(userID),instanceUserTypeID);
                }

                // Replace AppCode
                text = replaceText(text, TAG_CLOUD_ACCOUNT, cloudAccount);
            }
        } catch(Exception ex){
            Logger.logMessage("Error in method: processCloudAccount()", Logger.LEVEL.ERROR);
            //Logger.logException(ex);
        }

        return text;
    }

    public void createSubject(int subjectID, int communicationGroupID, int languageID) {

        // Retrieve Subject text
        String newSubject = this.systemTextBuilder(subjectID, communicationGroupID, languageID);

        // Insert Subject Text into Template
        newSubject = this.dynamicTextBuilder(newSubject);

        newSubject = ESAPI.encoder().decodeForHTML(newSubject);

        // Store Locally
        setSubject(newSubject);
    }

    public void createBody(String templateText, int messageID, int communicationGroupID, int languageID){

        // Build Body
        String messageText = systemTextBuilder(messageID, communicationGroupID, languageID);

        // Insert Message Text into Template
        String bodyText = dynamicTextBuilder(templateText, messageText, communicationGroupID);

        // Store locally
        setBody(bodyText);
    }

    private String replaceText(String text, String placeHolder, String textValue){
        // utility method for string replacements

        // counter to prevent infinite looping
        int count = 0;

        // loop through
        while(text.contains(placeHolder)){
            text = text.replace(placeHolder, textValue);
            count++;
            if(count>100){break;} // stop infinite loops
        }

        return text;
    }

    public static String decode(String text){
        text = ESAPI.encoder().decodeForHTML(text);
        text = text.replaceAll("_lt_", "<");
        text = text.replaceAll("_gt_", ">");
        return text;
    }

    private String getHostName(){

        String hostName="";

        try {
            // Get Hostname from Common Method
            hostName = CommonAPI.getHostName();

            // remove internal from mmhcloud shenanigans
            if(hostName.contains("internal.mmhcloud.com")){
                hostName = hostName.replace("internal.mmhcloud.com", "mmhcloud.com");
            }

            Logger.logMessage("Computed HostName: "+hostName, Logger.LEVEL.DEBUG);

        } catch(Exception ex){
            Logger.logMessage("Error in method: getHostName()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }
        return hostName;
    }

    private String processBranding(String text){

        if(text.contains(TAG_BRANDING_BGC1)){
            text = replaceText(text, TAG_BRANDING_BGC1, getBrandingColorBG1());
        }

        if(text.contains(TAG_BRANDING_BGC2)){
            text = replaceText(text, TAG_BRANDING_BGC2, getBrandingColorBG2());
        }

        if(text.contains(TAG_BRANDING_BGC3)){
            text = replaceText(text, TAG_BRANDING_BGC3, getBrandingColorBG3());
        }

        if(text.contains(TAG_BRANDING_FC1)){
            text = replaceText(text, TAG_BRANDING_FC1, getBrandingColorFont1());
        }

        if(text.contains(TAG_BRANDING_FC2)){
            text = replaceText(text, TAG_BRANDING_FC2, getBrandingColorFont2());
        }

        if(text.contains(TAG_BRANDING_FC3)){
            text = replaceText(text, TAG_BRANDING_FC3, getBrandingColorFont3());
        }

        if(text.contains(TAG_BRANDING_AC1)){
            text = replaceText(text, TAG_BRANDING_AC1, getBrandingColorAccent1());
        }

        if(text.contains(TAG_BRANDING_AC2)){
            text = replaceText(text, TAG_BRANDING_AC2, getBrandingColorAccent2());
        }

        if(text.contains(TAG_PROGRAM_NAME)){
            text = replaceText(text, TAG_PROGRAM_NAME, getBrandingProgramName());
        }

        return text;
    }

    private String getBrandingColorBG1() {
        if(brandingColorBG1.equals("")){
            loadBranding();
        }
        return brandingColorBG1;
    }

    private void setBrandingColorBG1(String brandingColorBG1) {
        this.brandingColorBG1 = brandingColorBG1;
    }

    private String getBrandingColorBG2() {
        if(brandingColorBG2.equals("")){
            loadBranding();
        }
        return brandingColorBG2;
    }

    private void setBrandingColorBG2(String brandingColorBG2) {
        this.brandingColorBG2 = brandingColorBG2;
    }

    private String getBrandingColorBG3() {
        if(brandingColorBG3.equals("")){
            loadBranding();
        }
        return brandingColorBG3;
    }

    private void setBrandingColorBG3(String brandingColorBG3) {
        this.brandingColorBG3 = brandingColorBG3;
    }

    private String getBrandingColorFont1() {
        if(brandingColorFont1.equals("")){
            loadBranding();
        }
        return brandingColorFont1;
    }

    private void setBrandingColorFont1(String brandingColorFont1) {
        this.brandingColorFont1 = brandingColorFont1;
    }

    private String getBrandingColorFont2() {
        if(brandingColorFont2.equals("")){
            loadBranding();
        }
        return brandingColorFont2;
    }

    private void setBrandingColorFont2(String brandingColorFont2) {
        this.brandingColorFont2 = brandingColorFont2;
    }

    private String getBrandingColorFont3() {
        if(brandingColorFont3.equals("")){
            loadBranding();
        }
        return brandingColorFont3;
    }

    private void setBrandingColorFont3(String brandingColorFont3) {
        this.brandingColorFont3 = brandingColorFont3;
    }

    private String getBrandingColorAccent1() {
        if(brandingColorAccent1.equals("")){
            loadBranding();
        }
        return brandingColorAccent1;
    }

    private void setBrandingColorAccent1(String brandingColorAccent1) {
        this.brandingColorAccent1 = brandingColorAccent1;
    }

    private String getBrandingColorAccent2() {
        if(brandingColorAccent2.equals("")){
            loadBranding();
        }
        return brandingColorAccent2;
    }

    private void setBrandingColorAccent2(String brandingColorAccent2) {
        this.brandingColorAccent2 = brandingColorAccent2;
    }

    private void loadBranding(){

        Logger.logMessage("Loading Branding Fields from QC_GLOBALS ...", Logger.LEVEL.DEBUG);

        try {
            // Lookup Tokenized FileName
            ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(QUERY_BRANDING_INFO, new Object[]{}, true);
            if(arrayList.isEmpty()){
                return;// No results so exit
            }

            if(arrayList.get(0).containsKey(FIELD_BRANDING_COLOR_BG1)){
                setBrandingColorBG1("#"+arrayList.get(0).get(FIELD_BRANDING_COLOR_BG1).toString());
            }

            if(arrayList.get(0).containsKey(FIELD_BRANDING_COLOR_BG2)){
                setBrandingColorBG2("#"+arrayList.get(0).get(FIELD_BRANDING_COLOR_BG2).toString());
            }

            if(arrayList.get(0).containsKey(FIELD_BRANDING_COLOR_BG3)){
                setBrandingColorBG3("#"+arrayList.get(0).get(FIELD_BRANDING_COLOR_BG3).toString());
            }

            if(arrayList.get(0).containsKey(FIELD_BRANDING_COLOR_F1)){
                setBrandingColorFont1("#"+arrayList.get(0).get(FIELD_BRANDING_COLOR_F1).toString());
            }

            if(arrayList.get(0).containsKey(FIELD_BRANDING_COLOR_F2)){
                setBrandingColorFont2("#"+arrayList.get(0).get(FIELD_BRANDING_COLOR_F2).toString());
            }

            if(arrayList.get(0).containsKey(FIELD_BRANDING_COLOR_F3)){
                setBrandingColorFont3("#"+arrayList.get(0).get(FIELD_BRANDING_COLOR_F3).toString());
            }

            if(arrayList.get(0).containsKey(FIELD_BRANDING_COLOR_A1)){
                setBrandingColorAccent1("#"+arrayList.get(0).get(FIELD_BRANDING_COLOR_A1).toString());
            }

            if(arrayList.get(0).containsKey(FIELD_BRANDING_COLOR_A2)){
                setBrandingColorAccent2("#"+arrayList.get(0).get(FIELD_BRANDING_COLOR_A2).toString());
            }

            if(arrayList.get(0).containsKey(FIELD_BRANDING_NAME)){
                setBrandingProgramName(arrayList.get(0).get(FIELD_BRANDING_NAME).toString());
            }

        } catch(Exception ex){
            Logger.logMessage("Error occured in method: loadBranding()");
            Logger.logException(ex);
        }

    }

    public String buildSubject(){
        // This method returns the final dynamically built Email Subject
        return this.getSubject();
    }

    public String buildBody(){
        // This method returns the final dynamically built Email Body
        return this.getBody();
    }

    private String getSubject() {
        return subject;
    }

    private void setSubject(String subject) {
        this.subject = subject;
    }

    private String getBody() {
        return body;
    }

    private void setBody(String body) {
        this.body = body;
    }

    private HashMap getParameterMap() {
        return parameterMap;
    }

    public void setParameterMap(HashMap parameterMap) {
        this.parameterMap = parameterMap;
    }

    private String getCompanyName() {
        return companyName;
    }

    private void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    private String getRecipientName() {
        return recipientName;
    }

    private void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public static String getAppCode() {
        if(MMHEmailBuilder.appCode == null || MMHEmailBuilder.appCode.isEmpty()){
            // LOG Missing App Code
            Logger.logMessage("Unable to retrieve AppCode. App Code is missing.", Logger.LEVEL.DEBUG);
        }
        return appCode;
    }

    public static void setAppCode(String appCode) {
        MMHEmailBuilder.appCode = appCode;
    }

    private String getBrandingProgramName() {
        if(brandingProgramName.equals("")){
            loadBranding();
        }
        return brandingProgramName;
    }

    private void setBrandingProgramName(String brandingProgramName) {
        this.brandingProgramName = brandingProgramName;
    }

    private String processAmount(String text){

        // Parse and Update text
        Logger.logMessage("Processing Amount ...", Logger.LEVEL.DEBUG);

        try {
            if(text.contains(TAG_AMOUNT)) {

                String amount = parameterMap.containsKey("Amount") ? parameterMap.get("Amount").toString(): "";
                String feeAmount = parameterMap.containsKey("FeeAmount") ? parameterMap.get("FeeAmount").toString(): "";
                String processedAmount = parameterMap.containsKey("ProcessedAmount") ? parameterMap.get("ProcessedAmount").toString(): "";

                // if feeAmount and processedAmount are not empty the compute amount as (amount = processedAmount - feeAmount)
                // otherwise use the amount as provided in the parameterMap
                if(!feeAmount.isEmpty() && !processedAmount.isEmpty()) {
                    BigDecimal fee = new BigDecimal(feeAmount);
                    BigDecimal processed = new BigDecimal(processedAmount);
                    BigDecimal fundedAmount = processed.subtract(fee);
                    amount = fundedAmount.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
                }

                // Replace merge field
                text = replaceText(text, TAG_AMOUNT, amount);
            }

        } catch (Exception ex){
            Logger.logMessage("Error in method processAmount(): "+ex.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return text;
    }

    private String processFeeText(String text){

        // Parse and Update text
        Logger.logMessage("Processing FeeText ...", Logger.LEVEL.DEBUG);

        try {
            if(text.contains(TAG_FEE_TEXT)) {

                // Get Parameters or reasonable defaults
                String feeText = "";
                String feeLabel = parameterMap.containsKey("FeeLabel") ? parameterMap.get("FeeLabel").toString(): "None";
                String feeAmount = parameterMap.containsKey("FeeAmount") ? parameterMap.get("FeeAmount").toString(): "0.00";
                Logger.logMessage("FeeLabel: "+feeLabel, Logger.LEVEL.DEBUG);
                Logger.logMessage("FeeAmount: "+feeAmount, Logger.LEVEL.DEBUG);

                if(!feeAmount.equalsIgnoreCase("0.00")){
                    feeText = feeLabel+" $"+feeAmount;
                }

                // Replace merge field
                text = replaceText(text, TAG_FEE_TEXT, feeText);
            }

        } catch (Exception ex){
            Logger.logMessage("Error in method processFeeText(): "+ex.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        return text;
    }

    private String processReceipt(String text) {

        Logger.logMessage("Processing Receipt ...", Logger.LEVEL.DEBUG);

        if(text.contains(TAG_RECEIPT)){

            // Initialize default value
            String receiptHTML = "";

            if(getParameterMap().containsKey("PATransactionID")){
                int PATransactionID = Integer.parseInt(parameterMap.get("PATransactionID").toString());

                ReceiptGen rg = new ReceiptGen();

                //Set renderer
                IRenderer htmlRenderer = rg.createBasicHTMLRenderer();

                //Set receipt data object
                rg.setReceiptData(rg.createReceiptData());
                rg.getReceiptData().setPopupVal(true);
                rg.getReceiptData().setReceiptTypeID(TypeData.ReceiptType.POPUPRECEIPT);

                //Build and generate the receipt
                if (htmlRenderer != null && rg.buildFromDBOrderNum(PATransactionID, TypeData.ReceiptType.POPUPRECEIPT, htmlRenderer)) {
                    receiptHTML = htmlRenderer.toString();

                    receiptHTML = receiptHTML.replace("table width='35%'", "table width='100%'");
                }
            }

            // Replace Receipt
            text = replaceText(text, TAG_RECEIPT, receiptHTML);
        }

        return text;
    }

    private String processParameterQuery(String text, String tag, String field, String key, String query) {
        Logger.logMessage("Processing " + tag + " ...", Logger.LEVEL.DEBUG);
        String replacement = "";

        //IF the text has the tag to replace
        if (text.contains(tag)) {
            //If the tag is a key in params, then use it's value
            HashMap params = getParameterMap();
            if (params.containsKey(tag)) {
                replacement = params.get(tag).toString();

            //Otherwise, we need to query the database and the params should hold those values
            } else {
                if (params.containsKey(key)) {
                    ArrayList<HashMap> arrayList = dm.parameterizedExecuteQuery(query, new Object[]{parameterMap.get(key).toString()}, true);
                    if (!arrayList.isEmpty()) {
                        if (arrayList.get(0).containsKey(field)) {
                            replacement = arrayList.get(0).get(field).toString();
                        }
                    }
                }
            }

            text = replaceText(text, tag, replacement);
        }
        return text;
    }
}
