package com.mmhayes.common.utils;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.mmhayes.common.utils.Logger;

/**
 * <p>Title: QuickCharge 4</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: MMHayes</p>
 * @author smcgarvey
 * @version 4.0
 */

public class MMHResultSet {
	public ResultSet resultSet;
	public Statement statement;
    public PreparedStatement preparedStatement = null;

	public MMHResultSet() {
	}

	public void close() {
		try {
            if (resultSet != null) {
			    resultSet.close();
            }
            if (statement != null) {
			    statement.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
		} catch (SQLException se) {
			Logger.logMessage("Could not close MMHResultSet (SQLException).", Logger.LEVEL.ERROR);
			se.printStackTrace();
        } catch (Exception ex) {
            Logger.logMessage("Could not close MMHResultSet (Generic Exception).", Logger.LEVEL.ERROR);
            ex.printStackTrace();
        }
	}
}