package com.mmhayes.common.utils;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/* Logger
 $Author: ecdyer $: Author of last commit
 $Date: 2021-07-27 11:27:32 -0400 (Tue, 27 Jul 2021) $: Date of last commit
 $Rev: 14541 $: Revision of last commit
*/

public class Logger {

    private static LEVEL defaultLevel = LEVEL.TRACE; // We use this level if none is supplied
    public static LEVEL currentLogLevel;

    private static final String outputFileDefault = "debug" + ((MMHServletStarter.getInstanceIdentifier() != null && MMHServletStarter.getInstanceIdentifier().length() > 0) ? "_" + MMHServletStarter.getInstanceIdentifier() : "") + ".log";
    private static String logFileFolder = ((MMHServletStarter.getApplicationLogLocation() != null && MMHServletStarter.getApplicationLogLocation().length() > 0) ? MMHServletStarter.getApplicationLogLocation() : "QC Log Files");
    private static SimpleDateFormat sdfLogMessageDTS = new SimpleDateFormat("MMddyy.HHmmss.SSS");

    static
    {
        currentLogLevel = LEVEL.TRACE;

        try {
            // Don't Log at ROOT
            Configurator.setRootLevel(Level.OFF);

            // Update Console Appender
            setConsoleAppender();

            // Set Log Level
            setLogLevel();

            // Output the initial state
            logMessage("LOGGING LEVEL SET TO " + currentLogLevel, LEVEL.IMPORTANT);
            logMessage("LOGGING LEVELS: I: IMPORTANT, E: ERROR, W: WARNING, T: TRACE, D: DEBUG, L: LUDICROUS",LEVEL.IMPORTANT);

            //SC2P: removing the properties file entirely, no need to have watcher
            /*
            // Start File Listener - Will listen for Logging config changes
            String propFilePath = (MMHServletStarter.getInstancePropertyLocation() + "/" + MMHProperties.APP_SETTINGS_FILE).replace("//", "/");
            if(!propFilePath.equalsIgnoreCase("/")){
                //Only start watcher if there is a valid file path
                PropertiesWatcher.start(propFilePath);
            }
            */

        } catch (Exception ex) {
            writeException(ex);
        }

    }

    private static void setConsoleAppender() {

        // Check for DEVELOPER
        Level level = Level.OFF;
        if(MMHServletStarter.isDevEnv()){
            // ENABLE CONSOLE LOGGER For developers
            level = Level.DEBUG;
        } else {
            // Remove Console Logger in non developer environment
            LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
            Configuration config = ctx.getConfiguration();
            config.getRootLogger().removeAppender("CONSOLE_APP");
            ctx.updateLoggers();
        }
        Configurator.setLevel("com.mmhayes", level);
        System.out.println("Console logging set to: "+level);

    }

    public static String getOutputFile() {
        return outputFileDefault;
    }

    public static void setLogLevel() {

        try {
            switch (MMHProperties.getAppSetting("site.logging.level").toUpperCase()) {
                case "NONE": currentLogLevel = LEVEL.NONE; break;
                case "IMPORTANT": currentLogLevel = LEVEL.IMPORTANT; break;
                case "ERROR": currentLogLevel = LEVEL.ERROR; break;
                case "WARNING": currentLogLevel = LEVEL.WARNING; break;
                case "TRACE": currentLogLevel = LEVEL.TRACE; break;
                case "DEBUG": currentLogLevel = LEVEL.DEBUG; break;
                case "LUDICROUS": currentLogLevel = LEVEL.LUDICROUS; break;
                default:
                    switch (MMHProperties.getAppSetting("site.debugmode").toUpperCase()) {
                        case "ON": currentLogLevel = LEVEL.TRACE; break;
                        case "DETAIL": currentLogLevel = LEVEL.DEBUG; break;
                        default: currentLogLevel = LEVEL.ERROR; break;
                    }
                    break;
            }
        } catch (Exception e) {
            try
            {
                switch (MMHProperties.getAppSetting("site.debugmode").toUpperCase()) {
                    case "ON": currentLogLevel = LEVEL.TRACE; break;
                    case "DETAIL": currentLogLevel = LEVEL.DEBUG; break;
                    default: currentLogLevel = LEVEL.ERROR; break;
                }
            } catch (Exception e2)
            {
                currentLogLevel = LEVEL.ERROR;
            }
        }

        try {
            // Update Log4J2 configuration
            updateLoggingConfig();

        } catch(Exception ex){
            writeException(ex);
        }

    }

    public static void logMessage(String message) {
        logMessage(message, outputFileDefault, defaultLevel);
    }

    public static void logMessage(String message, String outputFileName) {
        if (outputFileName.length() == 0) {
            logMessage(message, outputFileDefault, defaultLevel);
        } else {
            logMessage(message, outputFileName, defaultLevel);
        }
    }

    public static void logMessage(String message, LEVEL level) {
        logMessage(message, outputFileDefault, level);
    }

    @Deprecated
    public static void logMessage(String message, boolean useDebugModeFlag, String outputFile) {
        logMessage(message, outputFile, (useDebugModeFlag ? defaultLevel : LEVEL.TRACE));
    }

    @Deprecated
    public static void logMessage(String message, String outputFile, boolean debugMode) {
        logMessage(message, outputFile, (debugMode ? LEVEL.DEBUG : LEVEL.NONE));
    }

    // THIS IS THE ACTUAL METHOD THAT ALL OF THE OVERLOADS CALL
    public static void logMessage(String message, String outputFile, LEVEL level) {
        log(message, outputFile,null, level);
    }

    public static void logException(Exception ex) {
        logException(ex,"");
    }

    public static void logException(Exception ex, String outputFileName) {
        logException("",outputFileName, ex);
    }

    public static void logException(String message, String outputFileName, Exception ex) {
        log(message, outputFileName, ex, LEVEL.ERROR);
    }

    public static void shutDown() throws InterruptedException {
        try {
            // Log shut down
            System.out.println(sdfLogMessageDTS.format(new Date())+String.format(" %03d",Thread.currentThread().getId()) + " Shutting down...");

            //SC2P: removing the properties file entirely, no need to have watcher
            /*
            // Stop File Watcher
            PropertiesWatcher.stop();
            */

            // Stop Logger
            LogManager.shutdown();

        } catch (Exception ex){
            System.out.println("Error in Logger.shutDown method.");
            System.out.println(ex.getMessage());
        }
    }

    public static enum LEVEL {
        NONE(0),  // Turn off logging
        IMPORTANT(1), // only log "important" messages
        ERROR(2), // only log errors and "important" messages
        WARNING(3), // reasonable logging
        TRACE(4), // reasonable logging
        DEBUG(5), // big verbose log files
        LUDICROUS(6); // ridiculously verbose log files
        private final int value;
        LEVEL(int value) {
            this.value = value;
        }
        public int getCodeInt() {
            return value;
        }
        Level getLevel(){
            switch (this.value){
                case 0:
                    return Level.OFF;
                case 1:
                    return Level.FATAL;
                case 2:
                    return Level.ERROR;
                case 3:
                    return Level.WARN;
                case 4:
                    return Level.INFO;
                case 5:
                    return Level.DEBUG;
                case 6:
                    return Level.TRACE;
                default:
                    return Level.ERROR;
            }
        }
    }

    private static void log(String message, String outputFileName, Exception ex, LEVEL level){

        try {
            // Compute logFileName if empty
            outputFileName = outputFileName.isEmpty() ? outputFileDefault : outputFileName;

            // remove ".log" from file name if present
            if(outputFileName.toLowerCase().endsWith(".log")){
                outputFileName = outputFileName.replace(".log","");
            }

            //Format Date
            SimpleDateFormat dateFormat = new SimpleDateFormat(" yyyyMMdd");
            String replaceString = dateFormat.format(new Date());

            // Add parameters to the ThreadContext - LOG4J2 will use these params when writing log files
            ThreadContext.put("logPathName", logFileFolder);
            ThreadContext.put("logFileName", outputFileName+replaceString);
            ThreadContext.put("logFilePattern", outputFileName);
            ThreadContext.put("thread", String.format("%03d", Thread.currentThread().getId()));

            // MUST USER FULL QUIALIFIED NAMES TO ADVOID NAMESPACE ISSUES
            org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(Logger.class);

            // if level is NULL then DEFAULT to DEBUG
            if(level == null){
                level = LEVEL.DEBUG;
            }

            // Log Message if not empty
            if(!message.isEmpty()){
                switch (level.getCodeInt()) {
                    case 0:
                        // NONE - DON"T LOG
                        logger.log(Level.OFF, message);
                        break;
                    case 1:
                        // IMPORTANT
                        logger.log(Level.FATAL, message);
                        break;
                    case 2:
                        //  ERROR
                        logger.log(Level.ERROR, message);
                        break;
                    case 3:
                        // WARNING
                        logger.log(Level.WARN, message);
                        break;
                    case 4:
                        // TRACE
                        logger.log(Level.INFO, message);
                        break;
                    case 5:
                        // DEBUG
                        logger.log(Level.DEBUG, message);
                        break;
                    case 6:
                        // LUDICROUS
                        logger.log(Level.TRACE, message);
                        break;
                    default: break;
                }
            }

            // if exception is not null then log Exception
            if(ex != null) {
                // build error string
                String err = " exception: " + ex.getMessage() + "\n";
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                ex.printStackTrace(pw);
                err += sw.toString();

                // Write Exception to Log4J
                logger.log(Level.ERROR, err);
            }

            // reset context map
            ThreadContext.clearMap();

        } catch(Exception e){
            writeException(e);
        }

    }

    private static void writeException(Exception ex){
        try {
            String err = ex.getMessage() + "\n";
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            err += sw.toString();
            System.out.println("LOGGER exception: " + err);
        } catch(Exception e){
            System.out.println("Error in: Logger.writeException(): "+e.getMessage());
        }
    }

    private static void updateLoggingConfig(){

        //Set Log4J Level to match currentLogLevel
        Configurator.setLevel(LogManager.getLogger(Logger.class).getName(), currentLogLevel.getLevel());
        //Set Root Logger level to OFF to prevent duplicate logging to tomcat
        Configurator.setRootLevel(Level.OFF);

    }

}
