package com.mmhayes.common.utils;

import com.mmhayes.common.dataaccess.DataManager;

public abstract class MMHRunnable implements Runnable {

    private final int backgroundTaskId;
    private static final DataManager dm = new DataManager(); //the usual data manager

    protected MMHRunnable(int backgroundTaskId) {
        this.backgroundTaskId = backgroundTaskId;
    }

    int getBackgroundTaskId() {
        return this.backgroundTaskId;
    }

    protected boolean isActive() {
        boolean active = (boolean) dm.parameterizedExecuteScalar("data.scheduledTasks.getActiveTaskById", new Object[]{this.backgroundTaskId});
        Logger.logMessage("Background Task " + this.backgroundTaskId + " status: " + active, Logger.LEVEL.TRACE);
        return active;
    }

}
