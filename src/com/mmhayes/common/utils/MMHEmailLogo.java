package com.mmhayes.common.utils;

/**
 * Created by mmhayes on 5/22/2020.
 */
public class MMHEmailLogo {

    private String mergeField;
    private String image;
    private String style = "";
    private String className = "";
    private String height = "";
    private String width = "";

    MMHEmailLogo(String mergeField, String image, String style, String className,  String width, String height ){
        this.mergeField = mergeField;
        this.image = image;
        this.style = style;
        this.className = className;
        this.width = width;
        this.height = height;
    }

    MMHEmailLogo(String mergeField, String image, String style, String className,  String width){
        this.mergeField = mergeField;
        this.image = image;
        this.style = style;
        this.className = className;
        this.width = width;
    }

    MMHEmailLogo(String mergeField, String image, String style, String className){
        this.mergeField = mergeField;
        this.image = image;
        this.style = style;
        this.className = className;
    }

    MMHEmailLogo(String mergeField, String image, String style){
        this.mergeField = mergeField;
        this.image = image;
        this.style = style;
    }

    MMHEmailLogo(String mergeField, String image){
        this.mergeField = mergeField;
        this.image = image;
    }

    public String getMergeField(){
        return mergeField;
    }

    public String getImage(){
        return image;
    }

    public String getClassName(){
        return className;
    }

    public String getStyle(){
        return style;
    }

    public void setMergeField(String mergeField) {
        this.mergeField = mergeField;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
