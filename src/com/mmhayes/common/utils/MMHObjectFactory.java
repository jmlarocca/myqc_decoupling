package com.mmhayes.common.utils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-20 13:10:34 -0400 (Tue, 20 Apr 2021) $: Date of last commit
    $Rev: 13819 $: Revision of last commit
    Notes: Interface for creating objects of a generic type.
*/

public interface MMHObjectFactory<T> {

    /**
     * <p>Gets a new object that is an instance of the object with the given generic type.</p>
     *
     * @return An object of the given generic type.
     */
    T newInstance();

}