package com.mmhayes.common.utils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-20 13:10:34 -0400 (Tue, 20 Apr 2021) $: Date of last commit
    $Rev: 13819 $: Revision of last commit
    Notes: Concrete implementation of a pool containing objects of a generic type as well as the methods that may bee performed on the pool.
*/

import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class MMHBoundedNonBlockingPool<T> extends MMHAbstractPool<T> implements MMHBlockingPool<T> {

    // private member variables
    private int poolSize;
    private Queue<T> elements;
    private Validator<T> validator;
    private MMHObjectFactory<T> factory;
    private Semaphore permits;
    private AtomicBoolean shutdownCalled;

    /**
     * <p>Constructor for a {@link MMHBoundedNonBlockingPool}.</p>
     *
     * @param poolSize The number of elements to have within the {@link MMHPool}.
     * @param validator The {@link Validator} to use to validate the elements within the {@link MMHPool}.
     * @param factory The {@link MMHObjectFactory} to use to create elements within the {@link MMHPool}.
     */
    public MMHBoundedNonBlockingPool(int poolSize, Validator<T> validator, MMHObjectFactory<T> factory) {
        super();

        this.poolSize = poolSize;
        this.validator = validator;
        this.factory = factory;

        // set the number of permits to be equal to the size of the pool
        permits = new Semaphore(poolSize, true);

        elements = new LinkedList<>();
        initializeElementsInMMHPool();

        shutdownCalled = new AtomicBoolean(false);
    }

    /**
     * <p>Checks whether or not objects of the generic type are valid or not.</p>
     *
     * @param t The object to validate.
     * @return Whether or not objects of the generic type are valid or not.
     */
    @Override
    protected boolean isValid (T t) {
        return validator.isValid(t);
    }

    /**
     * <p>Returns the object of the generic type back into the {@link MMHPool}.</p>
     *
     * @param t The generic object to return to the pool.
     */
    @Override
    protected void returnToMMHPool (T t) {
        if (elements.add(t)) {
            permits.release();
        }
    }

    /**
     * <p>Handles the scenario where the object to return to the {@link MMHPool} is invalid.</p>
     *
     * @param t The generic job which is invalid and is unable to be returned to the {@link MMHPool}.
     */
    @Override
    protected void handleInvalidReturn (T t) {
        // invalidate the element which couldn't be returned to the pool
        validator.invalidate(t);
        Logger.logMessage(String.format("Failed to return the element: %s to the pool in MMHBoundedBlockingPool.handleInvalidReturn!",
                Objects.toString(t.toString(), "N/A")), Logger.LEVEL.ERROR);
    }

    /**
     * <p>Returns an instance from the {@link MMHPool}.</p>
     *
     * @param time The amount of time to block until a new instance is taken from the {@link MMHPool}.
     * @param timeUnit The {@link TimeUnit} used to interpret the time argument.
     * @return The instance retrieved from the {@link MMHPool}.
     * @throws InterruptedException
     */
    @Override
    public T get (long time, TimeUnit timeUnit) throws InterruptedException {
        T t = null;

        if (!shutdownCalled.get()) {
            if (permits.tryAcquire()) {
                t = elements.poll();
            }
        }

        return t;
    }

    /**
     * <p>Returns an instance from the {@link MMHPool}.</p>
     *
     * @return The instance retrieved from the {@link MMHPool}.
     */
    @Override
    public T get () {
        T t = null;

        if (!shutdownCalled.get()) {
            if (permits.tryAcquire()) {
                t = elements.poll();
            }
        }

        return t;
    }

    /**
     * <p>Shuts down the {@link MMHPool} and prevents subsequent requests from accessing resources within the {@link MMHPool}.</p>
     *
     */
    @Override
    public void shutdown () {
        shutdownCalled.set(true);
        // clean up elements in the pool so that they are considered invalid when a new pool is built
        invalidateElements();
    }

    /**
     * <p>Creates and adds initial elements for the {@link MMHPool}.</p>
     *
     */
    private void initializeElementsInMMHPool () {
        if (factory != null) {
            for (int i = 0; i < poolSize; i++) {
                elements.add(factory.newInstance());
            }
        }
    }

    /**
     * <p>Marks any elements remaining within the {@link MMHPool} as invalid.</p>
     *
     */
    private void invalidateElements () {
        if (!DataFunctions.isEmptyCollection(elements)) {
            for (T t : elements) {
                validator.invalidate(t);
            }
        }
    }

}