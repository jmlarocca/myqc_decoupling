package com.mmhayes.common.utils;

import com.mmhayes.common.receiptGen.HashMapDataFns;
import org.apache.commons.lang.math.NumberUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.StringReader;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>Title: QuickCharge 4</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: MMHayes</p>
 * @author jjohnson
 * @version 4.0
 */

public class DataFunctions {

//	public static ArrayList convertALFieldToInteger(ArrayList list, int i) {
//		if (list == null) return null;
//		Iterator iter = list.iterator();
//		while (iter.hasNext()) {
//			Object item = iter.next();
//			if (item instanceof ArrayList) {
//				ArrayList temp = (ArrayList)item;
//				// Convert field i to Integer
//				temp.set(i,convertToInteger(temp.get(i)));
//			} else if (item instanceof HashMap) {
//				System.out.println("ERROR: Calling convertALFieldToInteger with ArrayList of HashMaps");
//			}
//		}
//		return list;
//	}
//
//	public static ArrayList convertALFieldToInteger(ArrayList list, String fieldName) {
//		if (list == null) return null;
//		Iterator iter = list.iterator();
//		while (iter.hasNext()) {
//			Object item = iter.next();
//			if (item instanceof ArrayList) {
//				System.out.println("ERROR: Calling convertALFieldToInteger with ArrayList of ArrayLists");
//			} else if (item instanceof HashMap) {
//				HashMap temp = (HashMap)item;
//				fieldName = fieldName.toUpperCase();
//				temp.put(fieldName,convertToInteger(temp.get(fieldName)));
//			}
//		}
//		return list;
//	}
//
//	public static ArrayList convertALFieldToBoolean(ArrayList list, int i) {
//		if (list == null) return null;
//		Iterator iter = list.iterator();
//		while (iter.hasNext()) {
//			Object item = iter.next();
//			if (item instanceof ArrayList) {
//				ArrayList temp = (ArrayList)item;
//				// Convert field i to Boolean
//				temp.set(i,convertToBoolean(temp.get(i)));
//			} else if (item instanceof HashMap) {
//				System.out.println("ERROR: Calling convertALFieldToBoolean with ArrayList of HashMaps");
//			}
//		}
//		return list;
//	}
//
//	public static ArrayList convertALFieldToBoolean(ArrayList list, String fieldName) {
//		if (list == null) return null;
//		Iterator iter = list.iterator();
//		while (iter.hasNext()) {
//			Object item = iter.next();
//			if (item instanceof ArrayList) {
//				System.out.println("ERROR: Calling convertALFieldToBoolean with ArrayList of ArrayLists");
//			} else if (item instanceof HashMap) {
//				HashMap temp = (HashMap)item;
//				fieldName = fieldName.toUpperCase();
//				temp.put(fieldName,convertToBoolean(temp.get(fieldName)));
//			}
//		}
//		return list;
//	}

    public static String arrayListToString(ArrayList list) {
        Iterator iter = list.iterator();
        StringBuffer b = new StringBuffer();
        while (iter.hasNext()) {
            Object item = iter.next();
            b.append(item + ",");
        }
        return b.length() > 0 ? b.substring(0, b.length()-1) : b.toString();
    }

    public static ArrayList convertToArrayList(Vector v) {
        if (v == null) return null;
        ArrayList a = new ArrayList(v.size());
        Iterator iter = v.iterator();
        while (iter.hasNext()) {
            Object item = iter.next();
            if (item instanceof Vector) {
                item = convertToArrayList((Vector)item);
            }
            a.add(item);
        }
        return a;
    }

    public static Vector convertToVector(ArrayList a) {
        if (a == null) return null;
        Vector v = new Vector(a.size());
        Iterator iter = a.iterator();
        while (iter.hasNext()) {
            Object item = iter.next();
            if (item instanceof ArrayList) {
                item = convertToVector((ArrayList)item);
            }
            v.addElement(item);
        }
        return v;
    }

    /**
     * Returns a vector that contains the same objects as the array.
     * @param anArray  the array to be converted
     * @return  the new vector; if <code>anArray</code> is <code>null</code>,
     *				returns <code>null</code>
     */
    public static Vector convertToVector(Object[] anArray) {
        if (anArray == null)
            return null;

        Vector v = new Vector(anArray.length);
        for (int i=0; i < anArray.length; i++) {
            v.addElement(anArray[i]);
        }
        return v;
    }

    public static ArrayList convertToArrayList(Object[] anArray) {
        if (anArray == null)
            return null;

        ArrayList a = new ArrayList(anArray.length);
        for (int i=0; i < anArray.length; i++) {
            a.add(anArray[i]);
        }
        return a;
    }

    /**
     * Returns a vector of vectors that contains the same objects as the array.
     * @param anArray  the double array to be converted
     * @return the new vector of vectors; if <code>anArray</code> is
     *				<code>null</code>, returns <code>null</code>
     */
    public static Vector convertToVector(Object[][] anArray) {
        if (anArray == null)
            return null;

        Vector v = new Vector(anArray.length);
        for (int i=0; i < anArray.length; i++) {
            v.addElement(convertToVector(anArray[i]));
        }
        return v;
    }

    public static ArrayList convertToArrayList(Object[][] anArray) {
        if (anArray == null)
            return null;

        ArrayList a = new ArrayList(anArray.length);
        for (int i=0; i < anArray.length; i++) {
            a.add(convertToArrayList(anArray[i]));
        }
        return a;
    }

    public static Object[] convertToArray(ArrayList a) {
        Object[] ret = new Object[a.size()];
        for (int i = 0; i < a.size(); i++) {
            ret[i] = a.get(i);
        }
        return ret;
    }

    public static Object[][] convertTo2DArray(ArrayList a) {
        if (a == null || a.size() <= 0 || !(a.get(0) instanceof ArrayList)) {
            throw new IllegalArgumentException("ArrayList must be an ArrayList of equal sized ArrayList.");
        }
        Object[][] ret = new Object[a.size()][];
        for (int i = 0; i < a.size() - 1; i++) {
            ret[i] = convertToArray((ArrayList)a.get(i));
        }
        return ret;
    }

    public static double round(double x, int decimals) {  // rounds to the nearest 'decimals' places
        int factor = 1;
        for (int i = 0; i < Math.abs(decimals); i++) factor *= 10;
        if (decimals < 0) return factor * Math.rint(x / factor);
        else return Math.rint(factor * x) / factor;
    }
    public static double floor(double x, int decimals) {  // always rounds down
        int factor = 1;
        for (int i = 0; i < Math.abs(decimals); i++) factor *= 10;
        if (decimals < 0) return factor * Math.floor(x / factor);
        else return Math.floor(factor * x) / factor;
    }

    public static double ceil(double x, int decimals) {  // always rounds up
        int factor = 1;
        for (int i = 0; i < Math.abs(decimals); i++) factor *= 10;
        if (decimals < 0) return factor * Math.ceil(x / factor);
        else return Math.ceil(factor * x) / factor;
    }
    public static Boolean convertToBoolean(Object o) {
        Boolean ret;
        if ((o.toString().compareToIgnoreCase("true") == 0) || (o.toString().compareToIgnoreCase("false") == 0)) {
            ret = new Boolean (Boolean.valueOf(o.toString()).booleanValue());
        } else if (o instanceof Double) {
            ret = new Boolean (Double.valueOf(o.toString()).intValue() == 1);
        } else if (o instanceof Integer) {
            ret = new Boolean (Integer.valueOf(o.toString()).intValue() == 1);
        } else {
            ret = new Boolean (Integer.valueOf(o.toString()).intValue() == 1);
        }
        //if (QCApplet.DEBUG_MODE)
        System.out.println("convertToBoolean(" + o.toString() + ") = " + ret.toString());
        return ret;
    }
    public static Integer convertToInteger(Object o) {
        Integer ret;
        if (o instanceof Double) {
            ret = new Integer (Double.valueOf(o.toString()).intValue());
        } else {
            ret = new Integer (Integer.valueOf(o.toString()).intValue());
            //return o.toString().;
        }
        //if (QCApplet.DEBUG_MODE)
        System.out.println("convertToInteger(" + o.toString() + ") = " + ret.toString());
        return ret;
    }

    public static int[] convertIntALToIntArr (ArrayList<Integer> al) {

        if (!isEmptyCollection(al)) {
            int[] arr = new int[al.size()];
            for (int i = 0; i < al.size(); i++) {
                arr[i] = al.get(i);
            }
            return arr;
        }

        return null;
    }

    public static ArrayList<Integer> convertIntArrToIntAL (int[] arr) {

        if (arr.length > 0) {
            ArrayList<Integer> al = new ArrayList<>();
            for (int i : arr) {
                al.add(i);
            }
            return al;
        }

        return null;
    }

    /**
     * Generic method to compute the symmetric difference between two sets. Please see
     * https://en.wikipedia.org/wiki/Symmetric_difference for more information in symmetric differences.
     *
     * @param set1 {@link Set<T>}
     * @param set2 {@link Set<T>}
     * @return {@link Set<T>}
     */
    public static <T> Set<T> symmetricDifference (Set<T> set1, Set<T> set2) {

        try {
            // initialize the differencesSet to contain the elements of set1
            Set<T> differencesSet = new HashSet<>(set1);

            for (T element : set2) {
                // if the element in set2 can't be added to the differencesSet then we know the element was in set1
                // and can be removed from the differencesSet since it's a common element between set1 and set2.
                if (!differencesSet.add(element)) {
                    differencesSet.remove(element);
                }
            }

            // return the differences between the two sets
            return differencesSet;
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to compute thje symmetrice difference between two sets in " +
                    "DataFunctions.symmetricDifference", Logger.LEVEL.ERROR);
        }

        return null;
    }

    /**
     * Utility method to convert HashMap to HashMap<String, String>.
     *
     * @param genericHM {@link HashMap}
     * @return {@link HashMap<String, String>}
     */
    public static HashMap<String, String> convertGenericHMToStringHM (HashMap genericHM) {

        try {
            if ((genericHM != null) && (!genericHM.isEmpty())) {
                HashMap<String, String> stringHM = new HashMap<>();

                genericHM.forEach((key, value) -> {
                    if ((key != null) && (!key.toString().isEmpty()) && (value != null)) {
                        stringHM.put(key.toString(), value.toString());
                    }
                });

                if (!stringHM.isEmpty()) {
                    return stringHM;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to convert the HashMap to HashMap<String, String> in " +
                    "DataFunctions.convertGenericHMToStringHM", Logger.LEVEL.ERROR);
        }

        return null;
    }

    /**
     * Utility method to convert ArrayList<HashMap> to ArrayList<HashMap<String, String>>.
     *
     * @param alOfGenericHM {@link ArrayList<HashMap>}
     * @return {@link HashMap<String, String>}
     */
    public static ArrayList<HashMap<String, String>> convertALOfGenericHMToALOfStringHM (ArrayList<HashMap> alOfGenericHM) {

        try {
            if ((alOfGenericHM != null) && (!alOfGenericHM.isEmpty())) {
                ArrayList<HashMap<String, String>> alOfStringHM = new ArrayList<>();

                alOfGenericHM.forEach((genericHM) -> {
                    HashMap<String, String> stringHM = new HashMap<>();
                    if ((genericHM != null) && (!genericHM.isEmpty())) {
                        genericHM.forEach((key, value) -> {
                            if ((key != null) && (!key.toString().isEmpty()) && (value != null)) {
                                stringHM.put(key.toString(), value.toString());
                            }
                        });
                    }
                    if (!stringHM.isEmpty()) {
                        alOfStringHM.add(stringHM);
                    }
                });

                if (!alOfStringHM.isEmpty()) {
                    return alOfStringHM;
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to convert the ArrayList<HashMap> to " +
                            "ArrayList<HashMap<String, String>> in DataFunctions.convertALOfGenericHMToALOfStringHM",
                    Logger.LEVEL.ERROR);
        }

        return null;
    }

    /**
     * Generic utility method to concatenate two Arrays of the same type. Note this method will not work on primitive
     * types.
     *
     * @param a1 {@link T[]} Array to concatenate.
     * @param a2 {@link T[]} The other array to concatenate.
     * @return {@link T[]} The concatenation of a1 and a2.
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] concatenateArrays (T[] a1, T[] a2) {
        T[] retArray = null;

        try {
            retArray = (T[]) Array.newInstance(a1.getClass().getComponentType(), a1.length + a2.length);
            System.arraycopy(a1, 0, retArray, 0, a1.length);
            System.arraycopy(a2, 0, retArray, a1.length, a2.length);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("There was a problem trying to concatenate the two arrays of type %s " +
                            "in DataFunctions.concatenateArrays", Objects.toString(a1.getClass().toString(), "NULL")),
                    Logger.LEVEL.ERROR);
        }

        return retArray;
    }

    /**
     * Utility method to find all substrings within the given String that begin with the given start character sequence
     * and end with the given character sequence.
     *
     * @param s {@link String} String to parse.
     * @param startSeq {@link String} Starting character sequence.
     * @param endSeq {@link String} Ending character sequence.
     * @return {@link String[]} Substrings within the given String that begin with the given start character sequence
     *         and end with the given character sequence.
     */
    public static String[] substringsBetween (String s, String startSeq, String endSeq) {
        String[] substringsBetween = new String[]{};

        try {
            if ((s != null) && (!s.isEmpty()) && (startSeq != null) && (!startSeq.isEmpty()) && (endSeq != null) && (!endSeq.isEmpty())) {

                ArrayList<String> substringsAL = new ArrayList<>();

                int sLen = s.length();
                int startSeqLen = startSeq.length();
                int endSeqLen = endSeq.length();

                int currPos = 0;
                while (currPos < (sLen - endSeqLen)) {
                    // get starting index of the substring
                    int startPos = s.indexOf(startSeq, currPos);
                    if (startPos < 0) { break; }
                    startPos += startSeqLen;
                    // get ending index of the substring
                    int endPos = s.indexOf(endSeq, startPos);
                    if (endPos < 0) { break; }
                    // add the substring to the ArrayList of substrings
                    substringsAL.add(s.substring(startPos, endPos));
                    currPos = endPos + endSeqLen;
                }

                if (!substringsAL.isEmpty()) {
                    substringsBetween = substringsAL.toArray(new String[substringsAL.size()]);
                }
            }
        }
        catch (Exception e) {
            Logger.logMessage(String.format("There was a problem trying to get the substrings inbetween the starting " +
                            "character sequence of %s and ending at the character sequence of %s in " +
                            "DataFunctions.substringsBetween",
                    Objects.toString(startSeq, "NULL"),
                    Objects.toString(endSeq, "NULL")), Logger.LEVEL.ERROR);
        }

        return substringsBetween;
    }

    /**
     * Converts an int array into a String for SQL purposes.
     *
     * @param arr The array of ints to convert.
     * @return {@link String} Int array String for SQL.
     */
    public static String convertIntArrToStrForSQL (int[] arr) {
        String s = "";

        try {
            if ((arr != null) && (arr.length > 0)) {
                for (int i = 0; i < arr.length; i++) {
                    if (i == arr.length-1) {
                        s += "'"+arr[i]+"'";
                    }
                    else {
                        s += "'"+arr[i]+"',";
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to convert the array of Integers int a String in " +
                    "DataFunctions.convertIntArrToStrForSQL", Logger.LEVEL.ERROR);
        }

        return s;
    }

    /**
     * Utility method to check whether or not a map is empty.
     *
     * @param m {@link Map} The map to check.
     * @return Whether or not the given map is empty.
     */
    public static boolean isEmptyMap (Map m) {
        return ((m == null) || (m.isEmpty()));
    }


    /**
     * Utility method to check whether or not a collection is empty.
     *
     * @param c {@link Collection} The collection to check.
     * @return Whether or not the given collection is empty.
     */
    public static boolean isEmptyCollection (Collection c) {
        return ((c == null) || (c.isEmpty()));
    }

    /**
     * Utility method to check whether or not an int array is empty.
     *
     * @param iArr The int array to check.
     * @return Whether or not the given int array is empty.
     */
    public static boolean isEmptyIntArr (int[] iArr) {
        return ((iArr == null) || (iArr.length == 0));
    }

    /**
     * Utility method to check whether or not a byte array is empty.
     *
     * @param bArr The byte array to check.
     * @return Whether or not the given byte array is empty.
     */
    public static boolean isEmptyByteArr (byte[] bArr) {
        return ((bArr == null) || (bArr.length == 0));
    }

    /**
     * Utility method to check whether or not an array of a given type is empty.
     *
     * @param genericArr The array to check.
     * @return Whether or not the given array is empty.
     */
    public static <T> boolean isEmptyGenericArr (T[] genericArr) {
        return ((genericArr == null) || (genericArr.length == 0));
    }

    /**
     * <p>Utility method to check whether or not an {@link Enumeration} is null or empty.</p>
     *
     * @param e The {@link Enumeration} to check.
     * @return Whether or not the given {@link Enumeration} is null or empty.
     */
    public static boolean isEmptyEnumeration (Enumeration e) {
        return ((e == null) || (!e.hasMoreElements()));
    }

    /**
     * Utility method to convert an ArrayList of Integers to a comma delimited String.
     *
     * @param ints {@link ArrayList<Integer>} The ArrayList to convert.
     * @return {@link String} ArrayList of Integers as a comma delimited String.
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static String convertIntArrayListToStr (ArrayList<Integer> ints) {
        String s = "";

        try {
            if (!isEmptyCollection(ints)) {
                StringBuilder sb = new StringBuilder();
                for (int i : ints) {
                    sb.append(i);
                    sb.append(",");
                }
                // remove trailing comma
                sb.setLength(sb.length() - 1);
                s = sb.toString();
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to convert the ArrayList of Integers into a comma delimited " +
                    "String in DataFunctions.convertIntArrayListToStr", Logger.LEVEL.ERROR);
        }

        return s;
    }

    /**
     * Designed to take in a custom offline transaction from QCPOS.  {local PA Trans Id} + "T" + {local Terminal id}
     * i.e.- 123T1000, 123 is the PA Trans Id, 1000 is the Terminal Id
     *
     * @param offlineTransId
     * @param splitChar
     * @return
     * @throws Exception
     */
    public static HashMap<String, Integer> parseOfflineTransIdFormat(String offlineTransId, String splitChar) throws Exception {
        HashMap<String, Integer> offlineTransIdParts = new HashMap<>();
        String localPaTransIdField = "localPaTransId";
        String localTerminalField = "localTerminalId";

        try {

            String[] offlineTransParts = offlineTransId.split(splitChar);
            offlineTransIdParts.put(localPaTransIdField, Integer.parseInt(offlineTransParts[0].toString()));
            offlineTransIdParts.put(localTerminalField, Integer.parseInt(offlineTransParts[1].toString()));

        } catch (Exception ex) {
            Logger.logMessage("TransactionModel.parseOfflineTransIdFormat: Error parsing out offline transaction info.");
            throw ex;
        }

        return offlineTransIdParts;
    }

    /**
     * <p>Makes sure the given {@link ArrayList} of {@link HashMap} doesn't contain any null or empty {@link HashMap}.</p>
     *
     * @param alOfHm The {@link ArrayList} of {@link HashMap} to purge of null and empty {@link HashMap}.
     * @return The given {@link ArrayList} of {@link HashMap} with any null or empty {@link HashMap} removed.
     */
    @SuppressWarnings({"Convert2streamapi", "TypeMayBeWeakened"})
    public static ArrayList<HashMap> purgeAlOfHm (ArrayList<HashMap> alOfHm) {
        ArrayList<HashMap> purgedArrayList = new ArrayList<>();

        try {
            if (!isEmptyCollection(alOfHm)) {
                for (HashMap hm : alOfHm) {
                    if (!isEmptyMap(hm)) {
                        purgedArrayList.add(hm);
                    }
                }
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to purge any null or empty HashMaps from the given ArrayList " +
                    "of HashMaps in DataFunctions.purgeAlOfHm", Logger.LEVEL.ERROR);
        }

        return purgedArrayList;
    }

    /**
     * <p>Utility method to check if the given {@link String} can be parsed into a {@link LocalDateTime}.</p>
     *
     * @param s The {@link String} to test.
     * @return Whether or not the given {@link String} can be parsed into a {@link LocalDateTime}.
     */
    public static boolean isValidLocalDateTimeString (String s) {
        boolean valid = false;

        try {
            LocalDateTime.parse(s);
            valid = true;
        }
        catch (Exception e) {
            valid = false;
        }

        return valid;
    }

    /**
     * <p>Utility method to remove empty {@link String} from a {@link String} array.</p>
     *
     * @param strArr The {@link String} array to remove empty {@link String} from.
     * @return The {@link String} array with empty {@link String} removed.
     */
    public static String[] removeEmptyStringsFromStringArr (String[] strArr) {
        String[] resArr = null;

        try {
            if (!isEmptyGenericArr(strArr)) {
                // convert the string array into an ArrayList of Strings
                ArrayList<String> strAL = new ArrayList<>(Arrays.asList(strArr));
                // iterate over the String ArrayList and remove any empty Strings
                Iterator<String> strALItr = strAL.iterator();
                while (strALItr.hasNext()) {
                    if (!StringFunctions.stringHasContent(strALItr.next())) {
                        strALItr.remove();
                    }
                }
                // convert the ArrayList of Strings back into a String array
                resArr = strAL.toArray(new String[strAL.size()]);
            }
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("There was a problem trying to remove empty Strings from the String array in " +
                    "DataFunctions.removeEmptyStringsFromStringArr", Logger.LEVEL.ERROR);
        }

        return resArr;
    }

    /**
     * <p>Converts the {@link Collection} of {@link Integer} to a {@link Collection} of {@link String}.</p>
     *
     * @param intCol The {@link Collection} of {@link Integer} to convert to a {@link Collection} of {@link String}.
     * @return The {@link Collection} of {@link Integer} as a {@link Collection} of {@link String}.
     */
    public static Collection<String> convertIntCollectionToStrCollection (Collection<Integer> intCol) {

        if (!isEmptyCollection(intCol)) {
            return intCol.stream().map(String::valueOf).collect(Collectors.toList());
        }

        return null;
    }

    /**
     * <p>Converts the {@link Collection} of {@link String} to a {@link Collection} of {@link Integer}.</p>
     *
     * @param strCol The {@link Collection} of {@link String} to convert to a {@link Collection} of {@link Integer}.
     * @return The {@link Collection} of {@link String} as a {@link Collection} of {@link Integer}.
     */
    public static Collection<Integer> convertStrCollectionToIntCollection (Collection<String> strCol) {

        if (!isEmptyCollection(strCol)) {
            return strCol.stream().filter(s -> (StringFunctions.stringHasContent(s)) && (NumberUtils.isNumber(s))).map(Integer::parseInt).collect(Collectors.toList());
        }

        return null;
    }

    /**
     * <p>Tries to covert the given collection to a collection of hashmaps.</p>
     *
     * @param c The {@link Collection} to convert.
     * @return The converted {@link Collection} as a {@link Collection} of {@link HashMap}.
     */
    @SuppressWarnings("Convert2streamapi")
    public static Collection<HashMap> convertToCollectionOfHM (Collection c) {
        Collection<HashMap> resCollection = new ArrayList<>();

        if (!DataFunctions.isEmptyCollection(c)) {
            for (Object o : c) {
                if ((o != null) && (o instanceof HashMap)) {
                    resCollection.add(((HashMap) o));
                }
            }
        }

        return resCollection;
    }

    /**
     * <p>Gets the first {@link java.util.Map.Entry} within the given {@link Map}.</p>
     *
     * @param m The {@link Map} to get the first {@link java.util.Map.Entry} within.
     * @return The first {@link java.util.Map.Entry} within the given {@link Map}.
     */
    public static <K, V> Map.Entry<K, V> getFirstMapEntry (Map<K, V> m) {

        if (DataFunctions.isEmptyMap(m)) {
            Logger.logMessage("The map passed to DataFunctions.getFirstMapEntry can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        return m.entrySet().iterator().next();
    }

    /**
     * <p>Converts the given {@link Date} into a {@link LocalDateTime}.</p>
     *
     * @param d The {@link Date} to convert.
     * @return The converted {@link Date} as a {@link LocalDateTime}.
     */
    public static LocalDateTime convertDateToLocalDateTime (Date d) {

        if (d == null) {
            Logger.logMessage("The Date passed to DataFunctions.convertDateToLocalDateTime can't be null!");
            return null;
        }

        return new Timestamp(d.getTime()).toLocalDateTime();
    }

    /**
     * <p>Reads the given {@link File} into a {@link String}.</p>
     *
     * @param f The {@link File} to read into a {@link String}.
     * @param charset The {@link Charset} to use.
     * @return The {@link String} built from reading the given {@link File}.
     */
    public static String convertFileToString (File f, Charset charset) {
        if (f == null) {
            Logger.logMessage("The File passed to DataFunctions.convertFileToString can't be null!", Logger.LEVEL.ERROR);
            return null;
        }

        // read the File into a String
        StringBuilder stringBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(f.getAbsolutePath()), charset)) {
            stream.forEach(stringBuilder::append);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("Failed to read the given File into a String in DataFunctions.convertFileToString!", Logger.LEVEL.ERROR);
        }

        return stringBuilder.toString();
    }

    /**
     * <p>Converts the given byte array into a {@link String}.</p>
     *
     * @param bArr The byte array to convert into a {@link String};
     * @param charset The {@link Charset} to use.
     * @param showBytes Whether or not the {@link String} should show the individual bytes.
     * @return The given byte array as a {@link String}.
     */
    public static String convertByteArrToStr (byte[] bArr, Charset charset, boolean showBytes) {

        if (isEmptyByteArr(bArr)) {
            Logger.logMessage("The byte Array passed to DataFunctions.convertByteArrToStr can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        if (showBytes) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("[");
            for (int i = 0; i < bArr.length; i++) {
                if (i == bArr.length - 1) {
                    // last byte in the bArray
                    stringBuilder.append(String.format("0x%02X", bArr[i]));
                }
                else {
                    stringBuilder.append(String.format("0x%02X ,", bArr[i]));
                }
            }
            stringBuilder.append("]");
            return stringBuilder.toString();
        }
        else {
            return new String(bArr, charset);
        }

    }

    /**
     * <p>Converts the given {@link String} into a {@link Document}.</p>
     *
     * @param s The given {@link String} to convert into a {@link Document}.
     * @return The given {@link String} as a {@link Document}.
     */
    public static Document convertStrToDoc (String s) {

        // make sure we have a valid String
        if (!StringFunctions.stringHasContent(s)) {
            Logger.logMessage("The String passed to DataFunctions.convertStrToDoc can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        StringReader stringReader = null;
        try {
            stringReader = new StringReader(s);
            InputSource inputSource = new InputSource(stringReader);
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            return documentBuilder.parse(inputSource);
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage(String.format("A problem occurred in DataFunctions.convertStrToDoc while trying to convert the String %s to a Document!",
                    Objects.toString(s, "N/A")), Logger.LEVEL.ERROR);
        }
        finally {
            if (stringReader != null) {
                try {
                    stringReader.close();
                }
                catch (Exception e) {
                    Logger.logException(e);
                    Logger.logMessage("A problem occurred while trying to close the StringReader in DataFunctions.convertStrToDoc!", Logger.LEVEL.ERROR);
                }
            }
        }

        return null;
    }

    /**
     * <p>Transforms the given {@link Document} into a XML {@link File}.</p>
     *
     * @param doc The given {@link Document} to convert into a XML {@link File}.
     * @param f The {@link File} that will contain the XML.
     * @param transOutputProps A {@link HashMap} of whose {@link String} key is the transformer property and whose {@link String} value is the value for the transformer property.
     * @return The given {@link Document} as a XML {@link File}.
     */
    @SuppressWarnings("TypeMayBeWeakened")
    public static File transformDocToXML (Document doc, File f, HashMap<String, String> transOutputProps) {

        // make sure we have a valid Document
        if (doc == null) {
            Logger.logMessage("The Document passed to DataFunctions.transformDocToXML can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we have a valid File
        if (f == null) {
            Logger.logMessage("The File passed to DataFunctions.transformDocToXML can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        try {
            DOMSource domSource = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            // set transformer output properties if they are supplied
            if (!DataFunctions.isEmptyMap(transOutputProps)) {
                for (Map.Entry<String, String> entry : transOutputProps.entrySet()) {
                    transformer.setOutputProperty(entry.getKey(), entry.getValue());
                }
            }
            StreamResult streamResult = new StreamResult(f.toURI().getPath());
            // do the transformation
            transformer.transform(domSource, streamResult);
            return f;
        }
        catch (Exception e) {
            Logger.logException(e);
            Logger.logMessage("A problem occurred while trying to transform the given Document into a XML File in DataFunctions.transformDocToXML!", Logger.LEVEL.ERROR);
        }

        return null;
    }

    /**
     * <p>Converts a {@link FileTime} to a {@link LocalDateTime}.</p>
     *
     * @param ft The {@link FileTime} to convert into a {@link LocalDateTime}.
     * @return The {@link LocalDateTime} obtained from the given {@link FileTime}.
     */
    public static LocalDateTime convertFileTimeToLocalDateTime (FileTime ft) {

        if (ft == null) {
            Logger.logMessage("The FileTime passed to DataFunctions.convertFileTimeToLocalDateTime can't be null!", Logger.LEVEL.ERROR);
            return null;
        }

        return LocalDateTime.ofInstant(ft.toInstant(), ZoneId.systemDefault());
    }

    /**
     * <p>Swaps the keys and values for each entry within a Map.</p>
     *
     * @param m The {@link Map} to swap the keys and values for.
     * @return A {@link HashMap} whose key's are the given {@link Map} values and whose values are the given {@link Map} keys.
     */
    public static <K, V> HashMap<V, K> swapMapKV (Map<K, V> m) {

        // make sure the Map passed to this method has content
        if (isEmptyMap(m)) {
            Logger.logMessage("The Map passed to DataFunctions.swapMapKV can't be null or empty!", Logger.LEVEL.ERROR);
            return null;
        }

        // swap the keys and values
        HashMap<V, K> retMap = new HashMap<>();
        for (Map.Entry<K, V> e : m.entrySet()) {
            retMap.put(e.getValue(), e.getKey());
        }

        return retMap;
    }

    /**
     * <p>Iterates through an {@link ArrayList} of {@link HashMap} and extracts the unique values stored within each {@link HashMap} with the {@link K} key.</p>
     *
     * @param alOfHM The {@link ArrayList} of {@link HashMap} to iterate through.
     * @param prop The key of type {@link K} at which to find the value.
     * @return A {@link Set} of unique values extracted from the {@link ArrayList} of {@link HashMap}.
     */
    @SuppressWarnings({"unchecked", "Convert2streamapi"})
    public static <K, T> Set<T> getUniqueValsAtKeyFromALOfHM (ArrayList<HashMap> alOfHM, K prop) {

        // make sure we have an ArrayList of HashMaps to iterate through
        if (DataFunctions.isEmptyCollection(alOfHM)) {
            Logger.logMessage("The ArrayList of HashMaps passed to DataFunctions.getUniqueValsAtKeyFromALOfHM can't be null or empty, unable to get IDs at the given key, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure we were supplied a key to search for within the HashMap
        if (prop == null) {
            Logger.logMessage("The HashMap key passed to DataFunctions.getUniqueValsAtKeyFromALOfHM can't be null, unable to get IDs at the given key, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        Set<T> ids = new HashSet<>();
        for (HashMap hm : alOfHM) {
            if ((!DataFunctions.isEmptyMap(hm)) && (hm.containsKey(prop))) {
                ids.add(((T) HashMapDataFns.getValFromMap(hm, prop)));
            }
        }

        return ids;
    }

    /**
     * <p>Iterates through a collection of objects and organizes them based on an object property.</p>
     *
     * @param col The {@link Collection} of type {@link V} objects to sort.
     * @param clazz The {@link Class} of the type of data in the given {@link Collection}.
     * @param propName The name {@link String} of the object property to sort by.
     * @return A {@link Map} with a {@link K} key of the objects property and a {@link Collection} of {@link V} value corresponding to
     */
    public static <K, V> Map<K, Collection<V>> sortCollectionIntoMapBasedOnProp (Collection<V> col, Class clazz, String propName) {

        // make sure the collection isn't empty or null
        if (DataFunctions.isEmptyCollection(col)) {
            Logger.logMessage("The Collection passed to DataFunctions.sortCollectionIntoMapBasedOnProp can't be null or empty, unable to sort the collection, now returning null.", Logger.LEVEL.ERROR);
            return new HashMap<>();
        }

        // make sure the Class is valid
        if (clazz == null) {
            Logger.logMessage("The Class passed to DataFunctions.sortCollectionIntoMapBasedOnProp can't be null, unable to sort the collection, now returning null.", Logger.LEVEL.ERROR);
            return new HashMap<>();
        }

        // make sure the property name is valid
        if (!StringFunctions.stringHasContent(propName)) {
            Logger.logMessage("The property name passed to DataFunctions.sortCollectionIntoMapBasedOnProp can't be null or empty, unable to sort the collection, now returning null.", Logger.LEVEL.ERROR);
            return new HashMap<>();
        }

        Map<K, Collection<V>> m = new HashMap<>();
        Collection<V> c;
        for (V item : col) {
            K propVal = null;
            try {
                propVal = getObjProperty(item, clazz, propName);
            }
            catch (Exception e) {
                Logger.logMessage(String.format("Failed to extract the %s property from the class %s instance in DataFunctions.sortCollectionIntoMapBasedOnProp!",
                        Objects.toString(propName, "N/A"),
                        Objects.toString(clazz.getSimpleName(), "N/A")), Logger.LEVEL.ERROR);
            }
            if (propVal != null) {
                if (m.containsKey(propVal)) {
                    c = m.get(propVal);
                }
                else {
                    c = new ArrayList<>();
                }
                c.add(item);
                m.put(propVal, c);
            }
        }

        return m;
    }

    /**
     * <p>Uses the reflection API to get a property from an object.</p>
     *
     * @param obj The object instance of type {@link O} to get the property from.
     * @param clazz The {@link Class} of the object.
     * @param propName The name {@link String} of the property to get from the object.
     * @return The value of type {@link T} for the property within the object.
     * @throws IllegalAccessException
     */
    @SuppressWarnings("unchecked")
    public static <O, T> T getObjProperty (O obj, Class clazz, String propName) throws IllegalAccessException {

        // make sure the instance is valid
        if (obj == null) {
            Logger.logMessage("The object instance passed to DataFunctions.getObjProperty can't be null, unable to get the object property, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the Class is valid
        if (clazz == null) {
            Logger.logMessage("The Class passed to DataFunctions.getObjProperty can't be null, unable to get the object property, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the object's property name is valid
        if (!StringFunctions.stringHasContent(propName)) {
            Logger.logMessage("The property name passed to DataFunctions.getObjProperty can't be null or empty, unable to get the object property, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        T val = null;
        Field[] fields = clazz.getDeclaredFields();
        if (!isEmptyGenericArr(fields)) {
            for (Field field : fields) {
                field.setAccessible(true);
                if (field.getName().equalsIgnoreCase(propName)) {
                    val = ((T) field.get(obj));
                    break;
                }
            }
        }

        return val;
    }

    /**
     * <p>Safely converts a {@link List} of {@link Object} to an {@link ArrayList} of the specified type.</p>
     *
     * @param list The {@link List} to convert.
     * @param clazz The {@link Class} of the objects within the {@link List}.
     * @return An {@link ArrayList} containing elements of the specified type.
     */
    public static <T> ArrayList<T> getGenericALFromObjList (List list, Class<T> clazz) {

        // make sure the list is valid
        if (isEmptyCollection(list)) {
            Logger.logMessage("The List passed to DataFunctions.getGenericALFromObjList can't be null or empty, unable to convert the list, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        // make sure the Class is valid
        if (clazz == null) {
            Logger.logMessage("The Class passed to DataFunctions.getGenericALFromObjList can't be null, unable to convert the list, now returning null.", Logger.LEVEL.ERROR);
            return null;
        }

        ArrayList<T> retList = new ArrayList<>();
        // noinspection Convert2streamapi
        for (Object obj : list) {
            if ((obj != null) && (clazz.isInstance(obj))) {
                retList.add(clazz.cast(obj));
            }
        }

        return retList;
    }

    /**
     * <p>Makes sure all values within the int array are unique.</p>
     *
     * @param arr The int array to make the values unique within.
     * @return An int array whose values are all unique.
     */
    public static int[] makeArrOfUniqueInt (int[] arr) {
        if (!isEmptyIntArr(arr)) {
            HashSet<Integer> uniqueInts = new HashSet<>(Arrays.stream(arr).boxed().collect(Collectors.toList()));
            return uniqueInts.stream().mapToInt(Number::intValue).toArray();
        }
        return null;
    }

    /**
     * <p>Makes sure all values within the {@link List} of {@link Integer} are unique.</p>
     *
     * @param intList The {@link List} of {@link Integer} to make the values unique within.
     * @return An int array whose values are all unique.
     */
    public static int[] makeArrOfUniqueInt (List<Integer> intList) {
        if (!isEmptyCollection(intList)) {
            int[] intCollectionArr = new int[intList.size()];
            for (int i = 0; i < intList.size(); i++) {
                intCollectionArr[i] = intList.get(i);
            }
            return makeArrOfUniqueInt(intCollectionArr);
        }
        return null;
    }

    /**
     * <p>Converts a {@link List} of {@link Object} to a {@link List} of the specified {@link Class}.</p>
     *
     * @param clazz The specified {@link Class} to convert elements of the {@link List} to.
     * @param objs The {@link List} of {@link Object} to convert.
     * @return The {@link List} of {@link Object} converted to a {@link List} of the specified {@link Class}.
     */
    public static <T> List<T> magicalGenericListGetter (Class<T> clazz, List<Object> objs) {
        List<T> list = new ArrayList<>();
        for (Object obj : objs) {
            T t = convertObjectToType(obj, clazz);
            list.add(t);
        }
        return list;
    }

    /**
     * <p>Gets the value of a specific bit within a byte.</p>
     *
     * @param b The byte to get the specific bit from.
     * @param bitPos The position within the byte to get the bit value of [0-7].
     * @return The value of a specific bit within the given byte.
     */
    public static int getBit (byte b, int bitPos) throws IllegalArgumentException {

        if ((bitPos < 0) || (bitPos > 7)) {
            throw new IllegalArgumentException("The position of the bit within the byte must be an int within the range of [0,7]!");
        }

        return (b >> bitPos) & 1;
    }

    /**
     * <p>Converts the given {@link Object} to in instance of the given type.</p>
     *
     * @param o The {@link Object} to convert.
     * @param clazz The specified {@link Class} to convert the {@link Object} to.
     * @return The {@link Object} converted to an instance of the specified {@link Class}.
     */
    public static <T> T convertObjectToType (Object o, Class<T> clazz) {
        return clazz.cast(o);
    }

}