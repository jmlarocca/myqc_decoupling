package com.mmhayes.common.utils;

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.dataaccess.DataManager;

import java.util.ArrayList;
import java.util.HashMap;

/*
* $Author: ecdyer $: Author of last commit
* $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
* $Rev: 20495 $: Revision of last commit
* Notes: Common Helper class to determine if app and terminal versions meet
*/
public class MMHHashingHelper {

    private static DataManager dm = new DataManager(); // ye olde data manager
    //VERSIONS
    private static final String VERSION_MIN_SERVER_POS =  "1.8.0.7";
    private static final String VERSION_MIN_MYQC =        "1.6.0";
    private static final String VERSION_MIN_PA =          "1.5.0.18";
    private static final String VERSION_MIN_QCPOS_TERM =  "1.8.0.7";
    // QUERIES
    private static final String QUERY_GET_VERSIONS =    "data.common.checkCanUseBCrypt.GetAppVersions";
    private static final String QUERY_GET_ALL_TERM =    "data.common.checkCanUseBCrypt.ActiveTerminalVersions";
    // FIELDS
    private static final String FIELD_QCPOC =           "POSDMVERSION";
    private static final String FIELD_MYQC =            "MYQCAPIVERSION";
    private static final String FIELD_PA =              "POSANYWHEREVERSION";
    private static final String FIELD_SOFTWARE_VER =    "SOFTWAREVERSION";
    private static final String FIELD_TERM_ID =         "TERMINALID";
    private static final String FIELD_TERM_NAME=        "NAME";
    // PREFIX
    private static final String PREFIX =                "CheckCanUseBCrypt: ";
    // CACHE FLAG
    private static int CACHE_FLAG = -1;                 // -1 = NOT RUN CHECK YET

    public static boolean checkCanUseBCrypt(){

        // if first time run, cacheFlag value will be -1 so run method
        if(CACHE_FLAG > -1){
           // already run so return result
           // cacheFlag == 0 CANNOT use BCrypt | cacheFlag == 1 CAN use BCrypt
           Logger.logMessage(PREFIX +"CACHED: " + (CACHE_FLAG == 1), Logger.LEVEL.DEBUG);
           return CACHE_FLAG == 1;
        }

        // INIT return var
        boolean canBCrypt =                 false;
        boolean metMinimumQCPOSVersion =    false;
        boolean metMinimumMyQCVersion =     false;
        boolean metMinimumPAVersion =       false;
        boolean metMinimumTerminalVersion = false;

        ArrayList<HashMap> arrayList;

        try {
            Logger.logMessage(PREFIX + "Checking if BCrypt password hashing can be used...", Logger.LEVEL.DEBUG);
            // CHECK Application versions
            arrayList = dm.parameterizedExecuteQuery(QUERY_GET_VERSIONS, new Object[]{}, true);
            if(!arrayList.isEmpty()){
                // CHECK QCPOS Version
                if(arrayList.get(0).containsKey(FIELD_QCPOC)){
                    String posVersion = arrayList.get(0).get(FIELD_QCPOC).toString();
                    Logger.logMessage(PREFIX + "Current QCPOS version: ("+posVersion+
                            ") Min. req. version: ("+VERSION_MIN_SERVER_POS+")", Logger.LEVEL.DEBUG);
                    if(CommonAPI.verifyVersionIsSameOrGreater(posVersion, VERSION_MIN_SERVER_POS)){
                        Logger.logMessage(PREFIX + "QCPOS("+posVersion+") meets BCrypt minimum version requirements.", Logger.LEVEL.DEBUG);
                        metMinimumQCPOSVersion = true;
                    }
                }

                // CHECK MyQC Version
                if(arrayList.get(0).containsKey(FIELD_MYQC)){
                    String myqcVersion = arrayList.get(0).get(FIELD_MYQC).toString();
                    Logger.logMessage(PREFIX + "Current My Quickcharge version: ("+myqcVersion+
                            ") Min. req. version: ("+VERSION_MIN_MYQC+")", Logger.LEVEL.DEBUG);
                    if(CommonAPI.verifyVersionIsSameOrGreater(myqcVersion, VERSION_MIN_MYQC)){
                        Logger.logMessage(PREFIX + "My QC("+myqcVersion+") meets BCrypt minimum version requirements.", Logger.LEVEL.DEBUG);
                        metMinimumMyQCVersion = true;
                    }
                }

                // CHECK POSANYWHERE Version
                if(arrayList.get(0).containsKey(FIELD_PA)){
                    String paVersion = arrayList.get(0).get(FIELD_PA).toString();
                    Logger.logMessage(PREFIX + "Current POSAnywhere version: ("+paVersion+
                            ") Min. req. version: ("+VERSION_MIN_PA+")", Logger.LEVEL.DEBUG);
                    if(CommonAPI.verifyVersionIsSameOrGreater(paVersion, VERSION_MIN_PA )){
                        Logger.logMessage(PREFIX + "POSAnywhere("+paVersion+") meets BCrypt minimum version requirements.", Logger.LEVEL.DEBUG);
                        metMinimumPAVersion = true;
                    }
                }
            } else {
                Logger.logMessage(PREFIX + "Unable to determine minimum application versions.", Logger.LEVEL.ERROR);
            }

            // Check ALL ACTIVE Terminals meet minimum requirements
            arrayList = dm.parameterizedExecuteQuery(QUERY_GET_ALL_TERM, new Object[]{}, true);
            String softwareVersion,terminalName, terminalID;
            int terminalsPassedCount = 0;
            int terminalsBelowCount = 0;
            if(!arrayList.isEmpty()){
                for(HashMap record : arrayList){
                    softwareVersion = "";
                    terminalName = "";
                    terminalID = "";
                    if(record.containsKey(FIELD_SOFTWARE_VER)){
                        softwareVersion = record.get(FIELD_SOFTWARE_VER).toString();
                    }
                    if(record.containsKey(FIELD_TERM_NAME)){
                        terminalName = record.get(FIELD_TERM_NAME).toString();
                    }
                    if(record.containsKey(FIELD_TERM_ID)){
                        terminalID = record.get(FIELD_TERM_ID).toString();
                    }
                    if(CommonAPI.verifyVersionIsSameOrGreater(softwareVersion, VERSION_MIN_QCPOS_TERM)){
                        Logger.logMessage(PREFIX + "Terminal: "+terminalName+" ("+terminalID+") Version: "+softwareVersion +" meets minimum requirements.", Logger.LEVEL.DEBUG);
                        terminalsPassedCount++;
                    } else {
                        Logger.logMessage(PREFIX + "Terminal: "+terminalName+" ("+terminalID+") Version: "+softwareVersion +" BELOW minimum requirements.", Logger.LEVEL.DEBUG);
                        terminalsBelowCount++;
                    }
                }
                Logger.logMessage(terminalsPassedCount+" QCPOS terminals MEET minimum requirements.", Logger.LEVEL.TRACE);
                Logger.logMessage(terminalsBelowCount+" QCPOS terminals BELOW minimum requirements.", Logger.LEVEL.TRACE);
                if(terminalsPassedCount>0 && terminalsBelowCount == 0){
                    Logger.logMessage(PREFIX + "All QCPOS terminals meet minimum requirements for using BCrypt.", Logger.LEVEL.DEBUG);
                    metMinimumTerminalVersion = true;
                }
            } else {
                // if no terminals then NOT using QCPOS and hashing check should be true according to Team Review
                Logger.logMessage(PREFIX + "No Active QCPOS Terminals found. Met minimum requirements for using BCrypt.", Logger.LEVEL.TRACE);
                metMinimumTerminalVersion = true;
            }

            // Met All Requirements then PASS check
            if(metMinimumQCPOSVersion && metMinimumMyQCVersion && metMinimumPAVersion && metMinimumTerminalVersion){
                Logger.logMessage(PREFIX + "Met minimum application and terminal version checks. BCrypt hashing supported.", Logger.LEVEL.TRACE);
                canBCrypt = true;
            } else {
                if(!metMinimumQCPOSVersion){
                    Logger.logMessage(PREFIX + "QCPOS minimum version requirement not met. BCrypt hashing NOT supported.", Logger.LEVEL.TRACE);
                }
                if(!metMinimumMyQCVersion){
                    Logger.logMessage(PREFIX + "My Quickcharge minimum version requirement not met. BCrypt hashing NOT supported.", Logger.LEVEL.TRACE);
                }
                if(!metMinimumPAVersion){
                    Logger.logMessage(PREFIX + "POSAnywhere minimum version requirement not met. BCrypt hashing NOT supported.", Logger.LEVEL.TRACE);
                }
                if(!metMinimumTerminalVersion){
                    Logger.logMessage(PREFIX + "QCPOS terminals minimum version requirement not met. BCrypt hashing NOT supported.", Logger.LEVEL.TRACE);
                }
            }
        } catch(Exception ex) {
            Logger.logMessage("Error in method: checkCanUseBCrypt()", Logger.LEVEL.ERROR);
            Logger.logException(ex);
        }

        // LOG Final result
        Logger.logMessage(PREFIX + canBCrypt, Logger.LEVEL.TRACE);

        // SET STATIC CACHE FLAG
        CACHE_FLAG = canBCrypt ? 1 : 0;

        // Return Result
        return canBCrypt;
    }

}
