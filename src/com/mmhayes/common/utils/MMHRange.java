package com.mmhayes.common.utils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-09-10 09:30:06 -0400 (Thu, 10 Sep 2020) $: Date of last commit
    $Rev: 12603 $: Revision of last commit
    Notes: Represents a range.
*/

import java.util.Objects;

/**
 * <p>Represents a range.</p>
 *
 */
public class MMHRange {

    // private member variables of a MMHRange
    private int lowerBound = -1;
    private int upperBound = -1;

    /**
     * <p>Constructor for a {@link MMHRange}.</p>
     *
     * @param lowerBound The range's lower bound.
     * @param upperBound The range's upper bound.
     */
    public MMHRange (int lowerBound, int upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    /**
     * <p>Checks whether or not the given int is within the range including the upper an lower bounds.</p>
     *
     * @param i The int to check for being within the range.
     * @return Whether or not the given int is within the range including the upper an lower bounds.
     */
    public boolean containsInclusive (int i) {
        return ((i >= lowerBound) && (i <= upperBound));
    }

    /**
     * <p>Checks whether or not the given int is within the range excluding the upper an lower bounds.</p>
     *
     * @param i The int to check for being within the range.
     * @return Whether or not the given int is within the range excluding the upper an lower bounds.
     */
    public boolean containsExclusive (int i) {
        return ((i > lowerBound) && (i < upperBound));
    }

    /**
     * <p>Checks whether or not the given int is within the range excluding the lower bound but including the upper bound.</p>
     *
     * @param i The int to check for being within the range.
     * @return Whether or not the given int is within the range excluding the lower bound but including the upper bound.
     */
    public boolean containsLowerExclusive (int i) {
        return ((i > lowerBound) && (i <= upperBound));
    }

    /**
     * <p>Checks whether or not the given int is within the range excluding the upper bound but including the lower bound.</p>
     *
     * @param i The int to check for being within the range.
     * @return Whether or not the given int is within the range excluding the upper bound but including the lower bound.
     */
    public boolean containsUpperExclusive (int i) {
        return ((i >= lowerBound) && (i < upperBound));
    }

    /**
     * <p>String representation of a {@link MMHRange}.</p>
     *
     * @return The {@link MMHRange} instance as a {@link String}.
     */
    @Override
    public String toString () {

        return String.format("LOWER BOUND: %s, UPPER BOUND: %s",
                Objects.toString(lowerBound, "N/A"),
                Objects.toString(upperBound, "N/A"));

    }

    /**
     * <p>Checks whether or not two {@link MMHRange} instances are equal.</p>
     *
     * @param obj The {@link MMHRange} instance to compare against.
     * @return Whether or not two {@link MMHRange} instances are equal.
     */
    @Override
    public boolean equals (Object obj) {
        // check for obvious equality case
        if (this == obj) {
            return true;
        }

        // check for obvious inequality cases
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        // cast the given obj to a MMHRange and check if the obj's lower and upper bound are equal to this MMHRanges's lower and upper bound
        MMHRange mmhRange = ((MMHRange) obj);
        return ((Objects.equals(mmhRange.lowerBound, lowerBound)) && (Objects.equals(mmhRange.upperBound, upperBound)));
    }

    /**
     * <p>The hashCode method for a {@link MMHRange}.</p>
     *
     * @return The hash code for a {@link MMHRange}.
     */
    @Override
    public int hashCode () {
        return Objects.hash(lowerBound, upperBound);
    }

    /**
     * <p>Getter for the lowerBound field of the {@link MMHRange}.</p>
     *
     * @return The lowerBound field of the {@link MMHRange}.
     */
    public int getLowerBound () {
        return lowerBound;
    }

    /**
     * <p>Setter for the lowerBound field of the {@link MMHRange}.</p>
     *
     * @param lowerBound The lowerBound field of the {@link MMHRange}.
     */
    public void setLowerBound (int lowerBound) {
        this.lowerBound = lowerBound;
    }

    /**
     * <p>Getter for the upperBound field of the {@link MMHRange}.</p>
     *
     * @return The upperBound field of the {@link MMHRange}.
     */
    public int getUpperBound () {
        return upperBound;
    }

    /**
     * <p>Setter for the upperBound field of the {@link MMHRange}.</p>
     *
     * @param upperBound The upperBound field of the {@link MMHRange}.
     */
    public void setUpperBound (int upperBound) {
        this.upperBound = upperBound;
    }

}