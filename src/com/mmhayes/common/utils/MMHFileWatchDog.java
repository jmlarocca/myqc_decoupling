package com.mmhayes.common.utils;

import java.io.File;

public abstract class MMHFileWatchDog extends Thread {
    protected String filename;
    protected long delay = 60000L;
    File file;
    private long lastModif = 0L;
    private boolean warnedAlready = false;
    private boolean interrupted = false;

    protected MMHFileWatchDog(String filename) {
        super("MMHFileWatchDog");
        this.filename = filename;
        this.file = new File(filename);
        this.setDaemon(true);
        this.checkAndConfigure();
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    protected abstract void doOnChange();

    protected void checkAndConfigure() {
        boolean fileExists;
        try {
            fileExists = this.file.exists();
        } catch (SecurityException var4) {
            System.out.println("MMHFileWatchDog: Was not allowed to read check file existance, file:[" + this.filename + "].");
            this.interrupted = true;
            return;
        }

        if(fileExists) {
            long l = this.file.lastModified();
            if(l > this.lastModif) {
                this.lastModif = l;
                this.doOnChange();
                this.warnedAlready = false;
            }
        } else if(!this.warnedAlready) {
            System.out.println("MMHFileWatchDog: [" + this.filename + "] does not exist.");
            this.warnedAlready = true;
        }

    }

    public void run() {
        for(; !this.interrupted; this.checkAndConfigure()) {
            try {
                Thread.sleep(this.delay);
            } catch (InterruptedException var2) {
                this.interrupted = true;
            }
        }
        System.out.println("MMHFileWatchDog has completed.");
    }
}