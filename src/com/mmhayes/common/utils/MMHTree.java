package com.mmhayes.common.utils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-08-31 11:06:25 -0400 (Mon, 31 Aug 2020) $: Date of last commit
    $Rev: 12512 $: Revision of last commit
    Notes: Can be use to build a generic tree with an arbitrary number of children (n-ary tree).
*/

import java.util.ArrayList;

/**
 * <p>Can be use to build a generic tree with an arbitrary number of children (n-ary tree).</p>
 *
 */
public class MMHTree<T> {

    // private member variables of a MMHTree
    private T data = null;
    private MMHTree<T> parent = null;
    private ArrayList<MMHTree<T>> children = new ArrayList<>();

    /**
     * <p>Constructor for a {@link MMHTree}.</p>
     *
     * @param data The data to store within the {@link MMHTree} node.
     */
    public MMHTree (T data) {
        this.data = data;
    }

    /**
     * <p>Finds the root node of the tree from anywhere in the tree.</p>
     *
     * @return The {@link MMHTree} root node of the tree.
     */
    public MMHTree<T> getRootNode () {

        if (parent == null) {
            return this;
        }

        return parent.getRootNode();
    }

    /**
     * <p>Adds a new child node to the tree.</p>
     *
     * @param child The {@link MMHTree} child node to add to the tree.
     * @return The given {@link MMHTree} child node.
     */
    public MMHTree<T> addChild (MMHTree<T> child) {

        if (child == null) {
            Logger.logMessage("Can't add a null child node in MMHTree.addChild!", Logger.LEVEL.ERROR);
            return null;
        }

        // append the child node
        child.setParent(this);
        if (this.children == null) {
            this.children = new ArrayList<>();
        }
        this.children.add(child);

        return child;
    }

    /**
     * <p>Adds multiple child nodes to the tree at once.</p>
     *
     * @param children An {@link ArrayList} of {@link MMHTree} corresponding to the child nodes to add to the tree.
     */
    public void addChildren (ArrayList<MMHTree<T>> children) {

        if (DataFunctions.isEmptyCollection(children)) {
            Logger.logMessage("The list of child nodes passed to MMHTree.addChildren can't be null or empty!", Logger.LEVEL.ERROR);
            return;
        }

        // append the child nodes
        for (MMHTree<T> childNode : children) {
            childNode.setParent(this);
        }
        if (this.children == null) {
            this.children = new ArrayList<>();
        }
        this.children.addAll(children);

    }

    /**
     * <p>Removes the root node from the tree and returns the new root node.</p>
     *
     * @return The new {@link MMHTree} root node of the tree.
     */
    public MMHTree<T> removeRootNode () throws Exception {

        if (parent != null) {
            throw new Exception("MMHTree.removeRootNode must be called on the root node!");
        }

        // assign a new root node and assign child nodes to the new root node
        MMHTree<T> newParent = null;
        if (!DataFunctions.isEmptyCollection(getChildren())) {
            newParent = getChildren().get(0);
            newParent.setParent(null);
            getChildren().remove(0);
            // make sure there are still children
            if (!DataFunctions.isEmptyCollection(getChildren())) {
                for (MMHTree<T> child : children) {
                    child.setParent(newParent);
                }
            }
            newParent.getChildren().addAll(getChildren());
        }
        this.getChildren().clear();

        return newParent;
    }

    /**
     * <p>Removes this node from the tree assuming it's not the root node.</p>
     *
     * @throws Exception
     */
    public void removeNode () throws Exception {

        if (parent == null) {
            throw new Exception("Call MMHTree.removeRootNode to remove the root node!");
        }

        // remove the node and assign any child nodes to the removed node's parent node
        int index = this.parent.getChildren().indexOf(this);
        this.parent.getChildren().remove(this);
        if (!DataFunctions.isEmptyCollection(getChildren())) {
            for (MMHTree<T> child : getChildren()) {
                child.setParent(this.parent);
            }
        }
        this.parent.getChildren().addAll(index, this.getChildren());
        this.getChildren().clear();

    }

    /**
     * <p>Uses a depth first search to print out the nodes in the tree.</p>
     *
     * @param node The {@link MMHTree} node to start the search at.
     * @param appender A {@link String} to be used to improve the readability of the tree.
     */
    public void logTree (MMHTree<T> node, String appender) {

        if (node == null) {
            Logger.logMessage("The node passed to MMHTree.logTree can't be null!", Logger.LEVEL.ERROR);
            return;
        }

        Logger.logMessage(appender + node.getData().toString(), Logger.LEVEL.DEBUG);
        if (!DataFunctions.isEmptyCollection(node.getChildren())) {
            for (MMHTree<T> child : node.getChildren()) {
                logTree(child, appender + appender);
            }
        }
    }

    /**
     * <p>Uses a depth first search to print out the nodes in the tree.</p>
     *
     * @param node The {@link MMHTree} node to start the search at.
     * @param appender A {@link String} to be used to improve the readability of the tree.
     * @param log The file path {@link String} of the log file to log this tree ort any errors to.
     * @param logLevel The {@link com.mmhayes.common.utils.Logger.LEVEL} at which to log the tree.
     */
    public void logTree (MMHTree<T> node, String appender, String log, Logger.LEVEL logLevel) {

        if (node == null) {
            Logger.logMessage("The node passed to MMHTree.logTree can't be null!", log, Logger.LEVEL.ERROR);
            return;
        }

        Logger.logMessage(appender + node.getData().toString(), log, logLevel);
        if (!DataFunctions.isEmptyCollection(node.getChildren())) {
            for (MMHTree<T> child : node.getChildren()) {
                logTree(child, appender + appender, log, logLevel);
            }
        }
    }

    /**
     * <p>Getter for the data field of the {@link MMHTree}.</p>
     *
     * @return The data field of the {@link MMHTree}.
     */
    public T getData () {
        return data;
    }

    /**
     * <p>Setter for the data field of the {@link MMHTree}.</p>
     *
     * @param data The data field of the {@link MMHTree}.
     */
    public void setData (T data) {
        this.data = data;
    }

    /**
     * <p>Getter for the parent field of the {@link MMHTree}.</p>
     *
     * @return The parent field of the {@link MMHTree}.
     */
    public MMHTree<T> getParent () {
        return parent;
    }

    /**
     * <p>Setter for the parent field of the {@link MMHTree}.</p>
     *
     * @param parent The parent field of the {@link MMHTree}.
     */
    public void setParent (MMHTree<T> parent) {
        this.parent = parent;
    }

    /**
     * <p>Getter for the children field of the {@link MMHTree}.</p>
     *
     * @return The children field of the {@link MMHTree}.
     */
    public ArrayList<MMHTree<T>> getChildren () {
        return children;
    }

    /**
     * <p>Setter for the children field of the {@link MMHTree}.</p>
     *
     * @param children The children field of the {@link MMHTree}.
     */
    public void setChildren (ArrayList<MMHTree<T>> children) {
        this.children = children;
    }

    /**
     * <p>Performs a deep copy of the root node.</p>
     *
     * @return A copy of the root node for the {@link MMHTree}.
     */
    public MMHTree<T> deepCopyRoot () {

        // get deep copy the root node
        MMHTree<T> root = getRootNode();
        MMHTree<T> copy = new MMHTree<>(root.data);
        for(MMHTree<T> child : root.children){
            copy.addChild(child.deepCopy());
        }

        return copy;
    }

    /**
     * <p>Performs a deep copy of the tree starting at an arbitrary node.</p>
     *
     * @return A copy of the arbitrary node for the {@link MMHTree}.
     */
    private MMHTree<T> deepCopy () {

        // get a deep copy of the node
        MMHTree<T> copy = new MMHTree<>(this.data);
        for(MMHTree<T> child : this.children){
            copy.addChild(child.deepCopy());
        }

        return copy;
    }

}