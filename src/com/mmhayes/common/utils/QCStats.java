package com.mmhayes.common.utils;

import jpos.JposConst;
import jpos.JposException;
import jpos.POSPrinterConst;

import java.util.HashMap;


/*
 $Author: nyu $: Author of last commit
 $Date: 2016-09-14 20:50:39 -0400 (Wed, 14 Sep 2016) $: Date of last commit
 $Rev: 18351 $: Revision of last commit
 Notes:
*/
public class QCStats {
    private int responseCode;
    private String responseMsg;
    private String methodName;

    public static int SUCCESS = 0;
    public static int UNKNOWN_ERROR = 1;
    public static int ERROR_EXCEPTION = 2;
    public static int JPOS_EXCEPTION = 3;

    static boolean trackingStats = true;
    private long startTime;

    // Factory method
    public static QCStats Create(String name){
        return new QCStats(name);
    }

    // stat tracking
    public static void Queue(QCStats q){
       q.track();
    }

    public QCStats(String mName){
        methodName = mName;
        responseCode = SUCCESS;
        setResponseMsg("");
        if(trackingStats)
            startTime = System.nanoTime();
    }

    public void track() {
        long duration = (System.nanoTime() - getStartTime())/1000000;
        if (duration < 100)
        {
            Logger.logMessage(methodName + " Took " + duration + " milliseconds", Logger.LEVEL.DEBUG);
        }
        else if (duration < 1000)
        {
            Logger.logMessage(methodName + " Took " + duration + " milliseconds", Logger.LEVEL.TRACE);
        }
        else
        {
            Logger.logMessage(methodName + " Took " + duration + " milliseconds", Logger.LEVEL.WARNING);
        }
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    private String GetJPosExceptionMsg(JposException je){
        String msg;
        if(je.getErrorCode() == JposConst.JPOS_E_EXTENDED){
            msg = "Unknown Extended Error";

            switch( je.getErrorCodeExtended()){
                case POSPrinterConst.JPOS_EPTR_COVER_OPEN:
                    msg = "Cover Open";
                    break;
                case POSPrinterConst.JPOS_EPTR_JRN_EMPTY:
                    msg = "Journal Paper Empty";
                    break;
                case POSPrinterConst.JPOS_EPTR_REC_EMPTY:
                    msg = "Receipt Paper Empty";
                    break;
                case POSPrinterConst.JPOS_EPTR_SLP_EMPTY:
                    msg = "Slip Paper Empty";
                    break;
                case POSPrinterConst.JPOS_EPTR_SLP_FORM:
                    msg = "Form Present";
                    break;
                case POSPrinterConst.JPOS_EPTR_TOOBIG:
                    msg = "Bitmap Too Big";
                    break;
                case POSPrinterConst.JPOS_EPTR_BADFORMAT:
                    msg = "Bad Image Format";
                    break;
                case POSPrinterConst.JPOS_EPTR_JRN_CARTRIDGE_REMOVED:
                    msg = "Journal Cartridge Removed";
                    break;
                case POSPrinterConst.JPOS_EPTR_JRN_CARTRIDGE_EMPTY:
                    msg = "Journal Cartridge Empty";
                    break;
                case POSPrinterConst.JPOS_EPTR_JRN_HEAD_CLEANING:
                    msg = "Journal Head Cleaning";
                    break;
                case POSPrinterConst.JPOS_EPTR_REC_CARTRIDGE_REMOVED:
                    msg = "Receipt Cartridge Removed";
                    break;
                case POSPrinterConst.JPOS_EPTR_REC_CARTRIDGE_EMPTY:
                    msg = "Receipt Cartridge Empty";
                    break;
                case POSPrinterConst.JPOS_EPTR_REC_HEAD_CLEANING:
                    msg = "Receipt Head Cleaning";
                    break;
                case POSPrinterConst.JPOS_EPTR_SLP_CARTRIDGE_REMOVED:
                    msg = "Slip Cartridge Removed";
                    break;
                case POSPrinterConst.JPOS_EPTR_SLP_CARTRIDGE_EMPTY:
                    msg = "Slip Cartridge Empty";
                    break;
                case POSPrinterConst.JPOS_EPTR_SLP_HEAD_CLEANING:
                    msg = "Slip Head Cleaning";
                    break;
            }
        }else{
            msg = je.getMessage();
        }
        return msg;
    }

    public void ex(Exception e){
        if(e instanceof JposException){
            if(e.getMessage().equals("Communication with the Model Layer has failed."))
                responseCode = 333;
            else
                responseCode = JPOS_EXCEPTION;
            setResponseMsg(GetJPosExceptionMsg((JposException)e));
        }
        else {
            setResponseMsg(e.getMessage());
            responseCode = ERROR_EXCEPTION;
        }
    }

    public Object trackStatsAndBuildResponse(Object o, boolean returnCustomResponseCodes){

        if(trackingStats){
            QCStats.Queue(this);
        }
        if (returnCustomResponseCodes)
        {
            HashMap hMap = new HashMap();
            hMap.put("responseCode", responseCode);
            hMap.put("responseMsg", "");

            if(null != o && responseCode == SUCCESS)
                hMap.put("response", o);
            else {
                hMap.put("response", "");
                if(null != responseMsg && responseMsg.length()>0)
                    hMap.put("responseMsg", responseMsg);
            }

            return hMap;
        }
        else
        {
            return o;
        }
    }

    public long getStartTime() {
        return startTime;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }
}
