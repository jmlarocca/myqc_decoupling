package com.mmhayes.common.utils.stringMatchingUtils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-05-29 16:39:47 -0400 (Fri, 29 May 2020) $: Date of last commit
    $Rev: 11836 $: Revision of last commit
    Notes: Implementation of the Rabin-Karp algorithm, it works well for searching for a single pattern.
*/

import com.mmhayes.common.utils.DataFunctions;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.ArrayList;

/**
 * <p>Implementation of the Rabin-Karp algorithm, it works well for searching for a single pattern within some text.</p>
 *
 */
public class RabinKarp {

    /**
     * <p>
     *     Uses the Rabin-Karp algorithm to find the indices of the pattern within the text.
     *     NOTES:
     *         By text bucket I mean substring.
     *         best case: O(n+m)
     *         worst case: O(nm)
     * </p>
     *
     * @param pattern The pattern {@link String} to search for.
     * @param text The text {@link String} to search for the pattern within.
     * @param primeNumber A prime number that may used to calculate the hashed value.
     * @return An {@link ArrayList} of {@link Integer} corresponding to the indices of the pattern within the text.
     */
    public static ArrayList<Integer> search (String pattern, String text, int primeNumber) {
        ArrayList<Integer> indices = new ArrayList<>();

        if (!StringFunctions.stringHasContent(pattern)) {
            Logger.logMessage("No pattern to search for was found in StringFunctions.search, unable to perform the search!", Logger.LEVEL.ERROR);
            return null;
        }

        if (!StringFunctions.stringHasContent(text)) {
            Logger.logMessage("No text to search for the pattern within was found in StringFunctions.search, unable to perform the search!", Logger.LEVEL.ERROR);
            return null;
        }

        int patLen = pattern.length();
        int txtLen = text.length();

        // the pattern length should be less than the text length
        if (txtLen < patLen) {
            return indices;
        }

        // calculate the hashes for the pattern and the first bucket of text
        int patHash = buildRabinKarpHash(pattern, patLen, primeNumber);
        int txtBucketHash = buildRabinKarpHash(text, patLen, primeNumber);

        if ((txtLen == 1) && (patHash == txtBucketHash) && (pattern.charAt(0) == text.charAt(0))) {
            indices.add(0);
        }

        // iterate through the text and shift the text bucket by 1 each time
        for (int i = 0; i <= (txtLen - patLen); i++) {
            if (patHash == txtBucketHash) {
                // since the hashes match, lets make sure the characters match too
                boolean match = true;
                int j = 0;
                while ((match) && (j < patLen)) {
                    if (text.charAt(i+j) == pattern.charAt(j)) {
                        j++;
                    }
                    else {
                        // found characters that don't match
                        match = false;
                    }
                }

                // we found a match
                if (match) {
                    indices.add(i);
                    // update the text bucket hash if we need to keep searching
                    if ((i + patLen) < txtLen) {
                        txtBucketHash = updateRabinKarpHash(txtBucketHash, patLen, primeNumber, text.charAt(i), text.charAt(i + patLen));
                    }
                }
            }
            else if ((i + patLen) < txtLen) {
                // recalculate the new text bucket hash
                txtBucketHash = updateRabinKarpHash(txtBucketHash, patLen, primeNumber, text.charAt(i), text.charAt(i + patLen));
            }
        }

        return indices;
    }

    /**
     * <p>Calculates the hash value for the given {@link String} for use in the Rabin-Karp algorithm.</p>
     *
     * @param s The {@link String} to get the hashed value for.
     * @param length The length of the pattern to search for.
     * @param primeNumber A prime number that may used to calculate the hashed value.
     * @return The hash value for the given {@link String} for use in the Rabin-Karp algorithm.
     */
    private static int buildRabinKarpHash (String s, int length, int primeNumber) {

        if (!StringFunctions.stringHasContent(s)) {
            Logger.logMessage("The String passed to StringFunctions.buildRabinKarpHash can't be null or empty!", Logger.LEVEL.ERROR);
            return -1;
        }

        int hash = 0;
        for (int i = 0; i < length; i++) {
            hash = hash + s.charAt(i) * ((int) Math.pow(primeNumber, length - 1 - i));
        }

        return hash;
    }

    /**
     * <p>Updates the text bucket hash in O(1) time for the Rabin-Karp algorithm.</p>
     *
     * @param oldHash The old text bucket has value.
     * @param length length of the pattern.
     * @param primeNumber A prime number that may used to calculate the hashed value.
     * @param oldChar Character to remove from the hashed text bucket.
     * @param newChar Character to add to the hashed text bucket.
     * @return The updated text bucket hash value for the Rabin-Karp algorithm.
     */
    private static int updateRabinKarpHash (int oldHash, int length, int primeNumber, char oldChar, char newChar) {
        return (oldHash - oldChar * ((int) Math.pow(primeNumber, length - 1))) * primeNumber + newChar;
    }

    /**
     * Removes occurrences of the pattern within the text.
     *
     * @param pattern The pattern {@link String} to search for.
     * @param text The text {@link String} to search for the pattern within.
     * @param primeNumber A prime number that may used to calculate the hashed value.
     * @return The text {@link String} with the pattern {@link String} removed.
     */
    public static String removePattern (String pattern, String text, int primeNumber) {

        // search for occurrences of the text within the pattern using Rabin-Karp
        ArrayList<Integer> indices = search(pattern, text, primeNumber);

        if (!DataFunctions.isEmptyCollection(indices)) {
            int i = 0;
            for (int index : indices) {
                if (i == 0) {
                    text = text.substring(0, index) + text.substring(index + pattern.length());
                }
                else {
                    // shift the index to account for the size of the text being changed
                    index = index - (i * pattern.length());
                    text = text.substring(0, index) + text.substring(index + pattern.length());
                }
                i++;
            }
        }

        return text;
    }

}