package com.mmhayes.common.utils;

//import com.mmhayes.gateway.common.IQCSessionManager;
import redstone.xmlrpc.handlers.ReflectiveInvocationHandler;
import java.util.ArrayList;
import java.util.StringTokenizer;
import com.mmhayes.common.dataaccess.DataManager;

/**
 * <p>Title: QuickCharge 4</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: MMHayes</p>
 * @author smcgarvey
 * @version 4.0
 */


//public class MMHSessionManager extends ReflectiveInvocationHandler implements IQCSessionManager{
public class MMHSessionManager extends ReflectiveInvocationHandler {
  private static ArrayList sessionInfo = new ArrayList();
  DataManager dm;

  public MMHSessionManager() {
    dm = new DataManager();
  }

  public int setSelectedEmployees(int qcSessionId, String eids) {
    ArrayList row = new ArrayList();
    row.add("" + qcSessionId);
    row.add(eids);
    sessionInfo.add(row);
    //currentSession = "qcSessionID=" + qcSessionID + "; eids=" + eids;
    return 1;
  }

  public String getSelectedEmployees(int qcSessionId) {
    ArrayList row;
    int index = sessionInfo.size() - 1;
    boolean found = false;
    String thisSessionId = "";
    while ((found == false) & (index >= 0)) {
      row = (ArrayList)sessionInfo.get(index);
      thisSessionId = (String)row.get(0);
      if (thisSessionId.compareTo("" + qcSessionId) == 0) {
        found = true;
        return (String)row.get(1);
      } else {
        index--;
      }
    }
    return "";
  }

  public int getSelectedEmpCount(int qcSessionId) {
    String empids = getSelectedEmployees(qcSessionId);
    int res = 0;
    if (empids.length() > 0) {
      StringTokenizer t = new StringTokenizer(empids, ",", false);
      while (t.hasMoreTokens()) {
        res++;
        t.nextToken();
      }
    }
    return res;
  }
}