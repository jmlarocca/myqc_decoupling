package com.mmhayes.common.utils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-20 13:10:34 -0400 (Tue, 20 Apr 2021) $: Date of last commit
    $Rev: 13819 $: Revision of last commit
    Notes: Class which houses methods that will return elements to a MMHPool.
*/

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;

public class MMHPoolElementReturner<E> implements Callable<Void> {

    // private member variables
    private BlockingQueue<E> queue;
    private E element;

    /**
     * <p>Constructor for a {@link MMHPoolElementReturner}.</p>
     *
     * @param queue The {@link BlockingQueue} to return the element to.
     * @param element The element to return to the {@link BlockingQueue};
     */
    public MMHPoolElementReturner (BlockingQueue<E> queue, E element) {
        this.queue = queue;
        this.element = element;
    }

    /**
     * <p>Computes a result, or throws an exception if unable to do so.</p>
     *
     * @return A {@link Void} object.
     * @throws Exception
     */
    @Override
    public Void call () throws Exception {

        while (true) {
            try {
                queue.put(element);
                break;
            }
            catch (InterruptedException e) {
                Logger.logException(e);
                Thread.currentThread().interrupt();
            }
        }

        return null;
    }

}