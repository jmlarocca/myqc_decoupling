package com.mmhayes.common.utils;

/*
 $Author: rawithers $: Author of last commit
 $Date: 2021-03-08 16:10:58 -0500 (Mon, 08 Mar 2021) $: Date of last commit
 $Rev: 13597 $: Revision of last commit
 Notes: Context determined at servlet start-up but used in many places
*/

import javax.servlet.ServletContext;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

public class MMHServletStarter {

    //all variables are static
    private static String instanceIdentifier = "";
    private static String instancePropertyLocation = "";
    private static String instanceCommonPropertyLocation = "";
    private static String applicationLogLocation = "";
    private static String applicationIdentifier = "";
    private static String applicationPropertyLocation = "";
    private static Boolean devEnv = false;

    public MMHServletStarter(ServletContext sc) {
        String appName = "", appPath = "", realPath="";
        try {
            if (sc.getRealPath("/") != null) {
                realPath = sc.getRealPath("/");
            }
            try {
                // Try to get the name of the application (not the instance)
                applicationIdentifier = sc.getServletContextName().toUpperCase();
            } finally {
                if (applicationIdentifier.length() < 1) {
                    applicationIdentifier = "MMH";
                }
            }
            //SC2P: Remove all path functions as obsolete
            // /-*
            try {
                // Try to get the path of the application properties within the instance
                if (sc.getRealPath("/") != null) {
                    applicationPropertyLocation = (realPath.replace("\\", "/") + "/properties").replace("//", "/");
                }
            } finally {
                if (applicationPropertyLocation.length() < 1) {
                    applicationPropertyLocation = "properties";
                }
            }
            // *-/
            try {
                //SC2P: Remove all path functions as obsolete
                // /-*
                // Try to get the name from the context path, but only if the server supports it (servlet spec >= 2.5)
                if ((sc.getMajorVersion() + Float.parseFloat("0." + Integer.toString(sc.getMinorVersion()))) >= 2.5) {
                    appName = sc.getContextPath().replace("/", "");
                    //this line can be removed if we want to load properties file from C:\MMHayes\config\{instance}\{instance}.properties
                    if (realPath.replaceAll("\\s", "").toUpperCase().contains(("IdeaProjects").toUpperCase())) {
                        devEnv = true;
                    }
                }
                if (appName.length() < 1) {
                    // If the name is empty so far, try and discern it from the file path
                    appName = new File(realPath).getName();
                }
                // *-/
            } finally {
                if (appName.length() < 1) {
                    // If the application name cannot be discerned (or it's a dev environment), use the descriptor in the web.xml to avoid hard-coding
                    appName = applicationIdentifier;
                }
                // Remove unnecessary expansions to the application name, then set it as a variable
                appName = appName.replaceAll("^tmp\\d+", "");
                appName = appName.replaceAll("\\-exp\\.war$", "");
                appName = appName.replaceAll("\\.war$", "");
                instanceIdentifier = appName;
            }
            try {
                // Try to get the home path, which should be in the environmental variable
                if (System.getenv("MMH_HOME") != null && System.getenv("MMH_HOME").length() > 0) {
                    appPath = System.getenv("MMH_HOME");
                }
                //SC2P: Remove all path functions as obsolete
                // /-*
                else {
                    String tempDrive = realPath.split("\\\\")[0];
                    File fm = new File(tempDrive + "/MMHayes");
                    File fk = new File(tempDrive + "/Kronos");
                    if (fm.isDirectory()) {
                        appPath = fm.getPath();
                    } else if (fk.isDirectory()) {
                        appPath = fk.getPath();
                    }
                }
                // *-/
            } finally {
                //SC2P: Remove all path functions as obsolete
                // /-*
                if (devEnv) {
                    instancePropertyLocation = (realPath.replace("\\", "/") + "/properties").replace("//", "/"); // Use the exploded war directory on dev environments
                    //Move site.networkConnCode and site.networkConnectionName to Common App.Properties File - C:\MMHayes\application\common\config
                    instanceCommonPropertyLocation = (realPath.replace("\\", "/") + "/properties").replace("//", "/"); // Use the exploded war directory on dev environments
                    applicationLogLocation = (realPath.replace("\\", "/") + "/logs").replace("//", "/");
                } else {
                    if (appPath.length() < 1) {
                        appPath = "C:/MMHayes";
                    }
                    instancePropertyLocation = (appPath.replace("\\", "/") + "/application/" + instanceIdentifier + "/config").replace("//", "/");

                    instanceCommonPropertyLocation = (appPath.replace("\\", "/") + "/application/common/config").replace("//", "/");
                    applicationLogLocation = (appPath.replace("\\", "/") + "/application/" + instanceIdentifier + "/Log Files Web").replace("//", "/");
                }
                // *-/
            }
        } catch (Exception ex) {
            System.out.println("ERROR: Could not initialize servlet. Due to exception: "+ex.getMessage());
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw); //allows stack trace as a string -> sw.toString()
            System.out.println("ERROR: Could not initialize servlet. Due to exception: "+sw.toString());
        }
    }

    public static String getInstanceIdentifier() {
        return instanceIdentifier;
    }
    public static String getInstancePropertyLocation() {
        return instancePropertyLocation;
    }
    public static String getInstanceCommonPropertyLocation() {
        return instanceCommonPropertyLocation;
    }

    public static String getApplicationLogLocation() {
        return applicationLogLocation;
    }
    public static String getApplicationIdentifier() {
        return applicationIdentifier;
    }
    public static String getApplicationPropertyLocation() {
        return applicationPropertyLocation;
    }
    public static Boolean isDevEnv() {
        return devEnv;
    }

}
