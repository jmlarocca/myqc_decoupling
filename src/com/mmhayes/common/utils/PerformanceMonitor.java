package com.mmhayes.common.utils;

import com.mmhayes.common.dataaccess.commonMMHFunctions;
import sun.security.jca.GetInstance;

import javax.management.*;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;

/**
 * $Author: ecdyer $: Author of last commit
 * $Date: 2017-02-09 17:04:46 -0500 (Thu, 09 Feb 2017) $: Date of last commit
 * $Rev: 20495 $: Revision of last commit
 * Notes: Performance Monitor to determine CPU Usage
 */
public class PerformanceMonitor {

    private static Method methodGetSystemCpuLoad;
    private static Method methodGetProcessCpuLoad;
    private static OperatingSystemMXBean operatingSystemMXBean;
    private static MBeanServer serverMXBean;
    private static MemoryMXBean memoryMXBean;

    private static long lastCheckTime = 0;
    private static long casheLifeTime = 10000;//10s
    private static double t_minus_1 = 0;
    private static double t_minus_2 = 0;
    private static double lastCheckValue = 0;

    static {
        try {
            operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();

            Method getSystemCpuLoad = operatingSystemMXBean.getClass().getMethod("getSystemCpuLoad");
            getSystemCpuLoad.setAccessible(true);
            setMethodGetSystemCpuLoad(getSystemCpuLoad);

            Method getProcessCpuLoad = operatingSystemMXBean.getClass().getMethod("getProcessCpuLoad");
            getProcessCpuLoad.setAccessible(true);
            setMethodGetProcessCpuLoad(getProcessCpuLoad);

            serverMXBean = ManagementFactory.getPlatformMBeanServer();
            memoryMXBean = ManagementFactory.getMemoryMXBean();



        } catch (NoSuchMethodException e) {
            Logger.logMessage("Error in PerformanceMonitor initializer: "+e.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(e);
        }
    }

    /**
     * Get the average of the last 3 checks to the system CPU
     * @return CPU usage in 0 to 1 decimal format
     */
    public static double getRecentAverageSystemCPU(){
        getSystemCPU();
        return (lastCheckValue + t_minus_1 + t_minus_2) / 3.0;
    }

    /**
     * store the last CPU value
     * @param cpu
     */
    private static void setCPU(double cpu){
        t_minus_2 = t_minus_1;
        t_minus_1 = lastCheckValue;
        lastCheckValue = cpu;
    }

    public static double getSystemCPU() {
        long curTime = System.currentTimeMillis();
        double systemUsage = -1;

        try {
            if(curTime - lastCheckTime < casheLifeTime)
                systemUsage = lastCheckValue;
            else{
                Object value = getMethodGetSystemCpuLoad().invoke(operatingSystemMXBean);
                systemUsage = Double.valueOf(value.toString());
                if(systemUsage < 0)
                    Logger.logMessage("The CPU returned a negative value of " +systemUsage, Logger.LEVEL.DEBUG);
                setCPU(systemUsage);
            }
            lastCheckTime = curTime;
        } catch (Exception e) {
            Logger.logMessage("Error in method: getCpuUsage: "+e.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(e);
        }

        Logger.logMessage("getSystemCPU: "+Double.toString(systemUsage), Logger.LEVEL.DEBUG);
        return systemUsage;
    }

    public static double getProcessCPU() {

        double processUsage = -1;

        try {
            Object value = getMethodGetProcessCpuLoad().invoke(operatingSystemMXBean);
            processUsage = Double.valueOf(value.toString());
        } catch (Exception e) {
            Logger.logMessage("Error in method: getProcessCPU: "+e.getMessage(), Logger.LEVEL.ERROR);
            Logger.logException(e);
        }

        Logger.logMessage("getCpuUsage: "+Double.toString(processUsage), Logger.LEVEL.DEBUG);
        return processUsage;
    }

    /**
     * @return the number of bytes available to the application.
     */
    public static long getUsableSpace(String path){
        try{
            File f = new File(path);
            long usableSpace = f.getUsableSpace();
            return usableSpace;
        }catch (Exception e){
            Logger.logException(e);
        }
        return -1;
    }

    public static long getTotalSpace(String path){
        try{
            File f = new File(path);
            long totalSpace = f.getTotalSpace();
            return totalSpace;
        }catch (Exception e){
            Logger.logException(e);
        }
        return -1;
    }

    public static int getNumberOfSessions(){
        int numSessions = -1;
        String context = "/" + MMHServletStarter.getInstanceIdentifier();

        try {
            ObjectName objectName = new ObjectName("Catalina:type=Manager,context="+context+",host=localhost");
            Integer activeSessions = (Integer)serverMXBean.getAttribute(objectName, "activeSessions");
            numSessions = activeSessions;
        } catch (Exception e) {
            Logger.logException(e);
        }
        return numSessions;


    }

    public static long getMaximumHeapMemory(){
        return memoryMXBean.getHeapMemoryUsage().getMax();
    }

    public static long getUsedHeapMemory(){
        return memoryMXBean.getHeapMemoryUsage().getUsed();
    }

    public static long getMaximumNonHeapMemory(){
        return memoryMXBean.getNonHeapMemoryUsage().getMax();
    }

    public static long getUsedNonHeapMemory(){
        return memoryMXBean.getNonHeapMemoryUsage().getUsed();
    }

    public static Method getMethodGetSystemCpuLoad() {
        return methodGetSystemCpuLoad;
    }

    public static void setMethodGetSystemCpuLoad(Method methodGetSystemCpuLoad) {
        PerformanceMonitor.methodGetSystemCpuLoad = methodGetSystemCpuLoad;
    }

    public static Method getMethodGetProcessCpuLoad() {
        return methodGetProcessCpuLoad;
    }

    public static void setMethodGetProcessCpuLoad(Method methodGetProcessCpuLoad) {
        PerformanceMonitor.methodGetProcessCpuLoad = methodGetProcessCpuLoad;
    }
}
