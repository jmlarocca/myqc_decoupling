package com.mmhayes.common.utils;

import com.mmhayes.common.dataaccess.commonMMHFunctions;
import com.sun.jna.Library;
import com.sun.jna.Platform;

import java.io.File;
import java.util.Properties;

/**
 * Created by ecdyer on 7/16/2021.
 */

public class MMHCommonItemsLibrary {

    private static final String JNA_LIB_PATH = "jna.library.path";
    private static final String DLL_NAME_32 = "QCPM32"; //Managed DLL 32
    private static final String DLL_NAME_64 = "QCPM64"; //Managed DLL 64
    private static final String DLL_UM_NAME_32 = "QCPMUM32"; //Unmanaged DLL 32
    private static final String DLL_UM_NAME_64 = "QCPMUM64"; //Unmanaged DLL 64
    private static final String DEV_DLL_PATH = "C:\\IdeaProjects\\Libraries\\QCPM\\";
    private static String INSTANCE_PATH;
    private static String INSTANCE_PATH_LOCAL;
    static Boolean useEnhancedEncryption = false;

    public interface MMHCommonItems extends Library {

        String Encrypt(String stringToEncrypt);
        String Decrypt(String stringToDecrypt);

    }

    public static void setJnaLibraryPath(){

        INSTANCE_PATH = commonMMHFunctions.getMMH_Home() + commonMMHFunctions.PATH_APPLICATION + File.separator + "qc" + File.separator;
        INSTANCE_PATH_LOCAL = commonMMHFunctions.getMMH_Home() + commonMMHFunctions.PATH_APPLICATION + File.separator + commonMMHFunctions.getInstanceName() + File.separator;

        // Check for DLLS
        if(is64()){
            if(checkFileExists(DLL_NAME_64) && checkFileExists(DLL_UM_NAME_64)){
                Logger.logMessage("setJnaLibraryPath: Found 64 bit dll files.", Logger.LEVEL.TRACE);
                useEnhancedEncryption = true;
            }
        } else {
            if(checkFileExists(DLL_NAME_32) && checkFileExists(DLL_UM_NAME_32)){
                Logger.logMessage("setJnaLibraryPath: Found 32 bit dll files.", Logger.LEVEL.TRACE);
                useEnhancedEncryption = true;
            }
        }

        if(!useEnhancedEncryption){
            Logger.logMessage("No QCPM DLL files found. Falling back to legacy encryption.", Logger.LEVEL.TRACE);
            return;
        }

        if(MMHServletStarter.isDevEnv()){
            Properties systemProperties = System.getProperties();
            systemProperties.setProperty(JNA_LIB_PATH,DEV_DLL_PATH);
            Logger.logMessage("Set: "+JNA_LIB_PATH+" to: "+DEV_DLL_PATH, Logger.LEVEL.TRACE);
        } else {

            Properties systemProperties = System.getProperties();
            systemProperties.setProperty(JNA_LIB_PATH, INSTANCE_PATH );
            Logger.logMessage("Set: "+JNA_LIB_PATH+" to: "+INSTANCE_PATH, Logger.LEVEL.TRACE);
        }
    }

    public static void setJnaLibraryPathTest(){
        Properties systemProperties = System.getProperties();
        systemProperties.setProperty(JNA_LIB_PATH, MMHCommonItemsLibrary.DEV_DLL_PATH);
        Logger.logMessage("Set: "+JNA_LIB_PATH+" to: "+DEV_DLL_PATH, Logger.LEVEL.TRACE);
    }

    private static boolean checkFileExists(String fileName){

        boolean fileExists = false;

        try {

            if(MMHServletStarter.isDevEnv()){
                File file = new File(DEV_DLL_PATH+fileName+".dll");
                if(file.exists() && !file.isDirectory()) {
                    fileExists = true;
                    Logger.logMessage("Found: "+file.getAbsolutePath(), Logger.LEVEL.DEBUG);
                }
            } else {
                // CHECK FOR DLL in application\qc
                File file = new File(INSTANCE_PATH+fileName+".dll");
                File file_local = new File(INSTANCE_PATH_LOCAL+fileName+".dll");
                if(file.exists() && !file.isDirectory()) {
                    fileExists = true;
                    Logger.logMessage("Found: "+file.getAbsolutePath(), Logger.LEVEL.DEBUG);
                } else if(file_local.exists() && !file_local.isDirectory()){
                    // CHECK for DLL in application\warname ie (application\pos)
                    fileExists = true;
                    Logger.logMessage("Found: "+file_local.getAbsolutePath(), Logger.LEVEL.DEBUG);
                }
            }
        } catch(Exception ex){
            Logger.logMessage("Error in checkFileExists(): "+ex.getLocalizedMessage());
            Logger.logException(ex);
        }

        return fileExists;

    }

    public static boolean is64(){

        // Method checks if JVM is 64bit
        boolean isJVM64bit = false;

        if(Platform.is64Bit()){
            isJVM64bit = true;
        }
        Logger.logMessage("is64(): "+Boolean.toString(isJVM64bit), Logger.LEVEL.LUDICROUS);

        return isJVM64bit;
    }

    public static String getDLL(){

        String dllName = is64() ? DLL_UM_NAME_64 : DLL_UM_NAME_32;
        Logger.logMessage("getDLL: "+dllName, Logger.LEVEL.DEBUG);

        return dllName;

    }

}