package com.mmhayes.common.utils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-20 13:10:34 -0400 (Tue, 20 Apr 2021) $: Date of last commit
    $Rev: 13819 $: Revision of last commit
    Notes: Creates instances of a MMHBoundedBlockingPool or MMHBoundedNonBlockingPool.
*/

public class MMHBoundedPoolFactory {

    /**
     * <p>Private and empty constructor for a {@link MMHBoundedPoolFactory}.</p>
     *
     */
    private MMHBoundedPoolFactory () {}

    /**
     * <p>Creates a new {@link MMHBoundedBlockingPool}.</p>
     *
     * @param poolSize The number of elements to have within the {@link MMHPool}.
     * @param validator The {@link MMHPool.Validator} to use to validate the elements within the {@link MMHPool}.
     * @param factory The {@link MMHObjectFactory} to use to create elements within the {@link MMHPool}.
     * @return The newly created {@link MMHBoundedBlockingPool} instance.
     */
    public static <T> MMHPool<T> newMMHBoundedBlockingPool (int poolSize, MMHPool.Validator<T> validator, MMHObjectFactory<T> factory) {
        return new MMHBoundedBlockingPool<>(poolSize, validator, factory);
    }

    /**
     * <p>Creates a new {@link MMHBoundedNonBlockingPool}.</p>
     *
     * @param poolSize The number of elements to have within the {@link MMHPool}.
     * @param validator The {@link MMHPool.Validator} to use to validate the elements within the {@link MMHPool}.
     * @param factory The {@link MMHObjectFactory} to use to create elements within the {@link MMHPool}.
     * @return The newly created {@link MMHBoundedNonBlockingPool} instance.
     */
    public static <T> MMHPool<T> newMMHBoundedNonBlockingPool (int poolSize, MMHPool.Validator<T> validator, MMHObjectFactory<T> factory) {
        return new MMHBoundedNonBlockingPool<>(poolSize, validator, factory);
    }

}