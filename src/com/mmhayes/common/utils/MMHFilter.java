package com.mmhayes.common.utils;

//other dependencies

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/*
 MMHayes Common Filter
 
 Created with IntelliJ IDEA.
 User: jrmitaly
 Date: 10/18/14
 Time: 2:47 PM
 
 Last Updated (automatically updated by SVN)
 $Author: ijgerstein $: Author of last commit
 $Date: 2021-08-02 15:52:31 -0400 (Mon, 02 Aug 2021) $: Date of last commit
 $Rev: 14647 $: Revision of last commit

 Notes: Handles filtering of ALL requests
*/

public class MMHFilter implements Filter {

    private static SecureRandom random = new SecureRandom(); //static SecureRandom object for random int generation
    private static Boolean checkForDelay = true; //should this class check the properties for a delayFilter?
    private static Boolean delayAllRequests = null; //are we delaying all requests?
    private static String delayFilter = null; //if we aren't delaying all requests, what requests should we delay?
    private static Integer minRangeDelaySeconds = null; //what's the min range for the random delay?
    private static Integer maxRangeDelaySeconds = null; //what's the max range for the random delay?

    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        //cache control for response
        /*
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        */

        String contextPath = request.getContextPath(); //context of request
        String uri = request.getRequestURI(); //url of request
        String params = request.getQueryString(); //get the params (if any) for the request

        //delays requests based on app.property settings
        delayRequest(uri);

        //set cache headers
        if (uri.contains("/images/") || uri.contains("/MMHAccess/FileHandler")) {
            Calendar expireCalendar = Calendar.getInstance();
            int hoursUntilExpires = 24;
            int ageExpires = hoursUntilExpires * 60 * 60;
            expireCalendar.add(Calendar.HOUR, hoursUntilExpires);
            response.setHeader("Cache-Control", "public, max-age=" + ageExpires);
            response.setDateHeader("Expires", expireCalendar.getTimeInMillis());
        } else {
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Expires", "-1");
        }

        //set security headers in the response

        //Protection against Type 1 Reflected XSS attacks
        response.setHeader("X-XSS-Protection", "1; mode=block");

        //Disabling browsers to perform risky mime sniffing
        response.setHeader("X-Content-Type-Options", "nosniff");

        //defends against "click jacking"
        if(!uri.contains("/QuickCharge/WebFileTransfer/WebFileDownloader")) {
            response.setHeader("X-Frame-Options", "DENY");
        }

        //Additional Security Headers
        response.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
        response.setHeader("Content-Security-Policy", "default-src 'self' https://*.mmhayes.com https://*.stripe.com https://*.mmhcloud.com https://*.googleapis.com https://www.google-analytics.com; img-src 'self' data: https:; script-src 'self' 'unsafe-eval' 'unsafe-inline' https://www.gstatic.com https://www.googletagmanager.com; style-src 'self' 'unsafe-inline'; font-src 'self' data: ; form-action 'self' https://*.stripe.com https://*.mmhcloud.com;");
        response.setHeader("Referrer-Policy", "no-referrer");
        response.setHeader("Feature-Policy", "accelerometer 'none'; geolocation 'none'; gyroscope 'none'; magnetometer 'none'; microphone 'none'; usb 'none'");

        //if this request is pointing to the old structure "/html/main"
        //AND this request contains the ".jsp" extension
        if (uri.contains("/html/main/") && uri.contains(".jsp")) {
            //get the path without .jsp and without "html/main"
            String path = uri.substring(0, uri.lastIndexOf(".jsp")).replace("html/main/","");

            //grab parameters and set to path
            if (params != null && params.length() > 0) {
                path += "?" + params;
            }

            //redirect to newly created path
            response.sendRedirect(path);
        } else {
            //if this request is not hitting an api
            //AND this request is not hitting a QuickCharge servlet
            //AND this request is not hitting a MMHAccess servlet
            //AND this request is not hitting ANY JSON-RPC servlet
            //AND this request is not hitting an Axis Soap end point - "/services/"
            //AND this request is not hitting any SSO end points - "/sso/"
            //AND this request does not contain an extension (unless it's .jsp)
            //AND this request does not end in backslash
            //AND this request greater than the length of this context path
            if (!uri.contains("/api")
                    && (!uri.contains("/QuickCharge"))
                    && (!uri.contains("/MMHAccess"))
                    && (!uri.contains("/JSON-RPC"))
                    && (!uri.contains("/websocket/"))
                    && (!uri.contains("/services/"))
                    && (!uri.contains("/sso"))
                    && ((uri.endsWith(".jsp")) || (!uri.contains(".")))
                    && (!uri.endsWith("/"))
                    && (uri.length() > contextPath.length())
                    ) {

                //if the request is for the direct jsp page, then redirect them to the page without the jsp
                if (uri.endsWith(".jsp")) {

                    //get the path without .jsp
                    String path = uri.substring(0, uri.lastIndexOf(".jsp"));

                    //grab parameters and set to path
                    if (params != null && params.length() > 0) {
                        path += "?" + params;
                    }

                    //redirect to newly created path
                    response.sendRedirect(path);

                    //if the request does not contain .jsp, then append it
                } else {

                    //get the path without the context information
                    String path = uri.substring(contextPath.length());

                    //append jsp and forward the user to the jsp page
                    req.getRequestDispatcher(path + ".jsp").forward(req, res);
                }
            } else {
                //if this request is not hitting the POSAPIManager or XmlRpcManager
                if(uri.contains("/websocket/")){
                    chain.doFilter(request, response);
                }
                else if (uri.contains("/QuickCharge/POSApiManager") || uri.contains("/QuickCharge/XmlRpcManager") ) {
                    chain.doFilter(request, response);
                } else {
                    if(request.getHeader("X-MMHayes-RequestId") != null) {
                        response.setHeader("X-MMHayes-RequestId", request.getHeader("X-MMHayes-RequestId"));
                    }
                    chain.doFilter(new MMHServletRequestWrapper(request), response);
                }

            }
        }
    }

    @Override
    public void destroy() {
    }


    /* delayRequest - delays request based on site.delay settings in the app.properties file - by jrmitaly 6/30/2015
    */
    private void delayRequest(String uri) {
        try {
            if (getCheckForDelay()) { //if we should be checking then let's do it!

                //initialize getDelayFilter if it's null
                if (getDelayFilter() == null) {
                    setDelayFilter(MMHProperties.getAppSetting("site.delay.request.filter")); //pull the delay request filter right out of app.properties
                }

                if (getDelayFilter() != null && !getDelayFilter().isEmpty()) { //invalid delay filter - better not do anything!

                    //initialize getMinRangeDelaySeconds if it's null
                    if (getMinRangeDelaySeconds() == null) {
                        setMinRangeDelaySeconds(MMHProperties.getAppSetting("site.delay.min.range.seconds"));
                    }

                    //initialize getMaxRangeSeconds if it's null
                    if (getMaxRangeDelaySeconds() == null) {
                        setMaxRangeDelaySeconds(MMHProperties.getAppSetting("site.delay.max.range.seconds"));
                    }

                    //initialize getDelayAllRequests if it's null
                    if (getDelayAllRequests() == null) {

                        //if site.delay.request.filter prop contains ALL or * then delay all requests
                        if (getDelayFilter().equals("ALL") || getDelayFilter().equals("*")) {
                            setDelayAllRequests(true);
                        } else {
                            setDelayAllRequests(false);
                        }
                    }

                    //if we are filtering all requests then go right ahead, if not then the uri has to contain the request filter
                    if (getDelayAllRequests() || uri.contains(getDelayFilter())) {
                        try {
                            if (getMinRangeDelaySeconds() > getMaxRangeDelaySeconds()) { //if the min is greater than the max
                                System.out.println("ERROR: Min range greater than max range in MMHFilter!");
                                setCheckForDelay(false); //stop checking as there are problems
                            } else if (getMinRangeDelaySeconds().equals(getMaxRangeDelaySeconds())) { //if min and max equal each other...
                                //sleep for exactly minRangeDelaySeconds
                                TimeUnit.SECONDS.sleep(minRangeDelaySeconds);
                            } else {
                                //sleep between minRangeDelaySeconds and maxRangeDelaySeconds
                                TimeUnit.SECONDS.sleep(randomInt(minRangeDelaySeconds, maxRangeDelaySeconds));
                            }
                        } catch (InterruptedException ex) {
                            System.out.println("ERROR: Sleep InterruptedException caught in MMHFilter!");
                            ex.printStackTrace();
                            setCheckForDelay(false); //stop checking as there are problems
                        }
                    }
                } else { //don't bother checking anymore as there is no site.delay.request.filter set
                    setCheckForDelay(false);
                }
            }
        } catch (Exception ex) {
            System.out.println("ERROR: General exception caught in MMHFilter.delayRequest");
            ex.printStackTrace();
            setCheckForDelay(false); //stop checking as there are problems
        }
    }


    /* randomInteger - returns a "random" int between the min and max params passed in
    */
    private static int randomInt(int min, int max) {
        return random.nextInt((max - min) + 1) + min;
    }

//START - getters and setters

    public Boolean getCheckForDelay() {
        return checkForDelay;
    }

    public void setCheckForDelay(Boolean checkForDelay) {
        this.checkForDelay = checkForDelay;
    }

    public Boolean getDelayAllRequests() {
        return delayAllRequests;
    }

    public void setDelayAllRequests(Boolean delayAllRequests) {
        this.delayAllRequests = delayAllRequests;
    }

    public String getDelayFilter() {
        return delayFilter;
    }

    public void setDelayFilter(String delayFilter) {
        this.delayFilter = delayFilter;
    }

    public Integer getMinRangeDelaySeconds() {
        return minRangeDelaySeconds;
    }

    public void setMinRangeDelaySeconds(String minRangeDelaySecondsProp) {
        try {
            if (minRangeDelaySecondsProp == null ||  minRangeDelaySecondsProp.isEmpty()) {
                this.minRangeDelaySeconds = 0; //defaults to 0
            } else {
                this.minRangeDelaySeconds = Integer.parseInt(minRangeDelaySecondsProp);
            }
        } catch (Exception ex) {
            System.out.println("ERROR: General exception caught in MMHFilter.setMinRangeDelaySeconds");
            this.minRangeDelaySeconds = 0; //defaults to 0
        }
    }

    public Integer getMaxRangeDelaySeconds() {
        return maxRangeDelaySeconds;
    }

    public void setMaxRangeDelaySeconds(String maxRangeDelaySecondsProp) {
        try {
            if (maxRangeDelaySecondsProp == null ||  maxRangeDelaySecondsProp.isEmpty()) {
                this.maxRangeDelaySeconds = 0; //defaults to 0
            } else {
                this.maxRangeDelaySeconds = Integer.parseInt(maxRangeDelaySecondsProp);
            }
        } catch (Exception ex) {
            System.out.println("ERROR: General exception caught in MMHFilter.setMaxRangeDelaySeconds");
            this.maxRangeDelaySeconds = 0; //defaults to 0
        }
    }

//END - getters and setters

}
