package com.mmhayes.common.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 Last Updated (automatically updated by SVN)
 $Author: jrmitaly $: Author of last commit
 $Date: 2015-04-30 09:16:16 -0400 (Thu, 30 Apr 2015) $: Date of last commit
 $Rev: 1538 $: Revision of last commit
 Notes: Implements Rijndael algorithm, depends on implementation in Rfc2898DeriveBytes class
        Creates an encrypted key that is 2 * (# of characters rounded up to the nearest 16)
        Max 112 characters (US-ASCII) creates a maximum encrypted length of 224 characters
        Produces the same encrypted string as the .Net version of MMHPassword
 */

public class MMHPassword {

    private static final String PasswordHash = "~57O6i8E7~";
    private static final String SaltKey = "%Kr0n8s%";
    private static final String VIKey = "@6178903232@mMh@";
    private static final String PADDING = "AES/CBC/NoPadding";
    private static final int BLOCK_SIZE = 16;
    private static final int KEY_SIZE = 256;
    private static final int ITERATIONS = 1000;
    private static final String INTERNAL_CHARSET = "US-ASCII";
    private static final String IO_CHARSET = "UTF-8";

    // Create an encrypted string representing a password that is decryptable
    public static String EncryptTwoWayPassword(String plainText) {
        if (plainText.length() > 0) {
            try {
                // Create and initialize encryption cipher mechanism for encryption
                Cipher cipher = Cipher.getInstance(PADDING);
                cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(getKeyBytes(), "AES"), new IvParameterSpec(VIKey.getBytes(Charset.forName(INTERNAL_CHARSET))));
                // Create the encrypted byte string using the cipher and return the string representation as hex
                return DatatypeConverter.printHexBinary(cipher.doFinal(ZeroPadByteArray(plainText.getBytes(IO_CHARSET))));
            } catch (Exception e) {
                throw new UnsupportedOperationException("Error encrypting password\n" + e.getMessage());
            }
        } else {
            throw new IllegalArgumentException ("Invalid password provided");
        }
    }

    // Derive the original password from an encrypted string by decryption
    public static String DecryptTwoWayPassword(String encryptedText) {
        if ((encryptedText.length() > 0) && ((encryptedText.length() % 2) == 0)) {
            try {
                byte[] cipherTextBytes = DatatypeConverter.parseHexBinary(encryptedText);
                // Create and initialize encryption cipher mechanism for decryption
                Cipher cipher = Cipher.getInstance(PADDING);
                cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(getKeyBytes(), "AES"), new IvParameterSpec(VIKey.getBytes(Charset.forName(INTERNAL_CHARSET))));
                // Get the decrypted byte string using the cipher and return that string without null unicode characters at the end
                return new String(cipher.doFinal(cipherTextBytes), Charset.forName(IO_CHARSET)).replaceAll("\\u0000","");
            } catch (Exception e) {
                throw new UnsupportedOperationException("Error decrypting password: " + encryptedText + "\n" + e.getMessage());
            }
        } else {
            throw new IllegalArgumentException ("Invalid encrypted password length");
        }
    }

    // Create the salt key using the Rfc2898DeriveBytes method and the static variables of this class
    private static byte[] getKeyBytes() {
        try {
            return new Rfc2898DeriveBytes(PasswordHash.getBytes(Charset.forName(INTERNAL_CHARSET)), SaltKey.getBytes(Charset.forName(INTERNAL_CHARSET)), ITERATIONS).getBytes(KEY_SIZE / 8);
        } catch (Exception e) {
            throw new UnsupportedOperationException("Error creating salt key\n" + e.getMessage());
        }
    }

    // Pads a byte array with zeros to the next byte block size
    private static byte[] ZeroPadByteArray(byte[] arr) throws IllegalArgumentException {
        return (((arr.length % BLOCK_SIZE) == 0) ? arr : (Arrays.copyOf(arr, (((arr.length / BLOCK_SIZE) + 1) * BLOCK_SIZE))));
    }

}
