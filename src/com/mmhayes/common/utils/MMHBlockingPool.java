package com.mmhayes.common.utils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2021-04-20 13:10:34 -0400 (Tue, 20 Apr 2021) $: Date of last commit
    $Rev: 13819 $: Revision of last commit
    Notes: Extension of the MMHPool interface that will allow for a timeout when trying to get an element from the MMHPool.
*/

import java.util.concurrent.TimeUnit;

public interface MMHBlockingPool<T> extends MMHPool<T> {

    /**
     * <p>Returns an instance from the {@link MMHPool}.</p>
     *
     * @return The instance retrieved from the {@link MMHPool}.
     */
    T get();

    /**
     * <p>Returns an instance from the {@link MMHPool}.</p>
     *
     * @param time The amount of time to block until a new instance is taken from the {@link MMHPool}.
     * @param timeUnit The {@link TimeUnit} used to interpret the time argument.
     * @return The instance retrieved from the {@link MMHPool}.
     * @throws InterruptedException
     */
    T get(long time, TimeUnit timeUnit) throws InterruptedException;

}