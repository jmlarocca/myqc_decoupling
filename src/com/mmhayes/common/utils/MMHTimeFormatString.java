package com.mmhayes.common.utils;

/*
    Last Updated (automatically updated by SVN)
    $Author: jkflanagan $: Author of last commit
    $Date: 2020-10-01 09:40:30 -0400 (Thu, 01 Oct 2020) $: Date of last commit
    $Rev: 12775 $: Revision of last commit
    Notes: Contains various time formatting Strings.
*/

/**
 * <p>Contains various time formatting Strings.</p>
 *
 */
public class MMHTimeFormatString {

    public static final String MO_DY_YR = "MM/dd/yyyy";
    public static final String MO_DY = "M/dd";
    public static final String HR_MIN_SEC_AMPM = "hh:mm:ss a";
    public static final String HR_MIN_AMPM = "h:mm a";
    public static final String YR_MO_DY_HR_MIN_SEC = "yyyy-MM-dd HH:mm:ss";
    public static final String YR_MO_DY_HR_MIN_SEC_MS = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String YR_MO_DY_HR_MIN_SEC_AMPM = "yyyy-MM-dd HH:mm:ss a";
    public static final String YR_MO_DY_SLASHES_HR_MIN_SEC_AMPM = "M/d/yyyy h:mm:ss a";

}