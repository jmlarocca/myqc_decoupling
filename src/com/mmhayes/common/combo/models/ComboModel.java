package com.mmhayes.common.combo.models;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.terminal.models.TerminalModel;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//API dependencies
//other dependencies

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2020-02-27 09:23:59 -0500 (Thu, 27 Feb 2020) $: Date of last commit
 $Rev: 10914 $: Revision of last commit
*/
public class ComboModel {
    private Integer id = null;
    private Integer paDiscountId = null;
    private BigDecimal originalAmount = BigDecimal.ZERO;

    private String name = "";

    private List<ComboDetailModel> details = new ArrayList<ComboDetailModel>();
    private ArrayList<Integer> detailIds = new ArrayList<>();
    static DataManager dm = new DataManager();

    public ComboModel() {

    }

    public ComboModel(Integer comboId, Integer terminalId) throws Exception {
        Object[] dataObj = new Object[]{
            comboId,
            terminalId
        };

        ArrayList<HashMap> comboList = dm.parameterizedExecuteQuery("data.combos.getComboModelById", dataObj, PosAPIHelper.getLogFileName(terminalId), true);

        if ( comboList == null || comboList.size() == 0 ) {
            Logger.logMessage("Combo discount is not mapped to the revenue center of the store", PosAPIHelper.getLogFileName(terminalId), Logger.LEVEL.TRACE);
            return;
        }

        setModelProperties( comboList.get(0), terminalId );
    }

    public static ComboModel createComboModel(HashMap comboHM, TerminalModel terminalModel) throws Exception {
        ComboModel comboModel = new ComboModel();
        comboModel.setModelProperties(comboHM, terminalModel.getId());
        return comboModel;
    }

    public void setModelProperties(HashMap modelDetailHM, Integer terminalId) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setPaDiscountId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PADISCOUNTID")));
        setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));

        retrieveDetails(terminalId);
    }

    public void retrieveDetails(Integer terminalId) throws Exception {
        if ( this.getId() == null ) {
            return;
        }

        Object[] dataObj = new Object[]{
            this.getId()
        };

        ArrayList<HashMap> comboList = dm.parameterizedExecuteQuery("data.combos.getComboDetailsByComboId", dataObj, PosAPIHelper.getLogFileName(terminalId), true);

        BigDecimal originalAmount = BigDecimal.ZERO;

        if(comboList != null && comboList.size() > 1) {
            for ( HashMap comboDetail : comboList ) {
                Integer comboDetailId = CommonAPI.convertModelDetailToInteger(comboDetail.get("PACOMBODETAILID"));
                ComboDetailModel comboDetailModel = new ComboDetailModel(comboDetailId, terminalId);
                this.getDetails().add(comboDetailModel);
                this.getDetailIds().add(comboDetailId);

                originalAmount = originalAmount.add(comboDetailModel.getComboPrice());
            }
        }

        setOriginalAmount(originalAmount);
    }

    public static ComboModel createComboModel(ComboModel comboModel) {
        ComboModel comboDisplayModel = new ComboModel();

        comboDisplayModel.setId(comboModel.getId());
        comboDisplayModel.setName(comboModel.getName());

        comboDisplayModel.setPaDiscountId(comboModel.getPaDiscountId());
        comboDisplayModel.setOriginalAmount(comboModel.getOriginalAmount());

        comboDisplayModel.setDetails(comboModel.getDetails());
        comboDisplayModel.setDetailIds(comboModel.getDetailIds());

        return comboModel;
    }

    public static ComboModel getComboModel(Integer comboId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> comboList = dm.parameterizedExecuteQuery("data.posapi30.getOneComboById",
                new Object[]{ comboId },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(comboList, CommonAPI.PosModelType.COMBO, "", terminalModel.getId());
        return ComboModel.createComboModel(comboList.get(0), terminalModel);

    }

    public static ComboModel getComboModel(String comboName, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> comboList = dm.parameterizedExecuteQuery("data.posapi30.getOneComboByName",
                new Object[]{ comboName },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(comboList, CommonAPI.PosModelType.COMBO, "", terminalModel.getId());
        return ComboModel.createComboModel(comboList.get(0), terminalModel);

    }

    public static ComboModel getComboModelActive(Integer comboId, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> comboList = dm.parameterizedExecuteQuery("data.posapi30.getOneComboByIdActive",
                new Object[]{ comboId, terminalModel.getRevenueCenterId() },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(comboList, CommonAPI.PosModelType.COMBO, "", terminalModel.getId());
        return ComboModel.createComboModel(comboList.get(0), terminalModel);

    }

    public static ComboModel getComboModelActive(String comboName, TerminalModel terminalModel) throws Exception {
        DataManager dm = new DataManager();

        //get all models in an array list
        ArrayList<HashMap> comboList = dm.parameterizedExecuteQuery("data.posapi30.getOneComboByNameActive",
                new Object[]{ comboName, terminalModel.getRevenueCenterId() },
                PosAPIHelper.getLogFileName(terminalModel.getId()),
                true
        );

        CommonAPI.checkIsNullOrEmptyList(comboList, CommonAPI.PosModelType.COMBO, "", terminalModel.getId());
        return ComboModel.createComboModel(comboList.get(0), terminalModel);

    }

    public void checkFoundDetails(ArrayList<Integer> foundComboDetailIds) {
        boolean foundAllIds = true;
        ArrayList<Integer> idsToRemove = new ArrayList<>();

        Integer index = 0;
        for( Integer comboDetailId : this.getDetailIds() ) {
            if( !foundComboDetailIds.contains(comboDetailId)) {
                foundAllIds = false;
            }
            idsToRemove.add(index);
            index ++;

        }

        if(foundAllIds) {
            //remove found ids from combo model
            for ( int i = idsToRemove.size() - 1; i >= 0; i-- ) {
                Integer indexToRemove = idsToRemove.get(i);
                foundComboDetailIds.remove( foundComboDetailIds.get( indexToRemove ) );
            }
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPaDiscountId() {
        return paDiscountId;
    }

    public void setPaDiscountId(Integer paDiscountId) {
        this.paDiscountId = paDiscountId;
    }

    public List<ComboDetailModel> getDetails() {
        return details;
    }

    public void setDetails(List<ComboDetailModel> details) {
        this.details = details;
    }

    public ArrayList<Integer> getDetailIds() {
        return detailIds;
    }

    public void setDetailIds(ArrayList<Integer> detailIds) {
        this.detailIds = detailIds;
    }

    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
    }
}
