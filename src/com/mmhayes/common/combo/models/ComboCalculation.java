package com.mmhayes.common.combo.models;

//mmhayes dependencies


import com.mmhayes.common.product.models.ProductCalculation;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//API dependencies
//other dependencies

/*
Last Updated (automatically updated by SVN)
$Author: gematuszyk $: Author of last commit
$Date: 2020-04-14 19:22:18 -0400 (Tue, 14 Apr 2020) $: Date of last commit
$Rev: 11365 $: Revision of last commit
*/

public class ComboCalculation {
    private Integer id; //combo id
    private ComboModel comboModel; //contains all of the combo properties
    private BigDecimal amount = null; //the total paid for this combo, considering all modifiers
    private BigDecimal quantity = BigDecimal.ZERO;
    private HashMap<Integer, ComboDetailModel> selectedProductsHM = null; //contains the selections for each comboDetail
    private boolean comboFilled = false;
    private boolean comboHasModifiers = false; //if there a modifiers set on any of the selected products, used when checking for duplicate productCalculations
    private boolean comboHasPrepOption = false; //if there is a prep option set on any of the selected products, used when checking for duplicate productCalculations
    private boolean validatedCombo = false; //if the combo is a manual combo
    private Integer temporaryComboLineIndex = null;
    private Integer lowestSelectProductIndex = 9999;
    private int roundingMode = BigDecimal.ROUND_HALF_UP;

    public ComboCalculation() throws Exception {

    }

    public ComboCalculation(ComboModel comboModel) throws Exception {
        setId(comboModel.getId());
        setComboModel(comboModel);

        HashMap<Integer, ComboDetailModel> detailHM = new HashMap<Integer, ComboDetailModel>();

        for ( ComboDetailModel comboDetailModel : comboModel.getDetails() ) {
            detailHM.put( comboDetailModel.getId(), null );
        }

        setSelectedProductsHM( detailHM );
    }

    public List<ComboDetailModel> attemptFillCombo(List<ComboDetailModel> potentialComboDetails) throws Exception {
        List<Integer> potentialComboDetailsToRemove = new ArrayList<Integer>();
        Integer detailsFilledCount = 0;

        for ( int i = 0; i < potentialComboDetails.size(); i++ ) {
            ComboDetailModel comboDetailModel = potentialComboDetails.get( i );

            //check if this detail belongs in this combo at all
            if ( !comboDetailModel.getComboId().equals( getComboModel().getId() ) || !getSelectedProductsHM().containsKey( comboDetailModel.getId() ) ) {
                continue;
            }

            //combo detail fits, but this detail is already filled
            if ( getSelectedProductsHM().get( comboDetailModel.getId() ) != null ) {
                continue;
            }

            //check the product calculation associated with this potential combo detail
            if ( comboDetailModel.getSelectedProduct().allQuantityInCombos() ) {
                continue;
            }

            detailsFilledCount++;

            //fill the detail slot with the selected product ID from the combo detail
            this.getSelectedProductsHM().put( comboDetailModel.getId(), comboDetailModel );

            if(comboDetailModel.getSelectedProduct().getModifiers().size() > 0) {
                this.setComboHasModifiers( true );
            }

            if(comboDetailModel.getSelectedProduct().getPrepOption() != null) {
                this.setComboHasPrepOption( true );
            }

            //check if the combo is filled now
            if ( detailsFilledCount == getSelectedProductsHM().size() ) {
                this.setComboFilled( true );
                this.setQuantity( BigDecimal.ONE );
                break;
            }
        }

        //combo is filled, remove the details it used from the potential list
        if ( this.isComboFilled() ) {
            Logger.logMessage("Combo " + this.getComboModel().getName() + " successfully filled with products:", Logger.LEVEL.TRACE);

            //try to find lowest quantity
            BigDecimal lowestQuantity = BigDecimal.ZERO;
            for ( Map.Entry<Integer, ComboDetailModel> entry : this.getSelectedProductsHM().entrySet() ) {
                ProductCalculation selectedProduct = entry.getValue().getSelectedProduct();
                selectedProduct.getComboOccurrences().add(this.getComboModel().getId());
                this.setQuantity(selectedProduct.getQuantity());

                if( lowestQuantity.compareTo(BigDecimal.ZERO) == 0 || selectedProduct.getQuantity().compareTo(lowestQuantity) < 0) {
                    lowestQuantity = selectedProduct.getQuantity();
                }

                Logger.logMessage("Product in combo - " + selectedProduct.getProductModel().getName(), Logger.LEVEL.TRACE);
            }

            this.setQuantity(lowestQuantity);

            for ( Map.Entry<Integer, ComboDetailModel> entry : this.getSelectedProductsHM().entrySet() ) {
                ProductCalculation selectedProduct = entry.getValue().getSelectedProduct();
                selectedProduct.setQuantityInCombos(lowestQuantity);
            }
        }

        return potentialComboDetails;
    }

    public void incrementQuantity() {
        this.setQuantity(this.getQuantity().add(BigDecimal.ONE));
    }

    //GETTERS AND SETTERS
    public int getRoundingMode() {
        return roundingMode;
    }

    public void setRoundingMode(int roundingMode) {
        this.roundingMode = roundingMode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public ComboModel getComboModel() {
        return comboModel;
    }

    public void setComboModel(ComboModel comboModel) {
        this.comboModel = comboModel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public HashMap<Integer, ComboDetailModel> getSelectedProductsHM() {
        return selectedProductsHM;
    }

    public void setSelectedProductsHM(HashMap<Integer, ComboDetailModel> selectedProductsHM) {
        this.selectedProductsHM = selectedProductsHM;
    }

    public boolean isComboFilled() {
        return comboFilled;
    }

    public void setComboFilled(boolean comboFilled) {
        this.comboFilled = comboFilled;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public Integer getTemporaryComboLineIndex() {
        return temporaryComboLineIndex;
    }

    public void setTemporaryComboLineIndex(Integer temporaryComboLineIndex) {
        this.temporaryComboLineIndex = temporaryComboLineIndex;
    }

    public Integer getLowestSelectProductIndex() {
        return lowestSelectProductIndex;
    }

    public void setLowestSelectProductIndex(Integer lowestSelectProductIndex) {
        this.lowestSelectProductIndex = lowestSelectProductIndex;
    }

    public boolean isComboHasModifiers() {
        return comboHasModifiers;
    }

    public void setComboHasModifiers(boolean comboHasModifiers) {
        this.comboHasModifiers = comboHasModifiers;
    }

    public boolean isComboHasPrepOption() {
        return comboHasPrepOption;
    }

    public void setComboHasPrepOption(boolean comboHasPrepOption) {
        this.comboHasPrepOption = comboHasPrepOption;
    }

    public boolean isValidatedCombo() {
        return validatedCombo;
    }

    public void setValidatedCombo(boolean validatedCombo) {
        this.validatedCombo = validatedCombo;
    }
}
