package com.mmhayes.common.combo.models;

//mmhayes dependencies

//API dependencies
//other dependencies

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.product.models.ModifierCalculation;
import com.mmhayes.common.product.models.ProductCalculation;
import com.mmhayes.common.transaction.models.ModifierLineItemModel;
import com.mmhayes.common.transaction.models.ProductLineItemModel;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-08-29 11:12:59 -0400 (Wed, 29 Aug 2018) $: Date of last commit
 $Rev: 7701 $: Revision of last commit
*/
public class ComboDetailModel {
    private Integer id = null;
    private Integer comboId = null;
    private Integer basePricePAPluId = null;
    private Integer keypadId = null;

    private BigDecimal basePrice = null;
    private BigDecimal comboPrice = null;
    private Integer sortOrder = null;

    static DataManager dm = new DataManager();

    @JsonIgnore
    private ProductCalculation selectedProduct = null;

    @JsonIgnore
    private List<String> keypadDetailPAPluIds = null;

    public ComboDetailModel() {

    }

    //for creating potential combo details, for figuring out which products go to which combo
    public ComboDetailModel(ComboDetailModel comboDetailModel, ProductCalculation product) throws Exception {
        setId( comboDetailModel.getId() );
        setComboId(comboDetailModel.getComboId());
        setKeypadId(comboDetailModel.getKeypadId());
        setBasePricePAPluId(comboDetailModel.getBasePricePAPluId());
        setBasePrice(comboDetailModel.getBasePrice());
        setComboPrice(comboDetailModel.getComboPrice());
        setSortOrder(comboDetailModel.getSortOrder());
        setKeypadDetailPAPluIds( comboDetailModel.getKeypadDetailPAPluIds() );
        setSelectedProduct( product );
    }

    public ComboDetailModel(Integer comboDetailId, Integer terminalId) throws Exception {
        Object[] dataObj = new Object[]{
            comboDetailId,
            terminalId
        };

        ArrayList<HashMap> comboDetailList = dm.parameterizedExecuteQuery("data.combos.getComboDetailById", dataObj, PosAPIHelper.getLogFileName(terminalId), true);

        CommonAPI.checkIsNullOrEmptyList( comboDetailList, "Combo Detail not found", terminalId );
        setModelProperties( comboDetailList.get(0) );
    }

    //sets keypadDetailPAPluIds which filters by the revenue center of the specified terminal
    public void setModelProperties(HashMap modelDetailHM) throws Exception {
        setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("ID")));
        setComboId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PACOMBOID")));
        setKeypadId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("PAKEYPADID")));
        setBasePricePAPluId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("BASEPRICEPAPLUID")));
        setBasePrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("BASEPRICE")));
        setComboPrice(CommonAPI.convertModelDetailToBigDecimal(modelDetailHM.get("COMBOPRICE")));
        setSortOrder(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("SORTORDER")));

        if ( CommonAPI.convertModelDetailToString(modelDetailHM.get("KEYPADPAPLUIDS")) != null ) {
            List<String> keypadDetailPAPluIds = Arrays.asList(modelDetailHM.get("KEYPADPAPLUIDS").toString().split(","));

            if ( keypadDetailPAPluIds.size() == 1 && keypadDetailPAPluIds.get(0).equals("") ) {
                Logger.logMessage("Combo Detail " + getId().toString() + " using keypad " + getKeypadId().toString() + " has no valid products");
            }

            setKeypadDetailPAPluIds(keypadDetailPAPluIds);
        }
    }

    public boolean isProductValid(ProductCalculation product) {
        if( !getKeypadDetailPAPluIds().contains(product.getId().toString()) ) {
            return false;
        }

        if( product.getProductModel().isWeighted() ) {
            return false;
        }

        // Check if the (Product price - Base price) + Combo price is greater than 0, if not then can't build combo
        return ((product.getLinePrice().subtract(getBasePrice())).add(getComboPrice())).compareTo(BigDecimal.ZERO) >= 0;
    }

    public boolean areModifiersValid(ProductLineItemModel productLineItemModel) {
        if(productLineItemModel.getModifiers().size() != this.getSelectedProduct().getModifiers().size()) {
            return false;
        }

        boolean foundAllModifiers = true;
        for(ModifierCalculation modifierCalculation : this.getSelectedProduct().getModifiers()) {

            boolean foundMod = false;
            for(ModifierLineItemModel modifierLineItemModel : productLineItemModel.getModifiers()) {

                if(modifierLineItemModel.getProduct().getId().equals(modifierCalculation.getId())) {
                    foundMod = true;
                    break;
                }
            }

            if(!foundMod) {
                foundAllModifiers = false;
                break;
            }
        }

        return  foundAllModifiers;
    }

    //checks if the the combo detail has already been found
    public boolean isDetailValid(List<ComboDetailModel> potentialComboDetails, ArrayList<Integer> foundComboDetailIds) {
        if(potentialComboDetails.size() == 0) {
            return true;
        }

        //if this combo detail is not in the foundComboDetailIds list then it's valid and need to break out of loop
        if( !foundComboDetailIds.contains(this.getId())) {
            return true;
        }

        return false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBasePricePAPluId() {
        return basePricePAPluId;
    }

    public void setBasePricePAPluId(Integer basePricePAPluId) {
        this.basePricePAPluId = basePricePAPluId;
    }

    public Integer getKeypadId() {
        return keypadId;
    }

    public void setKeypadId(Integer keypadId) {
        this.keypadId = keypadId;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public BigDecimal getComboPrice() {
        return comboPrice;
    }

    public void setComboPrice(BigDecimal comboPrice) {
        this.comboPrice = comboPrice;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Integer getComboId() {
        return comboId;
    }

    public void setComboId(Integer comboId) {
        this.comboId = comboId;
    }

    public List<String> getKeypadDetailPAPluIds() {
        return keypadDetailPAPluIds;
    }

    public void setKeypadDetailPAPluIds(List<String> keypadDetailPAPluIds) {
        this.keypadDetailPAPluIds = keypadDetailPAPluIds;
    }

    public ProductCalculation getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(ProductCalculation selectedProduct) {
        this.selectedProduct = selectedProduct;
    }
}
