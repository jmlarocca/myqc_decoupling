package com.mmhayes.common.combo.collections;

//mmhayes dependencies

import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.api.PosApi30.PosAPIHelper;
import com.mmhayes.common.api.PosApi30.PosAPIModelCache;
import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.combo.models.ComboCalculation;
import com.mmhayes.common.combo.models.ComboDetailModel;
import com.mmhayes.common.combo.models.ComboModel;
import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.product.collections.ProductCollectionCalculation;
import com.mmhayes.common.product.models.ProductCalculation;
import com.mmhayes.common.transaction.collections.ProductCollection;
import com.mmhayes.common.transaction.models.ComboLineItemModel;
import com.mmhayes.common.utils.Logger;

import java.math.BigDecimal;
import java.util.*;

//other dependencies

/*
Last Updated (automatically updated by SVN)
$Author: gematuszyk $: Author of last commit
$Date: 2020-04-14 19:22:18 -0400 (Tue, 14 Apr 2020) $: Date of last commit
$Rev: 11365 $: Revision of last commit
*/

public class ComboCollectionCalculation {
    DataManager dm = new DataManager();
    private List<ComboCalculation> collection = new ArrayList<ComboCalculation>();
    private List<ComboModel> combos = new ArrayList<ComboModel>();
    private HashMap<Integer, BigDecimal> comboProductEligibleCount = new HashMap<Integer, BigDecimal>();
    private List<ComboDetailModel> potentialComboDetails = new ArrayList<ComboDetailModel>();

    public ComboCollectionCalculation() {

    }

    //gets all ACTIVE combos assigned to the revenue center of the specified terminal
    public void retrieveValidCombos(Integer terminalId) throws Exception {
        Object[] dataObj = new Object[]{
            terminalId
        };

        ArrayList<HashMap> combosForRevCenter = dm.parameterizedExecuteQuery("data.combos.getValidCombosForRevCenter", dataObj, PosAPIHelper.getLogFileName(terminalId), true);

        if ( combosForRevCenter == null || combosForRevCenter.size() == 0 ) {
            Logger.logMessage("No combos available for this revenue center", Logger.LEVEL.TRACE);
            return;
        }

        HashMap<Integer, Object> globalComboCache = PosAPIModelCache.getObjectCache( terminalId, CommonAPI.PosModelType.COMBO );
        boolean setGlobalCache = (globalComboCache == null);

        //loop through and check cache, if not load it new
        for ( HashMap combo : combosForRevCenter ) {
            Integer comboId = CommonAPI.convertModelDetailToInteger(combo.get("PACOMBOID"));
            ComboModel validatedComboModel;

            //check if the model exists in the global cache
            if ( globalComboCache != null && globalComboCache.containsKey( comboId ) && globalComboCache.get( comboId ) != null ) {
                validatedComboModel = (ComboModel) globalComboCache.get( comboId );
            } else {
                //set flag to ensure the global cache gets set since at last 1 comboModel was not found there
                setGlobalCache = true;
                validatedComboModel = new ComboModel( comboId, terminalId );
            }

            if ( validatedComboModel.getDetails().size() == 0 ) {
                continue;
            }

            this.getCombos().add( validatedComboModel );
        }

        Collections.sort(this.getCombos(), new Comparator<ComboModel>() {
           public int compare(ComboModel list1, ComboModel list2) {
               return list2.getDetails().size() - list1.getDetails().size();
           }
        });

        if ( setGlobalCache ) {
            PosAPIModelCache.setComboCache(terminalId, CommonAPI.PosModelType.COMBO, this.getCombos());
        }
    }

    //if there are already products mapped to a combo, determine the combo and add it to the collection
    public void validateCurrentCombos(ProductCollectionCalculation productCollection) throws Exception {

        HashMap<Integer, List<ProductCalculation>> productsInCombos = new HashMap<>();

        //separate the products based on the combo they're mapped to using the comboTransLineItemId
        for ( ProductCalculation productCalculation : productCollection.getCollection() ) {

            if( productCalculation.getComboTransLineItemId() != null ) {

                List<ProductCalculation> comboProducts = new ArrayList<>();

                //if there is already a map using the comboTransLineItemId, add the product to it
                if(productsInCombos.containsKey(productCalculation.getComboTransLineItemId())) {
                    comboProducts = productsInCombos.get(productCalculation.getComboTransLineItemId());
                    comboProducts.add(productCalculation);
                } else {
                    comboProducts.add(productCalculation);
                }

                productsInCombos.put(productCalculation.getComboTransLineItemId(), comboProducts);
            }
        }

        //loop over each combo map, build the combo details using the products in the map
        for ( Map.Entry<Integer, List<ProductCalculation>> entry : productsInCombos.entrySet() ) {
            List<ProductCalculation> productDetails = entry.getValue();

            for( ComboModel comboModel : this.getCombos()) {

                List<ComboDetailModel> comboDetailModels = new ArrayList<>();
                BigDecimal possibleComboQuantity = BigDecimal.ZERO;

                for(ProductCalculation product : productDetails) {

                    for ( ComboDetailModel comboDetailModel : comboModel.getDetails() ) {

                        // Is the product a valid product for the combo detail
                        if ( !comboDetailModel.isProductValid( product ) ) {
                            continue;
                        }

                        // Does this product's quantity match the other product's quantity in the combo details
                        if ( possibleComboQuantity.compareTo(BigDecimal.ZERO) != 0 && possibleComboQuantity.compareTo(product.getQuantity()) != 0 ) {
                            continue;
                        }

                        ComboDetailModel newComboDetailModel = new ComboDetailModel( comboDetailModel, product );
                        newComboDetailModel.getSelectedProduct().setQuantityInCombos(BigDecimal.ZERO);
                        newComboDetailModel.getSelectedProduct().setComboTransLineItemId(null);

                        comboDetailModels.add(newComboDetailModel);

                        possibleComboQuantity = newComboDetailModel.getSelectedProduct().getQuantity();
                        break;
                    }
                }

                ComboCalculation comboCalculation = new ComboCalculation( comboModel );

                //try to fill the combo with the details, should work
                comboCalculation.attemptFillCombo( comboDetailModels );

                if ( !comboCalculation.isComboFilled() ) {
                    continue;
                }

                comboCalculation.setQuantity(possibleComboQuantity);

                //add to the list of combos
                this.addComboToCollection( comboCalculation );

                break;
            }

            Integer nextComboLineIndex = -1;

            //assign temporary combo line indexes to each combo
            for ( ComboCalculation combo : this.getCollection() ) {

                combo.setTemporaryComboLineIndex( nextComboLineIndex );

                //assign the products in this combo to the combo line index, splitting up product lines as necessary
                for ( Map.Entry<Integer, ComboDetailModel> comboEntry : combo.getSelectedProductsHM().entrySet()
                        ) {
                    ComboDetailModel comboDetail = comboEntry.getValue();
                    ProductCalculation selectedProduct = comboDetail.getSelectedProduct();

                    //check product and see if it has a comboLineIndex
                    if ( selectedProduct.getComboTransLineItemId() == null ) {

                        //if it doesn't then just set the index
                        selectedProduct.setComboTransLineItemId( nextComboLineIndex );

                        //update product with comboDetailId, basePrice, comboPrice, upcharge
                        selectedProduct.setComboDetailId( comboDetail.getId() );
                        selectedProduct.setBasePrice(comboDetail.getBasePrice());
                        selectedProduct.setComboPrice(comboDetail.getComboPrice());

                        BigDecimal currentPrice = selectedProduct.getCurrentPrice(true, true, null, null);
                        BigDecimal upcharge = currentPrice.subtract(comboDetail.getBasePrice().multiply(selectedProduct.getQuantity()));
                        if(upcharge.compareTo(BigDecimal.ZERO) != 0 && selectedProduct.getQuantity().compareTo(BigDecimal.ONE) > 0) {
                            upcharge = upcharge.divide(selectedProduct.getQuantity());
                        }

                        selectedProduct.setUpcharge( upcharge );
                    }
                }

                combo.setValidatedCombo(true);

                //decrement to next index
                nextComboLineIndex--;
            }
        }
    }

    public void determineProductCombos(ProductCollectionCalculation productCollection) throws Exception {
        Logger.logMessage("Determining Product Combos...", Logger.LEVEL.TRACE);

        List<ProductCalculation> itemDiscountProducts = new ArrayList<>();
        ArrayList<Integer> foundComboDetailIds = new ArrayList<>();

        //foreach product
        for ( ProductCalculation productCalculation : productCollection.getCollection() ) {

            boolean productEligibleForAnyCombo = false;
            BigDecimal eligibleProductQuantity = productCalculation.getQuantity();

            //check if the product has any item discount applied
            Integer itemDiscountCount = 0;

            if ( productCalculation.getDiscountsApplied().size() > 0 ) {
                boolean productHasItemDiscount = false;

                for ( HashMap discountApplied : productCalculation.getDiscountsApplied() ) {
                    if ( discountApplied.containsKey("isSingleQuantity") && (boolean) discountApplied.get("isSingleQuantity") ) {
                        productHasItemDiscount = true;
                        itemDiscountCount ++;
                    }
                }

                if ( productHasItemDiscount && itemDiscountCount == productCalculation.getQuantity().intValue() ) {
                    continue;
                }
            }

            //check if the product is already part of a combo
            if ( productCalculation.getComboTransLineItemId() != null ) {
                continue;
            }

            eligibleProductQuantity = eligibleProductQuantity.subtract(BigDecimal.valueOf(itemDiscountCount));

            //split the product quantity between the product's quantity with the item discounts and the product quantity without the discount
            if(productCalculation.getQuantity().compareTo(eligibleProductQuantity) != 0) {
                ProductCalculation splitProduct = new ProductCalculation( productCalculation );

                //set the original index to reference where this was split from
                splitProduct.setOriginalIndex( productCalculation.getOriginalIndex() );

                //set the quantity of the product to the item discount product's quantity, set discounts applied
                BigDecimal splitProductQuantity = productCalculation.getQuantity().subtract( eligibleProductQuantity );
                splitProduct.setQuantity( splitProductQuantity );
                splitProduct.setDiscountsApplied(productCalculation.getDiscountsApplied());

                //add item discount product to list to add at end of collection
                itemDiscountProducts.add(splitProduct);

                //set product to quantity without item discount product's quantity and remove discounts applied
                productCalculation.setQuantity(eligibleProductQuantity);
                productCalculation.setDiscountsApplied(new ArrayList<HashMap>());
            }

            //foreach combo
            for ( ComboModel comboModel : this.getCombos() ) {

                boolean productEligibleForThisCombo = false;

                //foreach combo detail
                for ( ComboDetailModel comboDetailModel : comboModel.getDetails() ) {

                    //set the selectedPAPluId on the details where the product is eligible, build list of eligible details
                    if ( !comboDetailModel.isProductValid( productCalculation ) ) {
                        continue;
                    }

                    productEligibleForThisCombo = true;

                    //set up a new comboDetailModel to use for determining products in combos
                    ComboDetailModel validComboDetailModel = new ComboDetailModel( comboDetailModel, productCalculation );

                    //add a separate detail for each quantity
                    BigDecimal addDetailsForMultipleQuantity = BigDecimal.ZERO;

                    do {
                        this.getPotentialComboDetails().add( validComboDetailModel );
                        addDetailsForMultipleQuantity = addDetailsForMultipleQuantity.add( BigDecimal.ONE );
                    } while (
                        addDetailsForMultipleQuantity.compareTo( productCalculation.getQuantity() ) < 0
                    );

                    // this is used if the same Product Keypad is assigned to multiple combo details in the combo
                    // Ex: you order Pepperoni Pizza and Cheese Pizza which are both on Cafe_Pizza keypad and Cafe_Pizza is assigned to both combo details
                    // this way only one potential combo detail will be created for Pepperoni Pizza and not two for when ComboCalculation.attemptFillCombo is called
                    if( validComboDetailModel.isDetailValid(this.getPotentialComboDetails(), foundComboDetailIds) ) {
                        foundComboDetailIds.add(validComboDetailModel.getId());
                        comboModel.checkFoundDetails(foundComboDetailIds);
                        break;
                    }
                }

                if ( productEligibleForThisCombo ) {
                    //if product is eligible for the combo, update a productComboCountHM<comboID, productEligibleCount>
                    this.incrementComboCount( comboModel.getId(), productCalculation.getQuantity() );
                    productEligibleForAnyCombo = true;
                }

            }

            //if the product is eligible for any combo, attempt to build combos, in order to preserve order
            if ( productEligibleForAnyCombo ) {
                this.attemptBuildCombos();
            }
        }

        //if there are no potential details then return
        if(this.getPotentialComboDetails().size() == 0) {
            return;
        }

        //add the products with item discounts to the end of the collection after splitting from original product
        for(ProductCalculation itemDiscountProductCalculation : itemDiscountProducts) {
            Integer lastIndex = productCollection.getCollection().get(productCollection.getCollection().size() - 1).getIndex() + 1;
            itemDiscountProductCalculation.setIndex(lastIndex);
            productCollection.getCollection().add(itemDiscountProductCalculation);
        }

        //if there are any combos, validate and update Products for Combos
        if ( this.getCollection().size() > 0 ) {
            this.updateProductsForCombos(productCollection);
        }
    }

    public void updateProductsForCombos(ProductCollectionCalculation productCollection) throws Exception {
        Logger.logMessage("Updating Products for Combos...", Logger.LEVEL.TRACE);

        Integer nextComboLineIndex = determineNextTemporaryComboLineIndex(productCollection);

        //assign temporary combo line indexes to each combo
        for ( ComboCalculation combo : this.getCollection() ) {

            if( !combo.isValidatedCombo() ) {

                combo.setTemporaryComboLineIndex( nextComboLineIndex );

                //assign the products in this combo to the combo line index, splitting up product lines as necessary
                for ( Map.Entry<Integer, ComboDetailModel> entry : combo.getSelectedProductsHM().entrySet() ) {
                    ComboDetailModel comboDetail = entry.getValue();
                    ProductCalculation selectedProduct = comboDetail.getSelectedProduct();

                    //check product and see if it has a comboLineIndex
                    if ( selectedProduct.getComboTransLineItemId() == null ) {
                        //if it doesn't then just set the index
                        selectedProduct.setComboTransLineItemId( nextComboLineIndex );

                        //if the product has a greater quantity than the quantity for this combo and all of the quantity of this product included in combos is for this combo, split the product line
                        if ( (combo.getQuantity().compareTo( selectedProduct.getComboOccurrences(combo.getId())) == 0 && selectedProduct.getQuantity().compareTo( combo.getQuantity() ) > 0)
                                || (selectedProduct.getQuantity().compareTo(combo.getQuantity()) > 0) ) {
                            //create new product
                            ProductCalculation splitProduct = new ProductCalculation( selectedProduct );

                            //set the original index to reference where this was split from
                            splitProduct.setOriginalIndex( selectedProduct.getOriginalIndex() );

                            //set new index to be beyond the previous highest index
                            Integer nextProductIndex = productCollection.determineNextProductIndex();
                            splitProduct.setIndex( nextProductIndex );

                            //set the new product line quantity to be the amount over the combo's quantity
                            BigDecimal splitProductQuantity = selectedProduct.getQuantity().subtract( combo.getQuantity() );
                            splitProduct.setQuantity( splitProductQuantity );
                            splitProduct.setComboTransLineItemId( null );

                            //if there are other combos using this product
                            if(selectedProduct.getComboOccurrences(combo.getId()).compareTo(BigDecimal.valueOf(selectedProduct.getComboOccurrences().size())) < 0) {

                                //get the number of other combos using this product
                                BigDecimal otherCombosUsingProductCount = selectedProduct.getOtherCombosUsingProductCount(combo.getId());

                                //the quantity will be the original product's quantity minus the occurrences of this product in this combo minus the number of other combos this product is used in
                                splitProductQuantity = selectedProduct.getQuantity().subtract(selectedProduct.getComboOccurrences(combo.getId())).subtract(otherCombosUsingProductCount);

                                //set the quantity to the number of number of combo occurrences
                                splitProduct.setQuantity( splitProductQuantity );
                                splitProduct.setQuantityInCombos(selectedProduct.getQuantityInCombos());
                                splitProduct.setComboOccurrences(selectedProduct.getComboOccurrences());

                                selectedProduct.setQuantityInCombos(selectedProduct.getComboOccurrences(combo.getId()));
                            }

                            if(splitProductQuantity.compareTo(BigDecimal.ZERO) > 0) {
                                //add the new product to the product list
                                productCollection.addToCollectionSplit( splitProduct, false );

                                //adjust the quantity of the original selected product
                                selectedProduct.setQuantity( selectedProduct.getQuantity().subtract( splitProductQuantity ) );

                            } else {
                                selectedProduct.setQuantity(selectedProduct.getComboOccurrences(combo.getId()));
                            }
                        }
                    } else {
                        //if it does, then split the line and point the comboDetail to the new product as the selectedProduct

                        //create new product
                        ProductCalculation splitProduct = new ProductCalculation( selectedProduct);
                        ProductCalculation selectProduct = new ProductCalculation(selectedProduct);

                        Integer originalIndex = selectedProduct.getIndex();

                        //set the original index to reference where this was split from
                        splitProduct.setOriginalIndex( selectProduct.getOriginalIndex() );

                        //set new index to be beyond the previous highest index
                        Integer nextProductIndex = productCollection.determineNextProductIndex();
                        splitProduct.setIndex( nextProductIndex );

                        //set the new product line quantity to be the amount over the combo's quantity
                        BigDecimal splitProductQuantity = selectedProduct.getQuantity().subtract( combo.getQuantity() );
                        splitProduct.setQuantity( splitProductQuantity );
                        splitProduct.setQuantityInCombos( combo.getQuantity() );
                        splitProduct.getComboOccurrences().add(combo.getId());

                        //adjust the combo detail's selected product to this new product
                        comboDetail.setSelectedProduct(splitProduct);

                        //add the new product to the product list
                        productCollection.addToCollectionSplit( splitProduct, false );

                        //adjust the quantity of the original selected product
                        selectProduct.setQuantity( selectedProduct.getComboOccurrences(combo.getId()) );

                        selectProduct = splitProduct;
                        selectedProduct = selectProduct;

                        selectedProduct.setComboTransLineItemId( nextComboLineIndex );

                        if(originalIndex != null && originalIndex < productCollection.getCollection().size()) {
                            for(ProductCalculation productCalculation : productCollection.getCollection()) {
                                if(productCalculation.getOriginalIndex().equals(originalIndex) && !productCalculation.getIndex().equals(originalIndex)) {
                                    originalIndex = productCalculation.getIndex();
                                    break;
                                }
                            }
                            selectedProduct.setIndex(originalIndex);
                            selectedProduct.setQuantity(combo.getQuantity());
                            productCollection.getCollection().set(originalIndex, selectedProduct);
                        }
                    }

                    //update product with comboDetailId, basePrice, comboPrice, upcharge
                    selectedProduct.setComboDetailId( comboDetail.getId() );
                    selectedProduct.setBasePrice(comboDetail.getBasePrice());
                    selectedProduct.setComboPrice(comboDetail.getComboPrice());

                    BigDecimal currentPrice = selectedProduct.getCurrentPrice(true, true, null, null);
                    BigDecimal upcharge = currentPrice.subtract(comboDetail.getBasePrice().multiply(selectedProduct.getQuantity()));
                    if(upcharge.compareTo(BigDecimal.ZERO) != 0 && selectedProduct.getQuantity().compareTo(BigDecimal.ONE) > 0) {
                        upcharge = upcharge.divide(selectedProduct.getQuantity());
                    }
                    selectedProduct.setUpcharge( upcharge );
                }

                //decrement to next index
                nextComboLineIndex--;

            }
        }
    }

    public void attemptBuildCombos() throws Exception {
        //no point in checking combos if there is only one potential detail
        if ( this.getPotentialComboDetails().size() < 2 ) {
            return;
        }

        //foreach combo
        for ( ComboModel comboModel : this.getCombos() ) {
            //if less than 2 potential combo details are left, no more combos will be made
            if ( this.getPotentialComboDetails().size() < 2 ) {
                break;
            }

            //check if the combo has enough eligible products to be considered
            if ( !getComboProductEligibleCount().containsKey( comboModel.getId() ) || getComboProductEligibleCount().get( comboModel.getId() ).compareTo(BigDecimal.valueOf(comboModel.getDetails().size())) < 0 ) {
                continue;
            }

            //if so attempt to build the combo ss many times as possible
            boolean attemptBuildCombo = true;

            Integer loopCount = 0;
            while ( attemptBuildCombo ) {
                loopCount ++;
                attemptBuildCombo = this.attemptBuildCombo( comboModel );

                if(loopCount > 100) {
                    Logger.logMessage("ERROR: Could not build combo. The same keypad may be assigned to more than one combo detail in a combo in ComboCollectionCaluclation.attemptBuildCombos - tried 100 times.. giving up...", Logger.LEVEL.ERROR);
                    attemptBuildCombo = false;
                }
            }
        }
    }

    public boolean attemptBuildCombo(ComboModel comboModel) throws Exception {
        ComboCalculation comboCalculation = new ComboCalculation( comboModel );

        comboCalculation.attemptFillCombo( this.getPotentialComboDetails() );

        if ( !comboCalculation.isComboFilled() ) {
            return false;
        }

        //add to the list of built combos
        this.addComboToCollection( comboCalculation );

        return true;
    }

    //add a combo to the collection, checking for duplicates
    public void addComboToCollection(ComboCalculation comboToAdd) throws Exception {
        boolean dupeFound = false;

        //check for duplicates
        if ( this.getCollection().size() > 0 ) {
            for ( ComboCalculation comboCalculation : this.getCollection() ) {
                //if not the same combo, skip checking for dupes
                if ( !comboCalculation.getId().equals( comboToAdd.getId() ) ) {
                    continue;
                }

                //same combo, need to check the individual products
                HashMap<Integer, ComboDetailModel> existingComboSelectedProducts = comboCalculation.getSelectedProductsHM();
                HashMap<Integer, ComboDetailModel> comboToAddSelectedProducts = comboToAdd.getSelectedProductsHM();
                Integer matchingProductCount = 0;

                for ( Map.Entry<Integer, ComboDetailModel> entry : existingComboSelectedProducts.entrySet() ) {
                    ProductCalculation existingProduct = entry.getValue().getSelectedProduct();
                    ProductCalculation comboToAddProduct = comboToAddSelectedProducts.get( entry.getKey() ).getSelectedProduct();

                    //if not the same product, not a dupe
                    if ( !existingProduct.getId().equals( comboToAddProduct.getId() ) ) {
                        break;
                    }

                    if ( ProductCollectionCalculation.productsHaveSameModifiers( existingProduct, comboToAddProduct ) && ProductCollectionCalculation.productsHaveSamePrepOption( existingProduct, comboToAddProduct ) ) {
                        matchingProductCount++;
                    }
                }

                //if not all products, matched, not a duplicate
                if ( matchingProductCount < existingComboSelectedProducts.size() ) {
                    continue;
                }

                //all products match, this combo is a duplicate, add quantity rather than adding new combo to the list
                dupeFound = true;
                Logger.logMessage("Newly created Combo " + comboToAdd.getComboModel().getName() + " is a duplicate of an existing with the same products, incrementing quantity of existing combo", Logger.LEVEL.TRACE);
                comboCalculation.incrementQuantity();

                //adjust quantity of product lines for dupe
                for ( Map.Entry<Integer, ComboDetailModel> entry : existingComboSelectedProducts.entrySet() ) {
                    ProductCalculation existingProduct = entry.getValue().getSelectedProduct();
                    ProductCalculation comboToAddProduct = comboToAddSelectedProducts.get( entry.getKey() ).getSelectedProduct();

                    //if the indexes are not equal, move a product from the add
                    if ( !existingProduct.getIndex().equals( comboToAddProduct.getIndex() ) ) {
                        existingProduct.incrementQuantity();
                        existingProduct.incrementQuantityInCombo();
                        comboToAddProduct.decrementQuantityInCombo();
                        comboToAddProduct.decrementQuantity();

                        existingProduct.getComboOccurrences().add(comboCalculation.getComboModel().getId());
                        List<Integer> comboOccurrences = comboToAddProduct.getComboOccurrences();
                        comboOccurrences.remove(comboCalculation.getComboModel().getId());
                    }
                }
            }
        }

        //if no dupe is found, add the combo to the list
        if ( !dupeFound ) {
            this.getCollection().add( comboToAdd );
        }
    }

    public void incrementComboCount(Integer comboId, BigDecimal productQuantity) throws Exception {
        if ( comboId == null || comboId == 0 ) {
            throw new MissingDataException("Error determining combo counts.");
        }

        BigDecimal comboCount = BigDecimal.ZERO;

        if ( this.getComboProductEligibleCount().containsKey(comboId) ) {
            comboCount = this.getComboProductEligibleCount().get(comboId);
        }

        BigDecimal quantity = productQuantity.add(comboCount);

        this.getComboProductEligibleCount().put(comboId, quantity);
    }

    private Integer determineNextTemporaryComboLineIndex(ProductCollectionCalculation productCollection) {
        Integer index = 0;

        //find the lowest comboTransLineItemID from all the products using this combo, the next combo will be the next lowest
        for ( ComboCalculation combo : this.getCollection() ) {
            Integer lowestNum = null;

            for(ProductCalculation product : productCollection.getCollection() ) {
                if(product.getComboTransLineItemId() != null && combo.getComboModel().getDetailIds().contains(product.getComboDetailId())) {

                    if(lowestNum == null || product.getComboTransLineItemId() < lowestNum) {
                        lowestNum = product.getComboTransLineItemId();
                    }
                }
            }
            combo.setTemporaryComboLineIndex(lowestNum);
        }

        for ( ComboCalculation combo : this.getCollection() ) {
            if ( combo.getTemporaryComboLineIndex() == null ) {
                continue;
            }

            if ( combo.getTemporaryComboLineIndex() < index ) {
                index = combo.getTemporaryComboLineIndex();
            }
        }

        return ( index - 1 );
    }

    public Integer getDiscountAppliedToCombo(Integer comboDetailID) {
        Integer comboPADiscountID = null;

        for(ComboCalculation comboCalculation : this.getCollection()) {

            Integer comboDiscountID = comboCalculation.getComboModel().getPaDiscountId();

            for(ComboDetailModel comboDetailModel : comboCalculation.getComboModel().getDetails()) {

                if(comboDetailModel.getId().equals(comboDetailID)) {
                    comboPADiscountID = comboDiscountID;
                    break;
                }
            }
        }
        return comboPADiscountID;
    }

    public List<ComboCalculation> getCollection() {
        return collection;
    }

    public void setCollection(List<ComboCalculation> collection) {
        this.collection = collection;
    }

    public List<ComboModel> getCombos() {
        return combos;
    }

    public void setCombos(List<ComboModel> combos) {
        this.combos = combos;
    }

    public HashMap<Integer, BigDecimal> getComboProductEligibleCount() {
        return comboProductEligibleCount;
    }

    public void setComboProductEligibleCount(HashMap<Integer, BigDecimal> comboProductEligibleCount) {
        this.comboProductEligibleCount = comboProductEligibleCount;
    }

    public List<ComboDetailModel> getPotentialComboDetails() {
        return potentialComboDetails;
    }

    public void setPotentialComboDetails(List<ComboDetailModel> potentialComboDetails) {
        this.potentialComboDetails = potentialComboDetails;
    }
}
