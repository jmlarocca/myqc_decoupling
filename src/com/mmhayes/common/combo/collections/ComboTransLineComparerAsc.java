package com.mmhayes.common.combo.collections;

import com.mmhayes.common.transaction.models.ProductLineItemModel;

import java.util.Comparator;

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-03-01 16:49:04 -0500 (Fri, 01 Mar 2019) $: Date of last commit
 $Rev: 8470 $: Revision of last commit
*/
public class ComboTransLineComparerAsc implements Comparator<ProductLineItemModel> {

    public int compare(ProductLineItemModel productLineA, ProductLineItemModel productLineB){

        if(productLineB.getComboTransLineItemId() == null && productLineA.getComboTransLineItemId() == null) {
            return 0;

        } else if(productLineB.getComboTransLineItemId() == null && productLineA.getComboTransLineItemId() != null) {
            return 1;

        } else if(productLineB.getComboTransLineItemId() != null && productLineA.getComboTransLineItemId() == null) {
            return -1;

        } else {
            return productLineB.getComboTransLineItemId().compareTo(productLineA.getComboTransLineItemId());
        }

    }
}