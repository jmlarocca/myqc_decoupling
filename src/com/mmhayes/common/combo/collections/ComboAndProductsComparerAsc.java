package com.mmhayes.common.combo.collections;

import com.mmhayes.common.transaction.models.ProductLineItemModel;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/*
 Last Updated (automatically updated by SVN)
 $Author: gematuszyk $: Author of last commit
 $Date: 2019-03-01 16:49:04 -0500 (Fri, 01 Mar 2019) $: Date of last commit
 $Rev: 8470 $: Revision of last commit
*/
public class ComboAndProductsComparerAsc implements Comparator<HashMap<Integer, List<ProductLineItemModel>>> {

    public int compare(HashMap<Integer, List<ProductLineItemModel>> hashMapA, HashMap<Integer, List<ProductLineItemModel>> hashMapB) {

        Integer intA = null;
        Integer intB = null;

        for(Integer key : hashMapA.keySet()) {
            intA = key;
        }

        for(Integer key : hashMapB.keySet()) {
            intB = key;
        }

        if(intA != null && intB != null) {
            return intA.compareTo(intB);
        }

        return 0;
    }

}