package com.mmhayes.common.disclaimer.collections;

import com.mmhayes.common.dataaccess.DataManager;
import com.mmhayes.common.disclaimer.models.DisclaimerModel;
import com.mmhayes.common.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mmhayes on 9/21/2020.
 */
public class DisclaimerCollection {

    private List<DisclaimerModel> collection = new ArrayList<>();
    private static DataManager dm = new DataManager();

    public DisclaimerCollection(){

    }


    public DisclaimerCollection getAllDisclaimersByProductId(Integer papluId) throws Exception {
        try{
//            DataManager dm = new DataManager();
//            DisclaimerCollection disclaimerCollection = new DisclaimerCollection();

            //get all models in an array list
            ArrayList<HashMap> disclaimerList = dm.parameterizedExecuteQuery("data.common.getAllDisclaimersByProductId",
                    new Object[]{papluId},
                    true
            );

            for (HashMap disclaimerHM : disclaimerList) {
                DisclaimerModel disclaimerModel = new DisclaimerModel(disclaimerHM);
                this.getCollection().add(disclaimerModel);  //create a new model and add to the collection
            }

            return this;
        }
        catch(Exception e){
            Logger.logException(e);
            return new DisclaimerCollection();
        }
    }

    public DisclaimerCollection getAllDisclaimersByStoreId(Integer storeID) throws Exception {
        try{

            //get all models in an array list
            ArrayList<HashMap> disclaimerList = dm.parameterizedExecuteQuery("data.common.getAllDisclaimersByStoreId",
                    new Object[]{storeID},
                    true
            );

            for (HashMap disclaimerHM : disclaimerList) {
                DisclaimerModel donationModel = new DisclaimerModel(disclaimerHM);
                this.getCollection().add(donationModel);  //create a new model and add to the collection
            }

            return this;
        }
        catch(Exception e){
            Logger.logException(e);
            return new DisclaimerCollection();
        }
    }

    public List<DisclaimerModel> getCollection() {
        return collection;
    }

    public void setCollection(List<DisclaimerModel> collection) {
        this.collection = collection;
    }
}
