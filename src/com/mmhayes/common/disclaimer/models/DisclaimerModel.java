package  com.mmhayes.common.disclaimer.models;


import com.mmhayes.common.api.CommonAPI;
import com.mmhayes.common.utils.Logger;
import com.mmhayes.common.utils.StringFunctions;

import java.util.HashMap;
import java.util.Objects;

public class DisclaimerModel {

    private Integer id = null;
    private Integer type = null;
    private String name;
    private String title;
    private String text;

    public DisclaimerModel() {

    }

    public DisclaimerModel(HashMap modelDetailHM) throws Exception{
        setModelProperties(modelDetailHM);
    }

    public DisclaimerModel setModelProperties(HashMap modelDetailHM) throws Exception {

        try{

            setId(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DISCLAIMERID")));
            setType(CommonAPI.convertModelDetailToInteger(modelDetailHM.get("DISCLAIMERTYPEID")));
            setName(CommonAPI.convertModelDetailToString(modelDetailHM.get("NAME")));
            setTitle(CommonAPI.convertModelDetailToString(modelDetailHM.get("DISCLAIMERTITLE")));
            setText(CommonAPI.convertModelDetailToString(modelDetailHM.get("DISCLAIMERDESCRIPTION")));

            return this;
        }
        catch(Exception e){
            Logger.logException(e);
            return this;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    /**
     * Overridden toString() method for a DisclaimerModel.
     * @return a {@link String} representation of a DisclaimerModel.
     */
    @Override
    public String toString () {

        return String.format("ID: %s, TYPE: %s, NAME: %s, TITLE: %s, TEXT: %s",
        Objects.toString((id != null && id > 0 ? id : "N/A"), "N/A"),
        Objects.toString((type != null && type > 0 ? type : "N/A"), "N/A"),
        Objects.toString(StringFunctions.stringHasContent(name) ? name : "N/A", "N/A"),
        Objects.toString(StringFunctions.stringHasContent(title) ? title : "N/A", "N/A"),
        Objects.toString(StringFunctions.stringHasContent(text) ? text : "N/A", "N/A"));

    }

}