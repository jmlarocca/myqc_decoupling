package com.mmhayes.common.transaction.models;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2017-04-20 16:34:33 -0400 (Thu, 20 Apr 2017) $: Date of last commit
 $Rev: 3849 $: Revision of last commit
*/
public interface ITenderModel {

    public Integer getId();
    public String getName();
}
