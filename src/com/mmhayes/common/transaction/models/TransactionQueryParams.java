package com.mmhayes.common.transaction.models;

//MMHayes Dependencies

import com.mmhayes.common.api.errorhandling.exceptions.MissingDataException;
import com.mmhayes.common.utils.Logger;

//API Dependencies
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.text.SimpleDateFormat;

//Other Dependencies
import org.apache.commons.lang3.StringUtils;

/*
 Last Updated (automatically updated by SVN)
 $Author: eglundin $: Author of last commit
 $Date: 2018-12-31 09:02:53 -0500 (Mon, 31 Dec 2018) $: Date of last commit
 $Rev: 8299 $: Revision of last commit
*/
public class TransactionQueryParams {

    private boolean openOnly = false;
    private Integer terminalId = null;
    private Integer employeeId = null;
    private String startDate = null;
    private String endDate = null;

    //populates the passed in paginationParamsHM with HTTP GET parameters in the uriInfo
    public TransactionQueryParams populateParamFromURI(@Context UriInfo uriInfo) throws Exception {
        TransactionQueryParams transactionQueryParams = new TransactionQueryParams();

        try {
            MultivaluedMap<String, String> httpGetParams = uriInfo.getQueryParameters();
            if (httpGetParams != null
                    && !httpGetParams.isEmpty()) {

                if (httpGetParams.get("openonly") != null
                        && httpGetParams.get("openonly").get(0) != null
                        && !httpGetParams.get("openonly").get(0).toString().isEmpty()
                        && httpGetParams.get("openonly").get(0).toString().equalsIgnoreCase("yes")) {

                    transactionQueryParams.setOpenOnly(true);
                }

                //Start Date
                if (httpGetParams.get("startdate") != null
                        && httpGetParams.get("startdate").get(0) != null
                        && !httpGetParams.get("startdate").get(0).toString().isEmpty()) {

                    if (httpGetParams.get("startdate").get(0).toString().length() != 8) {
                        throw new MissingDataException("Invalid Start Date Parameter");
                    }

                    if (!StringUtils.isNumeric(httpGetParams.get("startdate").get(0).toString())) {
                        throw new MissingDataException("Invalid Start Date Parameter");
                    }

                    transactionQueryParams.setStartDate(httpGetParams.get("startdate").get(0).toString());

                    String tempStartDate = transactionQueryParams.getStartDate().substring(0, 2) + "/" + transactionQueryParams.getStartDate().substring(2, 4) + "/" + transactionQueryParams.getStartDate().substring(4, 8);
                    transactionQueryParams.setStartDate(tempStartDate);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        dateFormat.parse(transactionQueryParams.getStartDate());
                    } catch (Exception ex) {
                        throw new MissingDataException("Invalid Start Date Parameter");
                    }
                }

                //End Date
                if (httpGetParams.get("enddate") != null
                        && httpGetParams.get("enddate").get(0) != null
                        && !httpGetParams.get("enddate").get(0).toString().isEmpty()) {

                    if (httpGetParams.get("enddate").get(0).toString().length() != 8) {
                        throw new MissingDataException("Invalid End Date Parameter");
                    }

                    if (!StringUtils.isNumeric(httpGetParams.get("enddate").get(0).toString())) {
                        throw new MissingDataException("Invalid End Date Parameter");
                    }

                    transactionQueryParams.setEndDate(httpGetParams.get("enddate").get(0).toString());

                    String tempEndDate = transactionQueryParams.getEndDate().substring(0, 2) + "/" + transactionQueryParams.getEndDate().substring(2, 4) + "/" + transactionQueryParams.getEndDate().substring(4, 8);
                    transactionQueryParams.setEndDate(tempEndDate);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        dateFormat.parse(transactionQueryParams.getEndDate());
                    } catch (Exception ex) {
                        throw new MissingDataException("Invalid End Date Parameter");
                    }
                }

                //Terminal ID
                if (httpGetParams.get("terminalid") != null
                        && httpGetParams.get("terminalid").get(0) != null
                        && !httpGetParams.get("terminalid").get(0).toString().isEmpty()) {

                    if (!StringUtils.isNumeric(httpGetParams.get("terminalid").get(0).toString())) {
                        throw new MissingDataException("Invalid Terminal ID Parameter");
                    }
                    transactionQueryParams.setTerminalId(Integer.parseInt(httpGetParams.get("terminalid").get(0).toString()));
                }

                //Employee ID
                if (httpGetParams.get("employeeid") != null
                        && httpGetParams.get("employeeid").get(0) != null
                        && !httpGetParams.get("employeeid").get(0).toString().isEmpty()) {

                    if (!StringUtils.isNumeric(httpGetParams.get("employeeid").get(0).toString())) {
                        throw new MissingDataException("Invalid Employee ID Parameter");
                    }

                    transactionQueryParams.setEmployeeId(Integer.parseInt(httpGetParams.get("employeeid").get(0).toString()));
                }
            }

        } catch (Exception ex) {
            Logger.logException(ex); //always log exceptions
            throw ex;
        }
        return transactionQueryParams;
    }

    public boolean isOpenOnly() {
        return openOnly;
    }

    public void setOpenOnly(boolean openOnly) {
        this.openOnly = openOnly;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }
}
